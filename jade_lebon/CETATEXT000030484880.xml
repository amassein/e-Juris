<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030484880</ID>
<ANCIEN_ID>JG_L_2015_04_000000371309</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/48/48/CETATEXT000030484880.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème / 4ème SSR, 15/04/2015, 371309</TITRE>
<DATE_DEC>2015-04-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>371309</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème / 4ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP HEMERY, THOMAS-RAQUIN ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>Mme Marie Gautier-Melleray</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:371309.20150415</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La société Cobat a demandé au tribunal administratif de Rennes d'annuler pour excès de pouvoir l'arrêté du 3 septembre 2008 du maire de Perros-Guirec (Côtes d'Armor) refusant de proroger la validité du permis de construire qui lui a été délivré le 29 septembre 2006 pour la réalisation d'un immeuble collectif sur une parcelle située 10 rue du général Leclerc, ainsi que la décision rejetant son recours gracieux. Par un jugement n° 0900596 du 8 décembre 2011, le tribunal administratif de Rennes a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 12NT00477 du 14 juin 2013, la cour administrative d'appel de Nantes a, sur la requête de la société Cobat, annulé ce jugement ainsi que l'arrêté du maire de Perros-Guirec du 3 septembre 2008.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 16 août et 18 novembre 2013 et le 31 octobre 2014 au secrétariat du contentieux du Conseil d'Etat, la commune de Perros-Guirec demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la société Cobat ; <br/>
<br/>
              3°) de mettre à la charge de la société Cobat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              le code de l'urbanisme ;<br/>
<br/>
              le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Gautier-Melleray, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la commune de Perros-Guirec et à la SCP Hémery, Thomas-Raquin, avocat de la société Cobat ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 25 mars 2015, présentée pour la commune de Perros-Guirec ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 29 septembre 2006, le maire de Perros-Guirec (Côtes d'Armor) a délivré à la société Cobat un permis de construire l'autorisant à édifier un bâtiment comprenant dix logements et un local commercial sur une parcelle que M. A...s'était engagé à lui céder, sous certaines conditions, par un acte sous seing privé du 2 août 2005 ; que, par un arrêté du 3 septembre 2008, le maire a rejeté une demande de la société tendant à ce que la validité de ce permis soit prorogée d'une année, au motif qu'elle n'était pas devenue propriétaire de la parcelle et qu'elle ne disposait plus d'un titre l'habilitant à construire ; que la société Cobat a saisi le tribunal administratif de Rennes d'un recours pour excès de pouvoir dirigé contre cet arrêté, qui a été rejeté par un jugement du 8 décembre 2011 ; que la commune de Perros-Guirec se pourvoit en cassation contre l'arrêt du 14 juin 2013 par lequel la cour administrative d'appel de Nantes, statuant sur l'appel de la société, a annulé ce jugement ainsi que l'arrêté litigieux ; <br/>
<br/>
              2. Considérant, d'une part, qu'aux termes de l'article R. 423-1 du code de l'urbanisme: " Les demandes de permis de construire, d'aménager ou de démolir et les déclarations préalables sont adressées (...) ou déposées à la mairie de la commune dans laquelle les travaux sont envisagés : a) Soit par le ou les propriétaires du ou des terrains, leur mandataire ou par une ou plusieurs personnes attestant être autorisées par eux à exécuter les travaux ; / b) Soit, en cas d'indivision, par un ou plusieurs co-indivisaires ou leur mandataire ; / c) Soit par une personne ayant qualité pour bénéficier de l'expropriation pour cause d'utilité publique " ; que l'article R. 431-5 du même code prévoit que la demande de permis de construire comporte notamment " l'attestation du ou des demandeurs qu'ils remplissent les conditions définies à l'article R. 423-1 pour déposer une demande de permis " ; <br/>
<br/>
              3. Considérant, d'autre part, qu'en vertu de l'article R. 424-17 du même code, le permis de construire est périmé si les travaux ne sont pas entrepris  dans le délai de deux ans à compter de la notification d'un permis exprès ou de la naissance d'un permis tacite ; qu'aux termes de l'article R. 424-21, le permis " peut être prorogé pour une année, sur demande de son bénéficiaire si les prescriptions d'urbanisme et les servitudes administratives de tous ordres auxquelles est soumis le projet n'ont pas évolué de façon défavorable à son égard " ; qu'aux termes de l'article R. 424-22 : " La demande de prorogation est établie en deux exemplaires et adressée par pli recommandé ou déposée à la mairie deux mois au moins avant l'expiration du délai de validité " ;  <br/>
<br/>
              4. Considérant qu'il résulte des dispositions précitées des articles R. 424-21 et R. 424-22 du code de l'urbanisme, issues du décret du 5 janvier 2007 pris pour l'application de l'ordonnance n° 2005-1527 du 8 décembre 2005 relative au permis de construire et aux autorisation d'urbanisme, que l'autorité compétente ne peut légalement refuser de faire droit à une demande de prorogation d'un permis de construire présentée deux mois au moins avant l'expiration de son délai de validité que si les règles d'urbanisme et les servitudes administratives de tous ordres s'imposant au projet ont été modifiées, postérieurement à la délivrance du permis de construire, dans un sens qui lui est défavorable ; qu'aucune disposition n'impose qu'une demande de prorogation soit accompagnée d'une attestation du demandeur selon laquelle il continue de remplir les conditions définies à l'article R. 423-1 du même code pour solliciter un permis de construire ; qu'ainsi, en jugeant que le maire de Perros-Guirec n'avait pu légalement, par son arrêté du 3 septembre 2008, rejeter la demande de prorogation présentée par la société Cobat au motif que cette société n'avait plus qualité pour mettre en oeuvre le permis de construire dont elle était titulaire, la cour administrative d'appel, qui n'a pas commis d'erreur de droit en se fondant sur les dispositions en vigueur à la date de cet arrêté, a fait de ces dispositions une exacte application ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que la commune de Perros-Guirec n'est pas fondée à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Perros-Guirec la somme de 3 000 euros à verser à la société Cobat, au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que les mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société Cobat, qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
		Article 1er : Le pourvoi de la commune de Perros-Guirec est rejeté.<br/>
<br/>
Article 2 : La commune de Perros-Guirec versera à la société Cobat une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la commune de Perros-Guirec et à la société Cobat. <br/>
Copie en sera adressée pour information à la ministre de l'écologie, du développement durable et de l'énergie. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-03-04-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. RÉGIME D'UTILISATION DU PERMIS. PROROGATION. - CONDITIONS - PRODUCTION PAR LE PÉTITIONNAIRE D'UNE ATTESTATION SELON LAQUELLE IL A QUALITÉ POUR PRÉSENTER LA DEMANDE - ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 68-03-04-02 Il résulte des articles R. 424-21 et R. 424-22 du code de l'urbanisme, issus du décret n° 2007-18 du 5 janvier 2007 pris pour l'application de l'ordonnance n° 2005-1527 du 8 décembre 2005, que l'autorité compétente ne peut légalement refuser de faire droit à une demande de prorogation d'un permis de construire présentée deux mois au moins avant l'expiration de son délai de validité que si les règles d'urbanisme et les servitudes administratives de tous ordres s'imposant au projet ont été modifiées, postérieurement à la délivrance du permis de construire, dans un sens qui lui est défavorable. Aucune disposition n'impose qu'une demande de prorogation soit accompagnée d'une attestation du demandeur selon laquelle il continue de remplir les conditions définies à l'article R. 423-1 du code de l'urbanisme pour solliciter un permis de construire.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Ab. jur. CE, 1er octobre 1993, Commune de Boos, n° 115703, T. p. 584.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
