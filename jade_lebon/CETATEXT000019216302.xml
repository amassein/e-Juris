<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000019216302</ID>
<ANCIEN_ID>JG_L_2008_07_000000300304</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/19/21/63/CETATEXT000019216302.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Section du Contentieux, 18/07/2008, 300304, Publié au recueil Lebon</TITRE>
<DATE_DEC>2008-07-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>300304</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Section du Contentieux</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Stirn</PRESIDENT>
<AVOCATS>SCP VIER, BARTHELEMY, MATUCHANSKY</AVOCATS>
<RAPPORTEUR>Mme Christine  Grenier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Derepas Luc</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 3 janvier et 26 mars 2007 au secrétariat du contentieux du Conseil d'Etat, présentés pour la FEDERATION DE L'HOSPITALISATION PRIVEE ; la FEDERATION DE L'HOSPITALISATION PRIVEE demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2006-1332 du 2 novembre 2006 relatif aux contrats pluriannuels d'objectifs et de moyens et modifiant le code de la santé publique ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment le Préambule et l'article 34 ;<br/>
<br/>
              Vu le code de la santé publique ;<br/>
<br/>
              Vu le code de la sécurité sociale ;<br/>
<br/>
              Vu l'ordonnance n° 2003-850 du 4 septembre 2003 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Christine Grenier, chargée des fonctions de Maître des Requêtes,<br/>
<br/>
              - les observations de la SCP Vier, Barthélemy, Matuchansky, avocat de la FEDERATION DE L'HOSPITALISATION PRIVEE,<br/>
<br/>
              - les conclusions de M. Luc Derepas, Commissaire du gouvernement ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant que l'article 7 de l'ordonnance du 4 septembre 2003 a inséré dans le code de la santé publique les articles L. 6114-1 à L. 6114-5 relatifs aux contrats pluriannuels d'objectifs et de moyens conclus par les agences régionales de l'hospitalisation avec les établissements de santé ; que l'article L. 6114-5 renvoie à un décret le soin de définir les conditions d'application de ces articles ; que le décret attaqué du 2 novembre 2006 insère à cette fin, dans la partie réglementaire de ce code, les dispositions figurant, d'une part, aux articles R. 6114-10 à R. 6114-13 et, d'autre part, aux articles D. 6114-1 à D. 6114-9 ;<br/>
<br/>
              Sur la légalité externe :<br/>
<br/>
              Considérant qu'il ne résulte ni de l'article L. 161-37 du code de la sécurité sociale relatif aux missions de la Haute autorité de santé, ni d'aucune autre disposition que le Gouvernement aurait été tenu de la consulter avant de prendre le décret attaqué ; que, par suite, le moyen tiré du défaut de consultation de la Haute autorité de santé ne peut qu'être écarté ;<br/>
<br/>
              Sur les dispositions relatives aux sanctions :<br/>
<br/>
              Considérant que l'article R. 6114-11 du code de la santé publique issu du décret attaqué autorise le directeur de l'agence régionale de l'hospitalisation à suspendre le contrat pluriannuel d'objectifs et de moyens lorsqu'il est constaté un manquement grave du titulaire de l'autorisation aux dispositions législatives ou réglementaires ou à ses obligations contractuelles ; qu'en vertu de l'article R. 6114-12, la résiliation du contrat peut être prononcée par la commission exécutive établie par l'article L. 6115-2 de ce même code s'il n'a pas été mis fin au manquement constaté à l'expiration du délai de suspension ; que l'article R. 6114-13 dispose que la commission exécutive peut prononcer une pénalité financière à l'encontre du titulaire de l'autorisation lorsqu'il est constaté qu'un engagement figurant au contrat n'a pas été exécuté ;<br/>
<br/>
              En ce qui concerne le moyen tiré de la méconnaissance du principe de légalité des délits et des peines :<br/>
<br/>
              Considérant que lorsque la définition des obligations auxquelles est soumis l'exercice d'une activité relève du législateur en application de l'article 34 de la Constitution, il n'appartient qu'à la loi de fixer, le cas échéant, le régime des sanctions administratives dont la méconnaissance de ces obligations peut être assortie et, en particulier, de déterminer tant les sanctions encourues que les éléments constitutifs des infractions que ces sanctions ont pour objet de réprimer ; que la circonstance que la loi ait renvoyé au décret le soin de définir ses modalités ou ses conditions d'application n'a ni pour objet ni pour effet d'habiliter le pouvoir réglementaire à intervenir dans le domaine de la loi pour définir ces éléments ;<br/>
<br/>
              Considérant, en premier lieu, que l'article R. 6114-11 du code de la santé publique issu du décret attaqué reprend les termes mêmes de l'article L. 6114-1 de ce code selon lesquels le contrat peut être suspendu ou résilié « en cas de manquement grave du titulaire de l'autorisation aux dispositions législatives et réglementaires ou à ses obligations contractuelles », tout en précisant la procédure applicable ; que la résiliation du contrat encourue, en application de R. 6114-12, par l'établissement qui n'a pas mis fin au manquement reproché, a pour effet, en application de l'article L. 6122-8 du même code, de permettre à l'agence régionale de l'hospitalisation de fixer unilatéralement tant les objectifs quantifiés d'activité des établissements que les pénalités financières auxquelles ils s'exposent en cas de non respect de ces objectifs ; que les conditions d'exercice de l'activité de ces établissements relèvent, en vertu de l'article 34 de la Constitution, de la loi ; qu'il n'appartient dès lors qu'à la loi de déterminer les éléments constitutifs des infractions dont l'auteur encourt de telles sanctions ; que par suite, le moyen tiré de ce que la définition reprise dans le décret attaqué méconnaîtrait en raison de son imprécision le principe de légalité des délits et des peines ne peut utilement être invoqué à l'encontre de ce décret ;<br/>
<br/>
              Considérant, en deuxième lieu, que l'article R. 6114-13 du code de la santé publique issu du décret attaqué, qui définit les conditions d'application des pénalités prévues par l'article L. 6114-1 de ce même code lorsqu'il est constaté qu'un engagement figurant au contrat n'a pas été exécuté, précise la procédure applicable et détermine le montant maximal de la pénalité financière qui peut être prononcée dans ce cas ; qu'il dispose que cette pénalité doit être proportionnée à la gravité du manquement constaté et précise que l'infraction est déterminée par référence aux engagements du contrat pluriannuel d'objectifs et de moyens ; que, contrairement à ce que soutient la requérante, ces dispositions réglementaires déterminent ainsi, avec une précision suffisante, les modalités d'application des prescriptions législatives qui ont prévu la suspension ou la résiliation du contrat en cas de méconnaissance par le titulaire de l'autorisation de ses obligations contractuelles ;<br/>
<br/>
              En ce qui concerne les autres moyens :<br/>
<br/>
              Considérant, en premier lieu, qu'il résulte de l'article L. 6114-1 du code de la santé publique que le contrat pluriannuel d'objectifs et de moyens est conclu entre les agences régionales de l'hospitalisation et les établissements de santé ; que ces contrats n'engagent que leurs seuls signataires ; qu'en vertu du même article, les sanctions qu'il prévoit ne peuvent être prononcées qu'à l'encontre du titulaire de l'autorisation, lequel doit s'entendre comme l'établissement de santé privé qui a conclu le contrat pluriannuel d'objectifs et de moyens ; que, par suite, le moyen tiré de ce que le décret attaqué méconnaîtrait le principe de l'effet relatif des contrats ne peut qu'être écarté ;<br/>
<br/>
              Considérant, en second lieu, qu'il résulte des termes mêmes de l'article L. 6114-1 du code de la santé publique que la loi a prévu, d'une part, la résiliation ou la suspension du contrat en cas de manquement grave du titulaire de l'autorisation, notamment à ses obligations contractuelles et, d'autre part, la possibilité de prononcer une pénalité financière à l'encontre du titulaire de l'autorisation en cas d'inexécution partielle ou totale de ses obligations contractuelles ; que, dès lors, le moyen tiré de ce que le décret attaqué, qui définit les conditions d'application des différents types de sanction prévus par loi, porterait atteinte au principe de non cumul de sanctions à raison de mêmes faits ne saurait être accueilli ;<br/>
<br/>
              Sur l'article D. 6114-3 :<br/>
<br/>
              Considérant que, selon l'article L. 6114-3 du code de la santé publique « les contrats définissent les objectifs en matière de qualité et de sécurité des soins  (...) » ; que l'article D. 6114-3, tel qu'il résulte du décret attaqué, dispose que « Le contrat pluriannuel d'objectifs et de moyens fixe (...) les objectifs relatifs à la sécurité des soins et à l'amélioration continue de la qualité, notamment en ce qui concerne : (...) 4° Le développement de l'évaluation des pratiques professionnelles mentionnée aux articles L. 4133-1-1 et  L. 6113-2 » ;<br/>
<br/>
              Considérant que l'évaluation des pratiques professionnelles tend à garantir l'amélioration de la qualité des soins, qui constitue l'un des objectifs assignés par la loi aux contrats pluriannuels d'objectifs et de moyens ; qu'en prévoyant qu'il fixe des objectifs relatifs au développement de l'évaluation des pratiques professionnelles, l'article D. 6114-3, en particulier son 4°, n'est pas allé au-delà de ce que prévoient les dispositions citées plus haut de l'article L. 6114-3 ; qu'il ne méconnaît pas non plus les dispositions de l'article L. 4133-1-1 du code de la santé publique, en vertu desquelles l'évaluation individuelle des pratiques professionnelles constitue une obligation pour les médecins libéraux, y compris ceux qui exercent dans les établissements de santé privés ; qu'il résulte de ce qui précède que la fédération requérante n'est pas fondée à soutenir que l'article D. 6114-3 est entaché d'illégalité ;<br/>
<br/>
              Sur l'article D. 6114-5 :<br/>
<br/>
              Considérant qu'aux termes de l'article L. 6114-1 du code de la santé publique : « (...) Les contrats fixent les éléments nécessaires à leur mise en oeuvre, le calendrier d'exécution et mentionnent les indicateurs de suivi et de résultats nécessaires à leur évaluation périodique (...) » ; que l'article L. 6114-3 dispose que les contrats définissent des objectifs en matière de qualité et de sécurité des soins ; qu'aux termes de l'article L. 6113-1 du même code : « Afin de dispenser des soins de qualité, les établissements de santés, publics ou privés, sont tenus de disposer des moyens adéquats et de procéder à l'évaluation de leur activité » ;<br/>
<br/>
              Considérant que l'article D. 6114-5, issu du décret attaqué, dispose que : « Le contrat pluriannuel d'objectifs et de moyens prévoit des engagements précis et mesurables du titulaire de l'autorisation en vue d'améliorer le service rendu au patient et de renforcer l'efficacité de sa gestion par une meilleure utilisation de ses ressources et le développement d'outils de gestion. Le contrat peut prévoir des actions d'accompagnement et des mesures d'intéressement aux résultats constatés » ; que ces mesures, qui tendent à garantir la qualité des soins dispensés aux patients, n'excèdent pas les limites fixées par les dispositions précitées du code de la santé publique ; qu'il ne saurait être sérieusement soutenu que ces dispositions porteraient une atteinte excessive au principe de la liberté du commerce et de l'industrie ; que les moyens tirés de l'illégalité de l'article D. 6114-5 ne peuvent, dès lors, qu'être écartés ;<br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que la requête de la FEDERATION DE L'HOSPITALISATION PRIVEE doit être rejetée, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées au même titre par le ministre de la santé, de la jeunesse et des sports ;<br/>
<br/>
<br/>
<br/>				D E C I D E  :<br/>
              				--------------<br/>
<br/>
Article 1er : La requête de la FEDERATION DE L'HOSPITALISATION PRIVEE est rejetée.<br/>
<br/>
Article 2 : Les conclusions présentées par le ministre de la santé, de la jeunesse et des sports au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la FEDERATION DE L'HOSPITALISATION PRIVEE, au Premier ministre et à la ministre de la santé, de la jeunesse, des sports et de la vie associative.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-02-01-02-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - COMPÉTENCE. LOI ET RÈGLEMENT. ARTICLES 34 ET 37 DE LA CONSTITUTION - MESURES RELEVANT DU DOMAINE DE LA LOI. RÈGLES CONCERNANT LA DÉTERMINATION DE CRIMES ET DÉLITS. - MANQUEMENTS AUX CONTRATS D'OBJECTIFS ET DE MOYENS CONCLUS PAR LES AGENCES RÉGIONALES D'HOSPITALISATION AVEC LES ÉTABLISSEMENTS DE SANTÉ - DISPOSITIONS RÉGLEMENTAIRES RELATIVES AUX SANCTIONS À CES MANQUEMENTS, REPRENANT LES MÊMES TERMES QU'UNE DISPOSITION LÉGISLATIVE EN LES COMPLÉTANT - DISPOSITIONS RELEVANT, EN VERTU DE L'ARTICLE 34 DE LA CONSTITUTION, DU DOMAINE DE LA LOI - CONSÉQUENCES - A) RENVOI DE SES CONDITIONS D'APPLICATION PAR LA LOI AU DÉCRET - HABILITATION DU POUVOIR RÉGLEMENTAIRE À INTERVENIR DANS LE DOMAINE LÉGISLATIF - ABSENCE [RJ1] - B) INVOCABILITÉ DU PRINCIPE DE LÉGALITÉ DES DÉLITS ET DES PEINES, PROTÉGÉ PAR L'ARTICLE 8 DE LA DÉCLARATION DES DROITS DE L'HOMME ET DU CITOYEN - ABSENCE - ECRAN LÉGISLATIF [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">01-04-005 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. CONSTITUTION ET PRINCIPES DE VALEUR CONSTITUTIONNELLE. - PRINCIPE DE LÉGALITÉ DES DÉLITS ET DES PEINES, PROTÉGÉ PAR L'ARTICLE 8 DE LA DÉCLARATION DES DROITS DE L'HOMME ET DU CITOYEN - INVOCABILITÉ - ABSENCE - ECRAN LÉGISLATIF [RJ2].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">61 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. CONSTITUTION ET PRINCIPES DE VALEUR CONSTITUTIONNELLE. - MANQUEMENTS AUX CONTRATS D'OBJECTIFS ET DE MOYENS CONCLUS PAR LES AGENCES RÉGIONALES D'HOSPITALISATION AVEC LES ÉTABLISSEMENTS DE SANTÉ - DISPOSITIONS RÉGLEMENTAIRES RELATIVES AUX SANCTIONS À CES MANQUEMENTS, REPRENANT LES MÊMES TERMES QU'UNE DISPOSITION LÉGISLATIVE EN LES COMPLÉTANT - DISPOSITIONS RELEVANT, EN VERTU DE L'ARTICLE 34 DE LA CONSTITUTION, DU DOMAINE DE LA LOI - CONSÉQUENCES - A) RENVOI DE SES CONDITIONS D'APPLICATION PAR LA LOI AU DÉCRET - HABILITATION DU POUVOIR RÉGLEMENTAIRE À INTERVENIR DANS LE DOMAINE LÉGISLATIF - ABSENCE [RJ1] - B) INVOCABILITÉ DU PRINCIPE DE LÉGALITÉ DES DÉLITS ET DES PEINES, PROTÉGÉ PAR L'ARTICLE 8 DE LA DÉCLARATION DES DROITS DE L'HOMME ET DU CITOYEN - ABSENCE - ECRAN LÉGISLATIF [RJ2].
</SCT>
<ANA ID="9A"> 01-02-01-02-02 a) Lorsque la définition des obligations auxquelles est soumis l'exercice d'une activité relève du législateur en application de l'article 34 de la Constitution, il n'appartient qu'à la loi de fixer, le cas échéant, les sanctions administratives dont la méconnaissance de ces obligations peut être assortie et, en particulier, de déterminer tant les sanctions encourues que les éléments constitutifs des infractions que ces sanctions ont pour objet de réprimer. La circonstance que la loi ait renvoyé au décret le soin de définir ses modalités ou ses conditions d'application n'a ni pour objet ni pour effet d'habiliter le pouvoir réglementaire à intervenir dans le domaine de la loi pour définir ces éléments. b) L'article R. 6114-11 du code de la santé publique, issu du décret n° 2006-1332 du 2 novembre 2006, reprend les mêmes termes que l'article L. 6114-1 du même code selon lesquels un contrat d'objectifs et de moyens peut être suspendu ou résilié en cas de manquement grave du titulaire de l'autorisation aux dispositions législatives et réglementaires ou à ses obligations contractuelles, tout en précisant la procédure applicable. La résiliation du contrat encourue par l'établissement qui n'a pas mis fin au manquement reproché a pour effet de permettre à l'agence régionale d'hospitalisation de fixer tant les objectifs quantifiés d'activité des établissements que les pénalités financières auxquelles ils s'exposent en cas de non-respect de ces objectifs. Les conditions d'exercice de l'activité de ces établissements relèvent, en vertu de l'article 34 de la Constitution, de la loi. Il n'appartient dès lors qu'à la loi de déterminer les éléments constitutifs des infractions dont l'auteur encourt de telles sanctions. Par suite, est inopérant le moyen tiré de ce que la définition reprise dans le décret attaqué méconnaîtrait en raison de son imprécision le principe de légalité des délits et des peines.</ANA>
<ANA ID="9B"> 01-04-005 L'article R. 6114-11 du code de la santé publique, issu du décret n° 2006-1332 du 2 novembre 2006, reprend les mêmes termes que l'article L. 6114-1 du même code selon lesquels un contrat d'objectifs et de moyens peut être suspendu ou résilié en cas de manquement grave du titulaire de l'autorisation aux dispositions législatives et réglementaires ou à ses obligations contractuelles, tout en précisant la procédure applicable. La résiliation du contrat encourue par l'établissement qui n'a pas mis fin au manquement reproché a pour effet de permettre à l'agence régionale d'hospitalisation de fixer tant les objectifs quantifiés d'activité des établissements que les pénalités financières auxquelles ils s'exposent en cas de non-respect de ces objectifs. Les conditions d'exercice de l'activité de ces établissements relèvent, en vertu de l'article 34 de la Constitution, de la loi. Il n'appartient dès lors qu'à la loi de déterminer les éléments constitutifs des infractions dont l'auteur encourt de telles sanctions. Par suite, est inopérant le moyen tiré de ce que la définition reprise dans le décret attaqué méconnaîtrait en raison de son imprécision le principe de légalité des délits et des peines.</ANA>
<ANA ID="9C"> 61 a) Lorsque la définition des obligations auxquelles est soumis l'exercice d'une activité relève du législateur en application de l'article 34 de la Constitution, il n'appartient qu'à la loi de fixer, le cas échéant, les sanctions administratives dont la méconnaissance de ces obligations peut être assortie et, en particulier, de déterminer tant les sanctions encourues que les éléments constitutifs des infractions que ces sanctions ont pour objet de réprimer. La circonstance que la loi ait renvoyé au décret le soin de définir ses modalités ou ses conditions d'application n'a ni pour objet ni pour effet d'habiliter le pouvoir réglementaire à intervenir dans le domaine de la loi pour définir ces éléments. b) L'article R. 6114-11 du code de la santé publique, issu du décret n° 2006-1332 du 2 novembre 2006, reprend les mêmes termes que l'article L. 6114-1 du même code selon lesquels un contrat d'objectifs et de moyens peut être suspendu ou résilié en cas de manquement grave du titulaire de l'autorisation aux dispositions législatives et réglementaires ou à ses obligations contractuelles, tout en précisant la procédure applicable. La résiliation du contrat encourue par l'établissement qui n'a pas mis fin au manquement reproché a pour effet de permettre à l'agence régionale d'hospitalisation de fixer tant les objectifs quantifiés d'activité des établissements que les pénalités financières auxquelles ils s'exposent en cas de non-respect de ces objectifs. Les conditions d'exercice de l'activité de ces établissements relèvent, en vertu de l'article 34 de la Constitution, de la loi. Il n'appartient dès lors qu'à la loi de déterminer les éléments constitutifs des infractions dont l'auteur encourt de telles sanctions. Par suite, est inopérant le moyen tiré de ce que la définition reprise dans le décret attaqué méconnaîtrait en raison de son imprécision le principe de légalité des délits et des peines.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp. Ass., 7 juillet 2004, Ministre de l'intérieur c/ Benkerrou, n° 255136, p. 297 ; Cons. const., 12 août 2004, 2004-504 DC ; rappr. Ass. 3 février 1967, Confédération générale des vignerons du midi et autre, p. 55 ; Cons. const., 26 juin 1969, 69-55 DC., ,[RJ2] Cf. Section, 6 novembre 1936, Arrighi, p. 966.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
