<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034570959</ID>
<ANCIEN_ID>JG_L_2017_05_000000389536</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/57/09/CETATEXT000034570959.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 03/05/2017, 389536</TITRE>
<DATE_DEC>2017-05-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389536</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO ; SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>Mme Sara-Lou GERBER</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:389536.20170503</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Procédure contentieuse antérieure :<br/>
<br/>
              M.  A...B...a demandé au tribunal administratif de Marseille d'annuler pour excès de pouvoir la décision du 27 novembre 2009 du ministre du travail, des relations sociales, de la famille, de la solidarité et de la ville autorisant son licenciement. Par un jugement n° 1000659 du 21 février 2012, le tribunal administratif a annulé cette décision.<br/>
<br/>
              Par un arrêt n° 12MA01559 du 19 février 2013, la cour administrative d'appel de Marseille a rejeté l'appel formé par la société Linpac Packaging Provence SAS contre ce jugement.<br/>
<br/>
              Par une décision n° 367929 du 15 janvier 2014, le Conseil d'Etat statuant au contentieux a, sur un pourvoi de la société Linpac Packaging Provence SAS, annulé cet arrêt et renvoyé l'affaire à la cour administrative d'appel de Marseille.<br/>
<br/>
              Par un arrêt n° 14MA00493 du 17 février 2015, la cour administrative d'appel de Marseille, statuant sur renvoi du Conseil d'Etat, a rejeté l'appel de la société Linpac Packaging Provence SAS.<br/>
<br/>
              Procédure devant le Conseil d'Etat :<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire, un mémoire en réplique et un nouveau mémoire, enregistrés les 16 avril 2015, 16 juillet 2015 et 5 février et 29 décembre 2016 au secrétariat du contentieux du Conseil d'Etat, la société Linpac Packaging Provence SAS demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de M. B... la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sara-Lou Gerber, auditeur, <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Le Prado, avocat de la société Linpac Packaging Provence SAS et à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'en vertu des dispositions du code du travail, le licenciement des salariés légalement investis de fonctions représentatives, qui bénéficient d'une protection exceptionnelle dans l'intérêt de l'ensemble des travailleurs qu'ils représentent, ne peut intervenir que sur autorisation de l'inspecteur du travail ; que, lorsque le licenciement d'un de ces salariés est envisagé, ce licenciement ne doit pas être en rapport avec les fonctions représentatives normalement exercées ou l'appartenance syndicale de l'intéressé ; que, dans le cas où la demande d'autorisation de licenciement présentée par l'employeur est fondée sur un motif de caractère économique, il appartient à l'inspecteur du travail et, le cas échéant, au ministre, de rechercher, sous le contrôle du juge de l'excès de pouvoir, si la situation de l'entreprise justifie le licenciement du salarié ;<br/>
<br/>
              2. Considérant qu'à ce titre, lorsque la demande d'autorisation de licenciement pour motif économique est fondée sur la cessation d'activité de l'entreprise, il appartient à l'autorité administrative de contrôler que cette cessation d'activité est totale et définitive ; qu'il ne lui appartient pas, en revanche, de contrôler si cette cessation d'activité est justifiée par l'existence de mutations technologiques, de difficultés économiques ou de menaces pesant sur la compétitivité de l'entreprise ; qu'il incombe ainsi à l'autorité administrative de tenir compte, à la date à laquelle elle se prononce, de tous les éléments de droit ou de fait recueillis lors de son enquête qui sont susceptibles de remettre en cause le caractère total et définitif de la cessation d'activité ; qu'il lui incombe également de tenir compte de toute autre circonstance qui serait de nature à faire obstacle au licenciement envisagé, notamment celle tenant à une reprise, même partielle, de l'activité de l'entreprise impliquant un transfert du contrat de travail du salarié à un nouvel employeur en application de l'article L. 1224-1 du code du travail ; que, lorsque l'entreprise appartient à un groupe, la seule circonstance que d'autres entreprises du groupe aient poursuivi une activité de même nature ne fait pas, par elle-même, obstacle à ce que la cessation d'activité de l'entreprise soit regardée comme totale et définitive ;<br/>
<br/>
              3. Considérant qu'il résulte de ce qui précède qu'en se fondant, pour annuler la décision du 27 novembre 2009 du ministre chargé du travail autorisant le licenciement de M. B..., salarié protégé au sein de la société Linpac Packaging Provence SAS, sur la seule circonstance qu'une activité de fabrication de barquettes en polystyrène expansé, identique à celle qu'exerçait cette société, se poursuivait dans d'autres sociétés du groupe Linpac, la cour administrative d'appel de Marseille a commis une erreur de droit ; que, dès lors et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt doit être annulé ;<br/>
<br/>
              4. Considérant qu'aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire " ; qu'il y a lieu, par suite, de régler l'affaire au fond ;<br/>
<br/>
              5. Considérant que, dès lors qu'une demande d'autorisation de licenciement fondée sur la cessation d'activité de l'entreprise n'a pas, ainsi qu'il a été dit au point 2, à être justifiée par l'existence de mutations technologiques, de difficultés économiques ou de menaces pesant sur la compétitivité de l'entreprise, il n'appartient pas à l'autorité administrative, pour apprécier la réalité d'un tel motif de cessation d'activité invoqué par une société faisant partie d'un groupe, d'examiner la situation économique des autres entreprises de ce groupe ; que, par suite, c'est à tort que, pour annuler la décision du ministre chargé du travail du 27 novembre 2009 autorisant le licenciement de M. B..., le tribunal administratif de Marseille s'est fondé sur l'absence de difficultés économiques au niveau du secteur d'activité auquel appartenait l'entreprise Linpac Packaging Provence SAS au sein du groupe Linpac ; <br/>
<br/>
              6. Considérant, toutefois, qu'il appartient au Conseil d'Etat, saisi de l'ensemble du litige par l'effet dévolutif de l'appel, d'examiner les autres moyens soulevés par M. B..., tant en première instance qu'en appel ;<br/>
<br/>
              7. Considérant, en premier lieu, qu'aux termes de l'article L. 2341-4 du code du travail : " Un comité d'entreprise européen ou une procédure d'information et de consultation est institué dans les entreprises ou groupes d'entreprises de dimension communautaire afin de garantir le droit des salariés à l'information et à la consultation à l'échelon européen " ; qu'aux termes de l'article L. 2341-8 du même code : " (...) la procédure mentionnée à l'article L. 2341-4 porte sur les questions transnationales. Sont considérées comme telles les questions qui concernent l'ensemble de l'entreprise ou du groupe d'entreprises de dimension communautaire ou au moins deux entreprises ou établissements de l'entreprise ou du groupe situés dans deux Etats membres " ; qu'il ressort des pièces du dossier que la cessation d'activité de l'entreprise Linpac Packaging Provence SAS ne relevait pas des questions transnationales au sens de ces dispositions ; que, par suite, le moyen tiré de ce que l'absence d'information et de consultation du comité d'entreprise européen du groupe Linpac entacherait d'irrégularité la décision litigieuse doit, en tout état de cause, être écarté ;<br/>
<br/>
              8. Considérant, en deuxième lieu, que la circonstance que d'autres entreprises du groupe Linpac poursuivent une activité de même nature ne suffit pas à caractériser une reprise, même partielle, de l'activité de cette entreprise susceptible de remettre en cause le caractère total et définitif de sa fermeture ; que si M. B...soutient également que l'activité n'avait pas cessé, au motif que la société mère Linpac Packaging Limited devait être regardée comme étant en réalité la véritable société et que son activité à elle n'avait pas cessé, il ne ressort pas des pièces du dossier et, notamment, ni des circonstances que le directeur et le directeur des ressources humaines de la société Linpac Packaging Provence SAS auraient été salariés de la société Linpac Packaging Limited, ou que le recours hiérarchique devant le ministre a été signé par le directeur de cette dernière société, que les rapports entre cette même société et la société Linpac Packaging Provence SAS excédaient les rapports de domination d'une société mère sur sa filiale, constitutifs d'un groupe ; que M. B...n'est, par suite, pas fondé à soutenir que la demande de licenciement ne pouvait pas se fonder sur la cessation d'activité de la société qui l'employait ;<br/>
<br/>
              9. Considérant qu'il n'appartient pas à l'administration, dans de telles circonstances, de rechercher si la cessation d'activité est due à la faute ou à la légèreté blâmable de l'employeur ; que le moyen tiré de ce que la cessation d'activité de la société Linpac Packaging Provence SAS serait la conséquence directe et nécessaire des fautes et de la légèreté blâmable de l'employeur est, par suite, inopérant ; <br/>
<br/>
              10. Considérant, en troisième lieu, qu'il ressort des pièces du dossier, d'une part, que la société Linpac Packaging Provence SAS a procédé à une recherche sérieuse des postes disponibles pour un reclassement au sein du groupe Linpac, qui a permis l'identification de vingt-deux postes disponibles, sans que puisse lui être reprochée l'absence d'identification de postes disponibles sur le site de Pontivy ; que, d'autre part, la société Linpac Packaging Provence SAS a adressé à M. B... une lettre lui proposant une offre de reclassement correspondant à son profil, à laquelle était jointe la liste des postes identifiés comme disponibles pour un reclassement ; que le salarié a, en outre, été reçu en entretien individuel afin de rechercher ses possibilités de reclassement ; qu'enfin, une liste des postes disponibles pour le reclassement, régulièrement mise à jour, a été affichée dans les locaux de l'entreprise ; que, dès lors, le moyen tiré de ce que la société Linpac Packaging Provence SAS n'a pas satisfait à son obligation de recherche de reclassement doit être écarté ;<br/>
<br/>
              11. Considérant, en quatrième lieu, que le moyen par lequel M. B... soutient que la décision attaquée est illégale dès lors que son employeur n'a pas déployé suffisamment d'efforts de formation et d'adaptation n'est pas assorti des précisions permettant d'en apprécier le bien-fondé ;<br/>
<br/>
              12. Considérant, en cinquième lieu, que la régularité de la procédure d'information et de consultation du comité d'entreprise sur un projet de licenciement économique n'est pas subordonnée au respect préalable, par l'employeur, de l'obligation d'engager tous les trois ans la négociation portant sur la gestion prévisionnelle des emplois et des compétences prévue par l'article L. 2242-15 du code du travail ; que, dès lors, le moyen tiré de ce que l'employeur aurait dû mettre en place une telle négociation doit également être écarté ;<br/>
<br/>
              13. Considérant qu'il résulte de tout ce qui précède que la société Linpac Packaging Provence SAS est fondée à soutenir que c'est à tort que, par son jugement du 21 février 2012, le tribunal administratif de Marseille a annulé la décision du 27 novembre 2009 du ministre du travail, des relations sociales, de la famille, de la solidarité et de la ville autorisant le licenciement de M.B... ;<br/>
<br/>
              14. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de la société Linpac Packaging Provence SAS, qui n'est pas la partie perdante dans la présente instance, la somme que demande à ce titre M.B... ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de M. B...les sommes que demande, au même titre, la société Linpac Packaging Provence SAS ; <br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : L'arrêt du 17 février 2015 de la cour administrative d'appel de Marseille et le jugement du 21 février 2012 du tribunal administratif de Marseille sont annulés. <br/>
Article 2 : La demande présentée par M. B... devant le tribunal administratif de Marseille est rejetée.<br/>
Article 3 : Les conclusions présentées par la société Linpac Packaging Provence SAS et M. B... au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la société Linpac Packaging Provence SAS et à M. A... B.... <br/>
Copie en sera adressée à la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-07-01-04-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. MOYENS INOPÉRANTS. - ABSENCE - CONTESTATION D'UNE AUTORISATION DE LICENCIEMENT D'UN SALARIÉ PROTÉGÉ POUR MOTIF ÉCONOMIQUE - MOYEN TIRÉ DE CE QU'UNE AUTRE SOCIÉTÉ SERAIT LE VÉRITABLE EMPLOYEUR DU SALARIÉ - EXISTENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">66-07-01-05 TRAVAIL ET EMPLOI. LICENCIEMENTS. AUTORISATION ADMINISTRATIVE - SALARIÉS PROTÉGÉS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. - MOYENS - OPÉRANCE - LICENCIEMENT POUR MOTIF ÉCONOMIQUE - MOYEN TIRÉ DE CE QU'UNE AUTRE SOCIÉTÉ SERAIT LE VÉRITABLE EMPLOYEUR DU SALARIÉ - EXISTENCE [RJ1].
</SCT>
<ANA ID="9A"> 54-07-01-04-03 Recours contre l'autorisation de licenciement pour motif économique d'un salarié protégé. Le moyen tiré de ce qu'une autre société serait le véritable employeur du salarié licencié est opérant.</ANA>
<ANA ID="9B"> 66-07-01-05 Recours contre l'autorisation de licenciement pour motif économique d'un salarié protégé. Le moyen tiré de ce qu'une autre société serait le véritable employeur du salarié licencié est opérant.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Rappr., en matière de PSE, CE, 17 octobre 2016, Société G Participations et autres, n°s 386306 386366, T. pp. 900-977.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
