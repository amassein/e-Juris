<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041569373</ID>
<ANCIEN_ID>JG_L_2020_02_000000418299</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/56/93/CETATEXT000041569373.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 12/02/2020, 418299</TITRE>
<DATE_DEC>2020-02-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418299</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY ; LE PRADO</AVOCATS>
<RAPPORTEUR>M. Florian Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2020:418299.20200212</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au tribunal administratif de Montreuil d'annuler pour excès de pouvoir la décision du 13 février 2014 par laquelle le directeur du centre hospitalier de Saint-Denis a résilié sa convention de stage. Par un jugement n° 1401900 du 25 septembre 2015, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 15VE03582 du 19 décembre 2017, la cour administrative d'appel de Versailles a rejeté l'appel formé par M. A... contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 19 février et 22 mai 2018 et le 9 février 2019 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge du centre hospitalier de Saint-Denis la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouvenin, Coudray, Grévy, avocat de M. A... et à Me Le Prado, avocat du centre hospitalier de Saint-Denis ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. A... a été accueilli en qualité de praticien stagiaire associé au sein du service de chirurgie générale, viscérale et digestive du centre hospitalier de Saint-Denis à compter du 30 septembre 2013. Lors de son arrivée dans l'établissement, le directeur lui a demandé de tailler sa barbe " pour en supprimer le caractère ostentatoire ". M. A... ayant refusé de le faire, le directeur du centre hospitalier a résilié sa convention de stage par une décision du 13 février 2014 qui se fondait aussi sur une insuffisante maîtrise de la langue française mais n'opposait aucun motif tenant aux exigences particulières de fonctionnement d'un bloc opératoire. M. A... se pourvoit en cassation contre l'arrêt du 19 décembre 2017 par lequel la cour administrative d'appel de Versailles a rejeté l'appel qu'il avait formé contre le jugement du tribunal administratif de Versailles du 25 septembre 2015 rejetant sa demande d'annulation de cette décision.<br/>
<br/>
              2. Aux termes de l'article L. 6134-1 du code de la santé publique, dans sa rédaction applicable à l'espèce : " Dans le cadre des missions qui leur sont imparties et dans les conditions fixées par voie réglementaire, les établissements publics de santé peuvent participer à des actions de coopération, y compris internationales (...) ". L'article R. 6134-2 du même code dispose que : " Bénéficient d'une formation complémentaire dans le cadre des conventions mentionnées à l'article L. 6134-1 : / 1° Les médecins et pharmaciens titulaires d'un diplôme de docteur en médecine ou en pharmacie permettant l'exercice dans le pays d'obtention ou d'origine et n'effectuant pas une formation universitaire en France. Ils sont désignés en qualité de stagiaires associés pour une période de six mois renouvelable une fois (...) ". Les praticiens étrangers qui sont, en application de ces dispositions, accueillis en tant que stagiaires associés dans un établissement public de santé doivent respecter les obligations qui s'imposent aux agents du service public hospitalier. A ce titre, s'ils bénéficient de la liberté de conscience qui interdit toute discrimination fondée sur la religion, le principe de laïcité fait obstacle à ce qu'ils manifestent leurs croyances religieuses dans le cadre du service public.<br/>
<br/>
              3. Pour juger que M. A... avait manqué aux obligations qui viennent d'être rappelées, la cour administrative d'appel s'est fondée sur ce que, alors même que la barbe qu'il portait ne pouvait, malgré sa taille, être regardée comme étant par elle-même un signe d'appartenance religieuse, il avait refusé de la tailler et n'avait pas nié que son apparence physique pouvait être perçue comme un signe d'appartenance religieuse. En se fondant sur ces seuls éléments, par eux-mêmes insuffisants pour caractériser la manifestation de convictions religieuses dans le cadre du service public, sans retenir aucune autre circonstance susceptible d'établir que M. A... aurait manifesté de telles convictions dans l'exercice de ses fonctions, la cour a entaché son arrêt d'erreur de droit.<br/>
<br/>
              4. M. A... est, par suite, sans qu'il soit besoin de se prononcer sur les autres moyens de son pourvoi, fondé à demander l'annulation de l'arrêt qu'il attaque.<br/>
<br/>
              5. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de M. A..., qui n'est pas dans la présente instance la partie perdante, la somme que demande à ce titre le centre hospitalier de Saint-Denis. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du centre hospitalier de Saint-Denis le versement à M. A... d'une somme de 3 000 euros au titre de ces mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Versailles du 19 décembre 2017 est annulé.<br/>
<br/>
Article 2 : Le centre hospitalier de de Saint-Denis versera à M. A... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : Les conclusions présentées par le centre hospitalier de Saint-Denis au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. B... A... et au centre hospitalier de Saint-Denis. <br/>
Copie en sera adressée à la ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-07-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. PRINCIPES INTÉRESSANT L'ACTION ADMINISTRATIVE. NEUTRALITÉ DU SERVICE PUBLIC. - PRINCIPE DE LAÏCITÉ - MANIFESTATION PAR LES AGENTS PUBLICS DE LEURS CROYANCES RELIGIEUSES - INTERDICTION [RJ1] - 1) APPLICATION AUX STAGIAIRES ASSOCIÉS DANS UN ÉTABLISSEMENT PUBLIC (ART. R. 6134-2 DU CSP) - EXISTENCE [RJ2] - 2) PORT D'UNE BARBE - ELÉMENT SUFFISANT POUR CARACTÉRISER LA MANIFESTATION DE CONVICTIONS RELIGIEUSES - ABSENCE, MÊME SI L'INTÉRESSÉ A REFUSÉ DE LA TAILLER ET N'A PAS NIÉ QU'ELLE POUVAIT ÊTRE PERÇUE COMME UN SIGNE D'APPARTENANCE RELIGIEUSE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-07-11 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. OBLIGATIONS DES FONCTIONNAIRES. - PRINCIPE DE LAÏCITÉ - MANIFESTATION PAR LES AGENTS PUBLICS DE LEURS CROYANCES RELIGIEUSES - INTERDICTION [RJ1] - 1) APPLICATION AUX STAGIAIRES ASSOCIÉS DANS UN ÉTABLISSEMENT PUBLIC (ART. R. 6134-2 DU CSP) - EXISTENCE [RJ2] - 2) PORT D'UNE BARBE - ELÉMENT SUFFISANT POUR CARACTÉRISER LA MANIFESTATION DE CONVICTIONS RELIGIEUSES - ABSENCE, MÊME SI L'INTÉRESSÉ A REFUSÉ DE LA TAILLER ET N'A PAS NIÉ QU'ELLE POUVAIT ÊTRE PERÇUE COMME UN SIGNE D'APPARTENANCE RELIGIEUSE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">61-06-03 SANTÉ PUBLIQUE. ÉTABLISSEMENTS PUBLICS DE SANTÉ. PERSONNEL (VOIR : FONCTIONNAIRES ET AGENTS PUBLICS). - PRINCIPE DE LAÏCITÉ - MANIFESTATION PAR LES AGENTS PUBLICS DE LEURS CROYANCES RELIGIEUSES - INTERDICTION [RJ1] - 1) APPLICATION AUX STAGIAIRES ASSOCIÉS DANS UN ÉTABLISSEMENT PUBLIC (ART. R. 6134-2 DU CSP) - EXISTENCE [RJ2] - 2) PORT D'UNE BARBE - ELÉMENT SUFFISANT POUR CARACTÉRISER LA MANIFESTATION DE CONVICTIONS RELIGIEUSES - ABSENCE, MÊME SI L'INTÉRESSÉ A REFUSÉ DE LA TAILLER ET N'A PAS NIÉ QU'ELLE POUVAIT ÊTRE PERÇUE COMME UN SIGNE D'APPARTENANCE RELIGIEUSE.
</SCT>
<ANA ID="9A"> 01-04-03-07-02 Les praticiens étrangers qui sont, en application des articles L. 6134-1 et R. 6134-2 du code de la santé publique (CSP), accueillis en tant que stagiaires associés dans un établissement public de santé doivent respecter les obligations qui s'imposent aux agents du service public hospitalier. A ce titre, s'ils bénéficient de la liberté de conscience qui interdit toute discrimination fondée sur la religion, le principe de laïcité fait obstacle à ce qu'ils manifestent leurs croyances religieuses dans le cadre du service public.,,,Pour juger que le requérant avait manqué à ces obligations, la cour administrative d'appel s'est fondée sur ce que, alors même que la barbe qu'il portait ne pouvait, malgré sa taille, être regardée comme étant par elle-même un signe d'appartenance religieuse, il avait refusé de la tailler et n'avait pas nié que son apparence physique pouvait être perçue comme un signe d'appartenance religieuse. En se fondant sur ces seuls éléments, par eux-mêmes insuffisants pour caractériser la manifestation de convictions religieuses dans le cadre du service public, sans retenir aucune autre circonstance susceptible d'établir que le requérant aurait manifesté de telles convictions dans l'exercice de ses fonctions, la cour a entaché son arrêt d'erreur de droit.</ANA>
<ANA ID="9B"> 36-07-11 Les praticiens étrangers qui sont, en application des articles L. 6134-1 et R. 6134-2 du code de la santé publique (CSP), accueillis en tant que stagiaires associés dans un établissement public de santé doivent respecter les obligations qui s'imposent aux agents du service public hospitalier. A ce titre, s'ils bénéficient de la liberté de conscience qui interdit toute discrimination fondée sur la religion, le principe de laïcité fait obstacle à ce qu'ils manifestent leurs croyances religieuses dans le cadre du service public.,,,Pour juger que le requérant avait manqué à ces obligations, la cour administrative d'appel s'est fondée sur ce que, alors même que la barbe qu'il portait ne pouvait, malgré sa taille, être regardée comme étant par elle-même un signe d'appartenance religieuse, il avait refusé de la tailler et n'avait pas nié que son apparence physique pouvait être perçue comme un signe d'appartenance religieuse. En se fondant sur ces seuls éléments, par eux-mêmes insuffisants pour caractériser la manifestation de convictions religieuses dans le cadre du service public, sans retenir aucune autre circonstance susceptible d'établir que le requérant aurait manifesté de telles convictions dans l'exercice de ses fonctions, la cour a entaché son arrêt d'erreur de droit.</ANA>
<ANA ID="9C"> 61-06-03 Les praticiens étrangers qui sont, en application des articles L. 6134-1 et R. 6134-2 du code de la santé publique (CSP), accueillis en tant que stagiaires associés dans un établissement public de santé doivent respecter les obligations qui s'imposent aux agents du service public hospitalier. A ce titre, s'ils bénéficient de la liberté de conscience qui interdit toute discrimination fondée sur la religion, le principe de laïcité fait obstacle à ce qu'ils manifestent leurs croyances religieuses dans le cadre du service public.,,,Pour juger que le requérant avait manqué à ces obligations, la cour administrative d'appel s'est fondée sur ce que, alors même que la barbe qu'il portait ne pouvait, malgré sa taille, être regardée comme étant par elle-même un signe d'appartenance religieuse, il avait refusé de la tailler et n'avait pas nié que son apparence physique pouvait être perçue comme un signe d'appartenance religieuse. En se fondant sur ces seuls éléments, par eux-mêmes insuffisants pour caractériser la manifestation de convictions religieuses dans le cadre du service public, sans retenir aucune autre circonstance susceptible d'établir que le requérant aurait manifesté de telles convictions dans l'exercice de ses fonctions, la cour a entaché son arrêt d'erreur de droit.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur l'interdiction aux agents du service public du port de signes d'appartenance religieuse, CE, 3 mai 2000, Mlle,, n° 217017, p. 169.,,[RJ2] Cf. CE, 28 juillet 2017, Mme,et autres, n°s 390740 390741 390742, T. pp. 446-596-626-782.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
