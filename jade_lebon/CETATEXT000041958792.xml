<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041958792</ID>
<ANCIEN_ID>JG_L_2020_06_000000428845</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/95/87/CETATEXT000041958792.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 03/06/2020, 428845</TITRE>
<DATE_DEC>2020-06-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428845</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; LE PRADO ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Marc Pichon de Vendeuil</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:428845.20200603</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le Bureau européen d'assurance hospitalière (BEAH) a demandé au tribunal administratif de Nîmes, d'une part, d'annuler ou, à défaut, de résilier le marché public d'assurance relatif au lot n° 1 " responsabilité civile " conclu le 24 novembre 2014 entre le centre hospitalier d'Avignon et la Société hospitalière d'assurances mutuelles (SHAM) et, d'autre part, de condamner le centre hospitalier d'Avignon à lui verser une indemnité de 273 750 euros en réparation du préjudice lié à son éviction de la procédure de passation de ce marché. Par un jugement n° 1500792 du 19 octobre 2017, le tribunal administratif de Nîmes a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 17MA04927 du 14 janvier 2019, la cour administrative d'appel de Marseille a, sur appel du Bureau européen d'assurance hospitalière (BEAH), annulé ce jugement, ordonné la résiliation, à compter du 1er mai 2019, du marché conclu entre le centre hospitalier d'Avignon et la SHAM et décidé, avant dire droit, qu'il sera procédé à une expertise contradictoire pour évaluer le préjudice subi par le BEAH.<br/>
<br/>
              1° Sous le n° 428845, par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 14 mars, 14 juin 2019 et 24 février 2020 au secrétariat du contentieux du Conseil d'Etat, le centre hospitalier d'Avignon demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge du Bureau européen d'assistance hospitalière (BEAH) la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              2° Sous le n° 428847, par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 14 mars, 14 juin et 20 décembre 2019 au secrétariat du contentieux du Conseil d'Etat, la Société hospitalière d'assurances mutuelles (SHAM) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le même arrêt ;<br/>
<br/>
              2°) de mettre à la charge du Bureau européen d'assistance hospitalière (BEAH) la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le règlement d'exécution n° 842/2011 de la Commission du 19 août 2011 ;<br/>
              - le code des assurances ;<br/>
              - le code des marchés publics ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 modifiée ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme A... B..., auditrice,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat du centre hospitalier d'Avignon, à la SCP Foussard, Froger, avocat du BEAH et à Me Le Prado, avocat de la SHAM ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Les deux pourvois présentent à juger des mêmes questions. Il y a donc lieu de les joindre pour statuer par une même décision.<br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que, par un avis d'appel public à la concurrence publié le 25 juin 2014 au Journal officiel de l'Union européenne et au Bulletin officiel des annonces de marchés publics, le centre hospitalier d'Avignon a lancé une procédure d'appel d'offres ouvert en vue de l'attribution de quatre lots destinés à couvrir ses besoins en matière d'assurances pour une durée de cinq ans. Le lot n° 1 " responsabilité civile hospitalière " a été attribué à la Société hospitalière d'assurances mutuelles (SHAM). Le Bureau européen d'assistance hospitalière (BEAH), dont l'offre, classée en deuxième position, a été rejetée, a contesté la validité du marché public conclu par le centre hospitalier d'Avignon avec la SHAM et demandé la condamnation de l'établissement public de santé à lui verser la somme de 273 750 euros en réparation des préjudices résultant de son éviction irrégulière. Par un jugement du 19 octobre 2017, le tribunal administratif de Nîmes a rejeté ses demandes. Le centre hospitalier d'Avignon et la SHAM se pourvoient en cassation contre l'arrêt de la cour administrative d'appel de Marseille qui, sur appel du BEAH, a annulé ce jugement, ordonné la résiliation, à compter du 1er mai 2019, du marché litigieux et décidé, avant dire droit, de procéder à une expertise contradictoire pour évaluer le préjudice subi par le BEAH.<br/>
<br/>
              Sur les conclusions contestant la validité du contrat :<br/>
<br/>
              3. Indépendamment des actions dont disposent les parties à un contrat administratif et des actions ouvertes devant le juge de l'excès de pouvoir contre les clauses réglementaires d'un contrat ou devant le juge du référé contractuel sur le fondement des articles L. 551-13 et suivants du code de justice administrative, tout tiers à un contrat administratif susceptible d'être lésé dans ses intérêts de façon suffisamment directe et certaine par sa passation ou ses clauses est recevable à former devant le juge du contrat un recours de pleine juridiction contestant la validité du contrat ou de certaines de ses clauses non réglementaires qui en sont divisibles. Ce recours doit être exercé, y compris si le contrat contesté est relatif à des travaux publics, dans un délai de deux mois à compter de l'accomplissement des mesures de publicité appropriées, notamment au moyen d'un avis mentionnant à la fois la conclusion du contrat et les modalités de sa consultation dans le respect des secrets protégés par la loi. La circonstance que l'avis ne mentionne pas la date de la conclusion du contrat est sans incidence sur le point de départ du délai de recours contentieux qui court à compter de cette publication.<br/>
<br/>
              4. Ainsi qu'il a été dit au point précédent, la publication d'un avis mentionnant à la fois la conclusion du contrat et les modalités de sa consultation dans le respect des secrets protégés par la loi permet de faire courir le délai de recours contre le contrat, la circonstance que l'avis ne mentionnerait pas la date de la conclusion du contrat étant sans incidence sur le point de départ du délai qui court à compter de cette publication. Par suite, en estimant, pour écarter la fin de non-recevoir tirée de ce que les conclusions du BEAH contestant la validité du contrat litigieux, déposées le 12 mars 2015 au greffe du tribunal administratif de Nîmes, étaient tardives, que les " avis d'attribution " du marché, publiés le 2 décembre 2014 au Journal officiel de l'Union européenne et au Bulletin officiel des annonces de marchés publics, conformément aux dispositions de l'article 85 du code des marchés publics alors applicable, figurant aujourd'hui à l'article R. 2183-1 du code de la commande publique, ne constituaient pas une mesure de publicité appropriée susceptible de faire courir le délai de recours contentieux, au motif que ces publications ne faisaient état que de l'attribution du marché, et non de sa conclusion, et ne mentionnaient que les coordonnées de la cellule des marchés du centre hospitalier, mention qui pourtant relevait des modalités de la consultation du contrat, la cour administrative d'appel de Marseille a commis une erreur de droit.<br/>
<br/>
              Sur les conclusions indemnitaires :<br/>
<br/>
              5. Aux termes de l'article 3.6.1 du règlement de la consultation du marché : " Conformément aux dispositions de la circulaire du 24 décembre 2007 relative à la passation des marchés publics d'assurances, les éventuelles réserves ou amendements formulés par les candidats aux clauses des CCAP et CCTP seront appréciés au regard de leur incidence (notamment économique) sur l'offre dans sa globalité et ce, afin de déterminer s'ils sont susceptibles de rendre cette dernière irrégulière : / - les réserves sans impact ou ayant un impact limité sur la qualité de l'offre seront sans incidences sur la notation ; / - les réserves ayant pour effet de baisser la qualité de l'offre entraîneront une réduction de la note (...) ; / les réserves rendant l'offre insatisfaisante au regard des besoins exprimés entraîneront un rejet pur et simple de l'offre jugée irrégulière (...) " . L'article 6.2.3 du même règlement précise, s'agissant du critère n° 3 " nature et étendue des garanties - qualité des clauses contractuelles ", que : " Les réserves éventuelles apportées par le candidat sont de quatre ordres. Ces dernières viendront, par rapport au CCAP et au CCTP propre au lot, en déduction d'une note de 100 ramenée à la note maximale de 30 points, à raison de : / - acceptées : car elles ne remettent pas en cause l'étendue des garanties ; il s'agit le plus souvent de précisions apportées par l'assureur ; / - moyennes ou fortes : réserve impactant simultanément un ou plusieurs aspects du marché technique, financier ou juridique, tout en dégradant partiellement la valeur économique, notée - 15 ; / - majeures : réserve diminuant, voire excluant une garantie et/ou modifiant les conditions financières et/ou la sécurité juridique de façon conséquente entraînant une dégradation réelle de la valeur économique du présent marché, notée - 25 ; / - non-conformité : - 50, voire irrecevable pour réserve non conforme ". Enfin, aux termes de l'article 5 du cahier des clauses techniques particulières : " territorialité du contrat : les garanties doivent s'exercer dans le monde entier ".<br/>
<br/>
              6. Pour estimer que la note attribuée à l'offre de la SHAM au titre du critère n° 3 aurait dû être pénalisée de vingt-cinq points, les juges d'appel ont relevé que l'annexe financière à l'acte d'engagement relatif à la variante n° 1 de son offre comportait une réserve ainsi libellée : " les garanties sont étendues au monde entier (...) / Cette extension ne s'applique pas aux conséquences d'actes médicaux ou de soins effectués aux Etats-Unis et au Canada, ainsi qu'aux dommages causés par les produits livrés dans ces deux pays ". En estimant que l'offre litigieuse ne pouvait dès lors être comprise que comme excluant ou réduisant une garantie au sens de l'article 6.2 du règlement de consultation du marché et devait en conséquence être regardée comme comportant une réserve " majeure " au sens de ces dispositions, " indépendamment de l'intérêt effectif de cette couverture pour l'établissement ", sans rechercher si  cette réserve entraînait une dégradation réelle de la valeur économique du marché alors qu'elle avait relevé les stipulations du contrat lui imposant de procéder à cette analyse, la cour administrative d'appel de Marseille a  commis une erreur de droit.<br/>
<br/>
              7. Il résulte de tout ce qui précède, et sans qu'il soit besoin d'examiner les autres moyens des pourvois, que l'arrêt attaqué doit être annulé.<br/>
<br/>
              8. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge du centre hospitalier d'Avignon et de la SHAM, qui n'ont pas la qualité de partie perdante dans la présente instance, la somme demandée par le BEAH au titre de l'article L. 761-1 du code de justice administrative. En revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du BEAH une somme de 3 000 euros à verser à chacun d'entre eux au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 14 janvier 2019 de la cour administrative d'appel de Marseille est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
Article 3 : Le Bureau européen d'assurance hospitalière versera une somme de 3 000 euros chacun au centre hospitalier d'Avignon et à la SHAM au titre de l'article L. 761-1 du code de justice administrative. Les conclusions présentées par le BEAH au titre des mêmes dispositions sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au centre hospitalier d'Avignon, à la Société hospitalière d'assurances mutuelles (SHAM) et au Bureau européen d'assistance hospitalière (BEAH).<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-08-01-03 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. RECEVABILITÉ. - MESURES DE PUBLICITÉ FAISANT COURIR LE DÉLAI DE RECOURS - PUBLICATION D'UN AVIS MENTIONNANT LA CONCLUSION DU CONTRAT ET LES MODALITÉS DE SA CONSULTATION [RJ1] - EXISTENCE, ALORS MÊME QUE L'AVIS NE MENTIONNE PAS LA DATE DE LA CONCLUSION DU CONTRAT.
</SCT>
<ANA ID="9A"> 39-08-01-03 La publication d'un avis mentionnant à la fois la conclusion du contrat et les modalités de sa consultation dans le respect des secrets protégés par la loi permet de faire courir le délai de recours contre le contrat, la circonstance que l'avis ne mentionnerait pas la date de la conclusion du contrat étant sans incidence sur le point de départ du délai qui court à compter de cette publication.... ,,Ainsi, les avis d'attribution d'un marché, publiés au Journal officiel de l'Union européenne et au Bulletin officiel des annonces de marchés publics, conformément aux dispositions de l'article 85 du code des marchés publics alors applicable, figurant aujourd'hui à l'article R. 2183-1 du code de la commande publique, constituent une mesure de publicité appropriée susceptible de faire courir le délai de recours contentieux, alors même que ces publications ne font état que de l'attribution du marché, et non de sa conclusion.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 4 avril 2014, Département de Tarn-et-Garonne, n° 358994, p. 70.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
