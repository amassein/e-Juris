<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032484120</ID>
<ANCIEN_ID>JG_L_2016_05_000000385545</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/48/41/CETATEXT000032484120.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 02/05/2016, 385545</TITRE>
<DATE_DEC>2016-05-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>385545</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BALAT</AVOCATS>
<RAPPORTEUR>Mme Manon Perrière</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:385545.20160502</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La société Guy Dauphin Environnement (GDE) a demandé au tribunal administratif de Caen, sur le fondement des dispositions de l'article R. 541-1 du code de justice administrative, de condamner l'Etat à lui verser une provision à valoir sur la réparation des préjudices subis du fait de l'absence d'exécution de la décision du 17 janvier 2014 du préfet de l'Orne lui accordant le concours de la force publique. Par un jugement n° 1400808 du 9 juillet 2014, le tribunal administratif a condamné l'Etat à lui verser une provision de 700 000 euros.<br/>
<br/>
              Par une ordonnance n° 14NT02045, 14NT02065 du 5 novembre 2014, enregistrée au secrétariat du contentieux du Conseil d'Etat le 7 novembre 2014, la cour administrative d'appel de Nantes a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, les pourvois de la société GDE et du ministre de l'intérieur.<br/>
<br/>
              1° Par un pourvoi enregistré au greffe de la cour administrative d'appel de Nantes le 30 juillet 2014 et un mémoire complémentaire, ainsi qu'un nouveau mémoire, enregistrés les 1er décembre 2014 et 19 juin 2015 au secrétariat du contentieux du Conseil d'Etat sous le numéro 385545, le ministre de l'intérieur demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement du tribunal administratif de Caen ;<br/>
<br/>
              2°) statuant comme juge des référés, de rejeter la demande de provision présentée par la société.<br/>
<br/>
<br/>
<br/>
              2° Par un pourvoi, enregistré au greffe de la cour administrative d'appel de Nantes le 30 juillet 2014 et un mémoire complémentaire, enregistré 30 janvier 2015 au secrétariat du contentieux du Conseil d'Etat sous le numéro 385593, la société GDE demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement du tribunal administratif de Caen en tant qu'il limite à la somme de 700 000 euros la provision mise à la charge de l'Etat ;<br/>
<br/>
              2°) statuant comme juge des référés, de lui accorder la totalité de la somme demandée en première instance ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 15 000 euros au titre des dispositions L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Manon Perrière, auditeur,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Balat, avocat de la société Guy dauphin Environnement ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que la société Guy Dauphin Environnement (GDE) a demandé au tribunal administratif de Caen, sur le fondement des dispositions de l'article R. 541-1 du code de justice administrative, de condamner l'Etat à lui verser une provision à valoir sur la réparation des préjudices subis du fait de l'absence d'exécution de la décision du 17 janvier 2014 du préfet de l'Orne lui accordant le concours de la force publique pour un site situé sur la commune de Nonant-le-Pin ; que par un jugement du 9 juillet 2014, le tribunal administratif a estimé que le fait de ne pas avoir prêté le concours de la force publique avait engagé la responsabilité sans faute de l'Etat pour la période du 1er février au 17 avril 2014 et a condamné l'Etat à verser à la société requérante une provision de 700 000 euros ; que la société et le ministre de l'intérieur se pourvoient en cassation contre ce jugement ; que ces pourvois étant dirigés contre le même jugement, il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              2. Considérant, en premier lieu, qu'aux termes de l'article R. 541-1 du code de justice administrative : " Le juge des référés peut, même en l'absence d'une demande au fond, accorder une provision au créancier qui l'a saisi lorsque l'existence de l'obligation n'est pas sérieusement contestable. Il peut, même d'office, subordonner le versement de la provision à la constitution d'une garantie " ; qu'il ne résulte d'aucune disposition du code de justice administrative ni d'aucun principe que le juge des référés, lorsqu'il statue en application de ces dispositions, ait l'obligation de tenir une audience publique ; que, toutefois, s'il est décidé de tenir une audience, les dispositions de l'article R. 613-2 de ce code sont applicables ; qu'aux termes de cet article : " Si le président de la formation de jugement n'a pas pris une ordonnance de clôture, l'instruction est close trois jours francs avant la date de l'audience indiquée dans l'avis d'audience prévu à l'article R. 711-2. Cet avis le mentionne (...) " ; que l'absence de mention dans l'avis d'audience de l'intervention de la clôture de l'instruction trois jours francs avant la date de l'audience fait obstacle à cette clôture ; qu'en pareil cas, l'instruction doit être considérée comme close le jour de l'audience lorsque l'affaire est appelée ;  <br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que les parties ont été informées par courrier du 10 juin 2014 que l'affaire serait examinée lors de l'audience du 26 juin 2014 ; qu'il est constant que ce courrier ne précisait pas que l'instruction serait close trois jours francs avant l'audience ; qu'aucun autre courrier émanant de la juridiction ne faisait état d'une date de clôture d'instruction ; que, dans ces conditions, l'instruction n'était pas close lors de l'enregistrement, le 24 juin 2014, d'un mémoire par lequel la société GDE, d'une part, réévaluait le montant de la provision demandée en faisant état de la poursuite de la situation de blocage depuis son précédent mémoire et, d'autre part, produisait de nombreux justificatifs des sommes demandées ; qu'en s'abstenant de viser et de prendre en compte ce mémoire, qui comportait tant des conclusions nouvelles que des éléments nouveaux, et en se fondant uniquement sur les précédentes productions de la société pour fixer le montant de la provision qu'il lui a allouée, le tribunal administratif a entaché son jugement d'irrégularité ; que la société est par suite fondée à demander pour ce motif l'annulation du jugement attaqué en tant qu'il rejette le surplus de sa demande ; <br/>
<br/>
              4. Considérant, en second lieu, qu'en se bornant à relever qu'eu égard aux pièces versées au dossier, et notamment aux éléments financiers attestés par un commissaire aux comptes, il y avait lieu d'allouer à la société GDE une provision de 700 000 euros, sans indiquer les chefs de préjudice retenus pour justifier ce montant et alors que les éléments avancés par le commissaire aux comptes faisaient état de préjudices bien supérieurs à cette somme, le juge des référés du tribunal administratif a, ainsi que le soutient le ministre, insuffisamment motivé son jugement ; <br/>
<br/>
              5. Considérant qu'il résulte de tout ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens des pourvois de la société GDE et du ministre de l'intérieur, le jugement du tribunal administratif doit être annulé dans son entier ;<br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que la somme demandée à ce titre par la société GDE soit mise à la charge de l'Etat qui n'est pas partie perdante dans le présent litige ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 9 juillet 2014 du tribunal administratif de Caen est annulé.<br/>
		Article 2 : L'affaire est renvoyée devant le tribunal administratif de Caen.<br/>
Article 3 : Les conclusions présentées par la société Guy Dauphin Environnement au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au ministre de l'intérieur et à la société Guy Dauphin Environnement.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-03-015-03 PROCÉDURE. PROCÉDURES DE RÉFÉRÉ AUTRES QUE CELLES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ-PROVISION. POUVOIRS ET DEVOIRS DU JUGE. - 1) OBLIGATION DE TENIR UNE AUDIENCE - ABSENCE - 2) CAS OÙ LE JUGE DÉCIDE DE TENIR UNE AUDIENCE - APPLICATION DE L'ART. R. 613-2 DU CJA RELATIF À LA CLÔTURE DE L'INSTRUCTION - EXISTENCE - PORTÉE.
</SCT>
<ANA ID="9A"> 54-03-015-03 1) Il ne résulte d'aucune disposition du code de justice administrative (CJA) ni d'aucun principe que le juge des référés, lorsqu'il statue en application de l'article R. 541-1 de ce code, ait l'obligation de tenir une audience publique.... ,,2) Toutefois, s'il est décidé de tenir une audience, les dispositions de l'article R. 613-2 de ce code sont applicables. Il en résulte que l'absence de mention dans l'avis d'audience de l'intervention de la clôture de l'instruction trois jours francs avant la date de l'audience fait obstacle à cette clôture. En pareil cas, l'instruction doit être considérée comme close le jour de l'audience lorsque l'affaire est appelée [RJ1].</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Comp., dans le cas d'une réouverture de l'instruction moins de trois jours avant l'audience, CE, 23 juin 2014, Société Deny All, n° 352504, p. 173.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
