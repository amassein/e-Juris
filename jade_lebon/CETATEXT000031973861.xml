<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031973861</ID>
<ANCIEN_ID>JG_L_2016_01_000000383514</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/97/38/CETATEXT000031973861.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème / 5ème SSR, 27/01/2016, 383514</TITRE>
<DATE_DEC>2016-01-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>383514</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème / 5ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARTHELEMY, MATUCHANSKY, VEXLIARD, POUPOT ; SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>M. Jean-François de Montgolfier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:383514.20160127</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Le conseil départemental de l'Hérault de l'ordre des médecins a porté plainte contre M. A...B...devant la chambre disciplinaire de première instance du conseil régional de Languedoc-Roussillon de l'ordre des médecins. Par une décision du 17 décembre 2012, la chambre disciplinaire de première instance a infligé à M. B...la sanction de radiation du tableau de l'ordre des médecins.<br/>
<br/>
              Par une décision n° 11855 du 4 juin 2014, la chambre disciplinaire nationale de l'ordre des médecins a rejeté l'appel formé par M. B...contre cette décision.<br/>
<br/>
              Par un pourvoi, enregistré le 6 août 2014 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) de mettre à la charge du conseil départemental de l'Hérault de l'ordre des médecins la somme de 2 800 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code pénal ;<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-François de Montgolfier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de M. B...et à la SCP Barthélémy, Matuchansky, Vexliard, Poupot, avocat du Conseil national de l'ordre des médecins ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que M. B...se pourvoit en cassation contre la décision du 4 juin 2014 par laquelle la chambre disciplinaire nationale de l'ordre des médecins a rejeté son appel formé contre la décision du 17 décembre 2012 de la chambre disciplinaire de première instance du conseil régional de Languedoc-Roussillon lui infligeant la sanction de radiation du tableau de l'ordre ;<br/>
<br/>
              Sur le moyen tiré de la violation des dispositions de l'article L. 4126-6 du code de la santé publique : <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 4127-1 du code de la santé publique : " Un code de déontologie, propre à chacune des professions de médecin, chirurgien-dentiste et sage-femme, préparé par le conseil national de l'ordre intéressé, est édicté sous la forme d'un décret en Conseil d'Etat " ; que l'article L. 4124-6 du code de la santé publique fixe la liste des sanctions disciplinaires qui peuvent être prononcées à l'encontre d'un médecin, d'un chirurgien-dentiste ou d'une sage-femme ayant méconnu les devoirs professionnels et les règles édictés par le code de déontologie propre à sa profession et précise le régime de ces sanctions ; que l'article L. 4126-5 du même code précise que l'exercice de l'action disciplinaire ne fait obstacle ni à des poursuites pénales, ni aux actions de nature indemnitaire, ni à l'action disciplinaire devant l'administration dont le praticien en cause dépend ni aux instances qui peuvent être engagées contre ces praticiens en raison des abus qui leur seraient reprochés dans leur participation aux soins médicaux prévus par les lois sociales ;<br/>
<br/>
              3. Considérant que si, aux termes du premier alinéa de l'article L. 4126-6 : " Lorsqu'un médecin, un chirurgien-dentiste ou une sage-femme a été condamné par une juridiction pénale pour tout autre fait qu'un crime ou délit contre la Nation, l'Etat ou la paix publique, la chambre disciplinaire de première instance de l'ordre peut prononcer, s'il y a lieu, à son égard, dans les conditions des articles L. 4126-1 et L. 4126-2, une des sanctions prévues à l'article L. 4124-6 ", ces dispositions, d'une part, ne permettent pas au juge disciplinaire de prononcer une sanction à l'encontre d'un praticien ayant fait l'objet d'une condamnation pénale pour un fait autre qu'un crime ou délit contre la Nation, l'Etat ou la paix publique, sans rechercher si ce fait constitue également un manquement aux obligations déontologiques auxquelles ce praticien est soumis en raison de sa profession, ni, d'autre part, ne font obstacle à ce que le juge disciplinaire sanctionne un praticien pour un fait ayant déjà donné lieu à une condamnation pénale pour crime ou délit contre la Nation, l'Etat ou la paix publique, dès lors que ce fait pénalement sanctionné constitue également un manquement au code de déontologie ; <br/>
<br/>
              4. Considérant qu'il résulte des énonciations de la décision attaquée que, pour rejeter l'appel formé par M. B...contre la sanction qui lui avait été infligée en première instance, la chambre disciplinaire nationale de l'ordre des médecins s'est fondée sur ce que les faits reprochés méconnaissaient les dispositions des articles R. 4127-3 et R. 4127-31 du code de la santé publique qui imposent au médecin de respecter les principes de moralité, de probité et de dévouement et de s'abstenir de tout acte de nature à déconsidérer la profession ; qu'il résulte de ce qui a été dit ci-dessus qu'en statuant ainsi, alors même qu'il ressort des pièces du dossier qui lui était soumis que les faits reprochés à M. B...avaient donné lieu à une condamnation pénale pour faux et usage de faux, qui constitue un délit contre la Nation, l'Etat ou la paix publique au sens du livre quatrième du code pénal, la chambre disciplinaire nationale n'a pas méconnu les dispositions de l'article L. 4126-6 du code de la santé publique ;<br/>
<br/>
              Sur la proportionnalité de la sanction :<br/>
<br/>
              5. Considérant, en premier lieu, qu'il résulte des énonciations non contestées de la décision attaquée que M. B...a continué d'exercer la médecine libérale malgré l'interdiction temporaire qui lui en avait été faite par le jugement du tribunal de grande instance d'Auxerre du 24 avril 2008 prononçant sa liquidation judiciaire et qu'il a, à cette fin, antidaté des feuilles de soins et des ordonnances à une date antérieure à sa liquidation judiciaire et fait verser les sommes correspondantes sur le compte bancaire de son épouse ; qu'eu égard, d'une part, à la gravité des faits ainsi retenus, et, d'autre part, à la circonstance que M. B...avait déjà été sanctionné pour de nombreux manquements graves à la déontologie médicale, la sanction de la radiation du tableau de l'ordre prononcée par la chambre disciplinaire de première instance du conseil régional de Languedoc-Roussillon de l'ordre des médecins, confirmée par la décision attaquée qui rejette l'appel de M.B..., n'est pas hors de proportion avec les fautes commises par ce praticien ;<br/>
<br/>
              6. Considérant, en second lieu, que le principe de la proportionnalité des peines fait obligation au juge disciplinaire, lorsqu'il entend prononcer une sanction pour des faits ayant déjà donné lieu à une sanction pénale, de veiller au respect de l'exigence selon laquelle le montant global des sanctions prononcées au titre de ces faits ne dépasse pas le montant le plus élevé de l'une des sanctions encourues ; qu'en l'espèce, le cumul de la peine d'interdiction d'exercer l'activité de médecin libéral pendant cinq ans prononcée à son encontre par le jugement du tribunal correctionnel d'Auxerre du 15 décembre 2011 et de la sanction de radiation prononcée par la chambre disciplinaire de première instance du conseil régional de Languedoc-Roussillon n'excède pas la sanction maximum de la radiation que pouvait prononcer le juge disciplinaire sur le fondement de l'article L. 4124-6 du code de la santé publique ; que M. B...ne saurait par suite utilement soutenir que la chambre disciplinaire nationale a entaché sa décision d'erreur de droit et d'insuffisance de motivation en ne tenant pas expressément compte, pour confirmer la sanction disciplinaire prononcée en première instance, de la sanction antérieurement prononcée par le juge pénal ;<br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que le pourvoi de M. B...doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Le pourvoi de M. B...est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. A...B...et au conseil départemental de l'Hérault de l'ordre des médecins.<br/>
Copie en sera adressée au Conseil national de l'ordre des médecins.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">55-04-02-01 PROFESSIONS, CHARGES ET OFFICES. DISCIPLINE PROFESSIONNELLE. SANCTIONS. FAITS DE NATURE À JUSTIFIER UNE SANCTION. - PROFESSIONS MÉDICALES - ARTICLE L. 4126-6 DU CODE DE LA SANTÉ PUBLIQUE - INTERPRÉTATION.
</SCT>
<ANA ID="9A"> 55-04-02-01 Les dispositions de l'article L. 4126-6 du code de la santé publique ne permettent pas au juge disciplinaire de prononcer une sanction à l'encontre d'un praticien ayant fait l'objet d'une condamnation pénale pour un fait autre qu'un crime ou délit contre la Nation, l'Etat ou la paix publique, sans rechercher si ce fait constitue également un manquement aux obligations déontologiques auxquelles ce praticien est soumis en raison de sa profession.... ,,En outre, ces dispositions ne font pas obstacle à ce que le juge disciplinaire sanctionne un praticien pour un fait ayant déjà donné lieu à une condamnation pénale pour crime ou délit contre la Nation, l'Etat ou la paix publique, dès lors que ce fait pénalement sanctionné constitue également un manquement au code de déontologie applicable à sa profession.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
