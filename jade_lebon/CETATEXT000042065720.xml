<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042065720</ID>
<ANCIEN_ID>JG_L_2020_06_000000420850</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/06/57/CETATEXT000042065720.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 29/06/2020, 420850</TITRE>
<DATE_DEC>2020-06-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420850</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO ; SCP ROCHETEAU, UZAN-SARANO ; SCP SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR>Mme Louise Cadin</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:420850.20200629</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme C... D... a demandé au tribunal administratif de Marseille d'ordonner une expertise aux fins d'évaluer ses préjudices et de condamner l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) et l'Assistance publique - hôpitaux de Marseille (AP-HM), solidairement ou l'un à défaut de l'autre, à lui verser une provision de 80 000 euros. Par un jugement n° 1104091 du 26 mai 2014, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 14MA03363 du 17 mars 2016, la cour administrative d'appel de Marseille a, sur appel de Mme D..., annulé ce jugement, condamné l'AP-HM et l'ONIAM à lui verser chacun la somme de 6 500 euros à titre de provision, décidé qu'il serait procédé à une expertise médicale sur les séquelles de l'intervention pratiquée le 4 février 2009 et réservé jusqu'en fin d'instance tous les droits et moyens des parties sur lesquels elle n'avait pas expressément statué.<br/>
<br/>
              Par une décision n° 399946 du 10 mars 2017, le Conseil d'Etat statuant au contentieux a, sur le pourvoi principal de l'ONIAM, annulé cet arrêt en tant qu'il statue sur la prise en charge par l'ONIAM des conséquences de l'intervention subie par Mme D... au titre de la solidarité nationale mais rejeté le pourvoi incident formé par l'AP-HM. <br/>
<br/>
              Par un nouvel arrêt n° 14MA03363, 17MA01110 du 23 mars 2018, la cour administrative d'appel de Marseille a condamné l'AP-HM à verser à Mme D... une somme de 23 204,39 euros et rejeté le surplus de ses conclusions.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 23 mai et 22 août 2018 au secrétariat du contentieux du Conseil d'Etat, l'AP-HM et la société hospitalière d'assurances mutuelles (SHAM) demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il condamne l'AP-HM à verser une indemnité à Mme D... ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter dans cette mesure l'appel de Mme D....<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ; <br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme B... A..., auditrice,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Le Prado, avocat de l'Assistance publique Hôpitaux de Marseille et de la Société hospitalière d'assurances mutuelles et à la SCP Rocheteau, Uzan-Sarano, avocat de Mme D... et à la SCP Sevaux, Mathonnet, avocat de l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 4 juin 2020, présentée par l'AP-HM et par la SHAM ; <br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme D... a subi le 4 février 2009 une opération chirurgicale au sein du service de neurochirurgie de l'hôpital de la Timone à Marseille, à la suite de laquelle elle a présenté une cécité complète de l'oeil droit, un ptosis, une immobilité oculaire ainsi qu'une paralysie complète du muscle des nerfs oculomoteurs. Elle a demandé au tribunal administratif de Marseille d'ordonner une expertise et de condamner l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) et l'Assistance Publique-Hôpitaux de Marseille (AP-HM) à lui verser une provision de 80 000 euros. Par un jugement du 26 mai 2014, le tribunal a rejeté sa demande. Par un arrêt avant-dire droit du 17 mars 2016, la cour administrative d'appel de Marseille a, sur appel de Mme D..., annulé ce jugement, condamné, d'une part, l'AP-HM au titre d'un manquement à son devoir d'information et, d'autre part, l'ONIAM au titre de la prise en charge par la solidarité nationale des accidents médicaux non fautifs, à lui verser chacun la somme de 6 500 euros à titre provisionnel et a ordonné une expertise médicale pour déterminer l'étendue des préjudices de Mme D..., en réservant jusqu'en fin d'instance tous les droits et moyens des parties sur lesquels elle n'avait pas expressément statué. Par une décision du 10 mars 2017, le Conseil d'Etat statuant au contentieux a fait droit au pourvoi principal de l'ONIAM en annulant cet arrêt en tant qu'il avait statué sur la prise en charge par l'ONIAM des dommages subis par Mme D... mais rejeté le pourvoi incident de l'AP-HM et de la SHAM dirigé contre le même arrêt en tant qu'il avait condamné l'AP-HM à verser à Mme D... une indemnité à titre provisionnel. Par un nouvel arrêt du 23 mars 2018, la cour administrative d'appel de Marseille, statuant à la fois sur la partie du litige que la décision du Conseil d'Etat lui avait renvoyée et sur la partie du litige dont elle était demeurée saisie après avoir ordonné une expertise, a rejeté les conclusions de Mme D... dirigées contre l'ONIAM, a condamné l'AP-HM à verser à Mme D... une somme de 23 204,39 euros et rejeté le surplus des conclusions de cette dernière. L'AP-HM et la SHAM se pourvoient en cassation contre ce dernier arrêt en tant qu'il a condamné l'AP-HM à verser une indemnité à Mme D....<br/>
<br/>
              2. En premier lieu, la seule circonstance qu'un rapport d'expertise, à l'initiative de l'expert, se prononce sur des questions excédant le champ de l'expertise ordonnée par la juridiction, n'est pas, par elle-même, de nature à entacher cette expertise d'irrégularité. Elle ne fait pas obstacle à ce que, s'ils ont été soumis au débat contradictoire en cours d'instance, les éléments de l'expertise par lesquels l'expert se prononce au-delà des termes de sa mission soient régulièrement pris en compte par le juge, soit lorsqu'ils ont le caractère d'éléments de pur fait non contestés par les parties, soit à titre d'éléments d'information dès lors qu'ils ne sont pas infirmés par d'autres éléments versés au dossier dans le cadre de l'instruction du litige. Par suite, l'AP-HM et la SHAM ne sont pas fondées à soutenir que la cour administrative d'appel aurait entaché son arrêt d'irrégularité en prenant en compte, pour juger qu'une faute avait été commise dans le choix de la technique opératoire, des éléments du rapport d'expertise du 22 juillet 2017 par lesquels l'expert s'est prononcé au-delà des termes de sa mission.<br/>
<br/>
              3. En deuxième lieu, il résulte des termes de l'arrêt attaqué que, pour juger que la responsabilité de l'AP-HM était engagée pour l'intégralité des conséquences dommageables de l'intervention subie par Mme D..., la cour a retenu non seulement le manquement par l'établissement de santé à son obligation d'information, qu'elle a jugé établi par son arrêt avant-dire droit du 17 mars 2016, mais également le caractère fautif du choix d'une intervention chirurgicale par voie orbito-péritonéale. Contrairement à ce que soutiennent l'AP-HM et la SHAM, l'invocation par Mme D... d'une seconde faute commise par l'établissement de santé était recevable pour la première fois en appel, y compris après l'expiration du délai d'appel contre le jugement du 26 mai 2014, dès lors que la victime avait déjà recherché, en première instance, la responsabilité de l'AP-HM sur le fondement de cette cause juridique en invoquant, devant le tribunal administratif, une faute commise par cet établissement. <br/>
<br/>
              4. Enfin, par l'arrêt attaqué, la cour administrative d'appel s'est à la fois prononcée sur la partie du litige qui lui avait été renvoyée par la décision du Conseil d'Etat, statuant au contentieux du 10 mars 2017 et sur la partie du litige dont elle était demeurée saisie, après son arrêt avant dire droit du 17 mars 2016 qui avait ordonné une expertise avant de statuer sur les conclusions indemnitaires de Mme D..., en réservant jusqu'en fin d'instance tous droits et moyens des parties sur lesquels il n'était pas expressément statué par cet arrêt. Dans ces conditions, elle a pu, sans revenir sur la chose déjà jugée, se prononcer sur la responsabilité de l'AP-HM à raison de la faute dans le choix de la technique chirurgicale, qui n'avait pas été antérieurement invoquée.<br/>
<br/>
              5. Il résulte de tout ce qui précède que l'AP-HM et de la SHAM ne sont pas fondées à demander l'annulation de l'arrêt qu'elles attaquent. Leur pourvoi doit par suite être rejeté, y compris les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'AP-HM et de la SHAM la somme de 1 500 euros à verser, chacune, à Mme D... au titre de l'article L. 761-1 du code de justice administrative. Les dispositions de cet article font, en revanche, obstacle à ce qu'il soit fait droit aux conclusions présentées à ce titre par l'ONIAM, auquel le pourvoi a été communiqué pour observations et qui n'a pas la qualité de partie dans la présente instance. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
		Article 1er : Le pourvoi de l'AP-HM et de la SHAM est rejeté.<br/>
<br/>
Article 2 : l'AP-HM et la SHAM verseront, chacune, une somme de 1 500 euros à Mme D... au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : Les conclusions présentées par l'ONIAM au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée à l'Assistance publique - hôpitaux de Marseille, à la Société hospitalière d'assurances mutuelles, à l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales et à Mme C... D....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-04-02-02-01-03 PROCÉDURE. INSTRUCTION. MOYENS D'INVESTIGATION. EXPERTISE. RECOURS À L'EXPERTISE. MISSION DE L'EXPERT. - RAPPORT D'EXPERTISE SE PRONONÇANT SUR DES QUESTIONS EXCÉDANT LE CHAMP DE L'EXPERTISE ORDONNÉE PAR LA JURIDICTION - 1) IRRÉGULARITÉ DE L'EXPERTISE - ABSENCE - 2) POSSIBILITÉ DE PRENDRE EN COMPTE LES ÉLÉMENTS EXCÉDANT LES TERMES DE LA MISSION CONFIÉE À L'EXPERT - EXISTENCE, S'AGISSANT DES D'ÉLÉMENTS DE PUR FAIT NON CONTESTÉS [RJ1] OU DES ÉLÉMENTS D'INFORMATION NON INFIRMÉS PAR D'AUTRES ÉLÉMENTS DU DOSSIER [RJ2].
</SCT>
<ANA ID="9A"> 54-04-02-02-01-03 1) La seule circonstance qu'un rapport d'expertise, à l'initiative de l'expert, se prononce sur des questions excédant le champ de l'expertise ordonnée par la juridiction, n'est pas, par elle-même, de nature à entacher cette expertise d'irrégularité.... ,,2) Elle ne fait pas obstacle à ce que, s'ils ont été soumis au débat contradictoire en cours d'instance, les éléments de l'expertise par lesquels l'expert se prononce au-delà des termes de sa mission soient régulièrement pris en compte par le juge, soit lorsqu'ils ont le caractère d'éléments de pur fait non contestés par les parties, soit à titre d'éléments d'information dès lors qu'ils ne sont pas infirmés par d'autres éléments versés au dossier dans le cadre de l'instruction du litige.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 26 juillet 1985, Seris et autre, n°s 41567 41636, T. pp. 690-731-732.,,[RJ2] Rappr., s'agissant de la possibilité de prendre en compte en compte les éléments d'information corroborés par d'autre éléments du dossier d'une expertise non contradictoire ou ordonnée dans le cadre d'un litige distinct, CE, 23 octobre 2019, Centre hospitalier Bretagne-Atlantique, n° 419274, à mentionner aux Tables du Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
