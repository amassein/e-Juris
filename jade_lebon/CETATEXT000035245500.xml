<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035245500</ID>
<ANCIEN_ID>JG_L_2017_07_000000389635</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/24/55/CETATEXT000035245500.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 5ème chambres réunies, 19/07/2017, 389635</TITRE>
<DATE_DEC>2017-07-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389635</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI ; SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>Mme Sophie Baron</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:389635.20170719</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Paris d'annuler pour excès de pouvoir la décision du 9 mai 2011 de l'inspecteur du travail autorisant la société Subventium à le licencier et la décision du 15 novembre 2011 du ministre chargé du travail rejetant son recours hiérarchique. Par un jugement n° 1200805 du 11 février 2014, le tribunal administratif a annulé ces décisions.<br/>
<br/>
              Par un arrêt n° 14PA01583 du 19 février 2015, la cour administrative d'appel de Paris a rejeté l'appel formé par la société Subventium contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 20 avril et 20 juillet 2015 au secrétariat du contentieux du Conseil d'Etat, la société Subventium, devenue société GSMC Innovation, Maître D...F..., agissant en qualité de mandataire judiciaire de cette société et Maître C...E..., agissant en qualité d'administrateur judiciaire de la même société, demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à l'appel de la société GSMC Innovation ;<br/>
<br/>
              3°) de mettre à la charge de M. A...la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie Baron, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, avocat de la société GSMC Innovation et autres et à la SCP Rocheteau, Uzan-Sarano, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 9 mai 2011, l'inspecteur du travail compétent a autorisé la société Subventium, devenue société GSMC Innovation, à licencier M. B... A..., salarié protégé, pour un motif disciplinaire tenant notamment à la souffrance au travail de ses collaborateurs ; que, par une décision du 15 novembre 2011, le ministre chargé du travail a rejeté le recours formé par M. A... contre cette décision ; que la société GSMC Innovation, Maître F...agissant en qualité de mandataire judiciaire de cette société et MaîtreE..., agissant en qualité d'administrateur judiciaire de la même société, se pourvoient en cassation contre l'arrêt du 19 février 2015 par lequel la cour administrative d'appel de Paris a rejeté l'appel formé par la société Subventium contre le jugement du 11 février 2014 du tribunal administratif de Paris annulant ces deux décisions ; <br/>
<br/>
              2. Considérant qu'en vertu des dispositions du code du travail, les salariés légalement investis de fonctions représentatives bénéficient, dans l'intérêt de l'ensemble des salariés qu'ils représentent, d'une protection exceptionnelle ; que, lorsque le licenciement d'un de ces salariés est envisagé, ce licenciement ne doit pas être en rapport avec les fonctions représentatives normalement exercées ou l'appartenance syndicale de l'intéressé ; que, dans le cas où la demande de licenciement est motivée par un comportement fautif, il appartient à l'inspecteur du travail, et le cas échéant au ministre, de rechercher, sous le contrôle du juge de l'excès de pouvoir, si les faits reprochés au salarié sont d'une gravité suffisante pour justifier son licenciement, compte tenu de l'ensemble des règles applicables au contrat de travail de l'intéressé et des exigences propres à l'exécution normale du mandat dont il est investi ; qu'à l'effet de concourir à la mise en oeuvre de la protection ainsi instituée, les articles R. 2421-4 et R. 2121-11 du code du travail disposent que l'inspecteur du travail, saisi d'une demande d'autorisation de licenciement d'un salarié protégé, " procède à une enquête contradictoire au cours de laquelle le salarié peut, sur sa demande, se faire assister d'un représentant de son syndicat " ;<br/>
<br/>
              3. Considérant que le caractère contradictoire de l'enquête menée conformément aux dispositions mentionnées ci-dessus impose à l'autorité administrative, saisie d'une demande d'autorisation de licenciement d'un salarié protégé fondée sur un motif disciplinaire, d'informer le salarié concerné des agissements qui lui sont reprochés et de l'identité des personnes qui en ont témoigné ; qu'il implique, en outre, que le salarié protégé soit mis à même de prendre connaissance de l'ensemble des pièces produites par l'employeur à l'appui de sa demande, dans des conditions et des délais lui permettant de présenter utilement sa défense, sans que la circonstance que le salarié est susceptible de connaître le contenu de certaines de ces pièces puisse exonérer l'inspecteur du travail de cette obligation ; que c'est seulement lorsque l'accès à certains de ces éléments serait de nature à porter gravement préjudice à leurs auteurs que l'inspecteur du travail doit se limiter à informer le salarié protégé, de façon suffisamment circonstanciée, de leur teneur ; <br/>
<br/>
              4. Considérant qu'il résulte des termes de l'arrêt attaqué que, pour juger que l'enquête contradictoire conduite par l'inspecteur du travail était entachée d'irrégularité, la cour administrative d'appel s'est fondée sur la circonstance que M. A... n'avait pas pu prendre connaissance des pièces produites par l'employeur à l'appui de sa demande d'autorisation de licenciement alors que, faute d'y avoir eu accès, il en avait demandé copie à l'administration ; que, dès lors qu'il appartenait à l'administration, saisie d'une telle demande, de lui assurer la possibilité, soit de consulter librement ces pièces et d'en prendre copie, soit de lui en adresser une copie, le cas échéant sous forme dématérialisée, la cour n'a, en statuant ainsi, pas commis d'erreur de droit ; <br/>
<br/>
              5. Considérant que l'accès, dans le cadre de l'enquête contradictoire prévue par les articles R. 2421-4 et R. 2421-11 du code du travail, à l'ensemble des pièces produites par l'employeur à l'appui de sa demande d'autorisation de licenciement, dans des conditions et des délais permettant de présenter utilement sa défense, constitue une garantie pour le salarié protégé ; qu'il suit de là que la cour administrative d'appel, qui a souverainement estimé, sans entacher son arrêt de dénaturation, que le déroulement de l'enquête contradictoire n'avait pas permis un tel accès à M.A..., n'a pas commis d'erreur de droit  en jugeant que la décision litigieuse de l'inspecteur du travail était, par suite, entachée d'illégalité ;<br/>
<br/>
              6. Considérant que la garantie offerte au salarié rappelée au point 5 ci-dessus constitue un droit pour celui-ci ; que son exercice ne saurait, par suite, revêtir par lui-même de caractère abusif ; que c'est, dès lors, à titre superfétatoire que la cour a retenu que la demande de communication formulée par M. A...n'était pas abusive ; que la société GSMC Innovation et autres ne sauraient ainsi utilement soutenir qu'elle a, ce faisant, dénaturé les pièces du dossier ; <br/>
<br/>
              7. Considérant que le moyen tiré de ce que le tribunal administratif ne pouvait juger irrégulier le refus de communication à M. A...des éléments qu'il sollicitait, au motif que ceux-ci étaient de nature à porter gravement préjudice à leurs auteurs, n'a pas été soulevé devant la cour administrative d'appel ; que, par suite, le moyen tiré de ce que la cour aurait commis une erreur de droit en ne recherchant pas si l'accès à ces documents pouvait causer un tel préjudice à leurs auteurs est inopérant ; qu'il doit ainsi être écarté ; <br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que le pourvoi de la société GSMC et autres doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ; que, dans les circonstances de l'espèce, il y a lieu de mettre à la charge de la société GSMC Innovation, de Maître F...et de Maître E...la somme de 1 000 euros chacun à verser  à M. A...au titre des mêmes dispositions ; <br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Le pourvoi de la société GSMC Innovation et autres est rejeté.<br/>
Article 2 : La société GSMC Innovation, Maître F...et Maître E...verseront, chacun, à M. A...la somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société GSMC Innovation, à Maître D...F..., à Maître C...E...et à M. B...A....<br/>
Copie en sera adressée à la ministre du travail.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-03-03-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. PROCÉDURE CONTRADICTOIRE. MODALITÉS. - ENQUÊTE CONTRADICTOIRE PRÉALABLE À LA DÉLIVRANCE D'UNE AUTORISATION ADMINISTRATIVE DE LICENCIEMENT D'UN SALARIÉ PROTÉGÉ POUR MOTIF DISCIPLINAIRE - 1) PORTÉE - A) OBLIGATION DE COMMUNIQUER AU SALARIÉ L'ENSEMBLE DES PIÈCES PRODUITES PAR L'EMPLOYEUR - INCLUSION [RJ1] - B) DROIT DE PRENDRE OU RECEVOIR COPIE DE CES PIÈCES - INCLUSION [RJ2] - 2) CARACTÈRE DE GARANTIE AU SENS DE LA JURISPRUDENCE DANTHONY [RJ3] - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">66-07-01-03-02-01 TRAVAIL ET EMPLOI. LICENCIEMENTS. AUTORISATION ADMINISTRATIVE - SALARIÉS PROTÉGÉS. MODALITÉS DE DÉLIVRANCE OU DE REFUS DE L'AUTORISATION. MODALITÉS D'INSTRUCTION DE LA DEMANDE. ENQUÊTE CONTRADICTOIRE. - 1) PORTÉE DANS LE CAS D'UNE DEMANDE FONDÉE SUR UN MOTIF DISCIPLINAIRE [RJ1] - A) OBLIGATION DE COMMUNIQUER AU SALARIÉ L'ENSEMBLE DES PIÈCES PRODUITES PAR L'EMPLOYEUR - INCLUSION [RJ1] - B) DROIT DE PRENDRE OU RECEVOIR COPIE DE CES PIÈCES - INCLUSION [RJ2] - 2) CARACTÈRE DE GARANTIE AU SENS DE LA JURISPRUDENCE DANTHONY [RJ3] - EXISTENCE.
</SCT>
<ANA ID="9A"> 01-03-03-03 1) a) Le caractère contradictoire de l'enquête menée conformément aux articles R. 2421-4 et R. 2421-11 du code du travail impose à l'autorité administrative, saisie d'une demande d'autorisation de licenciement d'un salarié protégé fondée sur un motif disciplinaire, d'informer le salarié concerné des agissements qui lui sont reprochés et de l'identité des personnes qui en ont témoigné. Il implique, en outre, que le salarié protégé soit mis à même de prendre connaissance de l'ensemble des pièces produites par l'employeur à l'appui de sa demande, dans des conditions et des délais lui permettant de présenter utilement sa défense, sans que la circonstance que le salarié est susceptible de connaître le contenu de certaines de ces pièces puisse exonérer l'inspecteur du travail de cette obligation. C'est seulement lorsque l'accès à certains de ces éléments serait de nature à porter gravement préjudice à leurs auteurs que l'inspecteur du travail doit se limiter à informer le salarié protégé, de façon suffisamment circonstanciée, de leur teneur.... ,,b) Il appartient à l'administration, saisie d'une demande présentée par le salarié concerné par la procédure de licenciement et tendant à ce que, faute d'y avoir eu accès, copie lui soit donnée des pièces produites par l'employeur à l'appui de sa demande d'autorisation de licenciement, d'assurer à ce salarié la possibilité soit de consulter librement ces pièces et d'en prendre copie, soit de lui en adresser une copie, le cas échéant sous forme dématérialisée.... ,,2) L'accès, dans le cadre de l'enquête contradictoire prévue par les articles R. 2421-4 et R. 2421-11 du code du travail, à l'ensemble des pièces produites par l'employeur à l'appui de sa demande d'autorisation de licenciement, dans des conditions et des délais permettant de présenter utilement sa défense, constitue une garantie pour le salarié protégé au sens de la jurisprudence Danthony.</ANA>
<ANA ID="9B"> 66-07-01-03-02-01 1) a) Le caractère contradictoire de l'enquête menée conformément aux articles R. 2421-4 et R. 2421-11 du code du travail impose à l'autorité administrative, saisie d'une demande d'autorisation de licenciement d'un salarié protégé fondée sur un motif disciplinaire, d'informer le salarié concerné des agissements qui lui sont reprochés et de l'identité des personnes qui en ont témoigné. Il implique, en outre, que le salarié protégé soit mis à même de prendre connaissance de l'ensemble des pièces produites par l'employeur à l'appui de sa demande, dans des conditions et des délais lui permettant de présenter utilement sa défense, sans que la circonstance que le salarié est susceptible de connaître le contenu de certaines de ces pièces puisse exonérer l'inspecteur du travail de cette obligation. C'est seulement lorsque l'accès à certains de ces éléments serait de nature à porter gravement préjudice à leurs auteurs que l'inspecteur du travail doit se limiter à informer le salarié protégé, de façon suffisamment circonstanciée, de leur teneur.... ,,b) Il appartient à l'administration, saisie d'une demande présentée par le salarié concerné par la procédure de licenciement et tendant à ce que, faute d'y avoir eu accès, copie lui soit donnée des pièces produites par l'employeur à l'appui de sa demande d'autorisation de licenciement, d'assurer à ce salarié la possibilité soit de consulter librement ces pièces et d'en prendre copie, soit de lui en adresser une copie, le cas échéant sous forme dématérialisée.... ,,2) L'accès, dans le cadre de l'enquête contradictoire prévue par les articles R. 2421-4 et R. 2421-11 du code du travail, à l'ensemble des pièces produites par l'employeur à l'appui de sa demande d'autorisation de licenciement, dans des conditions et des délais permettant de présenter utilement sa défense, constitue une garantie pour le salarié protégé au sens de la jurisprudence Danthony.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., en précisant, CE, Section, 24 novembre 2006,,, n° 284208, p. 481; CE, 9 juillet 2007,,, n° 288295, T. pp. 651-1109 ; CE, 22 février 2012,,, n° 346307, T. pp. 549-1012. Comp., s'agissant de la portée de la procédure contradictoire en cas de recours hiérarchique contre une décision d'autoriser le licenciement d'un salarié protégé pour un motif économique, décision du même jour, Société l'Agence du bâtiment, n° 391402, à mentionner aux Tables., ,[RJ2] Rappr., pour une procédure disciplinaire concernant un agent public, CE, 27 janvier 1982,,et autres, n° 29738, p. 36 ; CE, 2 avril 2015, Commune de Villecerf, n° 370242, T. pp. 723-729-733.,,[RJ3] Cf. CE, Assemblée, 23 décembre 2011,,et autres, n° 335033, p. 649.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
