<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028717845</ID>
<ANCIEN_ID>JG_L_2014_03_000000354629</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/71/78/CETATEXT000028717845.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 12/03/2014, 354629</TITRE>
<DATE_DEC>2014-03-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>354629</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Thierry Carriol</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Delphine Hedary</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:354629.20140312</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés au secrétariat du contentieux du Conseil d'Etat le 6 décembre 2011 et le 6 mars 2012, présentés pour la société Foncia Groupe, dont le siège est 3, rue de Londres à Paris (75009), représentée par son représentant légal ; la société Foncia Groupe demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la délibération n° 2011-205 du 6 octobre 2011 par laquelle la Commission nationale de l'informatique et des libertés a, d'une part, prononcé un avertissement à son encontre, et, d'autre part, décidé de le rendre public ;<br/>
<br/>
              2°) de rendre anonyme la décision à intervenir ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu la loi n° 78-17 du 7 janvier 1978, modifiée notamment par la loi n° 2011-334 du 29 mars 2011 ;<br/>
<br/>
              Vu le décret n° 2005-1309 du 20 octobre 2005 ;<br/>
<br/>
              Vu l'arrêté du Premier ministre du 9 octobre 2002 relatif au site internet Légifrance ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thierry Carriol, Maître des Requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de Mme Delphine Hedary, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de la société Foncia Groupe ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que, par une délibération du 6 octobre 2011, la formation restreinte de la Commission nationale de l'informatique et des libertés (CNIL) a infligé à la société Foncia Groupe, holding spécialisé dans l'administration de biens immobiliers, un avertissement rendu public pour avoir exploité un traitement informatique, dénommé " Totalimmo ", recensant les biens immobiliers disponibles pour des opérations de vente et de location, en méconnaissance de la loi du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés ;  <br/>
<br/>
              Sur les conclusions tendant à l'annulation de la décision attaquée : <br/>
<br/>
              Sur la légalité externe de la décision :<br/>
<br/>
              2. Considérant, en premier lieu, qu'en vertu des dispositions de l'article 44 de la loi du 6 janvier 1978 et de l'article 61 du décret du 20 octobre 2005,  lorsque des membres ou agents de la CNIL opèrent un contrôle dans des locaux servant à la mise en oeuvre d'un traitement de données à caractère personnel, le procureur territorialement compétent en est informé, au plus tard vingt-quatre heures avant le contrôle ; que le président de la CNIL a informé le 5 mai 2010 le procureur de la République près le tribunal de grande instance de Nanterre, territorialement compétent, du contrôle réalisé le lendemain dans les locaux de la société Foncia Groupe à Antony (Hauts-de-Seine) ; que si cette information a été communiquée à 15 heures pour un contrôle qui a débuté le lendemain à 9 heures 15, soit dans un délai inférieur à celui de vingt-quatre heures prescrit par les dispositions précitées, cette circonstance, dont il n'est pas établi ni même soutenu qu'en raison la brièveté de ces délais, elle aurait fait obstacle à l'exercice par le procureur de ses pouvoirs, et par suite à priver la société requérante d'une des garanties légales dont elle pouvait se prévaloir, n'est pas de nature à affecter la légalité de la décision attaquée ;<br/>
<br/>
              3. Considérant, en deuxième lieu, que si la société Foncia Groupe soutient que la décision qu'elle attaque a méconnu le principe du contradictoire et les droits de la défense, au motif que le rapport du 11 mai 2011 du rapporteur de la CNIL proposant de prononcer une sanction à son encontre n'était pas accompagné de pièces justifiant de la réalité des manquements critiqués, il est établi que la CNIL a informé la société, par des courriers des 12 mai et 21 juin 2011, de la possibilité, qu'elle a d'ailleurs utilisée le 23 juin 2011, de prendre connaissance de l'ensemble des pièces du dossier dans les locaux de la commission ; que le moyen tiré de l'irrégularité de la procédure sur ce point  ne peut ainsi qu'être écarté ;<br/>
<br/>
              4. Considérant, en troisième lieu, qu'il ressort des pièces du dossier que la procédure ayant conduit à infliger un avertissement à la société Foncia Groupe a été menée conformément aux dispositions de la loi du 6 janvier 1978, modifiées par celles de la loi du 29 mars 2011 relative au Défenseur des droits, entrées en vigueur le 31 mars 2011 ; qu'en vertu des dispositions de l'article 13 de cette loi ainsi modifiée, le président de la CNIL, compétent en matière de poursuites, ne siège pas dans la formation restreinte, compétente pour prononcer des sanctions ; que, par suite, le moyen tiré de ce que la procédure suivie aurait méconnu les stipulations de l'article 6 § 1 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, ainsi que les principes d'indépendance et d'impartialité, faute de séparation des fonctions de poursuite et de sanction au sein de la CNIL, doit être écarté ;<br/>
<br/>
              Sur le bien-fondé de la décision attaquée :<br/>
<br/>
              En ce qui concerne la qualité de responsable du traitement :<br/>
<br/>
              5. Considérant, en premier lieu, que si la société requérante soutient qu'elle ne saurait être regardée comme responsable du traitement contesté au sens des dispositions de l'article 3 de la loi du 6 janvier 1978, aux termes desquelles : " Le responsable d'un traitement de données à caractère personnel est, sauf désignation expresse par les dispositions législatives ou réglementaires relatives à ce traitement, la personne, l'autorité publique, le service ou l'organisme qui détermine ses finalités et ses moyens " et que, par suite,  la CNIL aurait commis une erreur de droit en lui infligeant une sanction, en méconnaissance de ces dispositions et du principe de personnalité des peines, applicable aux sanctions administratives, il résulte de l'instruction que la société Foncia Groupe, qui a mis à disposition des entités qui lui sont liées le traitement " Totalimmo ", a décidé de la nature des données collectées et déterminé les droits d'accès à celles-ci, puis, après le contrôle de la CNIL, a fixé la durée de conservation des données et apporté des correctifs à leur traitement ;  qu'ainsi, la société Foncia Groupe détermine les finalités et les moyens du traitement " Totalimmo " ; que la société ne peut être regardée comme sous-traitant au sens des dispositions de l'article 35 de la loi du 6 janvier 1978 ; que la désignation d'un correspondant à la protection des données par les entités n'a pas, par elle-même, pour effet de rendre celles-ci responsables des traitements ; que, par suite, en estimant que la société Foncia Groupe pouvait faire l'objet d'une sanction en tant que responsable de ce traitement, la CNIL a fait une exacte application des dispositions précitées  ; <br/>
<br/>
              En ce qui concerne la sanction infligée : <br/>
<br/>
              6. Considérant, en deuxième lieu, qu'aux termes de l'article 6 de la loi du 6 janvier 1978 : " Un traitement ne peut porter que sur des données à caractère personnel qui satisfont aux conditions suivantes / : (...) 3° Elles (les données) sont adéquates, pertinentes et non excessives au regard des finalités pour lesquelles elles sont collectées et de leurs traitements ultérieurs " ; qu'aux termes de l'article 8 de la même loi : " " I - Il est interdit de collecter ou de traiter des données à caractère personnel qui font apparaître, directement ou indirectement, les origines raciales ou ethniques, les opinions politiques, philosophiques ou religieuses ou l'appartenance syndicale des personnes, ou qui sont relatives à la santé ou à la vie sexuelle de celles-ci. / II - Dans la mesure où la finalité du traitement l'exige pour certaines catégories de données, ne sont pas soumis à l'interdiction prévue au I :  1° Les traitements pour lesquels la personne concernée a donné son consentement exprès, sauf dans le cas où la loi prévoit que l'interdiction visée au I ne peut être levée par le consentement de la personne concernée  " ; qu'aux termes du I de l'article 45 de cette loi : " La formation restreinte de la Commission nationale de l'informatique et des libertés peut prononcer, après une procédure contradictoire, un avertissement à l'égard du responsable d'un traitement qui ne respecte pas les obligations découlant de la présente loi. Cet avertissement a le caractère d'une sanction " ; qu'enfin, aux termes de l'article 46 de la même loi : " La formation restreinte peut rendre publiques les sanctions qu'elle prononce (...) " ; <br/>
<br/>
              7. Considérant que la société requérante soutient que la sanction d'un avertissement rendu public qui lui a été infligée est disproportionnée ; qu'il résulte toutefois de l'instruction et n'est d'ailleurs pas contesté que le contrôle du traitement des données à caractère personnel " Totalimmo " a révélé, dans la zone dite de " commentaires libres " de certaines fiches, la présence de données, portant notamment sur la santé ou les opinions religieuses des personnes, parfois formulées en termes outrageants, ne répondant pas aux prescriptions des articles 6 et 8 de la loi précitée ; que la société Foncia Groupe n'établit pas que des données mentionnées à l'article 8 et recensées dans certaines fiches individuelles auraient été recueillies, comme elle le prétend, avec le consentement exprès des personnes concernées ; que, dès lors, en estimant que la société Foncia Groupe avait méconnu les dispositions des articles 6 et 8 de la loi du 6 janvier 1978, la CNIL n'a pas commis d'erreur de droit et, eu égard à la gravité des manquements constatés, ne lui a pas infligé une sanction disproportionnée ;<br/>
<br/>
              8. Considérant, en troisième lieu, que si la décision par laquelle la CNIL rend publique la sanction prononcée a le caractère d'une sanction complémentaire, aucune disposition non plus qu'aucun principe n'impose qu'elle fasse l'objet d'une motivation spécifique, distincte de la motivation d'ensemble de la sanction principale qu'elle complète ; que, dès lors, le moyen tiré de ce que cette sanction complémentaire aurait été infligée irrégulièrement faute d'être motivée par l'intérêt général est sans incidence sur la légalité de la décision attaquée ;<br/>
<br/>
              9. Considérant qu'il résulte de ce qui précède que les conclusions de la société Foncia Groupe tendant à l'annulation de la délibération du 6 octobre 2011 par laquelle la CNIL a décidé de prononcer à son encontre un avertissement et de le rendre public doivent être rejetées ;<br/>
<br/>
              Sur les conclusions tendant à rendre anonyme la publication de la présente décision :<br/>
<br/>
              10. Considérant que la société requérante demande que la présente décision ne mentionne pas son nom lorsqu'elle sera publiée sur l'Internet par des bases de données juridiques, notamment par Légifrance ; <br/>
<br/>
              11. Considérant que l'arrêté du Premier ministre du 9 octobre 2002 régissant la publication sur le site Légifrance des décisions du Conseil d'Etat, statuant au contentieux ne prévoit pas que celles-ci puissent être rendues anonymes ; que, dès lors que ne peuvent trouver à s'appliquer, en l'espèce, les conditions tenant à la sauvegarde de l'ordre public ou au respect de l'intimité des personnes ou de secrets protégés par la loi auxquelles l'article L. 731-1 du code de justice administrative subordonne la possibilité de déroger aux principes de publicité des audiences et à celui de publicité des jugements, figurant respectivement aux articles L. 6 et L. 10 de ce code, toute personne peut obtenir copie de la présente décision ; que, par suite, les conclusions tendant à rendre celle-ci anonyme  doivent être rejetées ; <br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              12. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de la société Foncia Groupe est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la société Foncia Groupe et à la Commission nationale de l'informatique et des libertés.<br/>
 Copie en sera adressée, pour information, au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-07-04 DROITS CIVILS ET INDIVIDUELS. - IDENTIFICATION DE LA PERSONNE RESPONSABLE DU TRAITEMENT - INDICES [RJ2] - DÉFINITION DES FINALITÉS ET DES MOYENS DU TRAITEMENT.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">26-07-10-02 DROITS CIVILS ET INDIVIDUELS. - CONTRÔLE DANS DES LOCAUX SERVANT À LA MISE EN &#140;UVRE D'UN TRAITEMENT DE DONNÉES À CARACTÈRE PERSONNEL - 1) PRINCIPE - OBLIGATION D'INFORMER LE PROCUREUR TERRITORIALEMENT COMPÉTENT AU PLUS TARD VINGT-QUATRE HEURES AVANT LE CONTRÔLE - EXISTENCE - 2) ESPÈCE - INFORMATION COMMUNIQUÉE DANS UN DÉLAI INFÉRIEUR DE CINQ HEURES QUARANTE-CINQ MINUTES AU DÉLAI DE VINGT-QUATRE HEURES PRESCRIT - ABSENCE D'OBSTACLE À L'EXERCICE PAR LE PROCUREUR DE SES POUVOIRS - PRIVATION D'UNE GARANTIE AU SENS DE LA JURISPRUDENCE  DANTHONY  [RJ1] - ABSENCE.
</SCT>
<ANA ID="9A"> 26-07-04 Société ayant mis un traitement de données à caractère personnel à disposition des entités qui lui sont liées, décidé de la nature des données collectées et déterminé les droits d'accès à celles-ci puis, après le contrôle de la Commission nationale de l'informatique et des libertés (CNIL), ayant fixé la durée de conservation des données et apporté des correctifs à leur traitement. Ainsi, cette société, qui détermine les finalités et les moyens du traitement, doit être regardée comme le responsable du traitement, la désignation d'un correspondant à la protection des données par les autres entités n'ayant pas, par elle-même, pour effet de rendre celles-ci responsables des traitements.</ANA>
<ANA ID="9B"> 26-07-10-02 1) En vertu des dispositions de l'article 44 de la loi n° 78-17 du 6 janvier 1978 et de l'article 61 du décret n° 2005-1309 du 20 octobre 2005, lorsque des membres ou agents de la Commission nationale de l'informatique et des libertés (CNIL) opèrent un contrôle dans des locaux servant à la mise en oeuvre d'un traitement de données à caractère personnel, le procureur territorialement compétent doit en être informé, au plus tard vingt-quatre heures avant le contrôle.,,,2) En l'espèce, si l'information a été communiquée au procureur territorialement compétent à 15 heures pour un contrôle qui a débuté le lendemain à 9 heures 15, soit dans un délai inférieur de cinq heures quarante-cinq minutes au délai de vingt-quatre heures prescrit, cette circonstance, dont il n'est pas établi ni même soutenu qu'en raison de la brièveté de ces délais, elle aurait fait obstacle à l'exercice par le procureur de ses pouvoirs, et par suite privé la société requérante d'une des garanties légales dont elle pouvait se prévaloir, n'est pas de nature à affecter la légalité de la décision de sanction prononcée par la CNIL.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 23 décembre 2011, Danthony et autres, n° 335033, p. 649.,,[RJ2] Cf., pour le recours à la méthode du faisceau d'indices, CE, 27 juillet 2012, Société AIS 2, n° 340026, T. p. 766.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
