<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035245575</ID>
<ANCIEN_ID>JG_L_2017_07_000000405897</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/24/55/CETATEXT000035245575.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 19/07/2017, 405897</TITRE>
<DATE_DEC>2017-07-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>405897</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>M. François Weil</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:405897.20170719</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés les 12 décembre 2016 et 13 mars 2017 au secrétariat du contentieux du Conseil d'Etat, M. B...A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du 22 juillet 2016 rapportant le décret du 10 septembre 2014 lui ayant accordé la nationalité française ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros sur le fondement de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - le code civil ;<br/>
              - le décret n° 93-1362 du 30 décembre 1993 ;<br/>
              - l'arrêt de la Cour de justice de l'Union européenne C-135/08 du 2 mars 2010 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Weil, conseiller d'Etat,<br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de M. A...; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'aux termes de l'article 27-2 du code civil : " Les décrets portant acquisition, naturalisation ou réintégration peuvent être rapportés sur avis conforme du Conseil d'Etat dans le délai de deux ans à compter de leur publication au Journal officiel si le requérant ne satisfait pas aux conditions légales ; si la décision a été obtenue par mensonge ou fraude, ces décrets peuvent être rapportés dans le délai de deux ans à partir de la découverte de la fraude " ; <br/>
<br/>
              2.	Considérant qu'il ressort des pièces du dossier que M.A..., ressortissant algérien, a été naturalisé par décret du 10 septembre 2014, publié au Journal officiel du 12 septembre 2014 ; que ce décret a toutefois été rapporté par décret du 22 juillet 2016, au motif que l'intéressé avait entretenu des contacts réguliers avec des personnes ayant organisé le recrutement de Français pour le djihad en Syrie, qu'il avait tenté sans succès de rejoindre ce pays en 2013 et qu'il avait ensuite maintenu des liens avec la mouvance djihadiste en Syrie et des candidats au djihad en France et qu'il ne pouvait, en conséquence, être regardé comme satisfaisant aux conditions posées par les articles 21-23 et 21-24 du code civil ; <br/>
<br/>
              3.	Considérant, en premier lieu, qu'il ressort des mentions de l'ampliation du décret attaqué figurant au dossier, certifiée conforme par le secrétaire général du Gouvernement, que le décret a été signé par le Premier ministre et contresigné par le ministre de l'intérieur ; que l'ampliation notifiée à M. A...n'avait pas à être revêtue de ces signatures ;<br/>
<br/>
              4.	Considérant, en deuxième lieu, que le décret attaqué comporte l'indication des éléments de droit et de fait sur lesquels il se fonde et est ainsi suffisamment motivé ; <br/>
<br/>
              5.	Considérant, en troisième lieu, qu'il ressort des pièces versées au dossier par le ministre de l'intérieur que le texte du décret attaqué ne diffère pas de celui adopté le 5 juillet 2016 par la section de l'intérieur du Conseil d'Etat ; qu'ainsi, le moyen tiré de ce que le décret attaqué n'aurait pas été pris sur l'avis conforme du Conseil d'Etat ne peut qu'être écarté ;<br/>
<br/>
              6.	Considérant, en quatrième lieu, qu'aux termes du premier alinéa de l'article 21-23 du même code : " Nul ne peut être naturalisé s'il n'est pas de bonnes vie et moeurs ou s'il a fait l'objet de l'une des condamnations visées à l'article 21-27 du présent code " ; qu'aux termes du premier alinéa de l'article 21-24 du même code : " Nul ne peut être naturalisé s'il ne justifie de son assimilation à la communauté française, notamment par une connaissance suffisante, selon sa condition, de la langue, de l'histoire, de la culture et de la société françaises, dont le niveau et les modalités d'évaluation sont fixés par décret en Conseil d'Etat, et des droits et devoirs conférés par la nationalité française ainsi que par l'adhésion aux principes et aux valeurs essentiels de la République " ; <br/>
<br/>
              7.	Considérant, d'une part, qu'il ressort des pièces du dossier, en particulier des éléments portés à la connaissance des services du ministère chargé des naturalisations par une note de la direction générale de la sécurité intérieure du 31 août 2015, que M. A...a été en relation en France avec un recruteur de volontaires aspirant à partir combattre en Syrie ; qu'il a tenté, en juillet 2013, de se rendre en Syrie en compagnie de ce recruteur et de deux autres personnes qui ont gagné la Turquie, puis la Syrie ou l'Irak ; qu'il a, pour sa part, été refoulé par les autorités turques et expulsé vers l'Algérie, avant de revenir en France ; qu'il est ensuite resté en contact avec des personnes appartenant à la mouvance djihadiste ; qu'au vu de ces éléments, qui n'étaient pas connus des services en charge de l'instruction de la demande de naturalisation avant l'intervention du décret du 10 septembre 2014, le Premier ministre, qui a pris en considération l'ensemble des circonstances de l'espèce et a procédé à un examen particulier de la situation de l'intéressé, a pu légalement estimer que son comportement ne permettait pas de le regarder comme remplissant, à la date du décret de naturalisation, les conditions posées par les articles 21-23 et 21-24 du code civil ; que, par suite, en rapportant le décret ayant prononcé la naturalisation de M.A..., le Premier ministre n'a pas fait une inexacte application de l'article 27-2 du code civil ; <br/>
<br/>
              8.	Considérant, d'autre part, que la définition des conditions d'acquisition et de perte de la nationalité relève de la compétence de chaque État membre de l'Union européenne ; que, toutefois, dans la mesure où la perte de la nationalité d'un Etat membre a pour conséquence la perte du statut de citoyen de l'Union, la perte de la nationalité d'un Etat membre doit, pour être conforme au droit de l'Union, répondre à des motifs d'intérêt général et être proportionnée à la gravité des faits qui la fondent, au délai écoulé depuis l'acquisition de la nationalité et à la possibilité pour l'intéressé de recouvrer une autre nationalité ; que l'article 27-2 du code civil permet de rapporter, dans un délai de deux ans, un décret qui a conféré la nationalité française au motif que l'intéressé ne remplit pas les conditions mises par la loi à l'acquisition de la nationalité française ; que ces dispositions ne sont pas incompatibles avec les exigences résultant du droit de l'Union et permettaient de rapporter le décret de naturalisation, dès lors que M. A...ne remplissait pas, ainsi qu'il a été dit, les conditions posées par les articles 21-23 et 21-24 du code civil ;<br/>
<br/>
              9.	Considérant, en cinquième lieu, qu'un décret qui rapporte un décret ayant conféré la nationalité française est, par lui-même, dépourvu d'effet sur la présence sur le territoire français de celui qu'il vise, comme sur ses liens avec les membres de sa famille, et n'affecte pas, dès lors, le droit au respect de sa vie familiale ; qu'en revanche, un tel décret affecte un élément constitutif de l'identité de la personne concernée et est ainsi susceptible de porter atteinte au droit au respect de sa vie privée ; qu'en l'espèce, toutefois, eu égard à la date à laquelle il est intervenu et aux motifs qui le fondent, le décret attaqué ne peut être regardé comme ayant porté une atteinte disproportionnée au droit au respect de la vie privée de M. A... garanti par l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              10.	Considérant qu'il résulte de ce qui précède que M. A...n'est pas fondé à demander l'annulation pour excès de pouvoir du décret du 22 juillet 2016 rapportant le décret du 10 septembre 2014 qui lui avait accordé la nationalité française ; que ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, en conséquence, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de M. A...est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. B...A...et au ministre d'Etat, ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">15-05-04 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. RÈGLES APPLICABLES. CITOYENNETÉ EUROPÉENNE ET LUTTE CONTRE LES DISCRIMINATIONS. - 1) CONDITIONS DE CONFORMITÉ AU DROIT DE L'UNION DES RÈGLES DE PERTE DE LA NATIONALITÉ [RJ1] - 2) CAS DU RETRAIT D'UNE DÉCISION PORTANT ACQUISITION, NATURALISATION OU RÉINTÉGRATION AU MOTIF QUE LE REQUÉRANT NE SATISFAIT PAS AUX CONDITIONS LÉGALES (ART. 27-2 DU CODE CIVIL) - COMPATIBILITÉ AVEC LES EXIGENCES RÉSULTANT DU DROIT DE L'UNION - EXISTENCE [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">26-01-01-01 DROITS CIVILS ET INDIVIDUELS. ÉTAT DES PERSONNES. NATIONALITÉ. ACQUISITION DE LA NATIONALITÉ. - RETRAIT D'UNE DÉCISION PORTANT ACQUISITION, NATURALISATION OU RÉINTÉGRATION (ART. 27-2 DU CODE CIVIL) - CAS OÙ L'ÉTRANGER NE SATISFAIT PAS AUX CONDITIONS LÉGALES - 1) COMPATIBILITÉ AVEC LES EXIGENCES RÉSULTANT DU DROIT DE L'UNION - EXISTENCE [RJ2] - 2) ATTEINTE AUX DROITS GARANTIS PAR L'ARTICLE 8 DE LA CONVENTION EDH [RJ3] - A) DROIT AU RESPECT DE LA VIE FAMILIALE - ABSENCE - B) DROIT AU RESPECT DE LA VIE PRIVÉE - EXISTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">26-055-01-08-01 DROITS CIVILS ET INDIVIDUELS. CONVENTION EUROPÉENNE DES DROITS DE L'HOMME. DROITS GARANTIS PAR LA CONVENTION. DROIT AU RESPECT DE LA VIE PRIVÉE ET FAMILIALE (ART. 8). CHAMP D'APPLICATION. - RETRAIT D'UNE DÉCISION PORTANT ACQUISITION, NATURALISATION OU RÉINTÉGRATION AU MOTIF QUE LE REQUÉRANT NE SATISFAIT PAS AUX CONDITIONS LÉGALES (ART. 27-2 DU CODE CIVIL) [RJ3] - 1) DROIT AU RESPECT DE LA VIE FAMILIALE - EXCLUSION - 2) DROIT AU RESPECT DE LA VIE PRIVÉE - INCLUSION.
</SCT>
<ANA ID="9A"> 15-05-04 1) La définition des conditions d'acquisition et de perte de la nationalité relève de la compétence de chaque État membre de l'Union européenne. Toutefois, dans la mesure où la perte de la nationalité d'un Etat membre a pour conséquence la perte du statut de citoyen de l'Union, la perte de la nationalité d'un Etat membre doit, pour être conforme au droit de l'Union, répondre à des motifs d'intérêt général et être proportionnée à la gravité des faits qui la fondent, au délai écoulé depuis l'acquisition de la nationalité et à la possibilité pour l'intéressé de recouvrer une autre nationalité.... ,,2) L'article 27-2 du code civil, qui permet de rapporter, dans un délai de deux ans, un décret qui a conféré la nationalité française au motif que l'intéressé ne remplit pas les conditions mises par la loi à l'acquisition de la nationalité française, n'est pas incompatible avec les exigences résultant du droit de l'Union.</ANA>
<ANA ID="9B"> 26-01-01-01 1) L'article 27-2 du code civil, qui permet de rapporter, dans un délai de deux ans, un décret qui a conféré la nationalité française au motif que l'intéressé ne remplit pas les conditions mises par la loi à l'acquisition de la nationalité française, n'est pas incompatible avec les exigences résultant du droit de l'Union.,,,2) a) Un décret qui rapporte un décret ayant conféré la nationalité française est, par lui-même, dépourvu d'effet sur la présence sur le territoire français de celui qu'il vise, comme sur ses liens avec les membres de sa famille, et n'affecte pas, dès lors, le droit au respect de sa vie familiale.,,,b) En revanche, un tel décret affecte un élément constitutif de l'identité de la personne concernée et est ainsi susceptible de porter atteinte au droit au respect de sa vie privée.</ANA>
<ANA ID="9C"> 26-055-01-08-01 1) Un décret qui rapporte un décret ayant conféré la nationalité française est, par lui-même, dépourvu d'effet sur la présence sur le territoire français de celui qu'il vise, comme sur ses liens avec les membres de sa famille, et n'affecte pas, dès lors, le droit au respect de sa vie familiale.,,,2) En revanche, un tel décret affecte un élément constitutif de l'identité de la personne concernée et est ainsi susceptible de porter atteinte au droit au respect de sa vie privée.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Rappr. CJUE, Grande Chambre, 2 mars 2010, Rottmann, C-135/08.,,[RJ2]Rappr., s'agissant d'une décision de déchéance de nationalité, CE, 11 mai 2015, M.,, 383664, inédite au Recueil.,,[RJ3]Rappr., s'agissant d'une décision de déchéance de nationalité, CE, 8 juin 2016, M.,, n° 394348, p. 231.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
