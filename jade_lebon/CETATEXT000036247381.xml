<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036247381</ID>
<ANCIEN_ID>JG_L_2017_12_000000410381</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/24/73/CETATEXT000036247381.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 20/12/2017, 410381</TITRE>
<DATE_DEC>2017-12-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>410381</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Etienne de Lageneste</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:410381.20171220</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête enregistrée le 9 mai 2017 au secrétariat du contentieux du Conseil d'Etat, le Syndicat national des agents des douanes-CGT demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'article 3 de l'arrêté du 27 février 2017 du ministre de l'économie et des finances portant modification de la liste des bureaux des douanes et droits indirects ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 600 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des douanes, notamment son article 47 ;<br/>
              - la loi n° 83-634 du 13 juillet 1983, notamment son article 9 ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - la loi n° 95-115 du 4 février 1995 ;<br/>
              - la loi n° 2010-751 du 5 juillet 2010 ;<br/>
              - le décret n° 82-453 du 28 mai 1982 ;<br/>
              - le décret n° 90-437 du 28 mai 1990 ;<br/>
                           - le décret n° 2011-184 du 15 février 2011 ;<br/>
              - le code de justice administrative ;		<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Etienne de Lageneste, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du 1 de l'article 47 du code des douanes : " Les bureaux de douane sont établis et supprimés par des arrêtés du ministre de l'économie et des finances sur la proposition du directeur général des douanes et droits indirects ". Les dispositions attaquées de l'arrêté ministériel du 27 février 2017, prises sur le fondement des dispositions précitées, s'inscrivent dans le cadre d'un projet de réorganisation de l'administration des douanes engagé en 2013 et prévoient la suppression du bureau des douanes et droits indirects d'Evreux et le transfert de son activité aux bureaux de Rouen-port et Rouen-transports.<br/>
<br/>
              2. Les articles 15 et 16 de la loi du 11 janvier 1984 prévoient, respectivement et dans leur rédaction issue des lois n° 2013-1168 du 18 décembre 2013 et n° 2010-751 du 5 juillet 2010, que, dans toutes les administrations de l'Etat et dans les établissements publics ne présentant pas un caractère industriel et commercial, les comités techniques " connaissent des questions relatives à l'organisation et au fonctionnement des services " et les comités d'hygiène de sécurité et des conditions de travail ont " pour mission de contribuer à la protection de la santé physique et mentale et de la sécurité des agents dans leur travail, à l'amélioration des conditions de travail et de veiller à l'observation des prescriptions légales prises en ces matières ". L'article 34 du décret du 15 février 2011 relatif aux comités techniques dans les administrations et les établissements publics de l'Etat, pris pour l'application de l'article 15 de la loi du 11 janvier 1984, énumère les questions et projets de textes sur lesquels les comités techniques sont obligatoirement consultés, qui incluent ceux relatifs à l'organisation et au fonctionnement des administrations, établissements ou services. En vertu du même article, d'une part, les comités techniques ne sont consultés sur les questions relatives à l'hygiène, à la sécurité et aux conditions de travail que lorsqu'aucun comité d'hygiène de sécurité et de conditions de travail n'est placé auprès d'eux, d'autre part, les comités techniques bénéficient du concours du comité d'hygiène, peuvent le saisir de toute question et examinent toute question que lui soumet le comité d'hygiène. L'article 47 du décret du 28 mai 1982 pris pour l'application de l'article 16 de la loi du 11 janvier 1984, précise, dans sa rédaction issue du décret n° 2011-774 du 28 juin 2011, que les comités d'hygiène, de sécurité et des conditions de travail exercent leurs missions " sous réserve des compétences des comités techniques ". Le 1° de l'article 57 du même décret prévoit que le comité d'hygiène, de sécurité et des conditions de travail est notamment consulté " sur les projets d'aménagement importants modifiant les conditions de santé et de sécurité ou les conditions de travail (...) ".<br/>
<br/>
              3. Il résulte de ces dispositions qu'une question ou un projet de disposition ne doit être soumis à la consultation du comité d'hygiène, de sécurité et des conditions de travail que si le comité technique ne doit pas lui-même être consulté sur la question ou le projet de disposition en cause. Le comité d'hygiène, de sécurité et des conditions de travail ne doit ainsi être saisi que d'une question ou projet de disposition concernant exclusivement la santé, la sécurité ou les conditions de travail. En revanche, lorsqu'une question ou un projet de disposition concerne ces matières et l'une des matières énumérées à l'article 34 du décret du 15 février 2011, seul le comité technique doit être obligatoirement consulté. Ce comité peut, le cas échéant, saisir le comité d'hygiène, de sécurité et des conditions de travail de toute question qu'il juge utile de lui soumettre. En outre, l'administration a toujours la faculté de consulter le comité d'hygiène, de sécurité et des conditions de travail.<br/>
<br/>
              4. Dans le cas où, sans y être légalement tenue, elle sollicite l'avis d'un organisme consultatif au sujet, notamment, d'un projet de réorganisation des services, l'administration doit procéder à cette consultation dans des conditions régulières.<br/>
<br/>
              5. Aux termes de l'article 55 du décret du 28 mai 1982 relatif à l'hygiène et à la sécurité du travail ainsi qu'à la prévention médicale dans la fonction publique : " Le comité d'hygiène, de sécurité et des conditions de travail peut demander au président de faire appel à un expert agréé conformément aux articles R. 4614-6 et suivants du code du travail : / (...) 2° En cas de projet important modifiant les conditions de santé et de sécurité ou les conditions de travail, prévu à l'article 57. (...) / La décision de l'administration refusant de faire appel à un expert doit être substantiellement motivée. Cette décision est communiquée au comité d'hygiène, de sécurité et des conditions de travail ministériel. / En cas de désaccord sérieux et persistant entre le comité et l'autorité administrative sur le recours à l'expert agréé, la procédure prévue à l'article 5-5 peut être mise en oeuvre ". L'article 5-5 du même décret dispose que : " (...) en cas de désaccord sérieux et persistant entre l'administration et le comité d'hygiène, de sécurité et des conditions de travail, le chef de service compétent ainsi que le comité d'hygiène et de sécurité compétent peuvent solliciter l'intervention de l'inspection du travail. Les inspecteurs santé et sécurité au travail, peuvent également solliciter cette intervention. / Dans le cas d'un désaccord sérieux et persistant, l'inspection du travail n'est saisie que si le recours aux inspecteurs santé et sécurité au travail n'a pas permis de lever le désaccord (...) ".<br/>
<br/>
              6. D'une part, il ressort des pièces du dossier que la suppression du bureau de douanes d'Evreux, dont il n'est pas contesté qu'elle constitue un projet important modifiant les conditions de travail, au sens du 2° de l'article 55 du décret du 28 mai 1982, a été mise à l'ordre du jour du comité d'hygiène, de sécurité et des conditions de travail de la direction régionale de Rouen, dont relevait le bureau d'Evreux. Ce comité en a débattu à plusieurs reprises et notamment lors de sa séance du 9 janvier 2017, au cours de laquelle, après avoir constaté l'existence d'un désaccord sérieux et persistant, l'administration a décidé de mettre en oeuvre la procédure prévue à l'article 5-5 précité du décret du 28 mai 1982. L'administration a toutefois, au cours de la même séance et sans attendre que l'inspection du travail se prononce sur la question de la nomination d'un expert, fait procéder à un vote sur le projet envisagé, auquel les représentants du personnel ont refusé de participer.<br/>
<br/>
              7. D'autre part, le rapport de l'inspecteur du travail remis le 15 mai 2017, postérieurement à la publication de l'arrêté attaqué, recommande de nommer un expert en vue d'évaluer l'impact de la réorganisation envisagée sur les conditions de travail des agents concernés.<br/>
<br/>
              8. Il résulte de ce qui précède que, eu égard à la garantie que constitue le recours à un expert agréé et à l'influence que le rapport de ce dernier pouvait avoir sur les dispositions attaquées, le comité d'hygiène, de sécurité et des conditions de travail n'a pas disposé pas des éléments suffisants pour permettre sa consultation sur le projet en cause. Par suite, l'avis du comité du 9 janvier 2017 a été rendu au terme d'une procédure irrégulière. Dès lors, il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens de la requête, que le Syndicat national des agents des douanes-CGT est fondé à demander l'annulation de l'article 3 de l'arrêté attaqué.<br/>
<br/>
               9. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par le Syndicat national des agents des douanes-CGT au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>              D E C I D E :<br/>
                            --------------<br/>
<br/>
Article 1er : L'article 3 de l'arrêté du 27 février 2017 est annulé.<br/>
<br/>
Article 2 : Les conclusions du Syndicat national des agents des douanes-CGT présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée au Syndicat national des agents des douanes, Confédération générale du travail et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-03-02-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. PROCÉDURE CONSULTATIVE. CONSULTATION NON OBLIGATOIRE. - OBLIGATION POUR L'ADMINISTRATION DE CONSULTER DE MANIÈRE RÉGULIÈRE UN ORGANISME QU'ELLE CONSULTE À TITRE FACULTATIF - CAS D'UNE CONSULTATION FACULTATIVE DU CHSCT SUR UN PROJET D'ARRÊTÉ - ADMINISTRATION AYANT SAISI L'INSPECTEUR DU TRAVAIL SUR LE FONDEMENT DE L'ARTICLE 5-5 DU DÉCRET DU 28 MAI 1982, TOUT EN FAISANT PROCÉDER AU VOTE SANS ATTENDRE QUE CE DERNIER SE SOIT PRONONCÉ SUR LA NOMINATION D'UN EXPERT - IRRÉGULARITÉ DE NATURE À ENTRAÎNER L'ANNULATION DE L'ARRÊTÉ - EXISTENCE, EU ÉGARD À LA GARANTIE QUE CONSTITUE LE RECOURS À UN EXPERT AGRÉÉ ET À L'INFLUENCE QUE SON RAPPORT POUVAIT AVOIR SUR CE PROJET D'ARRÊTÉ [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-07-065 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. COMITÉS D'HYGIÈNE ET DE SÉCURITÉ. - OBLIGATION POUR L'ADMINISTRATION DE CONSULTER DE MANIÈRE RÉGULIÈRE UN ORGANISME QU'ELLE CONSULTE À TITRE FACULTATIF - CAS D'UNE CONSULTATION FACULTATIVE DU CHSCT SUR UN PROJET D'ARRÊTÉ - ADMINISTRATION AYANT SAISI L'INSPECTEUR DU TRAVAIL SUR LE FONDEMENT DE L'ARTICLE 5-5 DU DÉCRET DU 28 MAI 1982, TOUT EN FAISANT PROCÉDER AU VOTE SANS ATTENDRE QUE CE DERNIER SE SOIT PRONONCÉ SUR LA NOMINATION D'UN EXPERT - IRRÉGULARITÉ DE NATURE À ENTRAÎNER L'ANNULATION DE L'ARRÊTÉ - EXISTENCE, EU ÉGARD À LA GARANTIE QUE CONSTITUE LE RECOURS À UN EXPERT AGRÉÉ ET À L'INFLUENCE QUE SON RAPPORT POUVAIT AVOIR SUR CE PROJET D'ARRÊTÉ [RJ1].
</SCT>
<ANA ID="9A"> 01-03-02-03 Administration ayant, sans y être légalement tenue, consulté le comité d'hygiène, de sécurité et des conditions de travail (CHSCT) de la direction régionale des droits indirects au sujet d'un projet d'arrêté portant modification de la liste des bureaux des douanes et droits indirects, supprimant le bureau d'Evreux et transférant son activité à deux bureaux situés à Rouen.,,,Après avoir constaté l'existence d'un désaccord sérieux et persistant, l'administration a décidé de solliciter l'intervention de l'inspecteur du travail selon la procédure prévue à l'article 5-5 du décret n° 82-453 du 28 mai 1982, tout en mettant aux voix, sans attendre que ce dernier se prononce sur la question de la nomination d'un expert, le projet envisagé. Le rapport de l'inspecteur du travail, remis postérieurement à la publication de l'arrêté, a par la suite recommandé la nomination d'un expert en vue d'évaluer l'impact de la réorganisation envisagée sur les conditions de travail des agents concernés.,,,Eu égard à la garantie que constitue le recours à un expert agréé et à l'influence que le rapport de ce dernier pouvait avoir sur les dispositions de l'arrêté, le CHSCT n'a pas disposé des éléments suffisants pour permettre sa consultation sur le projet en cause. Par suite, son avis a été rendu au terme d'une procédure irrégulière. Dès lors, le syndicat requérant était fondé à demander l'annulation pour excès de pouvoir de l'arrêté.</ANA>
<ANA ID="9B"> 36-07-065 Administration ayant, sans y être légalement tenue, consulté le comité d'hygiène, de sécurité et des conditions de travail (CHSCT) de la direction régionale des droits indirects au sujet d'un projet d'arrêté portant modification de la liste des bureaux des douanes et droits indirects, supprimant le bureau d'Evreux et transférant son activité à deux bureaux situés à Rouen.,,,Après avoir constaté l'existence d'un désaccord sérieux et persistant, l'administration a décidé de solliciter l'intervention de l'inspecteur du travail selon la procédure prévue à l'article 5-5 du décret n° 82-453 du 28 mai 1982, tout en mettant aux voix, sans attendre que ce dernier se prononce sur la question de la nomination d'un expert, le projet envisagé. Le rapport de l'inspecteur du travail, remis postérieurement à la publication de l'arrêté, a par la suite recommandé la nomination d'un expert en vue d'évaluer l'impact de la réorganisation envisagée sur les conditions de travail des agents concernés.,,,Eu égard à la garantie que constitue le recours à un expert agréé et à l'influence que le rapport de ce dernier pouvait avoir sur les dispositions de l'arrêté, le CHSCT n'a pas disposé des éléments suffisants pour permettre sa consultation sur le projet en cause. Par suite, son avis a été rendu au terme d'une procédure irrégulière. Dès lors, le syndicat requérant était fondé à demander l'annulation pour excès de pouvoir de l'arrêté.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 23 décembre 2011,,et autres, n° 335033, p. 649 ; CE, Section, 23 décembre 2011,,et autres, n° 335477, p. 653.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
