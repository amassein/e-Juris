<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026537604</ID>
<ANCIEN_ID>JG_L_2012_10_000000350737</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/53/76/CETATEXT000026537604.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 26/10/2012, 350737</TITRE>
<DATE_DEC>2012-10-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>350737</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER ; SCP BOULLOCHE</AVOCATS>
<RAPPORTEUR>M. Rémi Decout-Paolini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:350737.20121026</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance n° 11MA02417 du 28 juin 2011, enregistrée le 8 juillet 2011 au secrétariat du contentieux du Conseil d'Etat, par laquelle le président de la cour administrative d'appel de Marseille a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi présenté devant cette cour par la commune de Saint-Jean-Cap-Ferrat ;<br/>
<br/>
              Vu le pourvoi, enregistré le 23 juin 2011 au greffe de la cour administrative d'appel de Marseille, et le nouveau mémoire, enregistré le 12 septembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune de Saint-Jean-Cap-Ferrat, représentée par son maire ; la commune demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 0805820 du 5 mai 2011 par lequel le tribunal administratif de Nice, à la demande de la société Changeventure Ltd, a annulé l'arrêté du 25 juillet 2008 par lequel son maire s'est opposé aux travaux déclarés par cette société ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de la société ; <br/>
<br/>
              3°) de mettre à la charge de la société Changeventure Ltd le versement de la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu le code de l'environnement ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Rémi Decout-Paolini, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Boulloche, avocat de la commune de Saint-Jean-Cap-Ferrat et de la SCP Célice, Blancpain, Soltner, avocat de la société Changeventure Ltd,<br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Boulloche, avocat de la commune de Saint-Jean-Cap-Ferrat et à la SCP Célice, Blancpain, Soltner, avocat de la société Changeventure Ltd ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Sur la régularité du jugement attaqué :<br/>
<br/>
              1. Considérant que, contrairement à ce que soutient la commune de Saint-Jean-Cap-Ferrat, le tribunal administratif de Nice n'a pas omis de viser et d'analyser ses deux mémoires en défense ; que, par ailleurs, le moyen tiré de ce que le tribunal se serait abstenu de répondre aux moyens soulevés dans ces mémoires n'est pas assorti des précisions permettant d'en apprécier le bien-fondé ;<br/>
<br/>
              Sur le bien-fondé du jugement attaqué :<br/>
<br/>
              2. Considérant, en premier lieu, qu'aux termes de l'article R. 424-1 du code de l'urbanisme : " A défaut de notification d'une décision expresse dans le délai d'instruction (...), le silence gardé par l'autorité compétente vaut, selon les cas : / a) Décision de non-opposition à la déclaration préalable ; / b) Permis de construire, permis d'aménager ou permis de démolir tacite. " ; qu'aux termes de l'article R. 425-17 du même code : " Lorsque le projet est situé dans un site classé (...), la décision prise sur la demande de permis de construire ou sur la déclaration préalable ne peut intervenir qu'avec l'accord exprès prévu par les articles L. 341-7 et L. 341-10 du code de l'environnement : a) Cet accord est donné par le préfet, après avis de l'architecte des Bâtiments de France, lorsque le projet fait l'objet d'une déclaration préalable (...) " ; qu'enfin, aux termes de l'article R. 424-2 du même code : " Par exception au b) de l'article R. 424-1, le défaut de notification d'une décision expresse dans le délai d'instruction vaut décision implicite de rejet dans les cas suivants : / a) Lorsque les travaux sont soumis (...) à une autorisation au titre des sites classés ou des réserves naturelles " ;<br/>
<br/>
              3. Considérant qu'il résulte des dispositions citées ci-dessus que, lorsqu'un projet ayant fait l'objet d'une déclaration préalable est situé dans un site classé, la décision de non-opposition à cette déclaration ne peut légalement intervenir que sous réserve de l'accord exprès du préfet, après avis de l'architecte des Bâtiments de France ; que nonobstant la circonstance que de tels travaux sont ainsi soumis, en vertu de l'article R. 425-17 du code de l'urbanisme, à une autorisation au titre des sites classés, l'exception prévue par l'article R. 424-2 de ce code et prévoyant la naissance d'une décision implicite de rejet ne leur est, en vertu de son texte même, pas applicable ; qu'ainsi, le silence gardé par l'autorité compétente pour statuer sur cette déclaration préalable au terme du délai d'instruction vaut, conformément aux dispositions de l'article R. 424-1 du même code, décision tacite de non-opposition ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède qu'en jugeant qu'en l'absence de notification, dans le délai d'instruction, d'une décision expresse du maire de Saint-Jean-Cap-Ferrat sur la déclaration préalable de travaux effectuée par la société Changeventure Ltd dans le site classé du Cap-Ferrat, cette société se trouvait titulaire d'une décision de non-opposition, le tribunal administratif de Nice n'a pas commis d'erreur de droit ;<br/>
<br/>
              5. Considérant, en second lieu, qu'il ressort des pièces du dossier soumis aux juges du fond, en particulier des données relatives à l'état du bâtiment existant et des indications figurant sur le formulaire de la déclaration préalable, qu'en estimant que la commune n'établissait pas que la société Changeventure Ltd se serait livrée à des manoeuvres frauduleuses de nature à tromper les services instructeurs sur sa déclaration préalable, le tribunal administratif s'est livré à une appréciation souveraine des faits exempte de dénaturation ;<br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que le pourvoi de la commune de Saint-Jean-Cap-Ferrat doit être rejeté y compris, par voie de conséquence, les conclusions qu'elle présente au titre de l'article L. 761-1 du code de justice administrative ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune, en application des mêmes dispositions, le versement à la société Changeventure Ltd d'une somme de 3 000 euros ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la commune de Saint-Jean-Cap-Ferrat est rejeté.<br/>
Article 2 : La commune de Saint-Jean-Cap-Ferrat versera à la société Changeventure Ltd la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la commune de Saint-Jean-Cap-Ferrat et à la société Changeventure Ltd.<br/>
Copie en sera adressée pour information à la ministre de l'égalité des territoires et du logement.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-08 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. DÉCISIONS IMPLICITES. - PROJET SITUÉ DANS UN SITE CLASSÉ ET AYANT FAIT L'OBJET D'UNE DÉCLARATION PRÉALABLE - 1) CONDITION DE LÉGALITÉ - ACCORD EXPRÈS OBLIGATOIRE DU PRÉFET APRÈS AVIS DE L'ABF - 2) SILENCE GARDÉ PAR L'ADMINISTRATION SUR UNE TELLE DÉCLARATION - DÉCISION IMPLICITE DE REJET - ABSENCE (INAPPLICABILITÉ DE L'ART. R. 424-2 DU CODE DE L'URBANISME) - DÉCISION TACITE DE NON-OPPOSITION - EXISTENCE (APPLICABILITÉ DE L'ART. R. 424-1 DU MÊME CODE).
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">41-02-03 MONUMENTS ET SITES. MONUMENTS NATURELS ET SITES. DISPOSITIONS D'URBANISME. - PROJET SITUÉ DANS UN SITE CLASSÉ ET AYANT FAIT L'OBJET D'UNE DÉCLARATION PRÉALABLE - 1) CONDITION DE LÉGALITÉ - ACCORD EXPRÈS OBLIGATOIRE DU PRÉFET APRÈS AVIS DE L'ABF - 2) SILENCE GARDÉ PAR L'ADMINISTRATION SUR UNE TELLE DÉCLARATION - DÉCISION IMPLICITE DE REJET - ABSENCE (INAPPLICABILITÉ DE L'ART. R. 424-2 DU CODE DE L'URBANISME) - DÉCISION TACITE DE NON-OPPOSITION - EXISTENCE (APPLICABILITÉ DE L'ART. R. 424-1 DU MÊME CODE).
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">68-04-045 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. AUTORISATIONS D`UTILISATION DES SOLS DIVERSES. RÉGIMES DE DÉCLARATION PRÉALABLE. - PROJET SITUÉ DANS UN SITE CLASSÉ ET AYANT FAIT L'OBJET D'UNE DÉCLARATION PRÉALABLE - 1) CONDITION DE LÉGALITÉ - ACCORD EXPRÈS OBLIGATOIRE DU PRÉFET APRÈS AVIS DE L'ABF - 2) SILENCE GARDÉ PAR L'ADMINISTRATION SUR UNE TELLE DÉCLARATION - DÉCISION IMPLICITE DE REJET - ABSENCE (INAPPLICABILITÉ DE L'ART. R. 424-2 DU CODE DE L'URBANISME) - DÉCISION TACITE DE NON-OPPOSITION - EXISTENCE (APPLICABILITÉ DE L'ART. R. 424-1 DU MÊME CODE).
</SCT>
<ANA ID="9A"> 01-01-08 1) Il résulte des dispositions des articles R. 424-1, R. 424-2 et R. 425-17 du code de l'urbanisme que lorsqu'un projet ayant fait l'objet d'une déclaration préalable est situé dans un site classé, la décision de non-opposition à cette déclaration ne peut légalement intervenir que sous réserve de l'accord exprès du préfet, après avis de l'architecte des Bâtiments de France (ABF).... ...2) Nonobstant la circonstance que de tels travaux sont ainsi soumis, en vertu de l'article R. 425-17 du code de l'urbanisme, à une autorisation au titre des sites classés, l'exception prévue par l'article... ...R. 424-2 de ce code et prévoyant la naissance d'une décision implicite de rejet ne leur est, en vertu de son texte même, pas applicable. Ainsi, le silence gardé par l'autorité compétente pour statuer sur cette déclaration préalable au terme du délai d'instruction vaut, conformément aux dispositions de l'article R. 424-1 du même code, décision tacite de non-opposition.</ANA>
<ANA ID="9B"> 41-02-03 1) Il résulte des dispositions des articles R. 424-1, R. 424-2 et R. 425-17 du code de l'urbanisme que lorsqu'un projet ayant fait l'objet d'une déclaration préalable est situé dans un site classé, la décision de non-opposition à cette déclaration ne peut légalement intervenir que sous réserve de l'accord exprès du préfet, après avis de l'architecte des Bâtiments de France (ABF).... ...2) Nonobstant la circonstance que de tels travaux sont ainsi soumis, en vertu de l'article R. 425-17 du code de l'urbanisme, à une autorisation au titre des sites classés, l'exception prévue par l'article... ...R. 424-2 de ce code et prévoyant la naissance d'une décision implicite de rejet ne leur est, en vertu de son texte même, pas applicable. Ainsi, le silence gardé par l'autorité compétente pour statuer sur cette déclaration préalable au terme du délai d'instruction vaut, conformément aux dispositions de l'article R. 424-1 du même code, décision tacite de non-opposition.</ANA>
<ANA ID="9C"> 68-04-045 1) Il résulte des dispositions des articles R. 424-1, R. 424-2 et R. 425-17 du code de l'urbanisme que lorsqu'un projet ayant fait l'objet d'une déclaration préalable est situé dans un site classé, la décision de non-opposition à cette déclaration ne peut légalement intervenir que sous réserve de l'accord exprès du préfet, après avis de l'architecte des Bâtiments de France (ABF).... ...2) Nonobstant la circonstance que de tels travaux sont ainsi soumis, en vertu de l'article R. 425-17 du code de l'urbanisme, à une autorisation au titre des sites classés, l'exception prévue par l'article... ...R. 424-2 de ce code et prévoyant la naissance d'une décision implicite de rejet ne leur est, en vertu de son texte même, pas applicable. Ainsi, le silence gardé par l'autorité compétente pour statuer sur cette déclaration préalable au terme du délai d'instruction vaut, conformément aux dispositions de l'article R. 424-1 du même code, décision tacite de non-opposition.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
