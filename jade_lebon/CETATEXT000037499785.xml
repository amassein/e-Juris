<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037499785</ID>
<ANCIEN_ID>JG_L_2018_10_000000417228</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/49/97/CETATEXT000037499785.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 6ème chambres réunies, 15/10/2018, 417228</TITRE>
<DATE_DEC>2018-10-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417228</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Alain Seban</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:417228.20181015</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée au secrétariat du contentieux du Conseil d'Etat le 11 janvier 2018, la société RTL France Radio demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du Conseil supérieur de l'audiovisuel (CSA) n° 2017-457 du 14 juin 2017 portant mise en demeure de la société CLT-UFA, ensemble la décision implicite rejetant le recours gracieux formé contre cette décision ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule ;<br/>
<br/>
              - la loi n° 86-1067 du 30 septembre 1986 ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alain Seban, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de la société RTL France Radio.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant, d'une part, qu'aux termes de l'article 1er de la loi du 30 septembre 1986 relative à la liberté de communication : " La communication au public par voie électronique est libre. / L'exercice de cette liberté ne peut être limité que dans la mesure requise, d'une part, par le respect de la dignité de la personne humaine, de la liberté et de la propriété d'autrui, du caractère pluraliste de l'expression des courants de pensée et d'opinion et, d'autre part, par la protection de l'enfance et de l'adolescence, par la sauvegarde de l'ordre public, par les besoins de la défense nationale, par les exigences de service public, par les contraintes techniques inhérentes aux moyens de communication, ainsi que par la nécessité, pour les services audiovisuels, de développer la production audiovisuelle (...) " ; qu'aux termes du premier alinéa de l'article 42 de la même loi : " Les éditeurs et distributeurs de services de communication audiovisuelle et les opérateurs de réseaux satellitaires peuvent être mis en demeure de respecter les obligations qui leur sont imposées par les textes législatifs et réglementaires et par les principes définis aux articles 1er et 3-1 " ; qu'aux termes de l'article 42-1, " si la personne faisant l'objet de la mise en demeure ne se conforme pas à celle-ci ", elle peut faire l'objet d'une sanction infligée par le Conseil supérieur de l'audiovisuel (CSA) " compte tenu de la gravité du manquement, et à la condition que celui-ci repose sur des faits distincts ou couvre une période distincte de ceux ayant déjà fait l'objet d'une mise en demeure " ;<br/>
<br/>
              2. Considérant, d'autre part, qu'aux termes du premier alinéa de l'article 28 de la loi du 30 septembre 1986 : " La délivrance des autorisations d'usage de la ressource radioélectrique pour chaque nouveau service diffusé par voie hertzienne terrestre autre que ceux exploités par les sociétés nationales de programme, est subordonnée à la conclusion d'une convention passée entre le Conseil supérieur de l'audiovisuel au nom de l'Etat et la personne qui demande l'autorisation " ; qu'aux termes de l'article 4-2-1 de la convention conclue le 2 octobre 2012 entre le CSA et la société CLT-UFA, relative au service de radio RTL : " Le Conseil supérieur de l'audiovisuel peut mettre en demeure le titulaire de respecter les obligations qui lui sont imposées par la décision d'autorisation ou les stipulations figurant dans la convention et dans les avenants qui pourraient lui être annexés. Il rend publiques ces mises en demeure " ; <br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier que le 2 février 2017, sur l'antenne de la radio RTL, éditée par la société requérante, M. A... a commenté de manière critique l'application faite selon lui par la Cour suprême des Etats-Unis d'Amérique de ce qu'il a appelé le " principe de non discrimination " et dénoncé l'influence de cette jurisprudence sur la Cour européenne des droits de l'homme, le Conseil constitutionnel et le Conseil d'Etat, accusés de perpétrer un " putsch judiciaire " ; que, par la décision attaquée du 14 juin 2017, le CSA a décidé d'adresser à la société requérante, à raison de cette séquence, une mise en demeure de respecter à l'avenir les obligations qui résultent pour elle de l'article 2-4 de la convention mentionnée ci-dessus du 2 octobre 2012, aux termes duquel : " Le titulaire veille dans son programme (...) à promouvoir les valeurs d'intégration et de solidarité qui sont celles de la République. (...) Le titulaire contribue aux actions en faveur de la cohésion sociale et à la lutte contre les discriminations " ;<br/>
<br/>
              4. Considérant que les principes républicains, notamment le principe d 'égalité devant la loi, qui interdit les discriminations et exige que des différences de traitement soient justifiées par des différences de situation objectives et pertinentes ou par l'intérêt général, confèrent une place éminente aux valeurs d'intégration et de solidarité ainsi qu'à l'objectif de cohésion sociale ; que l'engagement prévu à l'article 2-4 précité de la convention relative au service RTL de promouvoir ces valeurs et de contribuer à la lutte contre les discriminations doit se combiner avec le principe de la liberté de communication des pensées et des opinions, consacré et protégé par les dispositions de valeur constitutionnelle de la Déclaration des droits de l'homme et du citoyen de 1789 et rappelé par les dispositions précitées de l'article 1er de la loi du 30 septembre 1986 ; qu'eu égard à ce principe, cet engagement ne saurait être interprété comme imposant à l'éditeur du service de prohiber sur son antenne toute critique des principes et des valeurs républicains ;<br/>
<br/>
              5. Considérant qu'il ressort des pièces du dossier que, pendant la séquence en cause, M. A... a exprimé de manière polémique son point de vue sur la prohibition des discriminations, telle qu'interprétée, selon lui de manière extensive, par les juridictions aux Etats-Unis et en France, auxquelles il a reproché de rendre impossible toute différence de traitement ; que cette prise de parole intervenait dans le cadre d'une émission quotidienne de trois minutes intitulée " On n'est pas forcément d'accord ", à laquelle sont invités des chroniqueurs de différentes opinions et dont le titre même invite les auditeurs à ne la recevoir qu'en tenant compte de son caractère polémique ; que, dans ces conditions, c'est à tort que le CSA a estimé pouvoir relever une méconnaissance des obligations résultant de l'article 2-4 de la convention relative au service RTL et adresser en conséquence à la société requérante une mise en demeure ; que la société RTL France Radio est par suite fondée, sans qu'il soit besoin d'examiner les autres moyens de la requête, à demander l'annulation de la décision qu'elle attaque ainsi que de la décision implicite de rejet du recours gracieux qu'elle a formé contre cette décision ;<br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du CSA la somme de 3 000 euros que demande la société RTL France Radio au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La décision du CSA n° 2017-457 du 14 juin 2017 portant mise en demeure de la société CLT UFA et la décision implicite de rejet du recours gracieux formé contre cette décision par la société RTL France Radio sont annulées.<br/>
<br/>
Article 2 : Le CSA versera à la société RTL France Radio la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la société RTL France Radio et au Conseil supérieur de l'audiovisuel.<br/>
		Copie en sera adressée à la ministre de la culture.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-03-06 DROITS CIVILS ET INDIVIDUELS. LIBERTÉS PUBLIQUES ET LIBERTÉS DE LA PERSONNE. LIBERTÉ D'EXPRESSION. - CONVENTION CONCLUE ENTRE L'ETAT ET L'ÉDITEUR D'UN SERVICE AUDIOVISUEL (1ER AL. DE L'ART. 28 DE LA LOI N° 86-1067 DU 30 SEPTEMBRE 1986) - ENGAGEMENT DE VEILLER À LA PROMOTION DE PRINCIPES ET VALEURS RÉPUBLICAINS - PORTÉE - PROHIBITION DE TOUTE CRITIQUE, SUR L'ANTENNE, DE CES PRINCIPES ET VALEURS - ABSENCE, EU ÉGARD AU PRINCIPE DE LA LIBERTÉ DE COMMUNICATION (DDHC ET ART. 1ER DE CETTE LOI).
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">56-04 RADIO ET TÉLÉVISION. SERVICES PRIVÉS DE RADIO ET DE TÉLÉVISION. - CONVENTION CONCLUE ENTRE L'ETAT ET L'ÉDITEUR (1ER AL. DE L'ART. 28 DE LA LOI N° 86-1067 DU 30 SEPTEMBRE 1986) - ENGAGEMENT DE VEILLER À LA PROMOTION DE PRINCIPES ET VALEURS RÉPUBLICAINS - PORTÉE - PROHIBITION DE TOUTE CRITIQUE, SUR L'ANTENNE, DE CES PRINCIPES ET VALEURS - ABSENCE, EU ÉGARD AU PRINCIPE DE LA LIBERTÉ DE COMMUNICATION (DDHC ET ART. 1ER DE CETTE LOI).
</SCT>
<ANA ID="9A"> 26-03-06 Eu égard au principe de la liberté de communication des pensées et des opinions, consacré et protégé par les dispositions de valeur constitutionnelle de la Déclaration des droits de l'homme et du citoyen de 1789 (DDHC) et rappelé par l'article 1er de la loi n° 86-1067 du 30 septembre 1986, un engagement de veille(r) (&#133;) à promouvoir les valeurs d'intégration et de solidarité qui sont celles de la République et de contribue(r) aux actions en faveur de la cohésion sociale et à la lutte contre les discriminations, prévu par une convention conclue au titre du premier alinéa de l'article 28 de cette loi, ne saurait être interprété comme imposant à l'éditeur du service de prohiber sur son antenne toute critique des principes et des valeurs républicains.</ANA>
<ANA ID="9B"> 56-04 Eu égard au principe de la liberté de communication des pensées et des opinions, consacré et protégé par les dispositions de valeur constitutionnelle de la Déclaration des droits de l'homme et du citoyen de 1789 (DDHC) et rappelé par l'article 1er de la loi n° 86-1067 du 30 septembre 1986, un engagement de veille(r) (&#133;) à promouvoir les valeurs d'intégration et de solidarité qui sont celles de la République et de contribue(r) aux actions en faveur de la cohésion sociale et à la lutte contre les discriminations, prévu par une convention conclue au titre du premier alinéa de l'article 28 de cette loi, ne saurait être interprété comme imposant à l'éditeur du service de prohiber sur son antenne toute critique des principes et des valeurs républicains.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
