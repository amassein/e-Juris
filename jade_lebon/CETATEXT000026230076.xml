<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026230076</ID>
<ANCIEN_ID>JG_L_2012_07_000000316155</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/23/00/CETATEXT000026230076.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 27/07/2012, 316155</TITRE>
<DATE_DEC>2012-07-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>316155</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ODENT, POULET ; SCP THOUIN-PALAT, BOUCARD</AVOCATS>
<RAPPORTEUR>M. Thierry Carriol</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:316155.20120727</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 13 mai et 12 août 2008 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme A...B..., demeurant...; Mme B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement du 6 mars 2008 par lequel le tribunal administratif de Rouen a rejeté sa demande tendant à l'annulation de la décision du 2 juin 2006 du maire de la commune de Petit Couronne déclarant nulle et non avenue sa déclaration de travaux ;<br/>
<br/>
              2°) de mettre à la charge de la commune de Petit Couronne la  somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thierry Carriol, Maître des Requêtes en service extraordinaire,<br/>
<br/>
              - les observations de la SCP Odent, Poulet, avocat de la commune de Petit Couronne et de la SCP Thouin-Palat, Boucard, avocat de MmeB...,<br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Odent, Poulet, avocat de la commune de Petit Couronne et à la SCP Thouin-Palat, Boucard, avocat de MmeB...  ;<br/>
<br/>
<br/>
<br/>
<br/>1.	Considérant que par courrier en date du 2 juin 2006, le maire de la commune de Petit Couronne a fait connaître à Mme B...qu'il considérait la déclaration de travaux relative à la modification de l'aspect extérieur d'une construction qu'elle avait déposée comme étant " nulle et non avenue " au motif que celle-ci, comme il lui avait été indiqué par un courrier du 24 février 2006, avait été édifiée sans autorisation ; que Mme B...se pourvoit en cassation contre le jugement du 6 mars 2008 par lequel le tribunal administratif de Rouen a rejeté ses conclusions tendant à l'annulation de cette décision ;<br/>
<br/>
              Sur la régularité du jugement attaqué :<br/>
<br/>
              2.	Considérant qu'aux termes de l'article R. 611-1 du code de justice administrative : " (...) La requête, le mémoire complémentaire annoncé dans la requête et le premier mémoire de chaque défendeur sont communiqués aux parties avec les pièces jointes (...). " ;<br/>
<br/>
              3.	Considérant que la clôture de l'instruction, ordonnée par décision du président du tribunal administratif de Rouen du 5 décembre 2007, a été fixée non au 15 janvier 2007 comme indiqué du fait d'une erreur de plume mais au 15 janvier 2008 ; que la commune de Petit Couronne a produit un mémoire en défense enregistré au greffe du tribunal administratif le 17 décembre 2007 ; que cette circonstance ne nécessitait donc pas, contrairement à ce que soutient la requérante, que fût ouverte de nouveau l'instruction ; qu'il ressort, par ailleurs, des pièces relatives à cette instruction que ce mémoire a été communiqué à Mme B...le 21 décembre 2007 ; que, par suite, Mme B...n'est pas fondée à soutenir que le tribunal administratif aurait méconnu le caractère contradictoire de l'instruction ;<br/>
<br/>
              4.	Considérant qu'en jugeant que, dès lors que le maire de la commune de Petit Couronne était tenu de s'opposer à la déclaration de travaux déposée par Mme B..., les moyens invoqués par cette dernière étaient, par suite, inopérants, le tribunal administratif de Rouen n'a pas soulevé d'office un moyen mais a simplement exercé son office et répondu aux moyens qui étaient soulevés devant lui par un jugement qui est, par ailleurs, suffisamment motivé ; que, dès lors, le tribunal administratif de Rouen n'a pas entaché son arrêt d'irrégularité ;<br/>
<br/>
              Sur le bien-fondé du jugement attaqué :<br/>
<br/>
              5.	Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le maire de la commune de Petit Couronne a adressé, le 2 juin 2006, à Mme B...un courrier par lequel il retournait la déclaration de travaux que cette dernière avait déposée pour une parcelle située 36, rue du 11 novembre prolongée en la déclarant " nulle et non avenue " ; que ce courrier faisait explicitement référence à une précédente lettre du maire en date du 24 février 2006 qui déclarait irrecevable une précédente demande concernant la même parcelle et en indiquait les raisons ; que, dès lors, et compte tenu par ailleurs des termes mêmes de ces courriers, dépourvus d'ambiguïté, le tribunal administratif de Rouen n'a pas dénaturé les pièces du dossier en regardant le courrier du 2 juin 2006 contesté comme une décision d'opposition à travaux ;<br/>
<br/>
              6.	Considérant qu'aux termes de l'article L. 421-1 du code de l'urbanisme, dans sa rédaction applicable à la décision contestée : " Quiconque désire entreprendre ou implanter une construction à usage d'habitation ou non, même ne comportant pas de fondations, doit, au préalable, obtenir un permis de construire (...) " ; que ces prescriptions s'appliquent également dans l'hypothèse où l'autorité administrative est saisie d'une demande tendant à ce que soient autorisés des travaux portant sur un immeuble qui a été édifié sans autorisation, la demande devant alors porter sur l'ensemble du bâtiment ;<br/>
<br/>
              7.	Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la construction à usage d'habitation située au 36, rue du 11 novembre prolongée, sur un terrain où de précédentes constructions avaient été entièrement démolies, a été réalisée par Mme B...sans qu'ait été demandée l'autorisation de construire prévue à l'article L. 421-1 du code de l'urbanisme et nécessaire en l'espèce eu égard à la superficie de la construction ; que Mme B...a déposé, le 31 mai 2006, une déclaration de travaux portant exclusivement sur la modification de l'aspect extérieur de cette construction alors qu'elle était tenue de déposer une demande portant sur l'ensemble du bâtiment ; que, dès lors, en jugeant, compte tenu des conditions dans lesquelles avait été édifié le bâtiment sur lequel des travaux étaient envisagés, que ceux-ci rendaient nécessaires le dépôt d'un permis de construire préalable portant sur l'ensemble du bâtiment, le tribunal administratif n'a ni commis d'erreur de droit, ni dénaturé les pièces du dossier ;<br/>
<br/>
              8.	Considérant ainsi qu'il vient d'être dit que, dès lors qu'une demande porte sur des travaux qui concernent un bâtiment édifié sans autorisation, cette demande doit porter sur l'ensemble du bâtiment ; que, par suite, en jugeant que le maire de la commune de Petit Couronne était tenu de s'opposer à la déclaration de travaux déposée par Mme B..., le tribunal administratif n'a pas commis d'erreur de droit ;<br/>
<br/>
              9.	Considérant qu'il résulte de tout ce qui précède que Mme B...n'est pas fondée à demander  l'annulation du jugement du 6 mars 2008 du tribunal administratif de Rouen ;<br/>
<br/>
              Sur les conclusions de Mme B...présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              10.	Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de Petit Couronne qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de Mme B...la somme de 3 000 euros à verser à la commune de Petit Couronne au titre de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Le pourvoi de Mme B...est rejeté.<br/>
Article 2 : Mme B...versera à la commune de Petit Couronne une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à Mme A...B...et à la commune de Petit Couronne.<br/>
Copie en sera adressée pour information à la ministre de l'écologie, du développement durable et de l'énergie.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-05-01-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - MOTIFS. POUVOIRS ET OBLIGATIONS DE L'ADMINISTRATION. COMPÉTENCE LIÉE. - DÉCLARATION DE TRAVAUX CONCERNANT UNE CONSTRUCTION ÉDIFIÉE SANS AUTORISATION - COMPÉTENCE LIÉE DU MAIRE POUR S'OPPOSER - EXISTENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-04-045-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. AUTORISATIONS D`UTILISATION DES SOLS DIVERSES. RÉGIMES DE DÉCLARATION PRÉALABLE. DÉCLARATION DE TRAVAUX EXEMPTÉS DE PERMIS DE CONSTRUIRE. - DÉCLARATION DE TRAVAUX CONCERNANT UNE CONSTRUCTION ÉDIFIÉE SANS AUTORISATION - COMPÉTENCE LIÉE DU MAIRE POUR S'OPPOSER - EXISTENCE [RJ1].
</SCT>
<ANA ID="9A"> 01-05-01-03 Lorsqu'une demande porte sur des travaux qui concernent un bâtiment ayant été édifié sans l'autorisation prévue par les dispositions du code de l'urbanisme, cette demande doit porter sur l'ensemble du bâtiment. Le maire a donc compétence liée pour s'opposer à une déclaration de travaux concernant ces seuls travaux.</ANA>
<ANA ID="9B"> 68-04-045-02 Lorsqu'une demande porte sur des travaux qui concernent un bâtiment ayant été édifié sans l'autorisation prévue par les dispositions du code de l'urbanisme, cette demande doit porter sur l'ensemble du bâtiment. Le maire a donc compétence liée pour s'opposer à une déclaration de travaux concernant ces seuls travaux.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 3 mai 2011, Mme Ely, n° 320545, à mentionner aux Tables sur un autre point.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
