<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037158690</ID>
<ANCIEN_ID>JG_L_2018_07_000000396985</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/15/86/CETATEXT000037158690.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 04/07/2018, 396985</TITRE>
<DATE_DEC>2018-07-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>396985</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>Mme Céline  Guibé</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Yohann Bénard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:396985.20180704</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société civile immobilière JM6 a demandé au tribunal administratif de Strasbourg d'annuler les arrêtés du 4 juillet 2014 par lesquels le président de la communauté de communes " Rives de Moselle " a précisé le mode d'évaluation des sommes de 72 344,86 euros et 284 510 euros mises à sa charge au titre de la participation spécifique pour réalisation d'équipements publics exceptionnels par arrêtés portant permis de construire des 7 octobre 2004 et 19 décembre 2006, ensemble les décisions du 3 septembre 2014 par lesquelles il a rejeté ses recours gracieux. Par un jugement nos 1405199 et 1405200 du 17 mars 2015, le tribunal administratif de Strasbourg a fait droit à sa demande.  <br/>
<br/>
              Par un arrêt nos 15NC00683, 15NC00684 du 17 décembre 2015, la cour administrative d'appel de Nancy a, sur appel de la communauté de communes " Rives de Moselle ", annulé ce jugement et rejeté la demande présentée par la société JM6. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 15 février et 13 mai 2016 au secrétariat du contentieux du Conseil d'Etat, la société JM6 demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de la communauté de communes " Rives de Moselle " la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Céline Guibé, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Yohann Bénard, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de la société JM6  et à la SCP Lyon-Caen, Thiriez, avocat de la société Communauté de Communes Rives de Moselle.<br/>
<br/>
<br/>
              Vu la note en délibéré, enregistrée le 18 juin 2018, présentée par la société civile immobilière JM6 ; <br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que le syndicat intercommunal pour la création et l'aménagement de la zone d'activités de Talange-Hauconcourt (Moselle) a, par deux délibérations des 29 juin 2001 et 1er avril 2005, fixé les règles de calcul de la participation spécifique pour la réalisation d'équipements exceptionnels pouvant être exigée des bénéficiaires d'autorisations de construire sur cette zone en application des dispositions de l'article L. 332-8 du code de l'urbanisme. Par un arrêté du 7 octobre 2004, le maire d'Hauconcourt a délivré à la société par actions simplifiée Weigerding un permis de construire des locaux commerciaux dans cette zone et mis à sa charge une participation financière d'un montant de 72 344, 86 euros. Par un arrêté du 19 décembre 2006, le préfet de la Moselle a accordé à la société Weigerding un second permis de construire pour la création et l'extension de lots dans la zone d'activités et mis à sa charge une participation financière de 284 510 euros. Par un jugement du 1er octobre 2013, qui n'a pas été frappé d'appel, le tribunal administratif de Strasbourg a accordé à la société JM6, venue aux droits de la société Weigerding, la décharge de ces participations. Estimant que ces participations restaient exigibles, le président de la communauté de communes " Rives de la Moselle " en a précisé le mode d'évaluation par des arrêtés du 4 juillet 2014. Par un jugement du 17 mars 2015, le tribunal administratif de Strasbourg a, sur demande de la société JM6, annulé ces arrêtés au motif que les participations étaient réputées sans cause. La société JM6 se pourvoit en cassation contre l'arrêt du 17 décembre 2015 par lequel la cour administrative d'appel de Nancy a annulé ce jugement et rejeté sa demande tendant à l'annulation des arrêtés du 4 juillet 2014.<br/>
<br/>
              Sur la régularité de l'arrêt attaqué :<br/>
<br/>
              2. Aux termes de l'article L. 332-8 du code de l'urbanisme, dans sa rédaction applicable : " Une participation spécifique peut être exigée des bénéficiaires des autorisations de construire qui ont pour objet la réalisation de toute installation à caractère industriel, agricole, commercial ou artisanal qui, par sa nature, sa situation ou son importance, nécessite la réalisation d'équipements publics exceptionnels. / Lorsque la réalisation des équipements publics exceptionnels n'est pas de la compétence de l'autorité qui délivre le permis de construire, celle-ci détermine le montant de la contribution correspondante, après accord de la collectivité publique à laquelle incombent ces équipements ou de son concessionnaire. ". Aux termes de l'article L. 332-7 du même code : "  L'illégalité des prescriptions exigeant des taxes ou des contributions aux dépenses d'équipements publics est sans effet sur la légalité des autres dispositions de l'autorisation de construire. / Lorsque l'une de ces prescriptions est annulée pour illégalité, l'autorité qui a délivré l'autorisation prend, compte tenu de la décision juridictionnelle devenue définitive, un nouvel arrêté portant la prescription d'une taxe ou d'une contribution aux dépenses d'équipements publics. "<br/>
<br/>
              3. Il ressort des termes de l'arrêt attaqué que, si la cour administrative d'appel de Nancy a informé les parties, en application de l'article L 611-7 du code de justice administrative, que sa décision était susceptible d'être fondée sur une substitution de base légale, " les arrêtés litigieux étant fondés à tort sur l'article L 332-7 du code de l'urbanisme ", elle n'a en définitive procédé à aucune substitution de base légale pour juger que les arrêtés litigieux ne méconnaissaient pas l'article L. 332-8 du code de l'urbanisme, en application duquel ils avaient été pris. Par suite, la société requérante ne peut utilement soutenir qu'elle n'a pas disposé d'un temps suffisant pour répondre à l'information qui lui a été communiquée en application de l'article L 611-7 du code de justice administrative  ni  que l'arrêt attaqué serait insuffisamment motivé faute de se prononcer sur le respect des conditions dans lesquelles il peut être procédé à une substitution de base légale. <br/>
<br/>
              Sur le bien-fondé de l'arrêt attaqué :<br/>
<br/>
              4. En premier lieu, aux termes de l'article L. 332-6 du code de l'urbanisme : " Les bénéficiaires d'autorisations de construire ne peuvent être tenus que des obligations suivantes : (...) 2° Le versement des contributions aux dépenses d'équipements publics mentionnées à l'article L. 332-6-1 ". Aux termes de l'article L. 332-6-1 du même code : " Les contributions aux dépenses d'équipements publics prévus au 2° de l'article L. 332-6 sont les suivantes : (...) c) La participation spécifique pour la réalisation d'équipements publics exceptionnels prévue à l'article L. 332-8 (...) ". Aux termes de l'article L. 332-30 de ce code  : " Les taxes et contributions de toute nature qui sont obtenues ou imposées en violation des dispositions des articles L. 311-4 et L. 332-6 sont réputées sans cause ". Aux termes de l'article R. 421-29 du code : " Le permis de construire énumère celles des contributions prévues au 2° de l'article L. 332-6-1 ou à l'article L. 332-9 qu'il met, le cas échéant, à la charge du bénéficiaire du permis de construire. Il fixe le montant de chacune de ces contributions et en énonce le mode d'évaluation ". Par son jugement du 1er octobre 2013, le tribunal administratif de Strasbourg a accordé la décharge des participations mises à la charge de la société JM6 au motif que les arrêtés des 7 octobre 2004 et 9 décembre 2006 ne comportaient pas l'indication de leur mode d'évaluation, en méconnaissance de l'article R. 421-9 du code de l'urbanisme. En jugeant qu'un tel motif d'illégalité, qui n'est pas visé par l'article L. 332-30 du code de l'urbanisme, ne permettait pas de réputer sans cause les participations mises à la charge de la société JM6, la cour administrative d'appel de Nancy n'a pas commis d'erreur de droit. <br/>
<br/>
              5. En deuxième lieu, lorsque le juge a prononcé la décharge d'une contribution prévue à l'article L. 332-6-1 du code de l'urbanisme au motif que les dispositions de l'autorisation d'urbanisme portant prescription de cette contribution étaient entachées d'une irrégularité et que cette irrégularité ne conduit pas à réputer la contribution sans cause, l'autorité compétente peut de nouveau mettre cette contribution à la charge du bénéficiaire de l'autorisation d'urbanisme par une prescription financière légalement prise. Tel est notamment le cas lorsque le permis de construire n'énonce pas le mode d'évaluation des contributions mises à la charge de son bénéficiaire, en méconnaissance des dispositions de l'article R. 421-29 du code de l'urbanisme. Par suite, en jugeant que le président de la communauté de communes " Rives de Moselle " pouvait,  compte tenu de la nature de l'illégalité entachant les arrêtés des 7 octobre 2004 et 9 décembre 2006, prendre les arrêtés du 4 juillet 2014, qui précisaient le mode d'évaluation des participations financières sans en modifier le montant, la cour administrative d'appel de Nancy, qui n'a pas inexactement qualifié les faits dont elle était saisie, n'a pas commis d'erreur de droit ni méconnu l'autorité de la chose jugée par le tribunal administratif de Strasbourg dans son jugement du 1er octobre 2013.<br/>
<br/>
              6. En troisième lieu, ni l'achèvement, ni le démarrage des travaux ne constituent une condition de la prescription, par l'autorisation de construire, de la contribution prévue par l'article L. 332-8 du code de l'urbanisme. L'absence de réalisation des travaux dans les délais prévus ne permet pas de réputer sans cause une telle contribution. Par suite, en jugeant que la circonstance que les travaux n'avaient pas été réalisés dans le délai de cinq ans prévu par les conventions signées par la société Weigerding avec le syndicat intercommunal pour la création et l'aménagement de la zone d'activités de Talange-Hauconcourt était sans incidence sur la légalité des arrêtés attaqués, la cour n'a pas méconnu les dispositions de l'article L. 332-30 du code de l'urbanisme. Elle n'a pas davantage méconnu les dispositions des articles L. 332-9 et L. 332-11 du code de l'urbanisme, qui ne sont pas applicables aux contributions prévues par l'article L. 332-8 du même code.<br/>
<br/>
              7. En dernier lieu, l'article R. 421-9 du code de l'urbanisme impose seulement à l'autorité compétente d'énoncer dans le permis de construire le mode d'évaluation des contributions mises à la charge de son bénéficiaire. Par suite, en jugeant sans incidence sur la légalité des arrêtés contestés l'absence de communication de la délibération du 29 juin 2001, par laquelle le syndicat intercommunal pour la création et l'aménagement de la zone d'activités de Talange-Hauconcourt avait fixé le mode de calcul de la participation spécifique pour la réalisation d'équipements exceptionnels pouvant être exigée des bénéficiaires d'autorisations de construire sur cette zone, la cour n'a pas commis d'erreur de droit.<br/>
<br/>
              8. Il résulte de tout ce qui précède que la société JM6 n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque. Ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, par voie de conséquence, qu'être rejetées. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la communauté de communes " Rives de Moselle " au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société JM6 est rejeté.<br/>
Article 2 : Les conclusions de la communauté de communes " Rives de Moselle " présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à la société civile immobilière JM6 et à la communauté de communes " Rives de Moselle ".<br/>
Copie en sera adressée au ministre d'Etat, ministre de la transition écologique et solidaire. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-024 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. CONTRIBUTIONS DES CONSTRUCTEURS AUX DÉPENSES D'ÉQUIPEMENT PUBLIC. - CONTRIBUTIONS AUX DÉPENSES D'ÉQUIPEMENTS PUBLICS (ART. L. 332-6-1 DU CODE DE L'URBANISME) - DÉCHARGE PAR LE JUGE POUR CAUSE D'IRRÉGULARITÉ NE CONDUISANT PAS À REGARDER LA CONTRIBUTION MISE À LA CHARGE DU BÉNÉFICIAIRE DE L'AUTORISATION D'URBANISME SANS CAUSE - POSSIBILITÉ POUR L'AUTORITÉ COMPÉTENTE DE REMETTRE CETTE CONTRIBUTION À LA CHARGE DE CE DERNIER PAR UNE PRESCRIPTION FINANCIÈRE LÉGALEMENT PRISE - EXISTENCE - CAS DANS LEQUEL LE PERMIS DE CONSTRUIRE N'ÉNONCE PAS LE MODE D'ÉVALUATION DE LA CONTRIBUTION - EXISTENCE.
</SCT>
<ANA ID="9A"> 68-024 Lorsque le juge a prononcé la décharge d'une contribution prévue à l'article L. 332-6-1 du code de l'urbanisme au motif que les dispositions de l'autorisation d'urbanisme portant prescription de cette contribution étaient entachées d'une irrégularité et que cette irrégularité ne conduit pas à réputer la contribution sans cause, l'autorité compétente peut de nouveau mettre cette contribution à la charge du bénéficiaire de l'autorisation d'urbanisme par une prescription financière légalement prise. Tel est notamment le cas lorsque le permis de construire n'énonce pas le mode d'évaluation des contributions mises à la charge de son bénéficiaire, en méconnaissance des dispositions du code de l'urbanisme.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
