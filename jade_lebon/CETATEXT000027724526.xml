<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027724526</ID>
<ANCIEN_ID>JG_L_2013_07_000000359417</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/72/45/CETATEXT000027724526.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 17/07/2013, 359417, Publié au recueil Lebon</TITRE>
<DATE_DEC>2013-07-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>359417</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON</AVOCATS>
<RAPPORTEUR>M. Michel Bart</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:359417.20130717</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 15 mai et 16 août 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B...A..., demeurant... ; M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 11PA02364 du 15 mars 2012 par lequel la cour administrative d'appel de Paris a rejeté sa requête tendant, d'une part, à l'annulation du jugement n° 0905812/7-2 du 31 mars 2011 par lequel le tribunal administratif de Paris a rejeté sa demande tendant à l'annulation de la décision du 3 décembre 2008 par laquelle le procureur près le tribunal de grande instance de Paris a rejeté sa demande tendant à ce que soient effacées les mentions le concernant qui figurent dans le "système de traitement des infractions constatées" et du courrier du ministre de l'intérieur, de l'outre-mer et des collectivités territoriales du 10 février 2009 confirmant la décision du procureur et, d'autre part, à ce qu'il soit enjoint au procureur de la République près le tribunal de grande instance de Paris de procéder à l'effacement de ces mentions;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 2003-239 du 18 mars 2003 ;<br/>
<br/>
              Vu le décret n° 2001-583 du 5 juillet 2001 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Michel Bart, Conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boré, Salve de Bruneton, avocat de M. A...;<br/>
<br/>
<br/>
<br/>1. Considérant que, pour confirmer le jugement en date du 31 mars 2011 par lequel le tribunal administratif de Paris a rejeté la demande de M. A...tendant à l'annulation de la décision en date du 3 décembre 2008 du procureur près le tribunal de grande instance de Paris refusant d'ordonner l'effacement des mentions le concernant dans le "système de traitement des infractions constatées" (STIC), ainsi que du courrier du ministre de l'intérieur, de l'outre mer et des collectivités territoriales du 10 février 2009 confirmant la décision du procureur, la cour administrative d'appel de Paris a jugé que les décisions du procureur de la République relatives à l'effacement de mentions figurant dans ce fichier constituent des mesures d'administration judiciaire et qu'en conséquence la demande de M. A... était portée devant un ordre de juridiction incompétent pour en connaître ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 21 de la loi du 18 mars 2003 pour la sécurité intérieure, alors en vigueur : " I. Les services de la police nationale et de la gendarmerie nationale peuvent mettre en oeuvre des applications automatisées d'informations nominatives recueillies au cours des enquêtes préliminaires ou de flagrance ou des investigations exécutées sur commission rogatoire et concernant tout crime ou délit ainsi que les contraventions de la cinquième classe sanctionnant un trouble à la sécurité ou à la tranquillité publiques ou une atteinte aux personnes, aux biens ou à l'autorité de l'Etat, afin de faciliter la constatation des infractions à la loi pénale, le rassemblement des preuves de ces infractions et la recherche de leurs auteurs (...) / III. - Le traitement des informations nominatives est opéré sous le contrôle du procureur de la République compétent qui peut demander qu'elles soient effacées, complétées ou rectifiées, notamment en cas de requalification judiciaire. La rectification pour requalification judiciaire est de droit lorsque la personne concernée la demande. En cas de décision de relaxe ou d'acquittement devenue définitive, les données personnelles concernant les personnes mises en cause sont effacées sauf si le procureur de la République en prescrit le maintien pour des raisons liées à la finalité du fichier, auquel cas elle fait l'objet d'une mention. Les décisions de non-lieu et, lorsqu'elles sont motivées par une insuffisance de charges, de classement sans suite font l'objet d'une mention sauf si le procureur de la République ordonne l'effacement des données personnelles " ; que l'article 25 de la même loi, qui rétablit l'article 17-1 de la loi du 21 janvier 1995 d'orientation et de programmation relative à la sécurité, définit les conditions et finalités de la consultation, dans le cadre d'enquêtes administratives préalables aux décisions administratives qu'il mentionne, des données nominatives recueillies dans ces fichiers ; que l'article 1er du décret du 5 juillet 2001 pris pour l'application des dispositions du troisième alinéa de l'article 31 de la loi n°78-17 relative à l'informatique, aux fichiers et aux libertés et portant création du système de traitement des infractions constatées, dans sa rédaction alors en vigueur, autorise le ministère de l'intérieur (direction générale de la police nationale) à " mettre en oeuvre une application automatisée d'informations nominatives dénommée " système de traitement des infractions constatées " (STIC) "  ; qu'en application de l'article 2 de ce décret, les données à caractère personnel relatives aux personnes mises en cause ainsi que la qualification des faits, telles qu'elles sont enregistrées dans le STIC, sont transmises au procureur de la République territorialement compétent en même temps que la procédure ; qu'aux termes de l'article 3 de ce décret, " le traitement des données à caractère personnel est opéré sous le contrôle du procureur de la République territorialement compétent conformément aux dispositions du III de l'article 21 de la loi n° 2003-239 du 18 mars 2003 pour la sécurité intérieure " ;<br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions que, si les données nominatives figurant dans le " système de traitement des infractions constatées " portent sur des informations recueillies au cours d'enquêtes préliminaires ou de flagrance ou d'investigations exécutées sur commission rogatoire et concernant tout crime ou délit ainsi que certaines contraventions de cinquième classe, les décisions en matière d'effacement ou de rectification, qui ont pour objet la tenue à jour de ce fichier et sont détachables d'une procédure judiciaire, constituent des actes de gestion administrative du fichier et peuvent faire l'objet d'un recours pour excès de pouvoir devant le juge administratif ; qu'en jugeant que les décisions du procureur de la République relatives à l'effacement des mentions figurant dans le " système de traitement des infractions constatées "  constituent des mesures d'administration judiciaire, la cour administrative d'appel de Paris a commis une erreur de droit ; que, dès lors et sans qu'il soit besoin  d'examiner les autres moyens du pourvoi, son arrêt doit être annulé ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 1500 euros à verser à M.A..., au titre  des dispositions de l'article L 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 15 mars 2012 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br/>
Article 3 : L'Etat versera une somme de 1500 euros à M. A...au titre de l'article L 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. B...A..., au ministre de l'intérieur et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-05-01-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. ACTES ADMINISTRATIFS - NOTION. ACTES À CARACTÈRE ADMINISTRATIF. ACTES PRÉSENTANT CE CARACTÈRE. - FICHIER INTITULÉ  SYSTÈME DE TRAITEMENT DES INFRACTIONS CONSTATÉES  - DÉCISIONS RELATIVES À L'EFFACEMENT DE MENTIONS FIGURANT DANS CE FICHIER.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">17-03-02-005-01 COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE LES DEUX ORDRES DE JURIDICTION. COMPÉTENCE DÉTERMINÉE PAR UN CRITÈRE JURISPRUDENTIEL. ACTES. ACTES ADMINISTRATIFS. - FICHIER INTITULÉ  SYSTÈME DE TRAITEMENT DES INFRACTIONS CONSTATÉES  - DÉCISIONS RELATIVES À L'EFFACEMENT DE MENTIONS FIGURANT DANS CE FICHIER - MESURES D'ADMINISTRATION JUDICIAIRE - ABSENCE - ACTES DE GESTION ADMINISTRATIVE DU FICHIER - EXISTENCE - ACTES SUSCEPTIBLES D'UN RECOURS POUR EXCÈS DE POUVOIR DEVANT LE JUGE ADMINISTRATIF - EXISTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">26-07-05 DROITS CIVILS ET INDIVIDUELS. - FICHIER INTITULÉ  SYSTÈME DE TRAITEMENT DES INFRACTIONS CONSTATÉES  - DÉCISIONS RELATIVES À L'EFFACEMENT DE MENTIONS FIGURANT DANS CE FICHIER - MESURES D'ADMINISTRATION JUDICIAIRE - ABSENCE - ACTES DE GESTION ADMINISTRATIVE DU FICHIER - EXISTENCE - ACTES SUSCEPTIBLES D'UN RECOURS POUR EXCÈS DE POUVOIR DEVANT LE JUGE ADMINISTRATIF - EXISTENCE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">26-07-06 DROITS CIVILS ET INDIVIDUELS. - FICHIER INTITULÉ  SYSTÈME DE TRAITEMENT DES INFRACTIONS CONSTATÉES  - DÉCISIONS RELATIVES À L'EFFACEMENT DE MENTIONS FIGURANT DANS CE FICHIER - MESURES D'ADMINISTRATION JUDICIAIRE - ABSENCE - ACTES DE GESTION ADMINISTRATIVE DU FICHIER - EXISTENCE - ACTES SUSCEPTIBLES D'UN RECOURS POUR EXCÈS DE POUVOIR DEVANT LE JUGE ADMINISTRATIF - EXISTENCE.
</SCT>
<SCT ID="8E" TYPE="PRINCIPAL">37-02-02 JURIDICTIONS ADMINISTRATIVES ET JUDICIAIRES. SERVICE PUBLIC DE LA JUSTICE. FONCTIONNEMENT. - FICHIER INTITULÉ  SYSTÈME DE TRAITEMENT DES INFRACTIONS CONSTATÉES  - DÉCISIONS RELATIVES À L'EFFACEMENT DE MENTIONS FIGURANT DANS CE FICHIER - MESURES D'ADMINISTRATION JUDICIAIRE - ABSENCE - ACTES DE GESTION ADMINISTRATIVE DU FICHIER - EXISTENCE - ACTES SUSCEPTIBLES D'UN RECOURS POUR EXCÈS DE POUVOIR DEVANT LE JUGE ADMINISTRATIF - EXISTENCE.
</SCT>
<SCT ID="8F" TYPE="PRINCIPAL">54-01-01-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉCISIONS POUVANT OU NON FAIRE L'OBJET D'UN RECOURS. ACTES CONSTITUANT DES DÉCISIONS SUSCEPTIBLES DE RECOURS. - FICHIER INTITULÉ  SYSTÈME DE TRAITEMENT DES INFRACTIONS CONSTATÉES  - DÉCISIONS RELATIVES À L'EFFACEMENT DE MENTIONS FIGURANT DANS CE FICHIER.
</SCT>
<ANA ID="9A"> 01-01-05-01-01 Il résulte des dispositions de l'article 21 de la loi n° 2003-239 du 18 mars 2003 pour la sécurité intérieure, de l'article 25 de la même loi, qui modifie l'article 17-1 de la loi n° 95-73 du 21 janvier 1995 d'orientation et de programmation relative à la sécurité, ainsi que des articles 1er, 2 et 3 du décret n° 2001-583 du 5 juillet 2001 pris pour l'application des dispositions du troisième alinéa de l'article 31 de la loi n° 78-17 relative à l'informatique, aux fichiers et aux libertés et portant création du système de traitement des infractions constatées (STIC), que si les données nominatives figurant dans le  système de traitement des infractions constatées  portent sur des informations recueillies au cours d'enquêtes préliminaires ou de flagrance ou d'investigations exécutées sur commission rogatoire et concernant tout crime ou délit ainsi que certaines contraventions de cinquième classe, les décisions en matière d'effacement ou de rectification, qui ont pour objet la tenue à jour de ce fichier et sont  détachables d'une procédure judiciaire, constituent non pas des mesures d'administration judiciaire, mais des actes de gestion administrative du fichier et peuvent faire l'objet d'un recours pour excès de pouvoir devant le juge administratif.</ANA>
<ANA ID="9B"> 17-03-02-005-01 Il résulte des dispositions de l'article 21 de la loi n° 2003-239 du 18 mars 2003 pour la sécurité intérieure, de l'article 25 de la même loi, qui modifie l'article 17-1 de la loi n° 95-73 du 21 janvier 1995 d'orientation et de programmation relative à la sécurité, ainsi que des articles 1er, 2 et 3 du décret n° 2001-583 du 5 juillet 2001 pris pour l'application des dispositions du troisième alinéa de l'article 31 de la loi n° 78-17 relative à l'informatique, aux fichiers et aux libertés et portant création du système de traitement des infractions constatées (STIC), que si les données nominatives figurant dans le  système de traitement des infractions constatées  portent sur des informations recueillies au cours d'enquêtes préliminaires ou de flagrance ou d'investigations exécutées sur commission rogatoire et concernant tout crime ou délit ainsi que certaines contraventions de cinquième classe, les décisions en matière d'effacement ou de rectification, qui ont pour objet la tenue à jour de ce fichier et sont  détachables d'une procédure judiciaire, constituent non pas des mesures d'administration judiciaire, mais des actes de gestion administrative du fichier et peuvent faire l'objet d'un recours pour excès de pouvoir devant le juge administratif.</ANA>
<ANA ID="9C"> 26-07-05 Il résulte des dispositions de l'article 21 de la loi n° 2003-239 du 18 mars 2003 pour la sécurité intérieure, de l'article 25 de la même loi, qui modifie l'article 17-1 de la loi n° 95-73 du 21 janvier 1995 d'orientation et de programmation relative à la sécurité, ainsi que des articles 1er, 2 et 3 du décret n° 2001-583 du 5 juillet 2001 pris pour l'application des dispositions du troisième alinéa de l'article 31 de la loi n° 78-17 relative à l'informatique, aux fichiers et aux libertés et portant création du système de traitement des infractions constatées (STIC), que si les données nominatives figurant dans le  système de traitement des infractions constatées  portent sur des informations recueillies au cours d'enquêtes préliminaires ou de flagrance ou d'investigations exécutées sur commission rogatoire et concernant tout crime ou délit ainsi que certaines contraventions de cinquième classe, les décisions en matière d'effacement ou de rectification, qui ont pour objet la tenue à jour de ce fichier et sont  détachables d'une procédure judiciaire, constituent non pas des mesures d'administration judiciaire, mais des actes de gestion administrative du fichier et peuvent faire l'objet d'un recours pour excès de pouvoir devant le juge administratif.</ANA>
<ANA ID="9D"> 26-07-06 Il résulte des dispositions de l'article 21 de la loi n° 2003-239 du 18 mars 2003 pour la sécurité intérieure, de l'article 25 de la même loi, qui modifie l'article 17-1 de la loi n° 95-73 du 21 janvier 1995 d'orientation et de programmation relative à la sécurité, ainsi que des articles 1er, 2 et 3 du décret n° 2001-583 du 5 juillet 2001 pris pour l'application des dispositions du troisième alinéa de l'article 31 de la loi n° 78-17 relative à l'informatique, aux fichiers et aux libertés et portant création du système de traitement des infractions constatées (STIC), que si les données nominatives figurant dans le  système de traitement des infractions constatées  portent sur des informations recueillies au cours d'enquêtes préliminaires ou de flagrance ou d'investigations exécutées sur commission rogatoire et concernant tout crime ou délit ainsi que certaines contraventions de cinquième classe, les décisions en matière d'effacement ou de rectification, qui ont pour objet la tenue à jour de ce fichier et sont  détachables d'une procédure judiciaire, constituent non pas des mesures d'administration judiciaire, mais des actes de gestion administrative du fichier et peuvent faire l'objet d'un recours pour excès de pouvoir devant le juge administratif.</ANA>
<ANA ID="9E"> 37-02-02 Il résulte des dispositions de l'article 21 de la loi n° 2003-239 du 18 mars 2003 pour la sécurité intérieure, de l'article 25 de la même loi, qui modifie l'article 17-1 de la loi n° 95-73 du 21 janvier 1995 d'orientation et de programmation relative à la sécurité, ainsi que des articles 1er, 2 et 3 du décret n° 2001-583 du 5 juillet 2001 pris pour l'application des dispositions du troisième alinéa de l'article 31 de la loi n° 78-17 relative à l'informatique, aux fichiers et aux libertés et portant création du système de traitement des infractions constatées (STIC), que si les données nominatives figurant dans le  système de traitement des infractions constatées  portent sur des informations recueillies au cours d'enquêtes préliminaires ou de flagrance ou d'investigations exécutées sur commission rogatoire et concernant tout crime ou délit ainsi que certaines contraventions de cinquième classe, les décisions en matière d'effacement ou de rectification, qui ont pour objet la tenue à jour de ce fichier et sont  détachables d'une procédure judiciaire, constituent non pas des mesures d'administration judiciaire, mais des actes de gestion administrative du fichier et peuvent faire l'objet d'un recours pour excès de pouvoir devant le juge administratif.</ANA>
<ANA ID="9F"> 54-01-01-01 Il résulte des dispositions de l'article 21 de la loi n° 2003-239 du 18 mars 2003 pour la sécurité intérieure, de l'article 25 de la même loi, qui modifie l'article 17-1 de la loi n° 95-73 du 21 janvier 1995 d'orientation et de programmation relative à la sécurité, ainsi que des articles 1er, 2 et 3 du décret n° 2001-583 du 5 juillet 2001 pris pour l'application des dispositions du troisième alinéa de l'article 31 de la loi n° 78-17 relative à l'informatique, aux fichiers et aux libertés et portant création du système de traitement des infractions constatées (STIC), que si les données nominatives figurant dans le  système de traitement des infractions constatées  portent sur des informations recueillies au cours d'enquêtes préliminaires ou de flagrance ou d'investigations exécutées sur commission rogatoire et concernant tout crime ou délit ainsi que certaines contraventions de cinquième classe, les décisions en matière d'effacement ou de rectification, qui ont pour objet la tenue à jour de ce fichier et sont  détachables d'une procédure judiciaire, constituent non pas des mesures d'administration judiciaire, mais des actes de gestion administrative du fichier et peuvent faire l'objet d'un recours pour excès de pouvoir devant le juge administratif.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
