<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044167170</ID>
<ANCIEN_ID>JG_L_2021_10_000000438695</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/16/71/CETATEXT000044167170.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 04/10/2021, 438695</TITRE>
<DATE_DEC>2021-10-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>438695</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GADIOU, CHEVALLIER ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>Mme Catherine Calothy</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Stéphane Hoynck</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:438695.20211004</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La communauté d'agglomération du pays ajaccien a demandé au tribunal administratif de Lyon d'annuler pour excès de pouvoir la décision du 21 janvier 2014 par laquelle l'agence de l'eau Rhône Méditerranée et Corse a demandé la réfaction de la subvention qui lui avait été attribuée, ensemble la décision de rejet de son recours gracieux du 2 avril 2014, et d'enjoindre à l'agence de l'eau Rhône Méditerranée et Corse, en application de l'article L. 911-1 du code de justice administrative, de lui verser la subvention prévue par la convention du 16 février 2009 dans un délai d'un mois à compter du jugement à intervenir. Par un jugement n° 1406184 du 3 octobre 2017, le tribunal administratif de Lyon a annulé les décisions contestées. <br/>
<br/>
              Par un arrêt n° 17LY03901 du 17 décembre 2019, la cour administrative d'appel de Lyon a rejeté l'appel formé par l'agence de l'eau Rhône Méditerranée et Corse contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique enregistrés les 14 février et 11 juin 2020 et le 6 juillet 2021 au secrétariat du contentieux du Conseil d'Etat, l'agence de l'eau Rhône Méditerranée et Corse demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la communauté d'agglomération du pays ajaccien la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des relations entre le public et l'administration ;<br/>
              - la loi n° 79-587 du 11 juillet 1979 ;<br/>
              - la loi n° 2000-321 du 12 avril 2000 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Catherine Calothy, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Stéphane Hoynck, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de l'agence de l'eau Rhône Méditerranée et Corse et à la SCP Gadiou, Chevallier, avocat de la communauté d'agglomération du pays ajaccien ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un courrier du 21 janvier 2014, l'agence de l'eau Rhône Méditerranée et Corse a informé la communauté d'agglomération du pays ajaccien de sa décision de procéder à la réfaction totale de la subvention de 260 142 euros qui lui avait été attribuée par convention du 16 février 2009 en vue de la réhabilitation et de l'agrandissement de la station d'épuration située sur le territoire de la commune d'Afa. La communauté d'agglomération a demandé l'annulation de cette décision, ainsi que de celle du 2 avril 2014 rejetant son recours gracieux, au tribunal administratif de Lyon, lequel a fait droit à sa demande, par jugement du 3 octobre 2017. Par un arrêt du 17 décembre 2019, la cour administrative de Lyon a rejeté l'appel formé par l'agence de l'eau Rhône Méditerranée et Corse contre ce jugement. L'agence de l'eau Rhône Méditerranée et Corse se pourvoit en cassation contre cet arrêt.<br/>
<br/>
              2. Aux termes de l'article 1er de la loi du 11 janvier 1979, dont les dispositions sont désormais reprises à l'article L. 211-2 du code des relations entre le public et l'administration, doivent être motivées les décisions qui : " -(...) imposent des sujétions ; (...) -retirent ou abrogent une décision créatrice de droits (...) ". Aux termes de l'article 24 de la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations, dont les dispositions sont désormais reprises en substance à l'article L. 122-1 du code des relations entre le public et l'administration : " Exception faite des cas où il est statué sur une demande, les décisions individuelles qui doivent être motivées en application des articles 1er et 2 de la loi n° 79-587 du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public n'interviennent qu'après que la personne intéressée a été mise à même de présenter des observations écrites et, le cas échéant, sur sa demande, des observations orales. Cette personne peut se faire assister par un conseil ou représenter par un mandataire de son choix (...) ".<br/>
<br/>
              3. Si les décisions accordant une subvention publique à une personne morale constituent des décisions individuelles créatrices de droit, ce n'est que dans la mesure où les conditions dont elles sont assorties, qu'elles soient fixées par des normes générales et impersonnelles, ou propres à la décision d'attribution, sont respectées par leur bénéficiaire. Quand ces conditions ne sont pas respectées, la réfaction de la subvention peut intervenir sans condition de délai. En vertu des dispositions combinées des articles L. 122-1et L. 211-2 du code des relations entre le public et l'administration, l'administration qui envisage de procéder au retrait de la subvention pour ce motif doit mettre leur bénéficiaire en mesure de présenter ses observations. <br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond que, à la suite d'un contrôle effectué le 7 octobre 2011, l'agence de l'eau Rhône Méditerranée et Corse a constaté des manquements dans le respect des engagements pris au titre de la convention du 16 février 2009 par la communauté d'agglomérations du pays ajaccien, fixant les conditions d'octroi de la subvention litigieuse. La communauté d'agglomération du pays ajaccien a été informée de ces manquements par un courrier du 3 juillet 2012 qui indiquait que, faute de régularisation, un processus de réfaction de l'aide attribuée serait engagé. Par un courrier du 27 juillet 2012, la communauté d'agglomération du pays ajaccien a informé l'agence de l'eau Rhône Méditerranée et Corse de son intention d'engager les actions nécessaires à la mise en conformité du projet avec les conditions encadrant la subvention. Elle a ensuite adressé un courrier présentant ces actions le 21 septembre 2012, et confirmé ces deux correspondances par un courrier électronique du 27 septembre qui figurait au dossier soumis aux juges du fond et établissait qu'elle avait pris connaissance des précédents courriers de l'agence de l'eau. Par un courrier du 21 janvier 2014, l'agence de l'eau Rhône Méditerranée et Corse a informé la communauté d'agglomération du pays ajaccien que, faute de respect des engagements pris dans la convention et en dépit des mesures annoncées dans les courriers des 27 juillet et 21 septembre 2012, elle allait procéder à la réfaction de l'aide accordée. La communauté d'agglomération du pays ajaccien a répondu à cette information par un courrier du 20 mars 2014. Ces échanges ont mis la communauté d'agglomération du pays ajaccien en mesure de présenter ses observations écrites et d'établir, si elle s'y estimait fondée, le respect des conditions auxquelles était assortie la subvention dont elle a bénéficié. Il en résulte qu'en estimant que l'agence de l'eau Rhône Méditerranée et Corse n'avait pas mis en œuvre la procédure contradictoire prévue à l'article 24 de la loi du 12 avril 2000, la cour administrative d'appel a entaché son arrêt de dénaturation. Par suite, et sans qu'il soit nécessaire de se prononcer sur les autres moyens du pourvoi, l'agence de l'eau Rhône Méditerranée et Corse est fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la communauté d'agglomération du pays ajaccien la somme de 3 000 euros à verser à l'agence de l'eau Rhône Méditerranée et Corse, au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces dispositions font en revanche obstacle à ce qu'une somme soit mise à ce titre à la charge de l'agence de l'eau Rhône Méditerranée et Corse qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon du 17 décembre 2019 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon. <br/>
Article 3 : La communauté d'agglomération du pays ajaccien versera à l'agence de l'eau Méditerranée Rhône et Corse une somme de 3 000 euros, au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par la communauté d'agglomération du pays ajaccien sur le fondement de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à l'agence de l'eau Rhône Méditerranée et Corse et à la communauté d'agglomération du pays ajaccien.<br/>
Copie en sera adressée à la ministre de la transition écologique. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-06-02-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. - DIFFÉRENTES CATÉGORIES D'ACTES. - ACTES ADMINISTRATIFS - CLASSIFICATION. - ACTES INDIVIDUELS OU COLLECTIFS. - ACTES CRÉATEURS DE DROITS. - ATTRIBUTION D'UNE SUBVENTION PAR UNE PERSONNE PUBLIQUE - 1) EXISTENCE, DANS LA MESURE OÙ LE BÉNÉFICIAIRE RESPECTE LES CONDITIONS MISES À SON OCTROI [RJ1] - 2) POSSIBILITÉ DE RETIRER LA SUBVENTION EN CAS DE DE NON-RESPECT DES CONDITIONS MISES À SON OCTROI - EXISTENCE - OBLIGATION DE RESPECTER UNE PROCÉDURE CONTRADICTOIRE (ART. L. 122-1 DU CRPA) - EXISTENCE [RJ2], Y COMPRIS LORSQUE LE BÉNÉFICIAIRE EST UNE COLLECTIVITÉ PUBLIQUE [RJ3].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">01-03-03-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. - VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. - PROCÉDURE CONTRADICTOIRE. - CARACTÈRE OBLIGATOIRE. - DÉCISION DE RETRAIT D'UNE SUBVENTION EN CAS DE NON-RESPECT DES CONDITIONS MISES À SON OCTROI [RJ1] - EXISTENCE (ART. L. 122-1 DU CRPA) [RJ2], Y COMPRIS LORSQUE LE BÉNÉFICIAIRE EST UNE COLLECTIVITÉ PUBLIQUE [RJ3].
</SCT>
<ANA ID="9A"> 01-01-06-02-01 Si les décisions accordant une subvention publique à une personne morale constituent des décisions individuelles créatrices de droit, ce n'est que dans la mesure où les conditions dont elles sont assorties, qu'elles soient fixées par des normes générales et impersonnelles, ou propres à la décision d'attribution, sont respectées par leur bénéficiaire. Quand ces conditions ne sont pas respectées, la réfaction de la subvention peut intervenir sans condition de délai. ......En vertu des dispositions combinées des articles L. 122-1et L. 211-2 du code des relations entre le public et l'administration (CRPA), l'administration qui envisage de procéder au retrait de la subvention pour ce motif doit mettre son bénéficiaire, y compris lorsqu'il s'agit d'une collectivité publique, en mesure de présenter ses observations.</ANA>
<ANA ID="9B"> 01-03-03-01 Si les décisions accordant une subvention publique à une personne morale constituent des décisions individuelles créatrices de droit, ce n'est que dans la mesure où les conditions dont elles sont assorties, qu'elles soient fixées par des normes générales et impersonnelles, ou propres à la décision d'attribution, sont respectées par leur bénéficiaire. Quand ces conditions ne sont pas respectées, la réfaction de la subvention peut intervenir sans condition de délai. ......En vertu des dispositions combinées des articles L. 122-1et L. 211-2 du code des relations entre le public et l'administration (CRPA), l'administration qui envisage de procéder au retrait de la subvention pour ce motif doit mettre son bénéficiaire, y compris lorsqu'il s'agit d'une collectivité publique, en mesure de présenter ses observations.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 5 juillet 2010, Chambre de commerce et d'industrie de l'Indre, n° 308615, p. 238....[RJ2] Cf. CE, Section, 13 mars 2015, Office de développement de l'économie agricole d'outre-mer, n° 364612, p. 84....[RJ3] Comp., s'agissant de l'obligation de transmission des demandes à l'autorité compétente (art. L. 114-1 du CRPA), CE, 1er juillet 2005, Ville de Nice, n° 258509, p. 304 ; s'agissant de l'obligation d'accuser réception des demandes (art. L. 112-3 du CRPA), CE, 16 janvier 2006, Région Haute-Normandie, n° 269384, T. pp. 698-741.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
