<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034808264</ID>
<ANCIEN_ID>JG_L_2017_05_000000405787</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/80/82/CETATEXT000034808264.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 24/05/2017, 405787</TITRE>
<DATE_DEC>2017-05-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>405787</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Marc Firoud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:405787.20170524</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              La société Techno Logistique a demandé au juge des référés du tribunal administratif de Clermont-Ferrand, d'une part, d'enjoindre au ministre de la défense (Atelier industriel de l'aéronautique de Clermont-Ferrand) de lui communiquer les motifs du rejet de son offre concernant un marché de prestations de bourrellerie aéronautique et, d'autre part, d'annuler la procédure de passation de ce marché. <br/>
<br/>
              Par une ordonnance n° 1601950 du 23 novembre 2016, le juge des référés du tribunal administratif de Clermont-Ferrand a annulé la procédure de passation de ce marché et rejeté le surplus des conclusions des parties.<br/>
<br/>
              Par un pourvoi et trois nouveaux mémoires, enregistrés le 8 décembre 2016 et les 6 janvier, 5 avril et 5 mai 2017 au secrétariat du contentieux du Conseil d'Etat, le ministre de la défense demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire au titre de la procédure de référé, de rejeter le surplus de la demande de la société Techno Logistique ;<br/>
<br/>
              3°) de mettre à la charge de la société Techno Logistique la somme de 3 200 euros au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - l'ordonnance n° 2005-649 du 6 juin 2005 ;<br/>
              - l'ordonnance n° 2015-899 du 23 juillet 2015 ;<br/>
              - le code des marchés publics ;<br/>
              - le code de justice administrative ;		<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Firoud, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la société Techno Logistique.<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Clermont-Ferrand que, par un avis d'appel public à la concurrence, l'Atelier industriel de l'aéronautique (AIA) de Clermont-Ferrand a engagé une procédure d'appel d'offres restreint en vue de l'attribution d'un marché de prestations de bourrellerie aéronautique ; que, par courrier du 28 octobre 2016, l'AIA de Clermont-Ferrand a informé la société Techno Logistique du rejet de son offre et, par courrier du 16 novembre 2016, lui a communiqué les motifs de ce rejet ; que le ministre de la défense se pourvoit en cassation contre l'ordonnance du 23 novembre 2016 par laquelle le juge du référé précontractuel a, sur la demande de la société Techno Logistique, annulé la procédure d'attribution de ce marché et rejeté le surplus des conclusions présentées par les parties ; que le pourvoi du ministre doit être regardé comme dirigé contre cette ordonnance en tant qu'elle lui fait grief ;<br/>
<br/>
              2. Considérant, d'une part, qu'aux termes de l'article L. 551-1 du code de justice administrative : " Le président du tribunal administratif, ou le magistrat qu'il délègue, peut être saisi en cas de manquement aux obligations de publicité et de mise en concurrence auxquelles est soumise la passation par les pouvoirs adjudicateurs de contrats administratifs ayant pour objet l'exécution de travaux, la livraison de fournitures ou la prestation de services, avec une contrepartie économique constituée par un prix ou un droit d'exploitation (...) " ; qu'aux termes de l'article L. 551-2 du même code, dans sa rédaction applicable au marché en litige  : " I. - Le juge peut ordonner à l'auteur du manquement de se conformer à ses obligations et suspendre l'exécution de toute décision qui se rapporte à la passation du contrat (...) / Il peut, en outre, annuler les décisions qui se rapportent à la passation du contrat et supprimer les clauses ou prescriptions destinées à figurer dans le contrat et qui méconnaissent lesdites obligations. / II. - Toutefois, le I n'est pas applicable aux contrats passés dans les domaines de la défense ou de la sécurité au sens du II de l'article 2 de l'ordonnance n° 2005-649 du 6 juin 2005 relative aux marchés passés par certaines personnes publiques ou privées non soumises au code des marchés publics. / Pour ces contrats, il est fait application des articles L. 551-6 et L. 551-7 " ; qu'aux termes de l'article L. 551-6 du même code : " Le juge peut ordonner à l'auteur du manquement de se conformer à ses obligations en lui fixant un délai à cette fin. Il peut lui enjoindre de suspendre l'exécution de toute décision se rapportant à la passation du contrat (...) " ; qu'aux termes de l'article L. 551-7 du même code : " Le juge peut toutefois, en considération de l'ensemble des intérêts susceptibles d'être lésés et notamment de l'intérêt public, écarter les mesures énoncées au premier alinéa de l'article L. 551-6 lorsque leurs conséquences négatives pourraient l'emporter sur leurs avantages " ;<br/>
<br/>
              3. Considérant, d'autre part, qu'aux termes du II de l'article 2 de l'ordonnance du 6 juin 2005 relative aux marchés passés par certaines personnes publiques ou privées non soumises au code des marchés publics, en vigueur à la date du litige, repris à l'article 6 de l'ordonnance du 23 juillet 2015 relative aux marchés publics : " Les marchés et accords-cadres de défense ou de sécurité sont les marchés et accords-cadres ayant pour objet :/ 1° La fourniture d'équipements, y compris leurs pièces détachées, composants ou sous-assemblages, qui sont destinés à être utilisés comme armes, munitions ou matériel de guerre, qu'ils aient été spécifiquement conçus à des fins militaires ou qu'ils aient été initialement conçus pour une utilisation civile puis adaptés à des fins militaires ; / (...) 3° Des travaux, fournitures et services directement liés à un équipement visé au 1° ou 2°, (...) pour tout ou partie du cycle de vie de l'équipement ; le cycle de vie de l'équipement est l'ensemble des états successifs qu'il peut connaître, notamment (...) la réparation, la modernisation, la modification, l'entretien, la logistique, (...) " ; <br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que le marché en litige portait sur des prestations de bourrellerie aéronautique sur des aéronefs militaires, en particulier sur des avions de chasse ; qu'il porte ainsi sur des travaux, fournitures et services directement liés à un matériel de guerre et constitue, par suite, un marché de défense au sens du II de l'article 2 de l'ordonnance du 6 juin 2005 précitée ; qu'il résulte des dispositions précitées du II de l'article L. 551-2 du code de justice administrative que le juge des référés du tribunal administratif de Clermont-Ferrand ne pouvait pas prononcer l'annulation d'un tel marché sur le fondement des dispositions du I de cet article mais seulement prononcer, le cas échéant, les mesures d'injonction et d'astreinte prévues à l'article L. 551-6 du même code ; qu'il suit de là que le juge des référés, en se fondant sur des dispositions du code de justice administrative non applicables au marché en litige, a commis une erreur de droit en annulant le marché en litige et que son ordonnance doit être annulée, sans qu'il soit besoin de se prononcer sur les moyens soulevés par le ministre ;<br/>
<br/>
              5. Considérant que, dans les circonstances de l'espèce, il y a lieu, en application de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée par la société Techno Logistique ; <br/>
<br/>
              6. Considérant, en premier lieu, que le pouvoir adjudicateur définit librement la méthode de notation pour la mise en oeuvre de chacun des critères de sélection des offres qu'il a définis et rendus publics ; que, toutefois, ces méthodes de notation sont entachées d'irrégularité si, en méconnaissance des principes fondamentaux d'égalité de traitement des candidats et de transparence des procédures, elles sont, par elles-mêmes, de nature à priver de leur portée les critères de sélection ou à neutraliser leur pondération et sont, de ce fait, susceptibles de conduire, pour la mise en oeuvre de chaque critère, à ce que la meilleure note ne soit pas attribuée à la meilleure offre, ou, au regard de l'ensemble des critères pondérés, à ce que l'offre économiquement la plus avantageuse ne soit pas choisie ; qu'il en va ainsi alors même que le pouvoir adjudicateur, qui n'y est pas tenu, aurait rendu publiques, dans l'avis d'appel à concurrence ou les documents de la consultation, de telles méthodes de notation ; <br/>
<br/>
              7. Considérant que l'AIA de Clermont-Ferrand a fixé, pour l'attribution du marché public litigieux, trois critères : le prix, la valeur technique et la politique sociale, pondérés respectivement à 60 %, 30 % et 10 % ; qu'ainsi que le soutient la société Techno Logistique, la méthode de notation retenue par l'AIA de Clermont-Ferrand, conduisant automatiquement, sur le critère du prix, à l'attribution de la note maximale de 20 à l'offre la moins disante et de 0 à l'offre la plus onéreuse, a pour effet, compte tenu de la pondération élevée de ce critère, de neutraliser les deux autres critères en éliminant automatiquement l'offre la plus onéreuse, quel que soit l'écart entre son prix et celui des autres offres et alors même qu'elle aurait obtenu les meilleures notes sur les autres critères ; qu'elle peut ainsi avoir pour effet d'éliminer l'offre économiquement la plus avantageuse au profit de l'offre la mieux disante sur le seul critère du prix, et ce quel que soit le nombre de candidats, contrairement à ce que soutient le ministre de la défense ; qu'il résulte de ce qui a été dit au point 6 qu'en retenant une telle méthode de notation pour l'attribution du marché litigieux, l'AIA a manqué à ses obligations de mise en concurrence ;<br/>
<br/>
              8. Considérant, toutefois, qu'il résulte de l'instruction que la société Techno Logistique a obtenu une note inférieure à celle de la société attributaire du marché sur les critères du prix et de la valeur technique et une note égale (zéro) sur le critère social ; qu'ainsi, la société Techno Logistique n'a pu être lésée par le manquement relevé au point 7 dès lors qu'elle n'était, quelle que soit la méthode de notation retenue, pas susceptible de se voir attribuer le marché litigieux ; qu'il résulte de ce qui précède que la société Techno Logistique n'est pas fondée à se prévaloir du manquement aux obligations de mise en concurrence relevé au point précédent ; <br/>
<br/>
              9. Considérant, en deuxième lieu, que la société Techno Logistique soutient que l'AIA de Clermont-Ferrand a dénaturé les termes de son offre en estimant qu'elle ne satisfaisait pas le critère social ; que, toutefois, il résulte de l'instruction que la société requérante n'a pas complété l'annexe prévue dans les documents de la consultation afin de décrire ses efforts dans le domaine social ; que, par suite, ce moyen doit être écarté ;<br/>
<br/>
              10. Considérant, en dernier lieu, que la société Techno Logistique soutient qu'aucun motif, même d'urgence, ne justifie qu'un nouveau marché soit conclu par l'AIA avant l'expiration du marché à bons de commande dont elle est titulaire, lequel a un objet similaire à celui en litige ; que, toutefois, le contrôle exercé par le juge du référé précontractuel ne peut porter sur les conséquences éventuelles de l'application des stipulations d'un autre contrat, dont l'exécution est en cours ; <br/>
<br/>
              11. Considérant qu'il résulte de ce qui précède que la société Techno Logistique n'est pas fondée à demander au juge du référé précontractuel de prononcer l'une des mesures prévues à l'article L. 551-6 du code de justice administrative ;<br/>
<br/>
              12. Considérant que les conclusions présentées par la société Techno Logistique au titre de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ; qu'il y a lieu, en revanche, de mettre à charge de la société Techno Logistique le versement à l'Etat de la somme de 1 000 euros au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 23 novembre 2016 du juge des référés du tribunal administratif de Clermont-Ferrand est annulée.<br/>
Article 2 : La demande de la société Techno Logistique présentée devant le juge des référés du tribunal administratif de Clermont-Ferrand et ses conclusions présentées devant le Conseil d'Etat tendant à l'application de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La société Techno Logistique versera à l'Etat une somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la société Techno Logistique et à la ministre des armées.<br/>
Copie en sera adressée au ministre de l'économie. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">08-20 ARMÉES ET DÉFENSE. - MARCHÉS DE DÉFENSE OU DE SÉCURITÉ - NOTION DE MARCHÉ DE DÉFENSE (ART. 2 DE L'ORDONNANCE DU 6 JUIN 2005) - INCLUSION EN L'ESPÈCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-02-005 MARCHÉS ET CONTRATS ADMINISTRATIFS. FORMATION DES CONTRATS ET MARCHÉS. FORMALITÉS DE PUBLICITÉ ET DE MISE EN CONCURRENCE. - MÉTHODE DE NOTATION DES OFFRES - MÉTHODE AYANT POUR EFFET, COMPTE TENU DE LA PONDÉRATION DES CRITÈRES, DE RENDRE DÉTERMINANT UN SEUL CRITÈRE ET DE NEUTRALISER LES AUTRES - VIOLATION DES RÈGLES DE MISE EN CONCURRENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">39-08-015-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. PROCÉDURES D'URGENCE. - RÈGLES PARTICULIÈRES APPLICABLES AUX MARCHÉS DE DÉFENSE OU DE SÉCURITÉ AU SENS DE L'ORDONNANCE DU 6 JUIN 2005 (II DE L'ART. L. 551-2 DU CJA) - NOTION DE MARCHÉ DE DÉFENSE - INCLUSION EN L'ESPÈCE.
</SCT>
<ANA ID="9A"> 08-20 Marché portant sur des prestations de bourrellerie aéronautique sur des aéronefs militaires, en particulier sur des avions de chasse. Le marché porte ainsi sur des travaux, fournitures et services directement liés à un matériel de guerre et constitue, par suite, un marché de défense au sens du II de l'article 2 de l'ordonnance n° 2005-649 du 6 juin 2005, repris à l'article 6 de l'ordonnance n° 2015-889 du 23 juillet 2015 relative aux marchés publics.</ANA>
<ANA ID="9B"> 39-02-005 Pouvoir adjudicateur ayant fixé, pour l'attribution du marché public litigieux, trois critères : le prix, la valeur technique et la politique sociale, pondérés respectivement à 60 %, 30 % et 10 %.... ,,La méthode de notation retenue par le pouvoir adjudicateur, conduisant automatiquement, sur le critère du prix, à l'attribution de la note maximale de 20 à l'offre la moins disante et de 0 à l'offre la plus onéreuse, a pour effet, compte tenu de la pondération élevée de ce critère, de neutraliser les deux autres critères en éliminant automatiquement l'offre la plus onéreuse, quel que soit l'écart entre son prix et celui des autres offres et alors même qu'elle aurait obtenu les meilleures notes sur les autres critères. Elle peut ainsi avoir pour effet d'éliminer l'offre économiquement la plus avantageuse au profit de l'offre la mieux disante sur le seul critère du prix, et ce quel que soit le nombre de candidats. En retenant une telle méthode de notation pour l'attribution du marché litigieux, le pouvoir adjudicateur a manqué à ses obligations de mise en concurrence.</ANA>
<ANA ID="9C"> 39-08-015-01 Marché en litige portant sur des prestations de bourrellerie aéronautique sur des aéronefs militaires, en particulier sur des avions de chasse. Le marché porte ainsi sur des travaux, fournitures et services directement liés à un matériel de guerre et constitue, par suite, un marché de défense au sens du II de l'article 2 de l'ordonnance n° 2005-649 du 6 juin 2005, repris à l'article 6 de l'ordonnance n° 2015-889 du 23 juillet 2015 relative aux marchés publics.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
