<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037220708</ID>
<ANCIEN_ID>JG_L_2018_07_000000412153</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/22/07/CETATEXT000037220708.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 4ème chambres réunies, 18/07/2018, 412153</TITRE>
<DATE_DEC>2018-07-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412153</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean-Luc Nevache</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:412153.20180718</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 5 juillet 2017 au secrétariat du contentieux du Conseil d'Etat, la FNATH, association des accidentés de la vie, demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le 2° de l'article 1er du décret n° 2017-812 du 5 mai 2017 révisant et complétant les tableaux des maladies professionnelles annexés au livre IV du code de la sécurité sociale ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du travail ;<br/>
              - le code de la sécurité sociale ;<br/>
              - la loi n° 2015-994 du 17 août 2015 ;<br/>
              - le décret n° 2006-672 du 8 juin 2006 ;<br/>
              - le décret n° 2008-1217 du 25 novembre 2008 ;<br/>
              - le décret n° 2016-1834 du 22 décembre 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Nevache, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes des deuxième et troisième alinéas de l'article L. 461-1 du code de la sécurité sociale, dans sa rédaction applicable à la date du décret attaqué : " Est présumée d'origine professionnelle toute maladie désignée dans un tableau de maladies professionnelles et contractée dans les conditions mentionnées à ce tableau. / Si une ou plusieurs conditions tenant au délai de prise en charge, à la durée d'exposition ou à la liste limitative des travaux ne sont pas remplies, la maladie telle qu'elle est désignée dans un tableau de maladies professionnelles peut être reconnue d'origine professionnelle lorsqu'il est établi qu'elle est directement causée par le travail habituel de la victime ". En vertu de l'article L. 461-2 du même code, dans sa rédaction applicable à la date du décret attaqué : " Des tableaux annexés aux décrets énumèrent les manifestations morbides d'intoxications aiguës ou chroniques présentées par les travailleurs exposés d'une façon habituelle à l'action des agents nocifs mentionnés par lesdits tableaux, qui donnent, à titre indicatif, la liste des principaux travaux comportant la manipulation ou l'emploi de ces agents. Ces manifestations morbides sont présumées d'origine professionnelle. / Des tableaux spéciaux énumèrent les infections microbiennes mentionnées qui sont présumées avoir une origine professionnelle lorsque les victimes ont été occupées d'une façon habituelle aux travaux limitativement énumérés par ces tableaux. / D'autres tableaux peuvent déterminer des affections présumées résulter d'une ambiance ou d'attitudes particulières nécessitées par l'exécution des travaux limitativement énumérés. / Les tableaux mentionnés aux alinéas précédents peuvent être révisés et complétés par des décrets, après avis du Conseil d'orientation des conditions de travail. (...) " .<br/>
<br/>
              2. L'association requérante demande l'annulation pour excès de pouvoir du 2° de l'article 1er du décret du 5 mai 2017 révisant et complétant, en application des dispositions citées au point précédant, les tableaux des maladies professionnelles annexés au livre IV du code de la sécurité sociale pour y insérer un tableau n° 52 bis intitulé " Carcinome hépatocellulaire provoqué par l'exposition au chlorure de vinyle monomère ". Eu égard aux moyens qu'elle invoque, l'association requérante doit être regardée comme demandant l'annulation de ce tableau en tant qu'il prévoit, s'agissant de la désignation de la maladie, que le carcinome hépatocellulaire histologiquement confirmé doit être " associé à au moins deux des lésions suivantes du foie non tumoral : fibrose porte et pénicillée péri porte ou nodule(s) fibro-hyalin(s) capsulaire(s) ; congestion sinusoïdale ; hyperplasie ou dysplasie endothéliale ; nodule(s) d'hyperplasie hépatocytaire ; foyer(s) de dysplasie hépatocytaire ".<br/>
<br/>
              Sur la consultation du Conseil d'orientation des conditions de travail :<br/>
<br/>
              3. D'une part, l'article L. 461-2 du code de la sécurité sociale, dans sa rédaction en vigueur à la date du décret attaqué, issue de la loi du 17 août 2015 relative au dialogue social et à l'emploi, prévoit que les tableaux qu'il mentionne peuvent être révisés et complétés par des décrets pris après avis du Conseil d'orientation des conditions de travail. Si les dispositions critiquées du décret du 5 mai 2017 ont été précédées, le 18 mai 2015, de la consultation du Conseil d'orientation sur les conditions de travail, qui n'avait été institué alors que par le décret du 25 novembre 2008 relatif au Conseil d'orientation sur les conditions de travail, ce décret a entendu substituer ce conseil au conseil supérieur de la prévention des risques professionnels, dont la consultation était requise par les dispositions alors en vigueur de l'article L. 461-2 du code de la sécurité sociale. Par suite, la fédération requérante ne peut utilement se prévaloir des dispositions de l'article 2 du décret du 8 juin 2006 relatif à la création, à la composition et au fonctionnement de commissions administratives à caractère consultatif, en vertu desquelles une commission est créée par décret pour une durée maximale de cinq ans à moins que son existence soit prévue par la loi, pour soutenir que le Conseil d'orientation sur les conditions de travail n'avait plus d'existence légale à la date du 18 mai 2015 à laquelle il a rendu son avis. Au surplus, la consultation de ce conseil, tel qu'institué par le décret du 25 novembre 2008, permet de regarder comme remplie l'obligation de consultation du Conseil d'orientation des conditions de travail, telle qu'elle était prévue, à la date du décret attaqué, par l'article L. 461-2 du code de la sécurité sociale, cité au point 1. <br/>
<br/>
              4. D'autre part, en soutenant qu'il n'est pas établi que ce conseil était régulièrement composé et suffisamment informé du projet qui lui était soumis lorsqu'il a émis son avis le 18 mai 2015, la fédération requérante, qui, au demeurant, siège au sein de ce conseil, n'assortit pas le moyen qu'elle soulève des précisions permettant d'en apprécier le bien fondé. <br/>
<br/>
              Sur la présomption d'imputabilité des maladies professionnelles :<br/>
<br/>
              5. Il résulte des dispositions citées au point 1, d'une part, que les conditions qui définissent, en application du troisième alinéa de l'article L. 461-1 du code de la sécurité sociale, la manière dont sont contractées les maladies, et qui sont susceptibles de figurer, à ce titre, dans les tableaux désignant les maladies présumées d'origine professionnelle, ne peuvent légalement porter que sur le délai maximum de constatation d'une maladie, la durée d'exposition ou la liste limitative des travaux à même de provoquer une maladie et, d'autre part, que ces conditions ne sauraient méconnaître le principe de présomption d'imputabilité posé par le deuxième alinéa du même article et par l'article L. 461-2 du même code. <br/>
<br/>
              6. D'une part, ces dispositions ne font pas obstacle à ce que le pouvoir réglementaire, auquel il incombe de désigner avec suffisamment de précisions ces maladies, définisse à cette fin, dans le respect du principe de présomption d'imputabilité, les éléments du diagnostic d'une pathologie d'origine professionnelle. Ainsi, ce principe ne s'opposait pas à ce que le constat de certaines lésions associées soit exigé pour caractériser la pathologie. <br/>
<br/>
              7. D'autre part, et alors qu'il ne ressort pas des pièces du dossier que le groupe de travail sur les travaux duquel le pouvoir réglementaire s'est appuyé se serait adjoint le concours d'experts dépourvus d'une compétence suffisante ou en situation de conflit d'intérêts, il ne ressort pas de ces mêmes pièces, en l'état des éléments versés par les parties en réponse à la mesure d'instruction diligentée par la 1ère chambre de la section du contentieux, que le pouvoir réglementaire aurait inexactement apprécié les éléments du diagnostic d'un carcinome hépatocellulaire provoqué par l'exposition au chlorure de vinyle monomère en exigeant qu'il soit histologiquement confirmé et associé à deux au moins des lésions du foie non tumoral mentionnées au point 2. <br/>
<br/>
              8. Par suite, l'association requérante n'est pas fondée à soutenir que la désignation de cette maladie à laquelle procède le tableau n° 52 bis, en ce qu'elle impose la présence de ces lésions du foie non tumoral pour qu'elle soit présumée d'origine professionnelle lorsqu'elle est contractée dans les conditions de délai, de durée d'exposition et de travaux fixées par ailleurs par le tableau, méconnaît l'article L. 461-1 du code de la sécurité sociale. <br/>
<br/>
              9. Il résulte de tout ce qui précède que la fédération requérante n'est pas fondée à demander l'annulation du décret qu'elle attaque. Par suite, sa requête doit être rejetée, y compris les conclusions qu'elle présente au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                           --------------<br/>
<br/>
Article 1er : La requête de la FNATH, association des accidentés de la vie est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la FNATH, association des accidentés de la vie, à la ministre des solidarités et de la santé, à la ministre du travail et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">62-04-05 SÉCURITÉ SOCIALE. PRESTATIONS. PRESTATIONS D'ASSURANCES ACCIDENTS DU TRAVAIL ET MALADIES PROFESSIONNELLES. - DÉSIGNATION DES MALADIES PRÉSUMÉES D'ORIGINE PROFESSIONNELLE - PRINCIPE DE PRÉSOMPTION D'IMPUTABILITÉ (ART. L. 461-1 ET L. 461-2 DU CSS) - POSSIBILITÉ, DANS LE RESPECT DE CE PRINCIPE, DE DÉFINIR LES ÉLÉMENTS DE DIAGNOSTIC D'UNE PATHOLOGIE ET D'EXIGER LE CONSTAT DE CERTAINES LÉSIONS ASSOCIÉES - EXISTENCE [RJ1].
</SCT>
<ANA ID="9A"> 62-04-05 Les dispositions des articles L. 461-1 et L. 461-2 du code de la sécurité sociale (CSS) ne font pas obstacle à ce que le pouvoir réglementaire, auquel il incombe de désigner avec suffisamment de précisions les maladies présumées d'origine professionnelle, définisse à cette fin, dans le respect du principe de présomption d'imputabilité, les éléments du diagnostic d'une pathologie d'origine professionnelle. Ainsi, ce principe ne s'oppose pas à ce que le constat de certaines lésions associées soit exigé pour caractériser la pathologie.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp., s'agissant des éléments relatifs à l'imputabilité de la pathologie, CE, 10 mars 2010, Association des familles victimes du saturnisme, n° 322824, T. p. 991.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
