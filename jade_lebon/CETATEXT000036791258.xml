<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036791258</ID>
<ANCIEN_ID>JG_L_2018_04_000000418027</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/79/12/CETATEXT000036791258.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 11/04/2018, 418027, Publié au recueil Lebon</TITRE>
<DATE_DEC>2018-04-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418027</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>M. Stéphane Hoynck</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:418027.20180411</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme D...A...B...a demandé au juge des référés du tribunal administratif de Montreuil, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'une part, de suspendre l'exécution de la décision du 19 janvier 2018 par laquelle le ministre d'Etat, ministre de l'intérieur lui a refusé l'entrée sur le territoire français et la décision du même jour par laquelle la même autorité l'a placée en zone d'attente, d'autre part, d'enjoindre à l'administration d'exécuter l'ordonnance à intervenir à compter de son édiction sous astreinte de 200 euros par heure de retard. Par une ordonnance n° 1800593 du 25 janvier 2018, le juge des référés du tribunal administratif de Montreuil a suspendu l'exécution de la décision du 19 janvier 2018, enjoint à l'administration de permettre l'entrée de Mme A...B...sur le territoire français et rejeté le surplus des conclusions de sa requête.<br/>
<br/>
              Par un recours et deux mémoires, enregistrés les 9, 16 février et 27 mars 2018 au secrétariat du contentieux du Conseil d'Etat, le ministre d'Etat, ministre de l'intérieur demande au Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de rejeter les conclusions de la requête formée par Mme A...B...en première instance.<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu : <br/>
              - la Constitution et son préambule ;<br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - la convention d'application de l'accord de Schengen du 19 juin 1990 ;<br/>
              - le règlement (UE) 2016/399 du Parlement européen et du Conseil du 15 mars 2006 concernant un code de l'union relatif au régime de franchissement des frontières par les personnes (code frontières Schengen) ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Stéphane Hoynck, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de Mme D...A...B...;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ".<br/>
<br/>
              2. Il résulte de l'instruction que Mme A...B..., de nationalité congolaise, a été contrôlée à l'aéroport de Roissy-Charles-de-Gaulle, à l'arrivée d'un vol en provenance de Kiev (Ukraine) où elle réside, le 19 janvier 2018, alors qu'elle devait se rendre, le même jour, au Portugal par un vol Paris-Lisbonne. Elle était en possession d'un passeport ordinaire, établi le 31 juillet 2015 à Kinshasa (République démocratique du Congo), revêtu d'un visa Schengen de type C court séjour délivré le 12 janvier 2018 par le consulat du Portugal à Kiev, valable pour une entrée, utilisable jusqu'au 10 février 2018, pour une durée de séjour de 8 jours. À l'issue de ce contrôle, une décision de refus d'entrée sur le territoire lui a été opposée au motif qu'elle n'était pas munie d'un document de voyage en cours de validité lui permettant de voyager dans l'espace Schengen et l'intéressée a été placée en zone d'attente en vue de son réacheminement à Kiev. Mme A...B...a saisi, le 21 janvier 2018, le juge des référés du tribunal administratif de Montreuil, sur le fondement de l'article L. 521-2 du code de justice administrative, d'une demande de suspension des effets de la décision de refus d'entrée du 19 janvier 2018. Par une ordonnance n° 1800593 du 25 janvier 2018 rendue, après que le juge des libertés et de la détention eut autorisé le maintien en zone d'attente de l'intéressée, le juge des référés du tribunal administratif de Montreuil, qui a rejeté le surplus des conclusions de la demande, a suspendu l'exécution de cette décision et a enjoint à l'administration de permettre l'entrée de Mme A... B...sur le territoire français. Le ministre d'Etat, ministre de l'intérieur relève, dans cette mesure, appel de cette ordonnance.<br/>
<br/>
              3. Aux termes de l'article L. 211-1 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Pour entrer en France, tout étranger doit être muni : 1° Des documents et visas exigés par les conventions internationales et les règlements en vigueur (...) ". Aux termes de l'article L. 213-2 du même code : " Tout refus d'entrée en France fait l'objet d'une décision écrite motivée prise, sauf en cas de demande d'asile, par un agent relevant d'une catégorie fixée par voie réglementaire. (...) ". L'article L. 213.-3 de ce code rend ces dernières dispositions applicables " à l'étranger qui n'est pas ressortissant d'un Etat membre de l'Union européenne à qui l'entrée sur le territoire métropolitain a été refusée en application de l'article 5 du règlement (CE) n° 562/2006 du Parlement européen et du Conseil, du 15 mars 2006, établissant un code communautaire relatif au régime de franchissement des frontières par les personnes (code frontières Schengen) ", ce dernier article ayant été repris à l'article 6 du règlement (UE) 2016/399 du Parlement européen et du Conseil, du 9 mars 2016 en vertu duquel les ressortissants d'Etats non membres de l'Union européenne doivent notamment " être en possession d'un document de voyage en cours de validité autorisant son titulaire à franchir la frontière ".<br/>
<br/>
              4. La liberté d'aller et venir, composante de la liberté personnelle protégée par les articles 2 et 4 de la Déclaration des droits de l'homme et du citoyen, constitue une liberté fondamentale au sens de l'article L. 521-2 du code de justice administrative. Elle s'exerce, en ce qui concerne le franchissement des frontières, dans les limites découlant de la souveraineté de l'Etat et des accords internationaux et n'ouvre pas aux étrangers un droit général et absolu d'accès sur le territoire français. Celui-ci est en effet subordonné au respect tant de la législation et de la réglementation en vigueur que des règles qui résultent des engagements européens et internationaux de la France.<br/>
<br/>
              5. Il résulte tant des termes de l'article L. 521-2 du code de justice administrative que du but dans lequel la procédure qu'il instaure a été créée que doit exister un rapport direct entre l'illégalité relevée à l'encontre de l'autorité administrative et la gravité de ses effets au regard de l'exercice de la liberté fondamentale en cause. Or la circonstance que la décision de refus litigieuse serait insuffisamment motivée ne saurait, par elle-même, porter une atteinte grave à l'exercice de la liberté d'aller et venir.<br/>
<br/>
              6. Il s'ensuit que c'est à tort que le juge des référés du tribunal administratif de Montreuil s'est fondé, pour faire droit à la demande de Mme A...B..., sur le moyen tiré de l'insuffisance de motivation de la décision litigieuse.<br/>
<br/>
              7. Il appartient toutefois au Conseil d'Etat, saisi de l'ensemble du litige par l'effet dévolutif de l'appel, d'examiner les autres moyens présentés par Mme A...B...devant le juge des référés du tribunal administratif de Montreuil.<br/>
<br/>
              8. Conformément à l'article 8 du règlement (UE) 2016/399 précité, le mouvement transfrontalier des ressortissants des pays tiers aux frontières extérieures des États membres de l'Union européenne fait l'objet d'une vérification approfondie portant sur les éléments énumérés au 3. de cet article, au nombre desquels figure, à l'entrée, " la vérification que le ressortissant de pays tiers est en possession, pour franchir la frontière, d'un document valable et qui n'est pas arrivé à expiration, et que ce document est accompagné, le cas échéant, du visa ou du permis de séjour requis ". En l'espèce, il appartenait ainsi aux fonctionnaires de police chargés du contrôle aux frontières de s'assurer, comme ils l'ont fait, que Mme A...B..., titulaire d'un visa Schengen de type C court, était bien en possession d'un document valable pour franchir la frontière en provenance d'un Etat tiers à l'Union européenne. La décision de refus d'entrée en France du 19 janvier 2018 est fondée, en application des dispositions rappelées au point 3, sur l'invalidité du passeport de l'intéressée et repose, sans illégalité manifeste, sur l'indication donnée par la note verbale adressée par le ministère des affaires étrangères de la République démocratique du Congo, selon laquelle, seuls sont valables pour les ressortissants de cet Etat, à compter du 14 janvier 2018, les passeports biométriques délivrés après le 15 décembre 2015. <br/>
<br/>
              9. Au surplus, il résulte de l'instruction qu'en exécution de l'ordonnance frappée d'appel, le ministre d'Etat, ministre de l'intérieur a autorisé Mme A...B...à entrer sur le territoire français où elle se trouve encore, à la date de la présente décision, après l'expiration de la durée de validité du visa Schengen de type C court qui lui avait été délivré. Dans ces conditions, la mise en oeuvre d'un éloignement forcé de l'intéressée, qui pourrait procéder de l'exécution tant du refus d'entrée du 19 janvier 2018, que du refus d'entrée au titre de l'asile pris, le 25 janvier 2018, par le ministre de l'intérieur après avis de l'Office français de protection des réfugiés et des apatrides sur la demande d'asile présentée par l'intéressée le 21 janvier 2018, implique nécessairement l'intervention préalable d'une mesure d'obligation de quitter le territoire français. Il s'ensuit que l'exécution de la décision du 19 janvier 2018 n'est plus, en tout état de cause, susceptible de créer, par elle-même, une situation d'urgence caractérisée au sens de l'article L. 521-2 du code de justice administrative. <br/>
<br/>
              10. Il résulte de tout ce qui précède que le ministre d'Etat, ministre de l'intérieur est fondé à soutenir que c'est à tort que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Montreuil a fait droit à la demande présentée par Mme A...B...sur le fondement de l'article L. 521-2 du code de justice administrative et à demander le rejet de la demande présentée devant ce juge par Mme A...B....<br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : Les articles 1 et 2 de l'ordonnance n° 1800593 du juge des référés du tribunal administratif de Montreuil du 25 janvier 2018 sont annulés.<br/>
Article 2 : Les conclusions de la demande présentée par Mme A...B...devant le juge des référés du tribunal administratif de Montreuil tendant à la suspension des effets de la décision du 19 janvier 2018 et à ce qu'il soit enjoint à l'administration de permettre l'entrée de l'intéressée sur le territoire français sont rejetées.<br/>
Article 3 : La présente décision sera notifiée au ministre d'Etat, ministre de l'intérieur et à Mme C...B.... <br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">15-05-045-02 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. RÈGLES APPLICABLES. - ENTRÉE DANS L'ESPACE SCHENGEN - TENEUR DU CONTRÔLE EFFECTUÉ PAR LA POLICE AUX FRONTIÈRES - CONTRÔLE DE LA VALIDITÉ DU DOCUMENT D'UN RESSORTISSANT D'UN PAYS TIERS POUR FRANCHIR LA FRONTIÈRE - INCLUSION - ESPÈCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">26-03-05 DROITS CIVILS ET INDIVIDUELS. LIBERTÉS PUBLIQUES ET LIBERTÉS DE LA PERSONNE. LIBERTÉ D'ALLER ET VENIR. - FRANCHISSEMENT DES FRONTIÈRES - EXERCICE DE LA LIBERTÉ D'ALLER ET VENIR DANS LES LIMITES DÉCOULANT DE LA SOUVERAINETÉ DE L'ETAT ET DES ACCORDS INTERNATIONAUX - CONSÉQUENCE - DROIT GÉNÉRAL ET ABSOLU D'ACCÈS SUR LE TERRITOIRE FRANÇAIS POUR LES ÉTRANGERS - ABSENCE [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">335-005 ÉTRANGERS. ENTRÉE EN FRANCE. - LIBERTÉ D'ALLER ET DE VENIR - EXERCICE DE CETTE LIBERTÉ DANS LES LIMITES DÉCOULANT DE LA SOUVERAINETÉ DE L'ETAT ET DES ACCORDS INTERNATIONAUX - CONSÉQUENCE -DROIT GÉNÉRAL ET ABSOLU D'ACCÈS SUR LE TERRITOIRE FRANÇAIS POUR LES ÉTRANGERS - ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 15-05-045-02 Conformément à l'article 8 du règlement (UE) 2016/399, le mouvement transfrontalier des ressortissants des pays tiers aux frontières extérieures des États membres de l'Union européenne fait l'objet d'une vérification approfondie portant sur les éléments énumérés au 3 de cet article, au nombre desquels figure, à l'entrée, la vérification que le ressortissant de pays tiers est en possession, pour franchir la frontière, d'un document valable et qui n'est pas arrivé à expiration, et que ce document est accompagné, le cas échéant, du visa ou du permis de séjour requis.... ...En l'espèce, il appartenait ainsi aux fonctionnaires de police chargés du contrôle aux frontières de s'assurer, comme ils l'ont fait, que l'intéressée, de nationalité congolaise et titulaire d'un visa Schengen de type C court, était bien en possession d'un document valable pour franchir la frontière en provenance d'un Etat tiers à l'Union européenne. La décision de refus d'entrée en France du 19 janvier 2018 était fondée, en application des articles L. 211-2 et L. 213-3 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), sur l'invalidité du passeport de l'intéressée et reposait, sans illégalité manifeste, sur l'indication donnée par la note verbale adressée par le ministère des affaires étrangères de la République démocratique du Congo, selon laquelle, seuls sont valables pour les ressortissants de cet Etat, à compter du 14 janvier 2018, les passeports biométriques délivrés après le 15 décembre 2015.</ANA>
<ANA ID="9B"> 26-03-05 La liberté d'aller et venir, composante de la liberté personnelle protégée par les articles 2 et 4 de la Déclaration des droits de l'homme et du citoyen, constitue une liberté fondamentale au sens de l'article L. 521-2 du code de justice administrative. Elle s'exerce, en ce qui concerne le franchissement des frontières, dans les limites découlant de la souveraineté de l'Etat et des accords internationaux et n'ouvre pas aux étrangers un droit général et absolu d'accès sur le territoire français. Celui-ci est en effet subordonné au respect tant de la législation et de la réglementation en vigueur que des règles qui résultent des engagements européens et internationaux de la France.</ANA>
<ANA ID="9C"> 335-005 La liberté d'aller et venir, composante de la liberté personnelle protégée par les articles 2 et 4 de la Déclaration des droits de l'homme et du citoyen, constitue une liberté fondamentale au sens de l'article L. 521-2 du code de justice administrative. Elle s'exerce, en ce qui concerne le franchissement des frontières, dans les limites découlant de la souveraineté de l'Etat et des accords internationaux et n'ouvre pas aux étrangers un droit général et absolu d'accès sur le territoire français. Celui-ci est en effet subordonné au respect tant de la législation et de la réglementation en vigueur que des règles qui résultent des engagements européens et internationaux de la France.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, juge des référés, 21 septembre 2007, Ministre de l'intérieur, de l'outre-mer et des collectivités territoriales c/ Mlle,et Ministre de l'intérieur, de l'outre-mer et des collectivités territoriales c/ Mlle,n°s 309497 309498, aux Tables sur un autre point. Rappr. Cons. const., 15 mars 2018, n° 2018-762 DC, Loi permettant une bonne application du régime d'asile européen, cons. 9.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
