<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037134654</ID>
<ANCIEN_ID>JG_L_2018_06_000000408778</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/13/46/CETATEXT000037134654.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 5ème chambres réunies, 29/06/2018, 408778</TITRE>
<DATE_DEC>2018-06-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>408778</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LEDUC, VIGAND</AVOCATS>
<RAPPORTEUR>Mme Laurence Franceschini</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Julie Burguburu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:408778.20180629</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Bordeaux d'annuler pour excès de pouvoir l'arrêté du 23 février 2016 par lequel le préfet de la Gironde a refusé de lui délivrer un titre de séjour, lui a fait obligation de quitter le territoire français dans un délai de trente jours et a fixé le pays à destination duquel il pourra être renvoyé. Par un jugement n° 1601239 du 26 mai 2016, le tribunal administratif de Bordeaux a rejeté cette demande. <br/>
<br/>
              Par un arrêt n° 16BX02642 du 5 décembre 2016, la cour administrative d'appel de Bordeaux a rejeté l'appel formé par M. B...contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés au secrétariat du contentieux du Conseil d'Etat les 9 mars et 9 juin 2017, M. B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) d'enjoindre au préfet de la Gironde de réexaminer sa demande tendant à la délivrance d'un titre de séjour dans un délai d'un mois.  <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              -	la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              -	la convention internationale relative aux droits de l'enfant ;<br/>
              -	le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              -	le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Laurence Franceschini, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Julie Burguburu, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Leduc, Vigand, avocat de M.B....<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M.B..., de nationalité marocaine, entré irrégulièrement en France en mars 2014, a sollicité le 12 août 2015 la délivrance d'un titre de séjour sur le fondement des dispositions du 6° de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile, en se prévalant notamment de sa qualité de parent d'un enfant français né le 8 février 2015. Par un arrêté du 23 février 2016, le préfet de la Gironde a refusé de lui délivrer un titre de séjour, a assorti ce refus d'une obligation de quitter le territoire français et a fixé le pays à destination duquel M. B... serait renvoyé à défaut de se conformer à cette obligation. Par un jugement du 26 mai 2016 et un arrêt du 5 décembre 2016, contre lequel M. B...se pourvoit en cassation, le tribunal administratif de Bordeaux puis la cour administrative d'appel de Bordeaux ont rejeté les conclusions de l'intéressé tendant à l'annulation de cet arrêté. <br/>
<br/>
              2. Aux termes de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile, dans sa rédaction applicable au litige : " Sauf si sa présence constitue une menace pour l'ordre public, la carte de séjour temporaire portant la mention "vie privée et familiale" est délivrée de plein droit : (...) 6° A l'étranger ne vivant pas en état de polygamie, qui est père ou mère d'un enfant français mineur résidant en France, à la condition qu'il établisse contribuer effectivement à l'entretien et à l'éducation de l'enfant dans les conditions prévues par l'article 371-2 du code civil depuis la naissance de celui-ci ou depuis au moins deux ans, sans que la condition prévue à l'article L. 311-7 soit exigée ; (...). ".<br/>
<br/>
              3. Il ressort des énonciations de l'arrêt attaqué, d'une part, que M. B...a produit un certificat de nationalité française du 19 août 2016 du tribunal d'instance de Bordeaux selon lequel son fils Wahid était français depuis sa naissance et, d'autre part, que, par un jugement du 2 juillet 2015, le juge aux affaires familiales lui a accordé le bénéfice de l'exercice conjoint de l'autorité parentale avec la mère de l'enfant, comportant un droit de visite un samedi sur deux, et lui a imposé le versement mensuel de la somme de 50 euros au profit de son enfant. La cour a également précisé qu'il était constant que l'intéressé avait exercé le droit de visite qui lui avait été accordé et qu'il s'était conformé à l'obligation de verser une somme de 50 euros par mois. Après avoir ainsi relevé que M. B...s'était conformé en tous points à la décision du juge des affaires familiales, la cour a toutefois recherché s'il établissait, en outre, contribuer effectivement à l'éducation de son enfant et jugé qu'il ne satisfaisait pas aux conditions prévues par le 6° de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile, au motif qu'il ne fournissait pas d'attestations suffisantes sur ce point. En statuant ainsi, la cour a entaché son arrêt d'une erreur de droit et d'une contradiction de motifs. Par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, le requérant est fondé à demander l'annulation de l'arrêt qu'il attaque.   <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 5 décembre 2016 de la cour administrative d'appel de Bordeaux est annulé.<br/>
<br/>
      Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Bordeaux. <br/>
<br/>
Article 3 : La présente décision sera notifiée à Monsieur A...B...et au ministre d'Etat, ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">335-01-03-04 ÉTRANGERS. SÉJOUR DES ÉTRANGERS. REFUS DE SÉJOUR. MOTIFS. - CARTE DE SÉJOUR TEMPORAIRE VIE PRIVÉE ET FAMILIALE (6° DE L'ART. L. 313-11 DU CESEDA) - MOTIF FONDÉ SUR L'INSUFFISANCE D'ATTESTATIONS ÉTABLISSANT LA CONTRIBUTION EFFECTIVE DU DEMANDEUR À L'ÉDUCATION DE SON ENFANT, APRÈS AVOIR RELEVÉ QU'IL S'ÉTAIT CONFORMÉ EN TOUS POINTS À LA DÉCISION DU JUGE AUX AFFAIRES FAMILIALES LUI ACCORDANT LE BÉNÉFICE DE L'EXERCICE CONJOINT DE L'AUTORITÉ PARENTALE - CONTRADICTION DE MOTIFS - EXISTENCE.
</SCT>
<ANA ID="9A"> 335-01-03-04 Cour ayant relevé que le demandeur avait produit un certificat de nationalité française du 19 août 2016 du tribunal d'instance selon lequel son fils était français depuis sa naissance et que, par un jugement du 2 juillet 2015, le juge aux affaires familiales lui avait accordé le bénéfice de l'exercice conjoint de l'autorité parentale avec la mère de l'enfant comportant un droit de visite un samedi sur deux et lui avait imposé le versement mensuel de la somme de 50 euros au profit de son enfant.... ,,Après avoir ainsi relevé que l'intéressé s'était conformé en tous points à la décision du juge des affaires familiales, la cour a toutefois recherché s'il établissait, en outre, contribuer effectivement à l'éducation de son enfant et jugé qu'il ne satisfaisait pas aux conditions prévues par le 6° de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile, au motif qu'il ne fournissait pas d'attestations suffisantes sur ce point. En statuant ainsi  la cour a entaché son arrêt d'une erreur de droit et d'une contradiction de motifs. Annulation de l'arrêt.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
