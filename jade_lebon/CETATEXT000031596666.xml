<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031596666</ID>
<ANCIEN_ID>JG_L_2015_12_000000391961</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/59/66/CETATEXT000031596666.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème / 3ème SSR, 09/12/2015, 391961</TITRE>
<DATE_DEC>2015-12-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>391961</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème / 3ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>OCCHIPINTI ; RICARD</AVOCATS>
<RAPPORTEUR>Mme Karin Ciavaldini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:391961.20151209</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société à responsabilité limitée Yacht Club International de Saint-Laurent-du-Var a demandé au juge des référés du tribunal administratif de Nice, sur le fondement de l'article L. 521-3 du code de justice administrative :<br/>
              - d'ordonner, si besoin est, avec le concours de la force publique, l'expulsion de la société à responsabilité limitée La Perla Romana ainsi que celle de tout occupant de son chef des cellules commerciales nos 91, 92, 93 et 94 ainsi que des terrasses adjacentes situées sur le port de plaisance de Saint-Laurent-du-Var ;<br/>
              - d'ordonner à la société La Perla Romana de libérer les lieux de tout matériel, mobilier et marchandise, de procéder aux opérations de nettoyage et de remettre les clefs des locaux à la capitainerie ;<br/>
              - de prononcer une astreinte de 1 000 euros par jour de retard en cas de non exécution dans un délai de quinze jours à compter de la notification de l'ordonnance à intervenir. <br/>
<br/>
              Par une ordonnance n° 1502210 du 6 juillet 2015, le juge des référés a enjoint à la société La Perla Romana de libérer les cellules commerciales qu'elle occupait dans un délai d'un mois à compter de la notification de l'ordonnance, sous astreinte de 500 euros par jour de retard à compter de l'expiration de ce délai.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 22 et 28 juillet 2015 au secrétariat du contentieux du Conseil d'Etat, la société  La Perla Romana demande au Conseil d'Etat :<br/>
<br/>
<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire en référé, de rejeter la demande de la société Yacht Club International de Saint-Laurent-du-Var ;<br/>
<br/>
              3°) de mettre à la charge de la société Yacht Club International de Saint-Laurent-du-Var la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de commerce ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Karin Ciavaldini, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Occhipinti, avocat de la société La Perla Romana et à Me Ricard, avocat de la société Yacht Club International de Saint-Laurent-du-Var ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que, par un acte du 6 juillet 2011, la société Yacht Club International de Saint-Laurent-du-Var, concessionnaire du port de plaisance de Saint-Laurent-du-Var, a consenti à la société La Perla Romana un contrat d'amodiation portant sur l'occupation et l'exploitation des cellules commerciales nos 91 à 94 du port ; que, par courrier du 22 octobre 2014, la société concessionnaire a notifié à la société La Perla Romana la résiliation de ce contrat, pour non-paiement des redevances d'occupation ; que la société La Perla Romana a été placée en procédure de sauvegarde par un jugement du tribunal de commerce d'Antibes du 25 novembre 2014 ; que, par l'ordonnance attaquée du 6 juillet 2015, le juge des référés du tribunal administratif de Nice, saisi par la société Yacht Club International de Saint-Laurent-du-Var sur le fondement de l'article L. 521-3 du code de justice administrative, a enjoint à la société La Perla Romana de libérer les cellules commerciales qu'elle occupait, dans un délai d'un mois à compter de la notification de l'ordonnance, sous astreinte de 500 euros par jour de retard à compter de l'expiration de ce délai ;<br/>
<br/>
              2. Considérant, en premier lieu, qu'aux termes de l'article L. 622-21 du code de commerce : " I.- Le jugement d'ouverture interrompt ou interdit toute action en justice de la part de tous les créanciers dont la créance n'est pas mentionnée au I de l'article L. 622-17 et tendant : / 1° A la condamnation du débiteur au paiement d'une somme d'argent ; / 2° A la résolution d'un contrat pour défaut de paiement d'une somme d'argent. / II.- Il arrête ou interdit également toute procédure d'exécution de la part de ces créanciers tant sur les meubles que sur les immeubles ainsi que toute procédure de distribution n'ayant pas produit un effet attributif avant le jugement d'ouverture (...) " ; que si ces dispositions fixent le principe de la suspension ou de l'interdiction, à compter du jugement d'ouverture de la procédure de sauvegarde, de toute action en justice tendant au paiement d'une somme d'argent ou à la résolution d'un contrat pour défaut de paiement d'une somme d'argent, de la part de tous les créanciers autres que ceux détenteurs d'une créance postérieure privilégiée, elles ne comportent aucune dérogation aux dispositions régissant les compétences respectives des juridictions administratives et judiciaires ; qu'elles sont, en tout état de cause, sans influence sur la compétence du juge administratif pour se prononcer sur des conclusions tendant à l'expulsion d'un occupant irrégulier du domaine public, dès lors que celles-ci ne sont entachées d'aucune irrecevabilité au regard des dispositions dont l'appréciation relève de la juridiction administrative ; qu'il résulte de ce qui précède que le juge des référés n'a pas commis d'erreur de droit en jugeant que les dispositions de l'article L. 622-21 du code de commerce ne faisaient pas obstacle à ce qu'il statue sur les conclusions de la société Yacht Club International de Saint-Laurent-du-Var ;<br/>
<br/>
              3. Considérant, en deuxième lieu, que le moyen tiré de ce que la décision de résiliation du contrat d'amodiation ne serait pas devenue définitive, faute d'avoir comporté la mention des voies et délais de recours, est, en tout état de cause, nouveau en cassation et, par suite, sans influence sur le bien-fondé de l'ordonnance attaquée ;<br/>
<br/>
              4. Considérant, en troisième lieu, qu'il résulte de ce qui est dit au point 2 que le moyen tiré de ce que le juge des référés aurait commis une erreur de droit en jugeant que la demande ne se heurtait à aucune contestation sérieuse, alors qu'il existait une contestation sérieuse sur l'application de l'article L. 622-21 du code de commerce, ne peut qu'être écarté ;<br/>
<br/>
              5. Considérant, enfin, qu'en jugeant que la demande présentait un caractère d'urgence, eu égard au montant de 93 503 euros des redevances d'occupation impayées et à l'existence d'un repreneur des locaux qui était en possession des cent-huit actions de la société concessionnaire ouvrant droit à la jouissance des cellules commerciales nos 91 à 94, le juge des référés, qui a suffisamment motivé son ordonnance, n'a pas commis d'erreur de droit ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que la société La Perla Romana n'est pas fondée à demander l'annulation de l'ordonnance qu'elle attaque ;<br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société Yacht Club International de Saint-Laurent-du-Var qui n'est pas, dans la présente instance, la partie perdante ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées au même titre par la société Yacht Club International de Saint-Laurent-du-Var ; <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société La Perla Romana est rejeté. <br/>
Article 2 : Les conclusions présentées par la société Yacht Club International de Saint-Laurent-du-Var au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à la société à responsabilité limitée La Perla Romana et à la société Yacht Club International de Saint-Laurent-du-Var.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-03-01-01 COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE LES DEUX ORDRES DE JURIDICTION. COMPÉTENCE DÉTERMINÉE PAR DES TEXTES SPÉCIAUX. ATTRIBUTIONS LÉGALES DE COMPÉTENCE AU PROFIT DES JURIDICTIONS ADMINISTRATIVES. - EXPULSION D'UN OCCUPANT IRRÉGULIER DU DOMAINE PUBLIC - CAS D'UNE SOCIÉTÉ FAISANT L'OBJET D'UNE PROCÉDURE COLLECTIVE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">24-01-03-02 DOMAINE. DOMAINE PUBLIC. PROTECTION DU DOMAINE. PROTECTION CONTRE LES OCCUPATIONS IRRÉGULIÈRES. - EXPULSION DU DOMAINE PUBLIC D'UNE SOCIÉTÉ FAISANT L'OBJET D'UNE PROCÉDURE COLLECTIVE - COMPÉTENCE DU JUGE ADMINISTRATIF  [RJ1].
</SCT>
<ANA ID="9A"> 17-03-01-01 Si l'article L. 622-21 du code de commerce fixe le principe de la suspension ou de l'interdiction, à compter du jugement d'ouverture de la procédure de sauvegarde, de toute action en justice tendant au paiement d'une somme d'argent ou à la résolution d'un contrat pour défaut de paiement d'une somme d'argent, de la part de tous les créanciers autres que ceux détenteurs d'une créance postérieure privilégiée, il ne comporte aucune dérogation aux dispositions régissant les compétences respectives des juridictions administratives et judiciaires. Ces dispositions sont, en tout état de cause, sans influence sur la compétence du juge administratif pour se prononcer sur des conclusions tendant à l'expulsion d'un occupant irrégulier du domaine public, dès lors que celles-ci ne sont entachées d'aucune irrecevabilité au regard des dispositions dont l'appréciation relève de la juridiction administrative.</ANA>
<ANA ID="9B"> 24-01-03-02 Si l'article L. 622-21 du code de commerce fixe le principe de la suspension ou de l'interdiction, à compter du jugement d'ouverture de la procédure de sauvegarde, de toute action en justice tendant au paiement d'une somme d'argent ou à la résolution d'un contrat pour défaut de paiement d'une somme d'argent, de la part de tous les créanciers autres que ceux détenteurs d'une créance postérieure privilégiée, il ne comporte aucune dérogation aux dispositions régissant les compétences respectives des juridictions administratives et judiciaires. Ces dispositions sont, en tout état de cause, sans influence sur la compétence du juge administratif pour se prononcer sur des conclusions tendant à l'expulsion d'un occupant irrégulier du domaine public, dès lors que celles-ci ne sont entachées d'aucune irrecevabilité au regard des dispositions dont l'appréciation relève de la juridiction administrative.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Rappr., sur l'existence d'une contravention de grande voirie et son montant, CE, Section, 3 février 1978, Sieur,, n° 1008, p. 48 ; sur l'existence et le montant d'une créance vis à vis d'un entrepreneur de travaux public, CE, avis, 20 janvier 1992, Société Jules Viaux et flis, n° 130250, p. 31.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
