<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000022512963</ID>
<ANCIEN_ID>JG_L_2010_07_000000318932</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/22/51/29/CETATEXT000022512963.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 23/07/2010, 318932</TITRE>
<DATE_DEC>2010-07-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>318932</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Arrighi de Casanova</PRESIDENT>
<AVOCATS/>
<RAPPORTEUR>M. Michel  Thenault</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Roger-Lacan Cyril</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 29 juillet et 27 octobre 2008 au secrétariat du contentieux du Conseil d'Etat, présentés pour la FEDERATION NATIONALE DES GUIDES-INTERPRETES, dont le siège est 43 rue Beaubourg à Paris (75003) ; la fédération requérante demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir les dispositions de l'article 7 de l'ordonnance n° 2008-507 du 30 mai 2008 portant transposition de la directive 2005/36/CE du Parlement européen et du Conseil du 7 septembre 2005 relative à la reconnaissance des qualifications professionnelles ;<br/>
<br/>
              2°) à titre subsidiaire, de saisir à titre préjudiciel la Cour de justice des Communautés européennes de la question de la validité de la directive 2005/36/CE au regard des règles des articles 46 et 49 du traité instituant la Communauté européenne ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 5000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son article 34 ; <br/>
<br/>
              Vu le traité instituant la Communauté européenne ;<br/>
<br/>
              Vu la directive 2005/36/CE du 7 septembre 2005 ;<br/>
<br/>
              Vu le code du tourisme ;<br/>
<br/>
               Vu la loi n° 2007-1774 du 17 décembre 2007 ;<br/>
<br/>
               Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Michel Thenault, Conseiller d'Etat,  <br/>
<br/>
<br/>
<br/>
              - les conclusions de M. Cyril Roger-Lacan, rapporteur public ;<br/>
<br/>
<br/>
<br/>
              Considérant que l'article 7 de l'ordonnance du 30 mai 2008 portant transposition de la directive 2005/36/CE du Parlement européen et du Conseil du 7 septembre 2005 relative à la reconnaissance des qualifications professionnelles, dont la FEDERATION NATIONALE DES GUIDES-INTERPRETES demande l'annulation pour excès de pouvoir, introduit dans le chapitre unique du titre II du livre II du code du tourisme une section 2 relative à  la liberté d'établissement , comportant un article L. 221-2 aux termes duquel :  Pour s'établir en France, est considéré comme qualifié pour la conduite des visites commentées dans les musées et les monuments historiques dans les conditions prévues à l'article L. 221-1 tout ressortissant d'un Etat membre de la Communauté européenne ou d'un Etat partie à l'accord sur l'Espace économique européen, dès lors qu'il produit les pièces justificatives émanant de l'autorité compétente d'un de ces Etats prouvant qu'il possède la qualification conforme aux conditions de reconnaissance fixées par décret en Conseil d'Etat pour y exercer la profession de guide-interprète ou de conférencier (...)  ; qu'il ajoute par ailleurs une section 3 relative à  la libre prestation de services , comportant notamment un article L. 221-3 aux termes duquel :  Tout ressortissant d'un Etat membre de la Communauté européenne ou d'un autre Etat partie à l'Espace économique européen, légalement établi, pour l'exercice de la profession de guide-interprète ou de conférencier, dans un de ces Etats, peut exercer cette profession de façon temporaire et occasionnelle en France. / Toutefois, lorsque la profession de guide-interprète ou de conférencier ou la formation y conduisant n'est pas réglementée dans l'Etat d'établissement, le prestataire doit avoir exercé cette profession dans cet Etat pendant au moins deux ans au cours des dix années qui précèdent la prestation.  ;<br/>
<br/>
              Considérant en premier lieu, que, si l'article L. 221-3 exonère de l'obligation de produire une attestation des qualifications professionnelles les guides-interprètes ou conférenciers, ressortissants des Etats qu'il vise, lorsqu'ils interviennent dans le cadre du régime de la libre prestation de services, alors que l'article L. 221-1 soumet ceux qui souhaitent s'établir en France à un régime plus strict de contrôle des qualifications, cette différence de traitement trouve sa justification dans la différence de situation, au regard de l'objet du texte, entre les guides-interprètes ou conférenciers, selon qu'ils sont établis en  France, où ils exercent une activité habituelle et régulière, ou que, légalement établis dans l'un de ces Etats, ils assurent des prestations de services temporaires et occasionnelles sur le territoire national ; que, par suite, le moyen tiré de la méconnaissance du principe d'égalité doit être écarté ; qu'il en va de même, par voie de conséquence, de celui tiré de la méconnaissance des règles de la libre concurrence, qui n'est d'ailleurs assorti d'aucune autre précision ;<br/>
<br/>
              Considérant, en deuxième lieu, qu'aux termes de l'article 7 de la directive du 7 septembre 2005 :  1. Les États membres peuvent exiger que, lorsque le prestataire se déplace d'un État membre à l'autre pour la première fois pour fournir des services, il en informe préalablement l'autorité compétente de l'État membre d'accueil par une déclaration écrite (...) / 2. En outre, lors de la première prestation de services ou en cas de changement matériel relatif à la situation établie par les documents, les États membres peuvent exiger que la déclaration soit accompagnée des documents suivants: (...) / c) une preuve des qualifications professionnelles ; (...)  ; que l'article 6 de la loi d'habilitation du 17 décembre 2007 prévoit que le Gouvernement veille, dans la transposition de cette directive,  à justifier très précisément toute levée des options en matière de libre prestation de service  ; que si l'ordonnance attaquée ne fait pas usage de la faculté ouverte par la directive d'imposer des exigences formelles supplémentaires aux prestations de services des guides-interprètes ou conférenciers, cette circonstance est, en tout état de cause, sans incidence sur la légalité de la différence de traitement dont se plaint la requérante ; que, le Gouvernement n'ayant pas utilisé les options ouvertes par la directive, le moyen tiré de ce que l'ordonnance aurait été prise en méconnaissance des dispositions de l'article 6 de la loi du 17 décembre 2007 ne saurait être utilement invoqué ;<br/>
<br/>
              Considérant, en troisième lieu, que si le nouvel article L. 221-2 du code du tourisme prévoit que les conditions de reconnaissance des qualifications professionnelles des ressortissants d'un Etat membre de la Communauté européenne désireux de s'établir en France sont fixées par décret en Conseil d'Etat, ces dispositions ne renvoient pas au pouvoir règlementaire l'édiction de mesures qui relèveraient de la compétence du législateur en vertu de l'article 34 de la Constitution ; que, par suite, le moyen tiré de ce que l'ordonnance renverrait ainsi illégalement au pouvoir réglementaire doit être écarté ; <br/>
<br/>
              Considérant, en quatrième lieu, qu'aucune disposition ni aucun principe général du droit n'imposait l'édiction d'une réglementation plus restrictive quant à l'exercice de la profession de guide-interprète et de conférencier par les ressortissants d'un Etat membre de la Communauté européenne ; qu'il suit de là que la circonstance que le droit communautaire ne ferait pas obstacle, le cas échéant, à l'édiction d'une telle réglementation est, en tout état de cause, sans incidence sur la légalité de l'ordonnance attaquée ; que doit, de même, être écarté, sans qu'il y ait lieu de saisir la Cour de justice de l'Union européenne d'une question préjudicielle, le moyen mettant en cause, à titre subsidiaire, la validité de la directive 2005/36/CE du 7 septembre 2005 en ce qu'elle ferait obstacle à la prise en compte de motifs d'ordre public ou de raisons impérieuses d'intérêt général qui, selon la requérante, auraient dû conduire à adopter une réglementation plus restrictive  ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que la FEDERATION NATIONALE DES GUIDES-INTERPRETES n'est pas fondée à demander l'annulation de l'article 7 de l'ordonnance attaquée ; que ses conclusions présentées en application des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>
<br/>
      D E C I D E :<br/>
      --------------<br/>
Article 1er : La requête de la FEDERATION NATIONALE DES GUIDES-INTERPRETES est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la FEDERATION NATIONALE DES GUIDES-INTERPRETES, au Premier ministre et à la ministre de l'économie, de l'industrie et de l'emploi.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. ÉGALITÉ DEVANT LA LOI. - VIOLATION - ABSENCE - ABSENCE DE DISCRIMINATION À REBOURS INTRODUITE PAR DES DISPOSITIONS TENDANT À METTRE LE DROIT NATIONAL EN CONFORMITÉ AVEC LES EXIGENCES DE LA LIBRE PRESTATION DE SERVICES (ORDONNANCE DU 30 MAI 2008) [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">55-005-01 PROFESSIONS, CHARGES ET OFFICES. - GUIDES-INTERPRÈTES OU CONFÉRENCIERS - DISCRIMINATION À REBOURS INTRODUITE PAR L'ORDONNANCE DU 30 MAI 2008 - ABSENCE [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">55-02 PROFESSIONS, CHARGES ET OFFICES. ACCÈS AUX PROFESSIONS. - GUIDE-INTERPRÈTE OU CONFÉRENCIER - DISCRIMINATION À REBOURS INTRODUITE PAR DES DISPOSITIONS TENDANT À METTRE LE DROIT NATIONAL EN CONFORMITÉ AVEC LES EXIGENCES DE LA LIBRE PRESTATION DE SERVICES (ORDONNANCE DU 30 MAI 2008) - ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 01-04-03-01 Ordonnance n° 2008-507 du 30 mai 2008 portant transposition de la directive 2005/36/CE du 7 septembre 2005 relative à la reconnaissance des qualifications professionnelles. Demande d'annulation de l'article 7 de l'ordonnance, introduisant au code du tourisme les articles L. 221-2 et L. 221-3, permettant l'exercice en France de sa profession à tout guide-interprète qualifié ressortissant d'un Etat membre de la Communauté européenne ou d'un Etat partie à l'accord sur l'Espace économique européen. L'article L. 221-3 exonère de l'obligation de produire une attestation des qualifications professionnelles les guides-interprètes ou conférenciers, ressortissants des Etats qu'il vise, lorsqu'ils interviennent dans le cadre du régime de la libre prestation de services, alors que l'article L. 221-1 soumet ceux qui souhaitent s'établir en France à un régime plus strict de contrôle des qualifications. Cette différence de traitement trouve sa justification dans la différence de situation, au regard de l'objet du texte, entre les guides-interprètes ou conférenciers, selon qu'ils sont établis en France, où ils exercent une activité habituelle et régulière, ou que, légalement établis dans l'un de ces Etats, ils assurent des prestations de services temporaires et occasionnelles sur le territoire national. Le moyen tiré de la méconnaissance du principe d'égalité doit donc être écarté.</ANA>
<ANA ID="9B"> 55-005-01 Ordonnance n° 2008-507 du 30 mai 2008 portant transposition de la directive 2005/36/CE du 7 septembre 2005 relative à la reconnaissance des qualifications professionnelles. Demande d'annulation de l'article 7 de l'ordonnance, introduisant au code du tourisme les articles L. 221-2 et L. 221-3, permettant l'exercice en France de sa profession à tout guide-interprète qualifié ressortissant d'un Etat membre de la Communauté européenne ou d'un Etat partie à l'accord sur l'Espace économique européen. L'article L. 221-3 exonère de l'obligation de produire une attestation des qualifications professionnelles les guides-interprètes ou conférenciers, ressortissants des Etats qu'il vise, lorsqu'ils interviennent dans le cadre du régime de la libre prestation de services, alors que l'article L. 221-1 soumet ceux qui souhaitent s'établir en France à un régime plus strict de contrôle des qualifications. Cette différence de traitement trouve sa justification dans la différence de situation, au regard de l'objet du texte, entre les guides-interprètes ou conférenciers, selon qu'ils sont établis en France, où ils exercent une activité habituelle et régulière, ou que, légalement établis dans l'un de ces Etats, ils assurent des prestations de services temporaires et occasionnelles sur le territoire national. Le moyen tiré de la méconnaissance du principe d'égalité doit donc être écarté.</ANA>
<ANA ID="9C"> 55-02 Ordonnance n° 2008-507 du 30 mai 2008 portant transposition de la directive 2005/36/CE du 7 septembre 2005 relative à la reconnaissance des qualifications professionnelles. Demande d'annulation de l'article 7 de l'ordonnance, introduisant au code du tourisme les articles L. 221-2 et L. 221-3, permettant l'exercice en France de sa profession à tout guide-interprète qualifié ressortissant d'un Etat membre de la Communauté européenne ou d'un Etat partie à l'accord sur l'Espace économique européen. L'article L. 221-3 exonère de l'obligation de produire une attestation des qualifications professionnelles les guides-interprètes ou conférenciers, ressortissants des Etats qu'il vise, lorsqu'ils interviennent dans le cadre du régime de la libre prestation de services, alors que l'article L. 221-1 soumet ceux qui souhaitent s'établir en France à un régime plus strict de contrôle des qualifications. Cette différence de traitement trouve sa justification dans la différence de situation, au regard de l'objet du texte, entre les guides-interprètes ou conférenciers, selon qu'ils sont établis en France, où ils exercent une activité habituelle et régulière, ou que, légalement établis dans l'un de ces Etats, ils assurent des prestations de services temporaires et occasionnelles sur le territoire national. Le moyen tiré de la méconnaissance du principe d'égalité doit donc être écarté.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. sol. contr. 6 octobre 2008, Compagnie des architectes en chef des monuments historiques et autres et Association Architectes du patrimoine, n° 311080, p. 341.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
