<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043763558</ID>
<ANCIEN_ID>JG_L_2021_07_000000433537</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/76/35/CETATEXT000043763558.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 05/07/2021, 433537</TITRE>
<DATE_DEC>2021-07-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>433537</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC/>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOUTET-HOURDEAUX ; SCP DELAMARRE, JEHANNIN</AVOCATS>
<RAPPORTEUR>M. Mathieu  Le Coq</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:433537.20210705</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. C... D... a demandé au tribunal administratif de Lyon, d'une part, de déclarer inexistantes ou, à titre subsidiaire, d'annuler les délibérations du 30 janvier 2015 par lesquelles le conseil municipal de Messimy-sur-Saône (Ain) a accordé la protection fonctionnelle à Mme E... B..., maire de la commune, et à MM. Marc du Verne, Philippe Brunel, Pascal Clerc et Vincent Gelas anciens maires, ainsi que la décision du 5 mars 2015 par laquelle le maire de cette commune a rejeté son recours gracieux contre ces délibérations, d'autre part, de condamner la commune de Messimy-sur-Saône à lui verser une indemnité de 5 000 euros en réparation des préjudices qu'il estime avoir subi du fait de ces délibérations. M. D... a également demandé au tribunal administratif de Lyon d'annuler la délibération du 31 juillet 2015 par laquelle le conseil municipal de Messimy-sur-Saône a désigné M. A..., deuxième adjoint, pour ester en justice et désigné un cabinet d'avocats pour assurer la défense des intérêts de la commune devant la juridiction administrative. Par un jugement nos 1504754, 1504792 du 14 juin 2018, le tribunal administratif de Lyon a rejeté ces demandes.<br/>
<br/>
              Par une ordonnance n°18LY04439 du 26 février 2019, le président de la 3ème chambre de la cour administrative d'appel de Lyon a rejeté l'appel formé par M. D... contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire, un mémoire en réplique et un nouveau mémoire, enregistrés le 12 août 2019, le 8 novembre 2019, le 10 mars 2020 et le 21 août 2020 au secrétariat du contentieux du Conseil d'Etat, M. D... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Messimy-sur-Saône la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Mathieu Le Coq, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Delamarre, Jéhannin, avocat de M. D... et à la SCP Boutet-Hourdeaux, avocat de la commune de Messimy-sur-Saône ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. C... D..., conseiller municipal de Messimy-sur-Saône (Ain) a assigné devant le tribunal de grande instance de Bourg-en-Bresse, pour des faits d'entraves discriminatoires, MM. du Verne, Brunel, Clerc et Gelas, anciens maires de cette commune et Mme B..., maire en exercice. Par cinq délibérations du 30 janvier 2015, le conseil municipal de la commune de Messimy-sur-Saône, estimant que les faits qui leur étaient reprochés n'étaient pas détachables de l'exercice de leurs mandats, leur a accordé le bénéfice de la protection fonctionnelle et décidé de prendre en charge les honoraires de leur avocat. M. D... a formé un recours gracieux à l'encontre de ces délibérations, qui a été rejeté par courrier de la maire en date du 5 mars 2015. M. D... a attaqué ces délibérations, ainsi que le rejet de son recours gracieux, devant le tribunal administratif de Lyon, qui a rejeté ses demandes par un jugement du 14 juin 2018. Il se pourvoit en cassation contre l'ordonnance du 26 février 2019 par laquelle le président de la 3ème chambre de la cour administrative d'appel de Lyon a rejeté son appel contre ce jugement.<br/>
<br/>
              2. Aux termes du neuvième alinéa de l'article R. 222-1 du code de justice administrative : " Les présidents des cours administratives d'appel, (...) les présidents des formations de jugement des cours (...) peuvent, en outre, par ordonnance, rejeter (...) après l'expiration du délai de recours ou, lorsqu'un mémoire complémentaire a été annoncé, après la production de ce mémoire les requêtes d'appel manifestement dépourvues de fondement ".<br/>
<br/>
              3. Lorsqu'un agent public est mis en cause par un tiers à raison de ses fonctions, il incombe à la collectivité publique dont il dépend de lui accorder sa protection dans le cadre d'une instance civile non seulement en le couvrant des condamnations civiles prononcées contre lui mais aussi en prenant en charge l'ensemble des frais de cette instance, dans la mesure où une faute personnelle détachable du service ne lui est pas imputable. De même, il lui incombe de lui accorder sa protection dans le cas où il fait l'objet de poursuites pénales, sauf s'il a commis une faute personnelle, et, à moins qu'un motif d'intérêt général ne s'y oppose, de le protéger contre les menaces, violences, voies de fait, injures, diffamations ou outrages dont il est l'objet. Ce principe général du droit a d'ailleurs été expressément réaffirmé par la loi, notamment en ce qui concerne les fonctionnaires et agents non titulaires, par l'article 11 de la loi du 13 juillet 1983 portant statut général de la fonction publique et par les articles L. 2123-34, L. 2123-35, L. 3123-28, L. 3123-29, L. 4135-28 et L. 4135-29 du code général des collectivités territoriales, s'agissant des exécutifs des collectivités territoriales. Cette protection s'applique à tous les agents publics, quel que soit le mode d'accès à leurs fonctions.<br/>
<br/>
              4. En premier lieu, il ressort des pièces du dossier soumis aux juges du fond que les délibérations du 30 janvier 2015 de la commune de Messimy-sur-Saône avaient pour objet d'assurer la prise en charge des frais que MM. du Verne, Brunel, Clerc et Gelas et Mme B... étaient susceptibles d'engager pour assurer leur défense devant les juridictions civiles. Il résulte de ce qui a été dit au point 3 ci-dessus qu'en jugeant que l'attribution de la protection par la collectivité publique constitue une obligation lorsque l'agent fait l'objet de poursuites pénales ou d'une action civile, en l'absence de faute personnelle qui lui est imputable, le président de la 3ème chambre de la cour administrative d'appel n'a pas commis d'erreur de droit. <br/>
<br/>
              5. En deuxième lieu, c'est par une motivation suffisante et par un motif exempt d'erreur de droit que le président de la 3ème chambre de la cour administrative d'appel a écarté le moyen tiré de ce que les actes reprochés à MM. du Verne, Brunel, Clerc et Gelas et à Mme B... étaient détachables de l'exercice de leurs fonctions.<br/>
<br/>
              6. En troisième lieu, c'est sans commettre d'erreur de droit que le président de la 3ème chambre de la cour administrative d'appel a jugé que la commune pouvait légalement accorder sa protection sans qu'une demande écrite formalisée lui soit adressée par les bénéficiaires. <br/>
<br/>
              7. En quatrième lieu, aux termes de l'article L. 2121-10 du code général des collectivités territoriales alors en vigueur : " Toute convocation est faite par le maire. Elle indique les questions portées à l'ordre du jour. Elle est mentionnée au registre des délibérations, affichée ou publiée. Elle est adressée par écrit, sous quelque forme que ce soit, au domicile des conseillers municipaux, sauf s'ils font le choix d'une autre adresse ". Les mentions des délibérations font foi jusqu'à preuve du contraire. A défaut de toute précision apportée par le requérant, c'est sans commettre d'erreur de droit et par une ordonnance suffisamment motivée que le président de la 3ème chambre de la cour administrative d'appel a écarté le moyen tiré de ce que les conseillers municipaux n'avaient pas été convoqués conformément aux dispositions de l'article L. 2121-10 cité ci-dessus.<br/>
<br/>
              8. En cinquième lieu, aux termes de l'article L. 2121-12 du code général des collectivités territoriales : " la communication obligatoire d'une notice explicative ne s'applique qu'aux communes de plus de 3 500 habitants". Aux termes de l'article L. 2121-13 du même code : " Tout membre du conseil municipal a le droit, dans le cadre de sa fonction, d'être informé des affaires de la commune qui font l'objet d'une délibération ". Il résulte de ces dispositions que l'obligation de communiquer une notice explicative ne s'applique pas à la commune de Messimy-sur-Saône, qui compte moins de 3 500 habitants et il ne ressort pas des pièces du dossier soumis aux juges du fond qu'un membre du conseil municipal ait, à la réception de l'ordre du jour de la convocation en litige, fait valoir son droit à être informé plus précisément des sujets qui y figuraient. Dès lors, le président de la 3ème chambre de la cour administrative d'appel n'a pas entaché son ordonnance d'erreur de droit en écartant le moyen tiré d'une insuffisante information des membres du conseil municipal.   <br/>
<br/>
              9. En sixième lieu, aux termes du deuxième alinéa de l'article L. 2121-20 du code général des collectivités territoriales, " Les délibérations sont prises à la majorité absolue des suffrages exprimés ". Il résulte de ces dispositions que le conseil municipal doit, en principe, se prononcer par un vote formel ou donner son assentiment sur chaque projet de délibération. Toutefois, s'il est constant que les cinq délibérations attaquées ont été adoptées au terme d'un vote unique du conseil municipal, elles avaient pour objet commun d'accorder la protection de la commune à la maire en exercice et à quatre anciens maires et il ne ressort pas des pièces du dossier soumis aux juges du fond qu'un conseiller municipal ait demandé que le conseil municipal se prononce séparément sur chaque projet de délibération. Dans ces conditions, le président de la 3ème chambre de la cour administrative d'appel a pu, sans erreur de droit, écarter le moyen tiré de ce qu'elles avaient été irrégulièrement adoptées. <br/>
<br/>
              10. Il résulte de ce qui précède que le président de la 3ème chambre de la cour administrative d'appel de Lyon a pu faire, sans abus, usage de la faculté que lui reconnaissent les dispositions précitées de l'article R. 222-1 du code de justice administrative de rejeter par ordonnance la requête de M. D....<br/>
<br/>
              11. Il résulte de tout ce qui précède que le pourvoi de M. D... doit être rejeté, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. D... une somme de 3 500 euros à verser à la commune de Messimy-sur-Saône, au titre des mêmes dispositions. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. D... est rejeté. <br/>
Article 2 : M. D... versera à la commune de Messimy-sur-Saône une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : La présente décision sera notifiée à M. C... D... et à la commune de Messimy-sur-Saône.<br/>
Copie en sera adressée à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-02-01-02-01-01-02 COLLECTIVITÉS TERRITORIALES. COMMUNE. ORGANISATION DE LA COMMUNE. ORGANES DE LA COMMUNE. CONSEIL MUNICIPAL. FONCTIONNEMENT. DÉROULEMENT DES SÉANCES. - VOTE OU ASSENTIMENT SUR CHAQUE PROJET DISTINCT DE DÉLIBÉRATION (ART. L. 2121-20 DU CGCT) - EXCEPTION - PROJETS AYANT UN OBJET COMMUN, EN L'ABSENCE DE DEMANDE CONTRAIRE.
</SCT>
<ANA ID="9A"> 135-02-01-02-01-01-02 Il résulte de l'article L. 2121-20 du code général des collectivités territoriales (CGCT) que le conseil municipal doit, en principe, se prononcer par un vote formel ou donner son assentiment sur chaque projet de délibération.,,Toutefois, des délibérations ayant un objet commun, si aucun conseiller municipal ne demande que le conseil municipal se prononce séparément sur chaque projet de délibération, peuvent être régulièrement adoptées au terme d'un vote unique du conseil municipal.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
