<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033965898</ID>
<ANCIEN_ID>JG_L_2017_01_000000394206</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/96/58/CETATEXT000033965898.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 30/01/2017, 394206</TITRE>
<DATE_DEC>2017-01-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>394206</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>HAAS ; OCCHIPINTI ; SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR>M. Camille Pascal</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:394206.20170130</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Marseille d'annuler, pour excès de pouvoir, l'arrêté du 28 mars 2014 par lequel le maire de la commune <br/>
d'Aix-en-Provence a accordé un permis de construire à M. et Mme C...en vue de la modification de la toiture et des façades d'une maison individuelle et la construction d'un garage. Par une ordonnance n° 1405334 du 30 septembre 2015, la présidente de la 2ème chambre du tribunal administratif de Marseille a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 23 octobre 2015, 19 janvier 2016 et 23 décembre 2016 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) de mettre à la charge de la commune d'Aix-en-Provence la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Camille Pascal, conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de M.B..., à Me Haas, avocat de la commune d'Aix-en-Provence et à Me Occhipinti, avocat de M. et Mme C...; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant que, pour demander l'annulation de l'ordonnance qu'il attaque, M. B...soutient qu'elle est irrégulière dès lors qu'elle a été rendue, en méconnaissance du principe d'impartialité, par un magistrat qui s'était déjà prononcé sur le litige ; <br/>
<br/>
              2.	Considérant qu'aux termes de l'article L. 511-1 du code de justice administrative : " Le juge des référés statue par des mesures qui présentent un caractère provisoire. Il n'est pas saisi du principal et se prononce dans les meilleurs délais " ; qu'aux termes de l'article L. 521-1 du même code : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision./ Lorsque la suspension est prononcée, il est statué sur la requête en annulation ou en réformation de la décision dans les meilleurs délais " ;<br/>
<br/>
              3.	Considérant que saisi sur le fondement de l'article L. 521-1 du code de justice administrative d'une demande tendant à ce qu'il prononce, à titre provisoire et conservatoire, la suspension d'une décision administrative, le juge des référés procède, dans les plus brefs délais, à une instruction succincte - distincte de celle au vu de laquelle le juge saisi du principal statuera - pour apprécier si les préjudices que l'exécution de cette décision pourrait entraîner sont suffisamment graves et immédiats pour caractériser une situation d'urgence et si les moyens invoqués apparaissent, en cet état de l'instruction, de nature à faire naître un doute sérieux sur la légalité de la décision ; qu'il se prononce par une ordonnance qui n'est pas revêtue de l'autorité de la chose jugée et dont il peut lui-même modifier la portée au vu d'un élément nouveau invoqué devant lui par toute personne intéressée ;<br/>
<br/>
              4.	Considérant qu'eu égard à la nature de l'office ainsi attribué au juge des référés la seule circonstance qu'un magistrat a statué sur une demande tendant à la suspension de l'exécution d'une décision administrative n'est pas, par elle-même, de nature à faire obstacle à ce qu'il se prononce ultérieurement sur la requête en qualité de juge du principal ; que, toutefois, dans le cas où il apparaîtrait, compte tenu notamment des termes mêmes de l'ordonnance, qu'un magistrat statuant comme juge des référés aurait préjugé l'issue du litige, ce magistrat ne pourrait, sans méconnaître le principe d'impartialité, se prononcer ultérieurement comme juge du principal ; <br/>
<br/>
              5.	Considérant qu'il ressort des pièces du dossier soumis au juge du fond que, par un arrêté du 28 mars 2014, le maire d'Aix-en-Provence a autorisé M. et MmeC..., propriétaires d'une parcelle bâtie située 21 avenue Jules Ferry, à modifier la maison construite sur cette parcelle ; que M.B..., propriétaire d'un immeuble mitoyen situé sur une autre parcelle, a saisi le juge des référés du tribunal administratif de Marseille sur le fondement de l'article L. 521-1 du code de justice administrative d'une demande de  suspension du permis de construire ; qu'il a, en outre, saisi le tribunal d'une demande au fond tendant à l'annulation pour excès de pouvoir du permis de construire ; que, par une ordonnance du 13 août 2014, la présidente de la 2ème chambre du tribunal, statuant comme juge des référés, a rejeté la demande de suspension en raison de la tardiveté de la requête au fond ; que, par ordonnance du 30 septembre 2015, prise en application du 4° de l'article R. 222-1 du code de justice administrative, la même présidente a rejeté pour irrecevabilité manifeste la demande au fond en raison de sa tardiveté ;<br/>
<br/>
              6.	Considérant que l'ordonnance de référé se prononce sur le caractère régulier de l'affichage du permis de construire sur le terrain au regard des articles R. 424-15 et A. 424-18 du code de l'urbanisme et en déduit  que le délai de recours de deux mois était expiré à la date d'enregistrement de la requête au fond ; que le juge des référés a, ainsi, statué sur la question de la tardiveté de cette dernière requête et préjugé l'issue du litige ; que, dès lors, le requérant est fondé à soutenir que, le juge du fond étant le même magistrat que le juge des référés, l'ordonnance du 30 septembre 2015 contestée par le présent pourvoi a été rendue dans des conditions irrégulières ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, M. B...est fondé à en demander l'annulation ; <br/>
<br/>
              7.	Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond par application de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              8.	Considérant qu'aux termes de l'article R. 600-2 du code de l'urbanisme : " Le délai de recours contentieux à l'encontre d'une décision de non-opposition à une déclaration préalable ou d'un permis de construire, d'aménager ou de démolir court à l'égard des tiers à compter du premier jour d'une période continue de deux mois d'affichage sur le terrain des pièces mentionnées à l'article R. 424-15 " ; qu'aux termes de l'article R. 424-15 du même code : " Mention du permis explicite ou tacite ou de la déclaration préalable doit être affichée sur le terrain, de manière visible de l'extérieur, par les soins de son bénéficiaire, dès la notification de l'arrêté ou dès la date à laquelle le permis tacite ou la décision de non-opposition à la déclaration préalable est acquis et pendant toute la durée du chantier " ; que l'article A. 424-18 du même code précise, enfin, que : " Le panneau d'affichage doit être installé de telle sorte que les renseignements qu'il contient demeurent lisibles de la voie publique ou des espaces ouverts au public pendant toute la durée du chantier" ;<br/>
<br/>
              9.	Considérant qu'il ressort des pièces du dossier et notamment du constat d'huissier établi le 8 avril 2014 que mention du permis de construire délivré le 28 mars 2014 par le maire d'Aix-en-Provence à M. et Mme C...a été affiché sur le portail de leur propriété de façon continue pendant deux mois à compter du 8 avril 2014 ; qu'il était visible de l'extérieur et lisible depuis un espace ouvert au public dans les conditions conformes aux exigences des articles R. 424-15 et A. 424-18 du code de l'urbanisme ; qu'il en résulte que le délai de recours de deux mois mentionné à l'article R. 600-2 du code de l'urbanisme, qui a couru au plus tard à compter du 8 avril 2014 et n'a pas été suspendu par un recours administratif, était déjà expiré à la date du 24 juillet 2014 à laquelle la demande de M. B...tendant à l'annulation du permis a été enregistrée au greffe du tribunal administratif de Marseille ; que, dès lors, cette demande était tardive et, par suite, irrecevable ;<br/>
<br/>
              10.	Considérant qu'il résulte de ce qui précède que la demande de M. B... devant le tribunal administratif de Marseille doit être rejetée ;<br/>
<br/>
              11.	Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de la commune <br/>
d'Aix-en-Provence, qui n'est pas la partie perdante dans la présente instance ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. B...le versement à M. et Mme C..., d'une part, et à la commune d'Aix-en-Provence, d'autre part, de la somme de 1 500 euros chacun ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'ordonnance de la présidente de la 2ème chambre du tribunal administratif de Marseille du 30 septembre 2015 est annulée.<br/>
<br/>
Article 2 : La demande présentée par M. B...devant le tribunal administratif de Marseille est rejetée.<br/>
Article 3 : Le surplus des conclusions du pourvoi est rejeté.<br/>
<br/>
Article 4 : M. B...versera à M. et MmeC..., d'une part, et à la commune d'Aix-en-Provence, d'autre part, la somme de 1 500 euros chacun au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 5 : La présente décision sera notifiée à M. A...B..., à M. et Mme C...et à la commune d'Aix-en-Provence. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">37-03-05 JURIDICTIONS ADMINISTRATIVES ET JUDICIAIRES. RÈGLES GÉNÉRALES DE PROCÉDURE. COMPOSITION DES JURIDICTIONS. - MAGISTRAT AYANT STATUÉ SUR UNE DEMANDE TENDANT À LA SUSPENSION DE L'EXÉCUTION D'UN ACTE ADMINISTRATIF (ART. L. 521-1 DU CJA) - CIRCONSTANCE FAISANT OBSTACLE À CE QU'IL SE PRONONCE EN QUALITÉ DE JUGE DU PRINCIPAL - CAS OÙ LE JUGE DES RÉFÉRÉS S'EST PRONONCÉ SUR LA TARDIVETÉ DE LA REQUÊTE AU FOND - EXISTENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-035-02 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ SUSPENSION (ART. L. 521-1 DU CODE DE JUSTICE ADMINISTRATIVE). - MAGISTRAT AYANT STATUÉ SUR UNE DEMANDE TENDANT À LA SUSPENSION DE L'EXÉCUTION D'UN ACTE ADMINISTRATIF (ART. L. 521-1 DU CJA) - CIRCONSTANCE FAISANT OBSTACLE À CE QU'IL SE PRONONCE EN QUALITÉ DE JUGE DU PRINCIPAL - CAS OÙ LE JUGE DES RÉFÉRÉS S'EST PRONONCÉ SUR LA TARDIVETÉ DE LA REQUÊTE AU FOND - EXISTENCE [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-035-02-04 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ SUSPENSION (ART. L. 521-1 DU CODE DE JUSTICE ADMINISTRATIVE). POUVOIRS ET DEVOIRS DU JUGE. - MAGISTRAT AYANT STATUÉ SUR UNE DEMANDE TENDANT À LA SUSPENSION DE L'EXÉCUTION D'UN ACTE ADMINISTRATIF (ART. L. 521-1 DU CJA) - CIRCONSTANCE FAISANT OBSTACLE À CE QU'IL SE PRONONCE EN QUALITÉ DE JUGE DU PRINCIPAL - CAS OÙ LE JUGE DES RÉFÉRÉS S'EST PRONONCÉ SUR LA TARDIVETÉ DE LA REQUÊTE AU FOND - EXISTENCE [RJ1].
</SCT>
<ANA ID="9A"> 37-03-05 Juge des référés statuant sur le fondement de l'article L. 521-1 du code de justice administrative (CJA) ayant rejeté une demande de suspension d'un permis de construire par une ordonnance se prononçant sur le caractère régulier de l'affichage du permis de construire sur le terrain au regard des articles R. 424-15 et A. 424-18 du code de l'urbanisme et en déduisant que le délai de recours de deux mois était expiré à la date d'enregistrement de la requête au fond.,,,Le juge des référés a, ainsi, statué sur la question de la tardiveté de cette dernière requête et préjugé l'issue du litige. Dès lors, le requérant est fondé à soutenir que, le juge du fond étant le même magistrat que le juge des référés, l'ordonnance attaquée a été rendue dans des conditions irrégulières.</ANA>
<ANA ID="9B"> 54-035-02 Juge des référés statuant sur le fondement de l'article L. 521-1 du code de justice administrative (CJA) ayant rejeté une demande de suspension d'un permis de construire par une ordonnance se prononçant sur le caractère régulier de l'affichage du permis de construire sur le terrain au regard des articles R. 424-15 et A. 424-18 du code de l'urbanisme et en déduisant que le délai de recours de deux mois était expiré à la date d'enregistrement de la requête au fond.,,,Le juge des référés a, ainsi, statué sur la question de la tardiveté de cette dernière requête et préjugé l'issue du litige. Dès lors, le requérant est fondé à soutenir que, le juge du fond étant le même magistrat que le juge des référés, l'ordonnance attaquée a été rendue dans des conditions irrégulières.</ANA>
<ANA ID="9C"> 54-035-02-04 Juge des référés statuant sur le fondement de l'article L. 521-1 du code de justice administrative (CJA) ayant rejeté une demande de suspension d'un permis de construire par une ordonnance se prononçant sur le caractère régulier de l'affichage du permis de construire sur le terrain au regard des articles R. 424-15 et A. 424-18 du code de l'urbanisme et en déduisant que le délai de recours de deux mois était expiré à la date d'enregistrement de la requête au fond.,,,Le juge des référés a, ainsi, statué sur la question de la tardiveté de cette dernière requête et préjugé l'issue du litige. Dès lors, le requérant est fondé à soutenir que, le juge du fond étant le même magistrat que le juge des référés, l'ordonnance attaquée a été rendue dans des conditions irrégulières.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Comp., sur le principe, CE, Section, avis, 12 mai 2004, Commune de Rogerville, n° 265184, p. 223.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
