<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028938228</ID>
<ANCIEN_ID>JG_L_2007_02_000000297084</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/93/82/CETATEXT000028938228.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère / 6ème SSR, 26/02/2007, 297084</TITRE>
<DATE_DEC>2007-02-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>297084</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère / 6ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Eric  Berti</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Devys</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2007:297084.20070226</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance en date 29 août 2006, enregistrée le 4 septembre 2006 au secrétariat du contentieux du Conseil d'Etat, par laquelle le président du tribunal administratif de Paris a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la demande présentée à ce tribunal par Mlle A...B..., demeurant... ;<br/>
<br/>
<br/>
              Vu la requête, enregistrée le 4 avril 2006 au greffe du tribunal administratif de Paris le 4 avril 2006, présentée par Mlle B... et tendant :<br/>
<br/>
              1°) à l'annulation pour excès de pouvoir de l'arrêté du 24 janvier 2006 du ministre de la santé et des solidarités et du ministre délégué à la sécurité sociale, aux personnes âgées, aux personnes handicapées et à la famille, portant approbation des avenants n°s 10 et 11 à la convention nationale des médecins généralistes et des médecins spécialistes ;<br/>
<br/>
              2°) à l'annulation pour excès de pouvoir de l'arrêté du 3 février 2005 du ministre des solidarités, de la santé et de la famille et du secrétaire d'Etat à l'assurance maladie portant approbation de la convention nationale des médecins généralistes et des médecins spécialistes ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son préambule et son article 62 ;<br/>
<br/>
              Vu le code de la sécurité sociale ;<br/>
<br/>
              Vu le code de la santé publique ;<br/>
<br/>
              Vu la décision n° 2004-504 DC du 12 août 2004 du Conseil constitutionnel ;<br/>
<br/>
              Vu le décret n° 2005-1369 du 3 novembre 2005 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
- le rapport de M. Eric Berti, chargé des fonctions de Maître des requêtes, <br/>
              - les conclusions de M. Christophe Devys, Commissaire du gouvernement ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Sur les conclusions dirigées contre l'arrêté du 3 février 2005 :<br/>
<br/>
              Considérant que cet arrêté a été publié au Journal officiel de la République française le 11 février 2005 ; que, dès lors, Mlle B... dont la requête n'a été enregistrée que le 4 avril 2006, soit après l'expiration du délai de recours contentieux, n'est pas recevable à en demander l'annulation ; <br/>
<br/>
              Sur les conclusions dirigées contre l'arrêté du 24 janvier 2006 :<br/>
<br/>
              Considérant que l'arrêté du 24 janvier 2006 a approuvé l'avenant n° 10 à la convention nationale des médecins généralistes et des médecins spécialistes qui a modifié, pour certains patients, les conditions d'accès aux médecins psychiatres dans le cadre du " parcours de soins coordonnés " et prévoit en particulier que " les soins réalisés par les psychiatres et neuropsychiatres qui sont en accès spécifique au sens de l'article 1.3.1 de la convention médicale sont les soins de psychiatrie prodigués aux patients de moins de 26 ans (...) " ;<br/>
<br/>
              Considérant, en premier lieu, qu'aux termes du premier alinéa de l'article L. 162-5 du code de la sécurité sociale : " Les rapports entre les organismes d'assurance maladie et les médecins sont définis par des conventions nationales conclues séparément pour les médecins généralistes et les médecins spécialistes, par l'Union nationale des caisses d'assurance maladie et une ou plusieurs organisations syndicales les plus représentatives pour l'ensemble du territoire de médecins généralistes ou de médecins spécialistes ou par une convention nationale conclue par l'Union nationale des caisses d'assurance maladie et au moins une organisation syndicale représentative pour l'ensemble du territoire de médecins généralistes et une organisation syndicale représentative pour l'ensemble du territoire de médecins spécialistes " ;<br/>
<br/>
              Considérant qu'il n'est pas contesté que la Confédération des syndicats médicaux français et le Syndicat des médecins libéraux, tous deux signataires de la convention, sont des organisations représentatives tant des médecins généralistes que des médecins spécialistes ; qu'il en est de même pour l'Alliance intersyndicale des médecins indépendants de France, autre signataire de ce texte, qui est une organisation représentative des médecins spécialistes ; qu'il ne ressort ni des dispositions de l'article L. 162-5 du code de la sécurité sociale, ni d'aucune autre disposition législative que les textes conventionnels doivent être négociés et signés par une ou plusieurs organisations syndicales représentatives de spécialités médicales lorsque, comme en l'espèce, ces spécialités font l'objet de stipulations particulières ; que le moyen tiré du défaut de représentativité des signataires de la convention en matière de psychiatrie doit donc être écarté ;<br/>
<br/>
              Considérant, en deuxième lieu, d'une part, que le décret du 3 novembre 2005, dans ses dispositions insérées à l'article D. 162-1-7 du code de la sécurité sociale, a fixé les cas dans lesquels, en application du cinquième alinéa de l'article L. 165-5-3 du même code, la majoration de la participation de l'assuré, dite " ticket modérateur " n'est pas appliquée lorsque ce dernier consulte, sans prescription de son médecin traitant, les médecins relevant de certaines spécialités, dont la psychiatrie et la neuropsychiatrie ;<br/>
<br/>
              Considérant, d'autre part, que les dispositions des 17° et 18° de l'article L. 162-5 du code de la sécurité sociale donnent compétence aux partenaires conventionnels pour déterminer les modalités de l'organisation de la coordination des soins et notamment les dépassements d'honoraires pouvant être appliqués aux patients ne respectant pas le parcours de soins coordonnés ; qu'il appartient à ce titre à la convention de déterminer les cas où l'assuré social doit passer au préalable par son médecin traitant pour être ensuite adressé à un médecin spécialiste si sa pathologie le nécessite et ceux dans lesquels l'assuré social peut accéder directement à certaines spécialités sans s'exposer à une majoration de tarif ; que, dès lors, les partenaires conventionnels n'ont pas excédé leur compétence en décidant, par l'avenant n° 10 à la convention nationale des médecins généralistes et des médecins spécialistes, approuvé par l'arrêté attaqué du 24 janvier 2006, que relevaient d'un accès spécifique, dans le domaine de la psychiatrie et de la neuropsychiatrie, les seuls soins prodigués aux patients de moins de 26 ans ;<br/>
<br/>
              Considérant que la circonstance qu'un patient de plus de 26 ans, ne respectant pas le parcours de soins en consultant directement un psychiatre, ne subira aucune majoration du ticket modérateur en application de l'article D. 162-1-7 du code de la sécurité sociale précité mais pourra cependant se voir appliquer par son médecin un dépassement d'honoraires dans une limite de 17,5 % du tarif en application du 18° de l'article L. 162-5 du code de la sécurité sociale, est sans incidence sur la légalité des stipulations de l'avenant n° 10, dès lors qu'il ne résulte ni des dispositions mentionnées plus haut du code de la sécurité sociale, ni d'aucune autre disposition législative que la majoration de ticket modérateur et les dépassements d'honoraires devraient dans tous les cas être d'application simultanée ;<br/>
<br/>
              Considérant en troisième lieu qu'il ressort des pièces du dossier et notamment des études menées à la demande de la haute autorité de santé qu'il existe une plus grande vulnérabilité chez les patients de moins de 26 ans aux troubles de nature psychiatrique, trouvant fréquemment leur genèse dans l'existence de difficultés familiales ; qu'en faisant bénéficier de l'accès direct au médecin psychiatre les adolescents et les jeunes adultes de moins de vingt-six ans, qui se trouvent dans une situation objective différente de celle des autres adultes, lesquels ne bénéficient pour leur part d'un accès spécifique au médecin psychiatre qu'à titre dérogatoire, les partenaires conventionnels n'ont pas instauré une différence de traitement manifestement disproportionnée au regard des différences de leurs situations ; que le moyen tiré de la méconnaissance du principe d'égalité doit donc être écarté ;<br/>
<br/>
              Considérant, en quatrième lieu, que la suppression de l'accès direct aux médecins psychiatres pour les patients de plus de vingt-six ans les conduit, ainsi que le relève la requérante, soit à consulter préalablement le médecin traitant en vue d'une prescription éventuelle de la consultation du médecin psychiatre, ce qui augmente la participation forfaitaire due par le patient pour chaque consultation,  hormis les cas d'exonération prévus par les articles L. 322-2 et L. 331-2 du code de la sécurité sociale, soit à s'exposer à un dépassement d'honoraires s'ils décident de consulter directement un médecin psychiatre ; que cependant cette suppression, qui répond à l'objectif de réduction du déséquilibre financier de l'assurance maladie, ne méconnaît pas les dispositions du onzième alinéa du Préambule de la Constitution de 1946 garantissant à tous la protection de la santé, dès lors notamment que l'éventuel dépassement d'honoraires pouvant en résulter s'exerce dans des limites qui assurent le respect des dispositions de l'article L. 162-2-1 du même code  qui imposent aux médecins de respecter, dans leurs actes et prescriptions, " la plus stricte économie compatible avec la qualité, la sécurité et l'efficacité des soins " ; que, pour les mêmes motifs, le moyen tiré de " l'atteinte aux biens " qui résulterait, selon la requérante, de la suppression de l'accès direct aux médecins psychiatres pour les patients de plus de vingt-six ans ne peut, en tout état de cause, qu'être écarté ;<br/>
<br/>
              Considérant, enfin, que le détournement de pouvoir allégué par la requérante n'est pas établi ;<br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que, sans qu'il soit besoin de statuer sur la fin de non-recevoir opposée par le ministre, la requête de Mlle B... doit être rejetée ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de Mlle B...est rejetée.<br/>
Article 2 : La présente décision sera notifiée à Mlle A...B...et au ministre de la santé et des solidarités.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">62-02-01-01 SÉCURITÉ SOCIALE. RELATIONS AVEC LES PROFESSIONS ET LES ÉTABLISSEMENTS SANITAIRES. RELATIONS AVEC LES PROFESSIONS DE SANTÉ. MÉDECINS. - ORGANISATIONS SYNDICALES HABILITÉES À SIGNER UNE CONVENTION COMPORTANT DES STIPULATIONS PARTICULIÈRES À CERTAINES SPÉCIALITÉS - INCLUSION - ORGANISATIONS REPRÉSENTATIVES DES MÉDECINS GÉNÉRALISTES ET DES MÉDECINS SPÉCIALISTES.
</SCT>
<ANA ID="9A"> 62-02-01-01 Il ne ressort ni des dispositions de l'article L. 162-5 du code de la sécurité sociale, ni d'aucune autre disposition législative que les textes conventionnels doivent être négociés et signés par une ou plusieurs organisations syndicales représentatives de spécialités médicales lorsque ces spécialités font l'objet, comme en l'espèce, de stipulations particulières. Par suite, des organisations représentatives tant des médecins généralistes que des médecins spécialistes peuvent valablement signer une convention en matière de psychiatrie.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
