<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030286076</ID>
<ANCIEN_ID>JG_L_2015_02_000000375724</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/28/60/CETATEXT000030286076.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème / 1ère SSR, 25/02/2015, 375724</TITRE>
<DATE_DEC>2015-02-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>375724</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème / 1ère SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Mireille Le Corre</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:375724.20150225</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 24 février 2014 au secrétariat du contentieux du Conseil d'Etat, présentée par M. A...B..., domicilié ...; M. B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite par laquelle le Premier ministre a rejeté sa demande d'abrogation de l'alinéa premier de l'article 9 de l'annexe à l'article R. 57-6-18 du code de procédure pénale et des premier et deuxième alinéas de l'article 30 de l'annexe à l'article R. 57-6-18 du code de procédure pénale ;<br/>
<br/>
              2°) d'enjoindre au Premier ministre d'abroger l'alinéa premier de l'article 9 de l'annexe à l'article R. 57-6-18 du code de procédure pénale et les premier et deuxième alinéas de l'article 30 de l'annexe à l'article R. 57-6-18 du code de procédure pénale ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule et son article 61-1 ; <br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, notamment ses articles 8 et 9 ;<br/>
<br/>
              Vu le pacte international relatif aux droits civils et politiques ;<br/>
<br/>
              Vu la charte des droits fondamentaux de l'Union européenne ; <br/>
<br/>
              Vu l'ordonnance n° 58 -1067 du 7 novembre 1958 ; <br/>
<br/>
              Vu le code de procédure pénale ;<br/>
<br/>
              Vu la loi n° 2009-1436 du 24 novembre 2009 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Mireille Le Corre, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article 9 du règlement type des établissements pénitentiaires annexé à l'article R. 57-6-18 du code de procédure pénale, relatif à " l'alimentation " des personnes détenues : " Chaque personne détenue reçoit une alimentation variée, bien préparée et présentée, répondant tant en ce qui concerne la qualité que la quantité aux règles de la diététique et de l'hygiène, compte tenu de son âge, de son état de santé, de la nature de son travail et, dans toute la mesure du possible, de ses convictions philosophiques ou religieuses " ; qu'aux termes des premier et deuxième alinéas de l'article 30 du même règlement, relatif au " maintien des liens familiaux " : " Sur autorisation du chef d'établissement, les personnes détenues peuvent faire envoyer aux membres de leur famille des sommes figurant à la part disponible de leur compte nominatif. / Les personnes détenues peuvent recevoir des subsides en argent des personnes titulaires d'un permis permanent de visite ou autorisées par le chef d'établissement. Cette faculté s'exerce dans les conditions déterminées par arrêté du garde des sceaux, ministre de la justice. La destination à donner à ces subsides est réglée conformément aux dispositions applicables à la tenue du compte nominatif " ; que M. B...a saisi le Premier ministre d'une demande tendant à l'abrogation de ces dispositions ; qu'il demande au Conseil d'Etat d'annuler pour excès de pouvoir la décision implicite de refus née du silence gardé par le Premier ministre sur cette demande ; <br/>
<br/>
              Sur les conclusions tendant à l'abrogation du premier alinéa de l'article 9 du règlement intérieur type des établissements pénitentiaires :<br/>
<br/>
              2. Considérant qu'aux termes de l'article 9 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " 1. Toute personne a droit à la liberté de pensée, de conscience et de religion ; ce droit implique la liberté de changer de religion ou de conviction, ainsi que la liberté de manifester sa religion ou sa conviction individuellement ou collectivement, en public ou en privé, par le culte, l'enseignement, les pratiques et l'accomplissement des rites./ 2. La liberté de manifester sa religion ou ses convictions ne peut faire l'objet d'autres restrictions que celles qui, prévues par la loi, constituent des mesures nécessaires, dans une société démocratique, à la sécurité publique, à la protection de l'ordre, de la santé ou de la morale publiques, ou à la protection des droits et libertés d'autrui " ; qu'aux termes de l'article 18 du pacte international relatif aux droits civils et politiques : " 1. Toute personne a droit à la liberté de pensée, de conscience et de religion ; ce droit implique la liberté d'avoir ou d'adopter une religion ou une conviction de son choix, ainsi que la liberté de manifester sa religion ou sa conviction, individuellement ou en commun, tant en public qu'en privé, par le culte et l'accomplissement des rites, les pratiques et l'enseignement. / 2. Nul ne subira de contrainte pouvant porter atteinte à sa liberté d'avoir ou d'adopter une religion ou une conviction de son choix. / 3. La liberté de manifester sa religion ou ses convictions ne peut faire l'objet que des seules restrictions prévues par la loi et qui sont nécessaires à la protection de la sécurité, de l'ordre et de la santé publique, ou de la morale et des libertés et droits fondamentaux d'autrui. (...) " ; <br/>
<br/>
              3. Considérant qu'aux termes de l'article 22 de la loi du 24 novembre 2009 pénitentiaire : " L'administration pénitentiaire garantit à toute personne détenue le respect de sa dignité et de ses droits. L'exercice de ceux-ci ne peut faire l'objet d'autres restrictions que celles résultant des contraintes inhérentes à la détention, du maintien de la sécurité et du bon ordre des établissements, de la prévention de la récidive et de la protection de l'intérêt des victimes. Ces restrictions tiennent compte de l'âge, de l'état de santé, du handicap et de la personnalité de la personne détenue " ; qu'aux termes de l'article 26 de cette même loi : " Les personnes détenues ont droit à la liberté d'opinion, de conscience et de religion. Elles peuvent exercer le culte de leur choix, selon les conditions adaptées à l'organisation des lieux, sans autres limites que celles imposées par la sécurité et le bon ordre de l'établissement " ; qu'aux termes du premier alinéa de l'article R. 57-9-3 du code de procédure pénale : " Chaque personne détenue doit pouvoir satisfaire aux exigences de sa vie religieuse, morale ou spirituelle " ; <br/>
<br/>
              4. Considérant, en premier lieu, que si l'observation de prescriptions alimentaires peut être regardée comme une manifestation directe de croyances et pratiques religieuses au sens de l'article 9 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, les dispositions critiquées, qui visent à permettre l'exercice par les personnes détenues de leurs convictions religieuses en matière d'alimentation sans toutefois imposer à l'administration de garantir, en toute circonstance, une alimentation respectant ces convictions, ne peuvent être regardées, eu égard à l'objectif d'intérêt général du maintien du bon ordre des établissements pénitentiaires et aux contraintes matérielles propres à la gestion de ces établissements, comme portant une atteinte excessive au droit de ces derniers de pratiquer leur religion ; que, dès lors, le moyen tiré de ce qu'elles méconnaîtraient les stipulations de l'article 9 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales doit être écarté ; qu'il en va de même du moyen tiré de ce qu'elles méconnaîtraient les stipulations de l'article 18 du pacte international relatif aux droits civils et politiques ;<br/>
<br/>
              5. Considérant, en deuxième lieu, que le requérant n'est pas fondé, pour les mêmes raisons, à soutenir que les dispositions qu'il critique méconnaîtraient les droits et garanties prévus par les dispositions des articles 22 et 26 de la loi pénitentiaire du 24 novembre 2009, ni, en tout état de cause, celles de l'article R. 57-9-3 du code de procédure pénale ;<br/>
<br/>
              6. Considérant, en troisième lieu, qu'aux termes de l 'article 51 de la charte des droits fondamentaux de l'Union européenne : " 1. Les dispositions de la présente Charte s'adressent aux institutions, organes et organismes de l'Union dans le respect du principe de subsidiarité, ainsi qu'aux Etats membres uniquement lorsqu'ils mettent en oeuvre le droit de l'Union (...) " ; que le moyen tiré de la méconnaissance de l'article 10 de la charte est inopérant, dès lors que les dispositions critiquées ne mettent pas en oeuvre le droit de l'Union ;<br/>
<br/>
              Sur les conclusions tendant à l'abrogation des premier et deuxième alinéas de l'article 30 du règlement intérieur type des établissements pénitentiaires :<br/>
<br/>
              En ce qui concerne la question prioritaire de constitutionnalité :<br/>
<br/>
              7. Considérant qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) " ; qu'il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ; <br/>
<br/>
              8. Considérant qu'aux termes de l'article 728 du code de procédure pénale : " Des règlements intérieurs types, prévus par décret en Conseil d'Etat, déterminent les dispositions prises pour le fonctionnement de chacune des catégories d'établissements pénitentiaires " ; que M. B...soutient que ces dispositions, en ce qu'elles permettent de renvoyer au pouvoir réglementaire le soin de déterminer les modalités d'organisation des liens financiers entre la personne détenue et les membres de sa famille, méconnaissent la compétence confiée au législateur par l'article 34 de la Constitution pour fixer les règles de conciliation du droit à mener une vie familiale normale, qui résulte du dixième alinéa du Préambule de la Constitution de 1946, avec les exigences de la sauvegarde de l'ordre public ; <br/>
<br/>
              9. Considérant qu'aux termes de l'article 728-1 du code de procédure pénale : " I. - Les valeurs pécuniaires des détenus, inscrites à un compte nominatif ouvert à l'établissement pénitentiaire, sont divisées en trois parts : la première sur laquelle seules les parties civiles et les créanciers d'aliments peuvent faire valoir leurs droits ; la deuxième, affectée au pécule de libération, qui ne peut faire l'objet d'aucune voie d'exécution ; la troisième, laissée à la libre disposition des détenus. (...) " ;<br/>
<br/>
              10. Considérant que si l'article 728 habilite le pouvoir réglementaire à déterminer les dispositions relatives au fonctionnement des établissements pénitentiaires, il résulte de la combinaison des dispositions de l'article 728-1 du code de procédure pénale citées au point 9 et de l'article 22 de la loi pénitentiaire du 24 novembre 2009 citées au point 3, que les chefs d'établissement pénitentiaires ne peuvent restreindre les transferts, vers leur famille, des sommes dont les personnes détenues ont la libre disposition que lorsqu'elles sont justifiées par les contraintes inhérentes à la détention, le maintien de la sécurité et du bon ordre des établissements, la prévention de la récidive et la protection de l'intérêt des victimes ; que, par suite, le moyen tiré de ce que le législateur aurait méconnu l'étendue de sa compétence en s'abstenant d'assurer une conciliation équilibrée entre l'objectif de sauvegarde de l'ordre public dans les établissements pénitentiaires et le droit au respect de la vie privée et familiale dont bénéficient, compte tenu des contraintes inhérentes à la détention, les personnes détenues, ne présente pas un caractère sérieux ; qu'ainsi, il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ;  <br/>
<br/>
              En ce qui concerne les autres moyens :<br/>
<br/>
              11. Considérant qu'aux termes de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " 1. Toute personne a droit au respect de sa vie privée et familiale, de son domicile et de sa correspondance. / 2. Il ne peut y avoir ingérence d'une autorité publique dans l'exercice de ce droit que pour autant que cette ingérence est prévue par la loi et qu'elle constitue une mesure qui, dans une société démocratique, est nécessaire à la sécurité nationale, à la sûreté publique, au bien être économique du pays, à la défense de l'ordre et à la prévention des infractions pénales, à la protection de la santé ou de la morale, ou à la protection des droits et libertés d'autrui " ; <br/>
<br/>
              12. Considérant que ni les dispositions critiquées du premier alinéa de l'article 30 du règlement intérieur type des établissements pénitentiaires, en ce qu'elles soumettent à l'autorisation du chef d'établissement la possibilité pour les personnes détenues d'envoyer des sommes d'argent aux membres de leur famille, ni celles du deuxième alinéa de ce même article, en ce qu'elles limitent la possibilité pour les personnes détenues de recevoir des subsides en argent de la part des seules personnes titulaires d'un permis de visite ou sur autorisation du chef d'établissement, ne peuvent être regardées, dès lors que les restrictions qu'elles autorisent ne sont permises que si elles sont justifiées par les contraintes inhérentes à la détention, le maintien de la sécurité et du bon ordre des établissements, la prévention de la récidive et la protection de l'intérêt des victimes, comme portant une atteinte excessive au droit de ces derniers au respect de leur vie privée et familiale ; que, dès lors, le moyen tiré de ce que ces dispositions méconnaîtraient les stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales doit être écarté ; <br/>
<br/>
              13. Considérant qu'il résulte de tout ce qui précède que M. B...n'est pas fondé à demander l'annulation de la décision implicite qu'il attaque ; qu'en conséquence, ses conclusions à fin d'injonction et celles présentées au titre de l'article L. 761-1 du code de justice administrative doivent être rejetées ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M.B....<br/>
<br/>
      Article 2 : La requête de M. B...est rejetée.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. A...B..., au Premier ministre et à la garde des sceaux, ministre de la justice. <br/>
      Copie en sera adressée pour information au Conseil constitutionnel.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-055-01-09 DROITS CIVILS ET INDIVIDUELS. CONVENTION EUROPÉENNE DES DROITS DE L'HOMME. DROITS GARANTIS PAR LA CONVENTION. - MÉCONNAISSANCE - ABSENCE - OBLIGATION FAITE À L'ADMINISTRATION PÉNITENTIAIRE D'ASSURER UNE ALIMENTATION DES DÉTENUS CONFORME, DANS TOUTE LA MESURE DU POSSIBLE, À LEURS CONVICTIONS PHILOSOPHIQUES ET RELIGIEUSES.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">37-05-02-01 JURIDICTIONS ADMINISTRATIVES ET JUDICIAIRES. EXÉCUTION DES JUGEMENTS. EXÉCUTION DES PEINES. SERVICE PUBLIC PÉNITENTIAIRE. - 1) OBLIGATION FAITE À L'ADMINISTRATION PÉNITENTIAIRE D'ASSURER UNE ALIMENTATION DES DÉTENUS CONFORME, DANS TOUTE LA MESURE DU POSSIBLE, À LEURS CONVICTIONS PHILOSOPHIQUES ET RELIGIEUSES - MÉCONNAISSANCE DE L'ARTICLE 9 DE LA CONVENTION EDH - ABSENCE - 2) TRANSFERTS D'ARGENT DES DÉTENUS VERS LEUR FAMILLE - CONDITION DE LÉGALITÉ DES RESTRICTIONS APPORTÉES PAR LES CHEFS D'ÉTABLISSEMENT.
</SCT>
<ANA ID="9A"> 26-055-01-09 Si l'observation de prescriptions alimentaires peut être regardée comme une manifestation directe de croyances et pratiques religieuses au sens de l'article 9 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales (convention EDH), les dispositions du règlement type des établissements pénitentiaires annexé à l'article R. 57-6-18 du code de procédure pénale, qui visent à permettre l'exercice par les personnes détenues de leurs convictions religieuses en matière d'alimentation sans toutefois imposer à l'administration de garantir, en toute circonstance, une alimentation respectant ces convictions, ne peuvent être regardées, eu égard à l'objectif d'intérêt général du maintien du bon ordre et de la sécurité des établissements pénitentiaires et aux contraintes matérielles propres à la gestion de ces établissements, comme portant une atteinte excessive au droit des personnes détenues de pratiquer leur religion. Elles ne sont par suite pas incompatibles avec les stipulations de l'article 9 de la convention EDH.</ANA>
<ANA ID="9B"> 37-05-02-01 1) Si l'observation de prescriptions alimentaires peut être regardée comme une manifestation directe de croyances et pratiques religieuses au sens de l'article 9 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales (convention EDH), les dispositions du règlement type des établissements pénitentiaires annexé à l'article R. 57-6-18 du code de procédure pénale, qui visent à permettre l'exercice par les personnes détenues de leurs convictions religieuses en matière d'alimentation sans toutefois imposer à l'administration de garantir, en toute circonstance, une alimentation respectant ces convictions, ne peuvent être regardées, eu égard à l'objectif d'intérêt général du maintien du bon ordre et de la sécurité des établissements pénitentiaires et aux contraintes matérielles propres à la gestion de ces établissements, comme portant une atteinte excessive au droit des personnes détenues de pratiquer leur religion. Elles ne sont par suite pas incompatibles avec les stipulations de l'article 9 de la convention EDH.,,,2) Si l'article 728 du code de procédure pénale habilite le pouvoir réglementaire à déterminer les dispositions relatives au fonctionnement des établissements pénitentiaires, il résulte de la  combinaison des dispositions de l'article 728-1 du code de procédure pénale et de l'article 22 de la loi n° 2009-1436 du 24 novembre 2009 pénitentiaire que les chefs d'établissement pénitentiaires ne peuvent restreindre les transferts, vers leur famille, des sommes dont les détenus ont la libre disposition que lorsque ces restrictions sont justifiées par les contraintes inhérentes à la détention, au maintien de la sécurité et du bon ordre des établissements, à la prévention de la récidive et à la protection de l'intérêt des victimes.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
