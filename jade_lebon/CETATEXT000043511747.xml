<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043511747</ID>
<ANCIEN_ID>JG_L_2021_05_000000447953</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/51/17/CETATEXT000043511747.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 18/05/2021, 447953</TITRE>
<DATE_DEC>2021-05-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>447953</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BOUTHORS</AVOCATS>
<RAPPORTEUR>M. Guillaume Leforestier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Marc Pichon de Vendeuil</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:447953.20210518</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A... B... a demandé au tribunal administratif de Melun d'annuler partiellement l'arrêté du ministre de l'agriculture, de l'agroalimentaire et de la forêt du 1er juin 2015 la nommant ingénieur stagiaire de l'agriculture et de l'environnement, ainsi que la décision implicite de rejet de son recours gracieux, en tant que ces décisions l'ont classée à l'échelon 4 du corps des ingénieurs de l'agriculture et de l'environnement, à l'indice brut 492 correspondant à l'indice nouveau majoré 425, et d'enjoindre au ministre de la reclasser en tenant compte, au titre de sa rémunération antérieure de référence en qualité d'agent contractuel, d'une rémunération à taux plein et non à taux partiel, et en y intégrant le solde de la prime spéciale de résultats brut perçu en juillet 2014. Par un jugement n° 1509222 du 14 novembre 2017, le tribunal a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 18PA00186 du 22 octobre 2020, la cour administrative d'appel de Paris a, sur appel de Mme B..., annulé les décisions attaquées en tant qu'elles fixent à l'indice brut 492 le traitement personnel de l'intéressée et enjoint au ministre de fixer son traitement de façon à ce qu'il corresponde effectivement à l'indice le plus proche de celui qui lui permet d'obtenir un traitement mensuel brut égal à 70 % de sa rémunération mensuelle antérieure, en prenant en compte le solde de la prime de service et de résultats versé en juillet 2014.<br/>
<br/>
              Par un pourvoi, enregistré le 18 décembre 2020 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'agriculture et de l'alimentation demande au Conseil d'Etat d'annuler cet arrêt.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le décret n° 2006-1827 du 23 décembre 2006 ;<br/>
              - l'arrêté du 29 juin 2007 fixant le pourcentage et les éléments de rémunération pris en compte pour le maintien partiel de la rémunération de certains agents non titulaires accédant à un corps soumis aux dispositions du décret n° 2006-1827 du 23 décembre 2006 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Leforestier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Marc Pichon de Vendeuil, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à Me Bouthors, avocat de Mme B... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme B..., agent contractuel de l'Office national des forêts employée à temps partiel à 80 %, a été admise au concours réservé d'accès des agents non titulaires au corps des ingénieurs de l'agriculture et de l'environnement. Par un arrêté du 1er juin 2015, le ministre de l'agriculture a nommé Mme B... ingénieur de l'agriculture et de l'environnement stagiaire à compter de cette même date, en la classant au 4ème échelon de ce corps et en fixant son traitement à l'indice brut 492. Par un second arrêté du même jour, Mme B... a été autorisée à poursuivre son activité à temps partiel à 80 %. Par un jugement du 14 novembre 2017, le tribunal administratif de Melun a rejeté la demande de Mme B... tendant à l'annulation de l'arrêté du 1er juin 2015 en tant qu'il fixait sa rémunération à l'indice brut 492 et de la décision implicite de rejet de son recours gracieux. Par un arrêt du 22 octobre 2020, contre lequel le ministre de l'agriculture et de l'alimentation se pourvoit en cassation, la cour administrative d'appel de Paris, après avoir annulé ce jugement, a fait droit à cette demande et a enjoint au ministre de l'agriculture et de l'alimentation de fixer le traitement personnel de Mme B... de façon à ce qu'il corresponde effectivement à l'indice le plus proche de celui qui permet à l'intéressée d'obtenir un traitement mensuel brut égal à 70% de sa rémunération mensuelle antérieure, en prenant en compte le solde de la prime de service et de résultats versé en juillet 2014 dans la rémunération antérieure servant de référence pour le calcul du traitement.<br/>
<br/>
              2. Aux termes du I de l'article 7 du décret du 23 décembre 2006 relatif aux règles du classement d'échelon consécutif à la nomination dans certains corps de catégorie A de la fonction publique de l'Etat : " Les agents qui justifient de services d'ancien fonctionnaire civil, de services en tant qu'agent d'une organisation internationale intergouvernementale ou de services d'agent public non titulaire, autres que des services accomplis en qualité d'élève ou de stagiaire, sont classés à un échelon déterminé en prenant en compte une fraction de leur ancienneté de services publics civils (...) ". Aux termes du II de l'article 12 de ce décret : " Les agents qui avaient, avant leur nomination, la qualité d'agent non titulaire de droit public et qui sont classés en application de l'article 7 à un échelon doté d'un traitement dont le montant est inférieur à celui de la rémunération qu'ils percevaient avant leur nomination conservent à titre personnel le bénéfice d'un traitement représentant une fraction conservée de leur rémunération antérieure, jusqu'au jour où ils bénéficient dans leur nouveau grade d'un traitement au moins égal au montant ainsi déterminé (...) ".<br/>
<br/>
              3. Pour l'application de ces dispositions, l'article 1er de l'arrêté du 29 juin 2007 visé ci-dessus dispose que : " Le traitement maintenu, à titre personnel, en application du II de l'article 12 du décret du 23 décembre 2006 susvisé est celui qui correspond à l'indice majoré le plus proche de celui qui permet à l'intéressé d'obtenir un traitement mensuel brut égal à 70 % de sa rémunération mensuelle antérieure ". Aux termes de l'article 2 de cet arrêté : " La rémunération mensuelle antérieure prise en compte pour l'application de l'article 1er est la moyenne des six meilleures rémunérations mensuelles perçues par l'agent dans son dernier emploi, au cours de la période de douze mois précédant la nomination dans un corps de catégorie A (...) ".<br/>
<br/>
              4. Il résulte des dispositions citées aux point 2 et 3, dont l'objet est de garantir une rémunération minimale aux agents titularisés dans certains corps de catégorie A de la fonction publique de l'Etat, qu'à quotité de travail inchangée, le traitement brut effectivement perçu par un agent postérieurement à sa titularisation ne peut être inférieur à 70 % de la rémunération moyenne mensuelle brute effectivement perçue avant cette titularisation, calculée sur la base des six meilleures rémunérations mensuelles perçues par l'agent dans son dernier emploi au cours de la période de douze mois précédant sa titularisation. C'est dès lors au prix d'une erreur de droit que la cour administrative d'appel de Paris a retenu que ces dispositions méconnaissaient le principe d'égalité en ne prenant pas en compte la situation des agents exerçant leurs fonctions à temps partiel et en ne leur assurant pas le montant de rémunération minimal qu'elles prévoient. Son arrêt doit, par suite, être annulé pour ce motif, sans qu'il soit besoin de se prononcer sur l'autre moyen du pourvoi.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              6. En premier lieu, l'article 2 de l'arrêté du 29 juin 2007, cité au point 2, prévoit que, à quotité de travail inchangé, la rémunération mensuelle antérieure à prendre en compte est égale à la moyenne des six meilleures rémunérations mensuelles perçues par l'agent dans son dernier emploi, au cours de la période de douze mois précédant la nomination dans un corps de catégorie A. Par suite, Mme B... est fondée à soutenir que le ministre de l'agriculture devait tenir compte, pour le calcul de sa rémunération antérieure, du solde de la prime de service et de résultats qui lui avait été versé au mois de juillet 2014, nonobstant la circonstance que ce solde lui aurait été attribué au titre de l'année 2013.<br/>
<br/>
              7. Il ressort, en second lieu, des pièces du dossier qu'en fixant la rémunération de Mme B... à l'indice brut 492, Mme B..., qui demeure employée à temps partiel à 80 %, perçoit consécutivement à sa titularisation un traitement mensuel brut inférieur à 70 % de la rémunération mensuelle brute qu'elle percevait antérieurement. Les décisions litigieuses méconnaissent dès lors les dispositions citées aux points 2 et 3.<br/>
<br/>
              8. Il résulte de tout ce qui précède, sans qu'il soit besoin de se prononcer sur les autres moyens, que l'arrêté du ministre de l'agriculture et de l'alimentation du 1er juin 2015 et la décision implicite par laquelle il a rejeté le recours gracieux de Mme B... contre cet arrêté doivent être annulés en tant qu'ils fixent la rémunération de l'intéressée à l'indice brut 492.<br/>
<br/>
              9. Eu égard aux motifs de l'annulation prononcée, il y a lieu d'enjoindre au ministre de l'agriculture et de l'alimentation, sur le fondement de l'article L. 911-1 du code de justice administrative, de fixer le traitement de Mme B... à l'indice le plus proche de celui qui permet à l'intéressée d'obtenir un traitement mensuel brut égal à 70 % de sa rémunération mensuelle antérieure intégrant le solde de la prime de service et de résultats versé en juillet 2014.<br/>
<br/>
              10. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à Mme B... au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt du 22 octobre 2020 de la cour administrative d'appel de Paris est annulé.<br/>
<br/>
Article 2 : L'arrêté du 1er juin 2015 du ministre de l'agriculture et de l'alimentation et la décision implicite par laquelle il a rejeté le recours gracieux de Mme B... sont annulés en tant qu'ils fixent la rémunération de l'intéressée à l'indice brut 492.<br/>
<br/>
Article 3 : Il est enjoint au ministre de l'agriculture et de l'alimentation de fixer le traitement de Mme B... à l'indice le plus proche de celui qui permet à l'intéressée d'obtenir un traitement mensuel brut égal à 70 % de sa rémunération mensuelle antérieure intégrant le solde de la prime de service et de résultats versé en juillet 2014.<br/>
<br/>
Article 4 : L'Etat versera à Mme B... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 5 : La présente décision sera notifiée au ministre de l'agriculture et de l'alimentation et à Mme A... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-08-01 FONCTIONNAIRES ET AGENTS PUBLICS. RÉMUNÉRATION. QUESTIONS D'ORDRE GÉNÉRAL. - GARANTIE D'UNE RÉMUNÉRATION MINIMALE À CERTAINS AGENTS TITULARISÉS - 1) OBJET - MAINTIEN D'AU MOINS 70 % DE LA RÉMUNÉRATION À QUOTITÉ DE TRAVAIL INCHANGÉE - 2) ILLUSTRATION - APPLICATION À UN AGENT À TEMPS PARTIEL.
</SCT>
<ANA ID="9A"> 36-08-01 1) Il résulte du I de l'article 7 et du II de l'article 12 du décret n° 2006-1827 du 23 décembre 2006 et des articles 1er et 2 de l'arrêté du 29 juin 2007 pris pour son application, dont l'objet est de garantir une rémunération minimale aux agents titularisés dans certains corps de catégorie A de la fonction publique de l'Etat, qu'à quotité de travail inchangée, le traitement brut effectivement perçu par un agent postérieurement à sa titularisation ne peut être inférieur à 70 % de la rémunération moyenne mensuelle brute effectivement perçue avant cette titularisation, calculée sur la base des six meilleures rémunérations mensuelles perçues par l'agent dans son dernier emploi au cours de la période de douze mois précédant sa titularisation.,,,2) Agent employé, avant comme après sa titularisation, à temps partiel à 80 %.... ,,La décision fixant sa rémunération à un indice brut tel que l'intéressé perçoit, consécutivement à sa titularisation, un traitement mensuel brut inférieur à 70% de la rémunération mensuelle brute qu'il percevait antérieurement méconnaît les dispositions citées au point 1).</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
