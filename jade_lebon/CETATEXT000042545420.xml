<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042545420</ID>
<ANCIEN_ID>JG_L_2020_11_000000419778</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/54/54/CETATEXT000042545420.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Section, 20/11/2020, 419778, Publié au recueil Lebon</TITRE>
<DATE_DEC>2020-11-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>419778</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Section</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Florian Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESEC:2020:419778.20201120</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B... A... a demandé au tribunal administratif de Fort-de-France de condamner le centre hospitalier régional (CHR) de la Martinique à lui verser la somme de 1 123 297,98 euros en réparation des préjudices qu'elle estime avoir subis à la suite de sa prise en charge dans cet établissement. Par un jugement n° 1200865 du 23 mai 2013, le tribunal administratif a condamné le CHR de la Martinique à lui verser la somme de 91 772 euros et a rejeté le surplus de ses conclusions. <br/>
<br/>
              Par un arrêt n°s 13BX02582, 13BX02585 du 20 octobre 2015, la cour administrative d'appel de Bordeaux a, sur appels du CHR de la Martinique et de Mme A..., annulé ce jugement et rejeté la demande de Mme A.... <br/>
<br/>
              Par une décision n° 396432 du 10 mars 2017, le Conseil d'Etat, statuant au contentieux, a annulé cet arrêt et renvoyé l'affaire à la cour administrative d'appel de Bordeaux.<br/>
<br/>
              Par un arrêt n° 17BX00650 du 9 janvier 2018, la cour administrative d'appel, statuant sur renvoi du Conseil d'Etat, a annulé le jugement du tribunal administratif et rejeté la demande de première instance de Mme A....<br/>
<br/>
              Par un pourvoi, enregistré le 11 avril 2018 au secrétariat du contentieux du Conseil d'Etat, Mme A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge du CHR de la Martinique la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de Mme A... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'à la suite d'une chute sur son lieu de travail ayant provoqué une désinsertion du tendon du muscle jumeau externe de son genou gauche, Mme A... a subi une intervention chirurgicale visant à refixer ce tendon, le 30 décembre 2004, au centre hospitalier régional (CHR) de la Martinique. Cette intervention ayant été suivie d'une paralysie du pied, une nouvelle intervention, réalisée le 12 janvier 2006 dans un autre établissement de santé, a mis en évidence qu'une compression accidentelle du nerf fibulaire s'était produite lors de l'opération du 30 décembre 2004. Saisi par Mme A..., le tribunal administratif de Fort-de-France a, par un jugement du 23 mai 2013, condamné le CHR de la Martinique à lui verser une indemnité de 91 772 euros, en réparation du préjudice de perte de chance ayant résulté pour elle du manquement de cet établissement à son obligation d'information sur les risques inhérents à l'intervention du 30 décembre 2004. Mme A... se pourvoit en cassation contre l'arrêt du 9 janvier 2018 par lequel la cour administrative d'appel de Bordeaux, statuant sur renvoi du Conseil d'Etat après cassation d'un premier arrêt du 20 octobre 2015, a annulé le jugement du 23 mai 2013 et rejeté sa demande d'indemnisation.<br/>
<br/>
              2. En premier lieu, l'arrêt attaqué vise la décision du 10 mars 2017 du Conseil d'Etat statuant au contentieux ayant annulé le précédent arrêt de la même cour administrative d'appel, lequel visait et analysait les mémoires qui avaient alors été produits devant la cour. Par suite, le moyen tiré de ce que l'arrêt attaqué serait entaché d'irrégularité, faute de viser à nouveau ces mémoires produits devant la cour lors de la première instance d'appel, doit être écarté.<br/>
<br/>
              3. En deuxième lieu, si la cour a qualifié d'appel incident l'appel sur lequel elle statuait, cette erreur de plume est sans incidence sur la régularité de son arrêt.<br/>
<br/>
              4. En troisième lieu, en jugeant, pour écarter les conclusions de la requérante tendant à l'indemnisation intégrale de toutes les conséquences dommageables de l'intervention du 30 décembre 2004, que, bien que n'ayant pas été informée de tous les risques que comportait cette intervention, Mme A... devait être regardée comme y ayant consenti, la cour a porté sur les pièces du dossier qui lui était soumis une appréciation souveraine, exempte de dénaturation et n'a pas commis d'erreur de droit.<br/>
<br/>
              5. Enfin, en quatrième lieu, l'article L. 1111-2 du code de la santé publique dispose que : " Toute personne a le droit d'être informée sur son état de santé. Cette information porte sur les différentes investigations, traitements ou actions de prévention qui sont proposés, leur utilité, leur urgence éventuelle, leurs conséquences, les risques fréquents ou graves normalement prévisibles qu'ils comportent ainsi que sur les autres solutions possibles et sur les conséquences prévisibles en cas de refus ". Il résulte de ces dispositions que doivent être portés à la connaissance du patient, préalablement au recueil de son consentement à l'accomplissement d'un acte médical, les risques connus de cet acte qui, soit présentent une fréquence statistique significative, quelle que soit leur gravité, soit revêtent le caractère de risques graves, quelle que soit leur fréquence.<br/>
<br/>
              6. En cas de manquement à cette obligation d'information, si l'acte de diagnostic ou de soin entraîne pour le patient, y compris s'il a été réalisé conformément aux règles de l'art, un dommage en lien avec la réalisation du risque qui n'a pas été porté à sa connaissance, la faute commise en ne procédant pas à cette information engage la responsabilité de l'établissement de santé à son égard, pour sa perte de chance de se soustraire à ce risque en renonçant à l'opération. Il n'en va autrement que s'il résulte de l'instruction, compte tenu de ce qu'était l'état de santé du patient et son évolution prévisible en l'absence de réalisation de l'acte, des alternatives thérapeutiques qui pouvaient lui être proposées ainsi que de tous autres éléments de nature à révéler le choix qu'il aurait fait, qu'informé de la nature et de l'importance de ce risque, il aurait consenti à l'acte en question.<br/>
<br/>
              7. En estimant, au vu des pièces du dossier qui lui était soumis, qu'il était certain que Mme A..., qui souffrait en décembre 2004 d'importantes douleurs et de grandes difficultés à se déplacer, aurait, compte tenu de l'absence d'alternative thérapeutique à l'intervention chirurgicale qui lui était proposée, encore consenti à cette opération si elle avait été informée des risques d'atteinte au nerf fibulaire qu'elle comportait, la cour s'est livrée à une appréciation souveraine, exempte de dénaturation. Elle a pu, par suite, sans erreur de droit et par un arrêt suffisamment motivé sur ce point, juger que, alors même que le CHR de la Martinique n'apportait pas la preuve, qui lui incombait, que Mme A... avait été informée de ce que cette opération comportait ce risque, le manquement de l'établissement à son devoir d'information n'avait privé Mme A... d'aucune chance de se soustraire à ce risque en renonçant à l'opération.<br/>
<br/>
              8. Il résulte de tout ce qui précède que le pourvoi de Mme A... doit être rejeté, y compris, par voie de conséquence, les conclusions qu'elle présente au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi de Mme A... est rejeté.<br/>
Article 2 : La présente décision sera notifiée à Mme B... A..., au centre hospitalier régional de la Martinique et à la caisse générale de sécurité sociale de Martinique.<br/>
Copie en sera adressée au ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-08-02-02-01 PROCÉDURE. VOIES DE RECOURS. CASSATION. CONTRÔLE DU JUGE DE CASSATION. BIEN-FONDÉ. - ABSENCE DE PERTE DE CHANCE RÉSULTANT D'UN MANQUEMENT À L'OBLIGATION D'INFORMATION D'UN PATIENT [RJ5].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">55-03-01-02 PROFESSIONS, CHARGES ET OFFICES. CONDITIONS D'EXERCICE DES PROFESSIONS. MÉDECINS. RÈGLES DIVERSES S'IMPOSANT AUX MÉDECINS DANS L'EXERCICE DE LEUR PROFESSION. - OBLIGATION D'INFORMATION DU PATIENT (ART. L. 1111-2 DU CSP) - 1) PORTÉE [RJ1] - 2) CONSÉQUENCE DU MANQUEMENT - A) INDEMNISATION DE LA PERTE DE CHANCE DE SE SOUSTRAIRE AU RISQUE LIÉ À L'INTERVENTION [RJ2] - B) CAS OÙ LE PATIENT, MÊME INFORMÉ DE CE RISQUE, AURAIT CONSENTI À L'ACTE - ABSENCE DE PERTE DE CHANCE [RJ3] - ELÉMENTS À PRENDRE EN COMPTE [RJ4].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">60-02-01-01-01-01-04 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICE PUBLIC DE SANTÉ. ÉTABLISSEMENTS PUBLICS D'HOSPITALISATION. RESPONSABILITÉ POUR FAUTE SIMPLE : ORGANISATION ET FONCTIONNEMENT DU SERVICE HOSPITALIER. EXISTENCE D'UNE FAUTE. MANQUEMENTS À UNE OBLIGATION D'INFORMATION ET DÉFAUTS DE CONSENTEMENT. - MANQUEMENT À L'OBLIGATION D'INFORMATION DU PATIENT (ART. L. 1111-2 DU CSP) [RJ1] - 1) INDEMNISATION DE LA PERTE DE CHANCE DE SE SOUSTRAIRE AU RISQUE LIÉ À L'INTERVENTION [RJ2] - 2) CAS OÙ LE PATIENT, MÊME INFORMÉ DE CE RISQUE, AURAIT CONSENTI À L'ACTE - ABSENCE DE PERTE DE CHANCE [RJ3] - A) ELÉMENTS À PRENDRE EN COMPTE [RJ4] - B) CONTRÔLE DU JUGE DE CASSATION - DÉNATURATION [RJ5].
</SCT>
<ANA ID="9A"> 54-08-02-02-01 En cas de dommage lié à la réalisation d'un risque qui n'a, en méconnaissance de l'obligation d'information fixée à l'article L. 1111-2 du code de la santé publique (CSP), pas été porté à la connaissance du patient, le juge de cassation laisse à l'appréciation souveraine des juges du fond, sous réserve de dénaturation, le point de savoir si, informé de la nature et de l'importance de ce risque, le patient aurait consenti à l'acte en question, de sorte que le manquement à l'obligation d'information ne l'a privé d'aucune chance de s'y soustraire et ne peut, par suite, être indemnisé à ce titre.</ANA>
<ANA ID="9B"> 55-03-01-02 1) Il résulte de l'article L. 1111-2 du code de la santé publique (CSP) que doivent être portés à la connaissance du patient, préalablement au recueil de son consentement à l'accomplissement d'un acte médical, les risques connus de cet acte qui, soit présentent une fréquence statistique significative, quelle que soit leur gravité, soit revêtent le caractère de risques graves, quelle que soit leur fréquence.,,,2) a) En cas de manquement à cette obligation d'information, si l'acte de diagnostic ou de soin entraîne pour le patient, y compris s'il a été réalisé conformément aux règles de l'art, un dommage en lien avec la réalisation du risque qui n'a pas été porté à sa connaissance, la faute commise en ne procédant pas à cette information engage la responsabilité de l'établissement de santé à son égard, pour sa perte de chance de se soustraire à ce risque en renonçant à l'opération.... ,,b) Il n'en va autrement que s'il résulte de l'instruction, compte tenu de ce qu'était l'état de santé du patient et son évolution prévisible en l'absence de réalisation de l'acte, des alternatives thérapeutiques qui pouvaient lui être proposées ainsi que de tous autres éléments de nature à révéler le choix qu'il aurait fait, qu'informé de la nature et de l'importance de ce risque, il aurait consenti à l'acte en question.</ANA>
<ANA ID="9C"> 60-02-01-01-01-01-04 Il résulte de l'article L. 1111-2 du code de la santé publique (CSP) que doivent être portés à la connaissance du patient, préalablement au recueil de son consentement à l'accomplissement d'un acte médical, les risques connus de cet acte qui, soit présentent une fréquence statistique significative, quelle que soit leur gravité, soit revêtent le caractère de risques graves, quelle que soit leur fréquence.,,,1) En cas de manquement à cette obligation d'information, si l'acte de diagnostic ou de soin entraîne pour le patient, y compris s'il a été réalisé conformément aux règles de l'art, un dommage en lien avec la réalisation du risque qui n'a pas été porté à sa connaissance, la faute commise en ne procédant pas à cette information engage la responsabilité de l'établissement de santé à son égard, pour sa perte de chance de se soustraire à ce risque en renonçant à l'opération.... ,,2) a) Il n'en va autrement que s'il résulte de l'instruction, compte tenu de ce qu'était l'état de santé du patient et son évolution prévisible en l'absence de réalisation de l'acte, des alternatives thérapeutiques qui pouvaient lui être proposées ainsi que de tous autres éléments de nature à révéler le choix qu'il aurait fait, qu'informé de la nature et de l'importance de ce risque, il aurait consenti à l'acte en question.,,,b) Le juge de cassation laisse ce point à l'appréciation souveraine des juges du fond, sous réserve de dénaturation.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur la portée de cette obligation, CE, 19 octobre 2016, Centre hospitalier d'Issoire et autres, n° 391538, p. 435.,,[RJ2] Cf. Section, 5 janvier 2000, Consorts,, n° 181899, p. 5.,,[RJ3] Cf., en précisant, CE, 15 janvier 2001, Mme,et autres, n° 184386, T. pp. 1184-1186 ; CE, 11 juillet 2011, M.,, n° 328183, T. pp. 1109-1145 ; CE, 24 septembre 2012, Mlle,, n° 339285, pt. 3, aux Tables sur un autre point ; CE, 3 février 2016, Mme,, n° 376620, T. p. 944. Rappr. Cass. civ. 1ère, 27 novembre 2013, n° 12-27.961, inédit.,,[RJ4] Rappr. Cass. civ. 1ère, 20 juin 2000, 98-23.046, Bull. civ. I, n° 193., ,[RJ5] Cf. CE, 11 juillet 2011, M.,, n° 328183, T. pp. 1109-1145.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
