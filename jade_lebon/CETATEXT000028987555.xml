<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028987555</ID>
<ANCIEN_ID>JG_L_2014_05_000000344265</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/98/75/CETATEXT000028987555.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème / 9ème SSR, 26/05/2014, 344265, Publié au recueil Lebon</TITRE>
<DATE_DEC>2014-05-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>344265</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème / 9ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LEVIS</AVOCATS>
<RAPPORTEUR>M. Romain Godet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:344265.20140526</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi et le mémoire complémentaire, enregistrés les 10 novembre et 29 décembre 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B...A..., demeurant ...; M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision n° 642896, devenue n° 09001713, du 20 avril 2010 par laquelle la Cour nationale du droit d'asile a sursis à statuer sur sa demande tendant à l'annulation de la décision du 31 décembre 2008 du directeur général de l'Office français de protection des réfugiés et apatrides rejetant sa demande d'asile et lui a enjoint de justifier, dans un délai de deux mois à compter de la notification de cette décision, de sa diligence à saisir l'ambassade de Corée du sud en vue de la détermination de son droit à la nationalité sud-coréenne ;<br/>
<br/>
              2°) de mettre à la charge de l'Office de protection des réfugiés et apatrides la somme de 3 000 euros à verser à la SCP Defrenois, Levis, au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Romain Godet, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lévis, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>	1. Considérant qu'aux termes du A de l'article 1er de la convention de Genève relative au statut des réfugiés : " Aux fins de la présente convention, le terme " réfugié " s'appliquera à toute personne : (...) / 2°) qui, (...) craignant avec raison d'être persécutée du fait de sa race, de sa religion, de sa nationalité, de son appartenance à un certain groupe social ou de ses opinions politiques, se trouve hors du pays dont elle a la nationalité et qui ne peut ou, du fait de cette crainte, ne veut se réclamer de la protection de ce pays ; ou qui, si elle n'a pas de nationalité et se trouve hors du pays dans lequel elle avait sa résidence habituelle à la suite de tels évènements, ne peut ou, en raison de ladite crainte, ne veut y retourner. / Dans le cas d'une personne qui a plus d'une nationalité, l'expression " du pays dont elle a la nationalité " vise chacun des pays dont cette personne a la nationalité. Ne sera pas considérée comme privée de la protection du pays dont elle a la nationalité, toute personne qui, sans raison valable fondée sur une crainte justifiée, ne s'est pas réclamée de la protection de l'un des pays dont elle a la nationalité. " ; <br/>
<br/>
              2. Considérant, d'une part, qu'il appartient à la Cour nationale du droit d'asile, qui statue comme juge de plein contentieux, de se prononcer elle-même sur le droit d'un demandeur d'asile à la qualité de réfugié au vu de l'ensemble des circonstances de fait dont elle a connaissance au moment où elle statue ; qu'à ce titre, d'une part, la cour peut toujours, conformément aux dispositions du premier alinéa de l'article R. 733-18 du code de l'entrée et du séjour des étrangers et du droit d'asile, prescrire des mesures d'instruction afin d'être pleinement éclairée sur les circonstances nécessaires à la solution du litige qui lui est soumis, sous réserve que ces mesures ne soient pas inutiles ou frustratoires ; que, d'autre part, il lui revient le cas échéant, pour déterminer la nationalité d'un demandeur d'asile, d'interpréter les dispositions d'une loi étrangère qui déterminent les règles d'attribution ou d'acquisition de cette nationalité ; que, sous réserve de dénaturation, il n'appartient pas au Conseil d'Etat, juge de cassation, de contrôler l'interprétation ainsi faite par la Cour de cette loi étrangère, qui relève de son appréciation souveraine ; <br/>
<br/>
              3. Considérant, toutefois, qu'aux termes de l'article 29 du code civil : " La juridiction civile de droit commun est seule compétente pour connaître des contestations sur la nationalité française ou étrangère des personnes physiques. / Les questions de nationalité sont préjudicielles devant toute autre juridiction de l'ordre administratif ou judiciaire à l'exception des juridictions répressives comportant un jury criminel. " ; qu'il résulte de ces dispositions que la Cour nationale du droit d'asile ne peut trancher elle-même la question de la nationalité d'un demandeur d'asile lorsque cette question soulève une difficulté sérieuse, qui relève alors de la compétence exclusive de l'autorité judiciaire ; qu'en pareille hypothèse, il appartient à la Cour de surseoir à statuer dans l'attente que la juridiction judiciaire ait tranché la question de la nationalité du demandeur ;<br/>
<br/>
              4. Considérant qu'il résulte des énonciations de la décision attaquée qu'après avoir relevé qu'il n'était pas exclu, eu égard aux éléments présentés par le requérant qui prétendait notamment être né en Corée du Nord, que celui-ci possédât la nationalité nord-coréenne, la Cour a estimé, par une appréciation souveraine exempte de dénaturation, que les dispositions de la Constitution de la République de Corée du 12 juillet 1948 et de la loi de la République de Corée du 20 décembre 1948 relative à la nationalité ouvraient à un ressortissant de Corée du Nord le droit de se voir reconnaître la nationalité sud-coréenne à raison de sa naissance dans la péninsule coréenne ou ses îles adjacentes ; qu'en décidant, dans le cadre de son pouvoir d'instruction, de surseoir à statuer pour enjoindre à M. A...de saisir les autorités consulaires sud-coréennes afin que celles-ci " examinent son droit à la nationalité sud-coréenne ", alors que, s'il lui était loisible de se fonder sur l'absence de démarche de M. A...auprès des autorités sud-coréennes pour rejeter sa demande d'asile dans le cas où sa qualité de ressortissant de Corée du Nord aurait été établie, il résultait des constatations de la Cour que la nationalité nord-coréenne dont se prévalait le requérant soulevait une difficulté sérieuse et que cette question, qu'elle n'était pas compétente pour trancher elle-même, devait l'être avant de déterminer s'il pouvait se voir reconnaître la nationalité sud-coréenne par les autorités de cet Etat, la Cour a méconnu son office et entaché sa décision d'une erreur de droit ; qu'il résulte de ce qui précède que M. A...est fondé à demander l'annulation de la décision du 20 avril 2010 ;<br/>
<br/>
              	5. Considérant que M. A...a obtenu le bénéfice de l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Levis, avocat de M.A..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Office français de protection des réfugiés et apatrides la somme de 3 000 euros à verser à cette SCP ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La décision du 20 avril 2010 de la Cour nationale du droit d'asile est annulée.<br/>
Article 2 : L'affaire est renvoyée à la Cour nationale du droit d'asile.<br/>
Article 3 : L'Office français de protection des réfugiés et apatrides versera à la SCP Levis, une somme de 3 000 euros en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve qu'elle renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : La présente décision sera notifiée à M. B...A...et à l'Office français de protection des réfugiés et apatrides.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">095-08-05-01-06 - INTERPRÉTATION, POUR DÉTERMINER LA NATIONALITÉ D'UN DEMANDEUR D'ASILE, DES DISPOSITIONS D'UNE LOI ÉTRANGÈRE QUI DÉTERMINENT LES RÈGLES D'ATTRIBUTION OU D'ACQUISITION DE CETTE NATIONALITÉ - 1) A) EXISTENCE - B) CONTRÔLE DU JUGE DE CASSATION - DÉNATURATION - 2) ESPÈCE - DEMANDEUR D'ASILE SE PRÉVALANT DE LA NATIONALITÉ NORD-CORÉENNE - LOI SUD-CORÉENNE OUVRANT AUX RESSORTISSANTS DE CORÉE DU NORD LE DROIT DE SE VOIR RECONNAÎTRE LA NATIONALITÉ SUD-CORÉENNE - FACULTÉ DE LA CNDA DE SURSEOIR À STATUER POUR ENJOINDRE AU DEMANDEUR DE SAISIR LES AUTORITÉS SUD-CORÉENNES AFIN QUE CELLES-CI  EXAMINENT SON DROIT À LA NATIONALITÉ SUD-CORÉENNE  - ABSENCE - OBLIGATION DE LA CNDA, EN CAS DE DIFFICULTÉ SÉRIEUSE SUR LA NATIONALITÉ NORD-CORÉENNE DU DEMANDEUR, D'ADRESSER UNE QUESTION À L'AUTORITÉ JUDICIAIRE AVANT DE DÉTERMINER ELLE-MÊME SI L'INTÉRESSÉ PEUT SE VOIR RECONNAÎTRE LA NATIONALITÉ SUD-CORÉENNE - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">095-08-05-01-08-02 - DEMANDEUR D'ASILE SE PRÉVALANT DE LA NATIONALITÉ NORD-CORÉENNE - LOI SUD-CORÉENNE OUVRANT AUX RESSORTISSANTS DE CORÉE DU NORD LE DROIT DE SE VOIR RECONNAÎTRE LA NATIONALITÉ SUD-CORÉENNE - FACULTÉ DE LA CNDA DE SURSEOIR À STATUER POUR ENJOINDRE AU DEMANDEUR DE SAISIR LES AUTORITÉS SUD-CORÉENNES AFIN QUE CELLES-CI  EXAMINENT SON DROIT À LA NATIONALITÉ SUD-CORÉENNE  - ABSENCE - OBLIGATION DE LA CNDA, EN CAS DE DIFFICULTÉ SÉRIEUSE SUR LA NATIONALITÉ NORD-CORÉENNE DU DEMANDEUR, D'ADRESSER UNE QUESTION À L'AUTORITÉ JUDICIAIRE AVANT DE DÉTERMINER ELLE-MÊME SI L'INTÉRESSÉ PEUT SE VOIR RECONNAÎTRE LA NATIONALITÉ SUD-CORÉENNE - EXISTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">095-08-05-02 - INTERPRÉTATION, POUR DÉTERMINER LA NATIONALITÉ D'UN DEMANDEUR D'ASILE, DES DISPOSITIONS D'UNE LOI ÉTRANGÈRE QUI DÉTERMINENT LES RÈGLES D'ATTRIBUTION OU D'ACQUISITION DE CETTE NATIONALITÉ - 1) A) EXISTENCE - B) CONTRÔLE DU JUGE DE CASSATION - DÉNATURATION - 2) ESPÈCE - DEMANDEUR D'ASILE SE PRÉVALANT DE LA NATIONALITÉ NORD-CORÉENNE - LOI SUD-CORÉENNE OUVRANT AUX RESSORTISSANTS DE CORÉE DU NORD LE DROIT DE SE VOIR RECONNAÎTRE LA NATIONALITÉ SUD-CORÉENNE - FACULTÉ DE LA CNDA DE SURSEOIR À STATUER POUR ENJOINDRE AU DEMANDEUR DE SAISIR LES AUTORITÉS SUD-CORÉENNES AFIN QUE CELLES-CI  EXAMINENT SON DROIT À LA NATIONALITÉ SUD-CORÉENNE  - ABSENCE - OBLIGATION DE LA CNDA, EN CAS DE DIFFICULTÉ SÉRIEUSE SUR LA NATIONALITÉ NORD-CORÉENNE DU DEMANDEUR, D'ADRESSER UNE QUESTION À L'AUTORITÉ JUDICIAIRE AVANT DE DÉTERMINER ELLE-MÊME SI L'INTÉRESSÉ PEUT SE VOIR RECONNAÎTRE LA NATIONALITÉ SUD-CORÉENNE - EXISTENCE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">54-08-02-02-01-03 PROCÉDURE. VOIES DE RECOURS. CASSATION. CONTRÔLE DU JUGE DE CASSATION. BIEN-FONDÉ. APPRÉCIATION SOUVERAINE DES JUGES DU FOND. - INTERPRÉTATION PAR LA CNDA, POUR DÉTERMINER LA NATIONALITÉ D'UN DEMANDEUR D'ASILE, DES DISPOSITIONS D'UNE LOI ÉTRANGÈRE QUI DÉTERMINENT LES RÈGLES D'ATTRIBUTION OU D'ACQUISITION DE CETTE NATIONALITÉ.
</SCT>
<SCT ID="8E" TYPE="PRINCIPAL">54-08-02-02-01-04 PROCÉDURE. VOIES DE RECOURS. CASSATION. CONTRÔLE DU JUGE DE CASSATION. BIEN-FONDÉ. DÉNATURATION. - INTERPRÉTATION PAR LA CNDA, POUR DÉTERMINER LA NATIONALITÉ D'UN DEMANDEUR D'ASILE, DES DISPOSITIONS D'UNE LOI ÉTRANGÈRE QUI DÉTERMINENT LES RÈGLES D'ATTRIBUTION OU D'ACQUISITION DE CETTE NATIONALITÉ.
</SCT>
<ANA ID="9A"> 095-08-05-01-06 1) a) Il appartient à la Cour nationale du droit d'asile (CNDA), qui statue comme juge de plein contentieux, de se prononcer elle-même sur le droit d'un demandeur d'asile à la qualité de réfugié au vu de l'ensemble des circonstances de fait dont elle a connaissance au moment où elle statue. A ce titre, il lui revient le cas échéant, pour déterminer la nationalité d'un demandeur d'asile, d'interpréter les dispositions d'une loi étrangère qui déterminent les règles d'attribution ou d'acquisition de cette nationalité. b) Sous réserve de dénaturation, il n'appartient pas au Conseil d'Etat, juge de cassation, de contrôler l'interprétation ainsi faite par la Cour de cette loi étrangère, qui relève de son appréciation souveraine.,,,2) Demandeur d'asile se prévalant de la nationalité nord-coréenne.... ,,Après avoir estimé que les dispositions de la Constitution de la République de Corée du 12 juillet 1948 et de la loi de la République de Corée du 20 décembre 1948 relative à la nationalité ouvraient à un ressortissant de Corée du Nord le droit de se voir reconnaître la nationalité sud-coréenne à raison de sa naissance dans la péninsule coréenne ou ses îles adjacentes, la CNDA ne pouvait, sans méconnaître son office, décider, dans le cadre de son pouvoir d'instruction, de surseoir à statuer pour enjoindre au demandeur de saisir les autorités consulaires sud-coréennes afin que celles-ci  examinent son droit à la nationalité sud-coréenne , alors que, s'il lui était loisible de se fonder sur l'absence de démarche de l'intéressé auprès des autorités sud-coréennes pour rejeter sa demande d'asile dans le cas où sa qualité de ressortissant de Corée du Nord aurait été établie, il résultait des constatations de la Cour que la nationalité nord-coréenne dont se prévalait le requérant soulevait une difficulté sérieuse et que cette question, qu'elle n'était pas compétente pour trancher elle-même dès lors qu'elle relève de la compétence exclusive de l'autorité judiciaire, devait l'être avant de déterminer s'il pouvait se voir reconnaître la nationalité sud-coréenne par les autorités de cet Etat.</ANA>
<ANA ID="9B"> 095-08-05-01-08-02 Demandeur d'asile se prévalant de la nationalité nord-coréenne.... ,,Après avoir estimé que les dispositions de la Constitution de la République de Corée du 12 juillet 1948 et de la loi de la République de Corée du 20 décembre 1948 relative à la nationalité ouvraient à un ressortissant de Corée du Nord le droit de se voir reconnaître la nationalité sud-coréenne à raison de sa naissance dans la péninsule coréenne ou ses îles adjacentes, la CNDA ne pouvait, sans méconnaître son office, décider, dans le cadre de son pouvoir d'instruction, de surseoir à statuer pour enjoindre au demandeur de saisir les autorités consulaires sud-coréennes afin que celles-ci  examinent son droit à la nationalité sud-coréenne , alors que, s'il lui était loisible de se fonder sur l'absence de démarche de l'intéressé auprès des autorités sud-coréennes pour rejeter sa demande d'asile dans le cas où sa qualité de ressortissant de Corée du Nord aurait été établie, il résultait des constatations de la Cour que la nationalité nord-coréenne dont se prévalait le requérant soulevait une difficulté sérieuse et que cette question, qu'elle n'était pas compétente pour trancher elle-même dès lors qu'elle relève de la compétence exclusive de l'autorité judiciaire, devait l'être avant de déterminer s'il pouvait se voir reconnaître la nationalité sud-coréenne par les autorités de cet Etat.</ANA>
<ANA ID="9C"> 095-08-05-02 1) a) Il appartient à la Cour nationale du droit d'asile (CNDA), qui statue comme juge de plein contentieux, de se prononcer elle-même sur le droit d'un demandeur d'asile à la qualité de réfugié au vu de l'ensemble des circonstances de fait dont elle a connaissance au moment où elle statue. A ce titre, il lui revient le cas échéant, pour déterminer la nationalité d'un demandeur d'asile, d'interpréter les dispositions d'une loi étrangère qui déterminent les règles d'attribution ou d'acquisition de cette nationalité. b) Sous réserve de dénaturation, il n'appartient pas au Conseil d'Etat, juge de cassation, de contrôler l'interprétation ainsi faite par la Cour de cette loi étrangère, qui relève de son appréciation souveraine.,,,2) Demandeur d'asile se prévalant de la nationalité nord-coréenne.... ,,Après avoir estimé que les dispositions de la Constitution de la République de Corée du 12 juillet 1948 et de la loi de la République de Corée du 20 décembre 1948 relative à la nationalité ouvraient à un ressortissant de Corée du Nord le droit de se voir reconnaître la nationalité sud-coréenne à raison de sa naissance dans la péninsule coréenne ou ses îles adjacentes, la CNDA ne pouvait, sans méconnaître son office, décider, dans le cadre de son pouvoir d'instruction, de surseoir à statuer pour enjoindre au demandeur de saisir les autorités consulaires sud-coréennes afin que celles-ci  examinent son droit à la nationalité sud-coréenne , alors que, s'il lui était loisible de se fonder sur l'absence de démarche de l'intéressé auprès des autorités sud-coréennes pour rejeter sa demande d'asile dans le cas où sa qualité de ressortissant de Corée du Nord aurait été établie, il résultait des constatations de la Cour que la nationalité nord-coréenne dont se prévalait le requérant soulevait une difficulté sérieuse et que cette question, qu'elle n'était pas compétente pour trancher elle-même dès lors qu'elle relève de la compétence exclusive de l'autorité judiciaire, devait l'être avant de déterminer s'il pouvait se voir reconnaître la nationalité sud-coréenne par les autorités de cet Etat.</ANA>
<ANA ID="9D"> 54-08-02-02-01-03 Sous réserve de dénaturation, il n'appartient pas au Conseil d'Etat, juge de cassation, de contrôler l'interprétation faite par la Cour nationale du droit d'asile (CNDA) des dispositions d'une loi étrangère qui déterminent les règles d'attribution ou d'acquisition de la nationalité d'un demandeur d'asile, qui relève de son appréciation souveraine.</ANA>
<ANA ID="9E"> 54-08-02-02-01-04 Sous réserve de dénaturation, il n'appartient pas au Conseil d'Etat, juge de cassation, de contrôler l'interprétation faite par la Cour nationale du droit d'asile (CNDA) des dispositions d'une loi étrangère qui déterminent les règles d'attribution ou d'acquisition de la nationalité d'un demandeur d'asile, qui relève de son appréciation souveraine.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
