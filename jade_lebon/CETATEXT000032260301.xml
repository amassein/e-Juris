<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032260301</ID>
<ANCIEN_ID>JG_L_2016_03_000000377874</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/26/03/CETATEXT000032260301.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème SSR, 16/03/2016, 377874</TITRE>
<DATE_DEC>2016-03-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>377874</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>Mme Esther de Moustier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:377874.20160316</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société par actions simplifiée (SAS) Rapa a demandé au tribunal administratif d'Orléans de prononcer la décharge de la cotisation supplémentaire d'impôt sur les sociétés à laquelle elle a été assujettie au titre de l'exercice clos le 31 janvier 2005. Par un jugement n° 1100780 du 24 novembre 2011, le tribunal a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 12NT00232 du 13 février 2014, la cour administrative d'appel de Nantes a rejeté l'appel formé contre ce jugement par la SAS Rapa. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 17 avril et 16 juillet 2014 et le 19 mars 2015 au secrétariat du contentieux du Conseil d'Etat, la SAS Rapa demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le traité instituant la Communauté européenne, notamment ses articles 87, 88 et 249 ;<br/>
              - le règlement (CE) n° 70/2001 de la Commission du 12 janvier 2001 ;<br/>
              - la décision n° 2004/343/CE du 16 décembre 2003 de la Commission ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - la loi n° 2004-1485 du 30 décembre 2004 ;  <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Esther de Moustier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Odent, Poulet, avocat de la SAS Rapa ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la SAS Rapa a été constituée le 29 janvier 2003 pour reprendre à compter du 1er février suivant l'activité et les actifs de la SA Rapa, qui faisait l'objet d'une procédure de redressement judiciaire ; qu'elle s'est prévalue du bénéfice de l'exonération d'impôt sur les sociétés en cas de reprise d'entreprises industrielles en difficulté prévue à l'article 44 septies du code général des impôts, dans sa rédaction alors applicable, pour ses exercices clos les 31 janvier 2004 et 2005 ; qu'à la suite d'une vérification de comptabilité portant sur la période du 1er février 2004 au 31 janvier 2008, l'administration a remis en cause l'exonération du bénéfice de l'exercice clos le 31 janvier 2005 au motif que la requérante avait épuisé l'avantage fiscal lié au régime de l'article 44 septies du code général des impôts, tel qu'il résultait des modifications que lui avaient apportées les dispositions de l'article 41 de la loi du 30 décembre 2004, applicables aux exercices clos à compter du 16 décembre 2003 ; que la société Rapa se pourvoit en cassation contre l'arrêt du 13 février 2014 par lequel la cour administrative d'appel de Nantes a rejeté son appel dirigé contre le jugement du 24 novembre 2011 du tribunal administratif d'Orléans rejetant sa demande tendant à la décharge de la cotisation d'impôt sur les sociétés à laquelle elle a été de ce fait assujettie au titre de l'exercice clos le 31 janvier 2005 ;<br/>
<br/>
              2. Considérant que, par une décision du 16 décembre 2003, la Commission européenne a estimé que le dispositif d'exonération temporaire d'impôt sur les sociétés prévu par l'article 44 septies du code général des impôts en cas de reprise d'une entreprise industrielle en difficulté qui a fait l'objet d'une cession ordonnée par le tribunal de commerce dans le cadre d'une procédure de redressement judiciaire constituait, à l'exception des exonérations remplissant les conditions énoncées par le règlement (CE) n° 69/2001 du 12 janvier 2001 ou  par les règles de minimis applicables au moment de leur octroi, un régime d'aides d'Etat mis à exécution en violation de l'article 88, paragraphe 3, du traité instituant la Communauté européenne et incompatible avec le marché commun, sous réserve des aides d'un montant inférieur au seuil fixé par le règlement (CE) n° 70/2001 du 12 janvier 2001 et des aides compatibles au titre des régimes applicables aux aides à finalité régionale et aux aides en faveur des petites et moyennes entreprises ; qu'elle a ordonné, en conséquence, la suppression de ce régime et la récupération sans délai des aides illégalement octroyées ; qu'en application de cette décision de la Commission, l'article 41 de la loi de finances rectificative du 30 décembre 2004 a, d'une part, par le 1° de son paragraphe I, substitué à l'article 44 septies du code général des impôts alors en vigueur une nouvelle rédaction du même article, d'autre part, prévu, par son paragraphe II, que les dispositions du 1° du paragraphe I seraient applicables aux résultats des exercices clos à compter du 16 décembre 2003 et jusqu'au 31 décembre 2006 inclus ;<br/>
<br/>
              3. Considérant, en premier lieu, qu'en vertu de la décision de la Commission du 16 décembre 2003, dont la validité n'a pas été contestée devant les juridictions de l'Union européenne dans les conditions prévues par l'article 230 du traité instituant la Communauté européenne, devenu article 263 du traité sur le fonctionnement de l'Union européenne, les autorités nationales étaient tenues, ainsi qu'elles l'ont fait, de modifier l'article 44 septies du code général des impôts et de remettre à la charge des contribuables, dans les conditions prévues par le droit national, les exonérations d'impôt dont ils avaient irrégulièrement bénéficié ; que, par suite, en retenant que la société requérante ne pouvait pas utilement se prévaloir du principe de confiance légitime pour obtenir la décharge des impositions litigieuses, la cour administrative d'appel n'a, en tout état de cause, ni entaché son arrêt de contradiction de motifs ni commis d'erreur de droit ; <br/>
<br/>
              4. Considérant, en deuxième lieu, que la société requérante ne peut utilement contester le bien-fondé de l'arrêt qu'elle attaque en invoquant, pour la première fois devant le juge de cassation, le moyen, qui n'est pas d'ordre public et n'est pas né de cet arrêt, tiré de la méconnaissance d'une espérance légitime devant être regardée comme un bien au sens des stipulations de l'article 1er du premier protocole additionnel à la convention européenne des droits de sauvegarde des droits de l'homme et des libertés fondamentales ; que si la société requérante se prévaut aussi d'une méconnaissance de la garantie des droits qui résulte de l'article 16 de la Déclaration des droits de l'homme et du citoyen de 1789, ce moyen, dirigé contre une imposition établie sur le fondement de dispositions législatives, ne peut qu'être écarté dès lors qu'en dehors des cas et conditions où il est saisi sur le fondement de l'article 61-1 de la Constitution, il n'appartient pas, en tout état de cause, au Conseil d'Etat, statuant au contentieux, de se prononcer sur un moyen tiré de la non-conformité à la Constitution de dispositions législatives ;<br/>
<br/>
              5. Considérant, en troisième lieu, que la cour administrative d'appel de Nantes a écarté comme inopérant le moyen de la société requérante tiré de ce que l'administration fiscale aurait tardé à prévoir les modalités d'application du dispositif résultant de la décision n° 2004/343/CE du 16 décembre 2003 de la Commission européenne, en méconnaissance du principe de sécurité juridique ; qu'elle n'a ce faisant ni commis d'erreur de droit ni dénaturé les pièces du dossier qui lui était soumis dès lors que les impositions litigieuses ont été mises à la charge de la société requérante sur le fondement des seules dispositions de l'article 44 septies du code général des impôts, dans leur rédaction issue de l'article 41 de la loi de finances rectificative pour 2004 ;<br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que le pourvoi de la société Rapa doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>               D E C I D E :<br/>
                            --------------<br/>
<br/>
Article 1er : Le pourvoi de la SAS Rapa est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la société par actions simplifiée Rapa et au ministre des finances et des comptes publics. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">14-05-04 COMMERCE, INDUSTRIE, INTERVENTION ÉCONOMIQUE DE LA PUISSANCE PUBLIQUE. DÉFENSE DE LA CONCURRENCE. AIDES D'ETAT. - DÉCISION NON CONTESTÉE DE LA COMMISSION RELATIVE À UNE AIDE D'ETAT ILLICITE - ETAT-MEMBRE TENU D'EN TIRER LES CONSÉQUENCES [RJ1].
</SCT>
<ANA ID="9A"> 14-05-04 Par une décision du 16 décembre 2003, la Commission européenne a estimé que le dispositif d'exonération temporaire d'impôt sur les sociétés prévu par l'article 44 septies du code général des impôts (CGI) en cas de reprise d'une entreprise industrielle en difficulté constituait en partie une aide d'Etat instituée en violation de l'article 88 du traité instituant la Communauté européenne. En vertu de cette décision, dont la validité n'a pas été contestée devant les juridictions de l'Union européenne dans les conditions prévues par l'article 230 du traité instituant la Communauté européenne, devenu article 263 du traité sur le fonctionnement de l'Union européenne (TFUE), les autorités nationales étaient tenues, ainsi qu'elles l'ont fait, de modifier l'article 44 septies du CGI et de remettre à la charge des contribuables, dans les conditions prévues par le droit national, les exonérations d'impôt dont ils avaient irrégulièrement bénéficié. Par suite, en retenant que la société requérante ne pouvait pas utilement se prévaloir du principe de confiance légitime pour obtenir la décharge des impositions litigieuses, la cour administrative d'appel n'a, en tout état de cause, pas commis d'erreur de droit.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. CE, 23 juillet 2014, Commune de Vendranges, n° 364466, T. p. 556.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
