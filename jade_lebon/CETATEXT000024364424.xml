<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000024364424</ID>
<ANCIEN_ID>JG_L_2011_07_000000328183</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/24/36/44/CETATEXT000024364424.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 11/07/2011, 328183</TITRE>
<DATE_DEC>2011-07-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>328183</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; LE PRADO ; FOUSSARD</AVOCATS>
<RAPPORTEUR>M. Philippe Ranquet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2011:328183.20110711</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 22 mai et 11 août 2009 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. A...B..., demeurant... ; M.  B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 07NC01458 du 19 mars 2009 par lequel la cour administrative d'appel de Nancy, après avoir annulé les jugements n° 0401638 du tribunal administratif de Strasbourg des 22 novembre 2005 et 30 août 2007, a rejeté ses conclusions tendant à la condamnation des Hôpitaux universitaires de Strasbourg à réparer les conséquences dommageables résultant de l'intervention chirurgicale qu'il a subie le 3 mai 2001 dans le service de chirurgie maxillo-faciale et réparatrice ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ; <br/>
<br/>
              3°) de mettre à la charge des Hôpitaux universitaires de Strasbourg la somme de 1 500 euros en application des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Philippe Ranquet, Maître des Requêtes, <br/>
<br/>
              - les observations de la SCP Piwnica, Molinié, avocat de M. B..., de Me Le Prado, avocat des Hôpitaux universitaires de Strasbourg et de Me Foussard, avocat de la caisse primaire d'assurance maladie du Bas-Rhin, <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Piwnica, Molinié, avocat de M. B..., à Me Le Prado, avocat des Hôpitaux universitaires de Strasbourg et à Me Foussard, avocat de la caisse primaire d'assurance maladie du Bas-Rhin ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. B..., qui souffrait du syndrome des apnées obstructives du sommeil, a subi le 3 mai 2001 une intervention chirurgicale en vue de réaliser une ostéotomie de la mandibule ; qu'il en est résulté de graves complications infectieuses qui ont notamment nécessité l'ablation, le 23 juillet 2001, du matériel d'ostéosynthèse puis l'extraction de deux dents ; que M.  B...a dû subir plusieurs interventions sur une période de cinq ans et a présenté un ensemble de troubles comportant une perte de sensibilité de la cuisse gauche, une difficulté de la marche et des douleurs au genou droit ; que, par jugements du 22 novembre 2005 et du 30 août 2007, le tribunal administratif de Strasbourg, après avoir retenu une faute des Hôpitaux universitaires de Strasbourg consistant en un défaut d'information, les a condamnés à verser la somme de 14 063, 75 euros à M.  B...et celle de 8 672, 23 euros à la caisse primaire d'assurance maladie du Bas-Rhin ; que, par un arrêt du 19 mars 2009 contre lequel M.  B...se pourvoit en cassation, la cour administrative d'appel de Nancy a annulé les deux jugements et rejeté sa demande d'indemnisation ; <br/>
<br/>
              Considérant que lorsque l'acte médical envisagé, même accompli conformément aux règles de l'art, comporte des risques connus de décès ou d'invalidité, le patient doit en être informé dans des conditions qui permettent de recueillir son consentement éclairé ; que si cette information n'est pas requise en cas d'urgence, d'impossibilité ou de refus du patient d'être informé, la seule circonstance que les risques ne se réalisent qu'exceptionnellement ne dispense pas les praticiens de leur obligation ;<br/>
<br/>
              Considérant qu'un manquement des médecins à leur obligation d'information n'engage la responsabilité de l'hôpital que dans la mesure où il a privé le patient de la possibilité de se soustraire au risque lié à l'intervention ; que dès lors, pour juger que la faute consistant à ne pas avoir informé M.  B...du risque que comportait l'ostéotomie n'avait fait perdre à ce dernier aucune chance d'échapper au dommage, la cour a à bon droit recherché si cette intervention présentait un caractère indispensable ; qu'elle a toutefois dénaturé les faits de l'espèce en reconnaissant un tel caractère à l'ostéotomie subie par M.B..., alors qu'elle relevait seulement la circonstance qu'une abstention thérapeutique aurait comporté un risque de complications cardio-vasculaires sensiblement supérieur à la moyenne, d'asthénie prononcée et de somnolences diurnes et qu'il n'existait pas d'alternative thérapeutique moins risquée ; que l'arrêt attaqué doit, par suite, être annulé en tant qu'il statue sur la responsabilité des Hôpitaux universitaires de Strasbourg au titre d'un manquement à leur obligation d'information ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, en application des dispositions de l'article L. 761-1 du code de justice administrative, de mettre à la charge des Hôpitaux universitaires de Strasbourg le versement à M. B...d'une somme de 1 500 euros et à la caisse primaire d'assurance maladie du Bas-Rhin d'une somme de 1 500 euros ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt n° 07NC01458 du 19 mars 2009 de la cour administrative d'appel de Nancy est annulé en tant qu'il statue sur la responsabilité pour défaut d'information des Hôpitaux universitaires de Strasbourg.<br/>
<br/>
Article 2 : L'affaire est renvoyée dans cette mesure à la cour administrative d'appel de Nancy.<br/>
<br/>
Article 3 : Les Hôpitaux universitaires de Strasbourg verseront, au titre de l'article L. 761-1 du code de justice administrative, 1 500 euros à M.  B...et 1 500 euros à la caisse primaire d'assurance maladie du Bas-Rhin.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. A...B..., aux Hôpitaux universitaires de Strasbourg et à la caisse primaire d'assurance maladie du Bas-Rhin.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-08-02-02-01-03 PROCÉDURE. VOIES DE RECOURS. CASSATION. CONTRÔLE DU JUGE DE CASSATION. BIEN-FONDÉ. APPRÉCIATION SOUVERAINE DES JUGES DU FOND. - APPRÉCIATION PAR LE JUGE DU FOND DE L'EXISTENCE D'UNE PERTE DE CHANCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-02-01-01-01-01-04 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICE PUBLIC DE SANTÉ. ÉTABLISSEMENTS PUBLICS D'HOSPITALISATION. RESPONSABILITÉ POUR FAUTE SIMPLE : ORGANISATION ET FONCTIONNEMENT DU SERVICE HOSPITALIER. EXISTENCE D'UNE FAUTE. MANQUEMENTS À UNE OBLIGATION D'INFORMATION ET DÉFAUTS DE CONSENTEMENT. - PRÉJUDICE - PERTE DE CHANCE - ABSENCE - CAS OÙ L'OPÉRATION ENVISAGÉE EST INDISPENSABLE.
</SCT>
<ANA ID="9A"> 54-08-02-02-01-03 Le juge de cassation exerce un contrôle limité à la dénaturation et à l'erreur de droit sur l'appréciation par un juge du fond de l'existence d'une perte de chance.</ANA>
<ANA ID="9B"> 60-02-01-01-01-01-04 Un manquement des médecins à leur obligation d'information n'engage la responsabilité de l'hôpital que dans la mesure où il a privé le patient de la possibilité de se soustraire au risque lié à l'intervention. Dès lors, la faute consistant à ne pas avoir informé le patient du risque que comportait une opération présentant un caractère indispensable n'a fait perdre à ce dernier aucune chance d'échapper au dommage.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
