<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027515280</ID>
<ANCIEN_ID>JG_L_2013_06_000000328634</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/51/52/CETATEXT000027515280.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 03/06/2013, 328634</TITRE>
<DATE_DEC>2013-06-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>328634</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Isabelle Lemesle</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:328634.20130603</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la décision en date du 20 juin 2012 par laquelle le Conseil d'Etat statuant au contentieux a, avant dire droit sur les requêtes n°s 328634 et 328639 enregistrées le 8 juin 2009 au secrétariat du contentieux du Conseil d'Etat, présentées par M. B...A..., demeurant..., tendant à l'annulation des décisions par lesquelles la Commission nationale de l'informatique et des libertés (CNIL) a implicitement refusé de lui communiquer le résultat des recherches entreprises en ce qui concerne les informations le concernant contenues dans le fichier des personnes recherchées ainsi que dans le système informatique national du système d'information Schengen (SIS) et à ce qu'il soit enjoint à cette commission de lui communiquer ces informations, demandé à la CNIL de lui communiquer, dans un délai de deux mois, les éléments d'information relatifs à l'inscription de M. A...dans ces deux fichiers ;<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu la loi n° 78-17 du 6 janvier 1978 ;<br/>
<br/>
              Vu le décret n° 2005-1309 du 20 octobre 2005 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Isabelle Lemesle, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article 41 de la loi du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés : " (...) lorsqu'un traitement intéresse la sûreté de l'Etat, la défense ou la sécurité publique, le droit d'accès s'exerce dans les conditions prévues par le présent article pour l'ensemble des informations qu'il contient. / La demande est adressée à la commission qui désigne l'un de ses membres appartenant ou ayant appartenu au Conseil d'Etat, à la Cour de cassation ou à la Cour des comptes pour mener les investigations utiles et faire procéder aux modifications nécessaires. Celui-ci peut se faire assister d'un agent de la commission. Il est notifié au requérant qu'il a été procédé aux vérifications. / Lorsque la commission constate, en accord avec le responsable du traitement, que la communication des données qui y sont contenues ne met pas en cause ses finalités, la sûreté de l'Etat, la défense ou la sécurité publique, ces données peuvent être communiquées au requérant (...) " ; qu'aux termes de l'article 88 du décret du 20 octobre 2005 pris pour l'application de cette loi : " Aux termes de ses investigations, la commission constate, en accord avec le responsable du traitement, celles des informations susceptibles d'être communiquées au demandeur dès lors que leur communication ne met pas en cause les finalités du traitement, la sûreté de l'Etat, la défense ou la sécurité publique. Elle transmet au demandeur ces informations (...) / Lorsque le responsable du traitement s'oppose à la communication au demandeur de tout ou partie des informations le concernant, la commission l'informe qu'il a été procédé aux vérifications nécessaires. / La commission peut constater, en accord avec le responsable du traitement, que les informations concernant le demandeur doivent être rectifiées ou supprimées et qu'il y a lieu de l'en informer. En cas d'opposition du responsable du traitement, la commission se borne à informer le demandeur qu'il a été procédé aux vérifications nécessaires. / Lorsque le traitement ne contient aucune information concernant le demandeur, la commission informe celui-ci, avec l'accord du responsable du traitement. / En cas d'opposition du responsable du traitement, la commission se borne à informer le demandeur qu'il a été procédé aux vérifications nécessaires. / La réponse de la commission mentionne les voies et délais de recours ouverts au demandeur " ;<br/>
<br/>
              2. Considérant qu'il résulte de ces dispositions que, s'agissant de l'exercice du droit d'accès indirect et de rectification relatif à des données à caractère personnel contenues dans des fichiers intéressant la sûreté de l'Etat, la défense ou la sécurité publique, il revient à la Commission nationale de l'informatique et des libertés (CNIL), à laquelle la demande d'accès aux données est adressée, d'une part, de désigner l'un de ses membres pour mener, en son nom, les investigations utiles et faire procéder aux modifications nécessaires ; que le Conseil d'Etat est compétent, en application du 4° de l'article R. 311-1 du code de justice administrative, tant dans sa rédaction applicable au litige que dans sa rédaction désormais en vigueur, pour connaître en premier et dernier ressort de telles décisions, prises, au titre de sa mission de contrôle et de régulation, par l'une des autorités collégiales à compétence nationale désormais mentionnées à cet article ; que, d'autre part, il appartient à la Commission, en accord avec le responsable du traitement, en premier lieu, de constater les informations qui peuvent être communiquées au demandeur et de les lui transmettre, en deuxième lieu, de constater que les informations concernant le demandeur doivent être rectifiées ou supprimées et de l'en informer ou, en troisième lieu, d'informer le demandeur que le traitement ne comporte aucune information le concernant ; que lorsque le responsable du traitement s'oppose à la communication au demandeur de tout ou partie des informations le concernant, à ce qu'il soit informé que ces informations doivent être rectifiées ou supprimées ou à ce qu'il soit informé que le traitement ne contient aucune information le concernant, l'indication alors fournie au demandeur par le président de la Commission, selon laquelle il a été procédé aux vérifications nécessaires, ne peut être regardée comme l'exercice par la Commission de l'une de ses compétences mais comme la simple notification d'une décision de refus d'accès prise par le responsable du traitement ; que ni l'article R. 311-1 du code de justice administrative ni aucune autre disposition ne donne compétence au Conseil d'Etat pour connaître en premier et dernier ressort d'une telle décision, qui relève, en application de l'article R. 312-1 du même code, de la compétence du tribunal administratif dans le ressort duquel l'autorité qui l'a prise à son siège ; <br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier que M. A...a saisi la CNIL de deux demandes tendant à ce que lui soient communiquées les informations le concernant contenues, d'une part, dans le système informatique national du système d'information Schengen et, d'autre part, dans le fichier des personnes recherchées, traitements dont le ministre de l'intérieur est responsable ; que la lettre du 27 avril 2009 du président de la CNIL informant le conseil de M. A...qu'il avait été procédé aux vérifications nécessaires doit être regardée comme notifiant les décisions individuelles prises par le ministre de l'intérieur refusant à M. A...l'accès aux informations contenues dans ces deux fichiers ; que, dès lors, il y a lieu d'attribuer au tribunal administratif de Paris, le jugement des conclusions de M. A... tendant à l'annulation de ces décisions ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Le jugement des requêtes n°s 328634 et 328639 est attribué au tribunal administratif de Paris.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. B...A..., à la Commission nationale de l'informatique et des libertés et au président du tribunal administratif de Paris.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-05-01-01 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE EN PREMIER RESSORT DES TRIBUNAUX ADMINISTRATIFS. COMPÉTENCE MATÉRIELLE. - INCLUSION - DROIT D'ACCÈS INDIRECT ET DE RECTIFICATION RELATIF À DES DONNÉES À CARACTÈRE PERSONNEL CONTENUES DANS DES FICHIERS INTÉRESSANT LA SÛRETÉ DE L'ETAT, LA DÉFENSE OU LA SÉCURITÉ PUBLIQUE (ART. 41 DE LA LOI DU 6 JANVIER 1978) - CAS DE REFUS D'ACCÈS OPPOSÉ PAR LE RESPONSABLE DU TRAITEMENT - RECOURS CONTRE LA LETTRE DE LA CNIL INDIQUANT AU TITULAIRE DU DROIT D'ACCÈS QU'IL A ÉTÉ PROCÉDÉ AUX VÉRIFICATIONS NÉCESSAIRES [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">26-07-05-02-05 DROITS CIVILS ET INDIVIDUELS. - DROIT D'ACCÈS INDIRECT ET DE RECTIFICATION RELATIF À DES DONNÉES À CARACTÈRE PERSONNEL CONTENUES DANS DES FICHIERS INTÉRESSANT LA SÛRETÉ DE L'ETAT, LA DÉFENSE OU LA SÉCURITÉ PUBLIQUE (ART. 41 DE LA LOI DU 6 JANVIER 1978) - CAS DE REFUS D'ACCÈS OPPOSÉ PAR LE RESPONSABLE DU TRAITEMENT - RECOURS CONTRE LA LETTRE DE LA CNIL INDIQUANT AU TITULAIRE DU DROIT D'ACCÈS QU'IL A ÉTÉ PROCÉDÉ AUX VÉRIFICATIONS NÉCESSAIRES - COMPÉTENCE AU SEIN DE LA JURIDICTION ADMINISTRATIVE - COMPÉTENCE DE PREMIER RESSORT DU TRIBUNAL ADMINISTRATIF - EXISTENCE [RJ2].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-06-06-01-04 PROCÉDURE. JUGEMENTS. CHOSE JUGÉE. CHOSE JUGÉE PAR LA JURIDICTION ADMINISTRATIVE. ÉTENDUE. - DÉCISION AVANT DIRE DROIT SE BORNANT À PRESCRIRE UNE MESURE D'INSTRUCTION - CHOSE JUGÉE SUR LA COMPÉTENCE AU SEIN DE LA JURIDICTION ADMINISTRATIVE - EXCLUSION (SOL. IMPL.) [RJ1].
</SCT>
<ANA ID="9A"> 17-05-01-01 Il résulte des dispositions de l'article 41 de la loi n° 78-17 du 6 janvier 1978, dans sa rédaction issue de la loi n° 2004-801 du 6 août 2004, et de l'article 88 du décret n° 2005-1309 du 20 octobre 2005 pris pour son application que, quand le responsable d'un traitement intéressant la sûreté de l'Etat, la défense ou la sécurité publique oppose un refus à une demande d'accès indirect ou de rectification, l'indication alors fournie au demandeur par le président de la Commission nationale de l'informatique et des libertés (CNIL) selon laquelle il a été procédé aux vérifications nécessaires ne peut être regardée comme l'exercice par la CNIL de l'une de ses compétences, mais comme la simple notification d'une décision de refus d'accès prise par le responsable du traitement. Cette décision relève, en cas de litige, de la compétence du tribunal administratif dans le ressort duquel l'autorité qui l'a prise à son siège.</ANA>
<ANA ID="9B"> 26-07-05-02-05 Il résulte des dispositions de l'article 41 de la loi n° 78-17 du 6 janvier 1978, dans sa rédaction issue de la loi du 6 août 2004, et de l'article 88 du décret n° 2005-1309 du 20 octobre 2005 pris pour son application que, quand le responsable d'un traitement intéressant la sûreté de l'Etat, la défense ou la sécurité publique oppose un refus à une demande d'accès indirect ou de rectification, l'indication alors fournie au demandeur par le président de la Commission nationale de l'informatique et des libertés (CNIL) selon laquelle il a été procédé aux vérifications nécessaires ne peut être regardée comme l'exercice par la CNIL de l'une de ses compétences, mais comme la simple notification d'une décision de refus d'accès prise par le responsable du traitement. Cette décision relève, en cas de litige, de la compétence du tribunal administratif dans le ressort duquel l'autorité qui l'a prise à son siège.</ANA>
<ANA ID="9C"> 54-06-06-01-04 Une décision avant dire droit qui se borne à prescrire une mesure d'instruction ne peut être regardée comme ayant implicitement statué sur la compétence à l'intérieur de la juridiction administrative.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp., pour un jugement avant dire droit ordonnant une expertise, CE, 11 octobre 1972, Ministre de l'équipement et du logement c/ Judeaux, n° 84122, p. 630 ; pour des avant dire droit rejetant expressément une exception d'incompétence de la juridiction administrative, CE, 31 janvier 1969, Commune de Cabourg, n° 71615, T. pp. 924-933 et CE, 19 octobre 1979, Société d'économie mixte d'équipement de la ville d'Aix-en-Provence, n° 02434, T. pp. 845-849-884-910 ; pour l'autorité de la chose jugée qui s'attache à un avant dire droit posant à la Cour de justice de l'Union européenne une question préjudicielle en tant qu'il statue sur la recevabilité des conclusions présentées au fond, CE, 10 décembre 2012, Ministre du budget, des comptes publics et de la fonction publique c/ Société Rhodia, n° 317074, à mentionner aux Tables.,,,[RJ2] Rappr. CE, 23 juin 1993, Ruwayha, n° 138571, T. p. 180, à propos des informations contenues dans un fichier géré par les renseignement généraux sous l'empire du décret n° 91-1051 du 14 octobre 1991. Comp. avec la sol. impl. retenue par CE, 29 décembre 1997, Thorel, n° 140325, T. pp. 625 et autres sur un autre point ; CE, Assemblée, 6 novembre 2002, M. Moon Sun Myung c/ CNIL, n° 194295, p. 380, sous l'empire de l'ancienne rédaction de l'article 39 de la loi du 6 janvier 1978.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
