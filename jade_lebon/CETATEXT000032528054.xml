<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032528054</ID>
<ANCIEN_ID>JG_L_2016_05_000000383768</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/52/80/CETATEXT000032528054.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 11/05/2016, 383768, Publié au recueil Lebon</TITRE>
<DATE_DEC>2016-05-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>383768</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>HAAS ; SCP BOULLOCHE ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Frédéric Dieu</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:383768.20160511</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              1°) M. B...a demandé au tribunal administratif de Bordeaux d'annuler la délibération du 24 octobre 2011 par laquelle le conseil municipal de la commune de Bordeaux a autorisé la signature du contrat de partenariat conclu entre la commune et la société Stade Bordeaux Atlantique et ayant pour objet la conception, la construction, l'entretien, la maintenance et l'exploitation du nouveau stade de Bordeaux Atlantique. Par un jugement n° 1105078 du 19 décembre 2012, le tribunal administratif de Bordeaux a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 13BX00563 du 17 juin 2014, la cour administrative d'appel de Bordeaux a rejeté l'appel de M. B...tendant à l'annulation de ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 18 août et 18 novembre 2014 au secrétariat du contentieux du Conseil d'Etat sous le n° 383768, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ; <br/>
<br/>
              3°) de mettre à la charge de la commune de Bordeaux le versement d'une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 18 août et 18 novembre 2014 au secrétariat du contentieux du Conseil d'Etat sous le n° 383769, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ; <br/>
<br/>
              3°) de mettre à la charge de la commune de Bordeaux le versement d'une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Dieu, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Haas, avocat de M.B..., à la SCP Boulloche, avocat de la commune de Bordeaux et à la SCP Piwnica, Molinié, avocat de la société Stade Bordeaux Atlantique ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 8 avril 2016, présentée sous le n° 383768 par la commune de Bordeaux ;<br/>
<br/>
<br/>
<br/>1. Considérant que les pourvois visés ci-dessus présentent à juger des questions semblables ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la commune de Bordeaux a décidé, par une délibération du 31 mai 2010, le principe du recours à un contrat de partenariat et la mise en oeuvre d'une procédure de dialogue compétitif en vue de l'attribution d'un contrat ayant pour objet la conception, la construction, l'entretien, la maintenance et éventuellement l'exploitation, ainsi que le financement partiel du nouveau stade de Bordeaux ; qu'à l'issue de la procédure de dialogue compétitif, un projet de contrat de partenariat a été établi entre la commune de Bordeaux et la société Stade Bordeaux Atlantique ; que, par deux délibérations du 24 octobre 2011, le conseil municipal de Bordeaux a, d'une part, approuvé les termes de ce projet de contrat de partenariat et autorisé le maire de la commune à signer le contrat et, d'autre part, autorisé le maire à signer l'accord autonome ainsi que l'acte d'acceptation de cession de créances et tous actes et documents inhérents à l'exécution du contrat ; que, par les arrêts attaqués, la cour administrative d'appel de Bordeaux a rejeté les appels de M. B...dirigés contre les jugement du 19 décembre 2012 par lesquels le tribunal administratif de Bordeaux a rejeté ses demandes tendant à l'annulation de ces délibérations ;<br/>
<br/>
              Sur le pourvoi n° 383768 :<br/>
<br/>
              3. Considérant que le pourvoi n° 383768 de M. B...est dirigé contre l'arrêt n° 13BX00563 du 17 juin 2014 par lequel la cour administrative d'appel de Bordeaux a rejeté son appel relatif à la délibération du 24 octobre 2011 par laquelle le conseil municipal de Bordeaux a approuvé les termes du projet de contrat de partenariat ;<br/>
<br/>
              Sur le moyen relatif à l'information délivrée aux conseillers municipaux :<br/>
<br/>
              4. Considérant qu'aux termes de l'article L. 2121-13 du code général des collectivités territoriales : " Tout membre du conseil municipal a le droit, dans le cadre de sa fonction, d'être informé des affaires de la commune qui font l'objet d'une délibération " ; qu'aux termes de l'article L. 1414-10 du code général des collectivités territoriales : " L'assemblée délibérante ou l'organe délibérant autorise la signature du contrat de partenariat par l'organe exécutif ou déclare la procédure infructueuse. A cette fin, le projet de délibération est accompagné d'une information comportant le coût prévisionnel global du contrat, en moyenne annuelle, pour la personne publique et l'indication de la part que ce coût représente par rapport à la capacité de financement annuelle de la personne publique. Cette part est mesurée dans des conditions définies par décret " ; qu'aux termes de l'article D. 1414-4 du même code : " La part mentionnée au deuxième alinéa de l'article L. 1414-10 est mesurée par le ratio suivant : coût moyen annuel du contrat / recettes réelles de fonctionnement. Le coût moyen annuel du contrat prend en compte la totalité des coûts facturés par le titulaire du contrat à la personne publique dans le cadre de sa mise en oeuvre sur toute sa durée. Le cocontractant pressenti fournit les éléments nécessaires à l'établissement de ce coût " ;<br/>
<br/>
              5. Considérant que l'obligation instituée par les dispositions précitées des articles L. 1414-10 et D. 1414-4 du code général des collectivités territoriales d'assortir tout projet de délibération autorisant la signature d'un contrat de partenariat d'une information relative au coût prévisionnel global du contrat de partenariat en moyenne annuelle et à la part que ce coût représente par rapport à la capacité de financement annuelle de la personne publique vise à informer les élus des coûts auxquels la collectivité territoriale est exposée en raison de la conclusion d'un tel contrat pendant toute sa durée ; que ce coût doit prendre en compte, d'un côté, l'ensemble des sommes payées par la personne publique au titulaire à raison du contrat, de l'autre, les recettes procurées par le contrat à la personne publique ; <br/>
<br/>
              6. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la délibération du 24 octobre 2011 approuvant les termes du contrat de partenariat relatif à la réalisation du nouveau stade de Bordeaux est assortie d'une annexe indiquant le coût prévisionnel du contrat de partenariat en moyenne annuelle pour la personne publique et la part que ce coût représente par rapport à la capacité de financement annuelle de la personne publique ; que cette annexe indique que le coût prévisionnel du contrat prend en compte, d'une part, les redevances R1, R2, R3 et R4 versées par la commune de Bordeaux à la société Stade Bordeaux Atlantique et, d'autre part, les recettes nettes garanties procurées à cette dernière et tirées de l'exploitation du stade, les recettes additionnelles partagées représentant 60 % des recettes perçues au-delà du montant des recettes nettes garanties de 300 000 euros ainsi que la redevance versée par le club résident d'un montant de 3 850 000 euros hors taxes et de l'intéressement de ce club de 200 000 euros ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui a été dit au point 5 qu'en jugeant que, hormis les redevances payées par la personne publique pour rémunérer le titulaire du contrat des prestations qu'il a effectuées à raison de ce dernier ainsi que les recettes procurées par le contrat et reversées à la personne publique, les autres sommes qui pourraient être versées au partenaire ou à la personne publique en cours d'exécution du contrat n'ont pas à être intégrées dans le coût prévisionnel global dès lors qu'elles ne sont pas liées aux prestations confiées au partenaire et préfinancées par lui et ne participent donc pas à l'endettement de la personne publique à raison du contrat de partenariat et en estimant, pour ce motif, que, ni la " subvention " d'un montant de 17 millions d'euros qui devait être versée par la commune de Bordeaux à la société Stade Bordeaux Atlantique à titre d'avance sur rémunération ni les impôts et taxes qui devaient être acquittés par cette dernière puis refacturés par elle à la commune en vertu du contrat n'étaient au nombre des sommes qui devaient être intégrées dans le calcul hors taxe du coût prévisionnel global du contrat, la cour administrative d'appel de Bordeaux a procédé à une interprétation erronée des dispositions précitées des articles L. 1414-10 et D. 1414-4 du code général des collectivités territoriales et commis une erreur de droit ; <br/>
<br/>
              9. Considérant qu'il résulte de ce qui précède que M. B...est fondé, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, à demander l'annulation de l'arrêt n° 13BX00563 du 17 juin 2014 par lequel la cour administrative d'appel de Bordeaux a rejeté son appel relatif à la délibération du 24 octobre 2011 par laquelle le conseil municipal de Bordeaux a approuvé les termes de ce projet de contrat de partenariat et autorisé le maire de la commune à signer le contrat ;<br/>
<br/>
              10. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions du premier alinéa de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Sur les conclusions tendant à l'annulation de la délibération du 24 octobre 2011 autorisant la signature du contrat de partenariat :<br/>
<br/>
              11. Considérant qu'ainsi qu'il a été dit ci-dessus, la subvention d'un montant de 17 millions d'euros versée, à titre d'avance sur rémunération, par la commune de Bordeaux au titulaire du contrat litigieux ainsi que les impôts acquittés par le titulaire puis refacturés par lui à la commune de Bordeaux doivent être regardés comme des coûts facturés par le titulaire à la personne publique, au sens des dispositions des articles L. 1414-10 et D. 1414-4 du code général des collectivités territoriales ; qu'il ressort des pièces du dossier que ni la subvention  de 17 millions d'euros ni le montant estimatif annuel de 2,6 millions d'euros, correspondant au coût des impôts refacturés à la commune par le titulaire, n'ont été pris en compte dans le calcul du coût prévisionnel global du contrat, en moyenne annuelle, pour la personne publique, qui figurait en annexe du projet de délibération approuvant les termes du contrat ; que cette omission, qui caractérise une insuffisance d'information des membres du conseil municipal sur les conséquences financières du recours à un contrat de partenariat, a privé effectivement les membres du conseil municipal de la garantie octroyée par l'article L. 1414-10 du code général des collectivités territoriales ; que, par suite, M. B...est fondé à soutenir que c'est à tort que le tribunal administratif de Bordeaux a rejeté sa demande tendant à l'annulation de cette délibération ;<br/>
<br/>
              12. Considérant qu'il résulte de ce qui précède que M. B...est fondé à demander l'annulation de la délibération du 24 octobre 2011 par laquelle le conseil municipal de la commune de Bordeaux a autorisé la signature du contrat de partenariat conclu entre la commune et la société Stade Bordeaux Atlantique ;<br/>
<br/>
              Sur les conclusions à fin d'injonction :<br/>
<br/>
              13. Considérant que l'annulation d'un acte détachable d'un contrat n'implique pas nécessairement l'annulation de ce contrat ; qu'il appartient au juge de l'exécution, après avoir pris en considération la nature de l'illégalité commise, soit de décider que la poursuite de l'exécution du contrat est possible, éventuellement sous réserve de mesures de régularisation prises par la personne publique ou convenues entre les parties, soit, après avoir vérifié que sa décision ne portera pas une atteinte excessive à l'intérêt général, d'enjoindre à la personne publique de résilier le contrat, le cas échéant avec un effet différé, soit, eu égard à une illégalité d'une particulière gravité, d'inviter les parties à résoudre leurs relations contractuelles ou, à défaut d'entente sur cette résolution, à saisir le juge du contrat afin qu'il en règle les modalités s'il estime que la résolution peut être une solution appropriée ; <br/>
<br/>
              14. Considérant que l'illégalité de la délibération attaquée tenant à l'insuffisance de l'information des conseillers municipaux sur le coût prévisionnel global que représente le contrat litigieux pour la commune de Bordeaux en moyenne annuelle a affecté les conditions dans lesquelles le conseil municipal a donné son autorisation à la signature du contrat ; que, par suite, à défaut pour la commune, dans un délai de quatre mois à compter de la notification de la présente décision, de régulariser la signature du contrat par une délibération du conseil municipal ayant pour objet de confirmer l'approbation des termes du projet de contrat  et l'autorisation donnée au maire de la commune de Bordeaux de le signer, il y a lieu, eu égard à la gravité du vice entachant la délibération annulée, d'enjoindre à la commune de Bordeaux de résilier le contrat ; <br/>
<br/>
              Sur le pourvoi n° 383769 :<br/>
<br/>
              15. Considérant que le pourvoi n° 383769 de M. B...est dirigé contre l'arrêt n° 13BX00564 du 17 juin 2014 par lequel la cour administrative d'appel de Bordeaux a rejeté son appel relatif à la délibération du 24 octobre 2011 par laquelle le conseil municipal de Bordeaux a autorisé le maire de la commune à signer l'accord autonome ainsi que l'acte d'acceptation de cession de créances et tous actes et documents inhérents à l'exécution du contrat de partenariat litigieux ;<br/>
<br/>
              16. Considérant, en premier lieu, que la cour administrative d'appel de Bordeaux n'a pas commis d'erreur de droit en jugeant que l'absence de mention, dans les différents documents adressés aux conseillers municipaux préalablement à la séance du conseil municipal au cours de laquelle devait être débattue la délibération autorisant la conclusion de l'accord autonome et de l'acte d'acceptation de cession de créances, de l'identité de l'établissement financier mandataire des établissements bancaires parties à ces contrats n'a pas conduit à une méconnaissance du droit des conseillers municipaux, énoncé à l'article L. 2121-13 du code général des collectivités territoriales, d'être informés, dans le cadre de leurs fonctions, des affaires de la commune qui font l'objet d'une délibération, dès lors que les conseillers ont eu connaissance de l'identité des établissements parties aux contrats dont il leur était demandé d'autoriser la signature ;<br/>
<br/>
              17. Considérant, en deuxième lieu, que la convention tripartite appelée " accord autonome ", conclue par la personne publique, le titulaire du contrat de partenariat et les établissements bancaires, a pour objet de garantir la continuité du financement du projet, objet du contrat de partenariat, en cas de recours des tiers contre ce contrat ou l'un de ses actes détachables et d'annulation ou de déclaration ou de constatation de nullité du contrat de partenariat par le juge ; que même si elle constitue l'accessoire du contrat de partenariat conclu entre la commune de Bordeaux et la société Stade Bordeaux Atlantique, cette convention met à la charge des parties signataires des obligations indépendantes de celles nées du contrat de partenariat et ne constitue pas, par suite, en elle-même un contrat de la commande publique soumis au respect des principes de liberté d'accès à la commande publique, d'égalité de traitement des candidats et de transparence des procédures ; qu'en conséquence, la cour n'a pas commis d'erreur de droit en écartant le moyen tiré de la méconnaissance par la commune de Bordeaux, lors de la passation de cette convention tripartite, des obligations d'égalité de traitement et de transparence auxquelles était subordonnée la conclusion du contrat de partenariat ;<br/>
<br/>
              18. Considérant, en troisième lieu, qu'ainsi qu'il a été dit au point 16, si la convention tripartite appelée " accord autonome " constitue l'accessoire du contrat de partenariat, elle met à la charge des parties signataires des obligations indépendantes de celles nées du contrat de partenariat conclu entre la commune de Bordeaux et la société Stade Bordeaux Atlantique ; que, par suite, cette convention ne constitue pas un contrat de partenariat visé par l'article L. 1414-12 du code général des collectivités territoriales, qui énumère les clauses que doivent nécessairement comporter les contrats  de partenariat ; que, par voie de conséquence, la cour administrative d'appel de Bordeaux n'a pas commis d'erreur de droit en jugeant que M. B...ne pouvait utilement invoquer la méconnaissance des dispositions de cet article pour contester la légalité de la délibération attaquée ; <br/>
<br/>
              19. Considérant, en dernier lieu, qu'en vertu des règles générales applicables aux contrats administratifs, l'étendue et les modalités de l'indemnisation due par la personne publique à son cocontractant en cas d'annulation ou de résiliation pour un motif d'intérêt général du contrat peuvent être déterminées par les stipulations du contrat, sous réserve qu'il n'en résulte pas, au détriment de la personne publique, une disproportion manifeste entre l'indemnité ainsi fixée et le montant du préjudice résultant, pour le titulaire du contrat, des dépenses qu'il a exposées et du gain dont il a été privé ; que ce principe, qui s'applique à tous les contrats administratifs, découle de l'interdiction faite aux personnes publiques de consentir des libéralités ; qu'en jugeant que la convention tripartite en cause, en déterminant à son article 6 la garantie due par la commune de Bordeaux, en cas de recours des tiers contre le contrat de partenariat ou de l'un de ses actes détachables, sur la base de l'ensemble des dépenses utilement exposées par la société Stade Bordeaux Atlantique pour la bonne exécution du contrat de partenariat, y compris les frais financiers engagés par cette dernière, n'avait pas, en l'espèce, pour effet de contraindre la personne publique à verser une libéralité prohibée par la règle d'ordre public rappelée ci-dessus, la cour administrative d'appel de Bordeaux n'a pas commis d'erreur de droit ; que la cour, eu égard à l'absence de caractère manifestement disproportionné des droits à indemnité que les signataires de la convention tenaient de l'article 6 de celle-ci, n'a pas non plus commis d'erreur de qualification juridique ;<br/>
<br/>
              20. Considérant qu'il résulte de ce qui précède que le pourvoi n° 383769 de M. B... doit être rejeté ;<br/>
<br/>
              Sur les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              21. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de M.B..., qui n'est pas, dans la présente instance, la partie perdante, les sommes que demandent la commune de Bordeaux et la société Stade Bordeaux Atlantique au titre des frais exposés par elles et non compris dans les dépens ; qu'il y a lieu, en revanche, de mettre à la charge solidaire de ces dernières une somme de 3 000 euros à verser à M. B...au titre de la procédure suivie devant le Conseil d'Etat, la cour administrative d'appel de Bordeaux et le tribunal administratif de Bordeaux ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt n° 13BX00563 du 17 juin 2004 de la cour administrative d'appel de Bordeaux, le jugement n° 1105078 du 19 décembre 2012 du tribunal administratif de Bordeaux et la délibération du 24 octobre 2011 du conseil municipal de Bordeaux approuvant les termes du projet de contrat de partenariat et autorisant la signature du contrat sont annulés.<br/>
Article 2 : Il est enjoint à la commune de Bordeaux, à défaut pour elle, dans un délai de quatre mois à compter de la notification de la présente décision, de régulariser la signature du contrat par une décision du conseil municipal ayant pour objet de confirmer l'approbation des termes du projet de contrat et l'autorisation donnée au maire de la commune de Bordeaux de le signer, de résilier le contrat de partenariat.<br/>
Article 3 : Le pourvoi n° 383769 de M. B...est rejeté.<br/>
Article 4 : La commune de Bordeaux et la société Stade Bordeaux Atlantique verseront une somme de 3 000 euros à M. B...au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : Les conclusions présentées par la commune de Bordeaux et la société Stade Bordeaux Atlantique au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 6 : La présente décision sera notifiée à M. A...B..., à la commune de Bordeaux et à la société Stade Bordeaux Atlantique.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-02-03 MARCHÉS ET CONTRATS ADMINISTRATIFS. FORMATION DES CONTRATS ET MARCHÉS. APPROBATION. - DÉLIBÉRATION AUTORISANT LA SIGNATURE D'UN CONTRAT DE PARTENARIAT - OBLIGATION D'INFORMATION SUR LE COÛT PRÉVISIONNEL GLOBAL DU CONTRAT - PORTÉE.
</SCT>
<ANA ID="9A"> 39-02-03 L'obligation instituée par les dispositions des articles L. 1414-10 et D. 1414-4 du code général des collectivités territoriales (CGCT) d'assortir tout projet de délibération autorisant la signature d'un contrat de partenariat d'une information relative au coût prévisionnel global du contrat de partenariat en moyenne annuelle et à la part que ce coût représente par rapport à la capacité de financement annuelle de la personne publique vise à informer les élus des coûts auxquels la collectivité territoriale est exposée en raison de la conclusion d'un tel contrat pendant toute sa durée. Ce coût doit prendre en compte, d'un côté, l'ensemble des sommes payées par la personne publique au titulaire à raison du contrat, de l'autre, les recettes procurées par le contrat au titulaire.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
