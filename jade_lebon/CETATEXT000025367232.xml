<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025367232</ID>
<ANCIEN_ID>JG_L_2012_02_000000353357</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/36/72/CETATEXT000025367232.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 08/02/2012, 353357, Publié au recueil Lebon</TITRE>
<DATE_DEC>2012-02-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>353357</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Nicolas Labrune</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Julien Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:353357.20120208</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 13 octobre 2011 au secrétariat du contentieux du Conseil d'Etat, présentée par M. A...B..., élisant domicile chez...,; M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 22 septembre 2011 par laquelle la commission des sondages a rejeté les demandes formulées dans sa réclamation du 12 septembre 2011 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 800 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 26 janvier 2012, présentée par M.  B...;<br/>
<br/>
              Vu la loi n° 77-808 du 19 juillet 1977, modifiée notamment par la loi n° 2002-214 du 19 février 2002 ;<br/>
<br/>
              Vu la loi n° 78-753 du 17 juillet 1978 ;<br/>
<br/>
              Vu le décret n° 78-79 du 25 janvier 1978 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Nicolas Labrune, Auditeur,<br/>
<br/>
              - les conclusions de M. Julien Boucher, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant que le journal " Le Parisien " a publié le 7 septembre 2011 un sondage, réalisé par la société Harris Interactive, relatif aux intentions de vote au premier tour de l'élection présidentielle de mai 2012 ; que M. B...a déposé le 12 septembre 2011 auprès de la commission des sondages une réclamation motivée contre ce sondage, sur le fondement des articles 11 et suivants du décret du 25 janvier 1978 pris pour l'application de la loi du 19 juillet 1977 relative à la publication et à la diffusion de certains sondages d'opinion ; que la commission des sondages a rejeté cette réclamation par une décision du 22 septembre 2011 ; que M. B...demande l'annulation de cette décision ;<br/>
<br/>
              Considérant, en premier lieu, qu'aux termes du dernier alinéa de l'article 3 de la loi du 19 juillet 1977 relative à la publication et à la diffusion de certains sondages d'opinion, dans sa rédaction issue de la loi n° 2002-214 du 19 février 2002 : " Toute personne a le droit de consulter auprès de la commission des sondages la notice prévue par le présent article. " ; que si ces dispositions instaurent un régime particulier de consultation sur place de la notice d'un sondage, la communication d'une notice de sondage, document administratif, est quant à elle régie par les dispositions de la loi du 17 juillet 1978 portant diverses mesures d'amélioration des relations entre l'administration et le public et diverses dispositions d'ordre administratif, social et fiscal ; qu'en tout état de cause il ressort des pièces du dossier et n'est d'ailleurs pas contesté que M. B...a obtenu le 9 septembre 2011 copie de la notice du sondage qu'il conteste ;<br/>
<br/>
              Considérant, en deuxième lieu, que M. B...soutient que la commission des sondages aurait commis une erreur d'appréciation en refusant de lui communiquer l'ensemble des informations sur la base desquelles le sondage du 7 septembre 2011 a été publié ainsi que tous les détails de la méthode de redressement utilisée pour corriger les résultats du sondage le concernant ; que si, en application de l'article 4 de la loi du 19 juillet 1977, " L'organisme ayant réalisé un sondage (...) tient à la disposition de la commission des sondages (...) les documents sur la base desquels le sondage a été publié ou diffusé ", la communication de ces documents, qui, lorsqu'ils sont en possession de la commission, sont, en vertu de l'article 1er de la loi du 17 juillet 1978 et dès lors qu'ils sont reçus par la commission dans le cadre de ses missions de service public, des documents administratifs communicables sous réserve des secrets protégés par la loi et notamment du secret des affaires, est régie par les dispositions de la loi du 17 juillet 1978 ; qu'en tout état de cause, en l'espèce, le refus opposé par la commission des sondages, qui a d'ailleurs partiellement répondu à la demande de M.B..., ne saurait être utilement contesté, dès lors qu'il ne ressort pas des pièces du dossier que la commission aurait eu en sa possession d'autres documents que ceux qu'elle a communiqués au requérant et qu'il ne lui appartenait pas de procéder à des recherches complémentaires pour satisfaire cette demande ;<br/>
<br/>
              Considérant, en troisième lieu, que la commission des sondages ne tient d'aucune disposition législative ou réglementaire le pouvoir d'interdire la publication ou la diffusion d'un sondage ; qu'elle ne pouvait, par suite, que rejeter la demande de M.  B...tendant à ce qu'elle interdise la diffusion du sondage qu'il contestait ;<br/>
<br/>
              Considérant, en quatrième lieu, que selon l'article 8 de la loi du 19 juillet 1977 la commission des sondages dispose de " tout pouvoir pour vérifier que les sondages (...) ont été réalisés et que leur vente s'est effectuée conformément à la loi et aux textes réglementaires applicables " ; qu'en vertu de l'article 9 de cette même loi les organes d'information qui auraient méconnu les dispositions légales et réglementaires applicables sont tenus de publier sans délai les mises au point demandées par la commission des sondages, qui peut en outre, à tout moment, faire programmer et diffuser ces mises au point par les sociétés nationales de radiodiffusion et de télévision ; qu'il résulte de ces dispositions qu'il appartient à la commission des sondages de demander la publication ou la diffusion d'une mise au point appropriée lorsque les conditions de réalisation d'un sondage par un organisme ou de publication d'un sondage par un organe d'information ont porté une atteinte suffisamment caractérisée aux dispositions légales et réglementaires dont elle a pour mission d'assurer l'application en compromettant, préalablement à des consultations électorales, la qualité, l'objectivité ou la bonne compréhension par le public de ce sondage ; <br/>
<br/>
              Considérant qu'aux termes de l'article 2 de la loi du 19 juillet 1977 : " La publication et la diffusion de tout sondage (...) doivent être accompagnées des indications suivantes (...) : (...) / Une mention indiquant le droit de toute personne à consulter la notice prévue par l'article 3 " ; que l'article 3 de cette même loi dispose que : " Avant la publication ou la diffusion de tout sondage tel que défini à l'article 1er, l'organisme qui l'a réalisé doit procéder au dépôt auprès de la commission des sondages instituée en application de l'article 5 de la présente loi d'une notice précisant notamment : / L'objet du sondage ; / La méthode selon laquelle les personnes interrogées ont été choisies, le choix et la composition de l'échantillon ; / Les conditions dans lesquelles il a été procédé aux interrogations ; / Le texte intégral des questions posées ; / La proportion des personnes n'ayant pas répondu à chacune des questions ; / Les limites d'interprétation des résultats publiés ; / S'il y a lieu, la méthode utilisée pour en déduire les résultats de caractère indirect qui seraient publiés. / La commission des sondages peut ordonner la publication par ceux qui ont procédé à la publication ou à la diffusion d'un sondage tel que défini à l'article 1er des indications figurant dans la notice qui l'accompagne ou de certaines d'entre elles. / Toute personne a le droit de consulter auprès de la commission des sondages la notice prévue par le présent article. " ; que l'article 3-1 de cette même loi dispose que : " A l'occasion de la publication et de la diffusion de tout sondage (...), les données relatives aux réponses des personnes interrogées doivent être accompagnées du texte intégral des questions posées " ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier que le sondage Harris Interactive relatif aux intentions de vote au premier tour de l'élection présidentielle de 2012 n'a été accompagné, lors de sa publication dans le journal " Le Parisien " du 7 septembre 2011, ni d'une mention indiquant le droit de toute personne à consulter sa notice, ni du texte intégral des questions posées ; que la notice déposée auprès de la commission des sondages préalablement à la publication de ce sondage ne comportait ni le texte intégral des questions posées, ni la proportion des personnes n'ayant pas répondu à chacune des questions, ni les limites d'interprétation des résultats publiés ; que toutefois ces irrégularités, pour regrettables qu'elles soient, n'ont empêché ni la commission des sondages d'exercer son contrôle sur le sondage contesté, ni les personnes concernées d'exercer les droits que la loi leur reconnaît auprès d'elle ; que la commission, en dépit des carences relevées, a demandé et obtenu, dans l'exercice de ses pouvoirs de contrôle, l'ensemble des informations nécessaires à l'appréciation de la régularité du sondage ; que la présentation par le journal " Le Parisien " des questions posées n'a pas, au regard de leur formulation intégrale et compte tenu de l'objet du sondage, déformé pour le lecteur la portée des résultats de l'enquête ; qu'ainsi en estimant, sur la base de ces éléments, que l'ensemble des méconnaissances de la loi n'avaient pas compromis la qualité, l'objectivité, ou la bonne compréhension par le public de ce sondage et en rejetant la demande de M.  B...tendant à ce qu'elle demande la publication d'une mise au point, la commission des sondages n'a commis aucune erreur manifeste d'appréciation ;<br/>
<br/>
              Considérant que, n'ayant pas relevé de violation de la loi susceptible d'entraîner la demande de publication d'une mise au point, la commission des sondages a rejeté implicitement mais nécessairement la demande de M. B...tendant à ce qu'elle avise le procureur de la République des faits rappelés ci dessus ; qu'elle n'a, ce faisant, pas entaché sa décision d'erreur manifeste ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que M. B...n'est pas fondé à demander l'annulation de la décision attaquée ;<br/>
<br/>
              Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>				D E C I D E  :<br/>
              				--------------<br/>
<br/>
Article 1er : La requête de M. B...est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. A...B..., à la commission des sondages et à la société Harris Interactive.<br/>
Copie en sera adressée pour information au garde des sceaux, ministre de la justice et des libertés.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-05-02 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE DU CONSEIL D'ETAT EN PREMIER ET DERNIER RESSORT. - RECOURS DIRIGÉ CONTRE LA DÉCISION PAR LAQUELLE LA COMMISSION DES SONDAGES REJETTE UNE RÉCLAMATION MOTIVÉE CONTRE UN SONDAGE - COMPÉTENCE DU CONSEIL D'ETAT EN PREMIER RESSORT EN VERTU DE L'ARTICLE 10 DE LA LOI DU 19 JUILLET 1977 (SOL. IMPL.).
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">26-06-01 DROITS CIVILS ET INDIVIDUELS. ACCÈS AUX DOCUMENTS ADMINISTRATIFS. ACCÈS AUX DOCUMENTS ADMINISTRATIFS AU TITRE DE LA LOI DU 17 JUILLET 1978. - NOTICE D'UN SONDAGE ET DOCUMENTS, EN POSSESSION DE LA COMMISSION DES SONDAGES, SUR LA BASE DESQUELS CE SONDAGE A ÉTÉ PUBLIÉ OU DIFFUSÉ - DOCUMENTS ADMINISTRATIFS DONT LA COMMUNICATION EST RÉGIE PAR LA LOI DU 17 JUILLET 1978 - CONSÉQUENCE - DOCUMENTS COMMUNICABLES SOUS RÉSERVE DES SECRETS PROTÉGÉS PAR LA LOI ET NOTAMMENT DU SECRET DES AFFAIRES.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">28-005-02 ÉLECTIONS ET RÉFÉRENDUM. DISPOSITIONS GÉNÉRALES APPLICABLES AUX ÉLECTIONS. CAMPAGNE ET PROPAGANDE ÉLECTORALES. - COMMISSION DES SONDAGES - CONDITIONS DE DEMANDE DE PUBLICATION D'UNE MISE AU POINT - MÉCONNAISSANCES DE LOI DU 19 JUILLET 1977 COMPROMETTANT, PRÉALABLEMENT À DES CONSULTATIONS ÉLECTORALES, LA QUALITÉ, L'OBJECTIVITÉ, OU LA BONNE COMPRÉHENSION PAR LE PUBLIC DE CE SONDAGE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">53-03 PRESSE. PUBLICATION DANS LA PRESSE DES SONDAGES ÉLECTORAUX (LOI DU 19 JUILLET 1977). - COMMISSION DES SONDAGES - 1) CONDITIONS DE DEMANDE DE PUBLICATION D'UNE MISE AU POINT - MÉCONNAISSANCE DE LA LOI DU 19 JUILLET 1977 COMPROMETTANT LA QUALITÉ, L'OBJECTIVITÉ, OU LA BONNE COMPRÉHENSION PAR LE PUBLIC DE CE SONDAGE - 2) CONTRÔLE DU JUGE ADMINISTRATIF SUR L'APPRÉCIATION PORTÉE PAR LA COMMISSION - CONTRÔLE RESTREINT.
</SCT>
<SCT ID="8E" TYPE="PRINCIPAL">54-07-02-04 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR. APPRÉCIATIONS SOUMISES À UN CONTRÔLE RESTREINT. - APPRÉCIATION PORTÉE PAR LA COMMISSION DES SONDAGES SUR LE POINT DE SAVOIR SI LES MÉCONNAISSANCES DE LA LOI DU 19 JUILLET 1977 PAR UN SONDAGE N'ONT PAS COMPROMIS LA QUALITÉ, L'OBJECTIVITÉ, OU LA BONNE COMPRÉHENSION PAR LE PUBLIC DE CE SONDAGE ET NE RENDENT PAS, PAR SUITE, NÉCESSAIRE LA PUBLICATION D'UNE MISE AU POINT.
</SCT>
<ANA ID="9A"> 17-05-02 Le Conseil d'Etat est compétent, en vertu de l'article 10 de la loi n° 77-808 du 19 juillet 1977 relative à la publication et à la diffusion de certains sondages d'opinion, pour connaître en premier et dernier ressort d'un recours dirigé contre la décision par laquelle la commission des sondages rejette une réclamation motivée contre un sondage (sol. impl.).</ANA>
<ANA ID="9B"> 26-06-01 Si les dispositions de l'article 3 de la loi n° 77-808 du 19 juillet 1977 relative à la publication et à la diffusion de certains sondages d'opinion, dans sa rédaction issue de la loi n° 2002-214 du 19 février 2002, instaurent un régime particulier de consultation sur place de la notice d'un sondage, la communication d'une notice de sondage, qui constitue un document administratif au sens de la loi n° 78-753 du 17 juillet 1978 portant diverses mesures d'amélioration des relations entre l'administration et le public et diverses dispositions d'ordre administratif, social et fiscal, tout comme celle des documents sur la base desquels ce sondage a été publié ou diffusé, lorsqu'ils sont en possession de la commission des sondages, sont régies par les dispositions de la loi du 17 juillet 1978. Par suite, ces documents sont communicables sous réserve des secrets protégés par la loi et notamment du secret des affaires.</ANA>
<ANA ID="9C"> 28-005-02 Il appartient à la commission des sondages de demander la publication ou la diffusion d'une mise au point appropriée lorsque les conditions de réalisation d'un sondage par un organisme ou de publication d'un sondage par un organe d'information ont porté une atteinte suffisamment caractérisée aux dispositions légales et réglementaires dont elle a pour mission d'assurer l'application en compromettant, préalablement à des consultations électorales, la qualité, l'objectivité ou la bonne compréhension par le public de ce sondage.</ANA>
<ANA ID="9D"> 53-03 1) Il appartient à la commission des sondages de demander la publication ou la diffusion d'une mise au point appropriée lorsque les conditions de réalisation d'un sondage par un organisme ou de publication d'un sondage par un organe d'information ont porté une atteinte suffisamment caractérisée aux dispositions légales et réglementaires dont elle a pour mission d'assurer l'application en compromettant, préalablement à des consultations électorales, la qualité, l'objectivité ou la bonne compréhension par le public de ce sondage.,,2) Le juge administratif ne censure l'appréciation portée par la commission des sondages sur le point de savoir si les méconnaissances de la loi n° 77-808 du 19 juillet 1977 par un sondage n'ont pas compromis la qualité, l'objectivité, ou la bonne compréhension par le public de ce sondage et ne rendent pas, par suite, nécessaire la demande de publication d'une mise au point, que lorsqu'elle est entachée d'erreur manifeste.</ANA>
<ANA ID="9E"> 54-07-02-04 Le juge administratif ne censure l'appréciation portée par la commission des sondages sur le point de savoir si les méconnaissances de la loi n° 77-808 du 19 juillet 1977 par un sondage n'ont pas compromis la qualité, l'objectivité, ou la bonne compréhension par le public de ce sondage et ne rendent pas, par suite, nécessaire la demande de publication d'une mise au point, que lorsqu'elle est entachée d'erreur manifeste.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
