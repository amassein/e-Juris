<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042365885</ID>
<ANCIEN_ID>JG_L_2020_09_000000430945</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/36/58/CETATEXT000042365885.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 25/09/2020, 430945, Publié au recueil Lebon</TITRE>
<DATE_DEC>2020-09-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>430945</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP OHL, VEXLIARD ; SCP RICARD, BENDEL-VASSEUR, GHNASSIA</AVOCATS>
<RAPPORTEUR>M. Charles-Emmanuel Airy</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:430945.20200925</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société civile immobilière (SCI) La Chaumière et Mme B... A... ont demandé au tribunal administratif de Grenoble d'annuler l'arrêté du préfet de la Haute-Savoie du 3 août 2006 en tant qu'il a prononcé le transfert d'office et sans indemnité dans le domaine public communal des parcelles leur appartenant, cadastrées section AM nos 174 et 176, situées rue de la Petite Taverne à Megève. Par un jugement n° 1607373 du 20 février 2018, ce tribunal a rejeté cette demande.  <br/>
<br/>
              Par un arrêt n° 18LY01427 du 21 mars 2019, la cour administrative d'appel de Lyon a rejeté l'appel formé par la société La Chaumière et Mme A... contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 21 mai et 21 août 2019 au secrétariat du contentieux du Conseil d'Etat, la société La Chaumière et Mme A... demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat et de la commune de Megève la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Charles-Emmanuel Airy, auditeur,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Ohl, Vexliard, avocat de la société La Chaumiere et de Mme A... et à la SCP Ricard, Bendel-Vasseur, Ghnassia, avocat de la commune de Megève ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 318-3 du code de l'urbanisme, dans sa rédaction applicable au litige : " La propriété des voies privées ouvertes à la circulation publique dans des ensembles d'habitations peut, après enquête publique, être transférée d'office sans indemnité dans le domaine public de la commune sur le territoire de laquelle ces voies sont situées. / La décision de l'autorité administrative portant transfert vaut classement dans le domaine public et éteint, par elle-même et à sa date, tous droits réels et personnels existant sur les biens transférés. / Cette décision est prise par délibération du conseil municipal. Si un propriétaire intéressé a fait connaître son opposition, cette décision est prise par arrêté du représentant de l'Etat dans le département, à la demande de la commune (...) ".<br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que la commune de Megève (Haute-Savoie) a, en application de ces dispositions, voulu intégrer dans son domaine public diverses voies privées ouvertes à la circulation publique, dont des parcelles cadastrées section AM nos 174 et 176, situées rue de la Petite Taverne, appartenant respectivement à Mme A... et à la société La Chaumière, dont Mme A... est la gérante. En raison de l'opposition des propriétaires de certaines des parcelles concernées, la commune a demandé au préfet de la Haute-Savoie de prononcer leur transfert, ce qu'il a fait par un arrêté du 3 août 2006. Le 23 décembre 2016, Mme A... et la société La Chaumière ont demandé au tribunal administratif de Grenoble d'annuler cet arrêté en tant qu'il prononce le transfert d'office et sans indemnité dans le domaine public communal des parcelles leur appartenant, cadastrées sections AM nos 174 et 176. Par un jugement du 20 février 2018, le tribunal a rejeté leur requête pour tardiveté. Mme A... et la société La Chaumière se pourvoient en cassation contre l'arrêt du 21 mars 2019 par lequel la cour administrative d'appel de Lyon a rejeté leur appel dirigé contre ce jugement.<br/>
<br/>
              3. D'une part, le délai de recours contentieux contre une décision de transfert prise sur le fondement des dispositions de l'article L. 318-3 du code de l'urbanisme ne peut courir, pour les propriétaires intéressés, qu'à compter de la date à laquelle celle-ci leur a été notifiée, peu important que cette décision ait été par ailleurs publiée ou affichée.<br/>
<br/>
              4. D'autre part, aux termes du premier alinéa de l'article R. 421-1 du code de justice administrative, dans sa rédaction alors en vigueur : " Sauf en matière de travaux publics, la juridiction ne peut être saisie que par voie de recours formé contre une décision, et ce, dans les deux mois à partir de la notification ou de la publication de la décision attaquée. " L'article R. 421-5 de ce code dispose que : " Les délais de recours contre une décision administrative ne sont opposables qu'à la condition d'avoir été mentionnés, ainsi que les voies de recours, dans la notification de la décision. " Il résulte de ces dispositions que lorsque la notification ne comporte pas les mentions requises, ce délai n'est pas opposable.<br/>
<br/>
              5. Toutefois, le principe de sécurité juridique, qui implique que ne puissent être remises en cause sans condition de délai des situations consolidées par l'effet du temps, fait obstacle à ce que puisse être contestée indéfiniment une décision administrative individuelle qui a été notifiée à son destinataire, ou dont il est établi, à défaut d'une telle notification, que celui-ci a eu connaissance. En une telle hypothèse, si le non-respect de l'obligation d'informer l'intéressé sur les voies et les délais de recours, ou l'absence de preuve qu'une telle information a bien été fournie, ne permet pas que lui soient opposés les délais de recours fixés par le code de justice administrative, le destinataire de la décision ne peut exercer de recours juridictionnel au-delà d'un délai raisonnable. En règle générale et sauf circonstances particulières dont se prévaudrait le requérant, ce délai ne saurait, sous réserve de l'exercice de recours administratifs pour lesquels les textes prévoient des délais particuliers, excéder un an à compter de la date à laquelle une décision expresse lui a été notifiée ou de la date à laquelle il est établi qu'il en a eu connaissance. Ces règles sont également applicables à la contestation des décisions non réglementaires qui ne présentent pas le caractère de décisions individuelles, lorsque la contestation émane des destinataires de ces décisions à l'égard desquels une notification est requise pour déclencher le délai de recours.<br/>
<br/>
              6. Pour juger que la saisine, le 23 décembre 2016, du tribunal administratif de Grenoble était tardive, la cour administrative d'appel de Lyon a relevé, au terme d'une appréciation souveraine exempte de dénaturation et sans se fonder sur des faits matériellement inexacts, que l'arrêté du 3 août 2006 avait été notifié le 17 août 2006 à la société La Chaumière et à Mme A... par lettres recommandées avec accusé de réception et que, faute de préciser les voies et délais de recours, cette notification était incomplète au regard des dispositions de l'article R. 421-5 du code de justice administrative, de sorte que le délai de deux mois fixé par les dispositions de l'article R. 421-1 ne leur était pas opposable. Par une appréciation souveraine des faits exempte de dénaturation, la cour a également jugé qu'en se bornant à invoquer l'atteinte que porterait l'arrêté litigieux au droit de propriété, la société La Chaumière et Mme A... ne faisaient état d'aucune circonstance particulière justifiant de proroger au-delà d'un an le délai raisonnable dans lequel elles pouvaient exercer un recours juridictionnel. En statuant ainsi, après avoir relevé que le préfet de la Haute-Savoie n'avait pas commis de voie de fait en mettant en oeuvre cette procédure, la cour a répondu à l'ensemble des moyens opérants soulevés devant elle et n'a commis aucune erreur de droit. Par suite, le pourvoi de la société La Chaumière et de Mme A... doit être rejeté.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société La Chaumière et Mme A... la somme de 3 000 euros à verser à la commune de Megève, au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société La Chaumière et de Mme A... est rejeté.<br/>
Article 2 : La société La Chaumière et Mme A... verseront ensemble à la commune de Megève une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société civile immobilière La Chaumière, à Mme B... A..., à la commune de Megève et à la ministre de la transition écologique.<br/>
Copie en sera adressée au ministre de l'intérieur.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-07 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. PRINCIPES INTÉRESSANT L'ACTION ADMINISTRATIVE. - IMPOSSIBILITÉ D'EXERCER UN RECOURS JURIDICTIONNEL AU-DELÀ D'UN DÉLAI RAISONNABLE [RJ1] - OPPOSABILITÉ AUX RECOURS DIRIGÉS CONTRE LES DÉCISIONS D'ESPÈCE PAR LES REQUÉRANTS À L'ÉGARD DESQUELS UNE NOTIFICATION DE LA DÉCISION EST REQUISE POUR DÉCLENCHER LE DÉLAI DE RECOURS - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-07 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉLAIS. - RECOURS DIRIGÉS CONTRE LES DÉCISIONS D'ESPÈCE PAR LES REQUÉRANTS À L'ÉGARD DESQUELS UNE NOTIFICATION DE LA DÉCISION EST REQUISE POUR DÉCLENCHER LE DÉLAI DE RECOURS - IMPOSSIBILITÉ D'EXERCER UN RECOURS JURIDICTIONNEL AU-DELÀ D'UN DÉLAI RAISONNABLE [RJ1] - EXISTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-08-02-02-01-04 PROCÉDURE. VOIES DE RECOURS. CASSATION. CONTRÔLE DU JUGE DE CASSATION. BIEN-FONDÉ. DÉNATURATION. - CIRCONSTANCE PARTICULIÈRE JUSTIFIANT DE PROROGER AU-DELÀ D'UN AN LE DÉLAI RAISONNABLE AU-DELÀ DUQUEL IL EST IMPOSSIBLE D'EXERCER UN RECOURS JURIDICTIONNEL [RJ2].
</SCT>
<ANA ID="9A"> 01-04-03-07 Le délai raisonnable au-delà duquel il est impossible d'exercer un recours juridictionnel est opposable aux recours dirigés contre les décisions non réglementaires qui ne présentent pas le caractère de décisions individuelles, lorsque la contestation émane des destinataires de ces décisions à l'égard desquels une notification est requise pour déclencher le délai de recours.</ANA>
<ANA ID="9B"> 54-01-07 Le délai raisonnable au-delà duquel il est impossible d'exercer un recours juridictionnel est opposable aux recours dirigés contre les décisions non réglementaires qui ne présentent pas le caractère de décisions individuelles, lorsque la contestation émane des destinataires de ces décisions à l'égard desquels une notification est requise pour déclencher le délai de recours.</ANA>
<ANA ID="9C"> 54-08-02-02-01-04 Le juge de cassation laisse à l'appréciation souveraine des juges du fond, sous réserve de dénaturation, le point de savoir si un justiciable fait état d'une circonstance particulière justifiant de proroger au-delà d'un an le délai raisonnable au-delà duquel il est impossible d'exercer un recours juridictionnel.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 13 juillet 2016, M. Czabaj, n° 387763, p. 340.,,[RJ2] Rappr., s'agissant de l'obligation de faire état de circonstances particulières, CE, Assemblée, 13 juillet 2016, M. Czabaj, n° 387763, p. 340.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
