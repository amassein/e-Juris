<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044155763</ID>
<ANCIEN_ID>JG_L_2021_10_000000450771</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/15/57/CETATEXT000044155763.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 01/10/2021, 450771</TITRE>
<DATE_DEC>2021-10-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>450771</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Eric Buge</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:450771.20211001</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La Commission nationale des comptes de campagne et des financements politiques a saisi, sur le fondement de l'article L. 52-15 du code électoral, le tribunal administratif de Versailles du rejet du compte de campagne de M. A... B..., candidat tête de liste lors des élections municipales et communautaires des 15 mars et 28 juin 2020 dans la commune de Savigny-sur-Orge (Essonne). Par un jugement n° 2008396 du 16 février 2021, le tribunal administratif de Versailles a déclaré M. B... inéligible pour une durée de dix-huit mois.<br/>
<br/>
              Par une requête, enregistrée le 17 mars 2021 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat d'annuler ce jugement.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - le code de procédure civile ;<br/>
              - la loi n° 2020-290 du 23 mars 2020 ;<br/>
              - la loi n° 2020-760 du 22 juin 2020 ; <br/>
              - l'ordonnance n° 2020-390 du 1er avril 2020 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Eric Buge, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. D'une part, aux termes des deuxième et troisième alinéas de l'article L. 52-15 du code électoral : " Hors le cas prévu à l'article L. 118-2, [la Commission nationale des comptes de campagne et des financements politiques] se prononce dans un délai de six mois à compter de l'expiration du délai fixé au II de l'article L. 52-12. Passé ce délai, les comptes sont réputés approuvés. / Lorsque la commission a constaté que le compte de campagne n'a pas été déposé dans le délai prescrit, si le compte a été rejeté ou si, le cas échéant après réformation, il fait apparaître un dépassement du plafond des dépenses électorales, la commission saisit le juge de l'élection ". Le délai fixé au II de l'article L. 52-12 du même code prend fin le dixième vendredi suivant le premier tour de scrutin à 18 heures. Sur le fondement du premier alinéa de l'article L. 118-2 du même code : " Si le juge administratif est saisi de la contestation d'une élection dans une circonscription où le montant des dépenses électorales est plafonné, il sursoit à statuer jusqu'à réception des décisions de la commission instituée par l'article L. 52-14 qui doit se prononcer sur les comptes de campagne des candidats à cette élection dans le délai de deux mois suivant l'expiration du délai fixé au II de l'article L. 52-12. " Enfin, selon l'article 642 du code de procédure civile, " tout délai expire le dernier jour à vingt-quatre heures. Le délai qui expirerait normalement un samedi, un dimanche ou un jour férié ou chômé est prorogé jusqu'au premier jour ouvrable suivant ".<br/>
<br/>
              2. D'autre part, aux termes du 4° du XII de l'article 19 de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19 : " Pour les listes de candidats présentes au seul premier tour, la date limite mentionnée à la première phrase du deuxième alinéa de l'article L. 52-12 du code électoral est fixée au 10 juillet 2020 à 18 heures. Pour celles présentes au second tour, la date limite est fixée au 11 septembre 2020 à 18 heures ". Selon l'article 4 de l'ordonnance du 1er avril 2020 relative au report du second tour du renouvellement général des conseillers municipaux, des conseillers de Paris et des conseillers de la métropole de Lyon de 2020 et à l'établissement de l'aide publique 2020, dans sa rédaction issue de la loi du 22 juin 2020 tendant à sécuriser l'organisation du second tour des élections municipales et communautaires de juin 2020 et à reporter les élections consulaires, " pour le renouvellement général des conseillers municipaux et communautaires, des conseillers de Paris et des conseillers métropolitains de Lyon de 2020, le délai prévu au premier alinéa de l'article L. 118-2 du code électoral est fixé à trois mois à compter de la date prévue : / 1° A la première phrase du 4° du XII de l'article 19 de la loi n° 2020-290 du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19, en ce qui concerne les communes et secteurs où le premier tour de scrutin organisé le 15 mars 2020 a été conclusif ; / 2° A la seconde phrase du même 4°, en ce qui concerne les autres communes et secteurs ainsi que les circonscriptions de la métropole de Lyon ".<br/>
<br/>
              3. Il résulte de la combinaison de ces dispositions que, pour le renouvellement général des conseillers municipaux et communautaires ayant eu lieu en 2020, dans le cas où l'élection n'a pas été acquise au premier tour et a fait l'objet de contestations devant le juge, qu'il s'agisse des listes de candidats présents seulement au premier tour ou aux deux tours, le délai imparti par l'article L. 52-15 du code électoral à la Commission nationale des comptes de campagne et des financements politiques pour saisir le juge de l'élection était de trois mois à compter du 11 septembre 2020. Ce délai, présentant le caractère d'un délai franc, a expiré le 12 décembre 2020. Ce jour étant un samedi, il a été prorogé jusqu'au lundi 14 décembre 2020.<br/>
<br/>
              4. Il est constant que la Commission nationale des comptes de campagne et des financements politiques a saisi le juge de l'élection, en application de l'article L. 52-15 du code électoral, du rejet du compte de campagne de M. B... le 11 décembre 2020. Par suite, c'est à bon droit que le tribunal a jugé que cette saisine était recevable et M. B... n'est pas fondé à demander l'annulation pour ce motif du jugement par lequel il l'a déclaré inéligible pour une durée de dix-huit mois.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. B... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. A... B..., à la Commission nationale des comptes de campagne et des financements politiques et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">28-005-04-03 ÉLECTIONS ET RÉFÉRENDUM. - DISPOSITIONS GÉNÉRALES APPLICABLES AUX ÉLECTIONS. - FINANCEMENT ET PLAFONNEMENT DES DÉPENSES ÉLECTORALES. - COMMISSION NATIONALE DES COMPTES DE CAMPAGNE ET DES FINANCEMENTS POLITIQUES (CNCCFP). - DÉLAI DE SAISINE DU JUGE PAR LA CNCCFP POUR LES ÉLECTIONS MUNICIPALES ET COMMUNAUTAIRES DE 2020 -ECHÉANCE - 14 DÉCEMBRE 2020.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">28-08-01-02 ÉLECTIONS ET RÉFÉRENDUM. - RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. - INTRODUCTION DE L'INSTANCE. - DÉLAIS. - ELECTIONS MUNICIPALES ET COMMUNAUTAIRES DE 2020 - DÉLAI DE SAISINE DU JUGE PAR LA CNCCFP - ECHÉANCE - 14 DÉCEMBRE 2020.
</SCT>
<ANA ID="9A"> 28-005-04-03 Il résulte de la combinaison, d'une part, des articles L. 52-12, L. 52-15 et L. 118-2 du code électoral et de l'article 642 du code de procédure civile, et, d'autre part, du XII de l'article 19 de la loi n° 2020-290 du 23 mars 2020 et de l'article 4 de l'ordonnance n° 2020-390 du 1er avril 2020 que, pour le renouvellement général des conseillers municipaux et communautaires ayant eu lieu en 2020, dans le cas où l'élection n'a pas été acquise au premier tour et a fait l'objet de contestations devant le juge, qu'il s'agisse des listes de candidats présents seulement au premier tour ou aux deux tours, le délai imparti par l'article L. 52-15 du code électoral à la Commission nationale des comptes de campagne et des financements (CNCCFP) politiques pour saisir le juge de l'élection était de trois mois à compter du 11 septembre 2020. Ce délai, présentant le caractère d'un délai franc, a expiré le 12 décembre 2020. Ce jour étant un samedi, il a été prorogé jusqu'au lundi 14 décembre 2020.</ANA>
<ANA ID="9B"> 28-08-01-02 Il résulte de la combinaison, d'une part, des articles L. 52-12, L. 52-15 et L. 118-2 du code électoral et de l'article 642 du code de procédure civile, et, d'autre part, du XII de l'article 19 de la loi n° 2020-290 du 23 mars 2020 et de l'article 4 de l'ordonnance n° 2020-390 du 1er avril 2020 que, pour le renouvellement général des conseillers municipaux et communautaires ayant eu lieu en 2020, dans le cas où l'élection n'a pas été acquise au premier tour et a fait l'objet de contestations devant le juge, qu'il s'agisse des listes de candidats présents seulement au premier tour ou aux deux tours, le délai imparti par l'article L. 52-15 du code électoral à la Commission nationale des comptes de campagne et des financements (CNCCFP) politiques pour saisir le juge de l'élection était de trois mois à compter du 11 septembre 2020. Ce délai, présentant le caractère d'un délai franc, a expiré le 12 décembre 2020. Ce jour étant un samedi, il a été prorogé jusqu'au lundi 14 décembre 2020.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
