<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026972862</ID>
<ANCIEN_ID>JG_L_2013_01_000000355511</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/97/28/CETATEXT000026972862.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 22/01/2013, 355511</TITRE>
<DATE_DEC>2013-01-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>355511</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Marie-Astrid Nicolazo de Barmon</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:355511.20130122</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête, enregistrée le 2 janvier 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par le Syndicat national CGT des chancelleries et services judiciaires, ayant son siège 4, boulevard du Palais à Paris (75001), représenté par sa secrétaire générale ; il demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2011-1472 du 9 novembre 2011 fixant des modalités exceptionnelles de recrutement de greffiers des services judiciaires, les trois arrêtés du 9 novembre 2011 pris pour l'application de ce décret et les deux notes SJ-11-319-RHG4 du 16 novembre 2011 et SJ-11-349-RHG1 du 20 décembre 2011 adressées aux chefs de cours et de juridictions par le garde des sceaux, ministre de la justice et des libertés ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ainsi que le paiement des entiers dépens ; <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 84-16 du 11 janvier 1984 ; <br/>
<br/>
              Vu le décret n° 82-452 du 28 mai 1982 ; <br/>
<br/>
              Vu le décret n° 2003-466 du 30 mai 2003 ; <br/>
<br/>
              Vu le décret n° 2011-184 du 15 février 2011 ;  <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Astrid Nicolazo de Barmon, Maître des Requêtes, <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Sur les conclusions tendant à l'annulation du décret du 9 novembre 2011 fixant des modalités exceptionnelles de recrutement de greffiers des services judiciaires, des trois arrêtés du même jour pris pour son application et des deux notes de service des 16 novembre et  20 décembre 2011 :<br/>
<br/>
              En ce qui concerne la légalité externe :<br/>
<br/>
              Quant aux dispositions applicables :<br/>
<br/>
              1. Considérant qu'en l'absence de dispositions contraires, la régularité de la procédure de consultation d'un organisme s'apprécie au regard des textes en vigueur à la date à laquelle intervient la décision devant être précédée de cette consultation, alors même que celle-ci a eu lieu avant l'entrée en vigueur de ces textes et selon les règles prévues par les dispositions antérieurement applicables ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 57 du décret du 15 février 2011 relatif aux comités techniques dans les administrations et les établissements publics de l'Etat : " Les comités techniques paritaires dont le mandat a été renouvelé en 2010 ainsi que ceux pour lesquels la date limite de dépôt des candidatures pour le premier tour était antérieure au 31 décembre 2010 demeurent régis par les dispositions du décret n° 82-452 du 28 mai 1982 relatif aux comités techniques paritaires jusqu'au terme de leur mandat. Toutefois, les premier et quatrième alinéas de l'article 10, le troisième alinéa de l'article 11, les articles 34, 36, 37 et 38 à 53 du présent décret sont applicables à ces mêmes comités à compter du 1er novembre 2011. " ; qu'en l'absence de dispositions prévoyant que les consultations effectuées avant le 1er novembre 2011 sur des projets de textes signés après cette date resteraient régies par les dispositions antérieurement applicables, les articles 38 à 53 de ce décret, qui portent sur le fonctionnement des comités techniques, se sont en conséquence substituées, à compter de cette date, aux dispositions des articles 16 à 30 du décret du 28 mai 1982 régissant le fonctionnement des comités techniques paritaires ; <br/>
<br/>
              Quant aux moyens :<br/>
<br/>
              3. Considérant, en premier lieu, que si le syndicat requérant se prévaut des dispositions du premier alinéa de l'article 30 du décret du 28 mai 1982 relatif aux comités techniques paritaires selon lesquelles, pour l'examen des questions statutaires soumises aux comités techniques paritaires par application de l'article 14 de ce décret, ces comités entendent deux représentants du personnel à la commission administrative paritaire intéressée, désignés par les représentants du personnel au sein de cette commission, ces dispositions n'ont pas été reprises par le décret du 15 février 2011 ; que, par suite, et alors même que la séance du comité technique paritaire s'est tenue avant le 1er novembre 2011, la circonstance que l'avis de ce comité recueilli sur le projet de décret et sur les deux projets d'arrêtés réglementaires fixant les conditions d'application de ce texte aurait été rendu en méconnaissance de ces dispositions est insusceptible d'exercer une influence sur la légalité du décret attaqué ainsi que des arrêtés pris pour son application, des lors que la signature de ces textes est postérieure au 1er novembre 2011 ; que par suite ce moyen est inopérant ;<br/>
<br/>
              4. Considérant, en second lieu, qu'aux termes du  second alinéa de l'article 52 du décret du 15 février 2011, qui reprend les dispositions du troisième alinéa de l'article 30 du décret du 28 mai 1982 dont se prévaut le syndicat requérant  : " Les comités techniques doivent, dans un délai de deux mois, être informés, par une communication écrite du président à chacun des membres, des suites données à leurs propositions et avis. " ;<br/>
<br/>
              5. Considérant que si les actes administratifs doivent être pris selon les formes et conformément aux procédures prévues par les lois et règlements, un vice affectant le déroulement d'une procédure administrative préalable, suivie à titre obligatoire ou facultatif, n'est de nature à entacher d'illégalité la décision prise que s'il ressort des pièces du dossier qu'il a été susceptible d'exercer, en l'espèce, une influence sur le sens de la décision prise ou qu'il a privé les intéressés d'une garantie ;<br/>
<br/>
              6. Considérant qu'il est constant que les membres du comité technique paritaire ministériel n'ont pas été informés, par une communication écrite du président à chacun d'entre eux, des suites données aux avis et propositions qu'ils ont exprimés sur les projets de décret et d'arrêtés attaqués, en méconnaissance des dispositions citées au point 4 ; que, toutefois, si celles-ci reconnaissent à chacun des membres du comité le droit d'être informé des suites données à leurs propositions et avis, une telle obligation ne peut, par sa nature même, être satisfaite qu'après que l'administration a pris position sur les observations qu'ils ont exprimées et n'a pas pour objet, à la différence de la garantie tenant à la consultation du comité technique paritaire, de porter à la connaissance de l'administration des éléments d'appréciation avant qu'elle arrête une décision ; que la méconnaissance du droit d'information ainsi reconnu aux membres du comité est, par suite, sans incidence sur la teneur des dispositions adoptées après avis du comité technique paritaire et, par conséquent, sur la légalité des textes pris après avis de ce comité ; que l'omission de cette formalité n'a, dès lors, pas constitué une irrégularité de nature à entacher la légalité du décret et des arrêtés examinés par le comité technique paritaire ministériel ; que, par suite, ce moyen doit être écarté ; <br/>
<br/>
              En ce qui concerne la légalité interne : <br/>
<br/>
              7. Considérant, en premier lieu, qu'en vertu du 3° de l'article 19 de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat, pour l'accès à certains corps et dans les conditions fixées par leur statut particulier, des concours réservés aux candidats justifiant de l'exercice pendant une durée déterminée d'une ou plusieurs activités professionnelles dont les statuts particuliers fixent la nature et la durée, d'un ou de plusieurs mandats de membre d'une assemblée élue d'une collectivité territoriale ou d'une ou de plusieurs activités en qualité de responsable, y compris bénévole, d'une association, peuvent être organisés ; que, si le décret du 30 mai 2003 portant statut particulier des greffiers des services judiciaires ne prévoit pas l'organisation de tels concours réservés, le décret attaqué du 9 novembre 2011, émanant des mêmes autorités que le décret du 30 mai 2003 et pris, dans les mêmes formes que lui, en application du 3° de l'article 19 de la loi du 11 janvier 1984 mentionnée ci-dessus, a pour objet et pour effet d'ajouter aux modalités de recrutement prévues par le décret antérieur un recrutement exceptionnel de greffiers au titre de l'année 2012 et d'en fixer les conditions, notamment la nature et la durée des activités professionnelles exigées des candidats ; que le décret attaqué constitue ainsi un élément du statut des greffiers des services judiciaires permettant leur recrutement par la voie d'un concours réservé institué sur le fondement du 3° de l'article 19 de la loi du 11 janvier 1984 ; que le moyen tiré de la méconnaissance de ces dispositions doit, par suite, être écarté ; <br/>
<br/>
              8. Considérant, en second lieu et d'une part, qu'aucun principe n'interdisait au titulaire du pouvoir réglementaire d'édicter des modalités de recrutement complémentaires ou dérogatoires, à titre transitoire, pour répondre à des besoins que les modes de recrutement normaux ne permettraient pas de satisfaire ; qu'eu égard à la nécessité d'augmenter dans les meilleurs délais les effectifs du corps des greffiers des services judiciaires pour accompagner la mise en oeuvre de plusieurs réformes récentes concernant les juridictions civiles et pénales, les décisions attaquées prévoyant l'organisation d'un concours exceptionnel de recrutement de greffiers au titre de l'année 2012 ne procèdent pas d'une erreur manifeste d'appréciation ; que, d'autre part, en fixant à six semaines la durée de la formation initiale suivie par les lauréats de ce concours à l'Ecole nationale des greffes, alors que la durée de cette formation est de douze semaines pour les greffiers recrutés selon les autres voies d'accès au corps, les décisions litigieuses ne sont pas davantage entachées d'erreur manifeste d'appréciation, compte tenu de l'expérience professionnelle dans le domaine juridique requise pour se présenter à ce concours, de la durée globale de la formation initiale prévue pour les lauréats de ce concours, fixée à six mois, et de l'urgence pour les juridictions judiciaires de disposer d'effectifs supplémentaires de greffiers ; <br/>
<br/>
              Sur les dépens :<br/>
<br/>
              9. Considérant que, dans les circonstances de l'espèce, il y a lieu de laisser la contribution pour l'aide juridique à la charge du syndicat requérant ; <br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              10. Considérant que ces dispositions font obstacle à ce qu'une somme soit mise à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>
<br/>
              			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : La requête du Syndicat national CGT des chancelleries et services judiciaires est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée au Syndicat national CGT des chancelleries et services judiciaires, au Premier ministre et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-03-02-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. PROCÉDURE CONSULTATIVE. QUESTIONS GÉNÉRALES. - 1) RÉGULARITÉ DE LA PROCÉDURE DE CONSULTATION - TEXTES APPLICABLES - A) PRINCIPE - TEXTES EN VIGUEUR À LA DATE DE LA DÉCISION - EXISTENCE [RJ1], SAUF DISPOSITIONS CONTRAIRES, ALORS MÊME QUE LA CONSULTATION A EU LIEU AVANT L'ENTRÉE EN VIGUEUR DE CES TEXTES - B) APPLICATION EN L'ESPÈCE - CONSULTATION DU CTP EFFECTUÉE AVANT LE 1ER NOVEMBRE 2011 SUR UN PROJET DE TEXTE SIGNÉ APRÈS CETTE DATE - TEXTES APPLICABLES - ARTICLES 38 À 53 DU DÉCRET DU 15 FÉVRIER 2011 - EXISTENCE - ARTICLES 16 À 30 DU DÉCRET DU 28 MAI 1982 - ABSENCE - 2) CONSULTATION DES COMITÉS TECHNIQUES - OBLIGATION D'INFORMATION DES MEMBRES DU COMITÉ DES SUITES DONNÉES À LEURS PROPOSITIONS ET AVIS - MÉCONNAISSANCE - CONSÉQUENCE, AU REGARD DES PRINCIPES DÉGAGÉS PAR LA JURISPRUDENCE  DANTHONY  [RJ2], SUR LA LÉGALITÉ DE LA DÉCISION PRISE APRÈS AVIS DU COMITÉ - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">01-08-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. APPLICATION DANS LE TEMPS. TEXTE APPLICABLE. - DÉCISION DEVANT ÊTRE PRÉCÉDÉE D'UNE CONSULTATION - TEXTES AU REGARD DESQUELS S'APPRÉCIE LA RÉGULARITÉ DE LA CONSULTATION - 1) PRINCIPE - TEXTES EN VIGUEUR À LA DATE DE LA DÉCISION - EXISTENCE [RJ1], SAUF DISPOSITIONS CONTRAIRES, ALORS MÊME QUE LA CONSULTATION A EU LIEU AVANT L'ENTRÉE EN VIGUEUR DE CES TEXTES - 2) APPLICATION EN L'ESPÈCE - CONSULTATION DU CTP EFFECTUÉE AVANT LE 1ER NOVEMBRE 2011 SUR UN PROJET DE TEXTE SIGNÉ APRÈS CETTE DATE - TEXTES APPLICABLES - ARTICLES 38 À 53 DU DÉCRET DU 15 FÉVRIER 2011 - EXISTENCE - ARTICLES 16 À 30 DU DÉCRET DU 28 MAI 1982 - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">36-07-06-05 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. COMITÉS TECHNIQUES PARITAIRES. PROCÉDURE. - 1) DÉCISION PRÉCÉDÉE DE LA CONSULTATION DU CTP - TEXTES AU REGARD DESQUELS LA RÉGULARITÉ DE LA PROCÉDURE DE CONSULTATION EST APPRÉCIÉE - CONSULTATION DU COMITÉ EFFECTUÉE AVANT LE 1ER NOVEMBRE 2011 SUR UN PROJET DE TEXTE SIGNÉ APRÈS CETTE DATE - ARTICLES 38 À 53 DU DÉCRET DU 15 FÉVRIER 2011 - EXISTENCE - ARTICLES 16 À 30 DU DÉCRET DU 28 MAI 1982 - ABSENCE [RJ1] - 2) OBLIGATION D'INFORMATION DES MEMBRES DU COMITÉ DES SUITES DONNÉES À LEURS PROPOSITIONS ET AVIS - MÉCONNAISSANCE - CONSÉQUENCE, AU REGARD DES PRINCIPES DÉGAGÉS PAR LA JURISPRUDENCE  DANTHONY  [RJ2], SUR LA LÉGALITÉ DE LA DÉCISION PRISE APRÈS AVIS DU COMITÉ - ABSENCE.
</SCT>
<ANA ID="9A"> 01-03-02-01 1) a) En l'absence de dispositions contraires, la régularité de la procédure de consultation d'un organisme s'apprécie au regard des textes en vigueur à la date à laquelle intervient la décision devant être précédée de cette consultation, alors même que celle-ci a eu lieu avant l'entrée en vigueur de ces textes et selon les règles prévues par les dispositions antérieurement applicables.... ...b) En l'absence de dispositions prévoyant que les consultations effectuées avant le 1er novembre 2011 sur des projets de textes signés après cette date resteraient régies par les dispositions antérieurement applicables, les articles 38 à 53 du décret n° 2011-184 du 15 février 2011 relatif aux comités techniques dans les administrations et les établissements publics de l'Etat, qui portent sur le fonctionnement des comités techniques, se sont en conséquence substituées, à compter de cette date, aux dispositions des articles 16 à 30 du décret n° 82-451 du 28 mai 1982 régissant le fonctionnement des comités techniques paritaires (CTP).,,2) Si les dispositions du second alinéa de l'article 52 du décret n° 2011-184 du 15 février 2011, qui reprend celles du troisième alinéa de l'article 30 du décret n° 82-451 du 28 mai 1982, reconnaissent à chacun des membres du comité technique le droit d'être informé des suites données à leurs propositions et avis, une telle obligation ne peut, par sa nature même, être satisfaite qu'après que l'administration a pris position sur les observations qu'ils ont exprimées et n'a pas pour objet, à la différence de la garantie tenant à la consultation du comité, de porter à la connaissance de l'administration des éléments d'appréciation avant qu'elle arrête une décision. La méconnaissance du droit d'information ainsi reconnu aux membres du comité est, par suite, sans incidence sur la teneur des dispositions adoptées après avis du comité technique et, par conséquent, sur la légalité des textes pris après avis de ce comité. Dès lors, l'omission de cette formalité ne constitue pas une irrégularité de nature à entacher la légalité du décret et des arrêtés examinés par le comité.</ANA>
<ANA ID="9B"> 01-08-03 1) En l'absence de dispositions contraires, la régularité de la procédure de consultation d'un organisme s'apprécie au regard des textes en vigueur à la date à laquelle intervient la décision devant être précédée de cette consultation, alors même que celle-ci a eu lieu avant l'entrée en vigueur de ces textes et selon les règles prévues par les dispositions antérieurement applicables.... ...2) En l'absence de dispositions prévoyant que les consultations effectuées avant le 1er novembre 2011 sur des projets de textes signés après cette date resteraient régies par les dispositions antérieurement applicables, les articles 38 à 53 du décret n° 2011-184 du 15 février 2011 relatif aux comités techniques dans les administrations et les établissements publics de l'Etat, qui portent sur le fonctionnement des comités techniques, se sont en conséquence substituées, à compter de cette date, aux dispositions des articles 16 à 30 du décret n° 82-451 du 28 mai 1982 régissant le fonctionnement des comités techniques paritaires (CTP).</ANA>
<ANA ID="9C"> 36-07-06-05 1) En l'absence de dispositions prévoyant que les consultations effectuées avant le 1er novembre 2011 sur des projets de textes signés après cette date resteraient régies par les dispositions antérieurement applicables, les articles 38 à 53 du décret n° 2011-184 du 15 février 2011 relatif aux comités techniques dans les administrations et les établissements publics de l'Etat, qui portent sur le fonctionnement des comités techniques, se sont en conséquence substituées, à compter de cette date, aux dispositions des articles 16 à 30 du décret n° 82-451 du 28 mai 1982 régissant le fonctionnement des comités techniques paritaires.,,2) Si les dispositions du second alinéa de l'article 52 du décret du 15 février 2011, qui reprend celles du troisième alinéa de l'article 30 du décret du 28 mai 1982, reconnaissent à chacun des membres du comité technique le droit d'être informé des suites données à leurs propositions et avis, une telle obligation ne peut, par sa nature même, être satisfaite qu'après que l'administration a pris position sur les observations qu'ils ont exprimées et n'a pas pour objet, à la différence de la garantie tenant à la consultation du comité, de porter à la connaissance de l'administration des éléments d'appréciation avant qu'elle arrête une décision. La méconnaissance du droit d'information ainsi reconnu aux membres du comité est, par suite, sans incidence sur la teneur des dispositions adoptées après avis du comité technique et, par conséquent, sur la légalité des textes pris après avis de ce comité. Dès lors, l'omission de cette formalité ne constitue pas une irrégularité de nature à entacher la légalité du décret et des arrêtés examinés par le comité.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 18 novembre 1964, Caisse nationale des marchés de l'Etat, n° 51269, p. 554 ; CE, 22 mai 2012, Conseil national des fédérations aéronautiques et sportives et autres, n° 350567, à mentionner aux Tables.,,[RJ2] Cf. CE, Assemblée, 23 décembre 2011, Danthony et autres, n° 335033, p. 649.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
