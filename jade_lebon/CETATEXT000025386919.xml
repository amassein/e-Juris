<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025386919</ID>
<ANCIEN_ID>JG_L_2012_02_000000342366</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/38/69/CETATEXT000025386919.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 17/02/2012, 342366</TITRE>
<DATE_DEC>2012-02-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>342366</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Christian Vigouroux</PRESIDENT>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; LE PRADO</AVOCATS>
<RAPPORTEUR>M. Frédéric Desportes</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:342366.20120217</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 10 août et 10 novembre 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme Muriel A, M. Cédric A, Mlle Cristel A et Mlle Alexandra A, demeurant au ... ; les consorts A demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 08BX03038 du 21 janvier 2010 par lequel la cour administrative d'appel de Bordeaux, après avoir annulé le jugement n° 0603207 du tribunal administratif de Bordeaux du 8 octobre 2008, a rejeté leur requête tendant à la condamnation du centre hospitalier universitaire de Bordeaux à verser la somme de 60 000 euros à la succession de M. C, la somme de 168 908,40 euros à Mme A et la somme de 25 000 euros à chacun de ses trois enfants en réparation du préjudice subi par eux et M. C à la suite de l'hospitalisation de celui-ci au centre hospitalier universitaire de Bordeaux ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge du centre hospitalier universitaire de Bordeaux une somme de 6 000 euros à verser à la SCP Lyon-Caen, Fabiani et Thiriez au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ;<br/>
<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu la note en délibéré enregistrée le 18 janvier 2012, présentée pour les centres hospitaliers de Bordeaux et d'Agen ;<br/>
<br/>
              Vu le code de la santé publique ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu la loi n° 2002-303 du 4 mars 2002 ;<br/>
<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Desportes, chargé des fonctions de Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Lyon-Caen, Thiriez, avocat des consorts A et de Me Le Prado, avocat du centre hospitalier universitaire de Bordeaux et du centre hospitalier d'Agen, <br/>
<br/>
      - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Lyon-Caen, Thiriez, avocat des consorts A et à Me Le Prado, avocat du centre hospitalier universitaire de Bordeaux et du centre hospitalier d'Agen ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, le 20 mai 2002, M. C a été admis au centre hospitalier d'Agen dans un état de coma réactif à la suite d'un accident de karting lui ayant causé un traumatisme crânien entraînant une grave altération de son état neurologique et un traumatisme thoraco-pulmonaire associant de multiples fractures, une contusion bilatérale des bases pulmonaires et des brûlures du troisième degré de l'hémithorax droit ; que, le 21 mai 2002, l'intéressé a été transféré au centre hospitalier universitaire de Bordeaux où il a été pris en charge au sein du service de réanimation puis, à compter du 24 août, au sein du service de neurochirurgie ; que, transféré le 12 septembre 2002 dans le service de pneumologie de l'hôpital d'Agen, il y est décédé le 19 septembre du fait de complications infectieuses et neurologiques ; qu'après l'échec de la procédure de conciliation devant la commission régionale de conciliation et d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales des régions Aquitaine et Midi-Pyrénées, l'épouse de M. C et leurs trois enfants ont saisi le tribunal administratif de Bordeaux d'une demande tendant à ce que le centre hospitalier universitaire (CHU) de Bordeaux et le centre hospitalier d'Agen soient condamnés à réparer le préjudice subi par eux du fait du décès de M. C ; que, par un jugement du 8 octobre 2008, le tribunal administratif, estimant que ce décès était pour partie imputable à une infection nosocomiale contractée au sein du CHU de Bordeaux, a condamné celui-ci à verser diverses indemnités aux consorts A ; que sur l'appel de ces derniers et l'appel incident du CHU de Bordeaux et du centre hospitalier d'Agen, la cour administrative d'appel de Bordeaux a annulé le jugement et rejeté la demande d'indemnisation présentée par les consorts A, lesquels se pourvoient en cassation ;<br/>
<br/>
              Considérant qu'aux termes du I de l'article L. 1142-1 du code de la santé publique : "Hors le cas où leur responsabilité est encourue en raison d'un défaut d'un produit de santé, les professionnels de santé mentionnés à la quatrième partie du présent code, ainsi que tout établissement, service ou organisme dans lesquels sont réalisés des actes individuels de prévention, de diagnostic ou de soins ne sont responsables des conséquences dommageables d'actes de prévention, de diagnostic ou de soins qu'en cas de faute.  / Les établissements, services et organismes susmentionnés sont responsables des dommages résultant d'infections nosocomiales, sauf s'ils rapportent la preuve d'une cause étrangère " ; qu'en vertu de l'article 101 de la loi n° 2002-303 du 4 mars 2002, ces dispositions sont applicables aux infections nosocomiales consécutives à des soins réalisés à compter du 5 septembre 2001 ;<br/>
<br/>
              Considérant que, pour annuler le jugement du tribunal administratif retenant la responsabilité du centre hospitalier universitaire de Bordeaux sur le fondement des dispositions précitées, la cour administrative d'appel, après avoir relevé que les lésions subies par M. C avaient fortement diminué ses défenses immunitaires et provoqué des troubles de la déglutition augmentant les risques d'infection respiratoire, a estimé que la réanimation respiratoire prolongée, nécessitée par l'état de l'intéressé, avait " rendu inévitable la survenue d'infections pulmonaires " ; que la cour en a déduit qu'à supposer que le décès de M. C ait été provoqué par une infection nosocomiale, l'établissement devait être regardé, compte tenu de l'état initial fortement dégradé de la victime, comme rapportant la preuve d'une cause étrangère au sens du I de l'article L. 1142-1 du code de la santé publique ; qu'en statuant ainsi, alors que la réanimation respiratoire ne pouvait être regardée comme une circonstance extérieure à l'activité hospitalière, la cour administrative d'appel a commis une erreur de droit ; que son arrêt doit par suite être annulé ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce de régler l'affaire au fond par application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Sur la responsabilité :<br/>
<br/>
              Considérant, en premier lieu, qu'il résulte de l'instruction et, notamment, du rapport des experts, que l'infection de M. C par les bactéries staphylocoque doré et serratia marcescens est survenue au cours de son séjour dans le service de réanimation ; que, dès lors, le centre hospitalier universitaire de Bordeaux n'est pas fondé à soutenir, dans les conclusions de son appel incident, que cette infection ne serait pas d'origine nosocomiale ;<br/>
<br/>
              Considérant, en second lieu, que l'infection respiratoire nosocomiale trouvant son origine, selon le rapport des experts, dans les mesures de réanimation, avec intubation et ventilation, dont M. C a fait l'objet, il ne peut être soutenu par le CHU de Bordeaux qu'elle aurait été causée par un événement présentant un caractère d'extériorité ; que, dès lors, le CHU de Bordeaux n'est pas fondé à soutenir que le tribunal administratif aurait écarté à tort l'existence d'une cause étrangère ;<br/>
<br/>
              Sur le préjudice :<br/>
<br/>
              En ce qui concerne la fraction du préjudice réparable :<br/>
<br/>
              Considérant que dans le cas où une infection nosocomiale a compromis les chances d'un patient d'obtenir une amélioration de son état de santé ou d'échapper à son aggravation, le préjudice résultant directement de cette infection et qui doit être intégralement réparé n'est pas le dommage corporel constaté mais la perte de chance d'éviter la survenue de ce dommage ; que la réparation qui incombe à l'hôpital doit alors être évaluée à une fraction du dommage corporel déterminée en fonction de l'ampleur de la chance perdue ;<br/>
<br/>
              Considérant que l'infection nosocomiale dont M. C a été victime a entraîné pour celui-ci la perte d'une chance d'éviter une évolution fatale de son état de santé ; que, si, selon les experts, l'état neurologique et respiratoire de M. C était fortement dégradé, il ressort de leurs constatations qu'en l'absence d'infection, l'intéressé aurait eu une importante chance de survie ; que, dès lors, il sera fait une juste appréciation de la chance perdue, évaluée à 20 % par le tribunal administratif, en la fixant à 50 % des différents chefs de préjudice ayant résulté du décès de M. C ; <br/>
<br/>
              En ce qui concerne le préjudice subi par M. C :<br/>
<br/>
              Considérant, en premier lieu, que le tribunal administratif a fait une juste appréciation des souffrances endurées par M. C et imputables à la seule infection nosocomiale, évaluées par les experts à 3 / 7, en allouant à ce titre aux requérants, en leur qualité d'héritiers du défunt, une somme de 4 000 euros ;<br/>
<br/>
              Considérant, en second lieu, que les consorts A ont demandé au tribunal administratif réparation du " préjudice de vie perdue " qu'aurait subi M. C en faisant valoir que, son état s'améliorant, celui-ci pouvait espérer vivre encore de nombreuses années ; que c'est à bon droit que le tribunal administratif  a jugé qu'un tel préjudice n'est pas distinct de celui résultant du décès, lequel ne saurait ouvrir droit à réparation dans le chef du défunt ; que si les consorts A font valoir, à l'appui de leur appel, que ce préjudice aurait consisté dans la douleur morale éprouvée M. C du fait de la conscience d'une espérance de vie réduite, ils n'en établissent pas la réalité ;<br/>
<br/>
              En ce qui concerne le préjudice subi par les consorts A :<br/>
<br/>
              Considérant, en premier lieu, que, les frais d'obsèques de M. C s'étant élevés à 3 704,70 euros, il y a lieu d'allouer à Mme A, en remboursement de ces frais, 50 % de leur montant, correspondant à la fraction du préjudice réparable et de porter en conséquence à 1 852,35 euros la somme allouée par le tribunal administratif à ce titre ; que la requérante n'est pas fondée à soutenir que le CHU de Bordeaux aurait dû également être condamné à lui rembourser les frais de construction d'un caveau familial dès lors que, comme l'a relevé le tribunal administratif, de tels frais ne peuvent être regardés comme la conséquence directe du décès de son époux ;<br/>
<br/>
              Considérant, en deuxième lieu, qu'en évaluant à 60 % la part des revenus que M. C consommait pour son propre compte, le tribunal a retenu une proportion excessive ; que, dans les circonstances de l'espèce, Mme A est fondée à soutenir que cette part doit être ramenée à 40 % ; qu'il convient en conséquence de faire droit à sa demande tendant à ce que le préjudice résultant pour elle de la perte de revenus causée par le décès de son époux soit évalué, après capitalisation, à la somme de 128 370 euros ; que, la fraction indemnisable du préjudice ayant été fixée à 50 %, il y a lieu de porter à 64 185 euros le montant de l'indemnité que le CHU de Bordeaux devra verser à Mme A à ce titre ;<br/>
<br/>
              Considérant, enfin, que le tribunal a fait une juste appréciation du préjudice moral subi par Mme A et ses trois enfants, Cristel, Cédric et Alexandra ; que toutefois, après application du pourcentage correspondant à la fraction du préjudice indemnisable, il convient de porter la somme allouée à la première, à 10 000 euros et celle allouée à chacun de ses trois enfants à 7 500 euros ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que les consorts A sont fondés à demander que les indemnités mises à la charge du CHU de Bordeaux en réparation de leur préjudice personnel soient portées à la somme de 76 037, 35 euros pour Mme A et à la somme de 7500 euros pour chacun de ses trois enfants ; que le CHU de Bordeaux n'est, en revanche, pas fondé à demander à être déchargé de toute indemnité ;<br/>
<br/>
              En ce qui concerne les droits de la caisse primaire d'assurance maladie de Lot-et-Garonne :<br/>
<br/>
              Considérant que la caisse primaire d'assurance maladie de Lot-et-Garonne a demandé, outre le paiement de l'indemnité forfaitaire de gestion, le remboursement des débours exposés par elle au cours de l'hospitalisation de M. C au centre hospitalier d'Agen du 12 au 19 septembre 2002, soit d'une somme de 3 267, 25 euros au titre des dépenses de santé et d'une somme de 163,04 euros au titre des indemnités journalières ; que le tribunal a rejeté à bon droit ces demandes après avoir relevé, d'une part, que, même en l'absence d'infection nosocomiale, l'état de santé de M. C aurait imposé une hospitalisation pendant la période considérée et, d'autre part, que les états produits par la caisse primaire d'assurance maladie ne permettaient pas de déterminer ceux des frais exposés se rapportant à l'infection nosocomiale ; <br/>
<br/>
              Sur les conclusions des consorts A tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant que les consorts A ont obtenu le bénéfice de l'aide juridictionnelle devant le Conseil d'Etat ; que, par suite, leur avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Lyon-Caen, et Thiriez, avocat des consorts A, renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat le versement à la SCP Lyon-Caen, et Thiriez de la somme de 3 000 euros ;<br/>
<br/>
              Considérant que les consorts A ayant également bénéficié de l'aide juridictionnelle devant la cour administrative d'appel de Bordeaux, la demande de remboursement de frais exposés et non compris dans les dépens présentée en leur nom propre dans leur requête d'appel ne saurait être accueillie ; que les dispositions de l'article L. 761-1 du code de justice administrative fait obstacle à ce que soient mis à leur charge les frais exposés par le CHU de Bordeaux ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Bordeaux du 21 janvier 2010 est annulé.<br/>
<br/>
Article 2 : La somme que le centre hospitalier universitaire de Bordeaux est condamné à verser aux consorts A en réparation de leur préjudice personnel est fixée, en ce qui concerne Mme A, à 76 037, 35 euros et, en ce qui concerne chacun de ses trois enfants, à 7 500 euros, avec intérêts au taux légal à compter du 31 août 2006.<br/>
<br/>
Article 3 : Le jugement du tribunal administratif de Bordeaux du 8 octobre 2008 est réformé en ce qu'il a de contraire au présent arrêt.<br/>
<br/>
Article 4 : Le centre hospitalier universitaire de Bordeaux versera à la SCP Lyon-Caen et Thiriez, avocat de des consorts A, une somme de 3000 euros en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
<br/>
Article 5 : Le surplus de la requête d'appel des consorts A et les conclusions d'appel incident du centre hospitalier universitaire de Bordeaux, du centre hospitalier d'Agen et de la caisse primaire d'assurance maladie de Lot-et-Garonne sont rejetés.<br/>
<br/>
Article 6 : La présente décision sera notifiée à Mme Muriel A, M. Cédric A, Mlle Cristel A et Mlle Alexandra A, au centre hospitalier universitaire de Bordeaux au centre hospitalier d'Angers et à la caisse primaire d'assurance maladie de Lot-et-Garonne.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-02-01-01-02 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICE PUBLIC DE SANTÉ. ÉTABLISSEMENTS PUBLICS D'HOSPITALISATION. RESPONSABILITÉ POUR FAUTE MÉDICALE : ACTES MÉDICAUX. - INFECTIONS NOSOCOMIALES - PRÉSOMPTION DE RESPONSABILITÉ DE L'ÉTABLISSEMENT SAUF S'IL RAPPORTE LA PREUVE D'UNE CAUSE ÉTRANGÈRE (ART. L. 1142-1 DU CODE DE LA SANTÉ PUBLIQUE) - ETAT INITIAL FORTEMENT DÉGRADÉ DE LA VICTIME - CIRCONSTANCE DE NATURE À RAPPORTER LA PREUVE D'UNE CAUSE ÉTRANGÈRE [RJ1] - ABSENCE.
</SCT>
<ANA ID="9A"> 60-02-01-01-02 La circonstance de l'état initial fortement dégradé du patient ne suffit pas à rapporter la preuve que l'infection nosocomiale contractée serait due à une cause étrangère, au sens de l'article L. 1142-1 du code de la santé publique, dès lors que la condition d'extériorité n'est pas remplie (en l'espèce, infection nosocomiale trouvant son origine dans les mesures de réanimation respiratoire).</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., pour les autres critères de la cause étrangère, CE, 10 octobre 2011, Centre hospitalier universitaire d'Angers, n° 328500, à publier au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
