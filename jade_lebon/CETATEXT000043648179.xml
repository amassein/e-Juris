<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043648179</ID>
<ANCIEN_ID>JG_L_2021_06_000000447336</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/64/81/CETATEXT000043648179.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 09/06/2021, 447336</TITRE>
<DATE_DEC>2021-06-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>447336</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Frédéric Gueudar Delahaye</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:447336.20210609</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              Procédure contentieuse antérieure<br/>
<br/>
              1° D'une part, M. F... E... et M. L... K... ont demandé au tribunal administratif de la Guyane, d'une part, d'annuler les opérations électorales qui se sont déroulées le 15 mars 2020 en vue de l'élection des conseillers municipaux et communautaires de la commune d'Apatou et, d'autre part, de prononcer l'inéligibilité de M. C... J... pour une durée de trois années.<br/>
<br/>
              D'autre part, M. I... A... a demandé au tribunal administratif de la Guyane, d'une part, d'annuler les mêmes opérations électorales et, d'autre part, d'enjoindre au maire d'Apatou d'organiser de nouvelles élections dans les délais légaux sous astreinte de 50 euros par jour de retard, en application de l'article L. 911-1 du code de justice administrative.<br/>
<br/>
              Par un jugement nos 2000270, 2000278 du 29 septembre 2020, le tribunal administratif de la Guyane a annulé les opérations électorales en litige et rejeté le surplus des conclusions des protestations.<br/>
<br/>
              2° La Commission nationale des comptes de campagne et des financements politiques a saisi le tribunal administratif de la Guyane, en application de l'article L. 52-15 du code électoral, sur le fondement de sa décision du 28 septembre 2020 rejetant le compte de campagne de M. E..., candidat tête de liste lors des élections municipales et communautaires d'Apatou qui se sont déroulées le 15 mars 2020.<br/>
<br/>
              Par un jugement n° 2000865 du 24 décembre 2020, le tribunal administratif de la Guyane a jugé que le compte de campagne de M. E... a été rejeté à bon droit, et, sur le fondement de l'article L. 118-3 du code électoral, déclaré l'intéressé inéligible pour une durée d'un an, annulé son élection et proclamé élus les suivants de la liste, M. M... H..., en qualité de conseiller municipal, et Mme N... D..., en qualité de conseillère communautaire.<br/>
<br/>
              Procédures devant le Conseil d'Etat<br/>
<br/>
              1° sous le n° 447336, par une requête, enregistrée le 7 décembre 2020 au secrétariat du contentieux du Conseil d'Etat, M. O... G... B... et M. C... J... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement du 29 septembre 2020 ;<br/>
<br/>
              2°) de rejeter les protestations présentées en première instance.<br/>
<br/>
              2° sous le n° 449019, par une requête enregistrée le 22 janvier 2021 au secrétariat du contentieux du Conseil d'Etat, M. E... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement du 24 décembre 2020 ;<br/>
<br/>
              2°) d'annuler la décision de la Commission nationale des comptes de campagne et des financements politiques du 28 septembre 2020.<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - l'ordonnance n° 45-2592 du 2 novembre 1945 ;<br/>
              - la loi n° 2017-1339 du 15 septembre 2017 ;<br/>
              - la loi n° 2019-1269 du 2 décembre 2019 ;<br/>
              - la loi n° 2020-290 du 23 mars 2020 ;<br/>
              - l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Gueudar Delahaye, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteure publique ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Les requêtes visées ci-dessus sont relatives au scrutin organisé le 15 mars 2020 pour la désignation des conseillers municipaux et communautaires d'Apatou (Guyane). Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2. À l'issue des opérations électorales organisées le 15 mars 2020 dans la commune d'Apatou en vue de la désignation des conseillers municipaux et communautaires, la liste conduite par M. J..., maire sortant, a obtenu 649 voix, soit 50,46 % des 1 286 suffrages exprimés, tandis que la liste menée par M. E... a obtenu 366 voix, soit 28,46 % des suffrages exprimés, et celle conduite par M. A... a obtenu 271 voix, soit 21,07 % des suffrages exprimés. Sous le n° 447336, MM. Saint B... et J... relèvent appel du jugement du 29 septembre 2020 du tribunal administratif de la Guyane en tant qu'il a annulé ces opérations électorales.<br/>
<br/>
              3. Par une décision du 28 septembre 2020, la Commission nationale des comptes de campagne et des financements politiques a rejeté le compte de campagne de M. E... pour défaut de présentation par un expert-comptable, en méconnaissance des dispositions de l'article L. 52-12 du code électoral. La Commission a également saisi le tribunal administratif de la Guyane, juge de l'élection, en application des dispositions de l'article L. 52-15 du code électoral. Par un jugement du 24 décembre 2020, le tribunal administratif a confirmé le rejet du compte de campagne de M. E..., a déclaré celui-ci inéligible pour une durée d'un an en application des dispositions de l'article L. 118-3 du code électoral et a, par voie de conséquence, annulé son élection et proclamé élus M. H..., et Mme D..., respectivement en qualité de conseiller municipal et de conseillère communautaire. Sous le n° 449019, M. E... relève appel de ce jugement.<br/>
<br/>
              Sur les opérations électorales :<br/>
<br/>
              En ce qui concerne la recevabilité de la protestation de MM. E... et K... :<br/>
<br/>
              4. Il résulte de l'instruction que l'irrégularité de la protestation initiale présentée par M. K..., qui ne justifiait pas de sa qualité de mandataire de M. E..., a fait l'objet d'une régularisation par un mémoire enregistré le 2 mai 2020, dans le délai de recours prolongé par l'article 15 de l'ordonnance du 25 mars 2020 portant adaptation des règles applicables devant les juridictions de l'ordre administratif. Par suite, c'est à bon droit que, par un jugement suffisamment motivé sur ce point, le tribunal administratif a écarté la fin de non-recevoir opposée sur ce point par le préfet de la Guyane et M. G... B....<br/>
<br/>
              En ce qui concerne les griefs relatifs à la liste électorale :<br/>
<br/>
              5. Aux termes du I de l'article L. 11 du code électoral : " Sont inscrits sur la liste électorale de la commune, sur leur demande : /  1° Tous les électeurs qui ont leur domicile réel dans la commune ou y habitent depuis six mois au moins et leurs enfants de moins de 26 ans ; / 2° Ceux qui figurent pour la deuxième fois sans interruption, l'année de la demande d'inscription, au rôle d'une des contributions directes communales et, s'ils ne résident pas dans la commune, ont déclaré vouloir y exercer leurs droits électoraux. Tout électeur ou toute électrice peut être inscrit sur la même liste que son conjoint au titre de la présente disposition ; / 2° bis Ceux qui, sans figurer au rôle d'une des contributions directes communales, ont, pour la deuxième fois sans interruption l'année de la demande d'inscription, la qualité de gérant ou d'associé majoritaire ou unique d'une société figurant au rôle, dans des conditions déterminées par décret en Conseil d'Etat ; / 3° Ceux qui sont assujettis à une résidence obligatoire dans la commune en qualité de fonctionnaires ".<br/>
<br/>
              6. Il n'appartient pas au juge de l'élection d'apprécier si un électeur inscrit sur les listes électorales remplit effectivement la condition de domicile exigée par l'article L. 11 du code électoral, mais seulement de rechercher si des manoeuvres dans l'établissement de la liste électorale ont altéré la sincérité du scrutin.<br/>
<br/>
              7. M. E... et M. K... ont soutenu en première instance que 162 personnes ont été inscrites sur la liste électorale d'Apatou dans les semaines ayant précédé le scrutin avec pour seule justification de domicile une attestation de résidence rédigée par le maire sortant. Ils ont également produit une liste des personnes ayant, selon eux, bénéficié d'attestations de résidence établies par le maire sortant alors qu'elles ne disposaient pas de leur domicile réel dans la commune, ainsi que quatre procès-verbaux de constat établis par un clerc d'huissier à leur demande.  <br/>
<br/>
              8. En premier lieu, d'une part, il résulte de l'instruction que la clerc d'huissier sollicitée par M. E... avait pour mission de se rendre aux domiciles déclarés d'un certain nombre d'électeurs, afin de se rendre compte de la domiciliation ou de la non-domiciliation des intéressés, d'y interroger les résidents et ainsi de réaliser des constatations matérielles. D'autre part, la circonstance que la clerc d'huissier se soit déplacée le dimanche 21 juin 2020 alors que l'arrêté préfectoral du 15 juin 2020 portant mesures de prévention et de restriction nécessaires dans le département de la Guyane dans le cadre de la lutte contre le virus covid-19 interdisait, sauf exceptions, les déplacements dans la commune d'Apatou, n'est, en tout état de cause, pas de nature à priver de valeur probante le constat réalisé ce jour-là. Enfin, la circonstance alléguée selon laquelle les personnes rencontrées par la clerc d'huissier ignoraient ou ne maîtrisaient pas la langue française et n'auraient pu s'exprimer que par le truchement d'un interprète affilié aux demandeurs de première instance ne présentant aucune garantie de neutralité, ne peut être regardée comme établie.<br/>
<br/>
              9. En second lieu, ainsi que l'a estimé le tribunal administratif, par un jugement suffisamment motivé sur ce point, il résulte de l'instruction et notamment des quatre constats réalisés par la clerc d'huissier, qu'au moins seize personnes ayant voté dans la commune le 15 mars 2020 n'y avaient pas leur domicile réel. La délivrance par le maire sortant d'un nombre élevé d'attestations de domiciliation ne correspondant pas à la réalité puis l'inscription irrégulière des personnes bénéficiaires de ces attestations sur la liste électorale de la commune d'Apatou antérieurement à l'élection contestée ont, dans les circonstances de l'espèce, constitué une manoeuvre qui a été de nature, compte tenu de la très faible majorité dont a été créditée la liste conduite par M. J..., à avoir altéré la sincérité du scrutin.<br/>
<br/>
              10. Il résulte de tout ce qui précède que MM. Saint B... et J... ne sont pas fondés à soutenir que c'est à tort que, par le jugement attaqué du 29 septembre 2020, le tribunal administratif de la Guyane a annulé les opérations électorales auxquelles il a été procédé, le 15 mars 2020, pour l'élection des conseillers municipaux et communautaires de la commune d'Apatou.<br/>
<br/>
              Sur le rejet du compte de campagne de M. E... :<br/>
<br/>
              11. Aux termes de l'article L. 118-3 du code électoral, dans sa rédaction antérieure à la loi du 2 décembre 2019 visant à clarifier diverses dispositions du droit électoral : " Saisi par la commission instituée par l'article L. 52-14, le juge de l'élection peut prononcer l'inéligibilité du candidat dont le compte de campagne, le cas échéant après réformation, fait apparaître un dépassement du plafond des dépenses électorales. (...) : Saisi dans les mêmes conditions, le juge de l'élection peut prononcer l'inéligibilité du candidat ou des membres du binôme de candidats qui n'a pas déposé son compte de campagne dans les conditions et le délai prescrits à l'article L. 52-12. / Il prononce également l'inéligibilité du candidat ou des membres du binôme de candidats dont le compte de campagne a été rejeté à bon droit en cas de volonté de fraude ou de manquement d'une particulière gravité aux règles relatives au financement des campagnes électorales. ". Aux termes de ce même article dans sa rédaction issue de cette loi : " Lorsqu'il relève une volonté de fraude ou un manquement d'une particulière gravité aux règles de financement des campagnes électorales, le juge de l'élection, saisi par la Commission nationale des comptes de campagne et des financements politiques, peut déclarer inéligible : 1° Le candidat qui n'a pas déposé son compte de campagne dans les conditions et le délai prescrits à l'article L. 52-12 ; (...) ".<br/>
<br/>
              12. Aux termes du premier alinéa de l'article 15 de la loi du 2 décembre 2019 : " La présente loi, à l'exception de l'article 6, entre en vigueur le 30 juin 2020 ". Aux termes du XVI de l'article 19 de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19 : " A l'exception de son article 6, les dispositions de la loi n° 2019-1269 du 2 décembre 2019 visant à clarifier diverses dispositions du droit électoral ne sont pas applicables au second tour de scrutin régi par la présente loi ". Il résulte de ces dispositions que les dispositions de la loi du 2 décembre 2019 modifiant celles du code électoral, à l'exception de son article 6, ne sont pas applicables aux opérations électorales en vue de l'élection des conseillers municipaux et communautaires organisées les 15 mars et 28 juin 2020, y compris en ce qui concerne les comptes de campagne. <br/>
<br/>
              13. Toutefois, l'inéligibilité prévue par les dispositions de l'article L. 1183 du code électoral constitue une sanction ayant le caractère d'une punition. Il incombe, dès lors au juge de l'élection, lorsqu'il est saisi de conclusions tendant à ce qu'un candidat dont le compte de campagne est rejeté soit déclaré inéligible et à ce que son élection soit annulée, de faire application, le cas échéant, d'une loi nouvelle plus douce entrée en vigueur entre la date des faits litigieux et celle à laquelle il statue. Le législateur n'ayant pas entendu, par les dispositions citées au point 12, faire obstacle à ce principe, le juge doit faire application aux opérations électorales mentionnées à ce même point des dispositions de cet article dans sa rédaction issue de la loi du 2 décembre 2019. En effet, cette loi nouvelle laisse désormais au juge, de façon générale, une simple faculté de déclarer inéligible un candidat en la limitant aux cas où il relève une volonté de fraude ou un manquement d'une particulière gravité aux règles de financement des campagnes électorales, alors que l'article L. 118-3 dans sa version antérieure, d'une part, prévoyait le prononcé de plein droit d'une inéligibilité lorsque le compte de campagne avait été rejeté à bon droit en cas de volonté de fraude ou de manquement d'une particulière gravité et, d'autre part,  n'imposait pas cette dernière condition pour que puisse être prononcée une inéligibilité lorsque le candidat n'avait pas déposé son compte de campagne dans les conditions et le délai prescrit par l'article L. 52-12 de ce même code.<br/>
<br/>
              14. Aux termes de l'article L. 52-12 du code électoral, dans sa rédaction antérieure à l'intervention de la loi du 2 décembre 2019 : " Chaque candidat ou candidat tête de liste soumis au plafonnement prévu à l'article L. 52-11 et qui a obtenu au moins 1 % des suffrages exprimés est tenu d'établir un compte de campagne retraçant, selon leur origine, l'ensemble des recettes perçues et, selon leur nature, l'ensemble des dépenses engagées ou effectuées en vue de l'élection, hors celles de la campagne officielle par lui-même ou pour son compte, au cours de la période mentionnée à l'article L. 52-4 (...). Le compte de campagne est présenté par un membre de l'ordre des experts-comptables et des comptables agréés (...) ". Aux termes de l'article L. 52-15 du même code : " La Commission nationale des comptes de campagne et des financements politiques approuve et, après procédure contradictoire, rejette ou réforme les comptes de campagne. (...) Lorsque la commission a constaté que le compte de campagne n'a pas été déposé dans le délai prescrit, si le compte a été rejeté (...), la commission saisit le juge de l'élection (...) ". <br/>
<br/>
              15. En premier lieu, si M. E... soutient qu'il n'a pas été mis à même de présenter ses observations préalablement à la décision de la Commission nationale des comptes de campagne et des financements politiques, il résulte de l'instruction que la rapporteure auprès de la commission lui a adressé, le 24 août 2020, une lettre, qu'il a reçue le 11 septembre suivant, énonçant les irrégularités relevées dans son compte de campagne et l'invitant à lui communiquer ses observations en réponse dans un délai de huit jours. Par suite, le requérant n'est pas fondé à soutenir que la décision de la commission en date du 28 septembre 2020 serait irrégulière faute d'avoir été précédée de la procédure contradictoire prévue par les dispositions précitées de l'article L. 52-15 du code électoral.<br/>
<br/>
              16. En second lieu, en application des dispositions précitées de l'article L. 118-3 du code électoral, dans sa rédaction issue de la loi du 2 décembre 2019, en dehors des cas de fraude, le juge de l'élection ne peut prononcer l'inéligibilité d'un candidat sur le fondement de ces dispositions que s'il constate un manquement d'une particulière gravité aux règles de financement des campagnes électorales. Il lui incombe à cet effet de prendre en compte l'ensemble des circonstances de l'espèce et d'apprécier s'il s'agit d'un manquement caractérisé à une règle substantielle relative au financement des campagnes électorales et s'il présente un caractère délibéré. <br/>
<br/>
              17. Il est constant qu'alors même que la liste conduite par M. E... a obtenu plus de 1 % des suffrages exprimés, son compte de campagne n'a pas été présenté dans les délais légaux par un membre de l'ordre des experts-comptables et des comptables agréés, en méconnaissance des dispositions citées ci-dessus de l'article L. 52-12 du code électoral, ce qui a conduit au rejet de ce compte par la Commission nationale des comptes de campagne et des financements politiques. Il résulte toutefois de l'instruction que M. E... a, postérieurement à la décision de la commission, communiqué au tribunal administratif de la Guyane son compte de campagne présenté par un membre de l'ordre des experts comptables et des comptables agréés, sans que ce compte ne comporte d'irrégularités ni ne présente de différence notable avec celui qui avait été soumis préalablement à la Commission. Eu égard au faible montant des recettes et dépenses du compte, de l'ordre de 8 000 euros, et, dans les circonstances de l'espèce, au caractère non délibéré du manquement en cause, celui-ci ne justifie pas, dans ces circonstances, que M. E... soit déclaré inéligible en application des dispositions précitées de l'article L. 118-3 du code électoral.<br/>
<br/>
              18. Il résulte de tout ce qui précède que M. E... est seulement fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif l'a déclaré inéligible pour une durée d'un an et a, par voie de conséquence, annulé son élection et proclamé deux nouveaux élus respectivement au conseil municipal et au conseil communautaire.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de MM. Saint B... et J... est rejetée.<br/>
Article 2 : Les articles 2 et 3 du jugement du 24 décembre 2020 du tribunal administratif de la Guyane sont annulés. <br/>
Article 3 : Le surplus des conclusions de la requête de M. E... est rejeté.<br/>
Article 4 : La présente décision sera notifiée à M. O... G... B..., premier requérant dénommé, à M. F... E... et à M. I... A....<br/>
Copie en sera adressée à M. M... H... et à Mme N... D..., à la Commission nationale des comptes de campagne et des financements politiques et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-08-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. APPLICATION DANS LE TEMPS. TEXTE APPLICABLE. - LOI DU 2 DÉCEMBRE 2019 MODIFIANT LE CODE ÉLECTORAL - 1) PRINCIPE - APPLICABILITÉ AUX ÉLECTIONS MUNICIPALES DE 2020 - ABSENCE - 2) EXCEPTION - INÉLIGIBILITÉ PRÉVUE PAR L'ARTICLE L. 118-3 DU CODE ÉLECTORAL, DANS SA RÉDACTION ISSUE DE CETTE LOI - LOI RÉPRESSIVE NOUVELLE PLUS DOUCE - CONSÉQUENCE - APPLICATION IMMÉDIATE (RÉTROACTIVITÉ IN MITIUS) [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">28-005-04-02-04 ÉLECTIONS ET RÉFÉRENDUM. DISPOSITIONS GÉNÉRALES APPLICABLES AUX ÉLECTIONS. FINANCEMENT ET PLAFONNEMENT DES DÉPENSES ÉLECTORALES. COMPTE DE CAMPAGNE. DÉPENSES. - INÉLIGIBILITÉ PRÉVUE PAR L'ARTICLE L. 118-3 DU CODE ÉLECTORAL, DANS SA RÉDACTION ISSUE DE LA LOI DU 2 DÉCEMBRE 2019 - 1) MODALITÉS D'APPLICATION - 2) ESPÈCE [RJ2].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">28-005-04-04 ÉLECTIONS ET RÉFÉRENDUM. DISPOSITIONS GÉNÉRALES APPLICABLES AUX ÉLECTIONS. FINANCEMENT ET PLAFONNEMENT DES DÉPENSES ÉLECTORALES. PORTÉE DE L'INÉLIGIBILITÉ. - INÉLIGIBILITÉ PRÉVUE PAR L'ARTICLE L. 118-3 DU CODE ÉLECTORAL, DANS SA RÉDACTION ISSUE DE LA LOI DU 2 DÉCEMBRE 2019 - 1) MODALITÉS D'APPLICATION - 2) ESPÈCE [RJ2].
</SCT>
<ANA ID="9A"> 01-08-03 1) Il résulte du premier alinéa de l'article 15 de la loi n° 2019-1269 du 2 décembre 2019 et du XVI de l'article 19 de la loi n° 2020-290 du 23 mars 2020 que les dispositions de la loi du 2 décembre 2019 modifiant celles du code électoral, à l'exception de son article 6, ne sont pas applicables aux opérations électorales en vue de l'élection des conseillers municipaux et communautaires organisées les 15 mars et 28 juin 2020, y compris en ce qui concerne les comptes de campagne.... ,,2) Toutefois, l'inéligibilité prévue par l'article L. 118?3 du code électoral constitue une sanction ayant le caractère d'une punition. Il incombe dès lors au juge de l'élection, lorsqu'il est saisi de conclusions tendant à ce qu'un candidat dont le compte de campagne est rejeté soit déclaré inéligible et à ce que son élection soit annulée, de faire application, le cas échéant, d'une loi nouvelle plus douce entrée en vigueur entre la date des faits litigieux et celle à laquelle il statue.,,,Le législateur n'ayant pas entendu, par les dispositions mentionnées au point précédent, faire obstacle à ce principe, le juge doit faire application aux opérations électorales mentionnées à ce même point de cet article dans sa rédaction issue de la loi du 2 décembre 2019. En effet, cette loi nouvelle laisse désormais au juge, de façon générale, une simple faculté de déclarer inéligible un candidat en la limitant aux cas où il relève une volonté de fraude ou un manquement d'une particulière gravité aux règles de financement des campagnes électorales, alors que l'article L 118-3 dans sa version antérieure, d'une part, prévoyait le prononcé de plein droit d'une inéligibilité lorsque le compte de campagne avait été rejeté à bon droit en cas de volonté de fraude ou de manquement d'une particulière gravité et, d'autre part,  n'imposait pas cette dernière condition pour que puisse être prononcée une inéligibilité lorsque le candidat n'avait pas déposé son compte de campagne dans les conditions et le délai prescrits par l'article L 52-12 de ce même code.</ANA>
<ANA ID="9B"> 28-005-04-02-04 1) En application de l'article L. 118-3 du code électoral, dans sa rédaction issue de la loi n° 2019-1269 du 2 décembre 2019, en dehors des cas de fraude, le juge de l'élection ne peut prononcer l'inéligibilité d'un candidat sur le fondement de ces dispositions que s'il constate un manquement d'une particulière gravité aux règles de financement des campagnes électorales.... ,,Il lui incombe à cet effet de prendre en compte l'ensemble des circonstances de l'espèce et d'apprécier s'il s'agit d'un manquement caractérisé à une règle substantielle relative au financement des campagnes électorales et s'il présente un caractère délibéré.... ,,2) Liste ayant obtenu plus de 1 % des suffrages exprimés, mais dont le compte de campagne n'a pas été présenté dans les délais légaux par un membre de l'ordre des experts-comptables et des comptables agréés, en méconnaissance de l'article L. 52-12 du code électoral, ce qui a conduit au rejet de ce compte par la Commission nationale des comptes de campagne et des financements politiques (CNCCFP).,,,Tête de liste ayant, postérieurement à la décision de la Commission, communiqué au tribunal administratif son compte de campagne présenté par un membre de l'ordre des experts comptables et des comptables agréés, sans que ce compte ne comporte d'irrégularités ni ne présente de différence notable avec celui qui avait été soumis préalablement à la Commission.,,,Eu égard au faible montant des recettes et dépenses du compte, de l'ordre de 8 000 euros, et, dans les circonstances de l'espèce, au caractère non délibéré du manquement en cause, celui-ci ne justifie pas, dans ces circonstances, que la tête de liste soit déclarée inéligible en application de l'article L. 118-3 du code électoral.</ANA>
<ANA ID="9C"> 28-005-04-04 1) En application de l'article L. 118-3 du code électoral, dans sa rédaction issue de la loi n° 2019-1269 du 2 décembre 2019, en dehors des cas de fraude, le juge de l'élection ne peut prononcer l'inéligibilité d'un candidat sur le fondement de ces dispositions que s'il constate un manquement d'une particulière gravité aux règles de financement des campagnes électorales.... ,,Il lui incombe à cet effet de prendre en compte l'ensemble des circonstances de l'espèce et d'apprécier s'il s'agit d'un manquement caractérisé à une règle substantielle relative au financement des campagnes électorales et s'il présente un caractère délibéré.... ,,2) Liste ayant obtenu plus de 1 % des suffrages exprimés, mais dont le compte de campagne n'a pas été présenté dans les délais légaux par un membre de l'ordre des experts-comptables et des comptables agréés, en méconnaissance de l'article L. 52-12 du code électoral, ce qui a conduit au rejet de ce compte par la Commission nationale des comptes de campagne et des financements politiques (CNCCFP).... ,,Tête de liste ayant, postérieurement à la décision de la Commission, communiqué au tribunal administratif son compte de campagne présenté par un membre de l'ordre des experts comptables et des comptables agréés, sans que ce compte ne comporte d'irrégularités ni ne présente de différence notable avec celui qui avait été soumis préalablement à la Commission.,, ...Eu égard au faible montant des recettes et dépenses du compte, de l'ordre de 8 000 euros, et, dans les circonstances de l'espèce, au caractère non délibéré du manquement en cause, celui-ci ne justifie pas, dans ces circonstances, que la tête de liste soit déclarée inéligible en application de l'article L. 118-3 du code électoral.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur le caractère de punition de cette inéligibilité et sur l'application de la rétroactivité in mitius, CE, Assemblée, 4 juillet 2011, Elections régionales d'Ile-de-France, n°, 338033 338199, p. 317.,,[RJ2] Cf. sol. contr. CE, décision du même jour, M. Faroult, n° 449279, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
