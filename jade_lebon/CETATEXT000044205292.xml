<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044205292</ID>
<ANCIEN_ID>JG_L_2021_10_000000441390</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/20/52/CETATEXT000044205292.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 13/10/2021, 441390</TITRE>
<DATE_DEC>2021-10-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>441390</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MARLANGE, DE LA BURGADE</AVOCATS>
<RAPPORTEUR>M. Lionel Ferreira</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Céline  Guibé</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:441390.20211013</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. H... O... a demandé au tribunal administratif de Montpellier d'annuler l'arrêté du 8 juin 2018 par lequel le ministre de l'action et des comptes publics a annulé, à compter du 1er janvier 2015, la pension de réversion dont il bénéficiait. Par un jugement n° 1804011 du 28 février 2020, ce tribunal a rejeté sa demande.<br/>
<br/>
              Par une ordonnance n° 20MA01686 du 16 juin 2020, enregistrée le 24 juin 2020 au secrétariat du contentieux du Conseil d'Etat, la présidente de la cour administrative d'appel de Marseille a transmis au Conseil d'Etat, en application des articles R. 351-2 et R. 811-1 du code de justice administrative, le pourvoi formé par M. O... contre ce jugement.<br/>
<br/>
              Par ce pourvoi ainsi qu'un mémoire complémentaire et un mémoire en réplique enregistrés les 16 septembre 2020 et 22 septembre 2021 au secrétariat du contentieux du Conseil d'Etat, M. O... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement du tribunal administratif de Montpellier ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le sénatus-consulte du 14 juillet 1865 ;<br/>
              - l'ordonnance n° 45-2441 du 19 octobre 1945 ;<br/>
              - l'ordonnance n° 62-825 du 21 juillet 1962 ;<br/>
              - le code civil ;<br/>
              - le code des pensions civiles et militaires de retraite ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Lionel Ferreira, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Céline Guibé, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Marlange, de la Burgade, avocat de M. O... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que M. O... a épousé Mme B... le 13 juin 1957, puis Mme G... le 1er avril 1959. Par un arrêté du 8 juin 2018, le ministre de l'action et des comptes publics a remis en cause, à compter du 1er janvier 2015, le bénéfice la pension de réversion obtenue, à la suite du décès de sa seconde épouse en 2006, au motif que sa première union n'avait pas été dissoute. M. O... se pourvoit en cassation contre le jugement par lequel le tribunal administratif de Montpellier a rejeté sa demande tendant à l'annulation de cet arrêté.<br/>
<br/>
              2. En premier lieu, aux termes de l'article L. 38 du code des pensions civiles et militaires de retraite : " Les conjoints d'un fonctionnaire civil ont droit à une pension de réversion égale à 50 % de la pension obtenue par le fonctionnaire ou qu'il aurait pu obtenir au jour de son décès. " Aux termes de l'article L. 46 du même code : " Le conjoint survivant (...) qui contracte un nouveau mariage ou vit en état de concubinage notoire, perd son droit à pension. / Le conjoint survivant (...), dont la nouvelle union est dissoute ou qui cesse de vivre en état de concubinage notoire, peut, s'il le désire, recouvrer son droit à pension et demander qu'il soit mis fin à l'application qui a pu être faite des dispositions du premier alinéa du présent article ". Il résulte de ces dispositions, au regard de l'objet essentiellement alimentaire de la pension de réversion, que le conjoint qui a contracté, avant le décès du fonctionnaire, un autre mariage en vertu du statut personnel auquel il était soumis ou en vertu d'une loi étrangère, et qui ne vit ainsi pas de ses seules ressources, voit son droit à pension de réversion suspendu mais peut le recouvrer lorsque cesse cet autre mariage.<br/>
<br/>
              3. Il résulte de ce qui a été dit au point 2 que si le décès de la deuxième épouse de M. O... a ouvert, en l'absence d'annulation de ce mariage contracté en vertu du statut civil de droit local alors applicable en Algérie, un droit à pension de réversion, le tribunal n'a pas commis d'erreur de droit ni dénaturé les pièces du dossier en jugeant que les dispositions de l'article L. 46 du code des pensions civiles et militaires de retraite permettaient au ministre de l'action et des comptes publics de suspendre ce droit au motif que son premier mariage n'était pas dissous. <br/>
<br/>
              4. En second lieu, aux termes de l'article L. 93 du code des pensions civiles et militaires de retraite : " Sauf le cas de fraude, omission, déclaration inexacte ou de mauvaise foi de la part du bénéficiaire, la restitution des sommes payées indûment au titre des pensions, de leurs accessoires ou d'avances provisoires sur pensions, attribués en application des dispositions du présent code, ne peut être exigée que pour celles de ces sommes correspondant aux arrérages afférents à l'année au cours de laquelle le trop-perçu a été constaté et aux trois années antérieures. " Aux termes de l'article L. 55 du même code : " (...) la pension et la rente viagère d'invalidité sont définitivement acquises et ne peuvent être révisées ou supprimées à l'initiative de l'administration (...) que dans les conditions suivantes : / A tout moment en cas d'erreur matérielle ; / Dans un délai d'un an à compter de la notification de la décision de concession initiale de la pension (...), en cas d'erreur de droit. / La restitution des sommes payées indûment au titre de la pension (...) supprimée ou révisée est exigible lorsque l'intéressé était de mauvaise foi ". Les dispositions de ce dernier article ne s'appliquent pas lorsque l'administration constate, en application des dispositions de l'article L. 46 du code des pensions civiles et militaires de retraite, la perte du droit à pension de réversion, en cas de mariage ou de concubinage notoire du conjoint survivant ou divorcé, et qu'elle en suspend, en conséquence, le versement. Dans une telle hypothèse, elle peut exiger la restitution des pensions indûment versées dans le respect des règles de prescription édictées à l'article L. 93 du même code. <br/>
<br/>
              5. Il résulte de ce qui a été dit au point 4 que le tribunal n'a pas commis d'erreur de droit ni entaché son jugement d'insuffisance de motivation en jugeant que le ministre avait légalement pu remettre en cause le bénéfice de la pension de réversion accordée à M. O... en 2006 sur le fondement de l'article L. 46 du code des pensions civiles et militaires de retraite et que la circonstance que l'administration ait eu connaissance de son autre mariage était sans incidence dès lors que les règles de prescription prévues par l'article L. 93 de ce code avaient été respectées.<br/>
<br/>
              6. Il résulte de tout ce qui précède que le pourvoi de M. O... doit être rejeté.<br/>
<br/>
              7. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacles à ce qu'une somme soit mise à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. O... est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. O... et au ministre de l'économie, des finances et de la relance.<br/>
<br/>
              Délibéré à l'issue de la séance du 29 septembre 2021 où siégeaient : M. Bertrand Dacosta, président de chambre, présidant ; M. Frédéric Aladjidi, président de chambre ; Mme A... N..., Mme J... C..., M. E... F..., M. K... D..., Mme L... P..., M. Alain Seban, conseillers d'Etat et M. Lionel Ferreira, maître des requêtes en service extraordinaire-rapporteur.<br/>
<br/>
              Rendu le 13 octobre 2021.<br/>
                 Le président : <br/>
                 Signé : M. Bertrand Dacosta<br/>
 		Le rapporteur : <br/>
      Signé : M. Lionel Ferreira<br/>
                 La secrétaire :<br/>
                 Signé : Mme I... M...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">48-02-01-09 PENSIONS. - PENSIONS CIVILES ET MILITAIRES DE RETRAITE. - QUESTIONS COMMUNES. - AYANTS-CAUSE. - EXCLUSION - CONJOINT POLYGAME [RJ1].
</SCT>
<ANA ID="9A"> 48-02-01-09 Il résulte des articles L. 38 et L. 46 du code des pensions civiles et militaires de retraite (CPCMR), au regard de l'objet essentiellement alimentaire de la pension de réversion, que le conjoint qui a contracté, avant le décès du fonctionnaire, un autre mariage en vertu du statut personnel auquel il était soumis ou en vertu d'une loi étrangère, et qui ne vit ainsi pas de ses seules ressources, voit son droit à pension de réversion suspendu mais peut le recouvrer lorsque cesse cet autre mariage.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., en cas de concubinage notoire, CE, 13 juillet 2012, Mme Gaiero, n° 352571, T. p. 882.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
