<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041626670</ID>
<ANCIEN_ID>JG_L_2020_02_000000421086</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/62/66/CETATEXT000041626670.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 24/02/2020, 421086</TITRE>
<DATE_DEC>2020-02-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421086</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Jean-Marc Vié</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:421086.20200224</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société civile immobilière et forestière (SCIF) des Fourneaux et M. B... A..., son gérant, ont demandé au tribunal administratif d'Orléans d'annuler l'arrêté du 6 novembre 2014 par lequel le maire de la commune de Cerdon-du-Loiret a mis en demeure M. A... de retirer tous les obstacles à la circulation du chemin rural n° 20. Par un jugement n° 1500036 du 29 mars 2016, ce tribunal a fait droit à cette demande. <br/>
<br/>
              Par un arrêt n° 16NT01529 du 30 mars 2018, la cour administrative d'appel de Nantes a, sur appel de la commune de Cerdon-du-Loiret, annulé ce jugement et rejeté la demande présentée par la société des Fourneaux et M. A....<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 30 mai 2018, 29 août 2018 et 3 février 2020 au secrétariat du contentieux du Conseil d'Etat, la société des Fourneaux et M. A... demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la commune de Cerdon-du-Loiret ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Cerdon-du-Loiret la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 2000-321 du 12 avril 2000 ;<br/>
              - le code rural et de la pêche maritime ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Vié, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de la société des Fourneaux et de M. A... et à la SCP Waquet, Farge, Hazan, avocat de la commune de Cerdon-du-Loiret ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que la société des Fourneaux est propriétaire, sur le territoire de la commune de Cerdon-du-Loiret, de parcelles cadastrées AK n° 147 à 149, 151 à 155, 157, 272 à 275 situées au lieu-dit domaine de la " Pinaudière ", traversées par un chemin, dénommé par la commune " chemin rural n° 20 " ou " chemin de Clémont à Coullons ", sur lequel cette société a installé une chaîne afin d'en interdire l'accès, ainsi qu'un bloc de béton au sol. Par un arrêté du 6 novembre 2014, le maire de Cerdon-du-Loiret a mis en demeure M. A..., gérant de la société de retirer tous les obstacles à la circulation du chemin rural n° 20.  La société des Fourneaux et M. A... se pourvoient en cassation contre l'arrêt du 30 mars 2018 par lequel la cour administrative d'appel de Nantes a annulé le jugement du 29 mars 2016 du tribunal administratif d'Orléans annulant, à leur demande, l'arrêté du 6 novembre 2014, et a rejeté la demande présentée devant ce tribunal.<br/>
<br/>
              2. D'une part, aux termes de l'article L. 161-5 du code rural et de la pêche maritime : " L'autorité municipale est chargée de la police et de la conservation des chemins ruraux ". Aux termes de l'article D. 161-11 du même code : " Lorsqu'un obstacle s'oppose à la circulation sur un chemin rural, le maire y remédie d'urgence (...). ".<br/>
<br/>
              3. Il résulte de la lettre même de ces dispositions que le maire a l'obligation de remédier à l'obstacle qui s'oppose à la circulation sur un chemin rural. Toutefois, pour relever l'existence d'un obstacle à la circulation sur le chemin rural et pour déterminer les mesures qui s'imposent, le maire est nécessairement conduit à porter une appréciation sur les faits de l'espèce, notamment sur l'ampleur de la gêne occasionnée et ses conséquences. Ainsi, le maire ne peut être regardé comme se trouvant en situation de compétence liée pour prendre les mesures prévues par l'article D. 161-11 du code rural.<br/>
<br/>
              4. D'autre part, aux termes de l'article 24 de la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations, alors applicable : " Exception faite des cas où il est statué sur une demande, les décisions individuelles qui doivent être motivées en application des articles 1er et 2 de la loi n° 79-587 du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public n'interviennent qu'après que la personne intéressée a été mise à même de présenter des observations écrites et, le cas échéant, sur sa demande, des observations orales. Cette personne peut se faire assister par un conseil ou représenter par un mandataire de son choix. L'autorité administrative n'est pas tenue de satisfaire les demandes d'audition abusives, notamment par leur nombre, leur caractère répétitif ou systématique. / Les dispositions de l'alinéa précédent ne sont pas applicables : / 1° En cas d'urgence ou de circonstances exceptionnelles ; (...) ". <br/>
<br/>
              5. Si les dispositions de l'article D. 161-11 du code rural imposent au maire, lorsqu'un obstacle s'oppose à la circulation sur un chemin rural, de prendre sans délai les mesures propres à remédier à la situation, les conditions dans lesquelles il est ainsi tenu de mettre en oeuvre ses pouvoirs de police ne traduisent pas nécessairement l'existence d'une situation d'urgence, au sens du 1° précité du deuxième alinéa de l'article 24 de la loi du 12 avril 2000, de nature à dispenser l'autorité administrative de faire précéder sa décision d'une procédure contradictoire. L'existence d'une telle situation d'urgence doit être appréciée concrètement, en fonction des circonstances de l'espèce.<br/>
<br/>
              6. Pour écarter comme inopérant le moyen, soulevé par la société des Fourneaux et par M. A..., tiré de ce que l'arrêté du 6 novembre 2014 par lequel le maire de la commune de Cerdon-du-Loiret a mis en demeure M. A... de retirer tous les obstacles à la circulation sur le chemin rural n° 20 n'avait pas été précédé de la procédure contradictoire prévue par l'article 24 de la loi du 12 avril 2000, la cour s'est fondée sur ce que le maire était, une fois constatée la présence d'obstacles à la circulation sur ce chemin, en situation de compétence liée pour prendre l'arrêté contesté. En statuant ainsi, alors qu'ainsi qu'il a été dit au point 3, le maire ne se trouvait pas, pour procéder à cette mise en demeure, en situation de compétence liée, la cour administrative d'appel a commis une erreur de droit. Dès lors, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt être annulé. <br/>
<br/>
              7. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société des Fourneaux et de M. A... qui ne sont pas, dans la présente instance, la partie perdante. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Cerdon-de-Loiret la somme de 3 000 euros à verser à la société des Fourneaux et M. A... sur le même fondement.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 30 mars 2018 de la cour administrative d'appel de Nantes est annulé. <br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nantes.<br/>
Article 3 : La commune de Cerdon-du-Loiret versera à la société des Fourneaux et M. A... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de la commune de Cerdon-du-Loiret présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la société civile immobilière et forestière des Fourneaux et M. B... A... et à la commune de Cerdon-du-Loiret.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-03-03-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. PROCÉDURE CONTRADICTOIRE. CARACTÈRE NON OBLIGATOIRE. - OBLIGATION DU MAIRE DE REMÉDIER D'URGENCE À UN OBSTACLE S'OPPOSANT À LA CIRCULATION SUR UN CHEMIN RURAL (ART. D. 161-11 DU CRPM) - EXISTENCE D'UNE SITUATION D'URGENCE AU SENS DE L'ARTICLE L. 121-2 DU CRPA DISPENSANT DU RESPECT DE LA PROCÉDURE CONTRADICTOIRE - APPRÉCIATION EN FONCTION DES CIRCONSTANCES DE L'ESPÈCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">01-05-01-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - MOTIFS. POUVOIRS ET OBLIGATIONS DE L'ADMINISTRATION. COMPÉTENCE LIÉE. - ABSENCE [RJ1] - MISE EN &#140;UVRE PAR LE MAIRE DE SES POUVOIRS DE POLICE POUR REMÉDIER À UN OBSTACLE S'OPPOSANT À LA CIRCULATION SUR UN CHEMIN RURAL (ART. D. 161-11 DU CRPM).
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">135-02-02-04 COLLECTIVITÉS TERRITORIALES. COMMUNE. BIENS DE LA COMMUNE. CHEMINS RURAUX. - POLICE DES CHEMINS RURAUX - OBLIGATION DU MAIRE DE REMÉDIER D'URGENCE À UN OBSTACLE S'OPPOSANT À LA CIRCULATION (ART. D. 161-11 DU CRPM) - 1) COMPÉTENCE LIÉE [RJ1] - ABSENCE - 2) EXISTENCE D'UNE SITUATION D'URGENCE AU SENS DE L'ARTICLE L. 121-2 DU CRPA DISPENSANT DU RESPECT DE LA PROCÉDURE CONTRADICTOIRE - APPRÉCIATION EN FONCTION DES CIRCONSTANCES DE L'ESPÈCE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">135-02-03-02 COLLECTIVITÉS TERRITORIALES. COMMUNE. ATTRIBUTIONS. POLICE. - POLICE DES CHEMINS RURAUX - OBLIGATION DU MAIRE DE REMÉDIER D'URGENCE À UN OBSTACLE S'OPPOSANT À LA CIRCULATION (ART. D. 161-11 DU CRPM) - 1) COMPÉTENCE LIÉE [RJ1] - ABSENCE - 2) EXISTENCE D'UNE SITUATION D'URGENCE AU SENS DE L'ARTICLE L. 121-2 DU CRPA DISPENSANT DU RESPECT DE LA PROCÉDURE CONTRADICTOIRE - APPRÉCIATION EN FONCTION DES CIRCONSTANCES DE L'ESPÈCE.
</SCT>
<ANA ID="9A"> 01-03-03-02 Si les dispositions de l'article D. 161-11 du code rural et de la pêche maritime (CRPM) imposent au maire, lorsqu'un obstacle s'oppose à la circulation sur un chemin rural, de prendre sans délai les mesures propres à remédier à la situation, les conditions dans lesquelles il est ainsi tenu de mettre en oeuvre ses pouvoirs de police ne traduisent pas nécessairement l'existence d'une situation d'urgence, au sens du 1° du deuxième alinéa de l'article 24 de la loi n° 2000-321 du 12 avril 2000, désormais codifié à l'article L. 121-2 du code des relations entre le public et l'administration (CRPA), de nature à dispenser l'autorité administrative de faire précéder sa décision d'une procédure contradictoire. L'existence d'une telle situation d'urgence doit être appréciée concrètement, en fonction des circonstances de l'espèce.</ANA>
<ANA ID="9B"> 01-05-01-03 Il résulte des articles L. 161-5 et D. 161-11 du code rural et de la pêche maritime (CRPM) que le maire a l'obligation de remédier à l'obstacle qui s'oppose à la circulation sur un chemin rural. Toutefois, pour relever l'existence d'un obstacle à la circulation sur le chemin rural et pour déterminer les mesures qui s'imposent, le maire est nécessairement conduit à porter une appréciation sur les faits de l'espèce, notamment sur l'ampleur de la gêne occasionnée et ses conséquences. Ainsi, le maire ne peut être regardé comme se trouvant en situation de compétence liée pour prendre les mesures prévues par l'article D. 161-11 du code rural.</ANA>
<ANA ID="9C"> 135-02-02-04 1) Il résulte des articles L. 161-5 et D. 161-11 du code rural et de la pêche maritime (CRPM) que le maire a l'obligation de remédier à l'obstacle qui s'oppose à la circulation sur un chemin rural. Toutefois, pour relever l'existence d'un obstacle à la circulation sur le chemin rural et pour déterminer les mesures qui s'imposent, le maire est nécessairement conduit à porter une appréciation sur les faits de l'espèce, notamment sur l'ampleur de la gêne occasionnée et ses conséquences. Ainsi, le maire ne peut être regardé comme se trouvant en situation de compétence liée pour prendre les mesures prévues par l'article D. 161-11 du CRPM.,,,2) Si les dispositions de l'article D. 161-11 du CRPM imposent au maire, lorsqu'un obstacle s'oppose à la circulation sur un chemin rural, de prendre sans délai les mesures propres à remédier à la situation, les conditions dans lesquelles il est ainsi tenu de mettre en oeuvre ses pouvoirs de police ne traduisent pas nécessairement l'existence d'une situation d'urgence, au sens du 1° du deuxième alinéa de l'article 24 de la loi n° 2000-321 du 12 avril 2000, désormais codifié à l'article L. 121-2 du code des relations entre le public et l'administration (CRPA), de nature à dispenser l'autorité administrative de faire précéder sa décision d'une procédure contradictoire. L'existence d'une telle situation d'urgence doit être appréciée concrètement, en fonction des circonstances de l'espèce.</ANA>
<ANA ID="9D"> 135-02-03-02 1) Il résulte des articles L. 161-5 et D. 161-11 du code rural et de la pêche maritime (CRPM) que le maire a l'obligation de remédier à l'obstacle qui s'oppose à la circulation sur un chemin rural. Toutefois, pour relever l'existence d'un obstacle à la circulation sur le chemin rural et pour déterminer les mesures qui s'imposent, le maire est nécessairement conduit à porter une appréciation sur les faits de l'espèce, notamment sur l'ampleur de la gêne occasionnée et ses conséquences. Ainsi, le maire ne peut être regardé comme se trouvant en situation de compétence liée pour prendre les mesures prévues par l'article D. 161-11 du CRPM.,,,2) Si les dispositions de l'article D. 161-11 du CRPM imposent au maire, lorsqu'un obstacle s'oppose à la circulation sur un chemin rural, de prendre sans délai les mesures propres à remédier à la situation, les conditions dans lesquelles il est ainsi tenu de mettre en oeuvre ses pouvoirs de police ne traduisent pas nécessairement l'existence d'une situation d'urgence, au sens du 1° du deuxième alinéa de l'article 24 de la loi n° 2000-321 du 12 avril 2000, désormais codifié à l'article L. 121-2 du code des relations entre le public et l'administration (CRPA), de nature à dispenser l'autorité administrative de faire précéder sa décision d'une procédure contradictoire. L'existence d'une telle situation d'urgence doit être appréciée concrètement, en fonction des circonstances de l'espèce.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 3 février 1999, M.,, n° 149722, p. 6.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
