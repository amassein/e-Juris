<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033464862</ID>
<ANCIEN_ID>JG_L_2016_11_000000383134</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/46/48/CETATEXT000033464862.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 5ème chambres réunies, 23/11/2016, 383134</TITRE>
<DATE_DEC>2016-11-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>383134</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP RICHARD ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Jean-François de Montgolfier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:383134.20161123</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le conseil départemental de Loire-Atlantique de l'ordre des chirurgiens-dentistes a porté plainte contre M. B...A...devant la chambre disciplinaire de première instance des Pays de la Loire de l'ordre des chirurgiens-dentistes. Par une décision du 18 février 2013, la chambre disciplinaire de première instance a infligé à M. A...la sanction du blâme. <br/>
<br/>
              Par une décision du 26 mai 2014, la chambre disciplinaire nationale de l'ordre des chirurgiens-dentistes a rejeté l'appel formé par M. A...contre cette décision. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 28 juillet 2014, 27 octobre 2014 et 27 juin 2016 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ; <br/>
<br/>
              2°) de mettre à la charge du conseil départemental de Loire-Atlantique de l'ordre des chirurgiens-dentistes la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-François de Montgolfier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Richard, avocat de M. A...et à la SCP Lyon-Caen, Thiriez, avocat du conseil départemental de Loire-Atlantique de l'ordre des chirurgiens-dentistes ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, saisi par l'une des patientes de M.A..., chirurgien-dentiste, d'une plainte contre ce praticien, le président du conseil départemental de Loire-Atlantique de l'ordre des chirurgiens-dentistes a organisé une réunion de conciliation entre le praticien et la plaignante ainsi que le prévoit l'article L. 4123-2 du code de la santé publique ; qu'à l'issue de cette conciliation, la patiente a retiré sa plainte ; que, nonobstant ce retrait, le conseil départemental a alors, pour les mêmes faits, décidé de saisir la chambre disciplinaire de première instance des Pays de la Loire d'une plainte contre M. A...; que, par une décision du 18 février 2013, la chambre disciplinaire a infligé au praticien la sanction du blâme ; que M. A...se pourvoit en cassation contre la décision du 26 mai 2014 par laquelle le Conseil national de l'ordre des chirurgiens-dentistes a rejeté son appel contre cette décision ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 4123-2 du code de la santé publique : " Lorsqu'une plainte est portée devant le conseil départemental, son président en accuse réception à l'auteur, en informe le médecin, le chirurgien-dentiste ou la sage-femme mis en cause et les convoque dans un délai d'un mois à compter de la date d'enregistrement de la plainte en vue d'une conciliation. En cas d'échec de celle-ci, il transmet la plainte à la chambre disciplinaire de première instance avec l'avis motivé du conseil dans un délai de trois mois à compter de la date d'enregistrement de la plainte, en s'y associant le cas échéant. (...) " ; qu'aux termes de l'article R. 4126-1 du code de la santé publique : " L'action disciplinaire contre un médecin, un chirurgien-dentiste ou une sage-femme ne peut être introduite devant la chambre disciplinaire de première instance que par l'une des personnes ou autorités suivantes : / 1° Le Conseil national ou le conseil départemental de l'ordre au tableau duquel le praticien poursuivi est inscrit à la date de la saisine de la juridiction, agissant de leur propre initiative ou à la suite de plaintes, formées notamment par les patients, les organismes locaux d'assurance maladie obligatoires, les médecins-conseils chefs ou responsables du service du contrôle médical placé auprès d'une caisse ou d'un organisme de sécurité sociale, les associations de défense des droits des patients, des usagers du système de santé ou des personnes en situation de précarité, qu'ils transmettent, le cas échéant en s'y associant, dans le cadre de la procédure prévue à l'article L. 4123-2 (...) " ;<br/>
<br/>
              3. Considérant qu'il résulte des dispositions citées ci-dessus que les instances disciplinaires ordinales peuvent être saisies, soit par la voie d'une action introduite par les instances ordinales compétentes, soit par la voie d'une plainte qui doit, alors, donner préalablement lieu à une tentative de conciliation organisée par le conseil départemental de l'ordre ; qu'eu égard à l'objet de cette procédure de conciliation, qui est de permettre aux parties de régler le différend qui les oppose, et à la mission de l'ordre, qu'il exerce à travers ses différents conseils, de veiller au respect de la déontologie médicale, ces dispositions ne font pas obstacle à ce que, lorsqu'une plainte est retirée à la suite de la procédure de conciliation, le conseil départemental saisisse l'instance disciplinaire pour les mêmes faits que ceux ayant donné lieu à la plainte ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède qu'en jugeant que la plainte du conseil départemental de Loire-Atlantique de l'ordre des chirurgiens-dentistes était recevable nonobstant la circonstance que la patiente avait retiré sa plainte à l'issue de la procédure de conciliation organisée, conformément aux dispositions du code de la santé publique citées au point 2, par ce même conseil départemental, la chambre disciplinaire du Conseil national de l'ordre n'a pas commis d'erreur de droit ;<br/>
<br/>
              5. Considérant que la chambre disciplinaire nationale a relevé par une appréciation souveraine, au demeurant non contestée en cassation, que M. A...avait poursuivi la pose d'implants sur sa patiente, notamment lors de la séance du 11 mars 2010, sans avoir préalablement remédié aux complications survenues après la pose des premiers implants effectuée lors de la séance du 3 décembre 2009 ; qu'ainsi, et alors même qu'elle relevait par ailleurs que le traitement avait été interrompu postérieurement en raison de la décision de la patiente de changer de chirurgien-dentiste, la chambre disciplinaire n'a entaché sa décision ni de contradiction de motifs ni d'erreur de droit en jugeant un tel comportement fautif au regard des obligations déontologiques des chirurgiens-dentistes ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que M. A...n'est pas fondé à demander l'annulation de la décision qu'il attaque ; que, par suite, son pourvoi doit être rejeté, y compris les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ; que les conclusions présentées au même titre par le conseil départemental de Loire-Atlantique de l'ordre des chirurgiens-dentistes, qui ne mentionnent pas la partie à la charge de laquelle doit être mise la somme demandée, doivent, par suite, être également rejetées ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
			D E C I D E :<br/>
			--------------<br/>
Article 1er : Le pourvoi de M. A...est rejeté.<br/>
Article 2 : Les conclusions du conseil départemental de Loire-Atlantique de l'ordre des chirurgiens-dentistes présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. B...A...et au conseil départemental de Loire-Atlantique de l'ordre des chirurgiens-dentistes.<br/>
Copie en sera adressée au Conseil national de l'ordre des chirurgiens-dentistes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">55-04-01-01 PROFESSIONS, CHARGES ET OFFICES. DISCIPLINE PROFESSIONNELLE. PROCÉDURE DEVANT LES JURIDICTIONS ORDINALES. INTRODUCTION DE L'INSTANCE. - ORDRE DES MÉDECINS - CONSEIL DÉPARTEMENTAL AYANT ORGANISÉ UNE TENTATIVE DE CONCILIATION ENTRE LES PARTIES - FACULTÉ, EN CAS DE RETRAIT DE LA PLAINTE À LA SUITE DE LA CONCILIATION, DE SAISIR L'INSTANCE DISCIPLINAIRE POUR LES MÊMES FAITS - EXISTENCE [RJ1].
</SCT>
<ANA ID="9A"> 55-04-01-01 Il résulte des articles L. 4123-2 et R. 4126-2 du code de la santé publique que les instances disciplinaires ordinales peuvent être saisies, soit par la voie d'une action introduite par les instances ordinales compétentes, soit par la voie d'une plainte qui doit, alors, donner préalablement lieu à une tentative de conciliation organisée par le conseil départemental de l'ordre.... ,,Eu égard à l'objet de cette procédure de conciliation, qui est de permettre aux parties de régler le différend qui les oppose, et à la mission de l'ordre, qu'il exerce à travers ses différents conseils, de veiller au respect de la déontologie médicale, ces dispositions ne font pas obstacle à ce que, lorsqu'une plainte est retirée à la suite de la procédure de conciliation, le conseil départemental saisisse l'instance disciplinaire pour les mêmes faits que ceux ayant donné lieu à la plainte.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Rappr. CE, 28 décembre 2000,,, n° 196330, T. p. 1211.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
