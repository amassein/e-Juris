<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032713009</ID>
<ANCIEN_ID>JG_L_2016_06_000000387373</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/71/30/CETATEXT000032713009.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 13/06/2016, 387373, Publié au recueil Lebon</TITRE>
<DATE_DEC>2016-06-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>387373</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>DELAMARRE ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Pierre Lombard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:387373.20160613</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme A...B...épouse C...a demandé au tribunal administratif de Paris d'annuler, d'une part, les décisions des 25 janvier et 3 octobre 2012 par lesquelles le maire de Paris l'a licenciée pour inaptitude physique, puis, après suspension de l'exécution de la première de ces décisions, a de nouveau mis fin à son contrat d'assistante maternelle et, d'autre part, le contrat à durée déterminée du 15 octobre 2012 la recrutant en qualité d'animatrice remplaçante à la direction des affaires scolaires. Elle a en outre demandé au tribunal administratif de condamner la ville de Paris à réparer les préjudices qu'elle estimait avoir subis. <br/>
<br/>
              Par un jugement n° 1206948/5-3 du 2 octobre 2013, le tribunal administratif a fait droit à ses demandes.<br/>
<br/>
              Par un arrêt n° 13PA04299 du 9 octobre 2014, la cour administrative d'appel de Paris, faisant partiellement droit à l'appel de la ville de Paris, a annulé ce jugement en tant qu'il a omis de statuer sur les conclusions aux  fins de non lieu présentées par la ville de Paris et qu'il a annulé la décision du 3 octobre 2012 ainsi que le contrat conclu le 15 octobre 2012 avec Mme C....<br/>
<br/>
              Par un pourvoi et un mémoire complémentaire, enregistrés les 23 janvier et 21 avril 2015 au secrétariat du contentieux du Conseil d'Etat, Mme C... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il lui fait grief ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la ville de Paris ;<br/>
<br/>
              2°) de mettre à la charge de la ville de Paris la somme de 3 000 euros à verser à Me  Delamarre, son avocat, au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'action sociale et des familles ;<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - la loi n° 2012-347 du 12 mars 2012 ;<br/>
              - le code de justice administrative ;	<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pierre Lombard, auditeur,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Delamarre, avocat de Mme B...et à la SCP Foussard, Froger, avocat de la ville de Paris ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme C...a été recrutée le 29 août 2003 par la ville de Paris en tant qu'assistante maternelle par un contrat à durée indéterminée ; qu'à la suite d'un accident de service dont elle a été victime, la qualité de travailleur handicapé lui a été reconnue ; que son état de santé s'étant dégradé en 2011, elle a été déclarée inapte à exercer les fonctions d'assistante maternelle avant que, par un arrêté du 25 janvier 2012, le maire de Paris ne prononce son licenciement pour inaptitude physique ; que cet arrêté a cependant été suspendu, à la demande de MmeC..., par le juge des référés du tribunal administratif de Paris ; que le maire de Paris a alors mis fin au contrat à durée indéterminée de MmeC..., par un arrêté du 3 octobre 2012 prenant effet le 15 octobre, tout en signant avec elle un contrat à durée déterminée prenant effet à la même date, pour qu'elle exerce des fonctions d'animatrice ; que Mme C...a contesté ce nouvel arrêté ainsi que son contrat à durée déterminée devant le tribunal administratif de Paris ; que, par un jugement du 2 octobre 2013, le tribunal administratif de Paris, après avoir joint ces conclusions à celles par lesquelles Mme C...demandait l'annulation du premier arrêté, a fait droit à l'ensemble de ses demandes ; que, par un arrêt du 9 octobre 2014, la cour administrative d'appel de Paris a confirmé l'annulation par les premiers juges de l'arrêté du 25 janvier 2012 ; que, faisant partiellement droit à l'appel de la ville de Paris, la cour a toutefois jugé que les dispositions de l'article 3 de la loi du 26 janvier 1984 imposaient de ne proposer à l'intéressée, en vue de son reclassement, qu'un contrat à durée déterminée ; que Mme C... se pourvoit en cassation contre cet arrêt, en tant qu'il lui fait grief ;<br/>
<br/>
              2. Considérant qu'il résulte d'un principe général du droit dont s'inspirent tant les dispositions du code du travail relatives à la situation des salariés qui, pour des raisons médicales, ne peuvent plus occuper leur emploi que les règles statutaires applicables dans ce cas aux fonctionnaires, que, lorsqu'il a été médicalement constaté qu'un agent non titulaire se trouve de manière définitive atteint d'une inaptitude physique à occuper son emploi, il appartient  à l'employeur public de le reclasser dans un autre emploi et, en cas d'impossibilité, de prononcer, dans les conditions prévues pour l'intéressé, son licenciement; que ce principe est applicable en particulier aux agents contractuels de droit public ; que dans le cas où un tel agent, qui bénéficie des droits créés par son contrat de recrutement, est employé dans le cadre d'un contrat à durée indéterminée, cette caractéristique de son contrat doit être maintenue, sans que puissent y faire obstacle les dispositions applicables le cas échéant au recrutement des agents contractuels :  <br/>
<br/>
              3. Considérant qu'il résulte de ce qui précède que, si c'est à juste titre que la cour a jugé qu'il appartenait à la ville de Paris de reclasser Mme C... sur un autre emploi, les juges d'appel ont en revanche entaché leur arrêt d'une erreur de droit en déduisant des dispositions de l'article 3 de la loi du 26 janvier 1984, en vertu desquelles les contrats passés par les collectivités territoriales en vue de recruter des agents non titulaires sont en principe conclus pour une durée déterminée, que la ville de Paris ne pouvait s'acquitter de son obligation qu'en proposant à Mme C... un contrat à durée déterminée ; que, par suite et sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, les articles 1er et 2 de l'arrêt attaqué doivent être annulés en tant qu'ils statuent sur l'arrêté du 3 octobre 2012 et sur le contrat conclu le 15 octobre 2012 ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la ville de Paris le versement d'une somme de 3 000 euros à Me Delamarre, avocat de MmeC..., au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve que Me Delamarre renonce à percevoir la somme correspondant à la part contributive de l'Etat ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que la somme demandée au même titre par la ville de Paris soit mise à la charge de MmeC..., qui n'est pas la partie perdante dans la présente instance ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 1er et 2 de l'arrêt de la cour administrative d'appel de Paris du 9 octobre 2014 sont annulés en tant qu'ils statuent sur l'arrêté du 3 octobre 2012 et sur le contrat conclu le 15 octobre 2012. <br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris, dans la mesure de la cassation ainsi prononcée.<br/>
Article 3 : La ville de Paris versera la somme de 3 000 euros à Me Delamarre, avocat de Mme C..., au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve que Me Delamarre renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : Les conclusions présentées par la ville de Paris au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à Mme A...B...épouse C...et à la ville de Paris.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-08 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. RECONNAISSANCE DE DROITS SOCIAUX FONDAMENTAUX. - OBLIGATION POUR L'EMPLOYEUR DE RECLASSER UN SALARIÉ ATTEINT DE MANIÈRE DÉFINITIVE D'UNE INAPTITUDE PHYSIQUE À EXERCER SON EMPLOI ET, EN CAS D'IMPOSSIBILITÉ, DE PRONONCER SON LICENCIEMENT [RJ1] - APPLICATION AU CAS D'UN AGENT PUBLIC EN CDI - OBLIGATION DE RECLASSER EN CDI [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-12-02 FONCTIONNAIRES ET AGENTS PUBLICS. AGENTS CONTRACTUELS ET TEMPORAIRES. EXÉCUTION DU CONTRAT. - OBLIGATION POUR L'EMPLOYEUR DE RECLASSER UN SALARIÉ ATTEINT DE MANIÈRE DÉFINITIVE D'UNE INAPTITUDE PHYSIQUE À EXERCER SON EMPLOI ET, EN CAS D'IMPOSSIBILITÉ, DE PRONONCER SON LICENCIEMENT [RJ1] - APPLICATION AU CAS D'UN AGENT PUBLIC EN CDI - OBLIGATION DE RECLASSER EN CDI [RJ2].
</SCT>
<ANA ID="9A"> 01-04-03-08 Il résulte d'un principe général du droit dont s'inspirent tant les dispositions du code du travail relatives à la situation des salariés qui, pour des raisons médicales, ne peuvent plus occuper leur emploi que les règles statutaires applicables dans ce cas aux fonctionnaires, que, lorsqu'il a été médicalement constaté qu'un agent non titulaire se trouve de manière définitive atteint d'une inaptitude physique à occuper son emploi, il appartient à l'employeur public de le reclasser dans un autre emploi et, en cas d'impossibilité, de prononcer, dans les conditions prévues pour l'intéressé, son licenciement. Ce principe est applicable en particulier aux agents contractuels de droit public.,,,Dans le cas où un tel agent, qui bénéficie des droits créés par son contrat de recrutement, est employé dans le cadre d'un contrat à durée indéterminée, cette caractéristique de son contrat doit être maintenue, sans que puissent y faire obstacle les dispositions applicables le cas échéant au recrutement des agents contractuels.</ANA>
<ANA ID="9B"> 36-12-02 Il résulte d'un principe général du droit dont s'inspirent tant les dispositions du code du travail relatives à la situation des salariés qui, pour des raisons médicales, ne peuvent plus occuper leur emploi que les règles statutaires applicables dans ce cas aux fonctionnaires, que, lorsqu'il a été médicalement constaté qu'un agent non titulaire se trouve de manière définitive atteint d'une inaptitude physique à occuper son emploi, il appartient à l'employeur public de le reclasser dans un autre emploi et, en cas d'impossibilité, de prononcer, dans les conditions prévues pour l'intéressé, son licenciement. Ce principe est applicable en particulier aux agents contractuels de droit public.,,,Dans le cas où un tel agent, qui bénéficie des droits créés par son contrat de recrutement, est employé dans le cadre d'un contrat à durée indéterminée, cette caractéristique de son contrat doit être maintenue, sans que puissent y faire obstacle les dispositions applicables le cas échéant au recrutement des agents contractuels.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., pour la consécration du principe général du droit, CE, 2 octobre 2002, Chambre de commerce et d'industrie de Meurthe-et-Moselle, n° 227868, p. 319.,,[RJ2] Rappr., en ce qui concerne la création de droits par le contrat au profit de l'agent, CE, Section, 31 décembre 2008,,, n° 283256, p. 481.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
