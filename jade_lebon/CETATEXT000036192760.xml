<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036192760</ID>
<ANCIEN_ID>JG_L_2017_12_000000390906</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/19/27/CETATEXT000036192760.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 08/12/2017, 390906</TITRE>
<DATE_DEC>2017-12-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>390906</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>Mme Karin Ciavaldini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:390906.20171208</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Me A...B..., agissant en qualité de liquidateur de la société Malapert et en son nom personnel, a demandé au tribunal administratif de Melun d'annuler la décision prise par la société d'économie mixte d'aménagement et de gestion du marché d'intérêt national de la région parisienne (Semmaris) le 30 juin 2009 de résilier le contrat de concession d'emplacements sur le domaine public conclu le 25 juin 1981 avec la société Malapert, de condamner la Semmaris à lui verser les sommes de 51 000 euros et 50 000 euros en réparation de préjudices financier et moral subis en sa qualité de liquidateur de cette société et de 30 000 euros en réparation du préjudice moral subi à titre personnel. Par un jugement n° 1100155 du 20 mars 2013, le tribunal a rejeté ces demandes.<br/>
<br/>
              Par un arrêt n° 13PA01951 du 9 avril 2015, la cour administrative d'appel de Paris a rejeté l'appel formé par Me B...contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 10 juin et 10 septembre 2015 et le 21 novembre 2017 au secrétariat du contentieux du Conseil d'Etat, Me B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la Semmaris la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de commerce ;<br/>
              - la décision du Tribunal des conflits n° 4078 du 24 avril 2017 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Karin Ciavaldini, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de Me B...et à la SCP Odent, Poulet, avocat de la société Semmaris.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond, que, par un traité de concession conclu le 25 juin 1981, la société d'économie mixte d'aménagement et de gestion du marché d'intérêt national de la région parisienne (Semmaris) a mis à la disposition de la société Malapert un emplacement dans un bâtiment du marché d'intérêt national de Rungis (Val-de-Marne) et que de nouveaux emplacements ont été mis à la disposition de l'entreprise par l'effet d'avenants ultérieurs. Par un jugement du 24 mars 2009, le tribunal de commerce de Versailles a ouvert à l'encontre de la société Malapert une procédure de liquidation judiciaire. Estimant que le délai imparti par les dispositions de l'article L. 641-11-1 du code de commerce à MeB..., mandataire liquidateur de la société Malapert, pour prendre parti sur la poursuite de l'exécution du contrat de concession était venu à expiration sans que celui-ci n'ait fait connaître son intention, la Semmaris a fait savoir à celui-ci, par une lettre  du 30 juin 2009, que le traité de concession et ses avenants successifs étaient résiliés de plein droit. MeB..., agissant en qualité de liquidateur de la société Malapert et en son nom personnel, a demandé au tribunal administratif de Melun d'annuler la décision prise par la Semmaris le 30 juin 2009 et de la condamner à lui verser les sommes de 51 000 euros et 50 000 euros en réparation des préjudices financier et moral qu'il estimait avoir subis en sa qualité de liquidateur de la société Malapert, et la somme de 30 000 euros en réparation du préjudice moral qu'il estimait avoir subi à titre personnel. Par un jugement du 20 mars 2013, le tribunal administratif a rejeté la demande de Me B..., qui se pourvoit en cassation contre l'arrêt du 9 avril 2015 par lequel la cour administrative d'appel de Paris a confirmé ce jugement. <br/>
<br/>
              2. Par une décision du 24 avril 2017, le Tribunal des conflits a jugé que le litige qui oppose Me B...à la Semmaris, qui a pour objet l'annulation de la résiliation d'un contrat administratif en vue de l'indemnisation de son titulaire, relevait de la juridiction administrative.<br/>
<br/>
              3. Aux termes du III de l'article L. 641-11-1 du code de commerce : " Le contrat en cours est résilié de plein droit : / 1° Après une mise en demeure de prendre parti sur la poursuite du contrat adressée par le cocontractant au liquidateur et restée plus d'un mois sans réponse. Avant l'expiration de ce délai, le juge-commissaire peut impartir au liquidateur un délai plus court ou lui accorder une prolongation, qui ne peut excéder deux mois, pour se prononcer (...) ". <br/>
<br/>
              4. En premier lieu, il résulte de ces dispositions que, lorsque le liquidateur a été mis en demeure de se prononcer sur la poursuite d'un contrat en cours, son refus exprès de poursuivre ce contrat, ou l'expiration du délai dont il disposait pour se prononcer, entraîne la résiliation de plein droit du contrat, sans qu'il y ait lieu de faire nécessairement constater cette résiliation par le juge-commissaire. Il en résulte que les moyens tirés de ce que la cour administrative d'appel aurait commis une erreur de droit et méconnu sa compétence faute d'avoir relevé d'office l'incompétence de la Semmaris pour constater la résiliation de plein droit du contrat en cause dans le litige ne peuvent qu'être écartés. <br/>
<br/>
              5. En deuxième lieu, afin de statuer sur la demande de Me B...tendant à l'annulation de la lettre du 30 juin 2009 l'informant du constat, par la Semmaris, de ce que le contrat avait été résilié de plein droit, en application du 1° du III de l'article L. 641-11-1 du code de commerce, c'est-à-dire à ce que soit ordonnée la reprise des relations contractuelles ou à ce qu'une indemnité lui soit versée en réparation du préjudice subi, il incombait seulement au juge administratif de se prononcer sur le point de savoir si la Semmaris avait estimé à bon droit que les conditions posées par le 1° du III de l'article L. 641-11-1 du code de commerce étaient remplies. Il en résulte que la cour administrative d'appel n'a pas commis d'erreur de droit en jugeant que les vices propres dont serait, le cas échéant, entachée la lettre du 30 juin 2009, étaient sans incidence sur la solution du litige. <br/>
<br/>
              6. En troisième lieu, Me B...soutient que la cour aurait dû faire application des dispositions de l'article L. 641-12 du code de commerce, qui fixe les règles spécifiques qui s'appliquent, dans le cas d'une entreprise en liquidation judiciaire, pour la résiliation du bail des immeubles utilisés pour l'activité de l'entreprise, et non de celles précitées de l'article L. 641-11-1 du même code. Toutefois, compte tenu des règles spécifiques régissant la domanialité publique, les titres en vertu desquels l'occupation du domaine public est autorisée n'ont pas la nature de baux soumis à un régime de droit privé mais de contrats administratifs. Il en va notamment ainsi des contrats conclus par la Semmaris, personne morale de droit privé délégataire de service public, avec des tiers en vue d'une occupation du domaine public sur lequel elle exerce sa mission de service public, ainsi que l'a jugé le Tribunal des conflits dans sa décision du 24 avril 2017. Il en résulte que la cour administrative d'appel n'a ni commis d'erreur de droit, ni inexactement qualifié les faits qui lui étaient soumis en jugeant que le traité de concession dont était titulaire la société Malapert ne pouvait être un bail au sens de l'article L. 641-12 du code de commerce.<br/>
<br/>
              7. En quatrième lieu, d'une part, la cour n'a ni commis d'erreur de droit, ni dénaturé les pièces du dossier qui lui était soumis en estimant que la lettre du 25 mars 2009 que la Semmaris avait adressée à Me B...constituait la mise en demeure prévue par le 1° du III de l'article L. 641-11-1 du code de commerce, sans qu'ait d'incidence à cet égard la circonstance que la Semmaris y ait mentionné par erreur l'article L. 622-13 du code de commerce, dès lors que les dispositions de cet article, applicables en cas de procédure de sauvegarde, étaient équivalentes à celles de l'article L. 641-11-1 du même code. D'autre part, si Me B...soutient qu'il avait clairement manifesté sa volonté d'opter, à la suite de cette mise en demeure, pour la poursuite du contrat, la cour n'a pas dénaturé les pièces du dossier qui lui était soumis en regardant la lettre du 1er avril 2009 adressée à la Semmaris par Me B...comme une simple demande de renseignements sur la possibilité de céder la concession dont bénéficiait la société en liquidation judiciaire, dès lors notamment qu'il ressort des pièces du dossier soumis aux juges du fond que, par un autre courrier du 1er avril 2009, Me B...avait accusé réception de la mise en demeure et informé la Semmaris qu'il allait solliciter du juge-commissaire la prolongation du délai de réponse, comme le permettent les dispositions du III de l'article L. 641-11-1 du code de commerce. La cour n'a pas non plus méconnu ces dispositions en ne regardant pas l'ordonnance du 2 juin 2009 par laquelle le juge-commissaire a autorisé Me B...à céder le contrat en litige, ordonnance dont, au demeurant, il n'est pas contesté que la Semmaris n'a pas eu connaissance, comme une réponse à la mise en demeure du 25 mars 2009.<br/>
<br/>
              8. Enfin, la cour a estimé que la lettre du 20 avril 2009, par laquelle la Semmaris se bornait, en réponse à la demande de renseignements formulée par Me B...le 1er avril, à lui indiquer qu'il pouvait rechercher un acquéreur pour le contrat et à lui rappeler la procédure à respecter pour que le comité des affectations du marché de Rungis et elle-même agréent cet éventuel acquéreur, constituait une simple mesure d'information, qui n'autorisait pas le liquidateur à se croire dispensé du respect de l'obligation de donner suite à la mise en demeure. La cour, qui a ainsi suffisamment répondu au moyen tiré de ce que la lettre du 20 avril 2009 aurait constitué une décision créatrice de droits que la lettre du 30 juin 2009 aurait illégalement retirée, n'a pas non plus dénaturé les pièces du dossier qui lui était soumis en ne regardant pas la lettre du 20 avril 2009 comme une décision d'acceptation de la poursuite du contrat révélant que la Semmaris aurait estimé avoir reçu une réponse en ce sens de Me B...à la suite de la mise en demeure.<br/>
<br/>
              9. Il résulte de tout ce qui précède que Me B...n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque.<br/>
<br/>
              10. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la Semmaris, qui n'est pas, dans la présente instance, la partie perdante. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de Me B...le versement à la Semmaris de la somme de 3 000 euros au même titre.<br/>
<br/>
<br/>
<br/>                 D E C I D E :<br/>
                              --------------<br/>
<br/>
Article 1er : Le pourvoi de Me B...est rejeté.<br/>
<br/>
Article 2 : Me B...versera à la Semmaris une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à Maître A...B...et à la société d'économie mixte d'aménagement et de gestion du marché d'intérêt national de la région parisienne.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-03-02-03-02 COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE LES DEUX ORDRES DE JURIDICTION. COMPÉTENCE DÉTERMINÉE PAR UN CRITÈRE JURISPRUDENTIEL. CONTRATS. CONTRATS ADMINISTRATIFS. - MISE EN LIQUIDATION JUDICIAIRE DE L'ENTREPRISE TITULAIRE D'UN CONTRAT ADMINISTRATIF - CONTESTATION PAR CETTE ENTREPRISE DE LA VALIDITÉ DE LA RÉSILIATION DU CONTRAT PRONONCÉE SUR LE FONDEMENT DE L'ARTICLE L. 641-11-1 DU CODE DE COMMERCE - OBLIGATION DE FAIRE CONSTATER CETTE RÉSILIATION PAR LE JUGE-COMMISSAIRE - ABSENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-04-02 MARCHÉS ET CONTRATS ADMINISTRATIFS. FIN DES CONTRATS. RÉSILIATION. - MISE EN LIQUIDATION JUDICIAIRE DE L'ENTREPRISE TITULAIRE D'UN CONTRAT ADMINISTRATIF - CONTESTATION PAR CETTE ENTREPRISE DE LA VALIDITÉ DE LA RÉSILIATION DU CONTRAT PRONONCÉE SUR LE FONDEMENT DE L'ARTICLE L. 641-11-1 DU CODE DE COMMERCE - 1) OBLIGATION DE FAIRE CONSTATER CETTE RÉSILIATION PAR LE JUGE-COMMISSAIRE - ABSENCE [RJ1] - 2) CONTESTATION PAR LE LIQUIDATEUR DE L'ENTREPRISE TITULAIRE DU CONTRAT DE LA LETTRE DE L'AUTRE PARTIE AU CONTRAT, EN L'ESPÈCE UNE SEM, L'INFORMANT DE LA RÉSILIATION DE PLEIN DROIT DU CONTRAT - MOYENS TIRÉS DES VICES PROPRES DONT SERAIENT ENTACHÉS CETTE LETTRE - MOYENS INOPÉRANTS.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">39-08-005 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. COMPÉTENCE. - MISE EN LIQUIDATION JUDICIAIRE DE L'ENTREPRISE TITULAIRE D'UN CONTRAT ADMINISTRATIF - CONTESTATION PAR CETTE ENTREPRISE DE LA VALIDITÉ DE LA RÉSILIATION DU CONTRAT PRONONCÉE SUR LE FONDEMENT DE L'ARTICLE L. 641-11-1 DU CODE DE COMMERCE - OBLIGATION DE FAIRE CONSTATER CETTE RÉSILIATION PAR LE JUGE-COMMISSAIRE - ABSENCE [RJ1].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">54-07-01-04-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. MOYENS INOPÉRANTS. - CONTESTATION PAR LE LIQUIDATEUR DE L'ENTREPRISE TITULAIRE D'UN CONTRAT ADMINISTRATIF DE LA LETTRE DE L'AUTRE PARTIE AU CONTRAT, EN L'ESPÈCE UNE SEM, L'INFORMANT DE LA RÉSILIATION DE PLEIN DROIT DU CONTRAT SUR LE FONDEMENT DE L'ARTICLE L. 641-11-1 DU CODE DE COMMERCE - MOYENS TIRÉS DES VICES PROPRES DONT SERAIENT ENTACHÉS CETTE LETTRE.
</SCT>
<ANA ID="9A"> 17-03-02-03-02 Il résulte du III de l'art. L. 641-11-1 du code de commerce que, lorsque le liquidateur a été mis en demeure de se prononcer sur la poursuite d'un contrat en cours, son refus exprès de poursuivre ce contrat, ou l'expiration du délai dont il disposait pour se prononcer, entraîne la résiliation de plein droit du contrat, sans qu'il y ait lieu de faire nécessairement constater cette résiliation par le juge-commissaire.</ANA>
<ANA ID="9B"> 39-04-02 1) Il résulte du III de l'art. L. 641-11-1 du code de commerce que, lorsque le liquidateur a été mis en demeure de se prononcer sur la poursuite d'un contrat en cours, son refus exprès de poursuivre ce contrat, ou l'expiration du délai dont il disposait pour se prononcer, entraîne la résiliation de plein droit du contrat, sans qu'il y ait lieu de faire nécessairement constater cette résiliation par le juge-commissaire.... ,,2) Afin de statuer sur la demande du liquidateur de l'entreprise titulaire du contrat tendant à l'annulation de la lettre l'informant du constat, par l'autre partie au contrat, en l'espèce une SEM, de ce que le contrat a été résilié de plein droit, en application du 1° du III de l'article L. 641-11-1 du code de commerce, c'est-à-dire à ce que soit ordonnée la reprise des relations contractuelles ou à ce qu'une indemnité lui soit versée en réparation du préjudice subi, il incombe seulement au juge administratif de se prononcer sur le point de savoir si cette autre partie a estimé à bon droit que les conditions posées par ces dispositions étaient remplies. Il en résulte que les vices propres dont serait, le cas échéant, entachée cette lettre sont sans incidence sur la solution du litige.</ANA>
<ANA ID="9C"> 39-08-005 Il résulte du III de l'art. L. 641-11-1 du code de commerce que, lorsque le liquidateur a été mis en demeure de se prononcer sur la poursuite d'un contrat en cours, son refus exprès de poursuivre ce contrat, ou l'expiration du délai dont il disposait pour se prononcer, entraîne la résiliation de plein droit du contrat, sans qu'il y ait lieu de faire nécessairement constater cette résiliation par le juge-commissaire.</ANA>
<ANA ID="9D"> 54-07-01-04-03 Afin de statuer sur la demande du liquidateur de l'entreprise titulaire du contrat tendant à l'annulation de la lettre l'informant du constat, par l'autre partie au contrat, en l'espèce une SEM, de ce que le contrat a été résilié de plein droit, en application du 1° du III de l'article L. 641-11-1 du code de commerce, c'est-à-dire à ce que soit ordonnée la reprise des relations contractuelles ou à ce qu'une indemnité lui soit versée en réparation du préjudice subi, il incombe seulement au juge administratif de se prononcer sur le point de savoir si cette autre partie a estimé à bon droit que les conditions posées par ces dispositions étaient remplies. Il en résulte que les vices propres dont serait, le cas échéant, entachée cette lettre sont sans incidence sur la solution du litige.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. TC, 24 avril 2017, Me,, agissant en qualité de liquidateur judiciaire de la société Malapert c/ Société d'économie mixte du marché de Rungis (SEMMARIS), n° 4078, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
