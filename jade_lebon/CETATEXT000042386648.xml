<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042386648</ID>
<ANCIEN_ID>JG_L_2020_09_000000438253</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/38/66/CETATEXT000042386648.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 30/09/2020, 438253</TITRE>
<DATE_DEC>2020-09-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>438253</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>M. Florian Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:438253.20200930</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Sixt asset and finance (Sixt AF) a demandé à la commission du contentieux du stationnement payant d'annuler le titre exécutoire émis le 11 octobre 2018 par l'Agence nationale de traitement automatisé de infractions (ANTAI) en vue du recouvrement d'un forfait de post-stationnement mis à sa charge par la commune de Lacanau et de la majoration dont il est assorti. Par une ordonnance n° 19019528 du 26 novembre 2019, la commission du contentieux du stationnement payant a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 4 février et 19 juin 2020, la société Sixt AF demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa requête ;<br/>
<br/>
              3°) de mettre à la charge de l'ANTAI la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Par un mémoire, enregistré le 19 juin 2020 au secrétariat du contentieux du Conseil d'Etat, la société Sixt AF demande au Conseil d'Etat, à l'appui de son pourvoi, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions des articles L. 2333-87 du code général des collectivités territoriales et L. 2321-3-1 et L. 2323-7-1 du code général de la propriété des personnes publiques.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, et notamment son Préambule ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ; <br/>
              - le code général des collectivités territoriales ;<br/>
              - le code général de la propriété des personnes publiques ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de la société Sixt asset and finance.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 18 septembre 2020, présentée par la société Sixt asset and finance ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Le I de l'article L. 2333-87 du code général des collectivités territoriales prévoit que le conseil municipal ou l'organe délibérant de l'établissement public de coopération intercommunale peut instituer une redevance de stationnement, en établissant deux barèmes. Le premier, dit " barème de paiement immédiat ", est applicable lorsque le conducteur du véhicule règle, dès le début de son stationnement, la redevance correspondant à la totalité de la période de stationnement. Le second, dit " forfait de post-stationnement ", est applicable lorsque la redevance n'est pas réglée dès le début du stationnement ou est insuffisamment réglée. Par ailleurs, le IV du même article dispose que, dans cette seconde hypothèse, le paiement du forfait de post-stationnement doit s'effectuer dans les trois mois suivant la notification de l'avis de paiement apposé sur le véhicule ou envoyé au domicile du titulaire du certificat d'immatriculation. A défaut, le forfait de post-stationnement est regardé comme impayé et fait l'objet d'une majoration dont le produit est affecté à l'Etat.<br/>
<br/>
              2. La société Sixt asset and finance (Sixt AF) demande l'annulation de l'ordonnance du 26 novembre 2019 par laquelle la commission du contentieux du stationnement payant a rejeté sa requête tendant à l'annulation du titre exécutoire émis le 11 octobre 2018 par l'agence nationale de traitement automatisé des infractions en vue du recouvrement d'un forfait de post-stationnement mis à sa charge par la commune de Lacanau et de la majoration dont il est assorti.<br/>
<br/>
              Sur la question prioritaire de constitutionnalité :<br/>
<br/>
              3. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé, y compris pour la première fois en cassation, à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              4. Les II, IV et VII de l'article L. 2333-87 du code général des collectivités territoriales disposent que : " II. - Le montant du forfait de post-stationnement dû, déduction faite, le cas échéant, du montant de la redevance de stationnement réglée dès le début du stationnement, est notifié par un avis de paiement délivré soit par son apposition sur le véhicule concerné par un agent assermenté de la commune, de l'établissement public de coopération intercommunale, du syndicat mixte ou du tiers contractant désigné pour exercer cette mission, soit par envoi postal au domicile du titulaire du certificat d'immatriculation du véhicule concerné effectué par un établissement public spécialisé de l'Etat (...) / Lorsque cet avis de paiement est notifié par voie postale, la notification est réputée avoir été reçue  par le titulaire du certificat d'immatriculation cinq jours francs à compter du jour de l'envoi. L'établissement public de l'Etat mentionné au premier alinéa du présent II justifie par tout moyen de l'envoi à l'adresse connue du titulaire du certificat d'immatriculation du véhicule. / (...) IV. (...) Le forfait de post-stationnement impayé et la majoration sont dus par l'ensemble des titulaires du certificat d'immatriculation du véhicule, solidairement responsables du paiement. (...) VII. - Lorsque les mentions du certificat d'immatriculation permettent l'identification d'un locataire, celui-ci est substitué au titulaire dudit certificat dans la mise en oeuvre des dispositions prévues aux II et IV du présent article. (...) ". <br/>
<br/>
              5. Par ailleurs, l'article L. 2321-3-1 du code général de la propriété des personnes publiques dispose que : " L'article L. 2321-3 s'applique au recouvrement du forfait de post-stationnement prévu à l'article L. 2333-87 du code général des collectivités territoriales, sous réserve des modalités prévues aux trois derniers alinéas du présent article./ Pour l'application du premier alinéa du même 1° de l'art L. 2321-3, la délivrance de l'avis de paiement du montant du forfait de post-stationnement vaut émission du titre de recettes à l'encontre du titulaire du certificat d'immatriculation du véhicule concerné ". Et l'article L. 2323-7-1 du même code dispose que : " (...) Lors de l'émission du titre exécutoire prévu à l'article L. 2333-87 mentionné ci-dessus, un avertissement est adressé au redevable titulaire du certificat d'immatriculation du véhicule, dont les mentions et modalités de délivrance sont précisées par arrêté du ministre de l'intérieur et du ministre chargé du budget. La notification de l'avertissement est réputée avoir été reçue cinq jours francs à compter du jour de l'envoi. L'envoi à l'adresse connue est justifié par tout moyen. ".<br/>
<br/>
              6. La société Sixt AF soutient que les dispositions citées aux points 4 et 5 ci-dessus méconnaissent plusieurs principes garantis par la Déclaration des droits de l'homme et du citoyen du 26 août 1789 en ce qu'elles prévoient que le débiteur du forfait de post-stationnement et de sa majoration éventuelle est le titulaire du certificat d'immatriculation du véhicule, c'est-à-dire son propriétaire lorsque ce dernier est une personne morale, sans permettre aux sociétés qui ont une activité de location de courte durée des véhicules dont elles sont propriétaires de s'exonérer du paiement de la somme réclamée en communiquant à l'administration les coordonnées du locataire du véhicule.<br/>
<br/>
              7. En premier lieu, ainsi qu'il est dit au point 1 ci-dessus, le forfait de post-stationnement constitue le montant de la redevance d'occupation du domaine public qui doit être acquitté lorsque celle-ci n'a pas été payée dès le début du stationnement. Par suite, il n'a pas le caractère d'une indemnité qui viserait à réparer un dommage causé par une faute de celui qui doit l'acquitter. La société Sixt AF ne peut, dès lors, utilement soutenir que les dispositions relatives au paiement du forfait de post-stationnement méconnaissent le principe constitutionnel selon lequel nul ne peut s'exonérer de sa responsabilité personnelle, garanti par l'article 4 de la Déclaration de 1789.<br/>
<br/>
              8. En deuxième lieu, la personne morale propriétaire du véhicule étant, en sa qualité de titulaire du certificat d'immatriculation, débitrice du forfait de post-stationnement, les dispositions contestées, qui n'ont, au demeurant, pas pour effet de lui interdire de répercuter contractuellement sur le locataire les sommes dont elle s'est acquittée, n'ont pas, en prévoyant qu'elle serait redevable de la majoration prévue en l'absence de paiement de ce forfait dans les délais légaux, méconnu ce même principe de responsabilité.<br/>
<br/>
              9. En troisième lieu, les dispositions contestées mentionnées aux points 4 et 5 ne placent pas les propriétaires de véhicules qui exercent une activité de location dans une situation différente de celle des autres propriétaires titulaires de certificat d'immatriculation. Elles ne sauraient, par suite, méconnaître le principe d'égalité devant la loi et, dès lors qu'elles font reposer la charge du stationnement du véhicule sur son propriétaire, ne créent, en tout état de cause, aucune rupture caractérisée de l'égalité devant les charges publiques. Par suite, la société Sixt AF n'est pas fondée à soutenir qu'elles méconnaissent les articles 6 et 13 de la Déclaration de 1789.<br/>
<br/>
              10. En quatrième lieu, au regard de la possibilité donnée aux propriétaires loueurs, titulaires du certificat d'immatriculation, de contester le forfait de post-stationnement et sa majoration en saisissant la commission du contentieux du stationnement payant, les dispositions contestées,  qui n'ont, au demeurant, pas pour effet d'interdire au loueur du véhicule de prévoir, le cas échéant, de recueillir tous éléments justificatifs auprès du locataire, ne portent pas atteinte au droit au recours effectif garanti par l'article 16 de la Déclaration de 1789.<br/>
<br/>
              11. Enfin, le forfait de post-stationnement ne visant pas, ainsi qu'il a déjà été dit, à réprimer un manquement du titulaire du certificat d'immatriculation à une obligation légale ou contractuelle, il ne saurait avoir le caractère d'une sanction. Par suite, la société Sixt AF ne peut utilement soutenir que les dispositions législatives critiquées méconnaissent les principes des droits de la défense et de personnalité des peines garantis par l'article 8 de la Déclaration de 1789. Par ailleurs, les conditions dans lesquelles une majoration de ce forfait peut, trois mois après la notification de l'avis de paiement, être mis à la charge de cette même personne en cas d'absence de paiement, ne méconnaissent pas davantage, en tout état de cause, les droits de la défense et le principe de personnalité des peines garantis par cet article.<br/>
<br/>
              12. Il résulte de tout ce qui précède que la question de la conformité à la Constitution des dispositions des articles L. 2333-87 du code général des collectivités territoriales et L. 2323-1 et L. 2323-7-1 du code général de la propriété des personnes publiques, qui n'est pas nouvelle, ne présente pas un caractère sérieux. Par suite, il n'y a pas lieu de la renvoyer au Conseil constitutionnel.<br/>
<br/>
              Sur l'admission du pourvoi :<br/>
<br/>
              13. Pour demander l'annulation de l'ordonnance qu'elle attaque, la société Sixt asset and finance ne soulève pas d'autre moyen que le moyen tiré de l'absence de conformité à la Constitution des dispositions des articles L. 2333-87 du code général des collectivités territoriales et L. 2323-1 et L. 2323-7-1 du code général de la propriété des personnes publiques. Il résulte de ce qui précède que ce moyen n'est pas de nature à permettre l'admission du pourvoi.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La question de la conformité à la Constitution des articles L. 2333-87 du code général des collectivités territoriales et L. 2323-1 et L. 2323-7-1 du code général de la propriété des personnes publiques n'est pas renvoyée au Conseil constitutionnel.<br/>
<br/>
		Article 2 : Le pourvoi de la société Sixt asset and finance n'est pas admis.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la société Sixt asset and finance, à la commune de Lacanau et au ministre de l'économie, des finances et de la relance.<br/>
		Copie en sera adressée au Conseil constitutionnel et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">49-04-01-02-03 POLICE. POLICE GÉNÉRALE. CIRCULATION ET STATIONNEMENT. RÉGLEMENTATION DU STATIONNEMENT. STATIONNEMENT PAYANT. - FORFAIT DE POST-STATIONNEMENT (ART. L. 2333-87 DU CGCT) - DÉCISION PRÉSENTANT LE CARACTÈRE D'UNE SANCTION - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">59-02 RÉPRESSION. DOMAINE DE LA RÉPRESSION ADMINISTRATIVE - FORFAIT DE POST-STATIONNEMENT (ART. L. 2333-87 DU CGCT) - DÉCISION PRÉSENTANT LE CARACTÈRE D'UNE SANCTION - ABSENCE.
</SCT>
<ANA ID="9A"> 49-04-01-02-03 Le forfait de post-stationnement prévu par l'article L. 2333-87 du code général des collectivités territoriales (CGCT) constitue le montant de la redevance d'occupation du domaine public qui doit être acquitté lorsque celle-ci n'a pas été payée dès le début du stationnement. Ne visant pas à réprimer un manquement du titulaire du certificat d'immatriculation à une obligation légale ou contractuelle, il ne saurait avoir le caractère d'une sanction ni d'une indemnité qui viserait à réparer un dommage causé par une faute de celui qui doit l'acquitter.,,,Par suite, ne peut être utilement soutenu que les dispositions relatives au paiement du forfait de post-stationnement méconnaissent les principes des droits de la défense et de personnalité des peines et le principe selon lequel nul ne peut s'exonérer de sa responsabilité personnelle, garantis par les articles 8 et 4 de la Déclaration de 1789.</ANA>
<ANA ID="9B"> 59-02 Le forfait de post-stationnement prévu par l'article L. 2333-87 du code général des collectivités territoriales (CGCT) constitue le montant de la redevance d'occupation du domaine public qui doit être acquitté lorsque celle-ci n'a pas été payée dès le début du stationnement. Ne visant pas à réprimer un manquement du titulaire du certificat d'immatriculation à une obligation légale ou contractuelle, il ne saurait avoir le caractère d'une sanction ni d'une indemnité qui viserait à réparer un dommage causé par une faute de celui qui doit l'acquitter.,,,Par suite, ne peut être utilement soutenu que les dispositions relatives au paiement du forfait de post-stationnement méconnaissent les principes des droits de la défense et de personnalité des peines et le principe selon lequel nul ne peut s'exonérer de sa responsabilité personnelle, garantis par les articles 8 et 4 de la Déclaration de 1789.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
