<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026230120</ID>
<ANCIEN_ID>JG_L_2012_07_000000349173</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/23/01/CETATEXT000026230120.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 27/07/2012, 349173</TITRE>
<DATE_DEC>2012-07-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>349173</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Philippe Martin</PRESIDENT>
<AVOCATS/>
<RAPPORTEUR>M. Gaël Raimbault</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:349173.20120727</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu, 1° sous le n° 349173, la requête, enregistrée le 10 mai 2011 au secrétariat du contentieux du Conseil d'Etat, présentée par l'association des malades des syndromes de Lyell et de Stevens-Johnson (Amalyste), dont le siège est 4, rue Yves-Toudic à Paris (75010), représentée par sa présidente ; l'association des malades des syndromes de Lyell et de Stevens-Johnson demande au Conseil d'Etat d'annuler le décret n° 2011-258 du 10 mars 2011 portant modification des conditions de prise en charge des frais de transport pour les malades reconnus atteints d'une affection de longue durée ;<br/>
<br/>
<br/>
              Vu, 2° sous le n° 349184, la requête, enregistrée le 10 mai 2011 au secrétariat du contentieux du Conseil d'Etat, présentée par l'association FNATH, association des accidentés de la vie, dont le siège est 42, rue des alliés à Saint-Etienne (42000), représentée par son président, par l'association CISS, collectif inter-associatif sur la santé, dont le siège est 10, villa Bosquet à Paris (75007), représentée par son président et par M. Henri A, demeurant 91, rue André-Ternynck à Chauny (02300) ; ils demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le même décret du 10 mars 2011 attaqué sous le n° 349173 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu, 3° sous le n° 353263, la requête, enregistrée le 11 mai 2011 au secrétariat du contentieux du Conseil d'Etat, présentée par la fédération nationale d'aide aux insuffisants rénaux, dont le siège est 21-23, rue Ernest-Renan à Lyon (69007), représentée par son président ; la fédération nationale d'aide aux insuffisants rénaux demande au Conseil d'Etat d'annuler le même décret du 10 mars 2011 attaqué sous le n° 349173 ;<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
	Vu les autres pièces des dossiers ;<br/>
              Vu la Constitution, notamment son Préambule ;<br/>
              Vu le code de la sécurité sociale ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Gaël Raimbault, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que les requêtes de l'association des malades des syndromes de Lyell et de Stevens-Johnson (Amalyste), de l'association FNATH, association des accidentés de la vie, de l'association CISS, collectif inter associatif sur la santé, de M. Barbier et de la fédération nationale d'aide aux insuffisants rénaux sont dirigées contre le même décret ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 321-1 du code de la sécurité sociale : " L'assurance maladie comporte : (...) / 2°) La couverture des frais de transport de l'assuré ou des ayants droit se trouvant dans l'obligation de se déplacer pour recevoir les soins ou subir les examens appropriés à leur état (...) dans les conditions et limites tenant compte de l'état du malade et du coût du transport fixées par décret en Conseil d'Etat (...)" ; que sur le fondement de ces dispositions, le décret attaqué du 10 mars 2011 a modifié les dispositions de l'article R. 322-10 du code de la sécurité sociale qui fixe la liste des situations donnant lieu à une prise en charge des frais de transport, en prévoyant que, pour les patients bénéficiant du régime des affections de longue durée prévu par l'article L. 322-3 du même code, les frais de transport ne seront désormais pris en charge qu'à la condition que leur bénéficiaire présente une déficience ou une incapacité définie par un référentiel fixé par arrêté ; que ce référentiel, pris sur le fondement de l'article R. 322-10-1 du même code, précise les cas dans lesquels les déficiences et les incapacités du malade justifient la prescription d'une ambulance ou d'un transport assis professionnalisé ;<br/>
<br/>
              Sur la légalité externe du décret attaqué :<br/>
<br/>
              3. Considérant, en premier lieu, qu'en vertu de l'article 34 de la Constitution du 4 octobre 1958, relèvent du domaine de la loi les principes fondamentaux de la sécurité sociale  ; que si figure parmi ces principes fondamentaux la nature des conditions exigées pour l'attribution des prestations, il appartient au pouvoir réglementaire, sous réserve de ne pas dénaturer ces conditions, d'en préciser les éléments et les modalités ; que le décret attaqué se borne à prévoir que les frais de transport des patients bénéficiant du régime des affections de longue durée seront pris en charge à la condition que le recours à un transport adapté soit rendu nécessaire par les déficiences ou incapacités qu'ils présentent ; qu'il fixe ainsi, conformément aux dispositions de l'article L. 321-1 du code de la sécurité sociale cité ci-dessus, des conditions et des limites pour le droit à remboursement des frais de transport par l'assurance maladie, sans dénaturer la disposition législative qui lui sert de fondement ; que le moyen tiré de ce que son auteur aurait, ce faisant, excédé sa compétence, doit par suite être écarté ;<br/>
<br/>
              4. Considérant, en second lieu, qu'aucune disposition législative ou réglementaire ne faisait obligation au pouvoir réglementaire de consulter la Haute Autorité de santé préalablement à l'édiction du décret attaqué ; que le moyen tiré de ce que l'absence de consultation de cette autorité entacherait ce décret d'irrégularité ne peut donc qu'être écarté ;<br/>
<br/>
              Sur la légalité du décret attaqué au regard du principe d'égalité :<br/>
<br/>
              5. Considérant que le principe d'égalité ne s'oppose pas à ce que l'autorité investie du pouvoir réglementaire règle de façon différente des situations différentes ni à ce qu'elle déroge à l'égalité pour des raisons d'intérêt général, pourvu que dans l'un comme dans l'autre cas la différence de traitement qui en résulte soit en rapport direct avec l'objet de la norme qui l'établit et ne soit pas manifestement disproportionnée au regard des motifs susceptibles de la justifier ; qu'il résulte des termes mêmes de l'article L. 321-1 du code de la sécurité sociale cité ci-dessus que l'objet du remboursement des frais de transport par l'assurance maladie est de garantir une prise en charge de ces frais dans des conditions et limites relatives à leur coût et aux besoins particuliers de transport liés à l'état de la personne malade ;<br/>
<br/>
              6. Considérant que les déficiences ou incapacités dont souffrent les assurés sociaux sont, même lorsqu'elles n'ont pas de lien avec les soins ou les examens qui leur sont nécessaires, au nombre des caractéristiques de leur état qui sont liées à leurs besoins de transport ; qu'ainsi, en réservant, pour ceux des assurés sociaux qui bénéficient du régime des affections de longue durée, le remboursement systématique des frais de transport à ceux seulement de ces assurés qui présentent les déficiences ou incapacités justifiant, en application de l'arrêté pris sur le fondement de l'article R. 322-10-1 du code de la sécurité sociale, le recours à des modes de transport particuliers, le décret attaqué a institué une différence de traitement qui est en rapport direct avec l'objet de l'article L. 321-1 du même code ;<br/>
<br/>
              7. Considérant, par ailleurs, qu'eu égard notamment à l'ensemble des autres cas de prise en charge dont continuent de bénéficier les patients qui relevaient des dispositions abrogées par le décret attaqué, et notamment celles relatives à la prise en charge des frais liés aux hospitalisations, aux trajets de plus de 150 kilomètres ou aux transports en série, la différence de traitement ainsi créée, qui ne crée pas d'effet de seuil excessif, n'apparaît pas manifestement disproportionnée au regard de la différence de situation qui la justifie ;<br/>
<br/>
              8. Considérant, enfin, que le principe d'égalité n'impose pas au pouvoir réglementaire de traiter différemment des personnes se trouvant dans une situation différente ; que l'association Amalyste ne peut donc utilement soutenir que le décret qu'elle attaque méconnaîtrait ce principe faute d'avoir prévu une dérogation au profit des personnes atteintes d'une maladie rare ;<br/>
<br/>
              9. Considérant qu'il résulte de ce qui précède que le moyen tiré de ce que le décret attaqué méconnaîtrait le principe d'égalité ne peut qu'être écarté ;<br/>
<br/>
              Sur les autres moyens de légalité interne :<br/>
<br/>
              10. Considérant que le respect des exigences découlant du onzième alinéa du Préambule de la Constitution de 1946, en vertu duquel la Nation garantit, notamment à l'enfant, à la mère et aux vieux travailleurs, la protection de la santé, doit être apprécié, d'une part, compte tenu de l'ensemble des dispositions en vertu desquelles des sommes sont susceptibles d'être laissées à la charge des assurés sociaux à raison des dépenses de santé qu'ils exposent et, d'autre part, au regard des incidences de telles mesures sur la situation des personnes les plus vulnérables ou défavorisées ; <br/>
<br/>
              11. Considérant que le décret attaqué a pour effet de laisser à la charge des malades bénéficiant du régime des affections de longue durée, mais dont l'état ne justifie pas la prescription d'une ambulance ou d'un transport assis professionnalisé, les frais de transport qu'ils exposent pour recevoir les soins ou subir les examens appropriés à leur état dans les seuls cas où ces frais ne sont liés ni à une hospitalisation ni à un trajet de plus de 150 kilomètres ni à un transport en série au sens du e) du 1° de l'article R. 322-10 du code de la sécurité sociale ; que compte tenu de l'impact limité de la modification ainsi introduite, il ne ressort pas du dossier que, pour importantes qu'elles puissent être dans certaines situations particulières mentionnées par les requérants, l'ensemble des sommes susceptibles d'être laissées à la charge de cette catégorie de malades par la réglementation en vigueur, auxquelles s'ajoute, le cas échéant, le coût de la souscription d'un contrat d'assurance complémentaire de santé compte tenu de l'aide prévue à l'article L. 863-1 du même code, excèderaient, pour un nombre conséquent d'entre eux, la part de leurs revenus au-delà de laquelle les exigences du onzième alinéa du Préambule seraient méconnues ;<br/>
<br/>
              12. Considérant qu'il ne ressort pas des pièces du dossier que l'entrée en vigueur des règles nouvelles prévues par le décret attaqué trois semaines après sa publication aurait entraîné une atteinte excessive aux intérêts des malades concernés ou à des situations contractuelles légalement formées ; que, par suite, le décret attaqué n'a pas méconnu le principe de sécurité juridique ; que les requérants ne sont, par ailleurs, pas fondés à soutenir que ce décret méconnaîtrait l'objectif constitutionnel d'accessibilité et d'intelligibilité de la norme ;<br/>
<br/>
              13. Considérant que la circonstance, à la supposer établie, que le décret attaqué n'aurait pas les effets d'économies pour l'assurance maladie qui en sont attendus par le Gouvernement est sans incidence sur sa légalité ;<br/>
<br/>
              14. Considérant, enfin, que le bénéfice des nouvelles modalités de prise en charge des frais de transport prévues par le décret attaqué n'exige qu'une prescription établie par un médecin en fonction de l'état du malade ; que les requérants ne sont, dès lors, pas fondés à soutenir que ce décret conduirait à laisser sans prise en charge certaines situations qu'il entend couvrir et serait, pour ce motif, entaché d'erreur manifeste d'appréciation ; <br/>
<br/>
              15. Considérant qu'il résulte de tout ce qui précède que les requérants ne sont pas fondés à demander l'annulation du décret attaqué ;<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              16. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas la partie perdante, la somme demandée par l'association FNATH, association des accidentés de la vie et les autres requérants présents sous le n° 349184 ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les requêtes de l'association des malades des syndromes de Lyell et de Stevens-Johnson, de l'association FNATH, association des accidentés de la vie, de l'association CISS, collectif inter associatif sur la santé, de M. A et de la fédération nationale d'aide aux insuffisants rénaux sont rejetées.<br/>
Article 2 : La présente décision sera notifiée à l'association des malades des syndromes de Lyell et de Stevens-Johnson, à l'association FNATH, association des accidentés de la vie, à l'association CISS, collectif inter associatif sur la santé, à M. Henri A, à la fédération nationale d'aide aux insuffisants rénaux et à la ministre des affaires sociales et de la santé.<br/>
Copie en sera adressée pour information au Premier ministre et à la Haute Autorité de santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. ÉGALITÉ DEVANT LA LOI. - REMBOURSEMENT DES FRAIS DE TRANSPORT PAR L'ASSURANCE MALADIE (ART. L. 321-1 DU CSS) - DISPOSITION RÉSERVANT LE REMBOURSEMENT SYSTÉMATIQUE DES FRAIS AUX ASSURÉS HANDICAPÉS - DIFFÉRENCE DE TRAITEMENT EN RAPPORT AVEC L'OBJET DE LA LOI.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">62-04-01 SÉCURITÉ SOCIALE. PRESTATIONS. PRESTATIONS D'ASSURANCE MALADIE. - REMBOURSEMENT DES FRAIS DE TRANSPORT (ART. L. 321-1 DU CSS) - DISPOSITION RÉSERVANT LE REMBOURSEMENT SYSTÉMATIQUE DES FRAIS AUX ASSURÉS HANDICAPÉS - MÉCONNAISSANCE DU PRINCIPE D'ÉGALITÉ - ABSENCE.
</SCT>
<ANA ID="9A"> 01-04-03-01 Il résulte des termes mêmes de l'article L. 321-1 du code de la sécurité sociale (CSS) que l'objet du remboursement des frais de transports médicaux par l'assurance maladie qu'il prévoit est de garantir une prise en charge de ces frais dans des conditions et limites relatives à leur coût et aux besoins particuliers de transport liés à l'état de la personne malade. Les déficiences ou incapacités dont souffrent les assurés sociaux sont, même lorsqu'elles n'ont pas de lien avec les soins ou examens qui leur sont nécessaires, au nombre des caractéristiques de leur état qui sont liées à leurs besoins de transport. Ainsi, le fait de réserver, pour ceux des assurés sociaux qui bénéficient du régime des affections de longue durée, le remboursement systématique des frais de transport à ceux seulement de ces assurés qui présentent des déficiences ou incapacités justifiant le recours à des modes de transport particuliers, institue entre assurés sociaux une différence de traitement en rapport direct avec l'objet de l'article L. 321-1.</ANA>
<ANA ID="9B"> 62-04-01 Il résulte des termes mêmes de l'article L. 321-1 du code de la sécurité sociale (CSS) que l'objet du remboursement des frais de transports médicaux par l'assurance maladie qu'il prévoit est de garantir une prise en charge de ces frais dans des conditions et limites relatives à leur coût et aux besoins particuliers de transport liés à l'état de la personne malade. Les déficiences ou incapacités dont souffrent les assurés sociaux sont, même lorsqu'elles n'ont pas de lien avec les soins ou examens qui leur sont nécessaires, au nombre des caractéristiques de leur état qui sont liées à leurs besoins de transport. Ainsi, le fait de réserver, pour ceux des assurés sociaux qui bénéficient du régime des affections de longue durée, le remboursement systématique des frais de transport à ceux seulement de ces assurés qui présentent des déficiences ou incapacités justifiant le recours à des modes de transport particuliers, institue entre assurés sociaux une différence de traitement en rapport direct avec l'objet de l'article L. 321-1, qui n'est pas manifestement disproportionnée.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
