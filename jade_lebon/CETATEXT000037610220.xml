<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037610220</ID>
<ANCIEN_ID>JG_L_2018_11_000000405628</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/61/02/CETATEXT000037610220.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 14/11/2018, 405628</TITRE>
<DATE_DEC>2018-11-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>405628</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Géraud Sajust de Bergues</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:405628.20181114</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le préfet du Puy-de-Dôme a demandé au tribunal administratif de Clermont-Ferrand, en application des dispositions de l'article L. 2131-6 du code général des collectivités territoriales, d'annuler la décision du 29 mai 2013 par laquelle le syndicat mixte pour l'aménagement et le développement des Combrailles (SMADC) a accepté la transformation de la société d'économie mixte pour l'exploitation des réseaux d'eau potable et d'assainissement en société publique locale dénommée " société d'exploitation mutualisée pour l'eau, l'environnement, les réseaux, l'assainissement dans l'intérêt du public " (SEMERAP) et a approuvé le projet de statuts de la société.<br/>
<br/>
               Par un jugement n° 1301728 du 1er juillet 2014, le tribunal administratif de Clermont-Ferrand a annulé cette délibération. <br/>
<br/>
              Par un arrêt n° 14LY02753 du 4 octobre 2016, la cour administrative d'appel de Lyon a rejeté l'appel formé contre ce jugement par le SMADC.<br/>
<br/>
              1. Sous le n° 405628, par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 5 décembre 2016, 6 mars 2017 et 9 janvier 2018 au secrétariat du contentieux du Conseil d'Etat, le SMADC demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              2. Sous le n° 405690, par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 5 décembre 2016, 6 mars 2017 et 9 janvier 2018 au secrétariat du contentieux du Conseil d'Etat, la " société d'exploitation mutualisée pour l'eau, l'environnement, les réseaux, l'assainissement dans l'intérêt du public " (SEMERAP) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Géraud Sajust de Bergues, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat du Syndicat mixte pour l'aménagement et le développement des Combrailles et à la SCP Lyon-Caen, Thiriez, avocat de la société d'exploitation mutualisée pour l'eau, l'environnement, les réseaux, l'assainissement dans l'intérêt public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par une délibération du 29 mai 2013, le comité du syndicat mixte pour l'aménagement et le développement des Combrailles (SMADC), composé de communes et communautés de communes, a donné son accord à la transformation de la société d'économie mixte pour l'exploitation des réseaux d'eau potable et d'assainissement en société publique locale dénommée société d'exploitation mutualisée pour l'eau, l'environnement, les réseaux, l'assainissement dans l'intérêt du public (SEMERAP) et a approuvé le projet de statuts de cette société. Par un jugement du 1er juillet 2014, le tribunal administratif de Clermont-Ferrand a, sur déféré du préfet du Puy-de-Dôme, annulé cette délibération. Le SMADC et la SEMERAP se pourvoient en cassation contre l'arrêt du 4 octobre 2016 par lequel la cour administrative d'appel de Lyon a rejeté l'appel du SMADC contre ce jugement.<br/>
<br/>
              2. Ces deux pourvois présentent à juger les mêmes questions. Il y a lieu de les joindre pour y statuer par une même décision. <br/>
<br/>
              3. Aux termes de l'article L. 1531-1 du code général des collectivités territoriales, dans sa version applicable à la date de la délibération litigieuse : " Les collectivités territoriales et leurs groupements peuvent créer, dans le cadre des compétences qui leur sont attribuées par la loi, des sociétés publiques locales dont ils détiennent la totalité du capital. / Ces sociétés sont compétentes pour réaliser des opérations d'aménagement au sens de l'article L. 300-1 du code de l'urbanisme, des opérations de construction ou pour exploiter des services publics à caractère industriel ou commercial ou toutes autres activités d'intérêt général. / Ces sociétés exercent leurs activités exclusivement pour le compte de leurs actionnaires et sur le territoire des collectivités territoriales et des groupements de collectivités territoriales qui en sont membres. / Ces sociétés revêtent la forme de société anonyme régie par le livre II du code de commerce et sont composées, par dérogation à l'article L. 225-1 du même code, d'au moins deux actionnaires. / Sous réserve des dispositions du présent article, elles sont soumises au titre II du présent livre ". Aux termes du deuxième alinéa de l'article L. 1521-1 du même code, applicable aux sociétés publiques locales : " La commune actionnaire d'une société d'économie mixte locale dont l'objet social s'inscrit dans le cadre d'une compétence qu'elle a intégralement transférée à un établissement public de coopération intercommunale peut continuer à participer au capital de cette société à condition qu'elle cède à l'établissement public de coopération intercommunale plus des deux tiers des actions qu'elle détenait antérieurement au transfert de compétences ". Aux termes de l'article L. 1524-5 du même code, également applicable aux sociétés publiques locales : " Toute collectivité territoriale ou groupement de collectivités territoriales actionnaire a droit au moins à un représentant au conseil d'administration ou au conseil de surveillance (...). / (...) Les sièges sont attribués en proportion du capital détenu respectivement par chaque collectivité ou groupement. (...) ". <br/>
<br/>
              4. Il résulte de la combinaison de ces dispositions que, hormis le cas, prévu par l'article L. 1521-1 du code général des collectivités territoriales, où l'objet social de la société s'inscrit dans le cadre d'une compétence que la commune n'exerce plus du fait de son transfert, après la création de la société, à un établissement public de coopération intercommunale, la participation d'une collectivité territoriale ou d'un groupement de collectivités territoriales à une société publique locale, qui lui confère un siège au conseil d'administration ou au conseil de surveillance et a nécessairement pour effet de lui ouvrir droit à participer au vote des décisions prises par ces organes, est exclue lorsque cette collectivité territoriale ou ce groupement de collectivités territoriales n'exerce pas l'ensemble des compétences sur lesquelles porte l'objet social de la société. <br/>
<br/>
              5. Par suite, en jugeant que les dispositions de l'article L. 1531-1 du code général des collectivités territoriales permettent à une collectivité territoriale ou un groupement de collectivités territoriales d'être membre d'une société publique locale dont la partie prépondérante des missions n'outrepasse pas son domaine de compétence, la cour a commis une erreur de droit. Les requérants sont, par suite, fondés à demander l'annulation des articles 2, 3 et 4 de l'arrêt qu'ils attaquent, sans qu'il soit besoin d'examiner l'autre moyen des pourvois.  <br/>
<br/>
              6. Dans les circonstances de l'espèce, il y a lieu de mettre à la charge de l'Etat la somme de 3 000 euros à verser, respectivement, au SAMDC et à la SEMERAP au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 2, 3 et 4 de l'arrêt de la cour administrative d'appel de Lyon du 4 octobre 2016 sont annulés. <br/>
Article 2 : Les affaires sont renvoyées à la cour administrative d'appel de Lyon.<br/>
Article 3 : L'Etat versera, respectivement, au SAMDC et à la SEMERAP une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée au syndicat mixte pour l'aménagement et le développement des Combrailles, à la société d'exploitation mutualisée pour l'eau, l'environnement, les réseaux, l'assainissement dans l'intérêt du public et au ministre de l'intérieur. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-01-06 COLLECTIVITÉS TERRITORIALES. DISPOSITIONS GÉNÉRALES. DISPOSITIONS ÉCONOMIQUES. - SPL - PARTICIPATION D'UNE COLLECTIVITÉ TERRITORIALE OU D'UN GROUPEMENT DE COLLECTIVITÉS TERRITORIALES À UNE SPL - CONDITION - EXERCICE PAR CETTE COLLECTIVITÉ OU PAR CE GROUPEMENT DE L'ENSEMBLE DES COMPÉTENCES SUR LESQUELLES PORTE L'OBJET SOCIAL DE LA SOCIÉTÉ - EXCEPTION - CAS OÙ L'OBJET SOCIAL DE LA SPL S'INSCRIT DANS LE CADRE D'UNE COMPÉTENCE INTÉGRALEMENT TRANSFÉRÉE PAR LA COMMUNE ACTIONNAIRE À UN EPCI (2E AL. DE L'ART. L. 1521-1 DU CGCT).
</SCT>
<ANA ID="9A"> 135-01-06 Il résulte de la combinaison des articles L. 1531-1, L. 1521-1 et L. 1524-5 du code général des collectivités territoriales (CGCT) que, hormis le cas, prévu par l'article L. 1521-1 du CGCT, où l'objet social de la société s'inscrit dans le cadre d'une compétence que la commune n'exerce plus du fait de son transfert, après la création de la société, à un établissement public de coopération intercommunale, la participation d'une collectivité territoriale ou d'un groupement de collectivités territoriales à une société publique locale (SPL), qui lui confère un siège au conseil d'administration ou au conseil de surveillance et a nécessairement pour effet de lui ouvrir droit à participer au vote des décisions prises par ces organes, est exclue lorsque cette collectivité territoriale ou ce groupement de collectivités territoriales n'exerce pas l'ensemble des compétences sur lesquelles porte l'objet social de la société.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
