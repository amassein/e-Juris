<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037059321</ID>
<ANCIEN_ID>JG_L_2018_06_000000408325</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/05/93/CETATEXT000037059321.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 6ème chambres réunies, 13/06/2018, 408325</TITRE>
<DATE_DEC>2018-06-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>408325</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BRIARD</AVOCATS>
<RAPPORTEUR>M. Florian Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:408325.20180613</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              1° Sous le numéro 408325, par une requête sommaire et un mémoire complémentaire, enregistrés au secrétariat du contentieux du Conseil d'Etat les 23 février et 23 mai 2017, le conseil national de l'ordre des infirmiers demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'ordonnance n° 2017-50 du 19 janvier 2017 relative à la reconnaissance des qualifications professionnelles dans le domaine de la santé ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              2° Sous le numéro 409019, par une requête et un nouveau mémoire, enregistrés au secrétariat du contentieux du Conseil d'Etat les 17 et 28 mars 2017, la Fédération nationale des orthophonistes, la Fédération nationale des infirmiers, le syndicat Convergence Infirmière, le Syndicat national des audioprothésistes UNSAF, le Syndicat national autonome des orthoptistes, la Fédération nationale des podologues, Mme D...A..., M. F... C..., Mme D...H..., Mme I... G...et M. B...E...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'ordonnance n° 2017-50 du 19 janvier 2017 relative à la reconnaissance des qualifications professionnelles dans le domaine de la santé ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              3° Sous le numéro 409045, par une requête enregistrée le 20 mars 2017 au secrétariat du contentieux du Conseil d'Etat, le conseil national de l'ordre des pédicures-podologues (CNOPP) demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'ordonnance n° 2017-50 du 19 janvier 2017 relative à la reconnaissance des qualifications professionnelles dans le domaine de la santé ;<br/>
<br/>
              2°) à titre subsidiaire de surseoir à statuer et de saisir la Cour de justice de l'Union européenne des questions préjudicielles suivantes : " Les dispositions de l'article 4 septies de la directive 2005/36/CE du Parlement européen et du Conseil du 7 septembre 2005 relative à la reconnaissance des qualifications professionnelles issues de la directive 2013/55/UE du Parlement européen et du Conseil du 20 novembre 2013 et relatives à l'accès partiel doivent-elles s'entendre comme autorisant un Etat membre à exclure dans leur ensemble certaines professions du régime de l'accès partiel ' En cas de réponse affirmative, selon quels critères une profession peut-elle être exclue dans son ensemble du régime de l'accès partiel '"<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              4° Sous le numéro 409058, par une requête sommaire et un mémoire complémentaire, enregistrés au secrétariat du contentieux du Conseil d'Etat les 20 mars et 20 juin 2017, l'Ordre national des chirurgiens-dentistes demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'ordonnance n° 2017-50 du 19 janvier 2017 relative à la reconnaissance des qualifications professionnelles dans le domaine de la santé ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
<br/>
              - la Constitution, notamment son article 61-1 ; <br/>
<br/>
              - la directive 2013/55/UE du Parlement et du Conseil du 20 novembre 2013 ;<br/>
<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ; <br/>
<br/>
              - la loi n° 2018-132 du 26 février 2018 ;<br/>
<br/>
              - le code de la santé publique ;<br/>
<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Briard, avocat du Conseil national de l'ordre des infirmiers, à la SCP Matuchansky, Poupot, Valdelièvre, avocat du Conseil national de l'ordre des pédicures-podologues  et à la SCP Lyon-Caen, Thiriez, avocat de l'Ordre national des chirurgiens-dentistes.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 31 mai 2018, présentée par la Fédération nationale des orthophonistes et autres ; <br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que l'article 1er de l'ordonnance n° 2017-50 du 19 janvier 2017 relative à la reconnaissance des qualifications professionnelles dans le domaine de la santé a introduit dans le code de la santé publique les articles L. 4002-3, L. 4002-4, L. 4002-5 et L. 4002-6 relatifs à l'accès partiel aux professions de santé ; que ces articles ont pour objet d'assurer la transposition de l'article 4 septies de la directive 2005/36/CE du 7 septembre 2005 du Parlement et du Conseil relative à la reconnaissance des qualifications professionnelles, issu de la directive 2013/55/UE du Parlement et du Conseil du 20 novembre 2013 ; que le conseil national de l'ordre des infirmiers et les autres requérants mentionnés ci-dessus demandent l'annulation de cette ordonnance ; que, ces requêtes ayant le même objet, il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              2. Considérant que l'article 38 de la Constitution dispose que : " Le gouvernement peut, pour l'exécution de son programme, demander au Parlement l'autorisation de prendre par ordonnances, pendant un délai limité, des mesures qui sont normalement du domaine de la loi. / Les ordonnances sont prises en conseil des ministres après avis du Conseil d'État. Elles entrent en vigueur dès leur publication mais deviennent caduques si le projet de loi de ratification n'est pas déposé devant le Parlement avant la date fixée par la loi d'habilitation. Elles ne peuvent être ratifiées que de manière expresse. / A l'expiration du délai mentionné au premier alinéa du présent article, les ordonnances ne peuvent plus être modifiées que par la loi dans les matières qui sont du domaine législatif " ; <br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions que, dès lors que sa ratification est opérée par le législateur, une ordonnance acquiert valeur législative à compter de sa signature ; qu'il suit de là qu'en cas de ratification, la juridiction administrative cesse d'être compétente pour connaître d'une demande d'annulation de l'ordonnance ; que la circonstance, à la supposer établie, que les dispositions de l'ordonnance, ayant ainsi acquis valeur législative, porteraient atteinte aux droits et libertés garantis par la Constitution ou aux engagements internationaux de la France est sans incidence sur l'incompétence de la juridiction administrative pour statuer sur le recours contre l'ordonnance ; qu'il appartient aux personnes concernées d'invoquer de telles atteintes dans le cadre de litiges relatifs à l'application des dispositions ratifiées ;<br/>
<br/>
              4. Considérant que l'ordonnance attaquée du 19 janvier 2017 relative à la reconnaissance des qualifications professionnelles dans le domaine de la santé a été ratifiée en cours d'instance  par l'article 2 de la loi du 26 février 2018 ; <br/>
<br/>
              5. Considérant, d'une part, que la Fédération des orthophonistes de France et autres soulèvent une question prioritaire de constitutionnalité dirigée contre cet article 2 de la loi du 26 février 2018 portant ratification de l'ordonnance attaquée, en soutenant que les dispositions de cette ordonnance portent atteinte aux droits et libertés garantis par la Constitution ; que, toutefois, la question de la constitutionnalité de ces dispositions est, ainsi qu'il a été dit, sans incidence sur l'incompétence de la juridiction administrative pour prononcer l'annulation de l'ordonnance ratifiée ; que, par suite, il n'y a pas lieu de renvoyer la question prioritaire de constitutionnalité soulevée au Conseil constitutionnel ;<br/>
<br/>
              6. Considérant, d'autre part, que les requérants ne peuvent davantage soutenir utilement que les dispositions ratifiées seraient incompatibles avec la directive 2013/55/UE du Parlement et du Conseil du 20 novembre 2013, une telle circonstance, à la supposer établie, n'étant pas de nature à permettre à la juridiction administrative de statuer sur le recours dirigé contre l'ordonnance ratifiée ; <br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède qu'il n'y a plus lieu de statuer sur les requêtes ; <br/>
<br/>
              8. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par les requérants au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la Fédération nationale des orthophonistes et autres.<br/>
Article 2 : Il n'y a pas lieu de statuer sur les conclusions des requêtes du conseil national de l'ordre des infirmiers, de la Fédération nationale des orthophonistes et autres, du conseil national de l'ordre des pédicures-podologues et de l'Ordre national des chirurgiens-dentistes tendant à l'annulation de l'ordonnance n° 2017-50 du 19 janvier 2017.<br/>
Article 3 : Les conclusions présentées par les requérants au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée au conseil national de l'ordre des infirmiers, à la Fédération nationale des orthophonistes, premier requérant dénommé dans la requête n° 409019, au conseil national de l'ordre des pédicures-podologues, à l'Ordre national des chirurgiens-dentistes, au Premier ministre et à la ministre des solidarités et de la santé.<br/>
Copie en sera adressée au Conseil constitutionnel.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-045 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. - REP CONTRE UNE ORDONNANCE RATIFIÉE - INCOMPÉTENCE DE LA JURIDICTION ADMINISTRATIVE, NONOBSTANT LA CIRCONSTANCE QUE CETTE ORDONNANCE PORTERAIT ATTEINTE AUX DROITS ET LIBERTÉS GARANTIS PAR LA CONSTITUTION OU AUX ENGAGEMENTS INTERNATIONAUX DE LA FRANCE - CONSÉQUENCE - NON RENVOI DE LA QPC DIRIGÉE CONTRE LA LOI DE RATIFICATION DE CETTE ORDONNANCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">17-02-01-02 COMPÉTENCE. ACTES ÉCHAPPANT À LA COMPÉTENCE DES DEUX ORDRES DE JURIDICTION. ACTES LÉGISLATIFS. ACTES DE NATURE LÉGISLATIVE. - REP CONTRE UNE ORDONNANCE RATIFIÉE - INCOMPÉTENCE DE LA JURIDICTION ADMINISTRATIVE, NONOBSTANT LA CIRCONSTANCE QUE CETTE ORDONNANCE PORTERAIT ATTEINTE AUX DROITS ET LIBERTÉS GARANTIS PAR LA CONSTITUTION OU AUX ENGAGEMENTS INTERNATIONAUX DE LA FRANCE - CONSÉQUENCE - NON RENVOI DE LA QPC DIRIGÉE CONTRE LA LOI DE RATIFICATION DE CETTE ORDONNANCE [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-10 PROCÉDURE. - REP CONTRE UNE ORDONNANCE RATIFIÉE - INCOMPÉTENCE DE LA JURIDICTION ADMINISTRATIVE, NONOBSTANT LA CIRCONSTANCE QUE CETTE ORDONNANCE PORTERAIT ATTEINTE AUX DROITS ET LIBERTÉS GARANTIS PAR LA CONSTITUTION OU AUX ENGAGEMENTS INTERNATIONAUX DE LA FRANCE - CONSÉQUENCE - NON RENVOI DE LA QPC DIRIGÉE CONTRE LA LOI DE RATIFICATION DE CETTE ORDONNANCE [RJ1].
</SCT>
<ANA ID="9A"> 01-01-045 QPC soulevée contre une loi de ratification d'une ordonnance déjà ratifiée à l'appui de la demande d'annulation de cette ordonnance.,,,Il résulte de l'article 38 de la Constitution que, dès lors que sa ratification est opérée par le législateur, une ordonnance acquiert valeur législative à compter de sa signature. Il suit de là qu'en cas de ratification, la juridiction administrative cesse d'être compétente pour connaître d'une demande d'annulation de l'ordonnance. La circonstance, à la supposer établie, que les dispositions de l'ordonnance, ayant ainsi acquis valeur législative, porteraient atteinte aux droits et libertés garantis par la Constitution ou aux engagements internationaux de la France est sans incidence à cet égard sur l'incompétence de la juridiction administrative pour statuer sur le recours contre l'ordonnance. Il appartient aux personnes concernées d'invoquer de telles atteintes dans le cadre de litiges relatifs à l'application des dispositions ratifiées. Par suite, il n'y a pas lieu de renvoyer la question prioritaire de constitutionnalité (QPC) soulevée.</ANA>
<ANA ID="9B"> 17-02-01-02 QPC soulevée contre une loi de ratification d'une ordonnance déjà ratifiée à l'appui de la demande d'annulation de cette ordonnance.,,,Il résulte de l'article 38 de la Constitution que, dès lors que sa ratification est opérée par le législateur, une ordonnance acquiert valeur législative à compter de sa signature. Il suit de là qu'en cas de ratification, la juridiction administrative cesse d'être compétente pour connaître d'une demande d'annulation de l'ordonnance. La circonstance, à la supposer établie, que les dispositions de l'ordonnance, ayant ainsi acquis valeur législative, porteraient atteinte aux droits et libertés garantis par la Constitution ou aux engagements internationaux de la France est sans incidence à cet égard sur l'incompétence de la juridiction administrative pour statuer sur le recours contre l'ordonnance. Il appartient aux personnes concernées d'invoquer de telles atteintes dans le cadre de litiges relatifs à l'application des dispositions ratifiées. Par suite, il n'y a pas lieu de renvoyer la question prioritaire de constitutionnalité (QPC) soulevée.</ANA>
<ANA ID="9C"> 54-10 QPC soulevée contre une loi de ratification d'une ordonnance déjà ratifiée à l'appui de la demande d'annulation de cette ordonnance.,,,Il résulte de l'article 38 de la Constitution que, dès lors que sa ratification est opérée par le législateur, une ordonnance acquiert valeur législative à compter de sa signature. Il suit de là qu'en cas de ratification, la juridiction administrative cesse d'être compétente pour connaître d'une demande d'annulation de l'ordonnance. La circonstance, à la supposer établie, que les dispositions de l'ordonnance, ayant ainsi acquis valeur législative, porteraient atteinte aux droits et libertés garantis par la Constitution ou aux engagements internationaux de la France est sans incidence à cet égard sur l'incompétence de la juridiction administrative pour statuer sur le recours contre l'ordonnance. Il appartient aux personnes concernées d'invoquer de telles atteintes dans le cadre de litiges relatifs à l'application des dispositions ratifiées. Par suite, il n'y a pas lieu de renvoyer la question prioritaire de constitutionnalité (QPC) soulevée.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp. CE, 8 décembre 2000, Hoffer et autres, n°s 199072 199135 199761, p. 585.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
