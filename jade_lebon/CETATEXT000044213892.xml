<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044213892</ID>
<ANCIEN_ID>JG_L_2021_10_000000451866</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/21/38/CETATEXT000044213892.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 15/10/2021, 451866</TITRE>
<DATE_DEC>2021-10-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>451866</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI ; SCP OHL, VEXLIARD ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Rozen Noguellou</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier  Fuchs</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:451866.20211015</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par quatre mémoires et un mémoire en réplique, enregistrés les 19 juillet et 23 septembre 2021 au secrétariat du contentieux du Conseil d'État, M. D... E... demande au Conseil d'État, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 et à l'appui de sa requête tendant à l'annulation de la décision du Haut conseil du commissariat aux comptes du 19 février 2021 le radiant de la liste des commissaires aux comptes et prononçant à son encontre une sanction pécuniaire de 100 000 euros et une interdiction pour une durée de trois ans d'exercer des fonctions d'administration ou de direction au sein d'une société de commissaires aux comptes et au sein d'entités d'intérêt public, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution de l'article 53 de l'ordonnance n° 2016-315 du 17 mars 2016 relative au commissariat aux comptes, des articles 22 à 30 de la loi n° 2019-486 du 22 mai 2019 relative à la croissance et la transformation des entreprises, des articles L. 821-1, L. 821-6, L. 821-13, L. 821-14 du code de commerce, des articles L. 824-4 à L. 824-11 du code de commerce. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code de commerce ;<br/>
              - l'ordonnance n°2016-315 du 17 mars 2016 ;<br/>
              - la loi n°2019-486 du 22 mai 2019 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Rozen Noguellou, conseillère d'Etat,  <br/>
<br/>
              - les conclusions de M. Olivier Fuchs, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Spinosi, avocat de M. E..., à la SCP Ohl, Vexliard, avocat du Haut conseil du commissariat aux comptes et à la SCP Piwnica, Molinié, avocat de la société Pricewaterhousecoopers Audit SAS et autres ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. M. E... soulève, à l'appui des conclusions de sa requête tendant à l'annulation de la décision du Haut conseil du commissariat aux comptes du 19 février 2021 le radiant de la liste des commissaires aux comptes et le condamnant à une sanction pécuniaire de 100 000 euros et à une interdiction d'exercer des fonctions d'administration ou de direction au sein d'une société de commissaires aux comptes et au sein d'entités d'intérêt public pour une durée de trois ans, la question de la conformité aux droits et libertés garantis par la Constitution, en premier lieu, de l'article 53 de l'ordonnance du 17 mars 2016 relative au commissariat aux comptes ; en deuxième lieu, des articles 22 à 30 de la loi du 22 mai 2019 relative à la croissance et la transformation des entreprises ; en troisième lieu, des articles L. 821-1, L. 821-6, L. 821-13 et L. 821-14 du code de commerce ; en dernier lieu, des articles L. 824-4 à L. 824-11 du même code. <br/>
<br/>
              Sur la question prioritaire de constitutionnalité relative à l'article 53 de l'ordonnance du 17 mars 2016 :<br/>
<br/>
              3. L'article 53 de l'ordonnance du 17 mars 2016 relative au commissariat aux comptes précise les modalités d'entrée en vigueur des nouvelles dispositions que cette ordonnance insère dans le code de commerce en prévoyant, à son premier alinéa seul en cause ici, que " les dispositions de la présente ordonnance entrent en vigueur le 17 juin 2016 ". Il résulte de ses termes mêmes que cette disposition ne s'applique qu'à des faits commis postérieurement au 17 juin 2016 et ne confère aucune portée rétroactive aux dispositions introduites par l'ordonnance relatives aux manquement susceptibles d'être sanctionnés comme aux sanctions susceptibles d'être prononcées à l'égard des professionnels concernés. La disposition en cause de l'alinéa 1 de l'article 53 de l'ordonnance du 17 mars 2016 ne porte par suite aucune atteinte au principe de légalité des délits et des peines garanti par l'article 8 de la Déclaration des droits de l'homme et du citoyen de 1789.<br/>
<br/>
              4. Il résulte de ce qui précède que la question prioritaire de constitutionnalité soulevée, qui n'est pas nouvelle, ne présente pas de caractère sérieux. Il n'y a, dès lors, pas lieu de la renvoyer au Conseil constitutionnel.<br/>
<br/>
              Sur la question prioritaire de constitutionnalité relative aux articles 22 à 30 de la loi n° 2019-486 du 22 mai 2019 :<br/>
<br/>
              5. Telle qu'elle est formulée, la question prioritaire de constitutionnalité ne porte que sur les articles 24 et 25 de la loi du 22 mai 2019 relative à la croissance et la transformation des entreprises.<br/>
<br/>
              6. L'article 24 de la loi du 22 mai 2019 relative à la croissance et la transformation des entreprises modifie l'article L. 824-8 du code de commerce en supprimant les commissions régionales de discipline et en prévoyant que : " Le rapporteur général établit un rapport final qu'il adresse à la formation restreinte avec les observations de la personne intéressée ". L'article 25 de la loi du 22 mai 2019 permet quant à lui au rapporteur général d'obtenir tout document " utile à l'enquête ".<br/>
<br/>
              7. M. E... soutient que ces dispositions, en ce qu'elles ont modifié la procédure applicable sans prévoir d'application différée dans le temps, sont contraires aux droits de la défense, au principe du contradictoire et au droit à un recours effectif garantis par l'article 16 de la Déclaration des droits de l'homme et du citoyen.<br/>
<br/>
              8. La nouvelle rédaction de l'article L. 824-8 du code de commerce issue de l'article 24 de la loi du 22 mai 2019 tire les conséquences de la suppression, par cette même loi, des commissions régionales de discipline en prévoyant la saisine directe de la formation restreinte du Haut conseil, qui est désormais la seule entité compétente pour connaître des actions disciplinaires. Si, s'agissant d'une règle procédurale, celle-ci a vocation à s'appliquer dès l'entrée en vigueur de la loi, y compris à des procédures en cours, cette évolution n'a pas supprimé une possibilité de renoncer aux poursuites que ne prévoyait pas l'ancienne rédaction de l'article L. 824-8 du code de commerce, la seconde délibération du Haut conseil alors prévue par ce texte ayant pour seule finalité de choisir, au vu du rapport final du rapporteur général, la formation disciplinaire compétente et non pour objet de décider du maintien ou de l'abandon des griefs précédemment notifiés, et est par ailleurs sans incidence sur les droits, reconnus par l'article L. 824-8 du code de commerce aux personnes poursuivies, d'avoir accès au dossier, de présenter leurs observations et de se faire assister par un conseil de leur choix à toutes les étapes de la procédure. La possibilité donnée par l'article 25 de la loi du 22 mai 2019 au rapporteur général d'avoir accès à tout document d'information " utile à l'enquête " n'a pas plus d'incidence sur les droits des personnes concernées d'avoir accès au dossier. Ces dispositions ne portent, dès lors, aucune atteinte aux droits de la défense, au principe du contradictoire ou au droit à un recours effectif garantis par l'article 16 de la Déclaration des droits de l'homme et du citoyen.<br/>
<br/>
              9. Il résulte de ce qui précède que la question soulevée, qui n'est pas nouvelle, ne présente pas de caractère sérieux. Il n'y a, par suite, pas lieu de la renvoyer au Conseil constitutionnel.<br/>
<br/>
              Sur la question prioritaire de constitutionnalité relative aux articles L. 821-1, L. 821-6, L. 821-13 et L. 821-14 du code de commerce :<br/>
<br/>
              10 L'article L. 821-1 du code de commerce précise les missions du Haut conseil du commissariat aux comptes. L'article L. 821-6 du même code est relatif à la compagnie nationale des commissaires aux comptes et aux compagnies régionales des commissaires aux comptes. Aux termes de l'article L. 821-13 de ce code : " I.- Le commissaire aux comptes exerce sa mission conformément aux normes d'audit internationales adoptées par la Commission européenne dans les conditions définies par l'article 26 de la directive 2006/43/CE du 17 mai 2006 concernant les contrôles légaux des comptes annuels et des comptes consolidés et modifiant les directives 78/660/CEE et 83/349/CEE du Conseil, et abrogeant la directive 84/253/CEE du Conseil, ainsi que, le cas échéant, aux normes françaises venant compléter ces normes adoptées selon les conditions fixées au troisième alinéa du présent article. / II. Lorsqu'une norme d'audit internationale a été adoptée par la Commission européenne dans les conditions définies au premier alinéa du I, le Haut conseil peut, dans les conditions prévues à l'article L. 821-14, imposer des procédures ou des exigences supplémentaires, si elles sont nécessaires pour donner effet aux obligations légales nationales concernant le champ d'application du contrôle légal des comptes ou pour renforcer la crédibilité et la qualité des documents comptables. Les procédures et exigences supplémentaires sont communiquées à la Commission européenne au moins trois mois avant leur entrée en vigueur. Si elles sont déjà en vigueur à la date de l'adoption de la norme internationale qu'elles complètent, la Commission européenne en est informée dans les trois mois suivant cette date. / III.- Pour la certification des comptes des petites entreprises, au sens du 2 de l'article 3 de la directive 2013/34/UE du Parlement européen et du Conseil du 26 juin 2013 relative aux états financiers annuels, aux états financiers consolidés et aux rapports y afférents de certaines formes d'entreprises, modifiant la directive 2006/43/CE du Parlement européen et du Conseil et abrogeant les directives 78/660/CEE et 83/349/CEE du Conseil, le commissaire aux comptes applique les normes de manière proportionnée à la taille de la personne ou de l'entité et à la complexité de ses activités dans des conditions fixées par le Haut conseil ". Enfin, aux termes de l'article L. 821-14 : " Le Haut conseil, de sa propre initiative ou à la demande du garde des sceaux, ministre de la justice, de l'Autorité des marchés financiers, de l'Autorité de contrôle prudentiel et de résolution ou de la Compagnie nationale des commissaires aux comptes, adopte les normes prévues au 2° de l'article L. 821-1. Les projets de normes sont élaborés par la commission prévue au III de l'article L. 821-2 dans un délai fixé par décret. A défaut d'élaboration par la commission d'un projet de norme dans ce délai, le garde des sceaux, ministre de la justice, peut demander au Haut conseil de procéder à son élaboration. Les normes sont adoptées par le Haut conseil, après avis de la Compagnie nationale des commissaires aux comptes rendu dans un délai fixé par décret. Elles sont homologuées par arrêté du garde des sceaux, ministre de la justice ".<br/>
<br/>
              11. M. E... soutient que le législateur, en confiant au Haut conseil du commissariat aux comptes le pouvoir d'élaborer et d'adopter des normes d'exercice professionnel des commissaires aux comptes, a méconnu l'étendue de sa compétence dans des conditions affectant la liberté d'entreprendre, le principe de légalité des délits et des peines et les droits de la défense et que ces dispositions sont par ailleurs contraires à la garantie des droits en conférant à une même autorité un pouvoir réglementaire et de sanction.<br/>
<br/>
              12. L'article L. 821-1 du code de commerce confie au Haut conseil du commissariat aux comptes le pouvoir d'adopter voire d'élaborer des projets de normes relatives à la déontologie des commissaires aux comptes, au contrôle interne de qualité et à l'exercice professionnel, lesquels sont homologuées par le garde des sceaux, ministre de la justice. L'objet de ces normes est précisément défini à la fois par référence aux normes européennes auxquelles renvoie l'article L. 821-13 et par les missions des commissaires aux comptes détaillées aux articles L. 823-9 et suivants du code de commerce. En confiant au Haut conseil la compétence d'adopter de telles normes, le législateur n'a pas méconnu l'étendue de sa compétence ni porté atteinte à la liberté d'entreprendre et aux droits de la défense. L'attribution, au profit d'autres autorités de l'Etat que le Premier ministre, du pouvoir de fixer des normes permettant de mettre en œuvre une loi ne met par ailleurs par elle-même en cause aucun droit ou liberté constitutionnellement garanti.<br/>
<br/>
              13. Aux termes de l'article 16 de la Déclaration des droits de l'homme et du citoyen du 26 août 1789 : " Toute société dans laquelle la garantie des droits n'est pas assurée, ni la séparation des pouvoirs déterminée, n'a point de Constitution ".  L'attribution par la loi à une autorité administrative du pouvoir de fixer les règles dans un domaine déterminé et d'en assurer elle-même le respect, par l'exercice d'un pouvoir de contrôle des activités exercées et de sanction des manquements constatés, ne contrevient pas aux exigences découlant de l'article 16 de la Déclaration des droits de l'homme et du citoyen dès lors que ce pouvoir de sanction est aménagé de telle façon que soient assurés le respect des droits de la défense, le caractère contradictoire de la procédure et les principes d'indépendance et d'impartialité.<br/>
<br/>
              14. Si aucune disposition du code de commerce ne fait obstacle à ce que des membres de la formation restreinte du collège du Haut conseil aient par ailleurs siégé dans les instances de ce Haut conseil chargées d'élaborer ou d'adopter les normes dont la formation restreinte est amenée à faire application lorsqu'elle se prononce sur les procédures individuelles dont elle est saisie, cette circonstance n'est par elle-même pas de nature à méconnaître les exigences découlant de l'article 16 de la Déclaration des droits de l'homme et du citoyen, la formation restreinte n'ayant à connaître que de litiges individuels. <br/>
<br/>
              15. Il résulte de ce qui précède que la question prioritaire de constitutionnalité soulevée, qui n'est pas nouvelle, ne présente pas de caractère sérieux. Il n'y a, par suite, pas lieu de la renvoyer au Conseil constitutionnel.<br/>
<br/>
              Sur la question prioritaire de constitutionnalité relative aux articles L. 824-4 à L. 824-11 du code de commerce :<br/>
<br/>
              16. Telle qu'elle est formulée, la question prioritaire de constitutionnalité n'est relative qu'aux articles L. 824-8 et L. 824-11 du code de commerce. L'article L. 824-8 organise le pouvoir d'enquête du rapporteur général. L'article L. 824-11 précise la procédure de sanction devant la formation restreinte du Haut conseil.<br/>
<br/>
              17. M. E... soutient que le législateur a méconnu l'étendue de sa compétence en ce que ces dispositions, en ne prévoyant pas de mécanisme de sursis à statuer de la formation restreinte du Haut conseil dans l'hypothèse d'une concomitance de poursuites disciplinaires devant le Haut conseil et de poursuites pénales engagées contre le même commissaire aux comptes pour les mêmes faits, portent atteinte à son droit de rester silencieux, en violation de ses droits de la défense. Il soutient également qu'elles violent le principe d'égalité devant la loi pénale en ce qu'elles admettent la possibilité que certains commissaires aux comptes puissent faire l'objet d'une sanction disciplinaire et d'une sanction pénale.<br/>
<br/>
              18. Contrairement à ce que soutient M. E..., rien n'interdit à la personne faisant l'objet de poursuites disciplinaires et pénales de choisir de rester silencieuse dans le cadre de la procédure disciplinaire comme dans celui de la procédure pénale. Au demeurant, s'il appartient en principe au juge disciplinaire de statuer sur une plainte dont il est saisi sans attendre l'issue d'une procédure pénale en cours concernant les mêmes faits, il peut, sans qu'il soit besoin pour le législateur de le préciser, décider de surseoir à statuer jusqu'à la décision du juge pénal lorsque cela paraît utile à la qualité de l'instruction ou à la bonne administration de la justice. Le législateur n'a pas suite pas méconnu l'étendue de sa compétence ni porté atteinte aux droits de la défense en ne prévoyant pas un tel mécanisme de sursis à statuer de la formation restreinte en cas de concomitance avec une procédure pénale pendante relative aux mêmes faits.<br/>
<br/>
              19. Par ailleurs, la circonstance que des poursuites pénales puissent n'être engagées qu'à l'encontre de certains commissaires aux comptes ne découle pas des dispositions législatives contestées mais des appréciations portées sur leurs comportements par les autorités en charge d'initier ces procédures. Les dispositions du code de commerce contestées ne portent donc aucune atteinte au principe d'égalité devant la loi pénale.<br/>
<br/>
              20. Il résulte de ce qui précède que la question prioritaire de constitutionnalité soulevée, qui n'est pas nouvelle, ne présente pas de caractère sérieux. Il n'y a, dès lors, pas lieu de la renvoyer au Conseil constitutionnel.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel les questions prioritaires de constitutionnalité soulevées par M. E....<br/>
Article 2 : La présente décision sera notifiée à M. D... E..., au Haut conseil du commissariat aux comptes, au garde des sceaux, ministre de la justice et au ministre de l'économie, des finances et de la relance.<br/>
Copie en sera adressée au Conseil constitutionnel et au Premier ministre.<br/>
<br/>
              Délibéré à l'issue de la séance du 8 octobre 2021 où siégeaient : Mme Christine Maugüé, présidente adjointe de la Section du contentieux, présidant ; M. A... H..., M. Fabien Raynaud, présidents de chambre ; Mme N... J..., M. L... C..., Mme G... K..., M. F... I..., M. Cyril Roger-Lacan, conseillers d'Etat et Mme Rozen Noguellou, conseillère d'Etat-rapporteure. <br/>
<br/>
              Rendu le 15 octobre 2021.<br/>
<br/>
<br/>
                 La présidente : <br/>
                 Signé : Mme Christine Maugüé<br/>
 		La rapporteure : <br/>
      Signé : Mme Rozen Noguellou<br/>
                 La secrétaire :<br/>
                 Signé : Mme M... B...<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-02-02-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. - VALIDITÉ DES ACTES ADMINISTRATIFS - COMPÉTENCE. - RÉPARTITION DES COMPÉTENCES ENTRE AUTORITÉS DISPOSANT DU POUVOIR RÉGLEMENTAIRE. - AUTORITÉS DISPOSANT DU POUVOIR RÉGLEMENTAIRE. - ATTRIBUTION DU POUVOIR RÉGLEMENTAIRE D'APPLICATION D'UNE LOI À UNE AUTRE AUTORITÉ QUE LE PREMIER MINISTRE - MISE EN CAUSE DE DROITS OU LIBERTÉS GARANTIS PAR LA CONSTITUTION - ABSENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-10-01-02 PROCÉDURE. - ATTRIBUTION DU POUVOIR RÉGLEMENTAIRE D'APPLICATION D'UNE LOI À UNE AUTRE AUTORITÉ QUE LE PREMIER MINISTRE - EXCLUSION [RJ1].
</SCT>
<ANA ID="9A"> 01-02-02-01 L’attribution, au profit d’autres autorités de l’Etat que le Premier ministre, du pouvoir de fixer des normes permettant de mettre en œuvre une loi ne met par ailleurs par elle-même en cause aucun droit ou liberté constitutionnellement garanti.</ANA>
<ANA ID="9B"> 54-10-01-02 L’attribution, au profit d’autres autorités de l’Etat que le Premier ministre, du pouvoir de fixer des normes permettant de mettre en œuvre une loi ne met par ailleurs par elle-même en cause aucun droit ou liberté constitutionnellement garanti.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., sur l'absence d'inconstitutionnalité, par principe, d'une telle attribution, Cons. const., 17 janvier 1989, n° 88-248 DC.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
