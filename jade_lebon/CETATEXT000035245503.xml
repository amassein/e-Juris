<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035245503</ID>
<ANCIEN_ID>JG_L_2017_07_000000391849</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/24/55/CETATEXT000035245503.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 5ème chambres réunies, 19/07/2017, 391849, Publié au recueil Lebon</TITRE>
<DATE_DEC>2017-07-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>391849</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; HAAS</AVOCATS>
<RAPPORTEUR>Mme Sara-Lou Gerber</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:391849.20170719</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Marseille d'annuler pour excès de pouvoir la décision du 6 novembre 2013 par laquelle l'inspecteur du travail des Bouches-du-Rhône a autorisé la société Milonga à le licencier. Par un jugement n° 1400015 du 4 mars 2014, le tribunal administratif a annulé cette décision.<br/>
<br/>
              Par un arrêt n° 14MA01879-14MA02021 du 23 juin 2015, la cour administrative d'appel de Marseille a, sur les appels de la société Milonga et du ministre du travail, de l'emploi et du dialogue social, annulé ce jugement et rejeté la demande de M. B....<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 20 juillet et 31 août 2015 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat et de la SCP J.P Louis et A. Lageat, en qualité de liquidateur judiciaire de la société Milonga, une somme globale de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la décision n° 384114, 384219 du 30 mai 2016 du Conseil d'Etat statuant au contentieux ;<br/>
<br/>
              Vu :<br/>
              - le code du travail, modifié notamment par la loi n° 2015-990 du 6 août 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sara-Lou Gerber, auditeur,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de M. B...et à Me Haas, avocat de la SCP J.P Louis et A. Lageat liquidateur judiciaire de la société Milonga ;<br/>
<br/>
<br/>
<br/>
<br/>1.  Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un jugement du 2 octobre 2013, le tribunal de commerce de Marseille a prononcé la liquidation judiciaire sans poursuite d'activité de la société Milonga ; que, par une décision du 10 octobre 2013, le directeur régional des entreprises, de la concurrence, de la consommation, du travail et de l'emploi de Provence-Alpes-Côte d'Azur a homologué le plan de sauvegarde de l'emploi élaboré en raison du licenciement des 163 salariés de l'entreprise ; que, par une décision du 6 novembre 2013, l'inspecteur du travail compétent a autorisé le liquidateur judiciaire à procéder au licenciement pour motif économique de M.B..., salarié protégé ; que, par deux jugements n° 1308064 et n° 1400015 du 4 mars 2014, le tribunal administratif de Marseille a, respectivement, annulé la décision d'homologation du plan de sauvegarde de l'emploi et annulé par voie de conséquence l'autorisation de licencier M.B... ; que ce dernier se pourvoit en cassation contre l'arrêt du 23 juin 2015 par lequel la cour administrative d'appel de Marseille a annulé le jugement n° 1400015 du tribunal administratif de Marseille et rejeté sa demande d'annulation de la décision autorisant son licenciement ; <br/>
<br/>
              Sur le droit applicable : <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 1233-61 du code du travail : " Dans les entreprises d'au moins cinquante salariés, lorsque le projet de licenciement concerne au moins dix salariés dans une même période de trente jours, l'employeur établit et met en oeuvre un plan de sauvegarde de l'emploi pour éviter les licenciements ou en limiter le nombre. (...) " ; qu'il résulte des dispositions combinées des articles L. 1233-24-1 à L. 1233-24-4, L. 1233-57-2 et L. 1233-57-3 du même code que l'accord d'entreprise ou, à défaut, la décision unilatérale de l'employeur qui fixe ce plan de sauvegarde de l'emploi doit être validé ou homologué par l'autorité administrative ; <br/>
<br/>
              3. Considérant que le premier alinéa de l'article L. 1235-10 du code du travail, applicable aux licenciements collectifs dans les entreprises d'au moins cinquante salariés qui ne sont pas en redressement ou en liquidation judiciaire, dispose que : " (...) le licenciement intervenu en l'absence de toute décision relative à la validation ou à l'homologation ou alors qu'une décision négative a été rendue est nul "  ; que, s'agissant des entreprises en liquidation ou en redressement judiciaire, l'article L. 1233-58 du même code dispose que : " (...) L'employeur, l'administrateur ou le liquidateur ne peut procéder, sous peine d'irrégularité, à la rupture des contrats de travail avant la notification de la décision favorable de validation ou d'homologation (...) " ;<br/>
<br/>
              4. Considérant qu'il résulte des dispositions citées aux points 2 et 3 que, lorsque le licenciement pour motif économique d'un salarié protégé est inclus dans un licenciement collectif qui requiert l'élaboration d'un plan de sauvegarde de l'emploi, il appartient à l'inspecteur du travail saisi de la demande d'autorisation de ce licenciement, ou au ministre chargé du travail statuant sur recours hiérarchique, de s'assurer de l'existence, à la date à laquelle il statue sur cette demande, d'une décision de validation ou d'homologation du plan de sauvegarde de l'emploi, à défaut de laquelle l'autorisation de licenciement ne peut légalement être accordée ; qu'en revanche, dans le cadre de l'examen de cette demande, il n'appartient à ces autorités, ni d'apprécier la validité du plan de sauvegarde de l'emploi ni, plus généralement, de procéder aux contrôles mentionnés aux articles L. 1233-57-2 et L. 1233-57-3 du code du travail, qui n'incombent qu'au directeur régional des entreprises, de la concurrence, de la consommation, du travail et de l'emploi compétemment saisi de la demande de validation ou d'homologation du plan ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui a été dit ci-dessus que l'annulation, pour excès de pouvoir, d'une décision de validation ou d'homologation d'un plan de sauvegarde de l'emploi entraîne, par voie de conséquence, l'illégalité des autorisations de licenciement accordées, à la suite de cette validation ou de cette homologation, pour l'opération concernée ; qu'en revanche, le moyen tiré, par voie d'exception, de l'illégalité de la décision de validation ou d'homologation d'un plan de sauvegarde de l'emploi ne saurait être utilement soulevé au soutien d'un recours dirigé contre une autorisation de licenciement d'un salarié protégé ; <br/>
<br/>
              6. Considérant, toutefois, que les deux derniers alinéas de l'article L. 1235-16 du code du travail introduits par la loi du 6 août 2015 pour la croissance, l'activité et l'égalité des chances économiques, ainsi que les deux derniers alinéas de l'article L. 1233-58 du même code introduits par la même loi, ont prévu, dans des termes identiques pour les entreprises qui sont en liquidation ou redressement judiciaire et pour les entreprises qui ne le sont pas, que : " En cas d'annulation d'une décision de validation mentionnée à l'article L. 1233-57-2 ou d'homologation mentionnée à l'article L. 1233-57-3 en raison d'une insuffisance de motivation, l'autorité administrative prend une nouvelle décision suffisamment motivée dans un délai de quinze jours à compter de la notification du jugement à l'administration. (...) / Dès lors que l'autorité administrative a édicté cette nouvelle décision, l'annulation pour le seul motif de l'insuffisance de motivation de la première décision de l'autorité administrative est sans incidence sur la validité du licenciement (...) " ; que ces dispositions sont applicables aux décisions d'annulation prononcées par le juge de l'excès de pouvoir à compter du 8 août 2015, date de l'entrée en vigueur de cette loi ;<br/>
<br/>
              7. Considérant, dès lors, que, par exception à ce qui est dit au point 5, l'annulation d'une décision d'homologation ou de validation d'un plan de sauvegarde de l'emploi prononcée à compter du 8 août 2015 et pour le seul motif d'une insuffisance de motivation n'entraîne pas, par elle-même, l'illégalité des autorisations de licenciement accordées dans le cadre de ce licenciement collectif, sous réserve que l'autorité administrative ait pris, dans le délai prévu par le texte cité ci-dessus, une nouvelle décision suffisamment motivée ; <br/>
<br/>
              Sur l'espèce :<br/>
<br/>
              8. Considérant qu'il résulte de ce qui a été dit au point 5 que, par son jugement n° 1400015 du 4 mars 2014, le tribunal administratif de Marseille a jugé à bon droit que l'annulation de la décision d'homologation du plan de sauvegarde de l'emploi de la société Milonga qu'il venait de prononcer entraînait, par voie de conséquence, l'illégalité de la décision autorisant le licenciement de M.B... ; qu'en jugeant le contraire, la cour administrative d'appel a commis une erreur de droit ; que son arrêt doit être annulé ;<br/>
<br/>
              9. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative et de statuer sur la requête de la société J.P Louis et A. Lageat et le recours du ministre du travail, de l'emploi et du dialogue social contre le jugement n° 1400015 du 4 mars 2014 du tribunal administratif de Marseille ;<br/>
<br/>
              10. Considérant qu'il est constant que le licenciement pour motif économique de M. B...était inclus dans un licenciement collectif pour lequel l'élaboration d'un plan de sauvegarde de l'emploi était requise en vertu des dispositions citées ci-dessus de l'article L. 1233-61 du code du travail ; qu'ainsi qu'il est dit au point 4, l'existence d'une décision de validation ou d'homologation d'un tel plan était, par suite, au nombre des conditions à respecter pour que ce licenciement puisse être légalement autorisé ;<br/>
<br/>
              11. Considérant que, par un arrêt du 1er juillet 2014, la cour administrative d'appel de Marseille a rejeté les appels formés contre le jugement n° 1308064 du 4 mars 2014 par lequel le tribunal administratif de Marseille a annulé la décision d'homologation du plan de sauvegarde de l'emploi de la société Milonga ; que, par une décision du 30 mai 2016, le Conseil d'Etat, statuant au contentieux, a rejeté les pourvois dirigés contre cet arrêt ; qu'ainsi, eu égard à l'effet rétroactif des annulations contentieuses, aucune décision d'homologation du plan de sauvegarde de l'emploi n'était en vigueur à la date à laquelle l'inspecteur du travail a autorisé le licenciement de M.B... ; que, cette autorisation ne pouvant, par suite, être légalement accordée, la société J.P Louis et A. Lageat et le ministre chargé du travail ne sont pas fondés à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Marseille a annulé la décision du 6 novembre 2013 autorisant le licenciement de M. B... ;<br/>
<br/>
              12. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat et de la société J.P Louis et A. Lageat, tant pour l'instance d'appel que pour l'instance de cassation, une somme de 2 500 euros chacun à verser à M. B...au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'en revanche, ces même dispositions font obstacle à ce que soit mise à la charge de M.B..., qui n'est pas la partie perdante, la somme que demande au même titre la SCP J.P Louis et A. Lageat ;<br/>
<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 23 juin 2015 est annulé.<br/>
Article 2 : La requête de la société J.P Louis et A. Lageat et le recours du ministre du travail, de l'emploi et du dialogue social présentés devant la cour administrative d'appel de Marseille sont rejetés. <br/>
Article 3 : L'Etat et la SCP J.P Louis et A. Lageat, en qualité de liquidateur judiciaire de la société Milonga, verseront à M. B...la somme de 2 500 euros chacun au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de la SCP J.P Louis et A. Lageat présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à M. A...B..., à la SCP J.P Louis et A. Lageat et à la ministre du travail. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-05-06 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - MOTIFS. ANNULATION PAR VOIE DE CONSÉQUENCE. - ANNULATION D'UNE AUTORISATION DE LICENCIER UN SALARIÉ PROTÉGÉ POUR MOTIF ÉCONOMIQUE PAR VOIE DE CONSÉQUENCE DE L'ANNULATION DE LA DÉCISION DE VALIDATION OU D'HOMOLOGATION DU PSE [RJ3].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-01-04-04-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. EXCEPTION D'ILLÉGALITÉ. - MOYEN TIRÉ DE L'ILLÉGALITÉ DE LA DÉCISION DE VALIDATION OU D'HOMOLOGATION D'UN PSE SOULEVÉ DANS LE CADRE D'UN RECOURS DIRIGÉ CONTRE L'AUTORISATION DE LICENCIEMENT POUR MOTIF ÉCONOMIQUE D'UN SALARIÉ PROTÉGÉ.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">66-07-01-03-03 TRAVAIL ET EMPLOI. LICENCIEMENTS. AUTORISATION ADMINISTRATIVE - SALARIÉS PROTÉGÉS. MODALITÉS DE DÉLIVRANCE OU DE REFUS DE L'AUTORISATION. POUVOIRS DE L'AUTORITÉ ADMINISTRATIVE. - DEMANDE DE LICENCIEMENT POUR MOTIF ÉCONOMIQUE D'UN SALARIÉ PROTÉGÉ REQUÉRANT L'ÉLABORATION D'UN PSE - 1) ETENDUE DU CONTRÔLE - A) EXISTENCE D'UNE DÉCISION DE VALIDATION OU D'HOMOLOGATION DU PSE - INCLUSION - B) VALIDITÉ DU PSE - EXCLUSION [RJ1] - C) RÉGULARITÉ DE LA VALIDATION OU DE L'HOMOLOGATION DU PSE PAR LE DIRECCTE - EXCLUSION [RJ2] - 2) CONSÉQUENCES - A) ANNULATION DE L'AUTORISATION DE LICENCIEMENT PAR VOIE DE CONSÉQUENCE DE L'ANNULATION DE LA DÉCISION DE VALIDATION OU D'HOMOLOGATION DU PSE - EXISTENCE [RJ3] - B) EXCEPTION D'ILLÉGALITÉ DE LA DÉCISION DE VALIDATION OU D'HOMOLOGATION DU PSE SOULEVÉE DANS LE CADRE D'UN RECOURS DIRIGÉ CONTRE L'AUTORISATION DE LICENCIEMENT - MOYEN INOPÉRANT.
</SCT>
<ANA ID="9A"> 01-05-06 Demande de licenciement pour motif économique d'un salarié protégé requérant l'élaboration d'un plan de sauvegarde de l'emploi (PSE).... ,,L'annulation, pour excès de pouvoir, de la décision de validation ou d'homologation du PSE entraîne, par voie de conséquence, l'illégalité des autorisations de licenciement accordées, à la suite de cette validation ou de cette homologation, pour l'opération concernée.</ANA>
<ANA ID="9B"> 54-07-01-04-04-03 Demande de licenciement pour motif économique d'un salarié protégé requérant l'élaboration d'un plan de sauvegarde de l'emploi (PSE).... ,,Le moyen tiré, par voie d'exception, de l'illégalité de la décision de validation ou d'homologation du PSE ne saurait être utilement soulevé au soutien d'un recours dirigé contre l'autorisation de licenciement.</ANA>
<ANA ID="9C"> 66-07-01-03-03 1) a) Il résulte des articles L. 1235-10 et L. 1233-58 du code du travail que, lorsque le licenciement pour motif économique d'un salarié protégé est inclus dans un licenciement collectif qui requiert l'élaboration d'un plan de sauvegarde de l'emploi (PSE), il appartient à l'inspecteur du travail saisi de la demande d'autorisation de ce licenciement, ou au ministre chargé du travail statuant sur recours hiérarchique, de s'assurer de l'existence, à la date à laquelle il statue sur cette demande, d'une décision de validation ou d'homologation du plan de sauvegarde de l'emploi, à défaut de laquelle l'autorisation de licenciement ne peut légalement être accordée.... ,,b) En revanche, dans le cadre de l'examen de cette demande, il n'appartient pas à ces autorités d'apprécier la validité du PSE.,,,c) Il ne leur appartient pas davantage, plus généralement, de procéder aux contrôles mentionnés aux articles L. 1233-57-2 et L. 1233-57-3 du code du travail, qui n'incombent qu'au directeur régional des entreprises, de la concurrence, de la consommation, du travail et de l'emploi (DIRECCTE) compétemment saisi de la demande de validation ou d'homologation du plan.... ,,2) a) Il en résulte que l'annulation, pour excès de pouvoir, d'une décision de validation ou d'homologation d'un PSE entraîne, par voie de conséquence, l'illégalité des autorisations de licenciement accordées, à la suite de cette validation ou de cette homologation, pour l'opération concernée.... ,,b) En revanche, le moyen tiré, par voie d'exception, de l'illégalité de la décision de validation ou d'homologation d'un PSE ne saurait être utilement soulevé au soutien d'un recours dirigé contre une autorisation de licenciement d'un salarié protégé.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., en l'état antérieur des textes et en raison de la compétence de la juridiction judiciaire pour contrôler la validité du PSE, CE, 25 février 2015,,, n° 375590, T. p. 902., ,[RJ2] Comp. sur ce point, en l'état antérieur des textes, CE, Assemblée, 3 mars 1978,,, n° 01421, p. 114 ; CE, 25 février 2015,,, n° 375590, T. p. 902.,,[RJ3] Cf., sur les critères de l'annulation par voie de conséquence, CE, Section, 30 décembre 2013,,, n° 367615, p. 342.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
