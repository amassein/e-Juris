<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031973871</ID>
<ANCIEN_ID>JG_L_2016_01_000000386656</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/97/38/CETATEXT000031973871.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème / 5ème SSR, 27/01/2016, 386656</TITRE>
<DATE_DEC>2016-01-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>386656</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème / 5ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI ; SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>Mme Pauline Pannier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:386656.20160127</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Nantes d'annuler pour excès de pouvoir la décision du 23 août 2010 par laquelle le ministre du travail, de la solidarité et de la fonction publique a, d'une part, annulé la décision du 9 février 2010 de l'inspectrice du travail de la 11ème section de la Loire-Atlantique refusant d'autoriser la société Compagnie laitière de Derval à le licencier, et, d'autre part, autorisé ce licenciement. Par un jugement n° 1007853 du 11 juillet 2013, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 13NT02615 du 23 octobre 2014, la cour administrative d'appel de Nantes a, sur l'appel de M.A..., annulé ce jugement et la décision du 23 août 2010 du ministre du travail, de la solidarité et de la fonction publique.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique enregistrés les 23 décembre 2014, 23 mars 2015 et 22 décembre 2015 au secrétariat du contentieux du Conseil d'Etat, la société Compagnie laitière de Derval demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M. A...;<br/>
<br/>
              3°) de mettre à la charge de M. A...la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Pannier, auditeur,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, avocat de la société Compagnie laitière de Derval et à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société Compagnie laitière de Derval a demandé l'autorisation de licencier M. B...A..., salarié protégé, au motif de la cessation d'activité de l'entreprise ; que, saisi par la Compagnie laitière de Derval d'un recours dirigé contre la décision de l'inspecteur du travail refusant cette autorisation, le ministre chargé du travail a, par une décision du 23 août 2010, autorisé le licenciement de M. A...; que M. A...a demandé au tribunal administratif de Nantes l'annulation pour excès de pouvoir de cette décision ; que par un arrêt du 23 octobre 2014, la cour administrative d'appel de Nantes, statuant sur l'appel de M.A..., a annulé le jugement du tribunal administratif rejetant cette demande et la décision du 23 août 2010 ; que la société Compagnie laitière de Derval se pourvoit en cassation contre cet arrêt ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 1224-1 du code du travail : " Lorsque survient une modification dans la situation juridique de l'employeur, notamment par succession, vente, fusion, transformation du fonds, mise en société de l'entreprise, tous les contrats de travail en cours au jour de la modification subsistent entre le nouvel employeur et le personnel de l'entreprise " ; qu'aux termes de l'article L. 2414-1 du même code : " Le transfert d'un salarié compris dans un transfert partiel d'entreprise ou d'établissement par application de l'article L. 1224-1 ne peut intervenir qu'après autorisation de l'inspecteur du travail lorsqu'il est investi de l'un des mandats suivants (...) " ; qu'en application de ces dispositions, lorsqu'une entreprise cède tout ou partie de ses actifs dans des conditions de nature à caractériser le transfert d'une entité économique autonome, le salarié légalement investi de fonctions représentatives qui est en activité au sein de cette entité bénéficie du transfert de son contrat de travail ; que, d'une part, ce transfert du contrat de travail est de plein droit si l'activité de l'entreprise est intégralement transférée ; que, d'autre part, si la cession des actifs de l'entreprise constitue un transfert partiel d'entreprise ou d'établissement, le transfert du contrat de travail de ce salarié ne peut intervenir qu'après autorisation de l'inspecteur du travail ; <br/>
<br/>
              3. Considérant qu'en revanche, si le salarié n'est pas compris dans ce transfert partiel d'entreprise ou l'établissement, l'employeur qui cesse son activité peut demander à l'autorité administrative l'autorisation de le licencier au motif de la cessation d'activité de l'entreprise ; que s'il incombe, alors, à l'autorité administrative, pour établir la réalité du motif économique du licenciement, d'examiner si la cessation d'activité est totale et définitive, cet examen ne porte que sur les activités de l'entreprise qui n'ont pas fait l'objet du transfert partiel ;<br/>
<br/>
              4. Considérant qu'il ressort des termes de l'arrêt attaqué que la cour, qui a retenu que la société Compagnie laitière de Derval avait cédé son activité de fabrication de lait à la société Coralis et transféré son activité de fabrication de crème à la société Elvir, a jugé que la cessation d'activité invoquée par la société n'était, dès lors, pas totale et définitive ; qu'il résulte de ce qui a été dit ci-dessus que la cour a commis une erreur de droit en déduisant du constat de ces cessions et transferts que la cessation d'activité ne pouvait être regardée comme totale et définitive, sans limiter son examen aux activités de l'entreprise qui n'avaient pas fait l'objet du transfert partiel, et en annulant par suite, pour ce motif, l'autorisation de licencier M. A...; que dès lors, et sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, son arrêt doit être annulé ;<br/>
<br/>
              5. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société Compagnie laitière de Derval, qui n'est pas, dans la présente instance, la partie perdante ; que dans les circonstances de l'espèce, il n'y a pas lieu de mettre à la charge de M. A...la somme demandée par la société Compagnie laitière de Derval  au titre de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 23 octobre 2014 de la cour administrative d'appel de Nantes est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nantes.<br/>
Article 3 : Les conclusions présentées par M. A...et le surplus des conclusions de la société Compagnie laitière de Derval présenté au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetés.<br/>
Article 4 : La présente décision sera notifiée à la société Compagnie laitière de Derval et à M. B... A.... <br/>
Copie en sera adressée à la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">66-07-01-04-03 TRAVAIL ET EMPLOI. LICENCIEMENTS. AUTORISATION ADMINISTRATIVE - SALARIÉS PROTÉGÉS. CONDITIONS DE FOND DE L'AUTORISATION OU DU REFUS D'AUTORISATION. LICENCIEMENT POUR MOTIF ÉCONOMIQUE. - CESSATION D'ACTIVITÉ S'ACCOMPAGNANT D'UNE CESSION D'ACTIFS CARACTÉRISANT UN TRANSFERT PARTIEL - DEMANDE D'AUTORISATION DE LICENCIEMENT D'UN SALARIÉ PROTÉGÉ NON COMPRIS DANS LE TRANSFERT - CONTRÔLE DE L'INSPECTEUR DU TRAVAIL - CESSATION TOTALE ET DÉFINITIVE DES SEULES ACTIVITÉS NON COMPRISES DANS LE TRANSFERT [RJ1].
</SCT>
<ANA ID="9A"> 66-07-01-04-03 En cas de cessation d'activité d'une entreprise, lorsque celle-ci cède une partie de ses actifs dans des conditions de nature à caractériser le transfert d'une entité économique autonome mais que le salarié protégé n'est pas compris dans ce transfert partiel, l'employeur peut demander à l'autorité administrative l'autorisation de le licencier au motif de la cessation d'activité de l'entreprise. S'il incombe, alors, à l'autorité administrative, pour établir la réalité du motif économique du licenciement, d'examiner si la cessation d'activité est totale et définitive, cet examen ne porte que sur les activités de l'entreprise qui n'ont pas fait l'objet du transfert partiel.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf., sur les conditions d'autorisation du licenciement d'un salarié protégé en cas de cessation totale et définitive, CE, 8 avril 2013, M. Schintu, n° 348559, p. 59.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
