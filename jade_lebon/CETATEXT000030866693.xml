<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030866693</ID>
<ANCIEN_ID>JG_L_2015_07_000000371469</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/86/66/CETATEXT000030866693.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère / 6ème SSR, 10/07/2015, 371469</TITRE>
<DATE_DEC>2015-07-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>371469</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère / 6ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Frédéric Puigserver</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:371469.20150710</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              M. D...C...a demandé au tribunal administratif de Montpellier d'annuler pour excès de pouvoir l'arrêté du 11 juin 2007 par lequel le maire de Lattes (Hérault) a délivré à M. A...B...un permis de construire un bâtiment à usage agricole. Par un jugement n° 0704855 du 31 décembre 2008, le tribunal administratif de Montpellier a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 12MA01163 du 6 juin 2013, statuant sur renvoi du Conseil d'Etat, la cour administrative d'appel de Marseille a, à la demande de M.C..., annulé le jugement du tribunal administratif de Montpellier du 31 décembre 2008 et la décision du 11 juin 2007.<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 20 août 2013, 21 novembre 2013 et 24 février 2015 au secrétariat du contentieux du Conseil d'Etat, la commune de Lattes et M. B... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt de la cour administrative d'appel de Marseille du 6 juin 2013 ;<br/>
<br/>
              2°) de mettre à la charge de M. C...la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative, ainsi que la contribution pour l'aide juridique mentionnée à l'article R. 761-1 du même code.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Puigserver, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de la commune de Lattes, et de M. B...et à la SCP Didier, Pinet, avocat de M. C...;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant, en premier lieu, d'une part, qu'aux termes de l'article R. 222-24 du code de justice administrative, applicable dans les cours administratives d'appel en vertu de l'article R. 222-32 du même code : " Tout rapporteur public absent ou empêché est suppléé de droit par un autre rapporteur public. / A défaut, et si le fonctionnement du tribunal ou de la cour l'exige, ses fonctions sont temporairement exercées par un conseiller ou un premier conseiller désigné par le président du tribunal ou de la cour " ; que, d'autre part, aux termes de l'article R. 721-2 de ce code : " La partie qui veut récuser un juge doit, à peine d'irrecevabilité, le faire dès qu'elle a connaissance de la cause de la récusation. / En aucun cas la demande de récusation ne peut être formée après la fin de l'audience " ; que ces dernières dispositions s'appliquent tant aux membres de la formation de jugement qu'au rapporteur public ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces de la procédure devant la cour administrative d'appel de Marseille que, par une décision du 23 avril 2013, prise sur le fondement des dispositions de l'article R. 222-24 du code de justice administrative, la présidente de la cour, après avoir constaté " l'empêchement de M. Olivier Massin, rapporteur public près la première chambre, et l'impossibilité d'assurer sa suppléance par un autre rapporteur public ", a désigné M. Michaël Revert, premier conseiller, pour assurer les fonctions de rapporteur public dans l'affaire inscrite au rôle de l'audience du 7 mai 2013 ; qu'il ne résulte d'aucun texte ni d'aucune règle générale de procédure que la cour devait, avant l'audience, informer les parties de cette décision ; que la faculté dont disposent les parties de demander la récusation du rapporteur public jusqu'à la fin de l'audience, prévue à l'article R. 721-2 du même code, n'impose pas qu'il soit procédé à une telle information ; que, par suite, les requérants ne sont pas fondés à soutenir que la cour aurait statué au terme d'une procédure irrégulière, faute de les avoir informés avant l'audience du remplacement du rapporteur public ;<br/>
<br/>
              3. Considérant, en second lieu, qu'il ressort des énonciations de l'arrêt attaqué que la cour administrative d'appel de Marseille, après avoir jugé que M. C...était fondé à soutenir que le permis litigieux avait été délivré en violation des dispositions de l'article R. 421-1-2 du code de l'urbanisme, a jugé, pour l'application de l'article L. 600-4-1 du même code, que le moyen tiré de ce que l'autorisation en cause avait été donnée en violation des dispositions de l'article R. 421-2 de ce code fondait également l'annulation du jugement et du permis attaqués ; qu'il ressort toutefois des pièces du dossier soumis aux juges du fond que si M. C..., qui était le demandeur en première instance, avait soulevé un tel moyen devant le tribunal administratif, qui l'avait expressément écarté par le jugement attaqué, il ne l'avait pas repris dans sa requête d'appel ; qu'en examinant ce moyen, qui n'est pas d'ordre public, au titre de l'effet dévolutif de l'appel, la cour administrative d'appel de Marseille a méconnu son office ; qu'elle a ainsi commis une erreur de droit ;<br/>
<br/>
              4. Mais considérant, d'une part, qu'aux termes de l'article R. 421-1-2 du code de l'urbanisme, en vigueur à la date de délivrance du permis litigieux : " Conformément à l'article 1er du décret n° 77-190 du 3 mars 1977 modifié, ne sont pas tenues de recourir à un architecte pour établir le projet architectural à joindre à la demande d'autorisation de construire les personnes physiques qui déclarent vouloir édifier ou modifier pour elles-mêmes : / (...) / b) Une construction à usage agricole dont la surface de plancher hors oeuvre brute n'excède pas 800 mètres carrés (...) " ; que, d'autre part, aux termes de l'article R. 112-2 du même code, dans sa rédaction applicable au litige : " La surface de plancher hors oeuvre brute d'une construction est égale à la somme des surfaces de plancher de chaque niveau de la construction (...) " ;<br/>
<br/>
              5. Considérant qu'après avoir relevé que le pétitionnaire avait déclaré que les travaux autorisés par l'arrêté en litige entraîneraient une augmentation de 57 mètres carrés de la surface hors oeuvre brute (SHOB) d'une construction à usage agricole de 500 mètres carrés, la cour a jugé qu'il ressortait des pièces du dossier qui lui était soumis que cette construction reposait sur un vide sanitaire d'une même superficie, qui, compte tenu de sa hauteur de 1,90 mètre, de ses accès par des portails et de son affectation déclarée comme lieu de stockage éventuel, constituait une surface de plancher agricole à prendre en compte pour le calcul de la SHOB du bâtiment ; qu'en statuant ainsi, elle n'a pas dénaturé les pièces du dossier et ne s'est pas méprise sur la portée des écritures du requérant, qui s'était expressément prévalu devant elle de la méconnaissance des dispositions de l'article R. 421-1-2 du code de l'urbanisme ; qu'elle a pu déduire de ses constatations, sans commettre d'erreur de droit, que les travaux litigieux portaient sur une construction à usage agricole dont la surface de plancher hors oeuvre brute excédait 800 mètres carrés et que la demande de permis de construire devait dès lors être présentée par un architecte ; <br/>
<br/>
              6. Considérant que le moyen ainsi retenu est à lui seul de nature à justifier le dispositif de l'arrêt attaqué ; que, par suite, les requérants ne sont pas fondés à demander son annulation ;<br/>
<br/>
              7. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. B...une somme de 3 000 euros à verser à M.C... ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. C...qui n'est pas, dans la présente instance, la partie perdante ; que, dans les circonstances de l'espèce, il y a lieu de laisser la contribution pour l'aide juridique à la charge de la commune de Lattes et de M.B... ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la commune de Lattes et de M. B...est rejeté.<br/>
Article 2 : M. B...versera à M. C...une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la commune de Lattes, à M. A...B...et à M. D... C....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-04-03 PROCÉDURE. INSTRUCTION. CARACTÈRE CONTRADICTOIRE DE LA PROCÉDURE. - REMPLACEMENT D'UN RAPPORTEUR PUBLIC EMPÊCHÉ - OBLIGATION D'INFORMATION DES PARTIES - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-06-02 PROCÉDURE. JUGEMENTS. TENUE DES AUDIENCES. - REMPLACEMENT D'UN RAPPORTEUR PUBLIC EMPÊCHÉ - OBLIGATION D'INFORMATION DES PARTIES - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-08-01-04-01 PROCÉDURE. VOIES DE RECOURS. APPEL. EFFET DÉVOLUTIF ET ÉVOCATION. EFFET DÉVOLUTIF. - ETENDUE - POSSIBILITÉ D'EXAMINER UN MOYEN INVOQUÉ PAR LE DEMANDEUR EN PREMIÈRE INSTANCE ET ÉCARTÉ PAR LE TRIBUNAL ADMINISTRATIF MAIS NON REPRIS EN APPEL - ABSENCE.
</SCT>
<ANA ID="9A"> 54-04-03 Le président d'une cour administrative d'appel a, sur le fondement des dispositions de l'article R. 222-24 du code de justice administrative, après avoir constaté l'empêchement d'un rapporteur public et l'impossibilité d'assurer sa suppléance par un autre rapporteur public, désigné un magistrat de la juridiction pour assurer les fonctions de rapporteur public dans une affaire. Il ne résulte d'aucun texte ni d'aucune règle générale de procédure que la cour devait, avant l'audience, informer les parties de cette décision. La faculté dont disposent les parties de demander la récusation du rapporteur public jusqu'à la fin de l'audience, prévue à l'article R. 721-2 du même code, n'impose pas qu'il soit procédé à une telle information.</ANA>
<ANA ID="9B"> 54-06-02 Le président d'une cour administrative d'appel a, sur le fondement des dispositions de l'article R. 222-24 du code de justice administrative, après avoir constaté l'empêchement d'un rapporteur public et l'impossibilité d'assurer sa suppléance par un autre rapporteur public, désigné un magistrat de la juridiction pour assurer les fonctions de rapporteur public dans une affaire. Il ne résulte d'aucun texte ni d'aucune règle générale de procédure que la cour devait, avant l'audience, informer les parties de cette décision. La faculté dont disposent les parties de demander la récusation du rapporteur public jusqu'à la fin de l'audience, prévue à l'article R. 721-2 du même code, n'impose pas qu'il soit procédé à une telle information.</ANA>
<ANA ID="9C"> 54-08-01-04-01 Un requérant, qui était le demandeur en première instance, avait soulevé un moyen devant le tribunal administratif, qui l'avait expressément écarté par son jugement, mais ne l'avait pas repris dans sa requête d'appel. En examinant ce moyen, qui n'est pas d'ordre public, au titre de l'effet dévolutif de l'appel, la cour administrative d'appel a méconnu son office et ainsi commis une erreur de droit.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
