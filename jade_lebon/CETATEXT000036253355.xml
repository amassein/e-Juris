<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036253355</ID>
<ANCIEN_ID>JG_L_2017_12_000000396396</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/25/33/CETATEXT000036253355.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 22/12/2017, 396396</TITRE>
<DATE_DEC>2017-12-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>396396</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU ; SCP BORE, SALVE DE BRUNETON, MEGRET</AVOCATS>
<RAPPORTEUR>Mme Séverine Larere</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Yohann Bénard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:396396.20171222</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Highlands Technologies a demandé au tribunal administratif de Montreuil  d'annuler la décision du 18 octobre 2012 par laquelle la direction générale des douanes et droits indirects a révoqué dix décisions du 25 juillet 2012 valant renseignements tarifaires contraignants. Par une ordonnance n° 1401605 du 7 mars 2014 le président de la 3ème chambre du tribunal administratif de Montreuil a rejeté sa demande comme portée devant une juridiction manifestement incompétente pour en connaître.<br/>
<br/>
              Par un arrêt n° 14VE00910 du 31 décembre 2015, la cour administrative d'appel de Versailles a rejeté l'appel formé par la société Highlands Technologies contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 25 janvier 2016 et 25 avril 2016 au secrétariat du contentieux du Conseil d'Etat, la société Highlands Technologies demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le règlement (CEE) n° 2913/92 du Conseil, du 12 octobre 1992 ;<br/>
              - le code des douanes ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Séverine Larere, maître des requêtes,<br/>
<br/>
              - les conclusions de M. Yohann Bénard, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de la société Highlands Technologies ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Highlands Technologies, spécialisée dans l'importation de matériel destiné à l'enregistrement de fichiers vidéophoniques, a déposé le 19 juin 2012 auprès de l'administration des douanes une demande de renseignements tarifaires contraignants concernant dix produits. Par dix décisions du 25 juillet 2012 valant renseignements tarifaires contraignants, la direction générale des douanes et droits indirects lui a indiqué que ces produits étaient classés à la position tarifaire 8471490090. Toutefois, par une décision du 18 octobre 2012, l'administration, estimant s'être trompée dans la classification retenue, a " révoqué " ces renseignements tarifaires contraignants et a indiqué qu'ils relevaient de la position tarifaire 85219000. La société Highlands Technologies a contesté cette décision devant le tribunal administratif de Montreuil. Par une ordonnance du 7 mars 2014, le président de la 3ème chambre de ce tribunal a rejeté sa demande comme portée devant une juridiction manifestement incompétente pour en connaître. La société se pourvoit en cassation contre l'arrêt du 31 décembre 2015 par lequel la cour administrative d'appel de Versailles a rejeté son appel contre ce jugement.<br/>
<br/>
              2. Aux termes, d'une part, de l'article 12 du règlement (CEE) n° 2913/92 du Conseil, du 12 octobre 1992, établissant le code des douanes communautaire, applicable à la date de la décision attaquée : " 1. Les autorités douanières délivrent, sur demande écrite et selon des modalités déterminées selon la procédure du comité, des renseignements tarifaires contraignants. / 2. Le renseignement tarifaire contraignant ne lie les autorités douanières vis-à-vis du titulaire que pour le classement tarifaire d'une marchandise. / Le renseignement tarifaire contraignant ne lie les autorités douanières qu'à l'égard des marchandises pour lesquelles les formalités douanières sont accomplies postérieurement à la date de sa délivrance par lesdites autorités. / (...)./ 4. Un renseignement tarifaire contraignant est valable six ans à compter de la date de sa délivrance. (...)/ 5. Un renseignement tarifaire contraignant cesse d'être valable lorsque :/ (...) c) la révocation ou la modification du renseignement tarifaire contraignant est notifiée au titulaire. (...) 6. Le titulaire d'un renseignement tarifaire contraignant qui cesse d'être valable conformément au paragraphe 5 points b) ou c) peut continuer à s'en prévaloir pendant une période de six mois après cette publication ou cette notification (...) / 7. L'application, dans les conditions prévues au paragraphe 6, du classement figurant dans le renseignement tarifaire contraignant n'a d'effet qu'à l'égard: / - de la détermination des droits à l'importation ou à l'exportation (...)". <br/>
<br/>
              3. Aux termes, d'autre part, de l'article 357 bis du code des douanes, dans sa rédaction applicable au litige : " Les tribunaux d'instance connaissent des contestations concernant le paiement, la garantie ou le remboursement des créances de toute nature recouvrées par l'administration des douanes et des autres affaires de douane n'entrant pas dans la compétence des juridictions répressives. ". <br/>
<br/>
              4. En premier lieu, il résulte des dispositions précitées de l'article 12 du code des douanes communautaire que le renseignement tarifaire contraignant lie les autorités douanières quant au classement tarifaire de la marchandise concernée, lequel permet, combiné à l'origine de cette marchandise, de déterminer le tarif des droits de douane qui lui est applicable. Par suite, c'est sans erreur de droit ni erreur de qualification juridique des faits que la cour a jugé, d'une part, que la délivrance d'un renseignement tarifaire contraignant devait être regardée comme servant à la détermination des droits de douane à acquitter et, d'autre part, et en conséquence, que la révocation d'un renseignement tarifaire, qui s'accompagne d'un nouveau classement tarifaire, avait une incidence sur la détermination de ces droits.<br/>
<br/>
              5. En deuxième lieu, contrairement à ce que soutient la société requérante, la cour n'a pas jugé que la décision de révocation du 18 octobre 2012 entraînait une exonération des droits de douane pour la société Highlands Technologies mais seulement que les renseignements tarifaires délivrés par les décisions du 25 juillet 2012, " révoquées " par celle du 18 octobre suivant, conduisaient à une exonération de ces mêmes droits. En statuant ainsi, la cour n'a ni dénaturé les pièces du dossier ni commis d'erreur de droit et de qualification juridique.<br/>
<br/>
              6. Enfin, il résulte de ce qui a été dit au point 4 ci-dessus que la délivrance et la révocation des renseignements tarifaires contraignants concourent à la détermination des droits de douanes et constituent, dès lors, des " affaires de douane ", au sens de l'article 357 bis précité du code des douanes. Par suite, la cour n'a commis ni erreur de droit ni erreur de qualification juridique des faits en jugeant que la contestation formée par la société Highlands Technologies à l'encontre de la décision du 18 octobre 2012 révoquant les décisions du 25 juillet 2012 valant renseignements tarifaires contraignants relevait de la compétence de la juridiction judiciaire.<br/>
<br/>
              7. Il résulte de ce qui précède que la société Highlands Technologies n'est pas fondée à demander l'annulation de l'arrêt attaqué ni, par suite, qu'une somme soit mise à la charge de l'Etat au titre de l'article L. 761-1 du code de justice administrative. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à sa charge le versement d'une somme au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de  la société Highlands Technologies est rejeté.<br/>
Articles 2 : Les conclusions présentées par l'Etat au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à la Sociétés Highlands Technologies et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-03-01-02-03 COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE LES DEUX ORDRES DE JURIDICTION. COMPÉTENCE DÉTERMINÉE PAR DES TEXTES SPÉCIAUX. ATTRIBUTIONS LÉGALES DE COMPÉTENCE AU PROFIT DES JURIDICTIONS JUDICIAIRES. COMPÉTENCE DES JURIDICTIONS JUDICIAIRES EN MATIÈRE FISCALE ET PARAFISCALE. - CONTESTATIONS RELATIVES À L'ASSIETTE ET AU RECOUVREMENT DES DROITS DE DOUANE (ART. 357 BIS DU CODE DES DOUANES) [RJ1] - NOTION - CONTESTATION DIRIGÉE CONTRE UNE DÉCISION DE RÉVOCATION D'UNE DÉCISION VALANT RENSEIGNEMENT TARIFAIRE CONTRAIGNANT - INCLUSION.
</SCT>
<ANA ID="9A"> 17-03-01-02-03 Le renseignement tarifaire contraignant lie les autorités douanières quant au classement tarifaire de la marchandise concernée, lequel permet, combiné à l'origine de cette marchandise, de déterminer le tarif des droits de douane qui lui est applicable. Par suite, la délivrance d'un renseignement tarifaire contraignant doit être regardée comme servant à la détermination des droits de douane à acquitter et, en conséquence, la révocation d'un renseignement tarifaire, qui s'accompagne d'un nouveau classement tarifaire, a une incidence sur la détermination de ces droits.,,,Il en résulte que la délivrance et la révocation des renseignements tarifaires contraignants concourent à la détermination des droits de douanes et constituent, dès lors, des affaires de douane, au sens de l'article 357 bis du code des douanes. Par suite, la contestation formée par une société à l'encontre d'une décision de révocation d'une décision valant renseignement tarifaire contraignant relève de la compétence de la juridiction judiciaire.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. TC, 1er juillet 2002, Société Pinault Bretagne et Cie, n° 3295, p. 546 ; TC, 16 mars 1998,,, n° 3053, p. 536.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
