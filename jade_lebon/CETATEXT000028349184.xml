<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028349184</ID>
<ANCIEN_ID>JG_L_2013_12_000000360889</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/34/91/CETATEXT000028349184.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 18/12/2013, 360889</TITRE>
<DATE_DEC>2013-12-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>360889</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Tristan Aureau</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:360889.20131218</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 9 juillet 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par la société CSF France, dont le siège est ZI, route de Paris, à Mondeville (14120) ; la société CSF France demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision n° 1265 T du 4 avril 2012 par laquelle la Commission nationale d'aménagement commercial a rejeté son recours tendant à l'annulation de la décision du 17 novembre 2011 de la commission départementale d'aménagement commercial de Haute-Garonne accordant à la SARL Progagne l'autorisation préalable requise en vue de procéder à la création d'un ensemble commercial de 2 800 m² de surface de vente comprenant un hypermarché à l'enseigne Super U d'une surface de 2 500 m² et cinq boutiques d'une surface totale de vente de 300 m², sur le territoire de Gagnac-sur-Garonne (Haute-Garonne) ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le code de commerce ;<br/>
<br/>
              Vu la loi n° 73-1193 du 27 décembre 1973 ;<br/>
<br/>
              Vu le décret du 24 octobre 2011 portant nomination à la Commission nationale d'aménagement commercial ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Tristan Aureau, Auditeur,  <br/>
<br/>
              - les conclusions de M. Xavier Domino, Rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant que la requête de la société CSF France est dirigée contre la décision du 4 avril 2012 par laquelle la Commission nationale d'aménagement commercial a rejeté son recours exercé contre la décision de la commission départementale d'aménagement commercial de Haute-Garonne, accordant à la SARL Progagne l'autorisation préalable requise en vue de procéder à la création d'un ensemble commercial de 2 800 m² de surface de vente comprenant un hypermarché à l'enseigne Super U d'une surface de 2 500 m² et cinq boutiques d'une surface totale de vente de 300 m², sur le territoire de Gagnac-sur-Garonne ;<br/>
<br/>
              Sur la légalité externe de la décision attaquée :<br/>
<br/>
              2.	Considérant qu'aux termes de l'article L. 751-6 du code de commerce, la Commission nationale d'aménagement commercial se compose notamment de : " (...) 1° Un membre du Conseil d'Etat désigné par le vice-président du Conseil d'Etat ; 2° Un membre de la Cour des comptes désigné par le premier président de la Cour des comptes ; 3° Un membre de l'inspection générale des finances désigné par le chef de ce service ; (...) " ; que l'article R. 751-8 du même code dispose que : " Le président de la Commission nationale d'aménagement commercial est suppléé, en cas d'absence ou d'empêchement, par le membre de la Cour des comptes et, en cas d'absence ou d'empêchement de celui-ci, par le membre de l'inspection générale des finances " et qu'aux termes du dernier alinéa de l'article R. 751-9 du même code : " Pour chacun des membres hormis le président, un suppléant est nommé dans les mêmes conditions que celles de désignation du membre titulaire " ; qu'il résulte de ces dispositions qu'en cas d'absence ou d'empêchement du président de la Commission nationale d'aménagement commercial, la présidence de la séance est assurée par le membre titulaire de la Commission désigné par le premier président de la Cour des Comptes et, en cas d'absence ou d'empêchement de ce dernier, par le membre titulaire de la commission désigné par le chef de l'inspection générale des finances ; qu'il ressort du procès-verbal de la séance du 4 avril 2012 que le président de la Commission était absent ; que, par ailleurs, le mandat du membre titulaire de la Cour des comptes étant, à cette date, expiré, le membre de la Cour des comptes présent lors de cette séance était celui qui avait été nommé par décret du 24 octobre 2011 en qualité de suppléant ; que c'est, dès lors, à bon droit que la présidence de la réunion du 4 avril 2012 a été assurée par le membre titulaire de la Commission désigné par le chef de l'inspection générale des finances ; que la circonstance que l'absence du président et du membre titulaire de la Cour des comptes n'ait pas expressément été mentionnée dans la décision est sans incidence sur la légalité de celle-ci ;<br/>
<br/>
              3.	Considérant que si, eu égard à la nature, à la composition et aux attributions de la Commission nationale d'aménagement commercial, les décisions qu'elle prend doivent être motivées, cette obligation n'implique pas que la Commission soit tenue de prendre explicitement parti sur le respect, par le projet qui lui est soumis, de chacun des objectifs et des critères d'appréciation fixés par les dispositions législatives applicables ; qu'en l'espèce, la Commission nationale a suffisamment satisfait à cette obligation, notamment en ce qui concerne l'évolution de la population de la zone de la chalandise, l'implantation du projet dans une zone destinée à l'urbanisation, la desserte du site prévue tant par les transports en commun que les modes de déplacement doux et la qualité environnementale du projet ; qu'ainsi, le moyen tiré d'une motivation insuffisante de la décision attaquée doit être écarté ;<br/>
<br/>
              Sur la légalité interne de la décision attaquée :<br/>
<br/>
              4.	Considérant, d'une part, qu'aux termes du troisième alinéa de l'article 1er de la loi du 27 décembre 1973 : " Les pouvoirs publics veillent à ce que l'essor du commerce et de l'artisanat permette l'expansion de toutes les formes d'entreprises, indépendantes, groupées ou intégrées, en évitant qu'une croissance désordonnée des formes nouvelles de distribution ne provoque l'écrasement de la petite entreprise et le gaspillage des équipements commerciaux et ne soit préjudiciable à l'emploi " ; qu'aux termes de l'article L. 750-1 du code de commerce  : " Les implantations, extensions, transferts d'activités existantes et changements de secteur d'activité d'entreprises commerciales et artisanales doivent répondre aux exigences d'aménagement du territoire, de la protection de l'environnement et de la qualité de l'urbanisme. Ils doivent en particulier contribuer au maintien des activités dans les zones rurales et de montagne ainsi qu'au rééquilibrage des agglomérations par le développement des activités en centre-ville et dans les zones de dynamisation urbaine. Dans le cadre d'une concurrence loyale, ils doivent également contribuer à la modernisation des équipements commerciaux, à leur adaptation à l'évolution des modes de consommation et des techniques de commercialisation, au confort d'achat du consommateur et à l'amélioration des conditions de travail des salariés " ;<br/>
<br/>
              5.	Considérant, d'autre part, qu'aux termes de l'article L. 752-6 du même code : " Lorsqu'elle statue sur l'autorisation d'exploitation commerciale visée à l'article L. 752-1, la commission départementale d'aménagement commercial se prononce sur les effets du projet en matière d'aménagement du territoire, de développement durable et de protection des consommateurs. Les critères d'évaluation sont : 1° En matière d'aménagement du territoire : a) L'effet sur l'animation de la vie urbaine, rurale et de montagne ; b) L'effet du projet sur les flux de transport ; c) Les effets découlant des procédures prévues aux articles L. 303-1 du code de la construction et de l'habitation et L. 123-11 du code de l'urbanisme ; 2° En matière de développement durable :  a) La qualité environnementale du projet ; b) Son insertion dans les réseaux de transports collectifs " ; <br/>
<br/>
              6.	Considérant qu'il résulte de ces dispositions combinées que l'autorisation d'aménagement commercial ne peut être refusée que si, eu égard à ses effets, le projet contesté compromet la réalisation des objectifs énoncés par la loi ; qu'il appartient aux commissions d'aménagement commercial, lorsqu'elles statuent sur les dossiers de demande d'autorisation, d'apprécier la conformité du projet à ces objectifs, au vu des critères d'évaluation mentionnés à l'article L. 752-6 du code de commerce ;<br/>
<br/>
              7.	Considérant que, pour apprécier la conformité à ces dispositions du projet litigieux, la Commission nationale d'aménagement commercial a relevé que la population de la zone de chalandise du projet avait connu un accroissement significatif ; que le projet sera implanté dans une zone en cours d'urbanisation, qu'il contribuera à structurer ; qu'il sera desservi par les transports en commun et sera accessible par des voies cyclables et piétonnes ; qu'enfin, il respectera les normes de régulation thermique en vigueur et comportera des espaces verts étendus ; <br/>
<br/>
              8.	Considérant, s'agissant du critère d'aménagement du territoire, que la Commission a pu relever que la population de la zone de chalandise du projet avait connu un accroissement de 29 % en dix ans, pour apprécier l'effet du projet sur l'animation de la vie urbaine de la zone ; qu'il ressort, par ailleurs, des pièces du dossier que le projet s'intègre dans le programme d'urbanisation d'un secteur situé à l'est de la commune de Gagnac-sur-Garonne, prévu par le projet d'aménagement et de développement durable présenté dans le cadre de la procédure de révision du plan d'occupation des sols de la communauté urbaine du Grand Toulouse, et pour lequel des aménagements des accès routiers ainsi que la création de voies dédiées aux piétons et aux cyclistes sont prévus ; que des travaux d'aménagement de la route départementale 63, de nature à réguler le flux de transport que suscitera l'implantation de l'ensemble commercial, ont été prévus par la commune de Gagnac-sur-Garonne et le conseil général de Haute-Garonne ;<br/>
<br/>
              9.	Considérant, s'agissant du critère de développement durable, qu'il ressort des pièces du dossier que le site du projet, situé à quelques centaines de mètres du centre-ville de la commune de Gagnac-sur-Garonne, est desservi par une ligne de bus du réseau de la communauté urbaine du Grand Toulouse ; que des accès sécurisés pour les piétons et les cyclistes ont été prévus ; que la société pétitionnaire a prévu diverses mesures, notamment d'isolation, de nature à réduire la consommation énergétique du site ; <br/>
<br/>
              10.	Considérant que la circonstance que le projet se situe dans une zone incluse dans un plan de prévention des risques d'inondation est, par elle-même, sans influence sur la légalité de la décision prise par la Commission ;<br/>
<br/>
              11.	Considérant qu'au regard de l'ensemble de ces éléments, la Commission nationale d'aménagement commercial n'a pas fait une inexacte application des dispositions rappelées ci-dessus en rejetant le recours exercé par la société requérante contre la décision de la commission départementale d'aménagement commercial de Haute-Garonne ;<br/>
<br/>
              12.	Considérant qu'il résulte de tout ce qui précède que la société CSF France n'est pas fondée à demander l'annulation de la décision attaquée ;<br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              13.	Considérant que ces dispositions font obstacle à ce que soit mis à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance, le versement de la somme que demande la société requérante sur leur fondement ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de la société CSF France le versement de la somme 4 000 euros à la SARL Progagne, au titre des frais exposés par elle et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de la société CSF France est rejetée.<br/>
<br/>
Article 2 : La société CSF France versera la somme de 4 000 euros à la SARL Progagne au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la société CSF France, à la SARL Progagne et à la Commission nationale d'aménagement commercial.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">14-02-01-05-02-02 COMMERCE, INDUSTRIE, INTERVENTION ÉCONOMIQUE DE LA PUISSANCE PUBLIQUE. RÉGLEMENTATION DES ACTIVITÉS ÉCONOMIQUES. ACTIVITÉS SOUMISES À RÉGLEMENTATION. AMÉNAGEMENT COMMERCIAL. PROCÉDURE. COMMISSION NATIONALE D`AMÉNAGEMENT COMMERCIAL. - RÈGLES DE SUPPLÉANCE - ABSENCE OU EMPÊCHEMENT DU PRÉSIDENT - PRÉSIDENCE ASSURÉE PAR LE MEMBRE TITULAIRE DE LA CNAC DÉSIGNÉ PAR LE PREMIER PRÉSIDENT DE LA COUR DES COMPTES - ABSENCE OU EMPÊCHEMENT DE CE DERNIER - PRÉSIDENCE ASSURÉE PAR LE MEMBRE TITULAIRE DE LA CNAC DÉSIGNÉ PAR LE CHEF DE L'IGF.
</SCT>
<ANA ID="9A"> 14-02-01-05-02-02 Il résulte des dispositions de l'article L. 751-6 du code de commerce qu'en cas d'absence ou d'empêchement du président de la Commission nationale d'aménagement commercial (CNAC), la présidence de la séance est assurée par le membre titulaire de la commission désigné par le premier président de la Cour des comptes. En cas d'absence ou d'empêchement de ce dernier, la présidence est assurée par le membre titulaire de la commission désigné par le chef de l'inspection générale des finances (IGF).</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
