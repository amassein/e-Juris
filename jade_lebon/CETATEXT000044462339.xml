<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044462339</ID>
<ANCIEN_ID>JG_L_2021_12_000000447044</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/46/23/CETATEXT000044462339.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 08/12/2021, 447044</TITRE>
<DATE_DEC>2021-12-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>447044</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. David Moreau</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Arnaud Skzryerbak</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:447044.20211208</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. G... H... a demandé à la Cour nationale du droit d'asile d'annuler la décision du 31 mai 2018 par laquelle l'Office français de protection des réfugiés et apatrides (OFPRA) a rejeté sa demande d'asile.  <br/>
<br/>
              Par une décision n° 18031582 du 29 septembre 2020, la Cour nationale du droit d'asile a reconnu la qualité de réfugié à M. H....<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 30 novembre 2020, 1er mars et 23 novembre 2021 au secrétariat du contentieux du Conseil d'Etat, l'Office français de protection des réfugiés et apatrides demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) de renvoyer l'affaire à la Cour nationale du droit d'asile.<br/>
<br/>
              L'OFPRA soutient que la Cour nationale du droit d'asile a entaché sa décision :<br/>
<br/>
              - d'irrégularité faute d'avoir visé, analysé et tenu compte de son mémoire du 13 janvier 2020 ;<br/>
<br/>
              - d'erreur de droit en écartant l'application de la clause d'exclusion prévue au b) de l'article 1er F de la convention de Genève au seul motif que les poursuites engagées à l'encontre de M. H... par les autorités kazakhes reposaient sur un mobile politique sans rechercher si, nonobstant ce mobile, ces poursuites étaient justifiées par des considérations juridiques civiles ou pénales, au vu des éléments du dossier ;<br/>
              - d'erreur de droit et d'insuffisance de motivation en ne se prononçant pas sur la force probante particulière des décisions civiles rendues par les juridictions britanniques, telle qu'elle résulte du règlement (CE) n°1215/2012 du Conseil du 12 décembre 2012 concernant la compétence judiciaire, la reconnaissance et l'exécution des décisions en matière civile et commerciale, dit " Bruxelles I " ;<br/>
              - d'erreur de qualification juridique des faits ou, à tout le moins, de dénaturation des pièces du dossier en écartant l'application de la clause d'exclusion du b) de l'article 1er F de la convention de Genève alors même qu'il ressortait des pièces du dossier que M. H... était personnellement impliqué dans une affaire de fraude et de détournements de fonds massifs.<br/>
<br/>
<br/>
Vu les autres pièces du dossier ;<br/>
Vu :<br/>
- la convention de Genève du 28 juillet 1951 et le protocole signé à New-York le 31 janvier 1967 relatifs aux réfugiés ;<br/>
- le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
- le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. David Moreau, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Arnaud Skzryerbak, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Foussard, Froger, avocat de l'OFPRA et à la SCP Waquet, Farge, Hazan, avocat de M. G... H... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Par une décision du 31 mai 2018, l'Office français de protection des réfugiés et apatrides a rejeté la demande d'asile présentée par M. H..., ressortissant du Kazakhstan, au motif qu'il y avait des raisons sérieuses de penser qu'il s'était rendu coupable d'un crime grave de droit commun au sens de la clause d'exclusion du b) de l'article 1er F de la convention de Genève relative aux réfugiés. L'OFPRA se pourvoit en cassation contre la décision du 29 septembre 2020 par laquelle la Cour nationale du droit d'asile a reconnu la qualité de réfugié à M. H... à raison du risque de persécutions encouru en cas de retour dans son pays d'origine du fait de son engagement politique et a jugé qu'il n'entrait pas dans le champ d'une des clauses d'exclusion prévues par la convention de Genève. <br/>
<br/>
              2. Le paragraphe A, 2° de l'article 1er de la convention de Genève du 28 juillet 1951 relative aux réfugiés stipule que doit être considérée comme réfugié toute personne qui " craignant avec raison d'être persécutée du fait de sa race, de sa religion, de sa nationalité, de son appartenance à un certain groupe social ou de ses opinions politiques, se trouve hors du pays dont elle a la nationalité et qui ne peut ou, du fait de cette crainte, ne veut se réclamer de la protection de ce pays ". Aux termes des stipulations de l'article 1 F de la même convention : " Les dispositions de cette Convention ne seront pas applicables aux personnes dont on aura des raisons sérieuses de penser : (...)  b) qu'elles ont commis un crime grave de droit commun en dehors du pays d'accueil avant d'y être admises comme réfugiées (...) ". <br/>
<br/>
              3. Lorsqu'il existe des raisons sérieuses de penser qu'un demandeur d'asile a commis un crime grave de droit commun en dehors du pays d'accueil, les stipulations du b) de l'article 1 F de la convention de Genève, citées au point 2, permettent de lui refuser le bénéfice de la protection statutaire même si les poursuites dont il a pu faire l'objet à raison de cet acte criminel reposent sur un mobile politique. Des faits de détournement de fonds, d'escroquerie ou de corruption qui revêtent une grande ampleur ont le caractère de crime grave de droit commun. <br/>
<br/>
              4. Il ressort des pièces du dossier soumis à la Cour nationale du droit d'asile que M. H..., qui a dirigé la compagnie d'électricité du Kazakhstan et a été ministre de l'énergie, du commerce et de l'industrie de ce pays en 1998 et 1999, était détenteur de 75 % des parts de la première banque commerciale du pays, la BTA, qu'il avait acquises en 1998 avant de les revendre en 2002 à la suite de sa condamnation pour abus de pouvoir, détournement de fonds et évasion fiscale dans l'exercice de ses fonctions ministérielles. Ayant bénéficié d'une grâce présidentielle en mai 2003, il est devenu, en février 2005, président du conseil d'administration de la banque BTA. Il a exercé ces fonctions jusqu'à son départ à Londres en janvier 2009. Le 4 février 2009, cette banque a été nationalisée. Alors que M. H... a fait l'objet, dans son pays, d'une enquête pénale pour détournement de fonds de la banque BTA, cette dernière a engagé des actions civiles à son encontre devant les juridictions britanniques. La High Court a jugé fondée cette action et a condamné M. H... à payer à la banque BTA la somme de 4,6 milliards de dollars à raison des mécanismes frauduleux qu'il avait mis en place lorsqu'il dirigeait la banque afin de s'enrichir personnellement. Pour ce même motif, les autorités britanniques lui ont retiré le statut de réfugié. Il résulte de l'ensemble de ces éléments que les détournements de fond, dont il existe des raisons sérieuses de penser que M. H... est l'auteur, étaient dépourvus de tout mobile politique. Par suite, alors même qu'elle a relevé que les poursuites engagées à l'encontre de M. H... par la banque BTA et les autorités de son pays poursuivaient un but politique à raison de son opposition au pouvoir en place, la cour a entaché sa décision d'erreur de qualification juridique des faits en jugeant qu'il n'existait pas de raisons sérieuses de penser que M. H... se serait rendu coupable d'un crime grave de droit commun au sens et pour l'application de la clause d'exclusion prévue au b) de l'article 1er F de la convention de Genève.  <br/>
<br/>
              5. Il résulte de tout ce qui précède que, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, l'Office français de protection des réfugiés et apatrides est fondé à demander l'annulation de la décision qu'il attaque. Les conclusions présentées par M. H... au titre de l'article L. 761-1 du code de justice administrative ne peuvent dès lors, en tout état de cause, qu'être rejetées. <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
     --------------<br/>
<br/>
Article 1er : La décision du 29 septembre 2020 de la Cour nationale du droit d'asile est annulée.<br/>
Article 2 : L'affaire est renvoyée à la Cour nationale du droit d'asile.<br/>
Article 3 : Les conclusions présentées par M. H... au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 4 : La présente décision sera notifiée à l'Office français de protection des réfugiés et apatrides et à M. G... H....<br/>
              Délibéré à l'issue de la séance du 29 novembre 2021 où siégeaient : M. Rémy Schwartz, président adjoint de la section du contentieux présidant ; M. I... F..., M. Frédéric Aladjidi, présidents de chambre ; Mme K... D..., M. L... E..., Mme A... M..., M. B... C..., M. Bruno Delsol, conseillers d'Etat et M. David Moreau, maître des requêtes-rapporteur. <br/>
<br/>
              Rendu le 8 décembre 2021<br/>
<br/>
<br/>
                 Le Président : <br/>
                 Signé : M. Rémy Schwartz<br/>
 		Le rapporteur : <br/>
      Signé : M. David Moreau<br/>
                 La secrétaire :<br/>
     Signé : Mme J... N...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">095-04-01-01-02-03 - CRIME GRAVE DE DROIT COMMUN - 1) NOTION - A) CIRCONSTANCE SANS INCIDENCE - POURSUITES REPOSANT SUR UN MOBILE POLITIQUE - B) INCLUSION - INFRACTION FINANCIÈRE DE GRANDE AMPLEUR [RJ1] - 2) ESPÈCE.
</SCT>
<ANA ID="9A"> 095-04-01-01-02-03 Lorsqu'il existe des raisons sérieuses de penser qu'un demandeur d'asile a commis un crime grave de droit commun en dehors du pays d'accueil, le b de l'article 1er F de la convention de Genève relative aux réfugiés permet de lui refuser le bénéfice de la protection statutaire.......1) a) Il en va ainsi même si les poursuites dont il a pu faire l'objet à raison de cet acte criminel reposent sur un mobile politique.......b) Des faits de détournement de fonds, d'escroquerie ou de corruption qui revêtent une grande ampleur ont le caractère de crime grave de droit commun.......2) Demandeur d'asile, ex-dirigeant de la compagnie d'électricité de son pays et ex-ministre de l'énergie, du commerce et de l'industrie de celui-ci en 1998 et 1999, détenteur de la majorité des parts de la première banque commerciale du pays, qu'il avait acquises en 1998, les ayant revendues en 2002 à la suite de sa condamnation pour abus de pouvoir, détournement de fonds et évasion fiscale dans l'exercice de ses fonctions ministérielles avant de bénéficier d'une grâce présidentielle en mai 2003.......Demandeur d'asile, devenu, en février 2005, président du conseil d'administration de cette banque jusqu'à son départ à Londres en janvier 2009 et la nationalisation de la banque en février, ayant fait l'objet, dans son pays, d'une enquête pénale pour détournement de fonds de la banque, cette dernière ayant engagé des actions civiles à son encontre devant les juridictions britanniques. ......High Court of Justice ayant jugé fondée cette action et l'ayant condamné à payer à la banque la somme de 4,6 milliards de dollars à raison des mécanismes frauduleux qu'il avait mis en place lorsqu'il dirigeait la banque afin de s'enrichir personnellement. ......Pour le même motif, statut de réfugié lui ayant été retiré par les autorités britanniques. ......Il résulte de l'ensemble de ces éléments que les détournements de fond, dont il existe des raisons sérieuses de penser que l'intéressé est l'auteur, sont dépourvus de tout mobile politique.......Par suite, alors même que les poursuites engagées à son encontre par la banque et les autorités de son pays poursuivent un but politique à raison de son opposition au pouvoir en place, il existe des raisons sérieuses de penser que le demandeur d'asile s'est rendu coupable d'un crime grave de droit commun au sens et pour l'application de la clause d'exclusion prévue au b) de l'article 1er F de la convention de Genève.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., pour la mise en œuvre de la protection subsidiaire, sur la possibilité de qualifier de crime grave de droit commun une infraction constitutive d'un délit en droit interne, CE, 13 novembre 2020, M. Vukaj, n° 428582, T. pp. 612-956.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
