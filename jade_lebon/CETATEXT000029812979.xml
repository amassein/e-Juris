<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029812979</ID>
<ANCIEN_ID>JG_L_2014_11_000000370579</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/81/29/CETATEXT000029812979.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème / 1ère SSR, 28/11/2014, 370579</TITRE>
<DATE_DEC>2014-11-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>370579</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème / 1ère SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARTHELEMY, MATUCHANSKY, VEXLIARD, POUPOT</AVOCATS>
<RAPPORTEUR>M. Jean-Baptiste de Froment</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:370579.20141128</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et les mémoires complémentaires, enregistrés les 26 juillet, 29 octobre et 27 novembre 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour le Syndicat des juridictions financières, devenu le Syndicat des juridictions financières unifié, dont le siège est Chambre régionale des comptes du Nord Pas-de-Calais, Picardie, 14, rue du Marché au File à Arras Cedex (62012) ; le Syndicat des juridictions financières unifié demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du 27 mai 2013 du Président de la République portant nomination de Mme Estelle Fontaine, commissaire de 1ère classe des armées, pendant la période de son détachement, dans le corps des magistrats de chambre régionale des comptes, au grade de conseiller, à compter du 15 juin 2013 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la défense ;<br/>
<br/>
              Vu le code des juridictions financières ;<br/>
<br/>
              Vu la loi n° 83-634 du 13 juillet 1983 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Baptiste de Froment, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Barthélemy, Matuchansky, Vexliard, Poupot, avocat du Syndicat des juridictions financières ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 4139-2 du code de la défense, dans sa version applicable au litige : " Le militaire, remplissant les conditions de grade et d'ancienneté fixées par décret, peut, sur demande agréée, après un stage probatoire, être détaché pour occuper des emplois vacants et correspondant à ses qualifications au sein des administrations de l'Etat, des collectivités territoriales, de la fonction publique hospitalière et des établissements publics à caractère administratif, nonobstant les règles de recrutement pour ces emplois. / Les contingents annuels de ces emplois sont fixés par voie réglementaire pour chaque administration de l'Etat et pour chaque catégorie de collectivité territoriale ou établissement public administratif, compte tenu des possibilités d'accueil. / Après un an de détachement, le militaire peut demander, dans les conditions fixées par décret en Conseil d'Etat, son intégration ou sa titularisation dans le corps ou le cadre d'emploi dont relève l'emploi considéré, sous réserve de la vérification de son aptitude (...). / En cas d'intégration ou de titularisation, l'intéressé est reclassé à un échelon comportant un indice égal ou, à défaut, immédiatement supérieur à celui détenu dans le corps d'origine. " ; qu'aux termes de l'article R. 4139-14 du même code : " Le militaire qui remplit les conditions de grade et d'ancienneté fixées par la sous-section 2 de la présente section peut demander son détachement dans un emploi relevant d'un corps de fonctionnaires de l'Etat ou de ses établissements publics. Il adresse sa demande par la voie hiérarchique à l'autorité gestionnaire dont il relève. La demande est accompagnée d'un dossier dont la composition est fixée par arrêté du ministre de la défense, ou du ministre de l'intérieur pour les militaires de la gendarmerie nationale. (...) Après avoir reçu l'agrément du ministre de la défense ou pour les militaires de la gendarmerie nationale du ministre de l'intérieur sur avis du ministre de la défense, la demande est soumise pour avis à une Commission nationale d'orientation et d'intégration placée auprès du Premier ministre. " ; qu'aux termes de l'article R. 4139-15 du même code : " La Commission nationale d'orientation et d'intégration examine la demande en tenant compte de la qualification et de l'expérience professionnelle du militaire ainsi que des préférences qu'il a exprimées. Elle peut faire appel, pour l'appréciation des choix exprimés par le candidat, à des experts désignés par l'administration ou l'établissement public d'accueil. / Elle peut proposer à l'intéressé de se porter candidat à un emploi dans une autre administration ou un autre établissement public de l'Etat que ceux initialement envisagés. " ; qu'aux termes de l'article R. 4139-16 du même code : " L'avis de la commission est transmis au ministre de la défense, ou au ministre de l'intérieur pour les militaires de la gendarmerie nationale, et à l'autorité chargée de la gestion du corps d'accueil. Celle-ci se prononce dans le délai d'un mois à compter de cette transmission. Si sa candidature est retenue, une proposition d'affectation est adressée au militaire qui dispose d'un délai de quinze jours à compter de la notification de cette proposition pour l'accepter ou la refuser. / En cas d'acceptation, le militaire est mis à la disposition de l'administration ou de l'établissement public d'accueil pour effectuer un stage probatoire d'une durée de deux mois. (...) S'il a donné satisfaction, le militaire est placé à l'issue du stage probatoire en position de détachement, par décision conjointe du ministre de la défense, ou du ministre de l'intérieur pour les militaires de la gendarmerie nationale, et de l'autorité chargée de la gestion du corps d'accueil. " ; qu'aux termes de l'article R. 4139-19 du même code : " A l'issue du détachement, le militaire peut demander son intégration dans le corps dans lequel il a été détaché. (...) La décision de réintégration ou de maintien en détachement est prononcée après avis de la Commission nationale d'orientation et d'intégration, lequel est transmis au ministre de la défense, ou au ministre de l'intérieur pour les militaires de la gendarmerie nationale, et à l'autorité chargée de la gestion du corps d'accueil. (...) " ; que, par un décret du Président de la République du 27 mai 2013, pris en application de l'article L. 4139-2 du code de la défense, Mme Estelle Fontaine, commissaire capitaine de 1ère classe des armées, a été nommée, pendant la durée de son détachement, dans le corps des magistrats de chambre régionale des comptes, au grade de conseiller ; que le Syndicat des juridictions financières unifié demande l'annulation pour excès de pouvoir de ce décret ;<br/>
<br/>
              2. Considérant, en premier lieu, que le moyen tiré du défaut de consultation de la Commission nationale d'orientation et d'intégration placée auprès du Premier ministre prévue par les articles R. 4139-14 et suivants du code de la défense manque en fait ; que l'omission de la mention de cette consultation dans les visas du décret attaqué est sans incidence sur sa régularité ; <br/>
<br/>
              3. Considérant, en deuxième lieu, que ne sont pas davantage de nature à entacher d'irrégularité la procédure au terme de laquelle a été pris le décret attaqué les circonstances que ce serait la même personne, le directeur des ressources humaines des juridictions financières, qui aurait été présente au sein de la Commission nationale d'orientation et d'intégration et de la commission informelle d'évaluation mise en place par la Cour des comptes et qu'aucun représentant du corps des chambres régionales des comptes n'aurait siégé au sein de la première commission ;<br/>
<br/>
              4. Considérant, en troisième lieu, que les dispositions citées au point 1 instituent une procédure spécifique de détachement, pouvant le cas échéant conduire à une intégration, applicable aux militaires souhaitant accéder à l'une des trois fonctions publiques civiles ; que le décret attaqué a été pris sur le fondement de ces dispositions spéciales ; qu'il en résulte que les moyens tirés de ce que ce décret méconnaîtrait les dispositions des articles 13 bis, 13 ter et quater de la loi du 13 juillet 1983 portant droits et obligations des fonctionnaires relatives à la mobilité dans la fonction publique et qu'il serait entaché d'un défaut de base légale, faute que soit intervenu le décret d'application de l'article 13 ter de la même loi, sont, par suite, inopérants ; <br/>
<br/>
              5. Considérant, en quatrième lieu, d'une part, que si le syndicat requérant fait valoir que le code des juridictions financières, notamment le premier alinéa de l'article L. 212-5, comporterait une liste limitative de catégories de fonctionnaires pouvant être détachés dans le corps des magistrats de chambre régionale des comptes et ne prévoirait pas le détachement de militaires sous contrat, il résulte des termes mêmes de l'article L. 4139-2 du code de la défense qu'il institue, notamment au profit des militaires sous contrat, une possibilité de détachement " nonobstant les règles de recrutement pour (les) emplois " mentionnés par ces dispositions ; que le syndicat requérant n'est donc pas fondé à soutenir que le décret attaqué aurait été pris en méconnaissance du premier alinéa de l'article L. 212-5 du code des juridictions financières ; que, d'autre part, en vertu de l'article L. 4111-2 du code de la défense, le statut général des militaires s'applique tant aux militaires de carrière qu'à ceux servant en vertu d'un contrat ; que ces derniers peuvent faire l'objet, en vertu des dispositions les régissant, notamment celles de l'article L. 4138-8 du même code, d'un détachement dans d'autres corps ou emplois que ceux relevant de la fonction publique militaire ; qu'il ne résulte pas des termes de l'article L. 4139-2 cité ci-dessus ou de son objet que le législateur aurait entendu exclure la possibilité du détachement des intéressés dans des corps chargés de missions juridictionnelles ; que le détachement d'un militaire servant en contrat dans un corps chargé de missions juridictionnelles est soumis à la décision des autorités du corps d'accueil, auxquelles il appartient de se prononcer sur les candidatures qui leur sont proposées par la Commission nationale d'orientation et d'intégration ; que le deuxième alinéa de l'article R. 4139-17 du même code dispose que " lorsque le militaire sert en vertu d'un contrat, ce dernier est, le cas échéant, prorogé de droit pendant toute la durée du détachement " ; que le militaire servant en vertu d'un contrat détaché pour exercer des fonctions de magistrat de chambre régionale des comptes est soumis aux obligations et garanties prévues par le code des juridictions financières, et notamment par les deuxième, troisième et quatrième alinéas de l'article L. 212-5, qui prévoient qu'il ne peut être mis fin aux fonctions avant le terme du détachement que sur demande des intéressés ou pour motif disciplinaire ; qu'il résulte de ce qui précède que le moyen tiré de ce que le principe d'indépendance des juridictions et le principe de séparation des pouvoirs feraient obstacle au détachement d'un militaire contractuel dans les corps des magistrats de chambre régionale des comptes doit être écarté ;<br/>
<br/>
              6. Considérant, en cinquième lieu, que contrairement à ce que soutient le syndicat requérant, l'article L. 4139-2 précité ne subordonne pas le détachement des militaires au respect d'une condition d'équivalence de niveau de grade dans le corps d'origine avec celui détenu dans le corps d'accueil ; que le moyen tiré de la méconnaissance d'une telle condition doit, par suite, être écarté ;<br/>
<br/>
              7. Considérant, en sixième et dernier lieu, que les dispositions rappelées ci-dessus permettent à l'administration de vérifier la capacité des intéressés à occuper des emplois publics civils ; qu'il résulte, en particulier, de l'article L. 4139-2 du code de la défense que les qualifications des militaires candidats à un détachement doivent correspondre aux emplois auxquels ils postulent ; qu'il résulte des pièces du dossier que Mme Fontaine, commissaire capitaine de 1ère classe du corps des commissaires des armées, est titulaire notamment d'une maîtrise en droit, d'un diplôme de l'Ecole supérieure des métiers de la vente et de la gestion de Reims et d'un diplôme d'aptitude aux emplois d'officiers supérieurs ; qu'elle a exercé pendant plus de dix ans, après sa scolarité à l'école du commissariat de l'air, des fonctions diverses dans les domaines budgétaire, financier et comptable, non seulement au sein d'une base aérienne mais aussi en administration centrale, notamment en qualité de chef des sections " modernisation des processus budgétaires et comptables " et " performance financière " au sein de la sous-direction de la fonction financière et comptable de la direction des affaires financières du ministère de la défense ; que la Commission nationale d'orientation et d'intégration, qui est chargée, en application de l'article R. 4139-15 du code de la défense, d'examiner la demande en tenant compte de la qualification et de l'expérience professionnelle du militaire, a émis un avis favorable au détachement de Mme Fontaine, laquelle remplissait au demeurant les conditions de grade et d'ancienneté de services en qualité d'officier fixées par l'article D. 4139-11 du code de la défense ; que les autorités du corps d'accueil ont également émis un avis favorable à son détachement, au terme d'une procédure de sélection qui a conduit à ne retenir que deux personnes, dont l'intéressée, sur une liste de six candidats ; qu'il ne ressort pas des pièces du dossier, compte tenu du profil et du parcours professionnel de Mme Fontaine, que sa nomination en détachement dans le corps des magistrats de chambre régionale des comptes serait entachée d'une erreur manifeste dans l'appréciation de ses qualifications pour un tel emploi ; que, par voie de conséquence, le moyen tiré de l'atteinte au principe d'égalité, dès lors que l'emploi de détachement correspondrait à des qualifications d'un niveau supérieur à celles de l'intéressée, ne peut être accueilli ; <br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que la requête du Syndicat des juridictions financières unifié doit être rejetée, y compris les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
<br/>
Article 1er : La requête du syndicat des juridictions financières est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée au Syndicat des juridictions financières unifié, au Premier ministre, au ministre des finances et des comptes publics, au Premier président de la Cour des comptes et à Madame Estelle Fontaine.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">08-01-01-02 ARMÉES ET DÉFENSE. PERSONNELS MILITAIRES ET CIVILS DE LA DÉFENSE. QUESTIONS COMMUNES À L'ENSEMBLE DES PERSONNELS MILITAIRES. POSITIONS. - DÉTACHEMENT (L. 4139-2 DU CODE DE LA DÉFENSE) - CORPS D'ACCUEIL - CORPS DES MAGISTRATS DES CHAMBRES RÉGIONALES DES COMPTES - INCLUSION, EU ÉGARD AUX GARANTIES DONT LE DÉTACHEMENT EST ENTOURÉ.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">18-01-04-02 COMPTABILITÉ PUBLIQUE ET BUDGET. RÉGIME JURIDIQUE DES ORDONNATEURS ET DES COMPTABLES. JUGEMENT DES COMPTES. CHAMBRE RÉGIONALE DES COMPTES. - CORPS DES MAGISTRATS DES CHAMBRES RÉGIONALES DES COMPTES - POSSIBILITÉ DE DÉTACHEMENT DANS CE CORPS - MILITAIRE SOUS CONTRAT - EXISTENCE, EU ÉGARD AUX GARANTIES DONT LE DÉTACHEMENT EST ENTOURÉ.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">37-04-01 JURIDICTIONS ADMINISTRATIVES ET JUDICIAIRES. MAGISTRATS ET AUXILIAIRES DE LA JUSTICE. MAGISTRATS DE L'ORDRE ADMINISTRATIF. - CORPS DES MAGISTRATS DES CHAMBRES RÉGIONALES DES COMPTES - POSSIBILITÉ DE DÉTACHEMENT DANS CE CORPS - MILITAIRE SOUS CONTRAT - EXISTENCE, EU ÉGARD AUX GARANTIES DONT LE DÉTACHEMENT EST ENTOURÉ.
</SCT>
<ANA ID="9A"> 08-01-01-02 L'article L. 4139-2 du code de la défense institue, notamment au profit des militaires sous contrat, auxquels le statut général des militaires est applicable, une possibilité de détachement « nonobstant les règles de recrutement pour (les) emplois ». D'une part, il ne résulte pas des termes de l'article L. 4139-2 ou de son objet que le législateur aurait entendu exclure la possibilité du détachement des intéressés dans des corps chargés de missions juridictionnelles. D'autre part, eu égard aux garanties entourant ce détachement, les principes d'indépendance des juridictions et de séparation des pouvoirs ne font pas obstacle.</ANA>
<ANA ID="9B"> 18-01-04-02 L'article L. 4139-2 du code de la défense institue, notamment au profit des militaires sous contrat, auxquels le statut général des militaires est applicable, une possibilité de détachement « nonobstant les règles de recrutement pour (les) emplois ». D'une part, il ne résulte pas des termes de l'article L. 4139-2 ou de son objet que le législateur aurait entendu exclure la possibilité du détachement des intéressés dans des corps chargés de missions juridictionnelles. D'autre part, eu égard aux garanties entourant ce détachement, les principes d'indépendance des juridictions et de séparation des pouvoirs ne font pas obstacle.</ANA>
<ANA ID="9C"> 37-04-01 L'article L. 4139-2 du code de la défense institue, notamment au profit des militaires sous contrat, auxquels le statut général des militaires est applicable, une possibilité de détachement « nonobstant les règles de recrutement pour (les) emplois ». D'une part, il ne résulte pas des termes de l'article L. 4139-2 ou de son objet que le législateur aurait entendu exclure la possibilité du détachement des intéressés dans des corps chargés de missions juridictionnelles. D'autre part, eu égard aux garanties entourant ce détachement, les principes d'indépendance des juridictions et de séparation des pouvoirs ne font pas obstacle.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
