<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037882310</ID>
<ANCIEN_ID>JG_L_2018_12_000000420104</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/88/23/CETATEXT000037882310.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 4ème chambres réunies, 26/12/2018, 420104</TITRE>
<DATE_DEC>2018-12-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420104</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 4ème chambres réunies</FORMATION>
<TYPE_REC>Appréciation de la légalité</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Cécile Chaduteau-Monplaisir</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:420104.20181226</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un jugement du 16 avril 2018, le tribunal des affaires de sécurité sociale de Caen a sursis à statuer sur la demande de Mme A...B..., relative au refus de versement de l'allocation de rentrée scolaire en 2016, et saisi le Conseil d'Etat de la question de la légalité des articles R. 543-6 et R. 532-8 du code de la sécurité sociale.<br/>
<br/>
              Par un mémoire, enregistré le 1er août 2018 au secrétariat du contentieux du Conseil d'Etat, le ministre des solidarités et de la santé demande au Conseil d'Etat d'apprécier la légalité des articles R. 543-6 et R. 532-8 du code de la sécurité sociale et de déclarer qu'ils ne sont pas entachés d'illégalité.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu :<br/>
              - la Constitution ;<br/>
              - le code de sécurité sociale ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Chaduteau-Monplaisir, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Par un jugement du 16 avril 2018, le tribunal des affaires de sécurité sociale de Caen, saisi d'un litige opposant Mme B...à la caisse d'allocations familiales du Calvados, a sursis à statuer jusqu'à ce que le Conseil d'Etat se soit prononcé à titre préjudiciel sur la question de la légalité des articles R. 543-6 et R. 532-8 du code de la sécurité sociale, en ce qu'ils conduisent à déterminer un revenu forfaitaire supérieur au revenu réel pour certains demandeurs de l'allocation de rentrée scolaire et pas pour d'autres, au regard des règles constitutionnelles, en particulier du principe d'égalité résultant des articles 1er et 6 de la Déclaration des droits de l'homme et du citoyen de 1789 et du onzième alinéa du Préambule de la Constitution de 1946 garantissant à tous la sécurité matérielle.<br/>
<br/>
              2. Aux termes de l'article L. 543-1 du code de la sécurité sociale : " Une allocation de rentrée scolaire est attribuée au ménage ou à la personne dont les ressources ne dépassent pas un plafond variable en fonction du nombre des enfants à charge, pour chaque enfant inscrit en exécution de l'obligation scolaire dans un établissement ou organisme d'enseignement public ou privé. / (...) / Le niveau du plafond de ressources varie conformément à l'évolution des prix à la consommation des ménages hors les prix du tabac, dans des conditions prévues par décret en Conseil d'Etat. Son montant est fixé par décret et revalorisé par arrêté conjoint des ministres chargés de la sécurité sociale, du budget et de l'agriculture. / (...) ". Il résulte de ces dispositions que l'allocation de rentrée scolaire est versée aux familles qui remplissent les conditions relatives à la scolarisation des enfants, lorsque leurs ressources n'excèdent pas un plafond variable selon le nombre des enfants à charge.<br/>
<br/>
              3. L'article R. 543-5 du code de la sécurité sociale, pris en application de l'article L. 543-1 précité, prévoit que le bénéfice de l'allocation de rentrée scolaire n'est ouvert qu'aux ménages ou personnes qui, durant l'année civile de référence, définie à l'article R. 532-3 du même code comme étant l'avant-dernière année précédant la période de paiement de l'allocation, ont disposé de ressources dont le montant est inférieur à un plafond. L'article R. 543-6 du même code précise que, pour l'application de cette condition de ressources, la situation de famille est appréciée au 31 juillet précédant la rentrée scolaire considérée et qu'il est fait application des articles R. 532-3 à R. 532-8. <br/>
<br/>
              4. Enfin, le I de l'article R. 532-8 du code de la sécurité sociale prévoit qu'hormis pour les bénéficiaires du revenu de solidarité active ou de l'allocation aux adultes handicapés, les ressources de la personne et de son conjoint ou concubin font l'objet d'une évaluation forfaitaire lorsqu'il apparaît, au cours du mois civil précédant l'ouverture du droit ou du mois de novembre précédant le renouvellement du droit, que l'un d'entre eux perçoit une rémunération et que, soit, à l'ouverture du droit, le total des ressources de la personne et de son conjoint ou concubin perçu au cours de l'année civile de référence et apprécié selon les dispositions de l'article R. 532-3 est au plus égal à 1 015 fois le salaire minimum de croissance horaire en vigueur au 31 décembre de cette année, soit, lors du premier renouvellement du droit, les ressources lors de l'ouverture du droit ont déjà fait l'objet d'une évaluation forfaitaire, soit, lors des renouvellements suivants, aucune de ces personnes n'a disposé de ressources appréciées selon les dispositions de l'article R. 532-3. Selon le II du même article, l'évaluation forfaitaire correspond, s'il s'agit d'une personne exerçant une activité salariée, à douze fois la rémunération mensuelle perçue par l'intéressé le mois civil précédant l'ouverture du droit ou le mois de novembre précédant le renouvellement du droit, affectée de la déduction prévue au deuxième alinéa du 3° de l'article 83 du code général des impôts, et, s'il s'agit d'une personne exerçant une activité professionnelle non salariée, à 1 500 fois le salaire minimum de croissance horaire en vigueur au 1er juillet qui précède l'ouverture ou le renouvellement du droit. <br/>
<br/>
              5. L'évaluation forfaitaire ainsi instituée à l'article R. 532-8 du code de la sécurité sociale et applicable aux demandes d'allocation de rentrée scolaire en vertu de l'article R. 543-6 du même code a pour objet, ainsi que le fait valoir le ministre des solidarités et de la santé, d'éviter que la prise en compte des ressources de l'année de référence conduise à ce que cette prestation soit à tort versée à des foyers qui ne satisferaient plus, lors de l'ouverture ou du renouvellement de ce droit, à la condition de ressources à laquelle le bénéfice de cette prestation est subordonné. Toutefois, une telle évaluation n'est appliquée qu'à certains des foyers susceptibles d'avoir connu une modification de leurs revenus depuis l'année de référence, alors même qu'elles leur attribuent fictivement des ressources forfaitairement évaluées, sans leur ouvrir aucune possibilité de faire valoir et d'établir qu'ils ont disposé de revenus professionnels inférieurs à ceux qui résultent de l'évaluation forfaitaire. Ces dispositions peuvent ainsi conduire à ce que des foyers disposant de ressources identiques et inférieures au plafond au moment où le droit est ouvert soient traités de façon différente, certains d'entre eux, soumis à l'évaluation forfaitaire de leurs revenus, se trouvant privés du bénéfice de l'allocation de rentrée scolaire. Par suite, les dispositions de l'article R. 532-8 du code de la sécurité sociale introduisent une différence de traitement sans rapport avec l'objet de la norme qui l'établit et portent ainsi atteinte au principe d'égalité devant la loi. <br/>
<br/>
              6. Il résulte de ce qui précède que Mme B...est fondée à soutenir que les dispositions de l'article R. 532-8 du code de la sécurité sociale, ainsi que les dispositions de l'article R. 543-6 du même code en tant qu'elles renvoient à cet article, sont illégales.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Il est déclaré que les dispositions de l'article R. 532-8 du code de la sécurité sociale, ainsi que les dispositions de l'article R. 543-6 du même code en tant qu'elles renvoient à cet article, sont illégales.<br/>
Article 2 : La présente décision sera notifiée au tribunal des affaires de sécurité sociales de Caen, à Mme A...B..., à la caisse d'allocations familiales du Calvados et à la ministre des solidarités et de la santé.<br/>
Copie en sera adressée au tribunal de grande instance de Caen.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. ÉGALITÉ DEVANT LA LOI. - EVALUATION FORFAITAIRE APPLICABLE AUX DEMANDES D'ALLOCATION DE RENTRÉE SCOLAIRE (R. 532-8 DU CSS) - MÉCONNAISSANCE - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">62-04-06-07 SÉCURITÉ SOCIALE. PRESTATIONS. PRESTATIONS FAMILIALES ET ASSIMILÉES. ALLOCATION DE RENTRÉE SCOLAIRE. - EVALUATION FORFAITAIRE APPLICABLE AUX DEMANDES D'ALLOCATION DE RENTRÉE SCOLAIRE (R. 532-8 DU CSS) - MÉCONNAISSANCE DU PRINCIPE D'ÉGALITÉ DEVANT LA LOI - EXISTENCE.
</SCT>
<ANA ID="9A"> 01-04-03-01 L'évaluation forfaitaire instituée à l'article R. 532-8 du code de la sécurité sociale (CSS) et applicable aux demandes d'allocation de rentrée scolaire en vertu de l'article R. 543-6 du même code a pour objet d'éviter que la prise en compte des ressources de l'année de référence conduise à ce que cette prestation soit à tort versée à des foyers qui ne satisferaient plus, lors de l'ouverture ou du renouvellement de ce droit, à la condition de ressources à laquelle le bénéfice de cette prestation est subordonné. Toutefois, une telle évaluation n'est appliquée qu'à certains des foyers susceptibles d'avoir connu une modification de leurs revenus depuis l'année de référence, alors même qu'elles leur attribuent fictivement des ressources forfaitairement évaluées, sans leur ouvrir aucune possibilité de faire valoir et d'établir qu'ils ont disposé de revenus professionnels inférieurs à ceux qui résultent de l'évaluation forfaitaire. Ces dispositions peuvent ainsi conduire à ce que des foyers disposant de ressources identiques et inférieures au plafond au moment où le droit est ouvert soient traités de façon différente, certains d'entre eux, soumis à l'évaluation forfaitaire de leurs revenus, se trouvant privés du bénéfice de l'allocation de rentrée scolaire. Par suite, l'article R. 532-8 du CSS introduit une différence de traitement sans rapport avec l'objet de la norme qui l'établit et portent ainsi atteinte au principe d'égalité devant la loi.</ANA>
<ANA ID="9B"> 62-04-06-07 L'évaluation forfaitaire instituée à l'article R. 532-8 du code de la sécurité sociale (CSS) et applicable aux demandes d'allocation de rentrée scolaire en vertu de l'article R. 543-6 du même code a pour objet d'éviter que la prise en compte des ressources de l'année de référence conduise à ce que cette prestation soit à tort versée à des foyers qui ne satisferaient plus, lors de l'ouverture ou du renouvellement de ce droit, à la condition de ressources à laquelle le bénéfice de cette prestation est subordonné. Toutefois, une telle évaluation n'est appliquée qu'à certains des foyers susceptibles d'avoir connu une modification de leurs revenus depuis l'année de référence, alors même qu'elles leur attribuent fictivement des ressources forfaitairement évaluées, sans leur ouvrir aucune possibilité de faire valoir et d'établir qu'ils ont disposé de revenus professionnels inférieurs à ceux qui résultent de l'évaluation forfaitaire. Ces dispositions peuvent ainsi conduire à ce que des foyers disposant de ressources identiques et inférieures au plafond au moment où le droit est ouvert soient traités de façon différente, certains d'entre eux, soumis à l'évaluation forfaitaire de leurs revenus, se trouvant privés du bénéfice de l'allocation de rentrée scolaire. Par suite, l'article R. 532-8 du CSS introduit une différence de traitement sans rapport avec l'objet de la norme qui l'établit et portent ainsi atteinte au principe d'égalité devant la loi.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
