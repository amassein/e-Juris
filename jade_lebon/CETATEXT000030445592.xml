<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030445592</ID>
<ANCIEN_ID>JG_L_2015_03_000000369553</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/44/55/CETATEXT000030445592.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème SSR, 16/03/2015, 369553, Publié au recueil Lebon</TITRE>
<DATE_DEC>2015-03-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>369553</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BROUCHOT ; SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>M. Laurent Cytermann</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2015:369553.20150316</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              M. et Mme B...ont demandé au tribunal administratif de Grenoble d'annuler pour excès de pouvoir l'arrêté du 16 octobre 2008 par lequel le maire de la commune de Saint-Gervais-les-Bains (Haute-Savoie) a refusé de leur délivrer un permis de construire et la décision implicite de rejet de leur recours gracieux. Par un jugement n° 0901560 du 27 septembre 2012, le tribunal administratif de Grenoble a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 12LY02834 du 23 avril 2013, la cour administrative d'appel de Lyon a rejeté l'appel formé par M. et Mme B...contre le jugement du tribunal administratif de Grenoble.<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 21 juin 2013, 23 septembre 2013 et 19 février 2015 au secrétariat du contentieux du Conseil d'Etat, M. et Mme B...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt de la cour administrative d'appel de Lyon du 23 avril 2013 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Saint-Gervais-les-Bains la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Cytermann, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Brouchot, avocat de M. et Mme B...et à la SCP Gaschignard, avocat de la commune de Saint-Gervais-les-Bains ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il résulte des pièces du dossier soumis aux juges du fond que M. et Mme B...ont acquis en 1997 un chalet sur le territoire de la commune de Saint-Gervais-les-Bains ; que si ce chalet a été édifié en vertu de permis de construire délivrés en 1988 et en 1989 en vue de la construction d'un restaurant d'altitude, il a fait l'objet avant son acquisition par les époux B...d'un changement de destination pour être utilisé pour l'habitation, sans que les travaux ayant permis ce changement ne soient autorisés ; que les époux B...ont déposé le 22 août 2008 une demande de permis de construire portant sur une extension de leur chalet ; qu'ils se pourvoient en cassation contre l'arrêt par lequel la cour administrative d'appel de Lyon a confirmé le jugement du tribunal administratif de Grenoble rejetant leur demande d'annulation de la décision de refus du maire de Saint-Gervais-les-Bains en date du 16 octobre 2008 ;<br/>
<br/>
              2. Considérant que, lorsqu'une construction a fait l'objet de transformations sans les autorisations d'urbanisme requises, il appartient au propriétaire qui envisage d'y faire de nouveaux travaux de déposer une déclaration ou de présenter une demande de permis portant sur l'ensemble des éléments de la construction qui ont eu ou auront pour effet de modifier le bâtiment tel qu'il avait été initialement approuvé ou de changer sa destination ; qu'il en va ainsi même dans le cas où les éléments de construction résultant de ces travaux ne prennent pas directement appui sur une partie de l'édifice réalisée sans autorisation ;<br/>
<br/>
              3. Considérant qu'il appartient à l'autorité administrative, saisie d'une telle déclaration ou demande de permis, de statuer au vu de l'ensemble des pièces du dossier d'après les règles d'urbanisme en vigueur à la date de sa décision ; qu'elle doit tenir compte, le cas échéant, de l'application des dispositions de l'article L. 111-12 du code de l'urbanisme issues de la loi du 13 juillet 2006 portant engagement national pour le logement, qui prévoient la régularisation des travaux réalisés depuis plus de dix ans à l'occasion de la construction primitive ou des modifications apportées à celle-ci, sous réserve, notamment, que les travaux n'aient pas été réalisés sans permis de construire en méconnaissance des prescriptions légales alors applicables ; que, dans cette dernière hypothèse, si l'ensemble des éléments de la construction mentionnés au point 2 ne peuvent être autorisés au regard des règles d'urbanisme en vigueur à la date de sa décision, l'autorité administrative a toutefois la faculté, lorsque les éléments de construction non autorisés antérieurement sont anciens et ne peuvent plus faire l'objet d'aucune action pénale ou civile, après avoir apprécié les différents intérêts publics et privés en présence au vu de cette demande, d'autoriser, parmi les travaux demandés, ceux qui sont nécessaires à la préservation de la construction et au respect des normes ;<br/>
<br/>
              4. Considérant, en premier lieu, qu'il ressort des constatations opérées souverainement par les juges du fond, au demeurant non contestées, que la demande de permis de construire des époux B...ne portait que sur les travaux d'extension et non sur la régularisation des travaux ayant antérieurement permis le changement de destination du chalet ; que la cour n'a pas commis d'erreur de droit en jugeant qu'il incombait aux époux B...de présenter une demande portant sur l'ensemble des travaux qui ont eu ou qui auront pour effet de transformer le bâtiment tel qu'il avait été autorisé par le permis de construire initial et en en déduisant que le maire de la commune de Saint-Gervais-les-Bains était tenu de refuser le permis ;<br/>
<br/>
              5. Considérant, en second lieu, que contrairement à ce que soutient le pourvoi, la cour n'a pas jugé que la demande de permis de construire présentée le 22 août 2008 par les époux B...avait pour objet de changer la destination du bâtiment ; que, par suite, le moyen tiré de ce que la cour, en se prononçant ainsi, aurait dénaturé les termes de cette demande ne peut qu'être écarté ; <br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que le pourvoi de M. et Mme B... doit être rejeté, de même que leurs conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. et Mme B...le versement à la commune de Saint-Gervais-les-Bains de la somme de 3 000 euros au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. et Mme B...est rejeté.<br/>
Article 2 : M. et Mme B...verseront à la commune de Saint-Gervais-les-Bains la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à M. et Mme A...B...et à la commune de Saint-Gervais-les-Bains.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-03-01-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. TRAVAUX SOUMIS AU PERMIS. PRÉSENTENT CE CARACTÈRE. - CONSTRUCTION AYANT FAIT L'OBJET DE TRANSFORMATIONS SANS LES AUTORISATIONS D'URBANISME REQUISES - 1) OBLIGATION DE DEMANDER LA RÉGULARISATION DE L'ENSEMBLE DES ÉLÉMENTS MODIFIÉS - EXISTENCE EN PRINCIPE - CONSÉQUENCE - EXAMEN DE LA DEMANDE À LA DATE DE LA NOUVELLE DÉCISION [RJ1] - 2) EXCEPTION - CAS OÙ LE PÉTITIONNAIRE PEUT BÉNÉFICIER DES DISPOSITIONS DE L'ART. L. 111-12 DU CODE DE L'URBANISME (TRAVAUX RÉALISÉS DEPUIS PLUS DE DIX ANS) - 3) HYPOTHÈSE DE TRAVAUX ANCIENS RÉALISÉS SANS PERMIS, EXCLUS DU BÉNÉFICE DE L'ARTICLE L. 111-12 - POSSIBILITÉ D'AUTORISER CERTAINS TRAVAUX NÉCESSAIRES APRÈS AVOIR MIS EN BALANCE LES INTÉRÊTS EN PRÉSENCE - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-04 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. AUTORISATIONS D`UTILISATION DES SOLS DIVERSES. - CONSTRUCTION AYANT FAIT L'OBJET DE TRANSFORMATIONS SANS LES AUTORISATIONS D'URBANISME REQUISES - 1) OBLIGATION DE DEMANDER LA RÉGULARISATION DE L'ENSEMBLE DES ÉLÉMENTS MODIFIÉS - EXISTENCE EN PRINCIPE - CONSÉQUENCE - EXAMEN DE LA DEMANDE À LA DATE DE LA NOUVELLE DÉCISION [RJ1] - 2) EXCEPTION - CAS OÙ LE PÉTITIONNAIRE PEUT BÉNÉFICIER DES DISPOSITIONS DE L'ART. L. 111-12 DU CODE DE L'URBANISME (TRAVAUX RÉALISÉS DEPUIS PLUS DE DIX ANS) - 3) HYPOTHÈSE DE TRAVAUX ANCIENS RÉALISÉS SANS PERMIS, EXCLUS DU BÉNÉFICE DE L'ARTICLE L. 111-12 - POSSIBILITÉ D'AUTORISER CERTAINS TRAVAUX NÉCESSAIRES APRÈS AVOIR MIS EN BALANCE LES INTÉRÊTS EN PRÉSENCE - EXISTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">68-04-045 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. AUTORISATIONS D`UTILISATION DES SOLS DIVERSES. RÉGIMES DE DÉCLARATION PRÉALABLE. - CONSTRUCTION AYANT FAIT L'OBJET DE TRANSFORMATIONS SANS LES AUTORISATIONS D'URBANISME REQUISES - 1) OBLIGATION DE DEMANDER LA RÉGULARISATION DE L'ENSEMBLE DES ÉLÉMENTS MODIFIÉS - EXISTENCE EN PRINCIPE - CONSÉQUENCE - EXAMEN DE LA DEMANDE À LA DATE DE LA NOUVELLE DÉCISION [RJ1] - 2) EXCEPTION - CAS OÙ LE PÉTITIONNAIRE PEUT BÉNÉFICIER DES DISPOSITIONS DE L'ART. L. 111-12 DU CODE DE L'URBANISME (TRAVAUX RÉALISÉS DEPUIS PLUS DE DIX ANS) - 3) HYPOTHÈSE DE TRAVAUX ANCIENS RÉALISÉS SANS PERMIS, EXCLUS DU BÉNÉFICE DE L'ARTICLE L. 111-12 - POSSIBILITÉ D'AUTORISER CERTAINS TRAVAUX NÉCESSAIRES APRÈS AVOIR MIS EN BALANCE LES INTÉRÊTS EN PRÉSENCE - EXISTENCE.
</SCT>
<ANA ID="9A"> 68-03-01-01 1) Lorsqu'une construction a fait l'objet de transformations sans les autorisations d'urbanisme requises, il appartient au propriétaire qui envisage d'y faire de nouveaux travaux de déposer une déclaration ou de présenter une demande de permis portant sur l'ensemble des éléments de la construction qui ont eu ou auront pour effet de modifier le bâtiment tel qu'il avait été initialement approuvé ou de changer sa destination. Il en va ainsi même dans le cas où les éléments de construction résultant de ces travaux ne prennent pas directement appui sur une partie de l'édifice réalisée sans autorisation.,,,Il appartient à l'autorité administrative, saisie d'une telle déclaration ou demande de permis, de statuer au vu de l'ensemble des pièces du dossier d'après les règles d'urbanisme en vigueur à la date de sa décision.... ,,2) Elle doit tenir compte, le cas échéant, de l'application des dispositions de l'article L. 111-12 du code de l'urbanisme issues de la loi n°2006-872 du 13 juillet 2006 portant engagement national pour le logement, qui prévoient la régularisation des travaux réalisés depuis plus de dix ans à l'occasion de la construction primitive ou des modifications apportées à celle-ci, sous réserve, notamment, que les travaux n'aient pas été réalisés sans permis de construire en méconnaissance des prescriptions légales alors applicables.,,,3) Dans l'hypothèse où les travaux ont été réalisés sans permis de construire en méconnaissance des prescriptions légales alors applicables, si l'ensemble des éléments de la construction qui ont eu ou auront pour effet de modifier le bâtiment tel qu'il avait été initialement approuvé ou de changer sa destination ne peuvent être autorisés au regard des règles d'urbanisme en vigueur à la date de sa décision, l'autorité administrative a toutefois la faculté, lorsque les éléments de construction non autorisés antérieurement sont anciens et ne peuvent plus faire l'objet d'aucune action pénale ou civile, après avoir apprécié les différents intérêts publics et privés en présence au vu de cette demande, d'autoriser, parmi les travaux demandés, ceux qui sont nécessaires à la préservation de la construction et au respect des normes.</ANA>
<ANA ID="9B"> 68-04 1) Lorsqu'une construction a fait l'objet de transformations sans les autorisations d'urbanisme requises, il appartient au propriétaire qui envisage d'y faire de nouveaux travaux de déposer une déclaration ou de présenter une demande de permis portant sur l'ensemble des éléments de la construction qui ont eu ou auront pour effet de modifier le bâtiment tel qu'il avait été initialement approuvé ou de changer sa destination. Il en va ainsi même dans le cas où les éléments de construction résultant de ces travaux ne prennent pas directement appui sur une partie de l'édifice réalisée sans autorisation.,,,Il appartient à l'autorité administrative, saisie d'une telle déclaration ou demande de permis, de statuer au vu de l'ensemble des pièces du dossier d'après les règles d'urbanisme en vigueur à la date de sa décision.... ,,2) Elle doit tenir compte, le cas échéant, de l'application des dispositions de l'article L. 111-12 du code de l'urbanisme issues de la loi n°2006-872 du 13 juillet 2006 portant engagement national pour le logement, qui prévoient la régularisation des travaux réalisés depuis plus de dix ans à l'occasion de la construction primitive ou des modifications apportées à celle-ci, sous réserve, notamment, que les travaux n'aient pas été réalisés sans permis de construire en méconnaissance des prescriptions légales alors applicables.,,,3) Dans l'hypothèse où les travaux ont été réalisés sans permis de construire en méconnaissance des prescriptions légales alors applicables, si l'ensemble des éléments de la construction qui ont eu ou auront pour effet de modifier le bâtiment tel qu'il avait été initialement approuvé ou de changer sa destination ne peuvent être autorisés au regard des règles d'urbanisme en vigueur à la date de sa décision, l'autorité administrative a toutefois la faculté, lorsque les éléments de construction non autorisés antérieurement sont anciens et ne peuvent plus faire l'objet d'aucune action pénale ou civile, après avoir apprécié les différents intérêts publics et privés en présence au vu de cette demande, d'autoriser, parmi les travaux demandés, ceux qui sont nécessaires à la préservation de la construction et au respect des normes.</ANA>
<ANA ID="9C"> 68-04-045 1) Lorsqu'une construction a fait l'objet de transformations sans les autorisations d'urbanisme requises, il appartient au propriétaire qui envisage d'y faire de nouveaux travaux de déposer une déclaration ou de présenter une demande de permis portant sur l'ensemble des éléments de la construction qui ont eu ou auront pour effet de modifier le bâtiment tel qu'il avait été initialement approuvé ou de changer sa destination. Il en va ainsi même dans le cas où les éléments de construction résultant de ces travaux ne prennent pas directement appui sur une partie de l'édifice réalisée sans autorisation.,,,Il appartient à l'autorité administrative, saisie d'une telle déclaration ou demande de permis, de statuer au vu de l'ensemble des pièces du dossier d'après les règles d'urbanisme en vigueur à la date de sa décision.... ,,2) Elle doit tenir compte, le cas échéant, de l'application des dispositions de l'article L. 111-12 du code de l'urbanisme issues de la loi n°2006-872 du 13 juillet 2006 portant engagement national pour le logement, qui prévoient la régularisation des travaux réalisés depuis plus de dix ans à l'occasion de la construction primitive ou des modifications apportées à celle-ci, sous réserve, notamment, que les travaux n'aient pas été réalisés sans permis de construire en méconnaissance des prescriptions légales alors applicables.,,,3) Dans l'hypothèse où les travaux ont été réalisés sans permis de construire en méconnaissance des prescriptions légales alors applicables, si l'ensemble des éléments de la construction qui ont eu ou auront pour effet de modifier le bâtiment tel qu'il avait été initialement approuvé ou de changer sa destination ne peuvent être autorisés au regard des règles d'urbanisme en vigueur à la date de sa décision, l'autorité administrative a toutefois la faculté, lorsque les éléments de construction non autorisés antérieurement sont anciens et ne peuvent plus faire l'objet d'aucune action pénale ou civile, après avoir apprécié les différents intérêts publics et privés en présence au vu de cette demande, d'autoriser, parmi les travaux demandés, ceux qui sont nécessaires à la préservation de la construction et au respect des normes.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Ab. jur. CE, 12 janvier 2007, M. et Mme Fernandez, n° 274362, T. pp. 1124-1127. Cf. CE, 13 décembre 2013, Mme Carn et autres, n° 349081, T. pp. 879-882 ; CE, 27 juillet 2009, SCI La Paix, n° 305920, T. p. 990.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
