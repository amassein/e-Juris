<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043852077</ID>
<ANCIEN_ID>JG_L_2021_07_000000438247</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/85/20/CETATEXT000043852077.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 22/07/2021, 438247</TITRE>
<DATE_DEC>2021-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>438247</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; HAAS</AVOCATS>
<RAPPORTEUR>Mme Manon Chonavel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie Sirinelli</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:438247.20210722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Le syndicat des copropriétaires de l'immeuble situé 9, place Hoche à Versailles a demandé au tribunal administratif de Versailles d'annuler pour excès de pouvoir l'arrêté du 27 juillet 2017 par lequel le maire de Versailles a refusé de lui délivrer le permis de construire un ascenseur à structure métallique dans la cour de cet immeuble. Par une ordonnance n° 1706761 du 30 avril 2018, le président de la 3ème chambre du tribunal administratif de Versailles a rejeté cette demande. <br/>
              Par un arrêt n°18VE02167 du 4 décembre 2019, la cour administrative d'appel de Versailles a, sur l'appel du syndicat des copropriétaires, annulé cette ordonnance et rejeté la demande présentée par ce syndicat de copropriétaires devant le tribunal. <br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 4 février et 21 avril 2020 et le 3 juin 2021 au secrétariat du contentieux du Conseil d'Etat, le syndicat des copropriétaires demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de la commune de Versailles la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du patrimoine ;<br/>
              - le code de l'urbanisme ; <br/>
              - la loi n° 2000-1208 du 13 décembre 2000 ;<br/>
              - la loi n° 2016-925 du 7 juillet 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme A... B..., auditrice,  <br/>
<br/>
              - les conclusions de Mme Marie Sirinelli, rapporteure publique;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Piwinica, Molinié, avocat du syndicat des copropriétaires de l'immeuble situé 9, place Hoche et à Me Haas, avocat de la commune de Versailles ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 27 juillet 2017, le maire de Versailles a refusé de délivrer au syndicat des copropriétaires de l'immeuble situé 9, place Hoche à Versailles, dans le périmètre du plan de sauvegarde et de mise en valeur de la commune et identifié par ce plan parmi les immeubles " à conserver ", un permis de construire dans la cour de l'immeuble un ascenseur à structure métallique pour deux motifs tirés, d'une part, que l'adjonction d'un volume au bâti existant contrevenait à l'objectif de préservation de ce bâti, dont la modification ou l'altération est interdite selon l'article 3 de ce plan et, d'autre part, que le projet de construction avait pour effet de créer de l'emprise au sol supplémentaire, en méconnaissance de l'article SA 9 de ce plan compte tenu de l'emprise au sol existante déjà excédentaire de l'immeuble. Par une ordonnance du 30 avril 2018, le président de la troisième chambre du tribunal administratif de Versailles a rejeté la demande du syndicat des copropriétaires tendant à l'annulation pour excès de pouvoir de cet arrêté. Par un arrêt du 4 décembre 2019, la cour administrative d'appel de Versailles a annulé l'ordonnance du 30 avril 2018 mais rejeté la demande présentée devant le tribunal administratif par le syndicat des copropriétaires. Le syndicat des copropriétaires se pourvoit en cassation contre cet arrêt en tant qu'il a rejeté sa demande.<br/>
<br/>
              2. Le I de l'article L. 313-1 du code de l'urbanisme, dans sa rédaction applicable au litige, issue de la loi du 7 juillet 2016 relative à la liberté de la création, à l'architecture et au patrimoine, permet d'établir, sur tout ou partie d'un site patrimonial remarquable créé en application du titre III du livre VI du code du patrimoine, un plan de sauvegarde et de mise en valeur qui, sur le périmètre qu'il recouvre, tient lieu de plan local d'urbanisme. Le deuxième alinéa du II de l'article 112 de la loi du 7 juillet 2016 prévoit que les " secteurs sauvegardés " créés, comme celui de Versailles, avant la publication de cette loi " deviennent de plein droit des sites patrimoniaux remarquables, au sens de l'article L. 631-1 du code du patrimoine, et sont soumis au titre III du livre VI du même code. Le plan de sauvegarde et de mise en valeur du secteur sauvegardé applicable à la date de publication de la présente loi est applicable après cette date dans le périmètre du site patrimonial remarquable ". <br/>
<br/>
              3. Aux termes du III de l'article L. 313-1 du code de l'urbanisme : " (...) Le plan de sauvegarde et de mise en valeur peut (...) comporter l'indication des immeubles ou parties intérieures ou extérieures d'immeubles : (...) dont la démolition, l'enlèvement ou l'altération sont interdits et dont la modification est soumise à des conditions spéciales (...) ". Antérieurement à la loi du 13 décembre 2000 relative à la solidarité et au renouvellement urbains, les dispositions de l'article L. 313-1 du code de l'urbanisme prévoyaient que les plans de sauvegarde et de mise en valeur comportaient notamment l'indication des immeubles ou parties d'immeubles " dont la démolition, l'enlèvement, la modification ou l'altération sont interdits ". Il résulte des dispositions du III de l'article L. 313-1 du code de l'urbanisme telles que modifiées par la loi du 13 décembre 2000, éclairée par ses travaux préparatoires, que si les plans de sauvegarde et de mise en valeur peuvent identifier les immeubles ou parties intérieures ou extérieures d'immeubles dont la démolition, l'enlèvement ou l'altération sont interdits et dont la modification est soumise à des conditions spéciales, ils ne peuvent désormais en interdire toute modification de façon générale et absolue. <br/>
<br/>
              4. En l'espèce, l'article 3 des dispositions générales du règlement du plan de sauvegarde et de mise en valeur de Versailles dispose qu'il identifie des " 3) Immeubles ou parties d'immeubles à conserver dont la démolition, l'enlèvement, la modification ou l'altération sont interdits " et précise que : " La conservation de ces immeubles est impérative : par suite, tous travaux effectués sur un immeuble ne peuvent avoir pour but que la restitution de l'immeuble dans son état primitif ou dans un état antérieur connu compatible avec son état primitif ". <br/>
<br/>
              5. Il résulte des termes mêmes des dispositions de cet article 3 qu'elles interdisent la modification des immeubles ou parties d'immeubles identifiés comme étant à conserver. En autorisant la seule réalisation, sur ces immeubles, de travaux en vue de la restitution dans leur état primitif ou dans un état antérieur connu compatible avec leur état primitif, elles ne sauraient être regardées comme permettant la modification de ces immeubles en se bornant à la soumettre à des conditions spéciales. Par suite, le syndicat requérant est fondé à soutenir qu'en jugeant que les dispositions de l'article 3 du règlement du plan de sauvegarde et de mise en valeur de la commune de Versailles ne méconnaissaient pas l'article L. 313-1 du code de l'urbanisme, la cour administrative d'appel a commis une erreur de droit. <br/>
<br/>
              6. Le motif par lequel la cour a ainsi statué sur la légalité d'un des deux motifs pour lesquels le maire de Versailles avait refusé de délivrer le permis de construire sollicité ne présentant pas un caractère surabondant, il résulte de ce qui précède que le syndicat des copropriétaires de l'immeuble situé 9, place Hoche à Versailles est fondé à demander l'annulation de l'arrêt qu'il attaque.<br/>
<br/>
              7. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge du syndicat des copropriétaires, qui n'est pas la partie perdante dans la présente instance. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Versailles le versement au syndicat des copropriétaires d'une somme de 3 000 euros au titre de ces dispositions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : L'arrêt du 4 décembre 2019 de la cour administrative d'appel de Versailles est annulé, à l'exception de son article 1er.<br/>
Article 2 :  L'affaire est renvoyée à la cour administrative d'appel de Versailles.<br/>
Article 3 : La commune de Versailles versera une somme de 3 000 euros au syndicat des copropriétaires de l'immeuble situé 9, place Hoche à Versailles au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par la commune de Versailles au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée au syndicat des copropriétaires de l'immeuble situé 9, place Hoche à Versailles et à la commune de Versailles. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-08-02-02 PROCÉDURE. VOIES DE RECOURS. CASSATION. CONTRÔLE DU JUGE DE CASSATION. - MOTIF D'UNE DÉCISION JURIDICTIONNELLE STATUANT SUR LA LÉGALITÉ D'UN DES MOTIFS D'UNE DÉCISION ADMINISTRATIVE FONDÉE SUR UNE PLURALITÉ DE MOTIFS [RJ1] - MOTIF SURABONDANT - ABSENCE [RJ2] - CONSÉQUENCE EN CAS DE CENSURE DE CE MOTIF PAR LE JUGE DE CASSATION - ANNULATION DE LA DÉCISION JURIDICTIONNELLE [RJ3].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-03-03-02-04 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. LÉGALITÉ INTERNE DU PERMIS DE CONSTRUIRE. LÉGALITÉ AU REGARD DE LA RÉGLEMENTATION LOCALE. PLANS DE SAUVEGARDE ET DE MISE EN VALEUR. - FACULTÉ D'INTERDIRE DE FAÇON GÉNÉRALE ET ABSOLUE TOUTE MODIFICATION DES IMMEUBLES IDENTIFIÉS COMME ÉTANT À CONSERVER - ABSENCE.
</SCT>
<ANA ID="9A"> 54-08-02-02 Le motif par lequel le juge de l'excès de pouvoir statue sur la légalité d'un des motifs d'une décision administrative reposant sur une pluralité de motifs ne peut pas être tenu pour surabondant.,,,Par suite, la censure de ce motif par le juge de cassation entraine l'annulation de la décision juridictionnelle.</ANA>
<ANA ID="9B"> 68-03-03-02-04 Il résulte du III de l'article L. 313-1 du code de l'urbanisme tel que modifié par la loi n° 2000-1208 du 13 décembre 2000, éclairée par ses travaux préparatoires, que si les plans de sauvegarde et de mise en valeur peuvent identifier les immeubles ou parties intérieures ou extérieures d'immeubles dont la démolition, l'enlèvement ou l'altération sont interdits et dont la modification est soumise à des conditions spéciales, ils ne peuvent désormais en interdire toute modification de façon générale et absolue.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., s'agissant de l'office du juge de l'excès de pouvoir dans une telle configuration, CE, Assemblée, 12 janvier 1968, Ministre de l'économie et des finances c/ Dame Perrot, p. 39., ,[RJ2] Comp. CE, 30 décembre 2015, Société Les Laboratoires Servier, n° 372230, p. 493.,,[RJ3] Rappr., sur l'obligation, en principe, d'accueillir un pourvoi dirigé contre une décision juridictionnelle fondée sur plusieurs motifs, dont l'un, ne présentant pas un caractère surabondant, est erroné, CE, Section, 22 avril 2005, Commune du Barcarès, n° 257877, p. 170.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
