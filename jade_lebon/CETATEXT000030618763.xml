<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030618763</ID>
<ANCIEN_ID>JG_L_2015_05_000000383653</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/61/87/CETATEXT000030618763.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème / 10ème SSR, 20/05/2015, 383653</TITRE>
<DATE_DEC>2015-05-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>383653</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème / 10ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU ; SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>Mme Marie-Gabrielle Merloz</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Astrid Nicolazo de Barmon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:383653.20150520</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés les 12 août et 12 novembre 2014 au secrétariat du contentieux du Conseil d'Etat, la société Monceau assurances, Mutuelles associées demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 12 juin 2014 par laquelle l'Autorité de contrôle prudentiel et de résolution l'a mise en demeure de se mettre en conformité, avant le 31 décembre 2014, avec les obligations prévues par les articles R. 322-84 et R. 322-53-2 du code des assurances ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des assurances ;<br/>
              - le code monétaire et financier ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Gabrielle Merloz, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Marie-Astrid Nicolazo de Barmon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de la Monceau assurances, Mutuelles associées et à la SCP Rocheteau, Uzan-Sarano, avocat de l'Autorité de contrôle prudentiel et de résolution ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 612-31 du code monétaire et financier : " L'Autorité de contrôle prudentiel et de résolution peut mettre en demeure toute personne soumise à son contrôle de prendre, dans un délai déterminé, toutes mesures destinées à sa mise en conformité avec les obligations au respect desquelles l'Autorité de contrôle prudentiel et de résolution a pour mission de veiller. " ; qu'en application de ces dispositions, par une décision du 12 juin 2014, le vice-président de l'Autorité de contrôle prudentiel et de résolution (ACPR) a, sur la base d'un rapport de contrôle définitif du 7 août 2013 et après l'avoir informée de la mesure qu'il envisageait de prendre et l'avoir invitée à présenter ses observations, mis en demeure la société Monceau assurances, Mutuelles associées de se mettre en conformité, avant le 31 décembre 2014, avec les obligations prévues par les articles R. 322-84 et R. 322-53-2 du code des assurances ; que la société Monceau assurances, Mutuelles associées demande l'annulation de cette décision ; <br/>
<br/>
              2. Considérant, en premier lieu, qu'aux termes de l'article R. 322-84 du code des assurances : " Il peut être formé, entre sociétés d'assurance mutuelle ou leurs unions ou entre entreprises affiliées par convention à une même société de groupe d'assurance mutuelle, des sociétés de réassurance mutuelles ayant pour objet la réassurance des sociétés qui en font partie./ Ces sociétés de réassurance sont soumises aux dispositions de la présente section. Toutefois, elles sont valablement constituées lorsqu'elles réunissent au moins sept sociétés adhérentes, ce nombre minimum n'étant pas requis lorsqu'elles ne comprennent que des entreprises relevant d'une même société de groupe d'assurance mutuelle ; leurs statuts fixent, sans être tenus par un minimum, le montant de leur fonds d'établissement ; l'assemblée générale est composée de toutes les sociétés adhérentes." ;<br/>
<br/>
              3. Considérant que, sur le fondement de ces dispositions, l'ACPR a mis en demeure la société requérante de " s'assurer d'avoir au moins sept membres à tout moment ", " à défaut de constituer une société de groupe d'assurance mutuelle, ayant notamment vocation à nouer et à gérer des liens de solidarité financière importants et durables entre ses affiliés, et ne nécessitant que deux membres pour être valablement constituée, pour reprendre les fonctions actuelles de Monceau assurances (en dehors de la réassurance) si celle-ci était dissoute " ;<br/>
<br/>
              4. Considérant que l'exigence d'un nombre minimal d'adhérents vise à garantir la mutualisation des risques pris par les sociétés de réassurance mutuelles ; que la poursuite de cet objectif justifie que ce seuil soit respecté non seulement au moment de la constitution de la société mais également à tout moment, sans qu'y fasse obstacle la circonstance que le code des assurances n'impose pas de dissoudre les sociétés de réassurance mutuelles qui ne respecteraient pas cette condition ; que la décision attaquée n'est, dès lors, pas entachée d'erreur de droit en ce qu'elle exige que la société requérante soit composée de sept membres à tout moment ;<br/>
<br/>
              5. Considérant que, contrairement à ce que soutient la société requérante, sans apporter au demeurant de précisions à l'appui de ses allégations, les dispositions de l'article R. 322-84 du code des assurances qui, comme il vient d'être dit, poursuivent un objectif d'intérêt général tenant à la protection du marché des assurances et des assurés, ne portent pas d'atteinte disproportionnée à la liberté d'entreprendre ni à la liberté du commerce et de l'industrie ;  <br/>
<br/>
              6. Considérant, enfin, que la société requérante invoque une double rupture d'égalité, d'une part, entre les différentes sociétés de réassurance mutuelles et, d'autre part, entre ces dernières et les sociétés de groupe d'assurance mutuelle ; que, toutefois, la première branche de ce moyen n'est pas assortie des précisions suffisantes permettant d'en apprécier la portée et le bien-fondé ; que, s'agissant de la seconde branche, il résulte de l'article L. 322-1-3 du code des assurances que les sociétés de groupe d'assurance mutuelle, qui peuvent " décider de fonctionner sans capital social à condition de compter au moins deux entreprises affiliées et dont l'une au moins est une société d'assurance mutuelle ", ont pour objet la mise en place de " liens de solidarité financière importants et durables " entre les membres affiliés, le champ et l'étendue de cette coopération commune étant librement définis dans la convention d'affiliation ; que la différence de situation entre ces sociétés et les sociétés de réassurance mutuelles, tenant tant à leur objet qu'à leur mode de fonctionnement, justifie que le législateur ait prévu pour les premières des règles de constitution moins contraignantes que pour les secondes ; que la différence de traitement qui en résulte est en rapport direct avec l'objet de la réglementation qui l'établit ; que le moyen tiré de ce que les dispositions de l'article R. 322-84 du code des assurances porteraient atteinte au principe d'égalité  ne peut qu'être écarté ;<br/>
<br/>
              7. Considérant, en second lieu, qu'en vertu de l'article R. 322-53-2 du code des assurances, lorsque la société d'assurance mutuelle est administrée par un conseil d'administration et une direction générale, le conseil d'administration a compétence pour nommer et révoquer le directeur général ;<br/>
<br/>
              8. Considérant que le traité d'adhésion de la société Monceau assurances, Mutuelles associées stipule, dans sa section III, que : " Le directeur général de Monceau peut, s'il l'estime nécessaire, demander au conseil d'administration d'un adhérent de le nommer directeur général, ce que le conseil d'administration de l'adhérent ne peut refuser. / Si le directeur général de l'adhérent n'est pas le directeur général de Monceau, il doit recevoir, préalablement à son entrée en fonction, l'agrément conjoint du directeur général et du conseil d'administration de Monceau. / Le conseil d'administration de Monceau et son directeur général peuvent ensemble sans qu'il soit nécessaire de motiver leur demande, exiger du conseil d'administration de l'adhérent qu'il révoque son directeur général, ce que ledit conseil d'administration ne peut refuser " ; que la société requérante soutient que la décision de l'ACPR, en tant qu'elle relève que ces clauses accordent au directeur général et au conseil d'administration de la société Monceau assurances, Mutuelles associées des pouvoirs qui, aux termes des dispositions de l'article R. 322-53-2 du code des assurances, relèvent exclusivement des conseils d'administration des sociétés adhérentes et  qu'elle met en demeure la société de les supprimer, est entachée d'une erreur de fait, dès lors que le traité d'adhésion ne retire pas aux conseils d'administration de ses adhérents la compétence de nommer et révoquer le directeur général ; que, toutefois, alors même que le préambule du traité d'adhésion stipule qu'il ne peut " avoir pour effet d'amputer les prérogatives du conseil d'administration de l'adhérent, qui reste seul responsable de sa gestion vis-à-vis de ses sociétaires ", il résulte des stipulations précitées de la section III de ce traité que les conseils d'administration des adhérents de la société requérante ne disposent pas d'une compétence exclusive pour nommer et révoquer les directeurs généraux ;<br/>
<br/>
              9. Considérant qu'il résulte de tout ce qui précède que la société Monceau assurances, Mutuelles associées n'est pas fondée à demander l'annulation de la décision qu'elle attaque ;<br/>
<br/>
              10. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'ACPR qui n'est pas, dans la présente instance, la partie perdante ; qu'il n'y a pas lieu de faire droit aux conclusions présentées par l'ACPR à ce même titre ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : La requête de la société Monceau assurances, Mutuelles associées est rejetée. <br/>
Article 2 : Les conclusions présentées par l'Autorité de contrôle prudentiel et de résolution au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à la société Monceau assurances, Mutuelles associées et à l'Autorité de contrôle prudentiel et de résolution.<br/>
Copie en sera adressée pour information au ministre de l'économie, de l'industrie et du numérique.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">42-01-01-02 MUTUALITÉ ET COOPÉRATION. MUTUELLES. QUESTIONS GÉNÉRALES. STATUTS ET RÈGLEMENTS. - SOCIÉTÉS DE RÉASSURANCE MUTUELLES - NOMBRE MINIMAL D'ADHÉRENTS (ART. R. 322-84 DU CODE DES ASSURANCES) - CONDITION DEVANT ÊTRE VÉRIFIÉE À TOUT MOMENT.
</SCT>
<ANA ID="9A"> 42-01-01-02 L'exigence d'un nombre minimal d'adhérents posée par l'article R. 322-84 du code des assurances vise à garantir la mutualisation des risques pris par les sociétés de réassurance mutuelles. La poursuite de cet objectif justifie que ce seuil soit respecté non seulement au moment de la constitution de la société mais également à tout moment, sans qu'y fasse obstacle la circonstance que le code des assurances n'impose pas de dissoudre les sociétés de réassurance mutuelles qui ne respecteraient pas cette condition.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
