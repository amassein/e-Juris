<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038601876</ID>
<ANCIEN_ID>JG_L_2019_06_000000410987</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/60/18/CETATEXT000038601876.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 12/06/2019, 410987</TITRE>
<DATE_DEC>2019-06-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>410987</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>M. Jean-François de Montgolfier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:410987.20190612</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. A... B...a demandé au tribunal administratif de Marseille d'annuler pour excès de pouvoir la décision du 24 janvier 2013 par laquelle l'inspectrice du travail de la 17ème section de l'unité territoriale des Bouches-du-Rhône a autorisé la société Vitembal Tarascon à le licencier, ainsi que la décision du 26 juillet 2013 par laquelle le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social a rejeté le recours hiérarchique qu'il avait formé contre cette décision. Par un jugement n° 1306087 du 17 novembre 2015, le tribunal administratif a annulé ces deux décisions.<br/>
<br/>
              Par un arrêt n° 16MA00134 du 30 mars 2017, la cour administrative d'appel de Marseille a rejeté l'appel formé par la société Vitembal Tarascon contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 30 mai et 29 août 2017 au secrétariat du contentieux du Conseil d'Etat, la société Vitembal Tarascon demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de M. B... la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de commerce ;<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-François de Montgolfier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Soltner, Texidor, Perier, avocat de la Société Vitembal Tarascon  et à la SCP Rocheteau, Uzan-Sarano, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :	<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un jugement du 8 avril 2011, le tribunal de commerce de Tarascon a ouvert une procédure de redressement judiciaire à l'égard de la société Vitembal Tarascon. Par deux ordonnances des 20 septembre et 20 décembre 2011, le juge commissaire désigné par ce tribunal a autorisé le licenciement de salariés de cette entreprise appartenant, notamment, à la catégorie professionnelle dont relevait M. B..., salarié protégé. Par une décision du 24 janvier 2013, l'inspecteur du travail compétent a autorisé la société Vitembal Tarascon à licencier M. B...et, par une décision du 26 juillet 2013, le ministre chargé du travail a rejeté le recours hiérarchique formé par M. B... contre cette décision. Par un jugement du 17 novembre 2015, le tribunal administratif de Marseille a, sur la demande de M.B..., annulé ces deux décisions. La société Vitembal Tarascon se pourvoit en cassation contre l'arrêt du 30 mars 2017 par lequel la cour administrative d'appel de Marseille a rejeté son appel formé contre ce jugement.<br/>
<br/>
              2. L'article L. 631-17 du code de commerce, relatif à la possibilité de procéder à des licenciements économiques lorsqu'une entreprise est, comme en l'espèce, placée en période d'observation dans le cadre d'une procédure de redressement judiciaire, dispose que : " Lorsque des licenciements pour motif économique présentent un caractère urgent, inévitable et indispensable pendant la période d'observation, l'administrateur peut être autorisé par le juge-commissaire à procéder à ces licenciements (...) ". En vertu de ces dispositions, lorsqu'une entreprise est placée en période d'observation dans le cadre d'une procédure de redressement judiciaire, l'administrateur judiciaire ne peut procéder à des licenciements pour motif économique que s'ils présentent un caractère urgent, inévitable et indispensable et après autorisation, non nominative, du juge-commissaire désigné par le tribunal de commerce. Si le salarié dont le licenciement est envisagé bénéficie du statut protecteur, l'administrateur doit, au surplus, obtenir préalablement l'autorisation nominative de l'inspecteur du travail qui vérifie, outre le respect des exigences procédurales légales et des garanties conventionnelles, que ce licenciement n'est pas en lien avec le mandat du salarié, que la suppression du poste en cause est réelle et a été autorisée par le juge-commissaire, que l'employeur s'est acquitté de son obligation de reclassement et, enfin, qu'aucun motif d'intérêt général ne s'oppose à ce que l'autorisation soit accordée. En revanche, il résulte des dispositions du code de commerce citées ci-dessus que le législateur a entendu que, pendant la période d'observation, la réalité des difficultés économiques de l'entreprise et la nécessité des suppressions de postes soient examinées par le juge de la procédure collective dans le cadre de la procédure de redressement judiciaire. Dès lors qu'un licenciement a été autorisé par une ordonnance du juge-commissaire, ces éléments du motif de licenciement ne peuvent être contestés qu'en exerçant les voies de recours ouvertes contre cette ordonnance et ne peuvent être discutés devant l'administration.<br/>
<br/>
              3.  Toutefois, ainsi qu'il vient d'être dit, il découle des termes mêmes de l'article L. 631-17 du code de commerce que l'autorisation délivrée par le juge-commissaire de procéder à des licenciements qui présentent un caractère urgent, inévitable et indispensable pendant la période d'observation ne peut être prise que durant cette période. Dans ces conditions, l'administration ne peut légalement autoriser un licenciement demandé sur le fondement d'une autorisation délivrée par le juge-commissaire si la période d'observation a expiré à la date à laquelle l'employeur la saisit de sa demande.<br/>
<br/>
              4. Ainsi, dès lors qu'il ressort du dossier qui lui était soumis que la période d'observation de la société Vitembal Tarascon avait pris fin le 12 octobre 2012 par un jugement du tribunal de commerce de Tarascon arrêtant le plan de redressement de la société, la cour administrative d'appel n'a pas commis d'erreur de droit en jugeant que, l'autorisation de licenciement ayant été demandée à l'administration après cette date sur le fondement d'une autorisation du juge-commissaire, elle ne pouvait plus légalement être accordée.<br/>
<br/>
              5. Il résulte de tout ce qui précède que la société Vitemblal Tarascon n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque. Son pourvoi doit, par suite, être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Vitembal Tarascon une somme de 2 500 euros à verser à M. B... au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société Vitembal Tarascon est rejeté.<br/>
<br/>
Article 2 : La société Vitembal Tarascon versera à M. B... la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société Vitembal Tarascon et à M. A... B....<br/>
Copie en sera adressée à la ministre du travail.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">66-07-01-04-03 TRAVAIL ET EMPLOI. LICENCIEMENTS. AUTORISATION ADMINISTRATIVE - SALARIÉS PROTÉGÉS. CONDITIONS DE FOND DE L'AUTORISATION OU DU REFUS D'AUTORISATION. LICENCIEMENT POUR MOTIF ÉCONOMIQUE. - ENTREPRISE PLACÉE EN PÉRIODE D'OBSERVATION DANS LE CADRE D'UNE PROCÉDURE DE REDRESSEMENT JUDICIAIRE - CAS OÙ LE JUGE-COMMISSAIRE DÉSIGNÉ PAR LE TRIBUNAL DE COMMERCE A AUTORISÉ LE LICENCIEMENT - CONSÉQUENCE - 1) POSSIBILITÉ DE DISCUTER, DEVANT L'ADMINISTRATION DU TRAVAIL, LES ÉLÉMENTS DU MOTIF DE LICENCIEMENT TIRÉS DE LA RÉALITÉ DES DIFFICULTÉS ÉCONOMIQUES DE L'ENTREPRISE ET DE LA NÉCESSITÉ DES SUPPRESSIONS DE POSTES - ABSENCE [RJ1] - 2) AUTORISATION ADMINISTRATIVE DE LICENCIER LE SALARIÉ PROTÉGÉ NE POUVANT LÉGALEMENT SE FONDER SUR L'AUTORISATION DU JUGE-COMMISSAIRE QUE SI LA PÉRIODE D'OBSERVATION N'AVAIT PAS EXPIRÉ À LA DATE À LAQUELLE L'EMPLOYEUR A SAISI L'ADMINISTRATION.
</SCT>
<ANA ID="9A"> 66-07-01-04-03 1) En vertu de l'article L. 631-17 du code de commerce, lorsqu'une entreprise est placée en période d'observation dans le cadre d'une procédure de redressement judiciaire, l'administrateur judiciaire ne peut procéder à des licenciements pour motif économique que s'ils présentent un caractère urgent, inévitable et indispensable et après autorisation, non nominative, du juge-commissaire désigné par le tribunal de commerce. Si le salarié dont le licenciement est envisagé bénéficie du statut protecteur, l'administrateur doit, en outre, solliciter l'autorisation nominative de l'inspecteur du travail qui vérifie, outre le respect des exigences procédurales légales et des garanties conventionnelles, que ce licenciement n'est pas en lien avec le mandat du salarié, que la suppression du poste en cause est réelle et a été autorisée par le juge-commissaire, que l'employeur s'est acquitté de son obligation de reclassement, et qu'aucun motif d'intérêt général ne s'oppose à ce que l'autorisation soit accordée. En revanche, il résulte des dispositions du code de commerce que le législateur a entendu que, pendant cette période d'observation, la réalité des difficultés économiques de l'entreprise et la nécessité des suppressions de postes soient examinées par le juge de la procédure collective dans le cadre de la procédure de redressement judiciaire. Dès lors qu'un licenciement a été autorisé par une ordonnance du juge-commissaire, ces éléments du motif de licenciement ne peuvent donc être contestés qu'en exerçant les voies de recours ouvertes contre cette ordonnance et ne peuvent être discutés devant l'administration.,,,2) Il découle des termes mêmes de l'article L. 631-17 du code de commerce que l'autorisation délivrée par le juge-commissaire de procéder à des licenciements qui présentent un caractère urgent, inévitable et indispensable pendant la période d'observation ne peut être prise que durant cette période. Dans ces conditions, l'administration ne peut légalement autoriser le licenciement d'un salarié protégé demandé sur le fondement d'une autorisation délivrée par le juge-commissaire si la période d'observation a expiré à la date à laquelle l'employeur la saisit de sa demande.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 3 juillet 2013, Société Elixens France, n° 361066, T. p. 866.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
