<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029955391</ID>
<ANCIEN_ID>JG_L_2014_12_000000375639</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/95/53/CETATEXT000029955391.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème / 5ème SSR, 23/12/2014, 375639</TITRE>
<DATE_DEC>2014-12-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>375639</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème / 5ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>M. Bruno Bachini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Keller</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:375639.20141223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu 1°, sous le n° 375639, la requête sommaire et le mémoire complémentaire, enregistrés les 20 février et 20 mai 2014 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune de Fournels, représentée par son maire ; la commune demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite de rejet née du silence du Premier ministre sur sa demande du 18 décembre 2013 tendant à l'abrogation du décret n° 2013-77 du 24 janvier 2013 relatif à l'organisation du temps scolaire dans les écoles maternelles et élémentaires ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Vu 2°, sous le n° 375828, la requête, enregistrée le 26 février 2014 au secrétariat du contentieux du Conseil d'Etat, présentée pour la commune de Janvry, représentée par son maire ; la commune demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite de rejet née du silence du Premier ministre sur sa demande du 20 décembre 2013 tendant à l'abrogation du même décret ; <br/>
<br/>
              2°) d'enjoindre au Premier ministre de statuer à nouveau sur sa demande dans un délai de quinze jours à compter de la décision du Conseil d'Etat, statuant au contentieux ; <br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu la Constitution, notamment ses articles 72 et 72-2 ; <br/>
<br/>
              Vu le code de l'éducation, modifié, notamment, par le décret n° 2013-77 du 24 janvier 2013 ;<br/>
<br/>
              Vu la loi n° 2000-321 du 12 avril 2000, notamment son article 16-1 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Bachini, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Keller, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la commune de Fournels et à la SCP Piwnica, Molinié, avocat de la commune de Janvry ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que les requêtes des communes de Fournels et de Janvry présentent à juger les mêmes questions ; qu'il y a lieu de les joindre pour statuer par une seule décision ; <br/>
<br/>
              2. Considérant que le décret dont l'abrogation a été demandée par ces communes au Premier ministre modifie l'organisation de la semaine scolaire des écoles maternelles et élémentaires, régie par les articles D. 521-10 à D. 521-13 du code de l'éducation ; qu'il prévoit que cette semaine comporte vingt-quatre heures d'enseignement, réparties sur neuf demi-journées et que le conseil d'école, la commune ou l'établissement public de coopération intercommunale peuvent transmettre un projet d'organisation de la semaine scolaire au directeur académique des services de l'éducation nationale qui est chargé, par délégation du recteur d'académie, d'arrêter l'organisation de la semaine scolaire dans chaque école du département dont il a la charge ; qu'aux termes du deuxième alinéa de l'article D. 521-10 du code de l'éducation, dans sa rédaction issue du décret du 24 janvier 2013 : " Les heures d'enseignement sont organisées les lundi, mardi, jeudi et vendredi et le mercredi matin, à raison de cinq heures trente maximum par jour et de trois heures trente maximum par demi-journée " ; que les élèves peuvent, en vertu de l'article D. 521-13 du code, bénéficier d'activités pédagogiques complémentaires arrêtées par l'inspecteur de l'éducation nationale et destinées à aider les élèves rencontrant des difficultés à accompagner leur travail personnel ou à les encadrer dans le cadre d'une activité prévue par le projet d'école ;<br/>
<br/>
              3. Considérant, en premier lieu, que le refus d'engager la procédure d'abrogation d'un acte réglementaire adopté après consultation préalable obligatoire d'un organisme n'implique pas une nouvelle consultation de cet organisme ; que, dès lors, si l'article L. 231-1 du code de l'éducation prévoit que le Conseil supérieur de l'éducation est obligatoirement consulté sur toutes les questions d'intérêt national concernant l'éducation, ces dispositions n'imposaient pas au Premier ministre de saisir de nouveau ce conseil, dès lors qu'il n'entendait pas faire droit à la demande d'abrogation dont il était saisi ; <br/>
<br/>
              4. Considérant, en deuxième lieu, que les communes requérantes soutiennent que le décret dont l'abrogation a été demandée au Premier ministre aurait créé une charge supplémentaire pour les communes et méconnaîtrait, de ce fait, le principe de libre administration des collectivités territoriales garanti par l'article 72 de la Constitution et, en ce qui concerne leur autonomie financière, par son article 72-2 ; <br/>
<br/>
              5. Considérant, toutefois, d'une part, que ce décret a pour seul objet de répartir un nombre d'heures d'enseignement inchangé sur neuf demi-journées au lieu des huit demi-journées prévues par la réglementation antérieure, mais ne régit pas l'organisation des activités périscolaires, qui conservent un caractère facultatif pour les communes ; que le décret n'opère aucun transfert de compétences vers les communes qui aurait impliqué, en vertu de l'article 72-2 de la Constitution, une compensation financière ; <br/>
<br/>
              6. Considérant, d'autre part, que la circonstance que la modification de la réglementation applicable aux rythmes scolaires aurait des conséquences sur les dépenses liées à l'utilisation des bâtiments scolaires et à la gestion des agents spécialisés des écoles maternelles, nécessairement limitées dès lors que le nombre d'heures d'enseignement hebdomadaire reste inchangé, ne saurait caractériser une atteinte illégale à la libre administration des communes ; <br/>
<br/>
              7. Considérant, en troisième lieu, que le moyen tiré de la méconnaissance du principe d'égalité, qui n'est invoqué qu'à raison des charges nouvelles mises à la charge des communes,  ne peut, compte tenu de ce qui a été dit ci-dessus, qu'être écarté ;<br/>
<br/>
              8. Considérant, en quatrième lieu, que le décret du 24 janvier 2013, qui a modifié l'organisation de la semaine scolaire dans les écoles maternelles et élémentaires, est sans incidence sur les obligations qui incombent au service public de l'éducation en matière d'accueil des élèves handicapés ou de sécurité des établissements recevant du public ; que, par suite, le moyen tiré de ce que le décret méconnaîtrait l'objectif de clarté et d'intelligibilité de la règle de droit quant à ses effets dans ces domaines ne peut qu'être écarté ; que, de même, le décret n'ayant ni pour objet ni pour effet de déroger à l'obligation posée à l'article L. 521-2 du code de l'éducation de tenir compte des besoins des élèves en exercice physique dans l'organisation de la scolarité, le moyen tiré de ce qu'il méconnaîtrait les dispositions de cet article doit, par suite, être également écarté ; <br/>
<br/>
              9. Considérant, enfin, que le moyen tiré de ce que le décret litigieux méconnaîtrait les dispositions de l'article L. 521-4 du code de l'éducation relatif à l'architecture scolaire n'est pas assorti des précisions permettant d'en apprécier le bien-fondé ; <br/>
<br/>
              10. Considérant qu'il résulte de tout ce qui précède que les communes de Fournels et de Janvry ne sont pas fondées à soutenir que les refus attaqués méconnaîtraient le principe, rappelé par l'article 16-1 de la loi du 12 avril 2000, selon lequel l'autorité administrative est tenue de faire droit à une demande d'abrogation d'un règlement illégal ni, par suite, à demander l'annulation de ces refus ; que leurs conclusions à fin d'injonction ainsi que celles présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, dès lors, qu'être également rejetées ; <br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
Article 1er : Les requêtes des communes de Fournels et de Janvry sont rejetées. <br/>
Article 2 : La présente décision sera notifiée à la commune de Fournels, à la commune de Janvry, à la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-03-02-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. PROCÉDURE CONSULTATIVE. CONSULTATION NON OBLIGATOIRE. - REFUS D'ABROGER UN ACTE RÉGLEMENTAIRE ADOPTÉ APRÈS UNE CONSULTATION PRÉALABLE OBLIGATOIRE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">01-04-005 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. CONSTITUTION ET PRINCIPES DE VALEUR CONSTITUTIONNELLE. - LIBRE ADMINISTRATION DES COLLECTIVITÉS TERRITORIALES - MODIFICATION PAR DÉCRET DE LA RÉPARTITION DES HEURES D'ENSEIGNEMENT SCOLAIRE - ABSENCE D'ATTEINTE EN L'ESPÈCE.
</SCT>
<ANA ID="9A"> 01-03-02-03 Le refus d'engager la procédure d'abrogation d'un acte réglementaire adopté après consultation préalable obligatoire d'un organisme n'implique pas une nouvelle consultation de cet organisme.</ANA>
<ANA ID="9B"> 01-04-005 La circonstance que la modification de la réglementation applicable aux rythmes scolaires aurait des conséquences sur les dépenses liées à l'utilisation des bâtiments scolaires et à la gestion des agents spécialisés des écoles maternelles, nécessairement limitées dès lors que le nombre d'heures d'enseignement hebdomadaire reste inchangé, ne saurait caractériser une atteinte illégale à la libre administration des communes.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
