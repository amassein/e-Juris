<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034993681</ID>
<ANCIEN_ID>JG_L_2017_06_000000396427</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/99/36/CETATEXT000034993681.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 1ère chambres réunies, 21/06/2017, 396427</TITRE>
<DATE_DEC>2017-06-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>396427</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>Mme Laurence Franceschini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:396427.20170621</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° La société centrale photovoltaïque Font de Leu a formé tierce opposition au jugement n° 1308192 du 2 juillet 2015 par lequel le tribunal administratif de Marseille a annulé la délibération du 27 juin 2013 du conseil municipal de Lançon-Provence approuvant son plan local d'urbanisme en tant qu'elle crée une zone Ne sur le secteur de Calissanne destinée à un projet de centrale photovoltaïque. Par une ordonnance n° 1506684 du 1er septembre 2015, le président de la 2ème chambre du tribunal administratif de Marseille a rejeté cette demande.<br/>
<br/>
              Par une ordonnance n° 15MA04156 du 24 novembre 2015, le président de la 9ème chambre de la cour administrative d'appel de Marseille a rejeté l'appel formé par la société centrale photovoltaïque Font de Leu contre cette ordonnance. <br/>
<br/>
              Sous le n° 396427, par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique enregistrés respectivement les 26 janvier et 21 avril 2016 et les 14 février 2017 au secrétariat du contentieux du Conseil d'Etat, la société centrale photovoltaïque Font de Leu demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la chambre d'agriculture des Bouches-du-Rhône la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Sous le n° 396429, par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés respectivement les 26 janvier et 21 avril 2016 et les 14 février 2017 au secrétariat du contentieux du Conseil d'Etat, la société centrale photovoltaïque Font de Leu demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la chambre d'agriculture des Bouches-du-Rhône la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Laurence Franceschini, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Odent, Poulet, avocat de la société centrale photovoltaïque Font de Leu et à la SCP Waquet, Farge, Hazan, avocat de la chambre d'agriculture des Bouches-du-Rhône ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le conseil municipal de Lançon-Provence a, par une première délibération en date du 13 juin 2013, déclaré d'intérêt général un projet de création d'une centrale photovoltaïque de 12 MW dans le secteur du domaine de Calissanne sur le site de Font de Leu et approuvé, en conséquence, la mise en compatibilité de son plan d'occupation des sols en plaçant en zone NDe 37 hectares précédemment classés en zone NC du plan d'occupation des sols et, par une seconde délibération en date du 27 juin 2013, approuvé le plan local d'urbanisme de la commune prévoyant, sur le même secteur du domaine de Calissanne, une zone Ne de 42 hectares ; que, par deux jugements en date du 2 juillet 2015 le tribunal administratif de Marseille a, à la demande de la chambre d'agriculture des Bouches-du-Rhône, annulé ces deux délibérations ; que la tierce opposition formée contre chacun de ces deux jugements par la société centrale photovoltaïque Font de Leu a été rejetée par deux ordonnances du président de la 2ème chambre du tribunal administratif de Marseille le 1er septembre 2015 ; que, par deux pourvois qu'il y a lieu de joindre pour statuer par une même décision, la société centrale photovoltaïque Font de Leu se pourvoit en cassation contre les deux ordonnances du 24 novembre 2015 par lesquelles le président de la 9ème chambre de la cour administrative d'appel de Marseille a rejeté ses appels contre ces ordonnances ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 832-1 du code de justice administrative : " Toute personne peut former tierce opposition à une décision juridictionnelle qui préjudicie à ses droits, dès lors que ni elle ni ceux qu'elle représente n'ont été présents ou régulièrement appelés dans l'instance ayant abouti à cette décision. " ; que la société centrale photovoltaïque Font de Leu n'a été ni présente ni représentée devant le tribunal administratif de Marseille au cours des instances ayant conduit aux jugements contre lesquels elle a formé tierce opposition ;<br/>
<br/>
              3. Considérant qu'un requérant n'est, en règle générale et sauf circonstances particulières dont il se prévaudrait, pas recevable à former tierce opposition à une décision ayant fait droit, totalement ou partiellement, à une demande d'annulation d'un document d'urbanisme au seul motif qu'il est partie à un litige portant sur la légalité d'une autorisation de construire qui lui a été délivrée sur le fondement de dispositions annulées de ce document ; <br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la délibération du conseil municipal de Lançon-Provence déclarant d'intérêt général un projet de centrale photovoltaïque de 12 MW dans le secteur du Domaine de Calissanne sur le site de Font de Leu et la mise en compatibilité du plan d'occupation des sols classant le secteur en cause en zone NDe ainsi que sa délibération portant révision du plan local d'urbanisme de la commune de Lançon-Provence en tant qu'elle classe le même secteur en zone Ne, avaient pour unique objet de permettre la réalisation du projet de centrale photovoltaïque pour lequel un permis de construire, faisant l'objet d'un recours juridictionnel n'ayant pas donné lieu à une décision de justice irrévocable, a ensuite été délivré à la société requérante par le préfet des Bouches-du-Rhône le 13 août 2013 ; que l'annulation de ces délibérations compromet ce projet de construction dans des conditions de nature à préjudicier aux droits de la société centrale photovoltaïque Font de Leu ; qu'ainsi, compte tenu de ces circonstances particulières, en jugeant que cette société ne pouvait être regardée comme recevable à former tierce opposition aux ordonnances du 2 juillet 2015 du président de la 2ème chambre du tribunal administratif de Marseille, le président de la 9ème chambre de la cour administrative d'appel de Marseille a inexactement qualifié les faits qui lui étaient soumis ; que, par suite, ses ordonnances doivent être annulées ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de faire application des dispositions de l'article L. 821-2 du code de justice administrative et de statuer sur l'appel présenté par la société centrale photovoltaïque Font de Leu contre les ordonnances en date du 1er septembre 2015 par lesquelles le président de la 2e chambre du tribunal administratif de Marseille a rejeté sa tierce-opposition aux jugements de ce tribunal en date du 2 juillet 2015 prononçant l'annulation des délibérations du conseil municipal de Lançon-Provence des 13 et 27 juin 2013 ;<br/>
<br/>
              6. Considérant que la cour administrative d'appel de Marseille s'est prononcée par deux arrêts en date du 21 février 2017 sur les appels formés contre les deux jugements du tribunal administratif faisant l'objet des demandes de tierce opposition ; que, par suite, il n'y a pas lieu de statuer sur les conclusions de la société centrale photovoltaïque Font de Leu, des recours en tierce-opposition ne pouvant, le cas échéant, être formés que contre ces arrêts ; <br/>
<br/>
              7. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la société centrale photovoltaïque Font de Leu au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que les mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société centrale photovoltaïque Font de Leu qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les ordonnances nos 15MA04156 et 15MA04157 du 24 novembre 2015 du président de la 9ème chambre de la cour administrative d'appel de Marseille sont annulées.<br/>
<br/>
Article 2 : Il n'y a pas lieu de statuer sur les appels formés devant la cour administrative d'appel de Marseille par la société centrale photovoltaïque Font de Leu. <br/>
<br/>
Article 3 : Les conclusions de la société centrale photovoltaïque Font de Leu et de la chambre d'agriculture des Bouches-du-Rhône au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la société centrale photovoltaïque Font de Leu, à la chambre d'agriculture des Bouches-du-Rhône et à la commune de Lançon-Provence.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-08-04-01 PROCÉDURE. VOIES DE RECOURS. TIERCE-OPPOSITION. RECEVABILITÉ. - ANNULATION D'UN DOCUMENT D'URBANISME - RECEVABILITÉ DE LA TIERCE-OPPOSITION D'UNE PARTIE À UN LITIGE RELATIF À UNE AUTORISATION DE CONSTRUIRE QUI LUI A ÉTÉ DÉLIVRÉE SUR LE FONDEMENT DE DISPOSITIONS ANNULÉES DE CE DOCUMENT - 1) PRINCIPE - ABSENCE [RJ1] - 2) EXCEPTION - CAS OÙ LES DISPOSITIONS AVAIENT POUR UNIQUE OBJET DE PERMETTRE LA RÉALISATION DU PROJET.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-06 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. - ANNULATION D'UN DOCUMENT D'URBANISME - TIERCE-OPPOSITION D'UNE PARTIE À UN LITIGE RELATIF À UNE AUTORISATION DE CONSTRUIRE QUI LUI A ÉTÉ DÉLIVRÉE SUR LE FONDEMENT DE DISPOSITIONS ANNULÉES DE CE DOCUMENT - RECEVABILITÉ - 1) PRINCIPE - ABSENCE [RJ1] - 2) EXCEPTION - CAS OÙ LES DISPOSITIONS AVAIENT POUR UNIQUE OBJET DE PERMETTRE LA RÉALISATION DU PROJET.
</SCT>
<ANA ID="9A"> 54-08-04-01 1) Un requérant n'est, en règle générale et sauf circonstances particulières dont il se prévaudrait, pas recevable à former tierce-opposition à une décision ayant fait droit, totalement ou partiellement, à une demande d'annulation d'un document d'urbanisme au seul motif qu'il est partie à un litige portant sur la légalité d'une autorisation de construire qui lui a été délivrée sur le fondement de dispositions annulées de ce document.,,,2) Délibération d'un conseil municipal déclarant d'intérêt général un projet de centrale photovoltaïque et la mise en compatibilité du plan d'occupation des sols et délibération portant révision du plan local d'urbanisme en tant qu'elle classe le secteur concerné par le projet. Ces délibérations avaient pour unique objet de permettre la réalisation du projet de centrale photovoltaïque, pour lequel un permis de construire, faisant l'objet d'un recours juridictionnel qui n'avait pas donné lieu à une décision de justice irrévocable, a ensuite été délivré. L'annulation de ces délibérations compromet ce projet de construction dans des conditions de nature à préjudicier aux droits de la société pétitionnaire. Ainsi, compte tenu de ces circonstances particulières, la société était recevable à former tierce-opposition.</ANA>
<ANA ID="9B"> 68-06 1) Un requérant n'est, en règle générale et sauf circonstances particulières dont il se prévaudrait, pas recevable à former tierce-opposition à une décision ayant fait droit, totalement ou partiellement, à une demande d'annulation d'un document d'urbanisme au seul motif qu'il est partie à un litige portant sur la légalité d'une autorisation de construire qui lui a été délivrée sur le fondement de dispositions annulées de ce document.,,,2) Délibération d'un conseil municipal déclarant d'intérêt général un projet de centrale photovoltaïque et la mise en compatibilité du plan d'occupation des sols et délibération portant révision du plan local d'urbanisme en tant qu'elle classe le secteur concerné par le projet. Ces délibérations avaient pour unique objet de permettre la réalisation du projet de centrale photovoltaïque, pour lequel un permis de construire, faisant l'objet d'un recours juridictionnel qui n'avait pas donné lieu à une décision de justice irrévocable, a ensuite été délivré. L'annulation de ces délibérations compromet ce projet de construction dans des conditions de nature à préjudicier aux droits de la société pétitionnaire. Ainsi, compte tenu de ces circonstances particulières, la société était recevable à former tierce opposition.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Rappr. CE, 16 novembre 2009, Société les résidences de Cavalière, n° 308624, T. pp. 926-991.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
