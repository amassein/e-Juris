<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028934611</ID>
<ANCIEN_ID>JG_L_2014_05_000000355924</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/93/46/CETATEXT000028934611.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère / 6ème SSR, 14/05/2014, 355924</TITRE>
<DATE_DEC>2014-05-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>355924</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère / 6ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Philippe Combettes</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:355924.20140514</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 17 janvier 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par la fédération UNSA spectacle et communication, dont le siège est 21 rue Jules Ferry à Bagnolet (93170), représentée par son secrétaire général ; la fédération requérante demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir, en tant qu'ils agréent le Fonds d'assurance formation des secteurs de la culture, de la communication et des loisirs (AFDAS), les deux arrêtés du 9 novembre 2011 du ministre du travail, de l'emploi et de la santé, portant agrément d'organismes collecteurs paritaires des fonds de la formation professionnelle continue au titre, respectivement, du plan de formation et de la professionnalisation en application des 1°, 2°, 3° et 4° de l'article L. 6332-7 du code du travail, et du congé individuel de formation, en application du 5° du même article L. 6332-7 ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code du travail ;<br/>
<br/>
              Vu la loi n° 2009-1437 du 24 novembre 2009 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Philippe Combettes, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il résulte du I de l'article 43 de la loi du 24 novembre 2009 relative à l'orientation et à la formation professionnelle tout au long de la vie que la validité des agréments délivrés aux organismes collecteurs paritaires des fonds de la formation professionnelle continue expirait au plus tard le 1er janvier 2012, la délivrance d'un nouvel agrément étant subordonnée à l'existence d'un accord conclu à cette fin entre les organisations syndicales de salariés et d'employeurs représentatives dans le champ d'application de l'accord ; que, dans ce cadre, un accord a été conclu le 5 juillet 2011 afin de modifier la convention du 12 septembre 1972 portant création, sous forme d'association, du Fonds d'assurance formation des secteurs de la culture, de la communication et des loisirs (AFDAS) et de confirmer le choix de ce fonds en tant qu'organisme collecteur paritaire agréé dans le champ professionnel de cette convention ; que, par deux arrêtés du 9 novembre 2011, dont l'UNSA spectacle-communication demande l'annulation pour excès de pouvoir, le ministre du travail, de l'emploi et de la santé a agréé l'AFDAS au titre du plan de formation et de la professionnalisation, d'une part, et du congé individuel de formation, d'autre part ; <br/>
<br/>
              Sur la compétence du Conseil d'Etat en premier et dernier ressort :<br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 311-1 du code de justice administrative : " Le Conseil d'Etat est compétent pour connaître en premier et dernier ressort : (...) 2° Des recours dirigés contre les actes réglementaires des ministres et des autres autorités à compétence nationale et contre leurs circulaires et instructions de portée générale (...) " ; que, eu égard à l'intérêt général de leurs activités, aux obligations qui leur sont imposées et aux contrôles dont ils font l'objet de la part des pouvoirs publics, les organismes collecteurs paritaires agréés sont des personnes de droit privé investies d'une mission de service public ; que par suite, les décisions du ministre par lesquelles est délivré l'agrément à ces organismes revêtent un caractère réglementaire ; que le Conseil d'Etat est donc compétent pour connaître en premier et dernier ressort des recours dirigés contre les arrêtés attaqués ;<br/>
<br/>
              Sur la légalité des arrêtés attaqués :<br/>
<br/>
              3. Considérant qu'il résulte de l'article L. 6332-1 du code du travail que l'agrément est accordé aux organismes collecteurs paritaires en fonction notamment " de l'application d'engagements relatifs à la transparence de la gouvernance " ; qu'aux termes du premier alinéa de l'article L. 6332-2-1 du code du travail : " Lorsqu'une personne exerce une fonction d'administrateur ou de salarié dans un établissement de formation, elle ne peut exercer une fonction d'administrateur ou de salarié dans un organisme collecteur paritaire agréé ou un organisme délégué par ce dernier " ; qu'il résulte de ces dispositions, éclairées par les travaux parlementaires qui ont précédé l'adoption de la loi du 24 novembre 2009 relative à l'orientation et à la formation professionnelle tout au long de la vie, que le législateur, qui a inséré ces dispositions dans une sous-section du code du travail consacrée à l'agrément des organismes collecteurs paritaires, a entendu, par cette exigence, renforcer la transparence de la gouvernance de ces organismes ; qu'en vertu de l'arrêté du 20 septembre 2011, pris sur le fondement de l'article R. 6332-2 du code du travail, la composition du conseil d'administration de l'organisme collecteur figure au nombre des renseignements que l'organisme doit fournir dans son dossier de demande d'agrément ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que l'agrément des organismes collecteurs paritaires ne peut être légalement délivré ou renouvelé que si les conditions prévues par les dispositions du premier alinéa de l'article L. 6332-2-1 du code du travail sont satisfaites à la date à laquelle l'agrément est demandé ou qu'est pris l'engagement de les satisfaire dès la délivrance ou le renouvellement de l'agrément ; qu'il ressort des pièces du dossier que plusieurs membres du conseil d'administration du Fonds d'assurance formation des secteurs de la culture, de la communication et des loisirs (AFDAS) ne respectaient pas les dispositions du premier alinéa de l'article L. 6332-2-1 du code du travail à la date à laquelle la demande de renouvellement d'agrément a été présentée, sans que cette demande ait été assortie d'un tel engagement ; que par suite, le ministre chargé de la formation professionnelle ne pouvait légalement renouveler l'agrément à l'AFDAS en application du I de l'article 43 de la loi du 24 novembre 2009 ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède, et sans qu'il soit besoin d'examiner les autres moyens de la requête, que la fédération UNSA spectacle et communication est fondée à demander l'annulation des arrêtés du 9 novembre 2011 en tant qu'ils agréent le l'AFDAS ; <br/>
<br/>
              Sur les conséquences de l'illégalité des arrêtés attaqués :<br/>
<br/>
              6. Considérant que l'annulation d'un acte administratif implique en principe que cet acte est réputé n'être jamais intervenu ; que, toutefois, s'il apparaît que cet effet rétroactif de l'annulation est de nature à emporter des conséquences manifestement excessives en raison tant des effets que cet acte a produits et des situations qui ont pu se constituer lorsqu'il était en vigueur que de l'intérêt général pouvant s'attacher à un maintien temporaire de ses effets, il appartient au juge administratif - après avoir recueilli sur ce point les observations des parties et examiné l'ensemble des moyens, d'ordre public ou invoqués devant lui, pouvant affecter la légalité de l'acte en cause - de prendre en considération, d'une part, les conséquences de la rétroactivité de l'annulation pour les divers intérêts publics ou privés en présence et, d'autre part, les inconvénients que présenterait, au regard du principe de légalité et du droit des justiciables à un recours effectif, une limitation dans le temps des effets de l'annulation ; qu'il lui revient d'apprécier, en rapprochant ces éléments, s'ils peuvent justifier qu'il soit dérogé au principe de l'effet rétroactif des annulations contentieuses et, dans l'affirmative, de prévoir dans sa décision d'annulation que, sous réserve des actions contentieuses engagées à la date de celle-ci contre les actes pris sur le fondement de l'acte en cause, tout ou partie des effets de cet acte antérieurs à son annulation devront être regardés comme définitifs ou même, le cas échéant, que l'annulation ne prendra effet qu'à une date ultérieure qu'il détermine ;<br/>
<br/>
              7. Considérant que l'annulation rétroactive des agréments délivrés à l'AFDAS aurait pour effet de remettre en cause l'effet libératoire des versements réalisés par les entreprises concernées au titre de leur obligation légale en matière de contribution au financement de la formation professionnelle et pourrait provoquer des demandes de remboursement des contributions des entreprises dans un champ d'activité concernant plus de 41 000 entreprises et de 300 000 salariés, pour les années 2012, 2013 ainsi que pour l'année 2014, la collecte au titre de cette dernière année étant déjà intervenue ; qu'il en résulterait une insécurité juridique et financière s'agissant de la restitution éventuelle des fonds collectés au cours des années concernées dont le montant s'est élevé au seul titre de l'année 2012 pour la formation professionnelle continue de ce champ d'activité à 183 349 397 euros pour des montants de prises en charge de plus de 142 millions d'euros ; <br/>
<br/>
              8. Considérant qu'au regard des effets manifestement excessifs qu'emporterait une annulation rétroactive des agréments litigieux, il y a lieu de prévoir que l'annulation prononcée par la présente décision ne prendra effet que le 1er septembre 2014 et que, sous réserve des actions contentieuses engagées à la date de la présente décision contre les actes pris sur leur fondement, les effets produits par les dispositions attaquées antérieurement à leur annulation seront réputés définitifs ;<br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              9. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 250 euros à verser à la fédération UNSA spectacle et communication au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Les arrêtés du 9 novembre 2011 du ministre du travail, de l'emploi et de la santé portant agrément d'organismes collecteurs paritaires des fonds de la formation professionnelle continue au titre du plan et de la professionnalisation en application des articles 1°, 2°, 3° et 4° de l'article L. 6332-7 du code du travail et au titre du congé individuel de formation en application du 5° de  l'article L. 6332-7 du code du travail sont annulés en tant qu'ils agréent le Fonds d'assurance formation des secteurs de la culture, de la communication et des loisirs (AFDAS). Cette annulation prendra effet le 1er septembre 2014.<br/>
Article 2 : Sous réserve des actions contentieuses engagées à la date de la présente décision contre des actes pris sur leur fondement, les effets produits par les arrêtés du 9 novembre 2011 mentionnés à l'article 1er, en tant qu'ils agréent le Fonds d'assurance formation des secteurs de la culture, de la communication et des loisirs (AFDAS), antérieurement à leur annulation sont réputés définitifs.<br/>
Article 3 : L'Etat versera à la fédération UNSA spectacle et communication une somme de 250 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la fédération UNSA spectacle et communication et au ministre du travail, de l'emploi, et du dialogue social.<br/>
Copie en sera adressée pour information au Fonds d'assurance formation des secteurs de la culture, de la communication et des loisirs (AFDAS)<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-06-01-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. ACTES ADMINISTRATIFS - CLASSIFICATION. ACTES RÉGLEMENTAIRES. PRÉSENTENT CE CARACTÈRE. - AGRÉMENT D'UN ORGANISME COLLECTEUR PARITAIRE AGRÉÉ.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">17-05-02-04 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE DU CONSEIL D'ETAT EN PREMIER ET DERNIER RESSORT. ACTES RÉGLEMENTAIRES DES MINISTRES. - AGRÉMENT D'UN ORGANISME COLLECTEUR PARITAIRE AGRÉÉ [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">66-09-01 TRAVAIL ET EMPLOI. FORMATION PROFESSIONNELLE. INSTITUTIONS ET PLANIFICATION DE LA FORMATION PROFESSIONNELLE. - ORGANISMES COLLECTEURS PARITAIRES AGRÉÉS - COMPOSITION - INCOMPATIBILITÉ ENTRE DES FONCTIONS AU SEIN DE L'ORGANISME ET DES FONCTIONS AU SEIN D'UN ÉTABLISSEMENT DE FORMATION (ART. L. 6332-2-1 DU CODE DU TRAVAIL) - CONDITION DEVANT ÊTRE RESPECTÉE À LA DATE DE DEMANDE D'AGRÉMENT, SAUF À CE QUE LE DEMANDEUR S'ENGAGE À CE QU'ELLE SOIT SATISFAITE DÈS LA DÉLIVRANCE OU LE RENOUVELLEMENT DE CELUI-CI - EXISTENCE.
</SCT>
<ANA ID="9A"> 01-01-06-01-01 Eu égard à l'intérêt général de leurs activités, aux obligations qui leur sont imposées et aux contrôles dont ils font l'objet de la part des pouvoirs publics, les organismes collecteurs paritaires agréés sont des personnes de droit privé investies d'une mission de service public. Par suite, la décision d'agrément d'un tel organisme revêt un caractère réglementaire.</ANA>
<ANA ID="9B"> 17-05-02-04 Le Conseil d'Etat est compétent pour connaître en premier et dernier ressort des recours dirigés contre l'arrêté, qui présente un caractère réglementaire, par lequel le ministre chargé de la formation professionnelle agrée un organisme collecteur paritaire agréé.</ANA>
<ANA ID="9C"> 66-09-01 Il résulte des dispositions des articles L. 6332-1 et L. 6332-2-1 du code du travail, éclairées par les travaux parlementaires qui ont précédé l'adoption de la loi n° 2009-1437 du 24 novembre 2009 relative à l'orientation et à la formation professionnelle tout au long de la vie, que l'agrément des organismes collecteurs paritaires ne peut être légalement délivré ou renouvelé que si les conditions prévues par les dispositions du premier alinéa de l'article L. 6332-2-1 du code du travail, qui interdisent le cumul entre les fonctions d'administrateur ou de salarié d'un établissement de formation et la fonction d'administrateur ou de salarié d'un organisme collecteur paritaire agréé, sont satisfaites à la date à laquelle l'agrément est demandé ou que si est pris l'engagement de les satisfaire dès la délivrance ou le renouvellement de l'agrément.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., pour la jurisprudence rendue sous l'empire de la rédaction antérieure du code de justice administrative, fondant la compétence du Conseil d'Etat en premier et dernier ressort sur les effets géographiques de l'acte attaqué, CE, 17 novembre 1997, Fédération générale des clercs et employés de notaires, n° 169814, T. p. 748.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
