<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034230332</ID>
<ANCIEN_ID>JG_L_2017_03_000000391226</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/23/03/CETATEXT000034230332.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 5ème chambres réunies, 20/03/2017, 391226</TITRE>
<DATE_DEC>2017-03-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>391226</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI ; SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>M. Bruno Bachini</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:391226.20170320</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              La société Smart France a demandé au tribunal administratif de Strasbourg d'annuler pour excès de pouvoir les décisions de l'inspecteur du travail de la 5ème section de la Moselle du 15 février 2010 et du ministre du travail, de la solidarité et de la fonction publique du 18 août 2010 lui refusant l'autorisation de licencier M. B...A.... Par un jugement n° 1004889 du 5 novembre 2013, le tribunal administratif a rejeté cette demande. <br/>
<br/>
              Par un arrêt n° 14NC00017 du 21 avril 2015, la cour administrative d'appel de Nancy a, sur appel de la société Smart France, annulé ce jugement ainsi que les décisions du ministre et de l'inspecteur du travail. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 22 juin et 22 septembre 2015 et le 28 avril 2016 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la société Smart France ; <br/>
<br/>
              3°) de mettre à la charge de cette dernière la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Bachini, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de M. A...et à la SCP Gatineau, Fattaccini, avocat de la société Smart France ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 15 février 2010, l'inspecteur du travail de la 5ème section de la Moselle a refusé d'autoriser la société Smart France à licencier, pour motif disciplinaire, M.A..., conducteur d'installation au sein d'un atelier de peinture et ayant la qualité de membre du comité d'hygiène, de sécurité et des conditions de travail, de délégué syndical, de délégué du personnel et de représentant syndical au comité d'entreprise ; que, par une décision du 18 août 2010, le ministre du travail, de la solidarité et de la fonction publique a, sur recours hiérarchique de l'employeur, confirmé ce refus ; que, par un jugement du 5 novembre 2013, le tribunal administratif de Strasbourg a rejeté la demande de la société dirigée contre ces deux décisions ; que, par un arrêt du 21 avril 2015 contre lequel M. A...se pourvoit en cassation, la cour administrative d'appel de Nancy a, sur appel de la société, annulé ce jugement et les décisions attaquées ;  <br/>
<br/>
              2. Considérant qu'il ressort des termes mêmes de l'arrêt attaqué que la cour administrative d'appel s'est saisie de l'ensemble des faits reprochés à M. A...par son employeur dans sa requête d'appel pour juger que, ces faits étant constitutifs de fautes d'une gravité suffisante pour justifier le licenciement de l'intéressé, le refus d'autoriser ce licenciement était entaché d'illégalité ; qu'en statuant ainsi, alors que, saisie de la légalité des décisions de l'inspecteur du travail et du ministre chargé du travail statuant sur des demandes de l'employeur, il lui appartenait seulement de se prononcer sur la régularité de ces décisions et sur le bien-fondé de leurs motifs, la cour a méconnu son office de juge de l'excès de pouvoir ; que, par suite, sans qu'il soit besoin d'examiner les moyens du pourvoi, le requérant est fondé à demander l'annulation de l'arrêt attaqué ; <br/>
<br/>
              3. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier que la société Smart France a, par un courrier du 21 décembre 2009, demandé à l'inspecteur du travail l'autorisation de licencier pour faute M.A..., en faisant valoir qu'il avait organisé, avec plusieurs de ses collègues, durant leur travail, dans la nuit du 13 au 14 novembre 2009, un repas au sein d'un local de maintenance et méconnu, ce faisant, " les règles générales en matière disciplinaire, d'hygiène et de sécurité " ainsi que des dispositions du règlement intérieur ; que l'inspecteur du travail, puis le ministre, saisi du recours hiérarchique formé par la société, ont refusé de délivrer cette autorisation en retenant, d'une part, que le règlement intérieur de la société n'était pas opposable au salarié, faute d'avoir été préalablement soumis aux institutions représentatives du personnel appropriées et, d'autre part, que les seuls faits établis contre lui ne constituaient pas une faute d'une gravité suffisante pour justifier son licenciement ; que l'inspecteur du travail a, en outre, retenu l'existence d'un lien entre le projet de licenciement et les mandats exercés ; <br/>
<br/>
              5. Considérant que les moyens par lesquels la société Smart France critique la régularité du jugement attaqué ne sont pas assortis des précisions permettant d'en apprécier le bien-fondé ;  <br/>
<br/>
              6. Considérant qu'en application des dispositions du code du travail, les salariés légalement investis de fonctions représentatives bénéficient, dans l'intérêt de l'ensemble des salariés qu'ils représentent, d'une protection exceptionnelle ; que lorsque le licenciement de l'un de ces salariés est envisagé, il ne doit pas être en rapport avec les fonctions représentatives normalement exercées ou l'appartenance syndicale ; que, dans le cas où la demande de licenciement est motivée par un comportement fautif, il appartient à l'inspecteur du travail de rechercher, sous le contrôle du juge de l'excès de pouvoir, si les faits reprochés au salarié sont d'une gravité suffisante pour justifier son licenciement, compte tenu de l'ensemble des règles applicables à son contrat de travail et des exigences propres à l'exécution normale du mandat dont il est investi ; <br/>
<br/>
              7. Considérant, en premier lieu, qu'aux termes de l'article L. 1321-1 du code du travail : " Le règlement intérieur est un document écrit par lequel l'employeur fixe exclusivement : / 1° Les mesures d'application de la réglementation en matière de santé et de sécurité dans l'entreprise ou l'établissement (...) " ; qu'aux termes de l'article L. 1321-4 du même code : " Le règlement intérieur ne peut être introduit qu'après avoir été soumis à l'avis du comité d'entreprise ou, à défaut, des délégués du personnel (...)" ; <br/>
<br/>
              8. Considérant qu'il ressort des pièces du dossier qu'à compter du 1er avril 2005, une partie de l'activité de la société SURTEMA, notamment son atelier de peinture, a été transférée à la société Smart France ; que, par un jugement du 25 août 2005, le tribunal d'instance de Sarreguemines a constaté, d'une part, que cet atelier de peinture, dénommé " Paintshop ", dans lequel M. A...travaillait, constituait, au sein de la société Smart France, un établissement distinct pour la désignation des délégués syndicaux et pour l'élection des délégués du personnel et, d'autre part, que les mandats des délégués du personnel élus dans le cadre des dernières élections intervenues au sein de la société SURTEMA pouvaient se poursuivre, au sein de l'atelier " Paintshop ", jusqu'à leur terme ; que, par une décision du 12 décembre 2005, le directeur départemental du travail et de la formation professionnelle de la Moselle a, en outre, reconnu à cet atelier la qualité d'établissement distinct au titre des élections des membres du comité d'entreprise ; que, dès lors, en se bornant à soumettre le projet de règlement intérieur à son comité d'entreprise, le 29 septembre 2005, avant l'installation du comité d'entreprise de l'établissement Paintshop et sans recueillir l'avis des délégués du personnel de cet établissement distinct, la société Smart France a méconnu les dispositions de l'article L. 122-36 du code du travail alors applicables, aujourd'hui reprises à l'article L. 1321-4 du même code ; que, par suite, la société n'est pas fondée à soutenir que l'inspecteur du travail et le ministre n'ont pu légalement estimer, pour ce motif, que son règlement intérieur n'était pas opposable à M. A...; <br/>
<br/>
              9. Considérant, en deuxième lieu, qu'aux termes de l'article R. 2421-10 du code du travail : " La demande d'autorisation de licenciement d'un délégué du personnel, d'un membre du comité d'entreprise ou d'un membre du comité d'hygiène, de sécurité et des conditions de travail est adressée à l'inspecteur du travail dont dépend l'établissement qui l'emploie. / (...) La demande énonce les motifs du licenciement envisagé (...) " ; <br/>
<br/>
              10. Considérant qu'il ressort des pièces du dossier que le courrier du 21 décembre 2009, par lequel la société requérante a demandé l'autorisation de licencier M. A... et qui, ainsi qu'il a été indiqué au point 4,  énonçait avec précision les fautes qui lui étaient reprochées, ne mentionnait pas la méconnaissance, par ce dernier, des prescriptions du document intitulé " B2P - Les bonnes pratiques en production ",  qui font notamment obligation aux salariés employés dans l'atelier de peinture de boire et manger exclusivement dans les zones de repos prévues à cet effet et de ne consommer que de l'eau à leur poste de travail, ni, en tout état de cause, de celles du règlement intérieur de la société SURTEMA ; que, dans ces conditions, la société Smart France ne saurait se prévaloir, pour la première fois devant le juge administratif, de ce que M. A...aurait méconnu certaines des prescriptions fixées par ces deux documents ; <br/>
<br/>
              11. Considérant, en troisième lieu, qu'il ressort des pièces du dossier qu'en organisant, pendant ses heures de travail et en compagnie de cinq autres salariés, un dîner dans un local de maintenance non affecté à cet usage et en y participant, M. A...a méconnu les règles générales d'hygiène et de sécurité qui s'imposaient à lui ; que, toutefois, il est constant que l'intéressé, qui justifiait d'une ancienneté importante et n'avait, jusque-là, jamais fait l'objet d'une sanction disciplinaire, s'est contenté d'apporter de la nourriture sur le lieu de travail sans introduire ni consommer d'alcool ; que, dès lors, l'inspecteur du travail et le ministre n'ont pas fait, dans les circonstances de l'espèce, une inexacte application des dispositions du droit du travail en estimant que la faute commise n'était pas suffisamment grave pour justifier le  licenciement de M. A... ; <br/>
<br/>
              12. Considérant, enfin, qu'il résulte de ce qui a été dit ci-dessus que l'inspecteur du travail ne pouvait légalement autoriser le licenciement demandé ; que, par suite, s'il s'est également fondé, pour refuser l'autorisation sollicitée, sur le motif tiré de ce " qu'il ne pouvait être exclu " que le projet de licenciement soit en rapport avec les mandats exercés par l'intéressé, le bien-fondé de ce motif est, en tout état de cause, sans incidence sur la légalité de sa décision de refus ; <br/>
<br/>
              13. Considérant qu'il résulte de tout ce qui précède que la société Smart France n'est pas fondée à soutenir que c'est à tort que le tribunal administratif de Strasbourg a rejeté sa demande ; <br/>
<br/>
              14. Considérant qu'il y a lieu, dans les circonstances de l'espèce de mettre à la charge de la société Smart France, pour la procédure en cassation et en appel, la somme de 5 000 euros à verser à M.A...  au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font, en revanche, obstacle à ce qu'une somme soit mise à ce titre à la charge de M.A...  qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nancy du 21 avril 2015 est annulé.<br/>
Article 2 : La requête de la société Smart France est rejetée. <br/>
Article 3 : La société Smart France versera à M. A...une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : Les conclusions de la société Smart France présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 5 : La présente décision sera notifiée à M. B...A...et à la société Smart France. <br/>
Copie sera adressée à la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-07-02 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR. - RECOURS CONTRE LES DÉCISIONS DE L'INSPECTEUR DU TRAVAIL ET DU MINISTRE REFUSANT D'AUTORISER LE LICENCIEMENT D'UN SALARIÉ PROTÉGÉ - OFFICE DU JUGE DE L'EXCÈS DE POUVOIR - EXAMEN DE LA VALIDITÉ DU PROJET DE LICENCIEMENT - ABSENCE - EXAMEN DE LA RÉGULARITÉ ET LE BIEN-FONDÉ DES DÉCISIONS ATTAQUÉES - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">66-07-01-05-01 TRAVAIL ET EMPLOI. LICENCIEMENTS. AUTORISATION ADMINISTRATIVE - SALARIÉS PROTÉGÉS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. POUVOIRS DU JUGE. - RECOURS CONTRE LES DÉCISIONS DE L'INSPECTEUR DU TRAVAIL ET DU MINISTRE REFUSANT D'AUTORISER LE LICENCIEMENT D'UN SALARIÉ PROTÉGÉ - OFFICE DU JUGE DE L'EXCÈS DE POUVOIR - JUGE SE PRONONÇANT SUR LA VALIDITÉ DU PROJET DE LICENCIEMENT - ABSENCE - JUGE SE PRONONÇANT SUR LA RÉGULARITÉ ET LE BIEN FONDÉ DES DÉCISIONS ATTAQUÉES - EXISTENCE.
</SCT>
<ANA ID="9A"> 54-07-02 Il appartient seulement au juge de l'excès de pouvoir de se prononcer sur la régularité et le bien-fondé des décisions de l'inspecteur du travail et du ministre refusant d'autoriser le licenciement d'un salarié protégé qui lui sont déférées. Méconnaît par suite son office une cour administrative d'appel qui, s'estimant saisie de l'ensemble des faits reprochés au requérant par son employeur, juge que le refus d'autoriser ce licenciement est justifié.</ANA>
<ANA ID="9B"> 66-07-01-05-01 Il appartient seulement au juge de l'excès de pouvoir de se prononcer sur la régularité et le bien fondé des décisions de l'inspecteur du travail et du ministre refusant d'autoriser le licenciement d'un salarié protégé qui lui sont déférées. Méconnaît par suite son office une cour administrative d'appel qui, s'estimant saisie de l'ensemble des faits reprochés au requérant par son employeur, juge que le refus d'autoriser ce licenciement est justifié.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
