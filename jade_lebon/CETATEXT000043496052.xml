<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043496052</ID>
<ANCIEN_ID>JG_L_2021_05_000000447948</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/49/60/CETATEXT000043496052.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 11/05/2021, 447948</TITRE>
<DATE_DEC>2021-05-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>447948</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Sébastien Ferrari</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:447948.20210511</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le syndicat des copropriétaires du parking transport international routier (TIR) plateforme douanière Borne 9 de Saint-Louis a demandé au juge des référés du tribunal administratif de Strasbourg d'enjoindre au préfet du Haut-Rhin, sur le fondement des dispositions de l'article L. 521-3 du code de justice administrative, de prendre les mesures nécessaires pour faire évacuer les véhicules qui occupent indûment le parking TIR de Saint-Louis. Par une ordonnance n° 2006949 du 7 décembre 2020, ce juge des référés a enjoint au préfet du Haut-Rhin de faire procéder à l'enlèvement des véhicules qui occupent abusivement le parking TIR de Saint-Louis dans un délai de 15 jours.<br/>
<br/>
              Par un pourvoi, enregistré le 17 décembre 2020 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat d'annuler cette ordonnance. Il soutient que le juge des référés du tribunal administratif de Strasbourg a commis une erreur de droit en enjoignant au préfet d'user des pouvoirs généraux de substitution qu'il tient des dispositions du 1° de l'article L. 2215-1 du code général des collectivités territoriales pour prendre les mesures demandées, alors que ces dispositions ne sont pas applicables dans les départements d'Alsace-Moselle.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de la route ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Sébastien Ferrari, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Waquet, Farge, Hazan, avocat du syndicat des copropriétaires du parking TIR ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Strasbourg que le syndicat des copropriétaires du parking TIR de Saint-Louis, à qui la gestion de cette plateforme destinée à permettre les opérations de dédouanement lors du passage des véhicules de fret entre la France et la Suisse a été confiée dans le cadre d'une convention d'occupation du domaine public routier national signée avec l'Etat en 1988, a fait constater par huissier de justice le 6 octobre 2020 la présence sur ce parking d'une trentaine de véhicules automobiles, alors que le stationnement de longue durée y est prohibé.  Par une lettre du 20 décembre 2019, ce syndicat a saisi le maire de la commune de Saint-Louis, sur le fondement des dispositions de l'article L. 417-1 du code de la route, d'une demande tendant à l'enlèvement et à la mise en fourrière des véhicules abandonnés ou en stationnement abusif sur le parking. En l'absence de réponse à cette demande, le syndicat des copropriétaires du parking TIR a saisi le préfet du Haut-Rhin par courrier du 20 octobre 2020 d'une demande tendant à ce qu'il fasse usage, sur le fondement des dispositions de l'article L. 2215-1 du code général des collectivités territoriales, de son pouvoir de substitution pour prendre les mesures nécessaires à l'enlèvement des véhicules. Cette demande étant également demeurée sans suite, le syndicat des copropriétaires a demandé au juge des référés du tribunal administratif de Strasbourg, sur le fondement des dispositions de l'article L. 521-3 du code de justice administrative, d'enjoindre au préfet du Haut-Rhin de prendre les mesures nécessaires à l'enlèvement des véhicules stationnés irrégulièrement sur ce parking, sous astreinte de 250 euros par jour de retard. Par une ordonnance du 7 décembre 2020, ce juge des référés a fait droit à sa demande et enjoint au préfet du Haut-Rhin de prendre les mesures appropriées dans un délai de 15 jours à compter de la notification de son ordonnance.<br/>
<br/>
              2. Aux termes de l'article L. 521-3 du code de justice administrative : " En cas d'urgence et sur simple requête qui sera recevable même en l'absence de décision administrative préalable, le juge des référés peut ordonner toutes autres mesures utiles sans faire obstacle à l'exécution d'aucune décision administrative ". Les mesures ainsi sollicitées ne doivent pas être manifestement insusceptibles de se rattacher à un litige relevant de la compétence de la juridiction administrative.<br/>
<br/>
              3. Aux termes de l'article L. 325-1 du code de la route  : " Les véhicules dont la circulation ou le stationnement en infraction aux dispositions du présent code ou aux règlements de police (...) compromettent la sécurité ou le droit à réparation des usagers de la route, la tranquillité ou l'hygiène publique, l'esthétique des sites et des paysages classés, la conservation ou l'utilisation normale des voies ouvertes à la circulation publique et de leurs dépendances, notamment par les véhicules de transport en commun peuvent à la demande et sous la responsabilité du maire ou de l'officier de police judiciaire territorialement compétent, même sans l'accord du propriétaire du véhicule, dans les cas et conditions précisés par le décret prévu aux articles L. 325-3 et L. 325-11, être immobilisés, mis en fourrière, retirés de la circulation et, le cas échéant, aliénés ou livrés à la destruction ". En vertu de l'article L. 417-1 du même code : " Les véhicules laissés en stationnement en un même point de la voie publique ou de ses dépendances pendant une durée excédant sept jours consécutifs peuvent être mis en fourrière ". <br/>
<br/>
              4. Il résulte de ces dispositions qu'une demande tendant à ce que des véhicules illégalement stationnés sur une dépendance du domaine public routier soient enlevés et mis en fourrière, qui vise à la mise en oeuvre de pouvoirs de police judiciaire, est manifestement insusceptible de se rattacher à un litige relevant de la compétence de la juridiction administrative. Par suite, le juge des référés du tribunal administratif de Strasbourg a méconnu sa compétence en enjoignant au préfet de faire procéder, en exerçant le pouvoir de substitution qu'il tient des dispositions de l'article L. 2215-1 du code général des collectivités territoriales en cas de carence des autorités municipales, au retrait des véhicules stationnant de manière irrégulière sur le parking TIR de Saint-Louis. Il en résulte que l'ordonnance attaquée doit être annulée, sans qu'il soit besoin d'examiner le moyen du pourvoi.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au titre de la procédure de référé engagée, en application de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              6. Ainsi qu'il a été dit au point 4, les litiges relatifs à l'enlèvement et à la mise en fourrière de véhicules illégalement stationnés sur une dépendance du domaine public routier sont relatifs à des opérations de police judiciaire et ressortissent à la compétence du juge judiciaire. Il s'ensuit que la demande présentée par le syndicat des copropriétaires du parking TIR de Saint-Louis devant le juge des référés du tribunal administratif de Strasbourg doit être rejetée comme portée devant une juridiction incompétente pour en connaître.<br/>
<br/>
              7. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 7 décembre 2020 du juge des référés du tribunal administratif de Strasbourg est annulée.<br/>
Article 2 : La demande présentée par le syndicat des copropriétaires du parking TIR devant le juge des référés du tribunal administratif de Strasbourg est rejetée.<br/>
Article 3 : Les conclusions présentées par le syndicat des copropriétaires du parking TIR au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au ministre de l'intérieur et au syndicat des copropriétaires du parking TIR Plateforme Douanière Borne 9 de Saint-Louis.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-03-02-07-05-02 COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE LES DEUX ORDRES DE JURIDICTION. COMPÉTENCE DÉTERMINÉE PAR UN CRITÈRE JURISPRUDENTIEL. PROBLÈMES PARTICULIERS POSÉS PAR CERTAINES CATÉGORIES DE SERVICES PUBLICS. SERVICE PUBLIC JUDICIAIRE. FONCTIONNEMENT. - MISE EN FOURRIÈRE D'UN VÉHICULE - POUVOIR DE POLICE JUDICIAIRE - INCLUSION [RJ1] - CONSÉQUENCE - RÉFÉRÉ-MESURES UTILES (ART. L. 521-3 DU CJA) TENDANT À LA MISE EN FOURRIÈRE D'UN VÉHICULE - MESURE MANIFESTEMENT INSUSCEPTIBLE DE SE RATTACHER À UN LITIGE RELEVANT DE LA COMPÉTENCE DE LA JURIDICTION ADMINISTRATIVE [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">49-01-02 POLICE. POLICE ADMINISTRATIVE ET JUDICIAIRE. NOTION DE POLICE JUDICIAIRE. - INCLUSION - MISE EN FOURRIÈRE D'UN VÉHICULE [RJ1] - CONSÉQUENCE - RÉFÉRÉ-MESURES UTILES (ART. L. 521-3 DU CJA) TENDANT À LA MISE EN FOURRIÈRE D'UN VÉHICULE - MESURE MANIFESTEMENT INSUSCEPTIBLE DE SE RATTACHER À UN LITIGE RELEVANT DE LA COMPÉTENCE DE LA JURIDICTION ADMINISTRATIVE [RJ2].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-035-04-01 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ TENDANT AU PRONONCÉ DE TOUTES MESURES UTILES (ART. L. 521-3 DU CODE DE JUSTICE ADMINISTRATIVE). COMPÉTENCE. - MESURE MANIFESTEMENT INSUSCEPTIBLE DE SE RATTACHER À UN LITIGE RELEVANT DE LA COMPÉTENCE DE LA JURIDICTION ADMINISTRATIVE [RJ2] - INCLUSION - MISE EN FOURRIÈRE D'UN VÉHICULE [RJ1].
</SCT>
<ANA ID="9A"> 17-03-02-07-05-02 Il résulte des articles L. 325-1 et L. 417-1 du code de la route qu'une demande tendant à ce que des véhicules illégalement stationnés sur une dépendance du domaine public routier soient enlevés et mis en fourrière, qui vise à la mise en oeuvre de pouvoirs de police judiciaire, est manifestement insusceptible de se rattacher à un litige relevant de la compétence de la juridiction administrative.,,,Par suite, le juge des référés saisi sur le fondement de l'article L. 521-3 du code de justice administrative (CJA) n'est pas compétent pour enjoindre au préfet de faire procéder, en exerçant le pouvoir de substitution qu'il tient de l'article L. 2215-1 du code général des collectivités territoriales (CGCT) en cas de carence des autorités municipales, au retrait des véhicules stationnant de manière irrégulière sur le domaine public routier.</ANA>
<ANA ID="9B"> 49-01-02 Il résulte des articles L. 325-1 et L. 417-1 du code de la route qu'une demande tendant à ce que des véhicules illégalement stationnés sur une dépendance du domaine public routier soient enlevés et mis en fourrière, qui vise à la mise en oeuvre de pouvoirs de police judiciaire, est manifestement insusceptible de se rattacher à un litige relevant de la compétence de la juridiction administrative.,,,Par suite, le juge des référés saisi sur le fondement de l'article L. 521-3 du code de justice administrative (CJA) n'est pas compétent pour enjoindre au préfet de faire procéder, en exerçant le pouvoir de substitution qu'il tient de l'article L. 2215-1 du code général des collectivités territoriales (CGCT) en cas de carence des autorités municipales, au retrait des véhicules stationnant de manière irrégulière sur le domaine public routier.</ANA>
<ANA ID="9C"> 54-035-04-01 Il résulte des articles L. 325-1 et L. 417-1 du code de la route qu'une demande tendant à ce que des véhicules illégalement stationnés sur une dépendance du domaine public routier soient enlevés et mis en fourrière, qui vise à la mise en oeuvre de pouvoirs de police judiciaire, est manifestement insusceptible de se rattacher à un litige relevant de la compétence de la juridiction administrative.,,,Par suite, le juge des référés saisi sur le fondement de l'article L. 521-3 du code de justice administrative (CJA) n'est pas compétent pour enjoindre au préfet de faire procéder, en exerçant le pouvoir de substitution qu'il tient de l'article L. 2215-1 du code général des collectivités territoriales (CGCT) en cas de carence des autorités municipales, au retrait des véhicules stationnant de manière irrégulière sur le domaine public routier.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 18 janvier 1924, Dame veuve,, n° 73651, p. 61 ; CE, 18 mars 1981, Consorts,, n° 17502, p. 148 ; CE, 12 avril 1995, Mme,, n° 125153, p. 61.,,[RJ2] Cf. CE, 16 février 1996, S.A.R.L. Bretagne Desosse, n° 165537, p. 44 ; CE, 22 octobre 2010,,, n° 335051, p. 420.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
