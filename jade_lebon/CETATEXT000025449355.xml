<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025449355</ID>
<ANCIEN_ID>JG_L_2012_03_000000354898</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/44/93/CETATEXT000025449355.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 01/03/2012, 354898, Publié au recueil Lebon</TITRE>
<DATE_DEC>2012-03-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>354898</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Francis Girault</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CESSR:2012:354898.20120301</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le jugement n° 0902837 du 15 novembre 2011, enregistré le 15 décembre 2011 au secrétariat du contentieux du Conseil d'Etat, par lequel le tribunal administratif d'Orléans, avant de statuer sur la demande de Mme Annette A, demeurant ... tendant à la condamnation de la commune de Semblançay à lui verser des sommes correspondant à la prise en charge de frais d'hospitalisation et de frais de transport, a décidé, par application des dispositions de l'article L. 113-1 du code de justice administrative de transmettre le dossier de cette demande au Conseil d'Etat, en soumettant à son examen la question de savoir si la prise en charge des frais médicaux et frais de déplacement rendus nécessaires par un accident reconnu imputable au service ou par une rechute d'un accident reconnu imputable au service est réservée aux seuls agents en activité au moment des soins ou si l'administration employeur au moment de l'accident est tenue de prendre en charge les frais postérieurement à la mise à la retraite de l'agent concerné ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des pensions civiles et militaires de retraite ;<br/>
<br/>
              Vu la loi n° 84-53 du 26 janvier 1984 ;<br/>
<br/>
              Vu le code de justice administrative, notamment son article L. 113-1 ; <br/>
<br/>
<br/>
               Après avoir entendu en séance publique :<br/>
<br/>
- le rapport de M. Francis Girault, Maître des Requêtes, <br/>
- les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              		REND L'AVIS SUIVANT :<br/>
<br/>
              Il  résulte des dispositions du 2° de l'article 57 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale que, lorsque la maladie provient de l'une des causes exceptionnelles prévues à l'article L. 27 du code des pensions civiles et militaires de retraite, lequel mentionne notamment les maladies contractées ou aggravées en service, ou d'un accident survenu dans l'exercice ou à l'occasion de l'exercice de ses fonctions, le fonctionnaire a droit au remboursement des honoraires médicaux et des frais directement entraînés par la maladie ou l'accident. Ces dispositions, qui s'inspirent du principe selon lequel l'administration doit garantir ses agents contre les dommages qu'ils peuvent subir dans l'accomplissement de leur service, s'appliquent à l'agent qui n'est plus en activité,  alors même que le premier alinéa du même article 57 mentionne les " fonctionnaires en activité ". Par suite, les agents radiés des cadres peuvent prétendre à la prise en charge des honoraires médicaux et frais directement exposés à la suite d'une maladie professionnelle ou d'un accident reconnu imputable au service. L'administration employeur à la date de l'accident ou au cours de la période à laquelle se rattache la maladie professionnelle est ainsi tenue de prendre en charge les honoraires et les frais exposés à ce titre postérieurement à la mise en retraite de l'agent.<br/>
<br/>
              Le présent avis sera notifié au tribunal administratif d'Orléans, à Madame Annette A et au ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-07-01-03 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. STATUT GÉNÉRAL DES FONCTIONNAIRES DE L'ÉTAT ET DES COLLECTIVITÉS LOCALES. DISPOSITIONS STATUTAIRES RELATIVES À LA FONCTION PUBLIQUE TERRITORIALE (LOI DU 26 JANVIER 1984). - MALADIE - MALADIE PROFESSIONNELLE OU ACCIDENT IMPUTABLE AU SERVICE - REMBOURSEMENT DES HONORAIRES MÉDICAUX ET FRAIS DIRECTEMENT ENTRAÎNÉS PAR LA MALADIE OU L'ACCIDENT (2° DE L'ART. 57, DANS SA RÉDACTION ANTÉRIEURE À LA LOI N° 2012-347 DU 12 MARS 2012) - CHAMP - FONCTIONNAIRE N'ÉTANT PLUS EN ACTIVITÉ AU MOMENT DES SOINS - INCLUSION [RJ1].
</SCT>
<ANA ID="9A"> 36-07-01-03 Les dispositions du 2° de l'article 57 de la loi n° 84-53 du 26 janvier 1984, qui prévoient, en cas de maladie provenant de l'une des causes exceptionnelles prévues à l'article L. 27 du code des pensions civiles et militaires de retraite ou d'un accident survenu dans ou à l'occasion de l'exercice des fonctions, le remboursement au fonctionnaire des honoraires médicaux et des frais directement entraînés par la maladie ou l'accident s'appliquent à l'agent qui n'est plus en activité, alors même que le premier alinéa du même article 57 mentionne les « fonctionnaires en activité ». L'administration employeur à la date de l'accident ou au cours de la période à laquelle se rattache la maladie professionnelle est ainsi tenue de prendre en charge les honoraires et les frais exposés à ce titre postérieurement à la sortie de service de l'agent.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., pour les magistrats, CE, 5 juillet 1999, Varin, n° 191517, T. p. 868.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
