<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036791208</ID>
<ANCIEN_ID>JG_L_2018_04_000000409648</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/79/12/CETATEXT000036791208.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 11/04/2018, 409648, Publié au recueil Lebon</TITRE>
<DATE_DEC>2018-04-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>409648</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Recours ds l'intérêt de la loi</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Louise Bréhier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:409648.20180411</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Procédure contentieuse antérieure<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Rennes d'annuler l'arrêté du 3 septembre 2004 par lequel le préfet du Morbihan a prorogé la déclaration d'utilité publique du projet de désenclavement de la commune d'Inzinzac-Lochrist sur les communes d'Hennebont, de Caudan et d'Inzinzac-Lochrist. Par un jugement n° 04-3960 du 12 septembre 2006, le tribunal administratif a rejeté cette demande. <br/>
<br/>
              Par un arrêt n° 06NT01922 du 16 octobre 2007, la cour administrative d'appel de Nantes a rejeté l'appel formé par M. B...contre ce jugement. <br/>
<br/>
              Par une décision n° 311999 du 14 octobre 2009, le Conseil d'Etat, statuant au contentieux a rejeté le pourvoi formé par M. B...contre cet arrêt. <br/>
<br/>
              Recours dans l'intérêt de la loi<br/>
<br/>
              Par un recours, enregistré le 10 avril 2017 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat d'annuler, dans l'intérêt de la loi, l'arrêt n° 06NT01922 du 16 octobre 2007 de la cour administrative d'appel de Nantes en tant qu'il juge que l'article L. 11-5 du code de l'expropriation pour cause d'utilité publique ne fait pas obstacle à ce que la demande de prorogation de la déclaration d'utilité publique d'un projet routier du département soit présentée par le président du conseil général et non par une délibération du conseil général. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Louise Bréhier, auditrice,<br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'en vertu des principes généraux de procédure, un recours peut être formé dans l'intérêt de la loi par un ministre intéressé devant le Conseil d'Etat contre tout jugement d'une juridiction administrative ayant acquis l'autorité de chose jugée, dès lors que ce jugement est devenu irrévocable ; que, si le jugement ainsi mis en cause avait été déféré au Conseil d'Etat par les parties intéressées, il ne peut être critiqué par le recours formé dans l'intérêt de la loi que dans la mesure où le Conseil d'Etat, statuant sur le recours des parties, ne s'est pas déjà prononcé ; <br/>
<br/>
              2.	Considérant que, par un arrêté du 3 septembre 2004, le préfet du Morbihan a prorogé la déclaration d'utilité publique d'un projet poursuivi par le département du Morbihan visant au désenclavement routier de la commune d'Inzinzac-Lochrist ; que M. B...a demandé au tribunal administratif de Rennes d'annuler pour excès de pouvoir cet arrêté ; que, par un jugement du 12 septembre 2006, le tribunal a rejeté sa demande ; que, par un arrêt du 16 octobre 2007, la cour administrative d'appel de Nantes a rejeté l'appel formé par M. B...contre ce jugement ; que, par une décision du 14 octobre 2009, le Conseil d'Etat, statuant au contentieux a rejeté le pourvoi en cassation formé par M. B...contre l'arrêt de la cour administrative d'appel de Nantes ; <br/>
<br/>
              3.	Considérant que le présent recours formé par le ministre de l'intérieur tend à l'annulation dans l'intérêt de la loi de l'arrêt de la cour administrative d'appel de Nantes du 16 octobre 2007 en tant seulement qu'il juge que le II de l'article L. 11-5 du code de l'expropriation pour cause d'utilité publique ne faisait pas obstacle à ce qu'une demande de prorogation d'une déclaration d'utilité publique soit présentée par le président du conseil général d'un département et non par une délibération du conseil général ; que la décision rendue le 14 octobre 2009 par le Conseil d'Etat, statuant au contentieux ne s'est pas prononcée sur les motifs de l'arrêt critiqués par le recours formé dans l'intérêt de la loi ; que le recours du ministre est, par suite, recevable ; <br/>
<br/>
              4. Considérant qu'aux termes de l'article L. 11-1 du code de l'expropriation pour cause d'utilité publique, dans sa rédaction applicable au litige jugé par l'arrêt critiqué : " L'expropriation d'immeubles, en tout ou partie, ou de droits réels immobiliers, ne peut être prononcée qu'autant qu'elle aura été précédée d'une déclaration d'utilité publique intervenue à la suite d'une enquête et qu'il aura été procédé contradictoirement à la détermination des parcelles à exproprier, ainsi qu'à la recherche des propriétaires, des titulaires de droits réels et des autres intéressés " ; qu'aux termes du deuxième alinéa du II de l'article L. 11-5 de ce code, dans sa rédaction applicable au litige jugé par l'arrêt critiqué : " Lorsque le délai accordé pour réaliser l'expropriation n'est pas supérieur à cinq ans, un acte pris dans la même forme que l'acte déclarant l'utilité publique peut, sans nouvelle enquête, proroger une fois les effets de la déclaration d'utilité publique pour une durée au plus égale " ; qu'aux termes de l'article L. 3211-1 du code général des collectivités territoriales, dans sa rédaction alors applicable : " Le conseil général règle par ses délibérations les affaires du département (...) " ; qu'aux termes de l'article L. 3221-1 du même code : " Le président du conseil général est l'organe exécutif du département. / Il prépare et exécute les délibérations du conseil général " ; <br/>
<br/>
              5. Considérant qu'il résulte des dispositions précitées du code général des collectivités territoriales  qu'une demande de prorogation d'un acte déclarant d'utilité publique une opération poursuivie par un département ne peut émaner que d'une délibération du conseil départemental ; <br/>
<br/>
              6. Considérant, toutefois, que les dispositions du II de l'article L. 11-5 du code de l'expropriation pour cause d'utilité publique ne font pas obligation à l'administration, lorsqu'elle entend faire usage de la faculté qu'elle tient de ces dispositions de proroger les effets d'un acte déclarant l'utilité publique un projet, de procéder aux formalités prévues pour l'édiction de cet acte ; qu'elles impliquent seulement que l'acte prononçant la prorogation émane de l'autorité qui était compétente, en vertu de l'article L. 11-2 du même code, pour déclarer l'utilité publique ; que, par suite, en se fondant sur un tel motif pour écarter le moyen dont elle était saisie, qui était seulement tiré de la violation des dispositions du deuxième alinéa du II de l'article L. 11-5 du code de l'expropriation pour cause d'utilité publique, la cour administrative d'appel n'a pas commis d'erreur de droit ; <br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que le ministre de l'intérieur n'est pas fondé à demander, dans l'intérêt de la loi, l'annulation de l'arrêt de la cour administrative d'appel de Nantes du 16 octobre 2007 en tant qu'il juge que l'article L. 11-5 du code de l'expropriation pour cause d'utilité publique ne fait pas obstacle à qu'une demande de prorogation d'une déclaration d'utilité publique soit présentée par le président du conseil départemental d'un département et non par une délibération du conseil départemental ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le recours du ministre d'Etat, ministre de l'intérieur est rejeté. <br/>
<br/>
Article 2 : La présente décision sera notifiée au ministre d'Etat, ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-03-01-02-01-03 COLLECTIVITÉS TERRITORIALES. DÉPARTEMENT. ORGANISATION DU DÉPARTEMENT. ORGANES DU DÉPARTEMENT. CONSEIL GÉNÉRAL. COMPÉTENCES. - ACTE DÉCLARANT D'UTILITÉ PUBLIQUE UNE OPÉRATION DU DÉPARTEMENT - COMPÉTENCE POUR EN DEMANDER LA PROROGATION - ORGANE DÉLIBÉRANT DU CONSEIL GÉNÉRAL [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">34-02-02-03 EXPROPRIATION POUR CAUSE D'UTILITÉ PUBLIQUE. RÈGLES GÉNÉRALES DE LA PROCÉDURE NORMALE. ACTE DÉCLARATIF D'UTILITÉ PUBLIQUE. PROROGATION. - ACTE DÉCLARANT D'UTILITÉ PUBLIQUE UNE OPÉRATION DU DÉPARTEMENT - COMPÉTENCE POUR EN DEMANDER LA PROROGATION - ORGANE DÉLIBÉRANT DU CONSEIL GÉNÉRAL [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-08-07 PROCÉDURE. VOIES DE RECOURS. RECOURS DANS L'INTÉRÊT DE LA LOI. - RECEVABILITÉ - CAS DANS LEQUEL LE CONSEIL D'ETAT S'EST PRONONCÉ SUR UN RECOURS CONTRE UNE DÉCISION JURIDICTIONNELLE FAISANT L'OBJET D'UN RECOURS DANS L'INTÉRÊT DE LA LOI - 1) PRINCIPE - EXISTENCE, DANS LA MESURE OÙ IL NE S'EST PAS DÉJÀ PRONONCÉ SUR LE POINT EN CAUSE DANS LE RECOURS DANS L'INTÉRÊT DE LA LOI - 2) ESPÈCE - HYPOTHÈSE DANS LAQUELLE LE CONSEIL D'ETAT NE S'EST PAS PRONONCÉ SUR LES MOTIFS DE L'ARRÊT CRITIQUÉS PAR LE RECOURS DANS L'INTÉRÊT DE LA LOI [RJ2].
</SCT>
<ANA ID="9A"> 135-03-01-02-01-03 Il résulte de l'article L. 3211-1 du code général des collectivités territoriales (CGCT) qu'une demande de prorogation d'un acte déclarant d'utilité publique une opération poursuivie par un département ne peut émaner que d'une délibération du conseil général.</ANA>
<ANA ID="9B"> 34-02-02-03 Il résulte de l'article L. 3211-1 du code général des collectivités territoriales (CGCT) qu'une demande de prorogation d'un acte déclarant d'utilité publique une opération poursuivie par un département ne peut émaner que d'une délibération du conseil général.</ANA>
<ANA ID="9C"> 54-08-07 1) En vertu des principes généraux de procédure, un recours peut être formé dans l'intérêt de la loi par un ministre intéressé devant le Conseil d'Etat contre tout jugement d'une juridiction administrative ayant acquis l'autorité de chose jugée, dès lors que ce jugement est devenu irrévocable. Si le jugement ainsi mis en cause avait été déféré au Conseil d'Etat par les parties intéressées, il ne peut être critiqué par le recours formé dans l'intérêt de la loi que dans la mesure où le Conseil d'Etat, statuant sur le recours des parties, ne s'est pas déjà prononcé.... ,,2) Recours formé par le ministre de l'intérieur tendant à l'annulation dans l'intérêt de la loi de l'arrêt d'une cour administrative d'appel en tant seulement  qu'il juge que le II de l'article L. 11-5 du code de l'expropriation pour cause d'utilité publique ne faisait pas obstacle à ce qu'une demande de prorogation d'une déclaration d'utilité publique soit présentée par le président du conseil général d'un département et non par une délibération du conseil général. La décision rendue par le Conseil d'Etat, statuant au contentieux ne s'est pas prononcée sur les motifs de l'arrêt critiqués par le recours formé dans l'intérêt de la loi. Le recours du ministre est, par suite, recevable.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 14 avril 1999, Association de défense des propriétaires et exploitants agricoles du technopôle de Château-Gombert, n° 193497, T. pp. 835-836-973.,,[RJ2] Cf., s'agissant de la possibilité d'exercer un recours dans l'intérêt de la loi contre les motifs d'une décision juridictionnelle, CE, 1er octobre 1997, Ministre de la défense, n° 180661, p. 324.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
