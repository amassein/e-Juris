<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000024448259</ID>
<ANCIEN_ID>JG_L_2011_07_000000320457</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/24/44/82/CETATEXT000024448259.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 26/07/2011, 320457, Publié au recueil Lebon</TITRE>
<DATE_DEC>2011-07-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>320457</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Jacques Arrighi de Casanova</PRESIDENT>
<AVOCATS>SCP THOUIN-PALAT, BOUCARD ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Jean Lessi</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 8 septembre et 8 décembre 2008 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SOCIETE INNOV IMMO, dont le siège est 95, rue Saint-Dominique à Paris (75007), représentée par son gérant en exercice, la SOCIETE IMMOSUD SA, dont le siège est 3, rue du Colonel-Moll à Paris (75017), et la SOCIETE D'ACHATS ET DE VENTES D'IMMEUBLES - SAVI, dont le siège est 266, avenue Daumesnil à Paris (75012), représentée par son gérant en exercice ; la SOCIETE INNOV IMMO et autres demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 07PA03281-07PA03282 du 8 juillet 2008 par lequel la cour administrative d'appel de Paris, d'une part, a annulé le jugement n° 0301634/4 du 31 mai 2007 par lequel le tribunal administratif de Melun avait fait droit à leur demande dirigée contre la délibération du 26 février 2003 par laquelle le conseil municipal de la commune de Boissise-le-Roi a approuvé le dossier de création de la zone d'aménagement concerté " Orgenoy Est " et rejeté leur demande présentée devant le tribunal, d'autre part, a décidé qu'il n'y avait pas lieu de statuer sur les conclusions tendant à ce qu'il soit sursis à l'exécution de ce jugement ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la commune de Boissise-le-Roi ; <br/>
<br/>
              3°) de mettre à la charge de la commune de Boissise-le-Roi la somme totale de 5 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des collectivités territoriales ; <br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu la loi n° 2000-1208 du 13 décembre 2000 ;<br/>
<br/>
              Vu le décret n° 77-1141 du 12 octobre 1977 ;<br/>
<br/>
              Vu le décret n° 2001-1208 du 27 mars 2001 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean Lessi, Auditeur,  <br/>
<br/>
              - les observations de la SCP Thouin-Palat, Boucard, avocat de la SOCIETE INNOV IMMO et autres et de la SCP Waquet, Farge, Hazan, avocat de la commune de Boissise-le-Roi, <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Thouin-Palat, Boucard, avocat de la SOCIETE INNOV IMMO et autres et à la SCP Waquet, Farge, Hazan, avocat de la commune de Boissise-le-Roi ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'il résulte de l'article R. 311-2 du code de l'urbanisme, dans sa rédaction issue du décret du 27 mars 2001 relatif aux zones d'aménagement concerté applicable en l'espèce, que le dossier de création d'une zone d'aménagement concerté comprend " (...) d) L'étude d'impact définie à l'article 2 du décret du 12 octobre 1977 modifié " ; que si ces dispositions, qui impliquent que la création de toute zone d'aménagement concerté soit précédée d'une étude d'impact, renvoient pour la définition de cette dernière à l'article 2 du décret du 12 octobre 1977 pris pour l'application de l'article 2 de la loi n° 76-629 du 10 juillet 1976 relative à la protection de la nature, désormais codifié à l'article R. 122-3 du code de l'environnement, elles ont, en revanche, implicitement mais nécessairement eu pour effet d'abroger les dispositions du 4° de l'annexe II et du 10° de l'annexe III de ce même décret, auxquelles renvoie son article 3, qui dispensaient de l'obligation de réaliser une telle étude d'impact dans le cas, prévu au dernier alinéa de l'ancienne rédaction de l'article L. 311-4 du code de l'urbanisme, où l'acte de création de la zone d'aménagement concerté décidait de maintenir en vigueur les dispositions du plan d'occupation des sols rendu public ou approuvé ;<br/>
<br/>
              Considérant que, pour écarter le moyen tiré de ce que la délibération du 26 février 2003, par laquelle le conseil municipal de la commune de Boissise-le-Roi a approuvé la création de la zone d'aménagement concerté dite " Orgenoy Est ", était illégale en raison de l'insuffisance de l'étude d'impact qui l'avait précédée, la cour administrative d'appel de Paris s'est fondée sur ce que, en vertu des dispositions combinées de l'article 3 du décret du 12 octobre 1977, du 4° de l'annexe II et du 10° de l'annexe III de ce même décret, aucune étude d'impact n'avait à précéder la création d'une zone d'aménagement concertée qui, comme en l'espèce, ne prévoyait pas l'édiction d'autres règles d'urbanisme que celles fixées par le plan d'occupation des sols en vigueur ; qu'il résulte de ce qui vient d'être dit qu'en statuant ainsi, la cour a commis une erreur de droit ; que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt doit, par suite, être annulé ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Considérant qu'il résulte de l'article L. 311-1 du code de l'urbanisme applicable en l'espèce qui, à la différence de ce qui résultait de l'état du droit antérieur à l'entrée en vigueur de la loi du 13 décembre 2000 relative à la solidarité et au renouvellement urbains, ne limite plus la possibilité de créer des zones d'aménagement concerté, sur les territoires couverts par un plan d'occupation des sols ou un plan local d'urbanisme, aux seules zones urbaines ou d'urbanisation future, que : " Les zones d'aménagement concerté sont les zones à l'intérieur desquelles une collectivité publique ou un établissement public y ayant vocation décide d'intervenir pour réaliser ou faire réaliser l'aménagement et l'équipement des terrains (...) / Le périmètre et le programme de la zone d'aménagement concerté sont approuvés par délibération du conseil municipal ou de l'organe délibérant de l'établissement public de coopération intercommunale (...) " ; qu'aux termes du premier alinéa de l'article R. 311-5 du même code, dans sa rédaction issue du décret du 27 mars 2001 : " L'acte qui crée la zone d'aménagement concerté en délimite le ou les périmètres. Il indique le programme global prévisionnel des constructions à édifier à l'intérieur de la zone (...) " ; qu'aux termes du premier alinéa de l'article R. 311-6 de ce code issu du même décret : " L'aménagement et l'équipement de la zone sont réalisés dans le respect des règles d'urbanisme applicables. Lorsque la commune est couverte par un plan local d'urbanisme, la réalisation de la zone d'aménagement concerté est subordonnée au respect de l'article L. 123-3 " ; qu'il résulte de l'ensemble de ces dispositions que, si les équipements et aménagements d'une zone d'aménagement concerté doivent être réalisés dans le respect des dispositions du règlement du plan local d'urbanisme ou du plan d'occupation des sols applicables au moment de leur réalisation, ces mêmes règles ne s'imposent pas, en revanche, à l'acte de création de la zone ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que c'est à tort que le tribunal administratif s'est fondé sur la violation des prescriptions de l'article 1NA5 du règlement du plan d'occupation des sols de la commune de Boissise-le-Roi pour annuler la délibération du 26 février 2003 du conseil municipal de cette commune mentionnée précédemment ;<br/>
<br/>
              Considérant, toutefois, qu'il appartient au Conseil d'Etat, saisi de l'ensemble du litige par l'effet dévolutif de l'appel, d'examiner les autres moyens soulevés par les sociétés INNOV IMMO, IMMOSUD et SAVI devant le tribunal administratif de Melun ;<br/>
<br/>
              Considérant, en premier lieu, d'une part, qu'aux termes de l'article L. 2121-12 du code général des collectivités territoriales : " Dans les communes de 3 500 habitants et plus, une note explicative de synthèse sur les affaires soumises à délibération doit être adressée avec la convocation aux membres du conseil municipal (...) " ; que le défaut d'envoi de cette note entache d'irrégularité les délibérations prises, à moins que les conseillers municipaux n'aient été rendus destinataires, en même temps que de la convocation, de documents leur permettant de disposer d'une information équivalente ; qu'il ressort des pièces du dossier que la convocation en vue du conseil municipal du 26 février 2003 était accompagnée du projet de délibération portant création de la zone d'aménagement concerté " Orgenoy Est ", qui rappelait les objectifs poursuivis et les principales lignes du projet ; qu'eu égard à la taille de la commune, ce projet de délibération, compte tenu des termes dans lesquels il est rédigé, doit être regardé, dans les circonstances de l'espèce, comme répondant par lui-même aux exigences d'information résultant de l'article L. 2121-12 ; <br/>
<br/>
              Considérant, d'autre part, que la circonstance que le maire, saisi d'une demande de délivrance d'une copie du dossier de création de la zone d'aménagement concertée par une personne qui, si elle avait par ailleurs la qualité de conseiller municipal, prétendait agir au nom d'une association, ait refusé de fournir ce document, n'est pas de nature à caractériser une violation des dispositions de l'article L. 2121-13 du même code selon lesquelles " tout membre du conseil municipal a le droit, dans le cadre de sa fonction, d'être informé des affaires de la commune qui font l'objet d'une délibération " ;<br/>
<br/>
              Considérant, en deuxième lieu, qu'il ressort des pièces du dossier que le projet de création de zone d'aménagement concerté a donné lieu à une concertation comprenant la diffusion d'une plaquette d'information en juin 2002, la tenue de plusieurs réunions d'un groupe de travail incluant des représentants d'associations, ainsi que de la commission d'urbanisme, et l'organisation d'une exposition publique de juin à novembre 2002 au cours de laquelle il n'est pas contesté qu'un registre a été mis à la disposition du public afin de recueillir ses observations ; que, dans ces conditions, le moyen tiré de ce que la concertation aurait été insuffisante au regard des dispositions de l'article L. 300-2 du code de l'urbanisme doit, en tout état de cause, être écarté ;<br/>
<br/>
              Considérant, en troisième lieu, que si, selon les sociétés requérantes, l'étude d'impact jointe au dossier de création de cette même zone d'aménagement concerté a omis de mentionner la présence de deux parcelles polluées, la commune soutient sans être contredite que ces terrains sont situés en dehors du périmètre de la zone d'aménagement concerté ; que, par ailleurs, conformément aux dispositions du 3° de l'article 2 du décret du 12 octobre 1977 selon lesquelles l'étude d'impact indique " Les raisons pour lesquelles, notamment du point de vue des préoccupations d'environnement, parmi les partis envisagés qui feront l'objet d'une description, le projet présenté a été retenu ", cette étude comporte en l'espèce la description d'un scénario alternatif ainsi que les raisons pour lesquelles le scénario finalement retenu lui a été préféré ; que la présentation des méthodes d'évaluation des effets du projet répond aux exigences du 5° de ce même article 2 ; qu'eu égard aux caractéristiques de l'opération d'aménagement qui, contrairement à ce qui est soutenu, comporte en elle-même les précautions destinées à en atténuer ou à en compenser les conséquences éventuelles sur l'environnement, l'absence dans l'étude d'impact d'une estimation spécifique des dépenses correspondantes ne saurait entacher d'illégalité l'acte créant la zone d'aménagement concerté ; qu'il suit de là que le moyen tiré de ce que l'étude d'impact serait insuffisante au regard des dispositions de l'article 2 du décret du 12 octobre 1977 doit être écarté ;<br/>
<br/>
              Considérant, en quatrième lieu, que la délibération du 26 février 2003 approuvant la création d'une zone d'aménagement concerté " Orgenoy Est " n'a pas été prise en application de la délibération du 20 décembre 2001 par laquelle le conseil municipal a désigné un groupement de sociétés afin d'engager les études préalables à l'aménagement de cette zone, laquelle ne constitue pas non plus la base légale de la délibération attaquée ; que le moyen tiré, par voie d'exception, de l'illégalité de la délibération du 20 décembre 2001 doit, par suite, être écarté comme inopérant ; <br/>
<br/>
              Considérant, en cinquième lieu, qu'il ressort des pièces du dossier que l'opération projetée a pour objet la réalisation d'environ 150 logements et d'équipements sportifs collectifs, ainsi que l'aménagement de dessertes routières et d'abords paysagers sur une zone d'une superficie totale d'environ 18 hectares ; que, contrairement à ce que soutiennent les sociétés requérantes, une telle opération constitue une opération d'aménagement et d'équipement au sens des dispositions de l'article L. 311-1 du code de l'urbanisme, nonobstant la circonstance que la commune, à la date de la délibération attaquée, ne soit pas propriétaire d'une partie importante des terrains d'assiette de la zone ;<br/>
<br/>
              Considérant, enfin, que le détournement de pouvoir allégué n'est pas établi ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que la commune de Boissise-le-Roi est fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Melun a annulé la délibération du 26 février 2003 ;<br/>
<br/>
              Considérant que la présente décision statue sur la requête en annulation du jugement du 31 mai 2007 du tribunal administratif de Melun ; que, par suite, les conclusions à fin de sursis à exécution de ce jugement sont devenues sans objet ;<br/>
<br/>
              Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de Boissise-le-Roi qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre solidairement à la charge des sociétés INNOV-IMMO, IMMOSUD et SAVI la somme de 5 000 euros à verser à la commune de Boissise-le-Roi au titre de ces mêmes dispositions en première instance, en appel et en cassation ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 8 juillet 2008 et le jugement du tribunal administratif de Melun du 31 mai 2007, sauf en ce qu'il statue sur les conclusions à fin d'application de l'article L. 741-2 du code de justice administrative, sont annulés.<br/>
Article 2 : La demande présentée par les sociétés INNOV IMMO, IMMOSUD et SAVI devant le tribunal administratif de Melun est rejetée.<br/>
Article 3 : Il n'y a pas lieu de statuer sur les conclusions à fin de sursis à exécution du jugement du tribunal administratif de Melun du 31 mai 2007.<br/>
Article 4 : Les sociétés INNOV-IMMO, IMMOSUD et SAVI verseront solidairement à la commune de Boissise-le-Roi une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : Les conclusions des sociétés INNOV-IMMO, IMMOSUD et SAVI présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 6 : La présente décision sera notifiée à la SOCIETE INNOV-IMMO, à la SOCIETE IMMOSUD SA, à la SOCIETE D'ACHATS ET DE VENTES D'IMMEUBLES - SAVI, à la commune de Boissise-le-Roi et à la ministre de l'écologie, du développement durable, des transports et du logement.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-09-02-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DISPARITION DE L'ACTE. ABROGATION. ABROGATION DES ACTES RÉGLEMENTAIRES. - ABROGATION IMPLICITE, PAR LES DISPOSITIONS ISSUES DU DÉCRET DU 27 MARS 2001 RELATIF AUX ZAC, DES DISPOSITIONS DU DÉCRET DU 12 OCTOBRE 1977 PRÉVOYANT UNE DISPENSE D'ÉTUDE D'IMPACT POUR LA CRÉATION D'UNE ZAC DANS CERTAINS CAS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-01-01-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PLANS D'AMÉNAGEMENT ET D'URBANISME. PLANS D'OCCUPATION DES SOLS ET PLANS LOCAUX D'URBANISME. APPLICATION DES RÈGLES FIXÉES PAR LES POS OU LES PLU. - ACTE DE CRÉATION D'UNE ZAC - INAPPLICABILITÉ DES RÈGLES POSÉES PAR LES DISPOSITIONS DU PLU OU DU POS, DONT LE RESPECT NE S'IMPOSE QU'À LA RÉALISATION DES ÉQUIPEMENTS ET AMÉNAGEMENT  DE LA ZAC.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">68-02-02-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PROCÉDURES D'INTERVENTION FONCIÈRE. OPÉRATIONS D'AMÉNAGEMENT URBAIN. ZONES D'AMÉNAGEMENT CONCERTÉ (ZAC). - ACTE DE CRÉATION D'UNE ZAC - 1)  INAPPLICABILITÉ DES RÈGLES POSÉES PAR LES DISPOSITIONS DU PLU OU DU POS, DONT LE RESPECT NE S'IMPOSE QU'À LA RÉALISATION DES ÉQUIPEMENTS ET AMÉNAGEMENT  DE LA ZAC - 2) OBLIGATION DE PROCÉDER À UNE ÉTUDE D'IMPACT - EXISTENCE - ABROGATION IMPLICITE, PAR LES DISPOSITIONS ISSUES DU DÉCRET DU 27 MARS 2001 RELATIF AUX ZAC, DES DISPOSITIONS DU DÉCRET DU 12 OCTOBRE 1977 PRÉVOYANT UNE DISPENSE D'ÉTUDE D'IMPACT POUR LA CRÉATION D'UNE ZAC DANS CERTAINS CAS.
</SCT>
<ANA ID="9A"> 01-09-02-01 Il résulte de l'article R. 311-2 du code de l'urbanisme, dans sa rédaction issue du décret n° 2001-1208 du 27 mars 2001 relatif aux zones d'aménagement concerté (ZAC) que le dossier de création d'une ZAC comprend « (&#133;) d) L'étude d'impact définie à l'article 2 du décret du 12 octobre 1977 modifié ». Si ces dispositions, qui impliquent que la création de toute ZAC soit précédée d'une étude d'impact, renvoient pour la définition de cette dernière à l'article 2 du décret n° 77-1141 du 12 octobre 1977 pris pour l'application de l'article 2 de la loi n° 76-629 du 10 juillet 1976 relative à la protection de la nature, désormais codifié à l'article R. 122-3 du code de l'environnement, elles ont, en revanche, implicitement mais nécessairement eu pour effet d'abroger les dispositions du 4° de l'annexe II et du 10° de l'annexe III de ce même décret, auxquelles renvoie son article 3, qui dispensaient de l'obligation de réaliser une telle étude d'impact dans le cas, prévu au dernier alinéa de l'ancienne rédaction de l'article L. 311-4 du code de l'urbanisme, où l'acte de création de la ZAC décidait de maintenir en vigueur les dispositions du plan d'occupation des sols rendu public ou approuvé.</ANA>
<ANA ID="9B"> 68-01-01-02 Si les équipements et aménagements d'une zone d'aménagement concerté doivent être réalisés dans le respect des dispositions du règlement du plan local d'urbanisme (PLU) ou du plan d'occupation des sols (POS) applicables au moment de leur réalisation, ces mêmes règles ne s'imposent pas, en revanche, à l'acte de création de la zone.</ANA>
<ANA ID="9C"> 68-02-02-01 1) Si les équipements et aménagements d'une zone d'aménagement concerté doivent être réalisés dans le respect des dispositions du règlement du plan local d'urbanisme (PLU) ou du plan d'occupation des sols (POS) applicables au moment de leur réalisation, ces mêmes règles ne s'imposent pas, en revanche, à l'acte de création de la zone.,,2) Il résulte de l'article R. 311-2 du code de l'urbanisme, dans sa rédaction issue du décret n° 2001-1208 du 27 mars 2001 relatif aux zones d'aménagement concerté (ZAC) que le dossier de création d'une ZAC comprend « (&#133;) d) L'étude d'impact définie à l'article 2 du décret du 12 octobre 1977 modifié ». Si ces dispositions, qui impliquent que la création de toute ZAC soit précédée d'une étude d'impact, renvoient pour la définition de cette dernière à l'article 2 du décret n° 77-1141 du 12 octobre 1977 pris pour l'application de l'article 2 de la loi n° 76-629 du 10 juillet 1976 relative à la protection de la nature, désormais codifié à l'article R. 122-3 du code de l'environnement, elles ont, en revanche, implicitement mais nécessairement eu pour effet d'abroger les dispositions du 4° de l'annexe II et du 10° de l'annexe III de ce même décret, auxquelles renvoie son article 3, qui dispensaient de l'obligation de réaliser une telle étude d'impact dans le cas, prévu au dernier alinéa de l'ancienne rédaction de l'article L. 311-4 du code de l'urbanisme, où l'acte de création de la ZAC décidait de maintenir en vigueur les dispositions du plan d'occupation des sols rendu public ou approuvé.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
