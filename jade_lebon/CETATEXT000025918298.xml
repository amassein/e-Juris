<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025918298</ID>
<ANCIEN_ID>JG_L_2012_05_000000339834</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/91/82/CETATEXT000025918298.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 15/05/2012, 339834</TITRE>
<DATE_DEC>2012-05-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>339834</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN</AVOCATS>
<RAPPORTEUR>Mme Chrystelle Naudan-Carastro</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:339834.20120515</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 21 mai et 5 juillet 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour la FEDERATION INTERCO CFDT, dont le siège est 47/49, avenue Simon Bolivar à Paris (75950), représentée par sa secrétaire générale ; la FEDERATION INTERCO CFDT demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2010-337 du 31 mars 2010 relatif au conseil de surveillance de l'agence régionale de santé ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu la Constitution, notamment son Préambule ;<br/>
<br/>
              Vu le code de la santé publique ;<br/>
<br/>
              Vu la loi n° 2009-879 du 21 juillet 2009 ;<br/>
<br/>
              Vu le décret n° 82-452 du 28 mai 1982 ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Chrystelle Naudan-Carastro, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de la SCP Masse-Dessen, Thouvenin, avocat de la FEDERATION INTERCO CFDT, <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Masse-Dessen, Thouvenin, avocat de la FEDERATION INTERCO CFDT ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Sur la fin de non-recevoir opposée par le ministre chargé de la santé : <br/>
<br/>
              Considérant qu'eu égard à la portée du décret attaqué, qui institue au sein des agences régionales de santé un conseil de surveillance chargé notamment, aux termes de l'article L. 1432-3 du code de la santé publique, d'approuver le budget de l'agence, la FEDERATION NATIONALE INTERCO CFDT, qui est une union de syndicats dont l'objet est de défendre les intérêts collectifs des personnels appartenant notamment au domaine des affaires sociales, justifie, alors même qu'existent des syndicats représentant spécifiquement certains personnels appelés à exercer leurs fonctions au sein des agences régionales de santé, d'un intérêt lui donnant qualité pour demander l'annulation de ce décret ; qu'il suit de là que la fin de non-recevoir opposée par le ministre chargé de la santé doit être écartée ; <br/>
<br/>
              Sur la légalité du décret attaqué :<br/>
<br/>
              Sans qu'il soit besoin d'examiner les autres moyens de la requête ;<br/>
<br/>
              Considérant que, dans sa rédaction en vigueur à la date de la publication du décret attaqué, l'article 2 du décret du 28 mai 1982 relatif aux comités techniques paritaires dispose : " Dans chaque département ministériel, un comité technique paritaire ministériel est créé auprès du ministre par arrêté du ministre intéressé " ; qu'aux termes de l'article 12 du même décret : " Les comités techniques paritaires connaissent dans les conditions et les limites précisées pour chaque catégorie de comité par les articles 13 et 14 du présent décret des questions et des projets de textes relatifs : 1° Aux problèmes généraux d'organisation des administrations, établissements ou services " ; qu'aux termes de l'article 13 du même décret : " La compétence respective des différents comités prévus au titre 1er du présent décret est déterminée par l'arrêté visé à l'article 2 en application des règles suivantes : 1° Le comité technique ministériel examine les questions intéressant l'ensemble des services centraux et déconcentrés du département ministériel considéré " ; <br/>
<br/>
              Considérant que le décret attaqué constitue une mesure d'organisation des services au sens du 1° précité de l'article 12 du décret du 28 mai 1982, dès lors que les agences régionales d'hospitalisation, nouveaux établissements publics de l'Etat, sont substituées, pour l'exercice des missions prévues à l'article L. 1431-1 du code de la santé publique, à des services déconcentrés de l'Etat, et que des personnels exerçant auparavant leurs fonctions au sein de ces services leur sont transférés ; que l'adoption de ce décret nécessitait donc la consultation d'un comité technique paritaire ; qu'eu égard au caractère général de cette réforme et à ses effets sur l'ensemble des services déconcentrés du ministère des affaires sociales, c'est le comité technique paritaire ministériel chargé des affaires sociales qui devait être en l'espèce consulté ; qu'il ressort cependant des pièces du dossier que cette consultation a été omise ;<br/>
<br/>
              Considérant que si les actes administratifs doivent être pris selon les formes et conformément aux procédures prévues par les lois et règlements, un vice affectant le déroulement d'une procédure administrative préalable, suivie à titre obligatoire ou facultatif, n'est de nature à entacher d'illégalité la décision prise que s'il ressort des pièces du dossier qu'il a été susceptible d'exercer, en l'espèce, une influence sur le sens de la décision prise ou qu'il a privé les intéressés d'une garantie ; qu'en l'espèce, la consultation du comité technique paritaire ministériel préalablement à l'adoption du décret attaqué constitue pour les personnels de l'Etat concernés une garantie qui découle du principe de participation des travailleurs à la détermination collective des conditions de travail consacré par le huitième alinéa du Préambule de la Constitution de 1946 ; que, par suite, l'omission de cette consultation, qui a privé les représentants de ces personnels d'une garantie, a constitué une irrégularité de nature à entacher la légalité du décret attaqué, dont la FEDERATION NATIONALE INTERCO-CFDT est donc fondée à demander l'annulation pour excès de pouvoir ;<br/>
<br/>
              Considérant qu'au regard, d'une part, des conséquences de la rétroactivité de l'annulation du décret attaqué, qui produirait des effets manifestement excessifs en raison du risque de mise en cause des actes pris sur le fondement de ses dispositions, relatifs au fonctionnement de l'ensemble des agences régionales de santé, d'autre part, de la nécessité de permettre au ministre du travail, de l'emploi et de la santé de prendre les dispositions nécessaires pour assurer la continuité du service public, et compte tenu tant de la nature du moyen d'annulation retenu que de ce qu'aucun des autres moyens soulevés ne peut être accueilli, il y a lieu de prévoir que l'annulation prononcée par la présente décision ne prendra effet qu'à compter du 30 novembre 2012 et que, sous réserve des actions contentieuses engagées à la date de la présente décision contre les actes pris sur son fondement, les effets produits par les dispositions du décret attaqué antérieurement à son annulation seront regardés comme définitifs ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement de la somme de 3 000 euros à la FEDERATION NATIONALE INTERCO CFDT au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le décret n° 2010-337 du 31 mars 2010 est annulé à compter du 30 novembre 2012.<br/>
Article 2 : Sous réserve des actions contentieuses engagées à la date de la présente décision contre les actes pris sur le fondement du décret du 31 mars 2010, les effets produits par ce dernier antérieurement à son annulation sont regardés comme définitifs.<br/>
Article 3 : L'Etat versera la somme de 3 000 euros à la FEDERATION INTERCO CFDT au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la FEDERATION INTERCO CFDT, au Premier ministre et au ministre du travail, de l'emploi et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-03-02-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. PROCÉDURE CONSULTATIVE. CONSULTATION OBLIGATOIRE. - DÉCRET SUBSTITUANT LES ARS AUX SERVICES DÉCONCENTRÉS POUR L'EXERCICE DE CERTAINES MISSIONS ET TRANSFERANT LES PERSONNELS - 1) NIVEAU DU CTP À CONSULTER - CTP MINISTÉRIEL, EU ÉGARD AU CARACTÈRE GÉNÉRAL DES MESURES ET À LEURS EFFETS SUR L'ENSEMBLE DES SERVICES DÉCONCENTRÉS [RJ1] - 2) ABSENCE DE CONSULTATION - PRIVATION D'UNE GARANTIE [RJ2] - CONSÉQUENCE - ANNULATION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-07-06-03 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. COMITÉS TECHNIQUES PARITAIRES. CONSULTATION OBLIGATOIRE. - DÉCRET SUBSTITUANT LES ARS AUX SERVICES DÉCONCENTRÉS POUR L'EXERCICE DE CERTAINES MISSIONS, ET TRANSFÉRANT LES PERSONNELS - 1) NIVEAU DU CTP À CONSULTER - CTP MINISTÉRIEL, EU ÉGARD AU CARACTÈRE GÉNÉRAL DES MESURES ET À LEURS EFFETS SUR L'ENSEMBLE DES SERVICES DÉCONCENTRÉS [RJ1] - 2) ABSENCE DE CONSULTATION - PRIVATION D'UNE GARANTIE [RJ2] - EXISTENCE - CONSÉQUENCE - ANNULATION.
</SCT>
<ANA ID="9A"> 01-03-02-02 Décret substituant, pour l'exercice de certaines missions, les agences régionales de santé (ARS), nouveaux établissements publics de l'Etat, à des services déconcentrés de l'Etat et transférant les personnels exerçant auparavant leurs fonctions au sein de ces services. 1) Eu égard au caractère général de cette réforme et à ses effets sur l'ensemble des services déconcentrés du ministère des affaires sociales, c'est le comité technique paritaire (CTP) ministériel chargé des affaires sociales qui devait être en l'espèce consulté. 2) L'absence de consultation de ce comité a privé les personnels concernés d'une garantie qui découle du principe de participation des travailleurs à la détermination collective des conditions de travail consacré par le huitième alinéa du Préambule de la Constitution de 1946 et a constitué une irrégularité de nature à entacher la légalité du décret adopté.</ANA>
<ANA ID="9B"> 36-07-06-03 Décret substituant, pour l'exercice de certaines missions, les agences régionales de santé (ARS), nouveaux établissements publics de l'Etat, à des services déconcentrés de l'Etat et transférant les personnels exerçant auparavant leurs fonctions au sein de ces services. 1) Eu égard au caractère général de cette réforme et à ses effets sur l'ensemble des services déconcentrés du ministère des affaires sociales, c'est le comité technique paritaire (CTP) ministériel chargé des affaires sociales qui devait être en l'espèce consulté. 2) L'absence de consultation de ce comité a privé les personnels concernés d'une garantie qui découle du principe de participation des travailleurs à la détermination collective des conditions de travail consacré par le huitième alinéa du Préambule de la Constitution de 1946 et a constitué une irrégularité de nature à entacher la légalité du décret adopté.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 19 décembre 2008,,et autres, n° 312553, p. 468.,,[RJ2] Cf. CE, Assemblée, 23 décembre 2011,,et autres, n° 335033, à publier au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
