<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043053006</ID>
<ANCIEN_ID>JG_L_2021_01_000000425539</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/05/30/CETATEXT000043053006.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Section, 25/01/2021, 425539, Publié au recueil Lebon</TITRE>
<DATE_DEC>2021-01-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>425539</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Section</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, TEXIDOR, PERIER ; LE PRADO</AVOCATS>
<RAPPORTEUR>M. Florian Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESEC:2021:425539.20210125</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A... C..., M. F... C..., M. B... C..., Mlle D... C... et Mme G... C... ont demandé au tribunal administratif de Melun de condamner le centre hospitalier de Lagny Marne-la-Vallée à leur verser la somme de 305 842,33 euros en réparation des préjudices qu'ils estiment avoir subis suite au décès de M. E... C.... Par un jugement n° 1408052 du 24 novembre 2017, le tribunal administratif a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 18PA00313 du 20 septembre 2018, la cour administrative d'appel de Paris a rejeté l'appel formé par Mme C... et autres contre ce jugement, ainsi que les conclusions de la caisse primaire d'assurance maladie de Seine-et-Marne tendant au remboursement de ses débours.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 20 novembre 2018 et 20 février 2019 au secrétariat du contentieux du Conseil d'État, Mme C... et autres demandent au Conseil d'État :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il rejette leur appel ; <br/>
<br/>
              2°) de mettre à la charge du centre hospitalier de Lagny Marne-la-Vallée la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Célice, Texidor, Perier, avocat de Mme C... et autres et à Me Le Prado, avocat du centre hospitalier de Lagny Marne-la-Vallée ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. C..., placé sous traitement anti-coagulant, a été admis en septembre 2013 au centre hospitalier de Lagny Marne-la-Vallée pour un sevrage médicalisé. Alors qu'il participait, le 8 octobre 2013, à une séance de psychomotricité organisée par le service de soins de suite et de réadaptation en addictologie, il a été victime d'une chute consécutive à la mauvaise réception d'une balle en mousse et est décédé, le 17 octobre suivant, des suites d'une hémorragie cérébrale. Mme C..., son épouse, ainsi que d'autres ayants droit, ont demandé au tribunal administratif de Melun de condamner le centre hospitalier à leur verser la somme de 305 842,33 euros en réparation des préjudices résultant de ce décès. Ils se pourvoient en cassation contre l'arrêt du 20 septembre 2018 par lequel la cour administrative d'appel de Paris a rejeté leur appel formé contre le jugement du 24 novembre 2017 du tribunal administratif qui a rejeté leur demande. <br/>
<br/>
              2. En premier lieu, en jugeant, au vu notamment du rapport d'expertise médicale réalisé dans le cadre de la procédure pénale engagée à la suite des mêmes faits, que l'expertise dont la réalisation était sollicitée par les requérants ne présentait pas un caractère utile, la cour, qui a suffisamment motivé son arrêt sur ce point, a porté sur les faits de l'espèce une appréciation souveraine, exempte de dénaturation et n'a pas commis d'erreur de droit.<br/>
<br/>
              3. En deuxième lieu, en jugeant, pour écarter l'existence d'une faute de la part de l'établissement de santé, que le traitement anticoagulant suivi par M. C... ne constituait pas une contre-indication à l'exercice d'une activité physique non violente et que l'activité de lancer de balle organisée à des fins thérapeutiques ne pouvait être regardée comme exposant l'intéressé à un risque particulier, la cour, dont l'arrêt est suffisamment motivé au regard de l'argumentation soulevée devant elle, n'a pas dénaturé les pièces du dossier qui lui étaient soumis et a exactement qualifié les faits.<br/>
<br/>
              4. Enfin, aux termes de l'article R. 613-5 du code de justice administrative : " Devant le Conseil d'Etat, l'instruction est close soit après que les avocats au Conseil d'Etat ont formulé leurs observations orales, soit, en l'absence d'avocat, après l'appel de l'affaire à l'audience. / Le président de la chambre chargée de l'instruction peut toutefois, par ordonnance, fixer la date à partir de laquelle l'instruction sera close. Cette ordonnance n'est pas motivée et ne peut faire l'objet d'aucun recours ", l'article R. 613-1 du même code prévoyant que, pour les tribunaux administratifs et les cours administratives d'appel : " Le président de la formation de jugement peut, par une ordonnance, fixer la date à partir de laquelle l'instruction sera close. Cette ordonnance n'est pas motivée et ne peut faire l'objet d'aucun recours (...) ". Par ailleurs, aux termes de l'article R. 611-7 du même code : " Lorsque la décision lui paraît susceptible d'être fondée sur un moyen relevé d'office, le président de la formation de jugement ou le président de la chambre chargée de l'instruction en informe les parties avant la séance de jugement et fixe le délai dans lequel elles peuvent, sans qu'y fasse obstacle la clôture éventuelle de l'instruction, présenter leurs observations sur le moyen communiqué ". <br/>
<br/>
              5. Lorsque, postérieurement à la clôture de l'instruction, le juge informe les parties, en application de l'article R. 611-7 du code de justice administrative cité ci-dessus, que sa décision est susceptible d'être fondée sur un moyen relevé d'office, cette information n'a pas par elle-même pour effet de rouvrir l'instruction. La communication par le juge, à l'ensemble des parties, des observations reçues sur ce moyen relevé d'office n'a pas non plus par elle-même pour effet de rouvrir l'instruction, y compris dans le cas où, par l'argumentation qu'elle développe, une partie doit être regardée comme ayant expressément repris le moyen énoncé par le juge et soulevé ainsi un nouveau moyen. La réception d'observations sur un moyen relevé d'office n'impose en effet au juge de rouvrir l'instruction, conformément à la règle applicable à tout mémoire reçu postérieurement à la clôture de l'instruction, que si ces observations contiennent l'exposé d'une circonstance de fait ou d'un élément de droit qui est susceptible d'exercer une influence sur le jugement de l'affaire et dont la partie qui l'invoque n'était pas en mesure de faire état avant la clôture de l'instruction.<br/>
<br/>
              6. L'instruction du présent pourvoi a été close au 15 juin 2020 par une ordonnance du 27 mars 2020 du président de la 5ème chambre de la section du contentieux du Conseil d'Etat. Par ailleurs, les parties ont été informées le 25 juin 2020, en application des dispositions de l'article R. 611-7 du code de justice administrative, que la décision du Conseil d'Etat était susceptible d'être fondée sur le moyen, relevé d'office, tiré de ce que le dommage subi par les requérants remplissait les conditions pour être indemnisé en tout ou partie sur le fondement de la solidarité nationale et que la cour avait, par suite, méconnu son office en s'abstenant de mettre en cause l'ONIAM. Dans leurs observations produites les 13 octobre 2020 et 8 janvier 2021 en réponse à ce moyen, Mme C... et autres ont présenté une argumentation qui doit les faire regarder comme ayant expressément repris ce moyen et comme ayant, ainsi, soulevé un nouveau moyen.<br/>
<br/>
              7. Lorsque les juges du fond statuent seulement, compte tenu des moyens dont ils sont saisis, sur l'existence d'une faute du service public hospitalier et que, ce faisant, ils écartent implicitement le moyen d'ordre public tiré de ce qu'une indemnisation devrait être accordée, au titre de la solidarité nationale, sur le fondement des dispositions de l'article L. 1142-21 du code de la santé publique, le juge de cassation ne saurait relever lui-même d'office ce moyen s'il implique de porter une appréciation sur les pièces du dossier soumis aux juges du fond. Il en va de même du moyen tiré de ce que les juges du fond auraient entaché leur décision d'irrégularité, faute d'avoir appelé d'office l'ONIAM en la cause aux fins de pouvoir mettre à sa charge la réparation qui lui incombe au titre de la solidarité nationale.<br/>
<br/>
              8. Le moyen repris par Mme C... et autres, qui implique de porter une appréciation sur les pièces du dossier soumis aux juges du fond, ne saurait, par suite, être relevé d'office par le juge de cassation. Ayant été présenté par les requérants postérieurement à la clôture de l'instruction, il n'y a pas lieu de se prononcer sur son bien-fondé.<br/>
<br/>
              9. Il résulte de tout ce qui précède que le pourvoi de Mme C... et autres doit être rejeté, y compris, par voie de conséquence, ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi de Mme C... et autres est rejeté.<br/>
Article 2 : La présente décision sera notifiée à Mme A... C..., première requérante dénommée et au centre hospitalier de Lagny Marne-la-Vallée.<br/>
Copie en sera adressée à la caisse primaire d'assurance maladie de Seine-et-Marne.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-04-01-05 PROCÉDURE. INSTRUCTION. POUVOIRS GÉNÉRAUX D'INSTRUCTION DU JUGE. CLÔTURE DE L'INSTRUCTION. - COMMUNICATION D'UN MOYEN RELEVÉ D'OFFICE (ART. R. 611-7 DU CJA) APRÈS LA CLÔTURE DE L'INSTRUCTION -  CONSÉQUENCES - 1) RÉOUVERTURE DE L'INSTRUCTION DE CE SEUL FAIT - ABSENCE - 2) RÉCEPTION D'OBSERVATIONS DES PARTIES SUR CE MOYEN - A) OBLIGATION DE LES COMMUNIQUER AUX AUTRES PARTIES - EXISTENCE, SANS QUE CETTE COMMUNICATION AIT POUR EFFET DE ROUVRIR L'INSTRUCTION [RJ1] - B) OBLIGATION DE ROUVRIR L'INSTRUCTION - ABSENCE, SAUF SI UNE CIRCONSTANCE DE FAIT OU UN ÉLÉMENT DE DROIT, DONT LA PARTIE QUI L'INVOQUE N'ÉTAIT PAS EN MESURE DE FAIRE ÉTAT AVANT LA CLÔTURE DE L'INSTRUCTION, EST SUSCEPTIBLE D'EXERCER UNE INFLUENCE SUR LE JUGEMENT DE L'AFFAIRE [RJ2] - 3) MOYEN COMMUNIQUÉ NE S'AVÉRANT PAS D'ORDRE PUBLIC - JUGE TENU D'EXAMINER SON BIEN-FONDÉ SI UNE PARTIE LE REPREND À SON COMPTE [RJ3] - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-04-03-02 PROCÉDURE. INSTRUCTION. CARACTÈRE CONTRADICTOIRE DE LA PROCÉDURE. COMMUNICATION DES MOYENS D'ORDRE PUBLIC. - COMMUNICATION D'UN MOYEN RELEVÉ D'OFFICE (ART. R. 611-7 DU CJA) APRÈS LA CLÔTURE DE L'INSTRUCTION -  CONSÉQUENCES - 1) RÉOUVERTURE DE L'INSTRUCTION DE CE SEUL FAIT - ABSENCE - 2) RÉCEPTION D'OBSERVATIONS DES PARTIES SUR CE MOYEN - A) OBLIGATION DE LES COMMUNIQUER AUX AUTRES PARTIES - EXISTENCE, SANS QUE CETTE COMMUNICATION AIT POUR EFFET DE ROUVRIR L'INSTRUCTION [RJ1]  - B) OBLIGATION DE ROUVRIR L'INSTRUCTION - ABSENCE [RJ1], SAUF SI UNE CIRCONSTANCE DE FAIT OU UN ÉLÉMENT DE DROIT, DONT LA PARTIE QUI L'INVOQUE N'ÉTAIT PAS EN MESURE DE FAIRE ÉTAT AVANT LA CLÔTURE DE L'INSTRUCTION, EST SUSCEPTIBLE D'EXERCER UNE INFLUENCE SUR LE JUGEMENT DE L'AFFAIRE [RJ2] - 3) A) MOYEN COMMUNIQUÉ NE S'AVÉRANT PAS D'ORDRE PUBLIC - JUGE TENU D'EXAMINER SON BIEN-FONDÉ SI UNE PARTIE LE REPREND À SON COMPTE [RJ3] - ABSENCE - B) ILLUSTRATION - MOYEN TIRÉ DE CE QU'UNE INDEMNISATION DEVRAIT ÊTRE ACCORDÉE AU TITRE DE LA SOLIDARITÉ NATIONALE (ART. L. 1142-21 DU CSP) - I) MOYEN DEVANT ÊTRE RELEVÉ D'OFFICE PAR LE JUGE DE CASSATION - EXISTENCE [RJ4], SAUF S'IL IMPLIQUE DE PORTER UNE APPRÉCIATION SUR LES PIÈCES DU DOSSIER SOUMIS AUX JUGES DU FOND [RJ5] - II) ESPÈCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-07-01-04-01-02 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. MOYENS D'ORDRE PUBLIC À SOULEVER D'OFFICE. EXISTENCE. - MOYEN TIRÉ DE CE QU'UNE INDEMNISATION DEVRAIT ÊTRE ACCORDÉE AU TITRE DE LA SOLIDARITÉ NATIONALE (ART. L. 1142-21 DU CSP) - MOYEN DEVANT ÊTRE RELEVÉ D'OFFICE PAR LE JUGE DE CASSATION - EXISTENCE [RJ4], SAUF S'IL IMPLIQUE DE PORTER UNE APPRÉCIATION SUR LES PIÈCES DU DOSSIER SOUMIS AUX JUGES DU FOND [RJ5].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">60-04-04-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RÉPARATION. MODALITÉS DE LA RÉPARATION. SOLIDARITÉ. - MOYEN TIRÉ DE CE QU'UNE INDEMNISATION DEVRAIT ÊTRE ACCORDÉE AU TITRE DE LA SOLIDARITÉ NATIONALE (ART. L. 1142-21 DU CSP) - MOYEN DEVANT ÊTRE RELEVÉ D'OFFICE PAR LE JUGE DE CASSATION - EXISTENCE [RJ4], SAUF S'IL IMPLIQUE DE PORTER UNE APPRÉCIATION SUR LES PIÈCES DU DOSSIER SOUMIS AUX JUGES DU FOND [RJ5].
</SCT>
<ANA ID="9A"> 54-04-01-05 1) Lorsque, postérieurement à la clôture de l'instruction, le juge informe les parties, en application de l'article R. 611-7 du code de justice administrative (CJA), que sa décision est susceptible d'être fondée sur un moyen relevé d'office, cette information n'a pas par elle-même pour effet de rouvrir l'instruction.... ,,2) a) La communication par le juge, à l'ensemble des parties, des observations reçues sur ce moyen relevé d'office n'a pas non plus par elle-même pour effet de rouvrir l'instruction, y compris dans le cas où, par l'argumentation qu'elle développe, une partie doit être regardée comme ayant expressément repris le moyen énoncé par le juge et soulevé ainsi un nouveau moyen.... ,,b) La réception d'observations sur un moyen relevé d'office n'impose en effet au juge de rouvrir l'instruction, conformément à la règle applicable à tout mémoire reçu postérieurement à la clôture de l'instruction, que si ces observations contiennent l'exposé d'une circonstance de fait ou d'un élément de droit qui est susceptible d'exercer une influence sur le jugement de l'affaire et dont la partie qui l'invoque n'était pas en mesure de faire état avant la clôture de l'instruction.,,,3) a) Lorsqu'en réponse à la communication qui lui a été faite par le juge qu'un moyen était susceptible d'être relevé d'office, une partie présente, postérieurement à la clôture de l'instruction, une argumentation qui doit la faire regarder comme ayant expressément repris ce moyen, et qu'il s'avère que ce moyen n'avait pas à être relevé d'office, il n'y a pas lieu pour le juge d'examiner son bien-fondé.,,,b) Lorsque les juges du fond statuent seulement, compte tenu des moyens dont ils sont saisis, sur l'existence d'une faute du service public hospitalier et que, ce faisant, ils écartent implicitement le moyen d'ordre public tiré de ce qu'une indemnisation devrait être accordée, au titre de la solidarité nationale, sur le fondement des dispositions de l'article L. 1142-21 du code de la santé publique (CSP), le juge de cassation ne saurait relever lui-même d'office ce moyen s'il implique de porter une appréciation sur les pièces du dossier soumis aux juges du fond. Il en va de même du moyen tiré de ce que les juges du fond auraient entaché leur décision d'irrégularité, faute d'avoir appelé d'office l'Office national d'indemnisation des accidents médicaux (ONIAM) en la cause aux fins de pouvoir mettre à sa charge la réparation qui lui incombe au titre de la solidarité nationale.</ANA>
<ANA ID="9B"> 54-04-03-02 1) Lorsque, postérieurement à la clôture de l'instruction, le juge informe les parties, en application de l'article R. 611-7 du code de justice administrative (CJA), que sa décision est susceptible d'être fondée sur un moyen relevé d'office, cette information n'a pas par elle-même pour effet de rouvrir l'instruction.... ,,2) a) La communication par le juge, à l'ensemble des parties, des observations reçues sur ce moyen relevé d'office n'a pas non plus par elle-même pour effet de rouvrir l'instruction, y compris dans le cas où, par l'argumentation qu'elle développe, une partie doit être regardée comme ayant expressément repris le moyen énoncé par le juge et soulevé ainsi un nouveau moyen.... ,,b) La réception d'observations sur un moyen relevé d'office n'impose en effet au juge de rouvrir l'instruction, conformément à la règle applicable à tout mémoire reçu postérieurement à la clôture de l'instruction, que si ces observations contiennent l'exposé d'une circonstance de fait ou d'un élément de droit qui est susceptible d'exercer une influence sur le jugement de l'affaire et dont la partie qui l'invoque n'était pas en mesure de faire état avant la clôture de l'instruction.,,,3) a) Lorsqu'en réponse à la communication qui lui a été faite par le juge qu'un moyen était susceptible d'être relevé d'office, une partie présente, postérieurement à la clôture de l'instruction, une argumentation qui doit la faire regarder comme ayant expressément repris ce moyen, et qu'il s'avère que ce moyen n'avait pas à être relevé d'office, il n'y a pas lieu pour le juge d'examiner son bien-fondé.,,,b) i) Lorsque les juges du fond statuent seulement, compte tenu des moyens dont ils sont saisis, sur l'existence d'une faute du service public hospitalier et que, ce faisant, ils écartent implicitement le moyen d'ordre public tiré de ce qu'une indemnisation devrait être accordée, au titre de la solidarité nationale, sur le fondement de l'article L. 1142-21 du code de la santé publique (CSP), le juge de cassation ne saurait relever lui-même d'office ce moyen s'il implique de porter une appréciation sur les pièces du dossier soumis aux juges du fond. Il en va de même du moyen tiré de ce que les juges du fond auraient entaché leur décision d'irrégularité, faute d'avoir appelé d'office l'Office national d'indemnisation des accidents médicaux (ONIAM) en la cause aux fins de pouvoir mettre à sa charge la réparation qui lui incombe au titre de la solidarité nationale.,,,ii) Instruction d'un pourvoi ayant été close au 15 juin 2020 par une ordonnance du 27 mars 2020 du président de la 5ème chambre de la section du contentieux du Conseil d'Etat. Parties ayant été informées le 25 juin 2020, en application des dispositions de l'article R. 611-7 du CJA, que la décision du Conseil d'Etat était susceptible d'être fondée sur le moyen, relevé d'office, tiré de ce que le dommage subi par les requérants remplissait les conditions pour être indemnisé en tout ou partie sur le fondement de la solidarité nationale et que la cour avait, par suite, méconnu son office en s'abstenant de mettre en cause l'ONIAM. Dans leurs observations produites les 13 octobre 2020 et 8 janvier 2021 en réponse à ce moyen, les requérants ont présenté une argumentation qui doit les faire regarder comme ayant expressément repris ce moyen et comme ayant, ainsi, soulevé un nouveau moyen.,,,Le moyen ainsi repris par les requérants, qui implique de porter une appréciation sur les pièces du dossier soumis aux juges du fond, ne saurait, par suite, être relevé d'office par le juge de cassation. Ayant été présenté par les requérants postérieurement à la clôture de l'instruction, il n'y a pas lieu de se prononcer sur son bien-fondé.</ANA>
<ANA ID="9C"> 54-07-01-04-01-02 Lorsque les juges du fond statuent seulement, compte tenu des moyens dont ils sont saisis, sur l'existence d'une faute du service public hospitalier et que, ce faisant, ils écartent implicitement le moyen d'ordre public tiré de ce qu'une indemnisation devrait être accordée, au titre de la solidarité nationale, sur le fondement des dispositions de l'article L. 1142-21 du code de la santé publique (CSP), le juge de cassation ne saurait relever lui-même d'office ce moyen s'il implique de porter une appréciation sur les pièces du dossier soumis aux juges du fond. Il en va de même du moyen tiré de ce que les juges du fond auraient entaché leur décision d'irrégularité, faute d'avoir appelé d'office l'Office national d'indemnisation des accidents médicaux (ONIAM) en la cause aux fins de pouvoir mettre à sa charge la réparation qui lui incombe au titre de la solidarité nationale.</ANA>
<ANA ID="9D"> 60-04-04-01 Lorsque les juges du fond statuent seulement, compte tenu des moyens dont ils sont saisis, sur l'existence d'une faute du service public hospitalier et que, ce faisant, ils écartent implicitement le moyen d'ordre public tiré de ce qu'une indemnisation devrait être accordée, au titre de la solidarité nationale, sur le fondement des dispositions de l'article L. 1142-21 du code de la santé publique (CSP), le juge de cassation ne saurait relever lui-même d'office ce moyen s'il implique de porter une appréciation sur les pièces du dossier soumis aux juges du fond. Il en va de même du moyen tiré de ce que les juges du fond auraient entaché leur décision d'irrégularité, faute d'avoir appelé d'office l'Office national d'indemnisation des accidents médicaux (ONIAM) en la cause aux fins de pouvoir mettre à sa charge la réparation qui lui incombe au titre de la solidarité nationale.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp., s'agissant de la communication d'un mémoire après la clôture de l'instruction, CE, 4 mars 2009, Elections cantonales de Belle-Ile-en mer, n° 317473, T. p. 896 ; CE, 7 décembre 2011, Département de la Haute-Garonne, n° 330751, T. p. 1084 ; CE, 23 juin 2014, Société Deny All, n° 352504, p. 173., ,[RJ2] Rappr., sur les circonstances imposant la réouverture de l'instruction en cas de production postérieure à sa clôture, CE, Section, 5 décembre 2014, M.,, n° 340943, p. 369.,,[RJ3] Comp., s'agissant d'un moyen repris par une partie avant la clôture de l'instruction, CE, 30 juin 1999,,, n° 190038, p. 232.,,[RJ4] Cf. CE, 30 mars 2011, Mme,, n° 320581, p. 146.,,[RJ5] Cf. CE, 8 janvier 1982,,, n°s 19875 21978, T. pp. 728-735.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
