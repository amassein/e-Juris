<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038351101</ID>
<ANCIEN_ID>JG_L_2019_04_000000416542</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/35/11/CETATEXT000038351101.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 05/04/2019, 416542</TITRE>
<DATE_DEC>2019-04-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>416542</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD ; SCP GADIOU, CHEVALLIER</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT>Mme Anne Iljic</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CEORD:2019:416542.20190405</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C...B...A...a demandé au tribunal administratif de La Réunion d'annuler pour excès de pouvoir la décision implicite par laquelle a été rejetée sa demande du 1er octobre 2016 tendant à la communication, par la communauté intercommunale des villes solidaires (CIVIS), de différents documents et d'enjoindre au président de la communauté intercommunale de communiquer les documents demandés.<br/>
<br/>
              Par un jugement n° 1700297 du 12 octobre 2017, le tribunal administratif de La Réunion a, d'une part, jugé qu'il n'y avait plus lieu de statuer sur une partie des conclusions et, d'autre part, annulé la décision litigieuse en tant qu'elle porte refus de communication de l'étude de marché réalisée par la société AID Observatoire en avril 2015, de la lettre de la SPLA Grand Sud adressée à la CIVIS lui transmettant la demande d'acquisition de la société Holding Ethève et de l'intégralité des consultations juridiques réalisées au bénéfice de la CIVIS et de la SPLA Grand Sud se rapportant à la réalisation de la zone d'aménagement concerté de Pierrefonds Aérodrome, ainsi que des bons de commande de prestations juridiques afférents. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 14 décembre 2017, 15 mars 2018 et 22 janvier 2019 au secrétariat du contentieux du Conseil d'Etat, la communauté intercommunale des villes solidaires demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement en tant qu'il fait droit aux conclusions de M. B... A...; <br/>
<br/>
              2°) de mettre à la charge de M. B... A...la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu :<br/>
              - le code général des collectivités territoriales ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Raphaël Chambon, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Anne Iljic, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de la communauté intercommunale des villes solidaires et à la SCP Gadiou, Chevallier, avocat de M. B...A...;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des énonciations du jugement attaqué que M. B...A...a demandé, en qualité de conseiller communautaire de la communauté intercommunale des villes solidaires (CIVIS), par un courrier du 1er octobre 2016 adressé au directeur général des services, la communication de documents concernant deux délibérations du conseil communautaire de la CIVIS du 18 décembre 2012 et du 31 août 2016. M. B...A...a demandé au tribunal administratif de La Réunion d'annuler la décision implicite ayant rejeté sa demande. La communauté intercommunale des villes solidaires se pourvoit en cassation contre le jugement du tribunal administratif en tant que ce jugement a fait droit à la demande de M. B...A.... <br/>
<br/>
              2. Aux termes de l'article L. 2121-13 du code général des collectivités territoriales, applicable aux établissements publics de coopération intercommunale tels que la communauté intercommunale des villes solidaires en vertu de l'article L. 5211-1 du même code : " Tout membre du conseil municipal a le droit, dans le cadre de sa fonction, d'être informé des affaires de la commune qui font l'objet d'une délibération ". En application de ces dispositions, le maire est tenu de communiquer aux membres du conseil municipal les documents nécessaires pour qu'ils puissent se prononcer utilement sur les affaires de la commune soumises à leur délibération. Lorsqu'un membre du conseil municipal demande, sur le fondement de ces dispositions du code général des collectivités territoriales, la communication de documents, il appartient au maire sous le contrôle du juge de l'excès de pouvoir, d'une part, d'apprécier si cette communication se rattache à une affaire de la commune qui fait l'objet d'une délibération du conseil municipal et, d'autre part, de s'assurer qu'aucun motif d'intérêt général n'y fait obstacle, avant de procéder, le cas échéant, à cette communication selon des modalités appropriées. Il en va de même des demandes de communication adressées au président d'un établissement public de coopération intercommunale par les membres du conseil communautaire. <br/>
<br/>
              3. En premier lieu, dès lors qu'il appartient au maire, sous réserve des délégations qu'il lui est loisible d'accorder, d'apprécier s'il y a lieu de procéder à la communication de documents demandés sur le fondement des dispositions précédemment citées, de telles demandes de communication doivent en principe lui être adressées, sauf à ce qu'il ait arrêté des modalités différentes pour la présentation de telles demandes. Toutefois, une demande adressée au directeur général des services ne saurait être rejetée comme mal dirigée, dans la mesure où il revient, en tout état de cause, au directeur général des services de la transmettre au maire pour qu'il puisse apprécier s'il y a lieu d'y donner suite. Il en résulte que ne peut qu'être écarté le moyen tiré de ce que le tribunal administratif aurait commis une erreur de droit faute de retenir que le directeur général des services de la CIVIS aurait été placé en situation de compétence liée pour rejeter la demande de communication qui lui avait été, à tort, adressée.<br/>
<br/>
              4. Mais, en second lieu, le tribunal administratif de La Réunion a fait droit à la demande de M. B...A...au motif que les documents dont ce dernier avait demandé la communication au directeur général des services de la CIVIS se rapportaient à des projets qui avaient donné lieu à des délibérations du conseil communautaire de cet établissement public. En se bornant à constater que les documents en cause étaient directement liés à des délibérations, sans rechercher, alors que les délibérations invoquées étaient antérieures à la date de la demande de communication, si les documents demandés pouvaient être regardés comme étant nécessaires pour que M. B...A...puisse se prononcer utilement sur les affaires en cours de l'établissement public de coopération intercommunale, susceptibles de faire l'objet de délibérations à venir au cours desquelles les élus auraient à se prononcer sur les projets en cause, le tribunal administratif de La Réunion a commis une erreur de droit.<br/>
<br/>
              5. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, que la communauté intercommunale des villes solidaires est fondée à demander l'annulation du jugement qu'elle attaque en tant qu'il a fait droit aux conclusions présentées par M. B...A....<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la M. B... A...une somme de 3 000 euros à verser à la CIVIS, au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces dispositions font en revanche obstacle à ce qu'une somme soit mise à ce titre à la charge de la CIVIS qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de La Réunion du 17 octobre 2017 est annulé en tant qu'il fait droit aux conclusions de M. B...A....<br/>
Article 2 : L'affaire est renvoyée, dans la limite de la cassation ainsi prononcée, au tribunal administratif de La Réunion.<br/>
Article 3 : M. B...A...versera à la communauté intercommunale des villes solidaires une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par M. B... A...au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 5 : La présente décision sera notifiée à la communauté intercommunale des villes solidaires (CIVIS) et à M. C...B...A....  <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-02-01-02-01-01 COLLECTIVITÉS TERRITORIALES. COMMUNE. ORGANISATION DE LA COMMUNE. ORGANES DE LA COMMUNE. CONSEIL MUNICIPAL. FONCTIONNEMENT. - INFORMATION DES MEMBRES DU CONSEIL MUNICIPAL (ART. L. 2121-13 DU CGCT) - 1) PORTÉE DE L'OBLIGATION PESANT SUR LE MAIRE DE COMMUNIQUER, À UN MEMBRE DE CE CONSEIL QUI EN FAIT LA DEMANDE, LES DOCUMENTS SE RATTACHANT AUX AFFAIRES DE LA COMMUNE [RJ1] - 2) DESTINATAIRE DE LA DEMANDE DE COMMUNICATION - A) DEMANDE DEVANT ÊTRE EN PRINCIPE ADRESSÉE AU MAIRE - B) CAS OÙ LA DEMANDE EST ADRESSÉE AU DGS DE LA MUNICIPALITÉ - CIRCONSTANCE JUSTIFIANT DE LA REJETER COMME MAL DIRIGÉE - ABSENCE - 3) DOCUMENTS COMMUNICABLES SUR CE FONDEMENT - NOTION [RJ1] - DOCUMENTS NÉCESSAIRES POUR QUE LE CONSEILLER PUISSE SE PRONONCER UTILEMENT SUR LES AFFAIRES EN COURS DE LA COLLECTIVITÉ, SUSCEPTIBLES DE FAIRE L'OBJET DE DÉLIBÉRATIONS À VENIR.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">135-05-01-01 COLLECTIVITÉS TERRITORIALES. COOPÉRATION. ÉTABLISSEMENTS PUBLICS DE COOPÉRATION INTERCOMMUNALE - QUESTIONS GÉNÉRALES. DISPOSITIONS GÉNÉRALES ET QUESTIONS COMMUNES. - INFORMATION DES MEMBRES DU CONSEIL MUNICIPAL OU COMMUNAUTAIRE (ART. L. 2121-13 ET L. 5211-1 DU CGCT) - 1) PORTÉE DE L'OBLIGATION PESANT SUR LE MAIRE OU LE PRÉSIDENT DE L'EPCI DE COMMUNIQUER, À UN MEMBRE DE CE CONSEIL QUI EN FAIT LA DEMANDE, LES DOCUMENTS SE RATTACHANT AUX AFFAIRES DE LA COLLECTIVITÉ [RJ1] - 2) DESTINATAIRE DE LA DEMANDE DE COMMUNICATION - A) DEMANDE DEVANT ÊTRE EN PRINCIPE ADRESSÉE AU MAIRE - B) CAS OÙ LA DEMANDE EST ADRESSÉE AU DGS DE LA COLLECTIVITÉ - CIRCONSTANCE JUSTIFIANT DE LA REJETER COMME MAL DIRIGÉE - ABSENCE - 3) DOCUMENTS COMMUNICABLES SUR CE FONDEMENT - NOTION [RJ1] - DOCUMENTS NÉCESSAIRES POUR QUE LE CONSEILLER PUISSE SE PRONONCER UTILEMENT SUR LES AFFAIRES EN COURS DE LA COLLECTIVITÉ, SUSCEPTIBLES DE FAIRE L'OBJET DE DÉLIBÉRATIONS À VENIR.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">26-06 DROITS CIVILS ET INDIVIDUELS. ACCÈS AUX DOCUMENTS ADMINISTRATIFS. - INFORMATION DES MEMBRES DU CONSEIL MUNICIPAL OU COMMUNAUTAIRE (ART. L. 2121-13 ET L. 5211-1 DU CGCT) - 1) PORTÉE DE L'OBLIGATION PESANT SUR LE MAIRE OU LE PRÉSIDENT DE L'EPCI DE COMMUNIQUER, À UN MEMBRE DE CE CONSEIL QUI EN FAIT LA DEMANDE, LES DOCUMENTS SE RATTACHANT AUX AFFAIRES DE LA COLLECTIVITÉ [RJ1] - 2) DESTINATAIRE DE LA DEMANDE DE COMMUNICATION - A) DEMANDE DEVANT ÊTRE EN PRINCIPE ADRESSÉE AU MAIRE - B) CAS OÙ LA DEMANDE EST ADRESSÉE AU DGS DE LA COLLECTIVITÉ - CIRCONSTANCE JUSTIFIANT DE LA REJETER COMME MAL DIRIGÉE - ABSENCE - 3) DOCUMENTS COMMUNICABLES SUR CE FONDEMENT - NOTION [RJ1] - DOCUMENTS NÉCESSAIRES POUR QUE LE CONSEILLER PUISSE SE PRONONCER UTILEMENT SUR LES AFFAIRES EN COURS DE LA COLLECTIVITÉ, SUSCEPTIBLES DE FAIRE L'OBJET DE DÉLIBÉRATIONS À VENIR.
</SCT>
<ANA ID="9A"> 135-02-01-02-01-01 1) En application de l'article L. 2121-13 du code général des collectivités territoriales (CGCT), le maire est tenu de communiquer aux membres du conseil municipal les documents nécessaires pour qu'ils puissent se prononcer utilement sur les affaires de la commune soumises à leur délibération.... ,,Lorsqu'un membre du conseil municipal demande, sur le fondement de ces dispositions du CGCT, la communication de documents, il appartient au maire sous le contrôle du juge de l'excès de pouvoir, d'une part, d'apprécier si cette communication se rattache à une affaire de la commune qui fait l'objet d'une délibération du conseil municipal et, d'autre part, de s'assurer qu'aucun motif d'intérêt général n'y fait obstacle, avant de procéder, le cas échéant, à cette communication selon des modalités appropriées.... ,,2) a) Dès lors qu'il appartient au maire, sous réserve des délégations qu'il lui est loisible d'accorder, d'apprécier s'il y a lieu de procéder à la communication de documents demandés sur le fondement des dispositions précédemment citées, de telles demandes de communication doivent en principe lui être adressées, sauf à ce qu'il ait arrêté des modalités différentes pour la présentation de telles demandes.... ...b) Toutefois, une demande adressée au directeur général des services (DGS) ne saurait être rejetée comme mal dirigée, dans la mesure où il revient, en tout état de cause, au DGS de la transmettre au maire pour qu'il puisse apprécier s'il y a lieu d'y donner suite.... ,,3) En se bornant à constater, pour faire droit à la demande de communication, que les documents en cause étaient directement liés à des délibérations, sans rechercher, alors que les délibérations invoquées étaient antérieures à la date de la demande de communication, si les documents demandés pouvaient être regardés comme étant nécessaires pour que l'élu requérant puisse se prononcer utilement sur les affaires en cours de la collectivité, susceptibles de faire l'objet de délibérations à venir au cours desquelles les élus auraient à se prononcer sur les projets en cause, le tribunal commet une erreur de droit.</ANA>
<ANA ID="9B"> 135-05-01-01 1) En application de l'article L. 2121-13 du code général des collectivités territoriales (CGCT), applicable aux établissements publics de coopération intercommunale (EPCI) en vertu de l'article L. 5211-1 du même code, le maire est tenu de communiquer aux membres du conseil municipal les documents nécessaires pour qu'ils puissent se prononcer utilement sur les affaires de la commune soumises à leur délibération.... ,,Lorsqu'un membre du conseil municipal demande, sur le fondement de ces dispositions du CGCT, la communication de documents, il appartient au maire sous le contrôle du juge de l'excès de pouvoir, d'une part, d'apprécier si cette communication se rattache à une affaire de la commune qui fait l'objet d'une délibération du conseil municipal et, d'autre part, de s'assurer qu'aucun motif d'intérêt général n'y fait obstacle, avant de procéder, le cas échéant, à cette communication selon des modalités appropriées. Il en va de même des demandes de communication adressées au président d'un EPCI par les membres du conseil communautaire.... ,,2) a) Dès lors qu'il appartient au maire, sous réserve des délégations qu'il lui est loisible d'accorder, d'apprécier s'il y a lieu de procéder à la communication de documents demandés sur le fondement des dispositions précédemment citées, de telles demandes de communication doivent en principe lui être adressées, sauf à ce qu'il ait arrêté des modalités différentes pour la présentation de telles demandes.... ...b) Toutefois, une demande adressée au directeur général des services (DGS) ne saurait être rejetée comme mal dirigée, dans la mesure où il revient, en tout état de cause, au DGS de la transmettre au maire pour qu'il puisse apprécier s'il y a lieu d'y donner suite.... ,,3) Tribunal administratif faisant droit à une demande de communication présentée sur ce fondement au motif que les documents dont le conseiller communautaire requérant avait demandé la communication au DGS de l'EPCI se rapportaient à des projets qui avaient donné lieu à des délibérations du conseil communautaire de cet établissement public. En se bornant à constater que les documents en cause étaient directement liés à des délibérations, sans rechercher, alors que les délibérations invoquées étaient antérieures à la date de la demande de communication, si les documents demandés pouvaient être regardés comme étant nécessaires pour que le requérant puisse se prononcer utilement sur les affaires en cours de l'EPCI, susceptibles de faire l'objet de délibérations à venir au cours desquelles les élus auraient à se prononcer sur les projets en cause, le tribunal commet une erreur de droit.</ANA>
<ANA ID="9C"> 26-06 1) En application de l'article L. 2121-13 du code général des collectivités territoriales (CGCT), applicable aux établissements publics de coopération intercommunale (EPCI) en vertu de l'article L. 5211-1 du même code, le maire est tenu de communiquer aux membres du conseil municipal les documents nécessaires pour qu'ils puissent se prononcer utilement sur les affaires de la commune soumises à leur délibération.... ,,Lorsqu'un membre du conseil municipal demande, sur le fondement de ces dispositions du CGCT, la communication de documents, il appartient au maire sous le contrôle du juge de l'excès de pouvoir, d'une part, d'apprécier si cette communication se rattache à une affaire de la commune qui fait l'objet d'une délibération du conseil municipal et, d'autre part, de s'assurer qu'aucun motif d'intérêt général n'y fait obstacle, avant de procéder, le cas échéant, à cette communication selon des modalités appropriées. Il en va de même des demandes de communication adressées au président d'un EPCI par les membres du conseil communautaire.... ,,2) a) Dès lors qu'il appartient au maire, sous réserve des délégations qu'il lui est loisible d'accorder, d'apprécier s'il y a lieu de procéder à la communication de documents demandés sur le fondement des dispositions précédemment citées, de telles demandes de communication doivent en principe lui être adressées, sauf à ce qu'il ait arrêté des modalités différentes pour la présentation de telles demandes.... ...b) Toutefois, une demande adressée au directeur général des services (DGS) ne saurait être rejetée comme mal dirigée, dans la mesure où il revient, en tout état de cause, au DGS de la transmettre au maire pour qu'il puisse apprécier s'il y a lieu d'y donner suite.... ,,3) Tribunal administratif faisant droit à une demande de communication présentée sur ce fondement au motif que les documents dont le conseiller communautaire requérant avait demandé la communication au DGS de l'EPCI se rapportaient à des projets qui avaient donné lieu à des délibérations du conseil communautaire de cet établissement public. En se bornant à constater que les documents en cause étaient directement liés à des délibérations, sans rechercher, alors que les délibérations invoquées étaient antérieures à la date de la demande de communication, si les documents demandés pouvaient être regardés comme étant nécessaires pour que le requérant puisse se prononcer utilement sur les affaires en cours de l'EPCI, susceptibles de faire l'objet de délibérations à venir au cours desquelles les élus auraient à se prononcer sur les projets en cause, le tribunal commet une erreur de droit.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 27 mai 2005, Commune d'Yvetot, n° 265494, p. 226.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
