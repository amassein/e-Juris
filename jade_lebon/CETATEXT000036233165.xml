<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036233165</ID>
<ANCIEN_ID>JG_L_2017_12_000000403734</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/23/31/CETATEXT000036233165.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 18/12/2017, 403734</TITRE>
<DATE_DEC>2017-12-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>403734</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU ; SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>M. Olivier Rousselle</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:403734.20171218</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une décision du 17 décembre 2014, la chambre de discipline du conseil central de la section G de l'ordre des pharmaciens, saisie d'une plainte de la société d'exercice libéral par actions simplifiées " Laboratoire de biologie médicale des Cordeliers ", a infligé à M. A... B...la sanction de l'interdiction d'exercer la pharmacie pendant six mois.<br/>
<br/>
              Par une décision du 22 juillet 2016, la chambre de discipline du Conseil national de l'ordre des pharmaciens a rejeté l'appel de M.B....<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés le 22 septembre et le 22 décembre 2016 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la société d'exercice libéral par actions simplifiées " Laboratoire de biologie médicale des Cordeliers " le versement de la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le code de la santé publique ;<br/>
<br/>
              - la loi n° 71-1130 du 31 décembre 1971 ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Rousselle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de M. B...et à la SCP Célice, Soltner, Texidor, Perier, avocat du Conseil national de l'ordre des pharmaciens.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 17 décembre 2014 statuant sur une plainte de la société d'exercice libéral par actions simplifiées (SELAS) " Laboratoire de biologie médicale des Cordeliers ", la chambre de discipline du conseil central de la section G de l'ordre des pharmaciens a infligé à M. B...la sanction de l'interdiction d'exercer la profession de pharmacien pour une durée de six mois ; que, par une décision du 22 juillet 2016, la chambre de discipline du Conseil national de l'ordre des pharmaciens a rejeté comme irrecevable l'appel de l'intéressé aux motifs qu'il n'avait pas signé sa requête et qu'il ne l'avait pas motivée dans le délai d'appel ; que M. B... se pourvoit en cassation contre cette décision ;<br/>
<br/>
              2. Considérant, en premier lieu, qu'en vertu d'une règle générale de procédure applicable, même sans texte, devant toute juridiction administrative, les requêtes et les mémoires doivent, à peine d'irrecevabilité, être signés par leur auteur ou son mandataire; que, saisie d'une requête ne respectant pas cette prescription, une juridiction administrative ne peut la rejeter comme irrecevable pour ce motif qu'après avoir invité le requérant à la régulariser, sauf dans le cas où une fin de non recevoir fondée sur le défaut de signature a été soulevée par une partie et communiquée au requérant ; que la régularisation de cette cause d'irrecevabilité peut intervenir tant que l'instruction n'a pas été close conformément aux dispositions des articles R. 613-1 à R. 613-4 du code de justice administrative, rendues applicables devant les chambres disciplinaires de l'ordre des pharmaciens par l'article R. 4234-33 du code de la santé publique ;<br/>
<br/>
              3. Considérant que l'article 4 de la loi du 31 décembre 1971 portant réforme de certaines professions judiciaires et juridiques donne qualité aux avocats pour " assister ou représenter les parties...devant les juridictions et les organismes juridictionnels ou disciplinaires de quelque nature que ce soit... " ; qu'il ressort des pièces du dossier soumis au juge du fond que Maître Rousseau, avocat au barreau de Bordeaux, avait qualité pour signer, au nom de M.B..., le mémoire de régularisation, enregistré au greffe de la chambre de discipline du Conseil national de l'ordre des pharmaciens, le 18 mai 2015, en réponse à la fin de non recevoir soulevée par la SELAS " Laboratoire de biologie médicale des Cordeliers " dans son mémoire du 12 mars 2015 et tirée du défaut de signature de la requête d'appel de M. B... ; qu'il suit de là qu'en faisant droit à la fin de non recevoir dont elle était saisie et en rejetant l'appel comme irrecevable, au motif que la requête ne portait pas la signature de M. B... et n'avait pu être régularisée par le mémoire produit par son avocat, la chambre de discipline a entaché sa décision d'erreur de droit ; <br/>
<br/>
              4. Considérant, en second lieu, que l'appel formé par un requérant devant la chambre de discipline du Conseil national de l'ordre des pharmaciens, juridiction administrative devant laquelle la procédure revêt un caractère essentiellement écrit, doit, à peine d'irrecevabilité, être assorti d'un exposé écrit des moyens invoqués ; que la chambre de discipline du Conseil national de l'ordre des pharmaciens peut, dès lors, rejeter un appel pour défaut de motivation lorsque le requérant, invité préalablement à régulariser sa requête, s'est abstenu de le faire ; qu'en revanche il résulte de l'article R. 4234-15 du code de la santé publique que l'appel, qui doit être formé dans le mois suivant la notification de la décision des premiers juges, peut être reçu au secrétariat du conseil national " par simple déclaration contre récépissé " et aucun texte ni aucun principe applicables à cette juridiction n'impose que la motivation soit exposée avant l'expiration du délai d'appel ; que, par suite, en jugeant que la requête de M. B...était également irrecevable faute pour celui-ci d'avoir présenté un mémoire motivé avant l'expiration du délai d'appel, la chambre de discipline du Conseil national de l'ordre des pharmaciens a entaché sa décision d'une seconde erreur de droit ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que M. B...est fondé à demander l'annulation de la décision par laquelle la chambre de discipline du Conseil national de l'ordre des pharmaciens a rejeté sa requête d'appel ;<br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la SELAS " Laboratoire de biologie médicale des Cordeliers " la somme de 3 000 euros à verser à M.B..., au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que les conclusions du Conseil national de l'ordre des pharmaciens, qui n'est pas partie à l'instance, présentées au titre de ces dispositions, sont irrecevables et doivent être rejetées ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La décision de la chambre de discipline du Conseil national de l'ordre des pharmaciens du 22 juillet 2016 est annulée.<br/>
<br/>
Article 2 : L'affaire est renvoyée devant la chambre discipline du Conseil national de l'ordre des pharmaciens.<br/>
<br/>
Article 3 : La société d'exercice libéral par actions simplifiées " Laboratoire de biologie médicale des Cordeliers " versera à M. A...B...une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions du Conseil national de l'ordre des pharmaciens présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à M. A...B...et à la société d'exercice libéral par actions simplifiées " Laboratoire de biologie médicale des Cordeliers ".<br/>
Copie en sera adressée au Conseil national de l'ordre des pharmaciens et à la ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-01-08 PROCÉDURE. INTRODUCTION DE L'INSTANCE. FORMES DE LA REQUÊTE. - 1) DÉFAUT DE SIGNATURE D'UNE REQUÊTE PAR L'AUTEUR OU SON MANDATAIRE - IRRECEVABILITÉ - 2)  POSSIBILITÉ DE RÉGULARISATION - EXISTENCE, JUSQU'À LA CLÔTURE DE L'INSTRUCTION [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-08-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. FORMES DE LA REQUÊTE. OBLIGATION DE MOTIVER LA REQUÊTE. - PROCÉDURE DEVANT LA CHAMBRE DE DISCIPLINE DU CONSEIL NATIONAL DE L'ORDRE DES PHARMACIENS - OBLIGATION DE MOTIVER LA REQUÊTE D'APPEL À PEINE D'IRRECEVABILITÉ - 1) EXISTENCE - 2) POSSIBILITÉ DE RÉGULARISATION - EXISTENCE, Y COMPRIS APRÈS L'EXPIRATION DU DÉLAI D'APPEL [RJ2].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-06-01 PROCÉDURE. JUGEMENTS. RÈGLES GÉNÉRALES DE PROCÉDURE. - SIGNATURE DES REQUÊTES ET MÉMOIRES PAR LEUR AUTEUR OU SON MANDATAIRE, À PEINE D'IRRECEVABILITÉ - 1) DÉFAUT DE SIGNATURE - CONSÉQUENCE - 2) POSSIBILITÉ DE RÉGULARISATION - EXISTENCE, JUSQU'À LA CLÔTURE DE L'INSTRUCTION [RJ1].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">55-04-01-01 PROFESSIONS, CHARGES ET OFFICES. DISCIPLINE PROFESSIONNELLE. PROCÉDURE DEVANT LES JURIDICTIONS ORDINALES. INTRODUCTION DE L'INSTANCE. - CHAMBRES DISCIPLINAIRES DE L'ORDRE DES PHARMACIENS - 1) A) DÉFAUT DE SIGNATURE DE LA REQUÊTE - IRRECEVABILITÉ - B) POSSIBILITÉ DE RÉGULARISATION - EXISTENCE, JUSQU'À LA CLÔTURE DE L'INSTRUCTION [RJ1] - C) ESPÈCE - 2) OBLIGATION DE MOTIVER LA REQUÊTE D'APPEL À PEINE D'IRRECEVABILITÉ - A) EXISTENCE - B) POSSIBILITÉ DE RÉGULARISATION - EXISTENCE, Y COMPRIS APRÈS L'EXPIRATION DU DÉLAI D'APPEL [RJ2].
</SCT>
<ANA ID="9A"> 54-01-08 1) En vertu d'une règle générale de procédure applicable, même sans texte, devant toute juridiction administrative, les requêtes et les mémoires doivent, à peine d'irrecevabilité, être signés par leur auteur ou son mandataire. Saisie d'une requête ne respectant pas cette prescription, une juridiction administrative ne peut la rejeter comme irrecevable pour ce motif qu'après avoir invité le requérant à la régulariser, sauf dans le cas où une fin de non-recevoir fondée sur le défaut de signature a été soulevée par une partie et communiquée au requérant.... ,,2) La régularisation de cette cause d'irrecevabilité peut intervenir tant que l'instruction n'a pas été close conformément aux dispositions des articles R. 613-1 à R. 613-4 du code de justice administrative.</ANA>
<ANA ID="9B"> 54-01-08-01 1) L'appel formé par un requérant devant la chambre de discipline du Conseil national de l'ordre des pharmaciens, juridiction administrative devant laquelle la procédure revêt un caractère essentiellement écrit, doit, à peine d'irrecevabilité, être assorti d'un exposé écrit des moyens invoqués. La chambre de discipline du Conseil national de l'ordre des pharmaciens peut, dès lors, rejeter un appel pour défaut de motivation lorsque le requérant, invité préalablement à régulariser sa requête, s'est abstenu de le faire.... ,,2) En revanche, il résulte de l'article R. 4234-15 du code de la santé publique que l'appel, qui doit être formé dans le mois suivant la notification de la décision des premiers juges, peut être reçu au secrétariat du conseil national par simple déclaration contre récépissé et aucun texte ni aucun principe applicables à cette juridiction n'impose que la motivation soit exposée avant l'expiration du délai d'appel.</ANA>
<ANA ID="9C"> 54-06-01 1) En vertu d'une règle générale de procédure applicable, même sans texte, devant toute juridiction administrative, les requêtes et les mémoires doivent, à peine d'irrecevabilité, être signés par leur auteur ou son mandataire. Saisie d'une requête ne respectant pas cette prescription, une juridiction administrative ne peut la rejeter comme irrecevable pour ce motif qu'après avoir invité le requérant à la régulariser, sauf dans le cas où une fin de non-recevoir fondée sur le défaut de signature a été soulevée par une partie et communiquée au requérant.... ,,2) La régularisation de cette cause d'irrecevabilité peut intervenir tant que l'instruction n'a pas été close conformément aux dispositions des articles R. 613-1 à R. 613-4 du code de justice administrative.</ANA>
<ANA ID="9D"> 55-04-01-01 1) a) En vertu d'une règle générale de procédure applicable, même sans texte, devant toute juridiction administrative, les requêtes et les mémoires doivent, à peine d'irrecevabilité, être signés par leur auteur ou son mandataire. Saisie d'une requête ne respectant pas cette prescription, une juridiction administrative ne peut la rejeter comme irrecevable pour ce motif qu'après avoir invité le requérant à la régulariser, sauf dans le cas où une fin de non-recevoir fondée sur le défaut de signature a été soulevée par une partie et communiquée au requérant.... ,,b) La régularisation de cette cause d'irrecevabilité peut intervenir tant que l'instruction n'a pas été close conformément aux dispositions des articles R. 613-1 à R. 613-4 du code de justice administrative, rendues applicables devant les chambres disciplinaires de l'ordre des pharmaciens par l'article R. 4234-33 du code de la santé publique (CSP).,,,c) Fin de non recevoir soulevée par un laboratoire tiré du défaut de signature de la requête d'appel du pharmacien sanctionné par une chambre de discipline de l'ordre des pharmaciens. Erreur de droit de la chambre de discipline du conseil national qui fait droit à cette fin de non recevoir alors qu'un mémoire de régularisation a été présenté avant la clôture de l'instruction.,,,2) a) L'appel formé par un requérant devant la chambre de discipline du Conseil national de l'ordre des pharmaciens, juridiction administrative devant laquelle la procédure revêt un caractère essentiellement écrit, doit, à peine d'irrecevabilité, être assorti d'un exposé écrit des moyens invoqués. La chambre de discipline du Conseil national de l'ordre des pharmaciens peut, dès lors, rejeter un appel pour défaut de motivation lorsque le requérant, invité préalablement à régulariser sa requête, s'est abstenu de le faire.... ,,b) En revanche, il résulte de l'article R. 4234-15 du CSP que l'appel, qui doit être formé dans le mois suivant la notification de la décision des premiers juges, peut être reçu au secrétariat du conseil national par simple déclaration contre récépissé et aucun texte ni aucun principe applicables à cette juridiction n'impose que la motivation soit exposée avant l'expiration du délai d'appel.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. CE, 18 décembre 1987,,, n° 89727, T. p. 879.,,[RJ2] Rappr., s'agissant d'un appel formé devant la commission supérieure des soins gratuits, CE, 3 décembre 2003, Pharmacie du soleil, n° 246315, T. pp. 846-880-940-957 ; s'agissant d'un appel formé devant les juridictions des pensions, CE, 11 novembre 2007,,, n° 299982, T. p. 972.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
