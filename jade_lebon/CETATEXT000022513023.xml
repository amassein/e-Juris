<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000022513023</ID>
<ANCIEN_ID>JG_L_2010_07_000000330308</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/22/51/30/CETATEXT000022513023.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 5ème sous-sections réunies, 23/07/2010, 330308, Publié au recueil Lebon</TITRE>
<DATE_DEC>2010-07-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>330308</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 5ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUIN-PALAT, BOUCARD ; SCP VIER, BARTHELEMY, MATUCHANSKY</AVOCATS>
<RAPPORTEUR>M. Christophe  Eoche-Duval</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Keller Rémi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2010:330308.20100723</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête, enregistrée le 31 juillet 2009 au secrétariat du contentieux du Conseil d'Etat, présentée pour M. Jérôme A, demeurant ... ; M. A demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision du 8 juillet 2009 par laquelle le conseil national de l'ordre des médecins a rejeté son recours tendant à l'annulation de la décision du 7 avril 2009 du conseil régional d'Ile-de-France rejetant son recours dirigé contre la décision du conseil départemental de la Ville de Paris du 30 janvier 2009 le rayant du tableau de l'ordre ;<br/>
<br/>
              2°) de mettre à la charge du conseil national de l'ordre des médecins la somme de 5 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la santé publique ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christophe Eoche-Duval, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Thouin-Palat, Boucard, avocat de M. A et de la SCP Vier, Barthélemy, Matuchansky, avocat du conseil national de l'ordre des médecins, <br/>
<br/>
              - les conclusions de M. Rémi Keller, rapporteur public,<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Thouin-Palat, Boucard, avocat de M. A et à la SCP Vier, Barthélemy, Matuchansky, avocat du conseil national de l'ordre des médecins ; <br/>
<br/>
<br/>
<br/>
<br/>
              Sans qu'il soit besoin d'examiner les autres moyens de la requête ;<br/>
<br/>
              Considérant qu'aux termes de l'article L. 4112-1 du code de la santé publique, dans sa rédaction applicable à la date de la décision attaquée : " Les médecins, les chirurgiens-dentistes et les sages-femmes qui exercent dans un département sont inscrits sur un tableau établi et tenu à jour par le conseil départemental de l'ordre dont ils relèvent. (...) / Nul ne peut être inscrit sur ce tableau s'il ne remplit pas les conditions requises par le présent titre (...) " ; qu'aux termes de l'article L. 4123-1  de ce code : " Le conseil départemental de l'ordre exerce, dans le cadre départemental et sous le contrôle du conseil national, les attributions générales de l'ordre, énumérées à l'article L. 4121-2. / Il statue sur les inscriptions au tableau (...) ", que selon l'article R.  4112-2 du même code : " A la réception de la demande (...) le conseil vérifie les titres du candidat et demande communication du bulletin n° 2 du casier judiciaire de l'intéressé. Il refuse l'inscription si le demandeur ne remplit pas les conditions nécessaires de moralité et d'indépendance ou s'il est constaté au vu d'un rapport d'expertise réalisée dans les conditions prévues à l'article R. 4124-3, une infirmité ou un état pathologique incompatible avec l'exercice de la profession (...) " ;<br/>
<br/>
              Considérant qu'il résulte de ces dispositions qu'il incombe au conseil départemental de l'ordre des médecins de tenir à jour le tableau relevant de son ressort et de radier de celui-ci les praticiens qui, par suite de l'intervention de circonstances postérieures à leur inscription, ont cessé de remplir les conditions requises pour y figurer ; que, pour cette procédure de mise à jour régulière du tableau, qui comporte une faculté de recours devant le conseil régional dont la décision est susceptible d'appel devant le conseil national, avant saisine éventuelle du juge de l'excès de pouvoir, les instances ordinales siègent dans leur formation administrative ; que les conditions de moralité mentionnées à l'article R. 4112-2 précité du code de la santé publique sont au nombre de celles qui doivent être remplies tant au moment de l'inscription que durant l'exercice de son art par le praticien après son inscription ; que, dès lors, la formation restreinte du conseil national de l'ordre des médecins a pu sans erreur de droit se fonder sur le motif que cette condition n'était plus remplie par M. A à la date à laquelle il s'est prononcé pour confirmer la décision de radiation prise par le conseil départemental puis le conseil régional ;<br/>
<br/>
              Considérant, toutefois, qu'il appartient aux instances ordinales, lorsqu'elles prononcent une radiation du tableau à raison de l'appréciation de la condition de moralité posée par l'article R. 4112-2 du code de la santé publique au vu de faits portés à leur connaissance postérieurement à l'inscription au tableau, de prendre en compte la gravité des faits intervenus et susceptibles de conduire à une telle mesure, comme l'urgence attachée à une telle décision ; <br/>
<br/>
              Considérant qu'il ressort des pièces du dossier, en particulier des pièces communiquées au conseil départemental de la ville de Paris par le conseil des médecins d'Irlande au titre de la coopération entre autorités européennes compétentes prévue par l'article 56 de la directive 2005/36/CE du 7 septembre 2005, que M. A a formé, le 11 mars 2006, une demande d'inscription au conseil des médecins d'Irlande et, en réponse aux questions posées, a certifié sur l'honneur n'avoir jamais fait l'objet de procédures disciplinaires de la part d'une autorité auprès de laquelle il serait ou aurait été inscrit, alors qu'il était l'objet en France, à la suite d'une décision du 11 février 2003 de la section des affaires sociales du conseil national de l'ordre des médecins devenue définitive après le rejet, le 20 mai 2005, d'un pourvoi en cassation devant le Conseil d'Etat, d'une mesure d'interdiction de donner des soins aux assurés sociaux pendant un an, rendue applicable à compter du 1er janvier 2006 ; que la décision du conseil national de l'ordre des médecins confirmant la radiation du tableau de l'ordre prononcée par le conseil départemental de la ville de Paris à l'encontre de M. A est fondée sur cette seule circonstance ; que cependant le fait d'avoir présenté cette déclaration mensongère ne suffit pas, par lui-même, à justifier légalement une décision de radiation du tableau de l'ordre des médecins, où M. A était inscrit depuis 1999 ; que, dès lors,  M. A est fondé à demander l'annulation de la décision du 8 juillet 2009 de la formation restreinte du conseil national de l'ordre des médecins ;<br/>
<br/>
              	Considérant qu'il y a lieu, dans les circonstances de l'espèce, de faire application des dispositions de l'article L. 761-1 du code de justice administrative et de mettre à la charge du conseil national de l'ordre des médecins le versement à M. A d'une somme de 3 000 euros ; que ces mêmes dispositions font obstacle à ce que soit mis à la charge de M. A qui n'est pas, dans la présente instance, la partie perdante, le versement d'une somme au titre des frais exposés par le conseil national de l'ordre des médecins et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>
<br/>
              				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La décision du 8 juillet 2009 du conseil national de l'ordre des médecins est annulée.<br/>
<br/>
Article 2 : Le conseil national de l'ordre des médecins versera à M. A une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : Les conclusions du conseil national de l'ordre des médecins tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article  4 : La présente décision sera notifiée à M. Jérôme A et au conseil national de l'ordre des médecins.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">55-01-02-01 PROFESSIONS, CHARGES ET OFFICES. ORDRES PROFESSIONNELS - ORGANISATION ET ATTRIBUTIONS NON DISCIPLINAIRES. QUESTIONS PROPRES À CHAQUE ORDRE PROFESSIONNEL. ORDRE DES MÉDECINS. - RADIATION DU TABLEAU PAR VOIE ADMINISTRATIVE, LA CONDITION DE MORALITÉ N'ÉTANT PLUS REMPLIE - 1) POSSIBILITÉ - EXISTENCE - 2) ESPÈCE - RADIATION INJUSTIFIÉE PAR LE SEUL MOTIF D'UNE DÉCLARATION MENSONGÈRE.
</SCT>
<ANA ID="9A"> 55-01-02-01 Il incombe au conseil départemental de l'ordre des médecins de tenir à jour le tableau relevant de son ressort et de radier de celui-ci les praticiens qui, par suite de l'intervention de circonstances postérieures à leur inscription, ont cessé de remplir les conditions requises pour y figurer. Pour cette procédure de mise à jour régulière du tableau, les instances ordinales siègent dans leur formation administrative.... ...1) Les conditions de moralité mentionnées à l'article R. 4112-2 du code de la santé publique sont au nombre de celles qui doivent être remplies tant au moment de l'inscription que durant l'exercice de son art par le praticien après son inscription. Dès lors, une radiation par la voie administrative peut être fondée sur le motif que cette condition n'est plus remplie.,,2) En l'espèce, le praticien a fait une déclaration mensongère lors d'une demande d'inscription au conseil des médecins d'Irlande, en omettant une procédure disciplinaire conduite à son encontre. La radiation du tableau de l'ordre, en France, pour ce seul motif n'est pas légalement justifiée.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
