<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026068971</ID>
<ANCIEN_ID>JG_L_2012_06_000000335169</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/06/89/CETATEXT000026068971.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 25/06/2012, 335169</TITRE>
<DATE_DEC>2012-06-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>335169</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Edmond Honorat</PRESIDENT>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; SCP PEIGNOT, GARREAU, BAUER-VIOLAS</AVOCATS>
<RAPPORTEUR>Mme Sophie Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:335169.20120625</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés le 31 décembre 2009 et le 30 mars 2010 au secrétariat du contentieux du Conseil d'Etat, présentés par le MINISTRE D'ETAT, MINISTRE DE L'ECOLOGIE, DE L'ENERGIE, DU DEVELOPPEMENT DURABLE ET DE LA MER, EN CHARGE DES TECHNOLOGIES VERTES ET DES NEGOCIATIONS SUR LE CLIMAT ; le MINISTRE demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 08NC00055 du 19 octobre 2009 par lequel la cour administrative d'appel de Nancy a annulé, à la demande de M. Frémont, d'une part, le jugement n° 0601398 du 23 octobre 2007 du tribunal administratif de Nancy, d'autre part, l'arrêté du 10 juillet 2006 du préfet de la Meuse modifiant la liste des terrains devant être soumis à l'action de l'association communale de chasse agréée de Landrecourt-Lempire ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M. Frémont ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'environnement ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie Roussel, Auditeur,  <br/>
<br/>
              - les observations de la SCP Peignot, Garreau, Bauer-Violas, avocat de M. A, <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ; <br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Peignot, Garreau, Bauer-Violas, avocat M. Bruno A ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant que, par un arrêté du 10 juillet 2006, le préfet de la Meuse a modifié la liste des terrains devant être soumis à l'action de l'association communale de chasse agréée de Landrecourt-Lempire en y incluant la parcelle actuellement cadastrée A1 3 de M. Frémont, sur laquelle ce dernier possède un droit de chasse ; que, par un jugement du 23 octobre 2007, le tribunal administratif de Nancy a rejeté la demande de M. Frémont tendant à l'annulation de cet arrêté ; que, par un arrêt du 19 octobre 2009, contre lequel le MINISTRE D'ETAT, MINISTRE DE L'ECOLOGIE, DE L'ENERGIE, DU DEVELOPPEMENT DURABLE ET DE LA MER, EN CHARGE DES TECHNOLOGIES VERTES ET DES NEGOCIATIONS SUR LE CLIMAT se pourvoit en cassation, la cour administrative d'appel de Nancy a fait droit à l'appel interjeté par M. Frémont en annulant le jugement du tribunal administratif de Nancy ainsi que l'arrêté préfectoral litigieux ;<br/>
<br/>
              Considérant qu'aux termes de l'article L. 422-10 du code de l'environnement : " L'association communale est constituée sur les terrains autres que ceux : / (...) 3° Ayant fait l'objet de l'opposition des propriétaires ou détenteurs de droits de chasse sur des superficies d'un seul tenant supérieures aux superficies minimales mentionnées à l'article L. 422-13 " ; que, selon le I de l'article L. 422-13 du même code : " Pour être recevable, l'opposition des propriétaires ou détenteurs de droits de chasse mentionnés au 3° de l'article L. 422-10 doit porter sur des terrains d'un seul tenant et d'une superficie minimum de vingt hectares " ; qu'aux termes des dispositions de l'article R. 422-42 du même code : " Le territoire de chasse pouvant faire l'objet d'une opposition en vertu du 3° de l'article L. 422-10 doit être d'un seul tenant. Les voies ferrées, routes, chemins, canaux et cours d'eau non domaniaux ainsi que les limites de communes n'interrompent pas la continuité des fonds " ; <br/>
<br/>
              Considérant qu'il résulte de l'ensemble de ces dispositions que le droit d'opposition d'un propriétaire foncier à l'apport forcé de ses terrains au territoire de chasse d'une association communale de chasse agréée est attaché à une superficie minimale afin de garantir que l'exercice de ce droit ne compromette pas la gestion rationnelle des ressources cynégétiques ; que, pour apprécier cette condition, plusieurs parcelles appartenant au même propriétaire peuvent être agrégées, dès lors qu'elles forment un ensemble d'un seul tenant ; que l'exigence de continuité des fonds doit être regardée comme remplie dès lors que les différentes parcelles en cause se touchent, même par un seul point ; qu'en outre, dès lors qu'ils ne font que traverser un fonds d'un seul tenant, les voies ferrées, routes, chemins, canaux et cours d'eau non domaniaux n'en interrompent pas la continuité ; qu'en revanche, de telles voies ne sauraient avoir pour effet de créer une continuité entre des parcelles qu'elles relieraient mais qui ne se toucheraient en aucun point ; <br/>
<br/>
              Considérant qu'il ressort des énonciations de l'arrêt attaqué que la parcelle cadastrée A1 3, propriété de M. Frémont, est séparée des parcelles cadastrées AC20-AC21, également propriété de M. Frémont, par un important croisement de deux chemins ruraux ainsi que par les parcelles cadastrées A 82 et A 14, situées d'un côté de ce croisement, et par la parcelle ZE 62, située de l'autre côté du croisement, appartenant toutes à des tiers ; <br/>
<br/>
              Considérant que, pour annuler le jugement du tribunal administratif de Nancy du 23 septembre 2007 et l'arrêté préfectoral attaqués, la cour administrative d'appel a jugé, après avoir souverainement apprécié les faits de l'espèce, que la circonstance que les deux parties constituant le territoire de chasse de M. Frémont soient séparées par les parcelles A82, A14 et ZE 62, propriété de tiers, n'était pas de nature, dès lors que les parcelles de M. Frémont comme celles appartenant à des tiers jouxtaient toutes un même croisement de chemins ruraux, à faire juridiquement obstacle à la continuité de son fonds ; qu'en en déduisant que les parcelles appartenant à M. Frémont formaient un ensemble d'un seul tenant, la cour administrative d'appel de Nancy n'a pas commis d'erreur de droit ni entaché son arrêt d'insuffisance de motivation ; <br/>
<br/>
              Considérant qu'il résulte de ce qui précède que le pourvoi du MINISTRE D'ETAT, MINISTRE DE L'ECOLOGIE, DE L'ENERGIE, DU DEVELOPPEMENT DURABLE ET DE LA MER, EN CHARGE DES TECHNOLOGIES VERTES ET DES NEGOCIATIONS SUR LE CLIMAT doit être rejeté ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à M. Frémont, au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : Le pourvoi du MINISTRE D'ETAT, MINISTRE DE L'ECOLOGIE, DE L'ENERGIE, DU DEVELOPPEMENT DURABLE ET DE LA MER, EN CHARGE DES TECHNOLOGIES VERTES ET DES NEGOCIATIONS SUR LE CLIMAT est rejeté.<br/>
<br/>
Article 2 : L'Etat versera à M. Bruno Frémont une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 3 : La présente décision sera notifiée à M. Bruno Frémont, à l'association communale de chasse agréée de Landrecourt-Lempire et à la ministre de l'écologie, du développement durable et de l'énergie. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">44-046-04 NATURE ET ENVIRONNEMENT. - DROIT D'OPPOSITION À L'APPORT FORCÉ - CONDITION DE SUPERFICIE MINIMALE DU TERRAIN - POSSIBILITÉ, POUR APPRÉCIER CETTE CONDITION, D'AGRÉGER PLUSIEURS PARCELLES FORMANT UN ENSEMBLE D'UN SEUL TENANT - NOTION - 1) PARCELLES SE TOUCHANT MÊME EN UN SEUL POINT - 2) INCIDENCE SUR LA CONTINUITÉ DES PARCELLES DE VOIES LES TRAVERSANT OU LES RELIANT - ABSENCE.
</SCT>
<ANA ID="9A"> 44-046-04 Le droit d'opposition d'un propriétaire foncier à l'apport forcé de ses terrains au territoire de chasse d'une association communale de chasse agréée est attaché à une superficie minimale. Pour apprécier cette condition, plusieurs parcelles appartenant au même propriétaire peuvent être agrégées, dès lors qu'elles forment un ensemble d'un seul tenant. 1) L'exigence de continuité des fonds doit être regardée comme remplie dès lors que les différentes parcelles en cause se touchent, même par un seul point. 2) Les voies ferrées, routes, chemins, canaux et cours d'eau non domaniaux qui ne font que traverser un fonds d'un seul tenant n'en interrompent pas la continuité. En revanche, de telles voies ne sauraient avoir pour effet de créer une continuité entre des parcelles qu'elles relieraient mais qui ne se toucheraient en aucun point.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
