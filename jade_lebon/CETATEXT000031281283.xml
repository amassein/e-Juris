<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031281283</ID>
<ANCIEN_ID>JG_L_2015_10_000000383787</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/28/12/CETATEXT000031281283.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème / 5ème SSR, 05/10/2015, 383787</TITRE>
<DATE_DEC>2015-10-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>383787</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème / 5ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Benjamin de Maillard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:383787.20151005</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante : <br/>
<br/>
              L'association de la santé au travail de la Haute-Marne a demandé au juge des référés du tribunal administratif de Châlons-en-Champagne, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de la décision du 27 mai 2014 par laquelle le directeur régional des entreprises, de la concurrence, de la consommation, du travail et de l'emploi de Champagne-Ardenne lui a retiré l'agrément visé à l'article D. 4622-48 du code du travail. <br/>
<br/>
              Par une ordonnance n° 1401413 du 31 juillet 2014, le juge des référés du tribunal administratif a suspendu l'exécution de cette décision.<br/>
<br/>
              Par un pourvoi, enregistré le 19 août 2014 au secrétariat du contentieux du Conseil d'Etat, le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de l'association de la santé au travail de la Haute-Marne.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Benjamin de Maillard, auditeur,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que les écritures de l'association de la santé au travail de la Haute-Marne, qui ont été présentées sans le ministère d'un avocat au Conseil d'Etat et à la Cour de cassation, bien qu'elle ait été informée de l'obligation de recourir à ce ministère, doivent être écartées des débats ;<br/>
<br/>
              2. Considérant qu'aux termes des dispositions de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision (...) " ;<br/>
<br/>
              3. Considérant qu'aux termes de l'article D. 4622-48 du code du travail : " Chaque service de santé au travail fait l'objet d'un agrément, pour une période de cinq ans, par le directeur régional des entreprises, de la concurrence, de la consommation, du travail et de l'emploi, après avis du médecin inspecteur du travail. (...) " ; qu'aux termes de l'article D. 4622-51 du même code, dans sa rédaction applicable au litige : " Lorsque le directeur régional des entreprises, de la concurrence, de la consommation, du travail et de l'emploi constate que les conditions de fonctionnement du service de santé ne satisfont pas aux obligations résultant des dispositions du présent titre, il peut, après avis du médecin inspecteur du travail : / 1° Soit mettre fin à l'agrément accordé et délivrer un agrément pour une durée maximale de deux ans non renouvelable, sous réserve d'un engagement précis et daté de mise en conformité de la part du service de santé au travail ; lorsqu'à l'issue de cette période, le service de santé au travail satisfait à ces obligations, l'agrément lui est accordé pour cinq ans ; / 2° Soit modifier ou retirer, par une décision motivée, l'agrément délivré. / Ces mesures ne peuvent intervenir que lorsque le service de santé au travail, invité par lettre recommandée avec avis de réception à se mettre en conformité dans un délai fixé par le directeur régional à six mois au maximum, n'a pas accompli dans ce délai les diligences nécessaires. / Le président du service de santé au travail informe individuellement les entreprises adhérentes de la modification ou du retrait de l'agrément " ; qu'aux termes des dispositions de l'article R. 4622-52 : " Le silence gardé pendant plus de quatre mois sur une demande d'agrément ou de renouvellement d'agrément vaut décision d'agrément. / Le silence gardé pendant plus de quatre mois par le ministre chargé du travail saisi d'un recours hiérarchique sur une décision relative à l'agrément vaut décision d'agrément " ; que les " décisions relatives à l'agrément " mentionnées à ce dernier article renvoient aux seules décisions par lesquelles le directeur régional des entreprises, de la concurrence, de la consommation, du travail et de l'emploi refuse de délivrer ou de renouveler un agrément ; <br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis au juge des référés du tribunal administratif que l'association de santé au travail de la Haute-Marne, qui s'était vu délivrer le 20 décembre 2012 un agrément de deux ans en application des dispositions du 1° de l'article D. 4622-51 du code du travail citées ci-dessus, a fait l'objet le 31 juillet 2013 d'une mise en demeure par le directeur régional des entreprises, de la concurrence, de la consommation, du travail et de l'emploi (DIRECCTE) de Champagne-Ardenne, contre laquelle elle a formé un recours hiérarchique sur lequel le ministre chargé du travail a gardé le silence ; que le 27 mai 2014, le DIRECCTE de Champagne-Ardenne a pris une décision retirant l'agrément qu'il avait délivré le 20 décembre 2012 ; que le ministre chargé du travail se pourvoit contre l'ordonnance par laquelle le juge des référés du tribunal administratif de Châlons-en-Champagne a fait droit à la demande de suspension de l'exécution de cette décision ;<br/>
<br/>
              5. Considérant que, pour suspendre l'exécution de la décision du 27 mai 2014 par laquelle le DIRECCTE de Champagne-Ardenne a, sur le fondement des dispositions de l'avant-dernier alinéa de l'article D. 4622-51 du code du travail, retiré l'agrément dont bénéficiait l'association de santé au travail de la Haute-Marne, le juge des référés du tribunal administratif de Châlons-en-Champagne a jugé que le silence gardé par le ministre chargé du travail sur le recours hiérarchique formé contre la mise en demeure du 31 juillet 2013 avait fait naître une nouvelle décision d'agrément au profit de l'association et que, dès lors, le moyen tiré de ce que la décision de retrait aurait dû être précédé d'une nouvelle mise en demeure était propre à créer un doute sérieux ; qu'en se prononçant ainsi, alors que la mise en demeure de se mettre en conformité adressée par le DIRECCTE à un service de santé au travail en application des dispositions citées ci-dessus du 2° de l'article D. 4622-51 du code du travail n'est pas une " décision relative à l'agrément ", au sens de l'article R. 4622-52 du même code, et que le silence gardé par le ministre chargé du travail sur le recours hiérarchique formé contre cette mise en demeure n'avait donc pu valoir nouvel agrément, le juge des référés du tribunal administratif de Châlons-en-Champagne a entaché son ordonnance d'une erreur de droit ; que, par suite, et sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, l'ordonnance attaquée doit être annulée ;<br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au titre de la procédure de référé engagée, en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              7. Considérant que l'agrément délivré à l'association de la santé au travail de la Haute-Marne le 20 décembre 2012 ayant, ainsi qu'il a été dit ci-dessus, une durée de deux ans, la demande tendant à la suspension de son retrait a, s'agissant d'un acte dont la validité serait désormais expirée s'il était resté en vigueur, perdu son objet ; que, par suite, il n'y a pas lieu de statuer sur la demande présentée par cette association ; <br/>
<br/>
<br/>
<br/>
<br/>
              				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : L'ordonnance du 31 juillet 2014 du juge des référés du tribunal administratif de Châlons-en-Champagne est annulée. <br/>
Article 2 : Il n'y a pas lieu de statuer sur les conclusions de la demande de l'association de la santé au travail de la Haute-Marne tendant à la suspension de l'exécution de la décision du 27 mai 2014 par laquelle le directeur régional des entreprises, de la concurrence, de la consommation, du travail et de l'emploi de Champagne-Ardenne lui a retiré l'agrément délivré le 20 décembre 2012.<br/>
Article 3 : La présente décision sera notifiée à la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social et à l'association de la santé au travail de la Haute-Marne.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">66-03-04 TRAVAIL ET EMPLOI. CONDITIONS DE TRAVAIL. MÉDECINE DU TRAVAIL. - AGRÉMENT DES SERVICES DE SANTÉ AU TRAVAIL (ART. D. 4622-48 DU CODE DU TRAVAIL) - EFFETS DU SILENCE DE L'ADMINISTRATION SUR LES DEMANDES D'AGRÉMENT ET LES RECOURS HIÉRARCHIQUES (ART. R. 4622-52 DU MÊME CODE) - INTERPRÉTATION DE LA RÈGLE.
</SCT>
<ANA ID="9A"> 66-03-04 Agrément des services de santé au travail (art. D. 4622-48 du code du travail). L'article R. 4622-52 du même code prévoit que le silence gardé pendant plus de quatre mois sur une demande d'agrément ou de renouvellement d'agrément vaut décision d'agrément et que le silence gardé pendant plus de quatre mois par le ministre chargé du travail saisi d'un recours hiérarchique sur une décision relative à l'agrément vaut décision d'agrément. Pour l'application de cet article, les « décisions relatives à l'agrément » renvoient aux seules décisions de refus de délivrer ou de renouveler un agrément.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
