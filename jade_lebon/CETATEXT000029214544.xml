<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029214544</ID>
<ANCIEN_ID>JG_L_2014_07_000000373295</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/21/45/CETATEXT000029214544.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème / 7ème SSR, 09/07/2014, 373295, Publié au recueil Lebon</TITRE>
<DATE_DEC>2014-07-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>373295</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème / 7ème SSR</FORMATION>
<TYPE_REC/>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER ; SCP TIFFREAU, MARLANGE, DE LA BURGADE</AVOCATS>
<RAPPORTEUR>M. Paul Bernard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:373295.20140709</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 15 novembre 2013 et 14 février 2014 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune de Chelles, représentée par son maire ; la commune de Chelles demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler le jugement n° 1201448/4 du 18 septembre 2013 par lequel le tribunal administratif de Melun a, sur la demande de la société Orange France, d'une part, annulé pour excès de pouvoir l'arrêté du 12 décembre 2011 par lequel le maire de Chelles s'est opposé à la déclaration préalable déposée le 18 octobre 2013 par la société Orange France en vue de la construction d'un relais de téléphonie mobile comportant un mât support d'antenne et un local technique, sur un terrain situé 14 bis, chemin du Sempin à Chelles et, d'autre part, enjoint au maire de statuer à nouveau sur cette déclaration préalable dans un délai de quinze jours à compter de la notification du jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de la société Orange France ; <br/>
<br/>
              3°) de mettre à la charge de la société Orange France la somme de 3 000 euros en application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Paul Bernard, maître des requêtes,<br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Blancpain, Soltner, avocat de la commune de Chelles, et à la SCP Tiffreau, Marlange, de la Burgade, avocat de la société Orange France ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société Orange France a déposé, le 18 octobre 2011, une déclaration préalable à la construction d'un relais de téléphonie mobile comportant un mât support d'antenne et un local technique attenant sur le territoire de la commune de Chelles (Seine-et-Marne) ; que, par un arrêté du 12 décembre 2011, le maire de Chelles s'est opposé à ce projet au motif qu'il était de nature à porter atteinte au caractère des lieux avoisinants et aux paysages naturels et urbains ; que, par un jugement du 18 septembre 2013, le tribunal administratif de Melun, saisi par la société Orange France, a annulé pour excès de pouvoir cet arrêté du maire de Chelles et a enjoint au maire de réexaminer la demande dans un délai déterminé ; que la commune de Chelles se pourvoit en cassation contre ce jugement ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 421-1 du code de l'urbanisme : " Les constructions, même ne comportant pas de fondations, doivent être précédées de la délivrance d'un permis de construire (...) " ; qu'aux termes de l'article L. 421-4 du même code : " Un décret en Conseil d'Etat arrête la liste des constructions, aménagements, installations et travaux qui, en raison de leurs dimensions, de leur nature ou de leur localisation, ne justifient pas l'exigence d'un permis et font l'objet d'une déclaration préalable (...) " ; que, selon l'article L. 421-5 du même code, un décret en Conseil d'Etat arrête la liste des constructions, aménagements, installations et travaux qui, par dérogation aux dispositions des articles L. 421-1 à L. 421-4, sont dispensés de toute formalité au titre de ce code en raison, notamment, de leur très faible importance ; <br/>
<br/>
              3. Considérant qu'en vertu de l'article R. 421-1 du code de l'urbanisme, les constructions nouvelles doivent être précédées de la délivrance d'un permis de construire à l'exception des constructions mentionnées aux articles R. 421-2 à R. 421-8 du même code, qui sont dispensées de toute formalité au titre du code de l'urbanisme, et des constructions mentionnées aux articles R. 421-9 à R. 421-12, qui doivent faire l'objet d'une déclaration préalable ; que selon le a) de l'article R. 421-2 du même code, dans sa rédaction applicable à la date de la décision contestée, les constructions nouvelles dont la hauteur au-dessus du sol est inférieure à douze mètres et qui n'ont pas pour effet de créer de surface de plancher ou qui ont pour effet de créer une surface hors oeuvre brute inférieure ou égale à deux mètres carrés sont dispensées, en dehors des secteurs sauvegardés et des sites classés, de toute formalité au titre du code de l'urbanisme, en raison de leur nature ou de leur très faible importance ; qu'en vertu du a) de l'article R. 421-9 du même code, dans sa rédaction applicable à la date de la décision contestée, doivent faire l'objet d'une déclaration préalable, en dehors des secteurs sauvegardés et des sites classés, les constructions nouvelles n'étant pas dispensées de toute formalité au titre du code qui ont " pour effet de créer une surface hors oeuvre brute supérieure à deux mètres carrés et inférieure ou égale à vingt mètres carrés " ; qu'en vertu des dispositions du c) du même article, sont également soumises à autorisation préalable les constructions " dont la hauteur au-dessus du sol est supérieure à douze mètres et qui n'ont pas pour effet de créer de surface hors oeuvre brute ou qui ont pour effet de créer une surface hors oeuvre brute inférieure ou égale à deux mètres carrés ", ces dernières dispositions n'étant pas applicables aux éoliennes et aux ouvrages de production d'électricité à partir de l'énergie solaire ; <br/>
<br/>
              4. Considérant, d'une part, qu'il résulte de la combinaison des dispositions qui précèdent que les antennes relais de téléphonie mobile dont la hauteur est supérieure à douze mètres et dont les installations techniques nécessaires à leur fonctionnement entraînent la création d'une surface hors oeuvre brute de plus de deux mètres carrés n'entrent pas, en raison de ce qu'elles constituent nécessairement un ensemble fonctionnel indissociable, dans le champ des exceptions prévues au a) et au c) de l'article R. 421-9 du code de l'urbanisme et doivent faire l'objet d'un permis de construire en vertu des articles L. 421-1 et R. 421-1 du même code ; <br/>
<br/>
              5. Considérant, d'autre part, que lorsqu'il est constaté que des travaux sont, en vertu des dispositions du code de l'urbanisme, soumis à l'obligation d'obtenir un permis de construire mais n'ont fait l'objet que d'une simple déclaration, le maire est tenu de s'opposer aux travaux déclarés et d'inviter le pétitionnaire à présenter une demande de permis de construire ; <br/>
<br/>
              6. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la déclaration préalable déposée par la société Orange France le 18 octobre 2011 avait pour objet la construction d'une antenne relais de téléphonie mobile composée, d'une part, d'un pylône de 24 mètres et, d'autre part, d'un local technique, d'une surface de plancher de 8,50 mètres carrés, indissociable du pylône ; que, par suite, le projet de la société Orange France devait faire l'objet d'un permis de construire ; que, dès lors, le maire de Chelles était tenu, ainsi qu'il l'a fait, de s'opposer aux travaux déclarés ; qu'il s'ensuit que le tribunal administratif a commis une erreur de droit en se fondant, pour annuler l'arrêté par lequel le maire de Chelles s'est opposé aux travaux déclarés, sur un moyen qui ne pouvait qu'être écarté comme inopérant ; que son jugement doit, en conséquence et sans qu'il soit besoin d'examiner les moyens du pourvoi, être annulé ; <br/>
<br/>
              7. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              8. Considérant qu'ainsi qu'il a été dit précédemment le maire de Chelles était tenu, ainsi qu'il l'a fait, de s'opposer aux travaux déclarés par la société Orange France, lesquels devaient faire l'objet d'une demande de permis de construire ; que, par suite, les moyens soulevés par la société Orange France à l'appui des conclusions à fin d'annulation formées contre l'arrêté du maire de Chelles ne peuvent être qu'écartés comme inopérants ; que les conclusions à fin d'annulation et d'injonction de la société Orange France ne peuvent, en conséquence, qu'être rejetées ;<br/>
<br/>
              9. Considérant que, dans les circonstances de l'espèce, il y a lieu de mettre à la charge de la société Orange France la somme de 3 000 euros à verser à la commune de Chelles au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que les dispositions de cet article font obstacle à ce qu'une somme soit mise à la charge de la commune de Chelles à ce même titre ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Melun du 18 septembre 2013 est annulé. <br/>
<br/>
Article 2 : La demande présentée par la société Orange France devant le tribunal administratif de Melun et les conclusions présentées par cette société devant le Conseil d'Etat sont rejetées. <br/>
<br/>
Article 3 : La société Orange France versera à la commune de Chelles la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la société Orange France et à la commune de Chelles. <br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-07-01-04-01-02 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. MOYENS D'ORDRE PUBLIC À SOULEVER D'OFFICE. EXISTENCE. - URBANISME - COMPÉTENCE LIÉE DU MAIRE POUR S'OPPOSER À DES TRAVAUX QUI ONT FAIT L'OBJET D'UNE SIMPLE DÉCLARATION LORSQU'IL CONSTATE QU'ILS DEVAIENT LÉGALEMENT FAIRE L'OBJET D'UN PERMIS DE CONSTRUIRE [RJ1] - MOYEN TIRÉ DE L'ERREUR DE DROIT DU JUGE DU FOND À ACCUEILLIR, POUR ANNULER L'OPPOSITION À TRAVAUX, UN MOYEN RENDU INOPÉRANT DU FAIT DE LA COMPÉTENCE LIÉE [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-03-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. TRAVAUX SOUMIS AU PERMIS. - TRAVAUX N'AYANT FAIT L'OBJET QUE D'UNE SIMPLE DÉCLARATION - 1) COMPÉTENCE LIÉE DU MAIRE QUI LE CONSTATE POUR S'OPPOSER AUX TRAVAUX DÉCLARÉS - EXISTENCE [RJ1] - 2) CONTENTIEUX - CONTESTATION DE L'OPPOSITION À TRAVAUX - ERREUR DE DROIT DU JUGE DU FOND À ACCUEILLIR UN MOYEN RENDU INOPÉRANT DU FAIT DE LA COMPÉTENCE LIÉE - MOYEN D'ORDRE PUBLIC SOULEVÉ D'OFFICE PAR LE JUGE DE CASSATION - EXISTENCE [RJ2].
</SCT>
<ANA ID="9A"> 54-07-01-04-01-02 Lorsqu'il est constaté que des travaux sont, en vertu des dispositions du code de l'urbanisme, soumis à l'obligation d'obtenir un permis de construire mais n'ont fait l'objet que d'une simple déclaration, le maire est tenu de s'opposer aux travaux déclarés et d'inviter le pétitionnaire à présenter une demande de permis de construire. L'erreur de droit commise par un juge du fond qui, dans une telle configuration, annule pour excès de pouvoir l'arrêté par lequel l'administration s'est opposée aux travaux, en se fondant sur un moyen qui ne pouvait qu'être écarté comme inopérant, est relevée d'office par le juge de cassation.</ANA>
<ANA ID="9B"> 68-03-01 1) Lorsqu'il est constaté que des travaux sont, en vertu des dispositions du code de l'urbanisme, soumis à l'obligation d'obtenir un permis de construire mais n'ont fait l'objet que d'une simple déclaration, le maire est tenu de s'opposer aux travaux déclarés et d'inviter le pétitionnaire à présenter une demande de permis de construire.,,,2) L'erreur de droit commise par un juge du fond qui, dans une telle configuration, annule pour excès de pouvoir l'arrêté par lequel l'administration s'est opposée aux travaux, en se fondant sur un moyen qui ne pouvait qu'être écarté comme inopérant, est relevée d'office par le juge de cassation.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., s'agissant du cantonnement de la compétence liée aux cas où l'autorité administrative se borne à opérer un constat sans porter d'appréciation, CE, Section, 3 février 1999, Montaignac, n°s 149722 152848, p. 6.,,[RJ2] Cf. CE, 4 juin 2014, Société Opilo et Société EURL Paris Plage, n°s 368254 368427, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
