<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036951470</ID>
<ANCIEN_ID>JG_L_2018_05_000000417350</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/95/14/CETATEXT000036951470.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 5ème chambres réunies, 25/05/2018, 417350, Publié au recueil Lebon</TITRE>
<DATE_DEC>2018-05-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417350</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 5ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>Mme Airelle Niepce</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHR:2018:417350.20180525</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le préfet des Yvelines a déféré au tribunal administratif de Versailles l'arrêté du 10 mars 2017 par lequel le maire de la commune de Mantes-la-Ville a refusé de délivrer un permis de construire sollicité par l'association des musulmans de Mantes sud (AMMS) tendant à la réalisation de travaux sur une construction existante, pour le changement de destination d'un bâtiment situé 10-12 rue des merisiers, en vue de créer le nouveau centre cultuel musulman de la commune. L'AMMS a demandé au même tribunal l'annulation de cet arrêté.<br/>
<br/>
              Par un jugement nos 1703192, 1703332 du 16 janvier 2018, le tribunal administratif de Versailles, a, d'une part, annulé l'arrêté du 10 mars 2017 et sursis à statuer sur les conclusions aux fins d'injonction et d'astreinte du déféré préfectoral et de la demande de l'association, d'autre part, transmis le dossier, en vertu des dispositions de l'article L. 113-1 du code de justice administrative, au Conseil d'Etat, en soumettant à son examen les questions suivantes : <br/>
<br/>
              1°) La combinaison des dispositions des articles L. 911-1 du code de justice administrative et des articles L. 600-4-1 et L. 424-3 du code de l'urbanisme et de la possibilité de présenter ou non une demande de substitution de motifs conduit-elle à ce que l'annulation d'un refus de permis de construire opposé après l'entrée en vigueur de l'article 108 de la loi du 6 août 2015 implique nécessairement que le juge administratif enjoigne à l'administration d'accorder le permis demandé, le cas échéant en l'assortissant de prescriptions ' Dans l'affirmative, avant d'exercer son pouvoir d'injonction, le juge administratif est-il préalablement tenu de demander spécifiquement aux parties de lui faire part de toute autre circonstance de droit ou de fait s'opposant à la délivrance du permis ' La solution est-elle différente selon la nature des moyens d'annulation retenus, notamment si le jugement accueille un moyen d'annulation tiré du détournement de pouvoir '<br/>
<br/>
              2°) Dans l'hypothèse d'une réponse négative aux questions précédentes, les dispositions de l'article L. 911-1 du code de justice administrative limitant le pouvoir d'injonction du juge à la prescription d'une mesure d'exécution dans un sens déterminé permettent-elles au juge d'enjoindre à l'administration de " ne pas " rejeter la demande de permis de construire, alors qu'une telle injonction laisse ouverte la possibilité pour l'administration de choisir entre autant de décisions qu'existent de possibilités de prescriptions '<br/>
<br/>
              3°) Dans l'hypothèse d'une réponse négative aux questions précédentes, le juge peut-il, en vertu des dispositions de l'article L. 911-2 du code de justice administrative, prescrire le réexamen de la demande de permis de construire, le cas échéant sous astreinte, en assortissant les motifs de son jugement d'une mention selon laquelle l'autorité administrative ne saurait, sauf circonstances de fait ou de droit nouvelles, rejeter de nouveau la demande sans méconnaitre l'autorité de la chose jugée qui s'attache à la décision d'annulation et aux motifs qui en constituent le soutien nécessaire '<br/>
<br/>
              Par un mémoire, enregistré le 2 février 2018, l'Association des musulmans de Mantes sud a présenté des observations.<br/>
<br/>
              Par un mémoire, enregistré le 5 avril 2018, le ministre de la cohésion des territoires a présenté des observations.<br/>
<br/>
              La commune de Mantes-la-Ville, invitée à produire, n'a pas produit d'observations. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - la loi n° 2015-990 du 6 août 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
- le rapport de Mme Airelle Niepce, maître des requêtes, <br/>
<br/>
- les conclusions de Mme Julie Burguburu, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat de l'association des musulmans de Mantes sud.<br/>
<br/>
<br/>REND L'AVIS SUIVANT :<br/>
<br/>
              1.	En premier lieu, aux termes de l'article L. 911-1 du code de justice administrative : " Lorsque sa décision implique nécessairement qu'une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public prenne une mesure d'exécution dans un sens déterminé, la juridiction, saisie de conclusions en ce sens, prescrit, par la même décision, cette mesure assortie, le cas échéant, d'un délai d'exécution. ". Lorsque l'exécution d'un jugement ou d'un arrêt implique normalement, eu égard aux motifs de ce jugement ou de cet arrêt, une mesure dans un sens déterminé, il appartient au juge administratif, saisi de conclusions sur le fondement des dispositions précitées, de statuer sur ces conclusions en tenant compte, le cas échéant après une mesure d'instruction, de la situation de droit et de fait existant à la date de sa décision. Si, au vu de cette situation de droit et de fait, il apparaît toujours que l'exécution du jugement ou de l'arrêt implique nécessairement une mesure d'exécution, il incombe au juge de la prescrire à l'autorité compétente.<br/>
<br/>
              2.	En deuxième lieu, aux termes de l'article L. 600-2 du code de l'urbanisme : " Lorsqu'un refus opposé à une demande d'autorisation d'occuper ou d'utiliser le sol ou l'opposition à une déclaration de travaux régies par le présent code a fait l'objet d'une annulation juridictionnelle, la demande d'autorisation ou la déclaration confirmée par l'intéressé ne peut faire l'objet d'un nouveau refus ou être assortie de prescriptions spéciales sur le fondement de dispositions d'urbanisme intervenues postérieurement à la date d'intervention de la décision annulée sous réserve que l'annulation soit devenue définitive et que la confirmation de la demande ou de la déclaration soit effectuée dans les six mois suivant la notification de l'annulation au pétitionnaire. ". Lorsqu'une juridiction, à la suite de l'annulation d'un refus opposé à une demande d'autorisation d'occuper ou d'utiliser le sol, fait droit à des conclusions aux fins d'injonction sur le fondement de l'article L. 911-1 du code de justice administrative, ces conclusions du requérant doivent être regardées comme confirmant sa demande initiale. Par suite, la condition posée par l'article L. 600-2 du code de l'urbanisme imposant que la demande ou la déclaration soit confirmée dans les six mois suivant la notification de l'annulation au pétitionnaire doit être regardée comme remplie lorsque la juridiction enjoint à l'autorité administrative de délivrer l'autorisation d'urbanisme sollicitée.<br/>
<br/>
              3.	Enfin, en troisième lieu, aux termes l'article L. 424-1 du code de l'urbanisme : " L'autorité compétente se prononce par arrêté sur la demande de permis ou, en cas d'opposition ou de prescriptions, sur la déclaration préalable. (...) ". Aux termes de l'article L. 424-3 du même code, dans sa rédaction issue de l'article 108 de la loi du 6 août 2015 pour la croissance, l'activité et l'égalité des chances économiques : " Lorsque la décision rejette la demande ou s'oppose à la déclaration préalable, elle doit être motivée. / Cette motivation doit indiquer l'intégralité des motifs justifiant la décision de rejet ou d'opposition, notamment l'ensemble des absences de conformité des travaux aux dispositions législatives et réglementaires mentionnées à l'article L. 421-6. / Il en est de même lorsqu'elle est assortie de prescriptions, oppose un sursis à statuer ou comporte une dérogation ou une adaptation mineure aux règles d'urbanisme applicables. ". Par ailleurs, aux termes de l'article de l'article L. 600-4-1 du code de l'urbanisme : " Lorsqu'elle annule pour excès de pouvoir un acte intervenu en matière d'urbanisme ou en ordonne la suspension, la juridiction administrative se prononce sur l'ensemble des moyens de la requête qu'elle estime susceptibles de fonder l'annulation ou la suspension, en l'état du dossier ".<br/>
<br/>
              4.	Les dispositions introduites au deuxième alinéa de l'article L. 424-3 du code de l'urbanisme par l'article 108 de la loi du 6 août 2015 visent à imposer à l'autorité compétente de faire connaitre tous les motifs susceptibles de fonder le rejet de la demande d'autorisation d'urbanisme ou de l'opposition à la déclaration préalable. Combinées avec les dispositions de l'article L. 600-4-1 du code de l'urbanisme, elles mettent le juge administratif en mesure de se prononcer sur tous les motifs susceptibles de fonder une telle décision. Il ressort des travaux parlementaires de la loi du 6 août 2015 que ces dispositions ont pour objet de permettre d'accélérer la mise en oeuvre de projets conformes aux règles d'urbanisme applicables en faisant obstacle à ce qu'en cas d'annulation par le juge du refus opposé à une demande d'autorisation d'urbanisme ou de l'opposition à la déclaration préalable, et compte tenu de ce que les dispositions de l'article L. 600-2 du même code cité au point 2 conduisent à appliquer le droit en vigueur à la date de la décision annulée, l'autorité compétente prenne une nouvelle décision de refus ou d'opposition.<br/>
<br/>
              5.	Il résulte de ce qui précède que, lorsque le juge annule un refus d'autorisation ou une opposition à une déclaration après avoir censuré l'ensemble des motifs que l'autorité compétente a énoncés dans sa décision conformément aux prescriptions de l'article L. 424-3 du code de l'urbanisme ainsi que, le cas échéant, les motifs qu'elle a pu invoquer en cours d'instance, il doit, s'il est saisi de conclusions à fin d'injonction, ordonner à l'autorité compétente de délivrer l'autorisation ou de prendre une décision de non-opposition. Il n'en va autrement que s'il résulte de l'instruction soit que les dispositions en vigueur à la date de la décision annulée, qui eu égard aux dispositions de l'article L. 600-2 citées au point 2 demeurent.applicables à la demande, interdisent de l'accueillir pour un motif que l'administration n'a pas relevé, ou que, par suite d'un changement de circonstances, la situation de fait existant à la date du jugement y fait obstacle L'autorisation d'occuper ou utiliser le sol délivrée dans ces conditions peut être contestée par les tiers sans qu'ils puissent se voir opposer les termes du jugement ou de l'arrêt.<br/>
<br/>
              6.	En cas d'annulation, par une nouvelle décision juridictionnelle, du jugement ou de l'arrêt ayant prononcé, dans ces conditions, une injonction de délivrer l'autorisation sollicitée et sous réserve que les motifs de cette décision ne fassent pas par eux-mêmes obstacle à un nouveau refus de cette autorisation, l'autorité compétente peut la retirer dans un délai raisonnable qui ne saurait, eu égard à l'objet et aux caractéristiques des autorisations d'urbanisme, excéder trois mois à compter de la notification à l'administration de la décision juridictionnelle. Elle doit, avant de procéder à ce retrait, inviter le pétitionnaire à présenter ses observations. <br/>
<br/>
              Le présent avis sera notifié au tribunal administratif de Versailles, au préfet des Yvelines, à l'Association des musulmans de Mantes sud et à la commune de Mantes-la Ville et publié au Journal officiel de la République française.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-06-04 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. POUVOIRS DU JUGE. - ANNULATION D'UN REFUS D'AUTORISATION D'URBANISME ASSORTI DE CONCLUSIONS À FIN D'INJONCTION - 1) FACULTÉ POUR LE JUGE D'ENJOINDRE À L'ADMINISTRATION DE DÉLIVRER L'AUTORISATION SOLLICITÉE - A) PRINCIPE - EXISTENCE, LORSQUE TOUS LES MOTIFS SUSCEPTIBLES DE FONDER LE REJET DE LA DEMANDE ÉNONCÉS PAR L'ADMINISTRATION ONT ÉTÉ CENSURÉS - B) EXCEPTIONS - DISPOSITION CRISTALLISÉE PAR L'APPLICATION DE L'ART. L. 600-2 Y FAISANT OBSTACLE POUR UN MOTIF NON RELEVÉ PAR L'ADMINISTRATION OU CHANGEMENT DE CIRCONSTANCES - 2) ANNULATION DE LA DÉCISION JURIDICTIONNELLE ANNULANT LE REFUS ET L'INJONCTION DE DÉLIVRER L'AUTORISATION - FACULTÉ DE RETRAIT DE L'AUTORISATION DÉLIVRÉE EN CONSÉQUENCE DE L'INJONCTION - EXISTENCE - CONDITIONS [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-06-05 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. EFFETS DES ANNULATIONS. - CRISTALLISATION DES DISPOSITIONS APPLICABLES EN CAS D'ANNULATION D'UN REFUS DE PERMIS DE CONSTRUIRE ET DE RENOUVELLEMENT DE LA DEMANDE DANS UN DÉLAI DE SIX MOIS (ART. L. 600-2 DU CODE DE L'URBANISME) - CAS OÙ LE JUGE, APRÈS AVOIR ANNULÉ UN REFUS D'AUTORISATION D'URBANISME, ENJOINT À L'ADMINISTRATION DE DÉLIVRER L'AUTORISATION SOLLICITÉE - EFFET - CONFIRMATION DE LA DEMANDE [RJ1].
</SCT>
<ANA ID="9A"> 68-06-04 1) a) Lorsque le juge annule un refus d'autorisation ou une opposition à une déclaration après avoir censuré l'ensemble des motifs que l'autorité compétente a énoncés dans sa décision conformément aux prescriptions de l'article L. 424-3 du code de l'urbanisme ainsi que, le cas échéant, les motifs qu'elle a pu invoquer en cours d'instance, il doit, s'il est saisi de conclusions à fin d'injonction, ordonner à l'autorité compétente de délivrer l'autorisation ou de prendre une décision de non-opposition.... ,,b) Il n'en va autrement que s'il résulte de l'instruction soit que les dispositions en vigueur à la date de la décision annulée, qui eu égard aux dispositions de l'article L. 600-2 du code de l'urbanisme demeurent applicables à la demande, interdisent de l'accueillir pour un motif que l'administration n'a pas relevé, ou que, par suite d'un changement de circonstances, la situation de fait existant à la date du jugement y fait obstacle. L'autorisation d'occuper ou utiliser le sol délivrée dans ces conditions peut être contestée par les tiers sans qu'ils puissent se voir opposer les termes du jugement ou de l'arrêt.,,,2) En cas d'annulation, par une nouvelle décision juridictionnelle, du jugement ou de l'arrêt ayant prononcé, dans ces conditions, une injonction de délivrer l'autorisation sollicitée et sous réserve que les motifs de cette décision ne fassent pas par eux-mêmes obstacle à un nouveau refus de cette autorisation, l'autorité compétente peut la retirer dans un délai raisonnable qui ne saurait, eu égard à l'objet et aux caractéristiques des autorisations d'urbanisme, excéder trois mois à compter de la notification à l'administration de la décision juridictionnelle. Elle doit, avant de procéder à ce retrait, inviter le pétitionnaire à présenter ses observations.</ANA>
<ANA ID="9B"> 68-06-05 Lorsqu'une juridiction, à la suite de l'annulation d'un refus opposé à une demande d'autorisation d'occuper ou d'utiliser le sol, fait droit à des conclusions aux fins d'injonction sur le fondement de l'article L. 911-1 du code de justice administrative (CJA), ces conclusions du requérant doivent être regardées comme confirmant sa demande initiale. Par suite, la condition posée par l'article L. 600-2 du code de l'urbanisme imposant que la demande ou la déclaration soit confirmée dans les six mois suivant la notification de l'annulation au pétitionnaire doit être regardée comme remplie lorsque la juridiction enjoint à l'autorité administrative de délivrer l'autorisation d'urbanisme sollicitée.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., dans l'hypothèse où le juge enjoint le réexamen de la demande, CE, 23 février 2017,,et SARL Côte d'Opale, n° 395274, à mentionner aux Tables.,,[RJ2] Rappr., s'agissant de la faculté de retirer, après le jugement rendu au principal, un permis provisoire délivré à la suite du réexamen ordonné en conséquence de la suspension d'un refus de permis ordonnée en référé, CE, Section, 7 octobre 2016, Commune de Bordeaux, n° 395211, p. 409.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
