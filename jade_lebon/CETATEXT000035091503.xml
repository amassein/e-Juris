<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035091503</ID>
<ANCIEN_ID>JG_L_2017_06_000000399607</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/09/15/CETATEXT000035091503.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 30/06/2017, 399607</TITRE>
<DATE_DEC>2017-06-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>399607</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Olivier Rousselle</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:399607.20170630</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A...a demandé au tribunal administratif de Nantes d'annuler la décision du 28 janvier 2014 par laquelle le préfet de la Loire-Atlantique a suspendu pour inaptitude physique la validité de son permis de conduire et d'enjoindre au préfet de la Loire-Atlantique de lui restituer son permis ou, subsidiairement, de procéder à un nouvel examen de sa demande de restitution. Par un jugement n° 1401810 du 4 mars 2016, rectifié par une ordonnance n° 1401810 du 31 mars 2016, le tribunal administratif a annulé la décision attaquée et enjoint au préfet de la Loire-Atlantique de restituer le permis de conduire dans un délai de quinze jours à compter de la notification de cette ordonnance.<br/>
<br/>
              Par un pourvoi, enregistré le 9 mai 2016 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement et cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter les demandes de M. A....<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la route ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - la loi n° 2003-495 du 12 juin 2003 ;<br/>
              - le décret n° 2012-886 du 17 juillet 2012 ;<br/>
              - le décret n° 2016-39 du 22 janvier 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Rousselle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de M.A....<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'à la suite d'une infraction pour conduite en état d'ivresse, M. A... a fait l'objet, le 22 juin 2012, d'une décision préfectorale de suspension de son permis de conduire d'une durée de quatre mois, à laquelle s'est substituée, le 2 octobre 2012, une décision judiciaire prononçant la même mesure pour la même période ; qu'il a été soumis le 9 octobre 2012 à un examen clinique mené par la commission médicale primaire afin d'évaluer son aptitude physique à la conduite automobile ; que cette commission a subordonné sa décision finale aux résultats d'examens complémentaires ; que M. A...ne s'étant pas soumis à ces examens, le préfet de la Loire-Atlantique a suspendu la validité de son permis de conduire par une décision du 28 janvier 2014 ; que, par un jugement du 4 mars 2016, rectifié par une ordonnance du 31 mars 2016, le tribunal administratif de Nantes a annulé la décision du 28 janvier 2014 et enjoint au préfet de restituer son permis de conduire à l'intéressé dans un délai de quinze jours à compter de la notification de cette ordonnance ; que le ministre de l'intérieur demande la cassation de ce jugement ainsi rectifié ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 224-14 du code de la route, dans sa rédaction issue de la loi du 12 juin 2003 : " En cas d'annulation du permis de conduire prononcée en application du présent code ou pour les délits prévus par les articles 221-6-1, 222-19-1 et 222-20-1 du code pénal ou en cas de suspension du permis de conduire dont la durée est fixée par décret en Conseil d'Etat, l'intéressé ne peut solliciter un nouveau permis ou la restitution de son permis sans avoir été reconnu apte après un examen ou une analyse médicale, clinique, biologique et psychotechnique effectué à ses frais " ; que la durée de suspension du permis de conduire mentionnée par ces dispositions législatives n'a été fixée que par le décret visé ci-dessus du 22 janvier 2016, qui a modifié l'article R. 224-21 du code de la route afin d'y mentionner, outre l'annulation du permis de conduire, une suspension " d'une durée de six mois ou plus " ;<br/>
<br/>
              3. Considérant, par ailleurs, qu'aux termes de l'article R. 221-14 du même code : " I. - Postérieurement à la délivrance du permis, le préfet peut enjoindre à un conducteur de se soumettre à un contrôle médical : (...) 3° Avant la restitution de son permis, à tout conducteur (...) à l'encontre duquel il a prononcé une mesure restrictive ou suspensive du droit de conduire pour l'une des infractions prévues par les articles L. 234-1 et L. 234-8, afin de déterminer si l'intéressé dispose des aptitudes physiques nécessaires à la conduite du véhicule. (...) / II. - Lorsque le titulaire du permis de conduire néglige ou refuse de se soumettre, dans les délais qui lui sont prescrits, au contrôle médical dans les conditions du présent article, le préfet peut prononcer ou maintenir la suspension du permis de conduire jusqu'à ce qu'un avis médical soit émis par le médecin agréé consultant hors commission médicale, ou par la commission médicale " ;<br/>
<br/>
              4. Considérant, enfin, que les modalités du contrôle médical de l'aptitude à la conduite sont fixées par les articles R. 226-1 et suivants du même code, issus du décret visé ci-dessus du 17 juillet 2012 ; qu'en vertu de l'article R. 226-2, ce contrôle est effectué par un médecin agréé par le préfet ou par une commission médicale primaire, un recours étant ouvert devant une commission médicale d'appel ; que, dans sa rédaction initiale, le quatrième alinéa de cet article disposait que : " Si le contrôle médical de l'aptitude à la conduite intervient à la suite d'une invalidation, annulation ou suspension du permis prononcée en application du présent code, il est complété par un examen psychotechnique réalisé dans les conditions prévues à l'article R. 224-22 " ; que le décret du 22 janvier 2016 a modifié cet alinéa pour y mentionner une suspension du permis de conduire " d'une durée de six mois ou plus " ; qu'aux termes du cinquième alinéa du même article R. 226-2 : " Lors de ce contrôle médical, le médecin agréé ou la commission médicale peut prescrire tout examen complémentaire (...) " ; que, sur le fondement de cette dernière disposition, le médecin agréé ou la commission médicale peut prescrire un examen psychotechnique si cet examen apparaît justifié, alors même que l'intéressé ne se trouve pas dans une situation où il est imposé en vertu des dispositions du quatrième alinéa ;<br/>
<br/>
              5. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. A...se trouvait dans la situation mentionnée au 3° du I de l'article R. 221-14 du code de la route cité au point 3 et a été soumis à ce titre à un contrôle médical ; que la décision litigieuse, prise le 28 janvier 2014, a été motivée par son refus de subir à ses frais un examen psychotechnique demandé par la commission médicale primaire ; que, pour annuler cette mesure, le tribunal administratif s'est fondé sur les dispositions du quatrième alinéa de l'article R. 226-2 du code de la route, dans sa rédaction issue du décret du 22 janvier 2016, alors que ces dispositions n'étaient pas en vigueur à la date de la décision attaquée ; que si M. A...fait valoir en défense que la rédaction antérieure du même alinéa n'avait pu légalement, eu égard aux dispositions de l'article L. 224-14 du code de la route, imposer un examen psychotechnique en cas de suspension du permis de conduire, quelle qu'en fût la durée, ce motif ne saurait, en tout état de cause, être substitué au motif erroné retenu par le juge du fond dès lors qu'il ressort des pièces du dossier soumis aux juges du fond que l'examen psychotechnique avait été prescrit par la commission médicale primaire sur le fondement du cinquième alinéa de l'article R. 226-2 ; qu'il suit de là que le jugement attaqué et l'ordonnance qui l'a rectifié doivent être annulées ;<br/>
<br/>
              6. Considérant que l'Etat n'étant pas la partie perdante dans la présente instance, les dispositions de l'article L.761-1 du code de justice administrative font obstacle à ce que soit mise à sa charge la somme que demande M. A...au titre de ces dispositions et de celles de l'article 37 de la loi du 10 juillet 1991 ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Nantes du 4 mars 2016 et l'ordonnance du 31 mars 2016 le rectifiant sont annulés.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Nantes.<br/>
Article 3 : Les conclusions de M. A...présentées au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au ministre d'Etat, ministre de l'intérieur et à M. B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">49-04-01-04 POLICE. POLICE GÉNÉRALE. CIRCULATION ET STATIONNEMENT. PERMIS DE CONDUIRE. - CONTRÔLE MÉDICAL DE L'APTITUDE À LA CONDUITE - POSSIBILITÉ POUR LE MÉDECIN AGRÉÉ OU LA COMMISSION MÉDICALE DE PRESCRIRE UN EXAMEN PSYCHOTECHNIQUE SUR LE FONDEMENT DU CINQUIÈME ALINÉA DE L'ART. R. 226-1 DU CODE DE LA ROUTE - EXISTENCE.
</SCT>
<ANA ID="9A"> 49-04-01-04 Le médecin agréé ou la commission médicale peut prescrire un examen psychotechnique sur le fondement du cinquième alinéa de l'art. R. 226-1 du code de la route si cet examen apparaît justifié, alors même que l'intéressé ne se trouve pas dans une situation où un tel examen est imposé en vertu des dispositions du quatrième alinéa de cet article.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
