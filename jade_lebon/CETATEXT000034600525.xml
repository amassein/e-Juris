<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034600525</ID>
<ANCIEN_ID>JG_L_2017_05_000000391925</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/60/05/CETATEXT000034600525.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Section, 05/05/2017, 391925, Publié au recueil Lebon</TITRE>
<DATE_DEC>2017-05-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>391925</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Section</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>Mme Marie Sirinelli</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean Lessi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESEC:2017:391925.20170505</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Marseille d'annuler pour excès de pouvoir l'arrêté du 6 janvier 2014 par lequel le maire de la commune d'Istres (Bouches-du-Rhône) a retiré l'arrêté du 9 août 2013 lui délivrant un permis de construire modificatif. Par un jugement n°s 1308200, 1401265 du 30 avril 2015, le tribunal administratif de Marseille, également saisi d'un déféré du préfet des Bouches-du-Rhône tendant à l'annulation de l'arrêté du 9 août 2013, a annulé l'arrêté du 9 août 2013 et jugé qu'il n'y avait plus lieu de statuer sur la demande de M.B....<br/>
<br/>
              Par une ordonnance n° 15MA02656 du 17 juillet 2015, enregistrée le 21 juillet 2015 au secrétariat du contentieux du Conseil d'Etat, le président de la cour administrative d'appel de Marseille a transmis au Conseil d'Etat, en application des articles R. 343-3 et R. 351-2 du code de justice administrative, le pourvoi, enregistré le 30 juin 2015 au greffe de cette cour, présenté par M.B.... <br/>
<br/>
              Par ce pourvoi, M. B...demande l'annulation du jugement du tribunal administratif de Marseille du 30 avril 2015. Par deux nouveaux mémoires, enregistrés les 7 septembre et 8 décembre 2015 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat, à titre principal, de renvoyer l'entier litige à la cour administrative d'appel de Marseille ou, à titre subsidiaire, de renvoyer à cette cour les conclusions tendant à l'annulation de l'article 2 du jugement attaqué, d'annuler les articles 1er et 3 de ce jugement et de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'urbanisme ;<br/>
              - le décret n° 2013-392 du 10 mai 2013 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Sirinelli, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Jean Lessi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de M. B...;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que le préfet des Bouches-du-Rhône a déféré au tribunal administratif de Marseille le permis de construire modificatif délivré à M. B...par le maire de la commune d'Istres, le 9 août 2013, afin de régulariser des travaux non conformes à un permis de construire précédemment délivré. Le maire de la commune ayant toutefois, par un arrêté du 6 janvier 2014, retiré son arrêté du 9 août 2013, l'intéressé a également saisi le tribunal administratif de Marseille d'une demande tendant à l'annulation de ce retrait. Ayant joint les deux requêtes, le tribunal administratif a, par un jugement du 30 avril 2015, fait droit au déféré préfectoral en prononçant l'annulation de l'arrêté du 9 août 2013 délivrant le permis de construire modificatif et jugé qu'il n'y avait plus lieu, en conséquence, de statuer sur les conclusions de M. B...tendant à l'annulation de l'arrêté du 6 janvier 2014 ayant retiré ce permis.<br/>
<br/>
              Sur la compétence du Conseil d'Etat :<br/>
<br/>
              2. Il résulte des dispositions de l'article R. 811-1-1 du code de justice administrative, issu du décret du 1er octobre 2013 relatif au contentieux de l'urbanisme, applicable à la commune d'Istres en vertu du décret du 10 mai 2013 relatif au champ d'application de la taxe annuelle sur les logements vacants instituée par l'article 232 du code général des impôts, que les tribunaux administratifs statuent en premier et dernier ressort sur les recours, introduits entre le 1er décembre 2013 et le 1er décembre 2018, dirigés contre " les permis de construire ou de démolir un bâtiment à usage principal d'habitation ou contre les permis d'aménager un lotissement lorsque le bâtiment ou le lotissement est implanté en tout ou partie sur le territoire d'une des communes mentionnées à l'article 232 du code général des impôts et son décret d'application ". <br/>
<br/>
              3. Les dispositions précitées de l'article R. 811-1-1 du code de justice administrative, qui ont pour objectif, dans les zones où la tension entre l'offre et la demande de logements est particulièrement vive, de réduire le délai de traitement des recours pouvant retarder la réalisation d'opérations de construction de logements ayant bénéficié d'un droit à construire, doivent être regardées comme concernant non seulement les recours dirigés contre des autorisations de construire, de démolir ou d'aménager, mais également, lorsque ces autorisations ont été accordées puis retirées, les recours dirigés contre ces retraits. <br/>
<br/>
              4. Il en résulte que le jugement attaqué a été rendu en dernier ressort aussi bien en tant qu'il a statué sur le déféré préfectoral tendant à l'annulation du permis de construire délivré le 9 août 2013 à M. B...qu'en tant qu'il a statué sur la demande de ce dernier tendant à l'annulation de l'arrêté du 6 janvier 2014 retirant ce permis. Par suite, la requête de M. B... tendant à l'annulation de ce jugement a le caractère d'un pourvoi en cassation et relève ainsi de la compétence du Conseil d'Etat.<br/>
<br/>
              Sur le bien-fondé du jugement attaqué :<br/>
<br/>
              5. Le juge de l'excès de pouvoir ne peut, en principe, déduire d'une décision juridictionnelle rendue par lui-même ou par une autre juridiction qu'il n'y a plus lieu de statuer sur des conclusions à fin d'annulation dont il est saisi, tant que cette décision n'est pas devenue irrévocable. Il en va toutefois différemment lorsque, faisant usage de la faculté dont il dispose dans l'intérêt d'une bonne administration de la justice, il joint les requêtes pour statuer par une même décision, en tirant les conséquences nécessaires de ses propres énonciations. Dans cette hypothèse, toutes les parties concernées seront, en cas d'exercice d'une voie de recours, mises en cause et celle à laquelle un non-lieu a été opposé, mise à même de former, si elle le souhaite, un recours incident contre cette partie du dispositif du jugement. <br/>
<br/>
              6. A ce titre, lorsque le juge est parallèlement saisi de conclusions tendant, d'une part, à l'annulation d'une décision et, d'autre part, à celle de son retrait et qu'il statue par une même décision, il lui appartient de se prononcer sur les conclusions dirigées contre le retrait puis, sauf si, par l'effet de l'annulation qu'il prononce, la décision retirée est rétablie dans l'ordonnancement juridique, de constater qu'il n'y a plus lieu pour lui de statuer sur les conclusions dirigées contre cette dernière. <br/>
<br/>
              7. En l'espèce, en commençant par statuer sur les conclusions à fin d'annulation du permis de construire du 9 août 2013, alors qu'à la date de son jugement, il avait été retiré, avant d'en déduire que, par l'effet de l'annulation qu'il prononçait, il n'y avait plus lieu de statuer sur les conclusions tendant à l'annulation du retrait de ce permis, le tribunal a commis une erreur de droit. Il en résulte, sans qu'il soit besoin d'examiner les autres moyens soulevés par M.B..., que le jugement du tribunal administratif de Marseille du 30 avril 2015 doit être annulé. <br/>
<br/>
              Sur les conclusions présentées par M. B...au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              7. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme que M. B...demande au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Marseille du 30 avril 2015 est annulé.<br/>
Article 2 : Les affaires sont renvoyées au tribunal administratif de Marseille.<br/>
Article 3 : Les conclusions de M. B...présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à M. A...B..., à la commune d'Istres et à la ministre du logement et de l'habitat durable.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-05-05 PROCÉDURE. INCIDENTS. NON-LIEU. - NON-LIEU À RAISON DE L'INTERVENTION D'UNE DÉCISION JURIDICTIONNELLE NON IRRÉVOCABLE - 1) PRINCIPE - ABSENCE [RJ1] - 2) EXCEPTION - CAS OÙ LE JUGE A JOINT DEUX REQUÊTES ET TIRE LES CONSÉQUENCES NÉCESSAIRES DE SES PROPRES ÉNONCIATIONS - A) EXISTENCE [RJ2] - B) CONSÉQUENCES - MISE EN CAUSE DE TOUTES LES PARTIES CONCERNÉES EN CAS D'EXERCICE D'UNE VOIE DE RECOURS ET FACULTÉ POUR LA PARTIE À QUI LE NON-LIEU A ÉTÉ OPPOSÉ D'EXERCER UN RECOURS INCIDENT [RJ3] - C) APPLICATION - CAS OÙ LE JUGE EST SAISI DE CONCLUSIONS TENDANT À L'ANNULATION D'UNE DÉCISION ET DE SON RETRAIT.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-01-01 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. JONCTION DES POURVOIS. - 1) PERTE D'OBJET DE CERTAINES CONCLUSIONS DÉCOULANT DE LA RÉPONSE AUX AUTRES CONCLUSIONS - EXISTENCE, ALORS MÊME QUE LE JUGE NE STATUE PAS PAR UNE DÉCISION IRRÉVOCABLE [RJ2] - 2) CONSÉQUENCES - MISE EN CAUSE DE TOUTES LES PARTIES CONCERNÉES EN CAS D'EXERCICE D'UNE VOIE DE RECOURS ET FACULTÉ POUR LA PARTIE À QUI LE NON-LIEU A ÉTÉ OPPOSÉ D'EXERCER UN RECOURS INCIDENT [RJ3] - 3) CAS OÙ LE JUGE EST SAISI DE CONCLUSIONS TENDANT À L'ANNULATION D'UNE DÉCISION ET DE SON RETRAIT - OBLIGATION D'EXAMINER D'ABORD LES CONCLUSIONS DIRIGÉES CONTRE LE RETRAIT - EXISTENCE - PERTE D'OBJET, LE CAS ÉCHÉANT, DES CONCLUSIONS DIRIGÉES CONTRE LA DÉCISION - EXISTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-07-01-07 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. DEVOIRS DU JUGE. - CAS OÙ LE JUGE DE L'EXCÈS DE POUVOIR STATUE PAR UNE MÊME DÉCISION SUR DES CONCLUSIONS DIRIGÉES CONTRE UNE DÉCISION ET DES CONCLUSIONS DIRIGÉES CONTRE SON RETRAIT - OBLIGATION D'EXAMINER D'ABORD LES CONCLUSIONS DIRIGÉES CONTRE LE RETRAIT - EXISTENCE.
</SCT>
<ANA ID="9A"> 54-05-05 1) Le juge de l'excès de pouvoir ne peut, en principe, déduire d'une décision juridictionnelle rendue par lui-même ou par une autre juridiction qu'il n'y a plus lieu de statuer sur des conclusions à fin d'annulation dont il est saisi, tant que cette décision n'est pas devenue irrévocable.,,,2) a) Il en va toutefois différemment lorsque, faisant usage de la faculté dont il dispose dans l'intérêt d'une bonne administration de la justice, il joint les requêtes pour statuer par une même décision, en tirant les conséquences nécessaires de ses propres énonciations.,,,b) Dans cette hypothèse, toutes les parties concernées seront, en cas d'exercice d'une voie de recours, mises en cause et celle à laquelle un non-lieu a été opposé, mise à même de former, si elle le souhaite, un recours incident contre cette partie du dispositif du jugement.... ,,c) A ce titre, lorsque le juge est parallèlement saisi de conclusions tendant, d'une part, à l'annulation d'une décision et, d'autre part, à celle de son retrait et qu'il statue par une même décision, il lui appartient de se prononcer sur les conclusions dirigées contre le retrait puis, sauf si, par l'effet de l'annulation qu'il prononce, la décision retirée est rétablie dans l'ordonnancement juridique, de constater qu'il n'y a plus lieu pour lui de statuer sur les conclusions dirigées contre cette dernière.</ANA>
<ANA ID="9B"> 54-07-01-01 1) Le juge de l'excès de pouvoir ne peut, en principe, déduire d'une décision juridictionnelle rendue par lui-même ou par une autre juridiction qu'il n'y a plus lieu de statuer sur des conclusions à fin d'annulation dont il est saisi, tant que cette décision n'est pas devenue irrévocable. Il en va toutefois différemment lorsque, faisant usage de la faculté dont il dispose dans l'intérêt d'une bonne administration de la justice, il joint les requêtes pour statuer par une même décision, en tirant les conséquences nécessaires de ses propres énonciations.,,,2) Dans cette hypothèse, toutes les parties concernées seront, en cas d'exercice d'une voie de recours, mises en cause et celle à laquelle un non-lieu a été opposé, mise à même de former, si elle le souhaite, un recours incident contre cette partie du dispositif du jugement.... ,,3) A ce titre, lorsque le juge est parallèlement saisi de conclusions tendant, d'une part, à l'annulation d'une décision et, d'autre part, à celle de son retrait et qu'il statue par une même décision, il lui appartient de se prononcer sur les conclusions dirigées contre le retrait puis, sauf si, par l'effet de l'annulation qu'il prononce, la décision retirée est rétablie dans l'ordonnancement juridique, de constater qu'il n'y a plus lieu pour lui de statuer sur les conclusions dirigées contre cette dernière.</ANA>
<ANA ID="9C"> 54-07-01-07 Lorsque le juge est parallèlement saisi de conclusions tendant, d'une part, à l'annulation d'une décision et, d'autre part, à celle de son retrait et qu'il statue par une même décision, il lui appartient de se prononcer sur les conclusions dirigées contre le retrait puis, sauf si, par l'effet de l'annulation qu'il prononce, la décision retirée est rétablie dans l'ordonnancement juridique, de constater qu'il n'y a plus lieu pour lui de statuer sur les conclusions dirigées contre cette dernière.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. décision du même jour, Société Sérénis, n° 393268, inédite au Recueil ; CE, 6 juillet 1988, Commune de Saumos, n°s 67156 71576, T. p. 962. Rappr., s'agissant du retrait de la décision, CE, 19 avril 2000,,, n° 207469, p. 157.,,[RJ2] Comp., sur le principe de neutralité de la jonction, CE, 28 janvier 1987, Comité de défense des espaces verts, n° 39145, inédite au Recueil ; CE, 27 juillet 2005,,, n° 228554, T. pp. 1042-1058-1061.,,[RJ3] Rappr. CE, 3 juillet 2013, Association des paralysés de France, n° 348099, T. pp. 419-802-865-867.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
