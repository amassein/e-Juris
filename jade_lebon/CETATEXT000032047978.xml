<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032047978</ID>
<ANCIEN_ID>JG_L_2016_02_000000394344</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/04/79/CETATEXT000032047978.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème / 9ème SSR, 12/02/2016, 394344</TITRE>
<DATE_DEC>2016-02-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>394344</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème / 9ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Isabelle Lemesle</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:394344.20160212</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire en réplique et un nouveau mémoire, enregistrés les 2 novembre, 17 décembre 2015 et 24 janvier 2016 au secrétariat du contentieux du Conseil d'Etat, la confédération des armateurs de Polynésie française demande au Conseil d'Etat :<br/>
<br/>
              1°) de déclarer la " loi du pays " n° 2015-5 LP/APF adoptée le 22 septembre 2015 relative à l'organisation du transport interinsulaire maritime et aérien, non conforme au bloc de légalité défini au III de l'article 176 de la loi organique du 27 février 2004 portant statut d'autonomie de la Polynésie française ;<br/>
<br/>
              2°) de mettre à la charge de la Polynésie française la somme de 4 500 euros à lui verser au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 74 ;<br/>
              - la loi organique n° 2004-192 du 27 février 2004 ;<br/>
              - la délibération n° 77-46 AT du 15 mars 1977 ;<br/>
              - la délibération n° 77-47 AT du 15 mars 1977 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Isabelle Lemesle, conseiller d'Etat,  <br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 27 janvier 2016, présentée par la confédération des armateurs de Polynésie française ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que la confédération des armateurs de Polynésie française (CAP) défère au Conseil d'Etat, sur le fondement des dispositions de l'article 176 de la loi organique du 27 février 2004 portant statut de la Polynésie française, la " loi du pays " adoptée le 22 septembre 2015 par l'assemblée de la Polynésie française, en application des dispositions de l'article 140 de la même loi organique, et relative à l'organisation du transport interinsulaire maritime et aérien ;<br/>
<br/>
              Sur la légalité externe :<br/>
<br/>
              En ce qui concerne la compétence :<br/>
<br/>
              2. Considérant, en premier lieu, que l'article 13 de la loi organique du 27 février 2004 donne compétence à la Polynésie française dans toutes les matières qui ne sont pas dévolues à l'Etat par l'article 14 de la même loi ou aux communes en vertu des lois et règlements applicables en Polynésie française ; qu'en matière maritime, le 9e de cet article 14 ne donne compétence à l'Etat qu'en matière de police et de sécurité de la circulation ; qu'en outre, le 4° de l'article 43 de la même loi organique donne compétence aux communes en matière de transports communaux ; qu'il résulte de ces dispositions que les autorités de la Polynésie française sont compétentes pour réglementer le transport public interinsulaire des personnes et des biens ; que la nouvelle réglementation en cause, portant atteinte, par nature, à la liberté d'entreprendre, dès lors qu'elle repose sur l'attribution de licences d'exploitation à durée déterminée, relève de la compétence de la loi ; que, par suite, elle pouvait faire l'objet d'un acte de l'assemblée de la Polynésie française dénommé " loi du pays " en application de l'article 140 de la loi organique du 27 février 2004 qui réserve cette dénomination aux actes qui relèvent du domaine réservé à la loi par l'article 34 de la Constitution et ressortissent de la compétence de la Polynésie française en application de l'article 13 ou qui sont pris au titre de la participation de la Polynésie française à l'exercice des compétences de l'Etat ; <br/>
<br/>
              3. Considérant, en deuxième lieu, que contrairement à ce que soutient la requérante, le législateur du pays avait compétence pour prévoir la caducité des licences d'exploitation délivrées antérieurement à son entrée en vigueur en cas d'incompatibilité avec le schéma directeur des déplacements durables interinsulaires ;<br/>
<br/>
              4. Considérant enfin que l'assemblée de la Polynésie française n'a pas méconnu l'étendue de sa compétence en renvoyant, en ses articles LP 5 et LP 10, à une délibération de cette même assemblée la définition, pour l'ensemble des opérateurs et sur l'ensemble du territoire de la Polynésie française, des obligations de service public dont elle précise l'objet ainsi que la détermination des conditions d'octroi et de retrait des licences d'exploitation, c'est-à-dire de la seule procédure administrative, dès lors que l'objet et les critères de l'attribution de la licence résultent directement de la " loi du pays " ;<br/>
<br/>
              En ce qui concerne la procédure :<br/>
<br/>
              5. Considérant qu'il ressort des pièces du dossier que le conseil économique, social et culturel, consulté sur les projets et propositions d'actes prévus à l'article 140 dénommés " lois du pays " à caractère économique ou social, en vertu du II de l'article 151 de la loi organique du 27 février 2004, a émis un avis favorable le 15 janvier sur le projet de " loi du pays " relatif à l'organisation du transport interinsulaire maritime et aérien transmis par le président de la Polynésie française ; que si la CAP soutient que ce conseil n'a pas été saisi du texte finalement transmis à l'assemblée de la Polynésie française, cette circonstance est sans incidence sur le caractère régulier de la consultation, dès lors qu'il ne ressort pas des pièces du dossier que le projet de texte soumis à l'assemblée présentait des questions nouvelles qui auraient nécessité que le conseil économique, social et culturel soit de nouveau consulté ; que pour le même motif le moyen tiré de ce que la consultation du haut conseil de la Polynésie française aurait été irrégulière ne peut, en tout état de cause, qu'être écarté ;<br/>
<br/>
              6. Considérant que si la délibération n° 77-46 AT du 15 mars 1977 prévoit à son article 2 que le comité consultatif de la navigation maritime interinsulaire est compétent pour donner son avis sur " toutes questions relatives aux liaisons maritimes interinsulaires ", elle ne rend obligatoire sa consultation que pour " tous projets concernant la construction, l'achat, (...) du matériel destiné à la navigation commerciale interinsulaire " ; que, par suite et en tout état de cause, sa consultation sur le projet de la " loi du pays " attaquée n'était pas requise ; qu'au surplus, la CAP produit à l'appui de sa requête le compte rendu de la réunion du 18 août 2015 au cours de laquelle ce comité consultatif a précisément émis un avis favorable sur ce projet de " loi du pays " ; <br/>
<br/>
              7. Considérant que si la CAP soutient que les représentants à l'assemblée de la Polynésie française n'auraient pas reçu le rapport déposé le 13 août 2015 par leurs collègues, M. B... et MmeA..., dans le délai de douze jours précédant la séance au cours de laquelle le projet de " loi du pays " a été examiné, en application de l'article 130 de la loi organique du 27 février 2004, elle n'apporte au soutien de ses allégations aucun élément permettant d'en apprécier le bien fondé ; que le contenu de ce rapport écrit, qui n'est pas tel qu'il doive être regardé comme inexistant, est sans incidence sur la régularité de la procédure d'adoption de la " loi du pays " ; que la circonstance que le texte adopté diffère de celui proposé par les rapporteurs résulte de l'exercice par le gouvernement et par les représentant de l'assemblée de la Polynésie française de leur droit d'amendement ;<br/>
<br/>
              8. Considérant que le caractère tardif allégué de la publication au Journal Officiel de la Polynésie française du compte rendu intégral de la séance du 22 septembre 2015 au cours de laquelle a été adoptée la délibération attaquée, au-delà du délai prévu par l'article 12 du règlement intérieur de l'assemblée de la Polynésie française, n'entache pas celle-ci d'illégalité ;<br/>
<br/>
              9. Considérant que la CAP n'apporte au soutien de ses allégations selon lesquelles le projet de " loi du pays " en litige n'aurait pas été transmis au président de la Polynésie française ni au haut-commissaire dans le délai prescrit par l'article 130 de la loi organique du 27 février 2004 aucun élément permettant d'en apprécier le bien fondé ; qu'en tout état de cause, à la supposer établie, la méconnaissance de ce délai n'est pas susceptible d'entacher d'illégalité la " loi du pays " adoptée ;<br/>
<br/>
              Sur la légalité interne :<br/>
<br/>
              En ce qui concerne l'égalité devant les charges publiques et le droit de propriété :<br/>
<br/>
              10. Considérant que l'article LP 12 de la " loi du pays " attaquée prévoit l'absence d'indemnisation des titulaires de licences d'exploitation délivrées avant l'entrée en vigueur de cette " loi du pays " et déclarées caduques pour incompatibilité avec le schéma directeur des déplacements durables interinsulaires ; que d'une part, en adoptant ces dispositions, l'assemblée de la Polynésie française n'a pas entendu et ne pouvait d'ailleurs légalement écarter toute indemnisation en cas de préjudice  grave et spécial né du prononcé par l'administration de la caducité d'une licence d'exploitation ; que, dans ces conditions, elle n'a pas méconnu le principe d'égalité devant les charges publiques résultant de l'article 13 de la Déclaration des droits et l'homme et du citoyen ; que, d'autre part, les licences d'exploitation délivrées avant l'intervention de la " loi du pays " contestée n'étant pas des biens au sens de l'article 17 de la Déclaration des droits de l'homme et du citoyen, la requérante ne peut invoquer une remise en cause de son droit de propriété, du fait de l'intervention de cette loi,  contraire à ces dispositions ; <br/>
<br/>
              En ce qui concerne la liberté d'entreprendre et la liberté professionnelle :<br/>
<br/>
              11. Considérant que, d'une part, l'objet de la " loi du pays " en litige, adoptée après plusieurs tentatives infructueuses de mise en place d'une délégation de service public pour le transport aérien, est de mettre en place un cadre juridique adapté au caractère intermodal du transport interinsulaire des personnes et marchandises en Polynésie française afin d'assurer la continuité territoriale, compte tenu des contraintes liées à l'enclavement et à l'éloignement, dans des conditions raisonnables de coût pour les collectivités, tout en offrant des services suffisants en terme de régularité, de fréquence, de qualité, de sécurité et de tarif ; qu'un tel but constitue un motif d'intérêt général ; que, d'autre part, les licences d'exploitation ne confèrent pas de droit exclusif à leurs titulaires ; que leur durée, auparavant indéterminée, est désormais limitée et fixée en fonction notamment de la durée normale d'amortissement du matériel de transport, des conditions de fonctionnement du service de transport assuré par l'exploitant et, le cas échéant, des obligations de service public y afférentes ; que les restrictions ainsi apportées à la liberté d'entreprendre ne sont pas, en l'espèce, disproportionnées par rapport à l'objectif poursuivi ; que par suite le moyen tiré de l'atteinte illégale portée à la liberté d'entreprendre, garantie par l'article 4 de la Déclaration des droits de l'homme et du citoyen, doit être écarté ; <br/>
<br/>
              12. Considérant que ces dispositions ne méconnaissent pas la liberté professionnelle des armateurs qui peuvent postuler à l'attribution de licences et exercer leur activité privée hors du champ d'application de la " loi du pays " attaquée ;<br/>
<br/>
              En ce qui concerne le régime des sanctions :<br/>
<br/>
              13. Considérant que l'article LP 11 de la " loi du pays " attaquée maintient en vigueur les sanctions administratives prévues par l'article 7 de la délibération n° 77-47 AT du 15 mars 1977, à savoir l'avertissement, la réduction ou la suppression des subventions et le retrait temporaire ou définitif de la licence, dans l'hypothèse où l'exploitant titulaire d'une licence d'exploitation ne respecterait pas son cahier des charges ; qu'il prévoit en plus de nouvelles sanctions, consistant, d'une part, en une amende financière plafonnée à 3 % du chiffre d'affaires annuel hors taxes, en fonction du nombre de touchées non effectuées, et d'autre part, en une amende plafonnée à 10 millions de francs CFP en cas d'exploitation sans licence ; que ces dispositions, qui sont claires, ne méconnaissent pas l'objectif d'intelligibilité de la loi ;<br/>
<br/>
              14. Considérant que les dispositions transitoires de la " loi du pays " contestée prolongent de 24 mois la validité des licences d'exploitation attribuées avant son entrée en vigueur et permettent à leurs titulaires de se mettre en conformité avec la norme nouvelle ; que les sanctions nouvelles prévues par l'article LP 11 ne s'appliquent qu'aux licences d'exploitation délivrées sur le fondement des dispositions de la " loi du pays " attaquée et non à celles antérieurement délivrées et maintenues en vigueur à titre transitoire ; que la requérante ne peut soutenir, dans ces conditions, que la " loi du pays " contestée méconnaîtrait le principe de sécurité juridique ;<br/>
<br/>
              15. Considérant que les sanctions prévues par la délibération du 15 mars 1977, à savoir l'avertissement, la réduction ou la suppression des subventions et le retrait temporaire ou définitif de la licence, s'appliquent également aux licences délivrées sur le fondement de la " loi du pays " attaquée lorsque leurs titulaires méconnaissent les obligations de service public qui leur incombent ; que l'adjonction aux sanctions déjà existantes, qui ne sont pas excessives, de deux sanctions nouvelles par la " loi du pays " attaquée, consistant, ainsi qu'il a été dit au point 13, d'une part, en une amende financière plafonnée à 3 % du chiffre d'affaires annuel hors taxes, en fonction du nombre de touchées non effectuées, et d'autre part, en une amende plafonnée à 10 millions de francs CFP en cas d'exploitation sans licence, qui s'appliquent à des manquements nouvellement définis, ne méconnaît ni le principe de nécessité des peines, ni le principe de légalité des délits ;<br/>
<br/>
              16. Considérant qu'il résulte de tout ce qui précède, compte tenu de ce que le choix de la réorganisation du service public de transports interinsulaire maritime et aérien, notamment pour les motifs exposés au point 11 de la présente décision, n'est pas entaché d'une erreur manifeste d'appréciation, que les conclusions de la confédération des armateurs de Polynésie française tendant à la déclaration d'illégalité de la " loi du pays " contestée ne peuvent qu'être rejetées, sans qu'il soit besoin de statuer sur la fin de non-recevoir opposée par le président de la Polynésie française, ainsi que, par voie de conséquence, ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'il n'y a pas lieu dans les circonstances de l'espèce, de mettre à la charge de la confédération des armateurs de Polynésie française la somme de 2 000 euros que la Polynésie française demande sur le fondement des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la confédération des armateurs de Polynésie française est rejetée.<br/>
Article 2 : Les conclusions de la Polynésie française tendant à ce qu'une somme soit mise à la charge de la confédération des armateurs de Polynésie française au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à la confédération des armateurs de Polynésie française, au président de la Polynésie française et au président de l'assemblée de la Polynésie française.<br/>
Copie en sera adressée à la ministre des outre-mer et au haut-commissaire de la République en Polynésie française.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-005 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. CONSTITUTION ET PRINCIPES DE VALEUR CONSTITUTIONNELLE. - LIBERTÉ D'ENTREPRENDRE - MÉCONNAISSANCE - ABSENCE - LOI DU PAYS DE POLYNÉSIE FRANÇAISE RELATIVE À L'ORGANISATION DU TRANSPORT INTERINSULAIRE MARITIME ET AÉRIEN, PRÉVOYANT L'ATTRIBUTION DE LICENCES D'EXPLOITATION.
</SCT>
<ANA ID="9A"> 01-04-005 Loi du pays de Polynésie française dont l'objet est de mettre en place un cadre juridique adapté au caractère intermodal du transport interinsulaire des personnes et marchandises en Polynésie française afin d'assurer la continuité territoriale.... ,,D'une part, un tel but constitue un motif d'intérêt général. D'autre part, les licences d'exploitation ne confèrent pas de droit exclusif à leurs titulaires. Leur durée, auparavant indéterminée, est désormais limitée et fixée en fonction notamment de la durée normale d'amortissement du matériel de transport, des conditions de fonctionnement du service de transport assuré par l'exploitant et, le cas échéant, des obligations de service public y afférentes. Les restrictions ainsi apportées à la liberté d'entreprendre ne sont pas, en l'espèce, disproportionnées par rapport à l'objectif poursuivi. La loi du pays en cause ne méconnaît donc pas la liberté d'entreprendre, garantie par l'article 4 de la Déclaration des droits de l'homme et du citoyen.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
