<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038179952</ID>
<ANCIEN_ID>JG_L_2019_02_000000418950</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/17/99/CETATEXT000038179952.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 27/02/2019, 418950, Publié au recueil Lebon</TITRE>
<DATE_DEC>2019-02-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418950</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR>M. Jean-Yves Ollier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:418950.20190227</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de La Réunion, d'une part, d'annuler le titre de pension n° B16019679L du 21 mars 2016 en tant qu'il prévoit la liquidation de sa pension sur la base de l'indice majoré 517 et non 562,  d'autre part, d'enjoindre au ministre de l'action et des comptes publics de lui octroyer une pension de retraite sur la base de l'indice majoré 562 à compter du 1er mai 2016 et, à défaut, de réexaminer sa situation sous astreinte de 200 euros par jour de retard. <br/>
              Par un jugement n° 1600521 du 15 janvier 2018, le tribunal administratif de La Réunion a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 12 mars et 12 juin 2018 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des pensions civiles et militaires de retraite ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Yves Ollier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de M.A....<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article R. 611-7 du code de justice administrative : " Lorsque la décision lui paraît susceptible d'être fondée sur un moyen relevé d'office, le président de la formation de jugement ou, au Conseil d'Etat, la chambre chargée de l'instruction en informe les parties avant la séance de jugement et fixe le délai dans lequel elles peuvent, sans qu'y fasse obstacle la clôture éventuelle de l'instruction, présenter leurs observations sur le moyen communiqué ".<br/>
<br/>
              2. M. A...a demandé au tribunal administratif de La Réunion d'annuler son titre de pension du 21 mars 2016 en tant qu'il prévoit la liquidation de sa pension sur la base de l'indice majoré 517, correspondant au quatorzième échelon du grade de contrôleur de France Télécom et non de l'indice majoré 562, correspondant au dixième échelon du grade de contrôleur divisionnaireC.... Le tribunal a, d'office, relevé l'irrecevabilité du moyen tiré de ce que la décision de liquidation de sa pension serait illégale en conséquence de l'illégalité de la décision refusant de le promouvoir au grade de contrôleur divisionnaire, en se fondant sur la circonstance que cette première décision était devenue définitive faute d'avoir fait l'objet d'un recours contentieux dans un délai raisonnable. En ne communiquant pas à M. A...son intention de soulever cette irrecevabilité, le tribunal administratif de La Réunion a méconnu les dispositions de l'article R. 611-7 du code de justice administrative. Son jugement doit dès lors être annulé, sans qu'il soit besoin d'examiner les autres moyens du pourvoi.<br/>
<br/>
              3. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              4. L'illégalité d'un acte administratif, qu'il soit ou non réglementaire, ne peut être utilement invoquée par voie d'exception à l'appui de conclusions dirigées contre une décision administrative ultérieure que si cette dernière décision a été prise pour l'application du premier acte ou s'il en constitue la base légale. S'agissant d'un acte non réglementaire, l'exception n'est recevable que si l'acte n'est pas devenu définitif à la date à laquelle elle est invoquée, sauf dans le cas où l'acte et la décision ultérieure constituant les éléments d'une même opération complexe, l'illégalité dont l'acte serait entaché peut être invoquée en dépit du caractère définitif de cet acte.<br/>
<br/>
              5. Aux termes de l'article R. 421-1 du code de justice administrative, dans sa rédaction alors applicable : " Sauf en matière de travaux publics, la juridiction ne peut être saisie que par voie de recours formé contre une décision, et ce, dans les deux mois à partir de la notification ou de la publication de la décision attaquée ". Il résulte des dispositions de l'article R. 421-5 du même code que ce délai n'est toutefois opposable qu'à la condition d'avoir été mentionné, ainsi que les voies de recours, dans la notification de la décision.<br/>
<br/>
              6. Le principe de sécurité juridique, qui implique que ne puissent être remises en cause sans condition de délai des situations consolidées par l'effet du temps, fait obstacle à ce que puisse être contestée indéfiniment une décision administrative individuelle qui a été notifiée à son destinataire, ou dont il est établi, à défaut d'une telle notification, que celui-ci a eu connaissance. En une telle hypothèse, si le non-respect de l'obligation d'informer l'intéressé sur les voies et les délais de recours, ou l'absence de preuve qu'une telle information a bien été fournie, ne permet pas que lui soient opposés les délais de recours fixés par le code de justice administrative, le destinataire de la décision ne peut exercer de recours juridictionnel au-delà d'un délai raisonnable. En règle générale et sauf circonstances particulières dont se prévaudrait le requérant, ce délai ne saurait, sous réserve de l'exercice de recours administratifs pour lesquels les textes prévoient des délais particuliers, excéder un an à compter de la date à laquelle une décision expresse lui a été notifiée ou de la date à laquelle il est établi qu'il en a eu connaissance.<br/>
<br/>
              7. Il résulte de l'instruction que M. A...a demandé à son employeur, le 11 juillet 2012, sa promotion dans le corps des contrôleurs divisionnaires de France Télécom. Cette demande a été rejetée par une lettre du 17 octobre 2012 de la directrice des ressources humaines de la direction Orange Réunion Mayotte. Ce refus a été confirmé en dernier lieu par un courriel du 24 décembre 2013, dont la copie lui a été adressée le 6 janvier 2014. Si le délai de deux mois fixé par les dispositions précitées de l'article R. 421-1 du code de justice administrative n'était pas opposable à M. A...en ce qui concerne cette décision, en l'absence d'indications sur les voies et les délais de recours, il résulte de l'instruction que l'intéressé, qui ne fait état d'aucune circonstance particulière qui aurait été de nature à conserver à son égard le délai de recours contentieux, n'a introduit un recours devant le tribunal administratif de La Réunion contre cette décision que le 11 avril 2015, soit plus d'un an après en avoir eu connaissance. Ce recours était dès lors tardif. Le moyen tiré de l'illégalité de cette décision, qui n'a été soulevé par le requérant devant le tribunal administratif de La Réunion que dans sa requête enregistrée le 21 avril 2016 à l'encontre du titre de pension en litige, est par suite et en tout état de cause irrecevable.<br/>
<br/>
              8. Il résulte de ce qui précède que les conclusions à fin d'annulation du titre de pension de M. A...doivent être rejetées. Il y a lieu de rejeter également, par voie de conséquence, ses conclusions à fin d'injonction ainsi que celles présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 15 janvier 2018 du tribunal administratif de La Réunion est annulé. <br/>
Article 2 : La demande présentée par M. A...devant le tribunal administratif de La Réunion et ses conclusions présentées devant le Conseil d'Etat au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 3 : La présente décision sera notifiée à M. B...A...et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-07 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. PRINCIPES INTÉRESSANT L'ACTION ADMINISTRATIVE. - PRINCIPE DE SÉCURITÉ JURIDIQUE - PORTÉE - 1) IMPOSSIBILITÉ DE CONTESTER INDÉFINIMENT UNE DÉCISION INDIVIDUELLE DONT SON DESTINATAIRE A EU CONNAISSANCE [RJ1] - 2) APPLICATION À LA CONTESTATION D'UNE DÉCISION INDIVIDUELLE PAR VOIE D'EXCEPTION - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-01-04-04-01 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. EXCEPTION D'ILLÉGALITÉ. IRRECEVABILITÉ. - MOYEN TIRÉ DE L'ILLÉGALITÉ D'UNE DÉCISION INDIVIDUELLE, NOTIFIÉE SANS MENTION DES VOIES ET DÉLAIS DE RECOURS, SOULEVÉ APRÈS L'EXPIRATION D'UN DÉLAI RAISONNABLE À COMPTER DE LA DATE À LAQUELLE IL EST ÉTABLI QUE L'INTÉRESSÉ EN A EU CONNAISSANCE [RJ1].
</SCT>
<ANA ID="9A"> 01-04-03-07 1) Le principe de sécurité juridique, qui implique que ne puissent être remises en cause sans condition de délai des situations consolidées par l'effet du temps, fait obstacle à ce que puisse être contestée indéfiniment une décision administrative individuelle qui a été notifiée à son destinataire, ou dont il est établi, à défaut d'une telle notification, que celui-ci a eu connaissance. En une telle hypothèse, si le non-respect de l'obligation d'informer l'intéressé sur les voies et les délais de recours, ou l'absence de preuve qu'une telle information a bien été fournie, ne permet pas que lui soient opposés les délais de recours fixés par le code de justice administrative, le destinataire de la décision ne peut exercer de recours juridictionnel au-delà d'un délai raisonnable. En règle générale et sauf circonstances particulières dont se prévaudrait le requérant, ce délai ne saurait, sous réserve de l'exercice de recours administratifs pour lesquels les textes prévoient des délais particuliers, excéder un an à compter de la date à laquelle une décision expresse lui a été notifiée ou de la date à laquelle il est établi qu'il en a eu connaissance.,,2) Refus de promotion, notifié sans indication des voies et délais de recours, dont l'intéressé a eu connaissance au plus tard le 6 janvier 2014. Recours introduit le 21 avril 2016 contre le titre de pension. Le moyen tiré de l'illégalité du refus de promotion, soulevé à l'occasion de ce recours, est irrecevable.</ANA>
<ANA ID="9B"> 54-07-01-04-04-01 Refus de promotion, notifié sans indication des voies et délais de recours, dont l'intéressé a eu connaissance au plus tard le 6 janvier 2014. Recours introduit le 21 avril 2016 contre le titre de pension. Le moyen tiré de l'illégalité du refus de promotion, soulevé à l'occasion de ce recours, est irrecevable.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 13 juillet 2016,,, n° 387763, p. 340.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
