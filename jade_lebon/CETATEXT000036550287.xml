<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036550287</ID>
<ANCIEN_ID>JG_L_2018_01_000000402270</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/55/02/CETATEXT000036550287.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 26/01/2018, 402270</TITRE>
<DATE_DEC>2018-01-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>402270</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON, MEGRET ; SCP DELVOLVE ET TRICHET ; SCP JEAN-PHILIPPE CASTON</AVOCATS>
<RAPPORTEUR>M. Marc Pichon de Vendeuil</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:402270.20180126</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Industrias Durmi a demandé au tribunal administratif de Toulouse, d'une part, de condamner le centre de gestion de la fonction publique territoriale de la Haute-Garonne à lui payer, avec intérêts au taux légal à compter du 21 mars 2011, la somme de 82 634 euros en règlement de la créance qui lui a été cédée par la société Athéma, titulaire du lot n° 5 d'un marché public relatif à la construction d'un bâtiment administratif et, d'autre part, d'annuler la décision implicite du payeur départemental de la Haute-Garonne rejetant sa demande, en date du 23 avril 2013, de paiement de la créance d'un montant de 82 634 euros qui lui a été cédée par la société Athéma, assortie des intérêts au taux légal à compter du 21 mars 2011. Par un jugement n°s 1102540, 1303419 du 3 février 2015, le tribunal administratif de Toulouse a condamné le centre de gestion à verser à la société Industrias Durmi la somme de 82 634 euros avec les intérêts au taux légal à compter du 21 mars 2011 et rejeté le surplus de ses demandes.<br/>
<br/>
              Par un arrêt n°s 15BX01011, 15BX01511 du 9 juin 2016, la cour administrative d'appel de Bordeaux a, sur appel du centre de gestion de la fonction publique territoriale de la Haute Garonne, annulé ce jugement et rejeté les demandes présentées par la société Industrias Durmi.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 9 août et 8 novembre 2016 au secrétariat du contentieux du Conseil d'Etat, la société Industrias Durmi demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge du centre de gestion de la fonction publique territoriale de la Haute-Garonne la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code civil ;<br/>
              - le code des marchés publics ;<br/>
              - la loi n° 75-1334 du 31 décembre 1975 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Pichon de Vendeuil, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boré, Salve de Bruneton, Mégret, avocat de la société Industrias Durmi, à la SCP Delvolvé et Trichet, avocat du centre de gestion de la fonction publique territoriale de la Haute-Garonne et à la SCP Caston, avocat de la société Bpifrance financement.<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par acte d'engagement en date du 28 avril 2009, le centre de gestion de la fonction publique territoriale de la Haute-Garonne a confié à la société Athéma la réalisation du lot n° 5 " Menuiseries extérieures " du marché passé pour la construction du bâtiment administratif de son siège à Labège ; que, pour l'exécution de ces travaux, la société Athéma a elle-même passé commande de différents équipements auprès de la société Industrias Durmi, qui est spécialisée dans la fabrication de produits d'habillage métallique pour bâtiments et fenêtres, pour un montant de 82 634 euros ; que, pour procéder au règlement de cette somme, la société Athéma lui a cédé le 24 février 2010 une partie de sa créance résultant du marché passé avec le centre de gestion de la fonction publique territoriale de la Haute-Garonne ; que, toutefois, la société Athéma avait préalablement cédé la totalité de la créance résultant de ce marché à l'organisme de financement OSEO, cession notifiée au comptable public du centre de gestion par lettre recommandée avec accusé de réception le 21 octobre 2009 ; que la société Industrias Durmi a quant à elle notifié l'acte de cession de créance à ce comptable le 10 mai 2010 ; que le comptable de l'établissement lui a indiqué, par courrier du 11 mai 2010, que la signification de cette cession de créance ne produirait aucun effet compte tenu de la cession totale de la créance intervenue antérieurement au profit d'OSEO ; que, par lettre en date du 28 juillet 2010, OSEO a adressé au payeur départemental une " mainlevée partielle " de la cession qui lui avait été consentie, à hauteur de la somme de 82 634 euros ; que le comptable public a cependant continué de procéder aux derniers paiements des travaux au titre de l'exécution du marché public au profit de la société OSEO ; que la société Industrias Durmi a pour sa part fait procéder à une nouvelle signification de la cession de créance au comptable public le 22 septembre 2010 ; que le centre de gestion de la fonction publique territoriale de la Haute-Garonne a été à nouveau saisi le 21 mars 2011 par la société Industrias Durmi d'une demande de paiement de la somme de 82 634 euros, qu'il a rejetée par courrier du 29 mars 2011 ; que, saisi par la société Industrias Durmi d'un recours indemnitaire, le tribunal administratif de Toulouse a, par jugement du 3 février 2015, condamné le centre de gestion de la fonction publique territoriale de la Haute-Garonne à verser à celle-ci la somme de 82 634 euros assortie des intérêts au taux légal à compter du 21 mars 2011 ; que, par un arrêt en date du 9 juin 2016 contre lequel se pourvoit la société Industrias Durmi, la cour administrative d'appel de Bordeaux a annulé ce jugement et rejeté la demande de la société Industrias Durmi ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 1690 du code civil : " Le cessionnaire n'est saisi à l'égard des tiers que par la signification du transport faite au débiteur. / Néanmoins, le cessionnaire peut être également saisi par l'acceptation du transport faite par le débiteur dans un acte authentique " ; que le cédant d'une créance ne pouvant transmettre plus de droits qu'il n'en détient, la signification d'une cession de créance dont le cédant n'est pas titulaire à la date où elle est faite doit être regardée comme nulle, même lorsqu'elle est régulière en la forme ; qu'il résulte des dispositions précitées que la simple connaissance de la cession de créance par le débiteur cédé ne suffit pas à la lui rendre opposable ; que ni les dispositions précitées ni aucune autre ne permettent au débiteur cédé d'exercer un contrôle sur les motifs de la cession de créance qui lui est signifiée ou de son éventuelle mainlevée ;<br/>
<br/>
              3. Considérant que, pour rejeter la demande de la société Industrias Durmi, la cour administrative d'appel de Bordeaux s'est fondée sur le motif tiré de ce que la mainlevée partielle donnée par l'établissement OSEO au comptable assignataire de la dépense le 28 juillet 2010 n'autorisait pas la société Industrias Durmi à se prévaloir de la cession de créance qui lui avait été antérieurement consentie par la société Athéma sur la même somme, dès lors que cette mainlevée était fondée, à tort, sur la circonstance inexacte que la société Industrias Durmi avait été admise au paiement direct en qualité de sous-traitant, et que le comptable du centre de gestion, qui devait vérifier la qualité de sous-traitant admis au paiement direct de la société Industrias Durmi, ne pouvait régulièrement s'acquitter du paiement de la créance entre les mains de cette dernière ; qu'il résulte de ce qui a été dit au point précédent que s'il appartenait à la cour administrative d'appel de rechercher si, au regard des principes énoncés au point 2, les différents actes par lesquels a été signifiée au débiteur cédé la cession de créance intervenue le 24 février 2010 au profit de la société Industrias Durmi avaient pu produire des effets juridiques, elle a commis une erreur de droit en déniant la qualité de cessionnaire de cette société du seul fait que l'établissement OSEO avait commis une erreur en notifiant au centre de gestion la mainlevée partielle de sa créance ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt doit être annulé ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du centre de gestion de la fonction publique territoriale de la Haute-Garonne la somme de 3 000 euros à verser à la société Industrias Durmi, au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'en revanche, les mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société Industrias Durmi qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Bordeaux du 9 juin 2016 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Bordeaux.<br/>
Article 3 : Le centre de gestion de la fonction publique territoriale de la Haute-Garonne versera une somme de 3 000 euros à la société Industrias Durmi au titre de l'article L. 761-1 du code de justice administrative. Les conclusions du centre de gestion de la fonction publique territoriale de la Haute-Garonne présentées au titre des mêmes dispositions sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la société Industrias Durmi et au centre de gestion de la fonction publique territoriale de la Haute-Garonne. <br/>
Copie en sera adressée au ministre de l'action et des comptes publics et à la société Bpifrance financement.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-05-02 MARCHÉS ET CONTRATS ADMINISTRATIFS. EXÉCUTION FINANCIÈRE DU CONTRAT. RÈGLEMENT DES MARCHÉS. - CESSION D'UNE CRÉANCE NÉE DE L'EXÉCUTION D'UN MARCHÉ PUBLIC - 1) OPPOSABILITÉ - A) SIGNIFICATION DE LA CESSION D'UNE CRÉANCE DONT LE CÉDANT N'EST PAS TITULAIRE - NULLITÉ DE LA SIGNIFICATION - CONSÉQUENCE - ABSENCE - B) CONNAISSANCE PAR LE DÉBITEUR CÉDÉ DE LA CESSION DE CRÉANCE - ABSENCE, FAUTE DE SIGNIFICATION - 2) CONTRÔLE DES MOTIFS DE LA CESSION DE CRÉANCE - A) PAR LE DÉBITEUR CÉDÉ - ABSENCE - B) PAR LE JUGE ADMINISTRATIF - ABSENCE.
</SCT>
<ANA ID="9A"> 39-05-02 1) a) Le cédant d'une créance ne pouvant transmettre plus de droits qu'il n'en détient, la signification d'une cession de créance dont le cédant n'est pas titulaire à la date où elle est faite doit être regardée comme nulle, même lorsqu'elle est régulière en la forme.... ,,b) Il résulte de l'article 1690 du code civil que la simple connaissance de la cession de créance par le débiteur cédé ne suffit pas à la lui rendre opposable.... ,,2) a) Ni cet article, ni aucune autre disposition du code civil ne permet au débiteur cédé d'exercer un contrôle sur les motifs de la cession de créance qui lui est signifiée ou de son éventuelle mainlevée.... ,,b) S'il appartient au juge administratif de rechercher si les différents actes par lesquels a été signifiée au débiteur cédé une cession de créance ont pu produire des effets juridiques, il ne lui incombe pas de contrôler les motifs de cette cession.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
