<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030484858</ID>
<ANCIEN_ID>JG_L_2015_04_000000365655</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/48/48/CETATEXT000030484858.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère / 6ème SSR, 15/04/2015, 365655</TITRE>
<DATE_DEC>2015-04-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>365655</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère / 6ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>FOUSSARD ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>Mme Marie Grosset</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:365655.20150415</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé à la commission départementale d'aide sociale de la Mayenne d'annuler la décision du président du conseil général de la Mayenne du 11 mai 2011 d'exercer à son encontre un recours sur donation pour un montant de 14 666,66 euros. Par une décision du 7 juillet 2011, la commission départementale a rejeté sa demande.<br/>
<br/>
              Par une décision n° 111050 du 30 novembre 2012, la Commission centrale d'aide sociale a rejeté l'appel formé par M. B...contre la décision de la commission départementale d'aide sociale de la Mayenne du 7 juillet 2011.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et deux mémoires en réplique, enregistrés les 31 janvier 2013, 29 avril 2013, 21 novembre 2013 et 20 mars 2015 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette décision de la Commission centrale d'aide sociale ;<br/>
<br/>
              2°) de mettre à la charge du département de la Mayenne la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ainsi que, au titre des dépens, la contribution pour l'aide juridique mentionnée à l'article R. 761-1 du même code.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code civil ;<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Grosset, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Foussard, avocat de M. B...et à la SCP Lyon-Caen, Thiriez, avocat du département de la Mayenne ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. B...a acquis avec sa mère, en mai 2006, un pavillon situé à Mayenne, pour les quatre quinzièmes en pleine propriété et pour les onze quinzièmes en nue-propriété, Mme B...acquérant l'usufruit correspondant ; que Mme B...a bénéficié de l'aide sociale départementale pour la prise en charge de ses frais de séjour en établissement d'hébergement pour personnes âgées dépendantes entre le 24 avril 2008 et le 24 juillet 2010, date de son décès ; que, par un acte notarié du 16 septembre 2008, elle a consenti à son fils une donation des onze quinzièmes de l'usufruit du bien acquis en 2006 ; qu'à la suite du décès de MmeB..., le département de la Mayenne a décidé d'exercer un recours en récupération contre cette donation pour un montant de 14 666,66 euros ;<br/>
<br/>
              2. Considérant, en premier lieu, qu'en vertu des règles générales de procédure applicables, même sans texte, à toute juridiction administrative, la minute d'une décision rendue par la Commission centrale d'aide sociale doit au moins être revêtue de la signature du président de la formation de jugement aux fins d'en attester la conformité au délibéré ; que rien ne fait par ailleurs obstacle à ce que la décision soit également signée par le rapporteur ; qu'en l'espèce, il résulte de l'examen de la minute de la décision attaquée qu'elle a été signée par le président de la formation de jugement et par le rapporteur ; que la circonstance qu'elle ne soit pas revêtue de la signature d'un greffier d'audience est sans incidence sur sa régularité ; que, par suite, M. B... n'est pas fondé à soutenir que la  décision de la Commission centrale serait irrégulière faute de comporter les signatures requises ; <br/>
<br/>
              3. Considérant, en deuxième lieu, qu'il ne ressort pas des pièces du dossier soumis aux juges du fond que le signataire de la décision de récupération sur donation du 11 mai 2011 n'aurait pas bénéficié d'une délégation de signature régulière du président du conseil général de la Mayenne ; que, dès lors, M. B...n'est pas fondé à soutenir que la Commission centrale aurait commis une erreur de droit en s'abstenant de relever d'office l'incompétence de l'auteur de la décision litigieuse ;<br/>
<br/>
              4. Considérant, en troisième lieu, que l'article L. 132-8 du code de l'action sociale et des familles dispose que : " Des recours sont exercés, selon le cas, par l'Etat ou le département : / 1° Contre le bénéficiaire revenu à meilleure fortune ou contre la succession du bénéficiaire ; / 2° Contre le donataire, lorsque la donation est intervenue postérieurement à la demande d'aide sociale ou dans les dix ans qui ont précédé cette demande ; / 3° Contre le légataire (...) " ; qu'aux termes de l'article 894 du code civil : " La donation entre vifs est un acte par lequel le donateur se dépouille actuellement et irrévocablement de la chose donnée en faveur du donataire qui l'accepte " ; <br/>
<br/>
              5. Considérant que les dispositions de l'article L. 132-8 du code de l'action sociale et des familles ne font aucune distinction selon la nature de  la donation ; qu'il en résulte que la donation de l'usufruit d'un bien entre dans le champ de leurs prévisions, sauf lorsque, compte tenu notamment des charges dont elle est grevée, elle ne recouvre pas une intention libérale de son auteur ; que, toutefois, pour l'application de ces dispositions, il appartient aux juridictions de l'aide sociale, eu égard tant à la finalité de leur intervention qu'à leur qualité de juge de plein contentieux, non d'apprécier la légalité de la décision de récupération, mais de se prononcer elles-mêmes sur le bien-fondé de l'action engagée par la collectivité publique d'après l'ensemble des circonstances de fait dont il est justifié par l'une et l'autre parties à la date de leur propre décision ; qu'elles ont la faculté, en fonction des circonstances particulières de chaque espèce, d'aménager les modalités de cette récupération et, le cas échéant, d'en reporter les effets dans le temps ; <br/>
<br/>
              6. Considérant, tout d'abord, qu'il résulte de ce qui précède que la Commission centrale d'aide sociale n'a pas commis d'erreur de droit en jugeant que la donation de l'usufruit d'une partie d'un bien pouvait faire l'objet d'un recours contre le donataire sur le fondement du 2° de l'article L. 132-8 du code de l'action sociale et des familles ; <br/>
<br/>
              7. Considérant, ensuite, que la Commission centrale s'est fondée tant sur l'acte de donation du 12 septembre 2008 que sur l'ensemble des circonstances de fait dans lesquelles la donation était intervenue pour rechercher si celle-ci procédait bien d'une intention libérale ; que ce faisant, la Commission centrale, qui s'est fondée sur l'ensemble des éléments du dossier et non, comme le soutient M.B..., sur le seul acte de donation, n'a pas fait peser sur le requérant la charge de la preuve ni méconnu les règles gouvernant son office ; que c'est sans commettre d'erreur de droit qu'elle a déduit de ses constatations, au terme d'une appréciation souveraine exempte de dénaturation, que la donation procédait bien d'une intention libérale de MmeB..., qui s'était dépouillée d'une partie de son patrimoine au profit de son fils ; <br/>
<br/>
              8. Considérant, enfin, que la donation de l'usufruit d'un bien donne à celui qui en bénéficie le droit de jouir de ce bien et, notamment, d'en percevoir les revenus ; que la circonstance qu'en l'absence de donation, l'usufruit se serait éteint à la mort de la donatrice et le donataire, disposant déjà de la nue-propriété du bien, en aurait alors eu la pleine propriété, ne faisait pas obstacle à l'exercice d'un recours contre le donataire, sur le fondement du 2° de l'article L. 132-8 du code de l'action sociale et des familles ; que, par suite, la Commission centrale n'a pas commis d'erreur de droit en jugeant que la circonstance que le droit d'usufruit se serait éteint au décès de Mme B...en juillet 2010 était sans incidence sur le droit du département d'exercer un recours en récupération de l'aide sociale à l'encontre de son fils ; <br/>
<br/>
              9. Considérant, en quatrième lieu, que l'article R. 132-11 du même code dispose que : " Les recours prévus à l'article L. 132-8 sont exercés, dans tous les cas, dans la limite du montant des prestations allouées au bénéficiaire de l'aide sociale. / En cas de donation, le recours est exercé jusqu'à concurrence de la valeur des biens donnés par le bénéficiaire de l'aide sociale, appréciée au jour de l'introduction du recours, déduction faite, le cas échéant, des plus-values résultant des impenses ou du travail du donataire (...) " ; que si ces dispositions doivent conduire le juge de l'aide sociale, lorsque le décès du donateur d'un usufruit, intervenant avant l'action en récupération, a pour effet de faire disparaître cet usufruit, de tenir compte, pour déterminer la valeur de la donation, de la durée effective pendant laquelle le donataire a bénéficié de l'usufruit, elles lui imposent seulement de tenir compte de la valeur en pleine propriété, à la date d'exercice de l'action, du bien sur lequel portait l'usufruit lorsque, comme en l'espèce, la donation de l'usufruit intervient au profit du nu-propriétaire et lui confère ainsi la pleine propriété du bien ; qu'au vu des pièces du dossier qui lui était soumis et dont l'appréciation souveraine n'est pas contestée, s'agissant de la valeur du bien en pleine propriété, la Commission centrale n'a pas commis d'erreur de droit en se prononçant sur l'action en fonction de la valeur des droits d'usufruit telle qu'elle avait été appréciée à la date de la donation ;<br/>
<br/>
              10. Considérant qu'il résulte de tout ce qui précède que M. B...n'est pas fondé à demander l'annulation de la décision de la Commission centrale d'aide sociale qu'il attaque ;<br/>
<br/>
              11. Considérant que, dans les circonstances de l'espèce, il y a lieu de laisser la contribution pour l'aide juridique à la charge de M.B... ;<br/>
<br/>
              12. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge du département de la Mayenne, qui n'est pas la partie perdante, la somme que M. B...demande à ce titre ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions de M. B...présentées au même titre ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. B...est rejeté.<br/>
Article 2 : Les conclusions du département de la Mayenne présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. A...B...et au département de la Mayenne.<br/>
Copie en sera adressée pour information à la ministre des affaires sociales, de la santé et des droits des femmes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-02-03 AIDE SOCIALE. DIFFÉRENTES FORMES D'AIDE SOCIALE. AIDE SOCIALE AUX PERSONNES ÂGÉES. - RECOURS EN RÉCUPÉRATION (ART. L. 132-8 DU CASF) - DONATIONS - 1) NOTION - INCLUSION - DONATION D'UN USUFRUIT - 2) MODALITÉS D'ÉVALUATION DANS L'HYPOTHÈSE OÙ LE DÉCÈS DU DONATEUR FAIT DISPARAÎTRE L'USUFRUIT - CAS GÉNÉRAL - CAS PARTICULIER DE LA DONATION DE L'USUFRUIT INTERVENANT AU PROFIT DU NU-PROPRIÉTAIRE.
</SCT>
<ANA ID="9A"> 04-02-03 1) Les dispositions de l'article L. 132-8 du code de l'action sociale et des familles (CASF), relatives au recours en récupération exercé par l'Etat ou le département, ne font aucune distinction selon la nature de la donation. Il en résulte que la donation de l'usufruit d'un bien entre dans le champ de leurs prévisions, sauf lorsque, compte tenu notamment des charges dont elle est grevée, elle ne recouvre pas une intention libérale de son auteur.... ,,2) Toutefois, pour l'application de ces dispositions, il appartient aux juridictions de l'aide sociale, eu égard tant à la finalité de leur intervention qu'à leur qualité de juge de plein contentieux, non d'apprécier la légalité de la décision de récupération, mais de se prononcer elles-mêmes sur le bien-fondé de l'action engagée par la collectivité publique d'après l'ensemble des circonstances de fait dont il est justifié par l'une et l'autre parties à la date de leur propre décision. Elles ont la faculté, en fonction des circonstances particulières de chaque espèce, d'aménager les modalités de cette récupération et, le cas échéant, d'en reporter les effets dans le temps.,,,Si les dispositions de l'article R. 132-11 du CASF doivent conduire le juge de l'aide sociale, lorsque le décès du donateur d'un usufruit, intervenant avant l'action en récupération, a pour effet de faire disparaître cet usufruit, à tenir compte, pour déterminer la valeur de la donation, de la durée effective pendant laquelle le donataire a bénéficié de l'usufruit pour déterminer la valeur de la donation, elles lui imposent seulement de tenir compte de la valeur en pleine propriété, à la date d'exercice de l'action, du bien sur lequel portait l'usufruit lorsque la donation de l'usufruit intervient au profit du nu-propriétaire et lui confère ainsi la pleine propriété du bien.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
