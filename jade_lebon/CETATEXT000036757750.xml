<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036757750</ID>
<ANCIEN_ID>JG_L_2018_03_000000411122</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/75/77/CETATEXT000036757750.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 6ème chambres réunies, 30/03/2018, 411122</TITRE>
<DATE_DEC>2018-03-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411122</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>M. Florian Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:411122.20180330</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Daniel Ashde a demandé au tribunal administratif de Nantes d'annuler pour excès de pouvoir les arrêtés des 5 juin et 18 décembre 2014 du maire des Sables-d'Olonne refusant de lui accorder le permis de construire qu'elle sollicitait pour la construction de 13 logements sur la parcelle cadastrée AP n° 49-50 située avenue Joseph Chaillet. Par un jugement  n°1406129,1500217 du 13 décembre 2016, le tribunal administratif de Nantes a annulé ces deux décisions.<br/>
<br/>
              Par un arrêt n° 17NT00542 du 17 mai 2017, la cour administrative d'appel de Nantes a rejeté la requête de la commune des Sables-d'Olonne tendant à ce qu'il soit sursis à l'exécution de ce jugement sur le fondement de l'article R. 811-15 du code de justice administrative.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire, un mémoire en réplique et un nouveau mémoire, enregistrés les 1er juin, 15 juin, 13 septembre et 3 octobre 2017 au secrétariat du contentieux du Conseil d'Etat, la commune des Sables-d'Olonne demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de la société Daniel Ashde la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de la commune des Sables-d'Olonne et à la SCP Odent, Poulet, avocat de la société Daniel Ashde.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du sursis à exécution que, le 10 mars 2014, la SAS Daniel Ashde a sollicité la délivrance d'un permis de construire en vue de la réalisation de treize logements situés avenue Joseph Chaillet aux Sables-d'Olonne (Vendée) ; que, par un arrêté du 5 juin 2014, le maire a refusé de lui délivrer ce permis pour des motifs tirés du non-respect de la typologie des logements prévue par l'article UB 2.3 du règlement du plan local d'urbanisme de la commune ainsi que de la méconnaissance par le projet des articles UB 3.2, UB 7.2 et UB 13.1, relatifs, respectivement, à la largeur de la voie d'accès aux bâtiments, aux règles d'implantation par rapport aux limites séparatives latérales et aux espaces libres et aux plantations ; que, saisi par la société sur le fondement de l'article L. 521-1 du code de justice administrative, le juge des référés a, par une ordonnance du 7 août 2014, suspendu l'exécution de la décision litigieuse et enjoint au maire des Sables-d'Olonne de procéder à une nouvelle instruction de la demande ; qu'à la suite de cette injonction, le maire, par un arrêté du 18 décembre 2014, a de nouveau refusé le permis sollicité, en se fondant cette fois sur des motifs tirés de l'atteinte portée par le projet au caractère des lieux avoisinants et de la méconnaissance des articles UB 10.1 et UB 11.2.3 du règlement, relatifs au nombre des dispositifs d'éclairement au niveau des combles et à la couverture des bâtiments ; que, par un jugement du 13 décembre 2016, le tribunal administratif de Nantes a annulé les arrêtés municipaux des 5 juin et 18 décembre 2014, en censurant les différents motifs de rejet opposés à la société pétitionnaire ; que la commune se pourvoit en cassation contre l'arrêt du 17 mai 2017 par lequel la cour administrative d'appel de Nantes a rejeté sa demande tendant à ce qu'il soit sursis à l'exécution de ce jugement ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 811-15 du code de justice administrative : " Lorsqu'il est fait appel d'un jugement de tribunal administratif prononçant l'annulation d'une décision administrative, la juridiction d'appel peut, à la demande de l'appelant, ordonner qu'il soit sursis à l'exécution de ce jugement si les moyens invoqués par l'appelant paraissent, en l'état de l'instruction, sérieux et de nature à justifier, outre l'annulation ou la réformation du jugement attaqué, le rejet des conclusions à fin d'annulation accueillies par ce jugement " ;<br/>
<br/>
              3. Considérant que, pour rejeter une demande tendant au sursis à exécution d'un jugement d'un tribunal administratif prononçant l'annulation d'une décision administrative, le juge d'appel, afin de mettre le juge de cassation à même d'exercer son contrôle, doit faire apparaître le raisonnement qu'il a suivi ; qu'à cet effet, il peut se borner à relever qu'aucun des moyens de l'appelant mettant en cause la régularité du jugement attaqué ou le bien-fondé du ou des moyens d'annulation retenus par les premiers juges ne paraît, en l'état de l'instruction, sérieux, dès lors qu'il a procédé à l'analyse, dans les visas ou les motifs de sa décision, des moyens invoqués par l'appelant ; qu'en revanche, si l'un des moyens invoqués en appel apparaît sérieux mais que la demande de sursis doit en définitive être rejetée au motif qu'un des moyens soulevés par le demandeur de première instance ou qu'un moyen d'ordre public semble de nature à confirmer, en l'état de l'instruction, l'annulation de la décision administrative en litige, il incombe au juge d'appel de désigner avec précision tant le moyen d'appel regardé comme sérieux que celui qu'il estime, en l'état du dossier, de nature à confirmer l'annulation prononcée par les premiers juges ;<br/>
<br/>
              4. Considérant qu'en l'espèce, le jugement attaqué retenait notamment que le maire n'avait pas pu légalement fonder le refus de permis de construire litigieux sur les dispositions de l'article UB 2.3 du règlement du plan local d'urbanisme dès lors que cet article, en exigeant une certaine proportion de plusieurs types de logements dans tout projet de construction d'un immeuble collectif d'habitation, allait au-delà de ce que permettait l'article L. 123-1-5 du code de l'urbanisme ; que la commune des Sables-d'Olonne invitait la cour administrative d'appel à censurer ce motif d'annulation ; que, dans ses écritures en défense, la société intimée, reprenant un moyen qu'elle avait invoqué en première instance, soutenait qu'en tout état de cause, l'article UB 2.3 était entaché d'une autre illégalité, tenant au fait qu'il avait été rendu applicable à toutes les zones urbaines et à urbaniser, en méconnaissance des mêmes dispositions du code de l'urbanisme ; qu'en se bornant à énoncer, dans l'arrêt attaqué, qu'aucun des moyens invoqués par la commune des Sables-d'Olonne n'apparaissait de nature à justifier l'annulation du jugement attaqué et le rejet des conclusions accueillies par ce jugement, la cour administrative d'appel n'a, notamment, pas fait apparaître si elle estimait que le moyen de la commune relatif à l'article UB 2.3 n'était pas sérieux ou si elle retenait que le moyen relatif au même article invoqué en défense par la société devait conduire à confirmer l'annulation du refus de permis de construire ; qu'ainsi, la cour n'a pas mis le juge de cassation en mesure d'exercer le contrôle qui lui incombe ; que son arrêt est, par suite, entaché d'insuffisance de motivation et doit, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, être annulé ;<br/>
<br/>
              5. Considérant que, dans les circonstances de l'espèce, il y a lieu de faire application des dispositions de l'article L. 821-2 du code de justice administrative et de régler l'affaire au titre de la procédure de sursis engagée par la commune des Sables-d'Olonne ;<br/>
<br/>
              6. Considérant qu'aux termes du II de l'article L. 123-1-5 du code de l'urbanisme, alors applicable, désormais repris à l'article L. 151-14 de ce code : " Le règlement peut fixer les règles suivantes relatives à l'usage des sols et la destination des construction : / (...) 3° Délimiter, dans les zones urbaines ou à urbaniser, des secteurs dans lesquels les programmes de logements comportent une proportion de logements d'une taille minimale qu'il fixe ; (...) " ;<br/>
<br/>
              7. Considérant que si, en application de ces dispositions, éclairées par les travaux préparatoires de la loi du 25 mars 2009 dont elles sont issues, le plan local d'urbanisme peut imposer, dans les secteurs des zones urbaines ou à urbaniser qu'il définit, que les programmes immobiliers comportent, afin d'assurer une meilleure prise en compte des besoins des familles, une proportion de logements d'une taille minimale, définie en fonction du nombre de pièces dont ils se composent, proportion qui peut être exprimée sous la forme d'un pourcentage de la surface totale des logements, il ne saurait, en revanche imposer sur ce fondement aux constructeurs une répartition détaillée des logements selon leur taille, notamment en imposant plusieurs types de logements et en fixant des proportions minimales à respecter pour plusieurs types ; <br/>
<br/>
              8. Considérant qu'aux termes de l'article UB 2.3. du plan local d'urbanisme de la commune : " (...) Les opérations comprenant plus de cinq logements (collectifs ou individuels) présentant des T1 ou des T2 doivent proposer au moins trois types de logements. 80 % minimum de la surface de plancher (affectée aux logements) seront composés de logements de types 3 ou plus grand, sachant que les types 3 ne pourront représenter plus de 50 % de la surface de plancher totale (affectée aux logements) des logements de type 3 ou plus grand " ; qu'il résulte de ce qui précède que le moyen de la commune des Sables-d'Olonne, tiré de ce que les premiers juges auraient estimé à tort que ces dispositions excèdaient ce que permet le 3° du II de l'article L. 123-1-5 du code de l'urbanisme et ne pouvaient, par suite, fournir une base légale au refus de permis de construire litigieux, ne présente pas un caractère sérieux ; <br/>
<br/>
              9. Considérant que les autres moyens d'appel soulevés par la commune, tirés de ce que le tribunal a, d'une part, méconnu l'article UB 11 du règlement du plan local d'urbanisme en retenant que la construction projetée ne portait pas atteinte au caractère des lieux avoisinants et, d'autre part, estimé à tort illégal le motif de refus opposé par le maire tiré de la méconnaissance par le pétitionnaire des dispositions de l'article UB 11.2.3 relatives aux matériaux de couverture des bâtiments, ne peuvent davantage être regardés, en l'état de l'instruction, comme sérieux ;<br/>
<br/>
              10. Considérant qu'il résulte de ce qui précède que la commune des Sables-d'Olonne n'est pas fondée à demander à ce qu'il soit sursis à l'exécution du jugement du tribunal administratif de Nantes du 13 décembre 2016 ;<br/>
<br/>
              11. Considérant, enfin, que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de la société Daniel Ashde la somme que la commune des Sables-d'Olonne, qui doit être regardée comme la partie qui perd pour l'essentiel, demande au titre des frais exposés par elle et non compris dans les dépens ; qu'en revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune des Sables-d'Olonne le versement à la société Daniel Ashde d'une somme de 3 000 euros au titre de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nantes du 17 mai 2017 est annulé.<br/>
<br/>
Article 2 : La demande de la Commune des Sables-d'Olonne tendant à ce qu'il soit sursis à l'exécution du jugement du tribunal administratif de Nantes du 13 décembre 2016 est rejetée.<br/>
<br/>
Article 3 : La commune des Sables-d'Olonne versera à la société Daniel Ashde une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions présentées par la commune des Sables-d'Olonne au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à la commune des Sables-d'Olonne et à la société Daniel Ashde. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-01-01-01-03 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PLANS D'AMÉNAGEMENT ET D'URBANISME. PLANS D`OCCUPATION DES SOLS (POS) ET PLANS LOCAUX D'URBANISME (PLU). LÉGALITÉ DES PLANS. LÉGALITÉ INTERNE. - DÉLIMITATION, DANS LES ZONES URBAINES OU À URBANISER, DES SECTEURS DANS LESQUELS LES PROGRAMMES DE LOGEMENTS COMPORTENT UNE PROPORTION DE LOGEMENTS D'UNE TAILLE MINIMALE FIXÉE PAR LE PLU (3° DU II DE L'ARTICLE L. 123-1-5 DU CODE DE L'URBANISME) - PORTÉE.
</SCT>
<ANA ID="9A"> 68-01-01-01-03 Si, en application du 3° du II de l'article L. 123-1-5 du code de l'urbanisme, désormais repris à l'article L. 151-14 de ce code, éclairé par les travaux préparatoires de la loi dont il est issu, le plan local d'urbanisme peut imposer, dans les secteurs des zones urbaines ou à urbaniser qu'il définit, que les programmes immobiliers comportent, afin d'assurer une meilleure prise en compte des besoins des familles, une proportion de logements d'une taille minimale, définie en fonction du nombre de pièces dont ils se composent, proportion qui peut être exprimée sous la forme d'un pourcentage de la surface totale des logements, il ne saurait, en revanche imposer sur ce fondement aux constructeurs une répartition détaillée des logements selon leur taille, notamment en imposant plusieurs types de logements et en fixant des proportions minimales à respecter pour plusieurs types.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
