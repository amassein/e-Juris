<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030713382</ID>
<ANCIEN_ID>JG_L_2015_06_000000365205</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/71/33/CETATEXT000030713382.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème / 5ème SSR, 08/06/2015, 365205</TITRE>
<DATE_DEC>2015-06-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>365205</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème / 5ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>M. Bruno Bachini</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:365205.20150608</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 15 janvier et 15 avril 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B... A..., demeurant... ; M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision n° 750 du 12 juillet 2012 par laquelle le Conseil national de l'enseignement supérieur et de la recherche, statuant en matière disciplinaire, a rejeté son appel formé contre la décision de la section disciplinaire du conseil d'administration de l'université Paris 4 du 21 mai 2010 prononçant sa révocation, accompagnée d'une interdiction définitive d'exercer toute fonction dans un établissement public ou privé ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'éducation ;<br/>
<br/>
              Vu le décret n° 92-657 du 13 juillet 1992 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Bachini, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>
              Sur la recevabilité de la plainte :<br/>
<br/>
              1. Considérant qu'aux termes de l'article 3 du décret du 13 juillet 1992 relatif à la procédure disciplinaire dans les établissements publics d'enseignement supérieur placés sous la tutelle du ministre chargé de l'enseignement supérieur, applicable à la date à laquelle le recteur de l'académie de Nice a saisi la section disciplinaire du conseil d'administration de l'université Sud-Toulon-Var d'une plainte dirigée contre M.A..., enseignant-chercheur et président de cette université : " Les enseignants-chercheurs et enseignants (...) relèvent de la section disciplinaire de l'établissement où les faits donnant lieu à des poursuites ont été commis. (...) " ; qu'aux termes de l'article 23 du même décret : " Les poursuites sont engagées devant la section disciplinaire compétente : / 1°) Par le président ou directeur de l'établissement dans les cas prévus à l'article 3. / En cas de défaillance de l'autorité responsable, le recteur d'académie peut engager la procédure après avoir saisi cette autorité depuis au moins un mois (...) " ; que ces dispositions donnaient qualité au recteur d'académie pour engager, le cas échéant, des poursuites contre un président d'université à raison de faits commis par lui dans son université, sans qu'il y ait lieu, dans une telle hypothèse, de mettre préalablement le président de l'université en demeure d'engager lui-même ces poursuites ; qu'ainsi, M. A...n'est pas fondé à soutenir que le Conseil national de l'enseignement supérieur et de la recherche, statuant en matière disciplinaire, aurait dû relever d'office une irrecevabilité de la plainte qui était à l'origine de la sanction qu'il a prononcée ; <br/>
<br/>
              Sur la procédure devant le Conseil national de l'enseignement supérieur et de la recherche : <br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 232-37 du code de l'éducation, qui fixe la procédure applicable devant le Conseil national de l'enseignement supérieur et de la recherche statuant en matière disciplinaire : " La commission d'instruction entend la personne déférée et instruit l'affaire par tous les moyens qu'elle juge propres à l'éclairer et en fait un rapport écrit comprenant l'exposé des faits et moyens des parties. Ce rapport est transmis au président dans un délai qu'il a préalablement fixé et qui ne peut être supérieur à trois mois. Toutefois, le président peut ordonner un supplément d'instruction s'il estime que l'affaire n'est pas en état d'être jugée. Le rapport et les pièces des dossiers sont déposés par le rapporteur au secrétariat du Conseil national de l'enseignement supérieur et de la recherche pour être tenus à la disposition des parties, de leur conseil et des membres du conseil statuant en matière disciplinaire, dix jours francs avant la date fixée pour la séance du jugement. (...) / Dans le cas où la juridiction est saisie de nouveaux éléments, le président ordonne la réouverture de l'instruction qui se déroule selon les formes prescrites à l'alinéa précédent du présent article " ; qu'aux termes de l'article R. 232-38 du même code, dans sa rédaction applicable à l'espèce : " Le président du Conseil national de l'enseignement supérieur et de la recherche statuant en matière disciplinaire convoque chacune des personnes intéressées devant la formation de jugement par lettre recommandée, avec demande d'avis de réception, quinze jours au moins avant la date de la séance de jugement. (...) / Au jour fixé pour la séance, un secrétaire est désigné en leur sein par les enseignants-chercheurs siégeant dans la formation de jugement. Le rapport de la commission d'instruction est lu par le rapporteur ou, en cas d'absence de celui-ci, par le secrétaire. S'il l'estime nécessaire, le président peut entendre des témoins à l'audience. Sur sa demande, le président ou le directeur d'un établissement mentionné aux articles 2 et 3 du décret n° 92-657 du 13 juillet 1992 cité à l'article R. 232-33 ou son représentant, est entendu ainsi que le recteur d'académie ou son représentant, s'il est l'auteur des poursuites disciplinaires ou de l'appel. La personne déférée et son conseil sont entendus dans leurs observations. La personne déférée a la parole en dernier (...) " ; <br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis au Conseil national de l'enseignement supérieur et de la recherche que le recteur de l'académie de Nice a produit son premier mémoire en défense devant cette juridiction postérieurement au dépôt du rapport de la commission d'instruction ; que ce mémoire, qui discutait de manière substantielle l'ensemble des faits reprochés à M. A...et comportait certains éléments supplémentaires par rapport à ceux déjà débattus en première instance, devait être regardé comme comportant des éléments nouveaux au sens des dispositions citées ci-dessus de l'article R. 232-7 du code de l'éducation ; que, conformément à ces dispositions, il appartenait, dès lors, au président du Conseil national de l'enseignement supérieur et de la recherche de rouvrir l'instruction ; qu'au surplus, cette production n'a été communiquée à M. A...que le 9 juillet 2012, moins de 48 heures avant l'audience disciplinaire du 11 juillet 2012, ce qui l'a privé d'un délai suffisant pour préparer utilement sa défense ; que, dès lors, celui-ci est fondé à soutenir que le Conseil national de l'enseignement supérieur et de la recherche a méconnu les droits de la défense et à demander, par suite, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, l'annulation de la décision qu'il attaque ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 500 euros à verser à M. A...au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
						--------------<br/>
Article 1er : La décision du Conseil national de l'enseignement supérieur et de la recherche du 12 juillet 2012 est annulée.<br/>
Article 2 : L'affaire est renvoyée au Conseil national de l'enseignement supérieur et de la recherche.<br/>
Article 3 : L'Etat versera à M. A...une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : La présente décision sera notifiée à M. B...A...et à la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche. <br/>
Copie en sera adressée, pour information, à l'université Paris 4.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">30-01-01-01-03 ENSEIGNEMENT ET RECHERCHE. QUESTIONS GÉNÉRALES. ORGANISATION SCOLAIRE ET UNIVERSITAIRE. ORGANISMES CONSULTATIFS NATIONAUX. CONSEIL NATIONAL DE L'ENSEIGNEMENT SUPÉRIEUR ET DE LA RECHERCHE. - CONSEIL STATUANT EN APPEL COMME JURIDICTION DISCIPLINAIRE - PRODUCTION D'UN MÉMOIRE CONTENANT DES ÉLÉMENTS NOUVEAUX  POSTÉRIEUREMENT À LA CLÔTURE DE L'INSTRUCTION - OBLIGATION DE RÉOUVERTURE -EXISTENCE - APPLICATION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-04-03 PROCÉDURE. INSTRUCTION. CARACTÈRE CONTRADICTOIRE DE LA PROCÉDURE. - CNESER STATUANT EN APPEL COMME JURIDICTION DISCIPLINAIRE - PRODUCTION D'UN MÉMOIRE CONTENANT DES ÉLÉMENTS NOUVEAUX  POSTÉRIEUREMENT À LA CLÔTURE DE L'INSTRUCTION - OBLIGATION DE RÉOUVERTURE - EXISTENCE - APPLICATION.
</SCT>
<ANA ID="9A"> 30-01-01-01-03 Conseil national de l'enseignement supérieur et de la recherche (CNESER) statuant en appel comme juridiction disciplinaire. En vertu de l'article R. 232-37 du code de l'éducation, le président ordonne la réouverture de l'instruction dans le cas où la juridiction est saisie de nouveaux éléments.... ,,Production du premier mémoire en défense postérieurement au dépôt du rapport de la commission d'instruction. Ce mémoire, qui discutait de manière substantielle l'ensemble des faits reprochés au mis en cause et comportait certains éléments supplémentaires par rapport à ceux déjà débattus en première instance, devait être regardé comme comportant des éléments nouveaux au sens des dispositions de l'article R. 232-7 du code de l'éducation. Conformément à ces dispositions, il appartenait, dès lors, au président du CNESER de rouvrir l'instruction. Au surplus, cette production n'a été communiquée au mis en cause que moins de 48 heures avant l'audience disciplinaire, ce qui l'a privé d'un délai suffisant pour préparer utilement sa défense. Méconnaissance des droits de la défense.</ANA>
<ANA ID="9B"> 54-04-03 Conseil national de l'enseignement supérieur et de la recherche (CNESER) statuant en appel comme juridiction disciplinaire. En vertu de l'article R. 232-37 du code de l'éducation, le président ordonne la réouverture de l'instruction dans le cas où la juridiction est saisie de nouveaux éléments.... ,,Production du premier mémoire en défense postérieurement au dépôt du rapport de la commission d'instruction. Ce mémoire, qui discutait de manière substantielle l'ensemble des faits reprochés au mis en cause et comportait certains éléments supplémentaires par rapport à ceux déjà débattus en première instance, devait être regardé comme comportant des éléments nouveaux au sens des dispositions de l'article R. 232-7 du code de l'éducation. Conformément à ces dispositions, il appartenait, dès lors, au président du CNESER de rouvrir l'instruction. Au surplus, cette production n'a été communiquée au mis en cause que moins de 48 heures avant l'audience disciplinaire, ce qui l'a privé d'un délai suffisant pour préparer utilement sa défense. Méconnaissance des droits de la défense.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
