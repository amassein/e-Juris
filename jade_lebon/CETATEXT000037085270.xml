<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037085270</ID>
<ANCIEN_ID>JG_L_2018_06_000000407310</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/08/52/CETATEXT000037085270.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 18/06/2018, 407310</TITRE>
<DATE_DEC>2018-06-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>407310</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER, PINATEL ; SCP POTIER DE LA VARDE, BUK LAMENT, ROBILLOT</AVOCATS>
<RAPPORTEUR>Mme Sophie-Caroline de Margerie</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:407310.20180618</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif de Nice d'annuler pour excès de pouvoir l'arrêté du préfet des Alpes-Maritimes du 17 octobre 2013 en tant qu'il a déclaré cessibles, au bénéfice de la commune de Nice, les immeubles dont l'acquisition est nécessaire au projet de création d'un équipement public sur la parcelle cadastrée NH n° 138, villa " La Luna ", située aux 265, promenade des Anglais et 195, avenue de la Californie, à Nice. Par un jugement n° 1400441 du 18 novembre 2014, le tribunal administratif de Nice a annulé cet arrêté.<br/>
<br/>
              Par un arrêt n° 15MA00411,15MA02339 du 28 novembre 2016, la cour administrative d'appel de Marseille a rejeté l'appel formé  par la commune de Nice contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 30 janvier et 2 mai 2017 au secrétariat du contentieux du Conseil d'Etat, la commune de Nice demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de Mme B...la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'expropriation pour cause d'utilité publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie-Caroline de Margerie, conseiller d'Etat, <br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Potier, de la Varde, Buk Lament, Robillot, avocat de la commune de Nice, et à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de MmeB.connu d'après les renseignements qu'il a pu recueillir auprès du service du cadastre ou du conservateur des hypothèques ou par tout autre moyen <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 17 octobre 2013, le préfet des Alpes-Maritimes a déclaré d'utilité publique le projet de création d'un équipement public sur la villa " La Luna ", située sur la promenade des Anglais, à Nice et déclaré cessibles au bénéfice de la commune de Nice les immeubles dont l'acquisition était nécessaire à la réalisation de cette opération ; que, par un jugement du 18 novembre 2014, le tribunal administratif de Nice a annulé l'arrêté du préfet des Alpes-Maritimes en tant qu'il a déclaré cessibles ces immeubles ; que, par un arrêt du 28 novembre 2016, contre lequel la ville de Nice se pourvoit en cassation, la cour administrative d'appel de Marseille a rejeté sa requête tendant à l'annulation de ce jugement ;<br/>
<br/>
              2.	Considérant qu'aux termes de l'article R. 11-19 du code de l'expropriation pour cause d'utilité publique, alors applicable et désormais repris à l'article R. 131-3 du même code : " L'expropriant adresse au préfet, pour être soumis à enquête parcellaire dans chacune des communes où sont situés les immeubles à exproprier : (...) / 2° La liste des propriétaires établie à l'aide d'extraits des documents cadastraux délivrés par le service du cadastre ou à l'aide des renseignements délivrés par le conservateur des hypothèques au vu du fichier immobilier ou par tous autres moyens " ; que l'article R. 11-22 du même code, alors applicable et désormais repris à l'article R. 131-6 de ce code, dispose que : " Notification individuelle du dépôt du dossier à la mairie est faite par l'expropriant, sous pli recommandé avec demande d'avis de réception aux propriétaires figurant sur la liste établie en application de l'article R. 11-19 lorsque leur domicile est connu d'après les renseignements recueillis par l'expropriant (...) ; en cas de domicile inconnu, la notification est faite en double copie au maire qui en fait afficher une, et, le cas échéant, aux locataires et preneurs à bail rural " ; <br/>
<br/>
              3.	Considérant qu'il résulte des dispositions rappelées au point 2 que l'expropriant doit notifier, sous pli recommandé, le dépôt du dossier d'enquête parcellaire aux propriétaires figurant sur la liste mentionnée au 2° de l'article R. 11-19, devenu l'article R. 131-3, et dont le domicile est connu d'après les renseignements qu'il a pu recueillir auprès du service du cadastre ou du conservateur des hypothèques ou par tout autre moyen; que ces dispositions n'imposent pas à l'expropriant de procéder à de nouvelles recherches lorsque l'avis de réception de la notification effectuée au domicile ainsi déterminé ne lui est pas retourné dans le délai normal d'acheminement, l'affichage en mairie se substituant alors régulièrement à la formalité de la notification individuelle ; <br/>
<br/>
              4.	Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le préfet des Alpes-Maritimes a envoyé à l'adresse de Mme B...figurant sur le tableau parcellaire, par lettre recommandée avec demande d'avis de réception, la notification prévue par les dispositions précitées de l'article R. 11-22 du code de l'expropriation pour cause d'utilité publique, alors applicable ; que, l'avis de réception de ce courrier recommandé n'étant pas revenu à l'expéditeur dans le délai normal d'acheminement, une copie de la notification a donné lieu à un affichage en mairie principale et en mairies annexes ; que, par suite, en jugeant que l'affichage en mairie n'avait pu se substituer régulièrement à la formalité de la notification individuelle, la cour administrative d'appel de Marseille a commis une erreur de droit ; que la ville de Nice est dès lors fondée, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, à demander l'annulation de l'arrêt qu'elle attaque ; <br/>
<br/>
              5.	Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de la commune, qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme B...une somme de 3 000 euros au titre de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 28 novembre 2016 est annulé. <br/>
<br/>
Article 2 : L'affaire est renvoyée devant la cour administrative d'appel de Marseille.<br/>
<br/>
Article 3 : Mme B...versera une somme de 3 000 euros à la ville de Nice au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 4 : Les conclusions présentées par Mme B...devant le Conseil d'Etat au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à la ville de Nice, à Mme A...B...et au ministre d'Etat, ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">34-02-01-02 EXPROPRIATION POUR CAUSE D'UTILITÉ PUBLIQUE. RÈGLES GÉNÉRALES DE LA PROCÉDURE NORMALE. ENQUÊTES. ENQUÊTE PARCELLAIRE. - DÉPÔT DU DOSSIER EN MAIRIE PAR L'EXPROPRIANT - NOTIFICATION AUX PROPRIÉTAIRES CONCERNÉS (ART. R. 131-6 DU CODE DE L'EXPROPRIATION POUR CAUSE D'UTILITÉ PUBLIQUE) - NOTIFICATION À PARTIR DE LA LISTE ÉTABLIE DANS LES CONDITIONS POSÉES À L'ARTICLE R. 131-3 DU MÊME CODE - AVIS DE RÉCEPTION DE LA NOTIFICATION NON RETOURNÉ DANS LE DÉLAI NORMAL D'ACHEMINEMENT - OBLIGATION POUR L'EXPROPRIANT DE PROCÉDER À DE NOUVELLES RECHERCHES - ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 34-02-01-02 Il résulte des dispositions des articles R. 11-19 et R. 11-22 du code de l'expropriation pour cause d'utilité publique, désormais repris aux articles R. 131-3 et R. 131-6 du même code que l'expropriant doit notifier, sous pli recommandé, le dépôt du dossier d'enquête parcellaire aux propriétaires figurant sur la liste mentionnée au 2° de l'article R. 11-19 de ce code, devenu son article R. 131-3, et dont le domicile est connu d'après les renseignements qu'il a pu recueillir auprès du service du cadastre ou du conservateur des hypothèques ou par tout autre moyen. Ces dispositions n'imposent pas à l'expropriant de procéder à de nouvelles recherches lorsque l'avis de réception de la notification effectuée au domicile ainsi déterminé ne lui est pas retourné dans le délai normal d'acheminement, l'affichage en mairie se substituant alors régulièrement à la formalité de la notification individuelle.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant d'un courrier non-réclamé, CE, 3 février 2013, Bongue et autres, n° 343164, T. p. 640.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
