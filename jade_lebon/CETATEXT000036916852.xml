<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036916852</ID>
<ANCIEN_ID>JG_L_2018_05_000000408950</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/91/68/CETATEXT000036916852.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème chambres réunies, 16/05/2018, 408950</TITRE>
<DATE_DEC>2018-05-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>408950</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MARLANGE, DE LA BURGADE ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Yves Doutriaux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:408950.20180516</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme E...F..., Mme C...F..., M. A...F..., M. B...F...et Mme D...F...ont demandé au tribunal administratif de Nantes d'annuler l'arrêté du 23 janvier 2012 par lequel le préfet de la Loire-Atlantique a délivré à la société PetT Technologie un permis de construire cinq éoliennes et un poste de livraison sur les terrains situés chemin rural du Bois Gautier et chemin rural des Ecobuts à La Chapelle Glain. Par un jugement n° 1203156 du 30 décembre 2014, le tribunal administratif a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 15NT00706 du 1er février 2017, la cour administrative d'appel de Nantes a, sur appel des mêmes requérants, annulé ce jugement et fait droit à leur demande.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés respectivement les 16 mars et  30 mai 2017 au secrétariat du contentieux du Conseil d'Etat, la société PetT Technologie demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de Mme E...F...et autres ;<br/>
<br/>
              3°) de mettre à la charge des requérants le versement d'une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'urbanisme ;<br/>
              - le code de l'environnement ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yves Doutriaux, conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la société PetT Technologie, et à la SCP Marlange, de La Burgade, avocat de Mme F...et autres ; <br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant que les consorts F...ont demandé au tribunal administratif de Nantes d'annuler l'arrêté du 23 janvier 2012 par lequel le préfet de la Loire-Atlantique a délivré à la société PetT Technologie un permis de construire cinq éoliennes et un poste de livraison sur les terrains situés dans la commune de La Chapelle-Glain ; que, par un jugement du 30 décembre 2014, le tribunal administratif a rejeté leur demande ; que toutefois, par un arrêt du 1er février 2017, la cour administrative d'appel de Nantes a annulé ce jugement et fait droit à leur demande ; que la société PetT Technologie demande l'annulation de cet arrêt ;<br/>
<br/>
              2.	Considérant que, pour écarter la fin de non recevoir opposée par la société PetT Technologie et tirée de ce que les consorts F...ne justifiaient pas d'un intérêt leur donnant qualité pour agir contre le permis litigieux, la cour administrative d'appel de Nantes a relevé qu'il ressortait des pièces du dossier et notamment de l'étude d'impact jointe à la demande de permis que le parc éolien, d'une hauteur totale de 116 mètres, serait visible à partir de la façade ouest du château dont ils sont propriétaires, visibilité qui n'apparaîtrait qu'à partir du deuxième étage de l'édifice et que ce  château était situé à 2,5 kilomètres environ du parc éolien ; qu'en jugeant que, dans ces conditions, les consorts F...disposaient d'un intérêt à agir suffisant pour contester le permis de construire délivré à la société PetT Technologie, la cour a donné aux faits ainsi énoncés une qualification juridique erronée ; que, par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la société PetT Technologie est fondée à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              3.	Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              4.	Considérant qu'il ressort des pièces du dossier, ainsi qu'il a été dit au 2. que la propriété des consorts F...est distante d'environ 2,5 kilomètres des cinq éoliennes prévues dans la demande de permis de construire dont la hauteur totale sera de 116 mètres ; que, même si, selon l'étude d'impact, le parc éolien sera visible à partir du deuxième étage de l'édifice, les consorts F...ne justifient pas, au regard tant de la distance qui sépare le château du site retenu pour l'implantation du projet éolien que de la configuration des lieux, d'un intérêt leur donnant qualité pour agir ; que, par suite, ils n'étaient pas recevables à demander l'annulation de l'arrêté du 23 janvier 2012 par lequel le préfet de la Loire-Atlantique a délivré à la société PetT Technologie un permis de construire cinq éoliennes et un poste de livraison sur un terrain situé à La Chapelle Glain ;<br/>
<br/>
              5.	Considérant qu'il résulte de ce qui précède que les consorts F...ne sont pas fondés à se plaindre de ce que, par le jugement attaqué, le tribunal administratif de Nantes a rejeté leur demande tendant à l'annulation de l'arrêté litigieux ;<br/>
<br/>
              6.	Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société PetT Technologie qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge des consorts F...la somme de 4 000 euros à verser la société PetT Technologie, au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt en date du 1er février 2017 de la cour administrative d'appel de Nantes est annulé.<br/>
<br/>
Article 2 : La requête présentée par les consorts F...devant la cour administrative d'appel de Nantes est rejetée.<br/>
<br/>
Article 3 : Les consorts F...verseront à la société PetT Technologie une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la société PetT Technologie et aux consortsF.... <br/>
Copie en sera adressée au ministre d'Etat, ministre de la transition écologique et solidaire.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">29-035 ENERGIE. - INTÉRÊT À CONTESTER POUR EXCÈS DE POUVOIR UN PERMIS DE CONSTRUIRE DES ÉOLIENNES [RJ1] - ESPÈCE - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-04-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. INTÉRÊT POUR AGIR. ABSENCE D'INTÉRÊT. - INTÉRÊT À CONTESTER POUR EXCÈS DE POUVOIR UN PERMIS DE CONSTRUIRE DES ÉOLIENNES [RJ1] - ESPÈCE - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">68-06-01-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. INTRODUCTION DE L'INSTANCE. INTÉRÊT À AGIR. - INTÉRÊT À CONTESTER POUR EXCÈS DE POUVOIR UN PERMIS DE CONSTRUIRE DES ÉOLIENNES [RJ1] - ESPÈCE - ABSENCE.
</SCT>
<ANA ID="9A"> 29-035 Permis de construire cinq éoliennes et un poste de livraison.... ,,La propriété des requérants est distante de 2,5 kilomètres des cinq éoliennes prévues dans la demande de permis dont la hauteur totale sera de 116 mètres. Même si, selon l'étude d'impact, le parc éolien sera visibile du deuxième étage de l'édifice propriété des requérants, ces derniers ne justifient pas, au regard tant de la distance qui sépare le château du site retenu pour l'implantation du projet éolien que de la configuration des lieux, d'un intérêt leur donnant qualité pour agir.</ANA>
<ANA ID="9B"> 54-01-04-01 Permis de construire cinq éoliennes et un poste de livraison.... ,,La propriété des requérants est distante de 2,5 kilomètres des cinq éoliennes prévues dans la demande de permis dont la hauteur totale sera de 116 mètres. Même si, selon l'étude d'impact, le parc éolien sera visibile du deuxième étage de l'édifice propriété des requérants, ces derniers ne justifient pas, au regard tant de la distance qui sépare le château du site retenu pour l'implantation du projet éolien que de la configuration des lieux, d'un intérêt leur donnant qualité pour agir.</ANA>
<ANA ID="9C"> 68-06-01-02 Permis de construire cinq éoliennes et un poste de livraison.... ,,La propriété des requérants est distante de 2,5 kilomètres des cinq éoliennes prévues dans la demande de permis dont la hauteur totale sera de 116 mètres. Même si, selon l'étude d'impact, le parc éolien sera visibile du deuxième étage de l'édifice propriété des requérants, ces derniers ne justifient pas, au regard tant de la distance qui sépare le château du site retenu pour l'implantation du projet éolien que de la configuration des lieux, d'un intérêt leur donnant qualité pour agir.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 15 avril 2005, Association des citoyens et contribuables de la communauté des communes de Saane-et-Vienne (ACSV) et autres, n° 273398, T. pp. 901-1012-1013-1143.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
