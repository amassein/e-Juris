<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000018006208</ID>
<ANCIEN_ID>JG_L_2007_05_000000277658</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/18/00/62/CETATEXT000018006208.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 16/05/2007, 277658</TITRE>
<DATE_DEC>2007-05-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>277658</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Martin</PRESIDENT>
<AVOCATS>SCP PEIGNOT, GARREAU ; LE PRADO ; DE NERVO</AVOCATS>
<RAPPORTEUR>M. Damien  Botteghi</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Chauvaux</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 16 février et 16 juin 2005 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme Alice A, demeurant ... ; Mme A demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt du 12 octobre 2004 par lequel la cour administrative d'appel de Nantes, saisie par la caisse primaire d'assurance maladie du Nord-Finistère, a réformé le jugement du tribunal administratif de Rennes du 26 septembre 2001 en tant qu'il a déclaré irrecevables ses conclusions tendant à ce que lui soit versée la somme de 187 500 euros en réparation de son préjudice résultant de la paraplégie dont elle est restée atteinte à la suite de quatre ponctions lombaires pratiquées au centre hospitalier universitaire de Brest entre le 16 et le 24 octobre 1996 ; <br/>
<br/>
              2°) de renvoyer l'affaire devant une nouvelle juridiction d'appel et, à titre subsidiaire, statuant au fond, de condamner le centre hospitalier universitaire de Brest à lui verser la somme de 187 500 euros ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 000 euros en application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la sécurité sociale ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Damien Botteghi, Auditeur,<br/>
<br/>
              - les observations de la SCP Peignot, Garreau, avocat de Mme A, de Me Le Prado, avocat du centre hospitalier universitaire de Brest et de Me de Nervo, avocat de la caisse primaire d'assurance maladie du Nord-Finistère, <br/>
<br/>
              - les conclusions de M. Didier Chauvaux, Commissaire du gouvernement ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant que, par un jugement du 26 septembre 2001, le tribunal administratif de Rennes a déclaré le centre hospitalier universitaire de Brest entièrement responsable des conséquences dommageables d'interventions subies par Mme A dans cet établissement en octobre 1996, fait partiellement droit aux conclusions indemnitaires de l'intéressée et rejeté celles de la caisse primaire d'assurance maladie du Nord-Finistère ; que par l'arrêt attaqué du 12 octobre 2004, la cour administrative d'appel de Nantes a fait droit à l'appel de la caisse primaire en condamnant le centre hospitalier à verser à cet organisme une somme de 218 006,30 euros et rejeté l'appel de Mme A au motif qu'il avait été formé plus de deux mois après la notification du jugement à l'intéressée ;<br/>
<br/>
              Considérant qu'aux termes de l'article L. 376-1 du code de la sécurité sociale, dans sa rédaction applicable au litige porté devant les juges du fond : « Lorsque, sans entrer dans les cas régis par les dispositions législatives applicables aux accidents du travail, la lésion dont l'assuré social ou son ayant droit est atteint est imputable à un tiers, l'assuré ou ses ayants droit conserve contre l'auteur de l'accident le droit de demander la réparation du préjudice causé conformément aux règles du droit commun, dans la mesure où ce préjudice n'est pas réparé par application du présent livre. / (...) Si la responsabilité du tiers est entière ou si elle est partagée avec la victime, la caisse est admise à poursuivre le remboursement des prestations mises à sa charge à due concurrence de la part d'indemnité mise à la charge du tiers qui répare l'atteinte à l'intégrité physique de la victime, à l'exclusion de la part d'indemnité, de caractère personnel, correspondant aux souffrances physiques ou morales par elle endurées et au préjudice esthétique et d'agrément.../ (...) La personne victime, les établissements de santé, le tiers responsable et son assureur sont tenus d'informer la caisse de la survenue des lésions causées par un tiers dans des conditions fixées par décret. (...) / L'intéressé ou ses ayants droit doivent indiquer, en tout état de la procédure, la qualité d'assuré social de la victime de l'accident ainsi que les caisses de sécurité sociale auxquelles celle-ci est ou était affiliée pour les divers risques. Ils doivent appeler ces caisses en déclaration de jugement commun. A défaut du respect de l'une de ces obligations, la nullité du jugement sur le fond pourra être demandée pendant deux ans, à compter de la date à partir de laquelle ledit jugement est devenu définitif, soit à la requête du ministère public, soit à la demande des caisses de sécurité sociale intéressées ou du tiers responsable, lorsque ces derniers y auront intérêt » ;<br/>
<br/>
              Considérant qu'eu égard au lien qu'établissent les dispositions de l'article L. 376-1 du code de la sécurité sociale entre la détermination des droits de la victime d'un accident et celle des droits de la caisse de sécurité sociale à laquelle elle est affiliée, quand un jugement ayant statué sur des conclusions indemnitaires de la victime et de la caisse dirigées contre l'auteur de l'accident a fait l'objet d'un appel de la victime introduit dans le délai légal, la caisse peut former appel à tout moment, en reprenant sa demande de première instance, augmentée le cas échéant pour tenir compte des prestations nouvelles servies depuis l'intervention du jugement ; que, de même, l'appel régulièrement exercé par la caisse ouvre à la victime la possibilité de former appel sans condition de délai en reprenant sa demande de première instance, augmentée le cas échéant pour tenir compte d'aggravations des préjudices postérieures au jugement ;<br/>
<br/>
              Considérant qu'en rejetant comme tardif l'appel formé par Mme A contre le jugement du tribunal administratif de Rennes, alors qu'elle avait été saisie dans le délai légal d'un appel de la caisse primaire d'assurance maladie du Nord-Finistère, la cour administrative d'appel de Nantes a commis une erreur de droit ;<br/>
<br/>
              Considérant, toutefois, qu'il ressort des pièces du dossier soumis aux juges du fond que, devant le tribunal administratif, Mme A avait expressément limité ses conclusions à la réparation de ses souffrances et de ses préjudices esthétique et d'agrément, exclus de l'assiette du recours des caisses de sécurité sociale par les dispositions précitées de l'article L. 376-1 du code de la sécurité sociale ; que les conclusions dont elle a saisi la cour administrative d'appel tendaient à la réparation des préjudices physiologiques soumis à ce recours ; que de telles conclusions étaient nouvelles en appel et, par suite, irrecevables ; qu'il y a lieu de substituer ce motif, qui répond à un moyen invoqué par le centre hospitalier de Brest tant devant la cour administrative d'appel que devant le conseil d'Etat et qui n'implique l'appréciation d'aucune circonstance de fait, au motif erroné sur lequel la cour s'est fondée pour rejeter les conclusions de Mme A ;<br/>
<br/>
              Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que les frais exposés par Mme A et non compris dans les dépens soient mis à la charge du centre hospitalier universitaire de Brest, qui n'est pas la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
		Article 1er : La requête de Mme A est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à Mme Alice A, au centre hospitalier universitaire de Brest, à la caisse primaire d'assurance maladie du Nord-Finistère et au ministre de la santé et des solidarités.<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-05-04 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RECOURS OUVERTS AUX DÉBITEURS DE L'INDEMNITÉ, AUX ASSUREURS DE LA VICTIME ET AUX CAISSES DE SÉCURITÉ SOCIALE. DROITS DES CAISSES DE SÉCURITÉ SOCIALE. - CONCLUSIONS DE LA CAISSE TENDANT AU REMBOURSEMENT DES PRESTATIONS VERSÉES PRÉSENTÉES À L'OCCASION DE L'APPEL INTERJETÉ PAR LA VICTIME - RECEVABILITÉ, DÈS LORS QUE LA CAISSE A ÉTÉ RÉGULIÈREMENT MISE EN CAUSE EN PREMIÈRE INSTANCE ET ALORS MÊME QUE CES CONCLUSIONS SONT PRÉSENTÉES APRÈS L'EXPIRATION DU DÉLAI D'APPEL [RJ1] - CONTREPARTIE - RECEVABILITÉ À TOUT MOMENT DE L'APPEL DE LA VICTIME, DANS LES MÊMES CONDITIONS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">62-05 SÉCURITÉ SOCIALE. CONTENTIEUX ET RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. - RECOURS DES CAISSES DE SÉCURITÉ SOCIALE - CONCLUSIONS DE LA CAISSE TENDANT AU REMBOURSEMENT DES PRESTATIONS VERSÉES PRÉSENTÉES À L'OCCASION DE L'APPEL INTERJETÉ PAR LA VICTIME - RECEVABILITÉ, DÈS LORS QUE LA CAISSE A ÉTÉ RÉGULIÈREMENT MISE EN CAUSE EN PREMIÈRE INSTANCE ET ALORS MÊME QUE CES CONCLUSIONS SONT PRÉSENTÉES APRÈS L'EXPIRATION DU DÉLAI D'APPEL [RJ1] - CONTREPARTIE - RECEVABILITÉ À TOUT MOMENT DE L'APPEL DE LA VICTIME, DANS LES MÊMES CONDITIONS.
</SCT>
<ANA ID="9A"> 60-05-04 Eu égard au lien qu'établissent les dispositions de l'article L. 376-1 du code de la sécurité sociale entre la détermination des droits de la victime d'un accident et celle des droits de la caisse de sécurité sociale à laquelle elle est affiliée, quand un jugement ayant statué sur des conclusions indemnitaires de la victime et de la caisse dirigées contre l'auteur de l'accident a fait l'objet d'un appel de la victime introduit dans le délai légal, la caisse peut former appel à tout moment, en reprenant sa demande de première instance, augmentée le cas échéant pour tenir compte des prestations nouvelles servies depuis l'intervention du jugement. De même, l'appel régulièrement exercé par la caisse ouvre à la victime la possibilité de former appel sans condition de délai en reprenant sa demande de première instance, augmentée le cas échéant pour tenir compte d'aggravations des préjudices postérieures au jugement.</ANA>
<ANA ID="9B"> 62-05 Eu égard au lien qu'établissent les dispositions de l'article L. 376-1 du code de la sécurité sociale entre la détermination des droits de la victime d'un accident et celle des droits de la caisse de sécurité sociale à laquelle elle est affiliée, quand un jugement ayant statué sur des conclusions indemnitaires de la victime et de la caisse dirigées contre l'auteur de l'accident a fait l'objet d'un appel de la victime introduit dans le délai légal, la caisse peut former appel à tout moment, en reprenant sa demande de première instance, augmentée le cas échéant pour tenir compte des prestations nouvelles servies depuis l'intervention du jugement. De même, l'appel régulièrement exercé par la caisse ouvre à la victime la possibilité de former appel sans condition de délai en reprenant sa demande de première instance, augmentée le cas échéant pour tenir compte d'aggravations des préjudices postérieures au jugement.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. Section, 1er juillet 2005, S., n° 234403, p. 300 ; 16 mai 2007, Caisse primaire d'assurance maladie d'Angers, n° 285514, à mentionner aux tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
