<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043834073</ID>
<ANCIEN_ID>JG_L_2021_07_000000444784</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/83/40/CETATEXT000043834073.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 20/07/2021, 444784</TITRE>
<DATE_DEC>2021-07-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>444784</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Guillaume Leforestier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:444784.20210720</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une ordonnance n° 1901294 du 18 septembre 2020, enregistrée le 21 septembre au secrétariat du contentieux du Conseil d'Etat, la présidente du tribunal administratif de Montpellier a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête présentée à ce tribunal par M. A... B....<br/>
<br/>
              Par cette requête, enregistrée au greffe du tribunal administratif de Montpellier le 14 mars 2019, et un mémoire en réplique enregistré le 20 janvier 2021 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 26 décembre 2018 par laquelle la ministre des armées lui a infligé un blâme du ministre ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la défense ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le décret n° 2005-850 du 27 juillet 2005 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Leforestier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteure publique ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier que M. B..., capitaine dans la gendarmerie nationale et fondateur de l'association des forces armées réunies dont il assurait la présidence, s'est vu infliger le 26 décembre 2018 par la ministre des armées la sanction de blâme du ministre, à raison de propos qu'il a tenus à plusieurs reprises en 2017 sur le site internet de l'association et sur une plateforme de partage en ligne à l'encontre de différentes autorités politiques, administratives et judiciaires.<br/>
<br/>
              Sur la légalité externe :<br/>
<br/>
              2. En premier lieu, aux termes de l'article 1er du décret du 27 juillet 2005 relatif aux délégations de signature des membres du Gouvernement : " (...) peuvent signer, au nom du ministre ou du secrétaire d'Etat et par délégation, l'ensemble des actes, à l'exception des décrets, relatifs aux affaires des services placés sous leur autorité : / (...) 3° Le chef d'état-major des armées, le délégué général pour l'armement, les chefs d'état-major de l'armée de terre, de la marine et de l'armée de l'air, le chef du contrôle général des armées, le major général des armées, les majors généraux de l'armée de terre, de la marine, de l'armée de l'air et de la gendarmerie et les sous-chefs de l'état-major des armées (...) ". Le major général de la gendarmerie nationale tenait de ces dispositions compétence pour signer, au nom de la ministre des armées, la décision de sanction en litige.<br/>
<br/>
              3. En deuxième lieu, aucune disposition législative ou réglementaire ne subordonne la légalité d'une sanction disciplinaire prononcée à l'encontre d'un militaire à l'apposition du sceau de la République française sur la décision prise.<br/>
<br/>
              4. En troisième lieu, la circonstance que le formulaire utilisé par l'administration pour la procédure disciplinaire ait comporté une référence à une instruction du ministre de la défense du 30 mai 2006 abrogée par une instruction du 12 juin 2014 est, par elle-même, sans incidence sur la régularité de la décision attaquée.<br/>
<br/>
              5. En quatrième lieu, la décision de sanction litigieuse comporte l'énoncé des motifs de droit et de fait qui en constituent le fondement. Par suite, elle est suffisamment motivée au regard des exigences de l'article L. 211-2 du code des relations entre le public et l'administration.<br/>
<br/>
              6. En cinquième lieu, aux termes de l'article R. 4137-15 du code de la défense: " Avant qu'une sanction ne lui soit infligée, le militaire a le droit de s'expliquer oralement ou par écrit, seul ou accompagné d'un militaire en activité de son choix sur les faits qui lui sont reprochés devant l'autorité militaire de premier niveau dont il relève. Au préalable, un délai de réflexion, qui ne peut être inférieur à un jour franc, lui est laissé pour organiser sa défense. / Lorsque la demande de sanction est transmise à une autorité militaire supérieure à l'autorité militaire de premier niveau, le militaire en cause peut également s'expliquer par écrit sur ces faits auprès de cette autorité supérieure. L'explication écrite de l'intéressé ou la renonciation écrite à l'exercice du droit de s'expliquer par écrit est jointe au dossier transmis à l'autorité militaire supérieure. / Avant d'être reçu par l'autorité militaire de premier niveau dont il relève, le militaire a connaissance de l'ensemble des pièces et documents au vu desquels il est envisagé de le sanctionner ". Si M. B... soutient qu'il n'a pas eu connaissance des avis de l'autorité militaire de premier niveau et de celle de deuxième niveau, visés par la décision contestée, il ressort des pièces du dossier que ces avis ne comportent aucun élément nouveau et se bornent à transmettre la demande de sanction à l'autorité militaire supérieure compétente pour prendre la sanction susceptible d'être infligée à l'intéressé, compte tenu de la nature des faits ou du comportement qui lui sont reprochés. Aucune disposition du code de la défense n'imposant leur communication au militaire, non plus d'ailleurs qu'un nouvel entretien avec l'autorité de premier niveau, le moyen tiré de ce qu'il n'aurait pas obtenu communication de l'ensemble des pièces de la procédure disciplinaire ne peut qu'être écarté. <br/>
<br/>
              7. En sixième lieu, il ressort des pièces du dossier que l'autorité militaire de premier niveau a invité à plusieurs reprises M. B..., placé en congé de longue durée pour maladie du 11 juillet 2017 au 11 janvier 2019, à proposer des dates pour la tenue d'un entretien et à lui faire parvenir ses observations en défense. Il ne ressort pas des pièces du dossier que l'état de santé de M. B... l'aurait privé de la faculté de présenter des observations écrites en application du premier alinéa de l'article R. 4137-15 du code de la défense. Par suite, le moyen tiré de ce que la sanction litigieuse aurait été prise à l'issue d'une procédure irrégulière ne peut qu'être écarté.<br/>
<br/>
              Sur la légalité interne :<br/>
<br/>
              8. En premier lieu, aux termes de l'article L. 4121-2 du code de la défense : " Les opinions ou croyances, notamment philosophiques, religieuses ou politiques, sont libres. / Elles ne peuvent cependant être exprimées qu'en dehors du service et avec la réserve exigée par l'état militaire. Cette règle s'applique à tous les moyens d'expression (...) / Indépendamment des dispositions du code pénal relatives à la violation du secret de la défense nationale et du secret professionnel, les militaires doivent faire preuve de discrétion pour tous les faits, informations ou documents dont ils ont connaissance dans l'exercice ou à l'occasion de l'exercice de leurs fonctions. En dehors des cas expressément prévus par la loi, les militaires ne peuvent être déliés de cette obligation que par décision expresse de l'autorité dont ils dépendent (...) ". Aux termes de l'article L. 4126-4 du même code : " Sans préjudice de l'article L. 4121-2, les membres des associations professionnelles nationales de militaires jouissent des garanties indispensables à leur liberté d'expression pour les questions relevant de la condition militaire ". <br/>
<br/>
              9. Si, en vertu de ces dispositions, les membres des associations professionnelles nationales de militaires peuvent exprimer des positions publiques sur les questions relevant de la condition militaire, les propos qu'ils tiennent publiquement ne sauraient excéder les limites que les militaires doivent respecter en raison de la réserve à laquelle ils sont tenus à l'égard des autorités publiques. En particulier, la circonstance qu'il soit membre d'une association professionnelle nationale de militaires ne saurait permettre à un militaire de tenir des propos diffamatoires ou outranciers à l'égard de cadres de l'armée ou des appréciations sur l'action d'autres autorités publiques. <br/>
<br/>
              10. Il ressort des pièces du dossier que M. B... a tenu et publié sur des sites internet, à de nombreuses reprises, des propos outranciers à l'égard de plusieurs cadres de l'armée, notamment le directeur général de la gendarmerie nationale, et qu'il a également tenu des propos déplacés sur l'action du Président de la République. La circonstance alléguée par le requérant qu'il serait président d'une association professionnelle nationale de militaires ne saurait, en tout état de cause, justifier l'expression de tels propos. Il s'ensuit que les faits reprochés à l'intéressé étaient de nature à justifier le prononcé d'une sanction disciplinaire.<br/>
<br/>
              11. Aux termes de l'article L. 4137-1 du code de la défense : " Sans préjudice des sanctions pénales qu'ils peuvent entraîner, les fautes ou manquements commis par les militaires les exposent : / 1° A des sanctions disciplinaires prévues à l'article L. 4137-2 (...) ". Aux termes de l'article L. 4137-2 du même code : " Les sanctions disciplinaires applicables aux militaires sont réparties en trois groupes : / 1° Les sanctions du premier groupe sont : / a) L'avertissement ; / b) La consigne ; / c) La réprimande ; / d) Le blâme ; / e) Les arrêts ; / f) Le blâme du ministre ". Eu égard à la nature des propos tenus publiquement et à leur répétition, l'autorité disciplinaire n'a pas pris une sanction disproportionnée en infligeant à M. B... un blâme du ministre.<br/>
<br/>
<br/>
<br/>
<br/>
              12. Il résulte de tout ce qui précède que M. B... n'est pas fondé à demander l'annulation pour excès de pouvoir de la décision qu'il attaque. Ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, en conséquence, qu'être rejetées.<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : La requête de M. B... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. A... B... et à la ministre des armées.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">08-01-01-08-03 ARMÉES ET DÉFENSE. PERSONNELS MILITAIRES ET CIVILS DE LA DÉFENSE. QUESTIONS COMMUNES À L'ENSEMBLE DES PERSONNELS MILITAIRES. - MILITAIRE MEMBRE D'UNE APNM (ART. L. 4216-4 DU CODE DE LA DÉFENSE) - PROPOS NE POUVANT EXCÉDER LES LIMITES QU'IMPOSE AUX MILITAIRES LEUR DEVOIR DE RÉSERVE (ART. L. 4121-2) [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-07-11-01 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. OBLIGATIONS DES FONCTIONNAIRES. DEVOIR DE RÉSERVE. - MILITAIRE MEMBRE D'UNE APNM (ART. L. 4216-4 DU CODE DE LA DÉFENSE) - PROPOS NE POUVANT EXCÉDER LES LIMITES QU'IMPOSE AUX MILITAIRES LEUR DEVOIR DE RÉSERVE (ART. L. 4121-2) [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">36-09-03-01 FONCTIONNAIRES ET AGENTS PUBLICS. DISCIPLINE. MOTIFS. FAITS DE NATURE À JUSTIFIER UNE SANCTION. - MILITAIRE MEMBRE D'UNE APNM (ART. L. 4216-4 DU CODE DE LA DÉFENSE) - PROPOS DIFFAMATOIRES OU OUTRANCIERS À L'ÉGARD DE CADRES DE L'ARMÉE OU DES APPRÉCIATIONS SUR L'ACTION D'AUTRES AUTORITÉS PUBLIQUES [RJ1].
</SCT>
<ANA ID="9A"> 08-01-01-08-03 Si, en vertu des articles L. 4121-2 et L. 4126-4 du code de la défense, les membres des associations professionnelles nationales de militaires (APNM) peuvent exprimer des positions publiques sur les questions relevant de la condition militaire, les propos qu'ils tiennent publiquement ne sauraient excéder les limites que les militaires doivent respecter en raison de la réserve à laquelle ils sont tenus à l'égard des autorités publiques.... ,,En particulier, la circonstance qu'il soit membre d'une association professionnelle nationale de militaires ne saurait permettre à un militaire de tenir des propos diffamatoires ou outranciers à l'égard de cadres de l'armée ou des appréciations sur l'action d'autres autorités publiques. De tels propos sont ainsi de nature à justifier le prononcé d'une sanction disciplinaire.</ANA>
<ANA ID="9B"> 36-07-11-01 Si, en vertu des articles L. 4121-2 et L. 4126-4 du code de la défense, les membres des associations professionnelles nationales de militaires (APNM) peuvent exprimer des positions publiques sur les questions relevant de la condition militaire, les propos qu'ils tiennent publiquement ne sauraient excéder les limites que les militaires doivent respecter en raison de la réserve à laquelle ils sont tenus à l'égard des autorités publiques.,,,En particulier, la circonstance qu'il soit membre d'une association professionnelle nationale de militaires ne saurait permettre à un militaire de tenir des propos diffamatoires ou outranciers à l'égard de cadres de l'armée ou des appréciations sur l'action d'autres autorités publiques. De tels propos sont ainsi de nature à justifier le prononcé d'une sanction disciplinaire.</ANA>
<ANA ID="9C"> 36-09-03-01 Si, en vertu des articles L. 4121-2 et L. 4126-4 du code de la défense, les membres des associations professionnelles nationales de militaires (APNM) peuvent exprimer des positions publiques sur les questions relevant de la condition militaire, les propos qu'ils tiennent publiquement ne sauraient excéder les limites que les militaires doivent respecter en raison de la réserve à laquelle ils sont tenus à l'égard des autorités publiques.,,,En particulier, la circonstance qu'il soit membre d'une association professionnelle nationale de militaires ne saurait permettre à un militaire de tenir des propos diffamatoires ou outranciers à l'égard de cadres de l'armée ou des appréciations sur l'action d'autres autorités publiques. De tels propos sont ainsi de nature à justifier le prononcé d'une sanction disciplinaire.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant du devoir de réserve d'un fonctionnaire de police représentant syndical, CE, 23 avril 1997, M.,, n° 144038, T. pp. 901-906-969.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
