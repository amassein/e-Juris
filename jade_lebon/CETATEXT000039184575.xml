<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039184575</ID>
<ANCIEN_ID>JG_L_2019_10_000000405992</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/18/45/CETATEXT000039184575.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 04/10/2019, 405992</TITRE>
<DATE_DEC>2019-10-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>405992</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE ; SCP RICHARD ; SCP LEVIS</AVOCATS>
<RAPPORTEUR>M. Olivier  Fuchs</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Raphaël Chambon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:405992.20191004</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. A...-E... C... a porté plainte contre M. D... B... devant la chambre disciplinaire de première instance de Provence-Alpes-Côte d'Azur et Corse de l'ordre des médecins. Par une décision du 31 août 2009, la chambre disciplinaire de première instance a rejeté cette plainte. <br/>
<br/>
              Par une ordonnance du 6 juin 2011, le président de la chambre disciplinaire nationale de l'ordre des médecins a rejeté l'appel formé par M. C... contre cette décision.<br/>
<br/>
              Par une décision n° 354273 du 23 octobre 2013, le Conseil d'Etat, statuant au contentieux a, sur le pourvoi de M. C..., annulé cette ordonnance et renvoyé l'affaire à la chambre disciplinaire nationale de l'ordre des médecins. <br/>
<br/>
              Par une décision du 19 juillet 2016, la chambre disciplinaire nationale de l'ordre des médecins a rejeté l'appel formé par M. C... contre la décision de la chambre disciplinaire de première instance. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 15 décembre 2016 et 15 mars 2017 au secrétariat du contentieux du Conseil d'Etat, M. C... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge du Conseil national de l'ordre des médecins la somme de 3 800 euros à verser à la SCP Lévis, son avocat, au titre des articles 37 de la loi du 10 juillet 1991 et L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - le code de la santé publique ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le décret n° 91-1266 du 19 décembre 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Fuchs, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lévis, avocat de M. A...-françois C... et à la SCP Richard, avocat de M. D... B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces soumises aux juges du fond que, par une décision du 31 août 2009, la chambre disciplinaire de première instance de Provence-Alpes-Côte d'Azur et Corse de l'ordre des médecins a rejeté la plainte déposée par M. C... contre M. B..., médecin qualifié spécialiste en psychiatrie. Par une décision du 19 juillet 2016 contre laquelle M. C... se pourvoit en cassation, la chambre disciplinaire nationale de l'ordre des médecins a rejeté l'appel formé par ce dernier contre la décision de première instance. <br/>
<br/>
              2. En premier lieu, aux termes de l'article L. 4126-2 du code de la santé publique : " Les parties (...) peuvent exercer devant les instances disciplinaires le droit de récusation mentionné à l'article L. 721-1 du code de justice administrative ". Aux termes de l'article R. 4126-24 du même code : " Les articles R. 721-2 à R. 721-9 du code de justice administrative relatifs à l'abstention et à la récusation sont applicables devant les chambres disciplinaires de première instance et nationales ". Aux termes de l'article R. 721-4 du code de justice administrative : " La demande de récusation est formée par acte remis au greffe de la juridiction (...). / La demande doit, à peine d'irrecevabilité, indiquer avec précision les motifs de la récusation et être accompagnée des pièces propres à la justifier (...) ". Aux termes de l'article R. 721-9 du même code : " Si le membre de la juridiction qui est récusé acquiesce à la demande de récusation, il est aussitôt remplacé. / Dans le cas contraire, la juridiction, par une décision non motivée, se prononce sur la demande (...) / La juridiction statue sans la participation de celui de ses membres dont la récusation est demandée (...) ".<br/>
<br/>
              3. Il ressort des pièces du dossier de la procédure suivie devant les juges du fond que, par une décision du 4 avril 2016, la chambre disciplinaire nationale de l'ordre des médecins, statuant dans une formation composée conformément aux exigences découlant du troisième alinéa de l'article R. 721-9 du code de justice administrative, a jugé que M. C... ne justifiait d'aucune cause légitime de nature à entraîner la récusation du président de la formation de jugement et de cinq autres de ses membres, mis en cause par le requérant. Par un acte du 6 juin 2016, et alors que l'affaire devait être appelée à l'audience du 9 juin suivant devant une nouvelle formation de jugement, le requérant a formé une nouvelle demande de récusation visant le président de la chambre disciplinaire nationale et quatre de ses membres, dont trois des assesseurs de la formation de jugement appelée à statuer sur son appel. Par sa décision du 19 juillet 2016, la chambre disciplinaire nationale, statuant en présence de certains de ceux de ses membres dont la récusation était demandée, a rejeté cette nouvelle demande de récusation comme irrecevable, faute pour elle d'être assortie d'un motif de récusation puis a rejeté la requête d'appel de M. C....<br/>
<br/>
              4. Il ressort des termes mêmes de la décision attaquée que la chambre disciplinaire nationale a relevé que la nouvelle demande de récusation présentée le 6 juin 2016 par M. C... se bornait à rappeler les textes en vigueur et la procédure engagée au titre de l'aide juridictionnelle et à indiquer que la demande de récusation " motivée et accompagnée des pièces et arrêts du Conseil d'Etat propres à la justifier sera exercée par l'avocat qui sera désigné au titre de l'aide juridictionnelle ". En jugeant que la demande ainsi formulée ne comportait " aucun motif " de récusation au sens de l'article R. 721-4 du code de justice administrative, pour en déduire que la juridiction n'était pas valablement saisie d'une demande de récusation, la chambre disciplinaire nationale ne s'est pas méprise sur la portée des écritures de M. C.... Il ne résulte, par ailleurs, d'aucun texte ni d'aucun principe ou règle générale de procédure que la chambre disciplinaire nationale aurait dû, à peine d'irrégularité de sa décision, inviter le requérant à régulariser sa demande de récusation. Dans ces conditions, la chambre disciplinaire nationale a pu, sans irrégularité, écarter cette demande dans la même formation de jugement que celle qui a statué sur la requête d'appel de M. C.... <br/>
<br/>
              5. En deuxième lieu, il ressort des pièces du dossier des juges du fond que, par une décision du bureau d'aide juridictionnelle du tribunal de grande instance de Paris du 25 novembre 2010, le bénéfice de l'aide juridictionnelle a été accordé à M. C... au titre de l'appel qu'il avait formé contre la décision de la chambre disciplinaire de première instance de Provence-Alpes-Côte d'Azur et Corse de l'ordre des médecins du 31 août 2009. Un premier avocat, désigné à ce titre, a présenté un mémoire d'appel, enregistré le 24 décembre 2013. Par un courrier du 28 juillet 2014, le bâtonnier de Paris a, en réponse à une demande de M. C... tendant à ce que le nouvel avocat qui avait été désigné, en remplacement du premier, pour l'assister dans cette instance soit à son tour déchargé, indiqué au requérant qu'il n'invoquait aucun motif légitime de nature à justifier qu'il soit fait droit à cette demande et l'a rejetée. Par suite, en refusant, dans ces circonstances, de différer l'examen de la demande présentée par le requérant le 6 juin 2016 afin de lui permettre de tenter d'obtenir la désignation d'un nouvel avocat au titre de l'aide juridictionnelle, la chambre disciplinaire nationale n'a pas entaché sa décision d'irrégularité, ni méconnu le droit du requérant à un recours effectif devant une juridiction tel que garanti par la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, ni commis une erreur de droit ou dénaturé les pièces du dossier qui lui était soumis. <br/>
<br/>
              6. En troisième lieu, aux termes de l'article L. 1110-4 du code de la santé publique, dans sa version alors en vigueur : " Toute personne prise en charge par un professionnel, un établissement, un réseau de santé ou tout autre organisme participant à la prévention et aux soins a droit au respect de sa vie privée et du secret des informations la concernant. / Excepté dans les cas de dérogation, expressément prévus par la loi, ce secret couvre l'ensemble des informations concernant la personne venues à la connaissance du professionnel de santé (...). Il s'impose à tout professionnel de santé, ainsi qu'à tous les professionnels intervenant dans le système de santé (...) ". Aux termes de l'article L. 3213-1 du même code : " I.-Le représentant de l'Etat dans le département prononce par arrêté, au vu d'un certificat médical circonstancié ne pouvant émaner d'un psychiatre exerçant dans l'établissement d'accueil, l'admission en soins psychiatriques des personnes dont les troubles mentaux nécessitent des soins et compromettent la sûreté des personnes ou portent atteinte, de façon grave, à l'ordre public (...) ". Aux termes de l'article L. 3213-2 du même code, dans sa version alors en vigueur : " En cas de danger imminent pour la sûreté des personnes, attesté par un avis médical ", le maire et, à Paris, les commissaires de police arrêtent, à l'égard des personnes dont le comportement révèle des troubles mentaux manifestes, toutes les mesures provisoires nécessaires. <br/>
<br/>
              7. Il résulte des énonciations de la décision attaquée que M. B... a adressé aux autorités de police de Marseille le certificat médical qu'il avait rédigé en vue du prononcé, par le préfet des Bouches-du-Rhône, de l'hospitalisation sans son consentement de M. C..., sur le fondement de l'article L. 3213-1 du code de la santé publique, et que ce certificat a été ultérieurement transmis par les autorités de police au maire de Marseille en vue du prononcé de la mesure prévue à l'article L. 3213-2 du code de la santé publique. En jugeant que M. B... n'avait pas méconnu le droit de M. C... au respect du secret des informations le concernant ainsi que l'obligation de secret professionnel qui lui incombe, en remettant aux autorités de police le certificat médical circonstancié prévu par les dispositions législatives précédemment citées du code de la santé publique, la chambre disciplinaire nationale a exactement qualifié les faits qui lui étaient soumis et n'a pas commis d'erreur de droit. <br/>
<br/>
              8. Il résulte de tout ce qui précède que M. C... n'est pas fondé à demander l'annulation de la décision de la chambre disciplinaire nationale de l'ordre des médecins qu'il attaque. <br/>
<br/>
              9. Les dispositions de l'article L. 761-1 du code de justice administrative font, par suite, obstacle à ce qu'il soit fait droit aux conclusions présentées sur le fondement de l'article 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. C... est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. A...-E... C... et à M. D... B....<br/>
Copie en sera adressée au Conseil national de l'ordre des médecins.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-03-10 DROITS CIVILS ET INDIVIDUELS. LIBERTÉS PUBLIQUES ET LIBERTÉS DE LA PERSONNE. SECRET DE LA VIE PRIVÉE. - SECRET MÉDICAL (ART. L. 1110-4 DU CSP) - ABSENCE DE VIOLATION - PSYCHIATRE TRANSMETTANT AUX AUTORITÉS DE POLICE LE CERTIFICAT RÉDIGÉ EN VUE D'UNE HOSPITALISATION D'OFFICE (ART. L. 3213-1 DU CSP) [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">49-05-01-01 POLICE. POLICES SPÉCIALES. POLICE DES ALIÉNÉS (VOIR AUSSI : SANTÉ PUBLIQUE). PLACEMENT D'OFFICE. - PSYCHIATRE TRANSMETTANT AUX AUTORITÉS DE POLICE LE CERTIFICAT RÉDIGÉ EN VUE D'UNE HOSPITALISATION D'OFFICE (ART. L. 3213-1 DU CSP) - VIOLATION DU SECRET MÉDICAL - ABSENCE [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">55-03-01-02 PROFESSIONS, CHARGES ET OFFICES. CONDITIONS D'EXERCICE DES PROFESSIONS. MÉDECINS. RÈGLES DIVERSES S'IMPOSANT AUX MÉDECINS DANS L'EXERCICE DE LEUR PROFESSION. - SECRET MÉDICAL (ART. L. 1110-4 DU CSP) - ABSENCE DE VIOLATION - PSYCHIATRE TRANSMETTANT AUX AUTORITÉS DE POLICE LE CERTIFICAT RÉDIGÉ EN VUE D'UNE HOSPITALISATION D'OFFICE (ART. L. 3213-1 DU CSP) [RJ1].
</SCT>
<ANA ID="9A"> 26-03-10 Ne méconnaît pas l'obligation de secret professionnel qui lui incombe ni le droit du patient au respect du secret des informations le concernant le psychiatre qui transmet aux autorités de police le certificat médical qu'il a rédigé en vue du prononcé, par le préfet, d'une hospitalisation sans consentement sur le fondement de l'article L. 3213-1 du code de la santé publique (CSP).</ANA>
<ANA ID="9B"> 49-05-01-01 Ne méconnaît pas l'obligation de secret professionnel qui lui incombe ni le droit du patient au respect du secret des informations le concernant le psychiatre qui transmet aux autorités de police le certificat médical qu'il a rédigé en vue du prononcé, par le préfet, d'une hospitalisation sans consentement sur le fondement de l'article L. 3213-1 du code de la santé publique (CSP).</ANA>
<ANA ID="9C"> 55-03-01-02 Ne méconnaît pas l'obligation de secret professionnel qui lui incombe ni le droit du patient au respect du secret des informations le concernant le psychiatre qui transmet aux autorités de police le certificat médical qu'il a rédigé en vue du prononcé, par le préfet, d'une hospitalisation sans consentement sur le fondement de l'article L. 3213-1 du code de la santé publique (CSP).</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., sur la possibilité d'apporter des restrictions au secret médical lorsqu'elles sont les conséquences nécessaires de la loi, CE, 8 février 1989, Conseil national de l'ordre des médecins et autres, n°s 54494 54678 54679 54812 54813, T. p. 462-937-950.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
