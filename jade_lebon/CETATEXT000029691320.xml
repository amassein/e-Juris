<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029691320</ID>
<ANCIEN_ID>JG_L_2014_11_000000373362</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/69/13/CETATEXT000029691320.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 03/11/2014, 373362, Publié au recueil Lebon</TITRE>
<DATE_DEC>2014-11-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>373362</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUIN-PALAT, BOUCARD</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Nuttens</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:373362.20141103</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 19 novembre 2013 et 19 février 2014 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune de Belleville-sur-Loire, représentée par son maire ; la commune de Belleville-sur-Loire demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 12NT01553 du 19 septembre 2013 par lequel la cour administrative d'appel de Nantes a rejeté sa requête tendant à l'annulation du jugement n° 1104148 du 5 avril 2012 par lequel le tribunal administratif d'Orléans a, sur déféré du préfet du Cher, annulé les contrats qu'elle avait conclus le 31 mai 2011 avec la société Milan Paysages pour la gestion et l'entretien des espaces verts communaux ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 5 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des marchés publics ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Nuttens, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouin-Palat, Boucard, avocat de la commune de Belleville-sur-Loire ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la commune de Belleville-sur-Loire a publié en 2011 un avis d'appel public à la concurrence en vue de l'attribution, selon la procédure de l'appel d'offres ouvert, d'un marché à bons de commande divisé en quatre lots pour l'entretien de ses espaces verts ; que les quatre lots ont été attribués à la société Milan Paysages ; que la commune de Belleville-sur Loire se pourvoit en cassation contre l'arrêt par lequel la cour administrative d'appel de Nantes a rejeté sa demande tendant à l'annulation du jugement par lequel le tribunal administratif d'Orléans a annulé les quatre contrats conclus avec la société Milan Paysages ;<br/>
<br/>
              2. Considérant, en premier lieu, que le pouvoir adjudicateur définit librement la méthode de notation pour la mise en oeuvre de chacun des critères de sélection des offres qu'il a définis et rendus publics ; que, toutefois, ces méthodes de notation sont entachées d'irrégularité si, en méconnaissance des principes fondamentaux d'égalité de traitement des candidats et de transparence des procédures, elles sont par elles-mêmes de nature à  priver de leur portée les critères de sélection ou à neutraliser leur pondération et sont, de ce fait, susceptibles de conduire, pour la mise en oeuvre de chaque critère, à ce que la meilleure note ne soit pas attribuée à la meilleure offre, ou, au regard de l'ensemble des critères pondérés, à ce que l'offre économiquement la plus avantageuse ne soit pas choisie ; qu'il en va ainsi alors même que le pouvoir adjudicateur, qui n'y est pas tenu, aurait rendu publiques, dans l'avis d'appel à concurrence ou les documents de la consultation, de telles méthodes de notation ; <br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que les critères d'attribution des quatre lots du marché litigieux étaient, pour deux d'entre eux, le prix et la valeur technique, et, pour les deux autres, le prix, la valeur technique et les délais d'exécution ; que le règlement de la consultation prévoyait que, pour la mise en oeuvre du critère du prix, chaque offre serait notée en fonction de son prix (P) et du prix de l'offre la plus basse (P0) selon la formule : 10/3 x (7 - P/P0) ; qu'en relevant qu'une telle méthode de notation avait pour effet de neutraliser les écarts entre les prix de sorte que les offres ne pouvaient être différenciées qu'au regard des autres critères de sélection et qu'elle était ainsi susceptible de conduire à ce que l'offre économiquement la plus avantageuse ne soit pas choisie et en déduisant que cette méthode était entachée d'irrégularité, la cour n'a commis aucune erreur de droit ;<br/>
<br/>
              4. Considérant, en second lieu, que si la commune de Belleville-sur-Loire fait valoir que la cour aurait commis une erreur de droit en tirant des conséquences de l'irrégularité qu'elle avait relevée pour l'ensemble des lots, un tel moyen ne peut qu'être écarté, dès lors qu'il ressort des pièces du dossier qui lui était soumis que la commune avait retenu un critère de prix pondéré à hauteur de 50 % pour les quatre lots ; <br/>
<br/>
<br/>
              5. Considérant qu'il résulte de tout ce qui précède que le pourvoi de la commune de Belleville-sur-Loire, y compris les conclusions qu'elle présente au titre de l'article L. 761-1 du code de justice administrative, doit être rejeté ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la commune de Belleville-sur-Loire est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la commune de Belleville-sur-Loire.<br/>
Copie en sera adressée pour information au ministre de l'intérieur et à la société Milan Paysages. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-02-005 MARCHÉS ET CONTRATS ADMINISTRATIFS. FORMATION DES CONTRATS ET MARCHÉS. FORMALITÉS DE PUBLICITÉ ET DE MISE EN CONCURRENCE. - MÉTHODE DE NOTATION - RÉGULARITÉ - 1) PRINCIPES - A) LIBERTÉ DU POUVOIR ADJUDICATEUR POUR DÉFINIR LA MÉTHODE DE NOTATION DE CHAQUE CRITÈRE DE SÉLECTION - EXISTENCE - B) LIMITES À CETTE LIBERTÉ - IRRÉGULARITÉ D'UNE MÉTHODE DE NATURE À PRIVER DE LEUR PORTÉE LES CRITÈRES OU À NEUTRALISER LEUR PONDÉRATION - EXISTENCE - IRRÉGULARITÉ SUSCEPTIBLE D'ÊTRE NEUTRALISÉE PAR LA PUBLICITÉ DONNÉE À CETTE MÉTHODE - ABSENCE - 2) ESPÈCE.
</SCT>
<ANA ID="9A"> 39-02-005 1) a) Le pouvoir adjudicateur définit librement la méthode de notation pour la mise en oeuvre de chacun des critères de sélection des offres qu'il a définis et rendus publics.... ,,b) Toutefois, ces méthodes de notation sont entachées d'irrégularité si, en méconnaissance des principes fondamentaux d'égalité de traitement des candidats et de transparence des procédures, elles sont par elles-mêmes de nature à priver de leur portée les critères de sélection ou à neutraliser leur pondération et sont, de ce fait, susceptibles de conduire, pour la mise en oeuvre de chaque critère, à ce que la meilleure note ne soit pas attribuée à la meilleure offre, ou, au regard de l'ensemble des critères pondérés, à ce que l'offre économiquement la plus avantageuse ne soit pas choisie. Il en va ainsi alors même que le pouvoir adjudicateur, qui n'y est pas tenu, aurait rendu publiques, dans l'avis d'appel à concurrence ou les documents de la consultation, de telles méthodes de notation.,,,2) Marché comprenant quatre lots dont les critères d'attribution sont, pour deux d'entre eux, le prix et la valeur technique, et, pour les deux autres, le prix, la valeur technique et les délais d'exécution. Le règlement de la consultation prévoit que, pour la mise en oeuvre du critère du prix, chaque offre serait notée en fonction de son prix (P) et du prix de l'offre la plus basse (P0) selon la formule : 10/3 x (7 - P/P0).,,Une telle méthode de notation a pour effet de neutraliser les écarts entre les prix de sorte que les offres ne pouvaient être différenciées qu'au regard des autres critères de sélection. Elle est ainsi susceptible de conduire à ce que l'offre économiquement la plus avantageuse ne soit pas choisie et est, par suite, entachée d'irrégularité.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
