<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042612686</ID>
<ANCIEN_ID>JG_L_2020_12_000000427860</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/61/26/CETATEXT000042612686.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 03/12/2020, 427860</TITRE>
<DATE_DEC>2020-12-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>427860</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Catherine Brouard-Gallet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:427860.20201203</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante : <br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés le 11 février 2019 et le 16 mars 2020 au secrétariat du contentieux du Conseil d'Etat, la fédération de la plasturgie et des composites demande au Conseil d'Etat d'annuler pour excès de pouvoir l'arrêté du 17 août 2018 de la ministre du travail portant extension de l'accord du 8 mars 2017 relatif à la modification du chapitre IV de l'annexe V sur les équipes de suppléance conclu dans le cadre de la convention collective nationale de la plasturgie, en tant que cet arrêté dispose que le cinquième alinéa de l'article 1-1 du chapitre IV de cette annexe est étendu " sous réserve de l'application des dispositions relatives au temps partiel aux salariés travaillant en équipe de suppléance, dès lors que le travail effectué est à temps partiel au sens de l'article L. 3123-1 du code du travail ".<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - l'ordonnance n° 2017-1718 du 20 décembre 2017 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Catherine Brouard-Gallet, conseiller d'Etat en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier qu'a été conclu, le 8 mars 2017, un accord relatif aux " Equipes de suppléance " modifiant l'annexe V du 13 octobre 1995 de la convention collective nationale de la plasturgie du 1er juillet 1960 étendue par un arrêté du 14 mai 1962. Selon son article 1er, cet accord a pour objet de permettre aux entreprises n'ayant pas un accord relatif aux équipes de suppléance de pouvoir accéder directement à ce dispositif en appliquant ses dispositions sans remettre en cause les accords d'entreprises ou d'établissements. Le cinquième alinéa de l'article 1-1 du chapitre IV de l'annexe V de la convention collective nationale de la plasturgie consacré à la définition des équipes de suppléance, dans sa rédaction issue de cet accord, stipule que " les salariés en équipes de suppléance sont assimilés à des salariés à temps plein, notamment pour le décompte des effectifs, et bénéficient des mêmes droits que les salariés travaillant en équipes de semaine ".<br/>
<br/>
              2. Par arrêté du 17 août 2018, la ministre du travail a procédé à l'extension de cet accord, sous réserve, en ce qui concerne le cinquième alinéa de l'article 1-1 du chapitre IV de l'annexe V de la convention collective nationale de la plasturgie, " de l'application des dispositions relatives au temps partiel aux salariés travaillant en équipe de suppléance, dès lors que le travail effectué est à temps partiel au sens de l'article L. 3123-1 du code du travail ". La fédération de la plasturgie et des composites demande l'annulation pour excès de pouvoir de cet arrêté en tant qu'il procède à l'extension de l'accord sous cette réserve. <br/>
<br/>
              3. En premier lieu, d'une part, aux termes, de l'article L. 3132-16 du code du travail dans sa version résultant de l'ordonnance du 20 décembre 2017 visant à compléter et mettre en cohérence les dispositions prises en application de la loi n° 2017-1340 du 15 septembre 2017 d'habilitation à prendre par ordonnances les mesures pour le renforcement du dialogue social : " Dans les industries ou les entreprises industrielles, une convention ou un accord d'entreprise ou d'établissement ou, à défaut, une convention ou un accord collectif de travail étendu peut prévoir que le personnel d'exécution fonctionne en deux groupes dont l'un, dénommé équipe de suppléance, a pour seule fonction de remplacer l'autre pendant le ou les jours de repos accordés au premier groupe. / Le repos hebdomadaire des salariés de l'équipe de suppléance est attribué un autre jour que le dimanche. / Cette dérogation s'applique également au personnel nécessaire à l'encadrement de cette équipe ". Aux termes de l'article R. 3132-11 du code du travail : " La durée quotidienne du travail des salariés affectés aux équipes de suppléance peut atteindre douze heures lorsque la durée de période de recours à ces équipes n'excède pas quarante-huit heures consécutives ./ Lorsque cette durée est supérieure à quarante-huit heures, la journée de travail ne peut excéder dix heures ". <br/>
<br/>
              4. D'autre part, aux termes de l'article L. 3123-1 du code du travail : " Est considéré comme salarié à temps partiel le salarié dont la durée du travail est inférieure : / 1° A la durée légale du travail ou, lorsque cette durée est inférieure à la durée légale, à la durée du travail fixée conventionnellement pour la branche ou l'entreprise ou à la durée du travail applicable dans l'établissement (...) ".<br/>
<br/>
              5. Enfin, les stipulations de l'article 2.1. de l'annexe VI de la convention collective nationale de la plasturgie du 17 octobre 2000 modifiée par l'accord du 15 mai 2013 sur l'organisation et la durée du temps de travail dans les entreprises relevant de la convention collective nationale de la plasturgie étendu par un arrêté du 19 décembre 2013 fixent, conformément aux dispositions légales, la durée hebdomadaire du travail à trente-cinq heures dans la branche de la plasturgie. Par ailleurs, l'article 1.2. du chapitre IV de l'annexe V de la convention collective nationale de la plasturgie, dans sa rédaction issue de l'accord du 8 mars 2017, stipule que " la durée quotidienne de travail des salariés affectés aux équipes de suppléance est de douze heures maximum (comprenant le temps de pause mentionné à l'article 4 du présent accord) lorsque la durée de la période de recours à ces équipes n'excède pas quarante-huit heures consécutives". <br/>
<br/>
              6. Il résulte des dispositions de l'article L. 3132-16 du code du travail, citées au point 3, que celles-ci, qui figurent dans la sous-section 2, intitulée " Dérogations au repos dominical ", de la section 2 du chapitre II du titre III du livre Ier intitulé " Durée du travail, repos et congés ", de la troisième partie du code du travail, ont pour objet de déroger au repos dominical et d'attribuer aux salariés des équipes de suppléance un repos hebdomadaire un autre jour que le dimanche, de sorte que ceux-ci puissent remplacer les salariés bénéficiant du repos dominical. Par ailleurs, il résulte clairement des stipulations citées au point 5 que la durée de travail fixée pour les salariés des équipes de suppléance dans la branche de la plasturgie est en principe inférieure à la durée légale du travail, sans pour autant exclure que, dans certains cas, ces salariés travaillent à temps plein. <br/>
<br/>
              7. Il découle de tout ce qui précède que les dispositions du code du travail relatives au temps partiel s'appliquent aux salariés travaillant en équipe de suppléance dans la branche de la plasturgie lorsque ceux-ci effectuent un travail à temps partiel au sens des dispositions de l'article L. 3123-1 du code du travail citées au point 4. Par suite, c'est sans commettre d'erreur de droit que la ministre du travail a étendu le cinquième alinéa de l'article 1-1 du chapitre IV de l'annexe V de la convention collective nationale de la plasturgie sous réserve "de l'application des dispositions relatives au temps partiel aux salariés travaillant en équipe de suppléance, dès lors que le travail effectué est à temps partiel au sens de l'article L. 3123-1 du code du travail ".<br/>
<br/>
              8. En deuxième lieu, la fédération requérante, qui ne demande l'annulation de l'arrêté qu'en tant qu'il étend l'article 1-1 du chapitre IV de l'annexe V de la convention collective nationale de la plasturgie sous réserve " de l'application des dispositions relatives au temps partiel aux salariés travaillant en équipe de suppléance, dès lors que le travail effectué est à temps partiel au sens de l'article L. 3123-1 du code du travail ", ne saurait utilement soutenir que la combinaison de cette réserve avec les autres stipulations de l'accord du 8 mars 2017 étendues par l'arrêté du 17 août 2018 fixerait une durée de travail hebdomadaire de vingt-deux heures trente pour les salariés travaillant en équipes de suppléance incompatible avec les dispositions de l'article L. 3123-27 du code du travail selon lesquelles, à défaut d'accord prévu à l'article L. 3123-19, la durée minimale de travail du salarié à temps partiel est fixée à vingt-quatre heures par semaine ou, le cas échéant, à l'équivalent mensuel de cette durée ou à l'équivalent calculé sur la période prévue par un accord collectif conclu en application de l'article L. 3121-44 du code du travail, dès lors que cette réserve a pour objet et pour effet d'assurer la conformité des stipulations de l'accord du 8 mars 2017 aux dispositions du code du travail applicables aux salariés à temps partiel.<br/>
<br/>
              9. En troisième et dernier lieu, la fédération requérante ne saurait utilement soutenir que les dispositions applicables aux salariés à temps partiel seraient incompatibles avec les stipulations conventionnelles applicables aux équipes de suppléance dès lors que la ministre du travail s'est bornée à assortir les stipulations en cause d'une réserve de nature à assurer leur conformité aux dispositions du code du travail.<br/>
<br/>
              10. Il résulte de tout ce qui précède que la requête de la fédération de la plasturgie et des composites doit être rejetée.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la fédération de la plasturgie et des composites est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la fédération de la plasturgie et des composites et à la ministre du travail, de l'emploi et de l'insertion.<br/>
Copie en sera adressée à la fédération chimie-énergie CFDT, à la fédération CMTE-CFTC secteur chimie, à la fédération nationale du personnel d'encadrement de la chimie CFE-CGC, à la fédération nationale de la chimie CGT-FO et à la fédération nationale des industries chimiques CGT.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">66-02-04 TRAVAIL ET EMPLOI. CONVENTIONS COLLECTIVES. APPLICATION DES CONVENTIONS COLLECTIVES. - CONVENTION COLLECTIVE NATIONALE DE LA PLASTURGIE IMPLIQUANT, EN RÈGLE GÉNÉRALE, UNE DURÉE DU TRAVAIL INFÉRIEURE À LA DURÉE LÉGALE POUR LES SALARIÉS DES ÉQUIPES DE SUPPLÉANCE - CONSÉQUENCE - APPLICATION, LE CAS ÉCHÉANT, DES RÈGLES DU CODE DU TRAVAIL RELATIVES AU TEMPS PARTIEL [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">66-03 TRAVAIL ET EMPLOI. CONDITIONS DE TRAVAIL. - RÈGLES DU CODE DU TRAVAIL RELATIVES AU TEMPS PARTIEL - APPLICATION AUX SALARIÉS DES ÉQUIPES DE SUPPLÉANCE - EXISTENCE, LE CAS ÉCHÉANT [RJ1] - ILLUSTRATION - BRANCHE DE LA PLASTURGIE.
</SCT>
<ANA ID="9A"> 66-02-04 Article L. 3123-1 du code du travail prévoyant qu'est considéré comme à temps partiel le salarié dont la durée du travail est inférieure à la durée légale du travail ou, lorsque cette durée est inférieure à la durée légale, à la durée du travail fixée conventionnellement pour la branche.,,,Convention collective nationale de la plasturgie fixant, conformément aux dispositions légales, la durée hebdomadaire du travail à trente-cinq heures dans la branche et stipulant que la durée quotidienne de travail des salariés affectés aux équipes de suppléance est de douze heures maximum lorsque la durée de la période de recours à ces équipes n'excède pas quarante-huit heures consécutives. Il en résulte clairement que la durée de travail fixée pour les salariés des équipes de suppléance dans la branche de la plasturgie est en principe inférieure à la durée légale du travail, sans pour autant exclure que, dans certains cas, ces salariés travaillent à temps plein.,,,Par suite, les dispositions du code du travail relatives au temps partiel s'appliquent aux salariés travaillant en équipe de suppléance dans la branche de la plasturgie lorsque ceux-ci effectuent un travail à temps partiel au sens de l'article L. 3123-1 du code du travail.</ANA>
<ANA ID="9B"> 66-03 Article L. 3123-1 du code du travail prévoyant qu'est considéré comme à temps partiel le salarié dont la durée du travail est inférieure à la durée légale du travail ou, lorsque cette durée est inférieure à la durée légale, à la durée du travail fixée conventionnellement pour la branche.,,,Convention collective nationale de la plasturgie fixant, conformément aux dispositions légales, la durée hebdomadaire du travail à trente-cinq heures dans la branche et stipulant que la durée quotidienne de travail des salariés affectés aux équipes de suppléance est de douze heures maximum lorsque la durée de la période de recours à ces équipes n'excède pas quarante-huit heures consécutives. Il en résulte clairement que la durée de travail fixée pour les salariés des équipes de suppléance dans la branche de la plasturgie est en principe inférieure à la durée légale du travail, sans pour autant exclure que, dans certains cas, ces salariés travaillent à temps plein.,,,Par suite, les dispositions du code du travail relatives au temps partiel s'appliquent aux salariés travaillant en équipe de suppléance dans la branche de la plasturgie lorsque ceux-ci effectuent un travail à temps partiel au sens de l'article L. 3123-1 du code du travail.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. Cass. soc., 16 mai 2007, Société Bodycote Hit C, n° 05-44.299, Bull. 2007, V, n° 81.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
