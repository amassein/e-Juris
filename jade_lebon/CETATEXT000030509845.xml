<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030509845</ID>
<ANCIEN_ID>JG_L_2015_04_000000386091</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/50/98/CETATEXT000030509845.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème / 2ème SSR, 17/04/2015, 386091</TITRE>
<DATE_DEC>2015-04-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>386091</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème / 2ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Stéphane Bouchard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:386091.20150417</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              M. C...D...a demandé au tribunal administratif de Strasbourg d'annuler les opérations électorales qui se sont déroulées les 23 et 30 mars 2014 dans la commune de Metz en vue de l'élection des conseillers municipaux et des conseillers communautaires. Par un jugement n° 1401771 du 30 octobre 2014, le tribunal administratif de Strasbourg a rejeté cette protestation.<br/>
<br/>
              Par une requête et un mémoire complémentaire, enregistrés les 1er et 4 décembre 2014 au secrétariat du contentieux du Conseil d'Etat, M. D...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de faire droit à sa protestation.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu : <br/>
- le code électoral ;<br/>
- le code de justice administrative ; <br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Stéphane Bouchard, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
<br/>
<br/>1. Considérant, en premier lieu, qu'aux termes des deux premiers alinéas de l'article L. 228 du code électoral : " Nul ne peut être élu conseiller municipal s'il n'est âgé de dix-huit ans révolus. / Sont éligibles au conseil municipal tous les électeurs de la commune et les citoyens inscrits au rôle des contributions directes ou justifiant qu'ils devaient y être inscrits au 1er janvier de l'année de l'élection. " ; qu'aux termes de l'article L. 265 du même code : " La déclaration de candidature résulte du dépôt à la préfecture ou à la sous-préfecture d'une liste (...). Il en est délivré récépissé. / Elle est faite collectivement pour chaque liste par la personne ayant la qualité de responsable de liste. (...) / Le dépôt de la liste doit être assorti, pour le premier tour, de l'ensemble des mandats des candidats qui y figurent ainsi que des documents officiels qui justifient qu'ils satisfont aux conditions posées par les deux premiers alinéas de l'article L. 228. / (...) Récépissé ne peut être délivré que si les conditions énumérées au présent article sont remplies et si les documents officiels visés au quatrième alinéa établissent que les candidats satisfont aux conditions d'éligibilité posées par les deux premiers alinéas de l'article L. 228. / En cas de refus de délivrance du récépissé, tout candidat de la liste intéressée dispose de vingt-quatre heures pour saisir le tribunal administratif qui statue, en premier et dernier ressort, dans les trois jours du dépôt de la requête. (...) " ; que l'article R. 128 du même code dispose : " A la déclaration de candidature en vue du premier tour, il est joint, pour chaque candidat visé à l'article L. 265 : / 1° Si le candidat est électeur dans la commune où il se présente, une attestation d'inscription sur la liste électorale de cette commune (...) ; / 2° Si le candidat est électeur dans une autre commune que celle où il se présente, une attestation d'inscription sur la liste électorale de cette commune (...) " ;<br/>
<br/>
              2. Considérant que l'existence du recours spécial prévu à l'article L. 265 du code électoral contre la décision préfectorale refusant la délivrance d'un récépissé de déclaration de candidature d'une liste ne fait pas obstacle à ce qu'un grief tiré de l'irrégularité de ce refus soit soulevé à l'occasion d'une protestation dirigée contre les opérations électorales, alors même que ce recours spécial a été effectivement exercé et rejeté, quel que soit le motif de ce rejet ; <br/>
<br/>
              3. Considérant qu'il résulte de l'instruction que le préfet de la Moselle a refusé, le 6 mars 2014, de délivrer à M.D..., en vue du premier tour des élections municipales qui se sont déroulées le 23 mars 2014 à Metz, le récépissé de déclaration de candidature de la liste " Metz " dont il était responsable, au motif que le dépôt de cette liste n'était pas accompagné d'une attestation d'inscription sur les listes électorales pour chacun des candidats y figurant ; que, bien que M. D...ait exercé devant le tribunal administratif de Strasbourg le recours spécial organisé par les dispositions citées ci-dessus du code électoral et que ce recours ait été rejeté pour tardiveté, le requérant était, contrairement à ce qu'a jugé le tribunal administratif de Strasbourg, recevable à soulever le grief tiré de l'irrégularité de ce refus à l'occasion de sa protestation contre les opérations électorales en cause ; que, toutefois, il ne résulte pas de l'instruction que M. D... ait joint à sa déclaration de candidature les attestations exigées par les dispositions de l'article R. 128 du code électoral ; que, par suite, le grief tiré de ce que le préfet aurait à tort refusé de délivrer le récépissé en cause doit être écarté ;<br/>
<br/>
              4. Considérant, en deuxième lieu, qu'aux termes du premier alinéa de l'article L. 49 du code électoral : " A partir de la veille du scrutin à zéro heure, il est interdit de distribuer ou faire distribuer des bulletins, circulaires et autres documents. " ; que, contrairement à ce que soutient M.D..., il ne résulte pas de l'instruction que des tracts électoraux auraient été distribués dans les rues de Metz par M.A..., maire sortant, ou pour le compte de ce dernier, le 22 mars 2014, veille du premier tour ; que par suite, et en tout état de cause, le grief tiré de la méconnaissance de ces dispositions doit être écarté ;<br/>
<br/>
              5. Considérant, en troisième lieu, qu'aux termes de l'article L. 52-1 du code électoral : " Pendant les six mois précédant le premier jour du mois d'une élection et jusqu'à la date du tour de scrutin où celle-ci est acquise, l'utilisation à des fins de propagande électorale de tout procédé de publicité commerciale par la voie de la presse ou par tout moyen de communication audiovisuelle est interdite. / A compter du premier jour du sixième mois précédant le mois au cours duquel il doit être procédé à des élections générales, aucune campagne de promotion publicitaire des réalisations ou de la gestion d'une collectivité ne peut être organisée sur le territoire des collectivités intéressées par le scrutin. Sans préjudice des dispositions du présent chapitre, cette interdiction ne s'applique pas à la présentation, par un candidat ou pour son compte, dans le cadre de l'organisation de sa campagne, du bilan de la gestion des mandats qu'il détient ou qu'il a détenus. (...). " ; qu'il résulte de l'instruction que le " publi-reportage " diffusé dans un organe de presse le 28 septembre 2013 se borne à faire état de la tenue, à l'initiative de l'office public de l'habitat " Metz Habitat territoires ", d'une journée d'inauguration marquant la fin des travaux de rénovation des immeubles du Clos du Lys menés par cet office, sans mentionner l'éventuel rôle du maire ou de l'équipe municipale sortante dans cette opération de rénovation urbaine ; que par suite, cette publication ne saurait être regardée comme une campagne de promotion publicitaire des réalisations ou de la gestion de la mairie de Metz organisée en méconnaissance des dispositions précitées ;<br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que M. D...n'est pas fondé à se plaindre de ce que, par le jugement attaqué, le tribunal administratif de Strasbourg a rejeté sa protestation tendant à l'annulation des opérations électorales des 23 et 30 mars 2014 ;<br/>
<br/>
              7. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par M. A...au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de M. D...est rejetée.<br/>
<br/>
Article 2 : Les conclusions présentées par M. A...au titre des dispositions de l'article L. 761-1 du code de justice administrative son rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. C... D..., à M. F... A...et au ministre de l'intérieur.<br/>
Copie en sera adressée à Mme E... H..., à Mme G...B..., et à la Commission nationale des comptes de campagne et des financements politiques.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">28-08 ÉLECTIONS ET RÉFÉRENDUM. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. - CONTESTATION D'UN REFUS DE RÉCÉPISSÉ DE DÉPÔT DE CANDIDATURE (ART. L. 265 DU CODE ÉLECTORAL) - CONTENTIEUX ULTÉRIEUR RELATIF AUX OPÉRATIONS ÉLECTORALES - POSSIBILITÉ D'INVOQUER DANS LE CADRE DE CE SECOND CONTENTIEUX UN MOYEN TIRÉ DE L'ILLÉGALITÉ DU REFUS - EXISTENCE, QUELLE QUE SOIT LE MOTIF DE REJET DE LA PREMIÈRE CONTESTATION [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">28-08-05-02-02 ÉLECTIONS ET RÉFÉRENDUM. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. POUVOIRS DU JUGE. GRIEFS. GRIEFS RECEVABLES. - CONTESTATION D'UN REFUS DE RÉCÉPISSÉ DE DÉPÔT DE CANDIDATURE (ART. L. 265 DU CODE ÉLECTORAL) - CONTENTIEUX ULTÉRIEUR RELATIF AUX OPÉRATIONS ÉLECTORALES - POSSIBILITÉ D'INVOQUER DANS LE CADRE DE CE SECOND CONTENTIEUX UN MOYEN TIRÉ DE L'ILLÉGALITÉ DU REFUS - EXISTENCE, QUELLE QUE SOIT LE MOTIF DE REJET DE LA PREMIÈRE CONTESTATION [RJ1].
</SCT>
<ANA ID="9A"> 28-08 L'existence du recours spécial prévu à l'article L. 265 du code électoral contre la décision préfectorale refusant la délivrance d'un récépissé de déclaration de candidature d'une liste ne fait pas obstacle à ce qu'un grief tiré de l'irrégularité de ce refus soit soulevé à l'occasion d'une protestation dirigée contre les opérations électorales, alors même que ce recours spécial a été effectivement exercé et rejeté, quel que soit le motif de ce rejet.</ANA>
<ANA ID="9B"> 28-08-05-02-02 L'existence du recours spécial prévu à l'article L. 265 du code électoral contre la décision préfectorale refusant la délivrance d'un récépissé de déclaration de candidature d'une liste ne fait pas obstacle à ce qu'un grief tiré de l'irrégularité de ce refus soit soulevé à l'occasion d'une protestation dirigée contre les opérations électorales, alors même que ce recours spécial a été effectivement exercé et rejeté, quel que soit le motif de ce rejet.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., pour une formulation différente de la possibilité de soulever ultérieurement un tel grief, quel que soit le motif de rejet du recours spécial, CE, 17 juillet 2009, Elections municipales de Roquefort-les-Pins, n° 317566, T. pp. 769-819.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
