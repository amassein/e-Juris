<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030253283</ID>
<ANCIEN_ID>JG_L_2015_02_000000383073</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/25/32/CETATEXT000030253283.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère / 6ème SSR, 17/02/2015, 383073</TITRE>
<DATE_DEC>2015-02-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>383073</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère / 6ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Denis Rapone</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:383073.20150217</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
		Procédure contentieuse antérieure<br/>
<br/>
              M. A...D..., Mme O...C..., M. J...K..., Mme N...E..., M. G...P...et Mme M...F...ont demandé au tribunal administratif de Lyon de déclarer M. H...I...inéligible comme conseiller municipal de Châtillon-sur-Chalaronne (Ain) et de réformer les résultats de l'élection du conseil municipal de Châtillon-sur-Chalaronne en conséquence. Par un jugement n° 1402114 du 24 juin 2014, le tribunal administratif de Lyon a rejeté leur protestation.<br/>
<br/>
		Procédure devant le Conseil d'Etat<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 22 juillet et 13 octobre 2014 au secrétariat du contentieux du Conseil d'Etat, M.D..., MmeC..., M. K..., MmeE..., M. P...et Mme F...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement du tribunal administratif de Lyon du 24 juin 2014 ;<br/>
<br/>
              2°) de déclarer M. H...I...inéligible comme conseiller municipal de Châtillon-sur-Chalaronne ;<br/>
<br/>
              3°) de réformer les résultats de l'élection du conseil municipal de Châtillon-sur-Chalaronne en conséquence.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Denis Rapone, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'à l'issue des opérations électorales qui se sont déroulées le 23 mars 2014 dans la commune de Châtillon-sur-Chalaronne (Ain), les 27 sièges de conseillers municipaux ont été pourvus ; que 21 de ces sièges ont été attribués à des candidats de la liste " Tous ensemble pour Châtillon ", conduite par M. B...L..., dont M. H...I..., qui se trouvait en onzième position sur cette liste, tandis que 6 sièges de conseillers municipaux ont été attribués à des candidats de la liste " Avançons autrement " conduite par M. A... D... ; que M. D... et les cinq autres membres de sa liste élus au conseil municipal contestent l'élection de M. H...I...;<br/>
<br/>
              2. Considérant, en premier lieu, qu'en vertu du 8° de l'article L. 231 du code électoral, dans sa rédaction issue de l'article 22 de la loi du 17 mai 2013 relative à l'élection des conseillers départementaux, des conseillers municipaux et des conseillers communautaires, et modifiant le calendrier électoral, ne peuvent être élus conseillers municipaux, dans les communes situées dans le ressort où ils exercent ou ont exercé leurs fonctions depuis moins de six mois : " Les personnes exerçant, au sein du conseil régional, du conseil départemental, de la collectivité territoriale de Corse, de Guyane ou de Martinique, d'un établissement public de coopération intercommunale à fiscalité propre ou de leurs établissements publics, les fonctions de directeur général des services, directeur général adjoint des services, directeur des services, directeur adjoint des services ou chef de service, ainsi que les fonctions de directeur de cabinet, directeur adjoint de cabinet ou chef de cabinet en ayant reçu délégation de signature du président, du président de l'assemblée ou du président du conseil exécutif (....) " ;<br/>
<br/>
              3. Considérant que les dispositions du 8° de l'article L. 231 du code électoral citées au point 2 doivent s'entendre, eu  égard à leur objet, comme visant non le conseil régional ou le conseil départemental mais les collectivités dont ils sont les organes délibérants ; qu'entrent ainsi dans le champ de ces dispositions, qui sont d'interprétation stricte, d'une part, les établissements publics dépendant exclusivement d'une région ou d'un département, ainsi que des autres collectivités territoriales et établissements mentionnés par ces dispositions, d'autre part, ceux qui sont communs à plusieurs de ces collectivités ; que doivent être seulement regardés comme dépendant de ces collectivités ou établissements ou comme communs à plusieurs collectivités, pour l'application de ces dispositions, les établissements publics créés par ces seuls collectivités ou établissements ou à leur demande ; qu'en revanche, il ne ressort pas de ces dispositions que l'inéligibilité qu'elles prévoient s'étende aux personnes exerçant les fonctions qu'elles mentionnent dans d'autres établissements publics que ceux qui dépendent d'une ou plusieurs des collectivités et établissements qu'elles citent ou sont communs à plusieurs de ces collectivités ;<br/>
<br/>
              4. Considérant qu'aux termes de l'article L. 324-1 du code de l'urbanisme, dans sa rédaction applicable à la date de l'élection : " Les établissements publics fonciers créés en application du présent chapitre sont des établissements publics locaux à caractère industriel et commercial. Ils sont compétents pour réaliser, pour leur compte, pour le compte de leurs membres ou de toute personne publique, toute acquisition foncière ou immobilière en vue de la constitution de réserves foncières en application des articles L. 221-1 et L. 221-2 ou de la réalisation d'actions ou d'opérations d'aménagement au sens de l'article L. 300-1. A l'intérieur des périmètres délimités en application de l'article L. 143-1, ils peuvent procéder, après information des communes et des établissements publics de coopération intercommunale concernés, aux acquisitions foncières nécessaires à la protection d'espaces agricoles et naturels périurbains, le cas échéant en exerçant, à la demande et au nom du département, le droit de préemption prévu par l'article L. 142-3 ou, en dehors des zones de préemption des espaces naturels sensibles, le droit de préemption prévu par le 9° de l'article L. 143-2 du code rural et de la pêche maritime. / Ces établissements interviennent sur le territoire des communes ou des établissements publics de coopération intercommunale qui en sont membres et, à titre exceptionnel, ils peuvent intervenir à l'extérieur de ce territoire pour des acquisitions nécessaires à des actions ou opérations menées à l'intérieur de celui-ci. / Les acquisitions et cessions foncières et immobilières réalisées par ces établissements pour leur propre compte ou pour le compte d'une collectivité territoriale, d'un établissement public de coopération intercommunale ou d'un syndicat mixte sont soumises aux dispositions relatives à la transparence des opérations immobilières de ces collectivités ou établissements. / Ils peuvent exercer, par délégation de leurs titulaires, les droits de préemption définis par le présent code dans les cas et conditions qu'il prévoit et agir par voie d'expropriation. / Aucune opération de l'établissement public ne peut être réalisée sans l'avis favorable de la commune sur le territoire de laquelle l'opération est prévue. Cet avis est réputé donné dans un délai de deux mois à compter de la saisine de la commune " ; qu'aux termes de l'article L. 324-2 du code de l'urbanisme, dans sa rédaction applicable à la même date : " L'établissement public foncier est créé par le préfet au vu des délibérations concordantes des organes délibérants d'établissements publics de coopération intercommunale, qui sont compétents en matière de schéma de cohérence territoriale, de réalisation de zones d'aménagement concerté et de programme local de l'habitat, ainsi que, le cas échéant, de conseils municipaux de communes non membres de l'un de ces établissements. Lorsque les établissements publics de coopération intercommunale et les communes appartiennent à plusieurs départements, la décision est prise par arrêté conjoint des préfets concernés. La région et le département peuvent participer à la création de l'établissement public ou y adhérer. Le ou les préfets disposent d'un délai de trois mois à compter de la transmission des délibérations pour donner leur accord. A défaut de décision à l'expiration de ce délai, l'arrêté créant l'établissement public est acquis tacitement. / Les délibérations fixent la liste des membres de l'établissement, les modalités de fonctionnement, la durée, le siège et la composition de l'assemblée générale (...) de l'établissement public foncier, en tenant compte de l'importance de la population des communes et des établissements publics de coopération intercommunale membres. / La décision de création comporte les éléments mentionnés à l'alinéa précédent " ;<br/>
<br/>
              5. Considérant qu'il résulte de l'instruction que l'établissement public foncier local de l'Ain, établissement public industriel et commercial créé par arrêté du préfet de l'Ain du 18 décembre 2006 en application des dispositions précitées des articles L. 324-1 et L. 324-2 du code de l'urbanisme, dont sont membres la région Rhône-Alpes, le département de l'Ain, seize établissements publics de coopération intercommunale à fiscalité propre et trente-quatre communes isolées au nombre desquelles figure la commune de Châtillon-sur-Chalaronne, n'est pas un établissement public dépendant seulement de collectivités ou établissements mentionnés par les dispositions du 8° de l'article L. 231 du code électoral ou commun à ces seuls collectivités ou établissements ; que, par suite, il n'entre pas dans le champ de ces dispositions ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que les requérants ne sont pas fondés à soutenir que M.I..., directeur de l'établissement public foncier local de l'Ain, était inéligible en application des dispositions du 8° de l'article L. 231 du code électoral ; <br/>
<br/>
              7. Considérant, en second lieu, qu'en vertu du 6° de l'article L. 231 du code électoral, ne peuvent être élus conseillers municipaux dans les communes situées dans le ressort où ils exercent ou ont exercé leurs fonctions depuis moins de six mois " les entrepreneurs de services municipaux " ; que si l'établissement public foncier local de l'Ain peut se voir confier, pour le compte de ses membres, des opérations d'acquisition foncière et immobilière en vue de la constitution de réserves foncières ou des opérations d'aménagement foncier, il ne résulte pas de l'instruction qu'il intervienne de façon régulière pour le compte de la commune de Châtillon-sur-Chalaronne et sous le contrôle de celle-ci dans des conditions telles que M. I... devrait être regardé comme ayant la qualité d'entrepreneur de services municipaux, au sens des dispositions du 6° de l'article L. 231 du code électoral ; que, par suite, les requérants ne sont pas fondés à soutenir que M. I...était inéligible en application de ces dispositions ;   <br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que M. D...et autres ne sont pas fondés à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Lyon a rejeté leur protestation dirigée contre l'élection de M. I...en qualité de conseiller municipal à l'issue des opérations électorales qui se sont déroulées le 23 mars 2014 dans la commune de Châtillon-sur-Chalaronne ; <br/>
<br/>
              9. Considérant quil n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge des requérants la somme que M. I...demande au titre des dispositions de l'article L. 761-1 du code de justice administrative ;  <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. D...et autres est rejetée. <br/>
Article 2 : Les conclusions de M. I...présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 3 : La présente décision sera notifiée à M. A...D..., à M. B...L...et au ministre de l'intérieur. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">28-04-02-02-065 ÉLECTIONS ET RÉFÉRENDUM. ÉLECTIONS MUNICIPALES. ÉLIGIBILITÉ. INÉLIGIBILITÉS. AGENTS DU CONSEIL GÉNÉRAL ET DU CONSEIL RÉGIONAL. - INÉLIGIBILITÉ, À RAISON DE LEURS FONCTIONS, DE CERTAINS AGENTS DES COLLECTIVITÉS TERRITORIALES, DES EPCI ET DES ÉTABLISSEMENTS PUBLICS LOCAUX DANS LES COMMUNES DU RESSORT OÙ ILS EXERCENT (8° DE L'ARTICLE L. 231 DU CODE ÉLECTORAL DANS SA RÉDACTION ISSUE DE LA LOI DU 17 MAI 2013) - CHAMP D'APPLICATION [RJ1] - EXCLUSION EN L'ESPÈCE - ETABLISSEMENT PUBLIC FONCIER LOCAL.
</SCT>
<ANA ID="9A"> 28-04-02-02-065 En l'espèce, un établissement public foncier local, établissement public industriel et commercial créé par arrêté du préfet de département en application des dispositions des articles L. 324-1 et L. 324-2 du code de l'urbanisme, dont sont membres la région, le département, des établissements publics de coopération intercommunale à fiscalité propre et des communes isolées, n'est pas un établissement public dépendant seulement de collectivités ou établissements mentionnés par les dispositions du 8° de l'article L. 231 du code électoral ou commun à ces seuls collectivités ou établissements et, par suite, n'entre pas dans le champ de ces dispositions.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur les principes relatifs au champ d'application de ces dispositions, CE, Section, 4 février 2015, Elections municipales de La Crèche, n° 382969, à publier au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
