<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043799792</ID>
<ANCIEN_ID>JG_L_2021_07_000000448741</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/79/97/CETATEXT000043799792.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 12/07/2021, 448741</TITRE>
<DATE_DEC>2021-07-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>448741</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Laurent-Xavier Simonel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:448741.20210712</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu les procédures suivantes :<br/>
<br/>
              1° Le préfet du Var a demandé au tribunal administratif de Toulon d'annuler les opérations électorales qui se sont déroulées au sein du syndicat intercommunal d'assainissement de Cogolin-Gassin le 13 octobre 2020, par lesquelles son comité syndical a élu MM. Erwan F..., Jean-Pascal D... et Serge A... en qualité de membres suppléants de la commission d'appel d'offres des marchés publics du syndicat. Par un jugement n° 2003042 du 15 décembre 2020, le tribunal administratif a rejeté le déféré du préfet.<br/>
<br/>
              Sous le n° 448741, par une requête, enregistrée le 15 janvier 2021 au secrétariat du contentieux du Conseil d'Etat, le préfet du Var demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) d'annuler l'élection de MM. Erwan F..., Jean-Pascal D... et Serge A... en qualité de membres suppléants de la commission d'appel d'offres.<br/>
<br/>
<br/>
<br/>
              Sous le n° 448742, par une requête, enregistrée le 15 janvier 2021 au secrétariat du contentieux du Conseil d'Etat, le préfet du Var demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) d'annuler l'élection de MM. Erwan F..., Jean-Pascal D... et Serge A... en qualité de membres suppléants de la commission de concessions.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code électoral ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent-Xavier Simonel, conseiller d'Etat en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Les deux requêtes du préfet du Var présentant à juger des questions semblables, il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2. A l'issue des opérations électorales qui se sont déroulées en son sein le 13 octobre 2020, le comité syndical du syndicat intercommunal d'assainissement de Cogolin-Gassin a élu M. B... F... et M. E... D..., délégués suppléants de la commune de Cogolin au comité syndical ainsi que M. C... A..., délégué suppléant de la commune de Gassin au comité syndical, en qualité de membres suppléants de la commission d'appel d'offres des marchés publics du syndicat, prévue par les dispositions de l'article L. 1414-2 du code général des collectivités territoriales, et en qualité de membres suppléants de la commission de concessions du syndicat, remplissant les attributions de la commission de délégation de service public prévue par les dispositions de l'article L. 1411-5 du même code. Le préfet du Var fait appel des deux jugements du 15 décembre 2020 par lesquels le tribunal administratif de Toulon a rejeté ses deux déférés dirigés contre ces opérations électorales.<br/>
<br/>
              3. D'une part, aux termes de l'article L. 1411-5 du code général des collectivités territoriales relatif aux délégations de service public : " I.- Une commission analyse les dossiers de candidature et dresse la liste des candidats admis à présenter une offre (...) / II.- La commission est composée : / a) Lorsqu'il s'agit (...) d'un établissement public, par l'autorité habilitée à signer la convention de délégation de service public ou son représentant, président, et par cinq membres de l'assemblée délibérante élus en son sein à la représentation proportionnelle au plus fort reste (...) / Il est procédé, selon les mêmes modalités, à l'élection de suppléants en nombre égal à celui de membres titulaires ". Aux termes de l'article L. 1414-2 du même code relatif aux marchés publics : " Pour les marchés publics passés selon une procédure formalisée dont la valeur estimée hors taxe prise individuellement est égale ou supérieure aux seuils européens qui figurent en annexe du code de la commande publique, à l'exception des marchés publics passés par les établissements publics sociaux ou médico-sociaux, le titulaire est choisi par une commission d'appel d'offres composée conformément aux dispositions de l'article L. 1411-5 (...) ". <br/>
<br/>
              4. D'autre part, aux termes de l'article L. 5211-7 du même code : " I. - Les syndicats de communes sont administrés par un organe délibérant composé de délégués élus par les conseils municipaux des communes membres dans les conditions prévues à l'article L. 2122-7. (...) / II. - Les conditions d'éligibilité (...) applicables aux délégués des communes sont celles prévues pour les élections au conseil municipal par les articles L. 44 à L. 45-1 (...) du code électoral (...) ". Aux termes de l'article L. 5212-6 du même code : " Le comité syndical est institué d'après les règles fixées aux articles L. 5211-7, L. 5211-8 et, sauf dispositions contraires prévues par la décision institutive, à l'article L. 5212-7 ". Aux termes de l'article L. 5212-7 du même code : " Chaque commune est représentée dans le comité par deux délégués titulaires. (...) La décision d'institution ou une décision modificative peut prévoir la désignation d'un ou plusieurs délégués suppléants, appelés à siéger au comité avec voix délibérative, en cas d'empêchement du ou des délégués titulaires. / Le choix du conseil municipal peut porter uniquement sur l'un de ses membres. (...) ". Aux termes de l'article L. 44 du code électoral : " Tout Français et toute Française ayant la qualité d'électeur peut faire acte de candidature et être élu, sous réserve des cas d'incapacité ou d'inéligibilité prévus par la loi ".<br/>
<br/>
              5. Il résulte de ces dispositions que, lorsqu'il est prévu qu'une commune soit représentée au sein du comité syndical d'un syndicat de communes dont elle est membre à la fois par des délégués titulaires et par des délégués suppléants, ces délégués titulaires et suppléants sont élus dans les mêmes conditions au comité syndical et, lorsqu'ils sont appelés à y siéger, participent de la même façon, avec une voix également délibérative, à ses délibérations. Par suite, les délégués suppléants au comité syndical sont éligibles, en qualité de membres de l'assemblée délibérante élus en son sein, au sens des dispositions du a) du II de l'article L. 1411-5 du code général des collectivités territoriales, pour être désignés en qualité de membres titulaires ou suppléants de la commission d'appel d'offres prévue par l'article L. 1414-2 du même code.<br/>
<br/>
              6. De même, les délégués suppléants au comité syndical sont éligibles, en qualité de membres de l'assemblée délibérante élus en son sein, au sens des dispositions du a) du II de l'article L. 1411-5 du code général des collectivités territoriales, pour être désignés en qualité de membres titulaires ou suppléants de la commission de délégation de service public prévue par ces dispositions.<br/>
<br/>
              7. Il résulte de tout ce qui précède que le préfet du Var n'est pas fondé à soutenir que c'est à tort que, par les jugements attaqués, le tribunal administratif de Toulon a rejeté ses déférés tendant à l'annulation des opérations électorales du 13 octobre 2020 au sein du syndicat intercommunal d'assainissement de Cogolin-Gassin ayant conduit à la désignation de trois représentants suppléants à la commission d'appel d'offres des marchés publics et à la commission de concessions parmi les délégués suppléants représentant les communes membres de Cogolin ou de Gassin au sein du comité syndical du syndicat intercommunal.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Les requêtes du préfet du Var sont rejetées.<br/>
Article 2 : La présente décision sera notifiée au préfet du Var, au syndicat intercommunal d'assainissement de Cogolin-Gassin, à M. B... F..., à M. E... D... et à M. C... A....<br/>
Copie en sera adressée au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-05-01-03-02 COLLECTIVITÉS TERRITORIALES. COOPÉRATION. ÉTABLISSEMENTS PUBLICS DE COOPÉRATION INTERCOMMUNALE - QUESTIONS GÉNÉRALES. SYNDICATS DE COMMUNES. ORGANES. - CAO ET CDSP - COMPOSITION - ELIGIBILITÉ D'UN DÉLÉGUÉ SUPPLÉANT DU COMITÉ SYNDICAL - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">28-07-03 ÉLECTIONS ET RÉFÉRENDUM. ÉLECTIONS DIVERSES. ÉLECTIONS LOCALES DIVERSES. - ELECTION À LA CAO ET À LA CDSP D'UN SYNDICAT DE COMMUNES - ELIGIBILITÉ D'UN DÉLÉGUÉ SUPPLÉANT DU COMITÉ SYNDICAL - EXISTENCE.
</SCT>
<ANA ID="9A"> 135-05-01-03-02 Il résulte des articles L. 5211-7, L. 5212-6 et L. 5212-7 du code général des collectivités territoriales (CGCT) et de l'article L. 44 du code électoral que, lorsqu'il est prévu qu'une commune soit représentée au sein du comité syndical d'un syndicat de communes dont elle est membre à la fois par des délégués titulaires et par des délégués suppléants, ces délégués titulaires et suppléants sont élus dans les mêmes conditions au comité syndical et, lorsqu'ils sont appelés à y siéger, participent de la même façon, avec une voix également délibérative, à ses délibérations.,,,Par suite, les délégués suppléants au comité syndical sont éligibles, en qualité de membres de l'assemblée délibérante élus en son sein, au sens du a) du II de l'article L. 1411-5 du CGCT, pour être désignés en qualité de membres titulaires ou suppléants de la commission d'appel d'offres (CAO) prévue par l'article L. 1414-2 du même code.,,,De même, les délégués suppléants au comité syndical sont éligibles, en qualité de membres de l'assemblée délibérante élus en son sein, au sens du a) du II de l'article L. 1411-5 du CGCT, pour être désignés en qualité de membres titulaires ou suppléants de la commission de délégation de service public (CDSP) prévue par ces dispositions.</ANA>
<ANA ID="9B"> 28-07-03 Il résulte des articles L. 5211-7, L. 5212-6 et L. 5212-7 du code général des collectivités territoriales (CGCT) et de l'article L. 44 du code électoral que, lorsqu'il est prévu qu'une commune soit représentée au sein du comité syndical d'un syndicat de communes dont elle est membre à la fois par des délégués titulaires et par des délégués suppléants, ces délégués titulaires et suppléants sont élus dans les mêmes conditions au comité syndical et, lorsqu'ils sont appelés à y siéger, participent de la même façon, avec une voix également délibérative, à ses délibérations.,,,Par suite, les délégués suppléants au comité syndical sont éligibles, en qualité de membres de l'assemblée délibérante élus en son sein, au sens du a) du II de l'article L. 1411-5 du CGCT, pour être désignés en qualité de membres titulaires ou suppléants de la commission d'appel d'offres (CAO) prévue par l'article L. 1414-2 du même code.,,,De même, les délégués suppléants au comité syndical sont éligibles, en qualité de membres de l'assemblée délibérante élus en son sein, au sens du a) du II de l'article L. 1411-5 du CGCT, pour être désignés en qualité de membres titulaires ou suppléants de la commission de délégation de service public (CDSP) prévue par ces dispositions.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
