<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000024448318</ID>
<ANCIEN_ID>JG_L_2011_07_000000331126</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/24/44/83/CETATEXT000024448318.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 28/07/2011, 331126</TITRE>
<DATE_DEC>2011-07-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>331126</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>FOUSSARD ; LE PRADO</AVOCATS>
<RAPPORTEUR>Mme Domitille Duval-Arnould</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi et le mémoire complémentaire, enregistrés le 25 août et le 9 décembre 2009 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme Jacqueline A, demeurant ..., Mlle Magali B, demeurant ... et Mlle Céline B, demeurant ... ; Mme A et autres demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 06LY00517 du 23 juin 2009 par lequel la cour administrative d'appel de Lyon, statuant sur l'appel du centre hospitalier d'Auxerre, a annulé le jugement du tribunal administratif de Dijon du 22 décembre 2005 et a rejeté leur demande tendant à la condamnation du centre hospitalier à leur verser des dommages-intérêts en réparation du préjudice résultant du décès de M. Yves C ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel du centre hospitalier d'Auxerre et de faire droit à leurs conclusions incidentes ; <br/>
<br/>
              3°) de mettre à la charge du centre hospitalier la somme de 5 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu le code de la santé publique ;<br/>
<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Domitille Duval-Arnould, chargée des fonctions de Maître des Requêtes,  <br/>
<br/>
              - les observations de Me Foussard, avocat des consorts A, et de Me Le Prado, avocat du centre hospitalier d'Auxerre, <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public,<br/>
<br/>
              La parole ayant été à nouveau donnée à Me Foussard, avocat des consorts A, et à Me Le Prado, avocat du centre hospitalier d'Auxerre ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. C, alors âgé de 57 ans, a subi au centre hospitalier d'Auxerre divers examens dont une radiographie thoracique le 13 mars 2001 et des biopsies bronchiques et un scanner thoracique le 10 mai 2001 ; que ces examens ont révélé l'existence d'un risque de cancer bronchique ; que M. C a de nouveau été admis au centre hospitalier d'Auxerre en janvier 2002 ; qu'a alors été diagnostiqué un cancer du poumon avec dissémination osseuse ; que M. C est décédé le 10 mars suivant ; que son épouse, Mme Jacqueline A, et ses deux filles, Mlles Magali et Céline B, se pourvoient en cassation contre l'arrêt par lequel la cour administrative d'appel de Lyon a annulé le jugement du tribunal administratif de Dijon qui avait condamné le centre à leur verser respectivement les sommes de 15 000 euros et 5 000 euros chacune ; <br/>
<br/>
              Sans qu'il soit besoin d'examiner les autres moyens du pourvoi ;<br/>
<br/>
              Considérant qu'aux termes de l'article L. 1112-1 du code de la santé publique, dans sa rédaction applicable à la date des faits litigieux : "Les établissements de santé, publics ou privés, sont tenus de communiquer aux personnes recevant ou ayant reçu des soins, sur leur demande et par l'intermédiaire du praticien qu'elles désignent, les informations médicales contenues dans leur dossier médical. Les praticiens qui ont prescrit l'hospitalisation ont accès, sur leur demande, à ces informations. / Dans le respect des règles déontologiques qui leur sont applicables, les praticiens des établissements assurent l'information des personnes soignées (...) " ; qu'en application de ces dispositions, il appartient aux praticiens des établissements publics de santé d'informer directement le patient des investigations pratiquées et de leurs résultats, en particulier lorsqu'elles mettent en évidence des risques pour sa santé, à moins que celui-ci n'ait expressément demandé que les informations médicales le concernant ne lui soient délivrées que par l'intermédiaire de son médecin traitant ; que, par suite, la cour, après avoir relevé que les pièces du dossier ne permettaient pas d'établir que le médecin pneumologue du centre hospitalier ait informé M. C de la suspicion d'un cancer bronchique à l'issue des investigations menées en mars et mai 2001, n'a pu, sans commettre d'erreur de droit, juger que l'information de M. C avait été assurée par le seul envoi par l'hôpital du compte rendu des examens à son médecin traitant alors qu'il n'avait pas expressément demandé que seul ce dernier lui indique les résultats des investigations pratiquées sur sa personne ; que son arrêt doit par suite être annulé ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Sur la responsabilité du centre hospitalier d'Auxerre :<br/>
<br/>
              Considérant qu'ainsi qu'il a été dit ci-dessus, les praticiens des établissements publics de santé ont l'obligation d'informer directement le patient des investigations pratiquées et de leurs résultats, en particulier lorsqu'elles mettent en évidence des risques pour sa santé, à moins que celui-ci n'ait expressément demandé que les informations médicales le concernant ne lui soient délivrées que par l'intermédiaire de son médecin traitant ; qu'il appartient aux établissements publics de santé d'établir que cette information a été délivrée ; <br/>
<br/>
              Considérant qu'il résulte de l'instruction que les résultats des examens pratiqués le 10 mai 2001 permettaient de suspecter que M. C était atteint d'un cancer bronchique et impliquaient nécessairement des investigations complémentaires afin de poser le diagnostic et de proposer un traitement ; que le centre hospitalier n'établit pas que cette information a été apportée à l'intéressé à l'issue des examens en cause ; que, comme il a été dit ci-dessus, la circonstance que les résultats des examens ont été adressés au médecin traitant de M. C ne dispensait pas le centre hospitalier de son obligation d'information du patient ; que le défaut d'information de M. C révèle, comme l'a jugé le tribunal administratif de Dijon, une faute dans l'organisation et le fonctionnement du service de nature à engager la responsabilité du centre hospitalier d'Auxerre ; que cette faute a fait perdre à M. C une chance de recevoir des soins permettant de retarder son décès ; <br/>
<br/>
              Sur le préjudice :<br/>
<br/>
              Considérant qu'il résulte de l'instruction et notamment du rapport d'expertise que le traitement du cancer dont été atteint M. C aurait pu, s'il avait été entrepris à l'issue des examens effectués en mai 2001, permettre à ce dernier de survivre entre 26 et 37 mois de plus ; qu'il s'ensuit que le centre hospitalier d'Auxerre n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Dijon l'a condamné à verser à Mme A la somme de 10 000 euros au titre du préjudice moral qu'elle a subi et, au vu des revenus de M. C avant son décès, de 5 000 euros au titre du préjudice économique, ainsi que la somme de 5 000 euros à chacune des filles de M. C au titre de leur préjudice moral ; que les requérantes ne démontrent pas, dans leur appel incident, que ces indemnités aient été fixées à un niveau insuffisant ;<br/>
<br/>
              Sur l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, en application de ces dispositions, de mettre à la charge du centre hospitalier d'Auxerre une somme globale de 5 000 euros au titre des frais exposés par Mme Jacqueline A et Mlles Magali et Céline B, devant la cour administrative d'appel et le Conseil d'Etat, et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon du 23 juin 2009 est annulé.<br/>
<br/>
Article 2 : La requête du centre hospitalier d'Auxerre et les conclusions incidentes de Mme Jacqueline A et de Mlles Magali et Céline B devant la cour administrative d'appel de Lyon sont rejetées. <br/>
<br/>
Article 3 : Le centre hospitalier d'Auxerre versera à Mme Jacqueline A et à Mlles Magali et Céline B la somme globale de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à Mme Jacqueline A, à Mlle Magali B, à Mlle Céline B et au centre hospitalier d'Auxerre.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-02-01-01-01-01-04 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICE PUBLIC DE SANTÉ. ÉTABLISSEMENTS PUBLICS D'HOSPITALISATION. RESPONSABILITÉ POUR FAUTE SIMPLE : ORGANISATION ET FONCTIONNEMENT DU SERVICE HOSPITALIER. EXISTENCE D'UNE FAUTE. MANQUEMENTS À UNE OBLIGATION D'INFORMATION ET DÉFAUTS DE CONSENTEMENT. - MANQUEMENT AU DEVOIR D'INFORMER DIRECTEMENT LES PATIENTS DES RÉSULTATS DES INVESTIGATIONS PRATIQUÉES - EXPÉDITION DES RÉSULTATS D'ANALYSE AU MÉDECIN TRAITANT DU PATIENT ALORS QUE CE DERNIER NE L'AVAIT PAS EXPRESSÉMENT DEMANDÉ - EXISTENCE D'UNE FAUTE.
</SCT>
<ANA ID="9A"> 60-02-01-01-01-01-04 Les praticiens des établissements publics de santé ont l'obligation d'informer directement le patient des investigations pratiquées et de leurs résultats, en particulier lorsqu'elles mettent en évidence des risques pour sa santé, à moins que celui-ci n'ait expressément demandé que les informations médicales le concernant ne lui soient délivrées que par l'intermédiaire de son médecin traitant. Il appartient aux établissements publics de santé d'établir que cette information a été délivrée. Tel n'est pas le cas lorsque le centre hospitalier a expédié les résultats au médecin traitant de l'intéressé, mais n'établit pas les lui avoir envoyés directement.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
