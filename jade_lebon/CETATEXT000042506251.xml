<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042506251</ID>
<ANCIEN_ID>JG_L_2020_11_000000437718</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/50/62/CETATEXT000042506251.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 06/11/2020, 437718</TITRE>
<DATE_DEC>2020-11-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>437718</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP DELVOLVE ET TRICHET</AVOCATS>
<RAPPORTEUR>Mme Mélanie Villiers</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Marc Pichon de Vendeuil</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:437718.20201106</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Manganelli Technology a demandé au juge des référés du tribunal administratif de Lille d'annuler, sur le fondement de l'article L. 551-1 du code de justice administrative, la procédure de mise en concurrence engagée par la métropole européenne de Lille en vue de l'attribution de l'accord-cadre mono-attributaire d'une durée de quarante-huit mois relatif à l'aménagement audiovisuel des bâtiments de la métropole et d'un marché subséquent n° 1 relatif à l'aménagement audiovisuel de son nouveau siège à Lille. <br/>
<br/>
              Par une ordonnance n° 1910789 du 13 janvier 2020, le juge des référés du tribunal administratif de Lille a annulé cette procédure.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et deux nouveaux mémoires, enregistrés les 17 et 30 janvier, 16 mars et 25 septembre 2020 au secrétariat du contentieux du Conseil d'Etat, la métropole européenne de Lille demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de la société Manganelli Technology ;<br/>
<br/>
              3°) de mettre à la charge de la société Manganelli Technology la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la commande publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme A... B..., maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Marc Pichon de Vendeuil, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de la métropole européenne de Lille, et à la SCP Delvolvé et Trichet, avocat de la société Manganelli Technology ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés que la métropole européenne de Lille a initié, le 18 octobre 2019, une procédure d'appel d'offres ouvert en vue de l'attribution d'un accord-cadre mono-attributaire d'une durée de quarante-huit mois, sans montant minimum ni montant maximum, relatif à l'aménagement audiovisuel des bâtiments de la métropole européenne de Lille et d'un marché subséquent n° 1 relatif à l'aménagement audiovisuel de son nouveau siège à Lille. L'offre de la société Manganelli, classée seconde, n'a pas été retenue et l'accord-cadre a été attribué au groupement formé par les sociétés Alive Technology et Elit Technologies. Par une ordonnance du 13 janvier 2020, contre laquelle la métropole européenne de Lille se pourvoit en cassation, le juge des référés du tribunal administratif de Lille, saisi sur le fondement de l'article L. 551-1 du code de justice administrative, a annulé, à la demande de la société Manganelli Technology, la procédure de passation de l'accord-cadre.<br/>
<br/>
              2. Aux termes de l'article L. 551-1 du code de justice administrative : " Le président du tribunal administratif, ou le magistrat qu'il délègue, peut être saisi en cas de manquement aux obligations de publicité et de mise en concurrence auxquelles est soumise la passation par les pouvoirs adjudicateurs de contrats administratifs ayant pour objet l'exécution de travaux, la livraison de fournitures ou la prestation de services, avec une contrepartie économique constituée par un prix (...). / Le juge est saisi avant la conclusion du contrat ". Aux termes du I de l'article L. 551-2 du même code : " Le juge peut ordonner à l'auteur du manquement de se conformer à ses obligations et suspendre l'exécution de toute décision qui se rapporte à la passation du contrat, sauf s'il estime, en considération de l'ensemble des intérêts susceptibles d'être lésés et notamment de l'intérêt public, que les conséquences négatives de ces mesures pourraient l'emporter sur leurs avantages. / Il peut, en outre, annuler les décisions qui se rapportent à la passation du contrat et supprimer les clauses ou prescriptions destinées à figurer dans le contrat et qui méconnaissent lesdites obligations ".<br/>
<br/>
              3. Aux termes de l'article L. 3 du code de la commande publique : " Les acheteurs et les autorités concédantes respectent le principe d'égalité de traitement des candidats à l'attribution d'un contrat de la commande publique. Ils mettent en oeuvre les principes de liberté d'accès et de transparence des procédures, dans les conditions définies dans le présent code (...) ".<br/>
<br/>
              4. Aux termes de l'article L. 2125-1 du code de la commande publique : " L'acheteur peut, dans le respect des règles applicables aux procédures définies au présent titre, recourir à des techniques d'achat pour procéder à la présélection d'opérateurs économiques susceptibles de répondre à son besoin ou permettre la présentation des offres ou leur sélection, selon des modalités particulières. / Les techniques d'achat sont les suivantes : / 1° L'accord-cadre, qui permet de présélectionner un ou plusieurs opérateurs économiques en vue de conclure un contrat établissant tout ou partie des règles relatives aux commandes à passer au cours d'une période donnée. (...) ". L'article R. 2162-2 du même code indique que : " Lorsque l'accord-cadre ne fixe pas toutes les stipulations contractuelles, il donne lieu à la conclusion de marchés subséquents dans les conditions fixées aux articles R. 2162-7 à R. 2162-12 ". L'article R. 2162-6 précise que : " Les marchés subséquents (...) sont conclus ou émis entre les acheteurs identifiés à cette fin dans l'avis d'appel à la concurrence, dans l'invitation à confirmer l'intérêt ou, en l'absence d'un tel avis ou d'une telle invitation, dans un autre document de la consultation, et le ou les opérateurs économiques titulaires de l'accord-cadre ". Aux termes de l'article R. 2162-7 : " Les marchés subséquents précisent les caractéristiques et les modalités d'exécution des prestations demandées qui n'ont pas été fixées dans l'accord-cadre. Ils ne peuvent entraîner des modifications substantielles des termes de l'accord-cadre ". Enfin, selon l'article R. 2162-9 : " Pour les pouvoirs adjudicateurs, lorsqu'un accord-cadre est conclu avec un seul opérateur économique, les marchés subséquents sont attribués dans les conditions fixées par l'accord-cadre. Préalablement à la conclusion des marchés subséquents, le pouvoir adjudicateur peut demander par écrit au titulaire de compléter son offre ". <br/>
<br/>
              5. Conformément aux dispositions citées au point 4, il appartient au pouvoir adjudicateur d'informer les candidats sur les conditions d'attribution des marchés subséquents à un accord-cadre mono-attributaire dès l'engagement de la procédure d'attribution de cet accord-cadre, dans l'avis d'appel public à la concurrence ou le cahier des charges tenu à la disposition des candidats. La circonstance qu'un accord-cadre soit conclu avec un seul opérateur économique n'implique pas que son titulaire bénéficie de l'octroi automatique des marchés subséquents passés dans ce cadre. Aucune disposition du code de la commande publique ni aucun principe ne fait en effet obstacle à ce que les offres remises par le titulaire d'un accord-cadre mono-attributaire pour l'attribution des marchés subséquents soient notées et analysées, et que les marchés ne lui soient attribués que sous réserve de remplir certaines conditions. Il en va de même dans l'hypothèse où la procédure de passation de l'accord-cadre mono-attributaire envisagerait l'attribution simultanée d'un premier marché subséquent et où les candidats à l'attribution de l'accord-cadre seraient de ce fait invités à remettre également une offre pour ce premier marché, sous réserve que la comparaison des offres des candidats porte uniquement sur l'accord-cadre et non, de façon concomitante, sur celles remises pour le premier marché. <br/>
<br/>
              6. Le juge des référés du tribunal administratif de Lille a annulé la procédure d'appel d'offres relative à l'attribution de l'accord-cadre mono-attributaire visant à l'aménagement audiovisuel des bâtiments de la métropole européenne de Lille et d'un marché subséquent n° 1 relatif à l'aménagement audiovisuel de son nouveau siège à Lille au seul motif que les caractéristiques de la consultation méconnaissaient le principe de transparence des procédures dès lors que le règlement de consultation comprenait, d'une part, des critères de sélection propres à l'appréciation de l'accord-cadre et, d'autre part, des critères d'appréciation à celle relatifs au marché subséquent n°1, laissant entendre que l'attribution de ce premier marché subséquent donnerait lieu à une confrontation des offres, voire que les éléments techniques et propositions financières déposés à ce titre pourraient être pris en considération par l'acheteur pour l'attribution de l'accord-cadre. Il résulte de ce qui a été dit au point précédent que le juge des référés a commis une erreur de droit en considérant qu'il était prohibé de prévoir des conditions d'attribution pour les marchés subséquents dans un accord-cadre mono-attributaire et contraire au principe de transparence de procéder à l'attribution simultanée d'un accord-cadre mono-attributaire et d'un marché subséquent. Dès lors, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'ordonnance attaquée doit être annulée.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au titre de la procédure de référé engagée, en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              8. En premier lieu, aux termes de l'article R. 2181-1 du code de la commande publique : " L'acheteur notifie sans délai à chaque candidat ou soumissionnaire concerné sa décision de rejeter sa candidature ou son offre ". Aux termes de l'article R. 2181-3 du même code : " La notification prévue à l'article R. 2181-1 mentionne les motifs du rejet de la candidature ou de l'offre. / (...) ". Aux termes de l'article R. 2181-4 du même code : " A la demande de tout soumissionnaire ayant fait une offre qui n'a pas été rejetée au motif qu'elle était irrégulière, inacceptable ou inappropriée, l'acheteur communique dans les meilleurs délais et au plus tard quinze jours à compter de la réception de cette demande : / 1° Lorsque les négociations ou le dialogue ne sont pas encore achevés, les informations relatives au déroulement et à l'avancement des négociations ou du dialogue ; / 2° Lorsque le marché a été attribué, les caractéristiques et les avantages de l'offre retenue ".<br/>
<br/>
              9. Si la société Manganelli Technology soutient que la métropole européenne de Lille a manqué à ses obligations en matière d'information des candidats évincés en omettant de l'informer des caractéristiques de l'offre retenue pour l'attribution du marché subséquent n° 1, il résulte de l'instruction que la lettre du 12 décembre 2019 par laquelle la métropole l'a informée du rejet de son offre présentée au titre de l'accord-cadre était suffisamment précise quant aux motifs de rejet de cette offre et aux caractéristiques de l'offre retenue et n'avait pas, ainsi qu'il a été relevé au point 5, à faire état de son offre au titre du marché subséquent n° 1, qui n'a pas été analysée en vue d'un classement dans le cadre de la procédure de mise en concurrence contestée. <br/>
<br/>
              10. En deuxième lieu, si la société Manganelli Technology soutient que le groupement attributaire de l'accord-cadre ne disposait pas des capacités financières, techniques et professionnelles suffisantes pour réaliser les prestations demandées, il résulte de l'instruction que ce manquement n'est pas établi. Par suite, le moyen tiré de ce que la métropole européenne de Lille aurait commis une erreur manifeste d'appréciation en admettant sa candidature doit être écarté.<br/>
<br/>
              11. En troisième lieu, il y a lieu d'écarter le moyen tiré de ce que la métropole européenne de Lille aurait méconnu le principe de transparence de la procédure en définissant des conditions d'appréciation  des marchés subséquents à l'accord-cadre mono-attributaire et en invitant les candidats à remettre simultanément une offre pour l'accord-cadre et une offre pour le marché subséquent n° 1, alors qu'il résulte de l'instruction que les deux étapes que constituaient l'attribution de l'accord-cadre mono-attributaire et l'attribution du marché subséquent n° 1 étaient clairement identifiées par les documents de la consultation et que la métropole, dont une précédente procédure relative à l'attribution du seul accord-cadre a été déclarée sans suite et qui s'est trouvée contrainte en termes de délai d'exécution, n'a pas, pour attribuer l'accord-cadre, pris en compte les offres remises pour le premier marché ni procédé à une confusion de ces deux phases. <br/>
<br/>
              12. Au surplus, et pour les mêmes motifs, il y a lieu d'écarter le moyen tiré de ce que la métropole aurait manqué aux règles de publicité et de mise en concurrence en ne tenant pas compte des conditions d'appréciation du marché subséquent n° 1 pour choisir le groupement attributaire.<br/>
<br/>
              13. Enfin, aux termes de l'article L. 2113-10 du code de la commande publique : " Les marchés sont passés en lots séparés, sauf si leur objet ne permet pas l'identification de prestations distinctes. / (...) " Aux termes de l'article L. 2113-11 du même code : " L'acheteur peut décider de ne pas allotir un marché dans l'un des cas suivants : / 1° Il n'est pas en mesure d'assurer par lui-même les missions d'organisation, de pilotage et de coordination ; / 2° La dévolution en lots séparés est de nature à restreindre la concurrence ou risque de rendre techniquement difficile ou financièrement plus coûteuse l'exécution des prestations. / Lorsqu'un acheteur décide de ne pas allotir le marché, il motive son choix en énonçant les considérations de droit et de fait qui constituent le fondement de sa décision ". <br/>
<br/>
              14. Il résulte de l'instruction que la procédure d'appel d'offre passée par la métropole européenne de Lille visait, en l'espèce, à ce que l'aménagement audiovisuel de ses bâtiments soit basé sur des solutions numériques et informatiques interconnectées entre elles et, par conséquent, rétives techniquement à tout allotissement. Par suite, la métropole européenne de Lille ne saurait être regardée comme ayant manqué à ses obligations de mise en concurrence en recourant à un marché global.<br/>
<br/>
              15. Il résulte de tout ce qui précède que la société Manganelli Technology n'est pas fondée à demander l'annulation de la procédure de passation de l'accord-cadre mono-attributaire relatif à l'aménagement audiovisuel des bâtiments de la métropole européenne de Lille.<br/>
<br/>
              16. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de la métropole européenne de Lille, qui n'est pas la partie perdante. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Manganelli Technology la somme de 5 000 euros à verser à la métropole européenne de Lille au titre de la première instance et de l'instance de cassation.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 13 janvier 2020 du juge des référés du tribunal administratif de Lille est annulée. <br/>
Article 2 : Les demandes de la société Manganelli Technology devant le juge des référés du tribunal administratif de Lille sont rejetées.<br/>
Article 3 : La société Manganelli Technology versera à la métropole européenne de Lille une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de la société Manganelli Technology présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la métropole européenne de Lille et à la société Manganelli Technology.<br/>
Copie en sera adressée aux sociétés Alive Technology et Elit Technologie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-02-005 MARCHÉS ET CONTRATS ADMINISTRATIFS. FORMATION DES CONTRATS ET MARCHÉS. FORMALITÉS DE PUBLICITÉ ET DE MISE EN CONCURRENCE. - ACCORD-CADRE MONO-ATTRIBUTAIRE - 1) OBLIGATION D'INFORMER LES CANDIDATS SUR LES CONDITIONS D'ATTRIBUTION DES MARCHÉS SUBSÉQUENTS - EXISTENCE [RJ1] - 2) OBLIGATION D'OCTROYER LES MARCHÉS SUBSÉQUENTS AU TITULAIRE - ABSENCE - 3) FACULTÉ D'ENGAGER LA PROCÉDURE DE PASSATION SIMULTANÉMENT À CELLE DU PREMIER MARCHÉ SUBSÉQUENT - EXISTENCE - CONDITIONS.
</SCT>
<ANA ID="9A"> 39-02-005 1) Conformément aux articles L. 2125-1, R. 2162-2, R. 2162-6, R. 2162-7 et R. 2162-9 du code de la commande publique (CCP), il appartient au pouvoir adjudicateur d'informer les candidats sur les conditions d'attribution des marchés subséquents à un accord-cadre mono-attributaire dès l'engagement de la procédure d'attribution de cet accord-cadre, dans l'avis d'appel public à la concurrence ou le cahier des charges tenu à la disposition des candidats.,,,2) La circonstance qu'un accord-cadre soit conclu avec un seul opérateur économique n'implique pas que son titulaire bénéficie de l'octroi automatique des marchés subséquents passés dans ce cadre. Aucune disposition du CCP ni aucun principe ne fait en effet obstacle à ce que les offres remises par le titulaire d'un accord-cadre mono-attributaire pour l'attribution des marchés subséquents soient notées et analysées, et que les marchés ne lui soient attribués que sous réserve de remplir certaines conditions.,,,3) Il en va de même dans l'hypothèse où la procédure de passation de l'accord-cadre mono-attributaire envisagerait l'attribution simultanée d'un premier marché subséquent et où les candidats à l'attribution de l'accord-cadre seraient de ce fait invités à remettre également une offre pour ce premier marché, sous réserve que la comparaison des offres des candidats porte uniquement sur l'accord-cadre et non, de façon concomitante, sur celles remises pour le premier marché.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., s'agissant d'un accord-cadre conclu sur le fondement de l'article 76 du code des marchés publics, CE, 5 juillet 2013, Union des groupements d'achats publics UGAP et Société SCC, n°s 368448 368461, T. p. 691.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
