<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039335857</ID>
<ANCIEN_ID>JG_L_2019_11_000000420979</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/33/58/CETATEXT000039335857.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 06/11/2019, 420979</TITRE>
<DATE_DEC>2019-11-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420979</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO</AVOCATS>
<RAPPORTEUR>Mme Françoise Tomé</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Raphaël Chambon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:420979.20191106</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Marseille d'annuler le titre de pension qui lui a été concédé le 14 mars 2016 et d'enjoindre au service des retraites de l'Etat de réviser le montant annuel de sa pension à compter de la date de sa radiation des cadres en tenant compte de l'indice nouveau majoré qu'il détenait au 31 mars 2016. Par un jugement n° 1603652 du 8 janvier 2018, le tribunal administratif de Marseille a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 28 mai et 28 août 2018 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros à verser à Me C..., son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
                          Vu les autres pièces du dossier ;<br/>
<br/>
                          Vu : <br/>
                      - le code des pensions civiles et militaires de retraite ;<br/>
                      - le décret n° 2003-466 du 30 mai 2003 ;<br/>
                      - le décret n° 2015-1275 du 13 octobre 2015 ;<br/>
                      - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Françoise Tomé, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me C..., avocat de M. B....<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que M.  B..., greffier des services judiciaires, occupait un emploi de greffier du premier grade et était  titulaire du 7ème échelon, doté de l'indice brut 638, jusqu'au 1er novembre 2015, date à compter de laquelle son grade a été supprimé et remplacé par le grade de greffier principal des services judiciaires en application du décret du 13 octobre 2015 fixant les nouvelles dispositions statutaires applicables au corps des greffiers des services judiciaires et abrogeant le décret du 30 mai 2003 portant statut particulier des greffiers des services judiciaires. M. B... a, en application de l'article 35 du décret du 13 octobre 2015, été reclassé, par arrêté du 15 octobre 2015, dans le grade de greffier principal des services judiciaires au 10ème échelon, doté de l'indice brut 675. Il a ensuite été radié des cadres le 1er avril 2016. M. B... se pourvoit en cassation contre le jugement du tribunal administratif de Marseille rejetant sa demande d'annulation du titre de pension qui lui a été concédé le 14 mars 2016, en tant qu'il ne tient pas compte de l'indice nouveau majoré qu'il détenait au 31 mars 2016.<br/>
<br/>
              2. Aux termes du II de l'article 35 du décret 13 octobre 2015 fixant les nouvelles dispositions statutaires applicables au corps des greffiers des services judiciaires et abrogeant le décret du 30 mai 2003 portant statut particulier des greffiers des services judiciaires :" les services accomplis dans le corps et les grades des greffiers des services judiciaires régis par le décret du 30 mai 2003 (...) sont assimilés à des services accomplis dans les corps et grades régis par le présent décret (...) ". <br/>
<br/>
              3. Aux termes du I de l'article L. 15 du code des pensions civiles et militaires de retraite : " Aux fins de liquidation de la pension, le montant de celle-ci est calculé en multipliant le pourcentage de liquidation tel qu'il résulte de l'application de l'article L. 13 par le traitement ou la solde soumis à retenue afférents à l'indice correspondant à l'emploi, grade, classe et échelon effectivement détenus depuis six mois au moins par le fonctionnaire ou militaire au moment de la cessation des services valables pour la retraite ou, à défaut, par le traitement ou la solde soumis à retenue afférents à l'emploi, grade, classe et échelon antérieurement occupés d'une manière effective (...) ". Il résulte de ces dispositions qu'un fonctionnaire ne peut légalement prétendre à ce que sa pension soit liquidée sur la base du traitement afférent au dernier indice obtenu avant sa radiation des cadres que dans la mesure où il justifie à cette date de six mois de services effectifs dans le grade, classe et échelon correspondant à cet indice. A ce titre, lorsque, dans le cadre d'une réforme statutaire, le reclassement d'un fonctionnaire dans un nouveau grade ou échelon est assorti d'une reprise d'ancienneté visant à tenir compte de l'ancienneté acquise dans le grade ou l'échelon précédent, l'ancienneté ainsi reprise n'équivaut pas à une occupation effective du nouveau grade ou échelon au sens de ces dispositions. <br/>
<br/>
              4. Il résulte de ce qui vient d'être dit que si, en application des dispositions de l'article 35 du décret du 13 octobre 2015, M. B... a été reclassé dans le grade de greffier principal des services judiciaires au 10ème échelon, doté de l'indice brut 675, avec reprise de son ancienneté, cette assimilation avait pour but de garantir la continuité de sa carrière en permettant notamment la prise en compte de ses services antérieurs mais n'équivalait pas à une occupation effective du grade ou échelon au sens des dispositions précitées de l'article L. 15 du code des pensions civiles et militaires de retraite. Il s'ensuit que le tribunal administratif de Marseille n'a pas commis d'erreur de droit en jugeant que la pension de retraite de M. B... avait pu légalement être calculée sur la base de l'indice qu'il détenait avant la réforme statutaire. Dès lors, M. B... n'est pas fondé à demander l'annulation du jugement qu'il attaque.<br/>
<br/>
              5. Les dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
            Article 1er : Le pourvoi de M. B... est rejeté. <br/>
Article 2 : La présente décision sera notifiée à M. A... B... et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">48-02-01-04 PENSIONS. PENSIONS CIVILES ET MILITAIRES DE RETRAITE. QUESTIONS COMMUNES. LIQUIDATION DES PENSIONS. - LIQUIDATION DE LA PENSION D'UN FONCTIONNAIRE DE L'ETAT SUR LA BASE DU TRAITEMENT AFFÉRENT À L'INDICE CORRESPONDANT AU GRADE OU ÉCHELON EFFECTIVEMENT DÉTENU DEPUIS SIX MOIS AU MOINS (ART. L. 15 DU CPCRM) - CALCUL DE CETTE PÉRIODE MINIMALE - ABSENCE DE PRISE EN COMPTE DE L'ANCIENNETÉ REPRISE LORS DU RECLASSEMENT D'UN FONCTIONNAIRE DANS CE GRADE OU CET ÉCHELON [RJ1], Y COMPRIS À L'OCCASION D'UNE RÉFORME STATUTAIRE [RJ2].
</SCT>
<ANA ID="9A"> 48-02-01-04 Il résulte du I de l'article L. 15 du code des pensions civiles et militaires de retraite (CPCMR) qu'un fonctionnaire ne peut légalement prétendre à ce que sa pension soit liquidée sur la base du traitement afférent au dernier indice obtenu avant sa radiation des cadres que dans la mesure où il justifie à cette date de six mois de services effectifs dans les grade, classe et échelon correspondant à cet indice. A ce titre, lorsque, dans le cadre d'une réforme statutaire, le reclassement d'un fonctionnaire dans un nouveau grade ou échelon est assorti d'une reprise d'ancienneté visant à tenir compte de l'ancienneté acquise dans le grade ou l'échelon précédent, l'ancienneté ainsi reprise n'équivaut pas à une occupation effective du nouveau grade ou échelon au sens de ces dispositions.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 6 novembre 2013, Ministère de l'économie et des finances c/ M.,, n° 365278, T. p. 729.,,[RJ2] Rappr., s'agissant du régime des pensions des collectivités territoriales (art. 17 du décret du 26 décembre 2003), CE, 4 février 2015, Mme,, n° 375181, T. p. 778.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
