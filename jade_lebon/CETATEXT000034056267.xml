<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034056267</ID>
<ANCIEN_ID>JG_L_2017_02_000000401514</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/05/62/CETATEXT000034056267.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème chambres réunies, 14/02/2017, 401514</TITRE>
<DATE_DEC>2017-02-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>401514</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON ; SCP COUTARD, MUNIER-APAIRE</AVOCATS>
<RAPPORTEUR>M. Yannick Faure</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean Lessi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:401514.20170214</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Mme B... A...a demandé au tribunal administratif de Toulon, au titre des dispositions de l'article R. 532-1 du code de justice administrative, d'ordonner une expertise à fin de déterminer les préjudices qu'elle estime avoir subis par suite de l'implantation d'une prothèse mammaire produite par la société Poly implant prothèse (PIP). Par une ordonnance n° 1600136 du 4 avril 2016, rectifiée le 13 avril 2016, le président du tribunal administratif de Nantes, à qui sa demande avait été transmise en application de l'article R. 351-3 du code de justice administrative, l'a rejetée.<br/>
<br/>
              Par un arrêt n° 16NT01255 du 30 juin 2016, la cour administrative d'appel de Nantes  a rejeté l'appel formé par Mme A...contre l'ordonnance du président du tribunal administratif de Nantes.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 15 juillet, 1er août et 22 septembre 2016 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt de la cour administrative d'appel de Nantes du 30 juin 2016 ;<br/>
<br/>
              2°) statuant en référé, de faire droit à son appel;<br/>
<br/>
              3°) de mettre à la charge de l'Agence nationale de sécurité du médicament et des produits de santé la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yannick Faure, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Jean Lessi, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boré, Salve de Bruneton, avocat de Mme A...et à la SCP Coutard, Munier-Apaire, avocat de l'Agence nationale de sécurité du médicament et des produits de santé.<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 511-1 du code de justice administrative : " Le juge des référés statue par des mesures qui présentent un caractère provisoire. Il n'est pas saisi du principal et se prononce dans les meilleurs délais " ; qu'aux termes du premier alinéa de l'article R. 532-1 du même code : " Le juge des référés peut, sur simple requête et même en l'absence de décision administrative préalable, prescrire toute mesure utile d'expertise ou d'instruction " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges des référés que Mme A...a demandé au juge des référés du tribunal administratif de Toulon, au titre de ces dispositions, d'ordonner une expertise à fin de déterminer les préjudices qu'elle estime avoir subis par suite de l'implantation le 21 mai 2005, à des fins reconstructrices après une mastectomie, d'une prothèse mammaire produite par la société Poly implant prothèse, explantée le 25 avril 2012, en vue de mettre ultérieurement en cause la responsabilité de l'Etat en raison de carences fautives de l'Agence française de sécurité sanitaire des produits de santé ; que, par une ordonnance du 4 avril 2016, rectifiée le 13 avril suivant, le président du tribunal administratif de Nantes, à qui sa demande avait été transmise, l'a rejetée en jugeant que la mesure sollicitée ne présentait pas de caractère utile ; que, par un arrêt du 30 juin 2016, contre lequel Mme A...se pourvoit en cassation, la cour administrative d'appel de Nantes a rejeté l'appel qu'elle avait formé contre cette ordonnance ;<br/>
<br/>
              3. Considérant que l'utilité d'une mesure d'instruction ou d'expertise qu'il est demandé au juge des référés d'ordonner sur le fondement de l'article R. 532-1 du code de justice administrative doit être appréciée, d'une part, au regard des éléments dont le demandeur dispose ou peut disposer par d'autres moyens et, d'autre part, bien que ce juge ne soit pas saisi du principal, au regard de l'intérêt que la mesure présente dans la perspective d'un litige principal, actuel ou éventuel, auquel elle est susceptible de se rattacher ; qu'à ce dernier titre, il ne peut faire droit à une demande d'expertise lorsque, en particulier, elle est formulée à l'appui de prétentions qui ne relèvent manifestement pas de la compétence de la juridiction administrative, qui sont irrecevables ou qui se heurtent à la prescription ; que, de même, il ne peut faire droit à une demande d'expertise permettant d'évaluer un préjudice, en vue d'engager la responsabilité d'une personne publique, en l'absence manifeste de lien de causalité entre le préjudice à évaluer et la faute alléguée de cette personne ;<br/>
<br/>
              4. Considérant que la cour a estimé que la demande d'expertise présentée par Mme A...ne présentait pas le caractère d'utilité exigé par l'article R. 532-1 du code de justice administrative au motif que les éléments du dossier ne permettaient pas, en l'état, d'établir de manière suffisante l'existence d'un lien de causalité entre les préjudices à évaluer et la carence alléguée de l'Agence française de sécurité sanitaire des produits de santé, aux droits de laquelle vient l'Agence nationale de sécurité du médicament et des produits de santé, en vue d'engager la responsabilité de la personne publique en raison d'une telle carence ; qu'en statuant ainsi, alors qu'elle ne pouvait rejeter une telle demande pour défaut d'utilité qu'en l'absence manifeste d'un tel lien de causalité, la cour a commis une erreur de droit ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que Mme A...est fondée, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, à demander l'annulation de l'arrêt qu'elle attaque ; <br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au titre de la procédure de référé engagée, en application des dispositions de l'article L. 821-2 du code de justice administrative, et de statuer sur la requête d'appel de MmeA... ;<br/>
<br/>
              7. Considérant qu'il résulte de l'instruction que, d'une part, si la responsabilité de l'Etat en raison de la carence alléguée de l'Agence française de sécurité sanitaire des produits de santé fait l'objet d'une contestation sérieuse, qu'il n'appartient pas au juge des référés de trancher, la mesure d'expertise sollicitée, portant sur la détermination des préjudices subis par Mme A...à la suite de l'implantation d'une prothèse mammaire de la société Poly implant prothèse, est insusceptible d'éclairer le juge du principal sur l'existence d'une telle carence ; que, d'autre part, l'existence de préjudices résultant, pour MmeA..., de l'implantation de cette prothèse, au demeurant pour partie indemnisés par un arrêt de la cour d'appel d'Aix-en-Provence du 2 mai 2016 et non contestés par le ministre des affaires sociales et de la santé ni par l'Agence nationale de sécurité du médicament et des produits de santé, paraît en l'état de l'instruction  établie, sans que la réunion d'éléments plus précis d'évaluation de ces préjudices soit utile à l'engagement d'un litige devant le juge du principal ; qu'au regard de cette double circonstance, la mesure d'expertise sollicitée ne présente pas de caractère utile au sens de l'article R. 532-1 du code de justice administrative ;<br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que Mme A...n'est pas fondée à se plaindre de ce que, par l'ordonnance attaquée, le président du tribunal administratif de Nantes a rejeté sa demande ; <br/>
<br/>
              9. Considérant que si Mme A...demande désormais au Conseil d'Etat d'ordonner une expertise à fin de recueillir tous éléments de fait propres à établir une éventuelle carence de l'Agence française de sécurité sanitaire des produits de santé, au regard des informations dont elle disposait, dans sa mission de surveillance et de contrôle à l'égard des dispositifs médicaux produits par la société Poly implant prothèse, ces conclusions ont le caractère d'une demande nouvelle et sont, par suite, irrecevables ;<br/>
<br/>
              10. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Agence nationale de sécurité du médicament et des produits de santé qui n'est pas, dans la présente instance, la partie perdante ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme A...une somme au titre de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                           --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nantes du 30 juin 2016 est annulé.<br/>
Article 2 : La requête présentée par Mme A...devant la cour administrative d'appel de Nantes et le surplus de ses conclusions présentées devant le Conseil d'Etat sont rejetés.<br/>
Article 3 : Les conclusions de l'Agence nationale de sécurité du médicament et des produits de santé présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à MmeB... A..., à la ministre des affaires sociales et de la santé et à l'Agence nationale de sécurité du médicament et des produits de santé.<br/>
Copie en sera adressée à la MGEN de Loire-Atlantique.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-03-011-04 PROCÉDURE. PROCÉDURES DE RÉFÉRÉ AUTRES QUE CELLES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ TENDANT AU PRONONCÉ D'UNE MESURE D'EXPERTISE OU D'INSTRUCTION. CONDITIONS. - APPRÉCIATION DE L'UTILITÉ DE LA MESURE DEMANDÉE [RJ1] - DEMANDE D'EXPERTISE TENDANT À L'ÉVALUATION D'UN PRÉJUDICE ALORS QUE L'ABSENCE DE LIEN DE CAUSALITÉ ENTRE LE PRÉJUDICE À ÉVALUER ET LA FAUTE ALLÉGUÉE EST MANIFESTE - ABSENCE D'UTILITÉ.
</SCT>
<ANA ID="9A"> 54-03-011-04 L'utilité d'une mesure d'instruction ou d'expertise qu'il est demandé au juge des référés d'ordonner sur le fondement de l'article R. 532-1 du code de justice administrative doit être appréciée, d'une part, au regard des éléments dont le demandeur dispose ou peut disposer par d'autres moyens et, d'autre part, bien que ce juge ne soit pas saisi du principal, au regard de l'intérêt que la mesure présente dans la perspective d'un litige principal, actuel ou éventuel, auquel elle est susceptible de se rattacher. A ce dernier titre, il ne peut faire droit à une demande d'expertise lorsque, en particulier, elle est formulée à l'appui de prétentions qui ne relèvent manifestement pas de la compétence de la juridiction administrative, qui sont irrecevables ou qui se heurtent à la prescription. De même, il ne peut faire droit à une demande d'expertise permettant d'évaluer un préjudice, en vue d'engager la responsabilité d'une personne publique, en l'absence manifeste de lien de causalité entre le préjudice à évaluer et la faute alléguée de cette personne.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. CE, Section, 11 février 2005, Organisme de gestion du cours du Sacré coeur et autres, n° 259290, p. 65.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
