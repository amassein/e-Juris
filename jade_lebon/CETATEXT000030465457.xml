<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030465457</ID>
<ANCIEN_ID>JG_L_2015_04_000000378595</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/46/54/CETATEXT000030465457.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère SSJS, 09/04/2015, 378595</TITRE>
<DATE_DEC>2015-04-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>378595</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP THOUIN-PALAT, BOUCARD</AVOCATS>
<RAPPORTEUR>M. Yannick Faure</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:378595.20150409</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante : <br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Cergy-Pontoise d'annuler la décision du 21 septembre 2010 par laquelle le président du conseil général du Val-d'Oise a refusé de lui accorder une remise de dette d'un montant de 2 769,96 euros, correspondant à un indu de revenu de solidarité active. Par un jugement n° 1009074 du 27 septembre 2012, le tribunal administratif de Cergy-Pontoise, après avoir transmis à la commission départementale d'aide sociale du Val-d'Oise des conclusions relatives à un indu de revenu minimum d'insertion, a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 12VE03904 du 1er avril 2014, la cour administrative d'appel de Versailles a rejeté l'appel formé contre l'article 2 du jugement du tribunal administratif de Cergy-Pontoise par M.B....<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 24 avril et 24 juillet 2014 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt de la cour administrative d'appel de Versailles du 1er avril 2014 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros à verser à la SCP Thouin-Palat, Boucard, son avocat, au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier.<br/>
<br/>
              Vu :<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le décret n° 91-1266 du 19 décembre 1991 ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yannick Faure, auditeur,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouin-Palat, Boucard, avocat de M.B..., et à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat du département du Val-d'Oise.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. D'une part, aux termes de l'article R. 411-1 du code de justice administrative : " La juridiction est saisie par requête. La requête (...) contient l'exposé des faits et moyens, ainsi que l'énoncé des conclusions soumises au juge. / L'auteur d'une requête ne contenant l'exposé d'aucun moyen ne peut la régulariser par le dépôt d'un mémoire exposant un ou plusieurs moyens que jusqu'à l'expiration du délai de recours ".<br/>
<br/>
              2. D'autre part, la loi du 10 juillet 1991 relative à l'aide juridique prévoit, à son article 2, que les personnes physiques dont les ressources sont insuffisantes pour faire valoir leurs droits en justice peuvent bénéficier d'une aide juridictionnelle et, à son article 25, que le bénéficiaire de l'aide juridictionnelle a droit à l'assistance d'un avocat choisi par lui ou, à défaut, désigné par le bâtonnier de l'ordre des avocats. Il résulte des articles 76 et 77 du décret du 19 décembre 1991 portant application de cette loi que si la personne qui demande l'aide juridictionnelle ne produit pas de document attestant l'acceptation d'un avocat choisi par elle, l'avocat peut être désigné sur-le-champ par le représentant de la profession qui siège au bureau d'aide juridictionnelle, à condition qu'il ait reçu délégation du bâtonnier à cet effet. Enfin, l'article 39 du même décret dispose que, lorsque l'aide juridictionnelle a été sollicitée à l'occasion d'une instance devant une juridiction administrative statuant à charge de recours devant le Conseil d'Etat avant l'expiration du délai imparti pour le dépôt des mémoires, ce délai est interrompu et " un nouveau délai court à compter du jour de la réception par l'intéressé de la notification de la décision du bureau d'aide juridictionnelle ou, si elle est plus tardive, de la date à laquelle un auxiliaire de justice a été désigné ".<br/>
<br/>
              3. Il résulte de l'ensemble de ces dispositions que les cours administratives d'appel peuvent rejeter, après l'expiration des délais de recours, les requêtes qui ne contiennent l'exposé d'aucun moyen. Toutefois, si le requérant, qui a demandé l'aide juridictionnelle avant l'expiration du délai de recours, a obtenu la désignation d'un avocat à ce titre et si cet avocat n'a pas produit de mémoire, le juge d'appel ne peut, afin d'assurer au requérant le bénéfice effectif du droit qu'il tire de la loi du 10 juillet 1991, rejeter la requête sans avoir préalablement mis l'avocat désigné en demeure d'accomplir, dans un délai qu'il détermine, les diligences qui lui incombent et porté cette carence à la connaissance du requérant, afin de le mettre en mesure, le cas échéant, de choisir un autre représentant.<br/>
<br/>
              4. Il ressort des énonciations de l'arrêt attaqué que, le 26 novembre 2012, par une requête non motivée, M. B...a relevé appel du jugement du tribunal administratif de Versailles, puis a formé le 27 novembre suivant, avant l'expiration du délai d'appel, une demande tendant au bénéfice de l'aide juridictionnelle. Par une décision du 24 mai 2013, notifiée le 3 juillet suivant, le bureau d'aide juridictionnelle près le tribunal de grande instance de Versailles a fait droit à sa demande et désigné un avocat pour le représenter. Cet avocat n'a pas régularisé l'appel de M. B...par la production d'un mémoire motivé avant l'expiration du nouveau délai d'appel qui avait couru à compter du 3 juillet 2014. En en déduisant que l'appel de M.B..., faute de motivation de sa requête introductive d'instance dans le délai de recours, était irrecevable, alors qu'il lui appartenait de mettre l'avocat désigné en demeure d'accomplir les diligences qui lui incombaient et de porter cette carence à la connaissance du requérant, la cour a commis une erreur de droit. <br/>
<br/>
              5. Il résulte de ce qui précède que M. B...est fondé à demander l'annulation de l'arrêt qu'il attaque. En revanche, l'Etat n'étant pas partie à la présente instance, les dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 font obstacle à ce qu'il soit fait droit aux conclusions présentées sur leur fondement par la SCP Thouin-Palat, Boucard, avocat de M.B....<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Versailles du 1er avril 2014 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Versailles.<br/>
Article 3 : Les conclusions présentées par la SCP Thouin-Palat, Boucard, avocat de M.B..., au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à M. A...B...et au département du Val-d'Oise.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-01-08-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. FORMES DE LA REQUÊTE. OBLIGATION DE MOTIVER LA REQUÊTE. - REJET APRÈS L'EXPIRATION DES DÉLAIS DE RECOURS DES REQUÊTES NON MOTIVÉES (ART. R. 411-1 DU CJA) - REQUÉRANT AYANT OBTENU LA DÉSIGNATION D'UN AVOCAT AU TITRE DE L'AIDE JURIDICTIONNELLE - OBLIGATION POUR LE JUGE DE METTRE PRÉALABLEMENT L'AVOCAT DÉSIGNÉ EN DEMEURE, ET DE PORTER LA CARENCE À LA CONNAISSANCE DU REQUÉRANT - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-06-05-09 PROCÉDURE. JUGEMENTS. FRAIS ET DÉPENS. AIDE JURIDICTIONNELLE. - REJET APRÈS L'EXPIRATION DES DÉLAIS DE RECOURS DES REQUÊTES NON MOTIVÉES (ART. R. 411-1 DU CJA) - REQUÉRANT AYANT OBTENU LA DÉSIGNATION D'UN AVOCAT AU TITRE DE L'AIDE JURIDICTIONNELLE - OBLIGATION POUR LE JUGE DE METTRE PRÉALABLEMENT L'AVOCAT DÉSIGNÉ EN DEMEURE, ET DE PORTER LA CARENCE À LA CONNAISSANCE DU REQUÉRANT - EXISTENCE.
</SCT>
<ANA ID="9A"> 54-01-08-01 Les cours administratives d'appel peuvent rejeter, après l'expiration des délais de recours, les requêtes qui ne contiennent l'exposé d'aucun moyen. Toutefois, si le requérant qui a demandé l'aide juridictionnelle avant l'expiration du délai de recours a obtenu la désignation d'un avocat à ce titre et si cet avocat n'a pas produit de mémoire, le juge d'appel ne peut, afin d'assurer au requérant le bénéfice effectif du droit qu'il tire de la loi du 10 juillet 1991, rejeter la requête sans avoir préalablement mis l'avocat désigné en demeure d'accomplir, dans un délai qu'il détermine, les diligences qui lui incombent et porté cette carence à la connaissance du requérant, afin de le mettre en mesure, le cas échéant, de choisir un autre représentant.</ANA>
<ANA ID="9B"> 54-06-05-09 Les cours administratives d'appel peuvent rejeter, après l'expiration des délais de recours, les requêtes qui ne contiennent l'exposé d'aucun moyen. Toutefois, si le requérant qui a demandé l'aide juridictionnelle avant l'expiration du délai de recours a obtenu la désignation d'un avocat à ce titre et si cet avocat n'a pas produit de mémoire, le juge d'appel ne peut, afin d'assurer au requérant le bénéfice effectif du droit qu'il tire de la loi du 10 juillet 1991, rejeter la requête sans avoir préalablement mis l'avocat désigné en demeure d'accomplir, dans un délai qu'il détermine, les diligences qui lui incombent et porté cette carence à la connaissance du requérant, afin de le mettre en mesure, le cas échéant, de choisir un autre représentant.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
