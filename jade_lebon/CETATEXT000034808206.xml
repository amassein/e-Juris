<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034808206</ID>
<ANCIEN_ID>JG_L_2017_05_000000389667</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/80/82/CETATEXT000034808206.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 1ère chambres réunies, 24/05/2017, 389667</TITRE>
<DATE_DEC>2017-05-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389667</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLEZ</AVOCATS>
<RAPPORTEUR>Mme Mireille Le Corre</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:389667.20170524</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Paris d'annuler pour excès de pouvoir la décision du 14 février 2012 par laquelle le conseil médical de l'aéronautique civile a constaté que l'affection motivant son inaptitude à exercer la profession de navigant n'était pas imputable au service aérien. Par un jugement n° 1207949/6-3 du 30 avril 2014, le tribunal administratif de Paris a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 14PA02895 du 19 février 2015, la cour administrative d'appel de Paris a rejeté l'appel présenté par M. B...contre ce jugement.<br/>
<br/>
              Par un pourvoi, un mémoire complémentaire et un mémoire en réplique, enregistrés les 20 avril 2015, 21 juillet 2015 et 19 avril 2016 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt de la cour administrative d'appel de Paris ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 8 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              -	le code de l'aviation civile ;<br/>
              -	le code des transports ; <br/>
              -	le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Mireille Le Corre, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boullez, avocat de M.B....<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 5 octobre 2011, le conseil médical de l'aéronautique civile a déclaré M. B..., membre du personnel navigant commercial de la société Air France, inapte définitivement à exercer cette profession ; que par une décision du 14 février 2012, le conseil médical de l'aéronautique civile a constaté que l'affection motivant son inaptitude à exercer la profession de navigant n'était pas imputable au service aérien ; que par un jugement du 30 avril 2014, le tribunal administratif de Paris a rejeté la demande de M. B...tendant à l'annulation de la décision du 14 février 2012 ; que M. B...se pourvoit en cassation contre l'arrêt du 19 février 2015 par lequel la cour administrative d'appel de Paris a rejeté son appel contre ce jugement ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 6526-5 du code des transports : " Lorsqu'un accident aérien survenu en service ou lorsqu'une maladie imputable au service et reconnue comme telle par la commission mentionnée à l'article L. 6511-4 ont entraîné le décès, ou une incapacité permanente totale au sens de la législation relative à la réparation des accidents du travail, une indemnité en capital est versée à l'intéressé ou à ses ayants droit par la caisse créée en application de l'article L. 6527-2. / Est considéré comme accident aérien tout accident du travail survenu à bord d'un aéronef. Un décret en Conseil d'Etat définit les événements ou les circonstances, directement liés au transport aérien ou à la formation des personnels navigants, assimilables à des accidents aériens. (...). " ; qu'aux termes de l'article L. 6526-6 du même code : " Si l'incapacité résultant des causes mentionnées à l'article L. 6526-5 entraîne seulement l'inaptitude permanente à exercer la profession de navigant, la caisse de retraites verse à l'intéressé une somme en capital " ; <br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions qu'est considéré comme un accident aérien survenu en service susceptible d'ouvrir droit au bénéfice de l'indemnité en capital prévue par les dispositions citées ci-dessus un accident du travail survenu à bord d'un aéronef ; qu'en revanche, le bénéfice de cette même indemnité n'est pas subordonnée, en cas de maladie imputable au service, à la condition qu'elle ait été contractée à bord d'un aéronef ; qu'il suit de là qu'en jugeant que la maladie d'un membre du personnel navigant d'une compagnie aéronautique ne peut être regardée comme imputable au service au sens de ces dispositions, dès lors qu'elle a été contractée au cours de ses escales professionnelles dans la zone d'endémie de cette maladie, et donc implicitement mais nécessairement au seul motif qu'elle n'a pas été contractée à bord d'un aéronef, la cour administrative d'appel de Paris a commis une erreur de droit ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède, et sans qu'il soit besoin de statuer sur les autres moyens du pourvoi, que M. B...est fondé à demander l'annulation de l'arrêt qu'il attaque ; <br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à M. B...au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 19 février 2015 est annulé.<br/>
<br/>
      Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br/>
<br/>
Article 3 : L'Etat versera la somme de 3 000 euros à M. B...au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. A...B...et au ministre d'Etat, ministre de la transition écologique et solidaire.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">65-03-01-01 TRANSPORTS. TRANSPORTS AÉRIENS. PERSONNELS. PERSONNELS DES COMPAGNIES AÉRIENNES. - DROIT À INDEMNITÉ EN CAS D'INCAPACITÉ PERMANENTE TOTALE - CONDITIONS - 1) ACCIDENT AÉRIEN SURVENU EN SERVICE (ART. L. 424-5 DU CODE DE L'AVIATION CIVILE, DEVENU L. 6526-5 DU CODE DES TRANSPORTS) - NOTION - ACCIDENT SURVENU À BORD D'UN AÉRONEF - EXISTENCE - 2) MALADIE IMPUTABLE AU SERVICE - NOTION - MALADIE CONTRACTÉE À BORD D'UN AÉRONEF - ABSENCE [RJ1] - ESPÈCE.
</SCT>
<ANA ID="9A"> 65-03-01-01 1) Il résulte des articles L. 6526-5 et L. 6526-6 du code des transports qu'est considéré comme un accident aérien survenu en service susceptible d'ouvrir droit au bénéfice de l'indemnité en capital prévue par ces articles un accident du travail survenu à bord d'un aéronef.... ,,2) En revanche, le bénéfice de cette même indemnité n'est pas subordonné, en cas de maladie imputable au service, à la condition qu'elle ait été contractée à bord d'un aéronef. La maladie contractée par un membre du personnel navigant d'une compagnie aéronautique au cours de ses escales professionnelles dans la zone d'endémie de cette maladie doit donc être regardée comme imputable au service au sens de ces articles.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Ab. jur. CE, 7 avril 1976, Secrétaire d'Etat aux transports c/,, n° 99306, p. 191 ; Rappr., s'agissant des agents publics en mission, Section, 3 décembre 2004,,, n° 260786, p. 448.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
