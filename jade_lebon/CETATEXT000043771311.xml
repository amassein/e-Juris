<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043771311</ID>
<ANCIEN_ID>JG_L_2021_07_000000440246</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/77/13/CETATEXT000043771311.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 07/07/2021, 440246</TITRE>
<DATE_DEC>2021-07-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440246</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Manon Chonavel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:440246.20210707</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire en réplique et deux nouveaux mémoires, enregistrés les 24 avril 2020, 3 mars, 21 mai et 9 juin 2021 au secrétariat du contentieux du Conseil d'Etat, les sociétés Centre spécialités pharmaceutiques et Proveca Pharma Limited demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 29 octobre 2019 du ministre des solidarités et de la santé et du ministre de l'action et des comptes publics en tant qu'elle rejette la demande d'inscription de la spécialité pharmaceutique Sialanar 320 mcg/ml, solution buvable, sur la liste des spécialités pharmaceutiques remboursables mentionnée à l'article L. 162-17 du code de la sécurité sociale, ainsi que la décision rejetant le recours gracieux de la première requérante ;<br/>
<br/>
              2°) d'enjoindre aux ministres compétents de réexaminer cette demande dans un délai de quinze jours à compter de la décision à intervenir ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 10 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ; <br/>
              - le code de la sécurité sociale ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme A... B..., auditrice,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. La société Centre spécialités pharmaceutiques commercialise en France, pour le compte de la société Proveca Pharma Limited, titulaire de l'autorisation de mise sur le marché obtenue par une procédure européenne centralisée spécifique en vue d'un usage pédiatrique exclusif, la spécialité pharmaceutique Sialanar, 320 mcg/ml, solution buvable. La société Alloga France, qui commercialisait alors cette spécialité en France, a demandé l'inscription de cette spécialité sur la liste des spécialités pharmaceutiques remboursables prévue à l'article L. 162-17 du code de la sécurité sociale et sur la liste des médicaments agréés à l'usage des collectivités publiques prévue à l'article L. 5123-2 du code de la santé publique, dans l'indication du traitement symptomatique de la sialorrhée sévère (salivation pathologique chronique) chez les enfants âgés de 3 ans et plus et les adolescents atteints de troubles neurologiques chroniques. Dans un avis du 3 octobre 2018, la commission de la transparence de la Haute autorité de santé a reconnu, d'une part, un service médical rendu modéré à la spécialité Sialanar dans l'indication, restreinte par rapport à celle de son autorisation de mise sur le marché, d'un traitement de deuxième intention après échec de la rééducation et, d'autre part, une absence d'amélioration du service médical rendu dans la prise en charge de la sialorrhée sévère de l'enfant à partir de 3 ans et de l'adolescent. Par une décision du 29 octobre 2019, les ministres compétents ont inscrit la spécialité, dans l'indication préconisée par la commission de la transparence, sur la liste des médicaments agréés à l'usage des collectivités publiques, mais non sur celle des spécialités pharmaceutiques remboursables. Les sociétés Centre spécialités pharmaceutiques et Proveca Pharma Limited demandent l'annulation pour excès de pouvoir de cette décision et du rejet du recours gracieux de la première requérante, dans la mesure du refus d'inscription opposé.  <br/>
<br/>
              Sur le cadre juridique : <br/>
<br/>
              2. En vertu de l'article L. 162-17 du code de la sécurité sociale, les spécialités pharmaceutiques ne peuvent être prises en charge ou donner lieu à remboursement par les caisses d'assurance maladie, lorsqu'elles sont dispensées en officine, que si elles figurent sur une liste établie, selon l'article R. 163-2 du même code, par arrêté conjoint du ministre chargé de la santé et du ministre chargé de la sécurité sociale. Aux termes du I de l'article R. 163-5 du code de la sécurité sociale, dans sa rédaction alors applicable : " Ne peuvent être inscrits sur la liste prévue au premier alinéa de l'article L. 162-17 : / (...) 2° Les médicaments qui n'apportent ni amélioration du service médical rendu appréciée par la commission mentionnée à l'article R. 163-15 [c'est-à-dire la commission de la transparence] ni économie dans le coût du traitement médicamenteux ; / 3° Les médicaments susceptibles d'entraîner des hausses de consommation ou des dépenses injustifiées ; / 4° Les médicaments dont le prix proposé par l'entreprise ne serait pas justifié eu égard aux critères prévus au premier alinéa de l'article L. 162-16-4 ". Ce dernier article prévoit que : " (...) La fixation de ce prix tient compte principalement de l'amélioration du service médical rendu par le médicament, le cas échéant des résultats de l'évaluation médico-économique, des prix des médicaments à même visée thérapeutique, des volumes de vente prévus ou constatés ainsi que des conditions prévisibles et réelles d'utilisation du médicament ". <br/>
<br/>
              3. Il résulte de ces dispositions que si les 2°, 3° et 4° de l'article R. 163-5 du code de la sécurité sociale prévoient chacun un motif de refus d'inscription sur la liste mentionnée à l'article L. 162-17 du même code fondé sur l'objectif de maîtrise des dépenses de santé, ils se rapportent en principe à des situations distinctes. En vertu du 2°, un refus doit être opposé à la demande d'inscription d'un médicament qui n'apporte aucune amélioration du service médical rendu, telle qu'elle est appréciée par la commission de la transparence, lorsqu'il ne permet pas à l'assurance maladie, eu égard au prix fixé pour cette spécialité, de réaliser une économie dans le coût du traitement médicamenteux par rapport à celui d'une spécialité déjà inscrite sur la liste. En vertu du 3°, un refus doit de même être opposé à la demande d'inscription d'une spécialité qui est susceptible d'entraîner des hausses de consommation ou des dépenses injustifiées pour l'assurance maladie au regard de son utilité pour la santé publique, eu égard par exemple aux charges d'exploitation afférentes, à ses conditions de forme, de dosage ou de présentation ou aux conditions dans lesquelles elle est prescrite. Enfin, en vertu du 4°, un refus doit être opposé si le prix proposé par l'entreprise qui exploite le médicament en vue de la conclusion avec le Comité économique des produits de santé de la convention fixant ce prix prévue à l'article L. 162-16-4 n'est pas justifié au regard des critères fixés à cet article. <br/>
<br/>
              Sur le litige :<br/>
<br/>
              4. La décision attaquée indique que le refus d'inscription se fonde tant sur le 3° que sur le 4° de l'article R. 163-5 du code de la sécurité sociale et le ministre des solidarités et de la santé fait valoir qu'elle était, en outre, légalement justifiée sur le fondement du 2° de cet article. D'une part, il résulte de ce qui a été dit au point 3 que ce 2° n'était pas susceptible de constituer la base légale de ce refus dès lors qu'il ressort des pièces du dossier qu'aucun prix n'était encore fixé pour la spécialité en cause, de sorte que les ministres n'étaient pas en mesure d'apprécier si l'inscription de cette spécialité permettrait de réaliser une économie dans le coût du traitement médicamenteux par rapport à celui d'une spécialité déjà inscrite sur la liste. D'autre part, le 3° n'était pas davantage susceptible de fonder la décision de refus d'inscription dès lors que les motifs avancés ne caractérisaient pas en eux-mêmes un risque de hausse de consommation ou de dépenses injustifiées pour l'assurance maladie. Partant, seul le 4° de cet article était susceptible de constituer le fondement légal de ce refus. Il revenait à ce titre aux ministres d'apprécier si le prix proposé par l'entreprise en vue de la conclusions d'une convention avec le Comité économique des produits de santé était justifié eu égard aux critères prévus au premier alinéa de l'article L. 162-16-4.<br/>
<br/>
              5. En l'espèce, d'une part, s'agissant de l'absence d'amélioration du service médical rendu prise en considération, il ressort des pièces du dossier que les ministres se sont appropriés l'avis de la commission de la transparence. Cette dernière a reconnu l'existence d'un besoin médical en l'absence de spécialité disposant d'une autorisation de mise sur le marché dans la prise en charge de la sialorrhée sévère de l'enfant à partir de 3 ans et l'adolescent, en particulier en cas d'échec de la rééducation. Mais elle a estimé que la spécialité Sialanar n'apportait pas d'amélioration du service médical rendu dans cette indication eu égard aux limites méthodologiques identifiées dans les études versus placebo, à la difficulté de transposer les données ainsi obtenues à la pratique, aux incertitudes sur la tolérance de cette spécialité, du fait de données limitées à court terme et de l'absence de données à long terme, ainsi qu'à l'absence de démonstration d'amélioration de la qualité de vie des patients. Les sociétés requérantes, qui se bornent à rappeler l'impact de la sialorrhée sur la qualité de vie et l'absence d'autre spécialité pharmaceutique disposant d'une autorisation de mise sur le marché dans cette indication, ne sont pas fondées à soutenir que les ministres auraient commis une erreur manifeste d'appréciation en retenant que la spécialité Sialanar n'apportait pas d'amélioration du service médical rendu. <br/>
<br/>
              6. D'autre part, s'agissant du prix des médicaments à même visée thérapeutique, également pris en considération, il ressort des pièces du dossier que les patchs de scopolamine commercialisés sous le nom de C... sont utilisés en pratique courante, en dehors des indications de leur autorisation de mise sur le marché, dans la prise en charge de la sialorrhée sévère de l'enfant à partir de 3 ans et de l'adolescent après échec de la rééducation et occupent ainsi la même place dans la stratégie thérapeutique que la spécialité Sialanar. Si les ministres pouvaient ainsi retenir cette spécialité comme un comparateur pertinent de cette dernière au sens de l'article L. 162-16-4 du code de la sécurité sociale, il leur appartenait toutefois, pour apprécier le caractère justifié du prix proposé par l'entreprise par rapport aux prix de médicaments à même visée thérapeutique, de déterminer, sur la base de critères objectifs et vérifiables, la méthode de comparaison des prix la plus adaptée aux caractéristiques des spécialités en cause. Il ressort des pièces du dossier, en particulier des pièces versées par le ministre des solidarités et de la santé en réponse à la mesure d'instruction diligentée par la 1ère chambre de la section du contentieux, que la comparaison qui a été opérée entre les coûts de traitement journaliers des spécialités C... et Sialanar n'a pris en compte ni la circonstance que Sialanar n'est, à la différence de C..., recommandé que pour une utilisation intermittente à court terme, ni les différences sensibles de population cible entre les deux spécialités, ni les imprécisions quant au protocole d'administration et quant aux posologies des spécialités comparées. En conséquence, la décision attaquée a retenu que la prise en charge de Sialanar " entraînerait une hausse comprise entre 38 % et 1 200 % du coût de traitement journalier d'un patient selon la dose administrée ", alors en outre que la spécialité C..., qui figure sur la seule liste des médicaments agréés à l'usage des collectivités et non sur la liste des médicaments remboursables, ne dispose d'aucun prix de vente au public fixé en application de l'article L. 162-16-4 du code de la sécurité sociale par voie de convention ou de manière unilatérale par le Comité économique des produits de santé. <br/>
<br/>
              7. Par suite, en se fondant sur ces éléments pour estimer que le prix proposé par l'entreprise n'était pas justifié au regard des critères prévus au premier alinéa de l'article L. 162-16-4 du code de la sécurité sociale, les ministres ont entaché leur décision d'illégalité. Les sociétés requérantes sont ainsi fondées, sans qu'il soit besoin de se prononcer sur les autres moyens de leur requête, à demander l'annulation de la décision qu'elles attaquent, ainsi que de la décision rejetant le recours gracieux de la société Centre spécialités pharmaceutiques.<br/>
<br/>
              8. Il y a lieu d'enjoindre au ministre des solidarités et de la santé et au ministre de l'économie, des finances et de la relance de réexaminer, dans un délai de deux mois à compter de la notification de la présente décision, la demande d'inscription de la spécialité Sialanar, dans l'indication du traitement symptomatique de la sialorrhée sévère chez les enfants âgés de 3 ans et plus et les adolescents atteints de troubles neurologiques chroniques, sur la liste des spécialités pharmaceutiques mentionnée à l'article L. 162-17 du code de la sécurité sociale.<br/>
<br/>
              9. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 1 500 euros à verser à la société Centre spécialités pharmaceutiques et une même somme à verser à la société Proveca au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Sont annulées la décision du 29 octobre 2019 du ministre des solidarités et de la santé et du ministre de l'action et des comptes publics, en tant qu'elle rejette la demande d'inscription de la spécialité pharmaceutique Sialanar 320 mcg/ml, solution buvable, sur la liste des spécialités pharmaceutiques remboursables mentionnée à l'article L. 162-17 du code de la sécurité sociale, ainsi que la décision rejetant le recours gracieux de la société Centre spécialités pharmaceutiques.  <br/>
Article 2 : Il est enjoint au ministre des solidarités et de la santé et au ministre de l'économie, des finances et de la relance de réexaminer, dans un délai de deux mois à compter de la notification de la présente décision, la demande d'inscription de la spécialité Sialanar, dans l'indication du traitement symptomatique de la sialorrhée sévère chez les enfants âgés de 3 ans et plus et les adolescents atteints de troubles neurologiques chroniques, sur la liste des spécialités pharmaceutiques remboursables mentionnée à l'article L. 162-17 du code de la sécurité sociale. <br/>
Article 3 : L'Etat versera une somme de 1 500 euros à la société Centre spécialités pharmaceutiques et à la société Proveca au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la société Centre spécialités pharmaceutiques, première dénommée, pour les deux sociétés requérantes, au ministre des solidarités et de la santé et au ministre de l'économie, des finances et de la relance.<br/>
Copie en sera adressée à la Haute autorité de santé et à la section du rapport et des études.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">61-04-01-023 SANTÉ PUBLIQUE. PHARMACIE. PRODUITS PHARMACEUTIQUES. - REFUS D'INSCRIPTION SUR LA LISTE DES SPÉCIALITÉS PHARMACEUTIQUES REMBOURSABLES EN VILLE (1ER AL. DE L'ART. L. 162-17 DU CSS) - MOTIFS (I DE L'ART. R. 163-5) - 1) ABSENCE D'ASMR ET ABSENCE D'ÉCONOMIE (2°) - 2) HAUSSES DE CONSOMMATION OU DES DÉPENSES INJUSTIFIÉES (3°) - 3) PRIX PROPOSÉ PAR L'ENTREPRISE NON JUSTIFIÉ AU REGARD DES CRITÈRES FIXÉS PAR L'ARTICLE L. 162-16-4 (4°).
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">62-04-01 SÉCURITÉ SOCIALE. PRESTATIONS. PRESTATIONS D'ASSURANCE MALADIE. - REFUS D'INSCRIPTION SUR LA LISTE DES SPÉCIALITÉS PHARMACEUTIQUES REMBOURSABLES EN VILLE (1ER AL. DE L'ART. L. 162-17 DU CSS) - MOTIFS (I DE L'ART. R. 163-5) - 1) ABSENCE D'ASMR ET ABSENCE D'ÉCONOMIE (2°) - 2) HAUSSES DE CONSOMMATION OU DES DÉPENSES INJUSTIFIÉES (3°) - 3) PRIX PROPOSÉ PAR L'ENTREPRISE NON JUSTIFIÉ AU REGARD DES CRITÈRES FIXÉS PAR L'ARTICLE L. 162-16-4 (4°).
</SCT>
<ANA ID="9A"> 61-04-01-023 Si les 2°, 3° et 4° de l'article R. 163-5 du code de la sécurité sociale (CSS) prévoient chacun un motif de refus d'inscription sur la liste mentionnée au premier alinéa de l'article L. 162-17 du même code fondé sur l'objectif de maîtrise des dépenses de santé, ils se rapportent en principe à des situations distinctes.... ,,1) En vertu du 2°, un refus doit être opposé à la demande d'inscription d'un médicament qui n'apporte aucune amélioration du service médical rendu (ASMR), telle qu'elle est appréciée par la commission de la transparence, lorsqu'il ne permet pas à l'assurance maladie, eu égard au prix fixé pour cette spécialité, de réaliser une économie dans le coût du traitement médicamenteux par rapport à celui d'une spécialité déjà inscrite sur la liste.... ,,2) En vertu du 3°, un refus doit de même être opposé à la demande d'inscription d'une spécialité qui est susceptible d'entraîner des hausses de consommation ou des dépenses injustifiées pour l'assurance maladie au regard de son utilité pour la santé publique, eu égard par exemple aux charges d'exploitation afférentes, à ses conditions de forme, de dosage ou de présentation ou aux conditions dans lesquelles elle est prescrite.... ,,3) Enfin, en vertu du 4°, un refus doit être opposé si le prix proposé par l'entreprise qui exploite le médicament en vue de la conclusion avec le comité économique des produits de santé (CEPS) de la convention fixant ce prix prévue à l'article L. 162-16-4 n'est pas justifié au regard des critères fixés à cet article.</ANA>
<ANA ID="9B"> 62-04-01 Si les 2°, 3° et 4° de l'article R. 163-5 du code de la sécurité sociale (CSS) prévoient chacun un motif de refus d'inscription sur la liste mentionnée au premier alinéa de l'article L. 162-17 du même code fondé sur l'objectif de maîtrise des dépenses de santé, ils se rapportent en principe à des situations distinctes.... ,,1) En vertu du 2°, un refus doit être opposé à la demande d'inscription d'un médicament qui n'apporte aucune amélioration du service médical rendu (ASMR), telle qu'elle est appréciée par la commission de la transparence, lorsqu'il ne permet pas à l'assurance maladie, eu égard au prix fixé pour cette spécialité, de réaliser une économie dans le coût du traitement médicamenteux par rapport à celui d'une spécialité déjà inscrite sur la liste.... ,,2) En vertu du 3°, un refus doit de même être opposé à la demande d'inscription d'une spécialité qui est susceptible d'entraîner des hausses de consommation ou des dépenses injustifiées pour l'assurance maladie au regard de son utilité pour la santé publique, eu égard par exemple aux charges d'exploitation afférentes, à ses conditions de forme, de dosage ou de présentation ou aux conditions dans lesquelles elle est prescrite.... ,,3) Enfin, en vertu du 4°, un refus doit être opposé si le prix proposé par l'entreprise qui exploite le médicament en vue de la conclusion avec le comité économique des produits de santé (CEPS) de la convention fixant ce prix prévue à l'article L. 162-16-4 n'est pas justifié au regard des critères fixés à cet article.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
