<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035803983</ID>
<ANCIEN_ID>JG_L_2017_10_000000400563</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/80/39/CETATEXT000035803983.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 13/10/2017, 400563</TITRE>
<DATE_DEC>2017-10-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>400563</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Catherine Bobo</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:400563.20171013</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête enregistrée le 10 juin 2016 au secrétariat du contentieux du Conseil d'Etat, la société Isa Média Développement demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la délibération du 9 décembre 2015 par laquelle le Conseil supérieur de l'audiovisuel (CSA), a émis, en application de l'article 42-12 de la loi du 30 septembre 1986, un avis défavorable sur son offre de reprise du service de radio dénommé Vitamine, ainsi que la décision implicite par laquelle il a rejeté son recours gracieux ; <br/>
<br/>
              2°) d'enjoindre au CSA de réexaminer son offre dans un délai de quinze jours à compter de la notification de la décision du Conseil d'Etat ;<br/>
<br/>
              3°) de mettre à la charge du CSA la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le code des relations entre le public et l'administration ;<br/>
<br/>
              - la loi n° 86-1067 du 30 septembre 1986 ;<br/>
<br/>
              - la loi n° 2000-321 du 12 avril 2000 ;<br/>
<br/>
              - le décret n° 2010-709 du 28 juin 2010 ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Catherine Bobo, auditeur,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article 42-12 de la loi du 30 septembre 1986 relative à la liberté de communication : " Lorsqu'un débiteur soumis à une procédure de sauvegarde, de redressement judiciaire ou de liquidation judiciaire est titulaire d'une autorisation relative à un service de communication audiovisuelle et que la cession d'une activité ou de l'entreprise est envisagée dans les conditions prévues aux articles L. 626-1, L. 631-22 ou L. 642-1 et suivants du code de commerce, le tribunal peut, à la demande du procureur de la République et après que ce magistrat a obtenu, dans un délai d'un mois, l'avis favorable du Conseil supérieur de l'audiovisuel, dans des conditions prévues par décret, autoriser la conclusion d'un contrat de location-gérance conformément aux articles L. 642-13 et suivants du code de commerce " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier que les personnes morales autorisées à exploiter un service de radio par voie hertzienne terrestre en mode numérique dénommé " Vitamine " ayant été placées en liquidation judiciaire, le procureur de la République près le tribunal de grande instance de Marseille a consulté le Conseil supérieur de l'audiovisuel (CSA), en application des dispositions législatives précitées, sur trois offres de reprise ; que, par une délibération du 9 décembre 2015, le CSA a rendu un avis défavorable sur chacune de ces offres ; que la société ISA Média Développement demande l'annulation, d'une part, de cette délibération en tant qu'elle concerne l'offre qu'elle avait présentée et, d'autre part, de la décision implicite par laquelle le CSA a rejeté le recours gracieux dont elle l'avait saisi le 11 février 2016 ; <br/>
<br/>
              Sur la délibération du 9 décembre 2015 :<br/>
<br/>
              En ce qui concerne la légalité externe :<br/>
<br/>
              3. Considérant qu'aux termes du deuxième alinéa de l'article 4 de la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations, en vigueur à la date de la délibération attaquée : " Toute décision prise par l'une des autorités administratives mentionnées à l'article 1er comporte, outre la signature de son auteur, la mention, en caractères lisibles, du prénom, du nom et de la qualité de celui-ci " ; que la lettre du 14 décembre 2015 notifiant au procureur de la République près le tribunal de grande instance de Marseille l'avis du CSA sur les projets de reprise porte la signature du président du conseil supérieur et mentionne son nom et son prénom ; que sa qualité est indiquée dans l'en-tête de la lettre ; que, dès lors, le moyen tiré de la violation des dispositions précitées doit être écarté ;<br/>
<br/>
              En ce qui concerne la légalité interne :<br/>
<br/>
              4. Considérant qu'aux termes de l'article 3 du décret du 28 juin 2010 portant application de l'article 42-12 de la loi du 30 septembre 1986 : " Le Conseil supérieur de l'audiovisuel donne son avis au regard, notamment, des critères énumérés à l'article 29 de la loi du 30 septembre 1986 susvisée " ; que l'article 29 de la loi, qui fixe les critères d'octroi des autorisations d'exploiter des services de radio, mentionne notamment " le financement et les perspectives d'exploitation du service " ; <br/>
<br/>
              5. Considérant que, pour émettre un avis défavorable sur l'offre de reprise présentée par la société ISA Média Développement, le CSA, après avoir relevé qu'aucun plan d'affaires n'avait été présenté par cette société, a estimé que la viabilité économique et la pérennité de son projet n'étaient pas établies ; qu'il s'est ainsi fondé sur le critère du financement et des perspectives d'exploitation du service, qui était au nombre de ceux qu'il lui appartenait de prendre en considération ; que si la société fait valoir qu'aucune disposition n'impose la présentation d'un plan d'affaires à l'appui d'une offre de reprise soumise au CSA, il appartient aux auteurs de telles offres d'apporter des éléments permettant d'en apprécier la viabilité économique ; qu'en l'absence, dans le dossier qui lui avait été transmis, de tout élément relatif à la viabilité du projet de la société ISA Média Développement, c'est sans commettre d'erreur de droit ni d'erreur d'appréciation que le conseil supérieur a émis un avis défavorable à ce projet ; <br/>
<br/>
              Sur la décision rejetant le recours gracieux de la société ISA Média Développement :<br/>
<br/>
              6. Considérant qu'il ressort des pièces du dossier qu'à l'appui du recours gracieux qu'elle a présenté le 11 février 2016 contre l'avis défavorable émis le 9 décembre 2015, la société ISA Média Développement a présenté des éléments relatifs à la viabilité économique de son projet ; que son recours a été rejeté par une décision implicite née le 11 avril 2016 ; que la société a, le 4 mai 2016, demandé à avoir communication des motifs de cette décision ; que, par une lettre du 15 juin 2016, le président du CSA a indiqué que celui-ci avait estimé, alors que Radio Vitamine avait cessé d'émettre et que son personnel avait été licencié, que la fréquence utilisée par ce service devait, dans l'intérêt du public et compte tenu de l'impératif de pluralisme, être attribuée après appel à candidatures ; qu'à l'appui de ses conclusions dirigées contre la décision rejetant son recours gracieux, la société requérante soulève un unique moyen, tiré de ce que cette décision n'a pas été motivée dans les conditions prévues par l'article L. 232-4 du code des relations entre le public et l'administration ; <br/>
<br/>
              7. Considérant qu'aux termes de l'article L. 211-2 du code des relations entre le public et l'administration : " Les personnes physiques et morales ont le droit d'être informées sans délai des motifs des décisions administratives individuelles défavorables qui les concernent./ A cet effet, doivent être motivées les décisions qui : (...) 7° Refusent une autorisation (...) " ; qu'il résulte des dispositions de l'article 42-12 de la loi du 30 septembre 1986 cité au point 1 que l'avis défavorable émis par le CSA sur un projet de reprise des parts sociales ou du capital d'une société titulaire d'une autorisation relative à un service de communication audiovisuelle interdit au tribunal de commerce d'autoriser le candidat à cette reprise à conclure un contrat de location-gérance en vue de l'exploitation du service en cause ; que, dans ces conditions, cet avis doit être assimilé, au sens des dispositions précitées, à une décision refusant une autorisation ; que le CSA était, par suite, tenu de motiver l'avis défavorable qu'il a émis le 9 décembre 2015 sur le projet de reprise présenté par la société ISA Média Développement ; que, dès lors que le recours gracieux de cette société reposait sur un élément nouveau, la décision rejetant ce recours était elle-même soumise à l'obligation de motivation, dont la société peut utilement invoquer la méconnaissance ; <br/>
<br/>
              8. Considérant qu'aux termes de l'article L. 232-4 du code des relations entre le public et l'administration : " Une décision implicite intervenue dans les cas où la décision explicite aurait dû être motivée n'est pas illégale du seul fait qu'elle n'est pas assortie de cette motivation. / Toutefois, à la demande de l'intéressé, formulée dans les délais du recours contentieux, les motifs de toute décision implicite de rejet devront lui être communiquées dans le mois de cette demande. (...) " ; qu'ainsi qu'il a été dit, le recours gracieux de la société requérante a été rejeté par une décision implicite ; que, saisi le 4 mai 2016 d'une demande tendant à la communication des motifs de cette décision, le CSA n'a répondu que par un courrier du 15 juin 2016 ; que, faute pour lui d'en avoir communiqué les motifs dans le délai d'un mois prévu par les dispositions précitées, sa décision implicite doit être annulée ; <br/>
<br/>
              9. Considérant qu'il résulte de tout ce qui précède que la société ISA Média Développement est seulement fondée à demander l'annulation de la décision implicite par laquelle le CSA a rejeté son recours gracieux contre l'avis émis le 9 décembre 2015 ;<br/>
<br/>
              10. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge du CSA la somme que la société requérante demande au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La décision implicite du CSA rejetant le recours gracieux présenté par la société ISA Média Développement contre l'avis émis le 9 décembre 2015 est annulée.<br/>
<br/>
Article 2 : Le surplus des conclusions de la requête de la société ISA Média Développement est rejeté.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la société ISA Média Développement et au Conseil supérieur de l'audiovisuel.<br/>
		Copie en sera adressée à la ministre de la culture.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-03-01-02-01-01-06 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. QUESTIONS GÉNÉRALES. MOTIVATION. MOTIVATION OBLIGATOIRE. MOTIVATION OBLIGATOIRE EN VERTU DES ARTICLES 1 ET 2 DE LA LOI DU 11 JUILLET 1979. DÉCISION REFUSANT UNE AUTORISATION. - AVIS DÉFAVORABLE DU CSA SUR LA CESSION D'UNE ACTIVITÉ OU D'UNE ENTREPRISE DANS LE CADRE D'UNE PROCÉDURE DE SAUVEGARDE, DE REDRESSEMENT JUDICIAIRE OU DE LIQUIDATION D'UNE SOCIÉTÉ TITULAIRE D'UNE AUTORISATION (ART. 42-12 DE LA LOI DU 30 SEPTEMBRE 1986) - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">56-01 RADIO ET TÉLÉVISION. CONSEIL SUPÉRIEUR DE L'AUDIOVISUEL. - AVIS DÉFAVORABLE SUR LA CESSION D'UNE ACTIVITÉ OU D'UNE ENTREPRISE DANS LE CADRE D'UNE PROCÉDURE DE SAUVEGARDE, DE REDRESSEMENT JUDICIAIRE OU DE LIQUIDATION D'UNE SOCIÉTÉ TITULAIRE D'UNE AUTORISATION (ART. 42-12 DE LA LOI DU 30 SEPTEMBRE 1986) - OBLIGATION DE MOTIVATION - EXISTENCE.
</SCT>
<ANA ID="9A"> 01-03-01-02-01-01-06 Il résulte de l'article 42-12 de la loi n° 86-1067 du 30 septembre 1986 que l'avis défavorable émis par le Conseil supérieur de l'audiovisuel (CSA) sur un projet de reprise des parts sociales ou du capital d'une société titulaire d'une autorisation relative à un service de communication audiovisuelle interdit au tribunal de commerce d'autoriser le candidat à cette reprise à conclure un contrat de location-gérance en vue de l'exploitation du service en cause. Dans ces conditions, cet avis doit être assimilé, au sens des dispositions de l'article L. 211-2 du code des relations entre le public et l'administration, à une décision refusant une autorisation. Par suite, le CSA est tenu de motiver un tel avis défavorable.</ANA>
<ANA ID="9B"> 56-01 Il résulte de l'article 42-12 de la loi n° 86-1067 du 30 septembre 1986 que l'avis défavorable émis par le Conseil supérieur de l'audiovisuel (CSA) sur un projet de reprise des parts sociales ou du capital d'une société titulaire d'une autorisation relative à un service de communication audiovisuelle interdit au tribunal de commerce d'autoriser le candidat à cette reprise à conclure un contrat de location-gérance en vue de l'exploitation du service en cause. Dans ces conditions, cet avis doit être assimilé, au sens des dispositions de l'article L. 211-2 du code des relations entre le public et l'administration, à une décision refusant une autorisation. Par suite, le CSA est tenu de motiver l'avis défavorable.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
