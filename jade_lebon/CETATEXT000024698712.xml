<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000024698712</ID>
<ANCIEN_ID>JG_L_2011_10_000000339207</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/24/69/87/CETATEXT000024698712.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 21/10/2011, 339207</TITRE>
<DATE_DEC>2011-10-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>339207</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ORTSCHEIDT ; SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Philippe Josse</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Claire Legras</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2011:339207.20111021</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 4 mai et 29 juillet 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour la BANQUE DELUBAC et CIE, dont le siège est 16 place Saléon-Terras au Cheylard (07160) ; la BANQUE DELUBAC et CIE demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 15 février 2010, notifiée le 8 mars 2010, par laquelle la Commission bancaire a décidé de lui enjoindre de détenir des fonds propres d'un montant supérieur au montant minimal prévu par la réglementation applicable afin de respecter, au plus tard à compter du 31 mars 2010, un ratio minimum de solvabilité sur fonds propres de base de 10 % ;<br/>
<br/>
              2°) de mettre à la charge de l'Autorité de contrôle prudentiel, venant aux droits de la Commission bancaire, la somme de 7 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la directive 2006/48/CE du Parlement européen et du Conseil du 14 juin 2006 concernant l'accès à l'activité des établissements de crédits et son exercice ;<br/>
<br/>
              Vu le code monétaire et financier ;<br/>
<br/>
              Vu la loi n° 2000-321 du 12 avril 2000 ;<br/>
<br/>
              Vu l'ordonnance n° 2010-76 du 21 janvier 2010 ;<br/>
<br/>
              Vu le règlement n° 90-02 du 23 février 1990 relatif aux fonds propres des établissements de crédits et l'arrêté du 20 février 2007 relatif aux exigences de fonds propres applicables aux établissements de crédit et aux entreprises d'investissement ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Philippe Josse, Conseiller d'Etat,  <br/>
<br/>
              - les observations de la SCP Ortscheidt, avocat de la BANQUE DELUBAC et CIE et de la SCP Rocheteau, Uzan-Sarano, avocat de l'Autorité de contrôle prudentiel, <br/>
<br/>
              - les conclusions de Mme Claire Legras, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Ortscheidt, avocat de la BANQUE DELUBAC et CIE et à la SCP Rocheteau, Uzan-Sarano, avocat de l'Autorité de contrôle prudentiel ;<br/>
<br/>
<br/>
<br/>Considérant que la BANQUE DELUBAC et CIE demande l'annulation de la décision du 15 février 2010, notifiée le 8 mars 2010, par laquelle la Commission bancaire lui a enjoint, en application du troisième alinéa de l'article L. 613-16 du code monétaire et financier, dans sa rédaction alors en vigueur par l'effet du I de l'article 22 de l'ordonnance du 21 janvier 2010 portant fusion des autorités d'agrément et de contrôle de la banque et de l'assurance, de détenir des fonds propres d'un montant supérieur au montant minimal prévu par la réglementation applicable afin de respecter, au plus tard à compter du 31 mars 2010, un ratio minimum de solvabilité sur fonds propres de base de 10 % ;<br/>
<br/>
              Sur la légalité externe de la décision attaquée :<br/>
<br/>
              Considérant, en premier lieu, qu'aux termes de l'article 1er de la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations : "Sont considérés comme autorités administratives au sens de la présente loi les administrations de l'Etat, les collectivités territoriales, les établissements publics à caractère administratif, les organismes de sécurité sociale et les autres organismes chargés de la gestion d'un service public administratif" ; qu'aux termes du second alinéa de l'article 4 de la même loi : "Toute décision prise par l'une des autorités administratives mentionnées à l'article 1er comporte, outre la signature de son auteur, la mention, en caractères lisibles, du prénom, du nom et de la qualité de celui-ci" ; que la Commission bancaire, qui est une autorité administrative au sens de l'article 1er de la loi du 12 avril 2000, est en conséquence soumise aux prescriptions de l'article 4 de cette loi ; que, s'agissant d'une autorité de caractère collégial, il est satisfait aux exigences qui en découlent dès lors que les décisions que prend la Commission portent la signature de son président, accompagnée des mentions, en caractères lisibles, prévues par cet article ; qu'il ressort des pièces du dossier que l'original de la décision attaquée, constitué du procès-verbal de la séance du 15 février 2010 de la Commission bancaire, répond à ces exigences ; que la circonstance que la lettre du 8 mars 2010 notifiant la décision du 15 février 2010 ait été signée par le secrétaire général et non par le président de la Commission est sans incidence sur la légalité de cette décision ; que, par suite, le moyen tiré de la méconnaissance des dispositions de l'article 4 de la loi du 12 avril 2000 doit être écarté ;<br/>
<br/>
              Considérant, en deuxième lieu, qu'aux termes de l'article 24 de la loi du 12 avril 2000 précitée : "Exception faite des cas où il est statué sur une demande, les décisions individuelles qui doivent être motivées en application des articles 1er et 2 la loi n° 79-587 du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public n'interviennent qu'après que la personne intéressée a été mise à même de présenter des observations écrites et, le cas échéant, sur sa demande, des observations orales (...)" ; que ces dispositions n'imposent pas que la personne à l'égard de laquelle il est envisagé de prendre une décision, même lorsqu'elle le sollicite, soit entendue par l'autorité investie du pouvoir de décision elle-même ou, s'agissant d'une autorité collégiale, par l'un de ses membres, dès lors que celle-ci, avant de se prononcer, prend connaissance des observations écrites et orales formulées par l'intéressé ; qu'il ressort des pièces du dossier que les dirigeants de la BANQUE DELUBAC et CIE ont été invités, par lettre du 24 juillet 2009 du secrétaire général adjoint de la Commission bancaire, à faire valoir leurs observations écrites, ce qu'ils ont d'ailleurs fait par lettre en date du 30 octobre 2009, et avisés de ce qu'ils pouvaient, s'ils le souhaitaient, être entendus par le chef du service des établissements indépendants, de gestion privée et monégasques, ou par son adjoint ; qu'en réponse à un courrier du conseil de la société requérante, le secrétaire général de la Commission a réitéré cette dernière proposition par lettre du 27 octobre 2009 ; que si la société requérante n'a pas fait usage de la faculté qui lui était ainsi offerte, elle a été mise à même de présenter des observations orales à un membre des services de la Commission bancaire disposant des compétences et de l'autorité nécessaires, à charge pour celui-ci d'en rendre compte de façon fidèle aux membres de la Commission bancaire ; que, par suite, la BANQUE DELUBAC et CIE n'est pas fondée à soutenir que, faute d'avoir pu être entendue par un membre de la Commission bancaire, les dispositions de l'article 24 de la loi du 12 avril 2000 auraient été méconnues ;<br/>
<br/>
              Considérant, en dernier lieu, que le moyen tiré de ce que l'audition par le secrétariat général de la Commission, proposée à la banque requérante, rendrait la procédure suivie contraire au principe selon lequel la même personne ne peut à la fois instruire des poursuites et statuer sur les faits concernés ne peut qu'être écarté s'agissant de l'exercice, par la Commission bancaire, des pouvoirs qu'elle tient de l'article L. 613-16 du code monétaire et financier, qui ne fixe pas un régime de sanction disciplinaire ; qu'au surplus la décision attaquée a été prise par la Commission bancaire et non par son secrétariat général ;<br/>
<br/>
              Sur la légalité interne de la décision attaquée :<br/>
<br/>
              Considérant qu'aux termes de l'article L. 613-16 du code monétaire et financier, alors en vigueur : "La Commission bancaire peut adresser à un établissement de crédit, un établissement de paiement et aux personnes mentionnées à l'article L. 613-2 une recommandation de prendre les mesures appropriées pour restaurer ou renforcer leur situation financière, améliorer leurs méthodes de gestion ou assurer l'adéquation de leur organisation à leurs activités ou à leurs objectifs de développement. L'établissement concerné est tenu de répondre dans un délai de deux mois en détaillant les mesures prises à la suite de cette recommandation. / La Commission bancaire peut, indépendamment des dispositions prévues à l'alinéa précédent, adresser à tout établissement de crédit, tout établissement de paiement, toute entreprise ou toute personne soumise à son contrôle en application de l'article L. 613-2 une injonction à l'effet notamment de prendre dans un délai déterminé toutes mesures destinées à restaurer ou renforcer sa situation financière, à améliorer ses méthodes de gestion ou à assurer l'adéquation de son organisation à ses activités ou à ses objectifs de développement. / La Commission bancaire peut en particulier enjoindre à ces établissements, entreprises ou personnes de détenir des fonds propres d'un montant supérieur au montant minimal prévu par la réglementation applicable et exiger d'eux qu'ils appliquent à leurs actifs une politique spécifique de provisionnement ou un traitement spécifique au regard des exigences de fonds propres. Elle peut aussi leur enjoindre de restreindre ou de limiter à titre temporaire leur activité (...)" ;<br/>
<br/>
              Considérant que, sur le fondement des dispositions du troisième alinéa précité de l'article L. 613-16 du code monétaire et financier, la Commission bancaire peut adresser une injonction à un établissement lorsque les informations dont elle dispose font apparaître la nécessité de restaurer ou renforcer sa situation financière, compte tenu des risques encourus tant par cet établissement que, le cas échéant et de son fait, par d'autres établissements ;<br/>
<br/>
              Considérant, en premier lieu, que si le règlement du comité de la réglementation bancaire et financière n° 90-02 du 23 février 1990 relatif aux fonds propres inclut dans la définition de ceux-ci tant les fonds propres de base que, sous certaines réserves, les fonds propres complémentaires, il ne fait pas obstacle à ce que la Commission bancaire, en application de l'article L. 613-16 du code monétaire et financier et compte tenu de l'objet même de la surveillance prudentielle instaurée par celui-ci, qui suppose le contrôle de la qualité des fonds propres aussi bien que celui de leur niveau arithmétique, puisse légalement enjoindre à un établissement de détenir un montant minimal de fonds propres de base, dès lors que ce montant est adapté à la nécessité de restaurer ou de renforcer la situation financière de l'établissement ; que, par suite, la société requérante n'est pas fondée à soutenir que la Commission bancaire aurait entaché sa décision d'erreur de droit en lui enjoignant de respecter un ratio minimum de solvabilité sur fonds propres de base ;<br/>
<br/>
              Considérant, en deuxième lieu, que la Commission bancaire a motivé l'injonction faite à la BANQUE DELUBAC et CIE de détenir des fonds propres d'un montant supérieur au montant minimal prévu par la réglementation applicable pour respecter, au plus tard à compter du 31 mars 2010, un ratio minimum de solvabilité sur fonds propres de base de 10 %, tout d'abord, dans un contexte de forte croissance des crédits à la clientèle, par la concentration de ces engagements sur des concours à des entreprises en procédure collective et par la proportion significative de ceux d'entre eux "assortis d'une cote péjorative par la Banque de France", ensuite par un taux de créances douteuses demeurant à un niveau élevé et, enfin, par une rentabilité d'exploitation insuffisante, la rentabilité nette étant ainsi assurée pour une part significative par l'enregistrement d'éléments non récurrents ;<br/>
<br/>
              Considérant que la société requérante fait valoir que les crédits qu'elle accorde à des entreprises en difficulté le sont sous forme d'affacturage, d'escompte ou de cession de créances consentie sur le fondement de l'article L. 313-23 du code monétaire et financier ; qu'ainsi, son risque de crédit n'est pas différent de celui d'une banque qui ferait crédit à une entreprise qui serait dans une situation normale, puisque les créances qu'elle détient ont pour débiteurs des clients d'entreprises en difficulté et non ces entreprises elles-mêmes ; que, si la circonstance que la garantie d'entreprises en difficulté soit plus difficilement appelée en cas de défaillance du débiteur cédé, pour incontestable qu'elle soit, n'implique pas en elle-même un risque supérieur à celui que peut entraîner un crédit non assorti d'une cession de créance, la Commission bancaire a pu légalement fonder sa décision sur les motifs tirés de ce que les crédits consentis par la banque requérante étaient en expansion rapide, que son coefficient d'exploitation courante témoignait d'une rentabilité d'exploitation insuffisante et que son taux de créances douteuses était supérieur à la moyenne ; que si la BANQUE DELUBAC et CIE se prévaut du contrat d'assurance de portefeuille qu'elle a conclu auprès d'une société d'assurance crédit, celui-ci assurait une somme d'un montant très inférieur aux crédits ainsi couverts ; qu'au regard de l'ensemble de ces éléments, la Commission bancaire n'a pas commis d'erreur de droit et n'a pas fait une appréciation inexacte de la situation de la BANQUE DELUBAC et CIE en lui enjoignant de respecter, au plus tard à compter du 31 mars 2010, un ratio minimum de solvabilité sur fonds propres de base de 10 % ;<br/>
<br/>
              Considérant que la circonstance que cette société avait jusque là enregistré un niveau de fonds propres supérieur est sans incidence sur la légalité de la décision attaquée ;<br/>
<br/>
              Considérant, enfin, que la société requérante ne soutient pas que l'Etat n'aurait pas pris, dans les délais impartis par ce texte, les mesures nécessaires à la transposition de la directive 2006/48/CE du Parlement européen et du Conseil du 14 juin 2006 concernant l'accès à l'activité des établissements de crédits et son exercice ; que, par suite, elle n'est, en tout état de cause, pas fondée à se prévaloir, à l'appui de son recours dirigé contre la décision attaquée, qui est dépourvue de caractère réglementaire, des dispositions de cette directive ;<br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que la BANQUE DELUBAC et CIE n'est pas fondée à demander l'annulation de la décision qu'elle attaque ;<br/>
<br/>
              Sur les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de l'Autorité de contrôle prudentiel venant aux droits de la Commission bancaire, qui n'est pas, dans la présente instance, la partie perdante, la somme que demande la BANQUE DELUBAC et CIE au titre des frais exposés par elle et non compris dans les dépens ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de la BANQUE DELUBAC et CIE le versement à l'Autorité de contrôle prudentiel de la somme que celle-ci demande au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de la BANQUE DELUBAC et CIE est rejetée.<br/>
Article 2 : Les conclusions présentées par l'Autorité de contrôle prudentiel au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à la BANQUE DELUBAC et CIE et à l'Autorité de contrôle prudentiel.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-03-03-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. PROCÉDURE CONTRADICTOIRE. MODALITÉS. - OBLIGATION DE PRENDRE CONNAISSANCE DES OBSERVATIONS ÉCRITES ET ORALES DE LA PERSONNE À L'ÉGARD DE LAQUELLE IL EST ENVISAGÉ DE PRENDRE UNE DÉCISION (ARTICLE 24 DE LA LOI DCRA) - 1) NÉCESSITÉ QUE LA PERSONNE SOIT ENTENDUE PAR L'AUTORITÉ INVESTIE DU POUVOIR DE DÉCISION ELLE-MÊME OU, S'AGISSANT D'UNE AUTORITÉ COLLÉGIALE, PAR L'UN DE SES MEMBRES - ABSENCE - 2) ESPÈCE - ETABLISSEMENT BANCAIRE INVITÉ À FAIRE VALOIR SES OBSERVATIONS ÉCRITES ET MIS À MÊME DE PRÉSENTER DES OBSERVATIONS ORALES À UN MEMBRE DES SERVICES DE LA COMMISSION BANCAIRE - CONSÉQUENCE - MÉCONNAISSANCE DE L'ARTICLE 24 - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">13-04-01 CAPITAUX, MONNAIE, BANQUES. BANQUES. COMMISSION BANCAIRE. - 1) OBLIGATION DE PRENDRE CONNAISSANCE DES OBSERVATIONS ÉCRITES ET ORALES DE LA PERSONNE À L'ÉGARD DE LAQUELLE IL EST ENVISAGÉ DE PRENDRE UNE DÉCISION (ARTICLE 24 DE LA LOI DCRA) - A) NÉCESSITÉ QUE LA PERSONNE SOIT ENTENDUE PAR L'AUTORITÉ INVESTIE DU POUVOIR DE DÉCISION ELLE-MÊME OU, S'AGISSANT D'UNE AUTORITÉ COLLÉGIALE, PAR L'UN DE SES MEMBRES - ABSENCE - B) ESPÈCE - ETABLISSEMENT BANCAIRE INVITÉ À FAIRE VALOIR SES OBSERVATIONS ÉCRITES ET MIS À MÊME DE PRÉSENTER DES OBSERVATIONS ORALES À UN MEMBRE DES SERVICES DE LA COMMISSION BANCAIRE - CONSÉQUENCE - MÉCONNAISSANCE DE L'ARTICLE 24 - ABSENCE - 2) POSSIBILITÉ D'EXIGER LE RESPECT D'UN RATIO SUR FONDS PROPRES DE BASE - EXISTENCE.
</SCT>
<ANA ID="9A"> 01-03-03-03 1) Les dispositions de l'article 24 de la loi n° 2000-321 du 12 avril 2000 (loi DCRA) n'imposent pas que la personne à l'égard de laquelle il est envisagé de prendre une décision, même lorsqu'elle le sollicite, soit entendue par l'autorité investie du pouvoir de décision elle-même ou, s'agissant d'une autorité collégiale, par l'un de ses membres, dès lors que celle-ci, avant de se prononcer, prend connaissance des observations écrites et orales formulées par l'intéressé.... ...2) En l'espèce, les dirigeants d'un établissement bancaire ont été invités à faire valoir leurs observations écrites et été avisés de ce qu'ils pouvaient, s'ils le souhaitaient, être entendus par le chef du service des établissements indépendants, de gestion privée et monégasques, ou par son adjoint. Si la société n'a pas fait usage de la faculté qui lui était ainsi offerte, elle a été mise à même de présenter des observations orales à un membre des services de la Commission bancaire disposant des compétences et de l'autorité nécessaires, à charge pour celui-ci d'en rendre compte de façon fidèle aux membres de la Commission bancaire. Par suite, absence de méconnaissance des dispositions de l'article 24 de la loi DCRA.</ANA>
<ANA ID="9B"> 13-04-01 1) a) Les dispositions de l'article 24 de la loi n° 2000-321 du 12 avril 2000 (loi DCRA) n'imposent pas que la personne à l'égard de laquelle il est envisagé de prendre une décision, même lorsqu'elle le sollicite, soit entendue par l'autorité investie du pouvoir de décision elle-même ou, s'agissant d'une autorité collégiale, par l'un de ses membres, dès lors que celle-ci, avant de se prononcer, prend connaissance des observations écrites et orales formulées par l'intéressé. b) En l'espèce, les dirigeants d'un établissement bancaire ont été invités à faire valoir leurs observations écrites et été avisés de ce qu'ils pouvaient, s'ils le souhaitaient, être entendus par le chef du service des établissements indépendants, de gestion privée et monégasques, ou par son adjoint. Si la société n'a pas fait usage de la faculté qui lui était ainsi offerte, elle a été mise à même de présenter des observations orales à un membre des services de la Commission bancaire disposant des compétences et de l'autorité nécessaires, à charge pour celui-ci d'en rendre compte de façon fidèle aux membres de la Commission bancaire. Par suite, absence de méconnaissance des dispositions de l'article 24 de la loi DCRA.,,2) Sur le fondement des dispositions du troisième alinéa de l'article L. 613-16 du code monétaire et financier, la Commission bancaire peut adresser une injonction à un établissement lorsque les informations dont elle dispose font apparaître la nécessité de restaurer ou renforcer sa situation financière, compte tenu des risques encourus tant par cet établissement que, le cas échéant et de son fait, par d'autres établissements. Si le règlement du comité de la réglementation bancaire et financière n° 90-02 du 23 février 1990 relatif aux fonds propres inclut dans la définition de ceux-ci tant les fonds propres de base que, sous certaines réserves, les fonds propres complémentaires, il ne fait pas obstacle à ce que la Commission bancaire, en application de l'article L. 613-16, et compte tenu de l'objet même de la surveillance prudentielle instaurée par celui-ci, qui suppose le contrôle de la qualité des fonds propres aussi bien que celui de leur niveau arithmétique, puisse légalement enjoindre à un établissement de détenir un montant minimal de fonds propres de base, dès lors que ce montant est adapté à la nécessité de restaurer ou de renforcer la situation financière de l'établissement.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
