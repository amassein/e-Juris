<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027353547</ID>
<ANCIEN_ID>JG_L_2013_04_000000354957</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/35/35/CETATEXT000027353547.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 24/04/2013, 354957</TITRE>
<DATE_DEC>2013-04-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>354957</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>Mme Emmanuelle Mignon</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Claire Legras</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:354957.20130424</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 19 décembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentée pour la société Printemps, dont le siège est 102, rue de Provence, à Paris (75009) ; la société Printemps demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision de l'Autorité de contrôle prudentiel (ACP) du 6 octobre 2011 l'exemptant d'agrément en qualité d'établissement de paiement, en tant qu'elle exclut les enseignes Citadium et FNAC du réseau dans lequel le moyen de paiement qu'elle propose peut être accepté et fixe deux conditions à l'exemption ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros sur le fondement de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la directive 2007/64/CE du Parlement européen et du Conseil du 13 novembre 2007 concernant les services de paiement dans le marché intérieur, modifiant les directives 97/7/CE, 2002/65/CE, 2005/60/CE ainsi que 2006/48/CE et abrogeant la directive 97/5/CE ; <br/>
<br/>
              Vu la directive 2009/110/CE du Parlement européen et du Conseil du 16 septembre 2009 concernant l'accès à l'activité des établissements de monnaie électronique et son exercice ainsi que la surveillance prudentielle de ces établissements, modifiant les directives 2005/60/CE et 2006/48/CE et abrogeant la directive 2000/46/CE ;<br/>
<br/>
              Vu le code monétaire et financier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Emmanuelle Mignon, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Piwnica, Molinié, avocat de la société Printemps et de la SCP Rocheteau, Uzan-Sarano, avocat de l'Autorité de contrôle prudentiel,<br/>
<br/>
              - les conclusions de Mme Claire Legras, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Piwnica, Molinié, avocat de la société Printemps et à la SCP Rocheteau, Uzan-Sarano, avocat de l'Autorité de contrôle prudentiel ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'en vertu des dispositions combinées des articles L521-1, L. 521-2, L. 521-3 et L. 522-6 du code monétaire et financier, prises pour la transposition en droit interne de la directive du 13 novembre 2007 concernant les services de paiement dans le marché intérieur, dans leur rédaction applicable à la date de la décision attaquée, une entreprise peut fournir des services de paiement fondés sur des moyens de paiement qui ne sont acceptés, pour l'acquisition de biens ou de services, que dans les locaux de cette entreprise ou, dans le cadre d'un accord commercial avec elle, dans un réseau limité de personnes acceptant ces moyens de paiement ou pour un éventail limité de biens ou de services, sans être tenue de demander à l'Autorité de contrôle prudentiel un agrément en tant qu'établissement de paiement ; qu'elle doit en revanche, avant de commencer à exercer ses activités, adresser une déclaration à l'Autorité de contrôle prudentiel, sauf si les instruments de paiement qu'elle émet sont délivrés exclusivement pour l'achat d'un bien ou d'un service déterminé auprès d'elle ou auprès d'entreprises liées avec elle par un accord de franchise commerciale ; que l'Autorité de contrôle prudentiel dispose alors d'un délai, le cas échéant après lui avoir demandé des compléments d'informations, pour lui notifier qu'elle ne respecte pas les conditions prévues par le code monétaire et financier pour bénéficier d'une dispense d'agrément ; que le silence gardé par l'Autorité de contrôle prudentiel à l'issue de ce délai vaut approbation du respect de ces conditions ; <br/>
<br/>
              2. Considérant que, par la décision attaquée, l'Autorité de contrôle prudentiel a notifié à la société Printemps que son service de listes de cadeaux, qu'elle lui avait déclaré, entrait dans les conditions prévues par les textes pour être dispensé d'agrément, sous réserve d'être limité aux enseignes Printemps et Printemps Voyages, à l'exclusion des enseignes Citadium et FNAC ; que l'ACP a par ailleurs assorti cette dispense d'agrément de l'obligation, pour la société Printemps, de mettre en place, dans un délai de trois mois, un système de tenue des comptes de paiement des listes de cadeaux exclusivement par la société Printemps et de centralisation des fonds versés sur des comptes bancaires dédiés de la société Printemps ; que la société Printemps demande l'annulation pour excès de pouvoir de cette décision en tant qu'elle exclut les sociétés Citadium et FNAC et est assortie des deux conditions mentionnées ci-dessus ; <br/>
<br/>
              Sur la légalité externe de la décision attaquée :<br/>
<br/>
              3. Considérant qu'aux termes du premier alinéa de l'article L. 612-13 du code monétaire et financier : " Chaque formation du collège de l'Autorité ne peut délibérer que si la majorité de ses membres sont présents " ; qu'il ressort des pièces du dossier que les huit membres du sous-collège sectoriel de la banque étaient tous présents lors de la séance au cours de laquelle la décision attaquée a été prise ; que le moyen tiré de ce que la décision attaquée aurait été prise par une autorité irrégulièrement composée manque donc en fait ;<br/>
<br/>
              Sur la légalité interne de la décision attaquée :<br/>
<br/>
              4. Considérant, en premier lieu, qu'aux termes de l'article L. 612-1 du code monétaire et financier : " I. L'Autorité de contrôle prudentiel, autorité administrative indépendante, veille à la préservation de la stabilité du système financier et à la protection des clients, assurés, adhérents et bénéficiaires des personnes soumises à son contrôle. L'Autorité contrôle le respect par ces personnes des dispositions du code monétaire et financier (...) " ; qu'il résulte de la combinaison de ces dispositions avec les dispositions mentionnées ci-dessus des articles L. 521-1, L. 521-2, L. 521-3 et L. 522-6 du même code que, si le législateur n'a pas expressément prévu la faculté, pour l'Autorité de contrôle prudentiel, d'assortir de conditions tenant aux modalités de gestion des moyens de paiement, des comptes ou des fonds les décisions par lesquelles elle indique à une entreprise qui souhaite fournir des services de paiement en dispense d'agrément qu'elle entre dans le champ d'application de cette dispense, il lui a confié la mission de veiller à la stabilité de l'ensemble du système financier ; que toute entreprise qui fournit des services de paiement participe à ce système, quand bien même elle est susceptible d'être dispensée d'agrément ; qu'eu égard à la mission dont la loi l'a ainsi investie, l'Autorité de contrôle prudentiel a pu légalement assortir la décision de dispense d'agrément qu'elle a adressée à la société Printemps de conditions tenant à la tenue des comptes de paiement et à la centralisation des fonds versés, qui sont de nature à préserver la sécurité des moyens de paiement ainsi  fournis et à protéger leurs usagers ; qu'il suit de là que le moyen tiré de ce que les conditions prévues par la décision attaquée seraient illégales doit être écarté ; que, sans qu'il soit besoin de statuer sur leur recevabilité, les conclusions de la société Printemps sur ce point ne peuvent qu'être rejetées ;<br/>
<br/>
              5. Considérant, en second lieu, que, pour justifier l'exclusion des enseignes Citadium et FNAC du réseau limité de personnes acceptant le moyen de paiement que la société Printemps voulait créer pour son service de listes de cadeaux, l'Autorité de contrôle prudentiel a estimé qu'un réseau limité de personnes, au sens de l'article L. 521-3 du code monétaire et financier, pris pour la transposition du k) de l'article 3 de la directive du 13 novembre 2007 mentionnée ci-dessus, s'entend d'un réseau dont le moyen de paiement est valable uniquement pour l'achat de biens et de services dans une chaîne de magasins donnée et qu'une telle chaîne se caractérise par une enseigne commune ; que si le considérant 5 de la directive du Parlement européen et du Conseil du 16 septembre 2009 concernant l'accès à l'activité des établissements de monnaie électronique et son exercice ainsi que la surveillance prudentielle de ces établissements, qui comporte la même exclusion de son champ d'application que celle prévue au k) de l'article 3 de la directive du 13 novembre 2007 transposée à l'article L. 521-3 du code monétaire et financier, dont l'Autorité de contrôle prudentiel a fait application, précise qu'un service de paiement devrait être réputé utilisé à l'intérieur d'un réseau limité de prestataires s'il est valable uniquement dans un magasin donné ou dans une chaîne de magasins donnée, il prévoit à cet égard une simple présomption ; qu'en dehors de l'hypothèse des franchises commerciales et de celle des chaînes de magasin placées sous une enseigne commune, un réseau peut également être regardé comme conforme aux exigences de l'article L. 521-3 du code monétaire et financier s'il satisfait à des critères objectifs, tels que, notamment, un périmètre géographique circonscrit, l'importance des liens capitalistiques entre ses membres, ou l'étroitesse de leurs relations commerciales, et que son caractère limité se trouve ainsi garanti ; qu'il suit de là qu'en excluant les sociétés Citadium et FNAC du réseau pour lequel la société Printemps pouvait bénéficier d'une dispense d'agrément au seul motif que ces deux sociétés correspondent à des enseignes différentes de celle du Printemps, l'Autorité de contrôle prudentiel a entaché sa décision d'une erreur de droit ; que, dès lors, et sans qu'il soit besoin de poser une question préjudicielle à la Cour de justice de l'Union européenne, sa décision doit être annulée en tant qu'elle exclut les sociétés Citadium et FNAC du réseau limité de services de paiement déclaré par la société Printemps au titre de son service de listes de cadeaux ;<br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              6. Considérant que ces dispositions font obstacle à ce que soit mise à la charge de la société Printemps, qui n'est pas, dans la présente instance, la partie perdante, la somme demandée par l'Autorité de contrôle prudentiel ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à la société Printemps d'une somme de 3 000 euros au même titre ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La décision du 6 octobre 2011 par laquelle l'Autorité de contrôle prudentiel a exempté d'agrément en qualité d'établissement de paiement la société Printemps au titre de son service de listes de cadeaux est annulée en tant qu'elle écarte les sociétés Citadium et FNAC de ce réseau limité de services de paiement.<br/>
Article 2 : Le surplus des conclusions de la société Printemps est rejeté.<br/>
Article 3 : Les conclusions de l'Autorité de contrôle prudentiel présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 4 : La présente décision sera notifiée à la société Printemps et à l'Autorité de contrôle prudentiel.<br/>
Copie en sera adressée pour information au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">13 CAPITAUX, MONNAIE, BANQUES. - ENTREPRISE PRESTATAIRE DE SERVICES DE PAIEMENT - MOYENS DE PAIEMENT ACCEPTÉS UNIQUEMENT, DANS LE CADRE D'UN ACCORD COMMERCIAL AVEC L'ENTREPRISE, DANS UN RÉSEAU LIMITÉ DE PERSONNES - DISPENSE D'AGRÉMENT EN TANT QU'ÉTABLISSEMENT DE PAIEMENT - 1) FACULTÉ DE L'ACP D'ASSORTIR LA DÉCISION DE DISPENSE D'AGRÉMENT DE CONDITIONS TENANT À LA TENUE DES COMPTES DE PAIEMENT ET À LA CENTRALISATION DES FONDS - EXISTENCE (ART. L. 612-1 DU CMF, COMBINÉ AVEC LES ART. L. 521-1, L. 521-2, L. 521-3 ET L. 522-6 DE CE CODE) - 2) RÉSEAU LIMITÉ (ART. L. 521-3 DU CMF) - NOTION - CHAÎNE DE MAGASINS SOUS ENSEIGNE COMMUNE - CONDITION NÉCESSAIRE - ABSENCE, UN RÉSEAU SANS ENSEIGNE COMMUNE POUVANT ÉGALEMENT ÊTRE REGARDÉ COMME CONFORME AUX EXIGENCES DE L'ARTICLE L. 521-3 DU CMF S'IL SATISFAIT À DES CRITÈRES OBJECTIFS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">13-027 CAPITAUX, MONNAIE, BANQUES. - DÉCISION DE DISPENSE D'AGRÉMENT D'UNE ENTREPRISE PRESTATAIRE DE SERVICES DE PAIEMENT FOURNISSANT DES MOYENS DE PAIEMENT ACCEPTÉS UNIQUEMENT, DANS LE CADRE D'UN ACCORD COMMERCIAL AVEC ELLE, DANS UN RÉSEAU LIMITÉ DE PERSONNES - 1) FACULTÉ DE L'ACP D'ASSORTIR SA DÉCISION DE CONDITIONS TENANT À LA TENUE DES COMPTES DE PAIEMENT ET À LA CENTRALISATION DES FONDS - EXISTENCE (ART. L. 612-1 DU CMF, COMBINÉ AVEC LES ART. L. 521-1, L. 521-2, L. 521-3 ET L. 522-6 DE CE CODE) - 2) RÉSEAU LIMITÉ (ART. L. 521-3 DU CMF) - CHAÎNE DE MAGASINS SOUS ENSEIGNE COMMUNE - CONDITION NÉCESSAIRE - ABSENCE - CONSÉQUENCE - FACULTÉ DE L'ACP DE REFUSER LA DISPENSE AU SEUL MOTIF QUE LES ENTREPRISES DU RÉSEAU ONT DES ENSEIGNES DIFFÉRENTES - ABSENCE.
</SCT>
<ANA ID="9A"> 13 1) Il résulte de la combinaison des dispositions de l'article L. 612-1 du code monétaire et financier (CMF) avec les dispositions des articles L. 521-1, L. 521-2, L. 521-3 et L. 522-6 du même code dans leur rédaction alors applicable que, si le législateur n'a pas expressément prévu la faculté, pour l'Autorité de contrôle prudentiel (ACP), d'assortir de conditions tenant aux modalités de gestion des moyens de paiement, des comptes ou des fonds les décisions par lesquelles elle indique à une entreprise qui souhaite fournir des services de paiement en dispense d'agrément qu'elle entre dans le champ d'application de cette dispense, il lui a confié la mission de veiller à la stabilité de l'ensemble du système financier. Toute entreprise qui fournit des services de paiement participe à ce système, quand bien même elle est susceptible d'être dispensée d'agrément. Eu égard à la mission dont la loi l'a ainsi investie, l'ACP a pu légalement assortir une décision de dispense d'agrément de conditions tenant à la tenue des comptes de paiement et à la centralisation des fonds versés, qui sont de nature à préserver la sécurité des moyens de paiement ainsi fournis et à protéger leurs usagers.,,,2) Si le considérant 5 de la directive 2009/110/CE du 16 septembre 2009, qui comporte la même exclusion de son champ d'application que celle prévue au k) de l'article 3 de la directive 2007/64/CE du 13 novembre 2007 transposée à l'article L. 521-3 du CMF, précise qu'un service de paiement devrait être réputé utilisé à l'intérieur d'un réseau limité de prestataires s'il est valable uniquement dans un magasin donné ou dans une chaîne de magasins donnée, il prévoit à cet égard une simple présomption. En dehors de l'hypothèse des franchises commerciales et de celle des chaînes de magasins placées sous une enseigne commune, un réseau peut également être regardé comme conforme aux exigences de l'article L. 521-3 du CMF s'il satisfait à des critères objectifs, tels que, notamment, un périmètre géographique circonscrit, l'importance des liens capitalistiques entre ses membres, ou l'étroitesse de leurs relations commerciales, et que son caractère limité se trouve ainsi garanti. Ainsi, l'ACP ne peut exclure du réseau pour lequel une société peut bénéficier d'une dispense d'agrément deux autres sociétés au seul motif que celles-ci correspondent à des enseignes différentes.</ANA>
<ANA ID="9B"> 13-027 1) Il résulte de la combinaison des dispositions de l'article L. 612-1 du code monétaire et financier (CMF) avec les dispositions des articles L. 521-1, L. 521-2, L. 521-3 et L. 522-6 du même code dans leur rédaction alors applicable que, si le législateur n'a pas expressément prévu la faculté, pour l'Autorité de contrôle prudentiel (ACP), d'assortir de conditions tenant aux modalités de gestion des moyens de paiement, des comptes ou des fonds les décisions par lesquelles elle indique à une entreprise qui souhaite fournir des services de paiement en dispense d'agrément qu'elle entre dans le champ d'application de cette dispense, il lui a confié la mission de veiller à la stabilité de l'ensemble du système financier. Toute entreprise qui fournit des services de paiement participe à ce système, quand bien même elle est susceptible d'être dispensée d'agrément. Eu égard à la mission dont la loi l'a ainsi investie, l'ACP a pu légalement assortir une décision de dispense d'agrément de conditions tenant à la tenue des comptes de paiement et à la centralisation des fonds versés, qui sont de nature à préserver la sécurité des moyens de paiement ainsi fournis et à protéger leurs usagers.,,,2) Si le considérant 5 de la directive 2009/110/CE du 16 septembre 2009, qui comporte la même exclusion de son champ d'application que celle prévue au k) de l'article 3 de la directive 2007/64/CE du 13 novembre 2007 transposée à l'article L. 521-3 du CMF, précise qu'un service de paiement devrait être réputé utilisé à l'intérieur d'un réseau limité de prestataires s'il est valable uniquement dans un magasin donné ou dans une chaîne de magasins donnée, il prévoit à cet égard une simple présomption. En dehors de l'hypothèse des franchises commerciales et de celle des chaînes de magasins placées sous une enseigne commune, un réseau peut également être regardé comme conforme aux exigences de l'article L. 521-3 du CMF s'il satisfait à des critères objectifs, tels que, notamment, un périmètre géographique circonscrit, l'importance des liens capitalistiques entre ses membres, ou l'étroitesse de leurs relations commerciales, et que son caractère limité se trouve ainsi garanti. Ainsi, l'ACP ne peut exclure du réseau pour lequel une société peut bénéficier d'une dispense d'agrément deux autres sociétés au seul motif que celles-ci correspondent à des enseignes différentes.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
