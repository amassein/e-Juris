<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031978219</ID>
<ANCIEN_ID>JG_L_2016_02_000000381825</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/97/82/CETATEXT000031978219.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème / 4ème SSR, 03/02/2016, 381825</TITRE>
<DATE_DEC>2016-02-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>381825</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème / 4ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER, TEXIDOR ; SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>M. Marc Lambron</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:381825.20160203</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. et Mme A...B...ont demandé au tribunal administratif de Caen de condamner la commune de Cormelles-le-Royal (Calvados) à leur verser une somme de 330 000 euros en réparation du préjudice causé par les nuisances sonores résultant de l'utilisation de la salle des fêtes municipale. Par un jugement n° 1001996 du 15 décembre 2011, le tribunal administratif a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 12NT00387 du 25 avril 2014, la cour administrative d'appel de Nantes, sur la requête de M. et MmeB..., a annulé ce jugement pour irrégularité puis, évoquant la demande de première instance, condamné la commune de Cormelles-le-Royal à verser aux intéressés une indemnité de 53 000 euros augmentée des intérêts légaux au taux légal à compter du 10 juillet 2010 et mis les frais d'expertise à la charge de la commune.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 25 juin et 25 septembre 2014 au secrétariat du contentieux du Conseil d'Etat, la commune de Cormelles-le-Royal demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel des épouxB... ;<br/>
<br/>
              3°) de mettre à leur charge la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - la loi n° 68-1250 du 31 décembre 1968 ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Lambron, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de la commune de Cormelles-le-Royal et à la SCP Célice, Blancpain, Soltner, Texidor, avocat de M. et Mme B...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. et MmeB..., propriétaires depuis 1977 d'une maison d'habitation située sur le territoire de la commune de Cormelles-le-Royal (Calvados), ont demandé au tribunal administratif de Caen de condamner cette dernière à les indemniser des préjudices subis du fait des manifestations se tenant dans la salle des fêtes municipale édifiée en 1983 sur une parcelle jouxtant leur propriété ; que leur demande a été rejetée par un jugement du 15 décembre 2011, dont ils ont relevé appel ; que, par un arrêt du 25 avril 2014, la cour administrative d'appel de Nantes a annulé ce jugement pour irrégularité puis, évoquant la demande de première instance, écarté l'exception de prescription quadriennale opposée en défense, retenu la responsabilité pour faute de la commune et mis à la charge de celle-ci une indemnité de 53 000 euros au titre des frais d'édification d'un mur d'isolation réalisé en 1989 par les intéressés, des frais d'équipement des fenêtres de leur maison par des vitres isolantes en 2008 et des troubles qu'ils avaient subis dans leurs conditions d'existence entre 1983 et 2009 ; que la commune de Cormelles-le-Royal se pourvoit en cassation contre cet arrêt ;<br/>
<br/>
              2. Considérant, en premier lieu, qu'il ressort des pièces du dossier soumis aux juges du fond que M. et Mme B...avaient soutenu devant le tribunal administratif qu'entre 1983 et 1998 la commune de Cormelles-le-Royal n'avait pas pris les mesures nécessaires pour atténuer les nuisances sonores engendrées par le fonctionnement de la salle des fêtes municipale ; que le jugement du tribunal n'est pas motivé sur ce point ; que c'est, dès lors, à juste titre que la cour administrative d'appel l'a annulé pour ce motif ; <br/>
<br/>
              3. Considérant, en deuxième lieu, qu'aux termes de l'article 1er de la loi du 31 décembre 1968 relative à la prescription des créances sur l'Etat, les départements, les communes et les établissements publics : " Sont prescrites, au profit de l'Etat, des départements et des communes, sans préjudice des déchéances particulières édictées par la loi, et sous réserve des dispositions de la présente loi, toutes créances qui n'ont pas été payées dans un délai de quatre ans à partir du premier jour de l'année suivant celle au cours de laquelle les droits ont été acquis. Sont prescrites, dans le même délai et sous la même réserve les créances sur les établissements publics dotés d'un comptable public " ; que, lorsque la responsabilité d'une personne publique est recherchée au titre d'un dommage causé à un tiers par un ouvrage public, les droits de créance invoqués par ce tiers en vue d'obtenir l'indemnisation de ses préjudices doivent être regardés comme acquis, au sens de ces dispositions, à la date à laquelle la réalité et l'étendue de ces préjudices ont été entièrement révélées, ces préjudices étant connus et pouvant être exactement mesurés ; que la créance indemnitaire relative à la réparation d'un préjudice présentant un caractère évolutif doit être rattachée à chacune des années au cours desquelles ce préjudice a été subi ; qu'aux termes de l'article 2 de la même loi : " La prescription est interrompue par : / Toute demande de paiement ou toute réclamation écrite adressée par un créancier à l'autorité administrative, dès lors que la demande ou la réclamation a trait au fait générateur, à l'existence ou au paiement de la créance alors même que l'administration saisie n'est pas celle qui aura finalement la charge du règlement. / Toute communication écrite d'une administration intéressée, même si cette communication n'a pas été faite directement au créancier qui s'en prévaut, dès lors que cette communication a trait au fait générateur, à l'existence, au montant ou au paiement de la créance ; (....) " ;<br/>
<br/>
              4. Considérant que le fait générateur de la créance dont M. et Mme B...se sont prévalus devant les juges du fond était constitué par les nuisances sonores résultant de l'exploitation de la salle des fêtes municipale, depuis sa construction en 1983 sur le terrain jouxtant leur maison d'habitation ; que, pour écarter l'exception de prescription quadriennale soulevée par la commune, la cour administrative d'appel s'est fondée sur des réclamations écrites relatives à ces nuisances, que les intéressés avaient adressées à l'administration entre le 20 octobre 1985 et le 7 juin 2009, ainsi que sur des communications écrites effectuées au cours de cette période  par les services concernés ; que la cour a exactement qualifié la lettre du 12 février 2008 par laquelle le maire de la commune, en réponse à une nouvelle démarche accomplie par les intéressés auprès des services municipaux, les a informés qu'un limiteur de sons avait été installé dans la salle, en estimant qu'elle se rapportait au fait générateur de la créance ; que la commune qui, à aucun moment de la procédure devant les juges du fond, n'a contesté avoir reçu les courriers de M. et Mme B...ne peut utilement soutenir, pour la première fois devant le juge de cassation, que la preuve de cette réception n'a pas été apportée ; qu'en estimant qu'il résultait de ces documents que, lors de la présentation d'une réclamation préalable le 9 juillet 2010, suivie le 5 octobre 2010 de la saisine du tribunal administratif de Caen, la créance des intéressés n'était prescrite pour aucun des préjudices invoqués, y compris ceux antérieurs à 2005, la cour administrative d'appel de Nantes n'a pas dénaturé les pièces du dossier et n'a commis aucune erreur de qualification juridique ;<br/>
<br/>
              5. Considérant, en troisième lieu, que la cour administrative d'appel n'a pas dénaturé les pièces qui lui étaient soumises, parmi lesquelles figurait le rapport d'un expert désigné par le juge des référés du tribunal administratif, faisant état de la fréquence des soirées, de l'importance des nuisances résultant du comportement des participants et de la présence en de nombreuses occasions d'un camion frigorifique fonctionnant toute la soirée, en estimant que si des mesures visant à limiter les nuisances sonores avaient été prises par la commune à partir de 1998, soit quinze ans après le début de l'utilisation de la salle des fêtes, celles-ci n'avaient pas permis de réduire de manière satisfaisante les troubles constatés, faute notamment pour le maire d'avoir sanctionné les manquements répétés des usagers aux obligations rappelées par des arrêtés municipaux du 26 juin 2009 et 22 juillet 2010 ; que la cour, qui n'a indemnisé les préjudices subis que jusqu'en 2009, a pu en déduire, sans commettre d'erreur de qualification juridique, que le maire avait commis une faute de nature à engager la responsabilité de cette commune ;<br/>
<br/>
              6. Considérant, en quatrième lieu, que c'est dans le cadre d'une appréciation souveraine des faits de l'espèce, exempte de dénaturation, que la cour administrative d'appel a estimé que le remplacement, en 2008, des vitres de la maison des époux B...par des vitrages présentant une meilleure qualité d'isolation phonique, compte tenu des progrès techniques accomplis en la matière, avait été rendu nécessaire par la persistance des nuisances sonores ; qu'en retenant, en conséquence, l'existence d'un lien direct entre la carence fautive de la commune et le préjudice ayant résulté du coût de ce remplacement, la cour n'a pas commis d'erreur de qualification juridique ; <br/>
<br/>
              7. Considérant, enfin, qu'en fixant à 53 000 euros le montant global des indemnités mises à la charge de la commune, la cour s'est livrée à une appréciation souveraine des faits de l'espèce, exempte de dénaturation ;<br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que la commune de Cormelles-le-Royal n'est pas fondée à demander l'annulation de l'arrêt de la cour administrative d'appel de Nantes du 25 avril 2014 ; que doivent, par suite, être également rejetées ses conclusions au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune le versement d'une somme de 3 500 euros aux époux B...au titre de ces dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
		Article 1er : Le pourvoi de la commune de Cormelles-le-Royal est rejeté.<br/>
<br/>
Article 2 : La commune de Cormelles-le-Royal versera la somme de 3 500 euros aux époux B...au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la commune de Cormelles-le-Royal, à M. et Mme A... B...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">18-04-02-05 COMPTABILITÉ PUBLIQUE ET BUDGET. DETTES DES COLLECTIVITÉS PUBLIQUES - PRESCRIPTION QUADRIENNALE. RÉGIME DE LA LOI DU 31 DÉCEMBRE 1968. INTERRUPTION DU COURS DU DÉLAI. - EXISTENCE - CAS DE RÉCLAMATIONS ÉCRITES RÉPÉTÉES À UNE COMMUNE RELATIVES À DES NUISANCES SONORES ET DE COMMUNICATIONS ÉCRITES EFFECTUÉES EN RÉPONSE PAR LES SERVICES COMMUNAUX [RJ1].
</SCT>
<ANA ID="9A"> 18-04-02-05 L'existence de réclamations écrites répétées relatives aux nuisances sonores causées par une salle des fêtes municipale, ainsi que de communications écrites effectuées en réponse par les services municipaux au cours de la période, est de nature à interrompre la prescription de la créance résultant des nuisances.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Comp. CE, 15 janvier 1965, Ministre de l'intérieur c/ dame veuve Corbin, p. 33 ; CE, 19 décembre 2007, Société Sogeparc-CGST-Compagnie générale de stationnement, n° 260327, aux Tables sur un autre point.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
