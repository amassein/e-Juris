<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038670498</ID>
<ANCIEN_ID>JG_L_2019_06_000000418512</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/67/04/CETATEXT000038670498.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 17/06/2019, 418512</TITRE>
<DATE_DEC>2019-06-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418512</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Marie Sirinelli</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:418512.20190617</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés le 23 février 2018 et le 11 mars 2019 au secrétariat du contentieux du Conseil d'Etat, l'association FNATH, association des accidentés de la vie, et l'Union nationale des associations agréées d'usagers du système de santé (UNAASS) demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 21 décembre 2017 fixant les montants du forfait journalier hospitalier prévu à l'article L. 174-4 du code de la sécurité sociale ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule ; <br/>
              - la convention relative aux droits des personnes handicapées, signée à New-York le 30 mars 2007 ;<br/>
              - le code de la sécurité sociale ;<br/>
              - le décret n° 2000-685 du 21 juillet 2000 ;<br/>
              - le décret n° 2005-850 du 27 juillet 2005 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Sirinelli, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article L. 174-4 du code de la sécurité sociale : " Un forfait journalier est supporté par les personnes admises dans des établissements hospitaliers ou médico-sociaux, à l'exclusion des établissements mentionnés à l'article L. 174-6 du présent code et au 6° du I de l'article L. 312-1 du code de l'action sociale et des familles. Ce forfait n'est pas pris en charge par les régimes obligatoires de protection sociale, sauf dans le cas des enfants et adolescents handicapés hébergés dans des établissements d'éducation spéciale ou professionnelle, des victimes d'accidents du travail et de maladies professionnelles, des bénéficiaires de l'assurance maternité et des bénéficiaires de l'article L. 115 du code des pensions militaires d'invalidité et des victimes de la guerre, ainsi que des donneurs d'éléments et produits du corps humain mentionnés à l'article L. 1211-2 du code de la santé publique. / Le forfait journalier peut être modulé dans des conditions fixées par décret en Conseil d'Etat, en fonction de l'un ou de plusieurs des critères suivants : catégorie de l'établissement, nature du service, durée du séjour. Ses différents montants sont fixés par arrêté. / Le forfait journalier s'impute à due concurrence sur la participation laissée éventuellement à la charge des assurés par leurs régimes respectifs d'assurance maladie, lorsque le montant de cette participation est supérieur ou égal à celui du forfait journalier ; dans le cas contraire, la participation est imputée sur le forfait. Cette disposition n'est toutefois pas applicable lorsqu'en vertu du l° de l'article L. 322-3 la participation de l'assuré à l'occasion d'une hospitalisation est limitée au motif que la dépense demeurant à.sa charge dépasse un certain montant) (... ". Aux termes de l'article R. 174-5 du même code : " Le forfait journalier institué à l'article L. 174-4 est déterminé compte tenu du coût journalier moyen d'hébergement. Son montant, qui ne peut excéder la moitié de ce coût, est fixé par arrêté des ministres chargés de la sécurité sociale, de la santé, de l'agriculture, de l'économie, des finances et du budget ". Enfin, aux termes de l'article R. 174-5-1 de ce code : " Le montant du forfait journalier applicable en cas d'hospitalisation dans un service de psychiatrie d'un établissement de santé ne peut excéder 75 % du montant du forfait fixé en application de l'article R. 174-5 ". L'association FNATH, association des accidentés de la vie, et l'Union nationale des associations agréées d'usagers du système de santé demandent l'annulation pour excès de pouvoir de l'arrêté interministériel du 21 décembre 2017 qui fixe, à compter du 1er janvier 2018, le forfait journalier à 20 euros et le forfait journalier en cas d'hospitalisation dans un service de psychiatrie d'un établissement de santé à 15 euros.<br/>
<br/>
              Sur la fin de non-recevoir opposée par la ministre des solidarités et de la santé et par le ministre de l'agriculture et de l'alimentation :<br/>
<br/>
              2. S'il n'appartient pas au Conseil d'Etat, statuant au contentieux, d'apprécier la conformité des dispositions de l'article L. 174-4 du code de la sécurité sociale à la Constitution, il lui incombe en revanche de contrôler la conformité à la Constitution des dispositions réglementaires prises pour leur application qui ne se bornent pas à les réitérer. Les dispositions attaquées de l'arrêté du 21 décembre 2017 fixant les montants du forfait journalier hospitalier prévu à l'article L. 174-4 du code de sécurité sociale ne se bornant pas à réitérer les dispositions de cet article, les ministres ne sont pas fondés à soutenir que la requête soulèverait uniquement un moyen sur lequel le Conseil d'Etat n'aurait pas compétence pour se prononcer et serait, par suite, irrecevable.   <br/>
<br/>
              Sur l'intervention de l'association des paralysés de France :<br/>
<br/>
              3. L'association des paralysés de France justifie, au vu de son objet statutaire, d'un intérêt suffisant à demander l'annulation de l'arrêté attaqué. Ainsi, son intervention est recevable. <br/>
<br/>
              Sur la légalité de l'arrêté attaqué :<br/>
<br/>
              4. En premier lieu, aux termes du onzième alinéa du Préambule de la Constitution du 27 octobre 1946, auquel se réfère le Préambule de la Constitution du 4 octobre 1958, la Nation " (sa charge dépasse un certain montant)) garantit à tous, notamment à l'enfant, à la mère et aux vieux travailleurs, la protection de la santé (...) ". Le respect des exigences découlant de ces dispositions par une mesure qui accroît le montant laissé à la charge des assurés sociaux à raison de leurs dépenses de santé doit être apprécié, s'agissant notamment de l'incidence de cette mesure sur la situation des personnes les plus vulnérables ou défavorisées, en tenant compte, d'une part, de l'ensemble des autres dispositions en vertu desquelles des frais de soins sont déjà susceptibles d'être laissés à la charge des assurés sociaux et, d'autre part, du coût et des effets, sur ces restes à charge, de la souscription d'un contrat d'assurance complémentaire de santé.<br/>
<br/>
              5. Le forfait journalier, dont le montant prévu à l'article R. 174-5 du code de la sécurité sociale, fixé à 18 euros depuis le 1er janvier 2010, est porté à 20 euros à compter du 1er janvier 2018 par l'article 1er de l'arrêté attaqué et dont le montant prévu à l'article R. 174-5-1 du même code, fixé à 13,5 euros depuis le 1er janvier 2010, est porté à 15 euros à compter du 1er janvier 2018 par l'article 2 du même arrêté, est pris en charge, en totalité pour les personnes aux revenus les plus faibles, dans le cadre de la protection complémentaire en matière de santé prévue à l'article L. 861-3 de ce code. Pour les personnes dont les revenus sont faibles mais dépassent les plafonds d'attribution de la protection complémentaire, l'article L. 863-1 du même code prévoit une aide au paiement d'une assurance complémentaire de santé, laquelle couvre ce forfait dans les établissements hospitaliers, de même, en vertu des articles L. 871-1 et R. 871-2 du même code, que tout dispositif d'assurance maladie complémentaire, dit " solidaire et responsable ", bénéficiant d'une aide, notamment au titre de la protection sociale complémentaire obligatoire des salariés prévue à l'article L. 911-7 de ce code. Il ne ressort pas des pièces du dossier que, s'agissant des personnes dont les revenus dépassent les plafonds prévus respectivement aux articles L. 861-1 et L. 863-1 du même code pour bénéficier de la protection complémentaire en matière de santé ou de l'aide au paiement d'une assurance complémentaire de santé, telles que certains bénéficiaires de l'allocation aux adultes handicapées et du complément de ressources qui majore cette allocation, les restes à charge globaux sur les frais d'hospitalisation et de soins ambulatoires auxquels peuvent conduire les montants fixés par l'arrêté litigieux excéderaient, compte tenu notamment des remboursements susceptibles d'être obtenus d'une assurance complémentaire et du coût prévisible de cette assurance, la part de leurs revenus au-delà de laquelle seraient méconnues les exigences du onzième alinéa du Préambule de la Constitution de 1946. <br/>
<br/>
              6. En deuxième lieu, les stipulations de l'article 25 de la convention relative aux droits des personnes handicapées, selon lesquelles " Les États Parties reconnaissent que les personnes handicapées ont le droit de jouir du meilleur état de santé possible sans discrimination fondée sur le handicap. Ils prennent toutes les mesures appropriées pour leur assurer l'accès à des services de santé qui prennent en compte les sexospécificités, y compris des services de réadaptation. En particulier, les États Parties : / a) Fournissent aux personnes handicapées des services de santé gratuits ou d'un coût abordable couvrant la même gamme et de la même qualité que ceux offerts aux autres personnes, y compris des services de santé sexuelle et génésique et des programmes de santé publique communautaires (...) ", requièrent l'intervention d'actes complémentaires pour produire des effets à l'égard des particuliers et sont, par suite, dépourvues d'effet direct. Dès lors, leur méconnaissance ne peut être utilement invoquée à l'encontre de l'arrêté attaqué.<br/>
<br/>
              7. En troisième lieu, toutefois, aux termes de l'article 1er du décret du 27 juillet 2005 relatif aux délégations de signature des membres du Gouvernement : " A compter du jour suivant la publication au Journal officiel de la République française de l'acte les nommant dans leurs fonctions ou à compter du jour où cet acte prend effet, si ce jour est postérieur, peuvent signer, au nom du ministre ou du secrétaire d'Etat et par délégation, l'ensemble des actes, à l'exception des décrets, relatifs aux affaires des services placés sous leur autorité : / (...) les directeurs d'administration centrale (...) ". L'article 6 du décret du 21 juillet 2000 relatif à l'organisation de l'administration centrale du ministère de l'emploi et de la solidarité et aux attributions de certains de ses services prévoit que : " La direction de la sécurité sociale est chargée de l'élaboration et de la mise en oeuvre de la politique relative à la sécurité sociale (...) ". Il suit de là que MmeA..., nommé directrice de la sécurité sociale par décret du 14 juin 2017, avait compétence pour signer l'arrêté attaqué au nom du ou des ministres chargés de la sécurité sociale. En revanche, en l'absence de la signature, exigée par l'article R. 174-5 du code de la sécurité sociale, de la ministre des solidarités et de la santé ou d'un agent ayant délégation pour signer un tel acte au nom de ce ministre, en sa qualité de ministre chargé de la santé, l'arrêté attaqué est entaché d'incompétence. Les associations requérantes sont ainsi fondées à en demander, pour ce motif, l'annulation.<br/>
<br/>
              Sur les conséquences de l'illégalité de l'arrêté attaqué :<br/>
<br/>
              8. Eu égard aux effets manifestement excessifs de l'annulation rétroactive de la hausse du montant du forfait journalier résultant de l'arrêté attaqué, qui serait susceptible de donner lieu à des actions en récupération de la part des personnes hospitalisées depuis l'entrée en vigueur de cette hausse et ainsi de renchérir le coût pour les régimes obligatoires de sécurité sociale de l'admission des patients dans les établissements concernés et d'affecter les conditions de l'équilibre des finances de la sécurité sociale, il y a lieu, compte tenu du motif de l'annulation de cet arrêté, d'en différer l'effet, sous réserve des droits des personnes qui auraient engagé une action contentieuse à la date de la présente décision, jusqu'au 1er septembre 2019, et de regarder comme définitifs les effets produits par cet arrêté antérieurement à son annulation.<br/>
<br/>
              Sur les frais liés au litige :<br/>
<br/>
              9. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 1 500 euros à verser à chacune des associations requérantes au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces dispositions font en revanche obstacle à ce qu'une somme soit versée au même titre à l'association des paralysés de France, qui ne peut être regardée comme une partie pour l'application de cet article.<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : L'intervention de l'association des paralysés de France est admise. <br/>
Article 2 : L'arrêté du 21 décembre 2017 fixant les montants du forfait journalier hospitalier prévu à l'article L. 174-4 du code de la sécurité sociale est annulé. Cette annulation prendra effet le 1er septembre 2019. <br/>
Article 3 : Sous réserve des actions contentieuses engagées à la date de la présente décision contre les actes pris sur son fondement, les effets produits par cet arrêté antérieurement à son annulation sont regardés comme définitifs.<br/>
Article 4 : L'Etat versera à la FNATH, association des accidentés de la vie, et à l'Union nationale des associations agréées des usagers du système de santé une somme de 1 500 euros chacune au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : Les conclusions présentées par l'association des paralysés de France au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 6 : La présente décision sera notifiée à la FNATH, association des accidentés de la vie, première dénommée pour les requérantes, à la ministre des solidarités et de la santé et à l'Association des paralysés de France.<br/>
Copie en sera adressée au ministre de l'économie et des finances, au ministre de l'agriculture et de l'alimentation et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-02-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. ACCORDS INTERNATIONAUX. APPLICABILITÉ. - CONVENTION RELATIVE AUX DROITS DES PERSONNES HANDICAPÉES - A) DE L'ARTICLE 25 - EFFET DIRECT - ABSENCE.
</SCT>
<ANA ID="9A"> 01-01-02-01 Les stipulations du a) de l'article 25 de la convention relative aux droits des personnes handicapées signée à New York le 30 mars 2007 requièrent l'intervention d'actes complémentaires pour produire des effets à l'égard des particuliers et sont, par suite, dépourvues d'effet direct.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
