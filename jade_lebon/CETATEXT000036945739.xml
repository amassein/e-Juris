<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036945739</ID>
<ANCIEN_ID>JG_L_2018_05_000000407336</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/94/57/CETATEXT000036945739.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème chambres réunies, 25/05/2018, 407336, Publié au recueil Lebon</TITRE>
<DATE_DEC>2018-05-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>407336</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP LE BRET-DESACHE</AVOCATS>
<RAPPORTEUR>M. Marc Firoud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:407336.20180525</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Melun d'annuler les décisions de Pôle emploi du 3 décembre 2012 et du 14 février 2013 rejetant ses demandes indemnitaires, et de condamner Pôle emploi à lui verser une somme de 150 000 euros assortie des intérêts avec capitalisation en réparation du préjudice subi à la suite de la gestion fautive de sa carrière et des conditions dans lesquelles son licenciement est intervenu. Par un jugement n°s 1300813, 1302020 du 16 juillet 2015, le tribunal administratif de Melun a condamné Pôle emploi à verser à M. A...une somme de 16 952,08 euros, assortie des intérêts au taux légal et de la capitalisation de ces intérêts, et a rejeté le surplus des conclusions de ses demandes. <br/>
<br/>
              Par un arrêt n° 15PA03596 du 29 novembre 2016, la cour administrative d'appel de Paris a rejeté l'appel formé par M. A...contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 30 janvier et 27 avril 2017 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) de mettre à la charge de Pôle emploi la somme de 2 700 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - le décret n° 86-83 du 17 janvier 1986 ; <br/>
              - le décret n° 2003-1370 du 31 décembre 2003 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Firoud, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Le Bret-Desaché, avocat de M. A...et à la SCP Piwnica, Molinié, avocat de Pôle emploi ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces soumises aux juges du fond que M.  A...a été recruté en qualité d'agent public pour exercer les fonctions de conseiller principal par l'Agence nationale pour l'emploi, devenue Pôle emploi, dans le cadre d'un contrat à durée indéterminée ; qu'il a été placé en congé de maladie, puis en congé de grave maladie pour une durée d'un an pour la période du 31 août 2006 au 30 août 2007 ; que, conformément à l'avis émis par le comité médical départemental, confirmé par le comité médical supérieur, Pôle emploi n'a pas renouvelé ce congé de grave maladie et a placé M. A...en congé de maladie sans traitement à compter du 31 août 2007 par une décision du 8 octobre 2008 ; que M. A...a été licencié par Pôle emploi pour inaptitude physique le 5 mars 2012 et a été admis à la retraite le 1er avril suivant ; qu'il a demandé au tribunal administratif de Melun d'annuler les décisions du 3 décembre 2012 et du 14 février 2013 par lesquelles Pôle emploi a rejeté ses demandes de régularisation de sa situation et d'indemnisation, et de condamner Pôle emploi à l'indemniser des préjudices qu'il estime avoir subis du fait des fautes commises par Pôle Emploi ; que, par un jugement du 16 juillet 2015, le tribunal administratif de Melun a condamné Pôle emploi à verser à M. A...une somme de 16 952,08 euros en réparation des préjudices causés par l'absence fautive de régularisation de sa situation entre le 1er septembre 2008 et le 5 mars 2012, et a rejeté le surplus des conclusions de ses demandes ; que, par un arrêt du 29 novembre 2016, la cour administrative d'appel de Paris a rejeté l'appel formé par M. A...contre ce jugement en tant qu'il avait, pour partie, rejeté ses demandes ; que M. A...se pourvoit en cassation contre cet arrêt ;<br/>
<br/>
              2. Considérant qu'il résulte d'un principe général du droit, dont s'inspirent tant les dispositions du code du travail relatives à la situation des salariés qui, pour des raisons médicales, ne peuvent plus occuper leur emploi que les règles statutaires applicables dans ce cas aux fonctionnaires, que, lorsqu'il a été médicalement constaté qu'un salarié se trouve, de manière définitive, atteint d'une inaptitude physique à occuper son emploi, il incombe à l'employeur public, avant de pouvoir prononcer son licenciement, de chercher à reclasser l'intéressé dans un autre emploi ; que la mise en oeuvre de ce principe implique que, sauf si l'agent manifeste expressément sa volonté non équivoque de ne pas reprendre une activité professionnelle, l'employeur propose à ce dernier un emploi compatible avec son état de santé et aussi équivalent que possible avec l'emploi précédemment occupé ou, à défaut d'un tel emploi, tout autre emploi si l'intéressé l'accepte ; que ce n'est que lorsque ce reclassement est impossible, soit qu'il n'existe aucun emploi vacant pouvant être proposé à l'intéressé, soit que l'intéressé est déclaré inapte à l'exercice de toutes fonctions ou soit que l'intéressé refuse la proposition d'emploi qui lui est faite, qu'il appartient à l'employeur de prononcer, dans les conditions applicables à l'intéressé, son licenciement ; que ce principe est applicable aux agents contractuels régis par les dispositions du décret du 31 décembre 2003 fixant les dispositions applicables aux agents contractuels de droit public de Pôle emploi ;<br/>
<br/>
              3. Considérant qu'il ressort des énonciations de l'arrêt attaqué que la cour, après avoir constaté que le médecin mandaté par Pôle emploi avait relevé, dans son certificat médical du 3 novembre 2011, que M. A...n'était pas apte à la reprise de " ses fonctions ", s'est fondée sur ce que M. A...n'avait pas contesté devant Pôle emploi l'inaptitude à toutes fonctions sur laquelle cet établissement public s'est fondé pour le licencier sans rechercher à le reclasser ; que, toutefois, alors même qu'il n'a pas contesté devant son employeur public la portée donnée au certificat médical le concernant établi par un médecin mandaté par l'administration, un agent peut soutenir devant le juge que cet employeur public s'est mépris sur la portée de ce certificat, en en déduisant à tort le constat d'une inaptitude définitive à l'exercice de toutes fonctions ; que dès lors, en se fondant, pour rejeter la requête de M.A..., sur la seule circonstance qu'il n'avait pas contesté auprès de Pôle emploi les constatations médicales faites sur son aptitude, la cour a commis une erreur de droit ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que M. A...est fondé à demander l'annulation de l'arrêt attaqué ; <br/>
<br/>
              5. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. A...qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a en revanche lieu, dans les circonstances de l'espèce, de mettre à la charge de Pôle emploi le versement d'une somme de 2 700 euros à M. A...à ce même titre ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 29 novembre 2016 de la cour administrative d'appel de Paris est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br/>
Article 3 : Pôle emploi versera une somme de 2 700 euros à M. A...au titre de l'article L. 761-1 du code de justice administrative. Les conclusions présentées par Pôle emploi au titre des mêmes dispositions sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à M. B...A..., à Pôle emploi et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-08 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. RECONNAISSANCE DE DROITS SOCIAUX FONDAMENTAUX. - OBLIGATION POUR L'EMPLOYEUR DE CHERCHER À RECLASSER UN SALARIÉ ATTEINT DE MANIÈRE DÉFINITIVE D'UNE INAPTITUDE À EXERCER SON EMPLOI AVANT DE POUVOIR PRONONCER SON LICENCIEMENT - PORTÉE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-10-06-02 FONCTIONNAIRES ET AGENTS PUBLICS. CESSATION DE FONCTIONS. LICENCIEMENT. AUXILIAIRES, AGENTS CONTRACTUELS ET TEMPORAIRES. - OBLIGATION POUR L'EMPLOYEUR DE CHERCHER À RECLASSER UN SALARIÉ ATTEINT DE MANIÈRE DÉFINITIVE D'UNE INAPTITUDE À EXERCER SON EMPLOI AVANT DE POUVOIR PRONONCER SON LICENCIEMENT - EXISTENCE, SAUF SI L'AGENT MANIFESTE EXPRESSÉMENT SA VOLONTÉ NON ÉQUIVOQUE DE NE PAS REPRENDRE UNE ACTIVITÉ PROFESSIONNELLE - CAS DANS LESQUELS LE RECLASSEMENT EST IMPOSSIBLE - ABSENCE D'EMPLOI VACANT, INTÉRESSÉ DÉCLARÉ INAPTE À TOUTES FONCTIONS, REFUS DE LA PROPOSITION D'EMPLOI PAR L'INTÉRESSÉ [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">36-12-03-01 FONCTIONNAIRES ET AGENTS PUBLICS. AGENTS CONTRACTUELS ET TEMPORAIRES. FIN DU CONTRAT. LICENCIEMENT. - OBLIGATION POUR L'EMPLOYEUR DE CHERCHER À RECLASSER UN SALARIÉ ATTEINT DE MANIÈRE DÉFINITIVE D'UNE INAPTITUDE À EXERCER SON EMPLOI AVANT DE POUVOIR PRONONCER SON LICENCIEMENT - EXISTENCE, SAUF SI L'AGENT MANIFESTE EXPRESSÉMENT SA VOLONTÉ NON ÉQUIVOQUE DE NE PAS REPRENDRE UNE ACTIVITÉ PROFESSIONNELLE - CAS DANS LESQUELS LE RECLASSEMENT EST IMPOSSIBLE - ABSENCE D'EMPLOI VACANT, INTÉRESSÉ DÉCLARÉ INAPTE À TOUTES FONCTIONS, REFUS DE LA PROPOSITION D'EMPLOI PAR L'INTÉRESSÉ [RJ1].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">66-11-001-01 TRAVAIL ET EMPLOI. SERVICE PUBLIC DE L'EMPLOI. - PRINCIPE GÉNÉRAL DU DROIT - OBLIGATION POUR L'EMPLOYEUR DE CHERCHER À RECLASSER UN SALARIÉ ATTEINT DE MANIÈRE DÉFINITIVE D'UNE INAPTITUDE À EXERCER SON EMPLOI AVANT DE POUVOIR PRONONCER SON LICENCIEMENT - 1) PORTÉE [RJ1] - 2) APPLICATION AUX AGENTS CONTRACTUELS DE DROITS PUBLICS DE PÔLE EMPLOI (DÉCRET N° 2003-1370) - EXISTENCE.
</SCT>
<ANA ID="9A"> 01-04-03-08 Il résulte d'un principe général du droit, dont s'inspirent tant les dispositions du code du travail relatives à la situation des salariés qui, pour des raisons médicales, ne peuvent plus occuper leur emploi que les règles statutaires applicables dans ce cas aux fonctionnaires, que, lorsqu'il a été médicalement constaté qu'un salarié se trouve, de manière définitive, atteint d'une inaptitude physique à occuper son emploi, il incombe à l'employeur public, avant de pouvoir prononcer son licenciement, de chercher à reclasser l'intéressé dans un autre emploi. La mise en oeuvre de ce principe implique que, sauf si l'agent manifeste expressément sa volonté non équivoque de ne pas reprendre une activité professionnelle, l'employeur propose à ce dernier un emploi compatible avec son état de santé et aussi équivalent que possible avec l'emploi précédemment occupé ou, à défaut d'un tel emploi, tout autre emploi si l'intéressé l'accepte. Ce n'est que lorsque ce reclassement est impossible, soit qu'il n'existe aucun emploi vacant pouvant être proposé à l'intéressé, soit que l'intéressé est déclaré inapte à l'exercice de toutes fonctions ou soit que l'intéressé refuse la proposition d'emploi qui lui est faite, qu'il appartient à l'employeur de prononcer, dans les conditions applicables à l'intéressé, son licenciement.</ANA>
<ANA ID="9B"> 36-10-06-02 Il résulte d'un principe général du droit, dont s'inspirent tant les dispositions du code du travail relatives à la situation des salariés qui, pour des raisons médicales, ne peuvent plus occuper leur emploi que les règles statutaires applicables dans ce cas aux fonctionnaires, que, lorsqu'il a été médicalement constaté qu'un salarié se trouve, de manière définitive, atteint d'une inaptitude physique à occuper son emploi, il incombe à l'employeur public, avant de pouvoir prononcer son licenciement, de chercher à reclasser l'intéressé dans un autre emploi. La mise en oeuvre de ce principe implique que, sauf si l'agent manifeste expressément sa volonté non équivoque de ne pas reprendre une activité professionnelle, l'employeur propose à ce dernier un emploi compatible avec son état de santé et aussi équivalent que possible avec l'emploi précédemment occupé ou, à défaut d'un tel emploi, tout autre emploi si l'intéressé l'accepte. Ce n'est que lorsque ce reclassement est impossible, soit qu'il n'existe aucun emploi vacant pouvant être proposé à l'intéressé, soit que l'intéressé est déclaré inapte à l'exercice de toutes fonctions ou soit que l'intéressé refuse la proposition d'emploi qui lui est faite, qu'il appartient à l'employeur de prononcer, dans les conditions applicables à l'intéressé, son licenciement.</ANA>
<ANA ID="9C"> 36-12-03-01 Il résulte d'un principe général du droit, dont s'inspirent tant les dispositions du code du travail relatives à la situation des salariés qui, pour des raisons médicales, ne peuvent plus occuper leur emploi que les règles statutaires applicables dans ce cas aux fonctionnaires, que, lorsqu'il a été médicalement constaté qu'un salarié se trouve, de manière définitive, atteint d'une inaptitude physique à occuper son emploi, il incombe à l'employeur public, avant de pouvoir prononcer son licenciement, de chercher à reclasser l'intéressé dans un autre emploi. La mise en oeuvre de ce principe implique que, sauf si l'agent manifeste expressément sa volonté non équivoque de ne pas reprendre une activité professionnelle, l'employeur propose à ce dernier un emploi compatible avec son état de santé et aussi équivalent que possible avec l'emploi précédemment occupé ou, à défaut d'un tel emploi, tout autre emploi si l'intéressé l'accepte. Ce n'est que lorsque ce reclassement est impossible, soit qu'il n'existe aucun emploi vacant pouvant être proposé à l'intéressé, soit que l'intéressé est déclaré inapte à l'exercice de toutes fonctions ou soit que l'intéressé refuse la proposition d'emploi qui lui est faite, qu'il appartient à l'employeur de prononcer, dans les conditions applicables à l'intéressé, son licenciement.</ANA>
<ANA ID="9D"> 66-11-001-01 1) Il résulte d'un principe général du droit, dont s'inspirent tant les dispositions du code du travail relatives à la situation des salariés qui, pour des raisons médicales, ne peuvent plus occuper leur emploi que les règles statutaires applicables dans ce cas aux fonctionnaires, que, lorsqu'il a été médicalement constaté qu'un salarié se trouve, de manière définitive, atteint d'une inaptitude physique à occuper son emploi, il incombe à l'employeur public, avant de pouvoir prononcer son licenciement, de chercher à reclasser l'intéressé dans un autre emploi. La mise en oeuvre de ce principe implique que, sauf si l'agent manifeste expressément sa volonté non équivoque de ne pas reprendre une activité professionnelle, l'employeur propose à ce dernier un emploi compatible avec son état de santé et aussi équivalent que possible avec l'emploi précédemment occupé ou, à défaut d'un tel emploi, tout autre emploi si l'intéressé l'accepte. Ce n'est que lorsque ce reclassement est impossible, soit qu'il n'existe aucun emploi vacant pouvant être proposé à l'intéressé, soit que l'intéressé est déclaré inapte à l'exercice de toutes fonctions ou soit que l'intéressé refuse la proposition d'emploi qui lui est faite, qu'il appartient à l'employeur de prononcer, dans les conditions applicables à l'intéressé, son licenciement. 2) Ce principe est applicable aux agents contractuels régis par les dispositions du décret n° 2003-1370 du 31 décembre 2003 fixant les dispositions applicables aux agents contractuels de droit public de Pôle emploi.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. , en précisant, CE, 19 mai 2017, M. Balland, n° 397577, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
