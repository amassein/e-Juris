<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036771647</ID>
<ANCIEN_ID>JG_L_2018_04_000000407445</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/77/16/CETATEXT000036771647.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 6ème chambres réunies, 04/04/2018, 407445</TITRE>
<DATE_DEC>2018-04-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>407445</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>RICARD ; SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Florian Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:407445.20180404</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. D...E...et Mme B...E...ont demandé au tribunal administratif de Nice d'annuler l'arrêté du maire de Grasse du 13 août 2013 portant délivrance d'un permis de construire à Mmes F...et A...C..., la décision du maire de Grasse du 27 novembre 2013 rejetant leur recours gracieux contre cet arrêté et l'arrêté du maire de Grasse du 21 novembre 2013 portant délivrance à Mmes C...d'un permis de construire modificatif. Par un jugement n° 1400272 du 1er décembre 2016, le tribunal administratif a fait droit à leur demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 2 février, 27 avril et 30 novembre 2017 au secrétariat du contentieux du Conseil d'Etat, Mmes C...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la requête de M. D...E...et Mme B...E... ;<br/>
<br/>
              3°) de mettre à la charge de M. D...E...et Mme B...E...la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le code de l'urbanisme ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de Mmes C...et à Me Ricard, avocat de M. et MmeE....<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par arrêté du 13 août 2013, le maire de Grasse a accordé à Mmes F...et A...C...un permis de construire relatif à la réalisation de travaux d'extension d'une habitation sise 18, chemin des plaines de Malbosc à Grasse, consistant en une surélévation et une augmentation de surface de plancher de 50 m², par aménagement des combles et déplacement des ouvertures ; que, par une décision du 27 novembre 2013, le maire de Grasse a rejeté le recours gracieux de M. D...et Mme B...E...tendant au retrait de cet arrêté; que, par un arrêté du 21 novembre 2013, le maire de Grasse a délivré à Mmes C... un permis de construire modificatif ; que M. et Mme E...ont introduit devant le tribunal administratif de Nice, le 23 janvier 2014, une demande tendant à l'annulation de ces arrêtés ; que par jugement du 1er décembre 2016, le tribunal a fait droit à leur demande ; que Mmes C...se pourvoient en cassation contre ce jugement ;<br/>
<br/>
              Sur la régularité du jugement :<br/>
<br/>
              2. Considérant que le premier alinéa de l'article R. 711-3 du code de justice administrative dispose que : " Si le jugement de l'affaire doit intervenir après le prononcé de conclusions du rapporteur public, les parties ou leurs mandataires sont mis en mesure de connaître, avant la tenue de l'audience, le sens de ces conclusions sur l'affaire qui les concerne " ; <br/>
<br/>
              3. Considérant qu'il ressort des pièces de la procédure que le sens des conclusions du rapporteur public sur l'affaire litigieuse a été porté à la connaissance des parties près de vingt-quatre heures avant l'audience ; que les parties ont ainsi été informées, dans un délai raisonnable avant l'audience, du sens des conclusions ; que le moyen tiré de ce que le jugement attaqué aurait été rendu au terme d'une procédure irrégulière doit, par suite, être écarté ;<br/>
<br/>
              Sur le bien-fondé du jugement :<br/>
<br/>
              4. Considérant que lorsqu'une construction existante n'est pas conforme à une ou plusieurs dispositions d'un plan local d'urbanisme régulièrement approuvé, un permis de construire ne peut être légalement délivré pour la modification de cette construction, sous réserve de dispositions de ce plan spécialement applicables à la modification des immeubles existants, que si les travaux envisagés rendent l'immeuble plus conforme aux dispositions réglementaires méconnues ou s'ils sont étrangers à ces dispositions ; <br/>
<br/>
              En ce qui concerne les dispositions du règlement du plan local d'urbanisme relatives aux aires de stationnement : <br/>
<br/>
              5. Considérant qu'aux termes de l'article UJ 12 du règlement du plan local d'urbanisme de la commune de Grasse : " Les aires de stationnement, (y compris pour les " deux-roues "), et leurs zones de manoeuvre doivent être réalisées en dehors des voies ouvertes à la circulation. Il est exigé un nombre de places de stationnement correspondant : - aux caractéristiques de l'opération, - à son environnement. / Cependant, pour les constructions à usage d'habitation, il est exigé 2,5 places de stationnement par logement. (...) ; <br/>
<br/>
              6. Considérant que, pour l'application de la règle rappelée au point 4, des travaux entrepris sur un immeuble existant qui n'impliquent pas la création de nouveaux logements mais seulement l'extension de logements existants doivent être regardés comme étrangers aux dispositions d'un plan local d'urbanisme imposant un nombre minimal de places de stationnement par logement ;<br/>
<br/>
              7. Considérant qu'après avoir relevé que le permis de construire litigieux concernait deux maisons d'habitation situées dans une propriété comprenant en tout cinq maisons, sur laquelle huit places de stationnement avaient été aménagées, le jugement attaqué énonce qu'à supposer que, comme le soutiennent les défendeurs, les travaux ne créent aucun nouveau logement, le permis est illégal dès lors qu'il ne ressort pas des pièces du dossier qu'au moins cinq des places existantes soient spécialement affectées aux deux maisons en cause ; que le tribunal administratif a ainsi retenu que les travaux seraient effectués sur des constructions qui, faute de disposer d'au moins 2,5 places de stationnement par logement, n'étaient pas conformes aux dispositions précitées de l'article UJ 12 du règlement du plan local d'urbanisme, qu'à supposer même qu'ils n'entraînent qu'une augmentation de surface sans création de nouveaux logements, ils n'étaient pas étrangers à ces dispositions et qu'ils ne rendraient pas les constructions plus conformes à la règle méconnue ; qu'en se prononçant ainsi, alors qu'il résulte de ce qui a été dit au point 6 que des travaux n'entraînant pas la création de nouveaux logements devaient être regardés comme étrangers aux dispositions de l'article UJ 12, le tribunal a commis une erreur de droit ; que, par suite, les requérantes sont fondées à soutenir, sans qu'il soit besoin d'examiner les autres moyens du pourvoi dirigés contre le même motif du jugement, que ce motif n'est pas de nature à justifier légalement l'annulation du permis de construire attaqué ;<br/>
<br/>
              En ce qui concerne les dispositions du règlement du plan local d'urbanisme relatives à l'implantation des constructions par rapport aux voies ouvertes à la circulation : <br/>
<br/>
              8. Considérant qu'aux termes de l'article UJ 6 du règlement du plan local d'urbanisme de la commune de Grasse (implantation des constructions par rapport aux voies et aux emprises publiques) : " Les dispositions du présent article s'appliquent aux voies publiques et privées ouvertes à la circulation générale, y compris aux voies piétonnes, ainsi qu'aux emprises publiques. / Les bâtiments doivent s'implanter à une distance au moins égale à 75 m de l'axe de la pénétrante Cannes-Grasse (RD 6185). / Les bâtiments doivent s'implanter à une distance de l'alignement existant ou futur au moins égale à : - 15 mètres de la route de Cannes, / - 20 mètres de la RD 304, / - 10 mètres pour les autres routes départementales, / - 5 mètres pour les autres routes. (...) " ;<br/>
<br/>
              9. Considérant que des travaux tendant à la surélévation d'un bâtiment implanté en méconnaissance des dispositions du plan local d'urbanisme relatives à l'implantation des constructions par rapport aux limites séparatives ou à la voie publique ne sont pas étrangers à ces dispositions et n'ont pas pour effet de rendre le bâtiment plus conforme à celles-ci ;<br/>
<br/>
              10. Considérant qu'en relevant que les travaux projetés par le permis de construire litigieux, qui comportaient une surélévation d'un bâtiment implanté à l'alignement de la voie publique, n'étaient pas étrangers aux dispositions précitées de l'article UJ 6 du règlement du plan local d'urbanisme de la commune, qui prescrivent que les constructions doivent être réalisées à cinq mètres au moins de l'alignement de la voie publique, pour en déduire que, n'ayant pas rendu ce bâtiment plus conforme à ces dispositions, ces travaux ne pouvaient être légalement autorisés, le tribunal administratif, qui a suffisamment motivé son jugement sur ce point, n'a pas commis d'erreur de droit ;<br/>
<br/>
              11. Considérant que le moyen, qui n'est pas d'ordre public et n'est pas né du jugement attaqué, tiré de ce que le tribunal aurait commis une erreur de droit en ne faisant pas application des dispositions de l'article L. 111-12 du code de l'urbanisme, dont il résulte que lorsqu'une construction est achevée depuis plus de dix ans, le refus de permis de construire ou de déclaration de travaux ne peut être fondé sur l'irrégularité de la construction initiale au regard du droit de l'urbanisme est nouveau en cassation et est, par suite, inopérant ;<br/>
<br/>
              12. Considérant qu'il résulte de ce qui précède que l'un des deux motifs retenus par le tribunal administratif de Nice, conformément aux dispositions de l'article L. 600-4-1 du code de l'urbanisme, justifie légalement l'annulation du permis de construire du 13 août 2013 modifié par le permis de construire du 21 novembre 2013 ; que, par suite, le pourvoi de Mmes C... doit être rejeté ; <br/>
<br/>
              13. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge des consorts E...une somme au titre des frais exposés par Mmes C...et non compris dans les dépens ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de Mmes C...le versement aux consorts E...d'une somme au titre de ces dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi de Mmes F...et A...C...est rejeté.<br/>
Article 2 : Les conclusions présentées par les parties au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à Mme A...C..., à Mme F...C..., à M. D...E...et à Mme B...E....<br/>
Copie en sera transmise à la commune de Grasse.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-03-03-02-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. LÉGALITÉ INTERNE DU PERMIS DE CONSTRUIRE. LÉGALITÉ AU REGARD DE LA RÉGLEMENTATION LOCALE. POS OU PLU (VOIR SUPRA : PLANS D`AMÉNAGEMENT ET D`URBANISME). - TRAVAUX EFFECTUÉS SUR UNE CONSTRUCTION NON CONFORME AUX DISPOSITIONS D'UN RÈGLEMENT DE PLU IMPOSANT UN NOMBRE MINIMAL DE PLACES DE STATIONNEMENT PAR LOGEMENT - TRAVAUX D'EXTENSION RÉALISÉS SUR UN IMMEUBLE EXISTANT N'IMPLIQUANT PAS LA CRÉATION DE NOUVEAUX LOGEMENTS - TRAVAUX ÉTRANGERS AUX DISPOSITIONS MÉCONNUES [RJ1].
</SCT>
<ANA ID="9A"> 68-03-03-02-02 Lorsqu'une construction existante n'est pas conforme à une ou plusieurs dispositions d'un plan local d'urbanisme (PLU) régulièrement approuvé, un permis de construire ne peut être légalement délivré pour la modification de cette construction, sous réserve de dispositions de ce plan spécialement applicables à la modification des immeubles existants, que si les travaux envisagés rendent l'immeuble plus conforme aux dispositions réglementaires méconnues ou s'ils sont étrangers à ces dispositions.... ,,Pour l'application de cette règle, des travaux entrepris sur un immeuble existant qui n'impliquent pas la création de nouveaux logements mais seulement l'extension de logements existants doivent être regardés comme étrangers aux dispositions d'un plan local d'urbanisme imposant un nombre minimal de places de stationnement par logement.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 27 mai 1988, Mme,, n° 79530, p. 223.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
