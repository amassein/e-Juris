<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032845203</ID>
<ANCIEN_ID>JG_L_2016_07_000000375076</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/84/52/CETATEXT000032845203.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Section, 01/07/2016, 375076, Publié au recueil Lebon</TITRE>
<DATE_DEC>2016-07-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>375076</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Section</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP VINCENT, OHL ; SCP CELICE, BLANCPAIN, SOLTNER, TEXIDOR</AVOCATS>
<RAPPORTEUR>M. Frédéric Puigserver</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESEC:2016:375076.20160701</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<p>Vu la procédure suivante :<br clear="none"/>
<br clear="none"/>
Procédure contentieuse antérieure<br clear="none"/>
<br clear="none"/>
La société Groupama Grand Est a demandé au tribunal administratif de Besançon de condamner le département du Jura à lui verser la somme de 21 500 euros qu'elle a été condamnée à verser au Fonds de garantie des victimes des actes de terrorisme et d'autres infractions en raison des préjudices causés par un mineur placé auprès des services de l'aide sociale à l'enfance de ce département. Par un jugement n° 1100848 du 19 décembre 2012, le tribunal administratif de Besançon a condamné le département du Jura à verser à la société Groupama Grand Est une somme de 20 000 euros assortie des intérêts au taux légal à compter du 3 juin 2010.<br clear="none"/>
<br clear="none"/>
Par un arrêt n° 13NC00283 du 28 novembre 2013, la cour administrative d'appel de Nancy, saisie de l'appel du département du Jura, a annulé le jugement du tribunal administratif de Besançon et rejeté la demande présentée par la société Groupama Grand Est devant ce tribunal.<br clear="none"/>
<br clear="none"/>
Procédure devant le Conseil d'Etat<br clear="none"/>
<br clear="none"/>
Par un pourvoi, un mémoire en réplique et un mémoire complémentaire, enregistrés les 31 janvier et 21 juillet 2014 et 8 juin 2016 au secrétariat du contentieux du Conseil d'Etat, la société Groupama Grand Est demande au Conseil d'Etat :<br clear="none"/>
<br clear="none"/>
1°) d'annuler cet arrêt de la cour administrative d'appel de Nancy du 28 novembre 2013 ;<br clear="none"/>
<br clear="none"/>
2°) réglant l'affaire au fond, de rejeter l'appel du département du Jura ;<br clear="none"/>
<br clear="none"/>
3°) de mettre à la charge du département du Jura la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
Vu les autres pièces du dossier ;<br clear="none"/>
<br clear="none"/>
Vu :<br clear="none"/>
- le code civil ;<br clear="none"/>
- le code de l'action sociale et des familles ;<br clear="none"/>
- le code de justice administrative ;<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
Après avoir entendu en séance publique :<br clear="none"/>
<br clear="none"/>
- le rapport de M. Frédéric Puigserver, maître des requêtes,<br clear="none"/>
<br clear="none"/>
- les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br clear="none"/>
<br clear="none"/>
La parole ayant été donnée, avant et après les conclusions, à la SCP Vincent, Ohl, avocat de la société Groupama Grand Est et à la SCP Célice, Blancpain, Soltner, Texidor, avocat du département du Jura ;<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'Aurélien M., né le 2 septembre 1982, a été pris en charge, en accord avec son père M. AlainM., par le service de l'aide sociale à l'enfance du département du Jura du 4 avril 1994 au 3 juillet 2000, en vertu de plusieurs décisions successives ayant pris la forme de " contrats de placement " signés par M. AlainM. et par le président du conseil général du Jura ; qu'il a été reconnu coupable par la cour d'assises des mineurs du Jura d'un crime et de délits commis entre le 3 septembre 1998 et le courant de l'année 1999 et qu'il a été condamné par cette cour, solidairement avec son père pris en sa qualité de civilement responsable, à payer à la représentante légale de la victime mineure des sommes de 15 000 et 5 000 euros en réparation du préjudice subi respectivement par la victime et par sa mère ; que le Fonds de garantie des victimes des actes de terrorisme et d'autres infractions a réglé ces indemnités ; que le tribunal de grande instance de Dole, par un jugement du 5 août 2009, a condamné solidairement M. AlainM. et la société Groupama Grand Est, son assureur, à payer au Fonds de garantie la somme de 20 000 euros au titre des intérêts civils et la somme de 1 500 euros au titre de l'article 700 du code de procédure civile, et a condamné la société d'assurance à garantir M. AlainM. des condamnations prononcées à son encontre ; que, par un jugement du 19 décembre 2012, le tribunal administratif de Besançon a condamné, à la demande de la société Groupama Grand Est, le département du Jura à verser à cette société la somme de 20 000 euros, exposée du fait de la réparation des préjudices causés par Aurélien M. ; que par un arrêt du 28 novembre 2013, contre lequel la société Groupama Grand Est se pourvoit en cassation, la cour administrative d'appel de Nancy a annulé le jugement du tribunal administratif et rejeté la demande de cette société ;<br clear="none"/>
<br clear="none"/>
2. Considérant qu'aux termes de l'article 46 du code de la famille et de l'aide sociale, en vigueur au moment des faits : " Sont pris en charge par le service d'aide sociale à l'enfance sur décision du président du conseil général : / 1° Les mineurs qui ne peuvent provisoirement être maintenus dans leur milieu de vie habituel (...) " ; qu'il appartient au juge administratif, saisi d'une action en responsabilité pour des faits imputables à un mineur pris en charge par le service d'aide sociale à l'enfance, de déterminer si, compte tenu des conditions d'accueil du mineur, notamment la durée de cet accueil et le rythme des retours du mineur dans sa famille, ainsi que des obligations qui en résultent pour le service d'aide sociale à l'enfance et pour les titulaires de l'autorité parentale, la décision du président du conseil général, devenu conseil départemental, prise sur le fondement de ces dispositions et aujourd'hui sur celui de l'article L. 222-5 du code de l'action sociale et des familles, avec le consentement des titulaires de l'autorité parentale, s'analyse comme une prise en charge durable et globale de ce mineur, pour une période convenue, par l'aide sociale à l'enfance ; que si tel est le cas, cette décision a pour effet de transférer au département la responsabilité d'organiser, de diriger et de contrôler la vie du mineur durant cette période ; que ni la circonstance que la décision de prise en charge du mineur prévoie un retour de celui-ci dans son milieu familial de façon ponctuelle ou selon un rythme qu'elle détermine ni celle que le mineur y retourne de sa propre initiative ne font par elles-mêmes obstacle à ce que cette décision entraîne un tel transfert de responsabilité ; qu'en raison des pouvoirs dont le département se trouve, dans ce cas, investi, sa responsabilité est engagée, même sans faute, pour les dommages causés aux tiers par ce mineur, y compris lorsque ces dommages sont survenus alors que le mineur est hébergé par ses parents, dès lors qu'il n'a pas été mis fin à cette prise en charge par le service d'aide sociale à l'enfance par décision des titulaires de l'autorité parentale ou qu'elle n'a pas été suspendue ou interrompue par l'autorité administrative ou judiciaire ; qu'à l'égard de la victime, cette responsabilité n'est susceptible d'être atténuée ou supprimée que dans le cas où le dommage est imputable à une faute de celle-ci ou à un cas de force majeure ; qu'en outre, dans le cadre d'une action en garantie, le département peut, le cas échéant, se prévaloir de la faute du tiers ayant concouru à la réalisation du dommage ;<br clear="none"/>
<br clear="none"/>
3. Considérant que, dans le cas où le département ne peut être regardé comme ayant pris une décision de prise en charge durable et globale du mineur, les titulaires de l'autorité parentale demeurent responsables des dommages causés aux tiers par celui-ci ;<br clear="none"/>
<br clear="none"/>
4. Considérant que la cour administrative d'appel de Nancy a relevé que les agissements délictueux d'Aurélien M. avaient été commis entre le 3 septembre 1998 et courant 1999, à proximité du domicile de son père dans le Jura, alors qu'il était accueilli durant la semaine dans un centre de formation de la Haute-Saône et les samedis et dimanches par son père à son domicile ; qu'elle en a déduit que, intervenus alors que le mineur était sous la garde de l'un de ses parents, ils n'engageaient pas la responsabilité du département du Jura ; qu'en se fondant sur la seule circonstance qu'Aurélien M. était accueilli en fin de semaine par son père pour en déduire que la responsabilité du département n'était pas engagée, sans rechercher si la décision par laquelle le président du conseil général avait décidé de le prendre en charge, formalisée dans des contrats de placement, avait eu pour effet de transférer au département la responsabilité d'organiser, diriger et contrôler sa vie pendant la durée de cette prise en charge, la cour a commis une erreur de droit ;<br clear="none"/>
<br clear="none"/>
5. Considérant que l'autorité relative de la chose jugée par le juge civil ne peut être utilement invoquée en l'absence d'identité d'objet, de cause et de parties ; que la cour d'assises des mineurs du Jura, par un arrêt du 23 novembre 2004, a statué sur les intérêts civils à la demande de la victime et de sa mère ; que le département du Jura, qui n'était pas partie à cette instance, n'est pas fondé à demander que le motif tiré de ce que cet arrêt a déclaré M. AlainM. civilement responsable de son fils mineur soit substitué au motif retenu par l'arrêt attaqué de la cour administrative d'appel de Nancy ;<br clear="none"/>
<br clear="none"/>
6. Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la société requérante est fondée à demander l'annulation de l'arrêt qu'elle attaque ;<br clear="none"/>
<br clear="none"/>
7. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du département du Jura la somme de 3 000 euros à verser à la société Groupama Grand Est au titre de l'article L. 761-1 du code de justice administrative ; que les dispositions de cet article font obstacle à ce qu'une somme soit mise au même titre à la charge de la société Groupama Grand Est, qui n'est pas, dans la présente instance, la partie perdante ;<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
D E C I D E :<br clear="none"/>
--------------<br clear="none"/>
<br clear="none"/>
Article 1er : L'arrêt de la cour administrative d'appel de Nancy du 28 novembre 2013 est annulé.<br clear="none"/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nancy.<br clear="none"/>
Article 3 : Le département du Jura versera à la société Groupama Grand Est la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br clear="none"/>
Article 4 : Les conclusions du département du Jura présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br clear="none"/>
Article 5 : La présente décision sera notifiée à la société Groupama Grand Est et au département du Jura.<br clear="none"/>
Copie en sera adressée à la ministre des familles, de l'enfance et des droits des femmes.</p>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-02-02-02 AIDE SOCIALE. DIFFÉRENTES FORMES D'AIDE SOCIALE. AIDE SOCIALE À L'ENFANCE. PLACEMENT DES MINEURS. - PLACEMENT D'UN MINEUR PAR DÉCISION DU PRÉSIDENT DU CONSEIL DÉPARTEMENTAL (ART. L. 222-5 DU CASF) - RESPONSABILITÉ SANS FAUTE DU DÉPARTEMENT POUR LES ACCIDENTS PROVOQUÉS PAR LE MINEUR - CONDITION - PRISE EN CHARGE DURABLE ET GLOBALE DU MINEUR [RJ1] - PORTÉE DE CETTE RESPONSABILITÉ.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-01-02-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. FAITS SUSCEPTIBLES OU NON D'OUVRIR UNE ACTION EN RESPONSABILITÉ. FONDEMENT DE LA RESPONSABILITÉ. RESPONSABILITÉ SANS FAUTE. - PLACEMENT D'UN MINEUR PAR DÉCISION DU PRÉSIDENT DU CONSEIL DÉPARTEMENTAL (ART. L. 222-5 DU CASF) - RESPONSABILITÉ SANS FAUTE DU DÉPARTEMENT POUR LES ACCIDENTS PROVOQUÉS PAR LE MINEUR - CONDITION - PRISE EN CHARGE DURABLE ET GLOBALE DU MINEUR [RJ1] - PORTÉE DE CETTE RESPONSABILITÉ.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">60-02-012 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICES SOCIAUX. - AIDE SOCIALE À L'ENFANCE - PLACEMENT D'UN MINEUR PAR DÉCISION DU PRÉSIDENT DU CONSEIL DÉPARTEMENTAL (ART. L. 222-5 DU CASF) - RESPONSABILITÉ SANS FAUTE DU DÉPARTEMENT POUR LES ACCIDENTS PROVOQUÉS PAR LE MINEUR - CONDITION - PRISE EN CHARGE DURABLE ET GLOBALE DU MINEUR [RJ1] - PORTÉE DE CETTE RESPONSABILITÉ.
</SCT>
<ANA ID="9A"> 04-02-02-02 Prise en charge d'un mineur par le service d'aide sociale à l'enfance sur décision du président du conseil départemental (art. 46 du code de la famille et de l'aide sociale, aujourd'hui art. L. 222-5 du code de l'action sociale et des familles (CASF)).... ,,Il appartient au juge administratif, saisi d'une action en responsabilité pour des faits imputables à un mineur ainsi pris en charge, de déterminer si, compte tenu des conditions d'accueil du mineur, notamment la durée de cet accueil et le rythme des retours du mineur dans sa famille, ainsi que des obligations qui en résultent pour le service d'aide sociale à l'enfance et pour les titulaires de l'autorité parentale, la décision du président du conseil départemental, avec le consentement des titulaires de l'autorité parentale, s'analyse comme une prise en charge durable et globale de ce mineur, pour une période convenue, par l'aide sociale à l'enfance. Si tel est le cas, cette décision a pour effet de transférer au département la responsabilité d'organiser, de diriger et de contrôler la vie du mineur durant cette période.... ,,Ni la circonstance que la décision de prise en charge du mineur prévoie un retour de celui-ci dans son milieu familial de façon ponctuelle ou selon un rythme qu'elle détermine ni celle que le mineur y retourne de sa propre initiative ne font par elles-mêmes obstacle à ce que cette décision entraîne un tel transfert de responsabilité. En raison des pouvoirs dont le département se trouve, dans ce cas, investi, sa responsabilité est engagée, même sans faute, pour les dommages causés aux tiers par ce mineur, y compris lorsque ces dommages sont survenus alors que le mineur est hébergé par ses parents, dès lors qu'il n'a pas été mis fin à cette prise en charge par le service d'aide sociale à l'enfance par décision des titulaires de l'autorité parentale ou qu'elle n'a pas été suspendue ou interrompue par l'autorité administrative ou judiciaire [RJ2].... ,,A l'égard de la victime, cette responsabilité n'est susceptible d'être atténuée ou supprimée que dans le cas où le dommage est imputable à une faute de celle-ci ou à un cas de force majeure. En outre, dans le cadre d'une action en garantie, le département peut, le cas échéant, se prévaloir de la faute du tiers ayant concouru à la réalisation du dommage.</ANA>
<ANA ID="9B"> 60-01-02-01 Prise en charge d'un mineur par le service d'aide sociale à l'enfance sur décision du président du conseil départemental (art. 46 du code de la famille et de l'aide sociale, auj. art. L. 222-5 du code de l'action sociale et des familles (CASF)).... ,,Il appartient au juge administratif, saisi d'une action en responsabilité pour des faits imputables à un mineur ainsi pris en charge, de déterminer si, compte tenu des conditions d'accueil du mineur, notamment la durée de cet accueil et le rythme des retours du mineur dans sa famille, ainsi que des obligations qui en résultent pour le service d'aide sociale à l'enfance et pour les titulaires de l'autorité parentale, la décision du président du conseil départemental, avec le consentement des titulaires de l'autorité parentale, s'analyse comme une prise en charge durable et globale de ce mineur, pour une période convenue, par l'aide sociale à l'enfance. Si tel est le cas, cette décision a pour effet de transférer au département la responsabilité d'organiser, de diriger et de contrôler la vie du mineur durant cette période.... ,,Ni la circonstance que la décision de prise en charge du mineur prévoie un retour de celui-ci dans son milieu familial de façon ponctuelle ou selon un rythme qu'elle détermine ni celle que le mineur y retourne de sa propre initiative ne font par elles-mêmes obstacle à ce que cette décision entraîne un tel transfert de responsabilité. En raison des pouvoirs dont le département se trouve, dans ce cas, investi, sa responsabilité est engagée, même sans faute, pour les dommages causés aux tiers par ce mineur, y compris lorsque ces dommages sont survenus alors que le mineur est hébergé par ses parents, dès lors qu'il n'a pas été mis fin à cette prise en charge par le service d'aide sociale à l'enfance par décision des titulaires de l'autorité parentale ou qu'elle n'a pas été suspendue ou interrompue par l'autorité administrative ou judiciaire [RJ2].... ,,A l'égard de la victime, cette responsabilité n'est susceptible d'être atténuée ou supprimée que dans le cas où le dommage est imputable à une faute de celle-ci ou à un cas de force majeure. En outre, dans le cadre d'une action en garantie, le département peut, le cas échéant, se prévaloir de la faute du tiers ayant concouru à la réalisation du dommage.</ANA>
<ANA ID="9C"> 60-02-012 Prise en charge d'un mineur par le service d'aide sociale à l'enfance sur décision du président du conseil départemental (art. 46 du code de la famille et de l'aide sociale, auj. art. L. 222-5 du code de l'action sociale et des familles (CASF)).... ,,Il appartient au juge administratif, saisi d'une action en responsabilité pour des faits imputables à un mineur ainsi pris en charge, de déterminer si, compte tenu des conditions d'accueil du mineur, notamment la durée de cet accueil et le rythme des retours du mineur dans sa famille, ainsi que des obligations qui en résultent pour le service d'aide sociale à l'enfance et pour les titulaires de l'autorité parentale, la décision du président du conseil départemental, avec le consentement des titulaires de l'autorité parentale, s'analyse comme une prise en charge durable et globale de ce mineur, pour une période convenue, par l'aide sociale à l'enfance. Si tel est le cas, cette décision a pour effet de transférer au département la responsabilité d'organiser, de diriger et de contrôler la vie du mineur durant cette période.... ,,Ni la circonstance que la décision de prise en charge du mineur prévoie un retour de celui-ci dans son milieu familial de façon ponctuelle ou selon un rythme qu'elle détermine ni celle que le mineur y retourne de sa propre initiative ne font par elles-mêmes obstacle à ce que cette décision entraîne un tel transfert de responsabilité. En raison des pouvoirs dont le département se trouve, dans ce cas, investi, sa responsabilité est engagée, même sans faute, pour les dommages causés aux tiers par ce mineur, y compris lorsque ces dommages sont survenus alors que le mineur est hébergé par ses parents, dès lors qu'il n'a pas été mis fin à cette prise en charge par le service d'aide sociale à l'enfance par décision des titulaires de l'autorité parentale ou qu'elle n'a pas été suspendue ou interrompue par l'autorité administrative ou judiciaire [RJ2].... ,,A l'égard de la victime, cette responsabilité n'est susceptible d'être atténuée ou supprimée que dans le cas où le dommage est imputable à une faute de celle-ci ou à un cas de force majeure. En outre, dans le cadre d'une action en garantie, le département peut, le cas échéant, se prévaloir de la faute du tiers ayant concouru à la réalisation du dommage.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 26 mai 2008, Département des Côtes d'Armor, n° 290495, T. p. 609. Rappr., s'agissant d'un placement judiciaire, CE, Section, 11 février 2005, GIE Axa Courtage, n° 252169, p. 45., ,,[RJ2]Rappr., s'agissant d'un placement judiciaire, CE, 3 juin 2009, Garde des sceaux, ministre de la justice c/ Société Gan Assurances, n° 300924, T. p. 632.


</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
