<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042520611</ID>
<ANCIEN_ID>JG_L_2020_11_000000427492</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/52/06/CETATEXT000042520611.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 13/11/2020, 427492</TITRE>
<DATE_DEC>2020-11-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>427492</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR>M. François Charmont</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:427492.20201113</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au tribunal administratif de Paris d'annuler pour excès de pouvoir la décision du 23 juillet 2015 par laquelle le ministre de l'intérieur a rejeté sa demande tendant à l'attribution de l'indemnité dite " forfaitaire dégressive " attribuée aux contrôleurs de classe normale des services techniques du ministère de l'intérieur, d'enjoindre au ministre de l'intérieur de lui attribuer une indemnité d'un montant mensuel au moins égal à 450 euros ou de réexaminer sa demande et de condamner l'Etat à lui verser une indemnité au moins égale à 30 800 euros en réparation des préjudices qu'il estime avoir subis en raison de la faute commise par l'Etat en refusant de lui verser cette indemnité alors qu'elle était payée à des fonctionnaires appartenant au même corps que lui. Par un jugement n° 1602514/5-1 du 16 mars 2017, le tribunal administratif a annulé cette décision, enjoint au ministre de l'intérieur de verser à M. A..., à compter du 1er janvier 2012, l'indemnité dite " forfaitaire dégressive " pour un montant égal à celui perçu par les agents placés dans une situation analogue à la sienne et rejeté le surplus de ses conclusions. <br/>
<br/>
              Par un arrêt n° 17PA01797 du 29 novembre 2018, la cour administrative d'appel de Paris a, sur appel du ministre d'Etat, ministre de l'intérieur, annulé ce jugement et rejeté la demande de M. A....<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 30 janvier et 2 mai 2019 et le 21 septembre 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L.761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu : <br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - le décret n° 55-755 du 25 mai 1955 ; <br/>
              - le décret n° 61-1226 du 6 novembre 1961 ; <br/>
              - le décret n°65-340 du 14 avril 1965 ;<br/>
              - le décret n° 2011-1988 du 27 décembre 2011 ;<br/>
              - le décret n° 2018-399 du 28 mai 2018 ;<br/>
	- le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Charmont, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de M. A....<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. A..., contrôleur de classe normale des services techniques du ministère de l'intérieur, a sollicité le 11 juin 2015 l'attribution de l'indemnité forfaitaire dégressive instituée par le décret du 6 novembre 1961. Par une décision du 23 juillet 2015, le ministre de l'intérieur a rejeté sa demande. Par un jugement du 16 mars 2017, le tribunal administratif de Paris a annulé cette décision et enjoint au ministre de l'intérieur de verser à M. A... une indemnité d'un montant égal à celui perçu par les agents placés dans une situation analogue à la sienne. M. A... se pourvoit en cassation contre l'arrêt du 29 novembre 2018 par lequel la cour administrative d'appel de Paris a, sur appel du ministre d'Etat, ministre de l'intérieur, annulé ce jugement et rejeté sa demande de première instance.<br/>
<br/>
              2. Aux termes de l'article 20 de la loi du 13 juillet 1983 portant droits et obligations des fonctionnaires : " Les fonctionnaires ont droit, après service fait, à une rémunération comprenant (...) les indemnités instituées par un texte législatif ou réglementaire (...) ".<br/>
<br/>
              3. Aux termes de l'article 1er du décret du 6 novembre 1961 attribuant une indemnité forfaitaire dégressive aux contrôleurs et agents de maîtrise des services du matériel du ministère de l'intérieur : " Il est accordé aux chefs d'équipe, contremaîtres, maîtres artisans et contrôleurs régis par le décret susvisé du 25 mai 1955, en service en métropole et en Algérie, une indemnité forfaitaire dégressive qui, pour les chefs d'équipe classés au premier échelon de leur grade, est égale à la moitié de la différence entre leur rémunération de fonctionnaire, comprenant notamment la prime de rendement moyenne, et celle d'un ouvrier du ministère de l'intérieur exerçant ses fonctions dans le même service, classé au huitième échelon du groupe VII, et recevant une prime de rendement moyenne ainsi que la prime de fonctions de chef d'équipe (...) ". Si ces dispositions n'ont été expressément abrogées que par l'article 1er du décret du 28 mai 2018 abrogeant le décret du 6 novembre 1961 attribuant une indemnité forfaitaire dégressive aux contrôleurs et agents de maîtrise des services du matériel du ministère de l'intérieur, il résulte des dispositions du décret du 14 avril 1965 relatif au statut particulier des contrôleurs des services techniques du matériel du ministère de l'intérieur, de celles du décret du 17 mars 1997 relatif au statut particulier du corps des contrôleurs des services techniques du ministère de l'intérieur et de celles du décret du 27 décembre 2011 portant statut particulier du corps des contrôleurs des services techniques du ministère de l'intérieur, que le corps des contrôleurs des services techniques du ministère de l'intérieur auquel appartient M. A... ne peut être regardé comme régi par le décret du 25 mai 1955 portant règlement d'administration publique relatif au statut du personnel technique des services du matériel du ministère de l'intérieur.<br/>
<br/>
              4. Par suite, la cour administrative d'appel de Paris n'a pas commis d'erreur de droit en jugeant que M. A..., n'appartenant pas à un corps régi par ce décret du 25 mai 1955, n'avait pas droit à l'attribution de l'indemnité forfaitaire dégressive instituée par les dispositions citées ci-dessus de l'article 1er du décret du 6 novembre 1961.<br/>
<br/>
              5. S'il ressort par ailleurs des pièces du dossier qui lui était soumis que certains agents appartenant, comme M. A..., au corps des contrôleurs des services techniques du ministère de l'intérieur, bénéficiaient, à la même date, du versement d'une indemnité calculée selon les modalités prévues par le décret du 6 novembre 1961, la cour n'a pas commis d'erreur de droit en jugeant que, dès lors que le ministre était tenu de rejeter la demande dont M. A... l'avait saisi, les moyens tirés de ce que le principe d'égalité aurait imposé le versement d'une telle indemnité, ainsi que les moyens tirés de l'incompétence de l'auteur de la décision de refus et de son insuffisante motivation étaient inopérants.<br/>
<br/>
              6. Enfin, en jugeant que la faute commise par l'administration en versant illégalement une prime à d'autres agents que M. A... est dépourvue de lien suffisamment direct avec le préjudice moral que ce dernier estime avoir subi à raison de ce que ces agents appartiennent au même corps que lui, la cour, qui n'a pas insuffisamment motivé son arrêt sur ce point, n'a pas inexactement qualifié les faits qui lui était soumis et n'a pas commis d'erreur de droit.<br/>
<br/>
              7. Il résulte de tout ce qui précède que M. A... n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque.<br/>
<br/>
              8. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A... est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. B... A... et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-03-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. ÉGALITÉ DEVANT LE SERVICE PUBLIC. ÉGALITÉ DE TRAITEMENT DES AGENTS PUBLICS. - REFUS D'ATTRIBUTION DE L'INDEMNITÉ FORFAITAIRE DÉGRESSIVE, PRÉVUE PAR LE DÉCRET DU 6 NOVEMBRE 1961 AU BÉNÉFICE DES CONTRÔLEURS ET AGENTS DE MAÎTRISE DES SERVICES DU MATÉRIEL DU MINISTÈRE DE L'INTÉRIEUR, À UN AGENT NE RELEVANT PAS DE CE CORPS - MOYEN INOPÉRANT [RJ2], LE MINISTRE ÉTANT EN SITUATION DE COMPÉTENCE LIÉE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">01-05-01-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - MOTIFS. POUVOIRS ET OBLIGATIONS DE L'ADMINISTRATION. COMPÉTENCE LIÉE. - EXISTENCE [RJ1] - REFUS D'ATTRIBUTION DE L'INDEMNITÉ FORFAITAIRE DÉGRESSIVE, PRÉVUE PAR LE DÉCRET DU 6 NOVEMBRE 1961 AU BÉNÉFICE DES CONTRÔLEURS ET AGENTS DE MAÎTRISE DES SERVICES DU MATÉRIEL DU MINISTÈRE DE L'INTÉRIEUR, À UN AGENT NE RELEVANT PAS DE CE CORPS - CONSÉQUENCE - MOYEN TIRÉ DE LA MÉCONNAISSANCE DU PRINCIPE D'ÉGALITÉ DE TRAITEMENT DES AGENTS PUBLICS - MOYEN INOPÉRANT [RJ2].
</SCT>
<ANA ID="9A"> 01-04-03-03-02 Il résulte des décrets n° 65-340 du 14 avril 1965, n° 97-259 du 17 mars 1997 et n° 2011-1988 du 27 décembre 2011 que le corps des contrôleurs des services techniques du ministère de l'intérieur ne peut être regardé comme régi par le décret n° 55-755 du 25 mai 1955 portant règlement d'administration publique relatif au statut du personnel technique des services du matériel du ministère de l'intérieur.,,,Un agent relevant du corps des contrôleurs des services techniques du ministère de l'intérieur n'appartient pas à un corps régi par le décret du 25 mai 1955 et, dès lors, n'a pas droit à l'attribution de l'indemnité forfaitaire dégressive instituée par l'article 1er du décret n° 61-1226 du 6 novembre 1961 au bénéfice des contrôleurs et agents de maîtrise des services du matériel du ministère de l'intérieur.,,,Par suite, le ministre se trouve en situation de compétence liée pour rejeter la demande formée par un tel agent tendant à l'attribution de cette indemnité et, dès lors, le moyen tiré de ce que le principe d'égalité aurait imposé le versement d'une telle indemnité est inopérant.</ANA>
<ANA ID="9B"> 01-05-01-03 Il résulte des décrets n° 65-340 du 14 avril 1965, n° 97-259 du 17 mars 1997 et n° 2011-1988 du 27 décembre 2011 que le corps des contrôleurs des services techniques du ministère de l'intérieur ne peut être regardé comme régi par le décret n° 55-755 du 25 mai 1955 portant règlement d'administration publique relatif au statut du personnel technique des services du matériel du ministère de l'intérieur.,,,Un agent relevant du corps des contrôleurs des services techniques du ministère de l'intérieur n'appartient pas à un corps régi par le décret du 25 mai 1955 et, dès lors, n'a pas droit à l'attribution de l'indemnité forfaitaire dégressive instituée par l'article 1er du décret n° 61-1226 du 6 novembre 1961 au bénéfice des contrôleurs et agents de maîtrise des services du matériel du ministère de l'intérieur.,,,Par suite, le ministre se trouve en situation de compétence liée pour rejeter la demande formée par un tel agent tendant à l'attribution de cette indemnité et, dès lors, le moyen tiré de ce que le principe d'égalité aurait imposé le versement d'une telle indemnité est inopérant.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 3 février 1999, M. Montaignac, n° 149722, p. 6.,,[RJ2] Comp., s'agissant de l'octroi d'un avantage non prévu par les textes, CE, 18 novembre 2011, Garde des sceaux, ministre de la justice et des libertés c/,, n° 344563, p. 573.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
