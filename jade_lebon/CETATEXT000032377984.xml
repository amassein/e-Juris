<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032377984</ID>
<ANCIEN_ID>JG_L_2016_04_000000373889</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/37/79/CETATEXT000032377984.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème / 4ème SSR, 07/04/2016, 373889</TITRE>
<DATE_DEC>2016-04-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>373889</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème / 4ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>HAAS ; LE PRADO</AVOCATS>
<RAPPORTEUR>Mme Manon Perrière</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:373889.20160407</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Procédure contentieuse antérieure :<br/>
<br/>
              Mme et M. A...B...ont demandé au tribunal administratif de Clermont-Ferrand de condamner le centre hospitalier de Riom à les indemniser, ainsi que leur fille JulieB..., des préjudices résultant du handicap dont celle-ci est atteinte en raison de fautes commises par cet établissement lors de sa naissance le 13 janvier 1999. Par un jugement n° 071812 du 28 janvier 2010, rectifié par ordonnances des 5 et 10 mars 2010, le tribunal administratif a condamné le centre hospitalier à verser aux requérants, au titre des frais liés au handicap, une indemnité de 14 590,95 euros et une rente annuelle de 161 541,26 euros, au titre des préjudices personnels de leur fille, une indemnité de 514 808 euros et, au titre de leurs préjudices personnels, une indemnité de 30 000 euros chacun.<br/>
<br/>
              Par un arrêt n° 10LY00746, 10LY00892, 10LY00895 du 12 mai 2011, la cour administrative d'appel de Lyon, saisie par le centre hospitalier de Riom, la caisse primaire d'assurance maladie (CPAM) du Puy-de-Dôme et M. et MmeB..., a réformé le jugement du tribunal administratif.<br/>
<br/>
              Par une décision n° 350 688 du 15 mai 2013, le Conseil d'Etat, statuant au contentieux, a, sur le pourvoi de M. et MmeB..., annulé cet arrêt et renvoyé l'affaire devant la cour administrative d'appel de Lyon.<br/>
<br/>
              Par un arrêt n° 13LY01327, 13LY01429, 13LY01434 du 10 octobre 2013, la cour administrative d'appel de Lyon a condamné le centre hospitalier à verser à M. et Mme B..., en leur qualité de représentants légaux de leur fille et jusqu'à la majorité de celle-ci, une indemnité de 220 euros par nuit passée au domicile familial et une rente annuelle de 9 430 euros au titre des frais liés au handicap et a réformé le jugement du tribunal administratif du 28 janvier 2010 en ce qu'il avait de contraire à son arrêt. <br/>
<br/>
              Procédure devant le Conseil d'Etat :<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 10 décembre 2013 et 11 mars 2014 au secrétariat du contentieux du Conseil d'Etat, M. et Mme B... demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge du centre hospitalier de Riom la somme de 6 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Manon Perrière, auditeur,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Haas, avocat de M. et Mme B...et à Me Le Prado, avocat du centre hospitalier de Riom ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que JulieB..., née le 13 janvier 1999 en état de mort apparente au centre hospitalier de Riom, est demeurée atteinte à l'issue de sa réanimation de lourdes séquelles neurologiques ; que M. et MmeB..., agissant au nom de leur fille et en leur nom personnel, ont recherché la responsabilité de l'établissement ; que, par un jugement du 21 octobre 2003, confirmé en appel, le tribunal administratif de Clermont-Ferrand, a retenu l'existence de fautes dans la prise en charge obstétricale de Mme B...de nature à engager sa responsabilité, l'a condamné à indemniser la caisse primaire d'assurance maladie du Puy-de-Dôme mais a rejeté comme non chiffrées les conclusions de M. et Mme B...; qu'après avoir obtenu du juge des référés, selon le cas, du tribunal administratif de Clermont-Ferrand ou de la cour administrative d'appel de Lyon le bénéfice de provisions au titre de dépenses liées à l'état de santé de leur fille, M. et Mme B...ont présenté le 17 octobre 2007 un nouveau recours indemnitaire contre le centre hospitalier ; que, par un jugement du 28 janvier 2010 rectifié par des ordonnances des 5 et 10 mars 2010, le tribunal administratif a condamné cet établissement à leur verser, au titre des dépenses liées au handicap, une somme de 14 594 euros et une rente annuelle de 161 541,26 euros, au titre des préjudices personnels de JulieB..., une indemnité de 514 808 euros et, au titre des troubles subis par ses parents dans leurs conditions d'existence, une indemnité de 30 000 euros chacun ; que, saisie par les intéressés, le centre hospitalier et la caisse primaire, la cour administrative d'appel de Lyon a rendu le 12 mai 2011 un arrêt qui a été annulé par le Conseil d'Etat ; que, statuant à nouveau après renvoi de l'affaire par un arrêt du 10 octobre 2013, elle a retenu que les dépenses exposées pendant la période antérieure à son arrêt s'élevaient au total à 138 127,31 euros, plus 220 euros par nuit passée par l'enfant au domicile familial, que Julie B...avait subi un préjudice scolaire évalué à 30 000 euros et des préjudices personnels évalués à 560 000 euros et que ses parents avaient subi des troubles dans leurs conditions d'existence évalués à 40 000 euros chacun ; que la cour a toutefois refusé de relever les indemnités en capital accordées par les premiers juges au motif que les intéressés n'étaient pas recevables, en l'absence d'aggravation du dommage postérieure au jugement, à demander un montant supérieur à celui qu'ils avaient sollicité en première instance ; qu'elle s'est bornée à mettre à la charge du centre hospitalier, au titre des dépenses futures, le versement, à compter de la date de son arrêt et jusqu'à la majorité de JulieB..., d'une rente calculée sur la base d'un taux de 220 euros par nuit passée au domicile familial destinée à couvrir les frais d'assistance par une tierce personne et d'une rente annuelle de 9 430 euros destinée à couvrir les autres frais liés au handicap ; que M. et Mme B...se pourvoient en cassation contre cet arrêt ; <br/>
<br/>
              Sur l'arrêt attaqué en tant qu'il évalue les préjudices subis par Julie B...et par ses parents :<br/>
<br/>
              2. Considérant, en premier lieu, que la cour a visé et analysé les mémoires enregistrés les 22 août et 12 septembre 2013 par lesquels M. et Mme B...apportaient des précisions et des justificatifs relatifs à l'évaluation de leurs préjudices ; que si elle n'a pas retenu les montants mentionnés dans ces mémoires, rien ne permet d'affirmer qu'elle ait, comme le soutiennent les requérants, refusé de prendre ces mémoires en considération ;<br/>
<br/>
              3. Considérant, en deuxième lieu, que c'est par une appréciation souveraine, exempte de dénaturation, que la cour a retenu, d'une part, un montant de dépenses annuelles de 1 460 euros pour les frais d'aides spécialisées à l'éveil sensoriel et communication, sans y inclure les dépenses afférentes à l'acquisition de jeux d'éveil dont elle a estimé qu'il n'était pas établi qu'ils n'auraient pas été acquis en l'absence de handicap, d'autre part, un montant de dépenses annuelles de 1 460 euros pour les frais d'appareillage, comprenant les frais d'aides techniques au positionnement et à la mobilité et les frais de toilette et d'hygiène et, enfin, un montant de 4 681,41 euros pour les frais afférents au déménagement vers un logement de plain-pied ; <br/>
<br/>
              4. Considérant, en troisième lieu, qu'en évaluant le montant annuel des frais de transports à 3 000 euros alors que le tribunal administratif l'avait évalué à 5 300 euros, la cour administrative d'appel, devant laquelle le centre hospitalier de Riom soutenait que les sommes allouées par les premiers juges étaient excessives, n'a pas statué au-delà des conclusions dont elle était saisie et ne s'est pas méprise sur leur portée ;<br/>
<br/>
              5. Considérant, en cinquième lieu, qu'en évaluant à 220 euros par nuit passée au domicile familial les frais afférents à l'assistance d'une tierce personne, " nonobstant la circonstance que certains prestataires externes interviennent ponctuellement pour un tarif horaire plus élevé ", la cour administrative d'appel, dont l'arrêt est suffisamment motivé sur ce point, n'a pas commis d'erreur de droit et a porté sur les faits de l'espèce une appréciation souveraine exempte de dénaturation ; <br/>
<br/>
              Sur l'arrêt attaqué en tant qu'il fixe le montant des indemnités à la charge du centre hospitalier de Riom :<br/>
<br/>
              6. Considérant que la personne qui a demandé en première instance la réparation des conséquences dommageables d'un fait qu'elle impute à une administration est recevable à détailler ces conséquences devant le juge d'appel, en invoquant le cas échéant des chefs de préjudice dont elle n'avait pas fait état devant les premiers juges, dès lors que ces chefs de préjudice se rattachent au même fait générateur et que ses prétentions demeurent... ;<br/>
<br/>
              7. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, devant le tribunal administratif de Clermont-Ferrand, M. et Mme B...avaient demandé, d'une part, à titre personnel, une indemnité de 30 000 euros chacun et, d'autre part, en leur qualité de représentants légaux de leur fille, une indemnité de 514 808 euros et une rente annuelle de 169 426,59 euros à compter du 1er janvier 2007 ; que, pour appliquer la règle selon laquelle les prétentions d'une partie devant le juge d'appel ne peuvent excéder, en l'absence d'éléments nouveaux postérieurs à la décision des premiers juges, le montant total de l'indemnité chiffrée en première instance, il y avait lieu de distinguer le montant demandé par chacun des parents à titre personnel et le montant demandé au nom de l'enfant ; que pour déterminer ce dernier montant, il y avait lieu d'additionner l'indemnité demandée en son nom et le capital représentatif de la rente sollicitée ; <br/>
<br/>
              8. Considérant que, pour fixer les indemnités auxquelles les requérants pouvaient prétendre à un montant inférieur à l'évaluation des préjudices qu'elle avait retenue, la cour administrative d'appel de Lyon a estimé que " l'indemnité en capital allouée à M. et Mme B... ne peut excéder le montant total de 589 398,95 euros mis à la charge du centre hospitalier de Riom par le jugement attaqué " ; que, ce faisant, elle ne s'est pas conformée aux règles rappelées ci-dessus ; que son arrêt est, par suite, entaché d'erreur de droit ;<br/>
<br/>
              9. Considérant qu'il résulte de ce qui précède que l'arrêt attaqué n'encourt pas l'annulation en tant qu'il évalue les préjudices subis par Julie B...et par M. et Mme B... et qu'il met à la charge du centre hospitalier de Riom le versement, à compter de son arrêt et jusqu'à la majorité de JulieB..., des rentes destinées à couvrir les dépenses qui seront exposées au cours de cette période ; qu'il doit, en revanche, être annulé en tant qu'il statue sur le montant des indemnités auxquelles les intéressés peuvent prétendre au titre des préjudices pécuniaires et personnels subis pendant la période antérieure à l'arrêt et statue sur leurs conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
              10. Considérant qu'aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire " ; qu'il y a lieu, par suite, de régler l'affaire au fond dans la limite de la cassation ainsi prononcée ;<br/>
<br/>
              Sur les indemnités réparant les préjudices de JulieB... :<br/>
<br/>
              11. Considérant qu'il résulte des motifs de l'arrêt du 10 octobre 2013 de la cour administrative d'appel de Lyon, devenu définitif sur ce point, que les dépenses liées à l'état de santé de JulieB..., autres que les frais d'assistance par une tierce personne, exposées pendant la période comprise entre la naissance de l'enfant et le 10 octobre 2013 se sont élevés à 44 500 euros pour les frais de déplacement, 7 145,95 euros pour les frais d'aménagement d'un véhicule et d'acquisition d'un siège auto, 29 095 euros pour les frais médicaux, pharmaceutiques et paramédicaux non pris en charge par l'assurance maladie, 34 483,05 euros pour les frais d'appareillage, 16 526,90 euros au titre de frais d'aides relatives à l'éveil sensoriel et la communication, 1 695 euros pour les frais d'aides à la surveillance et facilitatrice tierce personne, et 4 681,41 euros pour les frais de déménagement et d'aménagement du logement, soit au total 138 127,31 euros ; <br/>
<br/>
              12. Considérant qu'il résulte également des motifs de l'arrêt de la cour administrative d'appel que les frais d'assistance par une tierce personne, depuis la naissance de Julie jusqu'au 10 octobre 2013, doivent être évalués sur la base d'une somme de 220 euros par nuit passée par l'enfant au domicile familial ; que, compte tenu du nombre de nuits effectivement passées à ce domicile, la somme due à ce titre doit être fixée à 1 107 040 euros ; <br/>
<br/>
              13. Considérant qu'il résulte enfin des motifs de l'arrêt du 10 octobre 2013 que les troubles de toute nature subis par Julie B...ont été évalués, à titre provisionnel dans l'attente de la consolidation de son état de santé, à 560 000 euros et son préjudice scolaire à 30 000 euros ;<br/>
<br/>
              14. Considérant qu'il résulte de ce qui précède que M. et Mme B...peuvent prétendre, en leur qualité de représentants légaux de leur fille, à une indemnité de 1 835 167,31 euros au titre des dépenses antérieures au 10 octobre 2013 et des préjudices personnels de JulieB... ; que le cumul de cette indemnité avec les rentes accordées par la cour administrative d'appel au titre des dépenses postérieures au 10 octobre 2013 n'excède pas le montant total demandé par M. et MmeB..., sous forme d'indemnités et de rente, devant le tribunal administratif ; que, dès lors que les dépenses ont été évaluées depuis la naissance de l'enfant, il y a lieu de déduire des sommes dues par le centre hospitalier celles qu'il a versées sous forme de provisions, en exécution des ordonnances du juge des référés mentionnées au point 1, pour couvrir des dépenses effectuées entre 1999 et 2007 ; que doivent également être déduites les sommes versées au titre des mêmes préjudices en exécution du jugement du 28 janvier 2010 du tribunal administratif de Clermont-Ferrand et de l'arrêt du 12 mai 2011 de la cour administrative d'appel de Lyon ; que le jugement du 28 janvier 2010 du tribunal administratif de Clermont-Ferrand doit être réformé en tant qu'il fixe à un montant différent les sommes dues par le centre hospitalier ; <br/>
<br/>
              Sur les indemnités réparant les préjudices de M. et MmeB... :<br/>
<br/>
              15. Considérant que, dans leur demande de première instance, M. et Mme B... ont chiffré à 30 000 euros pour chacun d'eux le montant de leur préjudice propre, résultant des troubles qu'ils subissent dans leurs conditions d'existence ; que le tribunal administratif leur a accordé cette somme ; qu'en l'absence d'éléments nouveaux postérieurs au jugement du tribunal administratif, ils ne sont pas recevables à demander qu'elle soit augmentée en appel ;<br/>
<br/>
              Sur les intérêts :<br/>
<br/>
              16. Considérant que les intérêts au taux légal sont dus à compter du 17 octobre 2007, date de la demande présentée par M. et Mme B...devant le tribunal administratif, pour les indemnités réparant les préjudices personnels et pour les dépenses exposées avant cette date ; que, pour les dépenses exposées entre le 17 octobre 2007 et le 10 octobre 2013, les intérêts courent à compter des dates auxquelles elles ont été exposées ; que la capitalisation des intérêts ayant été demandée le 17 octobre 2008, date à laquelle un an d'intérêts étaient dus, il y sera procédé à cette date et à chaque échéance annuelle ultérieure ; <br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              17. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du centre hospitalier de Riom, au titre de ces dispositions, le versement à M. et Mme B... de la somme de 7 000 euros au titre des frais exposés par eux devant le Conseil d'Etat et devant la cour administrative d'appel de Lyon ; <br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : L'article 5 de l'arrêt du 10 octobre 2013 est annulé en tant qu'il rejette le surplus des conclusions de M. et MmeB.dans la limite du montant total de l'indemnité chiffrée en première instance, augmentée le cas échéant des éléments nouveaux apparus postérieurement au jugement, sous réserve des règles qui gouvernent la recevabilité des demandes fondées sur une cause juridique nouvelle<br/>
Article 2 : Le centre hospitalier de Riom versera à M. et MmeB..., en leur qualité de représentants légaux de leur fille JulieB..., au titre des dépenses antérieures au 10 octobre 2013 liées à son état de santé et de ses préjudices personnels, une indemnité de 1 835 167, 31 euros, sous déduction des provisions versées en exécution de décisions du juge des référés et des sommes versées au titre des mêmes préjudices par le jugement du 28 janvier 2010 du tribunal administratif de Clermont-Ferrand et l'arrêt du 12 mai 2011 de la cour administrative d'appel de Lyon. <br/>
Article 3 : L'indemnité due par le centre hospitalier à M. et Mme B...au titre des troubles dans leurs conditions d'existence demeure fixée à 30 000 euros chacun.<br/>
Article 4 : Les indemnités réparant des préjudices personnels porteront intérêts au taux légal à compter du 17 octobre 2007. Celles réparant des dépenses exposées entre le 17 octobre 2007 et le 10 octobre 2013 porteront intérêts à compter des dates auxquelles ces dépenses ont été exposées. Les intérêts dus au 17 octobre 2008 et à chaque échéance annuelle ultérieure seront capitalisés pour produire eux-mêmes intérêts. <br/>
Article 5 : Les articles 1er à 3 du jugement du 28 janvier 2010 modifié par les ordonnances des 5 et 10 mars 2010 sont annulés.<br/>
Article 6 : Le centre hospitalier de Riom versera à M. et Mme B...une somme de 7 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 7 : Le surplus des conclusions de M. et Mme B...est rejeté.<br/>
Article 8 : La présente décision sera notifiée à Mme C...B...et M. A...B..., ainsi qu'au centre hospitalier de Riom.<br/>
Copie en sera adressée à la caisse primaire d'assurance maladie du Puy-de-Dôme. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-08-01-02 PROCÉDURE. VOIES DE RECOURS. APPEL. CONCLUSIONS RECEVABLES EN APPEL. - CONCLUSIONS INDEMNITAIRES - LIMITATION PAR LES CONCLUSIONS PRÉSENTÉES EN PREMIÈRE INSTANCE - APPLICATION À DES PARENTS DEMANDANT UNE INDEMNITÉ EN PROPRE ET UNE INDEMNITÉ ET UNE RENTE ANNUELLE POUR LEUR ENFANT [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-04-03 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RÉPARATION. ÉVALUATION DU PRÉJUDICE. - ACTION INDEMNITAIRE - LIMITATION DES CONCLUSIONS D'APPEL PAR LES CONCLUSIONS PRÉSENTÉES EN PREMIÈRE INSTANCE - APPLICATION À DES PARENTS DEMANDANT UNE INDEMNITÉ EN PROPRE ET UNE RENTE ANNUELLE POUR LEUR ENFANT [RJ1].
</SCT>
<ANA ID="9A"> 54-08-01-02 Devant le tribunal administratif, des époux ont formé des conclusions indemnitaires tendant à obtenir, d'une part, à titre personnel, une indemnité pour chacun et, d'autre part, en leur qualité de représentants légaux de leur enfant, une indemnité et une rente annuelle. Pour appliquer la règle selon laquelle les prétentions d'une partie devant le juge d'appel ne peuvent excéder, en l'absence d'éléments nouveaux postérieurs à la décision des premiers juges, le montant total de l'indemnité chiffrée en première instance, il y a lieu de distinguer le montant demandé par chacun des parents à titre personnel et le montant demandé au nom de l'enfant. Pour déterminer ce dernier montant, il y a lieu d'additionner l'indemnité demandée en son nom et le capital représentatif de la rente sollicitée.</ANA>
<ANA ID="9B"> 60-04-03 Devant le tribunal administratif, des époux ont formé des conclusions indemnitaires tendant à obtenir, d'une part, à titre personnel, une indemnité pour chacun et, d'autre part, en leur qualité de représentants légaux de leur enfant, une indemnité et une rente annuelle. Pour appliquer la règle selon laquelle les prétentions d'une partie devant le juge d'appel ne peuvent excéder, en l'absence d'éléments nouveaux postérieurs à la décision des premiers juges, le montant total de l'indemnité chiffrée en première instance, il y a lieu de distinguer le montant demandé par chacun des parents à titre personnel et le montant demandé au nom de l'enfant. Pour déterminer ce dernier montant, il y a lieu d'additionner l'indemnité demandée en son nom et le capital représentatif de la rente sollicitée.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 31 mai 2007, Herbeth, n° 278905, p. 175.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
