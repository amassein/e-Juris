<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041549008</ID>
<ANCIEN_ID>JG_L_2020_02_000000425747</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/54/90/CETATEXT000041549008.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 05/02/2020, 425747</TITRE>
<DATE_DEC>2020-02-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>425747</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LE GRIEL</AVOCATS>
<RAPPORTEUR>M. Alain Seban</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2020:425747.20200205</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              Par un arrêt n°s 18PA01650, 18PA02432 du 22 novembre 2018, la cour administrative d'appel de Paris a transmis au Conseil d'Etat le jugement de deux requêtes présentées devant cette cour par l'association Comité de défense des auditeurs de Radio Solidarité (CDARS).<br/>
<br/>
              1° Sous le n° 425747, par une requête et deux mémoires, enregistrés les 15 mai, 19 juillet et 18 septembre 2018 au greffe de la cour administrative d'appel de Paris et par un nouveau mémoire, enregistré le 16 juillet 2019 au secrétariat du contentieux du Conseil d'Etat, l'association CDARS demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite née du silence gardé par le Conseil supérieur de l'audiovisuel (CSA) sur son recours du 18 janvier 2018 contre la décision du comité territorial de l'audiovisuel de Caen du 27 novembre 2017 refusant de déclarer reconductible, hors appel aux candidatures, l'autorisation d'émettre qui lui avait été accordée pour le service de radio dénommé " Radio Courtoisie " pour les zones de Caen, Chartres, Cherbourg, Le Mans et Le Havre ;<br/>
<br/>
              2°) de mettre à la charge du CSA la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              2° Sous le n° 425755, par une requête et un nouveau mémoire, enregistrés les 19 juillet et 18 septembre 2018 au greffe de la cour administrative d'appel de Paris, l'association CDARS demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 25 avril 2018 par laquelle le CSA a rejeté son recours contre la décision du comité territorial de l'audiovisuel de Caen du 27 novembre 2017 refusant de déclarer reconductible, hors appel aux candidatures, l'autorisation d'émettre qui lui avait été accordée pour le service de radio dénommé " Radio Courtoisie " pour les zones de Caen, Chartres, Cherbourg, Le Mans et Le Havre ; <br/>
<br/>
              2°) de mettre à la charge du CSA la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 86-1067 du 30 septembre 1986 ;<br/>
              - le décret n° 2011-732 du 24 juin 2011 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alain Seban, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Le Griel, avocat de l'association Comité de défense des auditeurs de Radio Solidarité.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Les deux requêtes de l'association Comité de défense des auditeurs de Radio Solidarité (CDARS) visées ci-dessus présentent à juger des questions semblables. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2. L'article 29 de la loi du 30 septembre 1986 relative à la liberté de communication prévoit que l'usage des fréquences pour la diffusion de services de radio par voie hertzienne terrestre en mode analogique est autorisé par le Conseil supérieur de l'audiovisuel (CSA). La reconduction de ces autorisations est prévue par l'article 28-1 de la même loi, aux termes duquel : " I - (...)  Les autorisations délivrées en application des articles 29, 29-1, 30 et 30-1 sont reconduites par le Conseil supérieur de l'audiovisuel, hors appel aux candidatures, dans la limite de deux fois en sus de l'autorisation initiale, et chaque fois pour cinq ans, sauf : / (...) 2° Si une sanction, une astreinte liquidée ou une condamnation dont le titulaire de l'autorisation a fait l'objet sur le fondement de la présente loi (...) est de nature à justifier que cette autorisation ne soit pas reconduite hors appel aux candidatures ; (...) ". Aux termes de l'article 29-3 de cette loi : " Des comités techniques, constitués par le Conseil supérieur de l'audiovisuel, assurent l'instruction des demandes d'autorisation visées aux articles 29 et 29-1 (...). Ils peuvent statuer, dans des conditions fixées par le Conseil supérieur de l'audiovisuel, sur la reconduction des autorisations délivrées en application des articles 29, 29-1, 30 et 30-1, pour les services à vocation locale, dans les conditions prévues à l'article 28-1 (...) Dans ce cas, le président du comité technique peut signer l'autorisation et la convention y afférente (...) ". Enfin, aux termes de l'article 20 du décret du 24 juin 2011 relatif à ces comités techniques : " Préalablement à l'exercice d'un recours contentieux devant la juridiction administrative compétente, les décisions des comités territoriaux de l'audiovisuel font l'objet d'un recours devant l'assemblée plénière du Conseil supérieur de l'audiovisuel dans le délai de deux mois à compter de leur notification ou de leur publication ".<br/>
<br/>
              3. Il ressort des pièces des dossiers que, sur le fondement des dispositions citées ci-dessus, l'association CDARS, qui édite le service radiophonique diffusé par voie hertzienne terrestre en mode analogique dénommé " Radio Courtoisie ", a demandé au comité territorial de l'audiovisuel de Caen de renouveler, hors appel aux candidatures, l'autorisation d'émettre dont elle était titulaire pour la zone de Caen, Chartres, Cherbourg, Le Mans et Le Havre, qui arrivait à expiration le 3 décembre 2018. Par une décision du 27 novembre 2017, le comité territorial a rejeté sa demande au motif que l'association avait fait l'objet d'une sanction pécuniaire de 25 000 euros, infligée par le CSA le 4 octobre 2017. L'association a formé le 18 janvier 2018 un recours administratif devant le CSA, qui l'a d'abord rejeté implicitement, puis, explicitement, par une décision du 25 avril 2018 qui reprend le même motif que celui qu'avait retenu le comité territorial de l'audiovisuel. Par une décision du 17 décembre 2018, le Conseil d'Etat, statuant au contentieux a décidé, sur le fondement de l'article L. 521-1 du code de justice administrative, d'ordonner la suspension de l'exécution de cette dernière décision. L'association CDARS demande l'annulation pour excès de pouvoir des deux décisions du CSA.<br/>
<br/>
              Sur la requête formée contre la décision implicite du Conseil supérieur de l'audiovisuel :<br/>
<br/>
              4. Les conclusions de l'association requérante dirigées contre cette décision, qui est née du silence gardé par le CSA sur le recours dont elle l'a saisi contre la décision du 27 novembre 2017 du comité territorial de l'audiovisuel de Caen, à laquelle elle s'est substituée, doivent être regardées comme dirigées contre la décision expresse du 25 avril 2018 qui s'est elle-même substituée à cette décision tacite.<br/>
<br/>
              5. Les deux requêtes de l'association CDARS doivent, par suite, être regardées comme dirigées contre la même décision du 25 avril 2018 du CSA.<br/>
<br/>
              Sur la légalité de la décision du 25 avril 2018 :<br/>
<br/>
              6. Il ressort des pièces du dossier que la sanction infligée par le CSA à l'association requérante le 4 octobre 2017, sur l'existence de laquelle celui-ci s'est fondé pour prendre la décision litigieuse, était relative à des propos à connotation raciste, xénophobe et incitant à la discrimination envers les personnes à raison de leur religion, tenus à plusieurs reprises par M. A... B..., alors président de l'association CDARS, ou par ses invités, dans l'émission " Le libre journal d'Henry B... ". Toutefois, il ressort également des pièces du dossier que les orientations prises, à partir de juillet 2017, par l'association, qui a notamment retiré à M. B... les responsabilités qu'il exerçait en son sein et l'a écarté de l'antenne, manifestent la volonté de la requérante de tirer les conséquences de la sanction qui lui a été infligée et d'éviter le renouvellement de faits similaires à ceux qui l'ont justifiée. Dans ces conditions, le CSA a fait une inexacte application des dispositions citées ci-dessus de l'article 28-1 de la loi du 30 septembre 1986 en se fondant sur l'existence de cette sanction pour refuser de reconduire, hors appel aux candidatures, l'autorisation d'émettre dont l'association était titulaire.<br/>
<br/>
              7. Par suite, sans qu'il soit besoin de se prononcer sur les autres moyens des requêtes, l'association requérante est fondée à demander l'annulation de la décision du 25 avril 2018.<br/>
<br/>
              Sur l'exécution de la présente décision :<br/>
<br/>
              8. L'exécution de la présente décision implique que le comité territorial de l'audiovisuel de Caen se prononce à nouveau sur la demande présentée par l'association, qui doit, dans l'attente de l'issue de la procédure, être autorisée à émettre sous couvert d'autorisations provisoires, dans le cadre de la convention qu'elle avait conclue au titre de la précédente autorisation. L'autorisation dont bénéficiait l'association CDARS étant, ainsi qu'il a été dit, arrivée à expiration le 3 décembre 2018, ces autorisations provisoires doivent prendre effet à compter de cette date.<br/>
<br/>
              9. Par ailleurs, aux termes du II de l'article 28-1 de la loi du 30 septembre 1986 : " Un an avant l'expiration de l'autorisation délivrée en application des articles 29 ou 30, le Conseil supérieur de l'audiovisuel publie sa décision motivée de recourir ou non à la procédure de reconduction hors appel aux candidatures. (...) / Dans l'hypothèse où le Conseil supérieur de l'audiovisuel décide de recourir à la reconduction hors appel aux candidatures, sa décision mentionne, pour les services de communication audiovisuelle autres que radiophoniques, les points principaux de la convention en vigueur qu'il souhaite voir réviser, ainsi que ceux dont le titulaire demande la modification. (...) / A défaut d'accord six mois au moins avant la date d'expiration de l'autorisation délivrée en application des articles 29 ou 30 (...) celle-ci n'est pas reconduite hors appel aux candidatures. Une nouvelle autorisation d'usage de fréquences ne peut être alors délivrée par le Conseil supérieur de l'audiovisuel que dans les conditions prévues aux articles 29, 29-1, 30 et 30-1. "<br/>
<br/>
              10. L'application des dispositions citées ci-dessus impose que si, au terme de l'instruction de la demande de l'association, le comité territorial de l'audiovisuel accepte de recourir à la reconduction hors appel aux candidatures et que soit aucune révision de la convention n'est demandée, soit un accord est trouvé sur sa révision dans un délai de six mois, l'autorisation soit reconduite pour une durée de cinq ans intégrant les périodes pendant lesquelles l'association aura effectivement émis sous couvert d'autorisations provisoires. Dans le cas où une révision de la convention est demandée mais qu'aucun accord n'est trouvé dans les six mois, les autorisations provisoires délivrées à l'association doivent cesser de produire effet au terme de six mois supplémentaires.<br/>
<br/>
              11. Si le comité territorial rejette la demande de reconduction hors appel aux candidatures, les autorisations provisoires d'émettre cesseront de produire effet au terme d'un délai d'un an à compter de la notification de cette décision de refus. <br/>
<br/>
              12. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du CSA une somme de 3 000 euros à verser à l'association CDARS au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
      Article 1er : La décision du CSA du 25 avril 2018 est annulée.<br/>
Article 2 : Le CSA versera à l'association CDARS la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à l'association Comité de défense des auditeurs de Radio Solidarité, au comité territorial de l'audiovisuel de Caen et au Conseil supérieur de l'audiovisuel.<br/>
      Copie en sera adressée au ministre de la culture.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">56-04-01 RADIO ET TÉLÉVISION. SERVICES PRIVÉS DE RADIO ET DE TÉLÉVISION. SERVICES DE RADIO. - DÉCISION DE NE PAS RECONDUIRE UNE AUTORISATION D'USAGE DES FRÉQUENCES HORS APPEL À CANDIDATURE (ART. 28-1 DE LA LOI DU 30 SEPTEMBRE 1986) - CONDITIONS - EXISTENCE D'UNE SANCTION DE NATURE À JUSTIFIER UN REFUS (2° DE L'ART. 28-1) - APPRÉCIATION DU CSA - OBLIGATION DE TENIR COMPTE DE LA VOLONTÉ DU TITULAIRE DE TIRER LES CONSÉQUENCES DE LA SANCTION ET D'ÉVITER LE RENOUVELLEMENT DE FAITS SIMILAIRES.
</SCT>
<ANA ID="9A"> 56-04-01 Il ressort des pièces du dossier que la sanction infligée par le Conseil supérieur de l'audiovisuel (CSA) à l'association requérante le 4 octobre 2017, sur l'existence de laquelle celui-ci s'est fondé pour prendre la décision de ne pas reconduire, hors appel aux candidatures, l'autorisation d'émettre dont l'association était titulaire, était relative à des propos à connotation raciste, xénophobe et incitant à la discrimination envers les personnes à raison de leur religion, tenus à plusieurs reprises par M. X, président de l'association, ou par ses invités dans son émission.,,,Toutefois, il ressort également des pièces du dossier que les orientations prises, à partir de juillet 2017, par l'association, qui a notamment retiré à M. X. les responsabilités qu'il exerçait en son sein et l'a écarté de l'antenne, manifestent la volonté de l'association de tirer les conséquences de la sanction qui lui a été infligée et d'éviter le renouvellement de faits similaires à ceux qui l'ont justifiée.... ,,Dans ces conditions, le CSA a fait une inexacte application des dispositions de l'article 28-1 de la loi n° 86-1067 du 30 septembre 1986 en se fondant sur l'existence de cette sanction pour refuser de reconduire, hors appel aux candidatures, l'autorisation d'émettre dont l'association était titulaire.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
