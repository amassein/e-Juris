<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042143059</ID>
<ANCIEN_ID>JG_L_2020_07_000000423313</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/14/30/CETATEXT000042143059.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 22/07/2020, 423313</TITRE>
<DATE_DEC>2020-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>423313</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>Mme Bénédicte Fauvarque-Cosson</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie Sirinelli</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:423313.20200722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au tribunal administratif de Nantes d'annuler pour excès de pouvoir la décision du 9 septembre 2014 par laquelle le directeur général de l'agence régionale de santé des Pays-de-la-Loire l'a mis en demeure de cesser la pratique des actes de chirurgie de la cataracte dans le cadre de son activité libérale en cabinet. Par un jugement n° 1408234 du 15 novembre 2016, le tribunal administratif de Nantes a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 17NT00078 du 21 juin 2018, la cour administrative d'appel de Nantes a rejeté l'appel formé par M. A... contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 16 août et 16 novembre 2018 et le 22 octobre 2019 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code de la santé publique, notamment son article L. 6122-1 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme C... D..., conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Marie Sirinelli, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de M. A... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 9 septembre 2014, le directeur général de l'agence régionale de santé des Pays-de-la-Loire a mis M. A... en demeure de cesser la pratique des actes de chirurgie de la cataracte en cabinet libéral, en l'informant que la persistance d'une telle pratique l'amènerait à saisir le procureur de la République à fin de poursuites pénales. Par un jugement du 15 novembre 2016, le tribunal administratif de Nantes a rejeté la demande de M. A... tendant à l'annulation de cette décision pour excès de pouvoir. M. A... a relevé appel de ce jugement et soulevé une question prioritaire de constitutionnalité dirigée contre les dispositions de l'article L. 6122-1 du code de la santé publique. Par une ordonnance du 21 juin 2017, la cour d'appel administrative de Nantes a jugé qu'il n'y avait pas lieu de transmettre au Conseil d'État cette question prioritaire de constitutionnalité et, par un arrêt du 21 juin 2018, elle a rejeté l'appel de M. A.... Celui-ci se pourvoit en cassation contre cet arrêt et conteste le refus de transmission de sa question prioritaire de constitutionnalité.<br/>
<br/>
              Sur l'intervention de l'association de chirurgie en soins externes : <br/>
<br/>
              2. L'association de chirurgie en soins externes justifie d'un intérêt suffisant à l'annulation de l'arrêt attaqué. Ainsi, son intervention est recevable.<br/>
<br/>
              Sur la contestation du refus de transmission de la question prioritaire de constitutionnalité :<br/>
<br/>
              3. Les dispositions de l'article 23-2 de l'ordonnance portant loi organique du 7 novembre 1958 prévoient que, lorsqu'une juridiction relevant du Conseil d'Etat est saisie de moyens contestant la conformité d'une disposition législative aux droits et libertés garantis par la Constitution, elle transmet au Conseil d'Etat la question de constitutionnalité ainsi posée à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et qu'elle ne soit pas dépourvue de caractère sérieux.<br/>
<br/>
              4. Il est loisible au législateur d'apporter à la liberté d'entreprendre, qui découle de l'article 4 de la Déclaration des droits de l'homme et du citoyen du 26 août 1789, des limitations liées à des exigences constitutionnelles ou justifiées par l'intérêt général, à la condition qu'il n'en résulte pas d'atteinte disproportionnée au regard de l'objectif poursuivi. <br/>
<br/>
              5. Aux termes de l'article L. 6122-1 du code de la santé publique : " Sont soumis à l'autorisation de l'agence régionale de santé les projets relatifs à la création de tout établissement de santé, la création, la conversion et le regroupement des activités de soins, y compris sous la forme d'alternatives à l'hospitalisation ou d'hospitalisation à domicile, et l'installation des équipements matériels lourds. / La liste des activités de soins et des équipements matériels lourds soumis à autorisation est fixée par décret en Conseil d'Etat ". Il résulte en outre des dispositions de l'article L. 6122-2 du même code que cette autorisation vise à vérifier que le projet répond aux besoins de santé de la population, est compatible avec les objectifs fixés à cette fin au niveau régional et satisfait à certaines conditions d'implantation et conditions techniques. <br/>
<br/>
              6. Par les dispositions de l'article L. 6122-1 du code de la santé publique, le législateur a entendu soumettre à autorisation non seulement la création des établissements de santé et l'installation de certains équipements matériels lourds, définis par l'article L. 6122-14 du même code, mais aussi la création, la conversion et le regroupement des activités de soins ayant vocation, compte tenu des moyens qu'elles nécessitent, à faire l'objet d'une prise en charge hospitalière, y compris lorsqu'elles sont exercées sous la forme d'alternatives à l'hospitalisation ou d'hospitalisation à domicile, pour favoriser une meilleure réponse aux besoins de santé de la population et veiller à la qualité et à la sécurité des soins offerts. Il a ainsi mis en oeuvre les exigences de valeur constitutionnelle, résultant des dispositions du onzième alinéa du Préambule de la Constitution de 1946, qui s'attachent à la protection de la santé et a, ce faisant, porté à la liberté d'entreprendre une atteinte qui ne revêt pas un caractère disproportionné. Si les dispositions contestées renvoient à un décret en Conseil d'Etat la fixation de la liste exacte des activités de soins soumises à autorisation, elles ne privent pas de garanties légales les exigences qui résultent de la liberté d'entreprendre. Par suite, en regardant comme dépourvue de caractère sérieux la question tiré de la méconnaissance de la liberté d'entreprendre et de la méconnaissance, par le législateur, de sa compétence dans des conditions affectant cette liberté, le président de la troisième chambre de la cour administrative d'appel de Nantes a exactement qualifié la question qui lui était soumise de la conformité des dispositions de l'article L. 6122-1 du code de la santé publique aux droits et libertés garantis par la Constitution.<br/>
<br/>
              7. Il s'ensuit que M. A... n'est pas fondé à demander l'annulation de l'ordonnance par laquelle le président de la 3ème chambre de la cour administrative d'appel de Nantes a refusé de transmettre au Conseil d'Etat la question prioritaire de constitutionnalité soulevée.  <br/>
<br/>
              Sur l'arrêt de la cour administrative d'appel de Nantes du 21 juin 2018, rejetant les autres moyens de l'appel de M. A... :<br/>
<br/>
              En ce qui concerne la régularité de l'arrêt attaqué :<br/>
<br/>
              8. Il ressort des écritures de M. A... devant la cour administrative d'appel de Nantes que celui-ci s'est borné à suggérer que la formation de jugement ait recours à une expertise ou à un avis technique, sur le fondement de l'article R. 625-2 du code de justice administrative, pour forger sa propre conviction. Par suite, le moyen tiré de ce que l'arrêt serait irrégulier faute d'avoir visé la demande d'expertise qu'il aurait formulée et d'y avoir répondu ne peut qu'être écarté. <br/>
<br/>
              En ce qui concerne le bien-fondé de l'arrêt attaqué :<br/>
<br/>
              9. Il résulte de l'article L. 6122-1 du code de la santé publique, ainsi qu'il a été dit au point 6, que sont soumis à l'autorisation de l'agence régionale de santé les projets relatifs à la création, la conversion et le regroupement des activités de soins ayant vocation, compte tenu des moyens que celles-ci nécessitent, à faire l'objet d'une prise en charge hospitalière, y compris sous la forme d'alternatives à l'hospitalisation ou d'hospitalisation à domicile. Aux termes de l'article L. 6122-3 du même code : " L'autorisation ne peut être accordée qu'à : / 1° Un ou plusieurs médecins, éventuellement associés pour leur exercice professionnel ou pour la mise en commun de moyens nécessaires à cet exercice ; / 2° Un établissement de santé ; / 3° Une personne morale dont l'objet porte, notamment, sur l'exploitation d'un établissement de santé, d'une activité de soins (...) ". Aux termes de l'article R. 6122-25 de ce même code : " Sont soumises à l'autorisation prévue à l'article L. 6122-1 les activités de soins, y compris lorsqu'elles sont exercées sous la forme d'alternatives à l'hospitalisation, énumérées ci-après : / 1° Médecine ; / 2° Chirurgie ; (...) ". Aux termes de l'article R. 6121-4 de ce même code : " Les alternatives à l'hospitalisation mentionnées à l'article L. 6121-2 ont pour objet d'éviter une hospitalisation à temps complet ou d'en diminuer la durée. Les prestations ainsi dispensées se distinguent de celles qui sont délivrées lors de consultations ou de visites à domicile. / Ces alternatives comprennent les activités de soins dispensées par : (...) 2° Les structures pratiquant l'anesthésie ou la chirurgie ambulatoires. / (...) Dans les structures pratiquant l'anesthésie ou la chirurgie ambulatoires sont mis en oeuvre, dans des conditions qui autorisent le patient à rejoindre sa résidence le jour même, des actes médicaux ou chirurgicaux nécessitant une anesthésie ou le recours à un secteur opératoire ". Enfin, aux termes de l'article D. 6124-301-1 de ce code : " Les structures d'hospitalisation à temps partiel de jour ou de nuit et les structures pratiquant l'anesthésie ou la chirurgie ambulatoires dispensent les prises en charge prévues à l'article R. 6121-4, d'une durée inférieure ou égale à douze heures, ne comprenant pas d'hébergement, au bénéfice de patients dont l'état de santé correspond à ces modes de prise en charge. / Les prestations délivrées équivalent par leur nature, leur complexité et la surveillance médicale qu'elles requièrent à des prestations habituellement effectuées dans le cadre d'une hospitalisation à temps complet. (...) ".<br/>
<br/>
              10. En premier lieu, il résulte de ces dispositions que peuvent être autorisés à créer une activité de soins de chirurgie non seulement les établissements de santé, mais également les personnes morales dont l'objet porte, notamment, sur l'exploitation d'un établissement de santé ou d'une activité de soins, de même qu'un ou plusieurs médecins, éventuellement associés, dans le cadre d'une structure alternative à l'hospitalisation pratiquant la chirurgie ambulatoire. Par suite, la cour administrative d'appel de Nantes n'a pas commis d'erreur de droit en jugeant que les dispositions des articles L. 6122-1 et R. 6122-25 du code de la santé publique n'avaient ni pour objet ni pour effet d'attribuer aux établissements de santé le monopole de la chirurgie de la cataracte, mais soumettaient à autorisation la pratique de soins tels que la chirurgie de la cataracte dans le cadre de structures de chirurgie ambulatoire, devant répondre aux conditions définies à l'article R. 6121-4 du même code.<br/>
<br/>
              11. En deuxième lieu, il résulte des dispositions mentionnées au point 9 que sont soumis à autorisation les actes chirurgicaux qui, se distinguant des prestations délivrées lors de consultations ou de visites à domicile, nécessitent une anesthésie au sens de l'article D. 6124-91 du code de la santé publique ou le recours à un secteur opératoire, lequel doit être conforme à des caractéristiques fixées par arrêté du ministre chargé de la santé en vertu de l'article D. 6124-302 du même code, prévoyant notamment une zone opératoire protégée propre à garantir la réduction maximale des risques de nature infectieuse. Ces actes peuvent être pratiqués dans le cadre d'une activité alternative à l'hospitalisation, au sein de structures qui ne sont pas nécessairement des établissements de santé, à la condition toutefois que cette activité ait été autorisée par l'agence régionale de santé et satisfasse aux conditions précisées notamment par les articles D. 6124-301-1 et suivants du code de la santé publique. En l'espèce, il ressort des pièces du dossier soumis aux juges du fond, notamment du rapport d'évaluation de la Haute Autorité de santé de juillet 2010 consacré aux conditions de réalisation de cette chirurgie, que la chirurgie de la cataracte, qui implique d'inciser l'oeil pour en extraire le cristallin, ne peut être regardée comme une prestation délivrée lors d'une consultation mais nécessite le recours à un secteur opératoire, quand bien même elle serait pratiquée sous anesthésie topique et non sous anesthésie générale ou loco-régionale. Par suite, la cour, qui ne s'est pas méprise sur la portée des écritures de M. A..., a exactement qualifié les faits de l'espèce en jugeant que les opérations de la cataracte pratiquées par le requérant au titre de son activité libérale en cabinet relevaient des activités de chirurgie soumises à autorisation en application des articles L. 6122-1 et R. 6122-25 du code de la santé publique.<br/>
<br/>
              12. En dernier lieu, il ressort des pièces du dossier soumis aux juges du fond que M. A... n'a pas sollicité de l'agence régionale de santé d'autorisation de créer une activité de soins de chirurgie mais a pratiqué la chirurgie de la cataracte dans ses cabinets principal et secondaire en estimant que son activité n'était pas soumise à autorisation. Par suite, en jugeant que, par sa décision du 9 septembre 2014, le directeur général de cette agence n'avait pas opposé à M. A... un refus d'autorisation mais l'avait mis en demeure de se conformer aux dispositions applicables du code de la santé publique, la cour a exactement interprété la portée de la décision attaquée.<br/>
<br/>
              13. Il résulte de tout ce qui précède que M. A... n'est pas fondé à demander l'annulation de l'arrêt de la cour administrative de Nantes qu'il attaque.<br/>
<br/>
              Sur les frais liés au litige :<br/>
<br/>
              14. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance, la somme que M. A... demande au titre des frais exposés par lui et non compris dans les dépens.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'intervention de l'association de chirurgie en soins externes est admise.<br/>
Article 2 : Le pourvoi de M. A... est rejeté.<br/>
Article 3 : La présente décision sera notifiée à M. B... A..., au ministre des solidarités et de la santé et à l'association de chirurgie en soins externes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">55-03-01-02 PROFESSIONS, CHARGES ET OFFICES. CONDITIONS D'EXERCICE DES PROFESSIONS. MÉDECINS. RÈGLES DIVERSES S'IMPOSANT AUX MÉDECINS DANS L'EXERCICE DE LEUR PROFESSION. - CRÉATION D'ACTIVITÉS DE SOINS SOUMISE À AUTORISATION DE L'ARS (ART. L. 6122-1 DU CSP) - 1) CHAMP D'APPLICATION - ACTIVITÉS AYANT VOCATION À FAIRE L'OBJET D'UNE PRISE EN CHARGE HOSPITALIÈRE - 2) INCLUSION - ACTES CHIRURGICAUX NÉCESSITANT UNE ANESTHÉSIE AU SENS DE L'ARTICLE D. 6124-91 DU CSP OU LE RECOURS À UN SECTEUR OPÉRATOIRE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">61-09-02-01 SANTÉ PUBLIQUE. ADMINISTRATION DE LA SANTÉ. - AUTORISATION DE CRÉATION D'ACTIVITÉS DE SOINS (ART. L. 6122-1 DU CSP) - 1) CHAMP D'APPLICATION - ACTIVITÉS AYANT VOCATION À FAIRE L'OBJET D'UNE PRISE EN CHARGE HOSPITALIÈRE - 2) INCLUSION - ACTES CHIRURGICAUX NÉCESSITANT UNE ANESTHÉSIE AU SENS DE L'ARTICLE D. 6124-91 DU CSP OU LE RECOURS À UN SECTEUR OPÉRATOIRE.
</SCT>
<ANA ID="9A"> 55-03-01-02 1) Par l'article L. 6122-1 du code de la santé publique (CSP), le législateur a entendu soumettre à autorisation non seulement la création des établissements de santé et l'installation de certains équipements matériels lourds, définis par l'article L. 6122-14 du même code, mais aussi la création, la conversion et le regroupement des activités de soins ayant vocation, compte tenu des moyens qu'elles nécessitent, à faire l'objet d'une prise en charge hospitalière, y compris lorsqu'elles sont exercées sous la forme d'alternatives à l'hospitalisation ou d'hospitalisation à domicile, pour favoriser une meilleure réponse aux besoins de santé de la population et veiller à la qualité et à la sécurité des soins offerts.,,,2) Il résulte des articles L. 6122-1, L. 6122-3, R. 6122-4, R. 6122-25 et D. 6124-301-1 du CSP que sont ainsi soumis à autorisation les actes chirurgicaux qui, se distinguant des prestations délivrées lors de consultations ou de visites à domicile, nécessitent une anesthésie au sens de l'article D. 6124-91 du CSP ou le recours à un secteur opératoire, lequel doit être conforme à des caractéristiques fixées par arrêté du ministre chargé de la santé en vertu de l'article D. 6124-302 du même code, prévoyant notamment une zone opératoire protégée propre à garantir la réduction maximale des risques de nature infectieuse. Ces actes peuvent être pratiqués dans le cadre d'une activité alternative à l'hospitalisation, au sein de structures qui ne sont pas nécessairement des établissements de santé, à la condition toutefois que cette activité ait été autorisée par l'agence régionale de santé (ARS) et satisfasse aux conditions précisées notamment par les articles D. 6124-301-1 et suivants du CSP.</ANA>
<ANA ID="9B"> 61-09-02-01 1) Par l'article L. 6122-1 du code de la santé publique (CSP), le législateur a entendu soumettre à autorisation non seulement la création des établissements de santé et l'installation de certains équipements matériels lourds, définis par l'article L. 6122-14 du même code, mais aussi la création, la conversion et le regroupement des activités de soins ayant vocation, compte tenu des moyens qu'elles nécessitent, à faire l'objet d'une prise en charge hospitalière, y compris lorsqu'elles sont exercées sous la forme d'alternatives à l'hospitalisation ou d'hospitalisation à domicile, pour favoriser une meilleure réponse aux besoins de santé de la population et veiller à la qualité et à la sécurité des soins offerts.,,,2) Il résulte des articles L. 6122-1, L. 6122-3, R. 6122-4, R. 6122-25 et D. 6124-301-1 du CSP que sont ainsi soumis à autorisation les actes chirurgicaux qui, se distinguant des prestations délivrées lors de consultations ou de visites à domicile, nécessitent une anesthésie au sens de l'article D. 6124-91 du CSP ou le recours à un secteur opératoire, lequel doit être conforme à des caractéristiques fixées par arrêté du ministre chargé de la santé en vertu de l'article D. 6124-302 du même code, prévoyant notamment une zone opératoire protégée propre à garantir la réduction maximale des risques de nature infectieuse. Ces actes peuvent être pratiqués dans le cadre d'une activité alternative à l'hospitalisation, au sein de structures qui ne sont pas nécessairement des établissements de santé, à la condition toutefois que cette activité ait été autorisée par l'agence régionale de santé (ARS) et satisfasse aux conditions précisées notamment par les articles D. 6124-301-1 et suivants du CSP.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
