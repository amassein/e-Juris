<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026321944</ID>
<ANCIEN_ID>JG_L_2012_08_000000361402</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/32/19/CETATEXT000026321944.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 27/08/2012, 361402</TITRE>
<DATE_DEC>2012-08-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>361402</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Denis Piveteau</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2012:361402.20120827</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 27 juillet 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par le Groupe d'information et de soutien des immigrés (GISTI), dont le siège est 3, villa Marcès à Paris (75011), représenté par son président en exercice, l'association Avocats pour la défense des droits des étrangers (ADDE), dont le siège est au bureau des associations de l'Ordre des avocats à la cour d'appel, 2-4, rue de Harley à Paris (75001), représentée par sa présidente en exercice, le Comité médical pour les exilés (COMEDE), dont le siège est 78, rue du Général Leclerc au Kremlin-Bicêtre (94272), représenté par sa présidente en exercice, la Fédération des associations de solidarité avec les travailleur-euse-s immigré-e-s (FASTI), dont le siège est 58, rue des Amandiers à Paris (75020), représentée par sa présidente, la Ligue des droits de l'homme (LDH), dont le siège est 138, rue de Marcadet à Paris (75018), représentée par son président en exercice, le Mouvement contre le racisme et pour l'amitié entre les peuples (MRAP), dont le siège est 43, boulevard de Magenta à Paris (75010), représenté par sa co-présidente et représentante légale, et le Syndicat des avocats de France (SAF), dont le siège est 34, rue Saint Lazare à Paris (75009), représenté par sa présidente en exercice ; les requérants demandent au juge des référés du Conseil d'Etat : <br/>
<br/>
              1°) d'ordonner, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution des dispositions du II et du III de la circulaire NOR INT/K/12/07283/C du 6 juillet 2012 relative à la mise en oeuvre de l'assignation à résidence prévue à l'article L. 561-2 du code de l'entrée et du séjour des étrangers et du droit d'asile, en alternative au placement en rétention administrative sur le fondement de l'article L. 555-1 du même code ; <br/>
<br/>
              2°) d'enjoindre au ministre de l'intérieur d'adopter de nouvelles instructions prohibant la rétention des familles avec enfants ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 1 000 euros par association requérante au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              ils soutiennent que :<br/>
              - il existe un doute sérieux quant à la légalité des dispositions du II de la circulaire, qui créent des régimes différenciés d'assignation, en violation de la loi et des articles 3-7 et 7 de la directive 2008/115/CE du 16 décembre 2008 ; <br/>
              - il existe également un doute sérieux quant à la légalité des dispositions du III de la circulaire en raison de ce que, par l'automaticité du placement en rétention des familles ayant cherché à se soustraire à une mesure d'éloignement, ces dispositions créent un régime de sanction en dehors de tout cadre légal et méconnaissent le pouvoir d'appréciation des préfets ; <br/>
              - ces dispositions violent les articles 5.1 et 5.4 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, ainsi que l'a jugé la Cour européenne des droits de l'homme dans son arrêt " Popov c/ France " du 19 janvier 2012 ; <br/>
<br/>
<br/>
              Vu la circulaire du ministre de l'intérieur dont la suspension de l'exécution est demandée ;<br/>
<br/>
              Vu la copie de la requête à fin d'annulation de cette circulaire ;<br/>
<br/>
              Vu le mémoire en défense, enregistré le 14 août 2012, présenté par le ministre de l'intérieur qui conclut au rejet de la requête ; il soutient que :<br/>
              - la requête est irrecevable car les associations requérantes n'ont pas d'intérêt à agir, que la circulaire n'a aucun caractère impératif et que les conclusions des requérantes aux fins d'injonction excèdent l'office du juge des référés ; <br/>
              - la condition d'urgence n'est pas remplie ; <br/>
              - les dispositions du II de la circulaire se bornent à rappeler la souplesse dont disposent les autorités administratives en vertu des dispositions de l'article R. 561-2 du code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
              - les dispositions du III de la circulaire respectent le principe d'examen particulier des situations et n'ont pas pour effet de créer un régime extra légal de sanction ; <br/>
              - les mineurs qui suivent leurs parents en rétention disposent de voies de recours conformes à l'article 5 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile, modifié notamment par la loi n°2011-672 du 16 juin 2011 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, le Groupe d'information et de soutien des immigrés (GISTI) et, d'autre part, le ministre de  l'intérieur ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 22 août 2012 à 10 heures, au cours de laquelle ont été entendus : <br/>
<br/>
               - les représentants du GISTI ;<br/>
<br/>
              - la représentante de l'association Avocats pour la défense des droits des étrangers et de la Ligue des droits de l'homme ;<br/>
<br/>
              - les représentantes du Syndicat des avocats de France ;<br/>
<br/>
              - les représentants du ministre de l'intérieur ;<br/>
<br/>
               et à l'issue de laquelle l'instruction a été close ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que le GISTI et les autres requérants demandent, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de la circulaire du 6 juillet 2012 du ministre de l'intérieur en tant, d'une part, qu'elle précise, en son II, les conditions dans lesquelles les ressortissants étrangers parents d'enfants mineurs sont susceptibles d'être assignés à résidence sur le fondement de l'article L. 561-2 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA) et en tant, d'autre part, qu'elle indique, en son III, les conditions et modalités de rétention administrative de ces mêmes ressortissants ; <br/>
<br/>
              Sur les dispositions litigieuses du II de la circulaire du 6 juillet 2012 :<br/>
<br/>
              2. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; que la condition de l'urgence ne peut être regardée comme remplie que lorsque la décision administrative contestée préjudicie de manière suffisamment grave et immédiate à un intérêt public, à la situation du requérant ou aux intérêts qu'il défend ; <br/>
<br/>
              3. Considérant que les mesures d'assignation à résidence prises à l'encontre des ressortissants étrangers en instance d'éloignement ne sauraient être regardées comme créant par elles-mêmes une situation d'urgence au sens des dispositions citées ci-dessus de l'article L. 521-1 du code de justice administrative ; que, s'il est vrai que les mesures d'assignation à résidence qui font l'objet des dispositions de la circulaire contestée sont spécifiques en ce qu'elles concernent, par hypothèse, des ressortissants étrangers accompagnés d'enfant mineurs, cette circonstance n'est pas cependant, à elle seule, de nature à caractériser une situation d'urgence ;<br/>
<br/>
              4. Considérant, par suite, que le GISTI et les autres requérants ne sont pas fondés à soutenir que la suspension des dispositions contestées du II de la circulaire du 6 juillet 2012 présenterait un caractère d'urgence au sens des dispositions de l'article L. 521-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Sur les dispositions litigieuses du III de la circulaire du 6 juillet 2012 : <br/>
<br/>
              5. Considérant que l'article L. 561-2 du CESEDA, dont la circulaire contestée a pour objet de préciser l'interprétation, prévoit, par dérogation aux cas dans lesquels un ressortissant étranger est susceptible d'être placé en rétention, la faculté de prendre une mesure d'assignation à résidence lorsque l'étranger présente des garanties propres à prévenir le risque qu'il se soustraie à son obligation de quitter le territoire français ; qu'en vertu des dispositions du 3° du II de l'article L. 511-1 du même code, ce risque doit être notamment regardé comme établi, sauf circonstance particulière, dans les cas où l'étranger s'est soustrait à l'exécution d'une mesure d'éloignement ou aux obligations afférentes à son assignation à résidence ; qu'il résulte de ces dispositions que le constat par l'autorité administrative de faits relevant du 3° du II de l'article L. 511-1 du CESEDA, s'il est de nature à faire présumer l'existence d'un risque que le ressortissant étranger se soustraie à son obligation de quitter le territoire, ne dispense pas cette même autorité, avant toute décision de placement en rétention, de l'examen particulier des circonstances propres à l'espèce ;<br/>
<br/>
              6. Considérant que le III de la circulaire contestée prévoit que, en cas de non respect, par une famille de ressortissants étrangers, des conditions de son assignation à résidence, en cas de fuite d'un ou plusieurs membres de la famille, ou en cas de refus d'embarquement, la famille concernée " ne pourra donc plus " bénéficier du dispositif d'assignation à résidence, et que le préfet compétent pourra procéder à sa mise en rétention administrative " dans les conditions de droit commun " ; qu'en raison de la référence ainsi faite aux conditions de droit commun de la rétention administrative, ces dispositions doivent s'entendre, ainsi qu'il l'a d'ailleurs été clairement confirmé par le gouvernement lors de l'audience publique, comme réservant pleinement l'obligation qui incombe au préfet, avant toute décision de placement en rétention, et même après avoir constaté l'existence de faits relevant du 3° du II de l'article L. 511-1 du CESEDA, de procéder chaque fois à un examen particulier, dans les circonstances de l'espèce, de l'existence du risque que l'étranger se soustraie à son obligation de quitter le territoire ;<br/>
<br/>
              7. Considérant, par suite, que les moyens tirés de ce que la circulaire aurait, sur ce point, institué une automaticité du placement en rétention qui violerait le pouvoir d'appréciation des préfets et instaurerait un " régime nouveau de sanction " ne sont pas, en l'état de l'instruction, de nature à faire naître un doute sérieux sur la légalité de ses dispositions ; <br/>
<br/>
              8. Considérant que le GISTI et les autres requérants soutiennent également qu'en permettant le maintien d'un régime de rétention administrative pour des ressortissants étrangers accompagnés d'enfants mineurs, ces mêmes dispositions de la circulaire ne sont pas compatibles avec les articles 5.1 et 5.4 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; qu'à l'appui de leur moyen, les requérants soutiennent que l'absence de fondement légal de l'accueil en centre de rétention administrative d'enfants mineurs et l'absence de voies de recours juridictionnelles effectives pour les enfants mineurs ont été sanctionnées par la Cour européenne des droits de l'homme, sur le fondement de ces articles, dans un arrêt rendu contre la France ;<br/>
<br/>
              9. Considérant toutefois qu'il résulte notamment des dispositions de l'article L. 553-1 du CESEDA, dans sa rédaction issue de la loi du 16 juin 2011 relative à l'immigration, à l'intégration et à la nationalité, que la loi française prévoit expressément la possibilité qu'un enfant mineur d'un ressortissant étranger soit accueilli dans un centre de rétention, par voie de conséquence du placement en rétention de l'un de ses parents ; que, la circulaire attaquée se bornant sur ce point à renvoyer aux dispositions législatives qui définissent les conditions générales du placement en rétention, le moyen analysé ci-dessus soulève la question de la compatibilité des dispositions législatives du CESEDA relatives à la rétention administrative avec les articles 5.1 et 5.4 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
<br/>
              10. Considérant que, eu égard à l'office du juge des référés, et sauf lorsqu'est soulevée l'incompatibilité manifeste de dispositions législatives avec les règles du droit de l'Union européenne, un moyen pris de la contrariété de la loi à des engagements internationaux n'est pas, en l'absence d'une décision juridictionnelle ayant statué en ce sens, rendue soit par le juge administratif saisi au principal, soit par le juge compétent à titre préjudiciel, propre à créer un doute sérieux quant à la légalité de l'acte dont la suspension est demandée ; que, les décisions de la Cour européenne des droits de l'homme n'étant pas rendues à titre préjudiciel, le moyen soulevé par les requérants n'est par suite, et en tout état de cause, pas de nature à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la circulaire contestée ;<br/>
<br/>
              11. Considérant qu'il résulte de tout ce qui précède que, sans qu'il soit besoin de statuer sur les fins de non-recevoir soulevées par le ministre de l'intérieur, la requête du GISTI et des autres requérants doit être rejetée, y compris en ses conclusions à fins d'injonction et en ses conclusions présentées sur le fondement de l'article L.761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : la requête du GISTI et autres est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée au Groupe d'information et de soutien des immigrés, à l'association Avocats pour la défense des droits des étrangers, au Comité médical pour les exilés, à la Fédération des associations de solidarité avec les travailleur-euse-s immigré-e-s, à la Ligue des droits de l'homme, au Mouvement contre le racisme et pour l'amitié entre les peuples, au Syndicat des avocats de France et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-035-02-03-01 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ SUSPENSION (ART. L. 521-1 DU CODE DE JUSTICE ADMINISTRATIVE). CONDITIONS D'OCTROI DE LA SUSPENSION DEMANDÉE. MOYEN PROPRE À CRÉER UN DOUTE SÉRIEUX SUR LA LÉGALITÉ DE LA DÉCISION. - MOYEN TIRÉ DE LA CONTRARIÉTÉ DE LA LOI À DES ENGAGEMENTS INTERNATIONAUX (HORS DROIT DE L'UE) [RJ1] - PRINCIPE - MOYEN N'ÉTANT PAS PROPRE À CRÉER UN DOUTE SÉRIEUX - EXCEPTION - DÉCISION JURIDICTIONNELLE AYANT STATUÉ EN CE SENS.
</SCT>
<ANA ID="9A"> 54-035-02-03-01 Eu égard à l'office du juge des référés, et sauf lorsqu'est soulevée l'incompatibilité manifeste de dispositions législatives avec les règles du droit de l'Union européenne (UE), un moyen pris de la contrariété de la loi à des engagements internationaux n'est pas, en l'absence d'une décision juridictionnelle ayant statué en ce sens, rendue soit par le juge administratif saisi au principal, soit par le juge compétent à titre préjudiciel, propre à créer un doute sérieux quant à la légalité de l'acte dont la suspension est demandée.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 30 décembre 2002, Ministre de l'aménagement du territoire et de l'environnement c/ Carminati, n° 204430, p. 510. Comp., pour le droit de l'UE, CE, juge des référés, 16 juin 2010, Mme Diakité, n° 340250, p. 205.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
