<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042420923</ID>
<ANCIEN_ID>JG_L_2020_10_000000429185</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/42/09/CETATEXT000042420923.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 12/10/2020, 429185</TITRE>
<DATE_DEC>2020-10-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>429185</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Géraud Sajust de Bergues</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:429185.20201012</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La société Château Chéri a demandé au tribunal administratif de Bordeaux d'annuler, d'une part, la décision du 2 juillet 2012 par laquelle le préfet de la Gironde l'a déchue de ses droits attribués par un contrat d'agriculture durable, ensemble le rejet de son recours gracieux et, d'autre part, la décision du 7 juillet 2014 par laquelle le préfet a confirmé la décision prise le 2 juillet 2012. Par un jugement n° 1403779 du 24 mai 2016, le tribunal administratif de Bordeaux a rejeté ses demandes.<br/>
<br/>
              Par un arrêt n° 16BX02465 du 8 février 2019, la cour administrative d'appel de Bordeaux a annulé ce jugement ainsi que les décisions du préfet de la région Aquitaine du 2 juillet 2012 et du 7 juillet 2014.<br/>
<br/>
              Par un pourvoi, enregistré le 27 mars 2019 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'agriculture et de l'alimentation demande au Conseil d'Etat :<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 2000-321 du 12 avril 2000 ;<br/>
              - le décret n° 2001-492 du 6 juin 2001 ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Géraud Sajust de Bergues, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'à la suite d'un contrôle sur place réalisé le 17 novembre 2009, concluant que la société n'avait pas respecté les conditions posées à l'attribution de l'aide qui lui avait été consentie dans le cadre d'un contrat d'agriculture durable qu'elle avait souscrit le 3 août 2005, le préfet de la région Aquitaine, après avoir invité la société Château Chéri à présenter ses observations, a, par une décision du 2 juillet 2012, décidé que la société devait rembourser les aides perçues. La société a formé un recours gracieux contre cette décision, qui a été reçu le 13 juillet 2012. Par un ordre de reversement du 22 octobre 2012, l'Agence de services et de paiement a mis en recouvrement la somme concernée, d'un montant de 13 153,88 euros. A la demande de la société Château Chéri, adressée le 9 novembre 2012, l'Agence de services et de paiement a, par une décision du 22 novembre 2012, suspendu les poursuites. Elle a cependant repris la procédure de mise en recouvrement de cette somme en février 2014 et, en réponse à la société qui lui indiquait ne pas avoir reçu de réponse à son recours gracieux, lui a confirmé son ordre de reversement le 1er avril 2014. La société Château Chéri a alors demandé au préfet de la Gironde, par lettre du 22 avril 2014, avec copie à l'Agence de services et de paiement, de se prononcer sur le recours gracieux qu'elle avait formé contre la décision du 2 juillet 2012. Cette demande a fait l'objet, d'une part, d'une réponse de l'Agence de services et de paiement, en date du 6 mai 2014, indiquant qu'elle la transmettait au préfet et qu'à défaut de réponse de ce dernier dans un délai de deux mois, elle devrait être regardée comme implicitement rejetée et, d'autre part, d'une décision de rejet explicite du préfet en date du 7 juillet 2014. La société Château Chéri a formé un recours pour excès de pouvoir contre les décisions du 2 juillet 2012 et du 7 juillet 2014 devant le tribunal administratif de Bordeaux, qui l'a rejeté par un jugement du 24 mai 2016. Le ministre de l'agriculture et de l'alimentation se pourvoit en cassation contre l'arrêt du 8 février 2019 par lequel la cour administrative d'appel de Bordeaux a annulé ce jugement, ainsi que les décisions du préfet de la région Aquitaine du 2 juillet 2012 et du 7 juillet 2014.<br/>
<br/>
              2 Aux termes de l'article 18 de la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations, en vigueur à la date de la décision attaquée, dont les dispositions sont désormais reprises à l'article L. 110-1 du code des relations entre le public et l'administration : " Sont considérées comme des demandes au sens du présent chapitre les demandes et les réclamations, y compris les recours gracieux ou hiérarchiques, adressées aux autorités administratives. / (...) ". Aux termes de l'article 19 de la même loi, dont les dispositions sont désormais reprises aux articles L. 112-3 et L. 112-6 du même code : " Toute demande adressée à une autorité administrative fait l'objet d'un accusé de réception délivré dans les conditions définies par décret en Conseil d'Etat. (...) / (...) / Les délais de recours ne sont pas opposables à l'auteur d'une demande lorsque l'accusé de réception ne lui a pas été transmis ou ne comporte pas les indications prévues par le décret mentionné au premier alinéa. / (...) ". Aux termes de l'article 21 de la même loi, dont les dispositions sont désormais reprises, s'agissant des réclamations ou des recours administratifs, à l'article L. 231-4 du même code : " Sauf dans les cas où un régime de décision implicite d'acceptation est institué dans les conditions prévues à l'article 22, le silence gardé pendant plus de deux mois par l'autorité administrative sur une demande vaut décision de rejet. / (...) ". Aux termes de l'article R. 421-2 du code de justice administrative, dans sa rédaction alors applicable : " Sauf disposition législative ou réglementaire contraire, le silence gardé pendant plus de deux mois sur une réclamation par l'autorité compétente vaut décision de rejet. / Les intéressés disposent, pour se pourvoir contre cette décision implicite, d'un délai de deux mois à compter du jour de l'expiration de la période mentionnée au premier alinéa. (...) ". Aux termes de l'article 1er du décret du 6 juin 2001 pris pour l'application du chapitre II du titre II de la loi n° 2000-321 du 12 avril 2000 et relatif à l'accusé de réception des demandes présentées aux autorités administratives, dont les dispositions sont désormais reprises à l'article R. 112-5 du même code : " L'accusé de réception prévu par l'article 19 de la loi du 12 avril 2000 susvisée comporte les mentions suivantes : / 1° La date de réception de la demande et la date à laquelle, à défaut d'une décision expresse, celle-ci sera réputée acceptée ou rejetée ; / (...) / L'accusé de réception indique si la demande est susceptible de donner lieu à une décision implicite de rejet ou à une décision implicite d'acceptation. Dans le premier cas, l'accusé de réception mentionne les délais et les voies de recours à l'encontre de la décision. (...) ". Il résulte de ces dispositions, d'une part, qu'en l'absence d'accusé de réception comportant les mentions prévues par ces dernières dispositions, les délais de recours contentieux contre une décision implicite de rejet ne sont, en principe, pas opposables à son destinataire et, d'autre part, qu'un recours gracieux constituant une demande, ce principe s'applique aux décisions rejetant implicitement un tel recours gracieux.<br/>
<br/>
              3. Le principe de sécurité juridique, qui implique que ne puissent être remises en cause sans condition de délai des situations consolidées par l'effet du temps, fait obstacle à ce que puisse être contestée indéfiniment une décision administrative individuelle qui a été notifiée à son destinataire, ou dont il est établi, à défaut d'une telle notification, que celui-ci a eu connaissance. En une telle hypothèse, si le non-respect de l'obligation d'informer l'intéressé sur les voies et délais de recours, ou l'absence de preuve qu'une telle information a bien été fournie, ne permet pas que lui soient opposés les délais de recours fixés par le code de justice administrative, le destinataire de la décision ne peut exercer de recours juridictionnel au-delà d'un délai raisonnable. En règle générale et sauf circonstances particulières dont se prévaudrait le requérant, ce délai ne saurait, sous réserve de l'exercice de recours administratifs pour lesquels les textes prévoient des délais particuliers, excéder un an à compter de la date à laquelle une décision expresse lui a été notifiée ou de la date à laquelle il est établi qu'il en a eu connaissance.<br/>
<br/>
              4. Les règles énoncées au point 3, relatives au délai raisonnable au-delà duquel le destinataire d'une décision ne peut exercer de recours juridictionnel sont également applicables à la contestation d'une décision implicite de rejet née du silence gardé par l'administration sur une demande présentée devant elle, lorsqu'il est établi que le demandeur a eu connaissance de la décision. Ce principe s'applique également au rejet implicite d'un recours gracieux. La preuve de la connaissance du rejet implicite d'un recours gracieux ne saurait résulter du seul écoulement du temps depuis la présentation du recours. Elle peut en revanche résulter de ce qu'il est établi, soit que l'intéressé a été clairement informé des conditions de naissance d'un refus implicite de son recours gracieux, soit que la décision prise sur ce recours a par la suite été expressément mentionnée au cours de ses échanges avec l'administration. S'il n'a pas été informé des voies et délais dans les conditions prévues par les textes cités au point 2, l'auteur du recours gracieux, dispose, pour saisir le juge, d'un délai raisonnable qui court, dans la première hypothèse, de la date de naissance de la décision implicite et, dans la seconde, de la date de l'événement établissant qu'il a eu connaissance de cette décision.<br/>
<br/>
              5. Il ressort des pièces du dossier soumis aux juges du fond que la société Château Chéri a formé, le 13 juillet 2012, contre la décision du 2 juillet 2012 prononçant la déchéance de ses droits, un recours gracieux, qui a interrompu le délai de recours contentieux. Ce recours gracieux n'a pas fait l'objet d'un accusé de réception comportant les mentions rappelées au point 2 ci-dessus, et informant donc la société des conditions de naissance d'une décision implicite. Compte tenu de ses échanges ultérieurs avec l'administration, tels que rappelés au point 1, elle ne peut être regardée comme ayant eu connaissance du rejet de son recours gracieux que, au plus tôt, le 7 juillet 2014. Dans ces conditions, il résulte de ce qui a été dit au point 4 que le délai de recours contre la décision rejetant son recours gracieux et de la décision visée par ce recours gracieux ne pouvait courir qu'à compter du 7 juillet 2014. Par suite, l'unique moyen du ministre de l'agriculture et de l'alimentation, tiré de ce que la cour administrative d'appel aurait méconnu son office en n'opposant pas d'office à la requête de la société Château Chéri une irrecevabilité découlant de ce que, présentée le 6 septembre 2014, elle avait été formée plus d'un an après le 13 juillet 2012, date à laquelle, compte tenu du recours gracieux qu'elle avait formé, la société devait être réputée avoir eu connaissance de la décision du 2 juillet 2012, doit être écarté et son pourvoi doit être rejeté.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du ministre de l'agriculture et de l'alimentation est rejeté.<br/>
Article 2 : La présente décision sera notifiée au ministre de l'agriculture et de l'alimentation, à l'Agence de services et de paiement et à la société Château Chéri.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-07 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. PRINCIPES INTÉRESSANT L'ACTION ADMINISTRATIVE. - IMPOSSIBILITÉ D'EXERCER UN RECOURS JURIDICTIONNEL AU-DELÀ D'UN DÉLAI RAISONNABLE [RJ1] - CAS DES DÉCISIONS IMPLICITES DE REJET [RJ2] - REJET IMPLICITE D'UN RECOURS GRACIEUX - INCLUSION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-07 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉLAIS. - CONTESTATION D'UNE DÉCISION IMPLICITE DE REJET - CAS DU REJET IMPLICITE D'UN RECOURS GRACIEUX - IMPOSSIBILITÉ D'EXERCER UN RECOURS JURIDICTIONNEL AU-DELÀ D'UN DÉLAI RAISONNABLE [RJ1] - EXISTENCE [RJ2].
</SCT>
<ANA ID="9A"> 01-04-03-07 Les règles relatives au délai raisonnable au-delà duquel le destinataire d'une décision ne peut exercer de recours juridictionnel sont applicables à la contestation du rejet implicite d'un recours gracieux.</ANA>
<ANA ID="9B"> 54-01-07 Les règles relatives au délai raisonnable au-delà duquel le destinataire d'une décision ne peut exercer de recours juridictionnel sont applicables à la contestation du rejet implicite d'un recours gracieux.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 13 juillet 2016, M. Czabaj, n° 387763, p. 340.,,[RJ2] Cf. CE, 18 mars 2019, M.,, n° 417270, p. 60.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
