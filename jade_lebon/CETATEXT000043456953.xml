<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043456953</ID>
<ANCIEN_ID>JG_L_2021_04_000000440348</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/45/69/CETATEXT000043456953.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 27/04/2021, 440348</TITRE>
<DATE_DEC>2021-04-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440348</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Yohann Bouquerel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:440348.20210427</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Lacroix Signalisation a demandé au tribunal administratif de Nantes d'annuler les opérations d'expertise confiées à M. A... par une ordonnance n° 1506225 du président du tribunal administratif de Nantes du 31 août 2015. Le département de la Loire-Atlantique a demandé au même tribunal de condamner la société Lacroix Signalisation à lui verser une indemnité de 5 millions d'euros en réparation du préjudice résultant des pratiques anticoncurrentielles de cette société lors de la passation, entre 1998 et 2005, de cinq marchés publics de signalisation routière verticale, ainsi que la somme de 28 573 euros correspondant aux frais d'expertise judiciaire. Par un jugement n°s 1607875, 1610255 du 19 juin 2019, le tribunal administratif de Nantes a condamné la société Lacroix Signalisation à verser au département de la Loire-Atlantique une somme de 3 746 476 euros, mis à la charge de cette société la somme de 28 573 euros au titre des frais d'expertise et rejeté le surplus des conclusions des parties.<br/>
<br/>
              Par un arrêt n°s 19NT02444, 19NT02442 du 6 mars 2020, la cour administrative d'appel de Nantes a rejeté l'appel formé par la société Lacroix Signalisation contre ce jugement et a jugé en conséquence qu'il n'y avait plus lieu de statuer sur sa demande de sursis à exécution de celui-ci. Elle a en revanche, sur appel incident du département de la Loire-Atlantique, réformé le jugement en portant la somme mise à la charge de cette société à 4 121 124 euros et elle a rejeté le surplus de ses conclusions. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 30 avril et 15 juillet 2020 et 24 février 2021 au secrétariat du contentieux du Conseil d'Etat, la société Lacroix City Saint-Herblain, venant aux droits de la société Lacroix Signalisation, demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge du département de la Loire-Atlantique la somme de 10 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - la directive 2014/104/UE du 26 novembre 2014 ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de commerce ; <br/>
              - le code des marchés publics ; <br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yohann Bouquerel, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de la société Lacroix City Saint-Herblain et à la SCP Matuchansky, Poupot, Valdelièvre, avocat du département de la Loire-Atlantique ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que le département de la Loire-Atlantique a conclu avec la société Lacroix Signalisation, entre 1998 et 2005, cinq marchés portant sur la fourniture de panneaux de signalisation routière pour un montant total d'environ 15 millions d'euros. Par une décision n° 10-D-39 du 22 décembre 2010, devenue définitive, l'Autorité de la concurrence a sanctionné huit entreprises, dont la société Lacroix Signalisation, pour s'être entendues entre 1997 et 2006 sur la répartition et le prix des marchés ayant un tel objet. Par une ordonnance du 31 août 2015, le juge des référés du tribunal administratif de Nantes a désigné un expert pour évaluer le préjudice subi par le département de la Loire Atlantique du fait de cette entente. Dans son rapport du 13 mai 2016 et son rapport complémentaire du 7 juillet suivant, l'expert a évalué ce préjudice à 5 millions d'euros environ. Par un jugement du 19 juin 2019, le tribunal administratif de Nantes a condamné la société Lacroix Signalisation à verser au département de la Loire-Atlantique la somme de 3 746 476 euros, ainsi que la somme de 28 573 euros au titre des frais d'expertise. La société Lacroix City Saint-Herblain, venant aux droits de la société Lacroix Signalisation, se pourvoit en cassation contre l'arrêt du 6 mars 2020 par lequel la cour administrative de Nantes a rejeté son appel contre ce jugement et a fait droit aux conclusions de l'appel incident du département de la Loire Atlantique en portant la somme mise à la charge de cette société à 4 121 124 euros. <br/>
<br/>
              2. En premier lieu, il ressort des énonciations de l'arrêt attaqué que la cour administrative d'appel de Nantes a répondu à l'ensemble des moyens invoqués par la requérante, y compris ceux présentés dans la deuxième partie de la requête d'appel, sans se méprendre sur la portée des écritures de la société requérante.<br/>
<br/>
              3. En deuxième lieu, en relevant que la délibération du 20 avril 2015 du conseil départemental de la Loire-Atlantique habilitait son président à agir en justice, au nom du département, pour intenter toutes actions en justice et défendre ses intérêts, et en en déduisant que l'appel incident présenté par le président du conseil départemental de la Loire-Atlantique était recevable, la cour administrative d'appel de Nantes n'a pas commis d'erreur de droit. <br/>
<br/>
              4. En troisième lieu, aux termes de l'article R. 621-7 du code de justice administrative : " Les parties sont averties par le ou les experts des jours et heures auxquels il sera procédé à l'expertise ; cet avis leur est adressé quatre jours au moins à l'avance, par lettre recommandée ". Si ces dispositions fixent les modalités selon lesquelles un expert désigné par le tribunal doit avertir les parties des réunions ou visites qu'il organise, elles n'ont ni pour objet ni pour effet de lui imposer d'en organiser. Il suit de là qu'en jugeant que ni ces dispositions ni les termes de la mission définie par l'ordonnance du juge des référés du tribunal administratif de Nantes du 31 août 2015, qui visait à l'évaluation du préjudice subi par le département de la Loire-Atlantique, n'imposaient à l'expert désigné par cette ordonnance d'organiser une réunion des parties avant de remettre son rapport et en admettant, par suite, la régularité des opérations d'expertise, la cour administrative d'appel de Nantes n'a pas méconnu son office et n'a pas commis d'erreur de droit.<br/>
<br/>
              5. En quatrième lieu, il ressort des énonciations de l'arrêt attaqué que la cour administrative d'appel de Nantes s'est fondée, pour calculer le préjudice subi par le département, sur la méthode préconisée par l'expert consistant à comparer les taux de marge de la requérante pendant la durée de l'entente et après la fin de celle-ci pour en déduire le surcoût supporté par le département de la Loire-Atlantique sur les marchés litigieux. Il ressort également des énonciations de l'arrêt attaqué que la cour a retenu, pour calculer ce préjudice, une part de 40% du chiffre d'affaires total de la requérante dédiée à l'activité de signalisation routière verticale. En se fondant sur ce taux, qui avait été établi par l'Autorité de la concurrence au point 43 de sa décision du 22 décembre 2010 et qui concernait spécifiquement l'activité de la société Lacroix Signalisation en 2009, et en excluant en conséquence le taux de 78% proposé par la requérante au motif que celle-ci ne justifiait pas la différence entre les deux taux, la cour administrative d'appel de Nantes a souverainement apprécié les faits de l'espèce, sans les dénaturer, et n'a pas commis d'erreur de droit, alors même que le paiement des marchés litigieux s'est étalé entre 1999 et 2010. En statuant ainsi, elle n'a pas davantage entaché son arrêt d'un défaut de motivation ou d'une contradiction de motifs.<br/>
<br/>
              6. En dernier lieu, il ressort des énonciations de l'arrêt attaqué que l'expert a vérifié la cohérence des résultats de la méthode mentionnée au point précédent en recourant à une autre méthode consistant à comparer les prix d'un échantillon de produits représentatifs, comportant neuf produits correspondants à la catégorie de la signalisation plastique. Si la société requérante soutient que ces produits ne pouvaient être inclus dans l'échantillon parce qu'elle n'aurait pas été condamnée pour des pratiques anticoncurrentielles sur le marché de la signalisation plastique, il ressort des énonciations de l'arrêt attaqué que la cour s'est référée à la décision de l'Autorité de la concurrence du 22 décembre 2010 qui indique que les pratiques anticoncurrentielles pour lesquelles elle a sanctionné la société requérante portaient sur " la signalisation routière verticale au sens large, laquelle concerne tant la signalisation verticale permanente et temporaire (panneaux métalliques) que la signalisation dite plastique (équipements de sécurité et de balisage en matière plastique) ". Par suite, la société requérante n'est pas fondée à soutenir qu'en jugeant que la prise en compte de ces produits plastiques n'entachait pas la représentativité de l'échantillon retenu, la cour administrative d'appel aurait commis une erreur de droit et dénaturé les pièces du dossier. <br/>
<br/>
              7. Il résulte de tout ce qui précède que la société Lacroix City Saint-Herblain n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              8. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge du département de la Loire-Atlantique, qui n'est pas la partie perdante, le versement d'une somme au titre des frais exposés par la requérante et non compris dans les dépens. Il y a lieu en revanche, dans les circonstances de l'espèce, de mettre à la charge de la société Lacroix City Saint-Herblain la somme de 3 000 euros à verser au département de la Loire-Atlantique au titre de ces mêmes dispositions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi de la société Lacroix City Saint-Herblain est rejeté.<br/>
<br/>
Article 2 : La société Lacroix City Saint-Herblain versera au département de la Loire-Atlantique une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la société Lacroix City Saint-Herblain et au département de la Loire-Atlantique.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-02 MARCHÉS ET CONTRATS ADMINISTRATIFS. FORMATION DES CONTRATS ET MARCHÉS. - PRÉJUDICE SUBI AU TITRE DU SURCOÛT LIÉ À DES PRATIQUES ANTICONCURRENTIELLES - MÉTHODE D'ÉVALUATION [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-04-02-02-01 PROCÉDURE. INSTRUCTION. MOYENS D'INVESTIGATION. EXPERTISE. RECOURS À L'EXPERTISE. - OBLIGATION POUR L'EXPERT D'ORGANISER DES RÉUNIONS OU VISITES - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">60-04-03-02 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RÉPARATION. ÉVALUATION DU PRÉJUDICE. PRÉJUDICE MATÉRIEL. - PRÉJUDICE SUBI AU TITRE DU SURCOÛT LIÉ À DES PRATIQUES ANTICONCURRENTIELLES - MÉTHODE D'ÉVALUATION [RJ1].
</SCT>
<ANA ID="9A"> 39-02 Pour évaluer l'ampleur du préjudice subi par une personne publique au titre du surcoût lié à une entente, il est loisible de se fonder sur la comparaison des taux de marge de la société pendant la durée de l'entente et après la fin de celle-ci pour en déduire le surcoût supporté par la personne publique sur les marchés litigieux.</ANA>
<ANA ID="9B"> 54-04-02-02-01 Si l'article R. 621-7 du code de justice administrative fixe les modalités selon lesquelles un expert désigné par le tribunal doit avertir les parties des réunions ou visites qu'il organise, elles n'ont ni pour objet ni pour effet de lui imposer d'en organiser.</ANA>
<ANA ID="9C"> 60-04-03-02 Pour évaluer l'ampleur du préjudice subi par une personne publique au titre du surcoût lié à une entente, il est loisible de se fonder sur la comparaison des taux de marge de la société pendant la durée de l'entente et après la fin de celle-ci pour en déduire le surcoût supporté par la personne publique sur les marchés litigieux.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., pour une autre méthode possible d'évaluation, CE, 27 mars 2020, Société Signalisation France, n° 420491, p. 152.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
