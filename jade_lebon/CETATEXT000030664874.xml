<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030664874</ID>
<ANCIEN_ID>JG_L_2015_06_000000372057</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/66/48/CETATEXT000030664874.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème / 9ème SSR, 01/06/2015, 372057, Publié au recueil Lebon</TITRE>
<DATE_DEC>2015-06-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>372057</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème / 9ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Paquita Morellet-Steiner</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:372057.20150601</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 10 septembre et 11 décembre 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour l'association Promouvoir, dont le siège est BP 48 à Pernes les Fontaines (84210) ; l'association demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 12PA00838 du 3 juillet 2013 par lequel la cour administrative d'appel de Paris a rejeté sa requête tendant à l'annulation du jugement n° 1021073/7-1 du 16 décembre 2011 par lequel le tribunal administratif avait rejeté sa demande tendant à l'annulation de la décision du ministre de la culture du 4 novembre 2010 accordant au film intitulé SAW 3 D Chapitre Final un visa d'exploitation comportant seulement une interdiction aux mineurs de seize ans et à ce qu'il soit enjoint au ministre de la culture et de la communication de prendre toutes les mesures nécessaires pour retirer ce film des salles ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du cinéma et de l'image animée ; <br/>
              - le code pénal, notamment son article 227-24 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Paquita Morellet-Steiner, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de l'sssociation Promouvoir et à la SCP Piwnica, Molinié, avocat de la ministre de la culture et de la communication ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 211-1 du code du cinéma et de l'image animée : " La représentation cinématographique est subordonnée à l'obtention d'un visa d'exploitation délivré par le ministre chargé de la culture./ Ce visa peut être refusé ou sa délivrance subordonnée à des conditions pour des motifs tirés de la protection de l'enfance et de la jeunesse ou du respect de la dignité humaine.(...) " ; qu'aux termes de l'article R. 211-10 du même code : " Le ministre chargé de la culture délivre le visa d'exploitation cinématographique aux oeuvres (...) cinématographiques (...) destinés à une représentation cinématographique, après avis de la commission de classification des oeuvres cinématographiques (...) " ; qu'aux termes de son article L. 211-11 : " Le visa d'exploitation cinématographique vaut autorisation de représentation publique des oeuvres ou documents sur tout le territoire de la France métropolitaine et des départements et régions d'outre-mer. (...) " ; qu'aux termes, enfin, de l'article R. 211 12 : " Le visa d'exploitation cinématographique s'accompagne de l'une des mesures de classification suivantes : /1° Autorisation de la représentation pour tous publics ; /2° Interdiction de la représentation aux mineurs de douze ans ; /3° Interdiction de la représentation aux mineurs de seize ans ; /4° Interdiction de la représentation aux mineurs de dix-huit ans sans inscription sur la liste prévue à l'article L. 311-2, lorsque l'oeuvre ou le document comporte des scènes de sexe non simulées ou de très grande violence mais qui, par la manière dont elles sont filmées et la nature du thème traité, ne justifient pas une telle inscription ; /5° Interdiction de la représentation aux mineurs de dix-huit ans avec inscription de l'oeuvre ou du document sur la liste prévue à l'article L. 311-2. " ; <br/>
<br/>
              2. Considérant que les dispositions de l'article L. 211-1 du code du cinéma et de l'image animée confèrent au ministre chargé de la culture l'exercice d'une police spéciale fondée sur les nécessités de la protection de l'enfance et de la jeunesse et du respect de la dignité humaine, en vertu de laquelle il lui incombe en particulier de prévenir la commission de l'infraction réprimée par les dispositions de l'article 227-24 du code pénal, qui interdisent la diffusion, par quelque moyen que ce soit et quel qu'en soit le support, d'un message à caractère violent ou de nature à porter gravement atteinte à la dignité humaine lorsqu'il est susceptible d'être vu ou perçu par un mineur, soit en refusant de délivrer à une oeuvre cinématographique un visa d'exploitation, soit en imposant à sa diffusion l'une des restrictions prévues à l'article R. 211-12 du code du cinéma et de l'image animée, qui lui paraît appropriée au regard tant des intérêts publics dont il doit assurer la préservation que du contenu particulier de cette oeuvre ; qu'il résulte de ce dernier article qu'il appartient aux juges du fond, saisis d'un recours dirigé contre le visa d'exploitation délivré à une oeuvre comportant des scènes violentes, de rechercher si les scènes en cause caractérisent ou non l'existence de scènes de très grande violence de la nature de celles dont le 4° et le 5° de cet article interdisent la projection à des mineurs ; que, dans l'hypothèse où le juge retient une telle qualification, il lui revient ensuite d'apprécier la manière dont ces scènes sont filmées et dont elles s'insèrent au sein de l'oeuvre considérée, pour déterminer laquelle de ces deux restrictions est appropriée, eu égard aux caractéristiques de cette oeuvre cinématographique ; <br/>
<br/>
              3. Considérant que, par une décision du 4 novembre 2010, le ministre de la culture a accordé au film intitulé SAW 3 D Chapitre Final un visa d'exploitation comportant une interdiction aux mineurs de seize ans, assorti de l'obligation d'informer les spectateurs de l'avertissement suivant : " Ce film comporte un grand nombre de scènes de torture particulièrement réalistes et d'une grande brutalité, voire sauvagerie " ; que l'association Promouvoir a contesté, devant le tribunal administratif de Paris, cette décision en tant qu'elle autorisait la diffusion de cette oeuvre à des mineurs ; que, par un jugement du 16 décembre 2011, le tribunal administratif a rejeté sa demande ; que l'association se pourvoit en cassation contre l'arrêt du 3 juillet 2013 par lequel la cour administrative de Paris a rejeté son appel dirigé contre ce jugement ;<br/>
<br/>
              4. Considérant qu'après avoir estimé que le film SAW 3 D Chapitre final comportait de nombreuses scènes qu'elle a qualifiées de scènes de très grande violence en relevant notamment que des personnages y sont soumis à " des jeux " et tués " dans des conditions particulièrement atroces ", la cour s'est ensuite fondée sur ce que ces scènes suivaient toutefois " les codes propres à la mise en scène des films d'horreur dits " gore " , pour juger que, compte tenu " de la distance critique " des jeunes de plus de seize ans à l'égard des oeuvres appartenant à ce genre cinématographique, leur caractère outrancier " compensait, en partie, la représentation très réaliste des sévices infligés " ; que, dès lors qu'elle avait relevé l'existence de passages caractérisant des scènes de très grande violence, la cour ne pouvait, sans erreur de droit, estimer que ces scènes pouvaient néanmoins justifier que le ministre de la culture délivrât à ce film un visa d'exploitation autorisant sa projection aux mineurs de seize ans ; que les appréciations qu'elle a portées sur la manière dont ces scènes sont filmées, si elles étaient de nature à justifier le choix entre le classement prévu au 5° et l'interdiction simple aux mineurs de dix-huit ans prévue au 4° de l'article R. 211-12 du code du cinéma et de l'image animée, ne pouvaient légalement lui permettre de regarder les scènes qu'elle qualifiait ainsi comme n'appelant pas un visa autre que l'un de ceux-ci ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'arrêt de la cour administrative d'appel de Paris doit être annulé ; <br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              6. Considérant que, lorsqu'une oeuvre cinématographique comporte, comme tel est le cas du film SAW 3 D Chapitre final, de nombreuses scènes violentes, il y a lieu de prendre en considération, pour déterminer si la présence de ces scènes doit entraîner une interdiction aux mineurs de dix-huit ans, la manière, plus ou moins réaliste, dont elles sont filmées, l'effet qu'elles sont destinées à produire sur les spectateurs, notamment de nature à inciter à la violence ou à la banaliser, enfin, toute caractéristique permettant d'apprécier la mise à distance de la violence et d'en relativiser l'impact sur la jeunesse ;<br/>
<br/>
              7. Considérant qu'il ressort des pièces du dossier que le film SAW 3 D Chapitre final comporte un grand nombre de scènes filmées avec un grand réalisme, montrant des actes répétés de torture et de barbarie et représentant, de manière particulièrement complaisante, les souffrances atroces, tant physiques que psychologiques, des victimes prises dans des pièges, mis au point par un tueur, où elles sont incitées à se mutiler elles-mêmes soit pour échapper à la mort, soit pour sauver des proches ; que de telles scènes, sans toutefois caractériser une incitation à la violence, comportent une représentation de la violence de nature à heurter la sensibilité des mineurs et justifient ainsi une interdiction de ce film aux mineurs de dix-huit ans ; que, par suite, le ministre de la culture a commis une erreur d'appréciation en interdisant la diffusion du film en cause aux seuls mineurs de seize ans ;<br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin d'examiner ses autres moyens, l'association Promouvoir est fondée à soutenir que c'est à tort que le tribunal administratif de Paris a rejeté sa demande tendant à l'annulation du visa délivré par le ministre de la culture à ce film ;<br/>
<br/>
              Sur les conclusions à fin d'injonction :<br/>
<br/>
              9. Considérant que l'exécution de la présente décision n'implique pas que le ministre de la culture prenne les mesures nécessaires pour retirer le film litigieux des salles ; qu'elle appelle seulement, après un réexamen par le ministre, la délivrance d'un nouveau visa d'exploitation à ce film ; <br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              10. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 500 euros à verser à l'association Promouvoir au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 3 juillet 2013 et le jugement du tribunal administratif de Paris du 16 décembre 2011 sont annulés.<br/>
Article 2 : Le visa délivré le 4 novembre 2010 au film SAW 3D Chapitre final par le ministre de la culture est annulé.<br/>
Article 3 : L'Etat versera à l'association Promouvoir la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Le surplus des conclusions de la requête est rejeté. <br/>
Article 5 : La présente décision sera notifiée à l'association Promouvoir et à la ministre de la culture et de la communication. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">09-05-01 ARTS ET LETTRES. CINÉMA. - 1) PRINCIPES GÉNÉRAUX DE LA POLICE SPÉCIALE DU CINÉMA CONFIÉE AU MINISTRE CHARGÉ DE LA CULTURE (ART. L. 211-1 DU CCIA) - 2) OFFICE DU JUGE SAISI D'UN RECOURS CONTRE LE VISA DÉLIVRÉ À UNE &#140;UVRE COMPORTANT DES SCÈNES VIOLENTES - A) RECHERCHE DE L'EXISTENCE DE SCÈNES DE TRÈS GRANDE VIOLENCE IMPLIQUANT L'INTERDICTION AUX MINEURS AU TITRE DU 4° OU DU 5° DE L'ARTICLE R. 211-12 DU CCIA - B) EXAMEN DE LA MANIÈRE DONT CES SCÈNES SONT FILMÉES POUR LE CHOIX ENTRE LES RESTRICTIONS PRÉVUES AUX 4° ET 5° - 3) APPLICATION À UN FILM COMPORTANT DE NOMBREUSES SCÈNES VIOLENTS.
</SCT>
<ANA ID="9A"> 09-05-01 1) Les dispositions de l'article L. 211-1 du code du cinéma et de l'image animée (CCIA) confèrent au ministre chargé de la culture l'exercice d'une police spéciale fondée sur les nécessités de la protection de l'enfance et de la jeunesse et du respect de la dignité humaine, en vertu de laquelle il lui incombe en particulier de prévenir la commission de l'infraction réprimée par les dispositions de l'article 227-24 du code pénal, qui interdisent la diffusion, par quelque moyen que ce soit et quel qu'en soit le support, d'un message à caractère violent ou de nature à porter gravement atteinte à la dignité humaine lorsqu'il est susceptible d'être vu ou perçu par un mineur, soit en refusant de délivrer à une oeuvre cinématographique un visa d'exploitation, soit en imposant à sa diffusion l'une des restrictions prévues à l'article R. 211-12 du code du cinéma et de l'image animée, qui lui paraît appropriée au regard tant des intérêts publics dont il doit assurer la préservation que du contenu particulier de cette oeuvre.... ,,2) a) Il résulte de cet article R. 211-12 qu'il appartient aux juges du fond, saisis d'un recours dirigé contre le visa d'exploitation délivré à une oeuvre comportant des scènes violentes, de rechercher si les scènes en cause caractérisent ou non l'existence de scènes de très grande violence de la nature de celles dont le 4° et le 5° de cet article interdisent la projection à des mineurs. En présence de scènes recevant cette qualification, aucun des autres classements, prévus au 1°, 2° et 3° du même article, ne saurait légalement être retenu.,,,b) Dans l'hypothèse où le juge retient la qualification de scènes de très grande violence, il lui revient ensuite d'apprécier la manière dont ces scènes sont filmées et dont elles s'insèrent au sein de l'oeuvre considérée, pour déterminer laquelle de ces deux restrictions est appropriée, eu égard aux caractéristiques de cette oeuvre cinématographique.... ,,3) Lorsqu'une oeuvre cinématographique comporte de nombreuses scènes violentes, il y a lieu de prendre en considération, pour déterminer si la présence de ces scènes doit entraîner une interdiction aux mineurs de dix-huit ans, la manière, plus ou moins réaliste, dont elles sont filmées, l'effet qu'elles sont destinées à produire sur les spectateurs, notamment de nature à inciter à la violence ou à la banaliser, enfin, toute caractéristique permettant d'apprécier la mise à distance de la violence et d'en relativiser l'impact sur la jeunesse.,,,En l'espèce, film comportant un grand nombre de scènes filmées avec un grand réalisme, montrant des actes répétés de torture et de barbarie. De telles scènes, sans toutefois caractériser une incitation à la violence, comportent une représentation de la violence de nature à heurter la sensibilité des mineurs et justifient ainsi une interdiction de ce film aux mineurs de dix-huit ans.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
