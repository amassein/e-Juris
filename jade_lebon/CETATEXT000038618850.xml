<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038618850</ID>
<ANCIEN_ID>JG_L_2019_05_000000427786</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/61/88/CETATEXT000038618850.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 22/05/2019, 427786</TITRE>
<DATE_DEC>2019-05-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>427786</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON, MEGRET</AVOCATS>
<RAPPORTEUR>Mme Cécile Renault</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHR:2019:427786.20190522</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un jugement n° 1706613 du 5 février 2019, enregistré le 6 février 2019 au secrétariat du contentieux du Conseil d'Etat, le tribunal administratif de Lyon, avant de statuer sur la requête du Fonds de garantie des victimes d'actes de terrorisme et autres infractions (FGTI) tendant à l'annulation de la décision du département de la Loire rejetant sa demande indemnitaire préalable tendant au remboursement des sommes versées à M. L. W. et à la condamnation du département de la Loire à lui verser la somme de 25 000 euros, a décidé, par application des dispositions de l'article L. 113-1 du code de justice administrative, de transmettre le dossier de cette requête au Conseil d'Etat, en soumettant à son examen les questions suivantes :<br/>
<br/>
              1°) Le Fonds de garantie des victimes d'actes de terrorisme et d'autres infractions est-il une personne morale de droit privé '<br/>
<br/>
              2°) En cas de réponse positive à la première question :<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ;<br/>
              - le code des assurances ;<br/>
              - le code civil ;<br/>
              - la loi n° 86-1020 du 9 septembre 1986 ; <br/>
              - l'arrêté du ministre d'Etat, ministre de l'économie, des finances et du budget et du garde des sceaux, ministre de la justice du 3 juillet 1991 approuvant les statuts du Fonds de garantie contre les actes de terrorisme et d'autres infractions ;<br/>
              - l'arrêté du ministre de l'économie et des finances du 16 mars 2017 approuvant les modifications des statuts du Fonds de garantie des victimes des actes de terrorisme et d'autres infractions;<br/>
              - le code de justice administrative, notamment son article L. 113-1 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Renault, auditeur, <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              - La parole ayant été donnée, avant et après les conclusions, à la SCP Boré, Salve de Bruneton, Mégret, avocat du Fonds de garantie des victimes d'actes de terrorisme et d'autres infractions ;<br/>
<br/>
<br/>
<br/>REND L'AVIS SUIVANT :<br/>
<br/>
              Sur la première question posée :<br/>
<br/>
              1. La loi du 9 septembre 1986 relative à la lutte contre le terrorisme a créé un fonds de garantie des victimes des actes de terrorisme, chargé d'assurer l'indemnisation des victimes d'actes de terrorisme. Les missions de ce fonds, devenu fonds de garantie des victimes d'actes de terrorisme et d'autres infractions (FGTI), ont été par la suite élargies aux victimes d'autre infractions et à une aide au recouvrement des dommages et intérêts pour les victimes d'infractions bénéficiaires d'une décision pénale définitive.<br/>
<br/>
              2. Aux termes de l'article L. 422-1 du code des assurances : " Ce fonds, doté de la personnalité civile, est alimenté par un prélèvement sur les contrats d'assurance de biens (...) Le fonds est également alimenté par des versements prévus au II de l'article 728-1 du code de procédure pénale ". Aux termes de l'article R. 422-1 du même code, le fonds " est géré par un conseil d'administration qui comprend :/ 1o Un président nommé par arrêté conjoint du ministre chargé de l'économie et des finances et du garde des sceaux, ministre de la justice, parmi les membres en activité ou honoraires du Conseil d'État ayant au moins atteint le grade de conseiller d'État ou parmi les membres en activité ou honoraires de la Cour de cassation ayant au moins atteint le grade de conseiller ou d'avocat général ;/ 2o Un représentant du ministre chargé de l'économie et des finances, nommé par arrêté ;/ 3o Un représentant du ministre de la justice, nommé par arrêté ;/ 4o Un représentant du ministre de l'intérieur, nommé par arrêté ; / 5o Un représentant du ministre chargé de la sécurité sociale, nommé par arrêté ; / 6o Trois personnes ayant manifesté leur intérêt pour les victimes d'actes de terrorisme et d'autres infractions, nommées par arrêté conjoint du ministre chargé de l'économie et des finances, du ministre de la justice, du ministre de l'intérieur et du ministre chargé de la sécurité sociale ;/ 7o Un professionnel du secteur de l'assurance, nommé par arrêté du ministre chargé de l'économie et des finances. ". Aux termes de l'article R. 422-2 du même code : " Les statuts du fonds de garantie sont approuvés par arrêté conjoint du garde des sceaux, ministre de la justice, et du ministre chargé des assurances ". Aux termes de l'article R. 422-3 du même code : " Le fonds de garantie est soumis au contrôle du ministre chargé des assurances qui nomme un commissaire du Gouvernement pour exercer en son nom un contrôle sur l'ensemble de la gestion du fonds. Le commissaire du Gouvernement peut assister à toutes les réunions du conseil d'administration ou des comités institués par ce conseil. Il peut se faire présenter tous les livres et documents comptables./ Les décisions prises par le conseil d'administration ou par les autorités auxquelles il accorde délégation sont exécutoires dans un délai de quinze jours à dater de la décision si le commissaire du Gouvernement ne signifie pas soit qu'il approuve immédiatement, soit qu'il s'oppose à la décision. Toutefois, le délai ci-dessus est ramené à cinq jours en ce qui concerne les décisions ne comportant pas un engagement financier pour le fonds. ". Aux termes de l'article R. 422-4 du même code : " Les opérations du fonds sont comptabilisées conformément aux règles applicables aux entreprises d'assurance ". Aux termes, enfin, de l'article 11 des statuts du FGTI, approuvés par un arrêté du 16 mars 2017 : " La gestion des opérations du Fonds est confiée au Fonds de garantie institué par l'article L. 421-1 du code des assurances. "<br/>
<br/>
              3. Le FGTI, organisme créé par la loi, joue un rôle essentiel dans la mise en oeuvre de la politique publique d'aide aux victimes et constitue ainsi un instrument de la solidarité nationale. Ainsi qu'il résulte des dispositions citées au point 2, ses ressources proviennent d'une contribution forfaitaire assise sur les contrats d'assurance qui a le caractère d'une imposition au sens de l'article 34 de la Constitution, les membres du conseil d'administration du fonds sont tous nommés par arrêté ministériel ou interministériel, quatre des neuf administrateurs sont des représentants de l'Etat et la gestion du fonds est contrôlée par un commissaire du Gouvernement qui peut s'opposer à toutes les décisions du conseil d'administration. Il résulte de l'ensemble de ces éléments que, alors même que le FGTI n'est pas doté de prérogatives de puissance publique, que sa comptabilité est soumise au droit privé et que sa gestion est assurée par le fonds de garantie des assurances obligatoires de dommages (FGAO), lui-même qualifié par l'article L. 421-2 du code des assurances de personne morale de droit privé, le FGTI doit être regardé comme un organisme de droit public.<br/>
<br/>
              Sur les deuxième et troisième questions posées :<br/>
<br/>
              4. Compte tenu de la réponse apportée à la première question, les questions 2 et 3 sont dépourvues d'objet.<br/>
<br/>
              Sur la quatrième question posée :<br/>
<br/>
              5. L'article 8 des statuts du FGTI dispose que : " Le conseil d'administration (...) autorise les actions judiciaires (...). D'une manière générale, il prend toutes les décisions nécessaires au bon fonctionnement du Fonds, les pouvoirs détaillés ci-dessus n'étant énoncés qu'à titre indicatif et non limitatif ". L'article 10 des mêmes statuts prévoit que : " Le Fonds est représenté en justice par le président du conseil d'administration ou par toute autre personne jouissant du plein exercice de ses droits civils et déléguée à cet effet par le conseil ". Aucune disposition des statuts du FGTI n'interdit par principe au conseil d'administration du fonds de déléguer sa compétence d'autoriser les actions en justice. <br/>
<br/>
              6. Par ailleurs, s'agissant de l'interprétation des statuts du FGTI, ainsi qu'il a été dit au point 2, l'article 11 a confié au FGAO, institué par l'article L. 421-1 du code des assurances, la gestion de ses opérations. L'article 1er de la convention de gestion entre le FGAO et le FGTI, signée le 13 mars 1991, stipule : " (...) Le directeur général du Fonds-circulation exerce, pour les opérations relevant du Fonds-terrorisme-infractions, les pouvoirs qui lui sont délégués par le conseil d'administration de ce dernier ". Le procès verbal de la réunion du conseil d'administration du FGTI du 11 juillet 2016 indique : " Le conseil délègue à compter du 11 juin 2016 à M. A... B..., ès qualité de Directeur général du FGAO, avec faculté de se substituer en cas de besoin pour tout ou partie toute personne appartenant au personnel de ce Fonds, les pouvoirs nécessaires pour la direction des affaires du FGTI, notamment : h) représenter le fonds de garantie des victimes d'actes de terrorisme et d'autres infractions devant toutes les juridictions et, d'une façon générale, exercer toutes actions tant en demande qu'en défense ".  <br/>
<br/>
              7. Il résulte des dispositions citées ci-dessus que le conseil d'administration du fonds a délégué de manière non limitée dans le temps au directeur général du FGAO la compétence d'exercer toutes actions en justice, y compris les recours subrogatoires exercés par le fonds afin de recouvrer tout ou partie des indemnités versées aux victimes. <br/>
<br/>
<br/>
<br/>
<br/>Le présent avis sera notifié au tribunal administratif de Lyon, au Fonds de garantie des victimes d'actes de terrorisme et autres infractions et au ministre de l'économie et des finances.<br/>
              Copie en sera transmise au département de la Loire, à la caisse primaire d'assurance maladie de la Loire et au ministre de la justice.<br/>
<br/>
              Il sera publié au Journal officiel de la République française.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-04 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RÉPARATION. - FONDS DE GARANTIE DES VICTIMES DES ACTES DE TERRORISME ET D'AUTRES INFRACTIONS (FGTI) - ORGANISME DE DROIT PUBLIC [RJ1].
</SCT>
<ANA ID="9A"> 60-04 Le fonds de garantie des victimes des actes de terrorisme et d'autres infractions (FGTI), organisme créé par la loi, joue un rôle essentiel dans la mise en oeuvre de la politique publique d'aide aux victimes et constitue ainsi un instrument de la solidarité nationale. Ainsi qu'il résulte des articles L. 422-1, R. 422-1 à R. 422-4 du code des assurances et 11 des statuts du FGTI, approuvés par un arrêté du 16 mars 2017, ses ressources proviennent d'une contribution forfaitaire assise sur les contrats d'assurance qui a le caractère d'une imposition au sens de l'article 34 de la Constitution, les membres du conseil d'administration du fonds sont tous nommés par arrêté ministériel ou interministériel, quatre des neuf administrateurs sont des représentants de l'Etat et la gestion du fonds est contrôlée par un commissaire du gouvernement qui peut s'opposer à toutes les décisions du conseil d'administration. Il résulte de l'ensemble de ces éléments que, alors même que le FGTI n'est pas doté de prérogatives de puissance publique, que sa comptabilité est soumise au droit privé et que sa gestion est assurée par le fonds de garantie des assurances obligatoires de dommages (FGAO), lui-même qualifié par l'article L. 421-2 du code des assurances de personne morale de droit privé, le FGTI doit être regardé comme un organisme de droit public.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. CE, Section des finances, avis, 18 avril 2017, n° 393137, Rapport public 2018, p. 318.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
