<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029003647</ID>
<ANCIEN_ID>JG_L_2014_05_000000356930</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/00/36/CETATEXT000029003647.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème / 8ème SSR, 28/05/2014, 356930</TITRE>
<DATE_DEC>2014-05-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>356930</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème / 8ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MEIER-BOURDEAU, LECUYER ; SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Romain Victor</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:356930.20140528</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 20 février et 21 mai 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour l'établissement public national des produits de l'agriculture et de la mer (FranceAgriMer), venant aux droits de l'Office national interprofessionnel de l'élevage et de ses productions (Oniep), lui-même venu aux droits de l'Office national interprofessionnel du lait et des produits laitiers (Onilait), dont le siège est 12, rue Henri Rol-Tanguy, Tsa 20002 à Montreuil-sous-Bois (93555) ; FranceAgriMer demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09PA02947 du 16 décembre 2011 de la cour administrative d'appel de Paris en tant que, par cet arrêt, la cour, après avoir annulé le jugement n° 0612329 du 19 mars 2009 par lequel le tribunal administratif de Paris a fait droit à la demande de la société Beuralia, venue aux droits de la société France Beurre, tendant à l'annulation de la décision du 6 juillet 2006 par laquelle l'Oniep a réclamé à cette société le versement d'une somme de 6 992,79 euros correspondant au montant d'une garantie de transformation, a annulé la décision litigieuse ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la société Beuralia la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le règlement (CEE) n° 2220/85 de la Commission du 22 juillet 1985 ;<br/>
<br/>
              Vu le règlement (CEE, Euratom) n° 2988/95 du Conseil du 18 décembre 1995 ;<br/>
<br/>
              Vu le règlement (CE) n° 2571/97 de la Commission du 15 décembre 1997 ;<br/>
<br/>
              Vu le code civil ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Romain Victor, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Meier-Bourdeau, Lecuyer, avocat de l'établissement public national des produits de l'agriculture et de la mer (FranceAgriMer) et à la SCP Rocheteau, Uzan-Sarano, avocat de la société Beuralia ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des énonciations de l'arrêt attaqué que la société France Beurre a participé, au cours de l'année 2000, à des adjudications particulières organisées par l'Office national interprofessionnel du lait et des produits laitiers (Onilait) en vue de l'octroi de l'aide communautaire, dite au " beurre pâtissier ", pour la fabrication de beurre destiné, après addition de traceurs, à être incorporé dans des produits de pâtisserie, glaces et autres produits alimentaires dans les conditions fixées par le règlement (CE) n° 2571/97 de la Commission du 15 décembre 1997 ; qu'à l'issue de ces procédures, la société a été déclarée adjudicataire de l'aide pour la fabrication de " beurre concentré tracé " qu'elle s'est engagée à fabriquer et à incorporer ou faire incorporer dans les produits finaux, en constituant à cette fin des garanties de transformation ; qu'après fabrication, la société France Beurre a cédé le beurre concentré tracé à la société Le Fournil du Val-de-Loire ; qu'un contrôle de cette société, réalisé du 23 au 25 octobre 2001, a établi qu'une quantité de 7 800 kilogrammes de beurre concentré tracé avait été perdue dans le processus de nettoyage des pompes d'injection de la matière grasse et n'avait pu ainsi être incorporée dans les produits finaux ; que les résultats de ce contrôle ont été portés à la connaissance de la société le Fournil du Val-de-Loire le 21 décembre 2001 ; que, par un courrier du 5 décembre 2005, le directeur de l'Onilait a informé la société France Beurre que le défaut d'incorporation d'une partie du beurre concentré tracé dans les produits finaux était susceptible d'entraîner le versement d'une somme représentant le montant de la garantie de transformation appliquée à la quantité nette de beurre concentré perdue ; que, par une décision du 6 juillet 2006, le directeur de l'Office national interprofessionnel de l'élevage et de ses productions (Oniep), venu aux droits de l'Onilait, a notifié à la société Beuralia, venue aux droits de la société France Beurre, qu'elle était assujettie pour ce motif au paiement de la somme de 6 992,79 euros ; que, par un jugement du 19 mars 2009, le tribunal administratif de Paris a fait droit à la demande la société Beuralia tendant à l'annulation de cette décision ; que l'établissement public national des produits de l'agriculture et de la mer (FranceAgriMer), qui vient aux droits de l'Oniep, se pourvoit en cassation contre l'arrêt du 16 décembre 2011 par lequel la cour administrative d'appel de Paris, après avoir annulé le jugement du tribunal administratif de Paris du 19 mars 2009, a annulé la décision du directeur de l'Oniep du 6 juillet 2006 au motif que l'action de l'office était atteinte par la prescription ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 1er du règlement (CEE, Euratom) n° 2988/95 du 18 décembre 1995 : " 1. Aux fins de la protection des intérêts financiers des Communautés européennes, est adoptée une réglementation générale relative à des contrôles homogènes et à des mesures et des sanctions administratives portant sur des irrégularités au regard du droit communautaire. / 2. Est constitutive d'une irrégularité toute violation d'une disposition du droit communautaire résultant d'un acte ou d'une omission d'un opérateur économique qui a ou aurait pour effet de porter préjudice au budget général des Communautés ou à des budgets gérés par celles-ci, soit par la diminution ou la suppression de recettes provenant des ressources propres perçues directement pour le compte des Communautés, soit par une dépense indue " ; qu'aux termes de l'article 3 du même règlement : " 1. Le délai de prescription des poursuites est de quatre ans à partir de la réalisation de l'irrégularité visée à l'article 1er paragraphe 1 (...). / La prescription des poursuites est interrompue par tout acte, porté à la connaissance de la personne en cause, émanant de l'autorité compétente et visant à l'instruction ou à la poursuite de l'irrégularité. Le délai de prescription court à nouveau à partir de chaque acte interruptif (...). / 3. Les Etats membres conservent la possibilité d'appliquer un délai plus long que celui prévu respectivement au paragraphe 1 et au paragraphe 2 " ; qu'aux termes de l'article 2262 du code civil, dans sa rédaction applicable à la date de la décision litigieuse : " Toutes les actions, tant réelles que personnelles, sont prescrites par trente ans " ; <br/>
<br/>
              4. Considérant, en premier lieu, qu'ainsi que l'a jugé la Cour de justice de l'Union européenne dans son arrêt du 5 mai 2011 Ze Fu Fleischhandel GmbH et Vion Trading GmbH (affaires C-201/10 et C-202/10), le principe de sécurité juridique s'oppose à ce qu'un délai de prescription " plus long ", au sens de l'article 3, paragraphe 3 du règlement n° 2988/95 résulte d'un délai de prescription de droit commun réduit par voie jurisprudentielle pour satisfaire au principe de proportionnalité ; qu'en l'absence d'un texte spécial fixant, dans le respect de ce principe, un délai de prescription plus long, seul le délai de prescription de quatre années prévu par l'article 3, paragraphe 1, premier alinéa, du règlement précité est applicable ; qu'ainsi, la cour n'a commis aucune erreur de droit en jugeant, après avoir rappelé les termes de cet arrêt, qu'en l'absence de réglementation nationale spécifique légalement applicable aux faits de la cause,  le délai de prescription trentenaire de droit commun de l'article 2262 du code civil, même réduit par la voie jurisprudentielle, ne pouvait être appliqué aux actions des organismes d'intervention agricole lorsqu'ils appréhendent une garantie de transformation en cas de non-respect par un opérateur de l'exigence principale relative à l'incorporation de la matière grasse tracée dans les produits finaux et que seule la règle de prescription quadriennale prévue par le règlement n° 2988/95 devait être appliquée ;<br/>
<br/>
              5. Considérant, en second lieu, qu'aux termes de l'article 19, paragraphe 4, du règlement n° 2571/97 : " Les droits et obligations découlant de l'adjudication ne sont pas transmissibles " ; qu'ainsi que l'a jugé la Cour de justice de l'Union européenne dans son arrêt du 22 décembre 2010 Corman SA contre Bureau d'intervention et de restitution belge (BIRB) (affaire C-131/10), il résulte de ces dispositions que l'adjudicataire est responsable de la destination finale du beurre et doit répondre du comportement de ses cocontractants ainsi que des acheteurs ultérieurs ; qu'il résulte par ailleurs du troisième alinéa du 1 de l'article 3 du règlement n° 2988/95 qu'un acte émanant de l'autorité compétente n'est interruptif du délai de prescription de quatre années mentionné au premier alinéa du 1 du même article que si, visant à l'instruction ou à la poursuite de l'irrégularité, il est adressé à la personne en cause au titre de cette irrégularité ; que, dès lors, la cour n'a entaché son arrêt d'aucune erreur de droit ni d'aucune erreur de qualification juridique en jugeant que la notification faite le 21 décembre 2001 à la société Le Fournil du Val-de-Loire, utilisatrice finale du beurre concentré tracé fabriqué par la société France Beurre, adjudicataire de l'aide, des résultats du contrôle diligenté par l'Onilait, n'avait pu interrompre le délai de prescription de quatre années, au motif que la société Le Fournil du Val-de-Loire n'avait pas, au regard de l'irrégularité poursuivie, la qualité de " personne en cause " au sens des dispositions précitées du règlement n° 2988/95 ;<br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que FranceAgriMer n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque ;<br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société Beuralia qui n'est pas, dans la présente instance, la partie perdante ; qu'en revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de FranceAgriMer la somme de 3 000 euros à verser à la société Beuralia au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de FranceAgriMer est rejeté.<br/>
<br/>
Article 2 : FranceAgriMer versera à la société Beuralia une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à l'établissement public national des produits de l'agriculture et de la mer (FranceAgrimer), à la société Beuralia et au ministre de l'agriculture, de l'agroalimentaire et de la forêt, porte parole du Gouvernement.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">03-05-03-02 AGRICULTURE ET FORÊTS. PRODUITS AGRICOLES. ÉLEVAGE ET PRODUITS DE L'ÉLEVAGE. PRODUITS LAITIERS. - AIDE EUROPÉENNE AU BEURRE PÂTISSIER (RÈGLEMENT DU 15 DÉCEMBRE 1997) - 1) RESPONSABILITÉ DE L'ADJUDICATAIRE S'AGISSANT DE LA DESTINATION FINALE DU BEURRE ET DU COMPORTEMENT DE SES COCONTRACTANTS AINSI QUE DES ACHETEURS ULTÉRIEURS [RJ1] - 2) POURSUITE DES IRRÉGULARITÉS - ACTE INTERRUPTIF DU DÉLAI DE PRESCRIPTION DE QUATRE ANNÉES (ART. 3 DU RÈGLEMENT DU 18 DÉCEMBRE 1995) - CONDITIONS - ACTE ADRESSÉ À LA PERSONNE EN CAUSE - 3) CONSÉQUENCE - NON-RESPECT DE L'OBLIGATION D'INCORPORATION DE BEURRE TRACÉ - NOTIFICATION DES RÉSULTATS DU CONTRÔLE À LA SOCIÉTÉ UTILISATRICE FINALE DU BEURRE AU LIEU DE LA SOCIÉTÉ ADJUDICATAIRE - INTERRUPTION DE LA PRESCRIPTION - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">15-02-02 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. PORTÉE DES RÈGLES DU DROIT DE L'UNION EUROPÉENNE. RÈGLEMENTS. - 1) RÈGLEMENT DU 15 DÉCEMBRE 1997 INSTITUANT UN RÉGIME D'AIDE AU BEURRE PÂTISSIER - RESPONSABILITÉ DE L'ADJUDICATAIRE S'AGISSANT DE LA DESTINATION FINALE DU BEURRE ET DU COMPORTEMENT DE SES COCONTRACTANTS AINSI QUE DES ACHETEURS ULTÉRIEURS - EXISTENCE [RJ1] - 2) RÈGLEMENT DU 18 DÉCEMBRE 1995 RELATIF À LA PROTECTION DES INTÉRÊTS FINANCIERS DES COMMUNAUTÉS EUROPÉENNES - POURSUITE DES IRRÉGULARITÉS - ACTE INTERRUPTIF DU DÉLAI DE PRESCRIPTION DE QUATRE ANNÉES (ART. 3) - CONDITIONS - ACTE ADRESSÉ À LA PERSONNE EN CAUSE - 3) CONSÉQUENCE - AIDE AU BEURRE PÂTISSIER - NON-RESPECT DE L'OBLIGATION D'INCORPORATION DE BEURRE TRACÉ - NOTIFICATION DES RÉSULTATS DU CONTRÔLE À LA SOCIÉTÉ UTILISATRICE FINALE DU BEURRE AU LIEU DE LA SOCIÉTÉ ADJUDICATAIRE - INTERRUPTION DE LA PRESCRIPTION - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">15-05-14 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. RÈGLES APPLICABLES. POLITIQUE AGRICOLE COMMUNE. - 1) AIDE EUROPÉENNE AU BEURRE PÂTISSIER (RÈGLEMENT DU 15 DÉCEMBRE 1997) - RESPONSABILITÉ DE L'ADJUDICATAIRE S'AGISSANT DE LA DESTINATION FINALE DU BEURRE ET DU COMPORTEMENT DE SES COCONTRACTANTS AINSI QUE DES ACHETEURS ULTÉRIEURS [RJ1] - 2) POURSUITE DES IRRÉGULARITÉS - ACTE INTERRUPTIF DU DÉLAI DE PRESCRIPTION DE QUATRE ANNÉES (ART. 3 DU RÈGLEMENT DU 18 DÉCEMBRE 1995) - CONDITIONS - ACTE ADRESSÉ À LA PERSONNE EN CAUSE - 3) CONSÉQUENCE - AIDE AU BEURRE PÂTISSIER - NON-RESPECT DE L'OBLIGATION D'INCORPORATION DE BEURRE TRACÉ - NOTIFICATION DES RÉSULTATS DU CONTRÔLE À LA SOCIÉTÉ UTILISATRICE FINALE DU BEURRE AU LIEU DE LA SOCIÉTÉ ADJUDICATAIRE - INTERRUPTION DE LA PRESCRIPTION - ABSENCE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">15-08 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. LITIGES RELATIFS AU VERSEMENT D`AIDES DE L'UNION EUROPÉENNE. - 1) AIDE EUROPÉENNE AU BEURRE PÂTISSIER (RÈGLEMENT DU 15 DÉCEMBRE 1997) - RESPONSABILITÉ DE L'ADJUDICATAIRE S'AGISSANT DE LA DESTINATION FINALE DU BEURRE ET DU COMPORTEMENT DE SES COCONTRACTANTS AINSI QUE DES ACHETEURS ULTÉRIEURS [RJ1] - 2) POURSUITE DES IRRÉGULARITÉS - ACTE INTERRUPTIF DU DÉLAI DE PRESCRIPTION DE QUATRE ANNÉES (ART. 3 DU RÈGLEMENT DU 18 DÉCEMBRE 1995) - CONDITIONS - ACTE ADRESSÉ À LA PERSONNE EN CAUSE - 3) CONSÉQUENCE - AIDE AU BEURRE PÂTISSIER - NON-RESPECT DE L'OBLIGATION D'INCORPORATION DE BEURRE TRACÉ - NOTIFICATION DES RÉSULTATS DU CONTRÔLE À LA SOCIÉTÉ UTILISATRICE FINALE DU BEURRE AU LIEU DE LA SOCIÉTÉ ADJUDICATAIRE - INTERRUPTION DE LA PRESCRIPTION - ABSENCE.
</SCT>
<ANA ID="9A"> 03-05-03-02 1) Ainsi que l'a jugé la Cour de justice de l'Union européenne dans son arrêt du 22 décembre 2010 Corman SA c/ Bureau d'intervention et de restitution belge (BIRB) (aff. C-131/10), il résulte des dispositions du paragraphe 4 de l'article 19 du règlement (CE) n° 2571/97 de la Commission du 15 décembre 1997, aux termes duquel :  Les droits et obligations découlant de l'adjudication ne sont pas transmissibles , que l'adjudicataire est responsable de la destination finale du beurre et doit répondre du comportement de ses cocontractants ainsi que des acheteurs ultérieurs.,,,2) Il résulte par ailleurs du troisième alinéa du 1 de l'article 3 du règlement (CE, Euratom) n° 2988/95 du Conseil du 18 décembre 1995 qu'un acte émanant de l'autorité compétente n'est interruptif du délai de prescription de quatre années mentionné au premier alinéa du 1 du même article que si, visant à l'instruction ou à la poursuite de l'irrégularité, il est adressé à la personne en cause au titre de cette irrégularité.... ,,3) Par suite, en cas de non-respect de l'obligation d'incorporation de beurre tracé dans les produits finaux, le délai de prescription de quatre années mentionné au premier alinéa du 1 de l'article 3 du règlement du 18 décembre 1995 ne saurait être interrompu par la notification des résultats du contrôle faite à la société utilisatrice finale du beurre concentré tracé fabriqué par la société adjudicataire de l'aide, la société utilisatrice finale n'ayant pas, au regard de l'irrégularité poursuivie, la qualité de  personne en cause  au sens des dispositions de ce règlement.</ANA>
<ANA ID="9B"> 15-02-02 1) Ainsi que l'a jugé la Cour de justice de l'Union européenne dans son arrêt du 22 décembre 2010 Corman SA c/ Bureau d'intervention et de restitution belge (BIRB) (aff. C-131/10), il résulte des dispositions du paragraphe 4 de l'article 19 du règlement (CE) n° 2571/97 de la Commission du 15 décembre 1997, aux termes duquel :  Les droits et obligations découlant de l'adjudication ne sont pas transmissibles , que l'adjudicataire est responsable de la destination finale du beurre et doit répondre du comportement de ses cocontractants ainsi que des acheteurs ultérieurs.,,,2) Il résulte par ailleurs du troisième alinéa du 1 de l'article 3 du règlement (CE, Euratom) n° 2988/95 du Conseil du 18 décembre 1995 qu'un acte émanant de l'autorité compétente n'est interruptif du délai de prescription de quatre années mentionné au premier alinéa du 1 du même article que si, visant à l'instruction ou à la poursuite de l'irrégularité, il est adressé à la personne en cause au titre de cette irrégularité.... ,,3) Par suite, en cas de non-respect de l'obligation d'incorporation de beurre tracé dans les produits finaux, le délai de prescription de quatre années mentionné au premier alinéa du 1 de l'article 3 du règlement du 18 décembre 1995 ne saurait être interrompu par la notification des résultats du contrôle faite à la société utilisatrice finale du beurre concentré tracé fabriqué par la société adjudicataire de l'aide, la société utilisatrice finale n'ayant pas, au regard de l'irrégularité poursuivie, la qualité de  personne en cause  au sens des dispositions de ce règlement.</ANA>
<ANA ID="9C"> 15-05-14 1) Ainsi que l'a jugé la Cour de justice de l'Union européenne dans son arrêt du 22 décembre 2010 Corman SA c/ Bureau d'intervention et de restitution belge (BIRB) (aff. C-131/10), il résulte des dispositions du paragraphe 4 de l'article 19 du règlement (CE) n° 2571/97 de la Commission du 15 décembre 1997, aux termes duquel :  Les droits et obligations découlant de l'adjudication ne sont pas transmissibles , que l'adjudicataire est responsable de la destination finale du beurre et doit répondre du comportement de ses cocontractants ainsi que des acheteurs ultérieurs.,,,2) Il résulte par ailleurs du troisième alinéa du 1 de l'article 3 du règlement (CE, Euratom) n° 2988/95 du Conseil du 18 décembre 1995 qu'un acte émanant de l'autorité compétente n'est interruptif du délai de prescription de quatre années mentionné au premier alinéa du 1 du même article que si, visant à l'instruction ou à la poursuite de l'irrégularité, il est adressé à la personne en cause au titre de cette irrégularité.... ,,3) Par suite, en cas de non-respect de l'obligation d'incorporation de beurre tracé dans les produits finaux, le délai de prescription de quatre années mentionné au premier alinéa du 1 de l'article 3 du règlement du 18 décembre 1995 ne saurait être interrompu par la notification des résultats du contrôle faite à la société utilisatrice finale du beurre concentré tracé fabriqué par la société adjudicataire de l'aide, la société utilisatrice finale n'ayant pas, au regard de l'irrégularité poursuivie, la qualité de  personne en cause  au sens des dispositions de ce règlement.</ANA>
<ANA ID="9D"> 15-08 1) Ainsi que l'a jugé la Cour de justice de l'Union européenne dans son arrêt du 22 décembre 2010 Corman SA c/ Bureau d'intervention et de restitution belge (BIRB) (aff. C-131/10), il résulte des dispositions du paragraphe 4 de l'article 19 du règlement (CE) n° 2571/97 de la Commission du 15 décembre 1997, aux termes duquel :  Les droits et obligations découlant de l'adjudication ne sont pas transmissibles , que l'adjudicataire est responsable de la destination finale du beurre et doit répondre du comportement de ses cocontractants ainsi que des acheteurs ultérieurs.,,,2) Il résulte par ailleurs du troisième alinéa du 1 de l'article 3 du règlement (CE, Euratom) n° 2988/95 du Conseil du 18 décembre 1995 qu'un acte émanant de l'autorité compétente n'est interruptif du délai de prescription de quatre années mentionné au premier alinéa du 1 du même article que si, visant à l'instruction ou à la poursuite de l'irrégularité, il est adressé à la personne en cause au titre de cette irrégularité.... ,,3) Par suite, en cas de non-respect de l'obligation d'incorporation de beurre tracé dans les produits finaux, le délai de prescription de quatre années mentionné au premier alinéa du 1 de l'article 3 du règlement du 18 décembre 1995 ne saurait être interrompu par la notification des résultats du contrôle faite à la société utilisatrice finale du beurre concentré tracé fabriqué par la société adjudicataire de l'aide, la société utilisatrice finale n'ayant pas, au regard de l'irrégularité poursuivie, la qualité de  personne en cause  au sens des dispositions de ce règlement.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CJUE, 22 décembre 2010, Corman SA c/ Bureau d'intervention et de restitution belge (BIRB), aff. C-131/10, Rec. p. I-14199.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
