<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036205227</ID>
<ANCIEN_ID>JG_L_2017_12_000000397060</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/20/52/CETATEXT000036205227.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 13/12/2017, 397060</TITRE>
<DATE_DEC>2017-12-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>397060</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; SCP SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR>M. Pierre Lombard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:397060.20171213</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La société ID3A a demandé au tribunal administratif de Strasbourg d'enjoindre à FranceAgriMer de produire la décision du 7 avril 2006 par laquelle la commission nationale technique aurait défavorablement statué sur sa demande de reconnaissance en qualité d'organisation de producteurs pour le secteur des fruits et légumes, de déclarer cette décision nulle et sans effet et d'annuler le titre de recette adressé le 8 février 2011 pour un montant de 145 255,08 euros.<br/>
<br/>
              Par un jugement n° 1102102 du 4 décembre 2014, le tribunal administratif de Strasbourg a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 15NC00248 du 17 décembre 2015, la cour administrative d'appel de Nancy a annulé ce jugement ainsi que le titre de recette du 8 février 2011.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 17 février et 17 mai 2016 au secrétariat du contentieux du Conseil d'Etat, FranceAgriMer demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de la SARL ID3A la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le règlement (CE) n° 2988/95 du Conseil, du 18 décembre 1995 ;<br/>
              - le règlement (CE) n° 2200/96 du Conseil, du 28 octobre 1996 ;<br/>
              - le règlement (CE) n° 1432/2003 de la Commission, du 11 août 2003 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pierre Lombard, Auditeur, <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de la l'Etablissement national des produits de l'agriculture et de la mer et à la SCP Sevaux, Mathonnet, avocat de la société ID3A ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 30 novembre 2017, présentée par la société ID3A ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, le 24 novembre 1998, la société ID3A a formé une demande de pré-reconnaissance en tant qu'organisation de producteurs pour le secteur des fruits et légumes, en application de l'article 14 du règlement n° 2200/96 du Conseil, du 28 octobre 1996, portant organisation commune des marchés dans le secteur des fruits et légumes, à laquelle il a été fait droit par une décision du ministre de l'agriculture et de la pêche du 23 décembre 1998. Le plan de reconnaissance déposé par la société a par ailleurs été agréé au 15 décembre 1998, portant ainsi l'expiration de la période de pré-reconnaissance au 15 décembre 2003. A ce titre, la société requérante a bénéficié d'aides communautaires pour les exercices 1998 à 2002. La demande de reconnaissance ultérieurement formée par l'intéressée le 1er décembre 2005 a fait l'objet d'un avis défavorable de la commission nationale technique (CNT) en date du 7 avril 2006, notifié au directeur départemental de l'agriculture et de la forêt du Haut-Rhin. Par une décision du 28 septembre 2007, annulée et remplacée par une nouvelle décision du 9 octobre 2007, Viniflhor a demandé à la société ID3A le reversement de 50 % des aides versées, représentant une somme de 145 255,08 euros. La société requérante a formé le 9 octobre 2007 un recours gracieux contre cette décision, auquel il n'a pas été répondu. Enfin, par une décision du 8 février 2011, FranceAgriMer, venant aux droits de Viniflhor, a de nouveau demandé à la société ID3A le reversement des aides versées et lui a notifié le titre de recettes exécutoire correspondant émis le 2 février 2011. Par un jugement du 4 décembre 2014, le tribunal administratif de Strasbourg a rejeté la demande de la société ID3A. FranceAgriMer se pourvoit en cassation contre l'arrêt par lequel la cour administrative d'appel de Nancy a, après avoir annulé ce jugement, annulé le titre de recettes du 2 février 2011.<br/>
<br/>
              2. Aux termes de l'article 11 du règlement du Conseil du 28 octobre 1996 : " 1. Aux fins du présent règlement, on entend par " organisation de producteurs " toute personne morale: / a) qui est constituée à l'initiative même des producteurs des catégories suivantes des produits visés à l'article 1er paragraphe 2 : / i) fruits et légumes ; / ii) fruits ; / iii) légumes ; / iv) produits destinés à la transformation ; / v) agrumes ; / vi) fruits à coques ; / vii) champignons ; / b) qui a notamment pour but : / 1) d'assurer la programmation de la production et son adaptation à la demande, notamment en quantité et en qualité ; / 2) de promouvoir la concentration de l'offre et la mise en marché de la production des membres ; / 3) de réduire les coûts de production et de régulariser les prix à la production ; / 4) de promouvoir des pratiques culturales et des techniques de production et de gestion des déchets respectueuses de l'environnement, notamment pour protéger la qualité des eaux, du sol, du paysage et pour préserver et/ou promouvoir la biodiversité ; / (...) e) qui a été reconnue par l'État membre concerné dans les conditions énoncées au paragraphe 2. / 2. Les États membres reconnaissent en tant qu'organisations de producteurs au sens du présent règlement les groupements de producteurs qui en font la demande, à condition : / a) qu'ils répondent aux exigences posées au paragraphe 1 et apportent à cette fin, entre autres justifications, la preuve qu'ils réunissent un nombre minimal de producteurs et un volume minimal de production commercialisable, à déterminer selon la procédure prévue à l'article 46 (...) ". Aux termes de l'article 21 du règlement de la Commission, du 11 août 2003, portant modalités d'application du règlement (CE) n° 2200/96 du Conseil en ce qui concerne la reconnaissance des organisations de producteurs et la préreconnaissance des groupements de producteurs : " (...) 4. Les États membres récupèrent au moins 50 % de l'aide payée en vertu des dispositions de l'article 14 du règlement (CE) n° 2200/96 si la mise en oeuvre du plan de reconnaissance ne mène pas à la reconnaissance, sauf en cas dûment justifié à la satisfaction de l'État membre ".<br/>
<br/>
              3. Il résulte de ces dispositions qu'il incombe à l'administration, pour déterminer si un opérateur peut être reconnu en tant qu'organisation de producteurs, de vérifier qu'il remplit les seuls critères fixés à l'article 11 du règlement du Conseil du 28 octobre 1996. Si tel n'est pas le cas, il lui appartient alors de mettre en oeuvre la procédure de répétition des aides versées pendant le plan de reconnaissance, prévue à l'article 21 du règlement de la Commission du 11 août 2003, sauf si des circonstances particulières, dûment justifiées par l'opérateur ayant perçu les aides, le justifient. Au titre de ces circonstances, l'administration peut légalement prendre en compte l'existence de difficultés économiques rencontrées par l'opérateur pendant la période de pré-reconnaissance, ou sa situation au moment où l'administration décide de mettre en oeuvre la procédure de répétition des aides versées pendant le plan de reconnaissance, pour moduler la récupération de ces aides ou même estimer qu'elle n'a pas lieu d'être.<br/>
<br/>
              4. Dans son arrêt, la cour administrative d'appel de Nancy, après avoir constaté que Viniflhor avait demandé à la société ID3A le reversement de 50 % de l'aide versée au titre de la procédure de pré-reconnaissance, a jugé que FranceAgriMer n'avait pas examiné les arguments exposés par la société dans son recours gracieux, tenant notamment à l'existence de circonstances économiques difficiles justifiant qu'elle n'ait pas été en mesure, malgré sa volonté et le fait qu'elle soit parvenue à réaliser les autres conditions exigées par les textes, de remplir une des conditions nécessaires à l'obtention de la qualité d'organisation de producteurs. Elle a, par suite, jugé que l'administration n'avait pas exercé le pouvoir d'appréciation que lui confère l'article 21, paragraphe 4, du règlement de la Commission du 11 août 2003 qui autorisait légalement, en l'espèce, l'autorité compétente à moduler la restitution et à ne pas exiger la restitution en cas dûment justifié. 	<br/>
<br/>
              5. Cependant, contrairement à ce que soutient la société ID3A en défense, il ne ressort pas des pièces du dossier soumis aux juges du fond, en particulier de son recours gracieux du 9 octobre 2007, qu'elle ait argué de circonstances économiques difficiles justifiant qu'elle n'ait pas été en mesure, en dépit de sa volonté, de mettre en oeuvre le plan de reconnaissance sur lequel elle s'était engagée. Dès lors, la cour a dénaturé les pièces du dossier qui lui était soumis en estimant que l'administration n'avait pas examiné les arguments exposés par la société dans ce recours.<br/>
<br/>
              6. Par suite, FranceAgriMer est fondé, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, à demander l'annulation de l'arrêt qu'il attaque. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société ID3A le versement à FranceAgriMer d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. Les dispositions de cet article font, en revanche, obstacle à ce que la somme demandée par la société ID3A au titre des frais qu'elle a exposés et qui ne sont pas compris dans les dépens soit mise à la charge de FranceAgriMer, qui n'est pas la partie perdante dans la présente instance.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nancy du 17 décembre 2015 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nancy.<br/>
Article 3 : La société ID3A versera à FranceAgriMer la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de la société ID3A au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à FranceAgriMer et à la société ID3A.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">03-03-06 AGRICULTURE ET FORÊTS. EXPLOITATIONS AGRICOLES. AIDES DE L'UNION EUROPÉENNE. - RÉPÉTITION DES AIDES VERSÉES À UN GROUPEMENT DE PRODUCTEURS PENDANT LE PLAN DE RECONNAISSANCE, EN L'ABSENCE DE RECONNAISSANCE DE CELUI-CI, PRÉVUE PAR LE PARAGRAPHE 4 DE L'ART. 21 DU RÈGLEMENT N°1432/2003 DE LA COMMISSION DU 11 AOÛT 2003 - CAS D'UN PLAN DE RECONNAISSANCE EN COURS À LA DATE D'ENTRÉE EN VIGUEUR DU RÈGLEMENT - APPLICABILITÉ - EXISTENCE (SOL. IMPL.).
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">15-08 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. LITIGES RELATIFS AU VERSEMENT D`AIDES DE L'UNION EUROPÉENNE. - RÉPÉTITION DES AIDES VERSÉES À UN GROUPEMENT DE PRODUCTEURS PENDANT LE PLAN DE RECONNAISSANCE, EN L'ABSENCE DE RECONNAISSANCE DE CELUI-CI, PRÉVUE PAR LE PARAGRAPHE 4 DE L'ART. 21 DU RÈGLEMENT N°1432/2003 DE LA COMMISSION DU 11 AOÛT 2003 - CAS D'UN PLAN DE RECONNAISSANCE EN COURS À LA DATE D'ENTRÉE EN VIGUEUR DU RÈGLEMENT - APPLICABILITÉ - EXISTENCE (SOL. IMPL.).
</SCT>
<ANA ID="9A"> 03-03-06 Les dispositions du paragraphe 4 de l'article 21 du règlement n°1432/2003 de la Commission du 11 août 2003 relatives aux modalités de répétition des aides versées à un groupement de producteurs pendant le plan de reconnaissance si la mise en oeuvre de ce plan ne mène pas à la reconnaissance, sont applicables à la récupération d'aides versées au titre des plans de reconnaissance en cours au 19 août 2003, date d'entrée en vigueur de ce règlement.</ANA>
<ANA ID="9B"> 15-08 Les dispositions du paragraphe 4 de l'article 21 du règlement n°1432/2003 de la Commission du 11 août 2003 relatives aux modalités de répétition des aides versées à un groupement de producteurs pendant le plan de reconnaissance si la mise en oeuvre de ce plan ne mène pas à la reconnaissance, sont applicables à la récupération d'aides versées au titre des plans de reconnaissance en cours au 19 août 2003, date d'entrée en vigueur de ce règlement.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
