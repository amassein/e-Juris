<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043266679</ID>
<ANCIEN_ID>JG_L_2021_03_000000430244</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/26/66/CETATEXT000043266679.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 17/03/2021, 430244</TITRE>
<DATE_DEC>2021-03-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>430244</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BAUER-VIOLAS, FESCHOTTE-DESBOIS, SEBAGH ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Pierre Boussaroque</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:430244.20210317</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'association syndicale autorisée des propriétaires du domaine de Beauvallon, M. B... A... et Mme C... A... ont demandé au tribunal administratif de Toulon d'annuler pour excès de pouvoir la délibération du 29 février 2016 par laquelle la commune de Grimaud a approuvé la modification n° 1 du plan local d'urbanisme de la commune ainsi que le rejet de leur recours gracieux. Par un jugement n° 1602588 du 13 mars 2018, le tribunal administratif a fait droit à cette demande en tant seulement que cette délibération approuve les modifications apportées après l'enquête publique aux dispositions des articles 1AU 3 et 2AU 3 du règlement du plan local d'urbanisme ainsi qu'aux dispositions des articles A 2, IN 2 et 2N 2 du même règlement relatives à la nature et aux modalités d'implantation des extensions et des annexes des bâtiments à usage d'habitation existants à la date d'approbation de ce plan local d'urbanisme.<br/>
<br/>
              Par un arrêt n° 18MA02056 du 28 février 2019, la cour administrative d'appel de Marseille a rejeté l'appel formé par les requérants contre ce jugement en tant qu'il n'avait que partiellement fait droit à leur demande et l'appel incident de la commune de Grimaud.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique enregistrés les 29 avril et 29 juillet 2019 et le 18 février 2021 au secrétariat du contentieux du Conseil d'Etat, l'association syndicale autorisée des propriétaires du domaine de Beauvallon et M. et Mme A... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ; <br/>
<br/>
              3°) de mettre à la charge de la commune de Grimaud la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la directive 2001/42/CE du Parlement européen et du Conseil du 27 juin 2001 ;<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pierre Boussaroque, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Bauer-Violas, Feschotte-Desbois, Sebagh, avocat de l'association syndicale autorisée des propriétaires du domaine de Beauvallon et de M. et Mme A... et à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la commune de Grimaud ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un jugement du 13 mars 2018, le tribunal administratif de Toulon, saisi par l'association syndicale autorisée des propriétaires du domaine de Beauvallon et par M. et Mme A..., n'a annulé la délibération du 29 février 2016 du conseil municipal de Grimaud approuvant la modification n° 1 du plan local d'urbanisme de la commune qu'en tant que, d'une part, elle détermine certaines dispositions relatives à la nature et aux modalités d'implantation des extensions et des annexes des bâtiments à usage d'habitation existants à la date d'approbation du plan local d'urbanisme et en tant, d'autre part, que certaines dispositions relatives à l'accès et à la voirie ont été ajoutées au règlement de ce plan local d'urbanisme postérieurement à l'enquête publique sans procéder de celle-ci. Les requérants se pourvoient en cassation contre l'arrêt du 28 février 2019 par lequel la cour administrative d'appel de Marseille a rejeté l'appel qu'ils avaient formé contre ce jugement. <br/>
<br/>
              Sur la régularité de l'arrêt attaqué : <br/>
<br/>
              2. Pour rejeter l'appel dont elle était saisie, la cour a, en premier lieu, jugé que les dispositions de l'article L. 123-13-l du code de l'urbanisme, devenu l'article L. 153-37, étaient entrées en vigueur le 1er janvier 2013 et qu'elles n'étaient donc pas applicables à la date à laquelle le conseil municipal de Grimaud avait décidé d'engager la procédure de modification du plan local d'urbanisme. Elle a, en deuxième lieu, jugé que le commissaire enquêteur n'était pas tenu de répondre à chacune des observations recueillies au cours de l'enquête publique et que la circonstance que celui-ci ait proposé d'écarter comme étrangères à l'enquête plusieurs observations n'était pas de nature à entacher l'enquête publique d'irrégularité, le fait qu'il ait proposé de donner suite à certaines mais pas à d'autres n'étant par ailleurs pas de nature à établir qu'il aurait méconnu son obligation d'impartialité. Elle a, en troisième lieu, précisé que le rapport de présentation de la modification du plan local d'urbanisme avait évalué les incidences de cette modification sur l'environnement, en l'occurrence le risque d'inondation, et avait exposé la manière dont ce plan avait pris ce risque en compte. Elle a, enfin, estimé que si le rapport de présentation du plan local d'urbanisme avait été complété, notamment pour exposer les dispositions prises en matière de risque d'incendie et corriger certaines données démographiques, il n'avait pas pour autant été " réécrit " de telle sorte qu'une procédure de révision aurait été nécessaire. Les requérants ne sont pas fondés à soutenir que la cour, qui n'était pas tenue de répondre à l'ensemble des arguments qu'ils développaient à l'appui de leurs moyens, aurait insuffisamment motivé son arrêt sur ces différents points. <br/>
<br/>
              Sur le bien-fondé de l'arrêt attaqué :<br/>
<br/>
              En ce qui concerne les modifications apportées au projet postérieurement à l'enquête publique :<br/>
<br/>
              3. Aux termes du septième alinéa de l'article L. 123-13-2, devenu l'article L. 153-43, du code de l'urbanisme, applicable au litige : " A l'issue de l'enquête publique, [le projet de modification], éventuellement modifié pour tenir compte des avis qui ont été joints au dossier, des observations du public et du rapport du commissaire ou de la commission d'enquête, est approuvé par délibération de l'organe délibérant de l'établissement public de coopération intercommunale ou du conseil municipal ". Il résulte de ces dispositions que le projet de plan ne peut subir de modifications, entre la date de sa soumission à l'enquête publique et celle de son approbation, qu'à la double condition que ces modifications ne remettent pas en cause l'économie générale du projet et qu'elles procèdent de l'enquête. Doivent être regardées comme procédant de l'enquête les modifications destinées à tenir compte des réserves et recommandations du commissaire enquêteur ou de la commission d'enquête, des observations du public et des avis émis par les autorités, collectivités et instances consultées et joints au dossier de l'enquête.<br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond qu'à la suite d'observations du public portant sur le caractère imprécis de la notion de surface minéralisée, le commissaire enquêteur avait recommandé, dans un souci de clarté et afin d'éviter tout éventuel litige relatif à une interprétation erronée du texte, de revoir la rédaction des articles UC 14-3ème, 1 AU 13-3ème et 2 AU 13-3ème du projet de règlement de plan local d'urbanisme relatifs aux espaces libres et plantations. Sur la base de cette recommandation, la collectivité a, postérieurement à l'enquête publique, modifié ces articles en vue de dispenser du respect des règles relatives aux espaces libres de plantations certaines constructions à usage commercial ou abritant des activités de services ou d'autres activités du secteur secondaire ou tertiaire. En jugeant que les modifications ainsi apportées à la suite de la recommandation du commissaire enquêteur devaient être regardées comme procédant de l'enquête publique, alors même, d'une part, que cette recommandation n'avait pas donné lieu à des observations préalables du public et que, d'autre part, la modification apportée, sans être dépourvue de lien avec la recommandation faite, a été au-delà de ce qui avait été recommandé par le commissaire enquêteur, la cour administrative d'appel n'a pas commis d'erreur de droit.<br/>
<br/>
              En ce qui concerne les autres moyens : <br/>
<br/>
              5. En premier lieu, selon l'article L. 121-10 du code de l'urbanisme, devenu l'article 104-2, applicable au litige, font l'objet d'une évaluation environnementale, dans les conditions prévues par la directive 2001/42/CE du 27 juin 2001, " les plans locaux d'urbanisme (...) qui sont susceptibles d'avoir des effets notables sur l'environnement " au sens de l'annexe II à la directive " compte tenu notamment de la superficie du territoire auquel ils s'appliquent, de la nature et de l'importance des travaux et aménagements qu'ils autorisent et de la sensibilité du milieu dans lequel ceux-ci doivent être réalisés ". La cour a relevé que les modifications apportées au plan local d'urbanisme autorisaient l'édification d'annexes aux habitations existantes en zones agricoles et naturelles, qu'elles assouplissaient les règles d'excavation, qu'elles portaient de 6 mètres à 6,5 mètres la hauteur maximale des constructions, qu'elles réduisaient la distance minimale entre les murs de soutènement en zone agricole et qu'elles dispensaient de règles de hauteur les équipements publics dans les zones naturelles. Elle n'a pas commis d'erreur de droit en déduisant de la nature et de l'ampleur de ces modifications qu'elles n'étaient pas susceptibles d'avoir des effets notables sur l'environnement et que les requérants n'étaient dès lors pas fondés à soutenir que la commune aurait dû les faire précéder d'une évaluation environnementale. <br/>
<br/>
              6. En deuxième lieu, la cour a également relevé que, d'une part, la modification du plan local d'urbanisme en litige introduisait dans le règlement un article 14 prenant en compte une coupure d'urbanisation, prévue par le schéma de cohérence territoriale des cantons de Grimaud et de Saint-Tropez, dans laquelle toute extension de l'urbanisation, même limitée, est interdite et, d'autre part, que le plan d'aménagement et de développement durables du plan de Grimaud comportait une orientation consacrée au réaménagement de la façade littorale de la commune, impliquant la réalisation d'un certain nombre d'aménagements, tels qu'un ponton destiné aux liaisons maritimes, une promenade de bord de mer et des aires de stationnement. En jugeant que ces aménagements, dont une partie se situait en dehors de la zone de coupure d'urbanisation et en jonction avec le secteur urbanisé de Port-Grimaud, n'étaient pas de nature à entraîner une densification significative de l'urbanisation telle qu'elle pourrait être regardée comme une extension de l'urbanisation au sens de l'article 14, la cour a porté sur les faits et pièces du dossier une appréciation souveraine, exempte de dénaturation. En en déduisant que la modification apportée au plan local d'urbanisme sur ce point n'était pas incohérente avec l'orientation du plan d'aménagement et de développement durables relative au réaménagement de la façade littorale, de sorte que la commune n'avait pas à mettre en oeuvre la procédure de révision prévue à l'article L. 153-31 du code de l'urbanisme, la cour n'a pas davantage commis d'erreur de droit.<br/>
<br/>
              7. En troisième lieu, si les requérants font valoir que la cour aurait dû, pour écarter le moyen tiré de ce que la création de l'emplacement réservé n° 96 était entachée d'un détournement de procédure, rechercher si l'inclusion de certaines voies du domaine de Beauvallon avait pour effet de priver leurs propriétaires du droit d'usage de ces voies, il ressort des pièces du dossier soumis aux juges du fond que la création de l'emplacement réservé n° 96 n'a eu ni pour objet ni pour effet de remettre en cause l'affectation des voies à la circulation publique. Le moyen ne peut par suite qu'être écarté. <br/>
<br/>
              8. Il résulte de tout ce qui précède que les requérants ne sont pas fondés à demander l'annulation de l'arrêt qu'ils attaquent.<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              9. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que la somme que les requérants réclament soit mise à la charge de la commune de Grimaud, qui n'est pas la partie perdante dans la présente instance. Il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de l'association requérante la somme de 1 500 euros à verser à la commune de Grimaud, au titre des mêmes dispositions. Il y a lieu, en outre, de mettre une même somme de 1 500 euros à la charge de M. et Mme A... au titre des frais exposés par la commune.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de l'association syndicale autorisée des propriétaires du domaine de Beauvallon et autres est rejeté.<br/>
Article 2 : L'association syndicale autorisée des propriétaires du domaine de Beauvallon, d'une part, M. et Mme A..., d'autre part, verseront à la commune de Grimaud une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à l'association syndicale autorisée des propriétaires du domaine de Beauvallon, à M. B... A... et Mme C... A... et à la commune de Grimaud.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-01-01-01-01-05 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PLANS D'AMÉNAGEMENT ET D'URBANISME. PLANS D`OCCUPATION DES SOLS (POS) ET PLANS LOCAUX D'URBANISME (PLU). LÉGALITÉ DES PLANS. PROCÉDURE D'ÉLABORATION. ENQUÊTE PUBLIQUE. - POSSIBILITÉ DE MODIFIER LE PLU APRÈS ENQUÊTE PUBLIQUE (ART. L. 153-43 DU CODE DE L'URBANISME) SOUS RÉSERVE QUE LA MODIFICATION PROCÈDE DE L'ENQUÊTE ET QU'ELLE NE REMETTE PAS EN CAUSE L'ÉCONOMIE GÉNÉRALE DU PLAN [RJ1] - NOTION DE MODIFICATION PROCÉDANT DE L'ENQUÊTE - INCLUSION EN L'ESPÈCE.
</SCT>
<ANA ID="9A"> 68-01-01-01-01-05 Il résulte de l'article L. 153-43 du code de l'urbanisme que le projet de plan local d'urbanisme (PLU) ne peut subir de modifications, entre la date de sa soumission à l'enquête publique et celle de son approbation, qu'à la double condition que ces modifications ne remettent pas en cause l'économie générale du projet et qu'elles procèdent de l'enquête. Doivent être regardées comme procédant de l'enquête les modifications destinées à tenir compte des réserves et recommandations du commissaire ou de la commission d'enquête, des observations du public et des avis émis par les autorités, collectivités et instances consultées et joints au dossier de l'enquête.,,,Commissaire enquêteur ayant recommandé, à la suite d'observations du public portant sur le caractère imprécis de la notion de surface minéralisée, dans un souci de clarté et afin d'éviter tout éventuel litige relatif à une interprétation erronée du texte, de revoir la rédaction de certains articles du projet de règlement de PLU relatifs aux espaces libres et plantations. Collectivité ayant, sur la base de cette recommandation et postérieurement à l'enquête publique, modifié ces articles en vue de dispenser du respect des règles relatives aux espaces libres de plantations certaines constructions à usage commercial ou abritant des activités de services ou d'autres activités du secteur secondaire ou tertiaire.... ,,Les modifications ainsi apportées à la suite de la recommandation du commissaire enquêteur doivent être regardées comme procédant de l'enquête publique, alors même, d'une part, que cette recommandation n'avait pas donné lieu à des observations préalables du public et que, d'autre part, la modification apportée, sans être dépourvue de lien avec la recommandation faite, a été au-delà de ce qui avait été recommandé par le commissaire enquêteur.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>RJ0 Cf. CE, 12 mars 2010, Lille métropole communauté urbaine, n° 312108, T. p. 1012.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
