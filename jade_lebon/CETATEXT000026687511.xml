<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026687511</ID>
<ANCIEN_ID>JG_L_2012_11_000000356105</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/68/75/CETATEXT000026687511.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 26/11/2012, 356105, Publié au recueil Lebon</TITRE>
<DATE_DEC>2012-11-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>356105</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Marc Perrin de Brichambaut</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:356105.20121126</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 24 janvier 2012 au secrétariat du contentieux, présentée par M. Sidy B, demeurant ... ; M. B demande au Conseil d'Etat d'annuler pour excès de pouvoir la décision du 11 janvier 2012 par laquelle le ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration a refusé de modifier le décret du 10 mai 2006 lui accordant la nationalité française pour y porter le nom de l'enfant Mouhamed né le 5 novembre 2004 ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code civil ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Perrin de Brichambaut, Conseiller d'Etat,<br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, Rapporteur public,<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article 22-1 du code civil : " L'enfant mineur dont l'un des deux parents acquiert la nationalité française, devient français de plein droit s'il a la même résidence habituelle que ce parent ou s'il réside alternativement avec ce parent dans le cas de séparation ou divorce. / Les dispositions du présent article ne sont applicables à l'enfant d'une personne qui acquiert la nationalité française par décision de l'autorité publique ou par déclaration de nationalité que si son nom est mentionné dans le décret ou dans la déclaration. " ;<br/>
<br/>
              2. Considérant que M. B a acquis la nationalité française par l'effet d'un décret du 10 mai 2006 ; qu'il a demandé, par lettre du 28 septembre 2011, la modification de ce décret pour faire bénéficier l'enfant Mouhamed, qui est né le 5 novembre 2004 et qu'il a reconnu le 26 janvier 2011, de la nationalité française en conséquence de sa naturalisation ; qu'il a formé devant le Conseil d'Etat un recours pour excès de pouvoir contre la décision du 11 janvier 2012 par laquelle le ministre chargé des naturalisations a refusé la modification du décret du 10 mai 2006 pour y porter mention du nom de l'enfant ; <br/>
<br/>
              3. Considérant qu'en vertu de l'article R. 311-1 du code de justice administrative, le Conseil d'Etat est compétent pour connaître en premier et dernier ressort des recours dirigés contre les décrets ; qu'un recours dirigé contre une décision par laquelle il a été refusé de modifier ou de compléter un décret, que ce décret présente ou non un caractère réglementaire, est en réalité dirigé contre le décret et relève, en conséquence, de la compétence de premier ressort du Conseil d'Etat ; <br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier et qu'il n'est pas contesté que M. B n'a pas porté à la connaissance de l'administration la naissance de l'enfant Mouhamed, qu'il n'a reconnu que postérieurement à la signature du décret du 10 mai 2006 lui accordant la nationalité française ; que, si M. B soutient qu'il était dans l'impossibilité de déclarer cet enfant avant l'intervention du décret parce qu'il ne connaissait pas alors son existence, cette circonstance, qui implique que l'enfant ne résidait pas habituellement avec lui à la date du décret, n'est, en tout état de cause, pas de nature à entacher d'illégalité la décision attaquée ; que, par suite, M. B n'est pas fondé à demander l'annulation pour excès de pouvoir de la décision du 11 janvier 2012 par laquelle le ministre chargé des naturalisations a refusé de faire droit à sa demande de modification du décret du 10 mai 2006 et de faire bénéficier l'enfant Mouhamed de l'effet collectif attaché à l'acquisition de la nationalité française ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de M. B est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. Sidy B et au ministre de l'intérieur.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-05-02-01 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE DU CONSEIL D'ETAT EN PREMIER ET DERNIER RESSORT. DÉCRETS RÉGLEMENTAIRES OU INDIVIDUELS. - RECOURS CONTRE LE REFUS DE MODIFIER OU DE COMPLÉTER UN DÉCRET - INCLUSION, QUE LE DÉCRET PRÉSENTE OU NON UN CARACTÈRE RÉGLEMENTAIRE [RJ1].
</SCT>
<ANA ID="9A"> 17-05-02-01 Un recours dirigé contre une décision par laquelle il a été refusé de modifier ou de compléter un décret, que ce décret présente ou non un caractère réglementaire, est en réalité une action dirigée contre le décret et relève, en conséquence, de la compétence de premier ressort du Conseil d'Etat en vertu de l'article R. 311-1 du code de justice administrative.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 17 février 1965, Commune de Saint-Hippolyte, n°s 63110 63111, p. 111. Comp., s'agissant du refus non pas de modifier ou de compléter, mais d'édicter un décret non réglementaire, CE, 5 juillet 2006, Association Tchernoblaye, n° 272741, inédite au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
