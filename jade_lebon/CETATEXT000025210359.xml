<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025210359</ID>
<ANCIEN_ID>JG_L_2012_01_000000344705</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/21/03/CETATEXT000025210359.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 25/01/2012, 344705</TITRE>
<DATE_DEC>2012-01-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>344705</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Jacques Arrighi de Casanova</PRESIDENT>
<AVOCATS>SCP DE CHAISEMARTIN, COURJON ; HAAS</AVOCATS>
<RAPPORTEUR>M. Pascal Trouilly</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Claire Landais</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:344705.20120125</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 3 décembre 2010 et 25 février 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. Jean-Claude A, demeurant ... ; M. A demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 09MA03415 du 22 mars 2010 par laquelle le président de la 1ère chambre de la cour administrative d'appel de Marseille a rejeté sa requête tendant à l'annulation du jugement n° 0806989 du 11 juin 2009 du tribunal administratif de Marseille rejetant sa demande d'annulation de la décision implicite par laquelle le maire de Salon-de-Provence ne s'est pas opposé aux travaux déclarés par la SCI des Grands Prés le 25 février 2008 ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'urbanisme ; <br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ; <br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pascal Trouilly, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP de Chaisemartin, Courjon, avocat de M. A et de Me Haas, avocat de la commune de Salon-de-Provence, <br/>
<br/>
              - les conclusions de Mme Claire Landais, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP de Chaisemartin, Courjon, avocat de M. A et à Me Haas, avocat de la commune de Salon-de-Provence ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant qu'en vertu des dispositions combinées des articles R. 222-13 et R. 811-1 du code de justice administrative, le tribunal administratif statue en dernier ressort dans les litiges relatifs aux déclarations de travaux exemptés de permis de construire ; qu'ainsi, les jugements rendus par les tribunaux administratifs dans cette matière ne peuvent faire l'objet que d'un pourvoi en cassation devant le Conseil d'Etat ; que, toutefois, aux termes de l'article R. 351-4 du code de justice administrative : " Lorsque tout ou partie des conclusions dont est saisi un tribunal administratif, une cour administrative d'appel ou le Conseil d'Etat relève de la compétence d'une juridiction administrative, le tribunal administratif, la cour administrative d'appel ou le Conseil d'Etat, selon le cas, est compétent, nonobstant les règles de répartition des compétences entre juridictions administratives, pour rejeter les conclusions entachées d'une irrecevabilité manifeste insusceptible d'être couverte en cours d'instance ou pour constater qu'il n'y a pas lieu de statuer sur tout ou partie des conclusions " ; <br/>
<br/>
              Considérant que l'irrecevabilité manifeste insusceptible d'être couverte en cours d'instance mentionnée à l'article R. 351-4 du code de justice administrative ne peut concerner, s'agissant d'une requête présentée devant une cour administrative d'appel, que les conclusions présentées devant cette cour ; qu'ainsi, en l'absence d'une telle irrecevabilité manifeste, ces dispositions ne permettent pas à une cour administrative d'appel de s'affranchir, au motif de l'irrecevabilité, même manifeste, de la demande de première instance, du respect des règles de répartition des compétences entre juridictions administratives ;<br/>
<br/>
              Considérant que, pour se reconnaître compétent, malgré les dispositions combinées des articles R. 222-13 et R. 811-1 du code de justice administrative, le président de la 1ère chambre de la cour administrative d'appel de Marseille s'est fondé sur l'irrecevabilité manifeste de la demande de première instance de M. A ; qu'il résulte de ce qui vient d'être dit qu'il a, ce faisant, fait une inexacte application des dispositions de l'article R. 351-4 du même code et que, statuant sur des conclusions dirigées contre le jugement du 11 juin 2009 rendu par le tribunal administratif de Marseille dans un litige de déclaration de travaux, il a méconnu l'étendue de sa compétence ; que, dès lors, son ordonnance doit être annulée ; <br/>
<br/>
              Considérant qu'il y a lieu de regarder les conclusions présentées par M. A comme des conclusions de cassation dirigées contre le jugement du tribunal administratif de Marseille statuant en premier et dernier ressort ;<br/>
<br/>
              Considérant que, pour rejeter comme irrecevable la demande de M. A tendant à l'annulation pour excès de pouvoir de la décision implicite par laquelle le maire de Salon-de-Provence ne s'est pas opposé aux travaux déclarés par la SCI des Grands Prés, le tribunal administratif de Marseille a notamment retenu que le requérant ne justifiait pas d'un intérêt lui donnant qualité pour agir ; <br/>
<br/>
              Considérant qu'en déniant à M. A un intérêt pour agir aux motifs que celui-ci n'avait pas de vue directe sur le bâtiment faisant l'objet des travaux déclarés et que le terrain d'assiette de ce bâtiment était séparé de sa propre parcelle par une parcelle construite, le tribunal administratif a porté sur la configuration des lieux une appréciation souveraine exempte de dénaturation et n'a pas commis d'erreur de droit dans l'utilisation des critères qui gouvernent l'appréciation de l'intérêt pour agir contre une autorisation individuelle d'urbanisme ; <br/>
<br/>
              Considérant qu'un tel motif, par lequel les juges du fond ont ainsi constaté que la demande de première instance était irrecevable, justifiait nécessairement, à lui seul, le dispositif de rejet de cette demande ; qu'il suit de là qu'est inopérant le moyen tiré de ce que l'autre motif d'irrecevabilité relevé par le tribunal administratif serait erroné ; <br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que M. A n'est pas fondé à demander l'annulation du jugement du 11 juin 2009 du tribunal administratif de Marseille ; que les conclusions présentées par son avocat au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ne peuvent, en conséquence, qu'être rejetées ; qu'enfin, il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la commune de Salon-de-Provence au titre des mêmes dispositions ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 22 mars 2010 du président de la 1ère chambre de la cour administrative d'appel de Marseille est annulée.<br/>
Article 2 : Le pourvoi de M. A dirigé contre le jugement du 11 juin 2009 du tribunal administratif de Marseille est rejeté.<br/>
Article 3 : Les conclusions de la commune de Salon-de-Provence présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à M. Jean-Claude A et à la commune de Salon-de-Provence. <br/>
Copie en sera adressée pour information à la SCI Les Grands Prés.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-05-015 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE D'APPEL DES COURS ADMINISTRATIVES D'APPEL. - 1) IRRECEVABILITÉ MANIFESTE INSUSCEPTIBLE D'ÊTRE COUVERTE EN COURS D'INSTANCE (ART. R. 351-4 DU CJA) - COUR ADMINISTRATIVE D'APPEL - CONCLUSIONS PRÉSENTÉES DEVANT CETTE COUR - INCLUSION - CONCLUSIONS DE LA DEMANDE DE PREMIÈRE INSTANCE - EXCLUSION - CONSÉQUENCE - AFFRANCHISSEMENT D'UNE COUR DU RESPECT DES RÈGLES DE RÉPARTITION DES COMPÉTENCES ENTRE JURIDICTIONS ADMINISTRATIVES AU MOTIF DE L'IRRECEVABILITÉ MANIFESTE DE LA DEMANDE DE PREMIÈRE INSTANCE - ABSENCE - 2) APPLICATION DE CE PRINCIPE EN L'ESPÈCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-08-02 PROCÉDURE. VOIES DE RECOURS. CASSATION. - JUGEMENT REJETANT POUR IRRECEVABILITÉ LA DEMANDE DE PREMIÈRE INSTANCE - MOTIFS MULTIPLES D'IRRECEVABILITÉ RETENUS PAR LES JUGES DU FOND - CONSTAT DU CARACTÈRE FONDÉ DU MOTIF DE DÉFAUT D'INTÉRÊT POUR AGIR - CONSÉQUENCE - INOPÉRANCE DU MOYEN TIRÉ DE CE QUE L'AUTRE MOTIF D'IRRECEVABILITÉ RELEVÉ PAR LES JUGES DU FOND SERAIT ERRONÉ [RJ1].
</SCT>
<ANA ID="9A"> 17-05-015 1) L'irrecevabilité manifeste insusceptible d'être couverte en cours d'instance mentionnée à l'article R. 351-4 du code de justice administrative (CJA) ne peut concerner, s'agissant d'une requête présentée devant une cour administrative d'appel, que les conclusions présentées devant cette cour. Ainsi, en l'absence d'une telle irrecevabilité manifeste, ces dispositions ne permettent pas à une cour administrative d'appel de s'affranchir, au motif de l'irrecevabilité, même manifeste, de la demande de première instance, du respect des règles de répartition des compétences entre juridictions administratives.,,2) Dès lors, en se fondant sur l'irrecevabilité manifeste de la demande de première instance pour se reconnaître compétente, malgré les dispositions combinées des articles R. 222-13 et R. 811-1 du CJA, pour statuer sur des conclusions dirigées contre un jugement rendu par le tribunal administratif dans un litige de déclaration de travaux, une cour administrative d'appel fait une inexacte application des dispositions de l'article R. 351-4 du même code et méconnaît l'étendue de sa compétence.</ANA>
<ANA ID="9B"> 54-08-02 Le motif de défaut d'intérêt pour agir, par lequel les juges du fond ont constaté que la demande de première instance était irrecevable, justifie nécessairement, à lui seul, le dispositif de rejet de cette demande. Par suite, le moyen tiré de ce que l'autre motif d'irrecevabilité relevé par le tribunal administratif serait erroné est inopérant.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 27 octobre 2011, Association de défense des victimes de l'amiante de Cherbourg, n° 338882, à publier au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
