<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037113484</ID>
<ANCIEN_ID>JG_L_2018_06_000000405783</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/11/34/CETATEXT000037113484.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 27/06/2018, 405783</TITRE>
<DATE_DEC>2018-06-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>405783</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Sylvain Monteillet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:405783.20180627</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme C...B...-A... a demandé au tribunal administratif de Versailles d'annuler : <br/>
<br/>
              1°) les arrêtés n° 2009-3033 et n° 2009-3034 du 19 octobre 2009 par lesquels le président du conseil d'administration du service départemental d'incendie et de secours (SDIS) des Yvelines a respectivement procédé à sa nomination en qualité d'adjoint administratif territorial de 1ère classe stagiaire, à compter du 15 septembre 2007, en la classant au 3ème échelon du grade d'adjoint administratif territorial de 1ère classe avec une ancienneté conservée de 1 an 2 mois et 13 jours et à sa titularisation à compter du 15 septembre 2008 au 4ème échelon du même grade avec une ancienneté conservée de 2 mois et 13 jours ; <br/>
<br/>
              2°) l'arrêté n° 2010-4343 du 5 août 2010 portant avancement au 5ème échelon du grade d'adjoint administratif territorial de 1ère classe ; <br/>
<br/>
              3°) l'arrêté n° 2012-982 du 5 avril 2012 lui attribuant un régime indemnitaire correspondant au grade d'adjoint administratif territorial de 1ère classe ;<br/>
<br/>
              4°) l'arrêté n° 2012-2665 du 26 juin 2012 la nommant au 6ème échelon du grade d'adjoint administratif territorial de 1ère classe ;<br/>
<br/>
              5°) l'arrêté n° 2014-1416 du 31 mars 2014 portant reclassement indiciaire à compter du 1er février 2014.<br/>
<br/>
              Par un jugement n° 1002016, 1203807, 1404590 du 25 novembre 2014, le tribunal administratif de Versailles a rejeté ses demandes. <br/>
<br/>
              Par un arrêt n° 15VE00311 du 6 octobre 2016, la cour administrative d'appel de Versailles a rejeté l'appel formé par Mme B...-A... contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 7 décembre 2016 et 7 mars 2017 au secrétariat du contentieux du Conseil d'Etat, Mme B... -A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge du SDIS des Yvelines la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le décret n° 87-1107 du 30 décembre 1987 ;<br/>
              - le décret n° 2003-673 du 22 juillet 2003 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Sylvain Monteillet, maître des requêtes en service extraordinaire, <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de Mme B...-A... et à la SCP Foussard, Froger, avocat du service départemental d'incendie et de secours des Yvelines (SDIS) ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme B...-A..., ressortissante italienne, a été employée par l'Institut national des assurances contre les accidents du travail et les maladies professionnelles italien de 1993 à 2001. Après avoir été admise au concours externe de recrutement dans le cadre d'emplois d'adjoint administratif territorial en décembre 2006, elle a été nommée adjointe administrative territoriale de 1ère classe stagiaire à compter du 15 septembre 2007 puis titularisée à compter du 15 septembre 2008. Au vu des justificatifs produits par l'intéressée relatifs à sa situation antérieure et après un avis du 30 avril 2009 de la commission d'équivalence pour le classement des ressortissants de la Communauté européenne, le président du conseil d'administration du service départemental d'incendie et de secours (SDIS) des Yvelines a modifié, par deux arrêtés du 19 octobre 2009, ses modalités de classement dans le cadre d'emploi des adjoints administratifs territoriaux, avec une reprise d'ancienneté de travail égale à la moitié de la durée de son travail au sein de l'Institut national des assurances contre les accidents du travail et les maladies professionnelles. Mme B... A...a demandé au tribunal administratif de Versailles d'annuler ces deux arrêtés du 19 octobre 2009, ainsi que l'arrêté du 5 août 2010 portant avancement au 5ème échelon du grade d'adjoint administratif territorial de 1ère classe, l'arrêté du 5 avril 2012 lui attribuant un régime indemnitaire correspondant au grade d'adjoint administratif territorial de 1ère classe, l'arrêté du 26 juin 2012 la nommant au 6ème échelon du grade d'adjoint administratif territorial de 1ère classe et, enfin, l'arrêté du 31 mars 2014 portant reclassement indiciaire à compter du 1er février 2014. Par un jugement du 25 novembre 2014, le tribunal administratif de Versailles a rejeté sa demande. Mme B...-A... se pourvoit en cassation contre l'arrêt du 6 octobre 2016 par lequel la cour administrative d'appel de Versailles a rejeté l'appel qu'elle a formé à l'encontre de ce jugement.<br/>
<br/>
              2. Aux termes, d'une part, de l'article 1er du décret du 22 juillet 2003 fixant les dispositions générales relatives à la situation et aux modalités de classement des ressortissants des Etats membres de la Communauté européenne ou d'un autre Etat partie à l'accord sur l'Espace économique européen nommés dans un cadre d'emplois de la fonction publique territoriale, décret alors en vigueur à la date des arrêtés attaqués et dont la teneur a été reprise par le décret n° 2010-311 du 22 mars 2010 : " Les ressortissants des Etats membres de la Communauté européenne ou d'un autre Etat partie à l'accord sur l'Espace économique européen autres que la France nommés dans un cadre d'emplois de fonctionnaires territoriaux sont régis par les dispositions statutaires du cadre d'emplois dans les mêmes conditions que les fonctionnaires français ". Aux termes de l'article 4 du même décret : " Les dispositions du présent titre s'appliquent aux ressortissants des Etats membres de la Communauté européenne ou d'un autre Etat partie à l'accord sur l'Espace économique européen qui justifient de services accomplis dans une administration, un organisme ou un établissement de l'Etat membre d'origine dont les missions sont comparables à celles des administrations et des établissements publics dans lesquels les fonctionnaires civils mentionnés à l'article 2 de la loi du 13 juillet 1983 susvisée exercent leurs fonctions (...) ". Aux termes de l'article 5 du même décret : " Lors de leur première nomination dans un cadre d'emplois de fonctionnaires territoriaux, les agents mentionnés à l'article 4 sont classés selon les règles de prise en compte des services antérieurs fixées par les dispositions statutaires régissant le cadre d'emplois d'accueil, à l'exception de toute disposition prévoyant le maintien, à titre individuel, du niveau de rémunération atteint avant leur accès à la fonction publique française ". Aux termes de l'article 6 du même décret : " Les modalités de prise en compte des services accomplis sont déterminées au regard de la nature juridique de l'engagement qui lie l'agent à son employeur en application des textes régissant le personnel de l'administration, de l'organisme et de l'établissement dans l'Etat membre d'origine. / La détermination de la nature juridique de l'engagement s'effectue comme suit : (...) / 3° Lorsque, dans l'administration, l'organisme ou l'établissement de l'Etat membre d'origine, le personnel est normalement régi par les stipulations d'un contrat de travail de droit privé : / a) L'agent qui justifie d'un contrat de travail de droit privé à durée indéterminée ou à durée déterminée renouvelable sans limite est classé selon les règles fixées par les dispositions statutaires régissant le cadre d'emplois d'accueil applicables aux fonctionnaires ; / b) L'agent qui justifie d'un contrat de travail de droit privé à durée déterminée renouvelable dans une limite maximale est classé selon les règles fixées par les dispositions statutaires régissant le cadre d'emplois d'accueil applicables aux agents non titulaires de droit public ".<br/>
<br/>
              3. Aux termes, d'autre part, de l'article 5 du décret du 30 décembre 1987 portant organisation des carrières des fonctionnaires territoriaux de catégorie C, dans sa rédaction applicable au litige : " I. - Les fonctionnaires de catégorie C relevant de grades dotés des échelles de rémunération 3, 4 et 5 qui sont classés par application des règles statutaires à l'un des grades ou emplois relevant des mêmes échelles sont maintenus dans leur nouveau grade à l'échelon dans lequel ils étaient parvenus dans leur précédent grade. / Les intéressés conservent, dans la limite de la durée maximale de service exigée pour l'accès à l'échelon supérieur du nouveau grade, l'ancienneté d'échelon qu'ils avaient acquise dans leur grade antérieur (...) ". Aux termes de l'article 6 du même décret, dans sa rédaction applicable au litige : " I. - Les autres fonctionnaires nommés à l'un des grades dotés des échelles de rémunération 3, 4 ou 5 qui relevaient antérieurement de grades ou emplois dotés d'une échelle indiciaire différente sont classés dans leur nouveau grade à un échelon doté d'un indice égal ou immédiatement supérieur à celui qu'ils détenaient dans leur situation antérieure. Toutefois, ils conservent, à titre personnel, l'indice qu'ils détenaient dans leur précédente situation si celui-ci est plus élevé que l'indice servi au dernier échelon du grade dans lequel ils sont nommés, dans la limite de l'indice correspondant à l'échelon le plus élevé du cadre d'emplois de catégorie C dans lequel ils sont intégrés. / Les intéressés conservent, dans la limite de la durée maximale de service exigée pour l'accès à l'échelon supérieur du nouveau grade, l'ancienneté d'échelon qu'ils avaient acquise dans leur grade antérieur (...) ". Aux termes de l'article 6-2 du même décret : " Les personnes nommées fonctionnaires dans un grade de catégorie C doté des échelles de rémunération 3, 4 ou 5 qui ont, ou qui avaient eu auparavant, la qualité d'agent de droit privé d'une administration, ou qui travaillent ou ont travaillé en qualité de salarié dans le secteur privé ou associatif sont classées avec une reprise d'ancienneté de travail égale à la moitié de sa durée, le cas échéant après calcul de conversion en équivalent temps plein. Ce classement est opéré sur la base de la durée maximale de chacun des échelons du grade dans lequel ils sont intégrés ".<br/>
<br/>
              4. Afin de procéder au classement des ressortissants concernés des Etats membres de l'Union européenne ou d'un autre Etat partie à l'accord sur l'Espace économique européen, lors de leur première nomination dans un cadre d'emplois de fonctionnaires territoriaux, l'article 5 du décret du 22 juillet 2003 précité prévoit que les services précédemment accomplis sont pris en compte en appliquant les règles de classement fixées par les dispositions statutaires régissant le cadre d'emplois d'accueil. Pour déterminer celles de ces règles qui sont applicables à un agent donné, l'article 6 du même décret établit un système d'équivalence à partir de la nature juridique de l'engagement antérieur de celui-ci. Ainsi, en vertu des dispositions du 3° de cet article, lorsque le personnel de l'administration à laquelle il appartenait est normalement régi par les stipulations d'un contrat de travail de droit privé, les services accomplis sont pris en compte en mettant en oeuvre les règles applicables aux fonctionnaires dans le cadre d'emplois d'accueil dès lors que l'agent justifie d'un contrat de travail de droit privé à durée indéterminée ou à durée déterminée renouvelable sans limite.<br/>
<br/>
              5. Il ressort des pièces du dossier soumis aux juges du fond que le personnel de l'Institut national des assurances contre les accidents du travail et les maladies professionnelles italien est normalement régi par les stipulations d'un contrat de travail de droit privé. Après avoir relevé que la situation de Mme B...-A... était ainsi régie par les dispositions du a) du 3° de l'article 6 du décret du 22 juillet 2003, la cour administrative d'appel a jugé que le président du conseil d'administration du SDIS des Yvelines avait pu appliquer à celle-ci les dispositions de l'article 6-2 du décret du 30 décembre 1987, relatives au classement des personnes ayant auparavant la qualité d'agent de droit privé ou de salarié de droit privé. En statuant ainsi, alors que les dispositions du a) du 3° de l'article 6 du décret du 22 juillet 2003 impliquaient, au cas d'espèce, d'appliquer les règles fixées par les dispositions statutaires applicables aux fonctionnaires, à savoir celles des articles 5 et 6 du décret du 30 décembre 1987, la cour a commis une erreur de droit.<br/>
<br/>
              6. Il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, Mme B...-A... est fondée à demander l'annulation de l'arrêt de la cour administrative d'appel de Versailles qu'elle attaque. <br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du SDIS des Yvelines le versement à Mme B...-A... d'une somme globale de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font obstacle à ce qu'une somme soit mise à la charge de Mme B...-A..., qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt n° 15VE00311 de la cour administrative de Versailles du 6 octobre 2016 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Versailles. <br/>
Article 3 : Le service départemental d'incendie et de secours des Yvelines versera à Mme B... -A... une somme de 3 000 euros, au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions du service départemental d'incendie et de secours des Yvelines présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à Mme C...B...-A... et au service départemental d'incendie et de secours des Yvelines.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-04-04 FONCTIONNAIRES ET AGENTS PUBLICS. CHANGEMENT DE CADRES, RECLASSEMENTS, INTÉGRATIONS. INTÉGRATION DE PERSONNELS N'APPARTENANT PAS ANTÉRIEUREMENT À LA FONCTION PUBLIQUE. - MODALITÉS DE CLASSEMENT DES RESSORTISSANT DES ETATS MEMBRES DE L'UE OU DE L'EEE NOMMÉS DANS UN CADRE D'EMPLOI DE LA FPT - DÉTERMINATION DES RÈGLES APPLICABLES À LA PRISE EN COMPTE DES SERVICES PRÉCÉDEMMENT ACCOMPLIS EN FONCTION DE LA NATURE JURIDIQUE DE L'ENGAGEMENT ANTÉRIEUR DE L'INTÉRESSÉ (ART. 6 DU DÉCRET N° 2003-673) - CAS D'UN RESSORTISSANT RÉGI PAR LES STIPULATIONS D'UN CONTRAT DE TRAVAIL DE DROIT PRIVÉ - RÈGLES APPLICABLES AUX FONCTIONNAIRES DANS LE CADRE D'EMPLOI D'ACCUEIL, DÈS LORS QUE L'AGENT JUSTIFIE D'UN CONTRAT DE TRAVAIL DE DROIT PRIVÉ À DURÉE INDÉTERMINÉE OU À DURÉE DÉTERMINÉE RENOUVELABLE SANS LIMITE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-07-01-03 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. STATUT GÉNÉRAL DES FONCTIONNAIRES DE L'ÉTAT ET DES COLLECTIVITÉS LOCALES. DISPOSITIONS STATUTAIRES RELATIVES À LA FONCTION PUBLIQUE TERRITORIALE (LOI DU 26 JANVIER 1984). - MODALITÉS DE CLASSEMENT DES RESSORTISSANT DES ETATS MEMBRES DE L'UE OU DE L'EEE NOMMÉS DANS UN CADRE D'EMPLOI - DÉTERMINATION DES RÈGLES APPLICABLES À LA PRISE EN COMPTE DES SERVICES PRÉCÉDEMMENT ACCOMPLIS EN FONCTION DE LA NATURE JURIDIQUE DE L'ENGAGEMENT ANTÉRIEUR DE L'INTÉRESSÉ (ART. 6 DU DÉCRET N° 2003-673) - CAS D'UN RESSORTISSANT RÉGI PAR LES STIPULATIONS D'UN CONTRAT DE TRAVAIL DE DROIT PRIVÉ - RÈGLES APPLICABLES AUX FONCTIONNAIRES DANS LE CADRE D'EMPLOI D'ACCUEIL, DÈS LORS QUE L'AGENT JUSTIFIE D'UN CONTRAT DE TRAVAIL DE DROIT PRIVÉ À DURÉE INDÉTERMINÉE OU À DURÉE DÉTERMINÉE RENOUVELABLE SANS LIMITE.
</SCT>
<ANA ID="9A"> 36-04-04 Afin de procéder au classement des ressortissants concernés des Etats membres de l'Union européenne (UE) ou d'un autre Etat partie à l'accord sur l'Espace économique européen (EEE), lors de leur première nomination dans un cadre d'emplois de fonctionnaires territoriaux, l'article 5 du décret n° 2003-673 du 22 juillet 2003 prévoit que les services précédemment accomplis sont pris en compte en appliquant les règles de classement fixées par les dispositions statutaires régissant le cadre d'emplois d'accueil. Pour déterminer celles de ces règles qui sont applicables à un agent donné, l'article 6 du même décret établit un système d'équivalence à partir de la nature juridique de l'engagement antérieur de celui-ci. Ainsi, en vertu des dispositions du 3° de cet article, lorsque le personnel de l'administration à laquelle il appartenait est normalement régi par les stipulations d'un contrat de travail de droit privé, les services accomplis sont pris en compte en mettant en oeuvre les règles applicables aux fonctionnaires dans le cadre d'emplois d'accueil dès lors que l'agent justifie d'un contrat de travail de droit privé à durée indéterminée ou à durée déterminée renouvelable sans limite.</ANA>
<ANA ID="9B"> 36-07-01-03 Afin de procéder au classement des ressortissants concernés des Etats membres de l'Union européenne (UE) ou d'un autre Etat partie à l'accord sur l'Espace économique européen (EEE), lors de leur première nomination dans un cadre d'emplois de fonctionnaires territoriaux, l'article 5 du décret n° 2003-673 du 22 juillet 2003 prévoit que les services précédemment accomplis sont pris en compte en appliquant les règles de classement fixées par les dispositions statutaires régissant le cadre d'emplois d'accueil. Pour déterminer celles de ces règles qui sont applicables à un agent donné, l'article 6 du même décret établit un système d'équivalence à partir de la nature juridique de l'engagement antérieur de celui-ci. Ainsi, en vertu des dispositions du 3° de cet article, lorsque le personnel de l'administration à laquelle il appartenait est normalement régi par les stipulations d'un contrat de travail de droit privé, les services accomplis sont pris en compte en mettant en oeuvre les règles applicables aux fonctionnaires dans le cadre d'emplois d'accueil dès lors que l'agent justifie d'un contrat de travail de droit privé à durée indéterminée ou à durée déterminée renouvelable sans limite.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
