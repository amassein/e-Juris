<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036757738</ID>
<ANCIEN_ID>JG_L_2018_03_000000408156</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/75/77/CETATEXT000036757738.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 6ème chambres réunies, 30/03/2018, 408156</TITRE>
<DATE_DEC>2018-03-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>408156</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>M. Guillaume Leforestier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:408156.20180330</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° La société Casino de Blotzheim a demandé au tribunal administratif de Strasbourg d'annuler le courrier du 23 août 2013 par lequel le directeur de cabinet du ministre de l'intérieur a demandé au directeur du casino de veiller à ce qu'aucune machine à sous ne soit installée dans les espaces fumeurs, d'annuler le courrier du ministre de l'intérieur du 25   novembre 2013 confirmant les termes du courrier du 23 août 2013 et rejetant sa demande indemnitaire, et de condamner l'Etat à réparer le préjudice économique résultant de l'exécution de ces actes. Par un jugement n° 1400328 du 11 mars 2015, le tribunal administratif de Strasbourg a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 15NC00876 du 20 décembre 2016, la cour administrative d'appel de Nancy a, sur appel de la société Casino de Blotzheim, annulé ce jugement, annulé le courrier du 23 août 2013 et rejeté le surplus de ses conclusions.<br/>
<br/>
              Sous le n° 408156, par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 20 février, 22 mai et 21 décembre 2017 au secrétariat du contentieux du Conseil d'Etat, la société Casino de Blotzheim demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il rejette le surplus de ses conclusions d'appel ;<br/>
<br/>
              2°) réglant l'affaire au fond, d'annuler le courrier du ministre de l'intérieur du 25 novembre 2013 et de condamner l'Etat à lui verser la somme de 501 euros par jour de maintien de l'interdiction de fumer dans le fumoir de son établissement à compter du 26 août 2013 et jusqu'à la date de sa décision, assortie des intérêts au taux légal à compter du 25 novembre 2013 et de la capitalisation de ces intérêts ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              2° La société Amnéville Loisirs a demandé au tribunal administratif de Strasbourg d'annuler le courrier du 23 août 2013 par lequel le directeur du cabinet du ministre de l'intérieur a demandé au directeur du casino d'Amnéville de veiller à ce qu'aucune machine à sous ne soit installée dans les espaces fumeurs, ainsi que le courrier du ministre de l'intérieur du 25 novembre 2013 confirmant les termes du courrier du 23 août 2013 et rejetant sa demande indemnitaire. Elle a également demandé à ce tribunal de condamner l'Etat à réparer le préjudice économique résultant de l'exécution des actes contestés. Par un jugement n° 1400351 du 11 mars 2015, le tribunal administratif de Strasbourg a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 15NC000921 du 20 décembre 2016, la cour administrative d'appel de Nancy a, sur appel de la société Amnéville Loisirs, annulé ce jugement, annulé le courrier du 23 août 2013 et rejeté le surplus de ses conclusions.  <br/>
<br/>
              Sous le n° 408338, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 23 février et 24 mai 2017 au secrétariat du contentieux du Conseil d'Etat, la société Amnéville Loisirs demande au Conseil d'Etat d'annuler cet arrêt en tant qu'il rejette le surplus de ses conclusions d'appel.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le code de la santé publique ;<br/>
<br/>
              - l'arrêté du 14 mai 2007 relatif à la réglementation des jeux dans les casinos ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Leforestier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la société Casino de Blotzheim et à la SCP Gaschignard, avocat de la société Amnéville Loisirs.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces des dossiers soumis aux juges du fond que les casinos d'Amnéville et de Blotzheim ont ouvert en leur sein, respectivement le 22 décembre 2012 et le 15 juin 2013, des espaces fumeurs équipés de machines à sous ; que, par courriers du 23 août 2013, le directeur de cabinet du ministre de l'intérieur a demandé aux directeurs de ces sociétés de veiller à ce qu'aucune machine à sous ne soit installée dans ces espaces ; que les casinos de Blotzheim et d'Amnéville ont formé un recours gracieux contre ces courriers et demandé à être indemnisés des préjudices économiques qu'ils leur causaient ; que ces demandes ont été rejetées par des courriers du directeur des libertés publiques et des affaires juridiques du 25 novembre 2013 ; que, par deux jugements du 11 mars 2015, le tribunal administratif de Strasbourg a rejeté les conclusions des casinos de Blotzheim et d'Amnéville tendant à l'annulation des courriers des 23 août et 25 novembre 2013 et à l'indemnisation de leurs préjudices ; que, par deux arrêts du 20 décembre 2016, la cour administrative d'appel de Nancy a annulé ces jugements, annulé les courriers du 23 août 2013 et rejeté le surplus de leurs conclusions ; que les sociétés requérantes demandent que ces arrêts soient annulés en tant qu'ils rejettent le surplus de leurs conclusions ; qu'il y a lieu de joindre leurs pourvois pour statuer par une seule décision ;<br/>
<br/>
              2. Considérant, d'une part, qu'aux termes de l'article L. 3511-7 du code de la santé publique, dans sa rédaction à la date des décisions attaquées : " Il est interdit de fumer dans les lieux affectés à un usage collectif, notamment scolaire, et dans les moyens de transport collectif, sauf dans les emplacements expressément réservés aux fumeurs. / Un décret en Conseil d'Etat fixe les conditions d'application de l'alinéa précédent " ; qu'aux termes de l'article R. 3511-1 du même code, dans sa rédaction applicable à la même date : " L'interdiction de fumer dans les lieux affectés à un usage collectif mentionnée à l'article L. 3511-7 s'applique : / 1° Dans tous les lieux fermés et couverts qui accueillent du public ou qui constituent des lieux de travail ; ( ...) " ; qu'aux termes de l'article R. 3511-2 du même code, dans sa rédaction applicable à la même date : " L'interdiction de fumer ne s'applique pas dans les emplacements mis à la disposition des fumeurs au sein des lieux mentionnés à l'article R. 3511-1 et créés, le cas échéant, par la personne ou l'organisme responsable des lieux (...) " ; qu'aux termes de son article R. 3511-3, alors en vigueur : " Les emplacements réservés mentionnés à l'article R. 3511-2 sont des salles closes, affectées à la consommation de tabac et dans lesquelles aucune prestation de service n'est délivrée. Aucune tâche d'entretien et de maintenance ne peut y être exécutée sans que l'air ait été renouvelé, en l'absence de tout occupant, pendant au moins une heure (...) " ;<br/>
<br/>
              3. Considérant, d'autre part, qu'aux termes de l'article 68-27 de l'arrêté du 14 mai 2007 relatif à la réglementation des jeux dans les casinos : " Tout casino qui exploite les machines à sous dans un local distinct doit au moins employer dans cette salle un caissier et affecter un membre du comité de direction au contrôle de ces jeux. Il pourra également employer un mécanicien pour effectuer les opérations courantes d'entretien et de dépannage (...) " ; <br/>
<br/>
              4. Considérant qu'il résulte de ces dispositions que l'installation de machines à sous au sein d'un local clos implique nécessairement la présence dans ce local d'un caissier ainsi que l'intervention d'un membre du comité de direction en cas d'événement ou d'incident requérant sa présence ; qu'une telle situation est incompatible avec le respect des dispositions du code de la santé publique citées ci-dessus, qui excluent l'intervention de personnels dans les salles closes réservées aux fumeurs avant que l'air y ait été renouvelé, en l'absence de tout occupant, pendant au moins une heure ; qu'il en résulte qu'en jugeant qu'une salle close affectée exclusivement à la consommation de tabac, au sens de l'article R. 3511-3 du code de la santé publique précité, constituait nécessairement un local distinct, au sens de l'article 68-27 de l'arrêté du 14 mai 2007 relatif à la réglementation des jeux dans les casinos, et en en déduisant que l'installation de machines à sous dans une telle salle méconnaissait les dispositions du code de la santé publique, la cour administrative d'appel n'a pas commis d'erreur de droit ni inexactement qualifié les faits qui lui étaient soumis ; qu'au demeurant, les dispositions de l'article R. 3511-3 du code de la santé publique selon lesquelles aucune prestation de services n'est délivrée dans les emplacements réservés aux fumeurs font obstacle à ce qu'un casino permette à ses clients de bénéficier de prestations de jeux dans un tel emplacement ; que les pourvois dirigés contre les arrêts susvisés doivent, par suite, être rejetés ;<br/>
<br/>
              5. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que l'Etat, qui n'est pas la partie perdante, verse à la société Casino de Blotzheim la somme qu'elle demande à ce titre ; <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Les pourvois des sociétés Casino de Blotzheim et Amnéville Loisirs sont rejetés.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la société Amnéville Loisirs, à la société Casino de Blotzheim, au ministre d'Etat, ministre de l'intérieur et à la ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">61-03-06-01 SANTÉ PUBLIQUE. LUTTE CONTRE LES FLÉAUX SOCIAUX. LUTTE CONTRE LA TOXICOMANIE. LUTTE CONTRE LE TABAGISME. - POSSIBILITÉ D'INSTALLER DES MACHINES À SOUS DANS UNE SALLE CLOSE AFFECTÉE UNIQUEMENT À LA CONSOMMATION DE TABAC (ART. R. 3511-3 DU CSP) - ABSENCE, DÈS LORS QU'ELLE CONSTITUE UNE LOCAL DISTINCT AU SENS L'ARTICLE 68-27 DE L'ARRÊTÉ DU 14 MAI 2007.
</SCT>
<ANA ID="9A"> 61-03-06-01 Il résulte des articles L. 3511-7, R. 3511-1 et R. 3511-3 du code de la santé publique (CSP) alors en vigueur et de l'article 68-27 de l'arrêté du 14 mai 2007 relatif à la réglementation des jeux dans les casinos que l'installation de machines à sous au sein d'un local clos implique nécessairement la présence dans ce local d'un caissier ainsi que l'intervention d'un membre du comité de direction en cas d'événement ou d'incident requérant sa présence. Une telle situation est incompatible avec le respect des articles du CSP susmentionnés, qui excluent l'intervention de personnels dans les salles closes réservées aux fumeurs avant que l'air y ait été renouvelé, en l'absence de tout occupant, pendant au moins une heure. Par suite, une salle close affectée exclusivement à la consommation de tabac, au sens de l'article R. 3511-3 du CSP, constitue nécessairement un local distinct, au sens de l'article 68-27 de l'arrêté du 14 mai 2007 relatif à la réglementation des jeux dans les casinos et l'installation de machines à sous dans une telle salle méconnait les dispositions du code de la santé publiques.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
