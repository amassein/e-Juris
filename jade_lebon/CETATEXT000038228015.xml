<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038228015</ID>
<ANCIEN_ID>JG_L_2019_03_000000425191</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/22/80/CETATEXT000038228015.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 13/03/2019, 425191</TITRE>
<DATE_DEC>2019-03-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>425191</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>Mme Cécile Renault</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:425191.20190313</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Sepur a demandé au juge des référés du tribunal administratif de Dijon d'annuler la décision du 25 septembre 2018 par laquelle la communauté d'agglomération du Grand Sénonais, dans le cadre de la procédure de passation d'un marché public portant sur la collecte et l'évacuation d'ordures ménagères et de déchets, a rejeté son offre comme anormalement basse, et d'ordonner à la communauté d'agglomération du Grand Sénonais de reprendre la procédure au stade de l'analyse des offres. <br/>
<br/>
              Par une ordonnance n° 1802525 du 17 octobre 2018, le juge des référés du tribunal administratif de Dijon a rejeté cette demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et deux nouveaux mémoires, enregistrés les 5, 20, 28 novembre 2018 et 20 février 2019 au secrétariat du contentieux du Conseil d'Etat, la société Sepur demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de la communauté d'agglomération du Grand Sénonais la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - l'ordonnance n° 2015-899 du 23 juillet 2015 ;<br/>
              - le décret n° 2016-360 du 25 mars 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Renault, auditrice,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la société Sepur et à la SCP Gatineau, Fattaccini, avocat de la communauté d'agglomération du Grand Sénonais.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 551-1 du code de justice administrative : " Le président du tribunal administratif, ou le magistrat qu'il délègue, peut être saisi en cas de manquement aux obligations de publicité et de mise en concurrence auxquelles est soumise la passation par les pouvoirs adjudicateurs de contrats administratifs ayant pour objet l'exécution de travaux, la livraison de fournitures ou la prestation de services, avec une contrepartie économique constituée par un prix ou un droit d'exploitation, ou la délégation d'un service public. / Le juge est saisi avant la conclusion du contrat ". Aux termes de l'article L. 551-2 du même code : " I. - Le juge peut ordonner à l'auteur du manquement de se conformer à ses obligations et suspendre l'exécution de toute décision qui se rapporte à la passation du contrat, sauf s'il estime, en considération de l'ensemble des intérêts susceptibles d'être lésés et notamment de l'intérêt public, que les conséquences négatives de ces mesures pourraient l'emporter sur leurs avantages. / Il peut, en outre, annuler les décisions qui se rapportent à la passation du contrat et supprimer les clauses ou prescriptions destinées à figurer dans le contrat et qui méconnaissent lesdites obligations ". Selon l'article L. 551-10 du même code : " Les personnes habilitées à engager les recours prévus aux articles L. 551-1 et L. 551-5 sont celles qui ont un intérêt à conclure le contrat et qui sont susceptibles d'être lésées par le manquement invoqué (...) ". <br/>
<br/>
              2. Il ressort des pièces du dossier que, par un avis d'appel à concurrence publié le 26 juin 2018, la communauté d'agglomération du Grand Sénonais a lancé une procédure d'appel d'offres ouvert en vue de l'attribution d'un marché public portant sur la collecte et l'évacuation des ordures ménagères résiduelles et des déchets d'emballage recyclables issus de la collecte en porte à porte ainsi que la collecte des cartons des gros producteurs. La société Sepur, candidate, a été informée le 25 septembre 2018 du rejet de son offre comme anormalement basse. Elle a demandé au juge du référé précontractuel du tribunal administratif de Dijon d'annuler cette décision. Elle se pourvoit en cassation contre l'ordonnance du 17 octobre 2018 par laquelle celui-ci a rejeté sa demande.<br/>
<br/>
              3. Aux termes de l'article 53 de l'ordonnance du 23 juillet 2015 relative aux marchés publics : " Lorsqu'une offre semble anormalement basse, l'acheteur exige que l'opérateur économique fournisse des précisions et justifications sur le montant de son offre. / Si, après vérification des justifications fournies par l'opérateur économique, l'acheteur établit que l'offre est anormalement basse, il la rejette dans des conditions fixées par voie réglementaire. / L'acheteur met en oeuvre tous moyens pour détecter les offres anormalement basses lui permettant de les écarter ". Aux termes de l'article 60 du décret du 25 mars 2016 relatif aux marchés publics : " I. - L'acheteur exige que le soumissionnaire justifie le prix ou les coûts proposés dans son offre lorsque celle-ci semble anormalement basse eu égard aux travaux, fournitures ou services, y compris pour la part du marché public qu'il envisage de sous traiter. (...) II. - l'acheteur rejette l'offre: / 1° Lorsque les éléments fournis par le soumissionnaire ne justifient pas de manière satisfaisante le bas niveau du prix ou des coûts proposés (...) ".<br/>
<br/>
              4. Il résulte de ces dispositions que l'existence d'un prix paraissant anormalement bas au sein de l'offre d'un candidat, pour l'une seulement des prestations faisant l'objet du marché, n'implique pas, à elle-seule, le rejet de son offre comme anormalement basse, y compris lorsque cette prestation fait l'objet d'un mode de rémunération différent ou d'une sous-pondération spécifique au sein du critère du prix. Le prix anormalement bas d'une offre s'apprécie en effet au regard de son prix global. Il s'ensuit que le juge des référés du tribunal administratif de Dijon a commis une erreur de droit en se fondant, pour juger que la communauté d'agglomération du Grand Sénonais n'avait pas commis d'erreur manifeste d'appréciation en rejetant l'offre de la société Sepur comme anormalement basse, sur le seul motif que celle-ci proposait de ne pas facturer les prestations de collecte supplémentaire des ordures ménagères produites par certains gros producteurs. <br/>
<br/>
              5. Toutefois, il ressort des pièces soumises au Conseil d'Etat par la communauté d'agglomération du Grand Sénonais le 22 février 2019 et communiquées à la requérante que le marché a été signé le 18 octobre 2018, soit antérieurement au 5 novembre 2018, date d'introduction du pourvoi de la société Sepur. Son pourvoi est par suite irrecevable. <br/>
<br/>
              6. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par les parties au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société Sepur est rejeté. <br/>
Article 2 : Les conclusions de la communauté d'agglomération du Grand Sénonais présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à la société Sepur et à la communauté d'agglomération du Grand Sénonais.<br/>
Copie en sera adressée à la société Coved.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-02-005 MARCHÉS ET CONTRATS ADMINISTRATIFS. FORMATION DES CONTRATS ET MARCHÉS. FORMALITÉS DE PUBLICITÉ ET DE MISE EN CONCURRENCE. - OFFRES ANORMALEMENT BASSES (ART. 53 DE L'ORDONNANCE DU 23 JUILLET 2015) - APPRÉCIATION AU REGARD DU PRIX GLOBAL DE L'OFFRE [RJ1] - CONSÉQUENCE - IMPOSSIBILITÉ DE REJETER UNE OFFRE COMME ANORMALEMENT BASSE AU MOTIF QUE LE PRIX DE L'UNE SEULEMENT DES PRESTATIONS FAISANT L'OBJET DU MARCHÉ PARAÎT ANORMALEMENT BAS - ESPÈCE.
</SCT>
<ANA ID="9A"> 39-02-005 Il résulte des articles 53 de l'ordonnance n° 2015-899 du 23 juillet 2015 et 60 du décret n° 2016-360 du 25 mars 2016 que l'existence d'un prix paraissant anormalement bas au sein de l'offre d'un candidat, pour l'une seulement des prestations faisant l'objet du marché, n'implique pas, à elle-seule, le rejet de son offre comme anormalement basse, y compris lorsque cette prestation fait l'objet d'un mode de rémunération différent ou d'une sous-pondération spécifique au sein du critère du prix. Le prix anormalement bas d'une offre s'apprécie en effet au regard de son prix global.... ...Commet une erreur de droit le juge des référés qui se fonde, pour juger que l'acheteur n'avait pas commis d'erreur manifeste d'appréciation en rejetant, dans le cadre de la procédure de passation d'un marché public portant sur la collecte et l'évacuation d'ordures ménagères et de déchets, l'offre du soumissionnaire comme anormalement basse, sur le seul motif que celui-ci proposait de ne pas facturer les prestations de collecte supplémentaire des ordures ménagères produites par certains gros producteurs.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant de l'obligation de rechercher si le prix est en lui-même manifestement sous-évalué et, ainsi, susceptible de compromettre la bonne exécution du marché CE, 29 mai 2013, Ministre de l'intérieur c/ Société Arteis, n° 366606, T. pp. 692-703.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
