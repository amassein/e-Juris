<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035911865</ID>
<ANCIEN_ID>JG_L_2017_10_000000396425</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/91/18/CETATEXT000035911865.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 25/10/2017, 396425</TITRE>
<DATE_DEC>2017-10-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>396425</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR>Mme Ophélie Champeaux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:396425.20171025</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Nantes, d'une part, d'annuler son titre de pension de retraite émis le 31 janvier 2011 ainsi que la décision du 12 avril 2011 par laquelle le directeur du service des retraites de l'Etat a rejeté son recours gracieux du 22 février 2011 et, d'autre part, d'enjoindre au ministre des finances et des comptes publics de réexaminer la pension qui lui a été attribuée, en la liquidant sur la base d'une durée de cotisation de cent-cinquante-quatre trimestres au lieu de cent-cinquante-trois. <br/>
<br/>
              Par un jugement n° 1200925 du 9 décembre 2015, le tribunal administratif de Nantes a fait droit à sa demande.<br/>
<br/>
              Par un pourvoi, enregistré le 26 janvier 2016 au secrétariat du contentieux du Conseil d'Etat, le ministre des finances et des comptes publics demande au Conseil d'Etat d'annuler ce jugement. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des pensions civiles et militaires de retraite ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Ophélie Champeaux, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article R. 26 du code des pensions civiles et militaires de retraite : " Dans le décompte final des trimestres liquidables, la fraction de trimestre égale ou supérieure à quarante-cinq jours est comptée pour un trimestre. La fraction de trimestre inférieure à quarante-cinq jours est négligée ". Il résulte du texte même de ces dispositions, qui fixent une durée exprimée en jours calendaires et non en mois ou en parties de trimestre, qu'une période de service égale ou supérieure à quarante-cinq jours calendaires constitue un trimestre liquidable.<br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond qu'au titre de ses services militaires et civils et de la bonification pour services actifs de police, M. B... a totalisé cent-cinquante-trois trimestres liquidables à la date du 31 décembre 2010. En jugeant que la période de service de fin de carrière de M. B..., courant du 1er janvier au 14 février 2011, qui représente quarante-cinq jours calendaires, devait être décomptée comme un trimestre liquidable supplémentaire en application des dispositions réglementaires citées au point 1 et, qu'ainsi, M. B...totalisait cent-cinquante-quatre trimestres liquidables, le tribunal n'a pas commis d'erreur de droit.<br/>
<br/>
              3. Il résulte de ce qui précède que le ministre des finances et des comptes publics n'est pas fondé à demander l'annulation du jugement attaqué.<br/>
<br/>
              4. En l'absence de dépôt de demande d'aide juridictionnelle par M. B..., il n'y a pas lieu de faire droit aux conclusions présentées sur le fondement des dispositions de l'article 37 de la loi du 10 juillet 1991 par la SCP Rousseau-Tapie, avocat de M. B.... Il y a lieu en revanche, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à M.B..., au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du ministre des finances et des comptes publics est rejeté.<br/>
Article 2 : L'Etat versera à M. B...une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée au ministre de l'action et des comptes publics et à M. A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">48-02-02-03-02 PENSIONS. PENSIONS CIVILES ET MILITAIRES DE RETRAITE. PENSIONS CIVILES. LIQUIDATION DE LA PENSION. SERVICES PRIS EN COMPTE. - DÉCOMPTE FINAL DES TRIMESTRES LIQUIDABLES (ART. R. 26 DU CPCMR) - NOTION DE TRIMESTRE LIQUIDABLE - PÉRIODE DE SERVICE ÉGALE OU SUPÉRIEURE À  QUARANTE-CINQ JOURS CALENDAIRES.
</SCT>
<ANA ID="9A"> 48-02-02-03-02 Il résulte du texte même de l'article R. 26 du code des pensions civiles et militaires de retraite (CPCMR), qui fixe une durée exprimée en jours calendaires et non en mois ou en partie de trimestre, qu'une période de service égale ou supérieure à quarante-cinq jours calendaires constitue un trimestre liquidable.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
