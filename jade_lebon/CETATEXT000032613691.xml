<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032613691</ID>
<ANCIEN_ID>JG_L_2016_05_000000387798</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/61/36/CETATEXT000032613691.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 5ème chambres réunies, 30/05/2016, 387798, Publié au recueil Lebon</TITRE>
<DATE_DEC>2016-05-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>387798</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY ; SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>Mme Pauline Pannier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:387798.20160530</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le comité central d'entreprise FNAC CODIREP, la Fédération des services CFDT et Mme B...A...ont demandé au tribunal administratif de Melun d'annuler pour excès de pouvoir l'article 2 de la décision du 10 février 2014 par laquelle la directrice régionale des entreprises, de la concurrence, de la consommation, du travail et de l'emploi d'Ile-de-France a homologué le document unilatéral complétant l'accord partiel relatif au plan de sauvegarde de l'emploi de la société FNAC CODIREP. Par un jugement n° 1403399 du 9 juillet 2014, le tribunal administratif a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 14PA03909 du 8 décembre 2014, la cour administrative d'appel de Paris a rejeté l'appel formé par le comité central d'entreprise FNAC CODIREP, la Fédération des services CFDT et Mme A...contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire, un mémoire en réplique et un mémoire en réponse à une mesure d'instruction, enregistrés les 9 février, 13 avril, 16 juillet 2015 et 17 février 2016 au secrétariat du contentieux du Conseil d'Etat, le comité central d'entreprise FNAC CODIREP et Mme A...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat et de la société FNAC CODIREP la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Pannier, auditeur,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat du comité central d'entreprise FNAC CODIREP et autre et à la SCP Odent, Poulet, avocat de la société FNAC CODIREP ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 10 février 2014, la directrice régionale des entreprises, de la concurrence, de la consommation, du travail et de l'emploi d'Ile-de-France a d'une part, par l'article 1er de sa décision, validé l'accord collectif majoritaire fixant le contenu du plan de sauvegarde de l'emploi présenté par la société FNAC CODIREP, qui exploite des magasins sous l'enseigne FNAC situés principalement en banlieue parisienne, et, d'autre part, par l'article 2 de la même décision, homologué le document unilatéral de cette société qui fixait, en complément de cet accord, le nombre de suppressions de postes et la catégorie professionnelle concernée par les licenciements ; que le comité central d'entreprise FNAC CODIREP et Mme A...se pourvoient en cassation contre l'arrêt du 8 décembre 2014 par lequel la cour administrative d'appel de Paris a rejeté leur requête dirigée contre le jugement du 9 juillet 2014 du tribunal administratif de Melun rejetant leur demande d'annulation pour excès de pouvoir du seul article 2 de la décision du 10 février 2014 ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 1233-24-1 du code du travail : " Dans les entreprises de cinquante salariés et plus, un accord collectif peut déterminer le contenu du plan de sauvegarde de l'emploi mentionné aux articles L. 1233-61 à L. 1233-63 ainsi que les modalités de consultation du comité d'entreprise et de mise en oeuvre des licenciements (...) " ; que l'article L. 1233-24-2 du même code dispose que cet accord collectif fixant le contenu du plan de sauvegarde de l'emploi " peut également porter sur : (...) / 4° Le nombre de suppressions d'emploi et les catégories professionnelles concernées (...) " ; que l'article L. 1233-24-4 dispose que : " A défaut d'accord mentionné à l'article L. 1233-24-1, un document élaboré par l'employeur après la dernière réunion du comité d'entreprise fixe le contenu du plan de sauvegarde de l'emploi et précise les éléments prévus aux 1° à 5° de l'article L. 1233-24-2, dans le cadre des dispositions légales et conventionnelles en vigueur " ; qu'enfin l'article L. 1233-57-4 prévoit qu'en l'absence d'accord collectif, ou en cas d'accord ne portant pas sur l'ensemble des éléments prévus aux 1° à 5° : " (...) l'autorité administrative homologue le document élaboré par l'employeur mentionné à l'article L. 1233-24-4, après avoir vérifié la conformité de son contenu aux dispositions législatives et aux stipulations conventionnelles relatives aux éléments mentionnés aux 1° à 5° de l'article L. 1233-24-2 (...) " ;<br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions que, lorsqu'elle est saisie d'une demande d'homologation d'un document qui fixe tout ou partie des éléments mentionnés aux 1° à 5° de l'article L. 1233-24-2 du code du travail, en raison, soit de l'absence d'accord collectif de travail portant sur le plan de sauvegarde de l'emploi, soit de l'absence, dans cet accord collectif, de tout ou partie des éléments mentionnés aux 1° à 5° de l'article L. 1233-24-2, il appartient à l'administration, sous le contrôle du juge de l'excès de pouvoir, de vérifier la conformité de ces éléments aux dispositions législatives et aux stipulations conventionnelles applicables ; qu'en particulier, s'agissant des catégories professionnelles concernées par le projet de licenciement, mentionnées au 4° de l'article L. 1233-24-2, il appartient à l'administration de vérifier qu'elles regroupent, chacune, l'ensemble des salariés qui exercent au sein de l'entreprise des fonctions de même nature supposant une formation professionnelle commune ; <br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le document unilatéral de la société FNAC CODIREP homologué par la décision litigieuse ne prévoit qu'une seule catégorie professionnelle concernée par le projet de licenciement collectif ; qu'il définit cette catégorie, sur la base d'un indicateur propre à l'entreprise appelé " rémunération expérience client ", comme regroupant les vendeurs affectés aux départements des filières " disques " ou " livres " des magasins FNAC CODIREP et dont les ventes, sur le premier semestre 2013, se sont majoritairement réalisées sur les départements de la seule filière " disque " ; qu'en estimant que cette définition, qui visait expressément à limiter le champ des licenciements à ceux des salariés dont l'activité était exclusivement ou principalement consacrée à la vente des produits de la filière " disques ", avait pour objet " d'éviter que des salariés ne soient concernés par le dispositif qu'eu égard à leur affectation à la filière disques ", la cour administrative d'appel de Paris a dénaturé les pièces du dossier qui lui était soumis ; que dès lors, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt doit être annulé ;<br/>
<br/>
              5. Considérant que, le délai de trois mois imparti à la cour administrative d'appel pour statuer par les dispositions de l'article L. 1235-7-1 du code du travail étant expiré, il y a lieu pour le Conseil d'Etat, en application des mêmes dispositions, de statuer immédiatement sur l'appel formé par le comité central d'entreprise FNAC CODIREP et Mme A...contre le jugement du 9 juillet 2014 du tribunal administratif de Melun ;<br/>
<br/>
              6. Considérant que, lorsqu'un accord collectif relatif à un plan de sauvegarde de l'emploi ne porte pas sur l'ensemble des éléments mentionnés aux 1° à 5° de l'article L. 1233-24-2 du code du travail, la décision par laquelle l'autorité administrative statue sur la demande de validation de cet accord est, en principe, divisible de celle par laquelle cette autorité statue sur la demande d'homologation du document unilatéral qui fixe ceux des éléments qui n'ont pas été déterminés par l'accord ; que la société FNAC CODIREP n'est, par suite, pas fondée à soutenir que la décision du 10 février 2014 revêtirait, pour ce seul motif, un caractère indivisible et que la demande d'annulation du seul article 2 relatif à la décision d'homologation serait, en conséquence, irrecevable ; <br/>
<br/>
              7. Considérant que la société FNAC CODIREP soutient que le choix d'une catégorie professionnelle limitée aux seuls vendeurs de la filière " disques " se justifie par les compétences particulières acquises par ces salariés dans l'exercice de leurs fonctions ; que si la caractérisation de l'appartenance à une même catégorie professionnelle doit, le cas échéant, tenir compte des acquis de l'expérience professionnelle pour apprécier, ainsi qu'il a été dit au point 3, l'existence d'une formation professionnelle commune, c'est toutefois à la condition, notamment, que de tels acquis équivalent à une formation complémentaire qui excède l'obligation d'adaptation qui incombe à l'employeur ; qu'en l'espèce, il ressort des pièces du dossier, et notamment des éléments fournis par l'employeur en réponse à la mesure d'instruction diligentée par la 4ème chambre de la Section du contentieux du Conseil d'Etat, qu'à la date de la décision litigieuse, les vendeurs de la société FNAC CODIREP qui travaillaient exclusivement ou principalement dans la filière " disques "  ne pouvaient être regardés, eu égard, d'une part, à la nature de leurs fonctions et, d'autre part, à leurs formations de base, aux formations complémentaires qui leur étaient délivrées et aux compétences acquises dans leur pratique professionnelle, comme appartenant à une catégorie professionnelle différente de celle, notamment, des vendeurs de la filière " livres " ;<br/>
<br/>
              8. Considérant, dès lors, que l'illégalité de cette définition servant de base au périmètre des licenciements faisait obstacle à ce que, par sa décision du 10 février 2014, la directrice régionale des entreprises, de la concurrence, de la consommation, du travail et de l'emploi d'Ile-de-France homologue le document unilatéral établi par la société FNAC CODIREP ; <br/>
<br/>
              9. Considérant qu'il résulte de tout ce qui précède que le comité central d'entreprise FNAC CODIREP et Mme A...sont fondés à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Melun a rejeté leur demande ;<br/>
<br/>
              10. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge du comité central d'entreprise FNAC CODIREP, qui n'est pas, dans la présente instance, la partie perdante ; qu'en revanche, il y a lieu, dans les circonstances de l'espèce, de faire application de ces dispositions et de mettre à la charge de la société FNAC CODIREP et de l'Etat la somme de 1 500 euros chacun à verser, d'une part au comité central d'entreprise FNAC CODIREP et, d'autre part, à Mme A... au titre des frais exposés par ces derniers tant en première instance qu'en appel et en cassation ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 8 décembre 2014, le jugement du tribunal administratif de Melun du 9 juillet 2014 et l'article 2 de la décision de la directrice régionale des entreprises, de la concurrence, de la consommation, du travail et de l'emploi d'Ile-de-France du 10 février 2014 sont annulés.<br/>
Article 2 : La société FNAC CODIREP et l'Etat verseront 1 500 euros chacun, d'une part au comité central d'entreprise FNAC CODIREP et, d'autre part, à MmeA..., au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée au comité central d'entreprise FNAC CODIREP, à la société FNAC CODIREP, à Mme B...A...et à la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-01-01-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉCISIONS POUVANT OU NON FAIRE L'OBJET D'UN RECOURS. ACTES CONSTITUANT DES DÉCISIONS SUSCEPTIBLES DE RECOURS. - ACTE DIVISIBLE - DÉCISION VALIDANT UN ACCORD COLLECTIF FIXANT UNE PARTIE D'UN PSE ET HOMOLOGUANT LE DOCUMENT UNILATÉRAL DE L'EMPLOYEUR FIXANT LE RESTE DU PLAN - EXISTENCE EN PRINCIPE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">66-07 TRAVAIL ET EMPLOI. LICENCIEMENTS. - AUTORISATION ADMINISTRATIVE DES PSE - CONTRÔLE DE LA DÉFINITION DES CATÉGORIES PROFESSIONNELLES CONCERNÉES - 1) DÉFINITION - SALARIÉS EXERÇANT DES FONCTIONS DE MÊME NATURE SUPPOSANT UNE FORMATION PROFESSIONNELLE COMMUNE [RJ1] - 2) PRISE EN COMPTE DES ACQUIS DE L'EXPÉRIENCE PROFESSIONNELLE POUR DISTINGUER DEUX CATÉGORIES - A) CONDITIONS [RJ2] - B) ESPÈCE - 3) CONTENTIEUX - DIVISIBILITÉ DE LA DÉCISION DE VALIDATION D'UN ACCORD COLLECTIF FIXANT UNE PARTIE DU PLAN ET DE LA DÉCISION HOMOLOGUANT LE DOCUMENT UNILATÉRAL FIXANT LE RESTE DU PLAN - EXISTENCE EN PRINCIPE.
</SCT>
<ANA ID="9A"> 54-01-01-01 Lorsqu'un accord collectif relatif à un plan de sauvegarde de l'emploi (PSE) ne porte pas sur l'ensemble des éléments mentionnés aux 1° à 5° de l'article L. 1233-24-2 du code du travail, la décision par laquelle l'autorité administrative statue sur la demande de validation de cet accord est, en principe, divisible de celle par laquelle cette autorité statue sur la demande d'homologation du document unilatéral qui fixe ceux des éléments qui n'ont pas été déterminés par l'accord.</ANA>
<ANA ID="9B"> 66-07 1) En vertu du code du travail, le plan de sauvegarde de l'emploi (PSE) précise le nombre de suppressions d'emploi et les catégories professionnelles concernées. Saisie d'une demande d'homologation d'un PSE, il appartient à l'administration de vérifier que les catégories professionnelles concernées par le projet de licenciement regroupent, chacune, l'ensemble des salariés qui exercent au sein de l'entreprise des fonctions de même nature supposant une formation professionnelle commune.... ,,2) Magasins vendant des disques et des livres. Employeur soutenant que le choix d'une catégorie professionnelle limitée aux seuls vendeurs de la filière disques se justifie par les compétences particulières acquises par ces salariés dans l'exercice de leurs fonctions.... ,,a) Si la caractérisation de l'appartenance à une même catégorie professionnelle doit, le cas échéant, tenir compte des acquis de l'expérience professionnelle pour apprécier l'existence d'une formation professionnelle commune, c'est toutefois à la condition, notamment, que de tels acquis équivalent à une formation complémentaire qui excède l'obligation d'adaptation qui incombe à l'employeur.... ,,b) En l'espèce, il ressort des pièces du dossier, notamment des éléments fournis par l'employeur à la suite d'une mesure d'instruction, qu'à la date de la décision litigieuse, les vendeurs qui travaillaient exclusivement ou principalement dans la filière disques  ne pouvaient être regardés, eu égard, d'une part, à la nature de leur fonctions et, d'autre part, à leurs formations de base, aux formations complémentaires qui leur étaient délivrées et aux compétences acquises dans leur pratique professionnelle, comme appartenant à une catégorie professionnelle différente de celle, notamment, des vendeurs de la filière livres.,,,3) Lorsqu'un accord collectif relatif à un PSE ne porte pas sur l'ensemble des éléments mentionnés aux 1° à 5° de l'article L. 1233-24-2 du code du travail, la décision par laquelle l'autorité administrative statue sur la demande de validation de cet accord est, en principe, divisible de celle par laquelle cette autorité statue sur la demande d'homologation du document unilatéral qui fixe ceux des éléments qui n'ont pas été déterminés par l'accord.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. Cass. soc., 13 février 1997, n° 95-16.648, Bull civ. V n° 63., ,[RJ2] Rappr. Cass. soc.,  23 mars 2011, n° 09-71.599.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
