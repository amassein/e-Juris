<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025284619</ID>
<ANCIEN_ID>JG_L_2012_02_000000353945</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/28/46/CETATEXT000025284619.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 01/02/2012, 353945</TITRE>
<DATE_DEC>2012-02-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>353945</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Philippe Martin</PRESIDENT>
<AVOCATS/>
<RAPPORTEUR>M. Guillaume Odinet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:353945.20120201</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance n° 10MA00386 du 7 novembre 2011, enregistrée le 9 novembre 2011 au secrétariat du contentieux du Conseil d'Etat, par laquelle le président de la 7ème chambre de la cour administrative d'appel de Marseille, avant qu'il soit statué sur l'appel du préfet des Pyrénées-Orientales, tendant à l'annulation du jugement du 17 novembre 2009 par lequel le tribunal administratif de Montpellier a rejeté son déféré tendant à l'annulation de la délibération du 5 juin 2008 du conseil municipal de la COMMUNE DES ANGLES, a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution de l'article L. 144-1 du code forestier ;<br/>
<br/>
              Vu le mémoire, enregistré le 3 août 2011 au greffe de la cour administrative d'appel de Marseille, présenté par la COMMUNE DES ANGLES, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule et son article 61-1 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ; <br/>
<br/>
              Vu le code forestier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Odinet, Auditeur,<br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
<br/>
              Considérant que, par une ordonnance du 7 novembre 2011, le président de la 7ème chambre de la cour administrative d'appel de Marseille a décidé, avant qu'il soit statué sur l'appel que le préfet des Pyrénées-Orientales a interjeté du jugement du tribunal administratif de Montpellier du 17 novembre 2009, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution de l'article L. 144-1 du code forestier posée par la COMMUNE DES ANGLES ; que, si le préfet des Pyrénées-Orientales a informé la cour administrative d'appel de Marseille, par un mémoire enregistré à son greffe le 30 novembre 2011, qu'il entendait se désister purement et simplement de son appel, la cour administrative d'appel n'a pas, à la date de la présente décision, donné acte de ce désistement ; que dès lors, la question prioritaire de constitutionnalité transmise par le président de la 7ème chambre de la cour administrative d'appel de Marseille n'a pas perdu son objet ; qu'il y a donc lieu, pour le Conseil d'Etat, de se prononcer sur le renvoi de cette question au Conseil constitutionnel ;<br/>
<br/>
              Considérant qu'aux termes de l'article L. 144-1 du code forestier : " Les ventes des coupes de toutes natures sont faites à la diligence de l'Office national des forêts, dans les mêmes formes que pour les bois de l'Etat et en présence du maire ou d'un adjoint pour les bois communaux et d'un administrateur pour les personnes morales mentionnées à l'article L. 141-1, sans toutefois que l'absence des maires ou administrateurs, régulièrement convoqués, puisse entraîner la nullité des opérations. / Toute vente ou coupe effectuée par ordre des représentants des collectivités et personnes morales mentionnées à l'article L. 141-1, en infraction aux dispositions de l'alinéa précédent, donne lieu contre eux à une amende de 4 500 euros, sans préjudice des dommages-intérêts qui sont dus aux propriétaires. Les ventes ainsi effectuées sont déclarées nulles. " ;<br/>
<br/>
              Considérant, en premier lieu, que les dispositions précitées de l'article L. 144-1 du code forestier n'entraînent aucune privation de propriété pour les collectivités territoriales propriétaires de bois et forêts soumis au " régime forestier " ; que si, en imposant que les coupes et produits des coupes de ces bois et forêts soient mis en vente par l'Office national des forêts (ONF), elles apportent une limitation au droit que les collectivités territoriales ont de disposer de leurs biens, une telle limitation répond à l'objectif d'intérêt général, que poursuit le régime forestier  mis en place par le code forestier, de cohérence de la politique forestière nationale et de mise en valeur de la forêt et de ses produits dans des conditions économiques satisfaisantes pour ses propriétaires et, notamment, pour les communes ; qu'il résulte en outre des dispositions des chapitres III et IV du titre IV du livre Ier du code forestier, notamment de son article L. 144-1-1, que les collectivités territoriales ont un rôle déterminant dans la programmation des coupes, choisissent les quantités mises en vente et la façon dont les coupes sont mises à disposition de l'ONF, et sont associées aux opérations de vente, dont le produit leur est reversé ; que, dès lors, les dispositions contestées ne méconnaissent pas les exigences constitutionnelles relatives à la propriété des personnes publiques ; <br/>
<br/>
              Considérant, en deuxième lieu, que, eu égard, d'une part, aux objectifs d'intérêt général précédemment rappelés, et, d'autre part, aux prérogatives que conservent les collectivités territoriales, le législateur ne peut être regardé comme ayant porté, en interdisant aux collectivités territoriales de procéder elles-mêmes aux ventes des coupes et produits de coupes issus de leurs bois et forêts, une  atteinte à leur libre administration contraire à l'article 72 de la Constitution ou à la liberté garantie par les articles 4 et 5 de la Déclaration des droits de l'homme et du citoyen de 1789 ;<br/>
<br/>
              Considérant, en troisième lieu, que les dispositions précitées n'ont ni pour objet, ni pour effet, de prévoir un traitement différencié des ventes de coupes et produits de coupes dans les bois et forêts selon les collectivités territoriales ou les groupements de collectivités propriétaires ; qu'elles n'ont, en tout état de cause, pas davantage pour objet ou pour effet de priver les collectivités territoriales de la possibilité de prendre part, à travers la mise en oeuvre de la politique forestière et la gestion de leurs forêts, à l'amélioration de l'environnement et à la promotion d'un développement durable ; qu'elles ne méconnaissent donc ni le principe d'égalité ni, en tout état de cause, les articles 2 et 6 de la Charte de l'environnement ;<br/>
<br/>
              Considérant, enfin, que les dispositions de l'article L. 144-1 du code forestier ne sauraient en tout état de cause être regardées comme contraires aux dispositions du neuvième alinéa du Préambule de la Constitution de 1946 selon lesquelles : " Tout bien, toute entreprise, dont l'exploitation a ou acquiert les caractères d'un service public national ou d'un monopole de fait, doit devenir la propriété de la collectivité " ;<br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que la question de constitutionnalité soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux ; que, par suite, il n'y a pas lieu de renvoyer au Conseil constitutionnel la question invoquée par la COMMUNE DES ANGLES ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité transmise par le président de la septième chambre de la cour administrative d'appel de Marseille.<br/>
Article 2 : La présente décision sera notifiée à la COMMUNE DES ANGLES et au  ministre de l'agriculture, de l'alimentation, de la pêche, de la ruralité et de l'aménagement du territoire.  Copie en sera adressée au Conseil constitutionnel et à la cour administrative d'appel de Marseille.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-05-04-02 PROCÉDURE. INCIDENTS. DÉSISTEMENT. PORTÉE ET EFFETS. - DÉSISTEMENT ANNONCÉ DEVANT LA JURIDICTION DU FOND APRÈS TRANSMISSION AU CONSEIL D'ETAT D'UNE QPC - CONSÉQUENCE - NON-LIEU À SE PRONONCER SUR LA QPC - ABSENCE, DÈS LORS QU'IL N'A PAS ÉTÉ DONNÉ ACTE DU DÉSISTEMENT.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-05-05-01 PROCÉDURE. INCIDENTS. NON-LIEU. ABSENCE. - QPC TRANSMISE AU CONSEIL D'ETAT - DÉSISTEMENT, POSTÉRIEUR À LA TRANSMISSION, DES CONCLUSIONS DU LITIGE, MAIS DONT  LA JURIDICTION SAISIE DU LITIGE N'A PAS DONNÉ ACTE DU DÉSISTEMENT.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-10 PROCÉDURE. - QUESTION TRANSMISE AU CONSEIL D'ETAT - DÉSISTEMENT, POSTÉRIEUR À LA TRANSMISSION, DES CONCLUSIONS DU LITIGE - NON-LIEU À SE PRONONCER SUR LA QPC - ABSENCE, DÈS LORS QUE LA JURIDICTION SAISIE DU LITIGE N'A PAS DONNÉ ACTE DU DÉSISTEMENT.
</SCT>
<ANA ID="9A"> 54-05-04-02 Il n'y a pas non-lieu, pour le Conseil d'Etat, à se prononcer sur le renvoi au Conseil constitutionnel d'une question prioritaire de constitutionnalité (QPC) transmise par une juridiction du fond au seul motif que le requérant devant cette juridiction a annoncé se désister de ses conclusions, dès lors qu'il n'a pas été donné acte de ce désistement.</ANA>
<ANA ID="9B"> 54-05-05-01 Il n'y a pas non-lieu, pour le Conseil d'Etat, à se prononcer sur le renvoi au Conseil constitutionnel d'une question prioritaire de constitutionnalité (QPC) transmise par une juridiction du fond au seul motif que le requérant devant cette juridiction a annoncé se désister de ses conclusions, dès lors qu'il n'a pas été donné acte de ce désistement.</ANA>
<ANA ID="9C"> 54-10 Il n'y a pas non-lieu, pour le Conseil d'Etat, à se prononcer sur le renvoi au Conseil constitutionnel d'une question prioritaire de constitutionnalité (QPC) transmise par une juridiction du fond au seul motif que le requérant devant cette juridiction a annoncé se désister de ses conclusions, dès lors qu'il n'a pas été donné acte de ce désistement.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
