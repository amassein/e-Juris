<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030236187</ID>
<ANCIEN_ID>JG_L_2015_02_000000370458</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/23/61/CETATEXT000030236187.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème / 3ème SSR, 13/02/2015, 370458</TITRE>
<DATE_DEC>2015-02-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>370458</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème / 3ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>Mme Karin Ciavaldini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:370458.20150213</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>		Vu la procédure suivante :		 <br/>
<br/>
              M. B...A...a demandé au tribunal administratif d'Orléans d'annuler les arrêtés du 31 août 2011 par lesquels le maire de la commune de Pithiviers s'est opposé à ses déclarations préalables portant, d'une part, sur la réhabilitation d'un bâtiment et, d'autre part, sur la reconstruction d'un mur, la pose d'un portail et la couverture de ce même bâtiment, situé chemin des Meuniers à Pithiviers. Par un jugement n° 1103756 du 21 mai 2013, le tribunal administratif d'Orléans a rejeté sa  demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 23 juillet et 23 octobre 2013 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler le jugement du 21 mai 2013 du tribunal administratif d'Orléans ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Pithiviers la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ainsi que les entiers dépens, y compris la contribution pour l'aide juridique au titre de l'article R. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de l'urbanisme ;<br/>
<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Karin Ciavaldini, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de M. A...et à la SCP Waquet, Farge, Hazan, avocat de la commune de Pithiviers.<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'en vertu de l'article L. 123-12 du code de l'urbanisme, dans sa rédaction applicable au litige, dans les communes non couvertes par un schéma de cohérence territoriale, l'acte publié approuvant le plan local d'urbanisme devient exécutoire un mois suivant sa transmission au préfet, sauf si le préfet demande que des modifications y soient apportées ; qu'aux termes de l'article R. 123-25 du même code, dans cette même rédaction : " Tout acte mentionné à l'article R. 123-24 est affiché pendant un mois en mairie ou au siège de l'établissement public compétent et, dans ce cas, dans les mairies des communes membres concernées. Mention de cet affichage est insérée en caractères apparents dans un journal diffusé dans le département. / Il est en outre publié : / a) au recueil des actes administratifs mentionné à l'article R. 2121-10 du code général des collectivités territoriales, lorsqu'il s'agit d'une délibération du conseil municipal d'une commune de 3 500 habitants et plus ; (...) / L'arrêté ou la délibération produit ses effets juridiques dès l'exécution de l'ensemble des formalités prévues au premier alinéa ci-dessus (...) " ; qu'aux termes de l'article R. 123-24 du même code : " Font l'objet des mesures de publicité et d'information édictées à l'article R. 123-25 : (...) la délibération qui approuve (...) un plan local d'urbanisme (...) " ;  <br/>
<br/>
              2. Considérant qu'il résulte de ces dispositions que l'acte approuvant un plan local d'urbanisme devient exécutoire un mois suivant sa transmission au préfet, sauf si le préfet demande que des modifications y soient apportées et sous réserve qu'il ait fait l'objet d'un affichage dans les conditions prévues au premier alinéa de l'article R. 123-25 du code de l'urbanisme et que mention de cet affichage ait été insérée en caractères apparents dans un journal diffusé dans le département ; qu'est en revanche sans incidence la circonstance qu'il ait ou non été publié au recueil des actes administratifs, en application, lorsqu'il s'agit d'une délibération du conseil municipal d'une commune de 3 500 habitants et plus, du troisième alinéa de ce même article ;  <br/>
<br/>
              3. Considérant que M. A...soutenait devant le tribunal administratif que le plan local d'urbanisme de la commune de Pithiviers approuvé par une délibération du conseil municipal du 5 juillet 2011, sur le fondement duquel le maire de la commune s'est opposé par arrêtés du 31 août 2011 à ses déclarations préalables de travaux, n'était pas exécutoire à cette date, faute que toutes les mesures de publicité prévues à l'article R. 123-25 du code de l'urbanisme aient été accomplies ; qu'il résulte du point 2 ci-dessus que la circonstance que la délibération du 5 juillet 2011 ait ou non été publiée au recueil des actes administratifs était sans incidence ; que, par suite, le tribunal administratif a suffisamment motivé son jugement en relevant, pour écarter le moyen soulevé par M.A..., que la délibération litigieuse avait été transmise au préfet le 8 juillet 2011, qu'elle avait fait l'objet d'un affichage à la même date et qu'une mention de l'approbation du plan local d'urbanisme et de la possibilité de venir consulter le dossier à la mairie avait été insérée dans un journal diffusé dans le département paru le 12 juillet 2011 ; que le pourvoi de M. A...doit donc être rejeté ;<br/>
<br/>
              4. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de Pithiviers qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. A...le versement à la commune de Pithiviers de la somme de 2 000 euros au titre de ces dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
		Article 1er : Le pourvoi de M. A...est rejeté.<br/>
Article 2 : M. A...versera à la commune de Pithiviers la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 3 : La présente décision sera notifiée à M. B...A...et à la commune de Pithiviers.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-01-01-02-01-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PLANS D'AMÉNAGEMENT ET D'URBANISME. PLANS D`OCCUPATION DES SOLS (POS) ET PLANS LOCAUX D'URBANISME (PLU). APPLICATION DES RÈGLES FIXÉES PAR LES POS OU LES PLU. APPLICATION DANS LE TEMPS. ENTRÉE EN VIGUEUR DU PLAN. - INCIDENCE DE LA PUBLICATION AU RECUEIL DES ACTES ADMINISTRATIFS DE L'ACTE APPROUVANT LE PLU - ABSENCE.
</SCT>
<ANA ID="9A"> 68-01-01-02-01-01 Il résulte des dispositions des articles L. 123-12,  R. 123-24 et R. 123-25 du code de l'urbanisme que l'acte approuvant un plan local d'urbanisme (PLU) devient exécutoire un mois suivant sa transmission au préfet, sauf si le préfet demande que des modifications y soient apportées et sous réserve qu'il ait fait l'objet d'un affichage dans les conditions prévues au premier alinéa de l'article R. 123-25 du code de l'urbanisme et que mention de cet affichage ait été insérée en caractères apparents dans un journal diffusé dans le département. Est en revanche sans incidence la circonstance qu'il ait ou non été publié au recueil des actes administratifs, en application, lorsqu'il s'agit d'une délibération du conseil municipal d'une commune de 3 500 habitants et plus, du troisième alinéa de ce même article.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
