<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034330357</ID>
<ANCIEN_ID>JG_L_2017_03_000000395624</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/33/03/CETATEXT000034330357.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème chambres réunies, 31/03/2017, 395624</TITRE>
<DATE_DEC>2017-03-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>395624</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU ; SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>M. Frédéric Pacoud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean Lessi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:395624.20170331</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une décision du 27 juin 2016, le Conseil d'Etat statuant au contentieux a prononcé l'admission des conclusions du pourvoi de Mme A...B...dirigées contre l'arrêt n° 15LY00152 du 22 octobre 2015 de la cour administrative d'appel de Lyon, en tant seulement qu'il s'est prononcé sur la décision du 20 mars 2012 par laquelle le président du conseil général de la Drôme a suspendu son agrément en qualité d'assistante familiale.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'action sociale et des familles ;<br/>
              - la loi n° 79-587 du 11 juillet 1979 ;<br/>
              - la loi n° 2000-321 du 12 avril 2000 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Pacoud, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Jean Lessi, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de Mme B...et à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat du département de la Drôme.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme B..., agréée en qualité d'assistante familiale par le département de la Drôme en juillet 2006, s'est vu confier, le 13 mars 2012, un mineur de 16 ans par l'association des maisons d'accueil protestantes pour enfants. A la suite de la tentative de suicide de ce jeune survenue le 18 mars 2012, le président du conseil général de la Drôme a, le 20 mars 2012, suspendu l'agrément de Mme B...pour une durée maximale de quatre mois, puis, le 25 juin 2012, retiré cet agrément. Par celles de ses conclusions admises par la décision du Conseil d'Etat statuant au contentieux du 27 juin 2016, Mme B...demande l'annulation de l'arrêt de la cour administrative d'appel de Lyon du 22 octobre 2015 en tant que, annulant dans cette mesure le jugement du tribunal administratif de Grenoble du 18 novembre 2014, il rejette ses conclusions tendant à l'annulation de la décision suspendant son agrément.<br/>
<br/>
              2. En premier lieu, d'une part, aux termes de l'article 1er de la loi du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public, applicable à la décision en litige et désormais codifié à l'article L. 211-2 du code des relations entre le public et l'administration : " Les personnes physiques ou morales ont le droit d'être informées sans délai des motifs des décisions administratives individuelles défavorables qui les concernent. / A cet effet, doivent être motivées les décisions qui : / - restreignent l'exercice des libertés publiques ou, de manière générale, constituent une mesure de police (...) ". L'article 24 de la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations, dont les dispositions sont désormais reprises aux articles L. 121-1 et L. 121-2 du même code, dispose que : " Exception faite des cas où il est statué sur une demande, les décisions individuelles qui doivent être motivées en application des articles 1er et 2 de la loi n° 79-587 du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public n'interviennent qu'après que la personne intéressée a été mise à même de présenter des observations écrites et, le cas échéant, sur sa demande, des observations orales. Cette personne peut se faire assister par un conseil ou représenter par un mandataire de son choix. (...) / Les dispositions de l'alinéa précédent ne sont pas applicables : / 1° En cas d'urgence ou de circonstances exceptionnelles ; / (...) / 3° Aux décisions pour lesquelles des dispositions législatives ont instauré une procédure contradictoire particulière. (...) ".<br/>
<br/>
              3. D'autre part, aux termes de l'article L. 421-3 du code de l'action sociale et des familles, dans sa rédaction alors en vigueur : " L'agrément nécessaire pour exercer la profession d'assistant maternel ou d'assistant familial est délivré par le président du conseil général du département où le demandeur réside. (...) ". Les troisième à cinquième alinéas de l'article L. 421-6 du même code disposent que : " Si les conditions de l'agrément cessent d'être remplies, le président du conseil général peut, après avis d'une commission consultative paritaire départementale, modifier le contenu de l'agrément ou procéder à son retrait. En cas d'urgence, le président du conseil général peut suspendre l'agrément. Tant que l'agrément reste suspendu, aucun enfant ne peut être confié. / Toute décision de retrait de l'agrément, de suspension de l'agrément ou de modification de son contenu doit être dûment motivée et transmise sans délai aux intéressés. / La composition, les attributions et les modalités de fonctionnement de la commission présidée par le président du conseil général ou son représentant, mentionnée au troisième alinéa, sont définies par voie réglementaire ". Enfin, l'article L. 423-8 de ce code, qui est applicable aux assistants familiaux employés tant par des personnes morales de droit privé que, en vertu de l'article L. 422-1, par des personnes morales de droit public, prévoit que : " En cas de suspension de l'agrément, l'assistant maternel ou l'assistant familial (...)  est suspendu de ses fonctions par l'employeur pendant une période qui ne peut excéder quatre mois. Durant cette période, l'assistant maternel ou l'assistant familial bénéficie d'une indemnité compensatrice qui ne peut être inférieure à un montant minimal fixé par décret. / (...) L'assistant maternel ou l'assistant familial suspendu de ses fonctions bénéficie, à sa demande, d'un accompagnement psychologique mis à sa disposition par son employeur pendant le temps de la suspension de ses fonctions ". <br/>
<br/>
              4. La décision par laquelle l'autorité administrative prononce la suspension de l'agrément d'un assistant maternel ou familial constitue une mesure de police administrative prise dans l'intérêt des enfants accueillis. Si elle doit être motivée en vertu des dispositions spéciales de l'article L. 421-6 du code de l'action sociale et des familles, elle n'en relève pas moins, contrairement à ce qu'a jugé la cour administrative d'appel, du champ d'application de l'article 1er de la loi du 11 juillet 1979. <br/>
<br/>
              5. Il résulte toutefois des articles L. 421-6 et L. 423-8 du code de l'action sociale et des familles que cette mesure de suspension, qui ne peut excéder quatre mois, constitue une mesure provisoire destinée à permettre de sauvegarder la santé, la sécurité et le bien-être des mineurs accueillis, durant les délais nécessaires notamment à la consultation de la commission consultative paritaire départementale et au respect du caractère contradictoire de la procédure, en vue, le cas échéant, d'une mesure de retrait ou de modification du contenu de l'agrément. Pendant la période de suspension de son agrément, l'assistant maternel ou familial employé par une personne morale de droit privé ou de droit public bénéficie d'une indemnité compensatrice. Le législateur a ainsi entendu, par ces dispositions, déterminer entièrement les règles de procédure auxquelles sont soumises ces mesures de suspension de l'agrément des assistants maternels ou familiaux, qui s'inscrivent dans le cadre de la modification ou du retrait éventuel de cet agrément, soumis à une procédure contradictoire préalable précisée à l'article R. 421-23 du même code. Dès lors, l'article 24 de la loi du 12 avril 2000, qui fixe des règles générales de procédure applicables aux décisions devant être motivées en vertu de la loi du 11 juillet 1979, ne saurait utilement être invoqué à l'encontre d'une telle mesure.<br/>
<br/>
              6. Ce motif, qui n'avait été retenu qu'à titre surabondant par la cour et dont l'examen n'implique l'appréciation d'aucune circonstance de fait, doit être substitué au motif erroné en droit retenu, à titre principal, par l'arrêt attaqué pour justifier que soit écarté comme inopérant le moyen tiré de la méconnaissance des dispositions de l'article 24 de la loi du 12 avril 2000. <br/>
<br/>
              7. En second lieu, en estimant que la tentative de suicide commise, au domicile de MmeB..., par l'adolescent qui lui avait été confié le 13 mars 2012 et qui a absorbé le 18 mars des médicaments dont il avait pu garder la libre disposition, alors qu'elle-même avait informé simultanément, le 19 mars, les autorités du département tant de l'accueil de cet adolescent que de cet accident, était de nature à justifier la suspension de son agrément, dans les conditions prévues par les dispositions précitées de l'article L. 421-6 du code de l'action sociale et des familles, la cour n'a pas commis d'erreur de droit et a exactement qualifié les faits de l'espèce.  <br/>
<br/>
              8. Il résulte de ce qui précède que Mme B...n'est pas fondée à demander l'annulation de l'arrêt attaqué.<br/>
<br/>
              9. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise sur ce fondement à la charge du département de la Drôme, qui n'est pas, dans la présente instance, la partie perdante. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre une somme à la charge de Mme B...au même titre.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de Mme B...est rejeté. <br/>
Article 2 : Les conclusions du département de la Drôme présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à Mme A...B...et au département de la Drôme. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-03-01-02-01-01-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. QUESTIONS GÉNÉRALES. MOTIVATION. MOTIVATION OBLIGATOIRE. MOTIVATION OBLIGATOIRE EN VERTU DES ARTICLES 1 ET 2 DE LA LOI DU 11 JUILLET 1979. DÉCISION RESTREIGNANT L'EXERCICE DES LIBERTÉS PUBLIQUES OU, DE MANIÈRE GÉNÉRALE, CONSTITUANT UNE MESURE DE POLICE. - SUSPENSION DE L'AGRÉMENT D'UN ASSISTANT MATERNEL OU FAMILIAL.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">01-03-03-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. PROCÉDURE CONTRADICTOIRE. CARACTÈRE NON OBLIGATOIRE. - SUSPENSION DE L'AGRÉMENT D'UN ASSISTANT MATERNEL OU FAMILIAL.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">04 AIDE SOCIALE. - PROFESSIONS ET ACTIVITÉS D'ACCUEIL - ASSISTANTS MATERNELS - AGRÉMENT PAR LE PRÉSIDENT DU CONSEIL GÉNÉRAL - SUSPENSION - 1) OBLIGATION DE MOTIVATION DE LA MESURE - EXISTENCE - 2) OBLIGATION DE FAIRE PRÉCÉDER LA MESURE D'UNE PROCÉDURE CONTRADICTOIRE - ABSENCE.
</SCT>
<ANA ID="9A"> 01-03-01-02-01-01-01 La décision par laquelle l'autorité administrative prononce la suspension de l'agrément d'un assistant maternel ou familial constitue une mesure de police administrative prise dans l'intérêt des enfants accueillis. Si elle doit être motivée en vertu des dispositions spéciales de l'article L. 421-6 du code de l'action sociale et des familles, elle n'en relève pas moins du champ d'application de l'article 1er de la loi n° 79-587 du 11 juillet 1979, désormais codifié à l'article L. 211-2 du code des relations entre le public et l'administration (CRPA).</ANA>
<ANA ID="9B"> 01-03-03-02 Le législateur a entendu, par les articles L. 421-6 et L. 423-8 du code de l'action sociale et des familles, déterminer entièrement les règles de procédure auxquelles sont soumises les mesures de suspension de l'agrément des assistants maternels ou familiaux. Dès lors, l'article 24 de la loi n° 2000-321 du 12 avril 2000, qui fixe des règles générales de procédure applicables aux décisions devant être motivées en vertu de la loi n° 79-587 du 11 juillet 1979, ne saurait utilement être invoqué à l'encontre d'une telle mesure.</ANA>
<ANA ID="9C"> 04 1) La décision par laquelle l'autorité administrative prononce la suspension de l'agrément d'un assistant maternel ou familial constitue une mesure de police administrative prise dans l'intérêt des enfants accueillis. Si elle doit être motivée en vertu des dispositions spéciales de l'article L. 421-6 du code de l'action sociale et des familles, elle n'en relève pas moins du champ d'application de l'article 1er de la loi n° 79-587 du 11 juillet 1979, désormais codifié à l'article L. 211-2 du code des relations entre le public et l'administration (CRPA).... ,,2) Le législateur a toutefois entendu, par les articles L. 421-6 et L. 423-8 du code de l'action sociale et des familles, déterminer entièrement les règles de procédure auxquelles sont soumises les mesures de suspension de l'agrément des assistants maternels ou familiaux. Dès lors, l'article 24 de la loi n° 2000-321 du 12 avril 2000, qui fixe des règles générales de procédure applicables aux décisions devant être motivées en vertu de la loi n° 79-587 du 11 juillet 1979, ne saurait utilement être invoqué à l'encontre d'une telle mesure.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
