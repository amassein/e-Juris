<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042520614</ID>
<ANCIEN_ID>JG_L_2020_11_000000427750</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/52/06/CETATEXT000042520614.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 13/11/2020, 427750</TITRE>
<DATE_DEC>2020-11-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>427750</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SEVAUX, MATHONNET ; LE PRADO ; SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>Mme Louise Cadin</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:427750.20201113</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. E... B..., M. C... B..., Mme G... B... et Mme F... B... ont demandé au tribunal administratif de Marseille de condamner l'Assistance publique - hôpitaux de Marseille (AP-HM) à leur verser la somme de 622 437,51 euros en réparation du préjudice dont ils estiment que M. E... B... a été victime lors de sa prise en charge à l'hôpital de La Timone, ainsi que les sommes respectivement de 40 000 euros chacun en réparation des préjudices propres de ses parents, M. C... B... et Mme G... B..., et de 25 000 euros pour le préjudice propre de sa soeur, Mme F... B.... Par un jugement n° 1402188 du 27 février 2017, le tribunal administratif, après l'avoir appelé en la cause, a mis à la charge de l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) le versement de la somme de 101 200 euros aux ayants droit de M. E... B..., décédé en cours d'instance le 28 janvier 2015.<br/>
<br/>
              Par un arrêt n°s 17MA01859, 17MA01897 du 6 décembre 2018, la cour administrative d'appel de Marseille a, sur appels de l'ONIAM et de M. B... et autres, condamné l'AP-HM à verser les sommes de 66 120 euros aux ayants droit de M. E... B..., de 5 000 euros à M. et Mme C... B... et de 3 000 euros à Mme F... B..., ramené la somme mise à la charge de l'ONIAM par le tribunal à 44 080 euros, réformé le jugement du tribunal en ce qu'il avait de contraire à son arrêt et rejeté le surplus des conclusions des parties.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés au secrétariat de la section du contentieux du Conseil d'Etat les 6 février et 6 mai 2019, l'ONIAM demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il met sa charge le versement d'indemnités à M. B... et autres ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme D... A..., auditrice,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Sevaux, Mathonnet, avocat de l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales, à la SCP Spinosi, Sureau, avocat de M. B... et autres et à Me Le Prado, avocat de l'Assistance publique Hôpitaux de Marseille ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. B..., atteint d'une neurofibromatose de type II, maladie génétique évolutive, a été pris en charge le 18 octobre 2005 à l'hôpital de la Timone de l'Assistance publique - hôpitaux de Marseille (AP-HM), où une radiochirurgie a été pratiquée pour traiter le neurinome dont il était atteint. Immédiatement après cette opération, M. B... a totalement perdu l'audition de l'oreille droite et présenté des acouphènes ainsi qu'une paralysie faciale avec des troubles oculaires, du goût et de la déglutition. Il a demandé au tribunal administratif de Marseille, avec plusieurs de ses ayants droit, de condamner l'AP-HM à l'indemniser de ces divers préjudices. Par un jugement du 27 février 2017, le tribunal administratif, après avoir appelé l'ONIAM en cause, a mis à sa charge le versement de 101 200 euros aux ayants droit de M. B..., lui-même étant décédé en cours de procédure. Sur appel de l'ONIAM, la cour administrative d'appel de Marseille a, par un arrêt du 6 décembre 2018, ramené cette somme à 44 080 euros et condamné l'AP-HM à verser diverses sommes aux ayants droit de M. B.... L'ONIAM se pourvoit en cassation contre cet arrêt en tant qu'il met à sa charge une partie de l'indemnisation des préjudices. Par la voie d'un pourvoi provoqué, les ayants droit de M. B... demandent que, si l'arrêt est annulé en tant qu'il condamne l'ONIAM à réparer leurs préjudices au titre de la solidarité nationale, il soit également annulé en tant qu'il rejette le surplus de leur appel dirigé contre l'AP-HM.<br/>
<br/>
              2. Il résulte des dispositions combinées du II de l'article L. 1142-1 du code de la santé publique et de l'article D. 1142-1 du même code que l'ONIAM doit assurer, au titre de la solidarité nationale, la réparation des dommages résultant directement d'actes de prévention, de diagnostic ou de soins à la double condition qu'ils présentent un caractère d'anormalité au regard de l'état de santé du patient comme de l'évolution prévisible de cet état et que leur gravité excède le seuil défini à l'article D. 1142-1. La condition d'anormalité du dommage prévue par ces dispositions doit notamment être regardée comme remplie lorsque l'acte médical a entraîné des conséquences notablement plus graves que celles auxquelles le patient était exposé de manière suffisamment probable en l'absence de traitement. <br/>
<br/>
              3. En premier lieu, en estimant, ainsi qu'il résulte des termes mêmes de son arrêt, que la radiothérapie pratiquée le 18 octobre 2005 avait, en entraînant de manière immédiate une surdité totale de l'oreille droite, une paralysie de la face ainsi que divers troubles de la sensibilité, du goût, de l'odorat et de la déglutition, compte tenu du jeune âge de M. B..., de son état de santé antérieur et de ce que les neurinomes du type de celui dont il était atteint sont d'évolution lente chez les sujets jeunes, entraîné une survenue prématurée des troubles en question, la cour n'a pas dénaturé les pièces du dossier qui lui était soumis. En en déduisant que, eu égard à cette survenue prématurée, les conséquences de l'intervention devaient être regardées comme notablement plus graves que les troubles auxquels M.B... était exposé de manière suffisamment probable, alors même qu'il aurait été exposé à long terme à des troubles identiques par l'évolution prévisible de sa pathologie et que, par suite, la condition d'anormalité justifiant leur réparation par la solidarité nationale était remplie, la cour a exactement qualifié les faits qui lui étaient soumis et n'a pas commis d'erreur de droit.<br/>
<br/>
              4. En deuxième lieu, si l'ONIAM soutient, à titre subsidiaire, que la cour a entaché son arrêt d'erreur de droit en la condamnant à indemniser des troubles au-delà de la date à laquelle ceux-ci auraient, en l'absence d'intervention, naturellement résulté de l'évolution prévisible de la pathologie, il résulte des dispositions rappelées ci-dessus de l'article L. 1142-1 du code de la santé publique que celles-ci font obstacle, en l'absence de certitude quant au terme auquel ces troubles seraient apparus en l'absence d'accident, à ce que leur réparation par la solidarité nationale soit limitée jusqu'à une telle échéance.<br/>
<br/>
              5. Il résulte de tout ce qui précède que l'ONIAM n'est pas fondé à demander l'annulation de l'arrêt attaqué. Son pourvoi doit ainsi être rejeté ainsi que, par voie de conséquence, ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'ONIAM le versement d'une somme de 3 000 euros à M. B... et autres au titre des dispositions de l'article L.761-1 du code de justice administrative. En revanche, le surplus des conclusions de M. B... et autres, présenté par la voie du pourvoi provoqué, doit, dès lors que les conclusions du pourvoi de l'ONIAM ne sont pas accueillies, être rejeté comme irrecevable.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
		Article 1er : Le pourvoi de l'ONIAM est rejeté.<br/>
<br/>
Article 2 : L'ONIAM versera à M. B... et autres la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
		Article 3 : Le surplus des conclusions de M. B... et autres est rejeté.<br/>
<br/>
Article 4 : La présente décision sera notifiée à l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales et à M. C... B..., premier défendeur dénommé.<br/>
		Copie en sera adressée au ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-02-01-01-005-02 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICE PUBLIC DE SANTÉ. ÉTABLISSEMENTS PUBLICS D'HOSPITALISATION. RESPONSABILITÉ SANS FAUTE. ACTES MÉDICAUX. - PRISE EN CHARGE PAR LA SOLIDARITÉ NATIONALE DES CONSÉQUENCES ANORMALES ET GRAVES DES ACTES MÉDICAUX (II DE L'ART. L. 1142-1 DU CSP) - CONDITION D'ANORMALITÉ [RJ1] - TROUBLES, ENTRAÎNÉS PAR UN ACTE MÉDICAL, SURVENUS CHEZ UN PATIENT DE MANIÈRE PRÉMATURÉE - 1) EXISTENCE - 2) RÉPARATION LIMITÉE AU TERME AUQUEL CES TROUBLES SERAIENT APPARUS EN L'ABSENCE D'ACCIDENT - ABSENCE, FAUTE DE CERTITUDE SUR CETTE ÉCHÉANCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-04-04-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RÉPARATION. MODALITÉS DE LA RÉPARATION. SOLIDARITÉ. - PRISE EN CHARGE PAR LA SOLIDARITÉ NATIONALE DES CONSÉQUENCES ANORMALES ET GRAVES DES ACTES MÉDICAUX (II DE L'ART. L. 1142-1 DU CSP) - CONDITION D'ANORMALITÉ [RJ1] - TROUBLES, ENTRAÎNÉS PAR UN ACTE MÉDICAL, SURVENUS CHEZ UN PATIENT DE MANIÈRE PRÉMATURÉE - 1) EXISTENCE - 2) RÉPARATION LIMITÉE AU TERME AUQUEL CES TROUBLES SERAIENT APPARUS EN L'ABSENCE D'ACCIDENT - ABSENCE, FAUTE DE CERTITUDE SUR CETTE ÉCHÉANCE.
</SCT>
<ANA ID="9A"> 60-02-01-01-005-02 Il résulte des dispositions combinées du II de l'article L. 1142-1 du code de la santé publique (CSP) et de l'article D. 1142-1 du même code que l'Office national d'indemnisation des accidents médicaux (ONIAM) doit assurer, au titre de la solidarité nationale, la réparation des dommages résultant directement d'actes de prévention, de diagnostic ou de soins à la double condition qu'ils présentent un caractère d'anormalité au regard de l'état de santé du patient comme de l'évolution prévisible de cet état et que leur gravité excède le seuil défini à l'article D. 1142-1. La condition d'anormalité du dommage prévue par ces dispositions doit notamment être regardée comme remplie lorsque l'acte médical a entraîné des conséquences notablement plus graves que celles auxquelles le patient était exposé de manière suffisamment probable en l'absence de traitement.... ,,1) Il en va ainsi des troubles, entraînés par un acte médical, survenus chez un patient de manière prématurée, alors même que l'intéressé aurait été exposé à long terme à des troubles identiques par l'évolution prévisible de sa pathologie.,,,2) L'article L. 1142-1 du CSP fait obstacle, en l'absence de certitude quant au terme auquel des troubles seraient apparus en l'absence d'accident, à ce que leur réparation par la solidarité nationale soit limitée jusqu'à une telle échéance.</ANA>
<ANA ID="9B"> 60-04-04-01 Il résulte des dispositions combinées du II de l'article L. 1142 1 du code de la santé publique (CSP) et de l'article D. 1142-1 du même code que l'Office national d'indemnisation des accidents médicaux (ONIAM) doit assurer, au titre de la solidarité nationale, la réparation des dommages résultant directement d'actes de prévention, de diagnostic ou de soins à la double condition qu'ils présentent un caractère d'anormalité au regard de l'état de santé du patient comme de l'évolution prévisible de cet état et que leur gravité excède le seuil défini à l'article D. 1142-1. La condition d'anormalité du dommage prévue par ces dispositions doit notamment être regardée comme remplie lorsque l'acte médical a entraîné des conséquences notablement plus graves que celles auxquelles le patient était exposé de manière suffisamment probable en l'absence de traitement.... ,,1) Il en va ainsi des troubles, entraînés par un acte médical, survenus chez un patient de manière prématurée, alors même que l'intéressé aurait été exposé à long terme à des troubles identiques par l'évolution prévisible de sa pathologie.,,,2) L'article L. 1142-1 du CSP fait obstacle, en l'absence de certitude quant au terme auquel des troubles seraient apparus en l'absence d'accident, à ce que leur réparation par la solidarité nationale soit limitée jusqu'à une telle échéance.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 12 décembre 2014, Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales c/ M. Bondoni, n° 355052, p. 385 ; CE, 15 octobre 2018, M. Chappaz, n° 409585, T. p. 904.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
