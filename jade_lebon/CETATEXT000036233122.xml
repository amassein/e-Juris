<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036233122</ID>
<ANCIEN_ID>JG_L_2017_12_000000398858</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/23/31/CETATEXT000036233122.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 5ème chambres réunies, 18/12/2017, 398858</TITRE>
<DATE_DEC>2017-12-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>398858</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>Mme Sara-Lou Gerber</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:398858.20171218</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 18 avril et 19 juillet 2016 et le 12 avril 2017 au secrétariat du contentieux du Conseil d'Etat, la Confédération générale du travail - Force ouvrière, la fédération nationale de l'action sociale - CGT Force ouvrière et l'union nationale des syndicats de la santé privée Force ouvrière demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 4 février 2016 de la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social portant extension d'un accord conclu dans le secteur sanitaire, social et médico-social à but non lucratif ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au bénéfice de chaque fédération requérante au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule ;<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sara-Lou Gerber, auditeur,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de la Confédération générale du travail - Force ouvrière et autres et à la SCP Célice, Soltner, Texidor, Perier, avocat de l'union des fédérations et syndicats nationaux d'employeurs du secteur sanitaire, médico-social et social privé à but non lucratif (UNIFED) ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier que l'union des fédérations et syndicats nationaux d'employeurs du secteur sanitaire, médico-social et social privé à but non lucratif (UNIFED) a conclu le 7 mai 2015 avec la fédération nationale des syndicats des services de santé et services sociaux CFDT un accord relatif à la formation professionnelle dans la branche sanitaire, sociale et médico-sociale privée à but non lucratif ; que cet accord prévoit en particulier, à ses articles 17.3, 20.5, 20.6 et 20.9, que la répartition des droits de vote entre organisations syndicales au sein des collèges salariés, d'une part de la " commission paritaire nationale de l'emploi et de la formation professionnelle " créée par cet accord, d'autre part des trois conseils d'administration des organismes paritaires collecteurs agréés de la branche et de leurs délégations paritaires régionales, s'effectue en fonction de leur audience dans la branche ; que la Confédération générale du travail - Force ouvrière et autres demandent l'annulation, pour excès de pouvoir, de l'arrêté du 4 février 2016 par lequel la ministre du travail, de l'emploi, du dialogue social et de la formation professionnelle a procédé à l'extension de cet accord, en tant qu'il procède à l'extension des stipulations qui viennent d'être citées ; <br/>
<br/>
              Sur la légalité externe de l'arrêté attaqué :<br/>
<br/>
              2. Considérant qu'il résulte des dispositions de l'article L. 2261-27 du code du travail que lorsque l'avis motivé favorable de la Commission nationale de la négociation collective sur une convention ou un accord soumis à la procédure d'extension fait l'objet de l'opposition écrite et motivée de deux organisations d'employeurs ou de deux organisations de salariés représentées à cette commission, le ministre chargé du travail doit, s'il entend poursuivre la procédure d'extension, consulter à nouveau la commission " sur la base d'un rapport précisant la portée des dispositions en cause ainsi que les conséquences d'une éventuelle extension " ; que le ministre chargé du travail peut décider l'extension, au vu du nouvel avis émis par la commission ;<br/>
<br/>
              3. Considérant, d'une part, qu'il ressort des pièces du dossier que deux syndicats membres de la Commission nationale de la négociation collective ont, à la suite de l'avis favorable donné par la sous-commission des conventions et accords, le 3 novembre 2015, à l'extension de l'accord du 7 mai 2015, exprimé leur opposition à l'extension ; que le rapport relatif à l'extension de l'accord, au vu duquel la sous-commission des conventions et accords a de nouveau délibéré lors de sa séance du 8 décembre 2015, tout en précisant que " les motifs d'opposition ne portent pas sur la légalité de l'avenant ", rappelle de manière détaillée les motifs de l'opposition des deux organisations syndicales, lesquels tenaient à la portée et aux conséquences prêtées par elles aux stipulations en cause ; que, par suite, le moyen tiré de ce que le contenu du rapport ne serait pas conforme aux dispositions l'article L. 2261-27 du code du travail doit être écarté ; <br/>
<br/>
              4. Considérant, d'autre part, qu'il ressort des pièces du dossier que les deux avis rendus par la sous-commission des conventions et accords de la Commission nationale de la négociation au cours de ses séances du 3 novembre et du 8 décembre 2015 rappellent la procédure suivie et les conditions de négociation de l'accord et retracent le contenu des principales remarques formulées en séance par les participants ; que les requérantes ne sont, par suite, pas fondées à soutenir que la motivation de ces avis méconnaît les exigences de l'article L. 2261-27 du code du travail ; <br/>
<br/>
              Sur la légalité interne de l'arrêté attaqué :<br/>
<br/>
              5. Considérant que, lorsqu'à l'occasion d'un litige relevant de la compétence de la juridiction administrative, une contestation sérieuse s'élève sur la validité d'un arrêté prononçant l'extension ou l'agrément d'une convention ou d'un accord collectif de travail, il appartient au juge saisi de ce litige de surseoir à statuer jusqu'à ce que l'autorité judiciaire se soit prononcée sur la question préjudicielle que présente à juger cette contestation ; que toutefois, eu égard à l'exigence de bonne administration de la justice et aux principes généraux qui gouvernent le fonctionnement des juridictions, en vertu desquels tout justiciable a droit à ce que sa demande soit jugée dans un délai raisonnable, il en va autrement s'il apparaît manifestement, au vu d'une jurisprudence établie, que la contestation peut être accueillie par le juge saisi au principal ; <br/>
<br/>
              6. Considérant, en premier lieu, que les dispositions de l'article L. 6332-1 du code du travail prévoient que : " L'agrément est accordé aux organismes collecteurs paritaires en fonction : (...) 3° De leur mode de gestion paritaire (...) " ; que l'article R. 6332-4 du même code dispose que " (...) Le conseil d'administration de l'organisme collecteur paritaire agréé est composé d'un nombre égal de représentants des employeurs et des salariés désignés par les organisations signataires  " ; qu'enfin, l'article R. 6332-16 de ce code dispose que : " L'acte de constitution d'un organisme collecteur paritaire détermine son champ d'intervention géographique et professionnel ou interprofessionnel ainsi que les conditions de sa gestion. Il fixe notamment : 1° La composition et l'étendue des pouvoirs du conseil d'administration paritaire (...) " ; qu'il résulte de ces dispositions que, si le législateur a prévu le principe d'une gestion paritaire des organismes paritaires collecteurs, il appartient aux partenaires sociaux, gestionnaires de ces organismes, de déterminer, par l'accord collectif qui constitue l'organisme, les modalités selon lesquelles s'effectue la prise de décision en son sein, sous réserve que les collèges salarié et employeur disposent globalement du même nombre de voix ; qu'il en va de même pour les modalités de vote au sein du collège salariés de la " commission paritaire nationale de l'emploi et de la formation professionnelle ", dont l'existence, ainsi qu'il a été dit au point 1, résulte d'un accord des partenaires conventionnels de la branche ; que, par suite, le moyen tiré de ce que les stipulations litigieuses n'entreraient pas dans le champ de compétence des partenaires conventionnels, qui ne soulève pas une contestation sérieuse, ne peut qu'être écarté ;<br/>
<br/>
              7. Considérant, en deuxième lieu, que le moyen tiré de ce qu'en répartissant les droits de vote entre organisations syndicales en fonction de leur audience dans la branche, les stipulations litigieuses auraient porté atteinte aux droit syndical et au principe de participation des travailleurs à la détermination collective des conditions de travail, reconnus par les 6ème et 8ème alinéas du Préambule de la Constitution de 1946 ne soulève pas non plus une question sérieuse ; qu'il doit, par suite, être écarté ;<br/>
<br/>
              8. Considérant, enfin, qu'alors même que le critère d'audience retenu pour le partage des voix au sein des collèges salariés permettrait d'atteindre, dans certains des organismes concernés, la majorité qualifiée, prévue par les statuts pour les décisions les plus importantes, par l'alliance d'un seul syndicat de salariés avec l'ensemble des représentants des employeurs, le moyen tiré de ce que ce critère introduit, entre les syndicats de salariés, une différence de traitement qui méconnaît le principe d'égalité ne peut être regardé comme soulevant une contestation sérieuse ; qu'il ne peut, par suite, qu'être écarté ; <br/>
<br/>
              9. Considérant qu'il résulte de tout ce qui précède que la Confédération générale du travail - Force ouvrière et autres ne sont pas fondées à demander l'annulation de l'arrêté attaqué ;<br/>
<br/>
              10. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la Confédération générale du travail - Force ouvrière, de la fédération nationale de l'action sociale - CGT Force ouvrière et de l'union nationale des syndicats de la santé privée Force ouvrière une somme de 1 000 euros chacune à verser à l'UNIFED au titre des mêmes dispositions ; <br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La requête de la Confédération générale du travail - Force ouvrière et autres est rejetée.<br/>
Article 2 : La Confédération générale du travail - Force ouvrière, la fédération nationale de l'action sociale - CGT Force ouvrière et l'union nationale des syndicats de la santé privée Force ouvrière verseront chacune une somme de 1 000 euros à l'union des fédérations et syndicats nationaux d'employeurs du secteur sanitaire, médico-social et social privé à but non lucratif, au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la Confédération générale du travail - Force ouvrière, première dénommée pour l'ensemble des requérantes, à l'union des fédérations et syndicats nationaux d'employeurs du secteur sanitaire, médico-social et social privé à but non lucratif, à la fédération nationale des syndicats des services de la santé et services sociaux - CFDT et à la ministre du travail.<br/>
Copie en sera adressée à la fédération SUD - santé sociaux, à la fédération de la santé et de l'action sociale - CGT.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">66-09-01 TRAVAIL ET EMPLOI. FORMATION PROFESSIONNELLE. INSTITUTIONS ET PLANIFICATION DE LA FORMATION PROFESSIONNELLE. - ORGANISMES COLLECTEURS AGRÉÉS - COMPÉTENCE DES PARTENAIRES SOCIAUX GESTIONNAIRES DE CES ORGANISMES POUR DÉTERMINER LES RÈGLES DE PRISE DE DÉCISION EN LEUR SEIN - EXISTENCE, SOUS RÉSERVE QUE LES COLLÈGES SALARIÉ ET EMPLOYEUR DISPOSENT GLOBALEMENT DU MÊME NOMBRE DE VOIX - COMPÉTENCE POUR DÉFINIR LES MODALITÉS DE VOTE AU SEIN DU COLLÈGE SALARIÉ - EXISTENCE.
</SCT>
<ANA ID="9A"> 66-09-01 Il résulte des articles L. 6332-1 et R. 6332-16 du code du travail que, si le législateur a prévu le principe d'une gestion paritaire des organismes paritaires collecteurs, il appartient aux partenaires sociaux, gestionnaires de ces organismes, de déterminer, par l'accord collectif qui constitue l'organisme, les modalités selon lesquelles s'effectue la prise de décision en son sein, sous réserve que les collèges salarié et employeur disposent globalement du même nombre de voix. Il en va de même pour les modalités de vote au sein du collège salariés de la « commission paritaire nationale de l'emploi et de la formation professionnelle », dont l'existence résulte d'un accord des partenaires conventionnels de la branche.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
