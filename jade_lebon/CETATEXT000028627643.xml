<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028627643</ID>
<ANCIEN_ID>JG_L_2014_02_000000361769</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/62/76/CETATEXT000028627643.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 19/02/2014, 361769</TITRE>
<DATE_DEC>2014-02-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>361769</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Jean-Marie Deligne</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Claire Legras</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:361769.20140219</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 8 août 2012 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre de la culture et de la communication ; le ministre demande au Conseil d'Etat d'annuler l'arrêt n° 10VE02744 du 10 mai 2012 par lequel la cour administrative d'appel de Versailles a rejeté son recours tendant à l'annulation du jugement n° 071930 du 15 juin 2010 du tribunal administratif de Versailles annulant, sur demande de la commune de Linas, la décision du 11 octobre 2007 par laquelle le préfet de la région Ile-de-France a émis un avis défavorable sur la demande de permis de construire présentée par M. A..., maire de la commune de Linas ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marie Deligne, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Claire Legras, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la commune de Linas ;<br/>
<br/>
<br/>
<br/>1. Considérant que, lorsque la délivrance d'une autorisation administrative est subordonnée à l'accord préalable d'une autre autorité, le refus d'un tel accord, qui s'impose à l'autorité compétente pour statuer sur la demande d'autorisation, ne constitue pas une décision susceptible de recours ; qu'aux termes de l'article R. 421-38-4 du code de l'urbanisme, alors en vigueur et dont les dispositions ont été reprises, en substance, aux articles R. 423-68 et R. 424-14 du même code : " Lorsque la construction est située dans le champ de visibilité d'un édifice classé ou inscrit, le permis de construire ne peut être délivré qu'avec l'accord de l'architecte des bâtiments de France. (...) Le préfet de région est saisi  (...) soit : / a) Par le maire ou l'autorité compétente pour délivrer le permis de construire, dans le délai d'un mois à compter de la réception de l'avis émis par l'architecte des bâtiments de France ; /b) Par le pétitionnaire, dans le délai de deux mois à compter de la notification du refus de permis de construire. / (...) Le préfet de région émet, après consultation de la section de la commission régionale du patrimoine et des sites, un avis qui se substitue à celui de l'architecte des bâtiments de France (...) " ; qu'il résulte de ces dispositions que la délivrance d'un permis de construire est subordonnée, lorsque les travaux envisagés sont situés dans le champ de visibilité d'un édifice classé ou inscrit ou en co-visibilité avec celui-ci, à l'avis conforme de l'architecte des bâtiments de France ou, lorsque celui-ci a été saisi, du préfet de région ; que si l'avis de celui-ci se substitue alors à celui de l'architecte des bâtiments de France, l'ouverture d'un tel recours administratif, qui est un préalable obligatoire à toute contestation de la position ainsi prise au regard de la protection d'un édifice classé ou inscrit, n'a ni pour objet ni pour effet de permettre l'exercice d'un recours contentieux contre cet avis ; que la régularité et le bien-fondé de l'avis de l'architecte des bâtiments de France ou, le cas échéant, de la décision du préfet de région ne peuvent être contestés qu'à l'appui d'un recours pour excès de pouvoir dirigé contre la décision de refus du permis de construire et présenté par une personne ayant un intérêt pour agir ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. A...a déposé le 10 avril 2007 une demande de permis de construire pour l'extension de son habitation ; que, le projet étant situé dans le champ de visibilité d'un édifice classé, l'avis conforme de l'architecte des bâtiments de France a été sollicité ; que ce dernier ayant émis un avis défavorable le 15 mai 2007, la commune de Linas a saisi le préfet de la région Ile-de-France, en application de l'article R. 421-38-4 du code de l'urbanisme ; que, le 11 octobre 2007, le préfet de région a lui-même émis un avis défavorable sur la demande de permis de construire, qui s'est substitué à celui de l'architecte des bâtiments de France ; que la commune de Linas a demandé au tribunal administratif de Versailles l'annulation, pour excès de pouvoir, de cet avis défavorable ; que, par un jugement du 15 juin 2010, le tribunal a fait droit à la demande de la commune ; <br/>
<br/>
              3. Considérant que, pour confirmer ce jugement par l'arrêt attaqué du 10 mai 2012, la cour administrative d'appel de Versailles a jugé que l'avis défavorable rendu le 11 octobre 2007 par le préfet de la région Ile-de-France présentait le caractère d'une décision que la commune de Linas était recevable à déférer au juge de l'excès de pouvoir ; qu'il résulte de ce qui a été dit ci-dessus que la cour a, ce faisant, commis une erreur de droit ; que le ministre de la culture et de la communication est, dès lors, fondé a demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui vient d'être dit que c'est à tort que le tribunal administratif a fait droit à la demande de la commune de Linas tendant à l'annulation de l'avis défavorable rendu le 11 octobre 2007 par le préfet de la région Ile-de-France ;<br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Versailles du 10 mai 2012 et le jugement du tribunal administratif de Versailles du 15 juin 2010 sont annulés.<br/>
Article 2 : La demande présentée par la commune de Linas devant le tribunal administratif de Versailles est rejetée.<br/>
Article 3 : Les conclusions présentées par la commune de Linas au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la ministre de la culture et de la communication et à la commune de Linas.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">41-01-05-03 MONUMENTS ET SITES. MONUMENTS HISTORIQUES. MESURES APPLICABLES AUX IMMEUBLES SITUÉS DANS LE CHAMP DE VISIBILITÉ D'UN ÉDIFICE CLASSÉ OU INSCRIT. PERMIS DE CONSTRUIRE. - AVIS DE L'ABF - MISE EN PLACE D'UN RECOURS ADMINISTRATIF OBLIGATOIRE POUR TOUTE CONTESTATION DE LA POSITION PRISE AU REGARD DE LA PROTECTION DE L'ÉDIFICE CLASSÉ OU INSCRIT [RJ1] - PORTÉE - OUVERTURE D'UNE VOIE DE RECOURS DIRECT CONTRE L'AVIS DE L'ABF - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-02-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. LIAISON DE L'INSTANCE. RECOURS ADMINISTRATIF PRÉALABLE. - PERMIS DE CONSTRUIRE SOUMIS À AVIS FAVORABLE DE L'ABF - MISE EN PLACE D'UN RECOURS ADMINISTRATIF PRÉALABLE OBLIGATOIRE POUR TOUTE CONTESTATION DE LA POSITION PRISE AU REGARD DE LA PROTECTION DE L'ÉDIFICE CLASSÉ OU INSCRIT [RJ1] - PORTÉE - OUVERTURE D'UNE VOIE DE RECOURS DIRECT CONTRE L'AVIS DE L'ABF - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">68-06-01-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. INTRODUCTION DE L'INSTANCE. DÉCISION FAISANT GRIEF. - ABSENCE - AVIS DE L'ABF - MISE EN PLACE D'UN RECOURS ADMINISTRATIF PRÉALABLE OBLIGATOIRE POUR TOUTE CONTESTATION DE LA POSITION PRISE AU REGARD DE LA PROTECTION DE L'ÉDIFICE CLASSÉ OU INSCRIT [RJ1] - PORTÉE - OUVERTURE D'UNE VOIE DE RECOURS DIRECT CONTRE L'AVIS DE L'ABF - ABSENCE.
</SCT>
<ANA ID="9A"> 41-01-05-03 L'ancien article R. 421-38-4 du code de l'urbanisme, repris en substance aux articles R. 423-68 et R. 424-14 du même code, subordonne toute contestation de la position prise sur un permis de construire au regard de la protection d'un édifice classé ou inscrit à l'exercice préalable d'un recours administratif contre l'avis de l'architecte des bâtiments de France (ABF) devant le préfet de région.,,,L'ouverture d'un tel recours administratif n'a ni pour objet ni pour effet de permettre l'exercice d'un recours contentieux contre l'avis de l'ABF, dont la régularité et le bien-fondé, de même que ceux, le cas échéant, de la décision du préfet de région qui s'y substitue, ne peuvent être contestés qu'à l'appui d'un recours pour excès de pouvoir dirigé contre la décision de refus du permis de construire et présenté par une personne ayant un intérêt pour agir.</ANA>
<ANA ID="9B"> 54-01-02-01 L'ancien article R. 421-38-4 du code de l'urbanisme, repris en substance aux articles R. 423-68 et R. 424-14 du même code, subordonne toute contestation de la position prise sur un permis de construire au regard de la protection d'un édifice classé ou inscrit à l'exercice préalable d'un recours administratif contre l'avis de l'architecte des bâtiments de France (ABF) devant le préfet de région.,,,L'ouverture d'un tel recours administratif n'a ni pour objet ni pour effet de permettre l'exercice d'un recours contentieux contre l'avis de l'ABF, dont la régularité et le bien-fondé, de même que ceux, le cas échéant, de la décision du préfet de région qui s'y substitue, ne peuvent être contestés qu'à l'appui d'un recours pour excès de pouvoir dirigé contre la décision de refus du permis de construire et présenté par une personne ayant un intérêt pour agir.</ANA>
<ANA ID="9C"> 68-06-01-01 L'ancien article R. 421-38-4 du code de l'urbanisme, repris en substance aux articles R. 423-68 et R. 424-14 du même code, subordonne toute contestation de la position prise sur un permis de construire au regard de la protection d'un édifice classé ou inscrit à l'exercice préalable d'un recours administratif contre l'avis de l'architecte des bâtiments de France (ABF) devant le préfet de région.,,,L'ouverture d'un tel recours administratif n'a ni pour objet ni pour effet de permettre l'exercice d'un recours contentieux contre l'avis de l'ABF, dont la régularité et le bien-fondé, de même que ceux, le cas échéant, de la décision du préfet de région qui s'y substitue, ne peuvent être contestés qu'à l'appui d'un recours pour excès de pouvoir dirigé contre la décision de refus du permis de construire et présenté par une personne ayant un intérêt pour agir.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 30 juin 2010, SARL Château d'Epinay, n° 334747, T. pp. 861-888-1021.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
