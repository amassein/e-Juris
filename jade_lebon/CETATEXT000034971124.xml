<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034971124</ID>
<ANCIEN_ID>JG_L_2017_06_000000389868</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/97/11/CETATEXT000034971124.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 19/06/2017, 389868</TITRE>
<DATE_DEC>2017-06-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389868</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>DELAMARRE ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Vincent Villette</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:389868.20170619</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé à la Cour nationale du droit d'asile d'annuler la décision du 14 juin 2011 par laquelle le directeur général de l'Office français de protection des réfugiés et apatrides (OFPRA) a rejeté sa demande d'asile. Par une décision n° 11015942 du 27 février 2015, la Cour nationale du droit d'asile, faisant droit à la demande que lui avait présentée M.A..., a annulé cette décision du 14 juin 2011 du directeur général de l'OFPRA et lui a reconnu la qualité de réfugié. <br/>
<br/>
              Par un pourvoi sommaire et deux mémoires complémentaires, enregistrés les 30 avril, 30 juillet et 24 décembre 2015 au secrétariat du contentieux du Conseil d'Etat, l'OFPRA demande au Conseil d'Etat d'annuler cette décision n° 11015942 du 27 février 2015 de la Cour nationale du droit d'asile. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention de Genève du 28 juillet 1951 relative au statut des réfugiés et le protocole signé à New York le 31 janvier 1967 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
              - la loi n° 2015-925 du 29 juillet 2015 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Villette, auditeur,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de l'Office français de protection des réfugiés et apatrides et à Me Delamarre, avocat de M. B...A...;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Par une décision du 14 juin 2011, l'Office français de protection des réfugiés et apatrides (OFPRA) a refusé à M.A..., ressortissant sri-lankais, la qualité de réfugié. Par une décision du 27 février 2015, la Cour nationale du droit d'asile a annulé cette décision et reconnu à M. A... la qualité de réfugié. L'OFPRA se pourvoit en cassation contre cet arrêt.   <br/>
<br/>
              2. D'une part, aux termes des stipulations du paragraphe A, 2°, de l'article 1er de la convention de Genève du 28 juillet 1951, doit être considéré comme réfugié toute personne qui " craignant avec raison d'être persécutée du fait de sa race, de sa religion, de sa nationalité, de son appartenance à un certain groupe social ou de ses opinions politiques, se trouve hors du pays dont elle a la nationalité et qui ne peut ou, du fait de cette crainte, ne veut se réclamer de la protection de ce pays " et aux termes de celles de l'article 1 F de la même convention, " Les dispositions de cette Convention ne seront pas applicables aux personnes dont on aura des raisons sérieuses de penser : a) qu'elles ont commis un crime contre la paix, un crime de guerre ou un crime contre l'humanité, au sens des instruments internationaux élaborés pour prévoir des dispositions relatives à ces crimes ; b) qu'elles ont commis un crime grave de droit commun en dehors du pays d'accueil avant d'y être admises comme réfugiés ; c) qu'elles se sont rendues coupables d'agissements contraires aux buts et aux principes des Nations Unies. "<br/>
<br/>
              3. D'autre part, il appartient au juge administratif, dans l'exercice de ses pouvoirs généraux de direction de la procédure, d'ordonner toutes les mesures d'instruction qu'il estime nécessaires à la solution des litiges qui lui sont soumis, et notamment de requérir des parties ainsi que, le cas échéant, de tiers, en particulier des administrations compétentes, la communication des documents qui lui permettent de vérifier les allégations des requérants et d'établir sa conviction. Il lui incombe, dans la mise en oeuvre de ses pouvoirs d'instruction, de veiller au respect des droits des parties, d'assurer l'égalité des armes entre elles et de garantir, selon les modalités propres à chacun d'entre eux, les secrets protégés par la loi. Le caractère contradictoire de la procédure fait en principe obstacle à ce que le juge se fonde sur des pièces produites au cours de l'instance qui n'auraient pas été préalablement communiquées à chacune des parties. Toutefois, avant même l'intervention de la loi du 29 juillet 2015 relative à la réforme du droit d'asile qui a créé l'article L. 733-4 du code de l'entrée et du séjour des étrangers et du droit d'asile, l'OFPRA pouvait refuser de révéler l'identité des personnes ou des organisations ayant fourni les informations qu'il verse au contradictoire, lorsqu'une telle divulgation aurait été de nature à compromettre la sécurité de ces sources. Dans cette hypothèse, le juge tient compte des informations en cause, mais ne saurait s'appuyer exclusivement sur elles pour fonder sa décision.<br/>
<br/>
              4. Il résulte des pièces de la procédure que, pour soutenir que M. A... se serait rendu coupable, comme auteur ou complice, à titre personnel, d'un des agissements visés à l'article 1er F de la Convention de Genève, l'OFPRA a, devant la Cour nationale du droit d'asile, versé, parmi d'autres éléments, une note rédigée par sa division de l'information, de la documentation et des recherches, comportant des informations relatives à l'implication de M. A...au sein des unités combattantes du mouvement séparatiste des " tigres libérateurs de l'Eelam tamoul ", notamment dans la préparation d'attentats. L'OFPRA, pour ne pas compromettre la sécurité des personnes ayant fourni ces informations, a refusé de divulguer leur identité dans le cadre du débat contradictoire. <br/>
<br/>
              5. Il ressort des énonciations de la décision attaquée que la cour a estimé pouvoir prendre en compte cette note tout en refusant de se fonder exclusivement sur les informations qu'elle contenait, dès lors que leur source était restée confidentielle à l'égard du requérant. Toutefois, la cour s'est d'abord prononcée au vu de l'ensemble des autres pièces de son dossier avant d'en déduire, faute d'avoir identifié suffisamment d'éléments constituant des raisons sérieuses de penser que le demandeur se serait rendu coupable, comme auteur ou complice, à titre personnel, d'un des agissements visés à l'article 1 F de la Convention de Genève, qu'elle ne pouvait, par suite, prendre en considération la note de la division de l'information, de la documentation et des recherches de l'OFPRA, pour opposer la clause d'exclusion du bénéfice de la protection conventionnelle à M. A.... En fondant ainsi sa décision uniquement au regard des seules pièces dont la source était connue et en s'interdisant de prendre en compte, dans son appréciation globale, la note litigieuse, la cour a entaché sa décision d'erreur de droit. L'OFPRA est fondé, pour ce motif et sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, à en demander l'annulation.  <br/>
<br/>
              6. Les dispositions des articles L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'OFPRA qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La décision de la Cour nationale du droit d'asile du 27 février 2015 est annulée. <br/>
Article 2 : L'affaire est renvoyée à la Cour nationale du droit d'asile.<br/>
<br/>
Article 3 : Les conclusions présentées par M. A...au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 sont rejetées. <br/>
Article 4 : La présente décision sera notifiée à l'Office français de protection des réfugiés et apatrides et à M. B...A.... <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">095-04-01-01-02 - APPRÉCIATION - CAS OÙ EST TRANSMISE À LA CNDA UNE NOTE DONT LA SOURCE EST DEMEURÉE CONFIDENTIELLE VIS-À-VIS DU DEMANDEUR D'ASILE [RJ1] - 1) POSSIBILITÉ POUR LE JUGE DE SE FONDER EXCLUSIVEMENT SUR LES ÉLÉMENTS CONTENUS DANS UN TEL DOCUMENT POUR FAIRE JOUER LA CLAUSE D'EXCLUSION - ABSENCE - OBLIGATION DE PRENDRE EN COMPTE LES ÉLÉMENTS CONTENUS DANS UN TEL DOCUMENT DANS LE CADRE DE SON APPRÉCIATION GLOBALE - EXISTENCE - 2) ESPÈCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">095-08-02-03-01 - APPLICATION DE LA CLAUSE D'EXCLUSION DU BÉNÉFICE DE LA PROTECTION CONVENTIONNELLE (ART. 1ER, F DE LA CONVENTION DE GENÈVE) - CAS OÙ EST TRANSMISE À LA CNDA UNE NOTE DONT LA SOURCE EST DEMEURÉE CONFIDENTIELLE VIS-À-VIS DU DEMANDEUR D'ASILE [RJ1] - 1) POSSIBILITÉ POUR LE JUGE DE SE FONDER EXCLUSIVEMENT SUR LES ÉLÉMENTS CONTENUS DANS UN TEL DOCUMENT POUR FAIRE JOUER LA CLAUSE D'EXCLUSION - ABSENCE - OBLIGATION DE PRENDRE EN COMPTE LES ÉLÉMENTS CONTENUS DANS UNE TEL DOCUMENT DANS LE CADRE DE SON APPRÉCIATION GLOBALE - EXISTENCE - 2) ESPÈCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-04-03-01 PROCÉDURE. INSTRUCTION. CARACTÈRE CONTRADICTOIRE DE LA PROCÉDURE. COMMUNICATION DES MÉMOIRES ET PIÈCES. - CAS OÙ EST TRANSMISE À LA CNDA UNE NOTE DONT LA SOURCE EST DEMEURÉE CONFIDENTIELLE VIS-À-VIS DU DEMANDEUR D'ASILE [RJ1] - 1)  POSSIBILITÉ POUR LE JUGE DE SE FONDER EXCLUSIVEMENT SUR LES ÉLÉMENTS CONTENUS DANS UN TEL DOCUMENT POUR FAIRE JOUER LA CLAUSE D'EXCLUSION DU BÉNÉFICE DE LA PROTECTION CONVENTIONNELLE (ART. 1ER, F DE LA CONVENTION DE GENÈVE) - ABSENCE - OBLIGATION DE PRENDRE EN COMPTE LES ÉLÉMENTS CONTENUS DANS UNE TEL DOCUMENT DANS LE CADRE DE SON APPRÉCIATION GLOBALE - EXISTENCE - 2) ESPÈCE.
</SCT>
<ANA ID="9A"> 095-04-01-01-02 1) Le caractère contradictoire de la procédure fait en principe obstacle à ce que le juge se fonde sur des pièces produites au cours de l'instance qui n'auraient pas été préalablement communiquées à chacune des parties. Toutefois, avant même l'intervention de la loi n° 2015-925 du 29 juillet 2015 relative à la réforme du droit d'asile qui a créé l'article L. 733-4 du code de l'entrée et du séjour des étrangers et du droit d'asile, l'OFPRA pouvait refuser de révéler l'identité des personnes ou des organisations ayant fourni les informations qu'il verse au contradictoire, lorsqu'une telle divulgation aurait été de nature à compromettre la sécurité de ces sources. Dans cette hypothèse, le juge tient compte des informations en cause, mais ne saurait s'appuyer exclusivement sur elles pour fonder sa décision.,,,2) OFPRA ayant versé devant la CNDA, parmi d'autres éléments, pour soutenir qu'un demandeur d'asile se serait rendu coupable, comme auteur ou complice, d'agissements visés à l'article 1er F de la Convention de Genève, une note comportant des informations sur son implication au sein des unités combattantes d'une organisation séparatiste et dans la préparation d'attentats et ayant refusé de divulguer dans le cadre du contradictoire l'identité des personnes ayant fourni ces informations, afin de ne pas compromettre leur sécurité. La cour a estimé pouvoir prendre en compte cette note tout en refusant de se fonder exclusivement sur les informations qu'elle contenait dès lors que leur source était restée confidentielle à l'égard du requérant. Toutefois, en se prononçant au vu de l'ensemble des autres pièces du dossier pour en déduire, faute d'avoir identifié suffisamment d'éléments constituant des raisons sérieuses de penser que le demandeur se serait rendu coupable d'un des agissements visés à l'article 1er F de la Convention de Genève, qu'elle ne pouvait, par suite, prendre en considération les informations contenues dans la note litigieuse pour opposer la clause d'exclusion du bénéfice de la protection conventionnelle, la cour a fondé sa décision sur les seuls documents dont la source était connue en s'interdisant de prendre en compte les informations contenues dans la note litigieuse. En statuant ainsi, elle a commis une erreur de droit.</ANA>
<ANA ID="9B"> 095-08-02-03-01 1) Le caractère contradictoire de la procédure fait en principe obstacle à ce que le juge se fonde sur des pièces produites au cours de l'instance qui n'auraient pas été préalablement communiquées à chacune des parties. Toutefois, avant même l'intervention de la loi n° 2015-925 du 29 juillet 2015 relative à la réforme du droit d'asile qui a créé l'article L. 733-4 du code de l'entrée et du séjour des étrangers et du droit d'asile, l'OFPRA pouvait refuser de révéler l'identité des personnes ou des organisations ayant fourni les informations qu'il verse au contradictoire, lorsqu'une telle divulgation aurait été de nature à compromettre la sécurité de ces sources. Dans cette hypothèse, le juge tient compte des informations en cause, mais ne saurait s'appuyer exclusivement sur elles pour fonder sa décision.,,,2) OFPRA ayant versé devant la CNDA, parmi d'autres éléments, pour soutenir qu'un demandeur d'asile se serait rendu coupable, comme auteur ou complice, d'agissements visés à l'article 1er F de la Convention de Genève, une note comportant des informations sur son implication au sein des unités combattantes d'une organisation séparatiste et dans la préparation d'attentats et ayant refusé de divulguer dans le cadre du contradictoire l'identité des personnes ayant fourni ces informations, afin de ne pas compromettre leur sécurité. La cour a estimé pouvoir prendre en compte cette note tout en refusant de se fonder exclusivement sur les informations qu'elle contenait dès lors que leur source était restée confidentielle à l'égard du requérant. Toutefois, en se prononçant au vu de l'ensemble des autres pièces du dossier pour en déduire, faute d'avoir identifié suffisamment d'éléments constituant des raisons sérieuses de penser que le demandeur se serait rendu coupable d'un des agissements visés à l'article 1er F de la Convention de Genève, qu'elle ne pouvait, par suite, prendre en considération les informations contenues dans la note litigieuse pour opposer la clause d'exclusion du bénéfice de la protection conventionnelle, la cour a fondé sa décision sur les seuls documents dont la source était connue en s'interdisant de prendre en compte les informations contenues dans la note litigieuse. En statuant ainsi, elle a commis une erreur de droit.</ANA>
<ANA ID="9C"> 54-04-03-01 1) Le caractère contradictoire de la procédure fait en principe obstacle à ce que le juge se fonde sur des pièces produites au cours de l'instance qui n'auraient pas été préalablement communiquées à chacune des parties. Toutefois, avant même l'intervention de la loi n° 2015-925 du 29 juillet 2015 relative à la réforme du droit d'asile qui a créé l'article L. 733-4 du code de l'entrée et du séjour des étrangers et du droit d'asile, l'OFPRA pouvait refuser de révéler l'identité des personnes ou des organisations ayant fourni les informations qu'il verse au contradictoire, lorsqu'une telle divulgation aurait été de nature à compromettre la sécurité de ces sources. Dans cette hypothèse, le juge tient compte des informations en cause, mais ne saurait s'appuyer exclusivement sur elles pour fonder sa décision.,,,2) OFPRA ayant versé devant la CNDA, parmi d'autres éléments, pour soutenir qu'un demandeur d'asile se serait rendu coupable, comme auteur ou complice, d'agissements visés à l'article 1er F de la Convention de Genève, une note comportant des informations sur son implication au sein des unités combattantes d'une organisation séparatiste et dans la préparation d'attentats et ayant refusé de divulguer dans le cadre du contradictoire l'identité des personnes ayant fourni ces informations, afin de ne pas compromettre leur sécurité. La cour a estimé pouvoir prendre en compte cette note tout en refusant de se fonder exclusivement sur les informations qu'elle contenait dès lors que leur source était restée confidentielle à l'égard du requérant. Toutefois, en se prononçant au vu de l'ensemble des autres pièces du dossier pour en déduire, faute d'avoir identifié suffisamment d'éléments constituant des raisons sérieuses de penser que le demandeur se serait rendu coupable d'un des agissements visés à l'article 1er F de la Convention de Genève, qu'elle ne pouvait, par suite, prendre en considération les informations contenues dans la note litigieuse pour opposer la clause d'exclusion du bénéfice de la protection conventionnelle, la cour a fondé sa décision sur les seuls documents dont la source était connue en s'interdisant de prendre en compte les informations contenues dans la note litigieuse. En statuant ainsi, elle a commis une erreur de droit.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., en précisant s'agissant du caractère contradictoire de la procédure, CE, Section, 1er octobre 2014, M.,,  n° 349560, p. 288.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
