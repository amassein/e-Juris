<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033163044</ID>
<ANCIEN_ID>JG_L_2016_09_000000389448</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/16/30/CETATEXT000033163044.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 28/09/2016, 389448, Publié au recueil Lebon</TITRE>
<DATE_DEC>2016-09-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389448</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>M. Vincent Villette</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:389448.20160928</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés le 13 avril et le 13 juillet 2015 au secrétariat du contentieux du Conseil d'Etat, le Théâtre national de Bretagne demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler la délibération n° 2015-040 de la formation restreinte de la Commission nationale de l'informatique et des libertés (CNIL) du 12 février 2015 ;<br/>
<br/>
              2°) de mettre à la charge de la CNIL la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la loi n° 78-17 du 6 janvier 1978 ; <br/>
              - le décret n° 2005-1309 du 20 octobre 2005 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Villette, auditeur,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat du Théâtre national de Bretagne ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 21 septembre 2016, présentée par la CNIL ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que, par délibération en date du 12 février 2015, la formation restreinte de la Commission nationale de l'informatique et des libertés (CNIL), saisie de manquements à la loi du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés en raison d'un message électronique envoyé par le directeur du Théâtre national de Bretagne (TNB) à certains abonnés de ce théâtre, a prononcé, à son encontre, un avertissement et a ordonné que cette sanction fasse l'objet d'une publication sur le site internet de la CNIL et sur le site Légifrance ; que le théâtre demande l'annulation de cette délibération ; <br/>
<br/>
              Sur les conclusions dirigées contre la sanction d'avertissement :<br/>
<br/>
              2. Considérant que, contrairement à ce que soutient le requérant, la délibération attaquée, qui énonce les éléments de fait et de droit sur lesquels elle se fonde, est suffisamment motivée au regard des exigences de l'article 78 du décret du 20 octobre 2005 pris pour l'application de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, modifiée par la loi n° 2004-801 du 6 août 2004 ; <br/>
<br/>
              3. Considérant qu'aux termes des dispositions de l'article 6 de la loi du 6 janvier 1978 : " Un traitement ne peut porter que sur des données à caractère personnel qui satisfont aux conditions suivantes :/ 1° Les données sont collectées et traitées de manière loyale et licite ;/ 2° Elles sont collectées pour des finalités déterminées, explicites et légitimes et ne sont pas traitées ultérieurement de manière incompatible avec ces finalités. (...) " ; <br/>
<br/>
              4. Considérant, en premier lieu, qu'il ressort de la délibération attaquée que la formation restreinte de la CNIL a, d'une part, relevé que le message électronique du directeur du TNB avait été adressé peu avant le premier tour des élections municipales, aux seuls abonnés rennais, en réaction à une tribune polémique publiée par un journal régional dans le cadre de cette campagne électorale, et que son titre, comme son contenu, valorisaient le bilan de la politique culturelle menée à Rennes ; que, dans ces conditions, c'est à bon droit que la formation restreinte a estimé que le courriel litigieux revêtait le caractère d'une communication politique ; <br/>
<br/>
              5. Considérant, en second lieu, que c'est sans commettre d'erreur de droit qu'après avoir rappelé que le traitement des adresses de messagerie électronique des abonnés au TNB n'avait été autorisé qu'aux seules fins de gestion de leurs abonnements et d'envoi d'informations culturelles, la formation restreinte a estimé que l'utilisation de ces adresses à des fins de communication politique méconnaissait les finalités du traitement, en violation du 2° de l'article 6 de la loi du 6 janvier 1978 ; <br/>
<br/>
              Sur les conclusions dirigées contre la sanction complémentaire de publication :<br/>
<br/>
              6. Considérant qu'aux termes de l'article 46 de la loi du 6 janvier 1978 : " La formation restreinte peut rendre publiques les sanctions qu'elle prononce. Elle peut également ordonner leur insertion dans des publications, journaux et supports qu'elle désigne aux frais des personnes sanctionnées " ; qu'aux termes de l'article 78 du décret du 20 octobre 2005 : " (...) Lorsque la formation restreinte décide de publier la décision de sanction, cette publication peut intervenir dès la notification de la décision de sanction à la personne concernée. La décision ainsi publiée indique qu'un recours est susceptible d'être exercé à son encontre devant le juge administratif " ; <br/>
<br/>
              7. Considérant, en premier lieu, que dès lors qu'elle prononce une sanction complémentaire de publication de sa décision de sanction, la formation restreinte de la CNIL doit être regardée comme ayant légalement admis les manquements qui la fondent et que, dans l'hypothèse où la sanction serait ultérieurement jugée illégale, les personnes sanctionnées pourraient obtenir, outre son annulation, l'indemnisation du préjudice né de sa publication antérieurement à la décision d'annulation ainsi que la publication, dans les mêmes conditions, des éventuelles décisions prononçant la suspension ou l'annulation de la sanction ; qu'il s'ensuit que, contrairement à ce qui est soutenu, la sanction attaquée ne méconnaît pas le principe de la présomption d'innocence garanti par les stipulations de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et par l'article 9 de la Déclaration de 1789 ; <br/>
<br/>
              8. Considérant, en deuxième lieu, que les moyens tirés de ce que les dispositions réglementaires précitées méconnaîtraient les articles 8 et 13 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et seraient entachées d'inconstitutionnalité ne sont pas assortis des précisions permettant d'en apprécier le bien-fondé ;<br/>
<br/>
              9. Considérant, en troisième lieu, que lorsque la CNIL prononce une sanction complémentaire de publication de sa décision de sanction, celle-là se trouve nécessairement soumise, et alors même que la loi ne le prévoirait pas expressément, au respect du principe de proportionnalité ; que la légalité de cette sanction s'apprécie, notamment, au regard du support de diffusion retenu et, le cas échéant, de la durée pendant laquelle cette publication est accessible de façon libre et continue ; <br/>
<br/>
              10. Considérant qu'en l'espèce, eu égard au retentissement causé par le courriel litigieux dans le débat politique local, la sanction complémentaire contestée, qui vise à renforcer le caractère dissuasif de la sanction principale en lui assurant une publicité à l'égard tant des destinataires du courriel que d'un public plus large, est justifiée, dans son principe, au regard de la gravité des manquements aux dispositions du 2° de l'article 6 de la loi du 6 janvier 1978 ; que, toutefois, si la délibération attaquée prévoit que cette publication est effectuée sur le site internet de la CNIL et sur le site Légifrance, elle ne précise pas la durée de son maintien en ligne sur ces deux sites ; qu'en omettant de fixer la durée pendant laquelle la publication de l'avertissement resterait accessible de manière non anonyme sur ces deux sites, la formation restreinte de la CNIL doit être regardée comme ayant infligé une sanction sans borne temporelle ; que, dans ces conditions, la sanction complémentaire est, dans cette mesure, excessive ; qu'il suit de là qu'elle doit être annulée seulement en tant qu'elle n'a pas fixé la durée de son maintien en ligne de manière non anonyme ;  <br/>
<br/>
              11. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La délibération du 12 février 2015 de la formation restreinte de la CNIL est annulée en tant qu'elle n'a pas fixé la durée de son maintien en ligne sur le site internet de la CNIL et sur le site Légifrance.<br/>
<br/>
Article 2 : La fixation de la durée pendant laquelle l'avertissement infligé au Théâtre national de Bretagne restera publié de manière non anonyme sur les sites internet de la CNIL et de Légifrance est renvoyée à la formation restreinte de la CNIL.<br/>
<br/>
Article 3 : Le surplus des conclusions de la requête est rejeté. <br/>
<br/>
Article 4 : L'Etat versera une somme de 2 000 euros au Théâtre national de Bretagne au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 5 : La présente décision sera notifiée au Théâtre national de Bretagne et à la Commission nationale de l'informatique et des libertés.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-07-10-03 DROITS CIVILS ET INDIVIDUELS. - SANCTION COMPLÉMENTAIRE DE PUBLICATION DE LA SANCTION - 1) CONDITION DE LÉGALITÉ - PROPORTIONNALITÉ DE LA DURÉE DE LA PUBLICATION - 2) ESPÈCE - ANNULATION D'UNE SANCTION COMPLÉMENTAIRE DE PUBLICATION EN TANT QU'ELLE NE FIXE PAS DE DURÉE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">59-02-02-03 RÉPRESSION. DOMAINE DE LA RÉPRESSION ADMINISTRATIVE RÉGIME DE LA SANCTION ADMINISTRATIVE. BIEN-FONDÉ. - SANCTION COMPLÉMENTAIRE DE PUBLICATION D'UNE SANCTION - 1) CONDITION DE LÉGALITÉ - PROPORTIONNALITÉ DE LA DURÉE DE LA PUBLICATION - 2) ESPÈCE - ANNULATION D'UNE SANCTION COMPLÉMENTAIRE DE PUBLICATION EN TANT QU'ELLE NE FIXE PAS DE DURÉE.
</SCT>
<ANA ID="9A"> 26-07-10-03 1) Lorsque la Commission nationale de l'informatique et des libertés (CNIL) prononce une sanction complémentaire de publication de sa décision de sanction, celle-là se trouve nécessairement soumise, et alors même que la loi ne le prévoirait pas expressément, au respect du principe de proportionnalité. La légalité de cette sanction s'apprécie, notamment, au regard du support de diffusion retenu et, le cas échéant, de la durée pendant laquelle cette publication est accessible de façon libre et continue....  ,,2) En l'espèce, la délibération infligeant une sanction complémentaire de publication, qui est justifiée dans son principe, prévoit que cette publication est effectuée sur le site internet de la CNIL et sur le site Légifrance sans préciser la durée de son maintien en ligne sur ces deux sites. La CNIL doit être regardée comme ayant infligé une sanction sans borne temporelle. Dans ces conditions, la sanction complémentaire est, dans cette mesure, excessive. Annulation de cette sanction complémentaire en tant qu'elle n'a pas fixé la durée de son maintien en ligne de manière non anonyme.</ANA>
<ANA ID="9B"> 59-02-02-03 1) Lorsque la Commission nationale de l'informatique et des libertés (CNIL) prononce une sanction complémentaire de publication de sa décision de sanction, celle-là se trouve nécessairement soumise, et alors même que la loi ne le prévoirait pas expressément, au respect du principe de proportionnalité. La légalité de cette sanction s'apprécie, notamment, au regard du support de diffusion retenu et, le cas échéant, de la durée pendant laquelle cette publication est accessible de façon libre et continue....  ,,2) En l'espèce, la délibération infligeant une sanction complémentaire de publication, qui est justifiée dans son principe, prévoit que cette publication est effectuée sur le site internet de la CNIL et sur le site Légifrance sans préciser la durée de son maintien en ligne sur ces deux sites. La CNIL doit être regardée comme ayant infligé une sanction sans borne temporelle. Dans ces conditions, la sanction complémentaire est, dans cette mesure, excessive. Annulation de cette sanction complémentaire en tant qu'elle n'a pas fixé la durée de son maintien en ligne de manière non anonyme.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
