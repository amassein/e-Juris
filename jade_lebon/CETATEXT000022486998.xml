<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000022486998</ID>
<ANCIEN_ID>JG_L_2010_07_000000320124</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/22/48/69/CETATEXT000022486998.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 09/07/2010, 320124</TITRE>
<DATE_DEC>2010-07-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>320124</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Arrighi de Casanova</PRESIDENT>
<AVOCATS>SCP VINCENT, OHL</AVOCATS>
<RAPPORTEUR>Mme Christine  Allais</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Geffray Edouard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2010:320124.20100709</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 27 août et 24 octobre 2008 au secrétariat du contentieux du Conseil d'Etat, présentés pour le GROUPEMENT AGRICOLE D'EXPLOITATION EN COMMUN (GAEC) DES SABLES, dont le siège est Grande Rue à Montureux-Les-Baulay (70500) ; le GAEC DES SABLES demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt du 26 juin 2008 par lequel la cour administrative d'appel de Nancy a annulé le jugement du 20 juillet 2006 du tribunal administratif de Besançon qui avait prononcé l'annulation de la décision du 1er décembre 2003 du préfet de la Haute-Saône lui ordonnant de rembourser les sommes indûment versées au titre de la prime de maintien des systèmes d'élevage extensifs (PMSEE) et a rejeté sa demande ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le règlement (CE) n° 746/96 du 24 avril 1996 ;<br/>
<br/>
              Vu le code rural ;<br/>
<br/>
              Vu le décret n° 98-196 du 20 mars 1998 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Christine Allais, chargée des fonctions de Maître des Requêtes,<br/>
<br/>
              - les observations de la SCP Vincent, Ohl, avocat du GROUPEMENT AGRICOLE D'EXPLOITATION EN COMMUN DES SABLES, <br/>
<br/>
              - les conclusions de M. Edouard Geffray, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Vincent, Ohl, avocat du GROUPEMENT AGRICOLE D'EXPLOITATION EN COMMUN DES SABLES ; <br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'aux termes du 1 de l'article 11 du règlement (CE) n° 746/96 de la Commission du 24 avril 1996 portant modalités d'application du règlement (CEE) n° 2078/92 du Conseil concernant des méthodes de production agricole compatibles avec les exigences de la protection de l'environnement ainsi que l'entretien de l'espace naturel : " Lorsque, pendant la période de son engagement, le bénéficiaire transfère tout ou partie de son exploitation à une autre personne, celle-ci peut reprendre l'engagement pour la période restant à courir. Si une telle reprise n'a pas lieu, le bénéficiaire est obligé de rembourser les aides perçues conformément à l'article 20 paragraphe 1. (...) " ; qu'aux termes de l'article 4 du décret du 20 mars 1998 instituant une prime au maintien des systèmes d'élevage extensifs : " Le bénéficiaire s'engage, pour chacune des cinq années à compter de la date de demande de la prime (...) à maintenir la surface toujours en herbe sur les mêmes parcelles pendant les cinq années (...) " ; qu'aux termes de l'article 7 du même décret : " Le bénéficiaire de la prime doit respecter ses engagements pendant au moins cinq ans. Il peut transférer tout ou partie de son engagement à une ou plusieurs autres personnes sous réserve que cette dernière ou ces dernières reprennent les engagements pour la période restant à courir. Si une telle reprise n'a pas lieu, le bénéficiaire est tenu de rembourser les primes perçues depuis le début de l'engagement nouveau. (...) " ; <br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une décision du préfet de la Haute-Saône du 6 novembre 1998, le GROUPEMENT AGRICOLE D'EXPLOITATION EN COMMUN (GAEC) DES SABLES a obtenu le bénéfice de la prime au maintien des systèmes d'élevage extensifs (PMSEE) pour une période de cinq ans, sous réserve du respect des engagements prévus par la réglementation relative à cette prime ; qu'en raison du non-respect de l'engagement relatif au maintien de la surface toujours en herbe, consécutif à un échange de parcelles avec un exploitant agricole non bénéficiaire de la prime, le préfet, qui avait antérieurement déchu le GAEC du droit au paiement des primes au titre des années 2000, 2001 et 2002, a, par une décision du 1er décembre 2003, confirmée le 13 janvier 2004 à la suite d'un recours gracieux formé par l'intéressé, ordonné la mise en recouvrement des sommes perçues au titre des annuités 1998 et 1999 de la prime, à hauteur de 10 935,16 euros ; que, sur la demande du GAEC DES SABLES, le tribunal administratif de Besançon a annulé cette décision par un jugement du 20 juillet 2006 ; que, alors que le tribunal administratif s'était fondé, pour faire droit aux conclusions du GAEC sur une fausse application par le préfet des dispositions de l'article 14 du décret du 20 mars 1998 relatives, notamment, aux conséquences à tirer du non-respect par le bénéficiaire de la PMSEE de ses engagements en matière de surfaces toujours en herbe, la cour administrative d'appel de Nancy, faisant droit par l'arrêt attaqué à la demande de l'administration tendant à ce que soit substituée, à cette base légale, celle de l'article 11 du règlement (CE) n° 746/96 de la Commission du 24 avril 1996 et de l'article 7 du décret du 20 mars 1998, a annulé ce jugement par un arrêt du 26 juin 2008 et rejeté la demande présentée par le GAEC DES SABLES ; <br/>
<br/>
              Considérant que la cour administrative d'appel de Nancy n'était pas tenue de répondre à l'argument tiré de ce que le GAEC DES SABLES avait informé les services de l'Etat de l'échange de surfaces auquel il avait procédé avec un autre exploitant agricole, cette circonstance étant sans incidence sur l'obligation de respecter les engagements souscrits et sur les conséquences du non-respect de ces derniers ; qu'elle a relevé que le changement des  surfaces en herbe au cours de la période d'engagement ressortait des propres écritures du requérant et ainsi suffisamment répondu au moyen tiré de ce que la diminution des surfaces en herbe mentionnée par l'administration était inexacte ; enfin, qu'en jugeant que le préfet était légalement fondé, en application des dispositions de l'article 11 du règlement communautaire du 24 avril 1996 et de l'article 7 du décret du 20 mars 1998, à faire procéder au remboursement des primes perçues par le GAEC DES SABLES, elle a implicitement mais nécessairement écarté la base légale primitivement retenue par l'administration ; qu'il s'ensuit que le GAEC DES SABLES n'est pas fondé  à soutenir que l'arrêt attaqué est entaché d'insuffisance de motivation ;<br/>
<br/>
              Considérant, en deuxième lieu, qu'en jugeant qu'il résulte des dispositions précitées de l'article 11 du règlement communautaire du 24 avril 1996, ainsi que de l'article 7 du décret du 20 mars 1998 qui en reproduit les termes, qu'en cas de transfert de tout ou partie de l'exploitation à une autre personne, le bénéficiaire de la prime est tenu de rembourser les aides perçues si les engagements ne sont pas repris par cette personne, la cour n'a pas commis d'erreur de droit ;<br/>
<br/>
              Considérant en troisième lieu, qu'il ressort des pièces du dossier soumis aux juges du fond que le GAEC DES SABLES a transféré, dans le cadre d'échanges de parcelles entre exploitants intervenus au cours de la période d'engagement, des parcelles primées à un autre exploitant agricole qui n'a pas repris les engagements souscrits par le groupement requérant au titre de ces parcelles ; qu'en jugeant que l'exploitation devait être regardée, pour l'application des dispositions précitées, comme constituée par l'ensemble des parcelles pour lesquelles la prime avait été demandée, qu'aucune compensation ne pouvait être opérée, pour apprécier le respect des engagements souscrits par le GAEC DES SABLES, entre les surfaces primées qui avaient été cédées et les surfaces acquises en échange de ces dernières et que, par suite, le préfet était légalement fondé à faire procéder au remboursement des primes perçues par le GAEC DES SABLES depuis le début de son engagement, la cour n'a entaché l'arrêt attaqué ni d'erreur de droit ni de dénaturation des pièces du dossier ;<br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que le GAEC DES SABLES n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque ; que, par suite, les conclusions qu'il a présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent qu'être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du GROUPEMENT AGRICOLE D'EXPLOITATION EN COMMUN DES SABLES est rejeté.<br/>
Article 2 : La présente décision sera notifiée au GROUPEMENT AGRICOLE D'EXPLOITATION EN COMMUN DES SABLES et au ministre de l'alimentation, de l'agriculture et de la pêche.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-07-01-05 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. SUBSTITUTION DE BASE LÉGALE. - OBLIGATION POUR LE JUGE DE SE PRONONCER SUR LE BIEN-FONDÉ DE LA BASE LÉGALE INITIALEMENT RETENUE - ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 54-07-01-05 Le juge peut procéder à une substitution de base légale sans se prononcer sur le bien-fondé de la base légale initialement retenue par l'administration.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur le cadre juridique, Section, 6 février 2004, Mme Hallal, n° 240560, p. 48, concl. De Silva.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
