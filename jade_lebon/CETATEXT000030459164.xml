<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030459164</ID>
<ANCIEN_ID>JG_L_2015_04_000000371042</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/45/91/CETATEXT000030459164.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème / 8ème SSR, 02/04/2015, 371042</TITRE>
<DATE_DEC>2015-04-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>371042</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème / 8ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MEIER-BOURDEAU, LECUYER ; SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>M. Christophe Pourreau</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:371042.20150402</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              Le 26 janvier 2007, la société Scagro a demandé au tribunal administratif d'Orléans d'annuler les titres de recette nos 2007000003 et 2007000007 à 2007000045, émis à son encontre par le directeur de l'Office national interprofessionnel de l'élevage et de ses productions (Oniep) le 23 novembre 2006, pour un montant global de 1 692 031,35 euros aux fins de remboursement de restitutions à l'exportation.<br/>
<br/>
              Par un jugement n° 07-446 du 5 mai 2008, le tribunal administratif d'Orléans a annulé les titres de recette nos 2007000011, 2007000012, 2007000020 à 2007000024, 2007000026, 2007000034 à 2007000037, 2007000041 et 2007000042, ainsi que le titre de recette n° 2007000003 en tant qu'il se rapporte aux déclarations d'exportation nos 791448 et 791673, et rejeté le surplus des conclusions de la société.<br/>
<br/>
              Par un premier arrêt n° 08NT01603, 08NT01726, 08NT01756 du 7 mai 2009, la cour administrative d'appel de Nantes, faisant droit à l'appel formé par l'Oniep et rejetant l'appel formé par la société Dawn Meats France, venant aux droits de la société Scago, a annulé le jugement du tribunal administratif d'Orléans, rejeté la demande de la société et jugé qu'il n'y avait pas lieu de statuer sur les conclusions à fins de sursis à exécution de ce jugement.<br/>
<br/>
              Par une décision n° 329508 du 28 juin 2010, le Conseil d'Etat, statuant au contentieux, faisant droit au pourvoi de la société Dawn Meats France, a annulé l'arrêt de la cour administrative d'appel de Nantes, lui a renvoyé l'affaire et a jugé qu'il n'y avait pas lieu de statuer sur les conclusions à fins de sursis à exécution de cet arrêt.<br/>
<br/>
              Par un second arrêt n° 10NT01655 du 7 juin 2013, la cour administrative d'appel de Nantes, faisant droit à l'appel formé par la société Dawn Meats France et rejetant l'appel formé par l'Oniep, a annulé les titres de recette nos 2007000007 à 2007000010, 2007000013 à 2007000019, 2007000025, 2007000027 à 2007000033, 2007000038 à 2007000040, 2007000043 à 2007000045, ainsi que le titre de recette n° 2007000003 en tant qu'il se rapporte aux déclarations d'exportation autres que les déclarations nos 791448 et 791673, et jugé qu'il n'y avait pas lieu de statuer sur les conclusions à fins de sursis à exécution du jugement du tribunal administratif d'Orléans.<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 8 août et 8 novembre 2013 au secrétariat du contentieux du Conseil d'Etat, l'établissement public national des produits de l'agriculture et de la mer (FranceAgriMer), venant aux droits de l'Oniep, demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt de la cour administrative d'appel de Nantes du 7 juin 2013 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel et de rejeter l'appel de la société Dawn Meats France ;<br/>
<br/>
              3°) de mettre à la charge de la société Dawn Meats France la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code civil ;<br/>
              - le règlement n° 3665/87 de la Commission du 27 novembre 1987 ;<br/>
              - le règlement n° 2988/95 du Conseil du 18 décembre 1995 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christophe Pourreau, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Meier-Bourdeau, Lecuyer, avocat de la l'établissement public national des produits de l'agriculture et de la mer (FranceAgriMer) et à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la société Dawn Meats France ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société Scagro a perçu des restitutions à l'exportation à taux différencié au titre d'exportations de viande bovine congelée à destination de la Russie auxquelles elle a procédé au cours des années 1996 à 1998 ; que, le 23 novembre 2006, à la suite de contrôles diligentés par la direction générale des douanes et des droits indirects, l'office national interprofessionnel de l'élevage et de ses productions (Oniep) a émis à l'encontre de la société 39 titres de recette nos 2007000007 à 2007000045 tendant au reversement des restitutions à l'exportation relatives à des déclarations d'exportation acceptées entre le 20 mai 1996 et le 4 juin 1998 ainsi qu'un titre de recette n° 2007000003 tendant au paiement de majorations relatives à certaines restitutions à l'exportation litigieuses ; qu'à la demande de la société Scagro, le tribunal administratif d'Orléans a annulé, par un jugement du 5 mai 2008, 14 des 39 titres de recette, ainsi que le titre de recette n° 2007000003 en tant qu'il prévoit des majorations se rapportant à des restitutions à l'exportation correspondant aux titres de recette ainsi annulés, et rejeté le surplus des conclusions de la demande de la société ; que, par un premier arrêt du 7 mai 2009, la cour administrative d'appel de Nantes, faisant droit à l'appel formé par l'Oniep et rejetant l'appel formé par la société Dawn Meats France, venant aux droits de la société Scagro, a annulé le jugement du tribunal administratif d'Orléans, rejeté la demande de la société et jugé qu'il n'y avait pas lieu de statuer sur les conclusions à fins de sursis à exécution de ce jugement ; que, par une décision du 28 juin 2010, le Conseil d'Etat, statuant au contentieux, a annulé cet arrêt ; que, par un second arrêt du 7 juin 2013, contre lequel l'établissement public national des produits de l'agriculture et de la mer (FranceAgriMer), venant aux droits de l'Oniep, se pourvoit en cassation, la cour administrative d'appel de Nantes a annulé les 25 autres titres de recette, ainsi que le titre de recette n° 2007000003 en tant qu'il prévoit des majorations se rapportant à des restitutions à l'exportation correspondant aux titres de recette ainsi annulés, et jugé, par l'article 3 de son arrêt, qu'il n'y avait pas lieu de statuer sur les conclusions à fins de sursis à exécution du jugement du tribunal administratif ; qu'eu égard aux moyens qu'il invoque, FranceAgriMer doit être regardé comme ne demandant l'annulation que des articles 1er, 2 et 4 de cet arrêt ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 16, paragraphe 1, du règlement de la Commission du 27 novembre 1987 portant modalités communes d'application du régime des restitutions à l'exportation pour les produits agricoles : " Dans le cas de différenciation du taux de la restitution selon la destination, le paiement de la restitution est subordonné aux conditions supplémentaires définies aux articles 17 et 18. " ; qu'aux termes de l'article 17 du même règlement : " 1. Le produit doit avoir été importé en l'état dans le pays tiers (...) pour lequel la restitution est prévue dans les douze mois suivant la date d'acceptation de la déclaration d'exportation (...). / 3. Le produit est considéré comme importé lorsque les formalités douanières de mise à la consommation dans le pays tiers ont été accomplies. " ; qu'aux termes de l'article 18 du même règlement : " 1. La preuve de l'accomplissement des formalités douanières de mise à la consommation est apportée : / a) par la production du document douanier ou de sa copie ou photocopie (...) / ou / b) par la production du certificat de dédouanement (...) / ou / c) par la production de tout autre document visé par les services douaniers du pays tiers concerné, comportant l'identification des produits et démontrant que ceux-ci ont été mis à la consommation dans ce pays tiers. (...) / 3. En outre, l'exportateur est tenu de présenter dans tous les cas une copie ou photocopie du document de transport. (...) " ; qu'aux termes de l'article 47 du même règlement : " (...) / 2. Le dossier pour le paiement de la restitution ou la libération de la garantie doit être déposé, sauf cas de force majeure, dans les douze mois suivant la date d'acceptation de la déclaration d'exportation. / (...) 4. Lorsque les documents exigés au titre de l'article 18 n'ont pas pu être produits dans le délai fixé au paragraphe 2, bien que l'exportateur ait fait diligence pour se les procurer et les communiquer dans ce délai, des délais supplémentaires peuvent lui être accordés pour la production de ces documents. (...) " ; qu'aux termes de l'article 1er, paragraphe 2, du règlement du Conseil du 18 décembre 1995 relatif à la protection des intérêts financiers des Communautés européennes : " Est constitutive d'une irrégularité toute violation d'une disposition du droit communautaire résultant d'un acte ou d'une omission d'un opérateur économique qui a ou aurait pour effet de porter préjudice au budget général des Communautés ou à des budgets gérés par celles-ci, soit par la diminution ou la suppression de recettes provenant des ressources propres perçues directement pour le compte des Communautés, soit par une dépense indue. " ; qu'aux termes de l'article 3, paragraphe 1, du même règlement : " Le délai de prescription des poursuites est de quatre ans à partir de la réalisation de l'irrégularité visée à l'article 1er paragraphe 1. Toutefois, les réglementations sectorielles peuvent prévoir un délai inférieur qui ne saurait aller en deçà de trois ans. (...) " ;<br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions que le délai de prescription des poursuites en matière de restitutions à l'exportation court à compter de la réalisation de l'irrégularité reprochée à l'opérateur ; que, lorsqu'elle tient à la méconnaissance des formalités douanières de mise à la consommation des produits exportés dans un pays tiers, l'irrégularité doit être regardée comme réalisée à l'expiration du délai imparti à l'opérateur pour produire les documents attestant de l'accomplissement de ces formalités, c'est-à-dire à l'expiration du délai de douze mois suivant la date d'acceptation des déclarations d'exportation des produits ou, si des délais supplémentaires lui ont été accordés, à l'expiration de ces délais supplémentaires ; que, dès lors, en faisant courir le délai de prescription des poursuites à l'encontre de la société Scagro à compter de la date d'acceptation de chacune des déclarations d'exportation litigieuses, et non à compter de l'expiration du délai de douze mois suivant ces dates d'acceptation, la cour administrative d'appel a commis une erreur de droit ; que FranceAgriMer est fondé, pour ce motif et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, à demander l'annulation des articles 1er, 2 et 4 de l'arrêt attaqué ;<br/>
<br/>
              4. Considérant qu'aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire. " ; qu'il y a lieu, par suite, de régler l'affaire au fond ;<br/>
<br/>
              5. Considérant, en premier lieu, que le tribunal administratif n'a fondé son jugement sur aucun élément de fait ou de droit qui aurait été énoncé pour la première fois dans le mémoire en réplique de la société Scagro du 4 avril 2008 ; que, par suite, le moyen tiré de ce que le jugement serait entaché d'irrégularité au motif que ce mémoire n'a pas été communiqué à l'Oniep doit être écarté ;<br/>
<br/>
              6. Considérant, en deuxième lieu, qu'il résulte de l'interprétation que la Cour de justice de l'Union européenne a donnée du règlement du 18 décembre 1995 dans son arrêt rendu le 5 mai 2011 dans les affaires C-201/10 et C-202/10, d'une part, que le délai de prescription trentenaire résultant de l'article 2262 du code civil dans sa rédaction alors en vigueur, même réduit par la voie jurisprudentielle, ne pouvait être appliqué à l'action de l'Oniep et, d'autre part, qu'en l'absence de réglementation nationale spécifique légalement applicable aux faits de la cause et prévoyant un délai de prescription plus long, le délai de prescription de quatre ans prévu par l'article 3, paragraphe 1, de ce règlement était applicable ; qu'ainsi, compte tenu de ce qui a été dit au point 3, et dès lors qu'il ne résulte pas de l'instruction que la société Scagro aurait bénéficié de délais supplémentaires pour accomplir les formalités de mise à la consommation des produits exportés, les poursuites relatives à chacune des restitutions à l'exportation litigieuses étaient prescrites à l'expiration du délai de quatre ans suivant l'expiration du délai de douze mois suivant la date d'acceptation de chacune des déclarations d'exportation correspondantes, sauf à ce que soit intervenu un acte interruptif de prescription ;<br/>
<br/>
              7. Considérant, en troisième lieu, qu'ainsi que l'a jugé la Cour de justice des Communautés européennes dans son arrêt rendu le 24 juin 2004 dans l'affaire C-278/02, le délai de prescription ne saurait être interrompu par un acte de contrôle, d'ordre général, de l'administration nationale sans rapport avec des soupçons d'irrégularités touchant des opérations circonscrites avec suffisamment de précisions ;<br/>
<br/>
              Sur les titres de recette n°s 2007000003, 2007000007 à 200700037 et 2007000039 à 2007000045 :<br/>
<br/>
              8. Considérant qu'il résulte de l'instruction que les déclarations d'exportation correspondant à ces titres de recette ont été acceptées antérieurement au 17 mars 1998 ; que les procès-verbaux de la direction générale des douanes et des droits indirects des 8 avril et 24 juin 1999 ne comportent pas d'indications suffisamment précises quant aux opérations litigieuses et à la nature des irrégularités soupçonnées pour avoir le caractère d'actes interruptifs de prescription ; que le procès-verbal de la direction générale des douanes et des droits indirects du 17 mars 2003 détaillant les irrégularités reprochées aux sociétés impliquées dans les exportations de viande vers la Russie est intervenu postérieurement à l'expiration du délai de prescription défini au point 7 ; qu'ainsi, à la date d'émission de ces titres de recette, les poursuites relatives aux restitutions à l'exportation correspondant à ces déclarations d'exportation étaient prescrites ;<br/>
<br/>
              Sur le titre de recette n° 200700038 :<br/>
<br/>
              9. Considérant qu'il résulte de l'instruction que la déclaration d'exportation correspondant à ce titre de recette a été acceptée le 4 juin 1998 ; que le procès-verbal de la direction générale des douanes et des droits indirects du 17 mars 2003, qui a été notifié à la société Scagro antérieurement à l'expiration du délai de prescription défini au point 7, a interrompu la prescription de l'action en remboursement des restitutions à l'exportation relatives à cette déclaration d'exportation et fait courir un nouveau délai de quatre ans ;<br/>
<br/>
              10. Considérant que, lorsque les autorités nationales diligentent un contrôle a posteriori destiné à vérifier l'exactitude de la déclaration en vertu de laquelle une entreprise a bénéficié de restitutions à l'exportation, il appartient à cette entreprise de fournir les documents mentionnés au point 1 ou, à défaut, au point 2 de l'article 18 du règlement du 27 novembre 1987 cité au point 2, ainsi qu'une copie du document de transport ; que, si l'entreprise a fourni de telles justifications, il incombe alors à l'organisme national d'intervention, s'il s'y croit fondé, eu égard notamment aux résultats des contrôles diligentés par l'administration des douanes ou par tout organisme de contrôle compétent, d'apporter la preuve de l'inexactitude de cette déclaration ; qu'en revanche, si l'entreprise ne fournit pas de telles justifications, l'organisme national d'intervention est fondé à ordonner le reversement des restitutions à l'exportation qui ont été versées à cette dernière ;<br/>
<br/>
              11. Considérant qu'il résulte de l'instruction qu'à la suite du contrôle diligenté par l'Oniep au sujet des restitutions à l'exportation relatives à la déclaration d'exportation acceptée le 4 juin 1998, la société Dawn Meats France a produit un certificat d'exportation, portant le tampon des autorités douanières françaises, et une déclaration en douane, portant le tampon des autorités douanières russes, faisant état de l'expédition, puis de la livraison en Russie, par l'intermédiaire de la société ETC, de 3 991 kg de viande de boeuf congelée en provenance de la société Scagro, ainsi qu'une copie du document de transport correspondant à cette livraison ; que, de son côté, FranceAgriMer a produit une lettre du 4 mai 2005 dans laquelle les autorités douanières russes, après avoir relevé que les déclarations en douane datant de la période antérieure au 24 mai 2004 ont toutes été détruites, indiquent que le bureau local des douanes de Moscou n'aurait pas enregistré la déclaration en douane litigieuse ; qu'une telle circonstance n'est toutefois pas de nature à établir l'absence de réalité de l'opération d'exportation litigieuse ; que, d'autre part, FranceAgriMer n'a relevé aucune autre irrégularité dans les documents relatifs à cette opération d'exportation ; que, dans les circonstances de l'espèce, l'inexactitude de la déclaration d'exportation correspondant au titre de recette n° 2007000038 n'est pas établie ;<br/>
<br/>
              12. Considérant qu'il résulte de tout ce qui précède que la société Dawn Meats France est fondée à soutenir que c'est à tort que le tribunal administratif a rejeté sa demande d'annulation des titres de recette n°s 2007000007 à 2007000010, 2007000013 à 2007000019, 2007000025, 2007000027 à 2007000033, 2007000038 à 2007000040, 2007000043 à 2007000045 ainsi que du titre de recette n°s 2007000003 en tant qu'il prévoit des majorations se rapportant à des restitutions à l'exportation correspondant à ces titres de recette ; qu'en revanche, FranceAgriMer n'est pas fondé à se plaindre que le tribunal administratif a annulé les titres de recette n°s 2007000011, 2007000012, 2007000020 à 2007000024, 2007000026, 2007000034 à 2007000037, 2007000041, 2007000042 ainsi que le titre de recette n° 2007000003 en tant qu'il prévoit des majorations se rapportant à des restitutions à l'exportation correspondant à ces titres de recette ;<br/>
<br/>
              13. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de FranceAgrimer la somme de 3 000 euros à verser à la société Dawn Meats France au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'en revanche, ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société Dawn Meats France, qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 1er, 2 et 4 de l'arrêt de la cour administrative d'appel de Nantes du 7 juin 2013 sont annulés.<br/>
Article 2 : Les titres de recette nos 2007000007 à 2007000010, 2007000013 à 2007000019, 2007000025, 2007000027 à 2007000033, 2007000038 à 2007000040, 2007000043 à 2007000045 du 23 novembre 2006, ainsi que le titre de recette n° 2007000003 en tant qu'il se rapporte aux déclarations d'exportation autres que les déclarations nos 791448 et 791673 sont annulés.<br/>
Article 3 : Le jugement du tribunal administratif d'Orléans du 5 mai 2008 est réformé en ce qu'il a de contraire à la présente décision.<br/>
Article 4 : Le surplus des conclusions du pourvoi et la requête de FranceAgriMer sont rejetés.<br/>
Article 5 : FranceAgriMer versera à la société Dawn Meats France une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 6 : La présente décision sera notifiée à la société Dawn Meats France et à l'établissement public national des produits de l'agriculture et de la mer.<br/>
Copie en sera adressée au ministre de l'agriculture, de l'agroalimentaire et de la forêt, porte-parole du Gouvernement.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">03-05-01 AGRICULTURE ET FORÊTS. PRODUITS AGRICOLES. GÉNÉRALITÉS. - RESTITUTIONS À L'EXPORTATION - REMBOURSEMENT DES MONTANTS INDÛMENT PERÇUS - DÉLAI DE PRESCRIPTION (RÈGLEMENT (CE, EURATOM) N° 2988/95 DU CONSEIL DU 18 DÉCEMBRE 1995) - INTERRUPTION - 1) PAR UN ACTE DE CONTRÔLE D'ORDRE GÉNÉRAL SANS RAPPORT AVEC DES SOUPÇONS D'IRRÉGULARITÉS TOUCHANT DES OPÉRATIONS CIRCONSCRITES AVEC SUFFISAMMENT DE PRÉCISIONS - ABSENCE [RJ1] - 2) CAS D'APPLICATION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">15-05-14 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. RÈGLES APPLICABLES. POLITIQUE AGRICOLE COMMUNE. - RESTITUTIONS À L'EXPORTATION - REMBOURSEMENT DES MONTANTS INDÛMENT PERÇUS - DÉLAI DE PRESCRIPTION (RÈGLEMENT (CE, EURATOM) N° 2988/95 DU CONSEIL DU 18 DÉCEMBRE 1995) - INTERRUPTION - 1) PAR UN ACTE DE CONTRÔLE D'ORDRE GÉNÉRAL SANS RAPPORT AVEC DES SOUPÇONS D'IRRÉGULARITÉS TOUCHANT DES OPÉRATIONS CIRCONSCRITES AVEC SUFFISAMMENT DE PRÉCISIONS - ABSENCE [RJ1] - 2) CAS D'APPLICATION.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">15-08 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. LITIGES RELATIFS AU VERSEMENT D`AIDES DE L'UNION EUROPÉENNE. - RESTITUTIONS À L'EXPORTATION - REMBOURSEMENT DES MONTANTS INDÛMENT PERÇUS - DÉLAI DE PRESCRIPTION (RÈGLEMENT (CE, EURATOM) N° 2988/95 DU CONSEIL DU 18 DÉCEMBRE 1995) - INTERRUPTION - 1) PAR UN ACTE DE CONTRÔLE D'ORDRE GÉNÉRAL SANS RAPPORT AVEC DES SOUPÇONS D'IRRÉGULARITÉS TOUCHANT DES OPÉRATIONS CIRCONSCRITES AVEC SUFFISAMMENT DE PRÉCISIONS - ABSENCE [RJ1] - 2) CAS D'APPLICATION.
</SCT>
<ANA ID="9A"> 03-05-01 1) Le délai de prescription des poursuites en matière de restitutions à l'exportation ne saurait être interrompu par un acte de contrôle, d'ordre général, de l'administration nationale sans rapport avec des soupçons d'irrégularités touchant des opérations circonscrites avec suffisamment de précisions.,,,2) En l'espèce, émission à l'encontre d'une société de titres de recette correspondant à certaines déclarations d'exportation de cette société. Les premiers procès-verbaux de la direction générale des douanes et des droits indirects ne comportaient pas d'indications suffisamment précises quant aux opérations litigieuses et à la nature des irrégularités soupçonnées pour avoir le caractère d'actes interruptifs de prescription. Le procès-verbal de cette direction détaillant les irrégularités reprochées aux sociétés impliquées dans les exportations litigieuses est, quant à lui, intervenu postérieurement à l'expiration du délai de prescription. A la date d'émission des titres de recette, les poursuites relatives aux restitutions à l'exportation correspondant aux déclarations d'exportation en cause étaient donc prescrites.</ANA>
<ANA ID="9B"> 15-05-14 1) Le délai de prescription des poursuites en matière de restitutions à l'exportation ne saurait être interrompu par un acte de contrôle, d'ordre général, de l'administration nationale sans rapport avec des soupçons d'irrégularités touchant des opérations circonscrites avec suffisamment de précisions.,,,2) En l'espèce, émission à l'encontre d'une société de titres de recette correspondant à certaines déclarations d'exportation de cette société. Les premiers procès-verbaux de la direction générale des douanes et des droits indirects ne comportaient pas d'indications suffisamment précises quant aux opérations litigieuses et à la nature des irrégularités soupçonnées pour avoir le caractère d'actes interruptifs de prescription. Le procès-verbal de cette direction détaillant les irrégularités reprochées aux sociétés impliquées dans les exportations litigieuses est, quant à lui, intervenu postérieurement à l'expiration du délai de prescription. A la date d'émission des titres de recette, les poursuites relatives aux restitutions à l'exportation correspondant aux déclarations d'exportation en cause étaient donc prescrites.</ANA>
<ANA ID="9C"> 15-08 1) Le délai de prescription des poursuites en matière de restitutions à l'exportation ne saurait être interrompu par un acte de contrôle, d'ordre général, de l'administration nationale sans rapport avec des soupçons d'irrégularités touchant des opérations circonscrites avec suffisamment de précisions.,,,2) En l'espèce, émission à l'encontre d'une société de titres de recette correspondant à certaines déclarations d'exportation de cette société. Les premiers procès-verbaux de la direction générale des douanes et des droits indirects ne comportaient pas d'indications suffisamment précises quant aux opérations litigieuses et à la nature des irrégularités soupçonnées pour avoir le caractère d'actes interruptifs de prescription. Le procès-verbal de cette direction détaillant les irrégularités reprochées aux sociétés impliquées dans les exportations litigieuses est, quant à lui, intervenu postérieurement à l'expiration du délai de prescription. A la date d'émission des titres de recette, les poursuites relatives aux restitutions à l'exportation correspondant aux déclarations d'exportation en cause étaient donc prescrites.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CJCE, 24 juin 2004, Herbert Handlbauer GmbH, aff. C-278/02.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
