<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036845244</ID>
<ANCIEN_ID>JG_L_2018_04_000000409688</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/84/52/CETATEXT000036845244.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 6ème chambres réunies, 26/04/2018, 409688</TITRE>
<DATE_DEC>2018-04-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>409688</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAMARRE, JEHANNIN</AVOCATS>
<RAPPORTEUR>M. Guillaume Leforestier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:409688.20180426</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu les procédures suivantes :<br/>
<br/>
              1° Sous le n° 409688, par une requête, un mémoire complémentaire et un mémoire en réplique, enregistrés au secrétariat du contentieux du Conseil d'Etat les 10 avril et 7 juillet 2017 et le 9 mars 2018, la société anonyme d'économie mixte (SAEM) Habiter à Yerres demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision du 14 février 2017 par laquelle la ministre du logement et de l'habitat durable a prononcé à son encontre une sanction pécuniaire de 129 000 euros ;<br/>
<br/>
              2°) à titre subsidiaire, de limiter le montant de cette sanction ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              2° Sous le n° 409703, par une requête, un mémoire complémentaire et un mémoire en réplique, enregistrés au secrétariat du contentieux du Conseil d'Etat les 11 avril et 7 juillet 2017 et le 9 mars 2018, la société anonyme d'économie mixte (SAEM) Habiter à Yerres demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le titre de recettes émis le 15 mars 2017 par la Caisse de garantie du logement locatif social (CGLLS) ;<br/>
<br/>
              2°) de mettre à la charge de la CGLLS une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de la construction et de l'habitation ;<br/>
<br/>
              - le code des relations entre le public et l'administration ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Leforestier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delamarre, Jéhannin, avocat de la SAEM Habiter à Yerres.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que la société anonyme d'économie mixte (SAEM) Habiter à Yerres a, du 26 novembre 2015 au 1er mars 2016, fait l'objet d'un contrôle diligenté par l'Agence nationale de contrôle du logement social (ANCOLS) sur le fondement des articles L. 342-2 et suivants du code de la construction et de l'habitation ; qu'à l'issue de ce contrôle et sur proposition du conseil d'administration de l'agence adoptée le 16 décembre 2016, la ministre du logement et de l'habitat durable a, par une décision du 14 février 2017, infligé à la SAEM Habiter à Yerres une sanction pécuniaire de 129 000 euros, motivée par l'attribution par celle-ci de quatorze logements appartenant au contingent préfectoral sans que leur vacance ait été signalée aux services de l'Etat ou sans que ces derniers aient été mis en mesure de présenter une seconde liste de candidats ; que le 15 mars 2017, la directrice générale de la Caisse de garantie du logement locatif social (CGLLS) à émis à l'encontre de la SAEM Habiter à Yerres un titre de recettes du même montant ; que la société requérante demande l'annulation de ces deux décisions par deux requêtes qu'il y a lieu de joindre pour statuer par une seule décision ;<br/>
<br/>
              Sur les conclusions à fin d'annulation :<br/>
<br/>
              2. Considérant, d'une part, qu'aux termes de l'article L. 342-9 du code de la construction et de l'habitation : " Le rapport provisoire est communiqué à la personne concernée, au président ou au dirigeant de l'organisme concerné, qui est mis en mesure de présenter ses observations dans un délai d'un mois./ Le rapport définitif et, le cas échéant, les observations de l'organisme contrôlé et les suites apportées au contrôle sont communiqués au conseil de surveillance, au conseil d'administration ou à l'organe délibérant en tenant lieu et soumis à délibération à sa plus proche réunion (...) " ; qu'aux termes de l'article R. 342-13 du même code : " Lorsque le contrôle se conclut par un rapport provisoire, celui-ci est notifié au président ou dirigeant de l'organisme (...)/ Dans le délai d'un mois commençant à courir le lendemain du jour de la notification du rapport, le président ou le dirigeant de l'organisme contrôlé (...) peut adresser des observations écrites à l'agence et, à sa demande, être entendu par l'agence (...) " ;  qu'aux termes de l'article R. 342-14 de ce code : " Le rapport définitif de contrôle est établi après examen des observations écrites apportées au rapport provisoire par le président ou le dirigeant de l'organisme contrôlé (...), complétées par leurs observations orales lorsqu'ils ont été entendus./ Le rapport définitif est notifié au président du conseil de surveillance, du conseil d'administration ou de l'organe délibérant ou, à défaut, au dirigeant de l'organisme contrôlé (...)/ Le président ou le dirigeant de l'organisme contrôlé communique le rapport définitif, accompagné de ses éventuelles observations écrites sur ce rapport, à chaque membre du conseil de surveillance, du conseil d'administration ou de l'organe délibérant. Il inscrit son examen à la plus proche réunion du conseil de surveillance, du conseil d'administration ou de l'organe délibérant, pour être soumis à délibération. La délibération est transmise à l'agence dans les quinze jours suivant son adoption./ Dans un délai de quatre mois à compter du lendemain du jour de la notification du rapport définitif à l'organisme, le conseil de surveillance, le conseil d'administration ou l'organe délibérant de l'organisme contrôlé peut adresser à l'agence ses observations écrites sur le rapport définitif de contrôle aux fins de leur publication " ;<br/>
<br/>
              3. Considérant, d'autre part, qu'aux termes de l'article L. 342-12 du même code : " En cas de manquements aux dispositions législatives et réglementaires qui lui sont applicables, d'irrégularité dans l'emploi des fonds de la participation à l'effort de construction ou des subventions, prêts ou avantages consentis par l'Etat ou par ses établissements publics et par les collectivités territoriales ou leurs établissements publics, de faute grave de gestion, de carence dans la réalisation de l'objet social ou de non-respect des conditions d'agrément constatés, l'agence demande à l'organisme ou la personne contrôlée de présenter ses observations et, le cas échéant, le met en demeure de procéder à la rectification des irrégularités dans un délai déterminé " ; qu'aux termes de l'article L. 342-14 de ce code, l'ANCOLS peut proposer au ministre chargé du logement de prononcer des sanctions " après que la personne ou l'organisme a été mis en mesure de présenter ses observations en application de l'article L. 342-12 " ; <br/>
<br/>
              4. Considérant qu'il résulte de l'ensemble de ces dispositions que l'ANCOLS ne peut valablement proposer au ministre chargé du logement de prononcer une sanction qu'après que le conseil de surveillance, le conseil d'administration ou l'organe délibérant de l'organisme contrôlé a été mis en mesure de présenter ses observations sur le rapport définitif de contrôle ; qu'il résulte de l'instruction que la SAEM Habiter à Yerres a été avertie du contrôle litigieux par courrier du 2 novembre 2015, qu'elle a reçu communication d'un rapport provisoire d'inspection le 30 juin 2016 faisant état de l'attribution irrégulière de plusieurs logements du contingent préfectoral et qu'elle a présenté ses observations sur ce rapport par courrier adressé à l'ANCOLS le 30 septembre 2016 ; que la délibération par laquelle le conseil d'administration de l'ANCOLS a décidé de proposer au ministre le prononcé d'une sanction a été prise le 16 décembre 2016, antérieurement à la communication du rapport définitif à la SAEM, intervenue le 23 décembre ; que la décision attaquée a été prise le 14 février 2017, alors que le délai de quatre mois imparti à la SAEM pour présenter ses observations sur la rapport définitif n'était pas expiré ; qu'il suit de là que cette décision a été prise en méconnaissance des dispositions citées aux points 2 et 3 ; qu'elle doit, dès lors, et sans qu'il soit besoin d'examiner les autres moyens de la requête, être annulée ainsi que, par voie de conséquence, le titre de recettes émis le 15 mars 2017 par la directrice générale de la CGLLS ;<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 4 000 euros à verser à la SAEM Habiter à Yerres au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La décision du 14 février 2017 de la ministre du logement et de l'habitat durable et le titre de recettes émis le 15 mars 2017 par la directrice générale de la CGLLS sont annulés.<br/>
<br/>
Article 2 : L'Etat versera à la SAEM Habiter à Yerres une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la société anonyme d'économie mixte Habiter à Yerres, au ministre de la cohésion des territoires, à l'Agence nationale de contrôle du logement social et à la Caisse de garantie du logement locatif social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-07-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. PRINCIPES INTÉRESSANT L'ACTION ADMINISTRATIVE. RESPECT DES DROITS DE LA DÉFENSE. - ANCOLS - PROPOSITION DE SANCTION À L'ENCONTRE D'UN ORGANISME DE LOGEMENT SOCIAL - OBLIGATION DE L'ANCOLS - ORGANISME PRÉALABLEMENT MIS EN MESURE DE PRÉSENTER SES OBSERVATIONS SUR LE RAPPORT DÉFINITIF DE CONTRÔLE - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">38-04 LOGEMENT. HABITATIONS À LOYER MODÉRÉ. - ANCOLS - PROPOSITION DE SANCTION À L'ENCONTRE D'UN ORGANISME DE LOGEMENT SOCIAL - LÉGALITÉ DE CETTE PROPOSITION SUBORDONNÉE À CE QUE CET ORGANISME AIT ÉTÉ MIS EN MESURE DE PRÉSENTER SES OBSERVATIONS - EXISTENCE.
</SCT>
<ANA ID="9A"> 01-04-03-07-03 Il résulte des articles L. 342-9, L. 342-12, L. 342-14, R. 343-13, R. 342-14 du code de la construction et de l'habitation (CCH) que l'Agence nationale de contrôle du logement social (ANCOLS) ne peut valablement proposer au ministre chargé du logement de prononcer une sanction qu'après que le conseil de surveillance, le conseil d'administration ou l'organe délibérant de l'organisme contrôlé a été mis en mesure de présenter ses observations sur le rapport définitif de contrôle.</ANA>
<ANA ID="9B"> 38-04 Il résulte des articles L. 342-9, L. 342-12, L. 342-14, R. 343-13, R. 342-14 du code de la construction et de l'habitation (CCH) que l'Agence nationale de contrôle du logement social (ANCOLS) ne peut valablement proposer au ministre chargé du logement de prononcer une sanction qu'après que le conseil de surveillance, le conseil d'administration ou l'organe délibérant de l'organisme contrôlé a été mis en mesure de présenter ses observations sur le rapport définitif de contrôle.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
