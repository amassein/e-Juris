<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034081867</ID>
<ANCIEN_ID>JG_L_2017_02_000000396286</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/08/18/CETATEXT000034081867.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème chambres réunies, 24/02/2017, 396286</TITRE>
<DATE_DEC>2017-02-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>396286</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Marc Thoumelou</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean Lessi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:396286.20170224</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire en réplique et un nouveau mémoire, enregistrés les 21 janvier, 23 mars et 10 octobre 2016 au secrétariat du contentieux du Conseil d'Etat, la Fédération des entreprises de boulangerie demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite par laquelle le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social a rejeté sa demande, reçue le 4 novembre 2015, d'abrogation de l'arrêté du préfet de Loire-Atlantique en date du 6 mars 1995 ordonnant dans ce département un jour de fermeture au public par semaine des boulangeries, boulangeries-pâtisseries et tout commerce spécialisé, ainsi que de leurs dépendances et dépôts, fixes ou ambulants et employant ou non des salariés, dans lesquels s'effectuent la vente, la distribution de pain ou de produits frais panifiés ; <br/>
<br/>
              2°) d'abroger cet arrêté ;<br/>
<br/>
              3°) à titre subsidiaire, d'enjoindre au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social de procéder à un réexamen de sa demande et de statuer à nouveau sur celle-ci dans un délai de trois mois, sous astreinte de 200 euros par jour de retard ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le code du travail ;<br/>
              - la loi n° 2000-321 du 12 avril 2000 ;<br/>
              - la loi n° 2015-990 du 6 août 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Thoumelou, maître des requêtes en service extraordinaire,  <br/>
<br/>
- les conclusions de M. Jean Lessi, rapporteur public.<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. D'une part, aux termes de l'article L. 311-1 du code de justice administrative : " Les tribunaux administratifs sont, en premier ressort, juges de droit commun du contentieux administratif, sous réserve des compétences que l'objet du litige ou l'intérêt d'une bonne administration de la justice conduisent à attribuer à une autre juridiction administrative ". <br/>
<br/>
              2. D'autre part, aux termes de l'article L. 3132-29 du code du travail, dans sa rédaction issue de l'article 255 de la loi du 6 août 2015 pour la croissance, l'activité et l'égalité des chances économiques : " Lorsqu'un accord est intervenu entre les organisations syndicales de salariés et les organisations d'employeurs d'une profession et d'une zone géographique déterminées sur les conditions dans lesquelles le repos hebdomadaire est donné aux salariés, le préfet peut, par arrêté, sur la demande des syndicats intéressés, ordonner la fermeture au public des établissements de la profession ou de la zone géographique concernée pendant toute la durée de ce repos. (...) / A la demande des organisations syndicales représentatives des salariés ou des organisations représentatives des employeurs de la zone géographique concernée exprimant la volonté de la majorité des membres de la profession de cette zone géographique, le préfet abroge l'arrêté mentionné au premier alinéa, sans que cette abrogation puisse prendre effet avant un délai de trois mois ". Aux termes de l'article R. 3132-22 du même code : " Lorsqu'un arrêté préfectoral de fermeture au public, pris en application de l'article L. 3132-29, concerne des établissements concourant d'une façon directe à l'approvisionnement de la population en denrées alimentaires, il peut être abrogé ou modifié par le ministre chargé du travail après consultation des organisations professionnelles intéressées. / Cette décision ne peut intervenir qu'après l'expiration d'un délai de six mois à compter de la mise en application de l'arrêté préfectoral ".<br/>
<br/>
              3. Il résulte du second alinéa de l'article L. 3132-29 du code du travail que, depuis le 8 août 2015, date d'entrée en vigueur de la loi du 6 août 2015, seul le préfet a compétence pour se prononcer sur une demande d'abrogation d'un arrêté de fermeture au public formée par une organisation syndicale représentative des salariés ou des employeurs et motivée par l'évolution de la majorité des membres de la profession de la zone géographique concernée. En conséquence, l'article R. 3132-22 du code du travail doit nécessairement être regardé comme ne régissant plus, à compter de cette date, les décisions susceptibles d'être prises en réponse à une telle demande d'abrogation, lorsque figure, au nombre des motifs fondant la demande, l'invocation de la modification de la volonté de la majorité des membres de la profession. <br/>
<br/>
              4. Il ressort des pièces du dossier que la Fédération des entreprises de boulangerie a adressé au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social une demande, reçue le 4 novembre 2015, d'abrogation de l'arrêté du préfet de Loire-Atlantique du 6 mars 1995 ordonnant dans ce département un jour de fermeture au public par semaine des commerces et établissements de vente ou de distribution de pain, motivée par la circonstance que les signataires de l'accord en vertu duquel cet arrêté avait été adopté " sont aujourd'hui tout à fait minoritaires ". Il résulte de ce qui a été dit ci-dessus que cette demande a été adressée à une autorité incompétente. En conséquence, le ministre étant réputé l'avoir transmise au préfet, en application des dispositions de l'article 20 de la loi du 12 avril 2000, reprises à l'article L. 114-2 du code des relations entre le public et l'administration, la décision implicite de rejet attaquée doit être regardée comme ayant été prise par ce dernier. Aucune disposition législative ou réglementaire, et notamment pas l'article R. 311-1 du code de justice administrative, ne donnant compétence au Conseil d'Etat pour connaître en premier et dernier ressort d'un recours dirigé contre une telle décision, il y a lieu d'en attribuer le jugement au tribunal administratif de Nantes, territorialement compétent pour en connaître en vertu de l'article R. 312-10 du même code.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le jugement de la requête de la Fédération des entreprises de boulangerie est attribué au tribunal administratif de Nantes. <br/>
Article 2 : La présente décision sera notifiée à la Fédération des entreprises de boulangerie, à la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social et au président du tribunal administratif de Nantes.<br/>
Copie en sera adressée à la Fédération des patrons boulangers et boulangers-pâtissiers de Loire-Atlantique, à l'Union départementale des syndicats CGT-FO Loire-Atlantique, à l'Union départementale des syndicats CFTC Loire-Atlantique et à l'Union départementale des syndicats CFDT Loire-Atlantique.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">66-03-02-02 TRAVAIL ET EMPLOI. CONDITIONS DE TRAVAIL. REPOS HEBDOMADAIRE. FERMETURE HEBDOMADAIRE DES ÉTABLISSEMENTS. - ARRÊTÉ PRÉFECTORAL ORDONNANT LA FERMETURE HEBDOMADAIRE DES ÉTABLISSEMENTS D'UNE PROFESSION DANS UN SECTEUR GÉOGRAPHIQUE (ART. L. 3132-29 DU CODE DU TRAVAIL) - DEMANDE D'ABROGATION OU DE MODIFICATION DE L'ARRÊTÉ - AUTORITÉ COMPÉTENTE - CAS OÙ LA DEMANDE EST FONDÉE NOTAMMENT SUR LA MODIFICATION DE LA VOLONTÉ DE LA MAJORITÉ DES MEMBRES DE LA PROFESSION - PRÉFET [RJ1].
</SCT>
<ANA ID="9A"> 66-03-02-02 Il résulte du second alinéa de l'article L. 3132-29 du code du travail que, depuis l'entrée en vigueur de la loi n° 2015-990 du 6 août 2015, seul le préfet a compétence pour se prononcer sur une demande d'abrogation ou de modification d'un arrêté de fermeture au public des établissements d'une profession dans un secteur géographique formée par une organisation syndicale représentative de salariés ou d'employeurs lorsque figure, au nombre des motifs fondant la demande, l'invocation de la modification de la volonté de la majorité des membres de la profession. En conséquence, l'article R. 3132-22 du code du travail, qui prévoit la compétence du ministre, doit être regardé comme ne régissant plus, à compter de cette date, cette hypothèse.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp., s'agissant de la rédaction de l'article L. 3132-29 du code du travail (ancien art. L. 221-17) antérieure à la loi n° 2015-990, CE, 3 décembre 2003, Sarl QSCT et SA France restauration rapide, n° 248840, T. pp. 720-964-1013 dont la solution a été abandonnée, s'agissant de la compétence en premier et dernier ressort du Conseil d'Etat, par CE, 28 juillet 2017, Société O'Tours du chocolat, n° 398816, B. Cf. décisions du même jour, Fédération des entreprises de boulangerie, n° 396287, Fédération des entreprises de boulangerie, n° 396288, inédites au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
