<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041709660</ID>
<ANCIEN_ID>JG_L_2020_03_000000432555</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/70/96/CETATEXT000041709660.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 10/03/2020, 432555, Publié au recueil Lebon</TITRE>
<DATE_DEC>2020-03-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>432555</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Appréciation de la légalité</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>Mme Liza Bellulo</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:432555.20200310</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une ordonnance du 24 septembre 2018, le tribunal de grande instance de Nanterre a sursis à statuer sur l'action, formée par l'association syndicale des propriétaires de la cité Boigues, Mme L... G..., M. B... C...-P..., M. J... M..., M. A... E..., M. D... I..., M. N... F..., M. Q... D... H...-R... et M. O... K... à l'encontre de la commune de Clamart, tendant à faire constater que la commune de Clamart est membre de l'association syndicale des copropriétaires de la cité Boigues en qualité de propriétaire de six parcelles, selon eux, situées à l'intérieur du périmètre de la cité, jusqu'à ce que la juridiction administrative se soit prononcée sur l'appartenance au domaine public communal des parcelles cadastrées sections AM 40, AL 107, AL 109, AL 119, AL 219 et AL 220 et dise, le cas échéant, si leur affectation, actuelle ou projetée, est compatible avec les obligations découlant de leur appartenance au périmètre de l'association syndicale.<br/>
<br/>
              Par un jugement n° 1810221 du 27 juin 2019, le tribunal administratif de Cergy-Pontoise a donné acte du désistement de M. K..., a déclaré que les parcelles en cause appartenaient au domaine public de la commune de Clamart et que leur affectation, actuelle ou projetée, était incompatible avec les obligations découlant de leur appartenance au périmètre de l'association syndicale de la cité Boigues.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 12 juillet 2019, 12 août 2019 et 27 janvier 2020 au secrétariat du contentieux du Conseil d'Etat, l'association syndicale des propriétaires de la cité Boigues, Mme G..., M. C...-P..., M. M..., M. E..., M. I..., M. F..., et M. H...-R... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler les articles 2 et 3 de ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de déclarer que les parcelles cadastrées sections AM 40, AL 107, AL 109, AL 119, AL 219 et AL 220 n'appartiennent pas au domaine public de la commune de Clamart et que l'affectation, actuelle ou projetée, des parcelles litigieuses est compatible avec les obligations découlant de leur appartenance au périmètre de l'association syndicale des propriétaires de la cité Boigues ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général de la propriété des personnes publiques ;<br/>
              - la loi du 21 juin 1865 relative aux associations syndicales ; <br/>
              - l'ordonnance n° 2004-632 du 1er juillet 2004 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Liza Bellulo, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat de l'association syndicale des proprietaires de la cité Boigues, de Mme G..., de M. C...-P..., de M. M..., de M. E..., de M. I..., de M. F... et de M. H...-R... et à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la commune de Clamart ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 25 février 2020, présentée par la commune de Clamart ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il résulte des pièces du dossier soumis aux juges du fond que l'association syndicale des propriétaires de la cité Boigues ainsi que Mme G..., M. C...-P..., M. M..., M. E..., M. I..., M. F..., et M. H...-R..., propriétaires dans cette cité, ont saisi le tribunal de grande instance de Nanterre d'une action tendant à faire constater que la commune de Clamart est membre de cette association syndicale libre, créée en 1858, en qualité de propriétaire de six parcelles cadastrales situées à l'intérieur du périmètre de la cité. Le tribunal de grande instance de Nanterre a sursis à statuer sur l'action introduite par les requérants et saisi la juridiction administrative afin, d'une part, qu'elle se prononce sur l'appartenance au domaine public communal des parcelles AM 40, AL 107, AL 109, AL 119, AL 219 et AL 220 et, d'autre part, dans l'affirmative, dise si leur affectation actuelle ou projetée est compatible avec les obligations découlant de leur appartenance au périmètre de l'association syndicale de la cité Boigues. L'association syndicale et les propriétaires précédemment mentionnés se pourvoient en cassation contre les articles 2 et 3 du jugement rendu le 27 juin 2019 par le tribunal administratif de Cergy-Pontoise.<br/>
<br/>
              2. Aucune disposition législative ou réglementaire, non plus qu'aucun principe ne fait obstacle à ce qu'une décision régulièrement prise par les organes compétents d'une association syndicale, conforme à l'objet de l'association tel que défini par ses statuts dans le respect de la loi, s'impose à une personne publique membre de cette association à raison d'une dépendance de son domaine public, alors même que cette personne publique n'en aurait pas approuvé l'adoption. Par suite, en se fondant, pour juger que le caractère de dépendance du domaine public de la commune de Clamart des parcelles cadastrées section AL nos 107, 109, 119 et 220, section AM n° 40 et section AL n° 219 était incompatible avec les obligations découlant de leur appartenance au périmètre de l'association syndicale des propriétaires de la cité Boigues, sur ce que les statuts de cette association prévoyaient que les délibérations étaient prises à la majorité des voix des membres présents et représentés et que les décisions des assemblées générales étaient obligatoires pour tous les membres de l'association, le tribunal administratif a commis une erreur de droit. <br/>
<br/>
              3. Il en résulte, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, que les requérants sont fondés à demander l'annulation des articles 2 et 3 du jugement qu'ils attaquent. <br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond, en vertu des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              Sur la compatibilité de l'appartenance au périmètre d'une association syndicale et du régime de la domanialité publique :<br/>
<br/>
              5. Antérieurement à l'entrée en vigueur de l'ordonnance du 1er juillet 2004 relative aux associations syndicales de propriétaires, aucune disposition législative ou réglementaire, notamment pas celles de la loi du 21 juin 1865 relative aux associations syndicales, ne faisait obstacle à ce que des personnes publiques soient membres d'une association syndicale de propriétaires à raison de biens constituant des dépendances de leur domaine public.<br/>
<br/>
               6. En revanche, aux termes de l'article 6 de l'ordonnance du 1er juillet 2004 relative aux associations syndicales de propriétaires, dont l'article 58 a abrogé la loi du 21 juin 1865 : " Les créances de toute nature d'une association syndicale de propriétaires à l'encontre d'un de ses membres sont garanties par une hypothèque légale sur les immeubles de ce membre compris dans le périmètre de l'association ". Il découle de ces dispositions que le régime des associations syndicales est, depuis leur entrée en vigueur, incompatible avec celui de la domanialité publique, notamment avec le principe d'inaliénabilité. <br/>
<br/>
              7. Il s'ensuit qu'un immeuble inclus dans le périmètre d'une association syndicale et qui, à la date d'entrée en vigueur de l'ordonnance du 1er juillet 2004, n'appartenait pas au domaine public d'une personne publique, ne peut devenir une dépendance de ce domaine, alors même qu'il serait affecté à l'usage direct du public ou qu'il serait affecté à un service public et aurait fait l'objet d'aménagements propres à lui conférer cette qualification.<br/>
<br/>
              8. L'entrée en vigueur de l'ordonnance du 1er juillet 2004 ne saurait toutefois avoir eu pour effet d'emporter le déclassement des biens qui, avant cette entrée en vigueur, appartenaient déjà au domaine public et se trouvaient compris dans le périmètre d'une association syndicale. Dans ce cas, sauf à ce qu'ils fassent l'objet d'un déclassement, ces biens continuent d'appartenir au domaine public et l'incompatibilité des dispositions de l'article 6 de l'ordonnance du 1er juillet 2004 avec le régime de la domanialité publique a pour seule conséquence l'impossibilité pour l'association syndicale de mettre en oeuvre, pour le recouvrement des créances qu'elle détient sur la personne publique propriétaire, la garantie de l'hypothèque légale sur les biens inclus dans le périmètre et appartenant au domaine public.<br/>
<br/>
              Sur l'appartenance au domaine public des parcelles en litige :<br/>
<br/>
              9. Avant l'entrée en vigueur, le 1er juillet 2006, du code général de la propriété des personnes publiques, l'appartenance au domaine public d'un bien était, sauf si ce bien était directement affecté à l'usage du public, subordonnée à la double condition que le bien ait été affecté au service public et spécialement aménagé en vue du service public auquel il était destiné. En l'absence de toute disposition en ce sens, l'entrée en vigueur du code général de la propriété des personnes publiques n'a pu, par elle-même, avoir pour effet d'entraîner le déclassement de dépendances qui appartenaient antérieurement au domaine public et qui, depuis le 1er juillet 2006, ne rempliraient plus les conditions désormais fixées par son article L. 2111-1.<br/>
<br/>
              10. En premier lieu, il résulte de l'instruction que les parcelles cadastrées section AL nos 107, 109, 119 et 220, propriété de la commune de Clamart, constituent l'emprise du conservatoire de musique Henri-Dutilleux, créé en 1971. Tant la fraction des parcelles n°107 et 109 correspondant au terrain d'assiette des bâtiments du conservatoire, qui constituent un aménagement spécial en vue de l'exécution de la mission de service public confié à cet établissement, que l'autre fraction de ces parcelles ainsi que les parcelles n°s 119 et 220, qui correspondent aux espaces verts et aux espaces de circulation entourant le bâtiment du conservatoire, dont ils ne sont pas matériellement séparés et constituent l'accessoire indispensable, satisfont ainsi, depuis une date antérieure à l'entrée en vigueur de l'ordonnance du 1er juillet 2004, aux critères de la domanialité publique. Ainsi qu'il a été dit, il n'existait avant l'entrée en vigueur de cette ordonnance aucune incompatibilité entre le régime des associations syndicales et celui de la domanialité publique et ni les statuts, ni le règlement de l'association syndicale des propriétaires de la cité Boigues, ne comportaient, en tout état de cause, de clause de nature à faire obstacle, pour les fractions comprises dans son périmètre, à l'entrée de ces parcelles dans le domaine public. L'entrée en vigueur de l'ordonnance du 1er juillet 2004 n'a pas eu pour effet de faire cesser l'appartenance au domaine public des parcelles en cause. Il en résulte que les parcelles cadastrées section AL nos 107, 109, 119 et 220 constituent des dépendances du domaine public communal.<br/>
<br/>
              11. En deuxième lieu, il résulte de l'instruction que la parcelle cadastrée section AM n° 40 a été acquise par l'Etat, par acte du 29 décembre 1952, afin d'y édifier un lycée professionnel, lequel constituait un aménagement spécial en vue de l'exécution de missions de service public. Cette parcelle satisfaisait ainsi, antérieurement à l'entrée en vigueur de l'ordonnance du 1er juillet 2004, aux critères de la domanialité publique. Ni le régime des associations syndicales alors en vigueur, ni, en tout état de cause, les statuts et règlement de l'association syndicale des propriétaires de la cité Boigues n'y faisant obstacle, cette parcelle est devenue une dépendance du domaine public de l'Etat. Elle a été cédée en 2014, sans déclassement, en application des dispositions de l'article L. 3112-1 du code général de la propriété des personnes publiques, à la commune de Clamart par la région Ile-de-France, dans le domaine public de laquelle elle avait été transférée par l'effet des dispositions législatives relatives aux transferts de compétences entre l'Etat et les régions en matière d'enseignement. En l'absence de toute décision de déclassement, cette parcelle demeure une dépendance du domaine public de la commune de Clamart.<br/>
<br/>
              12. En troisième lieu, il résulte de l'instruction que la parcelle cadastrée section AL n° 219 a été acquise par la commune de Clamart en 2003 dans le but d'y édifier une médiathèque municipale, ainsi que le mentionnent expressément l'acte de vente et la décision du préfet des Hauts-de-Seine autorisant la cession de ce bien par la fondation reconnue d'utilité publique qui en était auparavant propriétaire. L'affectation de cette parcelle au service public culturel, pour les besoins duquel elle était destinée à recevoir un aménagement spécial, a eu pour effet de lui conférer, dès son acquisition, le caractère d'une dépendance du domaine public communal sans, ainsi qu'il a été dit, que son inclusion dans le périmètre de l'association syndicale des propriétaires de la cité Boigues n'y fasse obstacle. L'entrée en vigueur de l'ordonnance du 1er juillet 2004 relative aux associations syndicales de propriétaires n'a, alors même que la médiathèque n'a été ouverte au public qu'en mars 2006, pas eu pour effet d'en entraîner le déclassement, pas davantage que l'entrée en vigueur du code général de la propriété des personnes publiques le 1er juillet 2006. Cette parcelle appartient ainsi au domaine public de la commune de Clamart.<br/>
<br/>
              13. Il résulte de tout ce qui précède qu'il y a lieu de déclarer, en réponse à la question posée par le tribunal de grande instance de Nanterre, que les parcelles cadastrées section AL nos 107, 109, 119 et 220, section AM n° 40 et section AL n° 219 appartiennent au domaine public de la commune de Clamart, sans que l'entrée en vigueur de l'ordonnance du 1er juillet 2004 ou celle du code général de la propriété des personnes publiques le 1er juillet 2006 aient eu pour effet d'en emporter le déclassement, et que cette appartenance est, dès lors que la garantie de l'hypothèque légale ne peut, conformément à ce qui a été dit au point 8, être mise en oeuvre sur ces biens compatible avec leur inclusion dans le périmètre de l'association syndicale de propriétaires de la cité Boigues.<br/>
<br/>
              14. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées au titre de l'article L. 761-1 du code de justice administrative par l'association syndicale des propriétaires de la cité Boigues et autres. Les dispositions de cet article font obstacle à ce que ces derniers, qui ne sont pas la partie perdante, versent la somme demandée au même titre par la commune de Clamart.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 2 et 3 du jugement du tribunal administratif de Cergy-Pontoise sont annulés. <br/>
Article 2 : Il est déclaré que les parcelles cadastrées section AL nos 107, 109, 119 et 220, section AM n° 40 et section AL n° 219 appartiennent au domaine public de la commune de Clamart. <br/>
Article 3 : Il est déclaré que l'appartenance au domaine public de ces parcelles est compatible avec les obligations découlant de leur appartenance au périmètre de l'association syndicale libre de propriétaires de la cité Boigues.<br/>
Article 4 : Les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative par, d'une part, l'association syndicale des propriétaires de la cité Boigues et autres et, d'autre part, la commune de Clamart sont rejetées.<br/>
Article 5 : La présente décision sera notifiée au président du tribunal judiciaire de Nanterre, à l'association syndicale des propriétaires de la cité Boigues, premier requérant dénommé, et à la commune de Clamart.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">24-01-01-01 DOMAINE. DOMAINE PUBLIC. CONSISTANCE ET DÉLIMITATION. DOMAINE PUBLIC ARTIFICIEL. - 1) COMPATIBILITÉ DU RÉGIME DES ASSOCIATIONS SYNDICALES DE PROPRIÉTAIRES AVEC CELUI DE LA DOMANIALITÉ PUBLIQUE [RJ1] - A) ANTÉRIEUREMENT À L'ENTRÉE EN VIGUEUR DE L'ORDONNANCE DU 1ER JUILLET 2014 - EXISTENCE - B) POSTÉRIEUREMENT - ABSENCE - 2) CONSÉQUENCES POUR L'APPARTENANCE AU DOMAINE PUBLIC D'UN IMMEUBLE INCLUS DANS LE PÉRIMÈTRE D'UNE TELLE ASSOCIATION - A) IMMEUBLE APPARTENANT AU DOMAINE PRIVÉ ANTÉRIEUREMENT À L'ENTRÉE EN VIGUEUR DE L'ORDONNANCE - IMMEUBLE NE POUVANT ENTRER DANS LE DOMAINE PUBLIC - B) IMMEUBLE APPARTENANT DÉJÀ AU DOMAINE PUBLIC - IMMEUBLE CONTINUANT D'Y APPARTENIR, LE RÉGIME DE LA DOMANIALITÉ PUBLIQUE S'OPPOSANT SEULEMENT À L'APPLICATION DES RÈGLES DU RÉGIME DES ASSOCIATIONS DE PROPRIÉTAIRES INCOMPATIBLES AVEC LUI.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">24-02-01 DOMAINE. DOMAINE PRIVÉ. CONSISTANCE. - 1) COMPATIBILITÉ DU RÉGIME DES ASSOCIATIONS SYNDICALES DE PROPRIÉTAIRES AVEC CELUI DE LA DOMANIALITÉ PUBLIQUE [RJ1] - A) ANTÉRIEUREMENT À L'ENTRÉE EN VIGUEUR DE L'ORDONNANCE DU 1ER JUILLET 2014 - EXISTENCE - B) POSTÉRIEUREMENT - ABSENCE - 2) CONSÉQUENCES POUR L'APPARTENANCE AU DOMAINE PUBLIC D'UN IMMEUBLE INCLUS DANS LE PÉRIMÈTRE D'UNE TELLE ASSOCIATION - A) IMMEUBLE APPARTENANT AU DOMAINE PRIVÉ ANTÉRIEUREMENT À L'ENTRÉE EN VIGUEUR DE L'ORDONNANCE - IMMEUBLE NE POUVANT ENTRER DANS LE DOMAINE PUBLIC - B) IMMEUBLE APPARTENANT DÉJÀ AU DOMAINE PUBLIC - IMMEUBLE CONTINUANT D'Y APPARTENIR, LE RÉGIME DE LA DOMANIALITÉ PUBLIQUE S'OPPOSANT SEULEMENT À L'APPLICATION DES RÈGLES DU RÉGIME DES ASSOCIATIONS DE PROPRIÉTAIRES INCOMPATIBLES AVEC LUI.
</SCT>
<ANA ID="9A"> 24-01-01-01 1) a) Antérieurement à l'entrée en vigueur de l'ordonnance n° 2004-632 du 1er juillet 2004, aucune disposition législative ou réglementaire, notamment pas celles de la loi du 21 juin 1865 relative aux associations syndicales, ne faisait obstacle à ce que des personnes publiques soient membres d'une association syndicale de propriétaires à raison de biens constituant des dépendances de leur domaine public.,,,b) En revanche, l'article 6 de l'ordonnance du 1er juillet 2004, dont l'article 58 a abrogé la loi du 21 juin 1865, prévoit que les créances de toute nature d'une association syndicale de propriétaires à l'encontre d'un de ses membres sont garanties par une hypothèque légale sur les immeubles de ce membre compris dans le périmètre de l'association. Il découle de cet article que le régime des associations syndicales est, depuis son entrée en vigueur, incompatible avec celui de la domanialité publique, notamment avec le principe d'inaliénabilité.... ,,2) a) Il s'ensuit qu'un immeuble inclus dans le périmètre d'une association syndicale et qui, à la date d'entrée en vigueur de l'ordonnance du 1er juillet 2004, n'appartenait pas au domaine public d'une personne publique, ne peut devenir une dépendance de ce domaine, alors même qu'il serait affecté à l'usage direct du public ou qu'il serait affecté à un service public et aurait fait l'objet d'aménagements propres à lui conférer cette qualification.,,,b) L'entrée en vigueur de l'ordonnance du 1er juillet 2004 ne saurait toutefois avoir eu pour effet d'emporter le déclassement des biens qui, avant cette entrée en vigueur, appartenaient déjà au domaine public et se trouvaient compris dans le périmètre d'une association syndicale. Dans ce cas, sauf à ce qu'ils fassent l'objet d'un déclassement, ces biens continuent d'appartenir au domaine public et l'incompatibilité de l'article 6 de l'ordonnance du 1er juillet 2004 avec le régime de la domanialité publique a pour seule conséquence l'impossibilité pour l'association syndicale de mettre en oeuvre, pour le recouvrement des créances qu'elle détient sur la personne publique propriétaire, la garantie de l'hypothèque légale sur les biens inclus dans le périmètre et appartenant au domaine public.</ANA>
<ANA ID="9B"> 24-02-01 1) a) Antérieurement à l'entrée en vigueur de l'ordonnance n° 2004-632 du 1er juillet 2004, aucune disposition législative ou réglementaire, notamment pas celles de la loi du 21 juin 1865 relative aux associations syndicales, ne faisait obstacle à ce que des personnes publiques soient membres d'une association syndicale de propriétaires à raison de biens constituant des dépendances de leur domaine public.,,,b) En revanche, l'article 6 de l'ordonnance du 1er juillet 2004, dont l'article 58 a abrogé la loi du 21 juin 1865, prévoit que les créances de toute nature d'une association syndicale de propriétaires à l'encontre d'un de ses membres sont garanties par une hypothèque légale sur les immeubles de ce membre compris dans le périmètre de l'association. Il découle de cet article que le régime des associations syndicales est, depuis son entrée en vigueur, incompatible avec celui de la domanialité publique, notamment avec le principe d'inaliénabilité.... ,,2) a) Il s'ensuit qu'un immeuble inclus dans le périmètre d'une association syndicale et qui, à la date d'entrée en vigueur de l'ordonnance du 1er juillet 2004, n'appartenait pas au domaine public d'une personne publique, ne peut devenir une dépendance de ce domaine, alors même qu'il serait affecté à l'usage direct du public ou qu'il serait affecté à un service public et aurait fait l'objet d'aménagements propres à lui conférer cette qualification.,,,b) L'entrée en vigueur de l'ordonnance du 1er juillet 2004 ne saurait toutefois avoir eu pour effet d'emporter le déclassement des biens qui, avant cette entrée en vigueur, appartenaient déjà au domaine public et se trouvaient compris dans le périmètre d'une association syndicale. Dans ce cas, sauf à ce qu'ils fassent l'objet d'un déclassement, ces biens continuent d'appartenir au domaine public et l'incompatibilité de l'article 6 de l'ordonnance du 1er juillet 2004 avec le régime de la domanialité publique a pour seule conséquence l'impossibilité pour l'association syndicale de mettre en oeuvre, pour le recouvrement des créances qu'elle détient sur la personne publique propriétaire, la garantie de l'hypothèque légale sur les biens inclus dans le périmètre et appartenant au domaine public.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant de l'incompatibilité entre le régime de la copropriété et celui de la domanialité publique, CE, Section, 11 février 1994, Compagnie d'assurances Préservatrice Foncière, n° 109564, p. 64 ; s'agissant du régime des associations foncières urbaines libres, CE, 23 janver 2020,,et,et SARL JV Immobilier c/ Commune de Bussy-Saint-Georges, n°s 430192 430359, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
