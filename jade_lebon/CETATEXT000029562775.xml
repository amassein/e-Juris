<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029562775</ID>
<ANCIEN_ID>JG_L_2014_10_000000368206</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/56/27/CETATEXT000029562775.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème / 7ème SSR, 10/10/2014, 368206</TITRE>
<DATE_DEC>2014-10-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>368206</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème / 7ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>M. Luc Briand</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:368206.20141010</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 15 mars et 17 juin 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour la région Nord-Pas-de-Calais, dont le siège est à l'Hôtel de Région, Centre Rihour à Lille (59555 Cedex) ; la région Nord-Pas-de-Calais demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision par laquelle la Société nationale des chemins de fer français (SNCF) a fixé les tarifs voyageurs applicables à la liaison TGV Nord-Lille-Paris, au départ ou à l'arrivée des gares de Lille-Flandres et Lille-Europe, à compter du 24 janvier 2013 ;<br/>
<br/>
              2°) de mettre à la charge de la SNCF le versement de la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 82 1153 du 30 décembre 1982 ;<br/>
<br/>
              Vu le décret n° 83-817 du 13 septembre 1983 ;<br/>
<br/>
              Vu l'arrêté interministériel du 16 décembre 2011 fixant les modalités d'application des articles 14 et 17 du cahier des charges de la Société nationale des chemins de fers français ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Luc Briand, maître des requêtes en service extraordinaire, <br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de la région Nord-Pas-de-Calais, et à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la SNCF ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant que la région Nord-Pas-de-Calais demande l'annulation pour excès de pouvoir de la décision par laquelle la Société nationale des chemins de fer français (SNCF) a fixé les tarifs voyageurs applicables à compter du 24 janvier 2013 sur la liaison TGV entre Lille et Paris, au départ ou à l'arrivée des gares de Lille-Flandres et Lille-Europe ;<br/>
<br/>
              2.	Considérant, en premier lieu, qu'aux termes du premier alinéa de l'article 17 du cahier des charges de la Société nationale des chemins de fer français approuvé par le décret n° 83-817 du 13 septembre 1983 : " La S.N.C.F. communique les tarifs qu'elle établit [...] au ministre chargé des transports quinze jours au moins avant la date à laquelle ces tarifs doivent entrer en vigueur. A défaut d'opposition notifiée dans les huit jours suivant leur dépôt, les tarifs établis par la S.N.C.F. sont réputés homologués. Ces tarifs sont portés à la connaissance du public six jours au moins avant la date de leur entrée en vigueur " ; qu'il ressort des pièces du dossier que la SNCF a communiqué le 8 janvier 2013 les nouveaux tarifs des liaisons entre Lille et Paris au ministre chargé des transports ; qu'en l'absence de refus d'homologation, ceux-ci doivent être regardés comme ayant été tacitement approuvés ; qu'il ressort également du dossier que ces tarifs ont été communiqués au public par leur mise en ligne sur le site Internet de la SNCF et affichage dans les gares six jours au moins avant la date prévue de leur entrée en vigueur ; qu'ainsi, le moyen tiré de la violation des dispositions de l'article 17 du cahier des charges de la Société nationale des chemins de fer français ne peut, en tout état de cause, qu'être écarté ;<br/>
<br/>
              3.	Considérant, en deuxième lieu, que selon l'article 13 du cahier des charges de la Société nationale des chemins de fer français : " La S.N.C.F. perçoit un prix en contrepartie des prestations qu'elle fournit. / Elle mène une politique tarifaire visant à développer l'usage du train en participant à la satisfaction du droit au transport, dans des conditions assurant l'équilibre global de son exploitation, compte tenu des participations des collectivités publiques et d'autres bénéficiaires publics ou privés qui, sans être usagers des services, en retirent un avantage direct ou indirect. / Cette politique tarifaire favorise la réalisation des objectifs définis par l'Etat pour obtenir l'utilisation la meilleure au plan économique et social du système des transports intérieurs français " ; qu'en vertu de l'article 14 du cahier des charges : " 1. Les prix payés par les usagers des services nationaux sont fixés par la S.N.C.F. en application : / 1° D'un tarif de base général correspondant au prix du voyage en seconde classe ; / 2° De tarifs réglementés de référence correspondant au prix du voyage en seconde classe, sur certaines relations, institués dans les conditions définies au 2 du présent article ; / 3° De l'ensemble des tarifs comportant diverses modulations par rapport à l'application du tarif de base général et des tarifs réglementés de référence et intégrant notamment les tarifs sociaux mis en oeuvre par la S.N.C.F. à la demande de l'Etat. / 2. Dans les conditions fixées par l'arrêté prévu au 3 du présent article, un tarif réglementé de référence peut être institué sur une relation : / 1° Lorsqu'elle présente pour les usagers des avantages particuliers de rapidité et de confort ; / 2° Ou lorsqu'elle est soumise à une forte concurrence de la part d'un autre mode de transport ou d'un autre exploitant ferroviaire et que l'institution du tarif réglementé de référence est susceptible, en développant l'usage du train, d'éviter la dégradation ou de concourir à l'amélioration des comptes de résultat de la S.N.C.F. " ;<br/>
<br/>
              4.	Considérant qu'il résulte de ces dispositions que la SNCF peut, en lieu et place du tarif de base, instituer, selon les conditions prévues, un tarif particulier sur une relation déterminée lorsque celle-ci présente pour les usagers des avantages particuliers de rapidité et de confort ou est soumise à une forte concurrence de la part d'un autre mode de transport ou d'un autre exploitant ferroviaire ; que le principe d'égalité ne s'oppose à ce que des tarifs différents soient ainsi appliqués à des liaisons différentes ; qu'en l'espèce, la liaison entre Paris et Lille est assurée par des rames circulant à grande vitesse pendant la plus grande partie du parcours, permettant ainsi d'assurer, dans des conditions de confort avantageuses, le parcours de 214 kilomètres en une heure ; que les conditions d'exploitation de cette ligne étant différentes notamment de celles des autres lignes ferroviaires, de même que le service rendu aux usagers, le moyen tiré de la méconnaissance du principe d'égalité entre les usagers du service public ne peut qu'être écarté ;<br/>
<br/>
              5.	Considérant, en troisième lieu, qu'aux termes de l'article 3 de l'arrêté du 16 décembre 2011 fixant les modalités d'application des articles 14 et 17 du cahier des charges de la Société nationale des chemins de fer français : " La gamme tarifaire de la SNCF est conçue de telle sorte que le voyage à un prix inférieur au tarif réglementé de référence soit raisonnablement accessible. Au cours d'une année et en seconde classe, le nombre de billets vendus pour lesquels le prix est inférieur ou égal au tarif réglementé de référence doit représenter, d'une part, au moins 50 % du nombre total des billets vendus, d'autre part, au moins 10 % des billets vendus pour les trains partant entre le vendredi 12 heures et le samedi 12 heures, et entre le dimanche 12 heures et le lundi 12 heures " ; qu'il ne ressort pas des éléments versés au dossier que les tarifs fixés par la décision contestée rendraient impossible le respect des exigences de l'article 3 de l'arrêté du 16 décembre 2011 fixant les modalités d'application des articles 14 et 17 du cahier des charges de la Société nationale des chemins de fer français ; que le moyen tiré de la violation des articles 14 et 44 du cahier des charges n'est pas assorti des précisions permettant d'en apprécier le bien fondé ;<br/>
<br/>
              6.	Considérant qu'il résulte de tout ce qui précède, sans qu'il soit besoin de statuer sur la fin de non-recevoir opposée par la SNCF, que la région Nord-Pas-de-Calais n'est pas fondée à demander l'annulation de la décision attaquée ;<br/>
<br/>
              7.	Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la SNCF, qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de la région Nord-Pas-de-Calais la somme de 3 000 euros à verser à la SNCF au même titre ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de la région Nord-Pas-de-Calais est rejetée.<br/>
<br/>
Article 2 : La région Nord-Pas-de-Calais versera à la Société nationale des chemins de fer français une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 3 : La présente décision sera notifiée à la région Nord-Pas-de-Calais et à la Société nationale des chemins de fer français.<br/>
Une copie en sera adressée pour information à la ministre de l'écologie, du développement durable et de l'énergie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-03-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. ÉGALITÉ DEVANT LE SERVICE PUBLIC. ÉGALITÉ DES USAGERS DEVANT LE SERVICE PUBLIC. - TARIFS SNCF - 1) POSSIBILITÉ D'INSTAURER UN TARIF PARTICULIER SUR UNE LIAISON DÉTERMINÉE - EXISTENCE - CONDITIONS - 2) CONFORMITÉ DES TARIFS PARIS-LILLE AU PRINCIPE D'ÉGALITÉ - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">65-01-01 TRANSPORTS. TRANSPORTS FERROVIAIRES. TARIFS. - TARIFS SNCF - 1) POSSIBILITÉ D'INSTAURER UN TARIF PARTICULIER SUR UNE LIAISON DÉTERMINÉE - EXISTENCE - CONDITIONS - 2) CONFORMITÉ DES TARIFS PARIS-LILLE AU PRINCIPE D'ÉGALITÉ - EXISTENCE.
</SCT>
<ANA ID="9A"> 01-04-03-03-03 Il résulte des articles 13 et 14 du cahier des charges de la Société nationale des chemins de fer français (SNCF), approuvé par le décret n° 83-817 du 13 septembre 1983, que cet établissement public peut, en lieu et place du tarif de base, instituer un tarif particulier sur une relation déterminée lorsque celle-ci présente pour les usagers des avantages particuliers de rapidité et de confort ou est soumise à une forte concurrence de la part d'un autre mode de transport ou d'un autre exploitant ferroviaire. Par ailleurs, le principe d'égalité ne s'oppose pas à ce que des tarifs différents soient ainsi appliqués à des liaisons différentes.... ,,En l'espèce, la liaison entre Paris et Lille est assurée par des rames circulant à grande vitesse pendant la plus grande partie du parcours, permettant ainsi d'effectuer, dans des conditions de confort avantageuses, le parcours de 214 kilomètres en une heure. Les conditions d'exploitation de cette ligne étant différentes notamment de celles des autres lignes ferroviaires, de même que le service rendu aux usagers, la différence entre le prix au kilomètre payé par les usagers de cette ligne et le prix payé sur d'autres liaisons n'est pas contraire au principe d'égalité entre les usagers du service public.</ANA>
<ANA ID="9B"> 65-01-01 Il résulte des articles 13 et 14 du cahier des charges de la Société nationale des chemins de fer français (SNCF), approuvé par le décret n° 83-817 du 13 septembre 1983, que cet établissement public peut, en lieu et place du tarif de base, instituer un tarif particulier sur une relation déterminée lorsque celle-ci présente pour les usagers des avantages particuliers de rapidité et de confort ou est soumise à une forte concurrence de la part d'un autre mode de transport ou d'un autre exploitant ferroviaire. Par ailleurs, le principe d'égalité ne s'oppose pas à ce que des tarifs différents soient ainsi appliqués à des liaisons différentes.... ,,En l'espèce, la liaison entre Paris et Lille est assurée par des rames circulant à grande vitesse pendant la plus grande partie du parcours, permettant ainsi d'effectuer, dans des conditions de confort avantageuses, le parcours de 214 kilomètres en une heure. Les conditions d'exploitation de cette ligne étant différentes notamment de celles des autres lignes ferroviaires, de même que le service rendu aux usagers, la différence entre le prix au kilomètre payé par les usagers de cette ligne et le prix payé sur d'autres liaisons n'est pas contraire au principe d'égalité entre les usagers du service public.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
