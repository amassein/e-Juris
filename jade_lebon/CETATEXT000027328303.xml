<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027328303</ID>
<ANCIEN_ID>JG_L_2013_04_000000335924</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/32/83/CETATEXT000027328303.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 17/04/2013, 335924</TITRE>
<DATE_DEC>2013-04-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>335924</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean Lessi</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:335924.20130417</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu, avec les pièces qui y sont visées, la décision du 7 avril 2011 par laquelle le Conseil d'Etat, statuant au contentieux sur la requête enregistrée sous le n° 335924 présentée par La CIMADE et le Groupe d'information et de soutient des immigrés (GISTI) et tendant à l'annulation, pour excès de pouvoir, de la circulaire interministérielle du 3 novembre 2009 relative à l'allocation temporaire d'attente, a sursis à statuer sur les conclusions de cette requête tendant à l'annulation du point I.2.2 de la première partie de cette circulaire en ce que celui-ci exclut du bénéfice de l'allocation temporaire d'attente les personnes dont la demande d'asile relève de la compétence d'un autre Etat européen ainsi qu'à l'application des dispositions de l'article L. 761-1 du code de justice administrative, jusqu'à ce que la Cour de justice de l'Union européenne se soit prononcée sur les questions de savoir :<br/>
<br/>
              1°) si la directive 2003/9/CE du Conseil du 27 janvier 2003 garantit le bénéfice des conditions minimales d'accueil qu'elle prévoit  aux demandeurs pour lesquels un Etat membre saisi d'une demande d'asile décide, en application du règlement (CE) n° 343/2003 du Conseil du 18 février 2003, de requérir un autre Etat membre qu'il estime responsable de l'examen de cette demande, pendant la durée de la procédure de prise en charge ou de reprise en charge par cet autre Etat membre ;<br/>
<br/>
              2°) en cas de réponse affirmative à cette question :<br/>
<br/>
              a) si l'obligation, incombant au premier Etat membre, de garantir le bénéfice des conditions minimales d'accueil, prend fin au moment de la décision d'acceptation par l'Etat requis, lors de la prise en charge ou reprise en charge effective du demandeur d'asile, ou à une toute autre date ;<br/>
<br/>
              b) à quel Etat membre incombe alors la charge financière de la délivrance des conditions minimales d'accueil pendant cette période ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu le règlement (CE) n° 343/2003 du Conseil du 18 février 2003 ;<br/>
<br/>
              Vu la directive 2003/9/CE du Conseil du 27 janvier 2003 ;<br/>
<br/>
              Vu la directive 2005/85/CE du Conseil du 1er décembre 2005 ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              Vu le code du travail ; <br/>
<br/>
              Vu l'arrêt du 27 septembre 2012 par lequel la Cour de justice de l'Union européenne s'est prononcée sur ces questions ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean Lessi, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du paragraphe 1 de l'article 3 de la directive du 27 janvier 2003 relative à des normes minimales pour l'accueil des demandeurs d'asile dans les Etats membres : " La présente directive s'applique à tous les ressortissants de pays tiers et apatrides qui déposent une demande d'asile à la frontière ou sur le territoire d'un Etat membre tant qu'ils sont autorisés à demeurer sur le territoire en qualité de demandeurs d'asile, ainsi qu'aux membres de leur famille, s'ils sont couverts par cette demande d'asile conformément au droit national " ; qu'aux termes des paragraphes 1 et 2 de l'article 13 de cette directive : " les Etats membres font en sorte que les demandeurs d'asile aient accès aux conditions matérielles d'accueil lorsqu'ils introduisent leur demande d'asile " et " les Etats membres prennent des mesures relatives aux conditions matérielles d'accueil qui permettent de garantir un niveau de vie adéquat pour la santé et d'assurer la subsistance des demandeurs " ; que l'article 2 de cette directive définit les conditions matérielles d'accueil comme " comprenant le logement, la nourriture et l'habillement, fournis en nature ou sous forme d'allocation financière ou de bons, ainsi qu'une allocation journalière " ; <br/>
<br/>
              2. Considérant que dans l'arrêt du 27 septembre 2012 par lequel elle s'est prononcée sur les questions dont le Conseil d'Etat, statuant au contentieux, l'avait saisie à titre préjudiciel, la Cour de Justice de l'Union européenne a dit pour droit que la directive du 27 janvier 2003 devait être interprétée en ce sens qu'un Etat membre saisi d'une demande d'asile est tenu d'octroyer les conditions minimales d'accueil garanties par cette directive, y compris à un demandeur d'asile pour lequel il décide, en application du règlement du Conseil du 18 février 2003 établissant les critères et mécanismes de détermination de l'Etat responsable de l'examen d'une demande d'asile présentée dans l'un des Etats membres par un ressortissant d'un pays tiers, dit " Dublin II, de requérir un autre Etat membre aux fins de prendre en charge ou de reprendre en charge ce demandeur en tant qu'Etat membre responsable de l'examen de sa demande d'asile, et que cette obligation ne prend fin, le cas échéant, que lors du transfert effectif du demandeur par l'Etat membre requérant, la charge financière de l'octroi des conditions minimales incombant, jusqu'à cette date, à ce dernier Etat membre ;<br/>
<br/>
              3. Considérant, d'une part, que si le demandeur d'asile dont la demande relève de la compétence d'un autre Etat européen, que la France décide de requérir en application du règlement du 18 février 2003, peut se voir refuser l'admission au séjour en application du 1° de l'article L. 741-4 du code de l'entrée et du séjour des étrangers et du droit d'asile, il dispose cependant du droit de rester en France en application des dispositions précises et inconditionnelles de l'article 7 de la directive du 1er décembre 2005 relative à des normes minimales concernant la procédure d'octroi et de retrait du statut de réfugié dans les Etats membres, ainsi que l'a d'ailleurs jugé la Cour de justice dans son arrêt du 27 septembre 2012 ; qu'il doit, dès lors, pouvoir accéder aux conditions minimales d'accueil prévues par la directive du 27 janvier 2003 ;<br/>
<br/>
              4. Considérant, d'autre part, que si, l'article L. 5423-8 du code du travail prévoit que " Sous réserve des dispositions de l'article L. 5423-9, peuvent bénéficier d'une allocation temporaire d'attente : / 1° Les ressortissants étrangers dont le titre de séjour ou le récépissé de demande de titre de séjour mentionne qu'ils ont sollicité l'asile en France et qui ont présenté une demande tendant à bénéficier du statut de réfugié, s'ils satisfont à des conditions d'âge et de ressources (...) ",  il résulte de ce qui a été dit ci-dessus que ces dispositions, qui doivent être interprétées à la lumière de la directive du 27 janvier 2003, n'ont pas pour objet et ne sauraient avoir pour effet d'exiger la détention d'un titre de séjour ou d'un récépissé pour le demandeur d'asile dont la demande relève de la compétence d'un autre Etat, que la France décide de requérir en application du règlement du 18 février 2003 ; que, par suite, ce demandeur a, sous réserve des dispositions de l'article L. 5423-9 du code du travail, droit à l'allocation temporaire d'attente lorsqu'il remplit les conditions d'âge et de ressources prévues, jusqu'à ce qu'il ait effectivement été transféré dans l'Etat requis ou, le cas échéant, jusqu'à ce que la France, ayant finalement engagé l'examen de sa demande, se soit prononcée sur celle-ci ; <br/>
<br/>
              5. Considérant que, dans l'intervalle, et en l'absence de dispositions nationales prises pour la transposition de l'article 16 de la directive du 27 janvier 2003, le bénéfice de l'allocation ne saurait être interrompu ;<br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède qu'en excluant du bénéfice de l'allocation temporaire d'attente les personnes dont la demande d'asile relève de la compétence d'un autre Etat européen en application du règlement du 18 février 2003, la circulaire attaquée a donné une interprétation erronée des dispositions législatives précitées ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens de leur requête, La CIMADE et le GISTI sont fondés à demander l'annulation du point I.2.2 de la première partie de la circulaire du 3 novembre 2009 en ce que celui-ci exclut du bénéfice de l'allocation temporaire d'attente les personnes dont la demande d'asile relève de la compétence d'un autre Etat européen ;<br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de La CIMADE et du GISTI, qui ne sont pas, dans la présente instance, les parties perdantes ; qu'en revanche il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 750 euros à verser à La CIMADE et une même somme à verser au GISTI sur le fondement de ces dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le point I.2.2 de la première partie de la circulaire du 3 novembre 2009 relative à l'allocation temporaire d'attente est annulé en tant qu'il exclut du bénéfice de cette allocation les personnes dont l'examen de la demande d'asile relève de la compétence d'un autre Etat européen.<br/>
Article 2 : L'Etat versera à La CIMADE et au GISTI une somme de 750 euros chacun en application de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Les conclusions présentées par l'Etat au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à La CIMADE, au Groupe d'information et de soutien des immigrés, au ministre de l'intérieur et au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">095-02-06-02-02 - DEMANDEUR D'ASILE DONT LA DEMANDE RELÈVE DE LA COMPÉTENCE D'UN AUTRE ETAT MEMBRE DE L'UNION EUROPÉENNE - 1) DROIT DE RESTER EN FRANCE (ART. 7 DE LA DIRECTIVE DU 1ER DÉCEMBRE 2005) - EXISTENCE [RJ1] - CONSÉQUENCE - BÉNÉFICE DES CONDITIONS MINIMALES D'ACCUEIL - 2) DROIT AU BÉNÉFICE DE L'ATA (ART. L. 5423-8 DU CODE DU TRAVAIL) - EXISTENCE - 3) POSSIBILITÉ D'INTERROMPRE CE VERSEMENT - ABSENCE, À DÉFAUT DE TRANSPOSITION DE L'ARTICLE 16 DE LA DIRECTIVE DU 27 JANVIER 2003.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">15-05-045-05 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. RÈGLES APPLICABLES. - DEMANDEUR D'ASILE DONT LA DEMANDE RELÈVE DE LA COMPÉTENCE D'UN AUTRE ETAT MEMBRE DE L'UNION EUROPÉENNE - 1) DROIT DE RESTER EN FRANCE (ART. 7 DE LA DIRECTIVE DU 1ER DÉCEMBRE 2005) - EXISTENCE [RJ1] - CONSÉQUENCE - BÉNÉFICE DES CONDITIONS MINIMALES D'ACCUEIL - 2) DROIT AU BÉNÉFICE DE L'ATA (ART. L. 5423-8 DU CODE DU TRAVAIL) - EXISTENCE - 3) POSSIBILITÉ D'INTERROMPRE CE VERSEMENT - ABSENCE, À DÉFAUT DE TRANSPOSITION DE L'ARTICLE 16 DE LA DIRECTIVE DU 27 JANVIER 2003.
</SCT>
<ANA ID="9A"> 095-02-06-02-02 1) Si le demandeur d'asile dont la demande relève de la compétence d'un autre Etat européen, que la France décide de requérir en application du règlement (CE) n° 343/2003 du Conseil du 18 février 2003, peut se voir refuser l'admission au séjour en application du 1° de l'article L. 741-4 du code de l'entrée et du séjour des étrangers et du droit d'asile, il dispose cependant du droit de rester en France en application des dispositions précises et inconditionnelles de l'article 7 de la directive 2005/85/CE du Conseil du 1er décembre 2005 relative à des normes minimales concernant la procédure d'octroi et de retrait du statut de réfugié dans les Etats membres, ainsi que l'a d'ailleurs jugé la Cour de justice de l'Union européenne dans son arrêt du 27 septembre 2012 (aff. C-179/11). Il doit dès lors pouvoir accéder aux conditions minimales d'accueil prévues par la directive 2003/9/CE du 27 janvier 2003.,,,2) Les dispositions de l'article L. 5423-8 du code du travail énumérant les personnes pouvant bénéficier d'une allocation temporaire d'attente (ATA), qui doivent être interprétées à la lumière de la directive du 27 janvier 2003, n'ont pas pour objet et ne sauraient avoir pour effet d'exiger la détention d'un titre de séjour ou d'un récépissé pour le demandeur d'asile dont la demande relève de la compétence d'un autre Etat, que la France décide de requérir en application du règlement du 18 février 2003. Par suite, ce demandeur a, sous réserve des dispositions de l'article L. 5423-9 du code du travail, droit à l'ATA lorsqu'il remplit les conditions d'âge et de ressources prévues, jusqu'à ce qu'il ait effectivement été transféré dans l'Etat requis ou, le cas échéant, jusqu'à ce que la France, ayant finalement engagé l'examen de sa demande, se soit prononcée sur celle-ci.... ,,3) Dans l'intervalle, et en l'absence de dispositions nationales prises pour la transposition de l'article 16 de la directive du 27 janvier 2003, le bénéfice de l'allocation ne saurait être interrompu.</ANA>
<ANA ID="9B"> 15-05-045-05 1) Si le demandeur d'asile dont la demande relève de la compétence d'un autre Etat européen, que la France décide de requérir en application du règlement (CE) n° 343/2003 du Conseil du 18 février 2003, peut se voir refuser l'admission au séjour en application du 1° de l'article L. 741-4 du code de l'entrée et du séjour des étrangers et du droit d'asile, il dispose cependant du droit de rester en France en application des dispositions précises et inconditionnelles de l'article 7 de la directive 2005/85/CE du Conseil du 1er décembre 2005 relative à des normes minimales concernant la procédure d'octroi et de retrait du statut de réfugié dans les Etats membres, ainsi que l'a d'ailleurs jugé la Cour de justice de l'Union européenne dans son arrêt du 27 septembre 2012 (aff. C-179/11). Il doit dès lors pouvoir accéder aux conditions minimales d'accueil prévues par la directive 2003/9/CE du 27 janvier 2003.,,,2) Les dispositions de l'article L. 5423-8 du code du travail énumérant les personnes pouvant bénéficier d'une allocation temporaire d'attente (ATA), qui doivent être interprétées à la lumière de la directive du 27 janvier 2003, n'ont pas pour objet et ne sauraient avoir pour effet d'exiger la détention d'un titre de séjour ou d'un récépissé pour le demandeur d'asile dont la demande relève de la compétence d'un autre Etat, que la France décide de requérir en application du règlement du 18 février 2003. Par suite, ce demandeur a, sous réserve des dispositions de l'article L. 5423-9 du code du travail, droit à l'ATA lorsqu'il remplit les conditions d'âge et de ressources prévues, jusqu'à ce qu'il ait effectivement été transféré dans l'Etat requis ou, le cas échéant, jusqu'à ce que la France, ayant finalement engagé l'examen de sa demande, se soit prononcée sur celle-ci.... ,,3) Dans l'intervalle, et en l'absence de dispositions nationales prises pour la transposition de l'article 16 de la directive du 27 janvier 2003, le bénéfice de l'allocation ne saurait être interrompu.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. CJUE, 27 septembre 2012, Cimade et GISTI contre Ministre de l'Intérieur, de l'Outre-mer, des Collectivités territoriales et de l'Immigration, aff.  C-179/11.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
