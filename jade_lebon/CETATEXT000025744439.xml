<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025744439</ID>
<ANCIEN_ID>JG_L_2012_04_000000346952</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/74/44/CETATEXT000025744439.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 24/04/2012, 346952, Publié au recueil Lebon</TITRE>
<DATE_DEC>2012-04-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>346952</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>M. Maxime Boutron</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Olléon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:346952.20120424</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et les mémoires complémentaires, enregistrés le 23 février 2011, le 23 mai 2011 et le 2 avril 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour le MINISTRE DE LA CULTURE ET DE LA COMMUNICATION ; le ministre demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 09BX00104 du 23 décembre 2010 par lequel la cour administrative d'appel de Bordeaux a rejeté son recours tendant à l'annulation du jugement n° 0602706 du 20 novembre 2008 par lequel le tribunal administratif de Poitiers, à la demande de M. B..., a annulé l'arrêté du préfet de la région Poitou-Charentes, préfet de la Vienne du 12 mai 2006 déclarant la grotte de Vilhonneur propriété de l'Etat et l'incorporant à son domaine public et au rejet de la demande présentée par M.  A...devant le tribunal administratif de Poitiers ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son recours ; <br/>
<br/>
              3°) de mettre à la charge de M.  A...la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et son premier protocole additionnel ;<br/>
<br/>
              Vu le code civil ;<br/>
<br/>
              Vu le code du patrimoine ;<br/>
<br/>
              Vu la loi n° 2001-44 du 17 janvier 2001 ;<br/>
<br/>
              Vu le décret n° 2004-490 du 3 juin 2004 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Maxime Boutron, Auditeur, <br/>
<br/>
              - les observations de la SCP Lyon-Caen, Thiriez, avocat du MINISTRE DE LA CULTURE ET DE LA COMMUNICATION et de la SCP Gaschignard, avocat de M. A..., <br/>
<br/>
              - les conclusions de M. Laurent Olléon, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Lyon-Caen, Thiriez, avocat du MINISTRE DE LA CULTURE ET DE LA COMMUNICATION et à la SCP Gaschignard, avocat de M. A... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M.  A...est propriétaire depuis 1991 d'un terrain situé sur la commune de Vilhonneur (Charente) ; qu'une équipe de spéléologues a découvert le 8 décembre 2005, dans le tréfonds de ce terrain, un réseau de galeries et de salles décorées de peintures pariétales pouvant dater du paléolithique supérieur ; que cette découverte a été déclarée par l'un des spéléologues le 9 décembre 2005 au service régional de l'archéologie ; que, par délibération du 7 avril 2006, le conseil municipal de Vilhonneur a renoncé à exercer les droits que la commune tenait sur ce vestige archéologique immobilier, par l'effet des dispositions combinées de l'article L. 541-1 du code du patrimoine, de l'article 713 du code civil et de l'article 63 du décret du 3 juin 2004 ; que par arrêté du 12 mai 2006, pris sur le fondement de ces mêmes dispositions, le préfet de la région Poitou-Charentes a estimé que ce vestige dénommé " Grotte de Vilhonneur " présentait un intérêt scientifique, patrimonial et culturel et a décidé qu'il devenait propriété de l'Etat par l'effet de la renonciation de la commune à ses droits et qu'il était incorporé au domaine public de l'Etat ; que le MINISTRE DE LA CULTURE ET DE LA COMMUNICATION se pourvoit en cassation contre l'arrêt du 23 décembre 2010 par lequel la cour administrative d'appel de Bordeaux a rejeté son recours tendant à l'annulation du jugement du 20 novembre 2008 par lequel le tribunal administratif de Poitiers a annulé cet arrêté à la demande de M. A... ;<br/>
<br/>
              Considérant, en premier lieu, que le ministre soutient que la cour a commis une erreur de droit en jugeant que les stipulations de l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales pouvaient être invoquées, alors que les dispositions mentionnées ci-dessus, sur lesquelles l'arrêté du préfet est fondé, n'ont pas eu pour objet ou pour effet de priver le propriétaire du sol du bien situé dans le sous-sol et constitué par ce vestige archéologique immobilier ; <br/>
<br/>
              Considérant qu'aux termes de ces stipulations : " Toute personne physique ou morale a droit au respect de ses biens. Nul ne peut être privé de sa propriété que pour cause d'utilité publique et dans les conditions prévues par la loi et les principes généraux du droit international. / Les dispositions précédentes ne portent pas atteinte au droit que possèdent les Etats de mettre en vigueur les lois qu'ils jugent nécessaires pour réglementer l'usage des biens conformément à l'intérêt général " ; <br/>
<br/>
              Considérant qu'aux termes de l'article 552 du code civil : " La propriété du sol emporte la propriété du dessus et du dessous. / Le propriétaire (...) peut faire au-dessous toutes les constructions et fouilles qu'il jugera à propos, et tirer de ces fouilles tous les produits qu'elles peuvent fournir, sauf les modifications résultant des lois et règlements relatifs aux mines, et des lois et règlements de police " ; que, toutefois, il résulte de l'article 13 de la loi du 17 janvier 2001 relative à l'archéologie préventive, désormais codifié à l'article L. 541-1 du code du patrimoine, que ces dispositions ne sont pas applicables aux vestiges archéologiques immobiliers ; que cet article prévoit en outre que " L'Etat verse au propriétaire du fonds où est situé le vestige une indemnité destinée à compenser le dommage qui peut lui être occasionné pour accéder audit vestige " et que " Lorsque le vestige est découvert fortuitement et qu'il donne lieu à une exploitation, la personne qui assure cette exploitation verse à l'inventeur une indemnité forfaitaire ou, à défaut, intéresse ce dernier au résultat de l'exploitation du vestige " ; qu'aux termes du premier alinéa de l'article 63 du décret du 3 juin 2004 relatif aux procédures administratives et financières en matière d'archéologie préventive, en vigueur à la date des décisions attaquées : " Sauf lorsque le propriétaire du fonds contenant un vestige archéologique immobilier, issu de fouilles ou découvert fortuitement, établit qu'il est propriétaire de ce vestige, un arrêté du préfet de région constate que ce dernier est propriété de l'Etat par l'effet des dispositions du premier alinéa de l'article L. 541-1 du code du patrimoine et de l'article 713 du code civil " ; qu'en vertu de ce dernier article : " Les biens qui n'ont pas de maître appartiennent à la commune sur le territoire de laquelle ils sont situés. Toutefois, la propriété est transférée de plein droit à l'Etat si la commune renonce à exercer ses droits " ; que le deuxième alinéa du même article 63 permet au préfet de région, si l'intérêt archéologique du vestige le justifie, d'autoriser l'incorporation du bien au domaine public affecté au ministère chargé de la culture ; qu'enfin, aux termes du dernier alinéa du même article : " Si, dans un délai de six mois à compter de la découverte du vestige, le préfet n'a procédé ni à son incorporation au domaine public de l'Etat ni à sa cession amiable, l'Etat est réputé avoir renoncé à la propriété de ce vestige. Le propriétaire du fonds peut, à tout moment après l'expiration de ce délai, demander au préfet de constater cette renonciation par un acte qui est publié au fichier immobilier de la conservation des hypothèques dans les conditions de droit commun " ;<br/>
<br/>
              Considérant qu'à compter de la date de leur entrée en vigueur, les dispositions de la loi du 17 janvier 2001, reprises à l'article L. 541-1 du code du patrimoine, ont rendu inapplicable, en ce qui concerne les vestiges archéologiques immobiliers qui s'y trouveraient, la présomption de propriété du sous-sol que l'article 552 du code civil établit au profit du propriétaire du sol ; que ces dispositions font en conséquence obstacle à ce que ceux qui deviennent, à compter de cette date, propriétaires du sol, que ce soit à titre onéreux ou à titre gratuit, puissent se prévaloir de cette présomption pour revendiquer la propriété de tels vestiges ; que l'application de l'article L. 541-1 du code du patrimoine aux personnes se trouvant dans cette situation ne saurait être regardée comme la privation d'un bien au sens des stipulations citées plus haut ;<br/>
<br/>
              Considérant, toutefois, qu'il en va différemment s'agissant des personnes qui étaient propriétaires du sol antérieurement à l'entrée en vigueur de l'article 13 de la loi du 17 janvier 2001, repris à l'article L. 541-1 du code du patrimoine ; <br/>
<br/>
              Considérant, il est vrai, que ces dispositions n'instituent pas une présomption de propriété des vestiges archéologiques immobiliers au bénéfice d'une personne publique et que l'article 63 du décret du 3 juin 2004, pris pour leur application, a réservé la possibilité au propriétaire du fonds de faire la preuve qu'il est propriétaire de ces vestiges, avant la constatation, après renonciation par une commune à ses droits, d'un transfert à l'Etat de la propriété de tels vestiges découverts tant à l'issue de fouilles que de manière fortuite ; <br/>
<br/>
              Mais considérant qu'avant l'entrée en vigueur de l'article 13 de la loi du 17 janvier 2001, le propriétaire du sol était réputé, par l'effet de l'article 552 du code civil, être propriétaire des éléments du sous-sol, sauf preuve contraire devant être apportée par des tiers qui en revendiqueraient la propriété ; que ces dispositions ont ainsi eu pour objet et pour effet de priver le propriétaire du sol, acquis avant leur entrée en vigueur, du bénéfice, qu'il tenait de l'article 552, de la présomption de propriété du sous-sol et, en conséquence, de celle des vestiges archéologiques immobiliers que celui-ci contiendrait et qui seraient découverts à l'occasion des fouilles permises par ce dernier article ; que la possibilité offerte à ce propriétaire, par la combinaison des dispositions de l'article L. 541-1 du code du patrimoine et de l'article 63 du décret du 3 juin 2004, d'apporter la preuve de la propriété d'un tel vestige, situé dans le sous-sol de sa propriété, ne peut en pratique être mise en oeuvre dès lors que, dans la mesure où l'existence de ce vestige était ignorée, il ne dispose d'aucun titre en faisant mention et qu'aucune prescription acquisitive ne peut être invoquée ; <br/>
<br/>
              Considérant que, par suite, la cour, qui a retenu ces éléments, n'a pas commis d'erreur de droit en jugeant que la combinaison de ces dispositions avait en réalité pour objet et pour effet de permettre la dépossession du propriétaire du sol d'une partie du tréfonds, en rendant sans maître des vestiges immobiliers qui appartenaient à ce propriétaire en vertu des dispositions en vigueur avant l'intervention de la loi du 17 janvier 2001 et du règlement pris pour son application ; qu'en en déduisant que ces dispositions portaient atteinte au droit de propriété de M. A..., dans le sous-sol duquel ont été découverts les vestiges archéologiques immobiliers qui lui appartenaient en vertu de la présomption posée par l'article 552 du code civil, et en estimant que, par suite, l'intéressé pouvait se prévaloir des stipulations de l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, la cour n'a pas commis d'erreur de droit ; <br/>
<br/>
              Considérant, en deuxième lieu, que ces dispositions poursuivent un but d'utilité publique au sens de ces stipulations, en ce qu'elles ont été édictées afin d'assurer la conservation et la sauvegarde du patrimoine archéologique ; que, toutefois, elles ne ménagent pas un juste équilibre entre les exigences de l'intérêt général et celles de la sauvegarde du droit de propriété, dès lors que le seul versement au propriétaire du terrain d'une indemnité destinée à compenser le dommage qui peut lui être occasionné pour accéder au vestige archéologique immobilier, prévu par le deuxième alinéa de l'article L. 541-1 du code du patrimoine, ne constitue pas une juste compensation de la privation de la propriété des vestiges eux-mêmes ;<br/>
<br/>
              Considérant qu'en retenant ces motifs pour juger que ces dispositions, en tant qu'elles privent le propriétaire d'un fonds acquis avant leur entrée en vigueur, de la propriété des vestiges archéologiques immobiliers qui se trouvent dans le tréfonds de son terrain, sans aucune compensation, méconnaissent les stipulations de l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, alors même que, comme le soutient le ministre, l'autorité publique assume les frais de conservation et de mise en valeur des vestiges, la cour n'a pas commis d'erreur de droit ;<br/>
<br/>
              Considérant, enfin, qu'en jugeant, après avoir rappelé les données du litige mentionnées ci-dessus, que le préfet de la région Poitou-Charentes ne pouvait légalement, en raison de leur incompatibilité avec ces stipulations, faire application à M.  A...de ces dispositions pour constater, par l'arrêté du 12 mai 2006, que le vestige dénommé " grotte de Vilhonneur " était propriété de l'Etat et l'incorporer au domaine public de l'Etat, la cour n'a pas donné aux faits une qualification inexacte et n'a pas dénaturé les pièces du dossier ;<br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que le MINISTRE DE LA CULTURE ET DE LA COMMUNICATION, qui n'apporte pas de précisions permettant d'apprécier le bien-fondé du moyen tiré de ce que la cour aurait omis de répondre à ses conclusions, n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque ; que son pourvoi doit ainsi être rejeté, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 2 500 euros à verser à M. A..., au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : Le pourvoi du MINISTRE DE LA CULTURE ET DE LA COMMUNICATION est rejeté.<br/>
<br/>
Article 2 : L'Etat versera à M.  A...une somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée au MINISTRE DE LA CULTURE ET DE LA COMMUNICATION et à M. B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-055-02-01 DROITS CIVILS ET INDIVIDUELS. CONVENTION EUROPÉENNE DES DROITS DE L'HOMME. DROITS GARANTIS PAR LES PROTOCOLES. DROIT AU RESPECT DE SES BIENS (ART. 1ER DU PREMIER PROTOCOLE ADDITIONNEL). - SUPPRESSION DE LA PRÉSOMPTION DE PROPRIÉTÉ DU SOUS-SOL DONT BÉNÉFICIAIENT LES PROPRIÉTAIRES DU SOL (ART. L. 541-1 DU CODE DU PATRIMOINE) - PRIVATION D'UN BIEN - 1) S'AGISSANT DE CEUX QUI DEVIENNENT PROPRIÉTAIRES DU SOL APRÈS CETTE SUPPRESSION - ABSENCE - 2) S'AGISSANT DE CEUX QUI ÉTAIENT PROPRIÉTAIRES DU SOL AU MOMENT DE LA SUPPRESSION - A) EXISTENCE - B) CARACTÈRE DISPROPORTIONNÉ - EXISTENCE.
</SCT>
<ANA ID="9A"> 26-055-02-01 L'article L. 541-1 du code du patrimoine a supprimé la présomption de propriété du sous-sol dont bénéficiaient les propriétaires du sol par l'effet de l'article 552 du code civil. 1) L'application de l'article L. 541-1 du code du patrimoine à ceux qui sont devenus propriétaires du sol à compter de l'entrée en vigueur de cet article ne saurait être regardée comme la privation d'un bien au sens des stipulations de l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. 2) a) Il en va différemment s'agissant des personnes qui étaient propriétaires du sol avant l'entrée en vigueur du nouvel article L. 541-1. b) Même si ces dispositions n'instituent pas une présomption de propriété au bénéfice de la personne publique et si l'article 63 du décret n° 2004-490 du 3 juin 2004 réserve la possibilité pour le propriétaire du sol de faire la preuve qu'il est propriétaire des vestiges immobiliers découverts dans le sous-sol, cette dernière possibilité ne peut en pratique être mise en oeuvre dès lors que, dans la mesure où l'existence de ces vestiges était ignorée, le propriétaire ne dispose d'aucun titre en faisant mention et ne peut invoquer aucune prescription acquisitive. Atteinte au bien disproportionnée au but d'utilité publique poursuivi.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
