<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042074726</ID>
<ANCIEN_ID>JG_L_2020_07_000000433079</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/07/47/CETATEXT000042074726.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 01/07/2020, 433079</TITRE>
<DATE_DEC>2020-07-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>433079</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE ; SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR>M. Yves Doutriaux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:433079.20200701</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'association Union Sportive des Clubs du Cortenais (USCC) a demandé au juge des référés du tribunal administratif de Bastia : <br/>
<br/>
              1°) d'ordonner, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de la décision de la commission fédérale des règlements et contentieux de la Fédération française de football en date du 17 mai 2019 infligeant à l'AS Furiani-Agliani la perte par pénalité de la rencontre du championnat de Régional 1 de la Ligue corse de football du 24 mars 2019 l'ayant opposée au FC Balagne et donnant la victoire au FC Balagne ;<br/>
<br/>
              2°) d'enjoindre à la Fédération française de football de constater que son équipe première sénior est classée à la première place du classement de régional 1 à l'issue de la saison 2018/2019 et d'inclure cette équipe au sein du championnat de National 3 lors de la saison 2019/2020.<br/>
<br/>
              Par une ordonnance n° 1900869 du 19 juillet 2019, le juge des référés du tribunal administratif a suspendu l'exécution de la décision de la Fédération française de football et enjoint à cette fédération d'inclure l'équipe première de l'USCC au sein du championnat de National 3 lors de la saison 2019/2020.<br/>
<br/>
              Par un pourvoi et des observations complémentaires, enregistrés les 29 juillet et 27 septembre 2019 au secrétariat du contentieux du Conseil d'Etat, la Fédération française de football demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) de mettre à la charge de l'Union sportive des clubs du Cortenais la somme de 3500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le code du sport ;<br/>
              - les règlements généraux de la Fédération française de football ;<br/>
              - les règlements de la coupe de Corse de la Ligue corse de football ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yves Doutriaux, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat de la Fédération française de football et à la SCP Rousseau, Tapie, avocat de l'union sportive des clubs du Cortenais ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Bastia que, par une décision du 17 mai 2019, la commission fédérale des règlements et contentieux de la Fédération française de football a infligé à l'AS Furiani-Agliani la perte par pénalité de la rencontre du championnat de Régional 1 de la ligue corse de football du 24 mars 2019 l'ayant opposée au FC Balagne et a donné la rencontre gagnée au FC Balagne. L'association Union Sportive des Clubs du Cortenais (USCC) doit être regardée comme ayant demandé au juge des référés de ce tribunal d'ordonner, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de cette décision en tant qu'elle avait pour effet, par la réattribution au profit du FC Balagne des points de la rencontre et par le reclassement en conséquence de ce club à la première place du championnat de Régional 1, de la priver de la promotion en division supérieure de National 3. La Fédération française de football se pourvoit en cassation contre l'ordonnance du 19 juillet 2019 par laquelle le juge des référés a suspendu l'exécution de la décision de la Fédération française de football et enjoint à cette fédération d'inclure l'équipe première de l'USCC au sein du championnat de National 3 lors de la saison 2019/2020. <br/>
<br/>
              2. Il est vrai que, comme le soutient la Fédération française de football, les clubs tiers ne sont pas recevables à contester directement les résultats d'une rencontre et que, dans ces conditions, aurait dû être relevé le défaut d'intérêt pour agir de l'USCC contre la mesure litigieuse, relative au résultat de la rencontre ayant opposé le FC Balagne à l'AS Furiani-Agliani. Toutefois, eu égard à la nature et à l'effet utile de la procédure de référé engagée sur le fondement de l'article L. 521-1 du code de justice administrative, le litige, qui porte sur la détermination des clubs appelés à participer au championnat de National 3 lors de la saison 2019/2020, doit être regardé comme privé d'objet dès la date à laquelle ce championnat a commencé, alors même que la décision contestée a été suspendue par le juge des référés. Par suite, il n'y a plus lieu de statuer sur les conclusions du pourvoi de la Fédération française de football dirigées contre cette suspension. <br/>
<br/>
              3. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions que présentent la Fédération française de football et l'USCC au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de statuer sur les conclusions du pourvoi de la Fédération française de football tendant à l'annulation de l'ordonnance du 19 juillet 2019 du juge des référés du tribunal administratif de Bastia.<br/>
Article 2 : Les conclusions présentées par la Fédération française de football et par l'association Union Sportive des Clubs du Cortenais au titre de l'article L.761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à la Fédération française de football et à l'Union sportive des clubs du Cortenais.<br/>
Copie en sera adressée au FC Balagne, à l'AS Furiani-Agliani et à la Ligue corse de football.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-02-01-02 PROCÉDURE. DIVERSES SORTES DE RECOURS. RECOURS POUR EXCÈS DE POUVOIR. CONDITIONS DE RECEVABILITÉ. - RECEVABILITÉ DU RECOURS DE CLUBS TIERS CONTRE LES RÉSULTATS D'UNE RENCONTRE SPORTIVE - ABSENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-035-02 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ SUSPENSION (ART. L. 521-1 DU CODE DE JUSTICE ADMINISTRATIVE). - NON-LIEU - EXISTENCE - LITIGE PORTANT SUR LA DÉTERMINATION DES CLUBS APPELÉS À PARTICIPER À UN CHAMPIONNAT - CHAMPIONNAT AYANT COMMENCÉ À LA DATE OÙ LE JUGE STATUE [RJ2].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-05-05-02 PROCÉDURE. INCIDENTS. NON-LIEU. EXISTENCE. - RÉFÉRÉ-SUSPENSION (ART. L. 521-1 DU CJA) - LITIGE PORTANT SUR LA DÉTERMINATION DES CLUBS APPELÉS À PARTICIPER À UN CHAMPIONNAT - CHAMPIONNAT AYANT COMMENCÉ À LA DATE OÙ LE JUGE STATUE [RJ2].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">63-05 SPORTS ET JEUX. SPORTS. - RECEVABILITÉ D'UN RECOURS POUR EXCÈS DE POUVOIR DES CLUBS TIERS CONTRE LES RÉSULTATS D'UNE RENCONTRE SPORTIVE- ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 54-02-01-02 Les clubs tiers ne sont pas recevables à contester directement les résultats d'une rencontre sportive.... ,,Par suite, un club tiers n'est pas recevable à former un recours pour excès de pouvoir contre la décision par laquelle la commission fédérale des règlements et contentieux de la Fédération française de football a infligé à un club la perte par pénalité d'une rencontre de championnat en tant qu'elle a pour effet, par la réattribution au profit d'un autre club des points de la rencontre et par le reclassement en conséquence de ce club à la première place du championnat, de priver le club requérant de la promotion en division supérieure.</ANA>
<ANA ID="9B"> 54-035-02 Pourvoi contre une ordonnance du juge des référés suspendant la décision par laquelle la commission fédérale des règlements et contentieux de la Fédération française de football a retiré une décision de relégation, et ayant ainsi pour effet, par la réattribution au profit d'un autre club des points de la rencontre et par le reclassement en conséquence de ce club à la première place d'un championnat, de priver le club requérant de la promotion en division supérieure.,,,Eu égard à la nature et à l'effet utile de la procédure de référé engagée sur le fondement de l'article L. 521-1 du code de justice administrative, le litige, qui porte sur la détermination des clubs appelés à participer à un championnat, doit être regardé comme privé d'objet dès la date à laquelle ce championnat a commencé, alors même que la décision contestée a été suspendue par le juge des référés. Par suite, il n'y a plus lieu de statuer sur les conclusions du pourvoi dirigées contre cette suspension.</ANA>
<ANA ID="9C"> 54-05-05-02 Pourvoi contre une ordonnance du juge des référés suspendant la décision par laquelle la commission fédérale des règlements et contentieux de la Fédération française de football a retiré une décision de relégation, et ayant ainsi pour effet, par la réattribution au profit d'un autre club des points de la rencontre et par le reclassement en conséquence de ce club à la première place d'un championnat, de priver le club requérant de la promotion en division supérieure.,,,Eu égard à la nature et à l'effet utile de la procédure de référé engagée sur le fondement de l'article L. 521-1 du code de justice administrative (CJA), le litige, qui porte sur la détermination des clubs appelés à participer à un championnat, doit être regardé comme privé d'objet dès la date à laquelle ce championnat a commencé, alors même que la décision contestée a été suspendue par le juge des référés. Par suite, il n'y a plus lieu de statuer sur les conclusions du pourvoi dirigées contre cette suspension.</ANA>
<ANA ID="9D"> 63-05 Les clubs tiers ne sont pas recevables à contester directement les résultats d'une rencontre sportive.... ,,Par suite, un club tiers n'est pas recevable à former un recours pour excès de pouvoir contre la décision par laquelle la commission fédérale des règlements et contentieux de la Fédération française de football a infligé à un club la perte par pénalité d'une rencontre de championnat en tant qu'elle a pour effet, par la réattribution au profit d'un autre club des points de la rencontre et par le reclassement en conséquence de ce club à la première place du championnat, de le priver de la promotion en division supérieure.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 4 avril 2008, SASP Rodez Aveyron Football et autre, n° 295007, aux Tables sur un autre point. Comp., sur la recevabilité de l'exception d'illégalité de décisions non définitives arrêtant les résultats d'un match soulevée à l'occasion d'un recours contre l'homologation du classement final, CE, Section, 25 juin 2001, Société à objet sportif Toulouse Football Club, n° 234363, p. 281 ; CE, 4 avril 2008, SASP Rodez Aveyron Football et autre, n° 295007, T. p. 945.,,[RJ2] Cf. CE, décision du même jour, EUSRL Gazélec FC Ajaccio, n° 433747, inédite au Recueil.,.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
