<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026420316</ID>
<ANCIEN_ID>JG_L_2012_09_000000345568</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/42/03/CETATEXT000026420316.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 26/09/2012, 345568</TITRE>
<DATE_DEC>2012-09-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>345568</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>HAAS ; SPINOSI</AVOCATS>
<RAPPORTEUR>M. Christophe Pourreau</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:345568.20120926</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 5 janvier et 15 février 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune de Martigues, représentée par son maire ; la commune de Martigues demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09MA01097 du 4 novembre 2010 par lequel la cour administrative d'appel de Marseille a rejeté l'appel qu'elle a interjeté du jugement n° 0803617 du 17 février 2009 par lequel le tribunal administratif de Marseille a fait droit à la demande de M. A...et autres tendant à l'annulation, d'une part, de la délibération n° 08-093 du 28 mars 2008 de son conseil municipal en tant qu'elle fixe la composition des treize commissions municipales permanentes créées par la même délibération et, d'autre part, des délibérations du même jour portant élection des conseillers municipaux au sein des commissions " sports ", " travaux ", " tourisme, animation, commerce et artisanat ", " enseignement et activités post et périscolaires ", " affaires sociales et solidarité ", " culture ", " jeunesse ", " prévention et sécurité civile " et " circulation et stationnement " ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de M. A...la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu la loi n° 92-125 du 6 février 1992 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christophe Pourreau, Maître des Requêtes,<br/>
<br/>
              - les observations de Me Haas, avocat de la commune de Martigues et de Me Spinosi, avocat de M. B...A...,<br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à Me Haas, avocat de la commune de Martigues et à Me Spinosi, avocat de M. B...A...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une délibération n° 08-093 du 28 mars 2008, le conseil municipal de la commune de Martigues a approuvé la création de treize commissions municipales permanentes compétentes dans chacun des domaines suivants : " administration générale et finances ", " sports ", " urbanisme ", " travaux ", " tourisme, animation, commerce et artisanat ", " enseignement et activités post et périscolaires ", " environnement et développement durable ", " affaires sociales et solidarité ", " culture ", " participation des citoyens à la vie locale ", " jeunesse ", " prévention et sécurité civile " et " circulation et stationnement " ; que, par treize autres délibérations adoptées le même jour, il a été procédé à l'élection des conseillers municipaux appelés à siéger au sein de ces différentes commissions ; qu'en vertu de ces délibérations, la liste " Ensemble pour Martigues citoyenne, écologique et solidaire ", dont l'unique élu siégeant au conseil municipal est M. A..., a été représentée dans les commissions " administration générale et finances ", " environnement et développement durable ", " urbanisme " et " participation des citoyens à la vie locale " ; que la commune de Martigues se pourvoit en cassation contre l'arrêt du 4 novembre 2010 par lequel la cour administrative d'appel de Marseille a rejeté l'appel qu'elle a interjeté du jugement du 17 février 2009 par lequel le tribunal administratif de Marseille, faisant droit à la demande de M. A..., a annulé la délibération n° 08-093 précitée, en tant qu'elle fixe la composition des différentes commissions municipales permanentes, ainsi que les neuf délibérations relatives à l'élection des membres des commissions dans lesquelles la liste " Ensemble pour Martigues citoyenne, écologique et solidaire " n'a pas été représentée ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 2121-22 du code général des collectivités territoriales, issu de l'article 33 de la loi du 6 février 1992 relative à l'organisation territoriale de la République : " Le conseil municipal peut former, au cours de chaque séance, des commissions chargées d'étudier les questions soumises au conseil soit par l'administration, soit à l'initiative d'un de ses membres. / [...] Dans les communes de plus de 3 500 habitants, la composition des différentes commissions, y compris les commissions d'appel d'offres et les bureaux d'adjudications, doit respecter le principe de la représentation proportionnelle pour permettre l'expression pluraliste des élus au sein de l'assemblée communale " ;<br/>
<br/>
              3. Considérant qu'en vertu des dispositions issues de l'article 34 de la loi du 6 février 1992, dans les communes de 3 500 habitants et plus, les commissions d'appel d'offres sont composées du maire ou de son représentant, président, et de cinq membres du conseil municipal élus en son sein à la représentation proportionnelle au plus fort reste ;<br/>
<br/>
              4. Considérant qu'il résulte de ces dispositions, éclairées par les travaux préparatoires à l'adoption de la loi du 6 février 1992, que, dans les communes de plus de 3 500 habitants, l'expression du pluralisme des élus au sein de l'assemblée communale  est garanti, pour les commissions d'appel d'offres, par l'élection à la représentation proportionnelle au plus fort reste des cinq membres appelés à y siéger aux côtés du maire ou de son représentant et, pour les autres commissions municipales, par la représentation proportionnelle des différentes tendances du conseil municipal, telles qu'elles existent à la date de formation de chacune des commissions, sous réserve que chaque tendance, quel que soit le nombre des élus qui la composent, ait la possibilité d'y être représentée ;  <br/>
<br/>
              5. Considérant, dès lors, qu'en jugeant que, sans préjudice des dispositions régissant la composition des commissions d'appel d'offres, les dispositions de l'article L. 2121-22 du code général des collectivités territoriales imposent, pour les commissions que forme le conseil municipal et dont il détermine librement le nombre de membres, que soit recherchée, dans le respect du principe de représentation proportionnelle, une pondération qui reflète fidèlement la composition de l'assemblée municipale et qui assure à chacune des tendances représentées en son sein la possibilité d'avoir au moins un représentant dans chaque commission, sans que les différentes tendances ne bénéficient nécessairement toujours d'un nombre de représentants strictement proportionnel au nombre de conseillers municipaux qui les composent, la cour n'a pas commis d'erreur de droit ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que la commune de Martigues n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque ; que, par suite, ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent qu'être rejetées ; qu'en revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Martigues la somme de 3 000 euros à verser à M. A...au titre de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la commune de Martigues est rejeté.<br/>
Article 2 : La commune de Martigues versera la somme de 3 000 euros à M.  A...au titre de <br/>
l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la commune de Martigues et à M. B... A....<br/>
Copie en sera transmise, pour information, au ministre de l'intérieur.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-02-01-02 COLLECTIVITÉS TERRITORIALES. COMMUNE. ORGANISATION DE LA COMMUNE. ORGANES DE LA COMMUNE. - COMMISSIONS MUNICIPALES - GARANTIE DE L'EXPRESSION DU PLURALISME DES ÉLUS AU SEIN DE L'ASSEMBLÉE COMMUNALE - MODALITÉS - 1) COMMISSIONS D'APPEL D'OFFRES - REPRÉSENTATION PROPORTIONNELLE AU PLUS FORT RESTE DES CINQ MEMBRES APPELÉS À Y SIÉGER AUX CÔTÉS DU MAIRE OU DE SON REPRÉSENTANT - 2) AUTRES COMMISSIONS - REPRÉSENTATION PROPORTIONNELLE DES DIFFÉRENTES TENDANCES DU CONSEIL MUNICIPAL, SOUS RÉSERVE QUE CHAQUE TENDANCE AIT LA POSSIBILITÉ D'Y ÊTRE REPRÉSENTÉE.
</SCT>
<ANA ID="9A"> 135-02-01-02 Il résulte des dispositions de l'article L. 2121-22 du code général des collectivités territoriales, issues de l'article 33 de la loi n° 92-125 du 6 février 1992 relative à l'organisation territoriale de la République, et des dispositions issues de l'article 34 de la même loi, éclairées par les travaux préparatoires à l'adoption de cette loi, que, dans les communes de plus de 3 500 habitants, l'expression du pluralisme des élus au sein de l'assemblée communale est garanti : 1) pour les commissions d'appel d'offres, par l'élection à la représentation proportionnelle au plus fort reste des cinq membres appelés à y siéger aux côtés du maire ou de son représentant, et 2) pour les autres commissions municipales, par la représentation proportionnelle des différentes tendances du conseil municipal, telles qu'elles existent à la date de formation de chacune des commissions, sous réserve que chaque tendance, quel que soit le nombre des élus qui la composent, ait la possibilité d'y être représentée.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
