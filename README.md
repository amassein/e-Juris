# e-Juris_projet

Ce projet vise à exploiter un corpus de décision de justice du Conseil d'Etat, et des cours d'appels administratives. 
Dans un premier temps, il s'agit d'affilier les décisions à leurs jurisprudence, en fonction des arrêts du Conseil d'Etat,
en particulier les arrêts publiés au registre Lebon (A & B). 

Pour vérifier la bonne attribution des arrêts à leur(s) jurisprudences, nous vérifions le seuil du cosinus auquel la
similarité est de 100% pour chaque considérant de principe, que l'on retrouve dans les sommaires des arrêts du conseil d'Etat. 
S'il n'est pas possible de trouver un cosinus pour lequel nous sommes sûrs de récupérer tous les bons individus, nous pouvons fixer le seuil du cosinus relativement bas, tout en contrôlant avec plusieurs méthodes l'affiliation entre un considérant et un considérant de principe.

Pourquoi est-ce que l'on reste sur du BOW ? :
- Langage juridique très codifié, on cherche à voir quand est-ce que les mêmes formulations (+ ou -) reviennent dans les considérants. Cela suppose que l'on cherche quand est-ce que l'on voit les mêmes mots. L'ordre a une certaine importance, mais il semble que cela fonctionne plutôt bien sans avoir à conserver l'ordre des mots.
- Il faut éviter de lemmatiser ou stemmatiser, dans la mesure où la conjuguaison d'un mot a une importance dans notre cas. Chaque mot étant important dans une phrase, le BOW fonctionne plutôt très bien. Les embeddings pourraient également fonctionner, parce qu'il prendre également en compte le contexte du document. Cependant, le contexte doit parfois être écarté pour rechercher le considérant de principe (Mr A... a fait...). Le fait de ne pas prendre en compte ce contexte donne, peut-être, un avantage au BOW, qui traite quantitativement le nombre de mot. 
- Egalement, nous n'utilisons pas le TFxIDF, dans la mesure où nous sommes plus intéressés par le nombre de mots dans un document qu'à leur fréquence d'apparition dans les documents. 



# Bugs et Travaux :

    ### PBME avec la méthode du cos 
	- Ne repère pas les formes intéressantes des jurisprudences (celles qui sont les moins similaires)
		- On ne peut pas checker sans annotations : mais 3 millions de docs...
		- On peut également réfléchier à utiliser des embeddings avec CamemBERT, juriBERT, ou encore doc2vec (mais mauvais résultats dans un premier test)
	### On peut chercher vers le machine learning, avec un premier traitement avec le cos.
		- Utiliser la matrice doc/mots à annoter avec une jurisprudence (sklearning).
		- Faire un test avec la juris danthony
		- Voir si on retrouve les considérants les moins similaires
		- L'idée est surtout de voir si l'on peut retrouver tous les considérants à partir d'un entraînement sur les individus les plus ressemblants (cos > 0.5/6)
		- Demande dans tous les cas d'annoter un sous-corpus, donc il faut récupérer des considérants avec le cos.
		- L'avantage, c'est qu'on peut travailler uniquement sur les CP qui sont les plus utilisés, et donc avoir un nombre de documents conséquents pour entraîner un modèle.  

        - Méthode cos pose le problème de ne pas être certains des associations : donc pas de certitudes sur les résultats chiffrés et individuels : marche cependant très bien pour étudier les tendances et les proportions.
        
# Fonctions développées

	Voir librairie-ejuris.py
	
# Fichiers 

	- all_fig.py : En lien avec Dash_render2.py : créé les figures pour le rendu sur Dash
	- Check_files.py : vérifie les fichiers sélectionnés pour les métadonnées, les considérants, les visas, en fonction de l'option choisies. Enregistre les fichiers qui ne sont pas sélectionnés dans un fichier texte /Result_temp/no_(consid | content | visa).txt
	- Dash_render2.py & Dash_Renderer.py : rendu du projet sous Dash
	- Dico_simi.py : créé et exporte le dictionnaire des doublons au sein des jurisprudences.
	- download_jade.py :  télécharge la base de donnée JADE
	- Export_simi_without_juris : programme qui réalise les associations entre jurisprudences et considérants. Exporte les résultats dans un fichier pickle dans "./Result_temp/futur_tab_without_juris.xport"
	- jade_articles.txt / jade_codes.txt / jades_matières.txt / jade_patterns.txt / jade_rewrite.txt : expressions régulières pour repérer des élements dans les considérants. Peut être améliorer avec NER ? Fonctionne avec jade_setup_class.py.
	- jade_index_all.py : créé l'index de similarité des considérants
	- jade_index.py : créé l'index de similarité des jurisprudences
	- jade_mining_somm.py : traite les considérants de principe : dictionnaire + corpus vectorisé
	- Jade_mining2.py : évolution de Jade_mining.py : dictionnaire + corpus pour les considérants
	- jade_setup_class.py : créé les classes qui permettent le traitement des documents textes : créé les tokens.
	- Legal_item.py : Sélectionne et exporte les informations liées aux références légales que l'on retrouve dans les visas des décisions.
	- Libraire_ejuris.py : regroupe les variables d'envrionnement nécessaires au fonctionnement des programmes + les fonctions qui permettent de faire fonctionner les programmes (un peu lourd sur la RAM au vu des variables chargées en mémoire). 
	- Metadata.py : Sélectionne et exporte les métadonnées que l'on retrouve dans les visas des décisions
	- Opti_cos_CP.py : calcul le cosinus optimal pour sélectionner les considérants associés à une jurisprudence
	- Pipe_jade_dependency : Programme qui automatise la création des différentes dépendances issues de la base JADE : dictionnaires, corpus , indexs
	- Simi_load.py : exporte le fichier pickle issu de l'association jurisprudence/considérants vers des fichiers parquet, csv
	

# Dossier

	- Jade_dependency : Dépendances intégrer au sein de librairie_ejuris.py : dictionnaires, index de similarité des jurisprudence, corpus vectorisé, ID des considérants,... Voir Librairie_ejuris.py
		- 
	- Jade_index_all : index de similarité des considérants.
	- Result_temp : export des résultats et des tableaux utilisés dans les analyses
    	- code_pcja.parquet.gzip : liste des codes PCJA annotée
		- Dico_simi.xp : doublons des jurisprudences
		- export_metadata.xp : pickle des métadonnées
		- futur_tab_without_juris2.xport : pickle des associations jurisprudences/considérants
		- legal_cod.parquet.gzip : matrice des codes
		- legal_Corpus.mm, legal_Coprus.mm.index, legal_Dico.dict : dépendences pour créer le tableau des références légales
		- legal.parquet.gzip : tableau de l'ensemble des références légales (1878 variables)
		- Matrice_cos_som_parquet.gzip : matrice des jurisprudences entre elles
		- metadata_recod.parquet.gzip : tableau des métadonnées
		- pcja_cp_extend : code pcja des jurisprudences
		- pcja_with_descri_1_level.py : code pcja avec description des niveaux
		- jointure entre tous les tableaux ( sauf référence légales )
		- tab_without_juris.parquet.gzip : tableau des associations jurisprudence/considérants uniquement.
		- Result_tab :
			- merge_tab_SOM.parquet.gzip : métadonnées sur les jurisprudences + associations
			- merge_tab : métadonnées sur les considérants + associations
		- export_tab : export parquet de futur_tab_without_juris.xport
