<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030525508</ID>
<ANCIEN_ID>JG_L_2015_04_000000374020</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/52/55/CETATEXT000030525508.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère sous-section jugeant seule, 27/04/2015, 374020, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-04-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>374020</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR>M. Laurent Cytermann</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:374020.20150427</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Lyon :<br/>
              - d'annuler pour excès de pouvoir la décision implicite de rejet née du silence gardé par le recteur de l'académie de Lyon sur son recours gracieux du 8 novembre 2010, d'enjoindre à ce recteur d'établir son obligation réglementaire de service à une durée de huit heures hebdomadaires et de réparer le préjudice matériel et moral causé par l'irrégularité de la fixation de ses obligations ;<br/>
              - d'annuler pour excès de pouvoir la décision du 29 novembre 2011 de ce recteur rejetant son recours gracieux du 14 novembre 2011, d'enjoindre à ce recteur d'établir son obligation réglementaire de service à une durée de huit heures hebdomadaires et de réparer le préjudice matériel et moral causé par l'irrégularité de la fixation de ses obligations ;<br/>
              - d'annuler pour excès de pouvoir la décision du 7 janvier 2013 de ce recteur rejetant son recours gracieux du 10 décembre 2012, d'enjoindre à ce recteur d'établir son obligation réglementaire de service à une durée de huit heures hebdomadaires et de réparer le préjudice matériel et moral causé par l'irrégularité de la fixation de ses obligations.<br/>
<br/>
              Par un jugement nos 1101568, 1200927, 1301894 du 16 octobre 2013, le tribunal administratif de Lyon a rejeté ses demandes.<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 16 décembre 2013 et 17 mars 2014 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement du tribunal administratif de Lyon du 16 octobre 2013 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier.<br/>
<br/>
              Vu :<br/>
              - le décret n° 50-581 du 25 mai 1950 ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Cytermann, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de M.B....<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Par trois requêtes, M.B..., professeur de chaire supérieure en langues vivantes, a demandé au tribunal administratif de Lyon d'annuler pour excès de pouvoir les décisions par lesquelles le recteur de l'académie de Lyon a rejeté ses recours gracieux dirigés contre les décisions fixant les heures d'enseignement mises à sa charge, d'enjoindre à ce recteur d'établir son obligation réglementaire de service à une durée de huit heures hebdomadaires et de réparer les préjudices matériels et moraux qu'il estime avoir subis en raison de la fixation irrégulière de ses obligations. Par un jugement du 16 octobre 2013, contre lequel M. B... se pourvoit en cassation, le tribunal administratif de Lyon a rejeté ces requêtes.<br/>
<br/>
              Sur les conclusions relatives aux obligations réglementaires de service de M. B... pour les années antérieures à l'année scolaire 2010-2011 :<br/>
<br/>
              2. Il ressort des pièces des dossiers soumis aux juges du fond que le recours gracieux formé le 8 novembre 2010 par M. B...était dirigé tant contre la nouvelle répartition des heures d'enseignement mises à sa charge à compter de la rentrée scolaire 2010 que contre cette répartition pour les années scolaires antérieures. La décision implicite résultant du silence gardé sur ce recours par le recteur de l'académie de Lyon, dont M. B...a demandé l'annulation pour excès de pouvoir, devait ainsi être regardée comme portant tant sur l'année scolaire 2010-2011 que sur les années scolaires antérieures. Par suite, en statuant sur les conclusions dirigées contre cette décision en tant seulement qu'elles concernaient les heures d'enseignement mises à la charge de M. B...à compter de la rentrée 2010, le tribunal a omis de statuer sur une partie des conclusions dont il était saisi.<br/>
<br/>
              Sur les conclusions relatives aux obligations réglementaires de service de M. B... pour les années scolaires 2010-2011, 2011-2012 et 2012-2013 :<br/>
<br/>
              3. Aux termes de l'article 7 du décret du 25 mai 1950 portant règlement d'administration publique pour la fixation des maximums de service hebdomadaire du personnel enseignant des établissements d'enseignement du second degré : " 1° Le maximum de service des professeurs de philosophie, lettres, histoire et géographie ou langues vivantes qui donnent tout leur enseignement dans la classe de première supérieure, dans celle de lettres supérieures, dans les classes préparatoires aux Ecoles normales supérieures (section des lettres), (...) est fixé ainsi qu'il suit : / Classes de Première supérieure : / Classes ayant un effectif de plus de 35 élèves : 8 heures ; / Classes ayant un effectif de 20 à 35 élèves : 9 heures ; / Classes ayant un effectif de moins de 20 élèves : 10 heures (...) / Les professeurs de philosophie, lettres, histoire et géographie ou langues vivantes dont le service est partagé entre la classe de première supérieure et celle de lettres supérieures ont le même maximum de service que s'ils donnaient tout leur enseignement en première supérieure. / 2° Le maximum de service des professeurs qui n'assurent dans la classe de première supérieure ou dans celle de lettres supérieures qu'une partie de leur service est fixé conformément aux articles 1er et 4 du présent décret. Toutefois, chaque heure d'enseignement faite soit en première supérieure, soit en lettres supérieures est comptée pour une heure et demie, sous réserve : (...) ; / b) Que le maximum de service effectif du professeur ne devienne pas, de ce fait, inférieur à celui prévu au 1° ci-dessus pour un professeur donnant tout son enseignement dans lesdites classes (...) ". Selon ce même article 7, la même règle est applicable aux professeurs de philosophie, lettres, histoire et géographie ou langues vivantes qui enseignent dans les classes de mathématiques spéciales, de mathématiques supérieures, dans les autres classes préparatoires aux grandes écoles dont la liste est fixée par décision ministérielle, sous réserve que le maximum de service de ces professeurs ne soit pas inférieur, pour les classes de mathématiques spéciales et classes préparatoires à l'Ecole normale supérieure, à 10, 11 ou 12 heures selon l'effectif des classes et, pour les classes de mathématiques supérieures et autres classes préparatoires aux grandes écoles, à 11, 12 ou 13 heures selon l'effectif des classes.<br/>
<br/>
              4. En premier lieu, eu égard à l'objet des dispositions de l'article 7 du décret du 15 mai 1950, le terme de classe doit être regardé, au sens et pour l'application de ces dernières, comme faisant référence au groupe d'élèves auxquels le professeur dispense de manière habituelle son enseignement. Par suite, le tribunal n'a pas commis d'erreur de droit en retenant, pour l'application de ces dispositions, l'effectif des élèves suivant la discipline enseignée par M. B... et non celui des divisions dont ils étaient issus. <br/>
<br/>
              5. En deuxième lieu, M. B...soutient que le tribunal administratif a commis une erreur de droit en jugeant que, du seul fait de son intervention devant des classes préparatoires scientifiques au cours des années 2010-2011 et 2011-2012, il devait être regardé comme relevant du 2° de l'article 7 du décret du 25 mai 1950, relatif aux professeurs qui n'assurent dans les classes littéraires qu'une partie de leur service, sans tenir compte de ce qu'il effectuait un service complet devant des classes préparatoires littéraires et n'intervenait devant des classes préparatoires scientifiques qu'au titre d'heures supplémentaires, de sorte qu'il relevait du 1° du même article. Il résulte toutefois des termes du jugement attaqué, lequel n'est pas entaché de dénaturation sur ce point, que, durant les années scolaires en litige, le nombre des élèves devant lesquels M. B...dispensait ses enseignements se situait entre dix et vingt, de sorte qu'une obligation réglementaire de services fixée à 10 heures hebdomadaires n'excédait pas le maximum prévu, pour les classes correspondant à de tels effectifs, par le 1° de ce même article. Ce motif, qui ne comporte aucune nouvelle appréciation des circonstances de fait, doit être substitué au motif erroné retenu par le jugement attaqué du tribunal administratif, dont il justifie légalement le dispositif.<br/>
<br/>
              6. En troisième lieu, en faisant application à M.B..., pour l'année scolaire 2012-2013, des dispositions du 2° de cet article, alors même qu'il n'était pas contesté qu'il assurait au cours de cette année la totalité de son service devant des classes préparatoires littéraires et relevait dès lors des dispositions du 1° de ce même article, le tribunal a entaché son jugement d'erreur de droit. Cependant, pour les raisons exposées ci-dessus, il y a lieu de substituer à ce motif erroné en droit celui tiré de ce que, dès lors que l'effectif des classes devant lesquelles enseignait le requérant n'excédait pas vingt élèves, ainsi que le tribunal l'a relevé, son obligation réglementaire de services pouvait légalement être fixée à 10 heures hebdomadaires sur le fondement du 1° de l'article 7 du décret du 25 mai 1950.<br/>
<br/>
              7. Il résulte de tout ce qui précède que le requérant est fondé à demander l'annulation du jugement qu'il attaque en tant seulement qu'il ne statue pas sur les années antérieures à 2010. <br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à M. B...d'une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Lyon du 16 octobre 2013 est annulé en tant qu'il ne statue pas sur la demande de M. B...portant sur les années antérieures à 2010.<br/>
Article 2 : L'affaire est renvoyée, dans la mesure de la cassation prononcée à l'article 1er, au tribunal administratif de Lyon.<br/>
Article 3 : L'Etat versera à M. B...une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Le surplus des conclusions du pourvoi de M. B...est rejeté.<br/>
Article 5 : La présente décision sera notifiée à M. A...B...et à la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
