<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026230077</ID>
<ANCIEN_ID>JG_L_2012_07_000000323669</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/23/00/CETATEXT000026230077.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 27/07/2012, 323669, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-07-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>323669</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Philippe Martin</PRESIDENT>
<AVOCATS>FOUSSARD ; SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Nicolas Labrune</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:323669.20120727</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la décision du 14 juin 2010 par laquelle le Conseil d'Etat statuant au contentieux a ordonné avant dire droit, après avoir annulé, sur pourvoi de l'Office français de protection des réfugiés et apatrides (OFPRA), la décision du 6 novembre 2008 de la Cour nationale du droit d'asile reconnaissant la qualité de réfugié à M. Aziz A, la convocation de celui-ci à une audience d'instruction ;<br/>
<br/>
              Vu le procès-verbal de l'audience d'instruction tenue par la 10ème sous-section le 3 novembre 2010 ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention relative au statut des réfugiés signée à Genève le 28 juillet 1951 et le protocole signé à New York le 31 janvier 1967 ;<br/>
<br/>
              Vu la directive 2004/83/CE du Conseil du 29 avril 2004, notamment son article 10 ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Nicolas Labrune, Auditeur,<br/>
<br/>
              - les observations de la SCP Rocheteau, Uzan-Sarano, avocat de M. A et de Me Foussard, avocat de l'Office français de protection des réfugiés et apatrides,<br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Rocheteau, Uzan-Sarano, avocat de M. A et à Me Foussard, avocat de l'Office français de protection des réfugiés et apatrides ;<br/>
<br/>
<br/>
<br/>
<br/>1.	Considérant que, pour statuer sur la demande d'asile présentée par M. A, de nationalité afghane, le Conseil d'Etat, statuant au contentieux, a, par la décision visée ci-dessus du 14 juin 2010, ordonné une audience d'instruction au cours de laquelle ont été entendus l'intéressé et l'Office français de protection des réfugiés et apatrides ;<br/>
<br/>
              2.	Considérant, d'une part, qu'aux termes du 2° du A de l'article 1er de la convention de Genève du 28 juillet 1951 relative au statut des réfugiés, la qualité de réfugié est reconnue à " toute personne qui, craignant avec raison d'être persécutée du fait de sa race, de sa religion, de sa nationalité, de son appartenance à un certain groupe social ou de ses opinions politiques, se trouve hors du pays dont elle a la nationalité et qui ne peut ou, du fait de cette crainte, ne veut se réclamer de la protection de ce pays (...) " ; qu'au regard de ces stipulations, les opinions politiques susceptibles d'ouvrir droit à la protection ne peuvent être regardées comme résultant d'un engagement au sein d'une institution de l'Etat que lorsque celle-ci subordonne l'accès des personnes à un emploi en son sein à une adhésion à de telles opinions, ou agit sur leur seul fondement, ou combat exclusivement tous ceux qui s'y opposent ; qu'il y a également lieu de tenir compte, pour interpréter ces stipulations, des dispositions du d) du paragraphe 1 de l'article 10 de la directive du 29 avril 2004 concernant les normes minimales relatives aux conditions que doivent remplir les ressortissants des pays tiers ou les apatrides pour pouvoir prétendre au statut de réfugié, selon lesquelles " un groupe est considéré comme un certain groupe social lorsque, en particulier : / ses membres partagent une caractéristique innée ou une histoire commune qui ne peut être modifiée, ou encore une caractéristique ou une croyance à ce point essentielle pour l'identité ou la conscience qu'il ne devrait pas être exigé d'une personne qu'elle y renonce, et / ce groupe a son identité propre dans le pays en question parce qu'il est perçu comme étant différent par la société environnante " ; que la seule appartenance à une institution telle que l'armée, la police, les services secrets ou la magistrature, qui est créée par l'Etat, ne peut dès lors être assimilée à l'appartenance à un groupe social au sens de la convention de Genève ;<br/>
<br/>
              3.	Considérant qu'il résulte de l'instruction que M. A est originaire de la province de Wardak ; qu'il aurait décidé de s'engager en 2006 comme informateur pour les services secrets afghans ; que sa mission dans la province de Ghazni ayant été découverte par les Talibans, il aurait fait l'objet au titre de ses fonctions de menaces et a décidé de quitter l'Afghanistan à la fin de l'année 2007 ; que, dans ces circonstances, le seul fait, à le supposer avéré, qu'il ait appartenu aux services secrets afghans et qu'il ait subi des menaces à ce titre ne peut en tout état de cause pas être assimilé à l'appartenance à un groupe social au sens de la convention de Genève ; qu'il ne ressort pas non plus des déclarations de l'intéressé faites lors de l'audience d'instruction que son engagement dans les services secrets correspondrait à l'expression d'une opinion politique au sens de la même convention, ni que les services  secrets afghans fonctionnent de façon telle qu'y appartenir puisse être regardé comme révélant une opinion ou un engagement politique ; qu'il n'est donc pas fondé à se réclamer des stipulations du 2° du A de l'article 1er de la convention de Genève ; que, dès lors, c'est à bon droit que, par sa décision du 7 mai 2008, le directeur général de l'Office français de protection des réfugiés et apatrides a refusé de lui reconnaître la qualité de réfugié ;<br/>
<br/>
              4.	Considérant, d'autre part, qu'aux termes de l'article L. 712-1 du code de l'entrée et du séjour des étrangers et du droit d'asile : " (...) le bénéfice de la protection subsidiaire est accordé à toute personne qui ne remplit pas les conditions pour se voir reconnaître la qualité de réfugié (...) et qui établit qu'elle est exposée dans son pays à l'une des menaces graves suivantes : / a) La peine de mort ; / b) La torture ou des peines ou traitements inhumains ou dégradants ; / c) S'agissant d'un civil, une menace grave, directe et individuelle contre sa vie ou sa personne en raison d'une violence généralisée résultant d'une situation de conflit armé interne ou international " ;<br/>
<br/>
              5.	Considérant que M. A a indiqué, notamment lors de l'audience d'instruction, qu'il s'exposerait, en cas de retour en Afghanistan, à être recherché par les Talibans en raison de ses anciennes fonctions et que sa sécurité ne pourrait pas être assurée par les autorités afghanes, sans apporter aucun élément précis au soutien de ses allégations ; qu'il ne résulte pas de l'instruction qu'il risque d'y subir des traitements inhumains ou dégradants, y compris de la part des autorités afghanes ; qu'il ne peut donc pas se prévaloir du bénéfice de la protection subsidiaire ;<br/>
<br/>
              6.	Considérant qu'il résulte de tout ce qui précède que la requête de M. A ne peut qu'être rejetée, y compris les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>				D E C I D E  :<br/>
              				--------------<br/>
<br/>
Article 1er : La requête de M. A est rejetée.<br/>
Article 2 : La présente décision sera notifiée à l'Office français de protection des réfugiés et apatrides et à M. Aziz A.<br/>
Copie en sera adressée pour information au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
