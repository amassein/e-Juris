<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027164318</ID>
<ANCIEN_ID>JG_L_2013_03_000000364191</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/16/43/CETATEXT000027164318.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 11/03/2013, 364191, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-03-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>364191</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP BOUTET</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Nuttens</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:364191.20130311</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 30 novembre et 10 décembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la chambre de commerce et d'industrie du Var, dont le siège est 236 boulevard du Maréchal Leclerc à Toulon (83097) ; la chambre de commerce et d'industrie demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 11MA01578 du 6 novembre 2012 par laquelle le juge des référés de la cour administrative d'appel de Marseille a rejeté sa requête tendant, d'une part, à l'annulation de l'ordonnance du 6 avril 2011 du juge des référés du tribunal administratif de Toulon rejetant sa demande tendant à ce que soit prescrit une mesure d'expertise en vue de déterminer les dépenses exposées et les sommes perçues par la société Securitas pour l'exécution du contrat d'inspection et de filtrage des passagers et de leurs bagages dans l'aéroport de <br/>
Toulon-Hyères " déclaré nul " au titre des années 2001 et 2002 par un arrêt de la cour administrative d'appel de Marseille du 8 février 2010, ainsi que l'exigibilité des sommes perçues par la société dans le cadre de l'exécution de ce contrat pour la période du 2 août au 31 décembre 2000, et, d'autre part, à ce que soit ordonnée la mesure d'expertise sollicitée ; <br/>
<br/>
              2°) de mettre à la charge de la société Securitas le versement d'une somme de 3 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Nuttens, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de la SCP Rocheteau, Uzan-Sarano, avocat de la chambre de commerce et d'industrie du Var et de la SCP Boutet, avocat de la société Securitas,<br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Rocheteau, Uzan-Sarano, avocat de la chambre de commerce et d'industrie du Var et à la SCP Boutet, avocat de la société Securitas ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article R. 532-1 du code de justice administrative : " Le juge des référés peut, sur simple requête et même en l'absence de décision administrative préalable, prescrire toute mesure utile d'expertise ou d'instruction. (...) " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que la chambre de commerce et d'industrie du Var a conclu, en août 2000, avec la société Securitas, un marché d'inspection et de filtrage des passagers et de leurs bagages à main dans l'aéroport de Toulon-Hyères ; que ce marché a été conclu pour une période initiale allant du 2 août au 31 décembre 2000 et prévoyait sa reconduction tacite annuelle pour les années 2001 et 2002 ainsi que pour la période du 1er janvier au 2 août 2003 ; qu'à la suite de la décision de la chambre de commerce et d'industrie du Var de ne pas reconduire le marché au-delà du 31 décembre 2002, la société Securitas a demandé à être indemnisée de surcoûts qu'elle aurait supportés dans l'exécution du marché ; qu'après rejet de cette demande par le tribunal administratif de Toulon, la cour administrative d'appel de Marseille, par un arrêt du 8 février 2010, a " déclaré nul " le contrat liant les parties au titre des années 2001 et 2002 au motif qu'il avait fait l'objet de reconductions tacites ; que la chambre de commerce et d'industrie du Var se pourvoit en cassation contre l'ordonnance du juge des référés de la cour administrative d'appel de Marseille du 6 novembre 2012 confirmant le rejet de sa demande d'expertise visant à évaluer, à la suite de la " déclaration de nullité " du contrat conclu avec la société Securitas, les dépenses utiles engagées par cette société pendant la période au cours de laquelle le contrat a été jugé " nul " et à déterminer les sommes qu'aurait perçues la société Securitas en application du contrat si elle avait facturé ses prestations sur la base des heures réelles d'ouverture du poste d'inspection et de filtrage ; <br/>
<br/>
              3. Considérant que, pour rejeter la demande d'expertise présentée par la chambre de commerce et d'industrie du Var, le juge des référés de la cour administrative d'appel de Marseille a jugé que l'arrêt du 8 février 2010 n'avait pas eu, par lui-même, pour effet de " déclarer nul " ni " d'annuler " le contrat conclu entre la chambre de commerce et d'industrie et la société Securitas et que la chambre ne pouvait en conséquence se prévaloir de sa " nullité " pour solliciter une expertise, en l'absence d'une décision juridictionnelle privant le contrat de ses effets ; que le juge des référés de la cour administrative d'appel a ainsi méconnu la portée de l'arrêt du 8 février 2010, lequel, ainsi qu'il a été dit, a " déclaré nul " le contrat litigieux pour les années 2001 et 2002, c'est à dire a décidé qu'il devait l'écarter pour régler le litige opposant la société Securitas à la chambre de commerce et d'industrie ; qu'ainsi, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la chambre de commerce et d'industrie du Var est fondée à demander l'annulation, pour ce motif, de l'ordonnance du 6 novembre 2012 ; <br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au titre de la procédure de référé engagée, en application des dispositions de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              5. Considérant que la mesure d'expertise sollicitée par la chambre de commerce et d'industrie du Var, dont l'objet a été rappelé au point 2, tend à rassembler des informations dont la requérante dispose déjà ou est en mesure d'obtenir communication par d'autres voies ; qu'elle ne présente pas, en conséquence, le caractère d'une mesure utile au sens des dispositions précitées de l'article R. 532-1 du code de justice administrative ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que la chambre de commerce et d'industrie du Var n'est pas fondée à soutenir que c'est à tort que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Toulon a rejeté sa demande ;<br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que la somme demandée par la chambre de commerce et d'industrie du Var au titre des frais exposés par elle et non compris dans les dépens soit mise à la charge de la société Securitas, qui n'est pas la partie perdante dans la présente instance ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de la chambre de commerce et d'industrie du Var le versement à la société Securitas d'une somme de 4 500 euros au titre des frais de même nature exposés par elle pour l'ensemble de la procédure ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du juge des référés de la cour administrative de Marseille du 6 novembre 2012 est annulée. <br/>
Article 2 : La requête de la chambre de commerce et d'industrie du Var devant le juge des référés de la cour administrative d'appel de Marseille et ses conclusions présentées devant le Conseil d'Etat au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.  <br/>
Article 3 : La chambre de commerce et d'industrie du Var versera une somme de 4 500 euros à la société Securitas au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : La présente décision sera notifiée à la chambre de commerce et d'industrie du Var et à la société Securitas.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
