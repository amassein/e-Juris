<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044310580</ID>
<ANCIEN_ID>JG_L_2021_11_000000457883</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/31/05/CETATEXT000044310580.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 04/11/2021, 457883, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-11-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>457883</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:457883.20211104</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
               Le Syndicat des hôteliers, restaurateurs, cafetiers et discothèques de Guyane (SHRCDG) et l'Union syndicale des opérateurs touristiques de Guyane (USOTG) ont demandé au juge des référés du tribunal administratif de la Guyane, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
               - à titre principal, en premier lieu, de suspendre l'exécution de l'arrêté du 22 octobre 2021 du préfet de la Guyane portant mesures de prévention et restrictions nécessaires pour lutter contre la propagation du covid-19 dans le département de la Guyane et, en particulier, de suspendre l'exécution des mesures restrictives portant sur la fermeture des établissements de type N visés par l'article 13 de l'arrêté ainsi que les restrictions de déplacement de l'article 3, en deuxième lieu, d'enjoindre au préfet de la Guyane de retirer l'arrêté du 22 octobre 2021 et d'autoriser la réouverture des établissements de type N ainsi que la circulation et les déplacements de leurs consommateurs tant en jours de semaine que de fin de semaine (samedi et dimanche) ;<br/>
               - à titre subsidiaire, d'ordonner la réouverture des établissements de type N avec la mise en place du passe sanitaire et de restreindre les déplacements à partir de 23 h 30 tous les jours de la semaine y compris les fins de semaine ;<br/>
               - à titre infiniment subsidiaire, d'une part, d'ordonner la réouverture des terrasses des établissements de type N, de restreindre les déplacements entre 23 h 30 et 5 heures du lundi au samedi et d'ordonner la réouverture des établissements de type N le dimanche de 12 heures à 16 heures et, d'autre part, de rendre l'ordonnance exécutoire dès qu'elle aura été rendue.<br/>
<br/>
               Par une ordonnance n° 2101401 du 25 octobre 2021, le juge des référés du tribunal administratif de la Guyane a, en premier lieu, suspendu l'exécution de la mesure édictée par le I  de l'article 13 de l'arrêté du préfet de la Guyane du 22 octobre 2021, en deuxième lieu, suspendu les dispositions comprises dans l'article 3 du même arrêté en tant que celles-ci s'opposent à un fonctionnement économiquement viable des établissements visés à l'article 1er en zone orange le soir et le dimanche, en troisième lieu, ordonné que les règles de fonctionnement définies pour les établissements situés en zone verte par le II de l'article 13 de l'arrêté du 22 octobre 2021 s'appliquent pour les établissements situés en zone orange concernés par l'ordonnance, en quatrième lieu, ordonné que la mesure de suspension prenne effet trente-six heures après notification de l'ordonnance. <br/>
<br/>
               Par une requête, enregistrée le 26 octobre 2021 au secrétariat du contentieux du Conseil d'Etat, le ministre des solidarités et de la santé demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
               1°) d'annuler cette ordonnance ;<br/>
<br/>
               2°) de rejeter la requête du Syndicat des hôteliers, restaurateurs, cafetiers et discothèques de Guyane et de l'Union syndicale des opérateurs touristiques de Guyane.<br/>
<br/>
<br/>
               Il soutient que :<br/>
               - il n'est pas porté d'atteinte grave et manifestement illégale à une liberté fondamentale ;<br/>
               - la situation sanitaire dans la zone orange est marquée par une circulation du virus encore active et un contexte épidémiologique incertain, de nature à justifier les restrictions de déplacement et d'accueil du public suspendues à tort par le juge des référés du tribunal administratif de la Guyane ;<br/>
               - la mesure litigieuse ne présente pas un caractère général et absolu.<br/>
<br/>
               Par un mémoire en défense, enregistré le 28 octobre 2021, le Syndicat des hôteliers, restaurateurs, cafetiers et discothèques de Guyane et l'Union syndicale des opérateurs touristiques de Guyane concluent qu'il n'y ait lieu à statuer et, à titre subsidiaire, au rejet de la requête et, en tout état de cause, à ce que la somme de 3 000 euros soit mise à la charge de l'Etat au titre de l'article L. 761-1 du code de justice administrative. Ils soutiennent que la requête est irrecevable et que les moyens soulevés ne sont pas fondés.<br/>
<br/>
               Vu le nouveau mémoire, enregistré le 29 octobre 2021, présenté par le ministre des solidarités et de la santé. Il conclut à ce qu'il n'y ait lieu à statuer. Il soutient que le préfet de la Guyane a décidé de prendre un nouvel arrêté, rendant sans objet son appel.<br/>
<br/>
               Vu les deux nouveaux mémoires en défense, enregistrés le 2 novembre 2021 avant la clôture de l'instruction, présentés par le syndicat des hôteliers, restaurateurs, cafetiers et discothèques de Guyane et l'Union syndicale des opérateurs touristiques de Guyane. Ils concluent, dans le dernier état de leurs écritures, à titre principal, à ce qu'il n'y ait lieu à statuer, à titre subsidiaire, au rejet de la requête et, en tout état de cause, à ce que la somme de 5 000 euros soit mise à la charge de l'Etat au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
               Vu les autres pièces du dossier ;<br/>
<br/>
               Vu le code de justice administrative ;<br/>
<br/>
<br/>
               Après avoir convoqué à une audience publique, d'une part, le ministre des solidarités et de la santé, le ministère des outre-mer et d'autre part, le Syndicat des hôteliers, restaurateurs, cafetiers et discothèques de Guyane et l'Union syndicale des opérateurs touristiques de Guyane ;<br/>
<br/>
               Ont été entendus lors de l'audience publique du 28 octobre 2021 à 16 heures : <br/>
<br/>
               - Me Pinatel, avocat au Conseil d'Etat et à la Cour de cassation, avocat du Syndicat des hôteliers, restaurateurs, cafetiers et discothèques de Guyane et de l'Union syndicale des opérateurs touristiques de Guyane ;<br/>
<br/>
               - la représentante du Syndicat des hôteliers, restaurateurs, cafetiers et discothèques de Guyane et de l'Union syndicale des opérateurs touristiques de Guyane ;<br/>
<br/>
               - les représentants du ministre des solidarités et de la santé ;<br/>
<br/>
               - le représentant du ministre des outre-mer ;<br/>
<br/>
               à l'issue de laquelle le juge des référés a différé la clôture de l'instruction au 29 octobre 2021 à 18 heures, puis au 2 novembre 2021 à 18 heures.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
               1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale (...) ". <br/>
<br/>
               2. Par une ordonnance du 25 octobre 2021, le juge des référés du tribunal administratif de la Guyane, saisi sur le fondement de l'article L. 521-2 du code de justice administrative par le Syndicat des hôteliers, restaurateurs, cafetiers et discothèques de Guyane et l'Union syndicale des opérateurs touristiques de Guyane, a suspendu l'exécution de l'arrêté du 22 octobre 2021 du préfet de la Guyane portant mesures de prévention et restrictions nécessaires pour lutter contre la propagation de la covid-19 dans le département de la Guyane en tant que, dans les communes classées en zone dite " orange ", le I de son article 13 impose la fermeture au public des restaurants, débits de boissons à consommer sur place et commerces ambulants et l'article 3 ne permet pas de déroger aux restrictions de déplacements qu'il pose pour y accéder et a ordonné, à effet de trente-six heures après la notification de son ordonnance, que les règles de fonctionnement définies pour les établissements situés en zone dite " verte " par le II de l'article 13 de l'arrêté du 22 octobre 2021 s'appliquent pour les établissements situés en zone orange concernés. Le ministre des solidarités et de la santé relève appel de cette ordonnance.<br/>
<br/>
               3. Toutefois, il résulte de l'instruction que, postérieurement à l'introduction de l'appel du ministre des solidarités et de la santé, le préfet de la Guyane a, par un arrêté du 29 octobre 2021, abrogé l'arrêté du 27 octobre 2021 qu'il avait pris pour l'exécution de l'ordonnance du juge des référés du tribunal administratif et adopté de nouvelles mesures permettant, sous certaines conditions, à compter du 30 octobre 2021, l'accueil du public dans les restaurants et les débits de boissons à consommer sur place et les déplacements à destination ou en provenance de ces établissements, y compris en zone orange.<br/>
<br/>
               4. Il en résulte que les effets de l'arrêté du 22 octobre 2021 et de l'ordonnance du juge des référés du tribunal administratif de la Guyane ont pris fin et que l'appel du ministre des solidarités et de la santé a perdu son objet. Il n'y a dès lors plus lieu d'y statuer.<br/>
<br/>
               5. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions du Syndicat des hôteliers, restaurateurs, cafetiers et discothèques de Guyane et de l'Union syndicale des opérateurs touristiques de Guyane présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de statuer sur la requête du ministre des solidarités et de la santé.<br/>
Article 2 : Les conclusions présentées par le Syndicat des hôteliers, restaurateurs, cafetiers et discothèques de Guyane et l'Union syndicale des opérateurs touristiques de Guyane au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente ordonnance sera notifiée au ministre des solidarités et de la santé et au Syndicat des hôteliers, restaurateurs, cafetiers et discothèque de Guyane, premier dénommé, pour les deux défendeurs.<br/>
Copie en sera adressée au ministre des outre-mer.<br/>
<br/>
Fait à Paris, le 4 novembre 2021<br/>
    Signé : Gaëlle Dumortier<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
