<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000008238582</ID>
<ANCIEN_ID>JG_L_2006_07_000000280167</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/08/23/85/CETATEXT000008238582.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère SSJS, 10/07/2006, 280167, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2006-07-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>280167</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère SSJS</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>ODENT</AVOCATS>
<RAPPORTEUR>M. Gilles de la Ménardière</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Devys</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2006:280167.20060710</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance en date du 29 avril 2005, enregistrée le 3 mai 2005 au secrétariat du contentieux du Conseil d'Etat, par laquelle le président de la cour administrative d'appel de Bordeaux a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête présentée à cette cour pour la CAISSE DES DEPOTS ET CONSIGNATIONS en sa qualité de gestionnaire de la Caisse nationale de retraite des agents des collectivités locales, dont le siège est Centre de gestion des pensions, rue du Vergne à Bordeaux cedex (33059) ;<br/>
<br/>
              Vu la requête, enregistrée le 3 juin 2004 au greffe de la cour administrative d'appel de Bordeaux, présentée par la CAISSE DES DEPOTS ET CONSIGNATIONS et tendant à l'annulation du jugement du 16 mars 2004 par lequel le tribunal administratif de Pau a annulé, à la demande de M. A...C..., les décisions des 9 octobre et 31 décembre 2003 du directeur général de la CAISSE ET DEPOTS ET CONSIGNATIONS en tant qu'elles ont refusé à l'intéressé le bénéfice de la bonification d'ancienneté prévue à l'article 11-I-3° du décret du 9 septembre 1965 ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 2003-775 du 21 août 2003 ;<br/>
<br/>
              Vu le décret n° 65-773 du 9 septembre 1965 ;<br/>
<br/>
              Vu le décret n° 2003-1306 du 26 décembre 2003 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. B...de la Ménardière, Conseiller d'Etat,  <br/>
<br/>
              - les observations de Me Odent, avocat de la CAISSE DES DEPOTS ET CONSIGNATIONS, <br/>
<br/>
              - les conclusions de M. Christophe Devys, Commissaire du gouvernement ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'aux termes de l'article L. 12 du code des pensions civiles et militaires de retraite, dans sa rédaction issue de la loi du 21 août 2003 portant réforme des retraites : " Aux services effectifs s'ajoutent, dans les conditions déterminées par un décret en Conseil d'Etat, les bonifications ci-après : /(...) b) Pour chacun de leurs enfants légitimes et de leurs enfants naturels nés antérieurement au 1er janvier 2004, pour chacun de leurs enfants dont l'adoption est antérieure au 1er janvier 2004 et, sous réserve qu'ils aient été élevés pendant neuf ans au moins avant leur vingt et unième anniversaire, pour chacun des autres enfants énumérés au II de l'article L. 18 dont la prise en charge a débuté antérieurement au 1er janvier 2004, les fonctionnaires civils et militaires bénéficient d'une bonification fixée à un an, qui s'ajoute aux services effectifs, à condition qu'ils aient interrompu leur activité dans des conditions fixées par décret en Conseil d'Etat " ; qu'aux termes du II de l'article 48 de la loi du 21 août 2003, applicable aux fonctionnaires affiliés à la Caisse nationale de retraite des agents des collectivités locales en vertu de l'article 40 de cette même loi, les dispositions mentionnées ci-dessus " s'appliquent aux pensions liquidées à compter du 28 mai 2003 " ;<br/>
<br/>
              Considérant qu'il résulte des termes mêmes du II, précité, de l'article 48 de la loi du 21 août 2003 que les dispositions du b) de l'article L. 12 du code des pensions civiles et militaires de retraite, dans leur rédaction antérieure à cette date, ne sont pas applicables aux pensions liquidées à compter du 28 mai 2003 ; qu'il ressort des pièces du dossier soumis aux juges du fond que la pension de M.  C...a été liquidée le 31 décembre 2003, donc postérieurement au 28 mai 2003 ; qu'ainsi, en estimant que M.  C...pouvait prétendre au bénéfice des bonifications pour enfant, sans rechercher s'il remplissait les conditions prévues par la loi du 21 août 2003, le tribunal administratif de Pau a commis une erreur de droit ; que, par suite, la CAISSE DES DEPOTS ET CONSIGNATIONS est fondée à demander l'annulation du jugement attaqué ;<br/>
<br/>
              Considérant qu'il y a lieu, en application des dispositions de l'article L. 821-1 du code de justice administrative, de régler l'affaire au fond ;<br/>
<br/>
              Considérant que M.  C...n'établit pas, ni même n'allègue, qu'il a interrompu son activité pour chacun des enfants pour lesquels il a demandé le bénéfice de la bonification d'ancienneté prévue par le b) de l'article L. 12 du code des pensions civiles et militaires de retraite ; que, par suite, M.  C...n'est pas fondé à demander l'annulation des décisions attaquées lui refusant le bénéfice de cet avantage ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 16 mars 2004 du tribunal administratif de Pau est annulé.<br/>
Article 2 : La demande présentée par M.  C...devant le tribunal administratif de Pau est rejetée.<br/>
Article 3 : La présente décision sera notifiée à la CAISSE DES DEPOTS ET CONSIGNATIONS, à M. A...C...et au ministre de l'économie, des finances et de l'industrie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
