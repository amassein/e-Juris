<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042434253</ID>
<ANCIEN_ID>JG_L_2020_10_000000434752</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/43/42/CETATEXT000042434253.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 16/10/2020, 434752, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-10-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>434752</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Coralie Albumazard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier  Fuchs</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:434752.20201016</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés le 20 septembre 2019 et le 18 juin 2020 au secrétariat du contentieux du Conseil d'État, l'association France nature environnement demande au Conseil d'État :<br/>
<br/>
              1°) à titre principal, d'annuler pour excès de pouvoir, d'une part, la décision implicite de rejet de sa demande de retrait du décret n° 2019-292 du 9 avril 2019 modifiant la nomenclature des installations classées pour la protection de l'environnement et, d'autre part, ce décret en tant qu'il modifie les rubriques 2521, 2564 et 2565 de la nomenclature des installations classées pour la protection de l'environnement et en tant qu'il ne modifie pas les autres rubriques de la nomenclature en ce qu'elle permet à des installations entrant dans le champ d'application des annexes I et II de la directive 2011/92/UE d'être exonérées d'évaluation environnementale sur le fondement d'éléments étrangers, en tout ou partie, aux critères de l'annexe III de la même directive ;<br/>
<br/>
              2°) à titre subsidiaire, de surseoir à statuer et de renvoyer à la Cour de justice de l'Union européenne une question préjudicielle relative à la faculté pour un Etat, en application de la directive 2011/92/UE modifiée par la directive 2014/52/UE, de prévoir qu'un projet susceptible d'avoir des incidences notables sur l'environnement fasse l'objet, d'une part, d'une décision de soumission à évaluation environnementale au cas par cas formulée par l'autorité qui est également chargée de statuer sur la demande d'autorisation du projet et, d'autre part, d'un examen au cas par cas qui ne comprend pas l'examen de l'ensemble des critères prévus à l'annexe III de la directive ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la directive 2011/92/UE du Parlement européen et du Conseil du 13 décembre 2011 ;<br/>
              - la directive 2014/52/UE du Parlement européen et du Conseil du 16 avril 2014 ;<br/>
              - le code de l'environnement ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme A... B..., maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Olivier Fuchs, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. L'association France nature environnement demande l'annulation pour excès de pouvoir du décret du 9 avril 2019 modifiant la nomenclature des installations classées pour la protection de l'environnement, en ce qu'il prévoit l'application d'un régime d'enregistrement aux installations relevant des rubriques 2521, 2564 et 2565 qui étaient auparavant soumises à un régime d'autorisation, ainsi que de la décision implicite rejetant son recours gracieux formé contre ce décret.<br/>
<br/>
              Sur le cadre juridique :<br/>
<br/>
              2. En application du premier alinéa de l'article L. 511-2 du code de l'environnement, la soumission des installations classées pour la protection de l'environnement à l'un des régimes d'autorisation, d'enregistrement ou de déclaration résulte de leur inscription, suivant la gravité des dangers et des inconvénients que peut présenter leur exploitation pour les intérêts mentionnés à l'article L. 511-1, dans les rubriques correspondantes d'une nomenclature. La répartition entre ces différents régimes est opérée, en référence à la nomenclature, en fonction de seuils et de critères, prenant en compte notamment les caractéristiques de ces installations et leur impact potentiel sur l'environnement. Ainsi, en vertu du premier alinéa de l'article L. 512-1 du même code, " sont soumises à autorisation les installations qui présentent de graves dangers ou inconvénients pour les intérêts mentionnés à l'article L. 511-1 ", tandis que l'article L. 512-7 du même code permet de soumettre " à autorisation simplifiée, sous la dénomination d'enregistrement, les installations qui présentent des dangers ou inconvénients graves pour les intérêts mentionnés à l'article L. 511-1, lorsque ces dangers et inconvénients peuvent, en principe, eu égard aux caractéristiques des installations et de leur impact potentiel, être prévenus par le respect de prescriptions générales édictées par le ministre chargé des installations classées ". Le deuxième alinéa de l'article L. 512-7 précise que " les activités pouvant, à ce titre, relever du régime d'enregistrement concernent les secteurs ou technologies dont les enjeux environnementaux et les risques sont bien connus, lorsque les installations ne sont soumises ni à la directive 2010/75/UE du Parlement européen et du Conseil du 24 novembre 2010 relative aux émissions industrielles au titre de son annexe I, ni à une obligation d'évaluation environnementale systématique au titre de l'annexe I de la directive 85/337/CEE du 27 juin 1985 concernant l'évaluation des incidences de certains projets publics et privés sur l'environnement ". L'article L. 512-7-2 du code de l'environnement prévoit cependant que le préfet peut décider que la demande d'enregistrement sera instruite selon les règles de procédure prévues par le chapitre unique du titre VIII du livre Ier pour les autorisations environnementales, c'est-à-dire selon le régime de l'autorisation, au vu de trois séries de considérations tenant à la sensibilité environnementale du milieu, au cumul des incidences du projet avec celles d'autres projets d'installations, ouvrages ou travaux et à la nécessité, à la demande de l'exploitant, d'aménager les prescriptions générales applicables à l'installation.<br/>
<br/>
              Sur la méconnaissance de la directive 2011/92/UE du Parlement européen et du Conseil du 13 décembre 2011 :<br/>
<br/>
              3. D'une part, en vertu du paragraphe 1 de l'article 4 de la directive du 13 décembre 2011 du Parlement européen et du Conseil concernant l'évaluation des incidences de certains projets publics et privés sur l'environnement, les projets énumérés à l'annexe I de la directive sont soumis à une évaluation systématique, sous réserve des exemptions exceptionnelles prévues au paragraphe 4 de l'article 2. Sous la même réserve, le paragraphe 2 de l'article 4 de la directive dispose que " pour les projets énumérés à l'annexe II, les États membres déterminent si le projet doit être soumis à une évaluation conformément aux articles 5 à 10. Les États membres procèdent à cette détermination : / a) sur la base d'un examen cas par cas ; / ou / b) sur la base des seuils ou critères fixés par l'État membre. / Les États membres peuvent décider d'appliquer les deux procédures visées aux points a) et b). " En vertu du paragraphe 3 du même article : " Pour l'examen au cas par cas ou la fixation des seuils ou critères en application du paragraphe 2, il est tenu compte des critères de sélection pertinents fixés à l'annexe III. Les États membres peuvent fixer des seuils ou des critères pour déterminer quand les projets n'ont pas à être soumis à la détermination prévue aux paragraphes 4 et 5 ou à une évaluation des incidences sur l'environnement, et/ou des seuils ou des critères pour déterminer quand les projets font l'objet, en tout état de cause, d'une évaluation des incidences sur l'environnement sans être soumis à la détermination prévue aux paragraphes 4 et 5. " L'annexe III de la directive fixe trois séries de critères visant à déterminer si les projets figurant à l'annexe II devraient faire l'objet d'une évaluation des incidences sur l'environnement, relatifs à la caractéristique des projets, à leur localisation et aux types et caractéristiques de l'impact potentiel.<br/>
<br/>
              4. Par ailleurs, en vertu du paragraphe 1 de l'article 6 de la même directive, " Les États membres prennent les mesures nécessaires pour que les autorités susceptibles d'être concernées par le projet, en raison de leurs responsabilités spécifiques en matière d'environnement ou de leurs compétences locales et régionales, aient la possibilité de donner leur avis sur les informations fournies par le maître d'ouvrage et sur la demande d'autorisation, en tenant compte, le cas échéant, des cas visés à l'article 8 bis, paragraphe 3. A cet effet, les États membres désignent les autorités à consulter, d'une manière générale ou au cas par cas. Celles-ci reçoivent les informations recueillies en vertu de l'article 5. Les modalités de cette consultation sont fixées par les États membres. " En outre, aux termes de l'article 9 bis de la même directive : " Les États membres veillent à ce que l'autorité ou les autorités compétentes accomplissent les missions résultant de la présente directive de façon objective et ne se trouvent pas dans une position donnant lieu à un conflit d'intérêts. / Lorsque l'autorité compétente est aussi le maître d'ouvrage, les États membres appliquent au minimum, dans leur organisation des compétences administratives, une séparation appropriée entre les fonctions en conflit lors de l'accomplissement des missions résultant de la présente directive ". <br/>
<br/>
              5. D'autre part, en vertu du II de l'article L. 122-1 du code de l'environnement, pris pour la transposition de la directive du 13 décembre 2011, " les projets qui, par leur nature, leur dimension ou leur localisation, sont susceptibles d'avoir des incidences notables sur l'environnement ou la santé humaine font l'objet d'une évaluation environnementale en fonction de critères et de seuils définis par voie réglementaire et, pour certains d'entre eux, après un examen au cas par cas effectué par l'autorité environnementale. / Pour la fixation de ces critères et seuils et pour la détermination des projets relevant d'un examen au cas par cas, il est tenu compte des données mentionnées à l'annexe III de la directive 2011/92/UE modifiée du Parlement européen et du Conseil du 13 décembre 2011 concernant l'évaluation des incidences de certains projets publics et privés sur l'environnement ". En vertu de I de l'article R. 122-2 du même code, " les projets relevant d'une ou plusieurs rubriques énumérées dans le tableau annexé au présent article font l'objet d'une évaluation environnementale, de façon systématique ou après un examen au cas par cas, en application du II de l'article L. 122-1, en fonction des critères et des seuils précisés dans ce tableau ". Le tableau annexé à cet article prévoit, à sa ligne 1, que les projets d'installations classées pour la protection de l'environnement soumises à enregistrement relèvent de l'examen au cas par cas, en précisant que pour ces installations, " l'examen au cas par cas est réalisé dans les conditions et formes prévues à l'article L. 512-7-2 du code de l'environnement ".<br/>
<br/>
              6. Ainsi qu'il a été dit au point 2, l'article L. 512-7-2 du code de l'environnement, dans sa rédaction applicable à la date du décret attaqué, prévoit que le préfet peut décider que la demande d'enregistrement sera instruite selon le régime de l'autorisation au vu de trois séries de considérations, et notamment " 1° Si, au regard de la localisation du projet, en prenant en compte les critères mentionnés au point 2 de l'annexe III de la directive 2011/92/UE du 13 décembre 2011 concernant l'évaluation des incidences de certains projets publics et privés sur l'environnement, la sensibilité environnementale du milieu le justifie ".<br/>
<br/>
              7. L'association requérante soutient que le décret attaqué méconnaît les objectifs de la directive 2011/92/UE en ce qu'il soumet des activités au régime de l'enregistrement prévu aux articles L. 512-7 et suivants du code de l'environnement, alors que l'examen au cas par cas de la nécessité d'une évaluation environnementale que comporte ce régime ne satisfait pas aux objectifs de cette directive, d'une part, parce que cet examen est effectué par le préfet par ailleurs compétent pour statuer sur la demande d'autorisation, d'autre part, parce ce qu'il ne prend pas en compte l'ensemble des critères définis dans l'annexe III de la directive, visant à déterminer si le projet d'exploitation doit faire l'objet d'une évaluation de ses incidences sur l'environnement.<br/>
<br/>
              8. En premier lieu, il résulte de la combinaison de l'article L. 512-7-2 et du tableau annexé à l'article R. 122-2 du code de l'environnement que le préfet, par ailleurs compétent pour statuer sur la demande d'enregistrement effectuée au titre de la législation des installations classées pour la protection de l'environnement, est chargé d'effectuer l'examen au cas par cas propre à ce type de projets, destiné à déterminer s'ils doivent faire l'objet d'une évaluation de leurs incidences sur l'environnement. Si les dispositions de l'article 6 de la directive du 13 décembre 2011 citées au point 4 ont pour finalité de garantir que l'avis sur l'évaluation environnementale des plans et programmes susceptibles d'avoir des incidences notables sur l'environnement soit rendu, avant leur approbation ou leur autorisation afin de permettre la prise en compte de ces incidences, par une autorité compétente et objective en matière d'environnement, il résulte clairement de ces mêmes dispositions que cette autorité est distincte de celle mentionnée à l'article 4, chargée de procéder à la détermination de la nécessité d'une évaluation environnementale par un examen au cas par cas. Par ailleurs, aucune disposition de la directive ne fait obstacle à ce que l'autorité chargée de procéder à cet examen au cas par cas soit celle compétente pour statuer sur l'autorisation administrative requise pour le projet, sous réserve qu'elle ne soit pas chargée de l'élaboration du projet ni n'assure la maîtrise d'ouvrage. <br/>
<br/>
              9. En second lieu, si, dans le cadre du régime de l'enregistrement, la nécessité d'une évaluation environnementale résulte d'un examen au cas par cas réalisé par le préfet dans les conditions et formes prévues à l'article L. 512-7-2 du code de l'environnement et si, par ailleurs, ce dernier article ne mentionne à son 1° que le critère de la localisation du projet, il résulte tant de l'article L. 122-1 du code de l'environnement, qui précise de façon générale que, pour la détermination des projets relevant d'un examen au cas par cas, il est tenu compte des données mentionnées à l'annexe III de la directive du 13 décembre 2011, que des dispositions citées au point 2, en vertu desquelles la répartition entre les différents régimes d'installations classées pour la protection de l'environnement est opérée, en référence à la nomenclature, en fonction de seuils et de critères qui sont au nombre de ceux qui sont mentionnés à l'annexe III de la directive, prenant en compte notamment les caractéristiques de ces installations et leur impact potentiel sur l'environnement, que le préfet, saisi d'une demande d'enregistrement d'une installation, doit se livrer à un examen du dossier afin d'apprécier, tant au regard de la localisation du projet que des autres critères mentionnés à l'annexe III de la directive, relatifs aux caractéristiques des projets et aux types et caractéristiques de l'impact potentiel, si le projet doit faire l'objet d'une évaluation environnementale, ce qui conduit alors, en application de l'article L. 512-7-2, à le soumettre au régime de l'autorisation environnementale. Au demeurant, l'article 34 de la loi du 8 novembre 2019 relative à l'énergie et au climat a modifié l'article L. 512-7-2 du code de l'environnement pour prévoir expressément que le préfet prend en compte l'ensemble des critères mentionnés à l'annexe III de la directive.<br/>
<br/>
              10. Il résulte de tout ce qui précède que le moyen tiré de la méconnaissance, par le régime de l'enregistrement, des objectifs de la directive du 13 décembre 2011 doit être écarté.<br/>
<br/>
              Sur la méconnaissance du principe de non-régression :<br/>
<br/>
              11. Aux termes du II de l'article L. 110-1 du code de l'environnement, les autorités s'inspirent, dans le cadre des lois qui en définissent la portée, du " principe de non-régression, selon lequel la protection de l'environnement, assurée par les dispositions législatives et réglementaires relatives à l'environnement, ne peut faire l'objet que d'une amélioration constante, compte tenu des connaissances scientifiques et techniques du moment ".<br/>
<br/>
              12. En premier lieu, le décret a pour effet de soumettre certaines des activités susceptibles d'affecter l'environnement au régime de l'enregistrement, les soumettant ainsi le cas échéant à l'obligation de réaliser une évaluation environnementale après un examen au cas par cas par le préfet. Alors même que certaines d'entre elles étaient auparavant au nombre des activités devant faire l'objet d'une évaluation environnementale de façon systématique, le décret ne méconnaît pas, par lui-même, le principe de non-régression de la protection de l'environnement énoncé au II de l'article L. 110-1 du code de l'environnement dès lors que, dans les deux cas, les activités susceptibles d'avoir des incidences notables sur l'environnement doivent faire l'objet, en application des dispositions de l'article L. 122-1 du code de l'environnement combinées avec celles de l'article L. 512-7-2 s'agissant de celles soumises au régime de l'enregistrement, d'une évaluation environnementale. Le moyen tiré de l'atteinte au principe de non régression doit, par suite, être écarté.<br/>
<br/>
              13. En second lieu, il n'est pas contesté qu'en 2017, sur 533 demandes d'enregistrement, quatre demandes seulement ont été instruites selon le régime d'autorisation en application des dispositions de l'article L. 512-7-2. Ces éléments statistiques, qui portent sur une période antérieure à l'entrée en vigueur du décret attaqué, ne permettent pas, en tout état de cause, d'établir que le décret attaqué a méconnu le principe de non régression.<br/>
<br/>
              14. Il résulte de tout ce qui précède, et sans qu'il y ait lieu de saisir la Cour de justice de l'Union européenne d'une question préjudicielle, que l'association France nature environnement n'est pas fondée à demander l'annulation des actes qu'elle attaque.<br/>
<br/>
              15. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de l'association France nature environnement est rejetée.<br/>
Article 2 : La présente décision sera notifiée à l'association France nature environnement, au Premier ministre et à la ministre de la transition écologique.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
