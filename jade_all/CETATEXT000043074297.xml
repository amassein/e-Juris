<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043074297</ID>
<ANCIEN_ID>JG_L_2021_01_000000448488</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/07/42/CETATEXT000043074297.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 26/01/2021, 448488, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-01-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>448488</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GOUZ-FITOUSSI ; LE PRADO ; SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:448488.20210126</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A... D... a demandé au juge des référés du tribunal administratif de la Guadeloupe, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'ordonner la suspension de l'exécution des deux décisions des 16 et 25 novembre 2020 du directeur général du centre hospitalier universitaire de la Guadeloupe supprimant les deux unités d'urgence fonctionnelles " urgences cardiologiques " et " post urgences cardiologiques " dont elle était auparavant responsable et la réaffectant au service de cardiologie de ce centre hospitalier. <br/>
<br/>
              Par une ordonnance n° 2001167 du 24 décembre 2020, le juge des référés du tribunal administratif de la Guadeloupe a, d'une part, ordonné la suspension de l'exécution des décisions des 16 et 25 novembre 2020 du directeur général du centre hospitalier universitaire de la Guadeloupe supprimant les unités fonctionnelles " urgences cardiologiques " et " post-urgences cardiologiques " et affectant Mme D... au service de cardiologie du pôle d'activités cliniques " médecine " et, d'autre part , enjoint au centre hospitalier universitaire de la Guadeloupe de mettre en oeuvre les moyens humains et matériels permettant à Mme D... d'exercer ses fonctions, conformément à son statut et aux responsabilités qui lui sont confiées et de mettre en place une organisation permettant à la requérante ne plus se trouver de quelque manière sous l'autorité de M. B... dans un délai de quinze jours. <br/>
<br/>
              Par une requête, un mémoire en réplique et un mémoire de production, enregistrés les 8, 21 et 22 janvier 2021 au secrétariat du contentieux du Conseil d'Etat, le centre hospitalier universitaire de la Guadeloupe demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative : <br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) de rejeter les demandes présentées par Mme D... devant le juge des référés du tribunal administratif de la Guadeloupe ; <br/>
<br/>
              3°) de mettre à la charge de Mme D... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Il soutient que :<br/>
              - le juge des référés du tribunal administratif de la Guadeloupe a insuffisamment motivé son ordonnance dès lors qu'il s'est abstenu de se prononcer sur la condition d'urgence propre au référé liberté ;<br/>
              - il a considéré à tort que les décisions dont il a ordonné la suspension portaient une atteinte grave et manifestement illégale au droit de Mme D... de ne pas être soumise à un harcèlement moral dès lors qu'aucun des faits, pris isolément ou ensemble, sur lesquels il a fondé sa décision, n'est de nature à caractériser une situation de harcèlement moral en ce que, en premier lieu, la plupart de ces faits sont matériellement inexacts, en deuxième lieu, le comportement délétère de Mme D... n'a pas été pris en compte et, en dernier lieu, les décisions contestées se bornent à traduire l'exercice, par le directeur général du centre hospitalier universitaire, de ses pouvoirs d'organisation des services placés sous son autorité, répondent à un besoin impérieux d'assurer la continuité et la qualité des soins, et ont été prises dans l'intérêt du service ; <br/>
              - il a, à tort, fait droit aux demandes de Mme D... dans la mesure où la condition d'urgence n'était pas satisfaite, Mme D... bénéficiant d'un arrêt de travail jusqu'au 23 janvier 2021 et l'intérêt public qui s'attache à l'exécution des décisions contestées faisant obstacle à la caractérisation d'une situation d'urgence.<br/>
<br/>
              Par un mémoire en intervention, enregistré le 10 janvier 2021, M. C... B... demande au juge des référés du Conseil d'Etat de faire droit aux conclusions de la requête présentée par le centre hospitalier universitaire de la Guadeloupe. Il soutient qu'il justifie d'un intérêt pour agir et s'associe aux moyens de la requête.<br/>
<br/>
              Par un mémoire en défense, enregistré le 19 janvier 2021, Mme D... conclut au rejet de la requête et à ce qu'il soit mis à la charge du centre hospitalier de la Guadeloupe la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. Elle soutient qu'aucun des moyens n'est fondé. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé public ;<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, le centre hospitalier universitaire de la Guadeloupe et M. B..., d'autre part, Mme D... ;<br/>
<br/>
              Ont été entendus lors de l'audience publique du 21 janvier 2021, à 16 heures : <br/>
<br/>
              - Me Gilbert, avocat au Conseil d'Etat et à la Cour de cassation, avocat du centre hospitalier universitaire de la Guadeloupe ;<br/>
<br/>
              - Me Coudray, avocat au Conseil d'Etat et à la Cour de cassation, avocat de Mme D... ; <br/>
<br/>
              - Me E..., avocate au Conseil d'Etat et à la Cour de cassation, avocate de M. B... ;<br/>
<br/>
<br/>
              à l'issue de laquelle le juge des référés a reporté la clôture de l'instruction au 22 janvier 2021 à 18 heures ; <br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 511-1 du code de justice administrative : " Le juge des référés statue par des mesures qui présentent un caractère provisoire. Il n'est pas saisi du principal et se prononce dans les meilleurs délais. " Aux termes de l'article L. 521-2 du même code de justice : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures. ".<br/>
<br/>
              2. Mme D..., praticien hospitalier cardiologue, a été affectée auprès du centre hospitalier universitaire de la Guadeloupe en 2004, d'abord au sein du service de cardiologie puis, à compter de janvier 2011, au sein du pôle " urgences - soins critiques ", où elle était, jusqu'à l'intervention des décisions des 16 et 25 novembre 2020 en litige, responsable des deux unités fonctionnelles " urgences cardiologiques " et " post urgences cardiologiques ". Le centre hospitalier universitaire de la Guadeloupe relève appel de l'ordonnance du 24décembre 2020 par laquelle le juge des référés du tribunal administratif de la Guadeloupe, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, après avoir estimé que le droit de Mme D... de ne pas subir de harcèlement moral avait été méconnu, d'une part, a ordonné la suspension de l'exécution des décisions des 16 et 25 novembre 2020 ayant supprimé les deux unités fonctionnelles dont elle était auparavant responsable et l'ayant réaffectée au service de cardiologie, d'autre part, a enjoint à ce même centre hospitalier de mettre en place une organisation permettant à Mme D... de ne plus se trouver, de quelque manière, sous l'autorité de M. B..., responsable du service de cardiologie.<br/>
<br/>
              3. L'ordonnance attaquée impute à M. B... certains des faits que le juge des référés du tribunal administratif de la Guadeloupe a regardés comme constitutifs de harcèlement moral et enjoint au centre hospitalier de mettre en place une organisation soustrayant Mme D... à son autorité, au motif que cette dernière serait, à défaut, exposée à un risque de harcèlement moral. Compte tenu de ces motifs de l'ordonnance attaquée et de son dispositif, M. B... justifie d'un intérêt suffisant à son annulation et son intervention doit être admise.<br/>
<br/>
              4. Aux termes du premier alinéa de l'article 6 quinquiès de la loi du 13 juillet 1983 portant droits et obligations des fonctionnaires : " Aucun fonctionnaire ne doit subir les agissements répétés de harcèlement moral qui ont pour objet ou pour effet une dégradation des conditions de travail susceptible de porter atteinte à ses droits et à sa dignité, d'altérer sa santé physique ou mentale ou de compromettre son avenir professionnel ". Le droit de ne pas être soumis à un harcèlement moral constitue pour un agent une liberté fondamentale au sens des dispositions de l'article L. 521-2 du code de la justice administrative. <br/>
<br/>
              5. Il appartient à l'agent public qui soutient avoir été victime d'agissements constitutifs de harcèlement moral, de soumettre au juge des éléments de fait susceptibles de caractériser l'existence de tels agissement. Il incombe à l'administration de produire, en sens contraire, une argumentation de nature à démontrer que les agissements en cause sont justifiés par des considérations étrangères à tout harcèlement. La conviction du juge, à qui il revient d'apprécier si les agissements de harcèlement sont ou non établis, se détermine au regard de ces échanges contradictoires, qu'il peut compléter, en cas de doute, en ordonnant toute mesure d'instruction utile. Pour apprécier si des agissements dont il est allégué qu'ils sont constitutifs d'un harcèlement moral revêtent un tel caractère, le juge administratif doit tenir compte de l'ensemble des faits qui lui sont soumis.<br/>
<br/>
              Sur l'ordonnance attaquée en tant qu'elle ordonne la suspension des décisions des 16 et 25 novembre 2020 :<br/>
<br/>
              6. Aux termes du troisième alinéa de l'article L. 6146-1 du code de la santé publique : " Les unités fonctionnelles sont les structures élémentaires de prise en charge des malades par une équipe soignante ou médico-technique, identifiées par leurs fonctions et leur organisation ainsi que les structures médico-techniques qui leur sont associées. ". Aux termes de l'article L. 6146-6 du même code : " L'unité fonctionnelle est placée sous la responsabilité d'un praticien titulaire ou d'un praticien hospitalo-universitaire temporaire du service ou du département dans le cadre de l'organisation générale définie par le chef de service ou de département et dans le respect du projet de service. / A titre exceptionnel, un praticien hospitalier peut être chargé de plusieurs unités fonctionnelles. / Le conseil d'administration désigne pour une période déterminée par voie réglementaire le praticien hospitalier chargé de l'unité fonctionnelle avec l'accord du chef de service ou de département après avis des praticiens titulaires du service ou du département et de la commission médicale d'établissement. ". Aux termes, enfin, de l'article R. 6146-5 de ce code : " Il peut être mis fin, dans l'intérêt du service, aux fonctions de responsable de structure interne, service ou unité fonctionnelle par décision du directeur, après avis du président de la commission médicale d'établissement et du chef de pôle. ".<br/>
<br/>
              7. Il résulte de l'instruction que Mme D..., au titre des deux unités fonctionnelles dont elle est responsable réalise, d'une part, des consultations de patients présentant des signes cliniques relevant de l'urgence, d'autre part, des consultations dites " post urgences ", qui relèvent en réalité du suivi cardiologique des patients qui en bénéficient, les secondes étant plus nombreuses que les premières. Par ailleurs, il n'est pas contesté qu'elle ne prend en charge qu'une partie très minoritaire des patients en situation d'urgence pour des motifs cardiologiques, ce qui est logique puisqu'elle est la seule médecin affectée aux unités fonctionnelles dont elle est responsable. Il en résulte que ce sont les médecins relevant du service de cardiologie qui contribuent à la prise en charge de la plus grande part des patients des urgences présentant des signes d'urgence cardiologique.<br/>
<br/>
              8. Il ressort, par ailleurs, du procès-verbal de la réunion du 28 septembre 2020 du directoire de l'établissement que celui-ci a examiné deux options, l'une maintenant une unité fonctionnelle d'urgence cardiologique au sein du pôle " urgences - soins critiques ", l'autre supprimant les unités fonctionnelles dont Mme D... est responsable, le service de cardiologie étant, alors, chargé de " l'avis cardiologique " pour l'ensemble des patients des urgences et du suivi ultérieur de l'ensemble des patients ayant été en situation d'urgence cardiologique. Il est relevé dans ce document que le premier de ces deux modes d'organisation, compte tenu de la permanence des besoins, requiert qu'un nombre important de praticiens hospitaliers lui soit dévolu, que le centre hospitalier de la Guadeloupe, confronté à une insuffisance du nombre de médecins, est dans l'incapacité de fournir.<br/>
<br/>
              9. Il résulte de ce qui précède que la décision de supprimer les unités fonctionnelles dont Mme D... est responsable et la décision corrélative de la réintégrer au sein du service de cardiologie est de nature à améliorer l'organisation des soins au sein du centre hospitalier universitaire de la Guadeloupe, que ces deux décisions ont été prises, conformément aux dispositions citées ci-dessus de l'article R. 6146-5 du code de la santé publique et ne constituent pas, en conséquence, des faits de harcèlement moral, la compétence professionnelle de Mme D... n'ayant, au demeurant, nullement été mise en cause à leur occasion. Si, au surplus, cette dernière soutient qu'elle n'a pas été associée à cette réorganisation, il ressort des pièces du dossier, et notamment d'un courrier du directeur général en date du 1er août 2019 que plusieurs réunions portant sur l'organisation de la filière cardiologique au sein des urgences ont été organisées au cours de l'année 2019 et que Mme D..., qui y était conviée, n'y a pas assisté. Il suit de là que le centre hospitalier requérant est fondé à soutenir que c'est à tort que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de la Guadeloupe lui a enjoint de suspendre les décisions des 16 et 25 novembre 2020.<br/>
<br/>
              Sur l'ordonnance attaquée en tant qu'elle enjoint au centre hospitalier de mettre en place une organisation permettant à Mme D... ne plus se trouver de quelque manière sous l'autorité de M. B... :<br/>
<br/>
              10. A l'appui de la partie de son dispositif qui enjoint au centre hospitalier de mettre en place une organisation permettant à Mme D... ne plus se trouver de quelque manière sous l'autorité de M. B..., l'ordonnance attaquée relève, d'une part, un incident qui s'est produit le 30 décembre 2010, d'autre part, le fait que le second aurait, à la suite de la création des deux unités fonctionnelles évoquées ci-dessus, critiqué l'organisation ainsi mise en place.<br/>
<br/>
              11. En premier lieu, il ne résulte pas de l'instruction que l'incident du 30 décembre 2010 pour lequel seuls figurent au dossier, d'une part, les témoignages des deux protagonistes, très différents, d'autre part, des compte-rendus de psychologues basés uniquement sur le témoignage de Mme D..., puisse relever d'un quelconque harcèlement moral.<br/>
<br/>
              12. En second lieu, il était loisible à M. B..., responsable du service de cardiologie, de remettre en cause une organisation concernant cette discipline qu'il considérait comme non optimale et il ne ressort pas des différents courriels versés au dossier de première instance par Mme D..., à caractère purement techniques, qu'il aurait tenu, à son endroit, des propos insultants ou même discourtois.<br/>
<br/>
              13. Il résulte de ce qui précède que le centre hospitalier universitaire de la Guadeloupe est fondé à soutenir que c'est à tort que l'ordonnance attaquée lui a enjoint de mettre en place une organisation permettant à Mme D... ne plus se trouver de quelque manière sous l'autorité de M. B....<br/>
<br/>
              Sur les autres faits mis en avant par Mme D... :<br/>
<br/>
              14. Les autres faits ou ensembles de faits mis en avant par Mme D... à l'appui de sa demande devant le juge de première instance et relevés par ce dernier dans l'ordonnance attaquée concernent ses relations avec les directeurs généraux successifs du centre hospitalier. Pour être qualifiés de harcèlement moral, des faits répétés doivent excéder les limites de l'exercice normal du pouvoir hiérarchique.<br/>
<br/>
              15. En premier lieu, si Mme D... soutient qu'elle et son équipe auraient été " expulsées de manière vexatoire " par la clinique " Les Eaux claires " des locaux qu'elles occupaient au sein de cet établissement à la suite de l'incendie qui a ravagé, en 2017, les bâtiments de l'hôpital, ce fait n'est, en tout état de cause, pas imputable au centre hospitalier. Par ailleurs, il ne ressort pas de la convention d'occupation passée par ce dernier avec la clinique que l'installation temporaire des unités de Mme D... aurait été expressément prévue au sein des locaux de cette même clinique. Il était donc loisible au directeur général du centre hospitalier de demander à Mme D... de réintégrer d'autres locaux. Enfin, il ressort d'une attestation du 23 janvier 2021 versée au dossier par le centre hospitalier que ce dernier acquittera d'éventuelles quittances de loyer qui seraient adressées par la clinique à Mme D... qui n'aura, ainsi, pas à supporter à titre personnel les conséquences de cette occupation, quand bien même il lui aurait été demandé d'y mettre un terme.<br/>
<br/>
              16. En deuxième lieu, si Mme D... fait état de ce que le 3 avril 2020, alors qu'elle travaillait encore à la clinique " Les Eaux claires ", des archives se trouvant dans un local qu'elle occupait antérieurement à l'incendie de 2017, auraient été " brutalement déménagées ", il résulte de l'instruction, d'une part, que ce déménagement a été décidé dans le contexte de la crise sanitaire liée à l'épidémie de Covid-19, afin de créer une salle de repos destinée au personnel affecté à l'unité Covid-19 de l'hôpital et, donc, pour un motif d'intérêt général, d'autre part, que les archives en question sont toujours accessibles et à la disposition de Mme D..., comme en atteste le directeur général par une déclaration sur l'honneur versée au dossier le 23 janvier 2021, enfin, que la note interne du 3 avril 2020 du centre hospitalier se bornait à présenter, à propos de cet incident, la réponse de celui-ci à la suite de propos tenus publiquement par Mme D....<br/>
<br/>
              17. En troisième lieu, il résulte de l'instruction que, contrairement à ce que soutient Mme D..., plusieurs locaux lui ont été proposés à la suite de son déménagement de la clinique " Les Eaux claires ", de surcroît dans le difficile contexte créé à la fois par l'incendie de 2017 et par la crise sanitaire actuelle.<br/>
<br/>
              18. Enfin, si Mme D... fait état de la rupture d'accès au système d'information de l'hôpital qu'elle et son équipe ont subie le 20 novembre 2020, cette rupture, dont il n'est pas établi qu'elle ne serait pas due à des motifs techniques, n'a, pour regrettable qu'elle soit, en tout état de cause duré que quelques heures.<br/>
<br/>
              19. Il résulte de tout ce qui précède que l'ordonnance du 24 décembre 2020 du juge des référés du tribunal administratif de la Guadeloupe ne peut qu'être annulée et que la demande de première instance de Mme D... devant ce juge doit être rejetée. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions du centre hospitalier universitaire de la Guadeloupe tendant à ce qu'une somme soit mise à la charge de Mme D... au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : L'intervention de M. B... est admise.<br/>
Article 2 : L'ordonnance du 24 décembre 2020 du juge des référés du tribunal administratif de la Guadeloupe est annulée.<br/>
Article 3 : La demande présentée par Mme D... devant le juge des référés du tribunal administratif de la Guadeloupe est rejetée.<br/>
Article 4 : Les conclusions du centre hospitalier universitaire de la Guadeloupe au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente ordonnance sera notifiée au centre hospitalier universitaire de la Guadeloupe, à Mme A... D... et à M. C... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
