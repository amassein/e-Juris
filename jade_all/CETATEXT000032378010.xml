<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032378010</ID>
<ANCIEN_ID>JG_L_2016_04_000000385997</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/37/80/CETATEXT000032378010.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème / 4ème SSR, 07/04/2016, 385997</TITRE>
<DATE_DEC>2016-04-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>385997</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème / 4ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON ; SCP CELICE, BLANCPAIN, SOLTNER, TEXIDOR</AVOCATS>
<RAPPORTEUR>M. Olivier Rousselle</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:385997.20160407</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 26 novembre 2014, 26 février et 31 août 2015 au secrétariat du contentieux du Conseil d'Etat, M. B... A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 11 septembre 2014 par laquelle le Conseil national de l'ordre des pharmaciens a pris à son égard une mesure de suspension temporaire du droit d'exercer la profession de pharmacien pendant une durée de six mois et subordonné la reprise de son activité professionnelle à la constatation de son aptitude par une nouvelle expertise ; <br/>
<br/>
              2°) de mettre à la charge du Conseil national de l'Ordre des pharmaciens la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de la santé publique ; <br/>
<br/>
              - le décret n° 2014-545 du 26 mai 2014 ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Rousselle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boré, Salve de Bruneton, avocat de M. A...et à la SCP Célice, Blancpain, Soltner, Texidor, avocat du Conseil national de l'ordre des pharmaciens ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du I de l'article R. 4221-15 du code de la santé publique : " Dans le cas d'infirmité ou d'état pathologique rendant dangereux l'exercice de la profession, la suspension temporaire du droit d'exercer est prononcée par le conseil régional ou le conseil central compétent pour une période déterminée, qui peut, s'il y a lieu, être renouvelée. Toutefois, lorsque cette infirmité ou l'état pathologique n'est pas de nature à interdire à l'intéressé toute activité de pharmacien, les autorités ci-dessus désignées peuvent se borner à lui imposer l'obligation de se faire assister " ; que, sur le fondement de ces dispositions, le Conseil national de l'ordre des pharmaciens a, par une décision du 11 septembre 2014 dont l'intéressé demande l'annulation pour excès de pouvoir, pris à l'égard de M. A...une mesure de suspension temporaire du droit d'exercer la profession de pharmacien pendant une durée de six mois et subordonné la reprise de son activité professionnelle à la constatation de son aptitude par une nouvelle expertise ; <br/>
<br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 4221-15 du code de la santé publique, dans sa rédaction issue du décret du 26 mai 2014, entrée en vigueur le lendemain de sa publication au Journal officiel du 28 mai 2014 et applicable à la date de la décision attaquée : "  (...)  III. - La suspension ne peut être ordonnée que sur un rapport motivé, établi à la demande du conseil régional ou du conseil central compétent par trois médecins désignés comme experts, le premier par l'intéressé, le deuxième par le conseil régional ou le conseil central compétent et le troisième par les deux premiers experts (...) " ; qu'il est constant que la décision attaquée a été prise à l'issue d'une instruction menée conformément aux règles applicables avant l'entrée en vigueur du décret du 26 mai 2014, selon lesquelles les décisions étaient prises " sur un rapport motivé établi après examen par un expert choisi en accord entre l'intéressé ou sa famille et le conseil compétent " ; que  le rapport préalable à la décision attaquée n'a ainsi pas été établi conformément aux dispositions réglementaires applicables à la date de cette décision ; que si M. A..., qui avait à cette date cédé l'officine qu'il possédait à Grenoble et s'apprêtait à reprendre une activité professionnelle à Aix-les-Bains, présentait des difficultés psychiques et relationnelles de nature à le faire regarder comme dangereux en cas de reprise d'une activité de pharmacien, ces circonstances ne suffisaient pas à caractériser une situation d'urgence de nature à dispenser le Conseil national du respect de la procédure d'expertise prévue par les dispositions nouvellement applicables, alors notamment qu'il était loisible à ce Conseil de saisir le directeur général de l'agence régionale de santé pour que celui-ci suspende immédiatement le droit d'exercer du professionnel concerné sur le fondement de l'article L. 4221-18 du code de la santé publique  ; que, par suite, la décision du Conseil national de l'ordre est intervenue à l'issue d'une procédure irrégulière et doit être annulée, sans qu'il soit besoin d'examiner les autres moyens de la requête ;<br/>
<br/>
              3. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du Conseil national de l'ordre des pharmaciens le versement à M. A...d'une somme de 3 000 euros au titre des frais exposés par ce dernier et non compris dans les dépens ; que les dispositions de l'article L. 761-1 du code de justice administrative font, en revanche, obstacle à ce que soit mise à la charge de M.A..., qui n'est pas la partie perdante dans la présente instance, la somme que le Conseil national de l'ordre des pharmaciens demande au même titre ; <br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : La décision du 11 septembre 2014 du Conseil national de l'ordre des pharmaciens est annulée.<br/>
Article 2 : Le Conseil national de l'ordre des pharmaciens versera à M. A...une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Les conclusions du Conseil national de l'ordre des pharmaciens tendant à l'application de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à M. B...A...et au Conseil national de l'ordre des pharmaciens. <br/>
Copie en sera adressée à la ministre des affaires sociales et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">55-03-04 PROFESSIONS, CHARGES ET OFFICES. CONDITIONS D'EXERCICE DES PROFESSIONS. PHARMACIENS. - SUSPENSION TEMPORAIRE EN CAS D'ÉTAT PATHOLOGIQUE (ART. R. 4221-15 DU CSP) - MODIFICATION DES RÈGLES DE PROCÉDURE IMMÉDIATEMENT APPLICABLE - EXISTENCE - URGENCE PERMETTANT DE S'AFFRANCHIR DES NOUVELLES RÈGLES - ABSENCE, EN L'ESPÈCE.
</SCT>
<ANA ID="9A"> 55-03-04 Une décision de suspension temporaire du droit d'exercer pour état pathologique rendant dangereux l'exercice de la profession a été prise le 11 septembre 2014 à l'issue d'une instruction menée conformément aux règles applicables avant l'entrée en vigueur du décret n° 2014-545 du 26 mai 2014. Le rapport préalable à cette décision de suspension n'a ainsi pas été établi conformément aux dispositions réglementaires applicables à la date de cette décision....  ,,Si l'intéressé, qui avait à cette date cédé son officine mais s'apprêtait à reprendre une activité professionnelle, présentait des difficultés psychiques et relationnelles de nature à le faire regarder comme dangereux en cas de reprise d'une activité de pharmacien, ces circonstances ne suffisaient pas à caractériser une situation d'urgence de nature à dispenser le Conseil national de l'ordre des pharmaciens du respect de la procédure d'expertise prévue par les dispositions nouvellement applicables, alors notamment qu'il était loisible à ce conseil de saisir le directeur général de l'agence régionale de santé pour que celui-ci suspende immédiatement le droit d'exercer du professionnel concerné sur le fondement de l'article L. 4221-18 du code de la santé publique. Par suite, irrégularité de la procédure et annulation de la décision.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
