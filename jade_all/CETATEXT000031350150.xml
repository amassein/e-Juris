<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031350150</ID>
<ANCIEN_ID>JG_L_2015_10_000000385683</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/35/01/CETATEXT000031350150.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 5ème SSR, 21/10/2015, 385683</TITRE>
<DATE_DEC>2015-10-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>385683</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 5ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY ; SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>Mme Pauline Pannier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2015:385683.20151021</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le comité central d'entreprise de l'unité économique et sociale ND Vrac Pulve, le syndicat national CFTC du groupe Norbert Dentressangle et la Fédération générale des transports et de l'équipement CFDT (FGTE-CFDT) ont demandé au tribunal administratif de Grenoble d'annuler pour excès de pouvoir la décision du 21 novembre 2013 par laquelle le directeur régional des entreprises, de la concurrence, de la consommation, du travail et de l'emploi (DIRECCTE) de Rhône-Alpes a homologué le document unilatéral portant plan de sauvegarde de l'emploi (PSE) de l'unité économique et sociale ND Vrac Pulve. Par un jugement n° 1400318 du 18 avril 2014, le tribunal administratif de Grenoble a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 14LY01839 du 11 septembre 2014, la cour administrative d'appel de Lyon a rejeté l'appel du comité central d'entreprise de l'unité économique et sociale ND Vrac Pulve et autres dirigé contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 12 novembre 2014, 15 décembre 2014 et 21 mai 2015 au secrétariat du contentieux du Conseil d'Etat, le comité d'entreprise de la société Norbert Dentressangle Silo, venant aux droits du comité central d'entreprise de l'unité économique et sociale ND Vrac Pulve, le syndicat national CFTC du groupe Norbert Dentressangle et la FGTE-CFDT demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ; <br/>
<br/>
              3°) de mettre solidairement à la charge de l'Etat et de la société Norbert Dentressangle Silo la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Pannier, auditeur,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat du Comité d'entreprise de la société Norbert Dentressangle Silo et autres et à la SCP Gatineau, Fattaccini, avocat de la société Norbert Dentresangle Silo et de la société Norbert Dentresangle Inter Pulve  ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que l'unité économique et sociale ND Vrac Pulve, regroupant deux sociétés du groupe Norbert Dentressangle, a engagé un projet de réorganisation comportant un projet de licenciement collectif pour motif économique ; que le directeur régional des entreprises, de la concurrence, de la consommation, du travail et de l'emploi de Rhône-Alpes a, par une décision du 21 novembre 2013, homologué le document unilatéral de l'employeur portant plan de sauvegarde de l'emploi ; que le comité d'entreprise de la société Norbert Dentressangle Silo, venant aux droits du comité central d'entreprise de l'unité économique et sociale ND Vrac Pulve, le syndicat national CFTC du groupe Norbert Dentressangle et la Fédération générale des transports et de l'équipement CFDT (FGTE-CFDT) se pourvoient en cassation contre l'arrêt du 11 septembre 2014 par lequel la cour administrative d'appel de Lyon a rejeté leur requête tendant à l'annulation du jugement du 18 avril 2014 du tribunal administratif de Grenoble rejetant leur demande d'annulation de la décision du 21 novembre 2013 ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 1233-61 du code du travail : " Dans les entreprises d'au moins cinquante salariés, lorsque le projet de licenciement concerne au moins dix salariés dans une même période de trente jours, l'employeur établit et met en oeuvre un plan de sauvegarde de l'emploi pour éviter les licenciements ou en limiter le nombre. / Ce plan intègre un plan de reclassement visant à faciliter le reclassement des salariés dont le licenciement ne pourrait être évité (...) " ; qu'aux termes de l'article L. 2323-15 du même code : " Le comité d'entreprise est saisi en temps utile des projets de restructuration et de compression des effectifs. / Il émet un avis sur l'opération projetée et ses modalités d'application dans les conditions et délais prévus à l'article L. 1233-30, [lorsque l'entreprise] est soumise à l'obligation d'établir un plan de sauvegarde de l'emploi " ; qu'aux termes de l'article L. 1233-24-4 du même code : " (...) un document élaboré par l'employeur après la dernière réunion du comité d'entreprise fixe le contenu du plan de sauvegarde de l'emploi (...) " ; qu'aux termes de l'article L. 1233-57-3 du même code : " (...) l'autorité administrative homologue le document élaboré par l'employeur mentionné à l'article L. 1233-24-4, après avoir vérifié (...) la régularité de la procédure d'information et de consultation du comité d'entreprise (...), le respect par le plan de sauvegarde de l'emploi des articles L. 1233-61 à L. 1233-63 en fonction des critères suivants : / 1° Les moyens dont disposent l'entreprise, l'unité économique et sociale et le groupe ; / 2° Les mesures d'accompagnement prévues au regard de l'importance du projet de licenciement ; / 3° Les efforts de formation et d'adaptation tels que mentionnés aux articles L. 1233-4 et L. 6321-1 " ; <br/>
<br/>
              Sur l'arrêt en tant qu'il statue sur la procédure d'information et de consultation du comité d'entreprise :<br/>
<br/>
              3. Considérant qu'il résulte des dispositions citées ci-dessus que, lorsqu'elle est saisie par un employeur d'une demande d'homologation d'un document élaboré en application de l'article L. 1233-24-4 du code du travail et fixant le contenu d'un plan de sauvegarde de l'emploi, il appartient à l'administration de s'assurer, sous le contrôle du juge de l'excès de pouvoir, que la procédure d'information et de consultation du comité d'entreprise a été régulière ; qu'elle ne peut légalement accorder l'homologation demandée que si ce comité a été mis à même d'émettre régulièrement un avis, d'une part sur l'opération projetée et ses modalités d'application et, d'autre part, sur le projet de licenciement collectif et le plan de sauvegarde de l'emploi ; qu'il appartient à ce titre à l'administration de s'assurer que l'employeur a adressé au comité d'entreprise, avec la convocation à sa première réunion, ainsi que, le cas échéant, en réponse à des demandes exprimées par le comité, tous les éléments utiles pour qu'il formule ses deux avis en toute connaissance de cause ;<br/>
<br/>
              4. Considérant qu'aux termes de l'article L. 1233-34 du même code : " Dans les entreprises d'au moins cinquante salariés, lorsque le projet de licenciement concerne au moins dix salariés dans une même période de trente jours, le comité d'entreprise peut recourir à l'assistance d'un expert-comptable (...) " ; qu'aux termes de l'article L. 1233-35 du même code : " L'expert désigné par le comité d'entreprise demande à l'employeur, au plus tard dans les dix jours à compter de sa désignation, toutes les informations qu'il juge nécessaires à la réalisation de sa mission. (...) " ; que lorsque l'assistance d'un expert-comptable a été demandée selon les modalités prévues par ces dispositions, l'administration doit s'assurer que celui-ci a pu exercer sa mission dans des conditions permettant au comité d'entreprise de formuler ses avis en toute connaissance de cause ; que la cour, tout en relevant que l'expert-comptable n'avait pas eu accès à l'intégralité des documents dont il avait demandé la communication, a estimé, par une appréciation souveraine qui n'est pas entachée de dénaturation, que les conditions dans lesquelles il avait accompli sa mission avaient néanmoins permis au comité d'entreprise de disposer de tous les éléments utiles pour formuler ses avis en toute connaissance de cause ; qu'elle a pu, dès lors, sans entacher sa décision d'erreur de droit, juger que la procédure d'information et de consultation du comité d'entreprise avait été conduite de manière régulière ;<br/>
<br/>
              Sur l'arrêt en tant qu'il statue sur le contenu du plan de sauvegarde de l'emploi :<br/>
<br/>
              5. Considérant que lorsqu'elle est saisie d'une demande d'homologation d'un document élaboré en application de l'article L. 1233-24-4 du code du travail, il appartient à l'administration, sous le contrôle du juge de l'excès de pouvoir, de vérifier la conformité de ce document et du plan de sauvegarde de l'emploi dont il fixe le contenu aux dispositions législatives et aux stipulations conventionnelles applicables, en s'assurant notamment du respect par le plan de sauvegarde de l'emploi des dispositions des articles L. 1233-61 à L. 1233-63 du même code ; qu'à ce titre elle doit, au regard de l'importance du projet de licenciement, apprécier si les mesures contenues dans le plan sont précises et concrètes et si, à raison, pour chacune, de sa contribution aux objectifs de maintien dans l'emploi et de reclassement des salariés, elles sont, prises dans leur ensemble, propres à satisfaire à ces objectifs compte tenu, d'une part, des efforts de formation et d'adaptation déjà réalisés par l'employeur et, d'autre part, des moyens dont disposent l'entreprise et, le cas échéant, l'unité économique et sociale et le groupe ; qu'à cet égard, il revient notamment à l'autorité administrative de s'assurer que le plan de reclassement intégré au plan de sauvegarde de l'emploi est de nature à faciliter le reclassement des salariés dont le licenciement ne pourrait être évité ; que l'employeur doit, à cette fin, avoir identifié dans le plan l'ensemble des possibilités de reclassement des salariés dans l'entreprise ; qu'en outre, lorsque l'entreprise appartient à un groupe, l'employeur, seul débiteur de l'obligation de reclassement, doit avoir procédé à une recherche sérieuse des postes disponibles pour un reclassement dans les autres entreprises du groupe ; que, pour l'ensemble des postes de reclassement ainsi identifiés, l'employeur doit avoir indiqué dans le plan leur nombre, leur nature et leur localisation ;<br/>
<br/>
              6. Considérant, en premier lieu, qu'il appartient au juge de l'excès de pouvoir de statuer sur la légalité d'une décision administrative au vu des éléments du dossier qui lui est soumis, lequel peut comporter non seulement les éléments dont disposait l'administration à la date de sa décision mais aussi, le cas échéant, des éléments produits seulement devant lui ; que, par suite, les requérants ne pouvaient utilement soutenir, devant les juges du fond, que la décision d'homologation litigeuse était illégale pour le motif que l'administration n'aurait pas disposé d'informations, notamment sur les moyens de l'entreprise et du groupe, de nature à justifier légalement sa décision ; que ce moyen était ainsi inopérant ; qu'il y a lieu de substituer ce motif au motif  retenu par les juges du fond ; <br/>
<br/>
              7. Considérant, en second lieu, qu'il résulte de ce qui a été dit au point 5 qu'en se fondant, par une appréciation d'ensemble du plan de sauvegarde de l'emploi de l'unité économique et sociale ND Vrac Pulve, sur le fait qu'il contenait des mesures précises et concrètes de nature à faciliter le maintien dans l'emploi et le reclassement des salariés licenciés, notamment, à ce titre, une liste de postes de reclassement dans les entreprises du groupe qui témoignait d'une recherche sérieuse accomplie par l'employeur, la cour administrative d'appel de Lyon a pu, sans erreur de droit et sans dénaturer les pièces du dossier, juger que l'administration avait pu légalement regarder les mesures de ce plan comme suffisantes et en décider, par suite, l'homologation ;<br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que le comité d'entreprise de la société Norbert Dentressangle Silo et autres ne sont pas fondés à demander l'annulation de l'arrêt attaqué ; que leurs conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées au titre de ces mêmes dispositions par la société Norbert Dentressangle Silo ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Le pourvoi du comité d'entreprise de la société Norbert Dentressangle Silo et autres est rejeté.<br/>
Article 2 : Les conclusions présentées par la société Norbert Dentressangle Silo au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée au comité d'entreprise de la société Norbert Dentressangle Silo, venant aux droits du comité central d'entreprise de l'unité économique et sociale ND Vrac Pulve, au syndicat national CFTC du groupe Norbert Dentressangle, à la Fédération générale des transports et de l'équipement CFDT (FGTE-CFDT), à la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social et à la société Norbert Dentressangle Silo.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">66-07 TRAVAIL ET EMPLOI. LICENCIEMENTS. - VALIDATION OU HOMOLOGATION ADMINISTRATIVE DES PSE (LOI DU 14 JUIN 2013) - CONSULTATION DU COMITÉ D'ENTREPRISE - ASSISTANCE D'UN EXPERT-COMPTABLE - 1) CONTRÔLE DE L'AUTORITÉ ADMINISTRATIVE [RJ1] - 2) INCIDENCE DE L'ABSENCE DE COMMUNICATION DE CERTAINS DOCUMENTS À L'EXPERT-COMPTABLE.
</SCT>
<ANA ID="9A"> 66-07 1) Contrôle de la régularité de la consultation du comité d'entreprise lors de l'élaboration d'un plan de sauvegarde de l'emploi (PSE). Lorsque l'assistance d'un expert-comptable a été demandée en application de l'article L. 1233-34 du code du travail, l'administration doit s'assurer que celui-ci a pu exercer sa mission dans des conditions permettant au comité d'entreprise de formuler ses avis en toute connaissance de cause.... ,,2) La circonstance que l'expert-comptable n'ait pas eu accès à l'intégralité des documents dont il a demandé la communication ne vicie pas la procédure d'information et de consultation du comité d'entreprise si les conditions dans lesquelles l'expert-comptable a accompli sa mission ont néanmoins permis au comité d'entreprise de disposer de tous les éléments utiles pour formuler ses avis en toute connaissance de cause.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. CE, 21 octobre 2015, Syndicat CFDT santé sociaux de la Seine-Saint-Denis et autres, n° 382633, à publier au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
