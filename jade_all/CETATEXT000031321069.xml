<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031321069</ID>
<ANCIEN_ID>JG_L_2015_10_000000364527</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/32/10/CETATEXT000031321069.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème SSJS, 15/10/2015, 364527, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-10-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>364527</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>HAAS</AVOCATS>
<RAPPORTEUR>Mme Marie-Gabrielle Merloz</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:364527.20151015</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société CHP a demandé au tribunal administratif de Versailles la décharge de la cotisation supplémentaire d'impôt sur les sociétés ainsi que de l'amende prévue par l'article 1763 A du code général des impôts mises à sa charge au titre de l'année 1996. Par un jugement n°s 0700830, 0806139 du 2 novembre 2010, le tribunal administratif de Versailles a rejeté sa demande.<br/>
<br/>
              Par un arrêt n°10VE04178 du 16 octobre 2012, la cour administrative d'appel de Versailles a, d'une part, déchargé la société CHP de la cotisation supplémentaire d'impôt sur les sociétés ainsi que de l'amende prévue à l'article 1763 A du code général des impôts mises à sa charge au titre de l'année 1996 et, d'autre part, réformé ce jugement en ce qu'il avait de contraire à son arrêt.<br/>
<br/>
              Par un pourvoi et un mémoire en réplique, enregistrés le 14 décembre 2012 et le 10 avril 2014 au secrétariat du contentieux du Conseil d'État, le ministre de l'économie et des finances demande au Conseil d'État d'annuler les articles 1er et 2 de cet arrêt.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Gabrielle Merloz, maître des requêtes,<br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Haas, avocat de la société CHP ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'à l'occasion d'une enquête effectuée sur commission rogatoire du juge d'instruction, l'administration fiscale a exercé, le 14 décembre 2004, son droit de communication auprès de l'autorité judiciaire. Au vu des informations ainsi recueillies, faisant apparaître l'existence d'un dispositif de facturations fictives entre la société CHP et les sociétés Sicobat et Financière DS et M.A..., l'administration a notifié à la société CHP, le 17 décembre 2004, des redressements résultant de la réintégration dans ses résultats imposables de commissions regardées comme rémunérant des prestations fictives, versées par elle à la société Sicobat au cours des exercices clos en 1996 et 1997. Par un jugement du 2 novembre 2010, le tribunal administratif de Versailles a déchargé la société CHP de la cotisation supplémentaire d'impôt sur les sociétés et de l'amende prévue par l'article 1763 A du code général des impôts mises à sa charge au titre de l'exercice 1997 en conséquence de ces redressements, en jugeant que l'administration ne pouvait se prévaloir, pour cet exercice, du délai spécial de reprise prévu par l'article L. 170 du livre des procédures fiscales. La société CHP a fait appel de ce jugement en tant que le tribunal avait, en revanche, rejeté sa demande de décharge des impositions supplémentaires et de l'amende mises à sa charge au titre de l'exercice 1996. Le ministre de l'économie et des finances se pourvoit en cassation contre l'arrêt du 16 octobre 2012 par lequel la cour administrative d'appel de Versailles a fait droit à cette requête.<br/>
<br/>
              2. Aux termes de l'article L. 170 du livre des procédures fiscales, alors en vigueur et dont les dispositions ont été reprises à l'article L. 188 C du même livre : " Même si les délais de reprise prévus à l'article L. 169 sont écoulés, les omissions ou insuffisances d'imposition révélées par une instance devant les tribunaux ou par une réclamation contentieuse peuvent être réparées par l'administration des impôts jusqu'à la fin de l'année suivant celle de la décision qui a clos l'instance et, au plus tard, jusqu'à la fin de la dixième année qui suit celle au titre de laquelle l'imposition est due ".  Des insuffisances ou omissions d'imposition ne peuvent pas être regardées comme révélées par une instance devant les tribunaux au sens de cet article lorsque l'administration dispose d'éléments suffisants lui permettant, par la mise en oeuvre des procédures d'investigation dont elle dispose, d'établir ces insuffisances ou omissions d'imposition dans le délai normal de reprise prévu à l'article L. 169 du livre des procédures fiscales.<br/>
<br/>
              3. Il ressort des énonciations de l'arrêt attaqué que l'administration disposait avant le 31 décembre 1999, date d'expiration du délai de reprise de droit commun découlant de l'article L. 169 du livre des procédures fiscales pour l'exercice clos en 1996, d'éléments faisant apparaître des incohérences dans les honoraires versés de 1995 à 1997 par la société CHP, qui l'avaient conduite à établir une note interne, dite " proposition 3939 ", mentionnant une " sous-traitance non validée " et faisant état de ce que la société Sicobat, à laquelle la société CHP avait versé des commissions d'apporteur d'affaires, s'avérait être une entreprise de maçonnerie. En outre, il ressort des pièces du dossier soumis aux juges du fond que la société Sicobat n'avait plus d'activité depuis 1993, selon une information qui était disponible sur le site Infogreffe. <br/>
<br/>
              4. Dès lors que ces éléments étaient suffisants pour permettre à l'administration fiscale de faire usage des procédures d'investigation dont elle dispose pour notifier à la société CHP, dans le délai normal de reprise, des redressements au titre de l'exercice 1996, la cour administrative d'appel de Versailles n'a commis ni erreur de droit ni erreur de qualification juridique des faits en jugeant que les omissions et insuffisances d'imposition litigieuses, qui découlaient du caractère non déductible des commissions versées à la société Sicobat, ne pouvaient être regardées comme ayant été révélées par l'instance pénale, au sens de l'article L. 170 du livre des procédures fiscales, en ce qui concerne les factures émanant de la société Sicobat. Par suite, le ministre n'est pas fondé à demander l'annulation des articles 1er et 2 de l'arrêt attaqué.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'État une somme de 3 500 euros à verser à la société CHP au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi du ministre de l'économie et des finances est rejeté.<br/>
Article 2 : L'État versera à la société CHP une somme de 3 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée au ministre des finances et des comptes publics et à la société CHP.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
