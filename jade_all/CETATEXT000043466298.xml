<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043466298</ID>
<ANCIEN_ID>JG_L_2021_04_000000448633</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/46/62/CETATEXT000043466298.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 30/04/2021, 448633, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-04-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>448633</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP GATINEAU, FATTACCINI, REBEYROL</AVOCATS>
<RAPPORTEUR>Mme Thalia Breton</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Raphaël Chambon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:448633.20210430</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              L'Union de recouvrement de cotisations de sécurité sociale et d'allocations familiales (URSSAF) d'Aquitaine a porté plainte contre M. D... A... devant la chambre disciplinaire de première instance d'Aquitaine de l'ordre des médecins, devenue la chambre disciplinaire de première instance de Nouvelle-Aquitaine de l'ordre des médecins. Par une décision du 17 octobre 2017, la chambre disciplinaire de première instance a rejeté la plainte de l'URSSAF d'Aquitaine.<br/>
<br/>
              Par une décision du 30 novembre 2020, la chambre disciplinaire nationale de l'ordre des médecins a, sur appels de l'URSSAF d'Aquitaine et du Conseil national de l'ordre des médecins, annulé la décision de la chambre disciplinaire de première instance, infligé à M. A... la sanction de l'interdiction d'exercer la médecine pendant une durée de dix-huit mois et décidé que cette sanction prendra effet au 1er mars 2021. <br/>
<br/>
              1° Sous le n° 448633, par un pourvoi enregistré le 13 janvier 2021 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de l'URSSAF d'Aquitaine et du Conseil national de l'ordre des médecins ;<br/>
<br/>
              3°) de mettre à la charge de l'URSSAF d'Aquitaine et du Conseil national de l'ordre des médecins la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Sous le n° 448634, par une requête, un mémoire en réplique et un nouveau mémoire, enregistrés les 13 janvier, 8 et 22 mars 2021 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat d'ordonner qu'il soit sursis à l'exécution de la même décision du 30 novembre 2020 et de mettre à la charge de l'URSSAF d'Aquitaine et du Conseil national de l'ordre des médecins la somme de 2 000 euros au titre de l'article L.761-1 du code de justice administrative. <br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - le code de la sécurité sociale ; <br/>
              - l'ordonnance n° 2020-1402 du 18 novembre 2020, notamment son article 2 ; <br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme C... B..., auditrice,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de M. A... ; à la SCP Gatineau, Fattaccini, Rebeyrol, avocat de l'URSSAF d'Aquitaine et à la SCP Matuchansky, Poupot, Valdelièvre, avocat du Conseil national de l'ordre des médecins ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Le pourvoi par lequel M. A... demande l'annulation de la décision du 30 novembre 2020 de la chambre disciplinaire nationale de l'ordre des médecins et la requête par laquelle il demande qu'il soit sursis à l'exécution de cette décision présentent à juger les mêmes questions. Il y a lieu d'y statuer par une seule décision.<br/>
<br/>
              2. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
<br/>
              3. Pour demander l'annulation de la décision de la chambre disciplinaire nationale de l'ordre des médecins qu'il attaque, M. A... soutient qu'elle est entachée :<br/>
              - d'erreur de droit en ce qu'elle s'abstient de relever d'office l'irrecevabilité de l'appel formé par le Conseil national de l'ordre des médecins, qui n'avait pas la qualité de partie devant la chambre disciplinaire de première instance ;<br/>
              - de contradiction de motifs et, à tout le moins, d'erreur de droit et d'insuffisance de motivation en ce qu'elle affirme, sans répondre de manière précise au moyen dont elle était saisie, que l'affiliation obligatoire à l'Union de recouvrement de cotisations de sécurité sociale et d'allocations familiales (URSSAF) est conforme au droit de l'Union européenne et que les contentieux s'y rapportant ressortissent de la compétence des juridictions du contentieux général de la sécurité sociale ;<br/>
               - d'erreur de droit en ce qu'elle se fonde, pour établir le manquement disciplinaire relatif au non-paiement de ses cotisations auprès de l'URSSAF, sur les jugements du tribunal des affaires de sécurité sociale de Bayonne assortis de l'exécution provisoire, bien qu'ils soient dépourvus de caractère définitif ; <br/>
              - d'erreur de droit par méconnaissance de sa compétence, d'inexacte qualification juridique des faits et d'insuffisance de motivation en ce qu'elle juge que les recours qu'il a formés devant les juridictions du contentieux de la sécurité sociale présentent un caractère dilatoire et abusif.<br/>
<br/>
              Il soutient, en outre, que la sanction prononcée est hors de proportion avec les fautes reprochées.<br/>
<br/>
              4. Aucun de ces moyens n'est de nature à permettre l'admission du pourvoi.<br/>
<br/>
              5. Le pourvoi formé par M. A... contre la décision du 30 novembre 2020 de la chambre disciplinaire nationale de l'ordre des médecins n'étant pas admis, sa requête tendant à ce qu'il soit sursis à l'exécution de cette décision est devenue sans objet. Il n'y a donc pas lieu d'y statuer.<br/>
<br/>
              6. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'URSSAF d'Aquitaine et du Conseil national de l'ordre des médecins à ce titre. Dans les circonstances de l'espèce, il y a lieu, au même titre, de mettre à la charge de M. A... le versement d'une somme de 1 500 euros à l'URSSAF d'Aquitaine et d'une même somme au Conseil national de l'ordre des médecins.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A... n'est pas admis.<br/>
Article 2 : Il n'y a pas lieu de statuer sur les conclusions de la requête à fin de sursis à exécution de la décision du 30 novembre 2020 de la chambre disciplinaire nationale de l'ordre des médecins. <br/>
Article 3 : Les conclusions de M. A... présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 4 : M. A... versera une somme de 1 500 euros à l'Union de recouvrement de cotisations de sécurité sociale et d'allocations familiales d'Aquitaine et une même somme au Conseil national de l'ordre des médecins au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : La présente décision sera notifiée à M. D... A..., à l'Union de recouvrement de cotisations de sécurité sociale et d'allocations familiales d'Aquitaine et au Conseil national de l'ordre des médecins.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
