<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044545397</ID>
<ANCIEN_ID>JG_L_2021_12_000000449727</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/54/53/CETATEXT000044545397.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 21/12/2021, 449727, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>449727</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>M. Frédéric Gueudar Delahaye</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Marc Pichon de Vendeuil</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:449727.20211221</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. D... B... a demandé au tribunal administratif de Toulouse d'annuler l'arrêté du 26 octobre 2017 par lequel le ministre de la transition écologique et solidaire l'a reclassé, à compter du 1er mars 2017, dans le corps des ingénieurs des travaux publics de l'Etat au 8ème échelon du premier niveau de grade avec un reliquat d'ancienneté de onze mois et quatre jours et d'enjoindre au ministre de la transition écologique et solidaire de procéder à un nouveau reclassement. Par un jugement n° 1706050 du 8 mars 2019, le tribunal administratif de Toulouse a annulé cet arrêté et enjoint au ministre de la transition écologique et solidaire de prendre, dans un délai de deux mois, une nouvelle décision de reclassement de M. B... dans le corps des ingénieurs de travaux publics de l'Etat tenant compte de son ancienneté déterminée en application du 2° de l'article 21 du décret du 30 mai 2005.<br/>
<br/>
              Par un arrêt n°s 19BX01969, 19BX01970 du 14 décembre 2020, la cour administrative d'appel de Bordeaux a, sur appel du ministre de la transition écologique et solidaire, d'une part, annulé ce jugement et rejeté la demande de M. B... devant le tribunal administratif de Toulouse, d'autre part, décidé qu'il n'y avait pas lieu de statuer sur la requête du ministre tendant au sursis à exécution de ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 15 février et 17 mai 2021 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter les conclusions d'appel du ministre de la transition écologique et solidaire ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - le décret n° 99-749 du 26 août 1999 ;<br/>
              - le décret n° 2005-631 du 30 mai 2005 ; <br/>
              - le décret n° 2009-1388 du 11 novembre 2009 ; <br/>
              - le décret n° 2012-1064 du 18 septembre 2012 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Gueudar Delahaye, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Marc Pichon de Vendeuil, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Thouvenin, Coudray, Grévy, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. M. B... a été recruté le 26 octobre 1987 dans le corps des techniciens supérieurs des travaux publics de l'Etat à l'issue de sa réussite au concours externe et titularisé à compter du 1er novembre 1989 en qualité d'assistant technique des travaux publics de l'Etat (ATTPE), corps de la catégorie B. A la suite de son inscription sur une liste d'aptitude, il a intégré le corps des ingénieurs des travaux publics de l'Etat (ITPE) relevant de la catégorie A, au titre de l'année 2017. Par un arrêté du 26 octobre 2017, le ministre de la transition écologique et solidaire l'a reclassé dans ce corps à compter du 1er mars 2017 au 8ème échelon du premier niveau de grade avec un reliquat d'ancienneté de onze mois et quatre jours. M. B... a contesté cet arrêté devant le tribunal administratif de Toulouse qui, par un jugement du 8 mars 2019, a fait droit à sa demande et annulé l'arrêté de reclassement. Il se pourvoit en cassation contre l'arrêt du 14 décembre 2020 par lequel la cour administrative d'appel de Bordeaux, après avoir annulé ce jugement sur l'appel de la ministre de la transition écologique, a rejeté sa demande présentée devant les premiers juges. <br/>
<br/>
              2. Aux termes de l'article 5 du décret du 30 mai 2005 portant statut particulier du corps des ingénieurs des travaux publics de l'Etat : " Les ingénieurs des travaux publics de l'Etat sont (...) recrutés (...) 4° Parmi les techniciens supérieurs du développement durable qui ont été inscrits sur une liste d'aptitude établie dans les conditions fixées à l'article 11 ". Aux termes de l'article 11 du même décret : " Pour pouvoir être inscrits sur la liste d'aptitude mentionnées au 4° de l'article 5, les techniciens supérieurs du développement durable doivent détenir le grade de technicien supérieur en chef et compter au moins huit ans de services effectifs dans ce grade. Sont également pris en compte les services accomplis dans les grades de (...) techniciens supérieur principal de l'équipement avant le 1er octobre 2012 (...) ". Aux termes de l'article 21 du même décret: " Les membres des corps et cadres d'emplois de catégorie B régis par les décrets n° 2009-1388 du 11 novembre 2009 portant dispositions statutaires communes à divers corps de fonctionnaires de la catégorie B de la fonction publique de l'Etat, n° 2010-329 du 22 mars 2010 portant dispositions statutaires communes à divers cadres d'emplois de fonctionnaires de la catégorie B de la fonction publique territoriale et n° 2011-661 du 14 juin 2011 portant dispositions statutaires communes à divers corps de fonctionnaires de la catégorie B de la fonction publique hospitalière sont classés, lors de leur nomination dans le corps des ingénieurs des travaux publics de l'Etat, à un échelon déterminé sur la base des durées fixées à l'article 28 pour chaque avancement d'échelon, en prenant en compte leur ancienneté dans cette catégorie dans les conditions définies aux alinéas suivants. / Cette ancienneté de carrière est calculée sur la base : / 1° Pour les fonctionnaires relevant de leur grade de recrutement, de la durée statutaire du temps passé dans les échelons de ce grade, augmenté, le cas échéant, de l'ancienneté acquise dans l'échelon détenu dans ce même grade ; / 2° Pour les fonctionnaires ayant bénéficié d'un ou de plusieurs avancements de grade dans leur corps ou cadre d'emplois d'origine, de l'ancienneté qu'il est nécessaire de détenir au minimum dans le ou les grades inférieurs dont ils ont été titulaires pour accéder au dernier grade détenu. Cette durée minimale est calculée en prenant en compte : / a) Pour le grade de recrutement, la durée minimale nécessaire pour atteindre l'échelon à partir duquel les agents peuvent accéder au grade supérieur ; / b) Pour les grades d'avancement, la durée requise pour atteindre l'échelon détenu depuis l'échelon dans lequel ils auraient été reclassés s'ils avaient été promus depuis l'échelon déterminé au 1° ci-dessus. / Cette ancienneté est augmentée, le cas échéant, de l'ancienneté acquise dans l'échelon détenu dans le dernier grade détenu. / Toutefois, l'ancienneté ainsi calculée ne peut être inférieure à celle qui aurait été retenue pour ce fonctionnaire dans le grade inférieur s'il n'avait pas obtenu d'avancement de grade. / L'ancienneté ainsi déterminée n'est pas retenue en ce qui concerne les quatre premières années ; elle est prise en compte à raison des deux tiers pour la fraction comprise entre quatre et dix ans et des trois quarts pour celle excédant dix ans. / Si l'application des dispositions qui précèdent ne leur est pas plus favorable, les fonctionnaires sont classés dans le grade d'ingénieur des travaux publics de l'Etat à l'échelon comportant un indice brut égal ou à défaut immédiatement supérieur à celui perçu dans leur ancien emploi avec conservation de l'ancienneté acquise dans l'échelon, dans les conditions définies en application des dispositions de l'article 4 du décret du 23 décembre 2006 précité. ".<br/>
<br/>
              3. En premier lieu, il ressort des pièces du dossier soumis aux juges du fond, et notamment de la fiche de calcul produite par le ministre de la transition écologique et solidaire devant la cour, que, contrairement à ce que soutient le requérant, l'administration a fait application des dispositions du 2° de l'article 21 du décret du 30 mai 2015 pour procéder au reclassement de M. B... dans le corps des ITPE. Il suit de là qu'en estimant que, pour déterminer l'ancienneté à prendre en compte lors de ce reclassement, l'administration s'est fondée sur ces dispositions, et non sur celles du 1°, comme l'ont relevé à tort les premiers juges, la cour administrative d'appel de Bordeaux n'a pas dénaturé les pièces du dossier. <br/>
<br/>
              4. En deuxième lieu, aux termes de l'article 2 du décret du 2 octobre 1970 relatif au statut particulier du corps des techniciens des travaux publics de l'Etat : " Le corps des techniciens des travaux publics de l'Etat (service de l'équipement) comprend les grades d'assistant technique, de chef de section et de chef de section principal. " En vertu de l'article 2 du décret du 26 août 1999 modifiant le décret du 2 octobre 1970 précité, le grade d'assistant technique des travaux publics de l'Etat (ATTPE) a été remplacé par le grade de technicien supérieur de l'équipement (TSE). Aux termes de l'article 21 du décret du 18 septembre 2012 portant statut particulier du corps des techniciens supérieurs du développement durable : " I. - Les fonctionnaires appartenant aux corps des techniciens supérieurs de l'équipement [...] sont intégrés dans le corps des techniciens supérieurs du développement durable régi par le présent décret [...]. / III - Les services accomplis dans les corps des techniciens supérieurs de l'équipement [...] avant la date d'entrée en vigueur du présent décret sont assimilés à des services accomplis dans le corps des techniciens supérieurs du développement durable régi par le présent décret. / Les intéressés conservent les réductions et majorations d'ancienneté accordées et non utilisées pour un avancement d'échelon dans leur ancien corps. " En vertu du tableau annexé à cet article, le grade de TSE équivaut au grade de technicien supérieur principal du développement durable (TSPDD) dans le corps des techniciens supérieurs du développement durable. Enfin, aux termes de l'article 38 du même décret : " Le décret n° 70-903 du 2 octobre 1970 relatif au statut particulier du corps des techniciens supérieurs de l'équipement, le décret n° 88-399 du 21 avril 1988 relatif au statut particulier du corps des contrôleurs des travaux publics de l'Etat et le décret n° 95-204 du 24 février 1995 relatif aux conditions de nomination et d'avancement dans l'emploi de chef de subdivision des services du ministère chargé de l'équipement sont abrogés. " <br/>
<br/>
              5. Il résulte des dispositions citées au point précédent qu'au 1er mars 2017, date de son reclassement dans le corps des ITPE, M. B... devait être regardé comme ayant été recruté dans un grade équivalent à celui d'ATTPE dans le corps des techniciens supérieurs du développement durable dont il relevait au moment de son intégration dans le corps des ITPE, à savoir le grade de technicien supérieur principal du développement durable (TSPDD). Par suite, en déduisant de ces circonstances que M. B... n'était pas fondé à soutenir que l'administration aurait dû tenir compte de son ancienneté dans le grade d'ATTPE dès lors que les dispositions du a) du 2° de l'article 21 du décret du 30 mai 2005 citées au point 2 ne le prévoyaient pas, la cour administrative d'appel de Bordeaux n'a pas commis d'erreur de droit ni entaché son arrêt d'insuffisance de motivation.<br/>
<br/>
              6. En troisième lieu, en vertu de l'article 2 du décret du 18 septembre 2012 cité au point 4, les grades de technicien supérieur du développement durable, technicien supérieur principal du développement durable et technicien supérieur en chef du développement durable sont respectivement assimilés aux premier, deuxième et troisième grades mentionnés par le décret du 11 novembre 2009 portant dispositions statutaires communes à divers corps de fonctionnaires de la catégorie B de la fonction publique de l'Etat. L'article 2 de ce décret prévoit que les deux premiers grades comportent treize échelons et le troisième grade onze échelons. Aux termes de l'article 16 du même décret : " La durée moyenne passée dans chacun des échelons des grades du corps des techniciens supérieurs du développement durable est fixée conformément aux dispositions de l'article 24 du décret du 11 novembre 2009 susvisé. " Elle s'établit, en application de ces dispositions, à un an pour le 1er échelon, deux ans pour les 2ème, 3ème, 4ème et 5ème échelon, et trois ans pour les 6ème, 7ème, 8ème, 9ème et 10ème échelon du troisième grade de technicien supérieur en chef du développement durable. <br/>
<br/>
              7. Il résulte de ce qui a été dit aux points 5 et 6 que, dès lors que M. B... doit être regardé comme ayant été recruté au deuxième grade du corps des techniciens supérieurs du développement durable, la durée à prendre en compte, au titre des dispositions du b) du 2° de l'article 21 du décret du 30 mai 2005, entre son avancement au 1er échelon du troisième grade et le dernier échelon détenu dans ce grade avant d'être reclassé dans le corps des ITPE, soit le 10ème échelon, s'établit à vingt-et-un ans. Par suite, le moyen tiré de ce que la cour administrative d'appel de Bordeaux aurait commis une erreur de droit en ne tenant compte, dans l'application de ces dispositions, que d'un seul des trois avancements de grade dont l'intéressé a bénéficié durant sa carrière, ne peut qu'être écarté. <br/>
<br/>
              8. Il résulte de ce qui précède que M. B... n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque.<br/>
<br/>
              9. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : Le pourvoi de M. B... est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. D... B... et à la ministre de la transition écologique. <br/>
              Délibéré à l'issue de la séance du 6 décembre 2021 où siégeaient : M. Benoît Bohnert, assesseur, présidant ; M. Gilles Pellissier, conseiller d'Etat et M. Frédéric Gueudar Delahaye, conseiller d'Etat-rapporteur. <br/>
<br/>
              Rendu le 21 décembre 2021.<br/>
<br/>
                 Le président : <br/>
                 Signé : M. Benoît Bohnert<br/>
 		Le rapporteur : <br/>
      Signé : M. Frédéric Gueudar Delahaye<br/>
                 La secrétaire :<br/>
                 Signé : Mme C... A...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
