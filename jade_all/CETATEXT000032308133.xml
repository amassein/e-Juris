<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032308133</ID>
<ANCIEN_ID>JG_L_2016_03_000000386623</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/30/81/CETATEXT000032308133.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème SSR, 25/03/2016, 386623</TITRE>
<DATE_DEC>2016-03-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>386623</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MARLANGE-DE LA BURGADE ; BLONDEL</AVOCATS>
<RAPPORTEUR>M. Pierre Lombard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2016:386623.20160325</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La commune de la Motte-Ternant (Côte d'Or) a saisi le tribunal administratif de Dijon d'une demande tendant, d'une part, à l'annulation de la décision implicite par laquelle le syndicat intercommunal d'adduction d'eau potable et d'assainissement (SIAEPA) de Semur-en-Auxois, auquel elle a adhéré, a refusé de prendre en charge le déficit du compte administratif communal, d'un montant de 29 137,68 euros, de gestion de son service " eau " à l'occasion du transfert des biens de ce service au syndicat, d'autre part, à la condamnation de ce syndicat à lui verser la somme de 29 137,68 euros. Par un jugement n° 1201633, le tribunal administratif de Dijon a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 13LY02970 du 21 octobre 2014, la cour administrative d'appel de Lyon, sur appel de la commune, a annulé le jugement du tribunal administratif de Dijon puis, statuant par voie d'évocation, a rejeté sa requête.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 22 décembre 2014 et 11 mars 2015 au secrétariat du contentieux du Conseil d'Etat, la commune de la Motte-Ternant demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge du SIAEPA la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pierre Lombard, auditeur,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Tiffreau, Marlange, de la Burgade, avocat de la commune de La Motte-Ternant et à Me Blondel, avocat du syndicat intercommunal d'adduction d'eau potable et d'assainissement de Semur-en-Auxois ;<br/>
<br/>
<br/>
<br/>
              1. Considérant que, par un arrêté du 31 août 2005, le sous-préfet de Montbard a étendu le périmètre du syndicat d'adduction d'eau potable et d'assainissement de Semur-en-Auxois (SIAEPA) à la commune de La Motte-Ternant ; que, le syndicat intercommunal et le conseil municipal de La Motte-Ternant ont, par délibérations respectives, prévu que l'adhésion de la commune au SIAEPA serait effective au 1er janvier 2007 ; que, par une délibération du 30 mars 2007, le conseil municipal de la commune a procédé aux constatations préalables à l'établissement du procès-verbal de transfert de la compétence " eau "  au syndicat ; que cette délibération prévoyait notamment la mise à la disposition du SIAEPA des biens meubles et immeubles utilisés, ainsi que le solde déficitaire du compte administratif 2006 du budget annexe " eau " d'un montant de 29 137,68 euros ; que, par un courrier du 23 décembre 2011, la commune de La Motte-Ternant a demandé au syndicat intercommunal de prendre en charge le déficit du compte administratif communal d'un montant de 29 137,68 euros ; que le SIAEPA a refusé implicitement de prendre en charge ce déficit ; que la commune de la Motte-Ternant a saisi le tribunal administratif de Dijon d'une demande tendant, d'une part, à l'annulation de la décision implicite du SIAEPA et, d'autre part, à la condamnation de ce syndicat à lui verser la somme de 29 137,68 euros ; que, par un jugement du 17 septembre 2013, le tribunal administratif de Dijon a rejeté sa demande ; que la commune de La Motte-Ternant se pourvoit en cassation contre l'arrêt du 21 octobre 2014 par lequel la cour administrative d'appel de Lyon a annulé ce jugement puis rejeté sa demande de première instance ;<br/>
<br/>
              2. Considérant qu'aux termes l'article L. 5211-18 du code général des collectivités territoriales : " I.-Sans préjudice des dispositions de l'article L. 5215-40, le périmètre de l'établissement public de coopération intercommunale peut être ultérieurement étendu, par arrêté du ou des représentants de l'Etat dans le ou les départements concernés, par adjonction de communes nouvelles : / (...) 3° Soit sur l'initiative du représentant de l'Etat. La modification est alors subordonnée à l'accord de l'organe délibérant et des conseils municipaux dont l'admission est envisagée. (...)/ II.-Le transfert des compétences entraîne de plein droit l'application à l'ensemble des biens, équipements et services publics nécessaires à leur exercice, ainsi qu'à l'ensemble des droits et obligations qui leur sont attachés à la date du transfert, des dispositions des trois premiers alinéas de l'article L. 1321-1 (...) " ; qu'aux termes du premier alinéa de l'article L. 1321-1 du même code : " Le transfert d'une compétence entraîne de plein droit la mise à la disposition de la collectivité bénéficiaire des biens meubles et immeubles utilisés, à la date de ce transfert, pour l'exercice de cette compétence " ; <br/>
<br/>
              3. Considérant que pour l'application de ces dispositions, le solde du compte administratif du budget annexe d'un service public à caractère industriel ou commercial ne constitue pas un bien qui serait nécessaire à l'exercice de ce service public, ni un ensemble de droits et obligations qui lui seraient attachés ; que, par suite, c'est sans commettre d'erreur de droit que la cour administrative d'appel a, par un arrêt suffisamment motivé, jugé que les dispositions précitées n'imposaient pas le transfert du solde du compte administratif du budget annexe du service transféré au SIAEPA par la commune de la Motte-Ternant ;<br/>
<br/>
              4. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge du SIAEPA, qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, en revanche, de mettre à la charge de la commune de La Motte-Ternant la somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la commune de la La Motte-Ternant est rejeté.<br/>
Article 2 : La commune de La Motte-Ternant versera au syndicat intercommunal d'adduction d'eau potable et d'assainissement de Semur-en-Auxois la somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article  3 : La présente décision sera notifiée à la commune de La Motte-Ternant et au syndicat intercommunal d'adduction d'eau potable et d'assainissement de Semur-en-Auxois.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-05-01-01 Collectivités territoriales. Coopération. Établissements publics de coopération intercommunale - Questions générales. Dispositions générales et questions communes.
</SCT>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
