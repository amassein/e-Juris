<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031281238</ID>
<ANCIEN_ID>JG_L_2015_10_000000368493</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/28/12/CETATEXT000031281238.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  9ème sous-section jugeant seule, 05/10/2015, 368493, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-10-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>368493</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 9ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>M. Bastien Lignereux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Astrid Nicolazo de Barmon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:368493.20151005</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La SARL Elce a demandé au tribunal administratif de Limoges de prononcer la décharge des cotisations supplémentaires d'impôt sur les sociétés et de contribution additionnelle à cet impôt auxquelles elle a été assujettie au titre des exercices clos de 2000 à 2002, ainsi que des pénalités correspondantes. Par un jugement n° 0901230 du 31 mars 2011, le tribunal administratif de Limoges a fait droit à sa demande.<br/>
<br/>
              Par un arrêt n° 11BX01745 du 14 mars 2013, la cour administrative d'appel de Bordeaux, sur le recours du ministre du budget des comptes publics et de la réforme de l'Etat a, d'une part, annulé ce jugement en tant qu'il a omis de prononcer un non-lieu à statuer sur les conclusions de la SARL Elce tendant à la décharge d'une partie des cotisations supplémentaires d'impôt sur les sociétés et de contribution additionnelle à cet impôt au titre de l'exercice clos en 2001 et prononcé un non-lieu partiel à statuer à concurrence de 16 592 euros en ce qui concerne ces impositions, d'autre part, arrêté le résultat fiscal imposable de la SARL Elce à 602 788 francs (91 894,44 euros) au titre de l'exercice clos en 2000 et à 60 999 euros au titre de l'exercice clos en 2002, remis à la charge de la société, conformément à ces bases, les cotisations supplémentaires d'impôt sur les sociétés et de contribution additionnelle à cet impôt au titre des exercices clos en 2000 et 2002 ainsi que les pénalités correspondantes et réformé l'article 1er de ce jugement en ce qu'il avait de contraire à cet arrêt, enfin, rejeté le surplus de la demande de la société devant le tribunal administratif et ses conclusions au titre de l'article L.761-1 du code de justice administrative.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 14 mai et 23 juillet 2013 et le 25 février 2015 au secrétariat du contentieux du Conseil d'Etat, la SARL Elce demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler les articles 3 à 5 de cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter le recours du ministre du budget, des comptes publics et de la réforme de l'Etat ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 8 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le livre des procédures fiscales et le code général des impôts ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bastien Lignereux, auditeur,  <br/>
<br/>
              - les conclusions de Mme Marie-Astrid Nicolazo de Barmon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de la SARL Elce ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la SARL Elce a pour activité la location de salles, l'exploitation de débit de boissons et d'achat-vente de distribution de sodas, boissons, et tous produits alimentaires, achat-vente de vaisselle, électroménager, hifi, TV et généralement toutes opérations industrielles, commerciales, financières, civiles, mobilières ou immobilières pouvant se rattacher directement ou indirectement à l'un de ces objets. Par un courrier du 13 octobre 2003, l'autorité judiciaire a, sur le fondement de l'article L. 101 du livre des procédures fiscales, informé l'administration fiscale de l'existence d'une procédure correctionnelle à l'encontre des représentants de la société Elce, en raison d'éléments permettant de présumer l'existence d'une fraude fiscale. A l'issue de la vérification de comptabilité dont la société Elce a fait l'objet en matière de bénéfices industriels et commerciaux au titre des exercices clos de 2000 à 2002, l'administration a reconstitué son chiffre d'affaires à raison d'une activité occulte d'organisation de loteries. Par un jugement du 31 mars 2011, le tribunal administratif de Limoges a prononcé la décharge des impositions litigieuses. La société Elce se pourvoit en cassation contre les articles 3 à 5 de l'arrêt du 14 mars 2013, par lesquels la cour administrative d'appel de Bordeaux, accueillant le recours du ministre du budget, des comptes publics et de la réforme de l'Etat tendant à ce que soit pris en compte le coût d'achat des lots mis en jeu pour un montant estimé à 20 % des recettes occultes, a arrêté le résultat fiscal imposable de la SARL Elce à 602 788 francs (91 894,44 euros) au titre de l'exercice clos en 2000 et à 60 999 euros au titre de l'exercice clos en 2002 et a remis à la charge de la société les cotisations supplémentaires d'impôt sur les sociétés et de contribution additionnelle à cet impôt restant en litige au titre des exercices clos en 2000 et 2002, correspondant à la rectification des bases d'imposition, ainsi que les pénalités correspondantes.<br/>
<br/>
              Sur la régularité de l'arrêt attaqué :<br/>
<br/>
              2. En premier lieu, la cour, qui n'était pas tenue de répondre à chacun des arguments de la société, a indiqué avec une précision suffisante, eu égard à l'argumentation dont elle était saisie, d'une part, les éléments retenus pour valider la méthode de reconstitution des recettes, d'autre part, les motifs pour lesquels la vérification de comptabilité n'a pu débuter le 2 septembre 2004 et n'a, dès lors, pas excédé le délai de trois mois prévu à l'article L. 53 du livre des procédures fiscales et, enfin, les motifs conduisant à imposer la société Elce, dont M. B...était le gérant de fait, à raison des profits présumés résulter d'une activité de jeux de hasard. Par suite, le moyen tiré de l'insuffisance de motivation de l'arrêt doit être écarté.<br/>
<br/>
              3. En second lieu, le ministre n'a pas invoqué en appel un nouveau motif propre à justifier l'imposition mais s'est borné à accueillir les conclusions subsidiaires présentées par la société Elce en première instance tendant à ce que soient prises en compte, pour reconstituer son chiffre d'affaire, les charges correspondant aux achats de lots mis en jeu pour un montant évalué forfaitairement à 20 %. La cour, qui ne saurait être regardée comme ayant fait droit à une demande de substitution de motifs, a pu en déduire, sans commettre d'erreur de droit, que la nouvelle méthode de reconstitution de recettes proposée par le ministre n'était plus radicalement viciée dans son principe et dans son montant, arrêter le résultat fiscal imposable de la société Elce en résultant et réformer le jugement attaqué en conséquence. La société requérante ne peut, dès lors, utilement invoquer le moyen tiré de la méconnaissance de l'article L. 199 C du livre des procédures fiscales et des règles régissant la substitution de motifs et des droits de la défense. <br/>
<br/>
              Sur les moyens relatifs à la régularité de la procédure d'imposition :<br/>
<br/>
              4. En premier lieu, aux termes de l'article L. 52 du livre des procédures fiscales : " Sous peine de nullité de l'imposition, la vérification sur place des livres ou documents comptables ne peut s'étendre sur une durée supérieure à trois mois en ce qui concerne :/ 1° Les entreprises industrielles et commerciales ou les contribuables se livrant à une activité non commerciale dont le chiffre d'affaires ou le montant annuel des recettes brutes n'excède pas les limites prévues au I de l'article 302 septies A du code général des impôts (...) ". La date à laquelle la vérification sur place des livres et documents mentionnée par ces dispositions doit être regardée comme ayant débuté est celle à laquelle le vérificateur commence à contrôler sur place la sincérité des déclarations fiscales.<br/>
<br/>
              5. La société Elce soutenait devant la cour que la durée de la vérification de comptabilité avait excédé le délai de trois mois prévu par ces dispositions. Il ressort des pièces du dossier soumis au juge du fond qu'à la demande de la société requérante, la première intervention du vérificateur, initialement prévue le 25 août 2004 dans l'avis de vérification de comptabilité portant sur l'année 2000 reçu le 24 juillet 2004, a été reportée au 2 septembre suivant. Toutefois, les opérations de contrôle n'ont pu effectivement débuter à cette date, en l'absence de présentation des documents comptables, ni à une date ultérieure, comme en atteste le procès-verbal de carence de comptabilité dressé le 27 septembre 2004. La cour n'a pas commis d'erreur de droit en jugeant que l'article L. 52 du livre des procédures fiscales n'avait pas été méconnu, après avoir constaté que la vérification de comptabilité n'avait pas débuté le 2 septembre 2004, alors même que l'absence de présentation de la comptabilité à cette date résultait du fait que la comptabilité avait été saisie par le juge judiciaire.<br/>
<br/>
              6. En deuxième lieu, l'administration a pu régulièrement fonder les rectifications litigieuses sur des éléments extracomptables obtenus dans le cadre de son droit de communication. Par suite, la cour n'a pas commis d'erreur de droit en jugeant la procédure de contrôle régulière malgré l'absence de présentation de documents comptables. Si la société requérante invoque, en outre, pour ce même motif, une méconnaissance des droits de la défense et du droit au respect de la vie privée, ces moyens, qui n'ont pas été soumis aux juges du fond, ne peuvent dès lors être utilement soulevés devant le juge de cassation.<br/>
<br/>
              7. En troisième lieu, la cour n'a pas entaché son arrêt d'erreur de droit en jugeant que la société requérante ne pouvait utilement invoquer une méconnaissance des droits de la défense faute pour le juge d'instruction de lui avoir restitué les documents comptables saisis, dès lors que ces documents n'ont, en tout état de cause, pas servi de fondement aux rectifications litigieuses.<br/>
<br/>
              8. En quatrième lieu, en estimant que l'administration était fondée à recourir à une méthode extracomptable pour reconstituer les recettes occultes générées par l'organisation de loteries, la cour s'est livrée à une appréciation souveraine des faits qui ne peut être discutée en cassation. Le moyen tiré de ce que le refus de l'autorité judiciaire de lui restituer les documents comptables saisis constituerait un cas de force majeure n'a pas été soumis aux juges du fond et ne peut dès lors être utilement soulevé devant le juge de cassation.<br/>
<br/>
              Sur les moyens relatifs au bien-fondé de l'imposition :<br/>
<br/>
              9. En premier lieu, aux termes de l'article L. 169 du livre des procédures fiscales, dans sa rédaction issue de l'article 115 de la loi du 30 décembre 1996 de finances pour 1997 applicable aux impositions en litige : " Pour l'impôt sur le revenu et l'impôt sur les sociétés, le droit de reprise de l'administration des impôts s'exerce jusqu'à la fin de la troisième année qui suit celle au titre de laquelle l'imposition est due./ Par exception aux dispositions du premier alinéa, le droit de reprise de l'administration s'exerce jusqu'à la fin de la sixième année qui suit celle au titre de laquelle l'imposition est due, lorsque le contribuable n'a pas déposé dans le délai légal les déclarations qu'il était tenu de souscrire et n'a pas fait connaître son activité à un centre de formalités des entreprises ou au greffe du tribunal de commerce/... ".<br/>
<br/>
              10. La SARL Elce ne conteste pas que l'activité occulte qu'elle exerçait consistant en l'organisation de jeux de hasard présentait un caractère commercial. Il ressort des pièces du dossier soumis aux juges du fond que la société n'a pas déposé dans le délai légal les déclarations qu'elle était tenue de souscrire à raison des recettes générées par cette activité et qu'elle n'a pas fait connaître son activité à un centre de formalité des entreprises. Ainsi, en application du deuxième alinéa de l'article L. 169 du livre des procédures fiscales, le droit de reprise de l'administration s'exerçait jusqu'à la fin de la sixième année qui suit celle au titre de laquelle l'imposition est due. La cour n'a, dès lors, pas entaché son arrêt d'erreur de droit en jugeant que le délai spécial de reprise prévu par ces dispositions trouvait, en l'espèce, à s'appliquer et que les impositions dues au titre de l'exercice clos en 2000 n'étaient pas prescrites.<br/>
<br/>
              11. En second lieu, la cour a relevé, par une appréciation souveraine des faits non arguée de dénaturation, que les détournements de recettes opérés par MmeA..., qui dirigeait l'association par l'entremise de laquelle les loteries étaient organisées, s'opéraient au préjudice de cette association et non de la société Elce. Elle a pu en déduire, sans commettre d'erreur de droit, que la société requérante n'était pas fondée à soutenir que la somme de 130 000 francs correspondant à ces détournements ne pouvait être incluse dans le montant du bénéfice sur le fondement de l'article 38 du code général des impôts.<br/>
<br/>
              12. Il résulte de tout ce qui précède que la SARL Elce n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              Sur les conclusions de la SARL Elce présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              13. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi de la SARL Elce est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la SARL Elce et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
