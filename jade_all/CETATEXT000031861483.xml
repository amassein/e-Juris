<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031861483</ID>
<ANCIEN_ID>JG_L_2016_01_000000395650</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/86/14/CETATEXT000031861483.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 08/01/2016, 395650, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-01-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>395650</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARTHELEMY, MATUCHANSKY, VEXLIARD, POUPOT</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2016:395650.20160108</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête enregistrée le 29 décembre 2015 au secrétariat du contentieux du Conseil d'Etat, la conférence des présidents d'université demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution du décret n° 2015-1617 du 10 décembre 2015 portant modification des modalités de nomination des recteurs et du décret n° 2015-1618 du 10 décembre 2015 relatif à la composition et aux modalités de fonctionnement de la commission prévue à l'article R. 222-13 du code de l'éducation ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - la condition d'urgence est remplie, dès lors que les décrets contestés portent une atteinte grave et immédiate à l'intérêt public qui s'attache au bon fonctionnement des universités et à la sécurité juridique ;<br/>
              - il existe un doute sérieux sur la légalité des décrets contestés ;<br/>
              - ils sont intervenus à l'issue d'une procédure irrégulière en l'absence de consultation du Conseil national de l'enseignement supérieur et de la recherche et, en ce qui concerne le décret n° 2015-1618, du Conseil supérieur de l'éducation ;<br/>
              - ils portent atteinte au principe d'autonomie des universités ;<br/>
              - ils sont entachés d'une erreur manifeste d'appréciation ;<br/>
              - le décret n° 2015-1618 est illégal en raison de l'illégalité du décret n°2015-1617 ;<br/>
              - il méconnait l'article L. 335-5 du code de l'éducation ;<br/>
              - il est entaché d'incompétence négative.<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
                          Vu : <br/>
                          - le code de l'éducation ;<br/>
                          - le code de justice administrative ;<br/>
<br/>
      Après avoir convoquée à une audience publique,<br/>
<br/>
<br/>
              Vu le procès verbal de l'audience publique du<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Barthélemy, Matuchansky, Vexliard, Poupot, avocat de la Société Conference Des Presidents D'universite ;<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; que l'urgence justifie que soit prononcée la suspension d'un acte administratif lorsque l'exécution de celui-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ; qu'il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte litigieux sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue ; qu'en vertu de l'article L. 522-3 du même code, le juge des référés peut rejeter une requête par une ordonnance motivée, sans instruction contradictoire ni audience publique, lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée ;<br/>
<br/>
              2. Considérant que les décrets dont la suspension est demandée prévoient respectivement une modification des conditions de nomination des recteurs et  la composition et les modalités de fonctionnement de la commission chargée d'émettre un avis préalablement à la nomination, à ces emplois, de personnes n'étant pas titulaires d'un doctorat ; que ces dispositions sont entrées en vigueur le 1er janvier dernier ;<br/>
<br/>
              3. Considérant que pour justifier de l'urgence qui s'attache à ce que soit ordonnée la suspension demandée, la conférence des présidents d'université se borne à faire valoir les conséquences de l'illégalité des décrets contestés sur la légalité des nominations de recteurs qui seront décidées sur leur fondement ainsi que des actes pris par les intéressés et, par suite, sur le fonctionnement des universités et la sécurité juridique ; que, toutefois la circonstance qu'un acte administratif serait entaché d'illégalité ne saurait, par elle-même, suffire à caractériser une situation d'urgence ; que la requérante n'est donc pas fondée à soutenir que l'exécution de ces décrets porterait atteinte, de manière grave et immédiate, à un intérêt public, à sa situation ou aux intérêts qu'elle entend défendre ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que la condition d'urgence n'est pas remplie ; qu'il y a lieu, par suite, de rejeter la requête, y compris les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative, selon la procédure prévue par l'article L. 522-3 de ce code ;<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de la conférence des présidents d'université est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à la conférence des présidents d'université.<br/>
Copie en sera adressée au Premier ministre et à la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
