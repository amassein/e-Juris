<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000021298053</ID>
<ANCIEN_ID>JG_L_2009_11_000000308623</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/21/29/80/CETATEXT000021298053.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 16/11/2009, 308623</TITRE>
<DATE_DEC>2009-11-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>308623</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Vigouroux</PRESIDENT>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; BALAT</AVOCATS>
<RAPPORTEUR>M. Alexandre  Lallet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mlle Courrèges Anne</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 17 août et 19 novembre 2007 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SARL LES RESIDENCES DE CAVALIERE, dont le siège est Cavalière, Le Lavandou (83980), représentée par son gérant en exercice ; la SARL LES RESIDENCES DE CAVALIERE demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt du 31 mai 2007 par lequel la cour administrative d'appel de Marseille a rejeté sa requête tendant à l'annulation du jugement du tribunal administratif de Nice du 9 décembre 2003 ayant, à la demande de l'association de défense de l'environnement de Bormes et du Lavandou, annulé l'arrêté du maire de la commune du Lavandou du 14 août 2001 qui lui avait accordé un permis de construire en vue de la réalisation de 39 logements avec garages et parkings sur un terrain cadastré AR n° 111 et 112 sis Le Vallon de Cavalière ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'association de défense de l'environnement de Bormes et du Lavandou le versement de la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alexandre Lallet, Maître des Requêtes,  <br/>
<br/>
              - les observations de Me Balat, avocat de la SOCIETE LES RESIDENCES DE CAVALIERE et de la SCP Waquet, Farge, Hazan, avocat de l'association de défense de l'environnement de Bormes et du Lavandou, <br/>
<br/>
              - les conclusions de Mlle Anne Courrèges, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau à Me Balat, avocat de la SOCIETE LES RESIDENCES DE CAVALIERE et à la SCP Waquet, Farge, Hazan, avocat de l'association de défense de l'environnement de Bormes et du Lavandou ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant que, si un permis de construire ne peut être délivré que pour un projet qui respecte la réglementation d'urbanisme en vigueur, il ne constitue pas un acte d'application de cette réglementation ; que, par suite, un requérant demandant l'annulation d'un permis de construire ne saurait utilement se borner à soutenir qu'il a été délivré sous l'empire d'un document d'urbanisme illégal, quelle que soit la nature de l'illégalité dont il se prévaut, ni à demander l'annulation de ce permis par voie de conséquence de celle du document sur le fondement duquel il a été accordé ; que, cependant, il résulte de l'article L. 125-5 devenu L. 121-8 du code de l'urbanisme que l'annulation pour excès de pouvoir d'un document d'urbanisme a pour effet de remettre en vigueur le document d'urbanisme immédiatement antérieur ; que, dès lors, il peut être utilement soutenu devant le juge qu'un permis de construire a été délivré sous l'empire d'un document d'urbanisme annulé - sous réserve, en ce qui concerne les vices de forme ou de procédure, des dispositions de l'article L. 600-1 du même code -, à la condition que le requérant fasse en outre valoir que ce permis méconnaît les dispositions pertinentes ainsi remises en vigueur ;<br/>
<br/>
              Considérant qu'il résulte de ce qui vient d'être dit qu'en se fondant, pour confirmer l'annulation par le tribunal administratif de Nice du permis de construire accordé le 14 août 2001 par le maire de la commune du Lavandou à la SARL LES RESIDENCES DE CAVALIERE, sur ce que ce permis n'avait pu l'être qu'à la faveur des dispositions illégales du plan d'occupation des sols sur le fondement duquel il avait été accordé et qu'il devait donc lui-même être annulé par voie de conséquence de l'annulation de ce plan, la cour administrative d'appel a commis une erreur de droit ; que par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la SARL LES RESIDENCES DE CAVALIERE est fondée à demander l'annulation de l'arrêt de la cour administrative d'appel de Marseille du 31 mai 2007 ; <br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              Sur la recevabilité de la demande de première instance :<br/>
<br/>
              Considérant que la présidente de l'association de défense de l'environnement de Bormes et du Lavandou a été autorisée par son conseil d'administration à introduire cette demande, conformément à l'article 9 des statuts de cette association, par une délibération du 8 septembre 2001 ; qu'il ne ressort pas des pièces du dossier que ce mandat serait irrégulier au regard des dispositions de ces statuts invoquées par la SARL LES RESIDENCES DE CAVALIERE ; que la fin de non-recevoir opposée par celle-ci à la demande de première instance de l'association doit, dès lors, être écartée ;<br/>
<br/>
              Sur la légalité du permis de construire délivré le 14 août 2001 :<br/>
<br/>
              Considérant qu'il résulte de qui vient d'être dit que c'est à tort que le tribunal administratif de Nice a annulé le permis de construire litigieux par voie de conséquence de l'illégalité partielle des délibérations décidant l'application anticipée du plan d'occupation des sols de la commune du Lavandou et de la délibération du 19 septembre 2001 approuvant ce plan ; <br/>
<br/>
              Considérant toutefois qu'il y a lieu pour le Conseil d'Etat, saisi par l'effet dévolutif de l'appel, d'examiner les autres moyens soulevés par l'association de défense de l'environnement de Bormes et du Lavandou à l'appui de sa demande d'annulation ;<br/>
<br/>
              Considérant qu'en vertu des dispositions de l'article L. 146-6 du code de l'urbanisme, un permis de construire ne peut être délivré dans les sites et paysages remarquables du littoral que pour la réalisation de certains aménagements légers ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier, notamment du procès-verbal de la visite des lieux à laquelle il a été procédé, que les parcelles d'assiette du projet litigieux se situent au sein d'un espace boisé dépourvu de construction et formant avec le site collinaire environnant et le site du Layet un paysage caractéristique du patrimoine naturel varois ; que ce paysage présente un caractère remarquable au sens de l'article L. 146-6 du code de l'urbanisme ; que, par suite, le permis de construire accordé à la SARL LES RESIDENCES DE CAVALIERE pour la réalisation de 39 logements, qui ne porte pas sur des aménagements légers au sens de ces dispositions, méconnaît ces dernières et doit donc être annulé ; <br/>
<br/>
              Considérant, en revanche, que, pour l'application de l'article L. 600-4-1 du code de l'urbanisme, ne sont pas de nature à justifier l'annulation de ce permis les autres moyens soulevés par l'association de défense de l'environnement de Bormes et du Lavandou, tirés, d'une part, de ce que l'illégalité du zonage en cause du plan d'occupation des sols de la commune, entaché de détournement de pouvoir, d'erreur manifeste d'appréciation, et de méconnaissance des articles L. 121-10, L. 146-2, L. 146-4 et L. 146-6 du code de l'urbanisme, ainsi que son annulation par le jugement du tribunal administratif de Nice du 9 juillet 2003, entraîneraient par voie de conséquence l'illégalité du permis attaqué et, d'autre part, de ce que ce permis, qui serait lui-même entaché d'erreur manifeste d'appréciation, méconnaîtrait les I et II de l'article L. 146-4 et l'article R. 111-21 de ce code, ainsi que les jugements rendus par le tribunal administratif de Nice le 11 avril 1991 ; <br/>
<br/>
              Considérant qu'il résulte de ce qui précède que la SARL LES RESIDENCES DE CAVALIERE n'est pas fondée à se plaindre de l'annulation, par le jugement attaqué, du permis de construire qui lui avait été délivré le 14 août 2001 ; <br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant que ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'association de défense de l'environnement de Bormes et du Lavandou, qui n'est pas la partie perdante dans la présente instance ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la SARL LES RESIDENCES DE CAVALIERE une somme de 3 000 euros à verser au même titre à cette association ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 31 mai 2007 est annulé.<br/>
Article 2 : Le surplus des conclusions présentées par la SARL LES RESIDENCES DE CAVALIERE est rejeté.<br/>
Article 3 : La SARL LES RESIDENCES DE CAVALIERE versera à l'association de défense de l'environnement de Bormes et du Lavandou une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la SARL LES RESIDENCES DE CAVALIERE, à l'association de défense de l'environnement de Bormes et du Lavandou et à la commune du Lavandou.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-07-025 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. ANNULATION PAR VOIE DE CONSÉQUENCE. - ABSENCE - ANNULATION D'UN PERMIS DE CONSTRUIRE À LA SUITE DE L'ANNULATION DU PLAN LOCAL D'URBANISME OU DU PLAN D'OCCUPATION DES SOLS - ERREUR DE DROIT, ALORS MÊME QUE LE PERMIS N'A PU ÊTRE DÉLIVRÉ QU'À LA FAVEUR DES DISPOSITIONS ILLÉGALES DU PLAN [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-01-01-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PLANS D'AMÉNAGEMENT ET D'URBANISME. PLANS D'OCCUPATION DES SOLS ET PLANS LOCAUX D'URBANISME. LÉGALITÉ DES PLANS. - ABSENCE - ANNULATION DU PLAN - CONSÉQUENCE SUR UN PERMIS DE CONSTRUIRE ACCORDÉ SOUS SON EMPIRE - ANNULATION PAR VOIE DE CONSÉQUENCE - ABSENCE, ALORS MÊME QUE LE PERMIS N'A PU ÊTRE DÉLIVRÉ QU'À LA FAVEUR DES DISPOSITIONS ILLÉGALES DU PLAN [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">68-06-05 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. EFFETS DES ANNULATIONS. - CONSÉQUENCE SUR LA LÉGALITÉ D'UN PERMIS DE CONSTRUIRE DE L'ANNULATION DU PLAN LOCAL D'URBANISME OU DU PLAN D'OCCUPATION DES SOLS SOUS L'EMPIRE DUQUEL IL A ÉTÉ DÉLIVRÉ - ANNULATION PAR VOIE DE CONSÉQUENCE - ABSENCE, ALORS MÊME QUE LE PERMIS N'A PU ÊTRE DÉLIVRÉ QU'À LA FAVEUR DES DISPOSITIONS ILLÉGALES DU PLAN [RJ1].
</SCT>
<ANA ID="9A"> 54-07-025 Erreur de droit à annuler un permis de construire par voie de conséquence de l'annulation d'un plan local d'urbanisme alors même que ce permis n'avait pu être délivré qu'à la faveur des dispositions illégales du plan sur le fondement duquel il avait été accordé.</ANA>
<ANA ID="9B"> 68-01-01-01 Erreur de droit à annuler un permis de construire par voie de conséquence de l'annulation d'un plan local d'urbanisme alors même que ce permis n'avait pu être délivré qu'à la faveur des dispositions illégales du plan sur le fondement duquel il avait été accordé.</ANA>
<ANA ID="9C"> 68-06-05 Erreur de droit à annuler un permis de construire par voie de conséquence de l'annulation d'un plan local d'urbanisme alors même que ce permis n'avait pu être délivré qu'à la faveur des dispositions illégales du plan sur le fondement duquel il avait été accordé.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., sur les conditions d'opérance de l'exception d'illégalité du document d'urbanisme, invoquée à l'appui de la demande d'annulation du permis de construire délivré sous son empire, Section, 7 février 2008, Commune de Courbevoie, n° 297227 et autres, p. 41.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
