<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032227709</ID>
<ANCIEN_ID>JG_L_2016_03_000000391418</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/22/77/CETATEXT000032227709.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème SSR, 14/03/2016, 391418, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-03-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>391418</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Marie-Anne Lévêque</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:391418.20160314</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une requête, enregistrée le 30 juin 2015 au secrétariat du contentieux du Conseil d'Etat, M. A...B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision implicite de rejet résultant du silence gardé par le Premier ministre sur sa demande tendant à ce que soit pris le décret d'application prévu par l'article 22 de la loi n° 99-894 du 22 octobre 1999, codifié aujourd'hui à l'article L. 4251-1 du code de la défense, relatif à la mise en place d'une prime de fidélité pouvant être servie aux réservistes exerçant une activité au titre de leur engagement dans la réserve opérationnelle;<br/>
<br/>
              2°) d'enjoindre au Premier ministre de prendre ce décret et d'assortir cette injonction d'une astreinte de 500 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3.000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son article 21 ;<br/>
              - le code de la défense ;<br/>
              - la loi n° 99-894 du 22 octobre 1999 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Anne Lévêque, conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'en vertu de l'article 21 de la Constitution, le Premier ministre " assure l'exécution des lois " et, sous réserve de la compétence conférée au Président de la République pour les décrets délibérés en Conseil des ministres par l'article 13 de la Constitution, " exerce le pouvoir réglementaire " ; que l'exercice du pouvoir réglementaire comporte non seulement le droit, mais aussi l'obligation de prendre dans un délai raisonnable les mesures qu'implique nécessairement l'application de la loi, hors le cas où le respect des engagements internationaux de la France y ferait obstacle ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 22 de la loi du 22 octobre 1999 portant organisation de la réserve militaire et du service de défense, codifié aujourd'hui à l'article L. 4251-1 du code de la défense : " Les réservistes, quand ils exercent une activité au titre de leur engagement dans la réserve opérationnelle ou au titre de la disponibilité, bénéficient de la solde et des accessoires qui s'y attachent dans les mêmes conditions que les militaires professionnels. / Les réservistes exerçant une activité au titre de leur engagement dans la réserve opérationnelle peuvent, en outre, bénéficier d'une prime de fidélité ainsi que d'autres mesures d'encouragement dans les conditions fixées par décret. Le montant de la prime de fidélité est le même quel que soit le grade " ;<br/>
<br/>
              3. Considérant que ces dispositions qui n'ouvrent pas au pouvoir réglementaire une simple faculté de décider s'il y a lieu de mettre en oeuvre la prime de fidélité qu'elles prévoient, impliquent nécessairement que soit pris, pour l'application du second alinéa, un décret fixant les conditions et modalités d'attribution de la prime, dont le montant doit être " le même quel que soit le grade " ;<br/>
<br/>
              4. Considérant que la décision de refus attaquée, née du silence gardé par le Premier ministre sur la demande de M.B..., méconnaît l'obligation de prendre dans un délai raisonnable le décret d'application du second alinéa de l'article L. 4251-1 du code de la défense ; que M. B...est fondé à demander l'annulation de cette décision ;<br/>
<br/>
              5. Considérant qu'aux termes de l'article L. 911-1 du code de justice administrative : " Lorsque sa décision implique nécessairement qu'une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public prenne une mesure d'exécution dans un sens déterminé, la juridiction, saisie de conclusions en ce sens, prescrit, par la même décision, cette mesure assortie, le cas échéant, d'un délai d'exécution " ; que l'annulation de la décision du Premier ministre implique nécessairement que soit pris un décret d'application des dispositions du code de la défense citées au point 2 ci-dessus ; qu'il y a lieu, dès lors, pour le Conseil d'Etat d'enjoindre au Premier ministre de prendre ces mesures dans un délai de six mois à compter de la notification de la présente décision ; que, dans les circonstances de l'espèce, il n'y a pas lieu d'assortir cette injonction de l'astreinte demandée par M. B...;<br/>
<br/>
              6. Considérant, enfin, qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à M. B...au titre des dispositions de l'article L. 761 1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La décision implicite du Premier ministre refusant de prendre le décret nécessaire à l'application du second alinéa de l'article L. 4251-1 du code de la défense est annulée.<br/>
Article 2 : Il est enjoint au Premier ministre de prendre un tel décret, dans un délai de six mois à compter de la notification de la présente décision.<br/>
Article 3 : L'Etat versera à M. B...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. A...B...et au Premier ministre.<br/>
Copie en sera adressée pour information au ministre de la défense.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
