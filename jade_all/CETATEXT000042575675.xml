<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042575675</ID>
<ANCIEN_ID>JG_L_2020_11_000000428703</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/57/56/CETATEXT000042575675.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 27/11/2020, 428703, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-11-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428703</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Bertrand Mathieu</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:428703.20201127</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé à la Cour nationale d'asile d'annuler la décision du 14 septembre 2017 par laquelle le directeur général de l'Office français de protection des réfugiés et apatrides (OFPRA) a rejeté sa demande d'asile.<br/>
<br/>
              Par une décision n° 17040507 du 4 janvier 2019, la Cour nationale du droit d'asile a annulé cette décision et reconnu la qualité de réfugié de M. A....<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 8 mars et 10 juin 2019 au secrétariat du contentieux du Conseil d'Etat, l'Office français de protection des réfugiés et apatrides demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ; <br/>
<br/>
              2°) de renvoyer l'affaire devant la Cour nationale du droit d'asile.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention de Genève du 28 juillet 1951 et le protocole signé à New-York le 31 janvier 1967 relatifs aux réfugiés ; <br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - Le rapport de M. Bertrand Mathieu, conseiller d'Etat en service extraordinaire, <br/>
<br/>
              - Les conclusions de Mme Sophie Roussel, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Foussard, Froger, avocat de l'Office français de protection des réfugiés et apatrides, et à la SCP Piwnica, Molinié, avocat de M. A... ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1.	Il ressort des pièces du dossier que, par une décision du 14 septembre 2017, l'Office français de protection des réfugiés et apatrides a refusé de reconnaître à M. B... A..., de nationalité syrienne, la qualité de réfugié. Par une décision du <br/>
4 janvier 2019, contre laquelle l'Office français de protection des réfugiés et apatrides (OFPRA) se pourvoit en cassation, la Cour nationale du droit d'asile lui a reconnu cette qualité à raison des risques de persécution auxquels il serait exposé en cas de retour en Syrie, estimant qu'il n'y avait pas lieu de lui opposer une clause d'exclusion.<br/>
<br/>
              2.	Aux termes du 2° du A de l'article 1er de la convention de Genève du 28 juillet 1951, doit être considérée comme réfugiée toute personne qui " craignant avec raison d'être persécutée du fait de sa race, de sa religion, de sa nationalité, de son appartenance à un certain groupe social ou de ses opinions politiques, se trouve hors du pays dont elle a la nationalité et qui ne peut, ou, du fait de cette crainte, ne veut se réclamer de la protection de ce pays ".<br/>
<br/>
              3.	Aux termes du F de l'article 1er de la convention de Genève : " les dispositions de cette convention ne seront pas applicables aux personnes dont on aura des raisons sérieuses de penser (...) c) qu'elles se sont rendues coupables d'agissements contraires aux buts et aux principes des Nations unies ". Aux termes de l'article L. 711-3 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Le statut de réfugié n'est pas accordé à une personne qui relève de l'une des clauses d'exclusion prévues aux sections D, E ou F de l'article 1er de la convention de Genève du 28 juillet 1951 [...] / La même section F s'applique également aux personnes qui sont les instigatrices ou les complices des crimes ou des agissements mentionnés à ladite section ou qui y sont personnellement impliquées ". Constituent des agissements contraires aux buts et aux principes des Nations unies ceux qui sont susceptibles d'affecter la paix et la sécurité internationale, les relations pacifiques entre Etats ainsi que les violations graves des droits de l'homme. L'exclusion du statut de réfugié prévue par le c) du F de l'article 1er précité de la convention de Genève est subordonnée à l'existence de raisons sérieuses de penser qu'une part de responsabilité dans les agissements qu'il mentionne peut être imputée personnellement au demandeur d'asile. Si cette part de responsabilité ne peut être déduite de seuls éléments contextuels, elle n'implique pas que soient établis des faits précis caractérisant l'implication de l'intéressé. Il appartient en conséquence à la Cour nationale du droit d'asile de rechercher si les éléments de fait résultant de l'instruction sont de nature à fonder de sérieuses raisons de penser que le demandeur était personnellement impliqué dans de tels agissements. <br/>
<br/>
              4.	Il ressort des pièces du dossier soumis au juge du fond que M. A..., né le 15 mars 1988, de nationalité syrienne et de confession sunnite, a été mobilisé en décembre 2010 pour effectuer son service militaire, avec le grade de sous-officier adjudant, sur une base militaire située à Jdeidet Artouz Al-Balad dans la banlieue de Damas. A l'issue de son service militaire, il a été maintenu en qualité de réserviste sur cette même base. Il aurait déserté en <br/>
mai 2015 et quitté la Syrie le 22 septembre 2015. Il est arrivé en France le 5 octobre 2015. <br/>
<br/>
              5.	Pour refuser de faire application de la clause d'exclusion prévue au c) du F de l'article 1er de la convention de Genève, la Cour nationale du droit d'asile a relevé, premièrement, que la simple présence de l'intéressé en qualité de sous-officier dans une zone de conflits marquée par une forte répression ne permettait pas de considérer qu'il avait eu une responsabilité dans les agissements commis par l'armée syrienne contre la population civile, alors qu'il ne ressortait pas des pièces du dossier que, en sa qualité de conscrit puis de réserviste comme officier subalterne, il exerçait une autorité au sein de l'armée ni des fonctions d'encadrement opérationnel, deuxièmement, qu'il n'existait pas d'éléments indiquant qu'il aurait été affecté à des tâches autres qu'administratives, au service du personnel, troisièmement, qu'il n'apparaissait pas avoir appartenu à l'une des unités militaires identifiées comme ayant participé à la répression de la population de Jdeidet Artouz, enfin, que le défaut de coopération allégué par l'Office était relatif. Au vu de ces constatations souveraines, la Cour a pu, sans erreur de droit ni de qualification juridique, estimer qu'il n'existait pas de raisons sérieuses de penser que <br/>
M. A... se serait rendu coupable d'agissements contraires aux buts et aux principes des Nations unies au sens du c) du F de l'article 1er de la convention de Genève.<br/>
<br/>
              6.	Il résulte de ce qui précède que l'OFPRA n'est pas fondé à demander l'annulation de la décision de la Cour nationale du droit d'asile qu'il attaque. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Office français de protection des réfugiés et apatrides le versement à M. A... d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi de l'Office français de protection des réfugiés et apatrides est rejeté.<br/>
<br/>
Article 2 : L'Office français de protection des réfugiés et apatrides versera à M. A... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à l'Office français de protection des réfugiés et apatrides et à M. B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
