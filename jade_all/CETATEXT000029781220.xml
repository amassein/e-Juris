<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029781220</ID>
<ANCIEN_ID>JG_L_2014_11_000000367754</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/78/12/CETATEXT000029781220.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  5ème sous-section jugeant seule, 19/11/2014, 367754, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-11-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>367754</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 5ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Lionel Collet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:367754.20141119</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête sommaire et le mémoire complémentaire, enregistrés les 15 avril 2013 et 15 juillet 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B..., demeurant ... ; M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 11MA03048 du 14 février 2013 par lequel la cour administrative d'appel de Marseille, réformant le jugement n° 0903103 du tribunal administratif de Nîmes du 16 juin 2011, a fixé à 47 140 euros la somme que l'Etat a été condamné à lui verser en réparation du préjudice qu'il a subi en conséquence de l'illégalité de la décision du ministre de l'intérieur du 17 mars 2005 notifiant au secrétariat général du Pari Mutuel Urbain (PMU) un avis défavorable à sa demande d'exploitation d'un point d'enregistrement PMU à Villeneuve-lès-Avignon (Gard) ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit aux conclusions de sa requête d'appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
     	Vu le décret n° 97-456 du 5 mai 1997 ;<br/>
<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Lionel Collet, Conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. B... a acquis en 2004 un débit de boissons à Villeneuve-lès-Avignon (Gard), en vue d'y exploiter notamment un point d'enregistrement de paris ; qu'à cette fin, il a conclu le 7 mars 2005 avec le Pari mutuel urbain (PMU) un contrat stipulant notamment que tout avis négatif formulé par le ministre de l'intérieur entraînerait la résiliation immédiate du contrat ; que le ministre de l'intérieur ayant émis le 17 mars 2005 un avis négatif, le représentant local du PMU a notifié à M. B... un " retrait d'agrément " qui a entraîné la fermeture définitive du poste d'enregistrement ; que l'avis du ministre de l'intérieur a été suspendu par une ordonnance du 11 janvier 2006 du juge des référés du tribunal administratif de Nîmes, puis annulé par un jugement du  12 juin 2007 du même tribunal qui a en outre enjoint au ministre de procéder à un nouvel examen de la demande de M. B... ; qu'à la suite d'une mesure d'instruction ordonnée par le tribunal administratif saisi d'une demande d'exécution de ce jugement, le ministre de l'intérieur a indiqué le 5 juin 2008 qu'il avait émis dès le 3 mars 2006 un avis favorable, se substituant à l'avis défavorable du 17 mars 2005 ; que M. B... ayant demandé que l'Etat soit condamné à réparer le préjudice qu'il avait subi du fait de cet avis défavorable entaché d'illégalité, le tribunal administratif, par un jugement du 16 juin 2011, lui a alloué une indemnité de 40 150 euros ; que ce jugement a été réformé, sur appel de l'intéressé, par un arrêt du 14 février 2013 de la cour administrative  de Marseille qui a porté l'indemnité à 47 140 euros ;  que M. B...se pourvoit en cassation contre cet arrêt ;<br/>
<br/>
              2. Considérant que l'arrêt attaqué juge que la responsabilité de l'Etat n'est engagée à l'égard de M.B..., au titre de l'illégalité fautive entachant l'avis défavorable du 17 mars 2005, qu'au titre de la période comprise entre le 22 mars 2005, date à laquelle le contrat d'exploitation du point d'enregistrement s'est trouvé résilié, et le 6 mai 2006, " deux mois après la date à laquelle le ministre de l'intérieur a émis un avis favorable à la demande d'exploitation d'un poste d'enregistrement de paris par le requérant, cette période de deux mois correspondant au délai de notification dudit avis aux services du PMU et au temps nécessaire auxdits service pour en assurer la mise en oeuvre " ; que la cour administrative d'appel a ajouté que la circonstance, pour regrettable qu'elle fût, que l'avis favorable n'avait été porté à la connaissance de M. B... que le 5 juin 2008 était sans incidence sur la détermination de la période de responsabilité ; qu'en statuant de la sorte, alors que l'Etat n'avait avancé aucun élément permettant de tenir pour établi que l'avis favorable portant la date du 3 mars 2006, qui n'avait été porté à la connaissance du tribunal administratif et de l'intéressé que le 5 juin 2008, avait été notifié antérieurement aux services du PMU, la cour a commis une erreur de droit ; qu'elle a, au surplus, omis de répondre à l'argumentation de M. B...selon laquelle la notification de l'avis favorable au PMU n'était pas, par elle-même, de nature à faire cesser le préjudice dès lors que l'avis défavorable avait conduit le PMU à passer avec un tiers un contrat comportant une clause d'exclusivité pour l'exploitation dans le secteur d'un point d'enregistrement de paris ; <br/>
<br/>
              3. Considérant qu'il résulte de qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que M. B...est fondé à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              4. Considérant qu'il y a lieu de faire application des dispositions de l'article L. 761-1 du code de justice administrative et de mettre à la charge de l'Etat à ce titre une somme de 3 000 euros à verser à ce titre à M. B...;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de  la cour administrative d'appel de Marseille du 14 février 2013 est annulé.<br/>
<br/>
		Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
<br/>
Article 3 : L'Etat versera à M. B...une somme de 3 000 euros au titre de l'article L.761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. A...B...et au ministre de l'intérieur. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
