<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036253410</ID>
<ANCIEN_ID>JG_L_2017_12_000000401868</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/25/34/CETATEXT000036253410.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre jugeant seule, 22/12/2017, 401868, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-12-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>401868</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT, ROBILLOT ; SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>M. Frédéric Puigserver</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:401868.20171222</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Toulon d'annuler la décision du 17 novembre 2014 par laquelle le directeur de la caisse d'allocations familiales du Var a refusé de lui accorder la remise d'un indu de revenu de solidarité active " socle " d'un montant de 1 511,10 euros. Par un jugement n° 1404319 du 31 mars 2016, le tribunal administratif de Toulon a rejeté sa demande. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 26 juillet et 19 octobre 2016 au secrétariat du contentieux du Conseil d'Etat, Mme B...A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros à verser à son avocat, la SCP Potier de la Varde, Buk Lament, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code général des impôts ;<br/>
              - le code des pensions militaires d'invalidité et des victimes de guerre ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Puigserver, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Potier de la Varde, Buk Lament, Robillot, avocat de MmeA..., et à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat du département du Var.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond qu'à la suite d'un contrôle ayant révélé que Mme A...n'avait pas déclaré une pension de 116 euros qui lui était servie mensuellement en application des dispositions du code des pensions militaires d'invalidité et des victimes de guerre, la caisse d'allocations familiales du Var a décidé la récupération des sommes qu'elle estimait avoir indument versées à Mme A...au titre du revenu de solidarité active pour la période allant de décembre 2012 à février 2014. Par une décision du 17 novembre 2014, cette caisse a rejeté la demande de remise gracieuse de sa dette, s'élevant alors à 1 511,10 euros, présentée par MmeA.... Par le jugement du 31 mars 2016 contre lequel l'intéressée se pourvoit en cassation, le tribunal administratif de Toulon a rejeté la demande de Mme A...tendant à l'annulation de cette décision de refus de remise gracieuse. <br/>
<br/>
              2. D'une part, l'article L. 262-17 du code de l'action sociale et des familles dispose que : " Lors du dépôt de sa demande, l'intéressé reçoit, de la part de l'organisme auprès duquel il effectue le dépôt, une information sur les droits et devoirs des bénéficiaires du revenu de solidarité active (...) " et l'article R. 262-37 du même code que : " Le bénéficiaire de l'allocation de revenu de solidarité active est tenu de faire connaître à l'organisme chargé du service de la prestation toutes informations relatives à sa résidence, à sa situation de famille, aux activités, aux ressources et aux biens des membres du foyer ; il doit faire connaître à cet organisme tout changement intervenu dans l'un ou l'autre de ces éléments ".<br/>
<br/>
              3. D'autre part, aux termes de l'article L. 262-46 du code de l'action sociale et des familles, dans sa rédaction applicable au litige : " Tout paiement indu de revenu de solidarité active est récupéré par l'organisme chargé du service de celui-ci ainsi que, dans les conditions définies au présent article, par les collectivités débitrices du revenu de solidarité active. / (...) La créance peut être remise ou réduite par le président du conseil général ou l'organisme chargé du service du revenu de solidarité active pour le compte de l'Etat, en cas de bonne foi ou de précarité de la situation du débiteur, sauf si cette créance résulte d'une manoeuvre frauduleuse ou d'une fausse déclaration ". Il résulte de ces dispositions qu'un allocataire du revenu de solidarité active ne peut bénéficier d'une remise gracieuse de la dette résultant d'un paiement indu d'allocation, quelle que soit la précarité de sa situation, lorsque l'indu trouve sa cause dans une manoeuvre frauduleuse de sa part ou dans une fausse déclaration, laquelle doit s'entendre comme désignant les inexactitudes ou omissions qui procèdent d'une volonté de dissimulation de l'allocataire caractérisant de sa part un manquement à ses obligations déclaratives.<br/>
<br/>
              4. Lorsqu'il statue sur un recours dirigé contre une décision rejetant une demande de remise gracieuse d'un indu de revenu de solidarité active, il appartient au juge administratif d'examiner si une remise gracieuse totale ou partielle est justifiée et de se prononcer lui-même sur la demande en recherchant si, au regard des circonstances de fait dont il est justifié par l'une et l'autre parties à la date de sa propre décision, la situation de précarité du débiteur et sa bonne foi justifient que lui soit accordée une remise. Lorsque l'indu résulte de ce que l'allocataire a omis de déclarer certaines de ses ressources, il y a lieu, pour apprécier la condition de bonne foi de l'intéressé, hors les hypothèses où les omissions déclaratives révèlent une volonté manifeste de dissimulation ou, à l'inverse, portent sur des ressources dépourvues d'incidence sur le droit de l'intéressé au revenu de solidarité active ou sur son montant, de tenir compte de la nature des ressources ainsi omises, de l'information reçue et de la présentation du formulaire de déclaration des ressources, du caractère réitéré ou non de l'omission, des justifications données par l'intéressé ainsi que de toute autre circonstance de nature à établir que l'allocataire pouvait de bonne foi ignorer qu'il était tenu de déclarer les ressources omises. A cet égard, si l'allocataire a pu légitimement, notamment eu égard à la nature du revenu en cause et de l'information reçue, ignorer qu'il était tenu de déclarer les ressources omises, la réitération de l'omission ne saurait alors suffire à caractériser une fausse déclaration.<br/>
<br/>
              5. Pour rejeter le recours de Mme A...contre le refus de remise gracieuse de l'indu dont le remboursement lui était demandé, le tribunal, après avoir constaté que l'intéressée n'avait pas déclaré, alors qu'elle devait l'être, la perception d'une pension de guerre, s'est fondé sur la seule circonstance que cette omission avait eu un caractère réitéré et n'avait été révélée qu'à la faveur d'un contrôle pour en déduire que la bonne foi de l'intéressée ne pouvait être regardée comme établie. Eu égard notamment à la nature des ressources en cause, qui, en vertu du a du 4° de l'article 81 du code général des impôts, ne constituent pas des revenus imposables, le tribunal a commis une erreur de droit en déduisant du seul caractère réitéré de l'omission ainsi commise, jusqu'au contrôle qui l'a révélée, que la requérante n'avait pu de bonne foi ignorer son obligation de déclarer ces ressources. <br/>
<br/>
              6. Par suite, Mme A...est fondée à demander l'annulation du jugement du tribunal administratif de Toulon qu'elle attaque. Le moyen retenu suffisant à entraîner cette annulation, il n'est pas nécessaire d'examiner les autres moyens de son pourvoi.<br/>
<br/>
              7. Il résulte des dispositions de l'article L. 262-46 du code de l'action sociale et des familles citées ci-dessus que les décisions de récupération d'indu prises par les caisses d'allocations familiales le sont au nom du département lorsque cette collectivité en a été débitrice. En l'espèce, il ressort des pièces du dossier soumis au juge du fond que l'indu dont le remboursement est demandé à Mme A...porte sur la part de l'allocation de revenu de solidarité active financée par les départements en application de l'article L. 262-24 du même code, dans sa rédaction applicable au litige. Par suite, les conclusions tendant à ce qu'une somme soit mise à la charge de l'Etat sur le fondement des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 sont mal dirigées et ne peuvent qu'être rejetées.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Toulon du 31 mars 2016 est annulé. <br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Toulon. <br/>
Article 3 : Les conclusions de Mme A...présentées au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 sont rejetées. <br/>
Article 4 : La présente décision sera notifiée à Mme B...A...et au département du Var.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
