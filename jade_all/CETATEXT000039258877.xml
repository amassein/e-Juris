<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039258877</ID>
<ANCIEN_ID>JG_L_2019_10_000000427995</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/25/88/CETATEXT000039258877.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 17/10/2019, 427995, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-10-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>427995</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MARLANGE, DE LA BURGADE</AVOCATS>
<RAPPORTEUR>M. Laurent Domingo</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:427995.20191017</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... D... a demandé au tribunal administratif de Toulouse de prononcer la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles il a été assujetti au titre de l'année 2010, des rappels de taxe sur la valeur ajoutée mis à sa charge au titre de la période correspondant à cette année ainsi que des pénalités correspondantes. Par un jugement n°s 1303124, 1303125, 1303126 du 12 juillet 2016, ce tribunal a rejeté ses demandes.<br/>
<br/>
              Par un arrêt n° 16BX03144 du 20 décembre 2018, la cour administrative d'appel de Bordeaux. après avoir prononcé un non-lieu à statuer à concurrence d'un dégrèvement intervenu en cours d'instance a rejeté le surplus de la requête de ce dernier. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 14 février et 7 mai 2019 au secrétariat du contentieux du Conseil d'Etat, M. D... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'article 2 de cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond dans cette mesure, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Domingo, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Marlange, de la Burgade, avocat de M. A... D... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'à l'issue d'une vérification de comptabilité de la société d'exercice libéral par actions simplifiée (SELAS) Wilson, l'administration a procédé à un contrôle sur pièces de la situation personnelle de M. A... D..., son dirigeant. Elle a regardé la somme perçue par l'intéressé en application d'un " protocole d'accord transactionnel " signé le 11 août 2010 entre, d'une part, la société Wilson et lui-même et, d'autre part, M. B... C..., la société Plus Pharmacie et la société Phoenix Pharma France, comme un produit provenant de l'exploitation d'une marque commerciale entrant dans le champ de la taxe sur la valeur ajoutée et taxable à l'impôt sur le revenu. M. A... D... se pourvoit en cassation contre l'article 2 de l'arrêt du 20 décembre 2018 par lequel la cour administrative de Bordeaux a rejeté l'appel qu'il avait formé contre le jugement du 12 juillet 2016 du tribunal administratif de Toulouse rejetant sa demande en décharge des cotisations supplémentaires d'impôt sur le revenu auxquelles il a été, en conséquence, assujetti au titre de l'année 2010, des rappels de taxe sur la valeur ajoutée mis à sa charge au titre de la période correspondant à cette année et des pénalités correspondantes.<br/>
<br/>
              2. Aux termes du premier alinéa de l'article R. 613-1 du code de justice administrative : " Le président de la formation de jugement peut, par une ordonnance, fixer la date à partir de laquelle l'instruction sera close. (...) ". Selon le premier alinéa de l'article R. 613-2 du même code : " Si le président de la formation de jugement n'a pas pris une ordonnance de clôture, l'instruction est close trois jours francs avant la date de l'audience indiquée dans l'avis d'audience prévu à l'article R.711-2. (...) ". Aux termes du premier alinéa de l'article R. 613-3 de ce code : " Les mémoires produits après la clôture de l'instruction ne donnent pas lieu à communication, sauf réouverture de l'instruction. ". Il résulte de ces dispositions que lorsque, postérieurement à la clôture de l'instruction, le juge est saisi d'un mémoire émanant de l'une des parties à l'instance, et conformément au principe selon lequel, devant les juridictions administratives, le juge dirige l'instruction, il lui appartient, dans tous les cas, de prendre connaissance de ce mémoire avant de rendre sa décision, ainsi que de le viser sans l'analyser.<br/>
<br/>
              3. Il ressort des pièces du dossier de la cour administrative d'appel de Bordeaux que M. D... a produit un mémoire le 19 novembre 2018, alors que l'instruction avait été clôturée le 3 octobre 2017 en vertu d'une ordonnance du 10 août 2017 du président de la 3ème chambre de cette cour et que l'audience publique s'est tenue le 29 novembre 2018. L'arrêt attaqué, dont les visas ne font pas mention de ce mémoire produit après la clôture de l'instruction, est ainsi entaché d'irrégularité. Par suite, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, M. D... est fondé à demander l'annulation de son article 2.<br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à M. D... au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
 Article 1er : L'article 2 de l'arrêt de la cour administrative d'appel de Bordeaux est annulé.<br/>
Article 2 : L'affaire est renvoyée dans la limite de la cassation prononcée à la cour administrative d'appel de Bordeaux.<br/>
<br/>
Article 3 : L'Etat versera une somme de 3 000 euros à M. D... au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. D... et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
