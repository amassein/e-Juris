<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035755960</ID>
<ANCIEN_ID>JG_L_2017_10_000000401021</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/75/59/CETATEXT000035755960.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 1ère chambres réunies, 09/10/2017, 401021</TITRE>
<DATE_DEC>2017-10-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>401021</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE CHAISEMARTIN, COURJON</AVOCATS>
<RAPPORTEUR>M. Bruno Chavanat</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:401021.20171009</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              1° Sous le n° 401021, par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 28 juin et 21 septembre 2016 et le 23 mai 2017 au secrétariat du contentieux du Conseil d'Etat, la Fédération nationale des chasseurs demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la note du 24 novembre 2015 de la commission nationale d'indemnisation des dégâts de gibier intitulée : " Dégâts sur semis : mise en oeuvre des seuils ouvrant droit à indemnisation " ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Sous le n° 401026, par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 28 juin et 21 septembre 2016 et le 23 mai 2017 au secrétariat du contentieux du Conseil d'Etat, la Fédération nationale des chasseurs demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la note du 24 novembre 2015 de la Commission nationale d'indemnisation des dégâts de gibier intitulée " Dégâts sur les prairies : mise en oeuvre des seuils ouvrant droit à indemnisation (dossiers ouverts entre le 1er janvier 2014 et le 6 février 2016) " ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
			....................................................................................<br/>
<br/>
              3° Sous le n° 401033, par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 28 juin et 21 septembre 2016 et le 23 mai 2017 au secrétariat du contentieux du Conseil d'Etat, la Fédération nationale des chasseurs demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la note diffusée le 3 juin 2016 par la Commission nationale d'indemnisation des dégâts de gibier intitulée " Fiche conseil relative aux déclarations abusives ou semi-abusives " ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
			....................................................................................<br/>
<br/>
              4° Sous le n° 401044, par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 28 juin et 21 septembre 2016 et le 23 mai 2017 au secrétariat du contentieux du Conseil d'Etat, la Fédération nationale des chasseurs demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la note de la Commission nationale d'indemnisation des dégâts de gibier intitulée " Dégâts sur les prairies : mise en oeuvre des seuils ouvrant droit à indemnisation. Dossiers ouverts à partir du 7 février 2016 ", diffusée le 3 juin 2016 ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
	Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code de l'environnement ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Chavanat, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP de Chaisemartin, Courjon, avocat de la Fédération nationale des chasseurs ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 18 septembre 2017, présentée par la Commission nationale d'indemnisation des dégâts de gibier sous les nos 401021, 401026 et 401044 ; <br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que les requêtes visées ci-dessus présentent à juger des questions semblables ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier que la Commission nationale d'indemnisation des dégâts de gibier (CNIDG) a adressé aux préfectures, le 3 juin 2016, quatre notes destinées à informer les commissions départementales compétentes en matière de chasse et de faune sauvage, mentionnées à l'article L. 426-5 du code de l'environnement, des modalités de mise en oeuvre de certaines dispositions de ce code relatives à la procédure non contentieuse d'indemnisation des dégâts causés par le grand gibier aux cultures et aux récoltes agricoles ; que la Fédération nationale des chasseurs demande l'annulation pour excès de pouvoir de ces notes ; <br/>
<br/>
              Sur la compétence du Conseil d'Etat :<br/>
<br/>
              3. Considérant qu'aux termes de l'article R. 311-1 du code de justice administrative : " Le Conseil d'État est compétent pour connaître en premier et dernier ressort : / (...) 2° Des recours dirigés contre les actes réglementaires des ministres et des autres autorités à compétence nationale et contre leurs circulaires et instructions de portée générale " ; <br/>
<br/>
              4. Considérant qu'au nombre des missions qui lui sont confiées par les dispositions du premier alinéa de l'article L. 426-5 du code de l'environnement, la CNIDG " fixe chaque année, pour les principales denrées, les valeurs minimale et maximale des prix à prendre en compte pour l'établissement des barèmes départementaux " arrêtés par les commissions départementales compétentes en matière de chasse et de faune sauvage pour l'indemnisation des dégâts mentionnés à l'article L. 423-1 causés par le grand gibier aux cultures et aux récoltes agricoles ; que les mêmes dispositions prévoient qu'elle " fixe également, chaque année, aux mêmes fins, les valeurs minimale et maximale des frais de remise en état " ; qu'eu égard au pouvoir réglementaire qui lui est ainsi dévolu, la CNIDG est au nombre des autorités à compétence nationale mentionnées au 2° de l'article R. 311-1 du code de justice administrative ; qu'il en résulte que les requêtes présentées par la Fédération nationale des chasseurs ressortissent à la compétence du Conseil d'État en premier et dernier ressort ;<br/>
<br/>
              Sur les fins de non recevoir opposées par la CNIDG :<br/>
<br/>
              5. Considérant, en premier lieu, que l'interprétation que, par voie de circulaires ou d'instructions, l'autorité administrative donne des lois et règlements qu'elle a pour mission de mettre en oeuvre n'est pas susceptible d'être déférée au juge de l'excès de pouvoir lorsque, étant dénuée de caractère impératif, elle ne saurait, quel qu'en soit le bien-fondé, faire grief ; qu'en revanche, les dispositions impératives à caractère général d'une circulaire ou d'une instruction doivent être regardées comme faisant grief ; que les trois notes dont l'annulation pour excès de pouvoir est demandée par la Fédération nationale des chasseurs sous les nos 401021, 401026 et 401044, relatives à la mise en oeuvre des seuils ouvrant droit à indemnisation pour les dégâts sur prairies et sur semis, comportent des dispositions impératives à caractère général énonçant les cas où l'indemnisation est due ; qu'il en est de même pour la note " relative aux déclarations abusives ou semi-abusives ", s'agissant de la procédure à suivre pour le règlement par le réclamant des frais d'estimation des dommages qui lui incombent, dont l'annulation pour excès de pouvoir est demandée par la Fédération nationale des chasseurs sous le no 401033 ; que les actes attaqués sont, par suite, susceptibles de faire l'objet d'un recours pour excès de pouvoir ;<br/>
<br/>
              6. Considérant, en second lieu, qu'en vertu de l'article L. 421-14 du code de l'environnement, la Fédération nationale des chasseurs assure la représentation des fédérations départementales, interdépartementales et régionales des chasseurs à l'échelon national et gère le Fonds cynégétique national assurant notamment l'indemnisation des dégâts de grand gibier par les fédérations départementales des chasseurs ; qu'elle dispose ainsi d'un intérêt lui donnant qualité pour agir contre les notes attaquées, relatives aux conditions d'indemnisation de ces dégâts ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que les fins de non recevoir opposées par la CNIDG doivent être écartées ; <br/>
<br/>
              Sur les conclusions à fin d'annulation :<br/>
<br/>
              En ce qui concerne les notes attaquées sous les nos 401021, 401026, 401044 : <br/>
<br/>
              8. Considérant qu'aux termes de l'article L. 426-1 du code de l'environnement : " En cas de dégâts causés aux cultures, aux inter-bandes des cultures pérennes, aux filets de récoltes agricoles ou aux récoltes agricoles soit par les sangliers, soit par les autres espèces de grand gibier soumises à plan de chasse, l'exploitant qui a subi un dommage nécessitant une remise en état, une remise en place des filets de récolte ou entraînant un préjudice de perte de récolte peut réclamer une indemnisation sur la base de barèmes départementaux à la fédération départementale ou interdépartementale des chasseurs " ; que l'article L. 426-3 du même code précise que : " L'indemnisation mentionnée à l'article L. 426-1 pour une parcelle culturale n'est due que lorsque les dégâts sont supérieurs à un seuil minimal fixé par décret en Conseil d'Etat ", un seuil spécifique pouvant être fixé pour une parcelle culturale de prairie ; que le premier alinéa de l'article R. 426-11 du code de l'environnement prévoit que : " Le seuil minimal donnant lieu à indemnisation prévu à l'article L. 426-3 est fixé à 3 % de la surface ou du nombre de plants de la parcelle culturale détruite. Toutefois, les dégâts sont indemnisés lorsque leur montant, avant l'abattement défini au deuxième alinéa du même article, y est supérieur à 230 &#128;. Dans le cas particulier des prairies, ce seuil est ramené à 100 &#128; " ;<br/>
<br/>
              9. Considérant, en premier lieu, que la prise en compte des travaux de remise en état, au nombre desquels figurent les ressemis, par le régime d'indemnisation des dégâts causés par le grand gibier résulte de la combinaison des dispositions de l'article L. 426-1 du code de l'environnement, qui les inclut au nombre des dégâts indemnisables, et de l'article L. 426-3 du même code, qui prévoit un seuil minimal de dégâts indemnisables ; que les notes attaquées détaillent les modalités de prise en compte des travaux de remise en état ou de ressemis pour l'appréciation des seuils de surface et de montant de dégâts fixés par l'article R. 426-11 du code de l'environnement ; que, par suite, elles ne fixent pas des règles nouvelles entachées d'incompétence ; <br/>
<br/>
              10. Considérant, en deuxième lieu, que les notes attaquées indiquent que, pour l'application de l'article R. 426-11 précité : " L'indemnisation est due : / si la surface détruite et concernée par la perte de récolte est supérieure à 3 % de la surface de la parcelle culturale / ou : / si la surface concernée par les travaux de remise en état (...) est supérieure à 3 % de la surface de la parcelle culturale / ou : / si la surface endommagée (entendue comme la surface concernée par la perte de récolte ou par les travaux de remise en état) est supérieure à 3 % de la surface de la parcelle culturale (...) " ; qu'elles précisent, en outre, pour les dégâts sur semis : " Il faut entendre par surface concernée par les travaux de ressemis la surface exacte sans correction qui est ressemée par l'agriculteur, base de calcul pour l'indemnisation "  et, pour les dégâts sur prairies  : " Il faut entendre par 'surface concernée par les travaux de remise en état' la surface exacte sans correction qui est travaillée par l'agriculteur, base de calcul pour l'indemnisation " ; que les notes ne méconnaissent ainsi ni le sens ni la portée des dispositions législatives et réglementaires citées au point 8 en ce qu'elles indiquent que le montant des dégâts à prendre en compte pour la fixation des indemnités résulte du cumul des pertes de récolte et des travaux de remise en état rendus nécessaires par les destructions, ces travaux devant, le cas échéant, être réalisés sur une surface excédant celle initialement détruite ; que, toutefois, en précisant que, pour calculer la surface à prendre en compte pour atteindre le seuil de 3% de la parcelle culturale détruite au-delà duquel les dégâts peuvent être indemnisés, il convient de se référer non seulement à la surface de parcelle initialement détruite mais aussi à la surface supplémentaire que l'exploitant agricole a dû éventuellement travailler pour accomplir les travaux de remise en état ou de ressemis, les notes ont méconnu l'article R. 426-11, qui se réfère à la surface ou au nombre de plants de la parcelle culturale détruite ; que les notes attaquées doivent, en ce qu'elles apportent cette précision, être annulées ;  <br/>
<br/>
              11. Considérant, en dernier lieu, qu'en prévoyant que " les dossiers d'indemnisation qui (...) n'auraient pas été examinés suivant les dispositions prévues à l'article R. 426-11 du code de l'environnement dans les conditions définies ci-avant doivent être révisés ", les notes contestées ont entendu rappeler que les dossiers sur lesquels les fédérations départementales des chasseurs ou, en cas de désaccord avec le réclamant, les commissions départementales, n'ont pas encore statué doivent être examinés conformément aux dispositions qu'elles contiennent ; que les mentions précitées ne sont, par suite, ni entachées d'une rétroactivité illégale, ni contraires au principe de sécurité juridique ;  <br/>
<br/>
              En ce qui concerne la note attaquée sous le n° 401033 : <br/>
<br/>
              12. Considérant qu'aux termes du premier alinéa de l'article L. 426-3 du code de l'environnement : " (...) S'il est établi que les dégâts constatés n'atteignent pas ces seuils, les frais d'estimation des dommages sont à la charge financière du réclamant " ; que le quatrième alinéa du même article dispose que : " Dans le cas où les quantités déclarées détruites par l'exploitant sont excessives par rapport à la réalité des dommages, tout ou partie des frais d'estimation sont à la charge financière du réclamant " ; que le cinquième alinéa de l'article R. 426-11 du même code précise que : " En application du quatrième alinéa de l'article L. 426-3, les frais d'estimation sont intégralement à la charge du réclamant lorsque les quantités déclarées détruites sont plus de 10 fois supérieures aux dommages réels et pour moitié lorsque cette surévaluation atteint 5 à 10 fois " ; que le dernier alinéa de ce même article prévoit que : " Dans le cas où le réclamant est redevable auprès de la fédération départementale ou interdépartementale des chasseurs de tout ou partie des frais d'estimation des dommages, celle-ci lui adresse la facture correspondante. A défaut de son paiement dans un délai de soixante jours après sa date d'émission, la fédération départementale ou interdépartementale peut en imputer le montant sur l'indemnisation due " ;<br/>
<br/>
              13. Considérant qu'il résulte de ces dispositions que les frais d'estimation des dommages mis à la charge du réclamant constituent une obligation financière à l'égard de la fédération départementale ou interdépartementale des chasseurs, susceptible d'être imputée sur les indemnisations par ailleurs dues au réclamant ; que la note précise que " si les frais d'estimation des dommages sont supérieurs à l'indemnisation proposée, abattement légal et réduction supplémentaire compris, le solde peut être reporté sur les indemnisations de dossiers de la même année culturale " ; qu'en limitant la possibilité d'une compensation à la même année culturale, la CNIDG a, dans le silence des textes, fixé une règle nouvelle excédant sa compétence ; que la note doit, dans cette mesure, être annulée ; <br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              14. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme globale de 4 000 euros à verser à la Fédération nationale des chasseurs, au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les note attaquées sous les nos 401021, 401026 et 401044 sont annulées en tant qu'elles définissent l'assiette prise en compte pour le calcul du seuil de surface de 3% prévu par l'article R. 426-11 du code de l'environnement en y intégrant les surfaces non initialement détruites mais utilisées pour les travaux de ressemis ou de remise en état. <br/>
Article 2 : La note attaquée sous le n° 401033 est annulée en tant qu'elle comporte le paragraphe suivant : " Si les frais d'estimation des dommages sont supérieurs à l'indemnisation proposée, abattement légal et réduction supplémentaire compris, le solde peut être reporté sur les indemnisations de dossiers de la même année culturale ".<br/>
<br/>
Article 3 : L'Etat versera à la Fédération nationale des chasseurs une somme globale de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
		Article 4 : Le surplus des conclusions des requêtes est rejeté.<br/>
Article 5 : La présente décision sera notifiée à la Fédération nationale des chasseurs, à la Commission nationale d'indemnisation des dégâts de gibier et au ministre d'Etat, ministre de la transition écologique et solidaire.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-05-02-04 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE DU CONSEIL D'ETAT EN PREMIER ET DERNIER RESSORT. ACTES RÉGLEMENTAIRES DES MINISTRES. - RECOURS DIRIGÉS CONTRE LES ACTES RÉGLEMENTAIRES DES AUTORITÉS À COMPÉTENCE NATIONALE ET CONTRE LEURS CIRCULAIRES ET INSTRUCTIONS DE PORTÉE GÉNÉRALE (2° DE L'ART. R. 311-1 DU CJA) - APPLICATION AUX SEULS ÉTABLISSEMENTS PUBLICS NATIONAUX DOTÉS D'UN POUVOIR RÉGLEMENTAIRE PAR UN TEXTE [RJ1] - CNIDG - INCLUSION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">44-046 NATURE ET ENVIRONNEMENT. - RECOURS CONTRE DES NOTES DE LA CNIDG RELATIVES À LA PROCÉDURE NON CONTENTIEUSE D'INDEMNISATION DES DÉGÂTS CAUSÉS PAR LE GRAND GIBIER AUX CULTURES ET AUX RÉCOLTES AGRICOLES - COMPÉTENCE EN PREMIER ET DERNIER RESSORT DU CONSEIL D'ETAT - INCLUSION [RJ1].
</SCT>
<ANA ID="9A"> 17-05-02-04 Eu égard au pouvoir réglementaire qui lui est dévolu par le premier alinéa de l'article L. 426-5 du code de l'environnement, la commission nationale d'indemnisation des dégâts de gibier (CNIDG) est au nombre des autorités à compétence nationale mentionnées au 2° de l'article R. 311-1 du code de justice administrative (CJA). Par suite, compétence en premier et dernier ressort du Conseil d'Etat pour connaître de recours pour excès de pouvoir contre des notes de la CNIDG destinées à informer les commissions départementales compétentes en matière de chasse et de faune sauvage des modalités de mise en oeuvre de certaines dispositions du code de l'environnement relatives à la procédure non contentieuse d'indemnisation des dégâts causés par le grand gibier aux cultures et aux récoltes agricoles.</ANA>
<ANA ID="9B"> 44-046 Eu égard au pouvoir réglementaire qui lui est dévolu par le premier alinéa de l'article L. 426-5 du code de l'environnement, la commission nationale d'indemnisation des dégâts de gibier (CNIDG) est au nombre des autorités à compétence nationale mentionnées au 2° de l'article R. 311-1 du code de justice administrative (CJA). Par suite, compétence en premier et dernier ressort du Conseil d'Etat pour connaître de recours pour excès de pouvoir contre des notes de la CNIDG destinées à informer les commissions départementales compétentes en matière de chasse et de faune sauvage des modalités de mise en oeuvre de certaines dispositions du code de l'environnement relatives à la procédure non contentieuse d'indemnisation des dégâts causés par le grand gibier aux cultures et aux récoltes agricoles.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 26 juillet 2011, Syndicat SNUTEFI-FSU et autres (SNUTEFI), n° 346771, p. 421.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
