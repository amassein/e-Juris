<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039379795</ID>
<ANCIEN_ID>JG_L_2019_10_000000435432</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/37/97/CETATEXT000039379795.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 31/10/2019, 435432, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-10-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>435432</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; SCP DELVOLVE ET TRICHET</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2019:435432.20191031</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              L'Union des syndicats CGT du Tarn-et-Garonne (UD CGT 82) a demandé au juge des référés du tribunal administratif de Toulouse, statuant sur le fondement de l'article L. 521-4 du code de justice administrative, à titre principal, d'enjoindre à la commune de Montauban de lui remettre l'ensemble des clés d'accès à ses locaux et à la salle Sellier situés dans la " Maison du peuple " afin de lui en assurer l'accès permanent ou, à titre subsidiaire, d'accuser réception des demandes de réservation de la salle Sellier adressées par l'UD CGT 82, de répondre en temps utile aux demandes de l'UD CGT 82 de réservation de la salle Sellier, d'assurer l'accès de la salle Sellier par l'UD CGT 82 à sa demande selon ses besoins planifiés ou urgents et d'assortir ces injonctions de 1 000 euros par jour de retard à compter de la notification de l'ordonnance. Par une ordonnance n° 1905240 du 3 octobre 2019, le juge des référés du tribunal administratif de Toulouse a enjoint à la commune de Montauban, d'une part, de réserver la salle Sellier à l'usage exclusif de l'UD CGT 82 le jeudi et de la laisser ouverte ce jour-là de 8h00 à 23h00, et d'autre part, de s'assurer de l'ouverture des locaux pour que l'UD CGT 82 puisse disposer de ses bureaux à partir de 8h00 et jusqu'à 19h00 tous les jours de la semaine, sous astreinte de 1 000 euros par jour de retard à compter de la notification de l'ordonnance.  <br/>
<br/>
              Par une requête, enregistrée 18 octobre 2019 au secrétariat du contentieux du Conseil d'Etat, la commune de Montauban demande au juge des référés du Conseil d'Etat, statuant sur le fondement des articles L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de rejeter les conclusions de première instance de l'UD CGT 82 ;<br/>
<br/>
              3°) de mettre à la charge de l'UD CGT 82 la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - son recours fondé sur l'article L. 521-2 du code de justice administrative est recevable dès lors que les ordonnances prises sur le fondement de l'article L. 521-4 du code de justice administrative sont de même nature que les ordonnances prises initialement par le juge des référés et sont susceptibles de faire l'objet des mêmes voies de recours ;<br/>
              - le juge des référés a méconnu son office en lui enjoignant de prendre des mesures qui ne découlaient pas de son ordonnance initiale du 28 mai 2019 prise sur le fondement de l'article L. 521-2 du code de justice administrative ;<br/>
              - après avoir annulé l'ordonnance en raison de son irrégularité, le Conseil d'Etat, dans l'exercice de son pouvoir d'évocation, rejettera la demande de l'UD CGT 82 car les précédentes ordonnances ont été correctement exécutées et il n'a été porté aucune atteinte grave et manifestement illégale à la liberté syndicale, dès lors que la mise à disposition de locaux au bénéfice des syndicats constitue une simple faculté pour une commune et non une obligation.<br/>
<br/>
              Par un mémoire en défense, enregistré le 22 octobre 2019, l'Union départementale des syndicats CGT du Tarn-et-Garonne conclut au rejet de la requête. Elle soutient, d'une part, que le juge des référés n'a pas méconnu son office, d'autre part, que la commune n'a pas accompli les diligences nécessaires pour exécuter intégralement les injonctions ordonnées par le juge des référés et demande à ce que soit mise à la charge de la commune de Montauban la somme de 2 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, la commune de Montauban et, d'autre part, l'Union départementale des syndicats CGT du Tarn-et-Garonne ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 25 octobre 2019 à 15 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Delvolve, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la commune de Montauban ;<br/>
<br/>
              - Me Pinet, avocat au Conseil d'Etat et à la Cour de cassation, avocat de l'Union départementale des syndicats CGT du département du Tarn-et-Garonne ;<br/>
             et à l'issue de laquelle le juge des référés a différé la clôture de l'instruction au 29 octobre à 13 heures ;<br/>
<br/>
<br/>
             Vu le nouveau mémoire, enregistré le 28 octobre 2019, par lequel l'Union départementale des syndicats CGT du Tarn-et-Garonne maintient ses conclusions et ses moyens ;<br/>
<br/>
             Vu le nouveau mémoire, enregistré le 28 octobre 2019, par lequel la commune de Montauban maintient ses conclusions et ses moyens ;<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
Considérant ce qui suit :<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public (...) aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale (...) ". Aux termes de l'article L. 521-4 du même code : " Saisi par toute personne intéressée, le juge des référés peut, à tout moment, au vu d'un élément nouveau, modifier les mesures qu'il avait ordonnées ou y mettre fin. " Si l'exécution d'une ordonnance prise par le juge des référés, sur le fondement de l'article L. 521-2 du code de justice administrative, peut être recherchée dans les conditions définies par le livre IX du même code, et en particulier les articles L. 911-4 et L. 911-5, la personne intéressée peut également demander au juge des référés, sur le fondement de l'article L. 521-4 du même code, d'assurer l'exécution des mesures ordonnées demeurées sans effet par de nouvelles injonctions et une astreinte.<br/>
<br/>
              2. La commune de Montauban est propriétaire d'un immeuble sis 18 rue Michelet, dénommé " Maison du peuple ", mis partiellement à la disposition de diverses organisations syndicales, dont la Confédération générale du travail (CGT), depuis 1945. A la suite de l'occupation irrégulière du rez-de-chaussée du bâtiment par des demandeurs d'asile entre le 5 et le 16 mai 2019, la municipalité a changé les serrures, empêchant ainsi les occupants d'accéder aux locaux. L'Union départementale des syndicats CGT du Tarn-et-Garonne (UD CGT 82) a saisi le juge des référés du tribunal administratif de Toulouse, le 23 mai 2019, sur le fondement de l'article L. 521-2 du code de justice administrative, d'une demande tendant à ce qu'il soit enjoint sous astreinte à la commune de Montauban de rétablir le libre accès des locaux de l'immeuble aux syndicalistes et usagers de la CGT et de mettre à leur disposition les moyens techniques nécessaires à leur fonctionnement. Par une ordonnance en date du 28 mai 2019, le juge des référés a enjoint à la commune de Montauban de réexaminer, dans un délai d'un mois, les conditions de relogement auxquels l'UD CGT 82 pouvait prétendre et, dans l'attente de ce réexamen, de rétablir l'Union départementale dans les droits dont elle bénéficiait antérieurement en lui restituant le local dont elle disposait jusqu'alors et l'ensemble des biens et documents qui s'y trouvaient, sous astreinte de 200 euros par jour de retard. Dès le lendemain, le 29 mai 2019, l'UD CGT 82 a saisi le juge des référés du tribunal administratif de Toulouse, sur le fondement de l'article L. 521-4, d'une demande tendant à ce qu'il modifie et précise le dispositif de l'ordonnance du 28 mai 2019, en enjoignant à la commune de lui remettre sans délai les clés permettant l'accès libre à la " Maison du peuple " et à ses locaux ainsi qu'à la salle de réunion Sellier, située au 1er étage du bâtiment. Par une ordonnance rendue le 19 juin 2019, le juge des référés, après avoir estimé que l'exécution de l'ordonnance du 28 mai 2019 impliquait une amplitude horaire d'ouverture des locaux allant au moins de 8h00 à 19h00, avec une possibilité pour l'UD CGT 82 de réserver de façon prioritaire la salle Sellier le jeudi sur un horaire plus tardif ne pouvant être limité en deçà de 23h00, a jugé que la commune n'avait pas pleinement exécuté cette ordonnance et porté l'astreinte à 1 000 euros par jour de retard. Le conseil municipal a alors adopté, le 26 juin 2019, une délibération portant modification de l'affectation de l'immeuble dit " Maison du peuple " à compter du 1er septembre 2019 et le maire de Montauban a pris la décision, le 5 juillet 2019, d'enjoindre à l'UD CGT 82 de quitter les lieux. Toutefois, à la demande de celle-ci, le juge des référés du tribunal administratif de Toulouse, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, a suspendu l'exécution de la délibération du 26 juin par une ordonnance en date du 29 juillet 2019, puis celle de la décision du 5 juillet par une ordonnance du 30 août 2019. Enfin, le 12 septembre 2019, l'UD CGT 82 a saisi le juge des référés du tribunal administratif de Toulouse, sur le fondement de l'article L. 521-4 du code de justice administrative, d'une demande tendant, à titre principal, à ce qu'il soit enjoint à la commune de Montauban de lui remettre l'ensemble des clés d'accès aux locaux et à la salle Sellier et, à titre subsidiaire, à ce qu'il lui soit enjoint d'accuser réception des demandes de réservation de la salle Sellier adressées par l'UD CGT 82, de répondre en temps utile à ses demandes de réservation de la salle Sellier, de lui assurer l'accès de la salle Sellier le jeudi de 8h00 à 23h00 et d'assurer l'accès à ses locaux et à la salle Sellier par l'UD CGT 82 à sa demande selon ses besoin planifiés ou urgents. Par une ordonnance du 3 octobre 2019, dont la commune de Montauban relève appel, le juge des référés a enjoint à celle-ci, d'une part, de réserver la salle Sellier à l'usage exclusif de l'UD CGT 82 le jeudi et de la laisser ouverte ce jour-là de 8h00 à 23h00, et, d'autre part, de s'assurer de l'ouverture des locaux pour que l'UD CGT 82 puisse disposer de ses bureaux à partir de 8h00 et jusqu'à 19h00 tous les jours de la semaine.<br/>
<br/>
              3. Ainsi qu'il a été dit au point 2, le juge des référés du tribunal administratif de Toulouse, saisi une première fois le 29 mai 2019 sur le fondement de l'article L. 521-4 du code de justice administrative, a estimé, par son ordonnance du 19 juin 2019 dont la commune de Montauban n'a pas relevé appel, que l'exécution de sa première ordonnance, qui n'avait pas davantage été frappée d'appel, impliquait, au titre du rétablissement de l'UD CGT 82 dans les droits dont elle bénéficiait antérieurement, que celle-ci devait pouvoir accéder à ses locaux tous les jours de la semaine de 8h00 à 19h00 et disposer d'une possibilité de réservation prioritaire de la salle Sellier le jeudi jusqu'à 23h00. Saisi une seconde fois sur le fondement de l'article L. 521-4, le juge des référés a relevé qu'à deux reprises, les 23 et 26 septembre 2019, l'UD CGT 82 n'avait pu disposer de la salle Sellier, malgré une réservation validée par la commune, et que, le 12 septembre, l'immeuble n'avait été ouvert qu'à 11h15. Ces éléments l'ont conduit à enjoindre à la commune de s'assurer de l'ouverture des locaux de 8h00 à 23h00 et de réserver l'usage exclusif de la salle Sellier à l'UD CGT 82 le jeudi de 8h00 à 23h00.<br/>
<br/>
              4. En prononçant une telle injonction pour assurer l'exécution des mesures qu'il avait précédemment ordonnées et dont il a estimé qu'elles n'avaient pas été pleinement mises en oeuvre par la commune de Montauban, le juge des référés du tribunal administratif de Toulouse, contrairement à ce que soutient celle-ci, n'a pas excédé les pouvoirs qu'il tient de l'article L. 521-4, ni, en tout état de cause, entaché son ordonnance d'irrégularité.<br/>
<br/>
              5. La commune de Montauban, qui ne peut soutenir utilement, dans le cadre du présent litige, qu'il lui est loisible de modifier l'affectation d'un immeuble lui appartenant sans que la liberté syndicale y fasse obstacle, n'apporte en appel aucun élément nouveau susceptible d'infirmer la solution retenue par le juge des référés.<br/>
<br/>
              6. Il résulte de tout ce qui précède que sa requête doit être rejetée, y compris ses conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées au même titre par l'UD CGT 82.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de la commune de Montauban est rejetée. <br/>
Article 2 : Les conclusions de l'Union départementale des syndicats CGT du Tarn-et-Garonne tendant à l'application de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente ordonnance sera notifiée à la commune de Montauban et à l'Union départementale des syndicats CGT du Tarn-et-Garonne. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
