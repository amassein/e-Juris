<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027507816</ID>
<ANCIEN_ID>JG_L_2013_05_000000368749</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/50/78/CETATEXT000027507816.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 31/05/2013, 368749, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-05-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>368749</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2013:368749.20130531</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 23 mai 2013 au secrétariat du contentieux du Conseil d'Etat, présentée par la société anonyme Total Raffinage Marketing, dont le siège est 24, cours Michelet à Puteaux (92800) ; la société requérante demande au juge des référés du Conseil d'Etat : <br/>
<br/>
              1°) d'ordonner, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de la délibération de la formation restreinte de la Commission nationale de l'informatique et des libertés (CNIL) n° 2013-091 du 11 avril 2013 prononçant un avertissement à son encontre et rendant publique sa décision sur le site internet de la CNIL et sur le site Légifrance ; <br/>
<br/>
              2°) de statuer ce que de droit quant aux dépens ;<br/>
<br/>
<br/>
              elle soutient que :<br/>
              - la condition d'urgence est remplie, dès lors que la publication de l'avertissement porte atteinte à l'image de la société requérante ;<br/>
              - il existe un doute sérieux quant à la légalité de la décision contestée ; <br/>
              - la formation restreinte de la Commission nationale de l'informatique et des libertés (CNIL) a commis une erreur manifeste d'appréciation, dès lors que le système de vote électronique mis en place l'a été dans le strict respect de la recommandation de la CNIL du 21 octobre 2010, et que la fiabilité de la société sous-traitante qu'elle a retenue a déjà été reconnue par le juge judiciaire ; <br/>
              - la formation restreinte de la CNIL a commis une erreur manifeste d'appréciation, dès lors que la société anonyme Total Raffinage Marketing a pris toutes les mesures nécessaires de nature à garantir la confidentialité des données, conformément aux dispositions de la loi du 6 janvier 1978 ;  <br/>
              - le système de vote mis en place n'a porté aucune atteinte aux données personnelles des électeurs, ni aux principes du droit électoral, ni aux libertés publiques ; <br/>
              - la décision contestée a été rendue en violation du principe de l'égalité de traitement ; <br/>
<br/>
<br/>
              Vu la délibération de la formation restreinte n° 2013-091 du 11 avril 2013 de la Commission nationale de l'informatique et des libertés ;<br/>
<br/>
              Vu la copie de la requête à fin d'annulation de la délibération contestée ;<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu la loi du 6 janvier 1978 ; <br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d' une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; qu'en vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée ;<br/>
<br/>
              2. Considérant que l'urgence justifie la suspension de l'exécution d'un acte administratif lorsque celle-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ; qu'il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte contesté sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue ; que l'urgence doit être appréciée objectivement et compte tenu de l'ensemble des circonstances de l'affaire ;<br/>
<br/>
              3. Considérant que, pour justifier l'urgence, la société requérante fait valoir que la publication de l'avertissement sur le site internet de la Commission nationale de l'informatique et des libertés et sur le site Légifrance porte atteinte à son image, notamment de par diverses publications sur un certain nombre de sites internet qu'elle a induit ; que, toutefois, en l'état de l'instruction, il n'apparaît pas que cette publication porterait atteinte à l'image et à la réputation de la société requérante, ni qu'elle serait de nature à perturber son activité à un point tel qu'elle porterait une atteinte grave et immédiate à ses intérêts ; qu'ainsi, la condition d'urgence posée par l'article L. 521-1 du code de justice administrative ne peut, en l'espèce, être regardée comme satisfaite ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que la requête de la société Total Raffinage Marketing doit être rejetée, y compris ses conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative, selon la procédure prévue par l'article L. 522-3 du même code ;<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de la société Total Raffinage Marketing est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à la société Total Raffinage Marketing.<br/>
Copie de la présente sera transmise à la Commission nationale de l'informatique et des libertés.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
