<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000021100669</ID>
<ANCIEN_ID>JG_L_2009_08_000000328781</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/21/10/06/CETATEXT000021100669.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 19/08/2009, 328781, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2009-08-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>328781</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Christnacht</PRESIDENT>
<AVOCATS>SCP PEIGNOT, GARREAU</AVOCATS>
<RAPPORTEUR>M. Alain  Christnacht</RAPPORTEUR>
<COMMISSAIRE_GVT/>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 11 juin 2009, au secrétariat du contentieux du Conseil d'État, présentée par Mlle Samira A, demeurant 120, ... ; Mlle A demande au juge des référés du Conseil d'État :<br/>
<br/>
              1°) d'ordonner, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de la décision implicite par laquelle la commission de recours contre les décisions de refus de visa d'entrée en France a rejeté son recours dirigé contre la décision du 10 octobre 2008 du consul général de France à Tunis (Tunisie), lui refusant un visa de long séjour en qualité d'étudiante ;<br/>
<br/>
              2°) d'enjoindre au consul général de France à Tunis de délivrer le visa sollicité, au besoin sous astreinte ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 600 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              elle soutient qu'il y a urgence, compte tenu du prochain début de l'année universitaire 2009-2010, le refus de visa risquant de lui faire perdre une année d'études ; qu'il existe un doute sérieux quant à la légalité de la décision contestée ; que la décision est entachée d'un défaut de motivation ; qu'elle est entachée d'une erreur de droit dès lors qu'en exerçant un contrôle sur la qualité de son projet d'études, l'autorité consulaire a ajouté pour l'octroi d'un visa une condition qui n'est prévue par aucun texte et qui méconnaît tant les dispositions de l'article 14 de la loi du 26 janvier 1984 sur l'enseignement supérieur, qui prohibent toute sélection à l'entrée des universités que celles des textes réglementant l'accès des étudiants étrangers à l'enseignement supérieur, qui ne prévoient pas une telle condition  ; que la décision attaquée est entachée d'une erreur de fait s'agissant de ses ressources qui seraient insuffisantes, alors qu'elle a bloqué sur un compte bancaire la somme nécessaire à ses besoins pendant cette année d'études en France ; que la décision est entachée d'une erreur manifeste d'appréciation en tant qu'elle relève un manque de sérieux dans ses études ;  <br/>
<br/>
<br/>
              Vu, la copie de l'accusé de réception du recours présenté le 17 novembre 2008 à la commission de recours contre les décisions de refus de visa d'entrée en France ;<br/>
<br/>
              Vu la copie de la requête en annulation présentée par Mlle A ;<br/>
<br/>
              Vu le mémoire, enregistré le 16 juillet 2009, présenté par le ministre de l'immigration de l'intégration, de l'identité nationale et du développement solidaire, qui conclut au rejet de la requête ; il soutient que la condition d'urgence n'est pas satisfaite dès lors que la requérante peut poursuivre ses études en Tunisie et qu'elle ne démontre pas l'existence d'une circonstance particulière de nature à justifier qu'il soit statué en urgence ; que les conclusions tendant à ce qu'il soit enjoint aux autorités consulaires de délivrer le visa sollicité sont irrecevables ; que la décision attaquée n'avait pas à être motivée, la requérante ne relevant pas d'une catégorie particulière de demandeurs de visa énumérés par l'article L. 211-2 du code de l'entrée et du séjour des étrangers et du droit d'asile ; que la décision n'étant pas fondée sur l'insuffisance des ressources de la requérante, le moyen tiré de l'erreur de fait sur le niveau de ces ressources est inopérant ; que la décision qui se fonde, comme le permet une jurisprudence constante, sur une appréciation du caractère peu cohérent du projet d'études de la requérante, n'est entachée ni d'erreur de droit ni de dénaturation, l'intéressée ne démontrant pas la nécessité d'une poursuite de son cursus universitaire en France et ce projet pouvant dissimuler son intention de s'installer en France, où réside son frère ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 84-52 du 26 janvier 1984  ; <br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, Mlle A et, d'autre part, le ministre de l'immigration, de l'intégration, de l'identité nationale et du développement solidaire ;<br/>
<br/>
              Vu le procès-verbal de l'audience publique du 17 juillet 2009 à 11 heures au cours de laquelle ont été entendus :<br/>
              - Me Garreau, avocat au Conseil d'État et à la Cour de cassation, avocat de la requérante ;<br/>
              - le frère de la requérante ;<br/>
              - la représentante du ministre de l'immigration, de l'intégration, de l'identité nationale et du développement solidaire ;<br/>
<br/>
<br/>
<br/>
              Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative :  Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision  ; <br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis au juge des référés que Mlle A, ressortissante tunisienne, a obtenu une maîtrise en langue et littérature françaises de la Faculté des lettres et des sciences humaines de l'université de Sousse ; qu'elle s'est inscrite, pour l'année universitaire 2008-2009, à l'université de Paris III Sorbonne Nouvelle en  master recherche  ; que le visa de long séjour qu'elle a sollicité pour poursuivre ces études supérieures en France lui a été refusé le 22 octobre 2008 ;<br/>
<br/>
              Considérant que Mlle A soutient qu'elle justifie d'un projet d'études sérieux, cohérent avec les études qu'elle a menées jusqu'alors en Tunisie et fait valoir que  le directeur de l'unité de formation et de recherche de littérature et linguistique françaises et latines de l'université de Paris III accepte de diriger le travail de recherche qu'elle a proposé ; que, toutefois, en l'absence de toute disposition conventionnelle, législative ou réglementaire déterminant les cas où le visa peut être refusé à un étranger désirant se rendre en France, et eu égard à la nature d'une telle décision, la commission de recours contre les décisions de refus de visa d'entrée en France dispose d'un large pouvoir d'appréciation à cet égard et peut se fonder non seulement sur des motifs tenant à l'ordre public mais sur toute considération d'intérêt général, notamment, dans le cas d'une demande de visa étudiant, sur le caractère sérieux et cohérent du projet d'étude envisagé, qu'il lui revient d'apprécier ; que le ministre de l'immigration, de l'intégration, de l'identité nationale et du développement solidaire fait valoir que Mlle A, qui a déjà poursuivi des études universitaires pendant de longues années et ne fait pas état d'un projet professionnel précis, n'apporte pas de justification convaincante de la nécessité pour elle de poursuivre ses études dans une université française alors qu'elle pourrait les poursuivre en Tunisie ; qu'au surplus le ministre relève un risque de détournement de l'objet du visa en raison de la présence de son frère en France ; que, par suite, en l'état de l'instruction, les moyens tirés de ce que la décision implicite de la commission, qui n'avait pas à être motivée, ne peut légalement être fondée sur une appréciation portée sur le caractère sérieux du projet d'études de Mlle A et de ce que cette appréciation est entachée d'une erreur manifeste ne sont pas de nature, en l'état de l'instruction, à faire naître un doute sérieux sur la légalité du refus de visa litigieux ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède, et sans qu'il soit besoin d'examiner le moyen tiré de ce que la condition d'urgence exigée par les dispositions de l'article L. 521-1 du code de justice administrative serait remplie, que les conclusions de Mlle A à fin de suspension de l'exécution de la décision lui refusant un visa de long séjour doivent être rejetées ; que, par voie de conséquence, ses conclusions à fin d'injonction, en tout état de cause, et celles tendant à l'application de l'article L. 761-1 du code de justice administrative, doivent également être rejetées ;<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de Mlle Samira A est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à Mlle Samira A et au ministre de l'immigration de l'intégration, de l'identité nationale et du développement solidaire.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
