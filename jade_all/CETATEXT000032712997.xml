<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032712997</ID>
<ANCIEN_ID>JG_L_2016_06_000000383722</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/71/29/CETATEXT000032712997.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 15/06/2016, 383722, Publié au recueil Lebon</TITRE>
<DATE_DEC>2016-06-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>383722</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Bastien Lignereux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:383722.20160615</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire en réplique et un nouveau mémoire, enregistrés le 14 août 2014 et les 31 juillet et 6 août 2015 au secrétariat du contentieux du Conseil d'Etat, l'Association nationale des opérateurs détaillants en énergie (ANODE) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 28 juillet 2014 du ministre de l'écologie, du développement durable et de l'énergie et du ministre de l'économie, du redressement productif et du numérique, modifiant l'arrêté du 26 juillet 2013 relatif aux tarifs réglementés de vente de l'électricité ;<br/>
<br/>
              2°) d'enjoindre aux auteurs de cet arrêté de prendre un nouvel arrêté fixant à titre rétroactif, du 1er août au 31 octobre 2014, des tarifs réglementés de vente de l'électricité conformes au droit applicable ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'énergie ;<br/>
              - la loi n° 2010-1488 du 7 décembre 2010 ;<br/>
              - le décret n° 2009-975 du 12 août 2009 ;<br/>
              - l'arrêté du 26 juillet 2013 relatif aux tarifs réglementés de vente de l'électricité ;<br/>
              - l'arrêté du 28 juillet 2014 relatif aux tarifs réglementés de vente de l'électricité pour la période comprise entre le 23 juillet 2012 et le 31 juillet 2013 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bastien Lignereux, auditeur,<br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de l'Association nationale des opérateurs détaillants en énergie (ANODE) ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 337-5 du code de l'énergie, dans sa rédaction applicable à la date de l'arrêté attaqué : " Les tarifs réglementés de vente d'électricité sont définis en fonction de catégories fondées sur les caractéristiques intrinsèques des fournitures, en fonction des coûts liés à ces fournitures. " ; qu'aux termes du premier alinéa de l'article L. 337-6, dans sa rédaction applicable à la même date : " Dans un délai s'achevant au plus tard le 31 décembre 2015, les tarifs réglementés de vente d'électricité sont progressivement établis en tenant compte de l'addition du prix d'accès régulé à l'électricité nucléaire historique, du coût du complément à la fourniture d'électricité qui inclut la garantie de capacité, des coûts d'acheminement de l'électricité et des coûts de commercialisation ainsi que d'une rémunération normale. " ; qu'enfin, aux termes de l'article 3 du décret du 12 août 2009 relatif aux tarifs réglementés de vente de l'électricité, dans sa rédaction alors applicable : " La part fixe et la part proportionnelle de chaque option ou version tarifaire sont chacune l'addition d'une part correspondant à l'acheminement et d'une part correspondant à la fourniture qui sont établies de manière à couvrir les coûts de production, les coûts d'approvisionnement, les coûts d'utilisation des réseaux publics de transport et de distribution et les coûts de commercialisation, que supportent pour fournir leurs clients Electricité de France et les distributeurs non nationalisés (...), ainsi qu'une marge raisonnable. (...) " ;<br/>
<br/>
              2. Considérant que, pour l'application de ces dispositions, il appartient aux ministres compétents, pour chaque tarif, premièrement, de permettre au moins la couverture des coûts moyens complets des opérateurs afférents à la fourniture de l'électricité à ce tarif, deuxièmement, de prendre en compte une estimation de l'évolution de ces coûts sur la période tarifaire à venir et, enfin, d'ajuster le tarif par une modulation dite de " rattrapage " s'ils constatent qu'un écart significatif s'est produit entre les coûts constatés et ce tarif, du fait d'une surévaluation ou d'une sous-évaluation de ce dernier, au moins au cours de la période tarifaire écoulée ;<br/>
<br/>
              3. Considérant que les ministres chargés de l'économie et de l'énergie ont fixé, par un arrêté du 26 juillet 2013, qui n'a fait l'objet d'aucun recours contentieux, les barèmes des tarifs réglementés " bleus ", " jaunes " et " verts " de vente de l'électricité applicables à compter du 1er août 2013 ; que, pour les tarifs " bleus ", l'arrêté a opéré une hausse moyenne de 5 % à compter de cette même date ; que, dans le but de compenser l'insuffisance de cette hausse par rapport aux exigences mentionnées au point 2 ci-dessus, outre la fixation des tarifs applicables à compter du 1er août 2013, l'arrêté du 26 juillet 2013 disposait, en son article 6, que : " (...) Les barèmes du Tarif Bleu, tels qu'annexés, sont augmentés de 5 % en moyenne à compter du 1er août 2014. Ce niveau sera ajusté en fonction de l'évolution effective des coûts sur la période tarifaire concernée " ; que, par un arrêté du 28 juillet 2014, entré en vigueur le 1er août 2014 et dont l'association nationale des opérateurs détaillants en énergie (ANODE) demande l'annulation pour excès de pouvoir, les ministres ont abrogé ces dernières dispositions ;<br/>
<br/>
              Sur la fin de non-recevoir opposée par le ministre chargé de l'économie :<br/>
<br/>
              4. Considérant que, en vertu des dispositions de l'arrêté du 26 juillet 2013 abrogées par l'arrêté attaqué, citées au point 3, les ministres chargés de l'économie et de l'énergie devraient adopter, avant le 1er août 2014, un nouvel arrêté fixant les tarifs réglementés " bleus " applicables à compter de cette date ; que, contrairement à ce que soutient le ministre chargé de l'économie, ces dispositions n'étaient pas dépourvues d'effet normatif ; que le ministre ne saurait soutenir que l'association requérante n'est pas recevable à demander l'annulation pour excès de pouvoir de l'arrêté par lequel elles ont été abrogées au motif qu'il ne ferait pas grief ; <br/>
<br/>
              Sur la légalité de l'arrêté attaqué :<br/>
<br/>
              5. Considérant qu'ainsi qu'il a été dit au point précédent, en vertu des dispositions de l'article 6 de l'arrêté du 26 juillet 2013 les ministres chargés de l'économie et de l'énergie devraient adopter un nouvel arrêté fixant les tarifs réglementés " bleus " applicables à compter du 1er août 2014 ; que cet arrêté devait prévoir une évolution de ces tarifs conforme aux principes énoncés au point 2 ; que, dans l'hypothèse où l'application de ces principes aurait conduit à une hausse moyenne des tarifs " bleus " inférieure à 5 %, l'arrêté du 26 juillet 2013 imposait de fixer le niveau de cette hausse, en moyenne pour l'ensemble des tarifs " bleus ", à 5 % ; que l'arrêté attaqué abroge ces dispositions trois jours seulement avant le 1er août, à une date où les fournisseurs d'électricité avaient pu déjà anticiper pleinement les effets de leur mise en oeuvre ; que, dans ces circonstances particulières, et compte tenu de l'importance du niveau des tarifs réglementés " bleus " pour l'activité des fournisseurs d'électricité et le contenu des offres qu'ils proposent, l'arrêté attaqué a été pris en méconnaissance du principe de sécurité juridique, alors même qu'un communiqué de presse du 19 juin 2014 en a annoncé le principe ; que, par suite et sans qu'il soit besoin d'examiner les autres moyens de la requête, cet arrêté doit être annulé ;<br/>
<br/>
              Sur les conclusions aux fins d'injonction :<br/>
<br/>
              6. Considérant qu'aux termes de l'article L. 911-1 du code de justice administrative : " Lorsque sa décision implique nécessairement qu'une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public prenne une mesure d'exécution dans un sens déterminé, la juridiction, saisie de conclusions en ce sens, prescrit, par la même décision, cette mesure assortie, le cas échéant, d'un délai d'exécution. " ;<br/>
<br/>
              7. Considérant que l'ANODE demande au Conseil d'Etat d'enjoindre aux ministres chargés de l'économie et de l'énergie de prendre un arrêté fixant, à titre rétroactif, les tarifs réglementés de vente de l'électricité " bleus " applicables du 1er août 2014 au 31 octobre 2014, veille de la date d'entrée en vigueur de l'arrêté tarifaire du 30 octobre 2014 ; que la présente décision implique nécessairement de faire droit à cette demande ; qu'il résulte de ce qui a été dit au point 5 ci-dessus que les ministres devront, dans la détermination de ces tarifs, permettre au moins la couverture des coûts moyens complets supportés par les fournisseurs historiques à la date du 1er août 2014, prendre en compte l'estimation de l'évolution de ces coûts sur la période tarifaire en cause et, enfin, ajuster les tarifs au titre de la modulation dite de " rattrapage " ; que, s'agissant de cette dernière, eu égard à la durée d'application de cet arrêté rétroactif, il n'appartient aux ministres de faire porter sur la période en cause que le quart de la sous-évaluation des tarifs " bleus " constatée au titre de la période tarifaire précédente, ouverte par l'arrêté tarifaire du 26 juillet 2013 ; que, dans l'hypothèse où l'application de ces principes conduirait à une hausse tarifaire moyenne inférieure à 5 %, les ministres devront fixer le niveau de cette hausse, en moyenne pour l'ensemble des tarifs " bleus ", à 5 % ;<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              8. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement d'une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : L'arrêté du 28 juillet 2014 du ministre de l'écologie, du développement durable et de l'énergie et du ministre de l'économie, du redressement productif et du numérique, modifiant l'arrêté du 26 juillet 2013 relatif aux tarifs réglementés de vente de l'électricité, est annulé.<br/>
Article 2 : Il est enjoint au ministre de l'économie, de l'industrie et du numérique et à la ministre de l'environnement, de l'énergie et de la mer de prendre, dans un délai de trois mois à compter de la notification de la présente décision, un nouvel arrêté fixant les tarifs réglementés " bleus " de l'électricité pour la période comprise entre le 1er août 2014 et le 31 octobre 2014 conformément aux principes énoncés dans la présente décision.<br/>
Article 3 : L'Etat versera à l'Association nationale des opérateurs détaillants en énergie une somme de 3 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à l'Association nationale des opérateurs détaillants en énergie (ANODE) et au ministre de l'économie, de l'industrie et du numérique.<br/>
Copie en sera adressée pour information à la ministre de l'environnement, de l'énergie et de la mer et à la Commission de régulation de l'énergie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-07 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. PRINCIPES INTÉRESSANT L'ACTION ADMINISTRATIVE. - PRINCIPE DE SÉCURITÉ JURIDIQUE - MÉCONNAISSANCE - ABROGATION DES DISPOSITIONS DONT DEVAIT RÉSULTER UNE HAUSSE DE TARIFS RÉGLEMENTÉS DE VENTE DE L'ÉLECTRICITÉ TROIS JOURS AVANT LA HAUSSE PRÉVUE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">29-06-02-005 ENERGIE. MARCHÉ DE L'ÉNERGIE. TARIFICATION. - ABROGATION DES DISPOSITIONS DONT DEVAIT RÉSULTER UNE HAUSSE DE TARIFS RÉGLEMENTÉS DE VENTE DE L'ÉLECTRICITÉ TROIS JOURS AVANT LA HAUSSE PRÉVUE - MÉCONNAISSANCE DU PRINCIPE DE SÉCURITÉ JURIDIQUE - EXISTENCE.
</SCT>
<ANA ID="9A"> 01-04-03-07 En vertu des dispositions de l'article 6 de l'arrêté du 26 juillet 2013 relatif aux tarifs réglementés de vente de l'électricité, les ministres chargés de l'économie et de l'énergie devaient adopter un nouvel arrêté fixant les tarifs réglementés bleus applicables à compter du 1er août 2014. Cet arrêté devait prévoir une évolution de ces tarifs conforme aux principes découlant des articles L. 337-5 et L. 337-6 du code de l'énergie et de l'article 3 du décret n° 2009-975 du 12 août 2009. Dans l'hypothèse où l'application de ces principes aurait conduit à une hausse moyenne des tarifs bleus inférieure à 5 %, l'arrêté du 26 juillet 2013 imposait de fixer le niveau de cette hausse, en moyenne pour l'ensemble des tarifs bleus, à 5 %.,,,L'arrêté du 28 juillet 2014 a abrogé ces dispositions trois jours seulement avant le 1er août, à une date où les fournisseurs d'électricité avaient pu déjà anticiper pleinement les effets de leur mise en oeuvre. Dans ces circonstances particulières, et compte tenu de l'importance du niveau des tarifs réglementés bleus sur l'activité des fournisseurs d'électricité et le contenu des offres qu'ils proposent, cet arrêté du 28 juillet 2014 a été pris en méconnaissance du principe de sécurité juridique, alors même qu'un communiqué de presse du 19 juin 2014 en avait annoncé le principe.</ANA>
<ANA ID="9B"> 29-06-02-005 En vertu des dispositions de l'article 6 de l'arrêté du 26 juillet 2013 relatif aux tarifs réglementés de vente de l'électricité, les ministres chargés de l'économie et de l'énergie devaient adopter un nouvel arrêté fixant les tarifs réglementés bleus applicables à compter du 1er août 2014. Cet arrêté devait prévoir une évolution de ces tarifs conforme aux principes découlant des articles L. 337-5 et L. 337-6 du code de l'énergie et de l'article 3 du décret n° 2009-975 du 12 août 2009. Dans l'hypothèse où l'application de ces principes aurait conduit à une hausse moyenne des tarifs bleus inférieure à 5 %, l'arrêté du 26 juillet 2013 imposait de fixer le niveau de cette hausse, en moyenne pour l'ensemble des tarifs bleus, à 5 %.,,,L'arrêté du 28 juillet 2014 a abrogé ces dispositions trois jours seulement avant le 1er août, à une date où les fournisseurs d'électricité avaient pu déjà anticiper pleinement les effets de leur mise en oeuvre. Dans ces circonstances particulières, et compte tenu de l'importance du niveau des tarifs réglementés bleus sur l'activité des fournisseurs d'électricité et le contenu des offres qu'ils proposent, cet arrêté du 28 juillet 2014 a été pris en méconnaissance du principe de sécurité juridique, alors même qu'un communiqué de presse du 19 juin 2014 en avait annoncé le principe.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
