<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030580660</ID>
<ANCIEN_ID>JG_L_2015_05_000000385722</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/58/06/CETATEXT000030580660.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème / 7ème SSR, 11/05/2015, 385722</TITRE>
<DATE_DEC>2015-05-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>385722</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème / 7ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LE GRIEL ; DELAMARRE ; SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR>M. Luc Briand</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:385722.20150511</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              Mme H...G...a demandé au tribunal administratif de Cergy-Pontoise d'annuler pour excès de pouvoir la décision du préfet des Hauts-de-Seine par laquelle celui-ci a refusé de lui délivrer le récépissé de candidature de la liste " Nous, citoyens de Puteaux " pour le premier tour de l'élection municipale et communautaire organisé le 23 mars 2014 dans la commune de Puteaux (Hauts-de-Seine).<br/>
<br/>
              Par un jugement n° 1402302 du 10 mars 2014, le tribunal administratif a rejeté cette requête.<br/>
<br/>
              Mme G...a ultérieurement demandé au tribunal administratif de Cergy-Pontoise d'annuler les opérations électorales qui se sont déroulées le 23 mars 2014 dans la commune de Puteaux.<br/>
<br/>
              Par un jugement n° 1403103 du 17 octobre 2014, le tribunal administratif a annulé l'attribution d'un siège de conseiller municipal à la liste " Puteaux Bleu Marine " ainsi que l'élection de M. C...B...en tant que conseiller municipal de la commune de Puteaux et rejeté le surplus de sa protestation.<br/>
<br/>
<br/>
              1) Sous le n° 385722, par une requête et un mémoire en réplique, enregistrés le 14 novembre 2014 et le 14 avril 2015 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler le jugement n° 1403103 du tribunal administratif de Cergy-Pontoise du 17 octobre 2014 en tant qu'il a annulé l'attribution d'un siège de conseiller municipal à la liste " Puteaux Bleu Marine " ainsi que son élection en qualité de conseiller municipal de la commune de Puteaux ; <br/>
<br/>
              2°) à titre subsidiaire, d'annuler l'ensemble des opérations électorales organisées le 23 mars 2014 en vue de la désignation des conseillers municipaux et communautaires de la commune de Puteaux ; <br/>
<br/>
              3°) de mettre à la charge de Mme G...la somme de 3 600 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              2) Sous les n°s 385745 et 385763, par deux requêtes sommaires, un mémoire complémentaire et un mémoire en réplique, enregistrés les 17 novembre et 17 décembre 2014 et le 8 avril 2015 au secrétariat du contentieux du Conseil d'Etat, Mme G...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler le jugement n° 1402302 du 10 mars 2014 du tribunal administratif de Cergy-Pontoise et, par voie de conséquence, la décision du préfet des Hauts-de-Seine du 7 mars précédent ;<br/>
<br/>
              2°) d'annuler le jugement n° 1403103 du 17 octobre 2014 en tant qu'il a rejeté ses conclusions tendant à l'annulation de l'ensemble des opérations électorales qui se sont déroulées le 23 mars 2014 dans la commune de Puteaux ;<br/>
<br/>
              3°) d'annuler l'ensemble de ces opérations électorales ; <br/>
<br/>
              4°) de rejeter les conclusions présentées par M. B...au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              5°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code électoral ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Luc Briand, maître des requêtes en service extraordinaire, <br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Le Griel, avocat de M.B..., à la SCP Delaporte, Briard, Trichet, avocat de MmeG..., et à Me Delamarre, avocat de Mme I... ; <br/>
<br/>
              Vu les notes en délibéré, enregistrées le 20 avril 2015, présentées pour Mme I... dans chacune des affaires ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant que Mme G...relève appel du jugement du 10 mars 2014 du tribunal administratif de Cergy-Pontoise par lequel celui-ci a rejeté sa demande tendant à l'annulation de la décision du préfet des Hauts-de-Seine refusant de lui délivrer le récépissé de candidature de la liste " Nous, citoyens de Puteaux " pour le premier tour de l'élection municipale et communautaire organisé le 23 mars 2014 dans la commune de Puteaux ; que M. B... et Mme G...font appel du jugement du 17 octobre 2014 par lequel le même tribunal a, pour le premier, annulé l'attribution d'un siège de conseiller municipal à la liste " Puteaux Bleu Marine " et l'élection de M. B...en tant que conseiller municipal de la commune de Puteaux et, pour la seconde, rejeté sa protestation tendant à l'annulation de l'ensemble des opérations électorales qui se sont déroulées le 23 mars 2014 dans la commune de Puteaux ; que les requêtes visées ci-dessus présentent à juger des questions semblables ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              Sur les conclusions dirigées contre le jugement du 10 mars 2014 :<br/>
<br/>
              2.	Considérant qu'aux termes du premier alinéa de l'article L. 265 du code électoral : " La déclaration de candidature résulte du dépôt à la préfecture ou à la sous-préfecture d'une liste (...). Il en est délivré récépissé " ; qu'aux termes des deux derniers alinéas du même article : " En cas de refus de délivrance du récépissé, tout candidat de la liste intéressée dispose de vingt-quatre heures pour saisir le tribunal administratif qui statue, en premier et dernier ressort, dans les trois jours du dépôt de la requête. / Faute pour le tribunal administratif d'avoir statué dans ce délai, le récépissé est délivré " ; qu'il résulte des termes mêmes de ces dispositions que le législateur a exclu tout appel direct contre les jugements des tribunaux administratifs statuant sur les refus de récépissé de déclaration de candidatures, actes préliminaires aux opérations électorales, eu égard à la nature de la décision en cause et à la brièveté du délai imparti par la loi au tribunal administratif pour statuer, délai au terme duquel le récépissé est délivré de plein droit ; qu'en revanche, le candidat concerné peut, à l'occasion d'une protestation dirigée contre les opérations électorales, soulever le grief tiré de l'irrégularité du refus de délivrance du récépissé ; qu'ainsi, les conclusions présentées par Mme G...contre le jugement du tribunal administratif de Cergy-Pontoise du 10 mars 2014 ne sont pas recevables ;<br/>
<br/>
              Sur les conclusions dirigées contre le jugement du 17 octobre  2014 :<br/>
<br/>
              3.	Considérant que le jugement attaqué vise le code électoral ; que  le moyen tiré de ce que le jugement attaqué, faute de viser l'article L. 263 du code électoral, serait irrégulier, doit dès lors être écarté ;<br/>
<br/>
              4.	Considérant qu'aux termes de l'article L. 264 du code électoral : " Une déclaration de candidature est obligatoire pour chaque tour de scrutin (...) " ; qu'aux termes de l'article L. 265 du même code : " La déclaration de candidature résulte du dépôt à la préfecture ou à la sous-préfecture d'une liste répondant aux conditions fixées aux articles L. 260, L. 263, L. 264 et LO. 265-1. Il en est délivré récépissé / (...) Pour chaque tour de scrutin, cette déclaration comporte la signature de chaque candidat, sauf le droit pour tout candidat de compléter la déclaration collective non signée de lui par une déclaration individuelle faite dans le même délai et portant sa signature " ; que selon l'article L. 263 du même code : " Nul ne peut être candidat dans plus d'une circonscription électorale, ni sur plus d'une liste " ;<br/>
<br/>
              5.	Considérant que la décision par laquelle le préfet des Hauts-de-Seine a refusé l'enregistrement de la liste conduite par Mme G...était fondée sur le seul motif que figurait sur celle-ci M. E...alors que ce dernier était également candidat sur la liste conduite par M.B..., déjà enregistrée ; qu'il résulte cependant de l'instruction que la signature de M. E...sur le formulaire de déclaration de candidature de la liste conduite par M. B...a été apposée lors d'un démarchage impromptu à son domicile, par des représentants de M. B...; que M.E..., qui avait antérieurement accepté d'être candidat sur la liste menée par MmeG..., n'avait alors pas exprimé le souhait de figurer sur la liste de M. B..., ce qu'il a ultérieurement confirmé dans deux attestations datées des 6 et 27 mars 2014 ;  qu'il a, enfin, déposé plainte dès le 12 mars 2014 pour faux ; qu'ainsi, la signature de M. E... figurant sur le mandat donné à la liste de M. B..."J..." résulte d'une manoeuvre ; <br/>
<br/>
              6.	Considérant qu'il appartient au juge de l'élection, lorsqu'il constate une manoeuvre de cette nature, de rechercher si, eu égard aux résultats des opérations électorales, elle a altéré la sincérité du scrutin dans son ensemble ; que, dans l'affirmative, il lui appartient d'annuler l'intégralité des opérations électorales ; que, dans la négative, il lui appartient seulement d'annuler, le cas échéant, l'élection des candidats figurant sur la liste irrégulièrement constituée ;<br/>
<br/>
              7.	Considérant que la manoeuvre commise par la liste " Puteaux Bleu Marine " a permis à celle-ci de pouvoir être enregistrée et de participer au scrutin alors qu'elle a empêché la liste conduite par MmeG..., compte tenu de l'unique motif opposé à son enregistrement, de participer à ce scrutin ; qu'eu égard aux conséquences sur les résultats du scrutin et sur la répartition des sièges entre les listes, de la participation de la liste " Puteaux Bleu Marine " et de l'absence de participation de la liste de MmeG..., alors que cette dernière bénéficiait d'une implantation locale et du soutien de l'ancien maire de la commune, cette manoeuvre a, nonobstant l'écart de voix séparant la liste conduite par Mme I...et les autres listes, été de nature à fausser les résultats du scrutin dans leur ensemble ;<br/>
<br/>
              8.	Considérant qu'il résulte de ce qui précède que Mme G...est fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Cergy-Pontoise a refusé d'annuler dans leur intégralité les opérations électorales auxquelles il a été procédé le 23 mars 2014 pour l'élection des conseillers municipaux de la commune de Puteaux ;<br/>
<br/>
              9.	Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas dans la présente instance la partie perdante, les sommes demandées par Mme G...et M. A...au titre des frais exposés par eux et non compris dans les dépens ; qu'elles font également obstacle à ce qu'une somme soit mise à ce titre à la charge de MmeG..., qui n'a pas la qualité de partie perdante dans la présente instance ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par Mme I...et M. B...au titre de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Les conclusions de Mme G...dirigées contre le jugement du tribunal administratif de Cergy-Pontoise du 10 mars 2014 sont rejetées.<br/>
<br/>
Article 2 : Les opérations électorales qui se sont déroulées le 23 mars 2014 pour l'élection des conseillers municipaux de la commune de Puteaux sont annulées.<br/>
<br/>
Article 3 : Le jugement du tribunal administratif de Cergy-Pontoise du 17 octobre 2014 est réformé en ce qu'il a de contraire à la présente décision.<br/>
<br/>
Article 4 : Les conclusions présentées par M.B..., MmeG..., M. A...et Mme I... au titre de l'article L. 761-1 du  code de justice administrative sont rejetées. <br/>
<br/>
Article 5 : La présente décision sera notifiée à M. C...B..., à Mme H...G..., à M. F...A..., à Mme D...I...et au ministre de l'intérieur. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">28-04-04-01-01 ÉLECTIONS ET RÉFÉRENDUM. ÉLECTIONS MUNICIPALES. CAMPAGNE ET PROPAGANDE ÉLECTORALES. CAMPAGNE ÉLECTORALE. PRÉSENTATION DES LISTES. - MANOEUVRE VISANT À OBTENIR LA SIGNATURE D'UN FORMULAIRE DE DÉCLARATION DE CANDIDATURE POUR PERMETTRE L'ENREGISTREMENT D'UNE LISTE - ANNULATION DE L'ENSEMBLE DE L'ÉLECTION EN L'ESPÈCE.
</SCT>
<ANA ID="9A"> 28-04-04-01-01 Refus d'enregistrement d'une liste au motif qu'un des candidats est déjà inscrit sur une liste enregistrée. Il résulte de l'instruction que la signature de ce candidat sur le formulaire de déclaration de candidature de la liste enregistrée la première a été apposée lors d'un démarchage impromptu au domicile de l'intéressé, qui n'avait pas exprimé le souhait de figurer sur cette liste et qui a ultérieurement porté plainte pour faux. Ainsi, cette signature résulte d'une manoeuvre qui a permis à une liste de participer au scrutin alors qu'elle a empêché l'autre liste d'y participer. Eu égard aux conséquences sur les résultats du scrutin et sur la répartition des sièges entre les listes, de la participation de la liste enregistrée la première et de l'absence de participation de l'autre liste, alors que cette dernière bénéficiait d'une implantation locale et du soutien de l'ancien maire de la commune, cette manoeuvre a été de nature à fausser les résultats du scrutin dans leur ensemble.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
