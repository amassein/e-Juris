<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044087018</ID>
<ANCIEN_ID>JG_L_2021_08_000000455572</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/08/70/CETATEXT000044087018.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 18/08/2021, 455572, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-08-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>455572</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:455572.20210818</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et deux nouveaux mémoires, enregistrés les 14, 16 et 17 août 2021 au secrétariat du contentieux du Conseil d'Etat, l'association Penya Blaugrana de Lyon du F.C Barcelone et M. B... D... demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) de suspendre l'exécution des délibérations de l'assemblée générale et de l'assemblée fédérale de la ligue de football professionnel des 12 et 14 décembre 2019 en tant qu'elles renvoient l'activation des capacités de sanction de la direction nationale du contrôle de gestion (DNCG) de la Ligue de football professionnel (LFP) au 15 mai 2023 et déclarer d'application rétroactive, ou à défaut immédiate, les dispositions adoptées ces mêmes jours ;<br/>
<br/>
              2°) de suspendre les effets de toute homologation intervenue préalablement à la décision du 25 juin 2021 de la DNCG jusqu'au respect des règles du " fair play financier " par le club du Paris-Saint-Germain (PSG) ;<br/>
<br/>
              3°) de suspendre les effets de tout processus d'homologation de contrat par la LFP à partir du 25 juin 2021, à tout le moins à partir de la demande de conciliation introduite le 9 août 2021, entre un joueur de football professionnel et tout club contrôlé par la DNCG ne respectant pas le " fair play financier " ; <br/>
<br/>
              4°) de suspendre l'exécution de toute délibération prise par la commission de contrôle des clubs professionnels de la DNCG depuis le 14 décembre 2019 concernant l'examen de la situation des clubs ne respectant pas les règles du " fair play financier " et n'ayant pas abouti à des sanctions ; <br/>
<br/>
              5°) d'ordonner l'application immédiate des dispositions relatives au " fair play financier " prévues par le règlement de la DNCG annexé à la LFP et adoptées par l'ensemble des ligues professionnelles de football européen ;<br/>
<br/>
              6°) d'ordonner à la commission de contrôle des clubs professionnels de la DNCG d'effectuer un contrôle d'opportunité afin de s'assurer de la compatibilité de tout recrutement par un club français postérieurement au 25 juin 2021 avec les exigences du i du 1 de l'article 11 du règlement et de suspendre tout effet d'homologation par la LFP des contrats signés postérieurement à cette date, ou à celle du 9 août 2021, par des clubs ne respectant pas les règles ; <br/>
              7°) à titre principal, de se substituer à la commission de contrôle des clubs professionnels de la DNCG et d'ordonner une interdiction provisoire de recrutement à l'encontre du PSG en raison de la violation des ratios prévus par l'article 11 du règlement et, à titre subsidiaire, d'ordonner à cette commission d'effectuer un contrôle d'opportunité ; <br/>
<br/>
              8°) de poser à la Cour de justice de l'Union européenne la question de savoir s'il est nécessaire d'harmoniser les règles des ligues de football professionnel <br/>
européennes en exigeant de toutes qu'elles adoptent, au même moment et de façon <br/>
strictement identique, les mêmes règles de " fair-play financier " ou si doivent être abolies l'ensemble des règles relatives au " fair play financier " adoptées par les ligues de football professionnel nationales au sein de l'Union européenne afin de faire cesser les distorsions de concurrence.<br/>
<br/>
<br/>
              Ils soutiennent que :<br/>
<br/>
              - ils justifient d'un intérêt à agir dès lors que, en premier lieu, le F.C Barcelone est une association sportive sans but lucratif appartenant à ses membres, qui disposent d'un pouvoir décisionnaire absolu sur ses activités, en deuxième lieu, les " socios " du F.C Barcelone financent le club à travers une contribution annuelle à les assimilant à des actionnaires, en troisième lieu, M. D... est " socio " du F.C Barcelone depuis le 3 mars 2020, en quatrième lieu, les penyas du F.C Barcelone sont parties intégrantes du club et la Penya Blaugrana de Lyon est la plus importante penya de France et, en dernier lieu, M. C... A... a été autorisé à agir ; <br/>
              - le Conseil d'Etat est compétent en premier et dernier ressort dès lors que, d'une part, par convention conclue la Fédération française de football (FFF) et la Ligue de football professionnel (LFP), la gestion du football professionnel a été déléguée à la Ligue, qui est notamment chargée d'organiser, de gérer et de réglementer le championnat de Ligue 1 et le championnat de Ligue 2, et que, d'autre part, leur requête porte sur des actes réglementaires pris par des autorités publiques ayant une compétence nationale et exerçant des prérogatives de puissance publique fondant un acte administratif ayant des conséquences individuelles et que, en dernier lieu, les décisions contestées se rapportent à l'organisation du service public ; <br/>
              - la condition d'urgence est satisfaite dès lors que, en premier lieu, la conclusion du contrat de recrutement de Lionel Messi par le PSG a été annoncée le 10 août 2021, son homologation prenant en moyenne 48 heures au sein de la LFP, et la participation de Lionel Messi au premier match du PSG aura lieu dans les dix jours suivants, en deuxième lieu, l'homologation du contrat et la participation du joueur aux compétitions sportives méconnaîtraient les règles adoptées à l'échelle européenne afin de faire respecter le " fair play financier ", en troisième lieu, cela aura pour effet de créer un déséquilibre irrémédiable au sein des compétitions nationales et internationales auxquelles le PSG participe, en quatrième lieu, ce recrutement produirait des conséquences économiques immédiates pour le F.C Barcelone ;<br/>
              - il est porté une atteinte grave et manifestement illégale à plusieurs libertés fondamentales ; <br/>
              - les décisions contestées méconnaissent les obligations conventionnelles et constitutionnelles relatives aux libertés économiques, notamment au droit de propriété et à la liberté d'entreprendre dès lors qu'elles ont pour effet de fausser le jeu de la concurrence au sein du marché intérieur de l'Union européenne et qu'elles méconnaissent les règles du " fair play financier " ; <br/>
              - elles méconnaissent le principe de sécurité juridique, le i du 1 de l'article 11 du règlement de la DNCG ne permettant pas d'établir avec certitude les dispositions d'application immédiate et celles reportées au 15 mai 2023 ;<br/>
              - elles portent atteinte au principe d'égalité, les différences entre les règlementations nationales au sein de l'Union européenne ayant pour conséquence des déséquilibres fondamentaux entre deux acteurs économiques agissant au sein du même marché.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ; <br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. Le juge des référés du Conseil d'Etat ne peut être régulièrement saisi, en premier et dernier ressort, d'une requête tendant à la mise en œuvre de l'une des procédures régies par le livre V du code de justice administrative que pour autant que le litige principal auquel se rattache ou est susceptible de se rattacher la mesure d'urgence qu'il lui est demandé de prendre ressortit lui-même de la compétence directe du Conseil d'Etat. L'article R. 522-8-1 du même code prévoit que, par dérogation aux dispositions du titre V du livre III relatif au règlement des questions de compétence au sein de la juridiction administrative, le juge des référés qui entend décliner la compétence de la juridiction rejette les conclusions dont il est saisi par voie d'ordonnance, sans qu'il ait à les transmettre à la juridiction compétente.<br/>
<br/>
              3. Les requérants demandent au juge des référés du Conseil d'Etat, saisi sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de plusieurs décisions de la Ligue de football professionnel, notamment de la commission de contrôle des clubs professionnels de sa direction nationale du contrôle de gestion. Leurs conclusions principales portent en réalité sur des décisions relatives au contrat de recrutement d'un joueur de football professionnel par le club du Paris-Saint-Germain, qui, étant spécifiques à ce club, constituent des décisions individuelles. Par conséquent, ainsi qu'il a été déjà jugé par une ordonnance du 11 août 2021, la présente requête n'est manifestement pas au nombre de celles dont il appartient au Conseil d'Etat de connaître en premier et dernier ressort en vertu des dispositions de l'article R. 311-1 du code de justice administrative ou d'autres dispositions. Le tribunal administratif de Paris a d'ailleurs statué sur des conclusions analogues à celles présentées dans la présente requête par une ordonnance du 16 août 2021. <br/>
<br/>
              4. Il résulte de ce qui précède qu'il est manifeste que la requête ne peut être accueillie. Par suite, elle doit être rejetée selon la procédure prévue par l'article L. 522-3 du code de justice administrative. <br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de l'association Penya Blaugrana de Lyon du F.C Barcelone et autre est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à l'association Penya Blaugrana de Lyon du F.C Barcelone et à M. B... D....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
