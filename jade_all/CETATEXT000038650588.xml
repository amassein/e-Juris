<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038650588</ID>
<ANCIEN_ID>JG_L_2019_06_000000421353</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/65/05/CETATEXT000038650588.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 18/06/2019, 421353, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-06-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421353</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:421353.20190618</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif de Paris, d'une part, d'annuler la décision du 23 décembre 2015 par laquelle le président du Conseil supérieur de l'audiovisuel  (CSA) a mis fin à son contrat à compter du 5 janvier 2016 ainsi que la décision du 18 mars 2016 rejetant son recours gracieux, d'autre part, de condamner le CSA au versement de la somme de 153 348,77 euros à parfaire, assortie des intérêts au taux légal à compter du 5 février 2016, en réparation des préjudices résultant de l'illégalité de son licenciement. Par un jugement n° 1605406/5-3 du 19 avril 2017, le tribunal administratif de Paris a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 17PA02028 du 11 avril 2018, la cour administrative d'appel de Paris a, sur appel de MmeB..., annulé ce jugement ainsi que les décisions attaquées et condamné le CSA à indemniser MmeB..., dans la limite d'un montant principal de 135 049,63 euros.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 11 juin 2018, 4 septembre 2018 et 25 janvier 2019, le CSA demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de Mme B...;<br/>
<br/>
              3°) de mettre à la charge de Mme B...la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
<br/>
              - la loi n° 86-1067 du 30 septembre 1986 ;<br/>
<br/>
              - le décret n° 86-83 du 17 janvier 1986 ; <br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat du Conseil supérieur de l'audiovisuel et à la SCP Lyon-Caen, Thiriez, avocat de MmeB....<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme A...B..., agent contractuel employé par le Conseil supérieur de l'audiovisuel (CSA) d'abord par deux contrats des 5 janvier et 5 juillet 2015 puis par un contrat de trois ans en date du 2 octobre 2015, a demandé au tribunal administratif de Paris, d'une part, l'annulation de la décision du 23 décembre 2015 mettant fin à la période d'essai prévue par le contrat du 2 octobre 2015 ainsi que de la décision de rejet de son recours gracieux, d'autre part, la condamnation du CSA à l'indemniser du préjudice ayant résulté pour elle de ces décisions. Le tribunal administratif de Paris a rejeté sa demande. Le CSA se pourvoit en cassation contre l'arrêt du 11 avril 2018 par lequel la cour administrative d'appel, faisant droit à l'appel de Mme B..., a annulé les décisions contestées et l'a condamné à indemniser celle-ci. <br/>
<br/>
              2. Aux termes de l'article 9 du décret du 17 janvier 1986 dans sa rédaction applicable au présent litige : " Le contrat ou l'engagement peut comporter une période d'essai qui permet à l'administration d'évaluer les compétences de l'agent dans son travail et à ce dernier d'apprécier si les fonctions occupées lui conviennent. / Toutefois, aucune période d'essai ne peut être prévue lorsqu'un nouveau contrat est conclu ou renouvelé par une même autorité administrative avec un même agent pour exercer les mêmes fonctions que celles prévues par le précédent contrat, ou pour occuper le même emploi que celui précédemment occupé (...) - Le licenciement en cours ou au terme de la période d'essai ne peut intervenir qu'à l'issue d'un entretien préalable. La décision de licenciement est notifiée à l'intéressé par lettre recommandée avec demande d'avis de réception ou par lettre remise en main propre contre décharge. / - Aucune durée de préavis n'est requise lorsque la décision de mettre fin au contrat intervient en cours ou à l'expiration d'une période d'essai. / Le licenciement au cours d'une période d'essai doit être motivé. / Le licenciement au cours ou à l'expiration d'une période d'essai ne donne pas lieu au versement de l'indemnité prévue au titre XII. ". L'article 45-2 de ce décret dispose : " L'agent contractuel peut être licencié pour un motif d'insuffisance professionnelle. L'agent doit préalablement être mis à même de demander la communication de l'intégralité de toute pièce figurant dans son dossier individuel, dans un délai suffisant permettant à l'intéressé d'en prendre connaissance. Le droit à communication concerne également toute pièce sur laquelle l'administration entend fonder sa décision, même si elle ne figure pas au dossier individuel ". Enfin, aux termes de l'article 47 de ce décret : " Le licenciement ne peut intervenir qu'à l'issue d'un entretien préalable. La convocation à l'entretien préalable est effectuée par lettre recommandée ou par lettre remise en main propre contre décharge. Cette lettre indique l'objet de la convocation. / - L'entretien préalable ne peut avoir lieu moins de cinq jours ouvrables après la présentation de la lettre recommandée ou la remise en main propre de la lettre de convocation. / L'agent peut se faire accompagner par la ou les personnes de son choix. / Au cours de l'entretien préalable, l'administration indique à l'agent les motifs du licenciement et le cas échéant le délai pendant lequel l'agent doit présenter sa demande écrite de reclassement ainsi que les conditions dans lesquelles les offres de reclassement sont présentées ".<br/>
<br/>
              3. Il ressort des mentions de l'arrêt attaqué que la cour, pour retenir que le licenciement de MmeB..., décidé en cours de contrat et prenant effet avant le terme de celui-ci, ne pouvait, en application des dispositions citées au point précédent, être regardé comme intervenu à l'issue d'une période d'essai et devait dès lors être soumis aux formalités prévues par les articles 45-2 et suivants du décret du 17 janvier 1986, s'est fondée sur la comparaison des fonctions exercées par Mme B...en application, d'une part, des contrats signés les 5 janvier et 5 juillet 2015, d'autre part, du contrat signé le 2 octobre 2015. Elle a, pour procéder à cette appréciation, tenu compte non seulement des termes des contrats ainsi que des fiches de poste correspondantes mais plus généralement des fonctions effectivement exercées, telles qu'elles ressortaient des pièces du dossier qui lui était soumis. En retenant au terme de cet examen l'existence d'une continuité dans les fonctions exercées par Mme B...avant et après la conclusion du contrat du 2 octobre 2015, la cour a porté une appréciation souveraine, exempte de dénaturation, n'a pas commis d'erreur de droit ni entaché son arrêt d'insuffisance de motivation, dès lors qu'elle n'était pas tenue de répondre à chacun des arguments soulevés par le CSA. <br/>
<br/>
              4. En relevant qu'il ne résultait pas du compte rendu de l'évaluation annuelle pour 2014-2015 de Mme B...que les fonctions qu'elle avaient exercées avant et après la date du 4 juillet 2015 aient été différentes, la cour n'a pas dénaturé les pièces du dossier ni commis d'erreur de qualification juridique.<br/>
<br/>
              5. En déduisant de son constat de l'identité des fonctions exercées en application du contrat du 2 octobre 2015 et au titre de la période antérieure que, faute pour le contrat du 2 octobre 2015 de pouvoir légalement prévoir une période d'essai, la décision contestée devait respecter les dispositions des articles 45-2 et 47 du décret du 17 janvier 1986, y compris la faculté pour Mme B...de prendre connaissance de son dossier et sa convocation à un entretien préalable, la cour n'a pas commis d'erreur de droit.<br/>
<br/>
              6. Enfin, il ne ressort pas des pièces du dossier soumis aux juges du fond que les difficultés rencontrées par Mme B...dans l'exercice de ses fonctions au CSA, qui se traduisaient par des tensions avec sa hiérarchie qui lui reprochait un manque de pédagogie dans ses analyses et une propension à ne pas faire valider celles-ci, sans que ses compétences professionnelles d'analyste financière soient remises en cause, aient caractérisé une inaptitude dans l'exercice de ses fonctions susceptible de justifier un licenciement pour insuffisance professionnelle au sens des dispositions citées au point 2. Dès lors, en retenant que les pièces du dossier ne permettaient pas de justifier son licenciement pour insuffisance professionnelle, la cour n'a ni dénaturé les pièces du dossier, ni inexactement qualifié les faits de l'espèce.<br/>
<br/>
              7. Il résulte de tout ce qui précède que le CSA n'est pas fondé à demander l'annulation de l'arrêt attaqué. Ses conclusions au titre de l'article L. 761-1 du code de justice administrative doivent, par suite, être rejetées. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du CSA la somme de 3 000 euros à verser à Mme B...au titre de ces mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du Conseil supérieur de l'audiovisuel est rejeté.<br/>
Article 2 : Le Conseil supérieur de l'audiovisuel versera à Mme B...une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée au Conseil supérieur de l'audiovisuel et à Mme A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
