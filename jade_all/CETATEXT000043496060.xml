<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043496060</ID>
<ANCIEN_ID>JG_L_2021_05_000000452052</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/49/60/CETATEXT000043496060.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 07/05/2021, 452052, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-05-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>452052</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:452052.20210507</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
              Par une requête, enregistrée le 27 avril 2021 au secrétariat du contentieux du Conseil d'Etat, M. A... B... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de la décision qui interdit à tous les citoyens de pays extérieurs à l'Union européenne de demander un visa pour la France afin d'y retrouver leur compagnon ou leur compagne ; <br/>
<br/>
              2°) d'enjoindre, par voie de conséquence, à l'administration de délivrer en urgence un visa à son amie ;<br/>
<br/>
              3°) de condamner l'Etat à de lui verser une indemnité d'un montant de 2 000 euros au titre du préjudice subi du fait de leur séparation forcée pendant plusieurs mois.<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - la condition d'urgence est satisfaite dès lors qu'il n'a pas pu voir sa compagne depuis le mois de janvier 2021 et souhaite passer avec elle ses congés du mois de mai, les suivants étant en septembre ; <br/>
              - il est porté une atteinte grave et manifestement illégale au droit de mener une vie de couple normale dès lors que, depuis l'intervention du décret n° 2021-99 du 30 janvier 2021, il ne leur est plus possible de se revoir.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - le décret n° 2020-1310 du 29 octobre 2020 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du même code : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              Sur le cadre juridique du litige :<br/>
<br/>
              2. Aux termes de l'article L. 3131-12 du code de la santé publique, issu de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19 : " L'état d'urgence sanitaire peut être déclaré sur tout ou partie du territoire métropolitain (...) en cas de catastrophe sanitaire mettant en péril, par sa nature et sa gravité, la santé de la population ". L'article L. 3131-13 du même code précise que " L'état d'urgence sanitaire est déclaré par décret en conseil des ministres pris sur le rapport du ministre chargé de la santé. Ce décret motivé détermine la ou les circonscriptions territoriales à l'intérieur desquelles il entre en vigueur et reçoit application. Les données scientifiques disponibles sur la situation sanitaire qui ont motivé la décision sont rendues publiques. / (...) ". Aux termes de l'article L. 3131-15 du même code : " I.- Dans les circonscriptions territoriales où l'état d'urgence sanitaire est déclaré, le Premier ministre peut, par décret réglementaire pris sur le rapport du ministre chargé de la santé, aux seules fins de garantir la santé publique : / (...) 1° Réglementer ou interdire la circulation des personnes (...). " Ces mesures " sont strictement proportionnées aux risques sanitaires encourus et appropriées aux circonstances de temps et de lieu. Il y est mis fin sans délai lorsqu'elles ne sont plus nécessaires. "<br/>
<br/>
              3. L'émergence d'un nouveau coronavirus, responsable de la maladie à coronavirus 2019 ou Covid-19 et particulièrement contagieux, a été qualifiée d'urgence de santé publique de portée internationale par l'Organisation mondiale de la santé le 30 janvier 2020, puis de pandémie le 11 mars 2020. La propagation du virus sur le territoire français a conduit les autorités compétentes à prendre diverses mesures destinées à réduire les risques de contagion. Une nouvelle progression de l'épidémie de covid-19 sur le territoire national a conduit le Président de la République à prendre, le 14 octobre 2020, sur le fondement des articles L. 3131-12 et L. 3131-13 du code de la santé publique, un décret déclarant l'état d'urgence sanitaire à compter du 17 octobre 2020 sur l'ensemble du territoire de la République. L'article 1er de la loi du 14 novembre 2020 autorisant la prorogation de l'état d'urgence sanitaire et portant diverses mesures de gestion de la crise sanitaire, modifié par la loi du 15 février 2021, proroge l'état d'urgence sanitaire jusqu'au 1er juin 2021 inclus. Le Premier ministre a pris, sur le fondement de l'article L. 3131-15 du code de la santé publique, le décret du 29 octobre 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19. Le décret susvisé du 30 janvier 2021 a inséré dans ce décret un article 56-5 qui prévoit, dans sa rédaction actuellement applicable, que : " I. - Sont interdits, sauf s'ils sont fondés sur un motif impérieux d'ordre personnel ou familial, un motif de santé relevant de l'urgence ou un motif professionnel ne pouvant être différé, les déplacements de personnes : / 1° Entre le territoire métropolitain et un pays étranger autre que ceux de l'Union européenne, Andorre, l'Australie, la Corée du Sud, l'Islande, Israël, le Japon, le Liechtenstein, Monaco, la Norvège, la Nouvelle-Zélande, le Royaume-Uni, Saint-Marin, le Saint-Siège, Singapour ou la Suisse ; / (...). ". Le Premier ministre, par une circulaire n° 6248/SG du 22 février 2021, a déterminé les catégories de personnes qui, " arrivant aux frontières extérieures de l'espace européen, (...) sont autorisées à entrer sur le territoire métropolitain " en limitant l'entrée sur le territoire métropolitain au " ressortissant de nationalité français ainsi que son conjoint (marié, pacsé et concubin sur présentation de justificatifs de communauté de vie) ".<br/>
<br/>
              Sur la demande en référé :<br/>
<br/>
              4. Le requérant doit être regardé comme demandant au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice, de suspendre l'exécution de cette instruction en tant qu'elle ne prévoit pas de dérogations aux restrictions de déplacement vers la France pour les ressortissants étrangers qui ont une relation de couple avec un ressortissant français sans être mariés, pacsés et en situation de concubinage et, par voie de conséquence, d'ordonner à l'administration de délivrer un visa à son amie. Il demande également au juge des référés de condamner l'Etat à lui verser une indemnité de 2 000 euros au titre du préjudice qu'il estime avoir subi.<br/>
<br/>
              5. En premier lieu, il n'appartient pas au juge des référés, qui ne prend que des mesures provisoires, de condamner au paiement d'une indemnité.<br/>
<br/>
              6. En deuxième lieu, à la date de la présente décision, la situation sanitaire reste à un niveau préoccupant sur le territoire national. Le dernier point épidémiologique hebdomadaire de Santé publique France, en date du 29 avril 2021, fait état de plus de 29 000 nouveaux cas positifs en moyenne chaque jour et de plus de 30 000 personnes hospitalisées pour covid-19 au 27 avril 2021, dont 5 959 en services de soins critiques, engendrant une très forte tension hospitalière, ainsi que d'un risque important de circulation de nouveaux variants du virus ayant une forte contagiosité dans une période où la vaccination de la population est encore en cours. Dans le contexte épidémique actuel, le maintien de mesures de restriction d'entrée sur le territoire français aux ressortissants étrangers engagés dans une relation de couple avec un ressortissant français sans être mariés, pacsés ou vivre en concubinage ne peut être regardé comme une mesure n'étant manifestement pas nécessaire, adaptée et proportionnée à l'objectif de sauvegarde de la santé public et portant une atteinte manifestement disproportionnée à la vie privée et familiale. Dans ces conditions, il est manifeste que la demande de M. B... est mal fondée.<br/>
<br/>
              7. Il résulte de ce qui précède que la requête de M. B... doit être rejetée selon la procédure prévue à l'article L. 522-3 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. B... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. A... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
