<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032509773</ID>
<ANCIEN_ID>JG_L_2016_05_000000389486</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/50/97/CETATEXT000032509773.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 06/05/2016, 389486, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-05-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389486</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD ; SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>M. Frédéric Puigserver</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean Lessi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:389486.20160506</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés les 14 avril et 15 juillet 2015 au secrétariat de la section du contentieux du Conseil d'Etat, Mme E...C..., M. G... H..., M. F...M..., M. A...I..., Mme B...P..., Mme K...J..., Mme O...R..., M. L...N..., M. S...et Mme D...Q...demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 21 juillet 2014 de l'Union nationale des caisses d'assurance maladie relative à la liste des actes et prestations pris en charge par l'assurance maladie ainsi que la décision du 12 février 2015 par laquelle cette union a rejeté leur recours gracieux ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - le code de la sécurité sociale ;<br/>
              - le code de justice administrative ; <br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Puigserver, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Jean Lessi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de Mme C...et autres, et à la SCP Baraduc, Duhamel, Rameix, avocat de l'Union nationale des caisses d'assurance maladie ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. L'article L. 162-1-7 du code de la sécurité sociale dispose que : " La prise en charge ou le remboursement par l'assurance maladie de tout acte ou prestation réalisé par un professionnel de santé (...) est subordonné à leur inscription sur une liste établie dans les conditions fixées au présent article. (...) / La hiérarchisation des prestations et des actes est établie dans le respect des règles déterminées par des commissions créées pour chacune des professions dont les rapports avec les organismes d'assurance maladie sont régis par une convention mentionnée à l'article L. 162-14-1. (...) / Les conditions d'inscription d'un acte ou d'une prestation, leur inscription et leur radiation sont décidées par l'Union nationale des caisses d'assurance maladie, après avis de la Haute Autorité de santé et de l'Union nationale des organismes d'assurance maladie complémentaire. (...) / Les décisions de l'Union nationale des caisses d'assurance maladie sont réputées approuvées sauf opposition motivée des ministres chargés de la santé et de la sécurité sociale (...) ".<br/>
<br/>
              2. Sur le fondement de ces dispositions, le collège des directeurs de l'Union nationale des caisses d'assurance maladie, par une décision du 11 mars 2005, a arrêté la liste des actes et prestations pris en charge ou remboursés par l'assurance maladie, en prévoyant à son livre III que les actes des auxiliaires médicaux continueraient de relever des dispositions de l'arrêté du 27 mars 1972 modifié relatif à la nomenclature générale des actes professionnels des médecins, des chirurgiens-dentistes, des sages-femmes et des auxiliaires médicaux, sous réserve des modifications que cette décision y apporte. Par la décision attaquée du 21 juillet 2014, le collège des directeurs a modifié cette décision pour apporter des modifications aux dispositions relatives aux perfusions figurant au titre XVI, consacré aux soins infirmiers, de l'arrêté du 27 mars 1972. <br/>
<br/>
              3. En premier lieu, le premier alinéa du II de l'article R. 162-52  du  code de la sécurité sociale dispose que : " Avant de procéder aux consultations rendues obligatoires par le troisième alinéa de l'article L. 162-1-7, l'Union nationale des caisses d'assurance maladie informe de son intention d'inscrire un acte ou une prestation, d'en modifier les conditions d'inscription ou de procéder à sa radiation les ministres chargés de la santé et de la sécurité sociale, l'Union nationale des professionnels de santé, les organisations représentatives des professionnels de santé autorisés à pratiquer l'acte ou la prestation et les organisations représentatives des établissements de santé ".<br/>
<br/>
              4. Il ressort des pièces versées au dossier que l'Union nationale des caisses d'assurance maladie a, par des courriers du 24 juin 2014, informé le ministre des affaires sociales et de la santé, le ministre des finances et des comptes publics, l'Union nationale des professionnels de santé, les organisations représentatives des infirmiers et des sages-femmes et les organisations représentatives des établissements de santé de son intention de modifier la liste des actes et prestations mentionnés à l'article L. 162-1-7 du code de la sécurité sociale en ce qui concerne les actes de perfusions, en joignant les modifications envisagées. Par suite, les requérants ne sont pas fondés à soutenir que l'obligation d'information résultant des dispositions citées ci-dessus de l'article R. 162-52 du code de la sécurité sociale aurait été méconnue. <br/>
<br/>
              5. En second lieu, l'article R. 4312-9 du code de la santé publique dispose que : " L'infirmier ou l'infirmière ne peut aliéner son indépendance professionnelle sous quelque forme que ce soit. Il ne peut notamment accepter une rétribution fondée sur des obligations de rendement qui auraient pour conséquence une restriction ou un abandon de cette indépendance ".<br/>
<br/>
              6. D'une part, la décision attaquée se borne à modifier la désignation et la hiérarchisation des actes liés aux perfusions accomplis par les sages-femmes et les infirmiers, en précisant ce qu'ils impliquent et en assortissant chacun d'eux d'un coefficient et d'une lettre-clé destinés à assurer cette hiérarchisation. Si elle distingue notamment, pour les assortir de coefficients différents, les " séances de perfusion courtes ", d'une " durée inférieure ou égale à une heure ", pour lesquelles la surveillance doit être continue, des séances plus longues, pour lesquelles l'organisation d'une surveillance est possible, elle ne prévoit aucunement que toute séance de perfusion doive nécessairement durer une heure, ni même qu'une séance doive durer une heure pour être prise en charge par l'assurance maladie. Par suite, les requérants ne sont pas fondés à soutenir que les dispositions qu'ils critiquent porteraient atteinte à l'indépendance professionnelle des infirmiers, seraient entachées d'une erreur manifeste d'appréciation ou procéderaient d'un détournement de pouvoir, en visant à réduire le nombre d'actes pris en charge par l'assurance maladie.<br/>
<br/>
              7. D'autre part, il résulte des dispositions de la décision attaquée que le supplément forfaitaire pour surveillance continue d'une perfusion au-delà de la première heure peut faire l'objet d'une prise en charge par l'assurance maladie pour chaque heure commencée, dans la limite d'un total de six heures pour une même séance de perfusion. Par suite, les requérants ne sont pas fondés à soutenir que la décision attaquée, par son imprécision, laisserait un pouvoir d'appréciation excessif aux caisses primaires d'assurance maladie et porterait atteinte, pour ce motif, à l'indépendance professionnelle des infirmiers.<br/>
<br/>
              8. Il résulte de tout ce qui précède que les requérants ne sont pas fondés à demander l'annulation de la décision de l'Union nationale des caisses d'assurance maladie qu'ils attaquent.<br/>
<br/>
              9. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'il soit fait droit aux conclusions des requérants présentées à ce titre. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à leur charge la somme que l'Union nationale des caisses d'assurance maladie demande au même titre.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de Mme C...et des autres requérants est rejetée.<br/>
Article 2 : Les conclusions de l'Union nationale des caisses d'assurance maladie présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à Mme E...C..., première requérante dénommée, et à l'Union nationale des caisses d'assurance maladie. <br/>
Les autres requérants seront informés de la présente décision par la SCP Gaschignard, avocat au Conseil d'Etat et à la Cour de cassation, qui les représente devant le Conseil d'Etat.<br/>
Copie en sera adressée à la ministre des affaires sociales et de la santé. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
