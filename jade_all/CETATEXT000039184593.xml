<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039184593</ID>
<ANCIEN_ID>JG_L_2019_10_000000418224</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/18/45/CETATEXT000039184593.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 04/10/2019, 418224</TITRE>
<DATE_DEC>2019-10-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418224</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>M. Sylvain Monteillet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:418224.20191004</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Saint-Denis prononcer le remboursement de la somme de 42 787,44 euros correspondant à la participation pour non-réalisation d'aires de stationnement prévue par un permis de construire délivré le 14 novembre 2006. Par un jugement n° 1200781 du 12 mars 2015, le tribunal administratif de Saint-Denis a fait droit à sa demande.<br/>
<br/>
              Par une décision n° 391024 du 2 mars 2017, le Conseil d'Etat, statuant au contentieux, saisi par la commune de Saint-Pierre d'un pourvoi en cassation contre ce jugement, a attribué le jugement de la requête à la cour administrative d'appel de Bordeaux.<br/>
<br/>
              Par un arrêt n° 17BX00785 du 19 décembre 2017, la cour administrative d'appel de Bordeaux a annulé le jugement du tribunal administratif de Saint-Denis du 12 mars 2015 et a condamné la commune de Saint-Pierre à restituer la somme de 42 787,44 euros à M. B....<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire, un mémoire en réplique et un nouveau mémoire, enregistrés les 15 février, 4 mai et 21 décembre 2018 et le 12 septembre 2019 au secrétariat du contentieux du Conseil d'Etat, la commune de Saint-Pierre demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'article 2 de cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de M. B... la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution ;<br/>
              - le code de l'urbanisme ;<br/>
              - le livre des procédures fiscales ; <br/>
              - la loi n° 76-1285 du 31 décembre 1976 ;<br/>
              - la loi n° 2008-561 du 17 juin 2008 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Sylvain Monteillet, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de la commune de Saint-pierre et à la SCP Spinosi, Sureau, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que le maire de la commune de Saint-Pierre (La Réunion) a délivré le 14 novembre 2006 un permis de construire à M. B... et a mis à sa charge une participation pour non-réalisation d'aires de stationnement. Le 20 septembre 2011, le maire a émis un titre de recette fixant le montant de cette participation à 42 787,44 euros. Après s'être acquitté de cette somme, M. B... a demandé au tribunal administratif de Saint-Denis d'en ordonner le remboursement. Par un jugement du 12 mars 2015, le tribunal administratif a condamné la commune de Saint-Pierre à rembourser la somme de 42 787,44 euros. La commune se pourvoit en cassation contre l'arrêt du 19 décembre 2017 par lequel la cour administrative d'appel de Bordeaux, après avoir annulé le jugement du tribunal administratif, l'a condamnée à restituer la somme de 42 787,44 euros à M. B.... Eu égard aux moyens soulevés, son pourvoi doit être regardé comme tendant seulement à l'annulation de l'article 2 de cet arrêt.<br/>
<br/>
              2. Aux termes de l'article L. 421-3 du code de l'urbanisme dans sa rédaction applicable à la date de délivrance du permis de construire : " (...) Lorsque le pétitionnaire ne peut satisfaire lui-même aux obligations imposées par un document d'urbanisme en matière de réalisation d'aires de stationnement, il peut être tenu quitte de ces obligations en justifiant, pour les places qu'il ne peut réaliser lui-même sur le terrain d'assiette ou dans son environnement immédiat, soit de l'obtention d'une concession à long terme dans un parc public de stationnement existant ou en cours de réalisation, soit de l'acquisition de places dans un parc privé de stationnement existant ou en cours de réalisation. / (...) A défaut de pouvoir réaliser l'obligation prévue au quatrième alinéa, le pétitionnaire peut être tenu de verser à la commune une participation fixée par le conseil municipal, en vue de la réalisation de parcs publics de stationnement. (...) / Un décret en Conseil d'Etat détermine les conditions d'application des quatrième et cinquième alinéas du présent article et précise notamment les modalités d'établissement, de liquidation et de recouvrement de la participation prévue au quatrième alinéa, ainsi que les sanctions et garanties y afférentes.  / (...) ". Aux termes de l'article R. 332-20 du même code : " La participation est recouvrée en vertu d'un titre de recette émis au vu du permis de construire par l'ordonnateur de la commune (...) " et aux termes de l'article R. 332-21 du même code : " L'action en recouvrement de la participation pour non-réalisation d'aires de stationnement dont dispose l'administration peut être exercée jusqu'à l'expiration de la quatrième année suivant celle au cours de laquelle le permis de construire a été délivré. La prescription est interrompue dans les conditions définies à l'article 1975 du code général des impôts ". Cette dernière disposition n'a pas pour objet de fixer au comptable le délai maximum dans lequel il peut procéder au recouvrement des sommes mentionnées sur le titre de recette mais d'imposer à l'ordonnateur un délai maximum, à compter du fait générateur de la participation, pour émettre, à peine de prescription, le titre de recette.<br/>
<br/>
              3. En premier lieu, il résulte des dispositions de l'article L. 421-3 du code de l'urbanisme alors en vigueur, rappelées ci-dessus, que le législateur a renvoyé au pouvoir règlementaire le soin de fixer les modalités de liquidation et de recouvrement de la participation pour non-réalisation d'aires de stationnement. La règle de prescription de l'article R. 332-21 de ce code a été prise sur ce fondement. La cour n'a, par suite, pas commis d'erreur de droit en écartant le moyen tiré de ce que cet article serait entaché d'inconstitutionnalité au motif qu'il empièterait sur le domaine de la loi dès lors qu'elle a relevé que la constitutionnalité de l'article L 421-3, habilitant ainsi le pouvoir réglementaire à poser une règle de prescription, n'était pas contestée.<br/>
<br/>
              4. En second lieu, aux termes de l'article 2224 du code civil, dans sa rédaction issue de la loi du 17 juin 2008 portant réforme de la prescription en matière civile : " Les actions personnelles ou mobilières se prescrivent par cinq ans à compter du jour où le titulaire d'un droit a connu ou aurait dû connaître les faits lui permettant de l'exercer ".<br/>
<br/>
              5. Pour juger que la commune de Saint-Pierre ne pouvait utilement soutenir que l'article R. 332-21 du code de l'urbanisme avait implicitement été abrogé par l'entrée en vigueur de l'article 2224 du code civil dans sa version issue de la loi du 17 juin 2008, la cour a relevé que l'article 2224 du code civil n'était susceptible de s'appliquer qu'en ce qui concerne la prescription des actions en recouvrement d'une créance publique. En statuant ainsi, alors que cet article du code civil s'applique également à la prescription d'assiette, la cour a commis une erreur de droit.<br/>
<br/>
              6. Toutefois, il résulte de l'article 2223 du code civil que les dispositions de l'article 2224 de ce code ne font pas obstacle à l'application des règles spéciales prévues par d'autres lois. La règle de prescription quadriennale fixée par l'article R. 332-21 du code de l'urbanisme en application de l'article L 421-3 du même code, dans sa rédaction applicable au litige, doit ainsi être regardée comme une règle spéciale rendant inapplicable la règle de la prescription quinquennale prévue par l'article 2224 du code civil. Ce motif, dont l'examen n'implique l'appréciation d'aucune circonstance de fait, doit être substitué au motif retenu par l'arrêt attaqué dont il justifie légalement sur ce point le dispositif.<br/>
<br/>
              7. Il résulte de tout ce qui précède que la commune de Saint-Pierre n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              8. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que M. B... verse à la commune de Saint-Pierre la somme qu'elle réclame sur ce fondement. La commune de Saint-Pierre versera à M. B... une somme de 3 000 euros au titre de ces dispositions.  <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la commune de Saint-Pierre est rejeté.<br/>
<br/>
Article 2 : La commune de Saint-Pierre versera à M. B... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la commune de Saint-Pierre et à M. A... B....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">18-03 COMPTABILITÉ PUBLIQUE ET BUDGET. CRÉANCES DES COLLECTIVITÉS PUBLIQUES. - PRESCRIPTION - DÉLAI QUINQUENNAL DE DROIT COMMUN (ART. 2224 DU CODE CIVIL) [RJ1] - APPLICATION À LA PRESCRIPTION D'ASSIETTE - EXISTENCE.
</SCT>
<ANA ID="9A"> 18-03 L'article 2224 du code civil s'applique non seulement à la prescription des actions en recouvrement d'une créance publique mais également à la prescription d'assiette.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur le caractère de droit commun de ce délai s'agissant des créances des personnes publiques, CE, 28 mai 2014,,et,, n°s 376501 376573, p. 143 ; CE, 31 mars 2017,,et,, n° 405797, p. 104. Rappr., sur l'application aux créances publiques de l'ancienne prescription trentenaire alors prévue par l'article 2262 du code civil, CE, Section, 13 décembre 1935, Ministre des colonies c/ Compagnie des messageries fluviales de Cochinchine, n° 24102, p. 1186 ; CE, Assemblée, 13 mai 1960, Secrétaire d'Etat à l'agriculture c/,, n° 34197, p. 328.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
