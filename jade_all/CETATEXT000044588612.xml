<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044588612</ID>
<ANCIEN_ID>JG_L_2021_12_000000454335</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/58/86/CETATEXT000044588612.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 27/12/2021, 454335, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>454335</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Charles-Emmanuel Airy</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:454335.20211227</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une protestation, enregistrée le 6 juillet 2021 au secrétariat du contentieux du Conseil d'Etat, M. E... J... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler les opérations électorales qui se sont déroulées les 20 et 27 juin 2021 en vue de l'élection des conseillers régionaux de la région des Hauts-de-France ;<br/>
<br/>
              2°) de rejeter le compte de campagne de M. F... G... et de déclarer ce dernier inéligible pour une durée de trois ans.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code électoral ;<br/>
              - le code de justice administrative, notamment son article R. 611-8 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Charles-Emmanuel Airy, auditeur,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteure publique ;<br/>
<br/>
<br/>
              Vu la note en délibéré, enregistrée le 20 décembre 2021, présentée par M. J... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. A l'issue du second tour des opérations électorales qui se sont déroulées les 20 et 27 juin 2021 pour l'élection des conseillers régionaux de la région Hauts-de-France, la liste " Se battre avec vous ", conduite par M. G..., a obtenu 708 518 voix et 52,37% des suffrage exprimés, la liste " Une région qui vous protège avec Sébastien B... ", conduite par M. B..., 346 918 voix et 25,64% des suffrages exprimés et la liste " Pour le climat, pour l'emploi avec Karima C.... Union de la gauche et des écologistes ", conduite par Mme C..., 297 395 voix et 21,98% des suffrages exprimés. M. J... demande au Conseil d'Etat d'annuler ces opérations électorales.<br/>
<br/>
              Sur la question prioritaire de constitutionnalité :<br/>
<br/>
              2. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              3. L'article L. 361 du code électoral prévoit que les élections au conseil régional peuvent être contestées dans les dix jours suivant la proclamation des résultats par tout candidat ou tout électeur de la région devant le Conseil d'Etat statuant au contentieux. <br/>
<br/>
              4. En premier lieu, l'existence d'un double degré de juridiction n'ayant pas par lui-même valeur constitutionnelle, il ne peut être sérieusement soutenu qu'en prévoyant l'existence d'un recours direct devant le Conseil d'Etat, ces dispositions porteraient atteinte au droit à un recours effectif garanti par l'article 16 de la Déclaration des droits de l'homme et du citoyen.<br/>
<br/>
              5. En second lieu, si le requérant fait valoir que les dispositions de l'article L. 222 du code électoral prévoient que les élections au conseil départemental peuvent être contestées devant le tribunal administratif, le principe d'égalité ne s'oppose ni à ce que le législateur règle de façon différente des situations différentes ni à ce qu'il déroge à l'égalité pour des raisons d'intérêt général pourvu que, dans l'un et l'autre cas, la différence de traitement qui en résulte soit en rapport direct avec l'objet de la loi qui l'établit. Eu égard aux différences entre les élections au conseil régional et les élections au conseil départemental, le grief tiré de ce que le législateur aurait méconnu le principe d'égalité en prévoyant des règles différentes de compétence au sein de la juridiction administrative pour connaître des protestations dirigées contre ces élections, ne présente pas un caractère sérieux. <br/>
<br/>
              6. Il résulte de ce qui précède que la question prioritaire de constitutionnalité soulevée, qui n'est pas nouvelle, ne présente pas de caractère sérieux. Par suite, il n'y a pas lieu de la transmettre au Conseil constitutionnel. <br/>
<br/>
              Sur les conclusions tendant à l'annulation des opérations électorales :<br/>
<br/>
              7. En premier lieu, M. J... fait valoir que le taux d'abstention élevé au second tour de scrutin, qui résulterait du contexte sanitaire dégradé et des insuffisances des services de l'Etat dans la distribution de la propagande électorale, aurait porté atteinte à la sincérité du scrutin. Toutefois, le niveau d'abstention n'est pas, par lui-même, de nature à remettre en cause les résultats du scrutin s'il n'a pas altéré, dans les circonstances de l'espèce, sa sincérité. Les circonstances invoquées par le protestataire, qui ne sont pas propres au déroulement de la campagne électorale ou du scrutin dans la région des Hauts-de France, ne sont pas susceptibles d'établir qu'il aurait été porté atteinte au libre exercice du droit de vote ou à l'égalité entre les candidats. Dans ces conditions, le grief ne peut qu'être écarté.<br/>
<br/>
              8. En deuxième lieu, il ne résulte pas de l'instruction que la circonstance qu'un nombre significatif d'électeurs n'ont pas reçu les documents électoraux en raison de dysfonctionnements d'acheminement a été de nature à altérer la sincérité du scrutin, dès lors que ces dysfonctionnements ont affecté l'ensemble des listes et que l'écart des voix entre les différentes listes en présence a été important. <br/>
<br/>
              9. En troisième lieu, aux termes de l'article L. 52-8 du code électoral, " (...) Les personnes morales, à l'exception des partis ou groupements politiques, ne peuvent participer au financement de la campagne électorale d'un candidat, ni en lui consentant des dons sous quelque forme que ce soit, ni en lui fournissant des biens, services ou autres avantages directs ou indirects à des prix inférieurs à ceux qui sont habituellement pratiqués ". <br/>
<br/>
              10. D'une part, le " temps d'échange " avec les maires auquel M. G... a été invité, en qualité de président de région, par l'association des maires et des présidents d'intercommunalité du Pas-de-Calais le 20 mars 2021 relève de l'exercice habituel du mandat des élus des collectivités territoriales. Une telle invitation ne saurait caractériser, en l'absence de circonstance particulière, un concours d'une personne morale à une campagne électorale. En l'espèce, le requérant n'apporte aucun élément de nature à établir que cette rencontre aurait été l'occasion d'échanges relatifs à la candidature de M. G.... Par suite, le grief tiré de ce que cette invitation serait constitutive d'un avantage prohibé par l'article L. 52-8 du code électoral ne peut qu'être écarté.<br/>
<br/>
              11. D'autre part, il ne résulte pas de l'instruction que la mise à disposition d'autres collectivités locales par la région Hauts-de-France de masques de protection puisse être regardée comme une action visant à soutenir la campagne électorale de la liste conduite par M. G.... Cette mise à disposition ne saurait par suite être constitutive d'un don prohibé par l'article L. 52-8 du code électoral.<br/>
<br/>
              12. En quatrième lieu, la circonstance que la liste de M. G... ait utilisé des affiches électorales distinctes au soutien des différentes sections départementales de la liste qu'il conduisait pour l'élection régionale dans la région des Hauts-de-France ne peut être regardée comme une irrégularité de nature à altérer la sincérité du scrutin.<br/>
<br/>
              13. Il résulte de ce qui précède que M. J... n'est pas fondé à demander l'annulation des opérations électorales des 20 et 27 juin 2021. <br/>
<br/>
              Sur les conclusions tendant au rejet du compte de campagne de M. G... :<br/>
<br/>
              14. Il résulte de ce qui a été dit aux points 10 et 11 que les conclusions tendant au rejet du compte de campagne de M. G..., qui sont soulevées par voie de conséquences des griefs relatifs au financement de sa campagne électorale, doivent être rejetées. <br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 118-4 du code électoral :<br/>
<br/>
              15. Aux termes du premier alinéa de l'article L.118-4 du même code : " Saisi d'une contestation formée contre l'élection, le juge de l'élection peut déclarer inéligible, pour une durée maximale de trois ans, le candidat qui a accompli des manœuvres frauduleuses ayant eu pour objet ou pour effet de porter atteinte à la sincérité du scrutin. ".  En l'absence de toute manœuvre frauduleuse ayant eu pour objet ou pour effet de porter atteinte à la sincérité du scrutin, les conclusions tendant à l'application des dispositions de l'article L. 118-4 du code électoral ne peuvent qu'être rejetées. <br/>
<br/>
              16. Il résulte de tout ce qui précède que la protestation de M. J... doit être rejetée. <br/>
<br/>
              17. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre une somme à la charge de M. J... au titre des dispositions de l'article L. 761-1 du code de justice administrative<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de transmettre au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M. J....<br/>
Article 2 : La protestation de M. J... est rejetée.<br/>
Article 3 : Les conclusions présentées par M. G... et ses colistiers sur le fondement de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à M. E... J..., à M. F... G... et au ministre de l'intérieur.<br/>
Copie en sera adressée au Conseil constitutionnel, à la Commission nationale des comptes de campagne et des financements politiques et au Premier ministre.<br/>
              Délibéré à l'issue de la séance du 16 décembre 2021 où siégeaient : M. Pierre Collin, président de chambre, présidant ; M. Mathieu Herondart, conseiller d'Etat et M. I... A..., auditeur-rapporteur. <br/>
<br/>
              Rendu le 27 décembre 2021.<br/>
<br/>
                 Le président : <br/>
                 Signé : M. Pierre Collin<br/>
 		Le rapporteur : <br/>
      Signé : M. Charles-Emmanuel Airy<br/>
                 La secrétaire :<br/>
                 Signé : Mme H... D...<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
