<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027656266</ID>
<ANCIEN_ID>JG_L_2013_07_000000360255</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/65/62/CETATEXT000027656266.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 5ème sous-sections réunies, 03/07/2013, 360255, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-07-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>360255</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 5ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>M. Hervé Guichon</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:360255.20130703</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête sommaire et le mémoire complémentaire, enregistrés les 15 juin et 13 septembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour le syndicat parisien des administrations centrales économiques et financières, dont le siège est 120 rue de Bercy, Bâtiment Necker, Pièce 2291 à Paris (Cedex 12 - 75552), représenté par son secrétaire général, et par l'association syndicale du contrôle général économique et financier, dont le siège est 139 rue de Bercy à Paris (75012), représentée par son président ; le syndicat et l'association demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du 18 avril 2012 portant nomination de M. A... B...en qualité de contrôleur général économique et financier de 1ère classe ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule ; <br/>
<br/>
              Vu la loi n° 84-834 du 13 septembre 1984 ; <br/>
<br/>
              Vu le décret n° 94-1085 du 14 décembre 1994 ; <br/>
<br/>
              Vu le décret n° 2005-436 du 9 mai 2005 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Hervé Guichon, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat du syndicat parisien des administrations centrales, économiques et financières et de l'association syndicale du contrôle général économique et financier ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article 8 de la loi du 13 septembre 1984 : " A l'exception de ceux de ces corps dont la mission le justifie et dont la liste est déterminée par décret en Conseil d'Etat, les statuts particuliers des corps d'inspection et de contrôle doivent prévoir la possibilité de pourvoir aux vacances d'emploi dans le grade d'inspecteur général ou de contrôleur général par décret en conseil des ministres sans condition autre que d'âge. (...) / Néanmoins, à l'exception des nominations dans les corps de l'inspection générale des finances, de l'inspection générale de l'administration et de l'inspection générale des affaires sociales, les nominations prononcées au titre de l'alinéa précédent ne peuvent intervenir qu'après consultation d'une commission chargée d'apprécier l'aptitude des intéressés à exercer les fonctions d'inspecteur général ou de contrôleur général en tenant compte de leurs fonctions antérieures et de leur expérience. (...) " ;<br/>
<br/>
              2. Considérant que, si ces dispositions laissent une liberté de choix au Gouvernement pour procéder aux nominations au tour extérieur dans les corps d'inspection et de contrôle, elles ne le dispensent pas de respecter la règle posée par l'article 6 de la Déclaration des droits de l'homme et du citoyen, selon laquelle " tous les citoyens (...) sont également admissibles à toutes dignités, places et emplois public, selon leur capacité et sans autre distinction que celle de leurs vertus et de leurs talents " ; que, pour les nominations dans ces corps, l'appréciation des capacités des candidats à laquelle se livre l'autorité investie du pouvoir de nomination doit s'effectuer en tenant compte, au vu notamment de l'avis de la commission d'aptitude instituée par la loi, des attributions confiées aux membres du corps concerné et des conditions dans lesquelles ils exercent leurs fonctions ; <br/>
<br/>
              3. Considérant que les membres du corps du contrôle général économique et financier sont chargés, en particulier, en application de l'article 1er du décret du 9 mai 2005 portant statut de ce corps, de missions d'inspection, d'audit, d'évaluation, d'étude et de conseil dans le domaine économique et financier en vue de l'amélioration de la gestion publique, ainsi que du contrôle financier des administrations de l'Etat et de ses établissements publics ; qu'ils sont également chargés de toute mission que les ministres chargés de l'économie, du budget et de l'industrie leurs confient ; qu'il ne ressort d'aucune pièce du dossier que les fonctions précédemment exercées par M. B... au sein de collectivités territoriales, principalement dans le domaine de la communication, puis de secrétaire général d'un groupe parlementaire à l'Assemblée nationale et, pendant dix mois, de directeur adjoint du cabinet du ministre chargé de la fonction publique,  l'aient conduit à développer des capacités d'analyse et d'expertise en matière de finances publiques, ni d'acquérir les aptitudes lui permettant d'exercer des fonctions de la nature et du niveau de celles assurées par le contrôle général économique et financier ; que, dès lors, en estimant que M. B...était apte à être nommé au tour extérieur contrôleur général économique et financier, l'auteur du décret attaqué a entaché son appréciation d'une erreur manifeste ; que, par suite et sans qu'il soit besoin d'examiner l'autre moyen de la requête, les requérants sont fondés à demander l'annulation du décret attaqué ; <br/>
<br/>
              4. Considérant qu'il y a lieu de mettre à la charge de l'Etat le versement au syndicat parisien des administrations centrales économiques et financières et à l'association syndicale du contrôle général économique et financier de la somme de 1 750 euros chacun au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces mêmes dispositions font obstacle à ce qu'il soit fait droit aux conclusions présentées par M. B...au même titre ;<br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : Le décret du 18 avril 2012 est annulé.  <br/>
Article 2 : l'Etat versera au syndicat parisien des administrations centrales économiques et financières et à l'association syndicale du contrôle général économique et financier la somme de 1 750 euros chacun au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Les conclusions présentées par M. B...au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au syndicat parisien des administrations centrales économiques et financières, à l'association syndicale du contrôle général économique et financier, à M. A...B..., au Premier ministre et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
