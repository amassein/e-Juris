<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029003687</ID>
<ANCIEN_ID>JG_L_2014_05_000000367665</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/00/36/CETATEXT000029003687.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 28/05/2014, 367665, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-05-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>367665</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SPINOSI</AVOCATS>
<RAPPORTEUR>Mme Anne Iljic</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Delphine Hedary</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:367665.20140528</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 12 et 29 avril 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. A...B..., demeurant ...; M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 12DA01420 du 12 mars 2013 par laquelle le juge des référés de la cour administrative d'appel de Douai a rejeté sa requête tendant à l'annulation de l'ordonnance n° 1200755 du 3 septembre 2012 par laquelle le juge des référés du tribunal administratif de Rouen, statuant sur le fondement de l'article R. 541-1 du code de justice administrative, a rejeté sa demande tendant à la condamnation de l'Etat à lui verser une provision de 2 800 euros en réparation du préjudice qu'il estime avoir subi du fait de ses conditions de détention à la maison d'arrêt de Rouen ; <br/>
<br/>
              2°) statuant en référé, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 000  euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
<br/>
              Vu le code de procédure pénale ;<br/>
<br/>
              Vu la loi n° 2009-1436 du 24 novembre 2009 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anne Iljic, auditeur,<br/>
<br/>
              - les conclusions de Mme Delphine Hedary, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Spinosi, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis au juge des référés de la cour administrative d'appel de Douai que M. B...a été détenu à ...; qu'estimant ses conditions de détention contraires au respect de la dignité humaine et de la vie privée, il a saisi le juge des référés du tribunal administratif de Rouen d'une demande tendant au versement d'une provision de 2 800 euros en réparation du préjudice qu'il estime avoir subi ; qu'il se pourvoit en cassation contre l'ordonnance du 12 mars 2013 par laquelle le juge des référés de la cour administrative d'appel de Douai a rejeté sa requête tendant à l'annulation de l'ordonnance du 3 septembre 2012 par laquelle le juge des référés du tribunal administratif de Rouen a rejeté sa demande ; <br/>
<br/>
              Sur la régularité de l'ordonnance attaquée : <br/>
<br/>
              2. Considérant, en premier lieu, qu'il ressort des énonciations de l'ordonnance attaquée qu'après avoir analysé le moyen dont il était saisi,  tiré de la violation des stipulations l'article 3 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales à raison de l'aménagement des cellules dans lesquelles le requérant a été détenu et en avoir rappelé la teneur dans les motifs de sa décision, le juge des référés de la cour administrative d'appel de Douai a estimé que les conditions de détention de M. B...n'étaient pas telles qu'elles caractérisent une méconnaissance des articles D. 350 et D. 351 du code de procédure pénale, qui rappellent les exigences minimales relatives à l'hygiène, à la salubrité et à l'agencement des locaux de détention ; qu'ainsi, il doit être regardé comme s'étant prononcé sur le moyen analysé ci-dessus ; qu'il suit de là que le moyen tiré de l'insuffisance de motivation de l'arrêt attaqué doit être écarté ; <br/>
<br/>
              3. Considérant, en second lieu, qu'il ressort des termes mêmes de l'ordonnance attaquée que le juge des référés a procédé à une appréciation concrète des conditions de détention du requérant pour juger que le défaut de détention en cellule individuelle n'était pas de nature à constituer, à lui seul, un traitement inhumain et dégradant ; que, dès lors, le moyen tiré de ce qu'il aurait dénaturé les écritures qui lui étaient soumises en estimant que le requérant se bornait à soutenir que le défaut d'encellulement individuel constitue en lui-même un traitement inhumain et dégradant ne peut qu'être écarté ; <br/>
<br/>
              Sur le bien-fondé de l'ordonnance attaquée : <br/>
<br/>
              En ce qui concerne la méconnaissance, par le juge des référés, de son propre office : <br/>
<br/>
              4. Considérant, en premier lieu, qu'il résulte des dispositions de l'article R. 541-1 du code de justice administrative qu'il appartient au juge des référés de s'assurer que les éléments qui lui sont soumis par les parties sont de nature à établir l'existence d'une obligation non sérieusement contestable avec un degré suffisant de certitude ; qu'en l'espèce, le juge des référés de la cour administrative d'appel de Douai s'est borné, sur la base des allégations des parties et au vu de l'ensemble des éléments en sa possession, à juger que le requérant n'apportait pas de précisions suffisantes sur ses conditions de détention pour que soit regardée comme non sérieusement contestable l'obligation invoquée devant lui ; qu'il suit de là que le moyen tiré de ce que le juge des référés aurait commis une erreur de droit en faisant à tort peser la charge de la preuve sur M. B...ne peut qu'être écarté ;<br/>
<br/>
              5. Considérant, en second lieu, qu'il appartient au juge des référés, saisi d'un moyen en ce sens, de contrôler si les dispositions réglementaires ou législatives dont il fait application ne sont pas manifestement incompatibles avec des normes internationales ; qu'en l'espèce, le juge des référés de la cour administrative d'appel de Douai, après avoir examiné si les dispositions de l'article 100 de la loi du 24 novembre 2009 étaient manifestement incompatibles avec les stipulations de l'article 3 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, a jugé que ce moyen n'était pas de nature, eu égard à son office, à ce que soit regardée comme non sérieusement contestable l'obligation invoquée devant lui ; qu'il suit de là que le moyen tiré de ce que le juge des référés aurait commis une erreur de droit en jugeant qu'il ne lui appartenait pas de procéder à un contrôle de conventionalité des dispositions précitées ne peut qu'être écarté ;<br/>
<br/>
              En ce qui concerne la méconnaissance des stipulations des articles 3 et 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : <br/>
<br/>
              6. Considérant qu'en jugeant, par une appréciation souveraine des faits exempte de dénaturation, que M. B...n'apportait pas de précisions suffisantes sur les conditions de détention auxquelles il avait été exposé pour que soit regardée comme non sérieusement contestable l'obligation invoquée devant lui, le juge des référés, qui devait s'en tenir aux éléments qui lui étaient soumis par les parties, n'a pas commis d'erreur de droit ni commis d'erreur dans la qualification juridique des faits qui lui étaient soumis ; <br/>
<br/>
              En ce qui concerne la méconnaissance des recommandations du comité pour la prévention de la torture : <br/>
<br/>
              7. Considérant que, contrairement à ce que soutient M.B..., les prescriptions du comité pour la prévention de la torture constituent de simples recommandations ; que, dès lors, c'est sans erreur de droit que le juge des référés a jugé qu'elles ne pouvaient utilement être invoquées devant lui ; <br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que le pourvoi de M. B... doit être rejeté, y compris ses conclusions présentées au titre  de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi de M. B...est rejeté. <br/>
Article 2 : La présente décision sera notifiée à Monsieur A...B...et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
