<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027064728</ID>
<ANCIEN_ID>JG_L_2013_02_000000348991</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/06/47/CETATEXT000027064728.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 13/02/2013, 348991, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-02-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>348991</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL ; SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Jean Lessi</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:348991.20130213</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 4 mai et 4 août 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour l'Union départementale des syndicats de la CGT Force Ouvrière, dont le siège est Cité Artisanale, 26 rue Francis Combe à Cergy-Pontoise cedex (95014), la Fédération des employés et cadres de la CGT Force Ouvrière, dont le siège est 28, rue des Petits Hôtels à Paris (75010), et pour le Syndicat Force Ouvrière des employés et cadres du commerce du Val-d'Oise, dont le siège est 26, rue Francis Combe à Cergy-Pontoise cedex (95014) ; les organisations syndicales requérantes demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09VE01596 du 1er mars 2011 par lequel la cour administrative d'appel de Versailles a rejeté leur requête tendant à l'annulation du jugement n° 0811951 du 7 avril 2009 par lequel le tribunal administratif de Cergy-Pontoise a rejeté leur demande tendant à l'annulation de l'arrêté du 29 octobre 2008 du préfet du Val-d'Oise autorisant la société Boulanger à ouvrir le dimanche son établissement d'Osny ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu le code du travail ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean Lessi, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Rocheteau, Uzan-Sarano, avocat de l'Union départementale des syndicats de la CGT Force Ouvrière et autres,<br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Rocheteau, Uzan-Sarano, avocat de l'Union départementale des syndicats de la CGT Force Ouvrière et autres ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 3132-20 du code du travail : " Lorsqu'il est établi que le repos simultané, le dimanche, de tous les salariés d'un établissement serait préjudiciable au public ou compromettrait le fonctionnement normal de cet établissement, le repos peut être autorisé par le préfet, soit toute l'année, soit à certaines époques de l'année seulement suivant l'une des modalités suivantes : / 1° Un autre jour que le dimanche à tous les salariés de l'établissement ; / 2° Du dimanche midi au lundi midi ; / 3° Le dimanche après-midi avec un repos compensateur d'une journée par roulement et par quinzaine ; / 4° Par roulement à tout ou partie des salariés " ; qu'aux termes de l'article R. 3132-16 du code du travail, dans sa rédaction alors en vigueur : " Les dérogations au repos dominical prévues aux articles L. 3132-20 et L. 3132-25 sont accordées après avis (...) des organisations d'employeurs et de salariés intéressées de la commune (...) " ;<br/>
<br/>
              2. Considérant que, pour l'application des dispositions de l'article R. 3132-16 du code du travail citées ci-dessus, il appartient au préfet, en l'absence d'organisations d'employeurs ou de salariés intéressées propres à la commune d'implantation de l'établissement concerné, ou qui y disposeraient d'une représentation, de recueillir l'avis des organisations d'employeurs ou de salariés intéressées qui sont présentes au niveau départemental ;<br/>
<br/>
              3. Considérant que, pour écarter le moyen tiré de ce que l'arrêté du 29 octobre 2008 du préfet du Val-d'Oise autorisant la société Boulanger à déroger au principe du repos dominical dans son établissement d'Osny n'avait pas été précédé des consultations exigées par l'article R. 3132-16 du code du travail, la cour administrative d'appel de Versailles a seulement relevé qu'il n'existait pas, sur le territoire de cette commune, d'organisations locales de salariés ou d'employeurs que le préfet aurait dû consulter ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui a été dit ci-dessus qu'en ne recherchant pas, dans ces conditions, s'il existait au niveau départemental, ainsi que le soutenaient l'Union départementale des syndicats de la CGT Force Ouvrière du Val-d'Oise et les autres appelants, des organisations d'employeurs et de salariés pouvant être regardées comme intéressées au sens de l'article R. 3132-16 du code du travail et dont, par suite, le préfet était tenu de recueillir l'avis, la cour a commis une erreur de droit ;<br/>
<br/>
              5. Considérant qu'il résulte de tout ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens de leur pourvoi, l'Union départementale des syndicats de la CGT Force Ouvrière du Val-d'Oise et autres sont fondés à demander l'annulation de l'arrêt qu'ils attaquent ; <br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Union départementale des syndicats de la CGT Force Ouvrière du Val-d'Oise et autres, qui ne sont pas, dans la présente instance, la partie perdante ; qu'en revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Boulanger la somme de 300 euros à verser à chacune des organisations requérantes à ce même titre ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt n° 09VE01596 du 1er mars 2011 de la cour administrative d'appel de Versailles est annulé.<br/>
Article 2 : L'affaire est renvoyée devant la cour administrative d'appel de Versailles.<br/>
Article 3 : La société Boulanger versera aux organisations requérantes la somme de 300 euros chacune au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par la société Boulanger au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à l'Union départementale des syndicats de la CGT Force Ouvrière, à la Fédération des employées et cadres de la CGT Force Ouvrière, au Syndicat Force Ouvrière des employées et cadres du commerce du Val-d'Oise, à la société Boulanger et au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
