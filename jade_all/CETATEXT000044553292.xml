<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044553292</ID>
<ANCIEN_ID>JG_L_2021_12_000000445026</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/55/32/CETATEXT000044553292.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 22/12/2021, 445026, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>445026</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Jean-Marc Vié</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:445026.20211222</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par deux demandes, la société civile (SC) Chavilma a demandé au tribunal administratif de Versailles, d'une part, d'annuler pour excès de pouvoir les délibérations des 7 octobre 2013, 29 septembre 2014 et 21 septembre 2015 du syndicat intercommunal d'évacuation et d'élimination des déchets (SIEED) de l'Ouest Yvelines en tant qu'elles n'ont pas inclus l'établissement qu'elle exploite dans la liste des établissements exonérés de la taxe d'enlèvement des ordures ménagères au titre des années 2014, 2015 et 2016, et, d'autre part, d'annuler la décision du 19 février 2016 par laquelle le directeur départemental des finances publiques des Yvelines a rejeté sa demande tendant à la décharge de cette taxe au titre des années 2014 et 2015 et de prononcer cette décharge.<br/>
<br/>
              Par un jugement nos 1507648, 1602640 du 18 janvier 2018, ce tribunal a rejeté ces demandes. <br/>
<br/>
              Par un arrêt n° 18VE00958 du 1er octobre 2020, enregistré le même jour au secrétariat du contentieux du Conseil d'Etat, la cour administrative d'appel a, d'une part, sur appel de la société Chavilma, annulé les délibérations des 7 octobre 2013, 29 septembre 2014 et 21 septembre 2015 du SIEED de l'Ouest Yvelines en tant qu'elles ont exclu l'établissement que cette société exploite du bénéfice de l'exonération de taxe d'enlèvement des ordures ménagères prévue à l'article 1521 du code général des impôts et annulé dans cette mesure le jugement du tribunal administratif de Versailles et, d'autre part, transmis au Conseil d'Etat le pourvoi dirigé contre ce même jugement en tant qu'il a statué sur les conclusions relatives aux cotisations de taxe d'enlèvement des ordures ménagères établies au titres des années 2014 et 2015.<br/>
<br/>
              Par ce pourvoi ainsi qu'un nouveau mémoire et un mémoire en réplique, enregistrés les 15 juillet 2021 et 27 septembre 2021 au secrétariat du contentieux du Conseil d'État, la société Chavilma demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler le jugement du tribunal administratif de Versailles en tant qu'il a statué sur sa demande en décharge des cotisations de taxe d'enlèvement des ordures ménagères établies au titre des années 2014 et 2015 ;<br/>
<br/>
              2°) réglant l'affaire au fond dans cette mesure, de faire droit à sa demande et de prononcer, en outre, la décharge de la cotisation établie au titre de l'année 2016 ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Vié, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Piwnica, Molinié, avocat de la société Chavilma ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société civile Chavilma, qui exerce une activité hôtelière au sein d'un établissement situé à Villiers-le-Mahieu (Yvelines), ne bénéficie plus depuis 2002, à raison de cet établissement, du service de collecte des déchets ménagers assuré dans cette commune par le syndicat intercommunal d'évacuation des déchets (SIEED) de l'Ouest Yvelines. Cette société a sollicité en vain de ce syndicat son exonération de taxe d'enlèvement des ordures ménagères au titre des années 2013, 2014 et 2015. Elle a demandé au tribunal administratif de Versailles, d'une part, d'annuler les délibérations des 7 octobre 2013, 29 septembre 2014 et 21 septembre 2015 par lesquelles le comité syndical du SIEED de l'Ouest Yvelines a fixé, en application des dispositions du 1 de l'article 1521 du code général des impôts, la liste des locaux à usage industriels exonérés de cette taxe au titre des années 2014 à 2016 en tant que l'établissement qu'elle exploite n'y figurait pas et, d'autre part, d'annuler la décision du directeur départemental des finances publiques des Yvelines rejetant sa demande tendant à la décharge des cotisations de taxe d'enlèvement des ordures ménagères auxquelles elle a été assujettie au titre des années 2014 et 2015 et de prononcer cette décharge. Ces demandes ont été rejetées par un jugement du 18 janvier 2018. Par un arrêt du 1er octobre 2020, la cour administrative d'appel de Versailles a annulé les délibérations en litige, en tant qu'elles ne mentionnaient pas l'établissement exploité par la société Chavilma dans la liste des établissements exonérés, et annulé le jugement du tribunal administratif, en tant qu'il avait rejeté les demandes correspondantes. La société Chavilma se pourvoit en cassation contre ce même jugement, en tant qu'il a statué sur ses demandes tendant à la décharge des impositions établies au titre des années 2014 et 2015. Elle demande en outre au Conseil d'Etat de prononcer la décharge des impositions établies au titre de l'année 2016.<br/>
<br/>
              Sur les conclusions tendant à la décharge des impositions établies au titre de 2016 :<br/>
<br/>
              2. Les conclusions tendant à la décharge de la taxe d'enlèvement des ordures ménagères due par la société Chavilma au titre de l'année 2016 n'ont pas été précédées de la réclamation préalable prévue par les dispositions de l'article R. 190-1 du livre des procédures fiscales. Le ministre est par suite fondé à soutenir que ces conclusions, qui sont au surplus présentées pour la première fois en cassation, sont irrecevables et ne peuvent, par suite, qu'être rejetées.<br/>
<br/>
              Sur les conclusions tendant à la décharge des impositions établies au titre de 2014 et 2015 :<br/>
<br/>
              3. Aux termes du III de l'article 1521 du code général des impôts : " 1. Les conseils municipaux déterminent annuellement les cas où les locaux à usage industriel ou commercial peuvent être exonérés de la taxe. La liste des établissements exonérés est affichée à la porte de la mairie. (...) / 3. Les exonérations visées aux 1 à 2 bis sont décidées par les organes délibérants des groupements de communes lorsque ces derniers sont substitués aux communes pour l'institution de la taxe d'enlèvement des ordures ménagères. (...) ".<br/>
<br/>
              4. Pour rejeter la demande de la société Chavilma tendant à la décharge des cotisations de taxe d'enlèvement des ordures ménagères auxquelles elle a été assujetties au titre des années 2014 et 2015 à raison de son établissement de Villiers-le-Mahieu, le tribunal administratif s'est fondé sur ce que, contrairement à ce que soutenait cette société, le SIEED de l'Ouest Yvelines avait pu légalement, par ses délibérations des 7 octobre 2013 et 29 septembre 2014, refuser de lui accorder l'exonération qu'elle sollicitait. En statuant ainsi alors qu'il résulte de l'article 2 de l'arrêt du 1er octobre 2020 de la cour administrative d'appel de Versailles, devenu définitif, que ces délibérations sont entachées d'illégalité en tant qu'elles ne mentionnent pas cet établissement dans la liste des locaux à usage industriel ou commercial exonérés en vertu des dispositions citées au point 3, le tribunal administratif a commis une erreur de droit. <br/>
<br/>
              5. La société Chavilma est, par suite, fondée à demander l'annulation du jugement qu'elle attaque, en tant que celui-ci s'est prononcé sur sa demande tendant à la décharge des cotisations de taxe d'enlèvement des ordures ménagères établies au titre des années 2014 et 2015.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de régler, dans cette mesure, l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              7. Par l'article 2 de son arrêt du 1er octobre 2020, la cour administrative d'appel de Versailles a annulé pour excès de pouvoir les délibérations du comité syndical du SIEED de l'Ouest Yvelines des 7 octobre 2013 et 29 septembre 2014 en tant que celles-ci ne mentionnaient pas l'établissement exploité par la société Chavilma dans la liste des locaux exonérés de taxe d'enlèvement des ordures ménagères en application du 1 du III de l'article 1521 du code général des impôts, au motif que ces délibérations méconnaissaient, dans cette mesure, le principe d'égalité devant les charges publiques. L'autorité absolue de la chose jugée qui s'attache à cette annulation pour excès de pouvoir et aux motifs qui en constituent le soutien nécessaire fait obstacle à ce que la société requérante soit assujettie à la taxe d'enlèvement des ordures ménagères à raison de son établissement de Villiers-le-Mahieu au titre des années 2014 et 2015.<br/>
<br/>
              8. Il résulte de tout ce qui précède que la société Chavilma est fondée à demander la décharge des impositions contestées.<br/>
<br/>
              9. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 1 000 euros, à verser à la société Chavilma, au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement nos 1507648, 1602640 du 18 janvier 2018 du tribunal administratif de Versailles est annulé en qu'il a statué sur les demandes tendant à la décharge des cotisations de taxe d'enlèvement des ordures ménagères auxquelles la société Chavilma a été assujettie au titre des années 2014 et 2015 à raison de son établissement situé à Villiers-le-Mahieu.<br/>
Article 2 : La société Chavilma est déchargée des cotisations mentionnées à l'article 1er ci-dessus.<br/>
Article 3 : L'Etat versera à la société Chavilma la somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Le surplus des conclusions du pourvoi de la société Chavilma est rejeté.<br/>
Article 5 : La présente décision sera notifiée à la société civile Chavilma et au ministre de l'économie, des finances et de la relance.<br/>
              Délibéré à l'issue de la séance du 2 décembre 2021 où siégeaient : M. Pierre Collin, président de chambre, présidant ; M. Hervé Cassagnabère, conseiller d'Etat et M. D... A..., maître des requêtes-rapporteur. <br/>
<br/>
              Rendu le 22 décembre 2021.<br/>
<br/>
<br/>
                 Le président : <br/>
                 Signé : M. Pierre Collin<br/>
 		Le rapporteur : <br/>
      Signé : M. Jean-Marc Vié<br/>
                 La secrétaire :<br/>
                 Signé : Mme C... B...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
