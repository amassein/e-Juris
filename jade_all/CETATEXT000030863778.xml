<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030863778</ID>
<ANCIEN_ID>JG_L_2015_07_000000389735</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/86/37/CETATEXT000030863778.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème / 7ème SSR, 09/07/2015, 389735, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-07-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389735</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème / 7ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>Mme Airelle Niepce</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:389735.20150709</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un mémoire et un mémoire en réplique, enregistrés les 24 avril et 3 juin 2015 au secrétariat du contentieux du Conseil d'Etat, présentés en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958, l'association Pierre Claver demande au Conseil d'Etat, à l'appui de sa requête tendant à l'annulation pour excès de pouvoir de la décision implicite du Premier ministre refusant d'abroger les articles R. 553-18 et R. 742-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution du 1° de l'article L. 741-4 ainsi que des articles L. 723-1 et L. 742-4 du code de l'entrée et du séjour des étrangers et du droit d'asile.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et ses articles 53-1, 61-1 et 62 ;<br/>
              - la convention de Genève relative au statut des réfugiés ;<br/>
              - le règlement (UE) du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile, notamment ses articles L. 723-1, L. 741-4 et L. 742-4 ; <br/>
              - la loi n° 2003-1176 du 10 décembre 2003 ;<br/>
              - l'ordonnance n° 2004-1248 du 24 novembre 2004 ;<br/>
              - la loi n° 2006-911 du 24 juillet 2006 ;<br/>
              - la loi n° 2007-1631 du 20 novembre 2007 ;<br/>
              - la loi n° 2011-672 du 16 juin 2011 ;<br/>
              - les décisions du Conseil constitutionnel n° 2003-485 DC du 4 décembre 2003 et n° 2011-120 QPC du 8 avril 2011 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Airelle Niepce, maître des requêtes en service extraordinaire, <br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de l'association Pierre Claver ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) " ; qu'il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2.	Considérant qu'aux termes du quatrième alinéa du préambule de la Constitution de 1946 : " Tout homme persécuté en raison de son action en faveur de la liberté a droit d'asile sur les territoires de la République " ; que l'article 53-1 de la Constitution dispose que : " La République peut conclure avec les Etats européens qui sont liés par des engagements identiques aux siens en matière d'asile et de protection des Droits de l'homme et des libertés fondamentales, des accords déterminant leurs compétences respectives pour l'examen des demandes d'asile qui leur sont présentées. / Toutefois, même si la demande n'entre pas dans leur compétence en vertu de ces accords, les autorités de la République ont toujours le droit de donner asile à tout étranger persécuté en raison de son action en faveur de la liberté ou qui sollicite la protection de la France pour un autre motif " ; <br/>
<br/>
              3.	Considérant qu'en vertu de l'article L. 723-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, l'Office français de protection des réfugiés et apatrides " statue sur les demandes d'asile dont il est saisi. Il n'est toutefois pas compétent pour connaître d'une demande présentée par une personne à laquelle l'admission au séjour a été refusée pour le motif prévu au 1° de l'article L. 741-4. (...) " ; que cet article L. 741-4 dispose que : " Sous réserve du respect des stipulations de l'article 33 de la convention de Genève du 28 juillet 1951 relative au statut des réfugiés, l'admission en France d'un étranger qui demande à bénéficier de l'asile ne peut être refusée que si : / 1° L'examen de la demande d'asile relève de la compétence d'un autre Etat en application des dispositions du règlement (CE) n° 343/2003 du Conseil du 18 février 2003 établissant les critères et mécanismes de détermination de l'Etat membre responsable de l'examen d'une demande d'asile présentée dans l'un des Etats membres par un ressortissant d'un pays tiers, ou d'engagements identiques à ceux prévus par ledit règlement avec d'autres Etats (...) " ; qu'aux termes de l'article L. 742-4 du même code : " Dans le cas où l'admission au séjour a été refusée pour le motif mentionné au 1° de l'article L. 741-4, l'intéressé n'est pas recevable à saisir la Cour nationale du droit d'asile " ; que l'association Pierre Claver soutient que ces dispositions, en ce qu'elles permettent de refuser l'admission au séjour des étrangers dont la demande d'asile relève d'un autre Etat membre de l'Union européenne en vertu de la législation applicable et excluent la compétence de l'Office français de protection des réfugiés et apatrides et de la Cour nationale du droit d'asile pour connaître des demandes d'asile formées par ces étrangers, méconnaissent le droit d'asile constitutionnel garanti par le quatrième alinéa du Préambule de la Constitution de 1946 et l'article 53-1 de la Constitution ; <br/>
<br/>
              4.	Considérant, toutefois, que le Conseil constitutionnel, dans les motifs et le dispositif de sa décision n° 2003-485 DC du 4 décembre 2003, a déclaré conforme à la Constitution l'article 5 de la loi du 10 décembre 2003 modifiant la loi n° 52-893 du 25 juillet 1952 relative au droit d'asile ; que les dispositions de cet article relatif aux cas d'admission au séjour des demandeurs d'asile ont été codifiées à l'article L. 741-4 du code de l'entrée et du séjour des étrangers et du droit d'asile et, pour ce qui concerne les organes compétents pour connaître des demandes d'asile, aux articles L. 723-1 et L. 742-4 du même code par l'effet de l'ordonnance du 24 novembre 2004 relative à la partie législative du code de l'entrée et du séjour des étrangers et du droit d'asile, ratifiée par l'article 120 de la loi du 24 juillet 2006 relative à l'immigration et à l'intégration ; que l'article L. 742-4 n'a été modifié par l'article 29 de la loi du 20 novembre 2007 relative à la maîtrise de l'immigration, à l'intégration et à l'asile que pour substituer à l'appellation " commission des recours des réfugiés " la nouvelle dénomination de Cour nationale du droit d'asile ; que l'article L. 741-4 n'a été modifié par l'article 96 de la loi du 16 juin 2011 relative à l'immigration, à l'intégration et à la nationalité que pour compléter le 4° relatif au cas de refus d'admission au séjour pour cause de fraude délibérée, lequel n'est pas visé par la question prioritaire de constitutionnalité soulevée ; que, dans les motifs et le dispositif de sa décision n° 2011-120 QPC du 8 avril 2011, le Conseil constitutionnel a, au surplus, déclaré conformes à la Constitution les dispositions de l'article L. 741-4 du code de l'entrée et du séjour des étrangers et du droit d'asile dans leur rédaction issue de la loi du 10 décembre 2003 ;<br/>
<br/>
              5.	Considérant que, sous la seule réserve de la dénomination de la juridiction appelée à statuer sur les demandes d'asile, les dispositions de l'article L. 723-1, du 1° de l'article L. 741-4 et de l'article L. 742-4 du code de l'entrée et du séjour des étrangers et du droit d'asile sont identiques aux dispositions résultant de l'article 5 de la loi du 10 décembre 2003, qui ont été déclarées conformes à la Constitution par la décision du Conseil constitutionnel du 4 décembre 2003 ; que les dispositions de l'article L. 741-4 ont été à nouveau déclarées conformes à la Constitution par la décision du Conseil constitutionnel n° 2011-120 QPC du 8 avril 2011 ; qu'aucun changement de circonstances n'est de nature à justifier que la conformité à la Constitution des dispositions en litige soit à nouveau examinée par le Conseil constitutionnel ; qu'ainsi, et alors même que les motifs de ces décisions du Conseil constitutionnel ne se sont pas explicitement prononcés sur les moyens relatifs au quatrième alinéa du Préambule de la Constitution du 27 octobre 1946 et à l'article 53-1 de la Constitution, il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil Constitutionnel la question prioritaire de constitutionnalité soulevée par l'association Pierre Claver.<br/>
<br/>
Article 2 : La présente décision sera notifiée à l'association Pierre Claver et au ministre de l'intérieur. Copie en sera adressée au Conseil constitutionnel et au Premier ministre. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
