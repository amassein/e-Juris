<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000020541069</ID>
<ANCIEN_ID>JG_L_2007_09_000000308460</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/20/54/10/CETATEXT000020541069.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 10/09/2007, 308460, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2007-09-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>308460</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Stirn</PRESIDENT>
<AVOCATS>SCP BORE ET SALVE DE BRUNETON ; SCP CELICE, BLANCPAIN, SOLTNER</AVOCATS>
<RAPPORTEUR>M. Bernard  Stirn</RAPPORTEUR>
<COMMISSAIRE_GVT/>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 10 août 2007 au secrétariat du contentieux du Conseil d'Etat, et les observations complémentaires, enregistrées le 20 août 2007, présentées pour M. et Mme B, demeurant ..., pour M. et Mme A, demeurant ... et pour le groupement foncier agricole CAPEYRON, dont le siège est ... ; les requérants demandent au juge des référés du Conseil d'Etat :<br/>
<br/>
<br/>
              1°) de suspendre, sur le fondement de l'article L. 521-1 du code de justice administrative, l'exécution de l'arrêté n° 05-0827 en date du 6 mars 2006, par lequel le préfet de la Gironde et le préfet de la Charente-Maritime ont autorisé le port autonome de Bordeaux à réaliser des opérations de dragage d'entretien du chenal et des ouvrages portuaires du port autonome de Bordeaux et d'amélioration du chenal de navigation ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros pour chacune des parties, en application des dispositions de l'article L. 761-1 du code de justice administrative ;  <br/>
<br/>
<br/>
<br/>
              ils soutiennent qu'il y a urgence, dès lors que l'exécution de l'arrêté inter-préfectoral litigieux préjudicie de manière grave et immédiate à leurs intérêts en entraînant une diminution de la superficie de leur propriété et de la capacité de production de leur exploitation viticole ; qu'il existe un doute quant à la légalité de la décision contestée ; qu'en effet l'acte a été signé par une autorité incompétente ; que l'arrêté est entaché de plusieurs vices de procédure, tenant à l'absence de consultation du préfet coordinateur de bassin et du comité de bassin, à l'absence d'avis de la Commission européenne, à l'absence de preuve de consultation du préfet de région en matière d'archéologie et du gestionnaire du domaine public, et au caractère irrégulier de la consultation des comités départementaux d'hygiène ; que la décision contestée méconnaît l'obligation de motivation des actes administratifs ; qu'elle porte en outre atteinte au droit de propriété et à la liberté d'entreprendre ; que l'administration a commis de nombreuses erreurs de droit concernant l'autorisation de déroctage, l'absence de prise en compte de la nomenclature « installations classées », l'oubli de rubriques de la nomenclature « eau » ; que le préfet avait  compétence liée pour refuser l'autorisation en raison de l'impact du projet sur une zone Natura 2000 et de son incompatibilité avec les schémas d'aménagement et de gestion des eaux ; que l'arrêté contesté est entaché d'erreur manifeste dans l'appréciation des faits ; que l'administration n'a pas pris en compte le lien de causalité entre les travaux et l'érosion des berges et a omis d'analyser les impacts des travaux sur ces berges ; que l'arrêté inter-préfectoral litigieux méconnaît les dispositions des articles L. 321-1 et L. 321-8 du code de l'environnement et L. 146-6 du code de l'urbanisme ; qu'enfin, l'impact des modifications du trafic fluvial sur l'environnement n'a pas été pris en compte ;<br/>
<br/>
<br/>
              Vu l'arrêté dont la suspension est demandée ;<br/>
<br/>
              Vu la requête en annulation présentée à l'encontre de cet arrêté ;<br/>
<br/>
<br/>
              Vu, enregistré le 3 septembre 2007, le mémoire en défense présenté par le ministre de l'écologie, du développement et de l'aménagement durables, qui conclut au rejet de la requête ; il soutient que les requérants n'ont obtenu satisfaction devant la cour administrative d'appel de Bordeaux qu'en raison de l'insuffisance de motivation d'un avis ; qu'il n'est pas utile de suspendre l'arrêté litigieux, dès lors que les travaux, qui ont déjà débuté, sont en cours d'achèvement ; que les requérants n'apportent pas la preuve d'une diminution antérieure de la superficie de leur propriété ni de la capacité de production de leur exploitation, liée aux activités du port autonome ; que le co-signataire de l'acte, auquel une délégation de signature a été consentie, était compétent ; que la consultation du préfet coordinateur de bassin, du comité de bassin, de la Commission européenne, ou du gestionnaire du domaine public n'était pas nécessaire ; que le préfet de région a bien été consulté au titre de l'archéologie ; que la procédure de consultation des comités départementaux d'hygiène n'a pas été viciée ; que l'absence dans les visas de l'arrêté de mention relative au schéma d'aménagement et de gestion des eaux est sans incidence sur sa légalité ; que l'arrêté litigieux n'est pas un acte devant être obligatoirement motivé ; qu'il ne porte pas atteinte aux propriétés privées riveraines et ne remet pas en cause la liberté d'entreprendre des requérants ; qu'il n'y a pas d'erreur de droit, car le déroctage est une opération d'entretien et d'amélioration d'aménagements et ouvrages existants ; que l'article L. 414-4 du code de l'environnement n'a pas été méconnu, en raison des incidences négligeables des travaux sur la conservation du site ; que la réglementation relative aux installations classées ne s'applique pas en l'espèce ; que les requérants ne sont pas fondés à soutenir que le préfet était tenu de refuser l'autorisation en raison de l'incompatibilité du projet avec les schémas d'aménagement et de gestion des eaux ; qu'en effet, il n'y pas de lien entre l'érosion des berges et les travaux, qui en outre ne portent pas atteinte à des espèces protégées ; que l'impact de ces nécessaires modifications du trafic fluvial sur l'environnement a été pris en compte ;<br/>
<br/>
<br/>
              Vu, enregistré le 3 septembre 2007, le mémoire en défense présenté pour le port autonome de Bordeaux, qui conclut au rejet de la requête et à ce que la somme de 12 000 euros soit mise à la charge des requérants en application de l'article L. 761-1 du code de justice administrative ; il soutient que la condition d'urgence n'est pas remplie ; qu'en effet, une attestation du directeur de l'aménagement et de l'environnement du port autonome de Bordeaux certifie que les travaux de déroctage sont sur le point de s'achever ; qu'au surplus il n'est nullement établi que les travaux autorisés auraient des conséquences sur les terres exploitées par les requérants ; que le préjudice invoqué est ainsi purement hypothétique ; qu'à l'inverse, les travaux litigieux répondent à des besoins d'intérêt général ; qu'aucun des moyens invoqués n'est par ailleurs de nature à faire naître un doute sérieux sur la légalité de la décision dont la suspension est demandée ; que le secrétaire général de la préfecture de la Gironde est titulaire d'une délégation de signature régulièrement publiée ; que ni le préfet coordonnateur de bassin ni le comité de bassin ni la Commission européenne ni les comités départementaux d'hygiène n'avaient à être consultés ; que le préfet de région a été consulté au titre de l'archéologie ; que le port autonome est le gestionnaire du domaine public et n'avait donc pas à se donner un avis à lui-même ; que l'arrêté litigieux n'avait pas à être motivé et que sa légalité n'est pas entachée par une éventuelle omission dans ses visas ; que cet arrêté ne porte pas d'atteinte illégale au droit de propriété ni à la liberté d'entreprendre ; qu'il n'est entaché ni d'erreur de droit ni d'erreur manifeste d'appréciation ; qu'il ne porte pas atteinte à la conservation du site et n'est pas incompatible avec les schémas d'aménagement et de gestion des eaux ; qu'il ne méconnaît aucune disposition législative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'environnement ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part M. et Mme B, M. et Mme A et le groupement foncier agricole CAPEYRON, et d'autre part le ministre de l'écologie, du développement et de l'aménagement durables ;<br/>
              Vu le procès-verbal de l'audience publique du mercredi 5 septembre 2007 à 14 heures 30, au cours de laquelle ont été entendus :<br/>
<br/>
              - Me Boré, avocat au Conseil d'Etat et à la Cour de cassation, avocat des requérants ;<br/>
<br/>
              - Mme B ;<br/>
<br/>
              - Me Blancpain, avocat au Conseil d'Etat et à la Cour de cassation, avocat du port autonome de Bordeaux ;<br/>
<br/>
<br/>
              Vu la note en délibéré, enregistrée le 6 septembre 2007, présentée pour M. et Mme B, M. et Mme A et le groupement foncier agricole CAPEYRON ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 6 septembre 2007, présentée pour le port autonome de Bordeaux ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'aux termes de l'article L. 521-1 du code de justice administrative : « Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision » ; que la condition d'urgence posée par ces dispositions n'est satisfaite que dans le cas où l'exécution de la décision dont la suspension est demandée porte atteinte de manière suffisamment grave et immédiate à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ;<br/>
<br/>
              Considérant qu'il ne résulte ni de l'instruction écrite ni des explications données au cours de l'audience publique que les travaux autorisés par l'arrêté dont la suspension est demandée auraient par eux-mêmes des conséquences graves et immédiates sur les terrains exploités par les requérants ; que ces travaux sont au surplus presque achevés au droit des propriétés des requérants ; que la condition d'urgence ne peut, dans ces conditions, être regardée comme remplie ; que les conclusions à fin de suspension et, par voie de conséquence, les conclusions à fin d'application de l'article L. 761-1 du code de justice administrative présentées par les requérants ne peuvent, dès lors, qu'être rejetées ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. et Mme Pierre B, de M. et Mme Alain A et du groupement foncier agricole CAPEYRON, en application de l'article L. 761-1 du code de justice administrative, la somme totale de 3 000 euros,  que les intéressés verseront au port autonome de Bordeaux, à hauteur respectivement de 1 000 euros pour M. et Mme B, 1 000 euros pour  M. et Mme A et 1 000 euros également pour le groupement foncier agricole CAPEYRON ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              			O R D O N N E :<br/>
              			--------------------<br/>
<br/>
<br/>
Article 1er : La requête de M. et Mme Pierre B, de M. et Mme Alain A et du groupement foncier agricole CAPEYRON est rejetée.<br/>
<br/>
Article 2 : M. et Mme Pierre B, M. et Mme Alain A et le groupement foncier agricole CAPEYRON verseront au port autonome de Bordeaux, en application de l'article L. 761-1 du code de justice administrative, la somme totale de 3 000 euros, mise à la charge de M. et Mme B à hauteur de 1 000 euros, de M. et Mme A à hauteur de 1 000 euros et du groupement foncier agricole CAPEYRON à hauteur de 1 000 euros également.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
Article 3 : La présente ordonnance sera notifiée à M. et Mme Pierre B, à M. et Mme Alain A, au groupement foncier agricole CAPEYRON, au port autonome de Bordeaux et au ministre d'Etat, ministre de l'écologie, du développement et de l'aménagement durables.<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
