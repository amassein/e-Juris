<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041958787</ID>
<ANCIEN_ID>JG_L_2020_06_000000428222</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/95/87/CETATEXT000041958787.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 03/06/2020, 428222</TITRE>
<DATE_DEC>2020-06-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428222</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELVOLVE ET TRICHET</AVOCATS>
<RAPPORTEUR>M. Yohann Bouquerel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:428222.20200603</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Sous le n° 1700237, Mme A... B..., veuve de M. C..., a demandé au tribunal administratif de Poitiers, d'une part, d'annuler la décision implicite par laquelle le ministre de la défense a rejeté sa demande du 20 juillet 2010 tendant à la décristallisation de sa pension militaire de retraite d'ayant cause de son mari avec effet rétroactif depuis la date d'ouverture de son droit à pension, le 1er décembre 1991, ainsi qu'au paiement des arrérages de la pension ainsi revalorisée au taux de droit commun et des intérêts capitalisés, d'autre part, d'enjoindre à l'Etat de lui verser les arrérages de la pension décristallisée de son mari pour la période du 1er décembre 1991 au 11 janvier 2012, assortis des intérêts à compter du 21 juillet 2010 et de la capitalisation annuelle des intérêts échus.<br/>
<br/>
              Sous le n° 1700238, l'intéressée a demandé au même tribunal, d'une part, d'annuler la décision implicite par laquelle le ministre de la défense a rejeté sa demande du 10 août 2012 tendant à la décristallisation de sa pension militaire de retraite d'ayant cause de son mari avec effet rétroactif depuis la date d'ouverture de son droit à pension, le 1er décembre 1991, ainsi qu'au paiement des arrérages de la pension ainsi revalorisée au taux de droit commun et des intérêts capitalisés, d'autre part, d'enjoindre à l'Etat de lui verser les arrérages de la pension décristallisée de son mari pour la période du 1er décembre 1991 au 11 janvier 2012, assortis des intérêts à compter du 14 août 2012 et de la capitalisation annuelle des intérêts échus.<br/>
<br/>
              Par un jugement n°s 1700237, 1700238 du 11 octobre 2018, le tribunal administratif de Poitiers a rejeté ces demandes.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 20 février et 20 mai 2019 au secrétariat du contentieux du Conseil d'Etat, Mme B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses demandes ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros à verser à la SCP Delvolvé et Trichet, son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des pensions civiles et militaires de retraite ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - la loi n° 2000-321 du 12 avril 2000 ;<br/>
              - le décret n° 2016-1480 du 2 novembre 2016 ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 modifiée ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thomas Pez-Lavergne, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delvolvé et Trichet, avocat de Mme B... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que Mme B..., veuve de M. C..., a demandé le 26 janvier 2017 au tribunal administratif de Poitiers d'annuler la décision implicite de rejet née du silence gardé par le ministre de la défense sur sa demande du 20 juillet 2010 tendant, d'une part, à la décristallisation de sa pension militaire de retraite d'ayant cause avec effet rétroactif depuis la date d'ouverture de son droit à pension, le 1er décembre 1991, ainsi qu'au paiement des arrérages de la pension revalorisée au taux de droit commun et des intérêts capitalisés, d'autre part, d'enjoindre à l'Etat de lui verser les arrérages de la pension décristallisée de son mari pour la période du 1er décembre 1991 au 11 janvier 2012. Mme B... a également demandé le 26 janvier 2017 au même tribunal d'annuler la décision implicite de rejet née du silence gardé par le ministre de la défense sur sa demande du 10 août 2012, dont l'objet était similaire à celui de sa demande du 20 juillet 2010. Mme B... se pourvoit en cassation contre le jugement du 11 octobre 2018 par lequel le tribunal administratif de Poitiers a rejeté ses deux demandes. <br/>
<br/>
              Sur le cadre juridique du litige :<br/>
<br/>
              2. En vertu de l'article R. 421-2 du code de justice administrative, sauf disposition législative ou réglementaire contraire, dans les cas où le silence gardé par l'autorité administrative sur une demande vaut décision de rejet, l'intéressé dispose, pour former un recours, d'un délai de deux mois à compter de la date à laquelle est née une décision implicite de rejet. Par dérogation à cette règle, l'article R. 421-3 du même code, dans sa rédaction applicable antérieurement à l'entrée en vigueur du décret du 2 novembre 2016 portant modification du code de justice administrative, disposait que " l'intéressé n'est forclos qu'après un délai de deux mois à compter du jour de la notification d'une décision expresse de rejet : / 1° En matière de plein contentieux (...) ". Il en résultait que lorsqu'une personne s'était vue tacitement opposer un refus susceptible d'être contesté dans le cadre d'un recours de plein contentieux, ce recours n'était enfermé, en l'état des textes alors en vigueur, dans aucun délai, sauf à ce que cette décision de refus soit, sous forme expresse, régulièrement notifiée à cette personne, un délai de recours de deux mois courant alors à compter de la date de cette notification. <br/>
<br/>
              3. Le décret du 2 novembre 2016 a supprimé le 1° de l'article R. 421-3 du code de justice administrative à compter du 1er janvier 2017 et a prévu que les nouvelles dispositions de cet article s'appliqueraient aux requêtes enregistrées à partir de cette date. Il en résulte que, s'agissant des décisions implicites relevant du plein contentieux, la nouvelle règle selon laquelle, sauf dispositions législatives ou réglementaires qui leur seraient propres, le délai de recours de deux mois court à compter de la date à laquelle elles sont nées, est applicable aux décisions nées à compter du 1er janvier 2017. En ce qui concerne les décisions implicites relevant du plein contentieux nées antérieurement à cette date, un délai franc de recours de deux mois court à compter du 1er janvier 2017, soit jusqu'au 2 mars 2017.<br/>
<br/>
              4. Les règles énoncées au point 3 doivent être combinées avec les dispositions issues de l'article 19 de la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec l'administration et désormais codifiées à l'article L. 112-6 du code des relations entre le public et l'administration, aux termes desquelles, sauf en ce qui concerne les relations entre l'administration et ses agents, les délais de recours contre une décision implicite de rejet ne sont pas opposables à l'auteur d'une demande lorsque l'accusé de réception prévu par l'article L. 112-3 du même code ne lui a pas été transmis ou que celui-ci ne porte pas les mentions prévues à l'article R. 112-5 de ce code et, en particulier, la mention des voies et délais de recours. Le principe de sécurité juridique fait cependant obstacle à ce que le demandeur, lorsqu'il est établi qu'il a eu connaissance de la décision implicite qui lui a été opposée, puisse la contester indéfiniment du seul fait que l'administration ne lui a pas délivré d'accusé de réception de sa demande ou n'a pas porté sur l'accusé de réception les mentions requises. La preuve d'une telle connaissance peut résulter de ce qu'il est établi, soit que l'intéressé a été clairement informé des conditions de naissance d'une décision implicite lors de la présentation de sa demande, soit que la décision a par la suite été expressément mentionnée au cours de ses échanges avec l'administration, notamment à l'occasion d'un recours gracieux dirigé contre cette décision. Le demandeur dispose alors, pour saisir le juge, d'un délai raisonnable qui, sauf circonstances particulières, ne saurait excéder un an et court, dans la première hypothèse, de la date de naissance de la décision implicite et, dans la seconde, de la date de l'événement établissant qu'il a eu connaissance de la décision. En ce qui concerne les décisions implicites de rejet relevant du plein contentieux nées avant le 1er janvier 2017, dont il est établi que le demandeur a eu connaissance avant cette date, mais pour lesquelles l'administration, alors qu'elle était soumise à cette obligation, n'a pas délivré d'accusé de réception ou a délivré un accusé de réception ne comportant pas les mentions requises, le délai de recours expire le 31 décembre 2017, sauf circonstances particulières invoquées par le requérant. <br/>
<br/>
              Sur le pourvoi :<br/>
<br/>
              5. D'une part, les litiges en matière de pensions relèvent du plein contentieux. Ainsi qu'il a été dit au point 2, le recours contre les décisions implicites de rejet relevant du plein contentieux intervenues antérieurement à l'entrée en vigueur du décret du 2 novembre 2016 n'était enfermé dans aucun délai et n'a commencé à courir que le 1er janvier 2017. D'autre part, un litige relatif à la pension de réversion de l'ayant droit d'un militaire ne saurait être regardé comme un litige entre l'administration et l'un de ses agents. Il ressort des pièces du dossier soumis aux juges du fond que l'administration, alors qu'elle était soumise à cette obligation, n'a pas transmis à Mme B..., pour chacune de ses demandes, l'accusé de réception mentionné à l'article 19 de la loi du 12 avril 2000. Le délai de recours contre les décisions attaquées expirait donc le 31 décembre 2017. Il résulte de ce qui précède qu'en jugeant, pour rejeter comme tardifs ses recours dirigés contre les décisions implicites de rejet nées du silence gardé par le ministre de la défense sur ses demandes des 20 juillet 2010 et 10 août 2012, que Mme B... disposait pour les contester d'un délai d'un an à compter des dates auxquelles il était établi qu'elle en avait eu connaissance, et que ses recours, enregistrés le 26 janvier 2017, étaient tardifs, le tribunal administratif a entaché son jugement d'erreur de droit.<br/>
<br/>
              6. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, que la requérante est fondée à demander l'annulation du jugement qu'elle attaque. <br/>
<br/>
              7. La requérante a obtenu le bénéfice de l'aide juridictionnelle. Par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Delvolvé et Trichet, avocat de la requérante, renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à cette SCP.  <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 11 octobre 2018 du tribunal administratif de Poitiers est annulé.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Poitiers.<br/>
Article 3 :  L'Etat versera à la SCP Delvolvé et Trichet, avocat de Mme B..., une somme de 3 000 euros au titre des articles L. 761-1 du code de justice administrative et  37, alinéa 2 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : La présente décision sera notifiée à Mme A... B.... <br/>
Copie en sera adressée à la ministre des armées.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-08 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. DÉCISIONS IMPLICITES. - DÉCISIONS IMPLICITES DONT LA CONTESTATION RELÈVE DU PLEIN CONTENTIEUX - DÉCRET DU 2 NOVEMBRE 2016 SOUMETTANT CES DÉCISIONS AU DROIT COMMUN DE LA NAISSANCE DU DÉLAI DE RECOURS, RENDU APPLICABLE AUX REQUÊTES ENREGISTRÉES À COMPTER DU 1ER JANVIER 2017 - CONSÉQUENCE - DÉCISIONS IMPLICITES NÉES ANTÉRIEUREMENT AU 1ER JANVIER 2017 [RJ1] - APPLICABILITÉ DU DÉLAI D'UN AN ISSU DE LA JURISPRUDENCE CZABAJ [RJ2] - EXISTENCE [RJ3], À COMPTER UNIQUEMENT DU 1ER JANVIER 2017.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">01-04-03-07 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. PRINCIPES INTÉRESSANT L'ACTION ADMINISTRATIVE. - PRINCIPE DE SÉCURITÉ JURIDIQUE - IMPOSSIBILITÉ D'EXERCER UN RECOURS JURIDICTIONNEL AU-DELÀ D'UN DÉLAI RAISONNABLE [RJ2] - CAS DES DÉCISIONS IMPLICITES NÉES AVANT LE 1ER JANVIER 2017, DATE À PARTIR DE LAQUELLE CES DÉCISIONS SONT SOUMISES EN VERTU DU DÉCRET DU 2 NOVEMBRE 2016 AU DROIT COMMUN DE LA NAISSANCE DU DÉLAI DE RECOURS [RJ1], ET DONT LA CONTESTATION RELÈVE DU PLEIN CONTENTIEUX - DÉLAI D'UN AN COURANT UNIQUEMENT À COMPTER DU 1ER JANVIER 2017 [RJ3].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">48-02-01-09 PENSIONS. PENSIONS CIVILES ET MILITAIRES DE RETRAITE. QUESTIONS COMMUNES. AYANTS-CAUSE. - LITIGE RELATIF À LA PENSION DE RÉVERSION DE L'AYANT-DROIT D'UN MILITAIRE - LITIGE ENTRE L'ADMINISTRATION ET L'UN DE SES AGENTS - ABSENCE - CONSÉQUENCE - APPLICABILITÉ DE L'ARTICLE L. 112-6 DU CRPA.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">54-01-07-02 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉLAIS. POINT DE DÉPART DES DÉLAIS. - DÉCISIONS IMPLICITES DONT LA CONTESTATION RELÈVE DU PLEIN CONTENTIEUX - DÉCRET DU 2 NOVEMBRE 2016 SOUMETTANT CES DÉCISIONS AU DROIT COMMUN DE LA NAISSANCE DU DÉLAI DE RECOURS, RENDU APPLICABLE AUX REQUÊTES ENREGISTRÉES À COMPTER DU 1ER JANVIER 2017 - CONSÉQUENCE - DÉCISIONS IMPLICITES NÉES ANTÉRIEUREMENT AU 1ER JANVIER 2017 [RJ1] - APPLICABILITÉ DU DÉLAI D'UN AN ISSU DE LA JURISPRUDENCE CZABAJ [RJ2] - EXISTENCE [RJ3], À COMPTER UNIQUEMENT DU 1ER JANVIER 2017.
</SCT>
<ANA ID="9A"> 01-01-08 Le principe de sécurité juridique fait obstacle à ce que le demandeur, lorsqu'il est établi qu'il a eu connaissance de la décision implicite qui lui a été opposée, puisse la contester indéfiniment du seul fait que l'administration ne lui a pas délivré d'accusé de réception de sa demande ou n'a pas porté sur l'accusé de réception les mentions requises. La preuve d'une telle connaissance peut résulter de ce qu'il est établi, soit que l'intéressé a été clairement informé des conditions de naissance d'une décision implicite lors de la présentation de sa demande, soit que la décision a par la suite été expressément mentionnée au cours de ses échanges avec l'administration, notamment à l'occasion d'un recours gracieux dirigé contre cette décision. Le demandeur dispose alors, pour saisir le juge, d'un délai raisonnable qui, sauf circonstances particulières, ne saurait excéder un an et court, dans la première hypothèse, de la date de naissance de la décision implicite et, dans la seconde, de la date de l'événement établissant qu'il a eu connaissance de la décision.... ,,En ce qui concerne les décisions implicites de rejet relevant du plein contentieux nées avant le 1er janvier 2017 (date à partir de laquelle ces décisions sont soumises en vertu du décret n° 2016-1480 du 2 novembre 2016 au droit commun de la naissance du délai de recours), dont il est établi que le demandeur a eu connaissance avant cette date, mais pour lesquelles l'administration, alors qu'elle était soumise à cette obligation, n'a pas délivré d'accusé de réception ou a délivré un accusé de réception ne comportant pas les mentions requises, le délai de recours expire le 31 décembre 2017, sauf circonstances particulières invoquées par le requérant.</ANA>
<ANA ID="9B"> 01-04-03-07 Le principe de sécurité juridique fait obstacle à ce que le demandeur, lorsqu'il est établi qu'il a eu connaissance de la décision implicite qui lui a été opposée, puisse la contester indéfiniment du seul fait que l'administration ne lui a pas délivré d'accusé de réception de sa demande ou n'a pas porté sur l'accusé de réception les mentions requises. La preuve d'une telle connaissance peut résulter de ce qu'il est établi, soit que l'intéressé a été clairement informé des conditions de naissance d'une décision implicite lors de la présentation de sa demande, soit que la décision a par la suite été expressément mentionnée au cours de ses échanges avec l'administration, notamment à l'occasion d'un recours gracieux dirigé contre cette décision. Le demandeur dispose alors, pour saisir le juge, d'un délai raisonnable qui, sauf circonstances particulières, ne saurait excéder un an et court, dans la première hypothèse, de la date de naissance de la décision implicite et, dans la seconde, de la date de l'événement établissant qu'il a eu connaissance de la décision.... ,,En ce qui concerne les décisions implicites de rejet relevant du plein contentieux nées avant le 1er janvier 2017 (date à partir de laquelle ces décisions sont soumises en vertu du décret n° 2016-1480 du 2 novembre 2016 au droit commun de la naissance du délai de recours), dont il est établi que le demandeur a eu connaissance avant cette date, mais pour lesquelles l'administration, alors qu'elle était soumise à cette obligation, n'a pas délivré d'accusé de réception ou a délivré un accusé de réception ne comportant pas les mentions requises, le délai de recours expire le 31 décembre 2017, sauf circonstances particulières invoquées par le requérant.</ANA>
<ANA ID="9C"> 48-02-01-09 Un litige relatif à la pension de réversion de l'ayant-droit d'un militaire ne saurait être regardé comme un litige entre l'administration et l'un de ses agents. Par suite, l'administration est soumise à l'obligation prévue à l'article L. 112-6 du code des relations entre le public et l'administration (CRPA) de transmettre au pétitionnaire l'accusé de réception prévu par l'article L. 112-3 du même code.</ANA>
<ANA ID="9D"> 54-01-07-02 Le principe de sécurité juridique fait obstacle à ce que le demandeur, lorsqu'il est établi qu'il a eu connaissance de la décision implicite qui lui a été opposée, puisse la contester indéfiniment du seul fait que l'administration ne lui a pas délivré d'accusé de réception de sa demande ou n'a pas porté sur l'accusé de réception les mentions requises. La preuve d'une telle connaissance peut résulter de ce qu'il est établi, soit que l'intéressé a été clairement informé des conditions de naissance d'une décision implicite lors de la présentation de sa demande, soit que la décision a par la suite été expressément mentionnée au cours de ses échanges avec l'administration, notamment à l'occasion d'un recours gracieux dirigé contre cette décision. Le demandeur dispose alors, pour saisir le juge, d'un délai raisonnable qui, sauf circonstances particulières, ne saurait excéder un an et court, dans la première hypothèse, de la date de naissance de la décision implicite et, dans la seconde, de la date de l'événement établissant qu'il a eu connaissance de la décision.... ,,En ce qui concerne les décisions implicites de rejet relevant du plein contentieux nées avant le 1er janvier 2017 (date à partir de laquelle ces décisions sont soumises en vertu du décret n° 2016-1480 du 2 novembre 2016 au droit commun de la naissance du délai de recours), dont il est établi que le demandeur a eu connaissance avant cette date, mais pour lesquelles l'administration, alors qu'elle était soumise à cette obligation, n'a pas délivré d'accusé de réception ou a délivré un accusé de réception ne comportant pas les mentions requises, le délai de recours expire le 31 décembre 2017, sauf circonstances particulières invoquées par le requérant.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., s'agissant des conditions d'applicabilité du décret du 2 novembre 2016 aux décisions implicites nées antérieurement au 1er janvier 2017, CE, 30 janvier 2019, M.,, n° 420797, p. 8.,,[RJ2] Cf. CE, Assemblée, 13 juillet 2016, M.,, n° 387763, p. 340., ,[RJ3] Rappr., sur l'applicabilité de la jurisprudence Czabaj aux décisions implicites ne relevant pas du plein contentieux, CE, 18 mars 2019, M.,, n° 417270, p. 67.,.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
