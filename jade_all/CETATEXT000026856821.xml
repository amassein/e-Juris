<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026856821</ID>
<ANCIEN_ID>JG_L_2012_12_000000347674</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/85/68/CETATEXT000026856821.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 28/12/2012, 347674</TITRE>
<DATE_DEC>2012-12-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>347674</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean-Marc Vié</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:347674.20121228</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi, enregistré le 21 mars 2011 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre de la défense ; il demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 0816834 du 20 janvier 2011 par lequel le tribunal administratif de Paris, statuant sur la demande de M.B..., en premier lieu, a annulé ses décisions des 24 juillet et 29 septembre 2008 par lesquelles il a respectivement porté à la connaissance de l'intéressé le fait qu'il mettait fin, à compter du 1er septembre 2008, au versement de la nouvelle bonification indiciaire qui lui était auparavant servie, et rejeté le recours administratif formé contre cet acte, et, en second lieu, lui a enjoint de rétablir le demandeur dans ses droits à cette bonification dans le délai de deux mois suivant notification du jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de M.B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 83-634 du 13 juillet 1983 ;<br/>
<br/>
              Vu la loi n° 84-16 du 11 janvier 1984 ;<br/>
<br/>
              Vu la loi n° 91-73 du 18 janvier 1991 ;<br/>
<br/>
              Vu le décret n° 2006-779 du 3 juillet 2006 ;<br/>
<br/>
              Vu le décret n° 2007-887 du 14 mai 2007 ; <br/>
<br/>
              Vu l'arrêté ministériel du 16 mai 2007 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Vié, Maître des Requêtes, <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du I de l'article 27 de la loi du 18 janvier 1991 : " La nouvelle bonification indiciaire des fonctionnaires et des militaires instituée à compter du 1er août 1990 est attribuée pour certains emplois comportant une responsabilité ou une technicité particulières dans des conditions fixées par décret. " ; qu'aux termes de l'article 1er du décret du 14 mai 2007 instituant la nouvelle bonification indiciaire dans les services du ministère de la défense : " Une nouvelle bonification indiciaire, prise en compte et soumise à cotisation pour le calcul de la pension de retraite, peut être versée mensuellement, dans la limite des crédits disponibles, aux fonctionnaires du ministère de la défense exerçant une des fonctions figurant en annexe au présent décret. " ; que l'article 5 de ce même décret précise que : " Pour chacune de ces fonctions, la désignation des emplois, le niveau de responsabilité, les montants en points d'indice majorés, le nombre maximum d'emplois et le nombre maximum de points sont fixés par un arrêté conjoint du ministre chargé de la défense, du ministre chargé de l'économie et des finances, du ministre chargé de la fonction publique et du ministre chargé du budget. " ; que l'arrêté du 14 mai 2007 a fixé les conditions d'attribution de la nouvelle bonification indiciaire dans les services du ministère de la défense ; qu'il a été complété par l'arrêté du 16 mai 2007, modifié par l'arrêté du 21 mai 2008, fixant la liste des emplois tenus par des fonctionnaires ouvrant droit au bénéfice d'une nouvelle bonification indiciaire ;<br/>
<br/>
              2. Considérant qu'il résulte des dispositions de l'article 27 de la loi du 18 janvier 1991 que le pouvoir réglementaire peut limiter le versement de la nouvelle bonification indiciaire aux agents occupant les emplois qu'il détermine, comportant une responsabilité ou une technicité particulières ; qu'il est loisible à l'administration, lorsqu'elle établit la liste des emplois ouvrant droit à cette bonification, de prendre en considération des raisons budgétaires et des orientations de politique de gestion des personnels ; que l'administration peut, sous le contrôle du juge, supprimer un emploi de cette liste en se fondant sur les mêmes motifs, l'agent occupant cet emploi n'ayant aucun droit au maintien de la bonification ; que, dans tous les cas, l'administration doit, conformément au principe d'égalité, traiter de la même manière tous les agents occupant les emplois correspondant aux fonctions ouvrant droit à la bonification ou n'y ouvrant plus droit et qui comportent la même responsabilité ou la même technicité particulières ; que lorsqu'un emploi a été légalement supprimé de la liste des emplois ouvrant droit à la nouvelle bonification indiciaire, l'administration est tenue de mettre fin au versement de la nouvelle bonification indiciaire à l'agent concerné ;<br/>
<br/>
              3. Considérant que, pour faire droit à la demande de M.B..., le tribunal administratif de Paris a estimé que l'administration ne contestait pas sérieusement ses allégations selon lesquelles tant les responsabilités que la technicité des fonctions qu'il exerçait, qui avaient justifié l'attribution de la nouvelle bonification indiciaire, n'avaient pas été affectées par le changement du périmètre de responsabilité et que la mission d'assistant " qualité interne " ne constituait qu'une activité annexe sans lien avec le poste permanent ;<br/>
<br/>
              4. Mais considérant que, dans son mémoire en défense devant le tribunal administratif, le ministre s'est également prévalu, pour justifier la légalité de l'acte en cause, du fait que la répartition de l'enveloppe des points impartis au titre de la nouvelle bonification indiciaire avait été révisée à la baisse lors des travaux d'actualisation de l'arrêté du 16 mai 2007 en raison de l'augmentation du nombre des personnels civils, et que seuls les emplois répertoriés dans la nouvelle liste annexée à cet arrêté, que l'administration pouvait librement modifier, donnaient lieu à bonification, de sorte que tant le contenu, le niveau de responsabilité des fonctions que la circonstance que l'intéressé occupait le même emploi qu'auparavant étaient sans incidence sur le droit au bénéfice de la bonification ;<br/>
<br/>
              5. Considérant qu'en estimant que les motifs ainsi invoqués par le ministre ne pouvaient justifier la suppression de l'emploi occupé par M. B...de la liste au regard de l'article 1er de la loi du 18 janvier 1991, alors que cette suppression résultait d'une nouvelle définition des emplois bénéficiant de la nouvelle bonification indiciaire, liée en particulier à des contraintes budgétaires, et que l'intéressé soutenait, non que des emplois de responsabilité ou de technicité équivalente auraient été maintenus dans cette liste, mais seulement, de manière inopérante, que ses fonctions n'avaient pas évolué, le tribunal administratif a commis une erreur de droit ; que le ministre de la défense est, par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, fondé  à demander, pour ce motif, l'annulation du jugement attaqué ;<br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              7. Considérant, ainsi qu'il vient d'être dit, qu'il était loisible au ministre de la défense, par l'arrêté du 21 mai 2008, de supprimer pour des raisons budgétaires et de réorganisation de ses services l'emploi de déclarant de douanes senior qu'occupait M. B...de la liste de ceux ouvrant droit au bénéfice de la nouvelle bonification indiciaire fixée par l'arrêté du 16 mai 2007 ; que ces motifs, invoqués devant le juge par le ministre, sont de nature à fonder légalement l'arrêté du 21 mai 2008 ; que l'administration aurait pris les mêmes actes si elle s'était fondée initialement sur ces motifs ; que la substitution de motifs proposée par le ministre ne prive le requérant d'aucune garantie procédurale ; que, dès lors que l'emploi occupé par l'intéressé a été légalement supprimé de cette liste, le ministre de la défense était tenu, à compter de l'entrée en vigueur de l'arrêté du 21 mai 2008, de mettre fin au versement de cette bonification à l'intéressé ; qu'il suit de là que les autres moyens, tirés des droits que la décision lui attribuant une nouvelle bonification indiciaire lui aurait conférés, de la méconnaissance du principe de sécurité juridique et de la violation du décret du 3 juillet 2006 portant attribution de la nouvelle bonification indiciaire à certains personnels de la fonction publique territoriale sont, en tout état de cause, inopérants ;<br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin de statuer sur la recevabilité de la demande de M.B..., celle-ci doit être rejetée, y compris ses conclusions à fin d'injonction ;  <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 20 janvier 2011 du tribunal administratif de Paris est annulé.<br/>
Article 2 : La demande de M. B...est rejetée.<br/>
Article 3 : La présente décision sera notifiée au ministre de la défense et à M. A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-08-03 FONCTIONNAIRES ET AGENTS PUBLICS. RÉMUNÉRATION. INDEMNITÉS ET AVANTAGES DIVERS. - NOUVELLE BONIFICATION INDICIAIRE (NBI) - ETABLISSEMENT DE LA LISTE DES EMPLOIS OUVRANT DROIT À CETTE BONIFICATION - 1) FACULTÉ POUR L'ADMINISTRATION DE PRENDRE EN CONSIDÉRATION DES RAISONS BUDGÉTAIRES ET DES ORIENTATIONS DE POLITIQUE DE GESTION DES PERSONNELS - EXISTENCE [RJ1] - 2) FACULTÉ POUR L'ADMINISTRATION DE SUPPRIMER UN EMPLOI DE CETTE LISTE EN SE FONDANT SUR LES MÊMES MOTIFS - EXISTENCE - 3) PRINCIPE D'ÉGALITÉ - CONSÉQUENCE - OBLIGATION DE TRAITER DE LA MÊME MANIÈRE TOUS LES AGENTS OCCUPANT LES EMPLOIS CORRESPONDANT AUX FONCTIONS OUVRANT DROIT À LA BONIFICATION OU N'Y OUVRANT PLUS DROIT ET QUI COMPORTENT LA MÊME RESPONSABILITÉ OU LA MÊME TECHNICITÉ PARTICULIÈRES [RJ2] - 4) SUPPRESSION D'UN EMPLOI DE LA LISTE - CONSÉQUENCE - OBLIGATION POUR L'ADMINISTRATION DE METTRE FIN AU VERSEMENT DE LA NBI À L'AGENT CONCERNÉ.
</SCT>
<ANA ID="9A"> 36-08-03 Il résulte des dispositions de l'article 27 de la loi n° 91-73 du 18 janvier 1991 que le pouvoir réglementaire peut limiter le versement de la nouvelle bonification indiciaire (NBI) aux agents occupant les emplois qu'il détermine, comportant une responsabilité ou une technicité particulières. 1) Il est loisible à l'administration, lorsqu'elle établit la liste des emplois ouvrant droit à cette bonification, de prendre en considération des raisons budgétaires et des orientations de politique de gestion des personnels. 2) L'administration peut, sous le contrôle du juge, supprimer un emploi de cette liste en se fondant sur les mêmes motifs, l'agent occupant cet emploi n'ayant aucun droit au maintien de la bonification. 3) Dans tous les cas, l'administration doit, conformément au principe d'égalité, traiter de la même manière tous les agents occupant les emplois correspondant aux fonctions ouvrant droit à la bonification ou n'y ouvrant plus droit et qui comportent la même responsabilité ou la même technicité particulières. 4) Lorsqu'un emploi a été légalement supprimé de la liste des emplois ouvrant droit à la nouvelle bonification indiciaire, l'administration est tenue de mettre fin au versement de la NBI à l'agent concerné.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., sur la prise en considération des contraintes budgétaires, CE, 30 janvier 2012, Mme Orsatelli, n° 341378, à mentionner aux Tables sur un autre point ; CE, 3 mai 2006, Mantin, n° 258449, T. pp. 710-716-733 sur un autre point.,,[RJ2] Rappr., sur les conséquences du principe d'égalité sur l'attribution de la NBI, CE, 30 janvier 2012, Mme Orsatelli, n° 341378, à mentionner aux Tables ; CE, 26 mai 2010, Garde des sceaux, ministre de la justice c/ Mlle Duchateau, n° 307786, T. pp. 617-824 ; CE, 3 octobre 2003, Association nationale des directeurs généraux et des directeurs généraux adjoints des conseils régionaux et des conseils généraux, n° 243483, T. pp. 638-643-833 ; CE, 22 juin 1998, Laye, n° 174860, p. 242 ; CE, 2 mai 1994, Volat, n° 138272, p. 221.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
