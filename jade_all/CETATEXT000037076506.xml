<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037076506</ID>
<ANCIEN_ID>JG_L_2018_06_000000420953</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/07/65/CETATEXT000037076506.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 14/06/2018, 420953, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-06-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420953</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2018:420953.20180614</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au juge des référés du tribunal administratif de Strasbourg, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'une part, d'enjoindre au préfet, à titre principal, d'enregistrer sa demande d'asile et de lui délivrer une attestation de demande d'asile dans un délai de trois jours à compter de la notification de l'ordonnance à intervenir et, à titre subsidiaire, de réexaminer sa situation et, d'autre part, d'ordonner la suspension de l'exécution de la décision du 13 avril 2018 par laquelle le préfet du Haut-Rhin a prolongé le délai de transfert aux autorités italiennes pour le porter à 18 mois. Par une ordonnance n° 1803094 du 18 mai 2018, le juge des référés du tribunal administratif de Strasbourg a rejeté sa demande.  <br/>
<br/>
              Par une requête enregistrée le 27 mai 2018 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de faire droit à sa demande de première instance.   <br/>
<br/>
<br/>
              Il soutient que : <br/>
              - la condition d'urgence est remplie dès lors que, actuellement placé en centre de rétention administrative en vue de l'exécution de l'arrêté du 1er décembre 2017, il est susceptible d'être transféré à tout moment en Italie ; <br/>
              - il est porté une atteinte grave et manifestement illégale à son droit d'asile dès lors que, d'une part, il revient aux autorités française de traiter sa demande d'asile dans la mesure où le délai d'exécution de six mois de la mesure de transfert aux autorités italiennes a expiré et, d'autre part, ce délai ne pouvait être légalement prolongé dès lors que l'intéressé ne s'est pas soustrait de façon intentionnelle et systématique au contrôle de l'autorité administrative en vue de faire obstacle à la mesure d'éloignement le concernant. <br/>
<br/>
              Vu les autres pièces du dossier : <br/>
              Vu :<br/>
              - le règlement (UE) n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              	- le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures " ; qu'en vertu de l'article L. 522-3 du même code, le juge des référés peut rejeter une requête par une ordonnance motivée, sans instruction contradictoire ni audience publique, lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée ; qu'à cet égard, il appartient au juge d'appel de prendre en considération les éléments recueillis par le juge de première instance dans le cadre de la procédure écrite et orale qu'il a diligentée ; <br/>
<br/>
              2. Considérant que le droit constitutionnel d'asile, qui a le caractère d'une liberté fondamentale, a pour corollaire le droit de solliciter le statut de réfugié ; que s'il implique que l'étranger qui sollicite la reconnaissance de la qualité de réfugié soit en principe autorisé à demeurer sur le territoire jusqu'à ce qu'il ait été statué sur sa demande, ce droit s'exerce dans les conditions définies par le code de l'entrée et du séjour des étrangers et du droit d'asile ; que l'article L. 742-3 de ce code prévoit que l'étranger dont l'examen de la demande d'asile relève de la responsabilité d'un autre Etat peut faire l'objet d'un transfert vers l'Etat qui est responsable de cet examen en application des dispositions du règlement du Parlement européen et du Conseil du 26 juin 2013 ; qu'en application de l'article 29 de ce règlement, et sauf dans les cas de prolongation qu'il prévoit en cas d'emprisonnement ou de fuite de la personne concernée, le transfert ne peut avoir lieu que dans un délai de six mois à compter de l'acceptation de la demande de prise en charge ; que ce délai est susceptible d'être porté à dix-huit mois si l'intéressé " prend la fuite " ;<br/>
<br/>
              3. Considérant que M.B..., de nationalité soudanaise, est entré sur le territoire français le 9 juillet 2017 et a sollicité l'asile le 9 août 2017 ; que la consultation du fichier " Eurodac " ayant permis d'établir que ses empreintes avaient été relevées en Italie le 19 juin 2017, le préfet du Haut-Rhin a sollicité sa prise en charge par les autorités italiennes ; que, par décision tacite du 4 novembre 2017, les autorités italiennes ont accepté de prendre en charge la demande d'asile de l'intéressé en application du règlement du 26 juin 2003 ; que, par un arrêté du 1er décembre 2017, le préfet du Haut-Rhin a décidé son transfert aux autorités italiennes et, par un arrêté du 9 janvier 2018, l'a assigné à résidence pour une durée de quarante-cinq jours avec obligation de se présenter tous les mardis à la brigade mobile de recherche de Mulhouse ; qu'estimant que M. B...était en fuite, le préfet du Haut-Rhin a informé les autorités italiennes le 13 avril 2018 que le délai de sa réadmission vers l'Italie était porté à dix-huit mois ; que le 7 mai 2018, M. B...a sollicité l'enregistrement de sa demande d'asile auprès de la préfecture du Haut-Rhin ; que le préfet a refusé d'enregistrer sa demande ; que M. B... a demandé au juge des référés du tribunal administratif de Strasbourg, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'une part, d'enjoindre au préfet, à titre principal, d'enregistrer sa demande d'asile et de lui délivrer une attestation de demande d'asile ou, à titre subsidiaire, de réexaminer sa situation et, d'autre part, d'ordonner la suspension de l'exécution de la décision du 13 avril 2018 par laquelle le préfet du Haut-Rhin a prolongé le délai de transfert ; que, par une ordonnance du 18 mai 2018, dont M. B...relève appel, le juge des référés du tribunal administratif de Strasbourg a rejeté sa demande ;  <br/>
<br/>
              4. Considérant qu'il résulte de l'instruction qu'à la suite de la décision prononçant son transfert vers l'Italie, M. B...a été convoqué, par un courrier en date du 14 février 2018, à se présenter à l'aéroport de Bale-Mulhouse le 12 mars 2018 en vue de son transfert vers l'Italie ; qu'il ne s'est pas présenté à l'aéroport ; que les services préfectoraux l'ont convoqué à nouveau, par un courrier recommandé daté du 16 avril 2018 et présenté le 19 avril, en vue de procéder à son transfert, à se présenter le 24 avril 2018 au même aéroport, ce que l'intéressé s'est abstenu de faire ; que l'intéressé ne justifie d'aucune impossibilité matérielle l'ayant empêché de se rendre à cette convocation, la circonstance que l'hébergement où il se trouvait était à une trentaine de kilomètres de l'aéroport ne pouvant être regardée comme constituant une telle impossibilité ; que l'intéressé ne peut, en outre, utilement soutenir que l'administration n'aurait pas mis en oeuvre les moyens matériels et humains permettant de le conduire à l'aéroport ; qu'en refusant de déférer à ces convocations, M. B...a fait obstacle de manière intentionnelle et systématique à la décision de transfert le concernant ; que, dès lors, le préfet a pu légalement estimer que M. B...était en fuite au sens de l'article 29 du règlement du 26 juin 2003 et porter pour ce motif à dix-huit mois le délai d'exécution de la décision de transfert ; qu'il n'a ainsi porté aucune atteinte grave et manifestement illégale au droit d'asile de l'intéressé en refusant d'enregistrer sa demande d'asile présentée le 7 mai 2018 ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède qu'il est ainsi manifeste que la requête de M. B...ne peut être accueillie ; qu'il y a lieu de rejeter cette requête, selon la procédure prévue à l'article L. 522-3 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. B...est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. A...B....<br/>
Copie de la présente ordonnance sera adressée au ministre d'Etat, ministre de l'intérieur.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
