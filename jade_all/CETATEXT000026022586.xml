<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026022586</ID>
<ANCIEN_ID>JG_L_2012_06_000000349537</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/02/25/CETATEXT000026022586.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  7ème sous-section jugeant seule, 13/06/2012, 349537, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-06-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>349537</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 7ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>FOUSSARD</AVOCATS>
<RAPPORTEUR>M. Stéphane Bouchard</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CESJS:2012:349537.20120613</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête et le mémoire complémentaire enregistrés les 23 mai et 2 septembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. A, demeurant ... ; M. A demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision du 7 mars 2011 du ministre de la défense et des anciens combattants lui infligeant un blâme et la décision implicite de rejet de son recours gracieux en date du 27 juin 2011 ;<br/>
<br/>
              2°) d'annuler la décision implicite du ministre de la défense rejetant sa demande indemnitaire préalable et de condamner l'Etat à lui verser une indemnité de 30 691,91 euros de dommages et intérêts, augmentés des intérêts au taux légal eux-mêmes capitalisés ;<br/>
<br/>
              3°) d'enjoindre au ministre de la défense de supprimer dans son dossier toute mention de cette sanction et de sa demande, y compris tous les rapports et comptes-rendus, dans un délai d'un mois à compter de la notification de la présente décision, sous astreinte de 100 euros par jour de retard ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 3 000 euros sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
<br/>
              Vu la note en délibéré, enregistrée le 22 mai 2012, présentée pour M. A,<br/>
<br/>
              Vu le code de la défense ; <br/>
<br/>
              Vu la loi du 22 avril 1905 ; <br/>
<br/>
              Vu le décret n° 2008-967 du 16 septembre 2008 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Stéphane Bouchard, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de Me Foussard, avocat de M. A, <br/>
<br/>
              - les conclusions de M. Nicolas Boulouis, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à Me Foussard, avocat de M. A ;<br/>
<br/>
<br/>
<br/>Sur les conclusions tendant à l'annulation de la décision du 7 mars 2011 du ministre de la défense et des anciens combattants lui infligeant un blâme et de la décision implicite de rejet de son recours gracieux : <br/>
<br/>
              Considérant que le ministre de la défense a, par une décision du 7 mars 2011, infligé un blâme à M. A au motif qu'il ne s'est pas conformé aux ordres écrits donnés le 30 juin 2010 lui prescrivant de cesser toutes pratiques médicales non scientifiquement validées, qu'il a demandé à ses patients une " contribution " financière pour des actes professionnels réalisés à l'occasion du service, et qu'il a négligé l'établissement de décisions " médico-administratives " ; que M. A, se fondant sur l'illégalité de cette sanction, a déposé un recours gracieux contre la décision de sanction prise à son encontre, lequel a été rejeté implicitement par le ministre de la défense ; que M. A a contesté la sanction du blâme dont il a fait l'objet ; <br/>
<br/>
              Considérant, d'une part, que si le ministre de la défense a retenu comme premier motif de la sanction infligée à M. A le fait de ne pas s'être conformé aux ordres du 30 juin 2010, lui prescrivant de cesser toutes pratiques médicales non scientifiquement validées tenant en l'exercice de " l'ozonothérapie ", il ressort des termes mêmes du rapport du médecin général inspecteur, directeur régional du service de santé de Saint-Germain-en-Laye, sur l'exercice de la pratique médicale par M. A, en date du 23 novembre 2010, que ce dernier avait cessé cette pratique de " l'ozonothérapie " suite aux ordres reçus ; qu'ainsi, un tel motif est entaché d'inexactitude matérielle des faits ; que si le ministre soutient dans son mémoire en défense que M. A a eu des pratiques fautives autres tenant au recours à la " médecine énergisante " avec passage " des mains au dessus du corps " afin de " rééquilibrer les flux énergétiques ", lesquelles auraient pu légalement fonder la sanction, il ne substitue pas ce motif à celui tiré du non-respect de l'ordre reçu de cesser la pratique de " l'ozonothérapie " ;<br/>
<br/>
              Considérant, d'autre part, que la sanction contestée repose sur un deuxième motif tiré de ce que M. A a demandé une " contribution " financière de cinquante centimes à ses patients pour des séances d'acupuncture ; qu'il ressort des pièces du dossier que cette " contribution " recouvre simplement l'achat d'aiguilles à prix coûtant par les patients souhaitant bénéficier de séances d'acupuncture, en cas d'indisponibilité dans le service, sans que M. A en retire un quelconque bénéfice ; que, par suite, un tel motif ne peut légalement justifier la décision de sanction litigieuse ;<br/>
<br/>
              Considérant, enfin, que si le ministre de la défense a fondé la sanction sur un dernier motif tiré de ce que M. A a négligé l'établissement de décisions " médico-administratives " en ce qu'il n'a pas présenté deux patients en commission de réforme, il ne résulte pas de l'instruction que le ministre aurait pris la même décision en se fondant exclusivement sur ce motif ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens de la requête, que M. A est fondé à demander l'annulation des décisions attaquées ; <br/>
<br/>
              Sur les conclusions indemnitaires présentées par M. A : <br/>
<br/>
              Considérant, qu'en tout état de cause, M. A ne justifie pas des préjudices allégués ; que, par suite, ses conclusions indemnitaires doivent être rejetées ;  <br/>
<br/>
              Sur les conclusions à fin d'injonction : <br/>
<br/>
              Considérant qu'il y a lieu d'enjoindre au ministre de la défense et des anciens combattants de faire disparaître du dossier de M. A toute mention de la sanction du blâme dans un délai d'un mois à compter de la notification de la présente décision ; que, dans les circonstances de l'espèce, il n'y a pas lieu d'assortir cette injonction d'une astreinte ;<br/>
<br/>
              Sur les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat, sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative, le versement à M. A d'une somme de 1 500 euros au titre des frais exposés par lui et non compris dans les dépens ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La décision du 7 mars 2011 du ministre de la défense et des anciens combattants infligeant un blâme à M. A et la décision implicite de rejet née du silence gardé par le ministre de la défense sur son recours gracieux sont annulées. <br/>
Article 2 : Il est enjoint au ministre de la défense et des anciens combattants de faire disparaître du dossier de M. A toute mention de la sanction, dans un délai d'un mois à compter de la notification de la présente décision.<br/>
Article 3 : L'Etat versera à M. A une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : Le surplus des conclusions de la requête de M. A est rejeté. <br/>
Article 5 : La présente décision sera notifiée à M. Bertrand A et au ministre de la défense.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
