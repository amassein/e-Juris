<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034978074</ID>
<ANCIEN_ID>J2_L_2017_06_000001503917</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/97/80/CETATEXT000034978074.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>CAA de LYON, 5ème chambre - formation à 3, 15/06/2017, 15LY03917, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-06-15</DATE_DEC>
<JURIDICTION>CAA de LYON</JURIDICTION>
<NUMERO>15LY03917</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre - formation à 3</FORMATION>
<TYPE_REC>excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. CLOT</PRESIDENT>
<AVOCATS>SABATIER</AVOCATS>
<RAPPORTEUR>Mme Pascale  DECHE</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme BOURION</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
        M. A... B...a demandé au tribunal administratif de Lyon d'annuler les décisions du 26 mars 2015 par lesquelles le préfet du Rhône a refusé de lui délivrer un titre de séjour, lui a fait obligation de quitter le territoire français dans un délai de trente jours et a désigné le pays de renvoi. <br/>
<br/>
        Par un jugement n° 1503779 du 17 novembre 2015, le tribunal administratif de Lyon a rejeté sa demande. <br/>
<br/>
Procédure devant la cour <br/>
<br/>
        Par une requête enregistrée le 14 décembre 2015, M. B..., représenté par Me C..., demande à la cour :<br/>
        1°) d'annuler ce jugement du tribunal administratif de Lyon du 17 novembre 2015 ; <br/>
        2°) d'annuler pour excès de pouvoir les décisions susmentionnées ; <br/>
        3°) d'enjoindre au préfet du Rhône de lui délivrer un titre de séjour temporaire portant la mention " vie privée et familiale ", ou de réexaminer sa demande dans un délai d'un mois à compter de la notification de l'arrêt à intervenir, sous astreinte de 100 euros par jour de retard ; <br/>
        4°) de mettre à la charge de l'Etat le paiement à son conseil d'une somme de 1 200 euros en application des dispositions de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi n° 91-647 du 10 juillet 1991.<br/>
<br/>
        Il soutient que : <br/>
        - le préfet devait saisir la commission du titre de séjour ;<br/>
        - souffrant d'un syndrome de stress post-traumatique, il a besoin d'un suivi psychiatrique et d'un traitement médicamenteux qui ne sont pas disponibles en Arménie ; de plus le préfet ne conteste pas qu'il ne peut voyager sans risque ; le refus de titre de séjour méconnaît le 11° de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
        - il vit en France depuis près de cinq ans où résident ses parents ; il justifie d'une bonne intégration en France ; dans ces conditions, le refus de titre de séjour a été pris en méconnaissance des stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et des dispositions du 7° de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile ;  <br/>
        - la décision portant obligation de quitter le territoire français est illégale du fait de l'illégalité du refus de titre de séjour ;<br/>
        - cette décision méconnaît le 10° de l'article L. 511-4 du code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
        - elle est entachée d'erreur manifeste d'appréciation des conséquences sur sa situation personnelle ;<br/>
        - elle a été prise en méconnaissance de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
        - la décision fixant le pays de renvoi est illégale du fait de l'illégalité du refus de titre de séjour et de l'obligation de quitter le territoire français. <br/>
<br/>
        Par un mémoire enregistré le 16 mai 2017, le préfet conclut au rejet de la requête. <br/>
<br/>
        Il soutient que : <br/>
        - le requérant peut bénéficier d'un traitement adapté à son état de santé dans son pays d'origine ; <br/>
        - pour le surplus, il s'en rapporte à ses écritures de première instance. <br/>
<br/>
        M. B... a été admis au bénéfice de l'aide juridictionnelle totale par une décision du 27 janvier 2016.<br/>
<br/>
        Vu les autres pièces du dossier ; <br/>
<br/>
        Vu :<br/>
        - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
        - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
        - la loi n° 91-647 du 10 juillet 1991 ;<br/>
        - le code de justice administrative ;<br/>
<br/>
        Les parties ayant été régulièrement averties du jour de l'audience ;<br/>
<br/>
        Le président de la formation de jugement ayant dispensé le rapporteur public, sur sa proposition, de prononcer des conclusions à l'audience ; <br/>
<br/>
        Après avoir entendu au cours de l'audience publique le rapport de Mme Dèche, premier conseiller ;<br/>
<br/>
<br/>
<br/>
        1. Considérant que M. B..., ressortissant arménien, né le 3 juin 1985 est entré en France le 12 décembre 2010 ; que sa demande d'asile a été rejetée par l'Office français de protection des réfugiés et apatrides, le 17 janvier 2012 ; que ce refus a été confirmé par la Cour nationale du droit d'asile, le 6 juillet 2012 ; que par décisions du 9 août 2012, le préfet de l'Eure a refusé de lui délivrer un titre de séjour, l'a obligé à quitter le territoire français et a fixé un pays de renvoi ; que le 16 octobre 2012, il a présenté une demande de titre de séjour au regard de son état de santé ; que par décisions du 29 avril 2013, le préfet du Rhône a refusé de lui délivrer un titre de séjour et l'a obligé à quitter le territoire français ; que ces décisions ont été confirmées par un jugement du tribunal administratif de Lyon du 3 octobre 2013 ; que M. B... a présenté une nouvelle demande de titre de séjour au regard de son état de santé ; que par décisions du 26 mars 2015, le préfet du Rhône a refusé de lui délivrer un titre de séjour, l'a obligé à quitter le territoire français dans un délai de trente jours et a fixé le pays de renvoi ; que M. B... relève appel du jugement du 17 novembre 2015 par lequel le tribunal administratif de Lyon a rejeté sa demande tendant à l'annulation de ces décisions du 26 mars 2015 ; <br/>
<br/>
        Sur la légalité du refus de titre de séjour :<br/>
<br/>
        2. Considérant, en premier lieu, qu'aux termes de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile, dans sa rédaction alors applicable : " Sauf si sa présence constitue une menace pour l'ordre public, la carte de séjour temporaire portant la mention " vie privée et familiale " est délivrée de plein droit : /(...)/ 11° A l'étranger résidant habituellement en France dont l'état de santé nécessite une prise en charge médicale dont le défaut pourrait entraîner pour lui des conséquences d'une exceptionnelle gravité, sous réserve de l'absence d'un traitement approprié dans le pays dont il est originaire, sauf circonstance humanitaire exceptionnelle appréciée par l'autorité administrative après avis du directeur général de l'agence régionale de santé, sans que la condition prévue à l'article L. 311-7 soit exigée. La décision de délivrer la carte de séjour est prise par l'autorité administrative, après avis du médecin de l'agence régionale de santé de la région de résidence de l'intéressé, désigné par le directeur général de l'agence (...) " ;<br/>
<br/>
        3. Considérant que sous réserve des cas où la loi attribue la charge de la preuve à l'une des parties, il appartient au juge administratif, au vu des pièces du dossier, et compte tenu, le cas échéant, de l'abstention d'une des parties à produire les éléments qu'elle est seule en mesure d'apporter et qui ne sauraient être réclamés qu'à elle-même, d'apprécier si l'état de santé d'un étranger nécessite une prise en charge médicale dont le défaut pourrait entraîner pour lui des conséquences d'une exceptionnelle gravité, sous réserve de l'absence d'un traitement approprié dans le pays de renvoi, sauf circonstance humanitaire exceptionnelle ; que la partie qui justifie d'un avis du médecin de l'agence régionale de santé qui lui est favorable doit être regardée comme apportant des éléments de fait susceptibles de faire présumer l'existence ou l'absence d'un état de santé de nature à justifier la délivrance ou le refus d'un titre de séjour ; que, dans ce cas, il appartient à l'autre partie, dans le respect des règles relatives au secret médical, de produire tous éléments permettant d'apprécier l'état de santé de l'étranger et, le cas échéant, l'existence ou l'absence d'un traitement approprié dans le pays de renvoi ; que la conviction du juge, à qui il revient d'apprécier si l'état de santé d'un étranger justifie la délivrance d'un titre de séjour dans les conditions ci-dessus rappelées, se détermine au vu de ces échanges contradictoires ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
        4. Considérant qu'il ressort des pièces du dossier que par un avis du 7 janvier 2015, le médecin de l'agence régionale de santé a estimé que l'état de santé de M. B... nécessite une prise en charge médicale dont le défaut peut entraîner des conséquences d'une exceptionnelle gravité, que le traitement approprié n'existe pas en Arménie et que les soins nécessités par son état de santé présentent un caractère de longue durée ;<br/>
<br/>
        5. Considérant toutefois que le préfet a estimé " que l'ensemble des éléments relatifs aux capacités locales en matière de soins médicaux et de médicaments disponibles en Arménie résultant notamment des éléments fournis par l'ambassade de France en Arménie en date du 4 octobre 2013, le conseiller santé auprès du directeur général des étrangers en France du ministère de l'intérieur en date du 7 novembre 2013 et l'institut de santé des enfants et adolescents d'Erevan en date du 12 avril 2013 démontre le sérieux et les capacités des institutions arméniennes qui sont à même de traiter la majorité des maladies courantes et que les ressortissants arméniens sont indéniablement à même de trouver en Arménie un traitement adapté à leur état de santé " ; que si M. B... fait valoir que certains médicaments qui lui sont prescrits tels le Lysanxia ne sont pas disponibles en Arménie, les documents qu'il produit ne permettent pas d'établir qu'il ne pourrait bénéficier dans ce pays d'un traitement équivalent ; qu'enfin, il ne ressort pas des pièces du dossier, eu égard au caractère peu circonstancié des certificats médicaux produits sur ce point, que l'intéressé ne pourrait pas voyager sans risque vers l'Arménie ; qu'ainsi, le préfet du Rhône n'a pas méconnu les dispositions du 11° de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
        6. Considérant, en deuxième lieu, qu'aux termes de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Sauf si sa présence constitue une menace pour l'ordre public, la carte de séjour temporaire portant la mention " vie privée et familiale " est délivrée de plein droit : (...) 7° A l'étranger ne vivant pas en état de polygamie, qui n'entre pas dans les catégories précédentes ou dans celles qui ouvrent droit au regroupement familial, dont les liens personnels et familiaux en France, appréciés notamment au regard de leur intensité, de leur ancienneté et de leur stabilité, des conditions d'existence de l'intéressé, de son insertion dans la société française ainsi que de la nature de ses liens avec la famille restée dans le pays d'origine, sont tels que le refus d'autoriser son séjour porterait à son droit au respect de sa vie privée et familiale une atteinte disproportionnée au regard des motifs du refus, sans que la condition prévue à l'article L. 311-7 soit exigée. L'insertion de l'étranger dans la société française est évaluée en tenant compte notamment de sa connaissance des valeurs de la République (...) " ; <br/>
<br/>
        7. Considérant qu'aux termes de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " 1. Toute personne a droit au respect de sa vie privée et familiale, de son domicile et de sa correspondance. 2. Il ne peut y avoir ingérence d'une autorité publique dans l'exercice de ce droit que pour autant que cette ingérence est prévue par la loi et qu'elle constitue une mesure qui, dans une société démocratique, est nécessaire à la sécurité nationale, à la sûreté publique, au bien-être économique du pays, à la défense de l'ordre et à la prévention des infractions pénales, à la protection de la santé ou de la morale ou à la protection des droits et libertés d'autrui. " ;<br/>
<br/>
        8. Considérant que M. B... soutient qu'il vit en France depuis près de cinq ans, que ses parents y résident également et qu'il justifie d'une bonne intégration ; que, toutefois, il ressort des pièces du dossier qu'il est célibataire et sans enfant et qu'il ne justifie pas qu'il serait dépourvu de toute attache familiale dans son pays d'origine ; que, dans ces circonstances et eu égard aux conditions de séjour de l'intéressé en France, la décision de refus de titre de séjour n'a pas porté à son droit au respect de sa vie privée et familiale une atteinte disproportionnée par rapport aux buts en vue desquels elle a été prise ; qu'elle n'a, ainsi, méconnu ni les dispositions du 7° de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile ni les stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
        9. Considérant, en dernier lieu, que l'article L. 312-1 du code de l'entrée et du séjour des étrangers et du droit d'asile dispose que : " Dans chaque département, est instituée une commission du titre de séjour (...) " et qu'aux termes de l'article L. 312-2 du même code : " La commission est saisie par l'autorité administrative lorsque celle-ci envisage de refuser de délivrer ou de renouveler une carte de séjour temporaire à un étranger mentionné à l'article L. 313-11 ou de délivrer une carte de résident à un étranger mentionné aux articles L. 314-11 et L. 314-12, ainsi que dans le cas prévu à l'article L. 431-3 (...) " ;<br/>
<br/>
        10. Considérant qu'il résulte de ces dispositions que le préfet est tenu de saisir la commission du cas des étrangers qui remplissent effectivement les conditions prévues à l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile, auxquels il envisage de refuser le titre de séjour sollicité et non de celui de tous les étrangers qui se prévalent de ces dispositions ; qu'ainsi qu'il vient d'être dit, M. B... ne remplissait pas les conditions posées par les dispositions de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile ; que, par suite, le préfet du Rhône n'était pas tenu de soumettre son cas à la commission du titre de séjour avant de lui refuser la délivrance d'un titre de séjour sur ce fondement ; <br/>
<br/>
        Sur la légalité de l'obligation de quitter le territoire français : <br/>
<br/>
        11. Considérant, en premier lieu, que, comme il a été dit ci-dessus, la décision refusant de délivrer un titre de séjour à M. B... n'est pas entachée d'illégalité ; que, par suite, le moyen tiré, par voie d'exception, de l'illégalité de ce refus de titre doit être écarté ;<br/>
<br/>
        12. Considérant, en deuxième lieu, qu'aux termes de l'article L. 511-4 du code de l'entrée et du séjour des étrangers et du droit d'asile dans sa rédaction alors applicable : " Ne peuvent faire l'objet d'une obligation de quitter le territoire français : (...) 10° L'étranger résidant habituellement en France dont l'état de santé nécessite une prise en charge médicale dont le défaut pourrait entraîner pour lui des conséquences d'une exceptionnelle gravité, sous réserve de l'absence d'un traitement approprié dans le pays de renvoi, sauf circonstance humanitaire exceptionnelle appréciée par l'autorité administrative après avis du directeur général de l'agence régionale de santé (...) " ;<br/>
<br/>
        13. Considérant que, pour les mêmes motifs que ceux énoncés ci-avant, en faisant obligation de quitter le territoire français à M. B..., le préfet du Rhône n'a pas méconnu les dispositions du 10° de l'article  L. 511-4 du code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
        14. Considérant, en dernier lieu, que, compte tenu des éléments précédemment exposés, la décision portant obligation de quitter le territoire français n'a pas méconnu les stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et n'est pas entachée d'erreur manifeste dans l'appréciation de ses conséquences sur la situation personnelle de l'intéressé ; <br/>
<br/>
<br/>
<br/>
        Sur la légalité de la décision fixant le pays de renvoi :<br/>
<br/>
        15. Considérant que, compte-tenu de ce qui précède, les décisions refusant à M. B... un titre de séjour et portant obligation de quitter le territoire français ne sont pas entachées d'illégalité ; que, dès lors, l'intéressé n'est pas fondé à soulever, par voie d'exception, l'illégalité desdites décisions à l'encontre de la décision fixant le pays de renvoi ;<br/>
<br/>
        16. Considérant qu'il résulte de ce qui précède, que M. B... n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Lyon a rejeté sa demande ; que ses conclusions aux fins d'injonction ainsi que celles de son conseil tendant au bénéfice des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ne peuvent, par voie de conséquence, qu'être rejetées ;<br/>
<br/>
<br/>
DÉCIDE :<br/>
Article 1er : La requête de M. B... est rejetée. <br/>
Article 2 : Le présent arrêt sera notifié à M . AlekB... et au ministre de l'intérieur. Copie en sera adressée au préfet du Rhône.<br/>
Délibéré après l'audience du 24 mai 2017 à laquelle siégeaient :<br/>
M. Clot, président de chambre,<br/>
Mme Dèche, premier conseiller,<br/>
M. Savouré, premier conseiller.<br/>
Lu en audience publique, le 15 juin 2017.<br/>
1<br/>
6<br/>
N° 15LY03917	<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">335-01-03 Étrangers. Séjour des étrangers. Refus de séjour.
</SCT>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
