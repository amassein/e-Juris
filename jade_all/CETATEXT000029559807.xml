<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029559807</ID>
<ANCIEN_ID>JG_L_2014_10_000000380368</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/55/98/CETATEXT000029559807.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème / 9ème SSR, 08/10/2014, 380368, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-10-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>380368</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème / 9ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER</AVOCATS>
<RAPPORTEUR>M. Frédéric Bereyziat</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:380368.20141008</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
	Mme B...A...a demandé au juge des référés du tribunal administratif de Melun de suspendre, sur le fondement de l'article L. 521-1 du code de justice administrative, l'exécution de la décision du 4 mars 2014 par laquelle le directeur départemental des finances publiques du Val-de-Marne a rejeté sa contestation dirigée contre l'avis de mise en recouvrement qui lui a été notifié le 21 décembre 2013. Par une ordonnance n° 1403503/13 du 30 avril 2014, le juge des référés a rejeté cette demande comme portée devant une juridiction incompétente pour en connaître. <br/>
Procédure devant le Conseil d'Etat<br/>
	Par un pourvoi, enregistré le 15 mai 2014 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au Conseil d'Etat :<br/>
	1°) d'annuler l'ordonnance n° 1403503/13 du 30 avril 2014 du juge des référés du tribunal administratif de Melun ; <br/>
	2°) statuant en référé, de faire droit à sa demande ;<br/>
	3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code du travail ;<br/>
              - le décret du 26 octobre 1849, notamment son article 35 ; <br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Béreyziat, maître des requêtes,<br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Fabiani, Luc-Thaler, avocat de MmeA....<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que la SARL " All Technics Communication ", qui a pour activité la réalisation d'actions de formation professionnelle et pour gérante MmeA..., a été placée en redressement judiciaire le 9 novembre 2011 ; que, par une décision du 24 janvier 2012, le préfet de la région Ile-de-France a ordonné à cette société en application des dispositions de la section 1 du chapitre II du titre VI du livre III de la sixième partie du code du travail, de verser au Trésor différentes sommes correspondant à des actions de formation professionnelle dont la réalité n'était pas justifiée et à des dépenses financées par des cocontractants et non remboursées en temps utile à ceux-ci par la société ; que la société a contesté cette décision, en application de l'article R. 6362-6 du code du travail, par une requête désormais pendante devant la cour administrative d'appel de Paris ; que les services de l'administration fiscale compétents en matière de taxes sur le chiffre d'affaires ont été chargés du recouvrement des créances détenues sur la société en exécution de cette décision ; que, par un jugement du 29 mai 2013, devenu définitif, le tribunal de commerce de Créteil a rejeté la demande de ces services tendant à ce qu'ils soient relevés de la forclusion des créances en cause ; que, par un avis de mise en recouvrement daté du 18 décembre 2013 et notifié à Mme A...le 21 décembre 2013, ces services ont mis les mêmes sommes à la charge de MmeA..., à titre personnel, sur le fondement de l'article L. 6362-7 du code du travail, afin qu'elles soient recouvrées conformément aux prévisions de l'article L. 6362-12 du même code, soit selon les modalités applicables aux taxes sur le chiffre d'affaires ; que, le 6 janvier 2014, l'intéressée a formé devant l'administration fiscale un recours préalable, aux fins d'être déchargée des sommes mentionnées sur cet avis ; que son recours a été rejeté le 4 mars 2014 par le directeur départemental des finances publiques du Val-de-Marne ; que Mme A...a, d'une part, contesté cette décision de rejet devant le tribunal administratif de Melun, d'autre part, saisi le juge des référés de ce tribunal d'une demande tendant à ce que soit suspendue l'exécution de cette décision, sur le fondement de l'article L. 521-1 du code de justice administrative ; que, pour rejeter cette dernière demande, le juge des référés a estimé, en premier lieu, que le litige porté devant les juges du fond avait trait à l'obligation de payer et à l'exigibilité des créances que le Trésor estimait détenir sur Mme A...et relevait, par suite, de l'action en recouvrement prévue à l'article L. 281 du livre des procédures fiscales, en second lieu, qu'une telle action ne relevait pas, en l'espèce, de la compétence de la juridiction administrative, dès lors, d'une part, que l'appréciation de ses mérites impliquait notamment de déterminer les conséquences attachées par le code de commerce au déroulement d'une procédure de redressement judiciaire, s'agissant de la situation du codébiteur de dettes contractées par l'entreprise faisant l'objet de cette procédure, d'autre part, que cette dernière n'était pas clôturée à la date à laquelle le juge du recouvrement avait été saisi ;   <br/>
<br/>
              2. Considérant qu'aux termes de l'article 35 du décret du 26 octobre 1849, reproduit à l'article R. 771-2 du code de justice administrative : " Lorsque le Conseil d'Etat statuant au contentieux, la Cour de cassation ou tout autre juridiction statuant souverainement et échappant ainsi au contrôle tant du Conseil d'Etat que de la Cour de cassation, est saisi d'un litige qui présente à juger, soit sur l'action introduite, soit sur une exception, une question de compétence soulevant une difficulté sérieuse, et mettant en jeu la séparation des autorités administratives et judiciaires, la juridiction saisie peut, par décision ou arrêt motivé qui n'est susceptible d'aucun recours, renvoyer au Tribunal des conflits le soin de décider sur cette question de compétence " ;<br/>
<br/>
              3. Considérant que le litige né de l'action de Mme A...présente à juger une question de compétence soulevant une difficulté sérieuse et de nature à justifier le recours à la procédure prévue par l'article 35 du décret du 26 octobre 1849 ; que, par suite, il y a lieu de renvoyer au Tribunal des conflits la question de savoir si l'action introduite par Mme A...relève ou non de la compétence de la juridiction administrative et de surseoir à toute procédure jusqu'à la décision de ce tribunal ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'affaire est renvoyée au Tribunal des conflits.<br/>
Article 2 : Il est sursis à statuer sur le pourvoi de Mme A...jusqu'à ce que le Tribunal des conflits ait tranché la question de savoir si le litige né de l'action de Mme A...tendant à la suspension de l'exécution de la décision du 4 mars 2014 du directeur départemental des finances publiques du Val-de-Marne relève ou non de la compétence de la juridiction administrative.<br/>
Article 3 : La présente décision sera notifiée à Mme B...A...et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
