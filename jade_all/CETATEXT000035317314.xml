<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035317314</ID>
<ANCIEN_ID>JG_L_2017_07_000000407191</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/31/73/CETATEXT000035317314.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème chambres réunies, 19/07/2017, 407191</TITRE>
<DATE_DEC>2017-07-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>407191</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème chambres réunies</FORMATION>
<TYPE_REC>Appréciation de la légalité</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI ; SCP BORE, SALVE DE BRUNETON, MEGRET</AVOCATS>
<RAPPORTEUR>Mme Dorothée Pradines</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:407191.20170719</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un jugement n° 21500606 du 23 décembre 2016, le tribunal des affaires de sécurité sociale de la Marne a sursis à statuer sur les recours formés par la société Pol Roger à l'encontre de la décision implicite puis de la décision expresse du 28 octobre 2015 par lesquelles la commission de recours amiable de l'URSSAF Champagne-Ardenne a confirmé le refus opposé par l'organisme à la demande de la société de restitution des sommes versées au titre de la cotisation de base pour le financement du Fonds national d'aide au logement au taux de 0,1 % pour la période allant du 1er janvier 2011 au 31 décembre 2014, et saisi le Conseil d'Etat de la question de la légalité de l'article R. 834-7 du code de la sécurité sociale.<br/>
<br/>
              Par deux mémoires, enregistrés au secrétariat du contentieux du Conseil d'Etat les 15 mars et 24 mai 2017, l'URSSAF Champagne-Ardenne demande au Conseil d'État :<br/>
              - d'apprécier la légalité de l'article R. 834-7 du code de la sécurité sociale et de déclarer que ses dispositions ne sont pas entachées d'illégalité ;<br/>
              - de mettre à la charge de la société Pol Roger la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ;<br/>
              - le code de la sécurité sociale ;<br/>
              - la loi n° 71-582 du 16 juillet 1971 ;<br/>
              - la loi n° 2010-1657 du 29 décembre 2010 ;<br/>
              - la loi n° 2014-892 du 8 août 2014 ;   <br/>
              - la loi n° 2014-1554 du 22 décembre 2014 ;<br/>
              - le décret n° 72-526 du 29 juin 1972 ;<br/>
              - la décision n° 2014-706 DC du Conseil constitutionnel  du 18 décembre 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Dorothée Pradines, auditeur, <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boré, Salve de Bruneton, Mégret, avocat de la société Pol Roger et à la SCP Gatineau, Fattaccini, avocat de l' Urssaf Champagne-Ardenne.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Par un jugement du 23 décembre 2016, le tribunal des affaires de sécurité sociale de la Marne a sursis à statuer sur les recours formés par la société Pol Roger jusqu'à ce que le Conseil d'Etat se soit prononcé sur la question de savoir si les dispositions de l'article R. 834-7 du code de la sécurité sociale étaient entachées d'illégalité du fait de l'incompétence du pouvoir réglementaire pour fixer le taux de la cotisation relative à l'allocation de logement, que le Conseil constitutionnel a qualifié d'imposition de toute nature au sens de l'article 34 de la Constitution par sa décision n° 2014 -706 DC du 18 décembre 2014.<br/>
<br/>
              2. Aux termes de l'article L. 834-1 du code de la sécurité sociale, dans sa rédaction issue de l'article 209 de la loi du 29 décembre 2010 de finances pour 2011, applicable au cours de la période du 1er janvier 2011 au 31 décembre 2014 en litige devant le tribunal des affaires de sécurité sociale de la Marne : " Le financement de l'allocation de logement relevant du présent titre et des dépenses de gestion qui s'y rapportent est assuré par le fonds national d'aide au logement mentionné à l'article L. 351-6 du code de la construction et de l'habitation. / Pour concourir à ce financement, les employeurs sont assujettis à : / 1° Une cotisation assise sur les salaires plafonnés et recouvrée selon les règles applicables en matière de sécurité sociale ; / 2° Une contribution calculée par application d'un taux de 0,40 % sur la part des salaires plafonnés et d'un taux de 0,50 % sur la part des salaires dépassant le plafond, cette contribution étant recouvrée suivant les règles applicables en matière de sécurité sociale. / Les employeurs occupant moins de vingt salariés et les employeurs relevant du régime agricole au regard des lois sur la sécurité sociale ne sont pas soumis à la contribution mentionnée au 2°. Le cinquième alinéa de l'article L. 620-10 du code du travail s'applique au calcul de l'effectif mentionné au présent article ". La détermination des modalités d'application du titre dont relèvent ces dispositions est renvoyée à un décret en Conseil d'Etat par l'article L. 835-7 du code de la sécurité sociale. Aux termes de l'article R. 834-7 du même code, pris sur ce fondement : " La cotisation relative à l'allocation de logement prévue aux articles L. 831-1 et suivants est due par toute personne physique ou morale employant un ou plusieurs salariés relevant soit des professions non-agricoles, soit des professions agricoles. / Cette cotisation est calculée sur les rémunérations versées aux travailleurs salariés ou assimilés dans la limite du plafond prévu pour la fixation du montant des cotisations d'accidents du travail, d'allocations familiales et d'assurance vieillesse. Le taux de cette cotisation est fixé à 0,10 % des rémunérations définies ci-dessus. / La cotisation est, sous réserve des dispositions de l'article R. 834-9, recouvrée, pour le compte du fonds national d'aide au logement mentionné à l'article L. 351-6 du code de la construction et de l'habitation, par les organismes ou services chargés du recouvrement des cotisations de sécurité sociale ou d'assurances sociales agricoles ".<br/>
<br/>
              3. En premier lieu, par sa décision n° 2014-706 DC du 18 décembre 2014, le Conseil constitutionnel a déclaré inconstitutionnels l'article L. 834-1 du code de la sécurité sociale dans sa rédaction résultant du 7° du paragraphe I de l'article 2 de la loi du 8 août 2014 de financement rectificative de la sécurité sociale pour 2014 ainsi que, avant sa promulgation, l'article 12 de la loi de financement de la sécurité sociale pour 2015 qui le modifiait. Toutefois, il a considéré, par des motifs de la même décision qui s'imposent au Conseil d'Etat en vertu de l'article 62 de la Constitution, que la déclaration d'inconstitutionnalité de ce 7°, qui devait entrer en vigueur le 1er janvier 2015, avait pour effet de maintenir en vigueur la rédaction de l'article L. 834-1 du code de la sécurité sociale résultant de l'article 209 de la loi du 29 décembre 2010 de finances pour 2011. Par suite, la société Pol Roger n'est pas fondée à soutenir que la déclaration d'inconstitutionnalité prononcée par le Conseil constitutionnel aurait privé de base légale l'article R. 834-7 du code de la sécurité sociale pour la période en litige.   <br/>
<br/>
              4. En second lieu, il résulte des dispositions du 1° de l'article L. 834-1 du code de la sécurité sociale et de l'article L. 835-7 du même code dans leur rédaction en vigueur jusqu'au 31 décembre 2014, éclairées par les travaux préparatoires du troisième alinéa de l'article 7 et de l'article 19 de la loi du 16 juillet 1971 relative à l'allocation de logement qu'elles codifient, que le législateur, qui était d'ailleurs informé du taux déjà envisagé de 0,1 %, a entendu que soit fixé par décret en Conseil d'Etat le taux de la cotisation qu'il instituait et à laquelle il choisissait de faire suivre le régime des cotisations de sécurité sociale. <br/>
<br/>
              5. Dès lors, la société Pol Roger ne saurait utilement se prévaloir des dispositions de l'article 34 de la Constitution, qui réservent au législateur la fixation des règles concernant le taux des impositions de toute nature, pour soutenir que les dispositions réglementaires fixant le taux de cette cotisation seraient entachées d'incompétence.<br/>
<br/>
              6. Il résulte de ce qui précède que la société Pol Roger n'est pas fondée à soutenir que l'article R. 834-7 du code de la sécurité sociale serait entaché d'illégalité. <br/>
<br/>
              7. Les conclusions de la société Pol Roger présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de cette société une somme de 2 000 euros à verser à l'URSSAF Champagne-Ardenne au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                            --------------<br/>
Article 1er : Il est déclaré que l'exception d'illégalité de l'article R. 834-7 du code de la sécurité sociale soulevée par la société Pol Roger devant le tribunal des affaires de sécurité sociale de la Marne n'est pas fondée.<br/>
Article 2 : La société Pol Roger versera une somme de 2 000 euros à l'URSSAF Champagne-Ardenne au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Les conclusions de la société Pol Roger présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la société Pol Roger, à l'URSSAF Champagne-Ardenne et au tribunal des affaires de sécurité sociale de la Marne.<br/>
Copie en sera adressée au Premier ministre et à la ministre des solidarités et de la santé.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-02-01-04 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - COMPÉTENCE. LOI ET RÈGLEMENT. HABILITATIONS LÉGISLATIVES. - LOI RENVOYANT AU POUVOIR RÉGLEMENTAIRE LE SOIN DE FIXER LE TAUX DE LA COTISATION QU'ELLE INSTITUE - CONSÉQUENCE - MOYEN TIRÉ DE L'INCOMPÉTENCE DU POUVOIR RÉGLEMENTAIRE POUR FIXER LE TAUX DE CETTE COTISATION - MOYEN INOPÉRANT.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-01-04-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. MOYENS INOPÉRANTS. - MOYEN TIRÉ DE L'INCOMPÉTENCE DU POUVOIR RÉGLEMENTAIRE POUR FIXER LE TAUX D'UNE COTISATION INSTITUÉE PAR LA LOI, LORSQUE CELLE-CI A RENVOYÉ À UN DÉCRET LE SOIN DE FIXER CE TAUX.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-10-09 PROCÉDURE. - DÉCISION DU CONSEIL CONSTITUTIONNEL PRÉCISANT QUE L'INCONSTITUTIONNALITÉ DES DISPOSITIONS MODIFICATIVES QU'ELLE CONSTATE A POUR EFFET DE MAINTENIR EN VIGUEUR L'ANCIENNE RÉDACTION DE L'ARTICLE MODIFIÉ - CONSÉQUENCE - PERTE DE BASE LÉGALE DE L'ARTICLE RÉGLEMENTAIRE PRIS POUR L'APPLICATION DE CET ARTICLE DANS SON ANCIENNE RÉDACTION - ABSENCE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">62-03-02 SÉCURITÉ SOCIALE. COTISATIONS. ASSIETTE, TAUX ET CALCUL DES COTISATIONS. - COTISATION RELATIVE À L'ALLOCATION LOGEMENT (ART. L. 834-1 ET R. 834-7 DU CSS) - 1) DÉCISION DU CONSEIL CONSTITUTIONNEL PRÉCISANT QUE L'INCONSTITUTIONNALITÉ DES DISPOSITIONS MODIFICATIVES QU'ELLE CONSTATE A POUR EFFET DE MAINTENIR EN VIGUEUR L'ANCIENNE RÉDACTION DE L'ART. L. 834-1 - CONSÉQUENCE - PERTE DE BASE LÉGALE DE L'ART. R. 834-7 PRIS POUR SON APPLICATION - ABSENCE - 2) FIXATION DU TAUX DE LA COTISATION PAR LE POUVOIR RÉGLEMENTAIRE - HABILITATION LÉGISLATIVE - EXISTENCE - CONSÉQUENCE - MOYEN TIRÉ DE L'INCOMPÉTENCE DU POUVOIR RÉGLEMENTAIRE - MOYEN INOPÉRANT.
</SCT>
<ANA ID="9A"> 01-02-01-04 Il résulte du 1° de l'article L. 834-1 du code de la sécurité sociale et de l'article L. 835-7 du même code dans leur rédaction en vigueur jusqu'au 31 décembre 2014, éclairés par les travaux préparatoires du troisième alinéa de l'article 7 et de l'article 19 de la loi n° 71-582 du 16 juillet 1971 qu'ils codifient, que le législateur, qui était d'ailleurs informé du taux déjà envisagé de 0,1 %, a entendu que soit fixé par décret en Conseil d'Etat le taux de la cotisation qu'il instituait et à laquelle il choisissait de faire suivre le régime des cotisations de sécurité sociale....  ,,Par suite, le moyen tiré de la méconnaissance de l'article 34 de la Constitution, qui réserve au législateur la fixation des règles concernant le taux des impositions de toute nature, pour soutenir que les dispositions réglementaires fixant le taux de cette cotisation seraient entachées d'incompétence, ne saurait être utilement invoqué.</ANA>
<ANA ID="9B"> 54-07-01-04-03 Il résulte du 1° de l'article L. 834-1 du code de la sécurité sociale et de l'article L. 835-7 du même code dans leur rédaction en vigueur jusqu'au 31 décembre 2014, éclairés par les travaux préparatoires du troisième alinéa de l'article 7 et de l'article 19 de la loi n° 71-582 du 16 juillet 1971 qu'ils codifient, que le législateur, qui était d'ailleurs informé du taux déjà envisagé de 0,1 %, a entendu que soit fixé par décret en Conseil d'Etat le taux de la cotisation qu'il instituait et à laquelle il choisissait de faire suivre le régime des cotisations de sécurité sociale.... ,,Par suite, le moyen tiré de la méconnaissance de l'article 34 de la Constitution, qui réserve au législateur la fixation des règles concernant le taux des impositions de toute nature, pour soutenir que les dispositions réglementaires fixant le taux de cette cotisation seraient entachées d'incompétence, ne saurait être utilement invoqué.</ANA>
<ANA ID="9C"> 54-10-09 Par sa décision n° 2014-706 DC du 18 décembre 2014, le Conseil constitutionnel a déclaré inconstitutionnels l'article L. 834-1 du code de la sécurité sociale (CSS) dans sa rédaction résultant du 7° du paragraphe I de l'article 2 de la loi n° 2014-892 du 8 août 2014 ainsi que, avant sa promulgation, l'article 12 de la loi de financement de la sécurité sociale pour 2015 qui le modifiait. Toutefois, il a considéré, par des motifs de la même décision qui s'imposent au Conseil d'Etat en vertu de l'article 62 de la Constitution, que la déclaration d'inconstitutionnalité de ce 7°, qui devait entrer en vigueur le 1er janvier 2015, avait pour effet de maintenir en vigueur la rédaction de l'article L. 834-1 du CSS résultant de l'article 209 de la loi n° 2010-1657 du 29 décembre 2010.... ,,Par suite, la déclaration d'inconstitutionnalité prononcée par le Conseil constitutionnel n'a pas privé de base légale l'article R. 834-7 du CSS pour la période allant du 1er janvier 2011 au 31 décembre 2014.</ANA>
<ANA ID="9D"> 62-03-02 1) Par sa décision n° 2014-706 DC du 18 décembre 2014, le Conseil constitutionnel a déclaré inconstitutionnels l'article L. 834-1 du code de la sécurité sociale (CSS) dans sa rédaction résultant du 7° du paragraphe I de l'article 2 de la loi n° 2014-892 du 8 août 2014 ainsi que, avant sa promulgation, l'article 12 de la loi de financement de la sécurité sociale pour 2015 qui le modifiait. Toutefois, il a considéré, par des motifs de la même décision qui s'imposent au Conseil d'Etat en vertu de l'article 62 de la Constitution, que la déclaration d'inconstitutionnalité de ce 7°, qui devait entrer en vigueur le 1er janvier 2015, avait pour effet de maintenir en vigueur la rédaction de l'article L. 834-1 du CSS résultant de l'article 209 de la loi n° 2010-1657 du 29 décembre 2010.... ,,Par suite, la déclaration d'inconstitutionnalité prononcée par le Conseil constitutionnel n'a pas privé de base légale l'article R. 834-7 du CSS pour la période allant du 1er janvier 2011 au 31 décembre 2014.... ,,2) Il résulte du 1° de l'article L. 834-1 du code de la sécurité sociale et de l'article L. 835-7 du même code dans leur rédaction en vigueur jusqu'au 31 décembre 2014, éclairés par les travaux préparatoires du troisième alinéa de l'article 7 et de l'article 19 de la loi n° 71-582 du 16 juillet 1971 qu'ils codifient, que le législateur, qui était d'ailleurs informé du taux déjà envisagé de 0,1 %, a entendu que soit fixé par décret en Conseil d'Etat le taux de la cotisation qu'il instituait et à laquelle il choisissait de faire suivre le régime des cotisations de sécurité sociale.... ,,Par suite, le moyen tiré de la méconnaissance de l'article 34 de la Constitution, qui réserve au législateur la fixation des règles concernant le taux des impositions de toute nature, pour soutenir que les dispositions réglementaires fixant le taux de cette cotisation seraient entachées d'incompétence, ne saurait être utilement invoqué.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
