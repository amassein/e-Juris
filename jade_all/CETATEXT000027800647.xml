<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027800647</ID>
<ANCIEN_ID>JG_L_2013_08_000000359440</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/80/06/CETATEXT000027800647.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  4ème sous-section jugeant seule, 01/08/2013, 359440, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-08-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>359440</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 4ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jérôme Marchand-Arvier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:359440.20130801</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi, enregistré le 15 mai 2012 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre de la défense et des anciens combattants ; le ministre demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 10/00008 du 27 mars 2012 par lequel la cour régionale des pensions de Poitiers a confirmé le jugement du 31 décembre 2009 du tribunal départemental des pensions de Charente-Maritime accordant à M. A...B...la revalorisation de sa pension militaire d'invalidité, calculée initialement au grade de gendarme, en fonction de l'indice afférent au grade équivalent en vigueur pour les personnels de la marine nationale ,<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des pensions militaires d'invalidité et des victimes de la guerre ;<br/>
<br/>
              Vu le décret n° 56-913 du 5 septembre 1956 ;<br/>
<br/>
              Vu le décret n° 59-327 du 20 février 1959 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jérôme Marchand-Arvier, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du premier alinéa de l'article L. 24 du code des pensions militaires d'invalidité et des victimes de la guerre, alors en vigueur : " Les pensions militaires prévues par le présent code sont liquidées et concédées (...) par le ministre des anciens combattants et des victimes de guerre ou par les fonctionnaires qu'il délègue à cet effet. Les décisions de rejet des demandes de pension sont prises dans la même forme " ; que, d'une part, en vertu de l'article 5 du décret du 20 février 1959 relatif aux juridictions des pensions, l'intéressé dispose d'un délai de six mois pour contester, devant le tribunal départemental des pensions, la décision prise sur ce fondement ; que d'autre part, aux termes de l'article L. 78 du même code : " Les pensions définitives ou temporaires attribuées au titre du présent code peuvent être révisées dans les cas suivants : / 1° Lorsqu'une erreur matérielle de liquidation a été commise. / 2° Lorsque les énonciations des actes ou des pièces sur le vu desquels l'arrêté de concession a été rendu sont reconnues inexactes soit en ce qui concerne le grade, le décès ou le genre du mort, soit en ce qui concerne l'état des services, soit en ce qui concerne l'état civil ou la situation de famille, soit en ce qui concerne le droit au bénéfice d'un statut légal générateur de droits. / Dans tous les cas, la révision a lieu sans condition de délai (...). " ; <br/>
<br/>
              2. Considérant que le décalage défavorable entre l'indice de la pension servie à un ancien sous-officier de l'armée de terre, de l'armée de l'air ou de la gendarmerie et l'indice afférent au grade équivalent au sein des personnels de la marine nationale, lequel ne résulte ni d'une erreur matérielle dans la liquidation de sa pension, ni d'une inexactitude entachant les informations relatives à sa personne, ne figure pas au nombre des cas permettant la révision, sans condition de délai, d'une pension militaire d'invalidité ; qu'ainsi la demande présentée par le titulaire d'une pension militaire d'invalidité, concédée à titre temporaire ou définitif sur la base du grade que l'intéressé détenait dans l'armée de terre, l'armée de l'air ou la gendarmerie, tendant à la revalorisation de cette pension en fonction de l'indice afférent au grade équivalent applicable aux personnels de la marine nationale, doit être formée dans le délai de six mois fixé par l'article 5 du décret du 20 février 1959 ; que passé ce délai de six mois ouvert au pensionné pour contester l'arrêté lui concédant sa pension, l'intéressé ne peut demander sa révision que pour l'un des motifs limitativement énumérés aux 1° et 2° de cet article L. 78 ;<br/>
<br/>
              3. Considérant, par ailleurs, que les dispositions de l'article L. 78 du code des pensions militaires d'invalidité et des victimes de la guerre s'appliquent aux pensionnés comme à l'administration ; que, si elles prémunissent cette dernière contre des contestations tardives pour des motifs autres que les erreurs et omissions matérielles évoquées ci dessus, elles garantissent réciproquement aux titulaires de pensions d'invalidité que leurs droits ne pourront être remis en cause par l'administration, sans condition de délai, pour des erreurs de droit ; qu'en tout état de cause, elles ne font pas obstacle à ce que les pensionnés puissent faire valoir utilement leurs droits devant la juridiction des pensions, pour quelque motif que ce soit, dans le délai de recours prévu par l'article 5 du décret du 20 février 1959, dont la durée de six mois, dérogatoire au droit commun, n'apparaît pas manifestement insuffisante à cet effet ; que, par suite, ces dispositions ne sont pas contraires aux stipulations de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et du premier protocole additionnel à cette convention garantissant le droit à un recours effectif devant une juridiction ;<br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. B... a demandé, par lettres du 8 août 2006 et du 7 novembre 2008, datedemandeau ministre de la défense et des anciens combattants de recalculer la pension militaire d'invalidité qui lui avait été concédée par arrêté du 7 juillet 1981 en fonction de l'indice, plus favorable, afférent au grade équivalent dans la marine nationale ; que ces deux lettres ne pouvaient être regardées comme des demandes de révision relevant des dispositions de l'article L. 78 du code des pensions militaires d'invalidité et des victimes de la guerre mais uniquement comme des recours gracieux contre l'arrêté du 7 juillet 1981 ; qu'ainsi, en se bornant à constater que la requête présentée par M. B..., le 21 avril 2009, devant le tribunal départemental des pensions des Hautes-Pyrénées, était dirigée, non à l'encontre de l'arrêté du 7 juillet 1981, mais contre les décisions implicites de rejet de sa demande tendant à la revalorisation de sa pension, sans rechercher si cette demande avait été introduite dans le délai de six mois prévu par l'article 5 du décret du 20 février 1959, la cour régionale des pensions de Poitiers a commis une erreur de droit ; que, par suite, le ministre de la défense et des anciens combattants est fondé à demander l'annulation de l'arrêt attaqué ; <br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              6. Considérant qu'aux termes du dernier alinéa de l'article L. 25 du code des pensions militaires d'invalidité et des victimes de la guerre : " La notification des décisions prises en vertu de l'article L. 24, premier alinéa, du présent code, doit mentionner que le délai de recours contentieux court à partir de cette notification et que les décisions confirmatives à intervenir n'ouvrent pas de nouveau délai de recours " ; qu'ainsi, le délai de recours contentieux de six mois prévu à l'article 5 du décret du 20 février 1959 ne commence à courir que du jour où la décision primitive, prise en application du premier alinéa de l'article L. 24 du code des pensions militaires d'invalidité et des victimes de la guerre, a été notifiée au pensionné dans les formes prévues à l'article L. 25 du même code ou, à défaut, du jour où l'arrêté par lequel cette pension a été concédée à titre définitif, en application du deuxième alinéa du même article L. 24, a été régulièrement notifié à l'intéressé ;<br/>
<br/>
              7. Considérant qu'il ne résulte pas de l'instruction que la décision primitive de concession de la pension d'invalidité de M.B..., prise en vertu du premier alinéa de l'article L. 24 du code des pensions militaires d'invalidité et des victimes de la guerre, ait été notifiée à l'intéressé dans les formes prescrites par l'article L. 25 du même code ; que, cependant, il résulte de l'instruction que l'arrêté du 7 juillet 1981 portant concession définitive de cette pension a, quant à lui, été régulièrement notifié à l'intéressé, au regard des dispositions alors en vigueur qui n'imposaient pas encore que la notification de toute décision administrative mentionne les voies et délais de recours ouverts contre cette décision ; que, par suite et à supposer même que l'arrêté du 7 juillet 1981 ait été purement confirmatif de la décision primitive contre laquelle le délai de recours contentieux n'avait pu commencer à courir, ce délai a couru, en tout état de cause, au plus tard à compter de la notification régulière, le 6 août 1981, de ce même arrêté ; que les courriers que M. B...a adressés à l'administration en vue d'obtenir la revalorisation de sa pension qui, ainsi qu'il a été dit plus haut, devaient être regardés comme des recours gracieux contre l'arrêté du 7 juillet 1981, ont été présentés après l'expiration du délai de six mois fixé par l'article 5 du décret du 20 février 1959 ; que, par suite, le recours contentieux que l'intéressé a formé devant le tribunal départemental des pensions de Charente-Maritime, le 21 avril 2009, en vue d'obtenir la réformation de l'arrêté du 7 juillet  1981 portant concession de sa pension à titre définitif, était tardif ; <br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que le ministre de la défense et des anciens combattants est fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal départemental des pensions a fait droit à la demande de M.B... ;<br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : L'arrêt de la cour régionale des pensions de Poitiers du 27 mars 2012 et le jugement du tribunal départemental des pensions de Charente-Maritime du 31 décembre 2009 sont annulés.<br/>
Article 2: La requête présentée par M. B...devant le tribunal départemental des pensions de Charente-Maritime est rejetée. <br/>
Article 3 : La présente décision sera notifiée au ministre de la défense et à M. A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
