<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029134469</ID>
<ANCIEN_ID>JG_L_2014_06_000000356538</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/13/44/CETATEXT000029134469.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème SSJS, 24/06/2014, 356538, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-06-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>356538</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARTHELEMY, MATUCHANSKY, VEXLIARD, POUPOT</AVOCATS>
<RAPPORTEUR>M. Olivier Rousselle</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:356538.20140624</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête sommaire et le mémoire complémentaire, enregistrés les 7 février et 7 mai 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme E...D..., épouseB..., demeurant..., et Mme A... C...veuveD..., demeurant..., agissant tant en leurs noms propres qu'au titre de la succession de M. F...D... ; Mme D... et Mme C...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision de la Commission nationale d'aménagement foncier du 4 juillet 2011 statuant sur une réclamation des consorts D...relatives aux terres qui leur ont été attribuées dans le cadre du remembrement de la commune de Vallerange (Moselle) ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement d'une somme de 4 000 euros en application de l'article L. 761-1 du code justice administrative ; <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 5 juin 2014, présentée par Mme E...D... ; <br/>
<br/>
              Vu le code de l'expropriation pour cause d'utilité publique ; <br/>
<br/>
              Vu le code rural ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Rousselle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Barthélemy, Matuchansky, Vexliard, Poupot, avocat de Mme D...et de MmeC... ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que M. et Mme D...ont contesté les opérations de remembrement rural de la commune de Vallerange, ordonnées par un arrêté du préfet de la Moselle du 1er octobre 1990, en formant une réclamation devant la commission départementale d'aménagement foncier puis en déférant la décision de la commission départementale devant le tribunal administratif de Strasbourg, qui en a prononcé l'annulation par un jugement du 21 février 1994 ; que leur réclamation a fait l'objet le 6 décembre 1996 d'une décision de la Commission nationale d'aménagement foncier (CNAF) qui a été annulée par une décision du Conseil d'Etat statuant au contentieux du 25 avril 2001 ; qu'à la suite de cette annulation, la CNAF a statué à nouveau par une décision du 23 décembre 2001 qui a été à son tour annulée par une décision du Conseil d'Etat du 15 octobre 2004 ; que, saisie à nouveau de la réclamation, la CNAF a statué par une décision du 4 juillet 2011 dont Mme C..., veuveD..., agissant en son nom propre et en qualité d'ayant droit de son époux, et Mme E...D..., en sa qualité d'ayant droit de son père, demandent l'annulation ;<br/>
<br/>
              Sur la régularité en la forme de la décision attaquée :<br/>
<br/>
              2. Considérant qu'aucun texte ni aucun principe n'impose qu'une décision de la CNAF comporte la liste de ses membres ayant participé à la délibération ou mentionne explicitement qu'elle a été adoptée à la majorité des membres présents ; qu'ainsi le moyen tiré de ce que la décision attaquée serait irrégulière faute d'être revêtue de ces mentions doit être écarté ;   <br/>
<br/>
              Sur le refus de réattribuer aux consorts D...certaines des parcelles figurant parmi leurs apports :<br/>
<br/>
              3. Considérant qu'aux termes de l'article 20 de l'ancien code rural, applicable au litige eu égard à la date à laquelle le remembrement de la commune de Villerange a été ordonné, et dont les dispositions ont été ultérieurement reprises à l'article L. 123-3 du nouveau code : " (...) Doivent être réattribués à leurs propriétaires, sauf accord contraire, et ne subir que les modifications de limites indispensables à l'aménagement : (...) 4° Les immeubles présentant, à la date de l'arrêté fixant le périmètre de remembrement, les caractéristiques d'un terrain à bâtir au sens de l'article L. 13-15 du code de l'expropriation pour cause d'utilité publique ; / 5° De façon générale, les immeubles dont les propriétaires ne peuvent bénéficier de l'opération de remembrement, en raison de l'utilisation spéciale desdits immeubles " ; qu'il résulte du II de l'article L. 13-15 du code de l'expropriation pour cause d'utilité publique que la qualification de " terrain à bâtir " implique, d'une part, que le terrain soit desservi par une voie d'accès, un réseau électrique, un réseau d'eau potable et, le cas échéant, un réseau d'assainissement et, d'autre part, qu'il soit situé dans un secteur désigné comme constructible par un plan d'occupation des sols ou un document d'urbanisme en tenant lieu ou, en l'absence d'un tel document, dans une partie de la commune actuellement urbanisée ou désignée comme constructible par le conseil municipal et le représentant de l'Etat en application de l'article L. 11-1-3 du code de l'urbanisme ; <br/>
<br/>
              4. Considérant, en premier lieu, qu'il ressort des pièces du dossier que la CNAF a réattribué aux consorts D...les parcelles B490 et B493, qu'elle a regardées comme des terrains à bâtir, en ne leur apportant que des modifications de limites mineures et indispensables à l'aménagement, n'excédant pas celles qu'autorisent les dispositions précitées du 4° de l'article 20 de l'ancien code rural ;<br/>
<br/>
              5. Considérant, en deuxième lieu, que la CNAF a estimé que les parcelles 535, 636 et 641 ne présentaient pas le caractère de terrain à bâtir " compte tenu de leur situation en dehors de l'agglomération " ; que si les requérantes affirment qu'elle se serait fondée sur des plans incomplets pour conclure à l'absence de constructions à proximité, elles n'apportent pas d'éléments de nature à remettre en cause le constat sur lequel repose la décision attaquée, qui est corroboré par les pièces du dossier ; qu'eu égard aux conditions cumulatives posée par l'article L. 13-15 précité du code de l'expropriation publique, la circonstance que les parcelles en cause étaient desservies par la voirie et les réseaux collectifs ne suffisait pas pour leur conférer le caractère de terrains à bâtir ; que l'indication figurant dans la décision, selon laquelle ces parcelles sont situées " à gauche du chemin départemental n°174 " et au lieu dit " Ketzermatt ", à la supposer erronée, n'a pas eu d'incidence sur la qualification de terrain à bâtir ; que si les requérantes font valoir que la commission départementale d'aménagement foncier avait en 1992 reconnu aux parcelles en cause le caractère de terrains à bâtir, la commission nationale n'était pas liée par cette appréciation ; qu'il ressort des pièces du dossier qu'en ne regardant pas les parcelles 535, 636 et 641 comme des terrains à bâtir, cette commission, dont la décision est suffisamment motivée sur ce point, a fait une exacte application des dispositions précitées du 4° de l'article 20 de l'ancien code rural ;<br/>
<br/>
              6. Considérant, en troisième lieu, qu'il ne ressort pas des pièces du dossier que le point d'eau situé sur la parcelle n° 40 au lieu-dit " Wintersbruch "  soit doté d'aménagements suffisants pour conférer à cette parcelle le caractère de terrain à utilisation spéciale au sens du 5° de l'article 20 précité de l'ancien code rural ; qu'ainsi, en ne procédant pas à la réattribution intégrale de la parcelle n° 40, la commission nationale, dont la décision est suffisamment motivée sur ce point, n'a pas méconnu les dispositions de cet alinéa ;<br/>
<br/>
              7. Considérant, en quatrième lieu, que si les requérantes soutiennent que les parcelles A903 et A904 auraient dû être réattribuées en raison de la présence d'un puits maçonné, il ressort des pièces du dossier que les intéressés avaient renoncé à cette réattribution en raison de la transformation des parcelles en cause en étang postérieurement aux opérations de remembrement ; que la décision attaquée maintient les attributions de terrains effectuées en compensation de ces parcelles et accorde en outre aux requérants le versement d'une soulte de 500 euros à titre d'indemnisation pour l'aménagement du puits ; que, dans ces conditions, le moyen tiré de ce que la commission nationale se serait bornée à compenser la perte de cet aménagement, méconnaissant ainsi la portée de la réclamation présentée par les consortsD..., manque en fait ;<br/>
<br/>
              8. Considérant, en cinquième lieu, que la circonstance que certaines parcelles figurant parmi les apports des consorts D...avaient été utilisées par l'armée de terre aux fins d'exercice de tir n'était pas, à elle seule, de nature à les faire regarder comme des terrains à utilisation spéciale ; qu'en l'absence de tout élément relatif à l'existence d'aménagements sur  ces parcelles, le moyen tiré de ce que la commission nationale aurait méconnu  les dispositions du 5° de l'article 20 de l'ancien code rural en s'abstenant de les réattribuer ne saurait être accueilli ; <br/>
<br/>
              9. Considérant, enfin, qu'il ne ressort pas des pièces du dossier que les consorts D...auraient demandé la réattribution d'une parcelle sur laquelle la commission nationale aurait omis de se prononcer ; <br/>
<br/>
              Sur l'application de la règle de l'équivalence en valeur de productivité réelle :<br/>
<br/>
              10. Considérant qu'aux termes du premier alinéa de l'article 21 de l'ancien code rural, dont les dispositions ont été reprises à L. 123-4 du nouveau code : " Chaque propriétaire doit recevoir, par la nouvelle distribution, une superficie globale équivalente, en valeur de productivité réelle, à celle des terrains qu'il a apportés, déduction faite de la surface nécessaire aux ouvrages collectifs visés à l'article 25 et compte tenu des servitudes maintenues ou créées " ; que les cinquième à huitième alinéas du même article disposent que : " Sauf accord exprès des intéressés, l'équivalence en valeur de productivité réelle doit, en outre, être assurée par la commission communale dans chacune des natures de culture qu'elle aura déterminées. Il peut toutefois être dérogé, dans les limites qu'aura fixées la commission départementale pour chaque région agricole du département, à l'obligation d'assurer l'équivalence par nature de culture. / La commission départementale détermine, à cet effet :  / 1° Après avis de la chambre d'agriculture, des tolérances exprimées en pourcentage des apports de chaque propriétaire dans les différentes natures de culture et ne pouvant excéder 20 p. 100 de la valeur des apports d'un même propriétaire dans chacune d'elles ; / 2° Une surface en-deçà de laquelle les apports d'un propriétaire pourront être compensés par des attributions dans une nature de culture différente et qui ne peut excéder 50 ares en polyculture, ou 1 % de la surface minimale d'installation si celle-ci est supérieure à 50 hectares " ; <br/>
<br/>
              11. Considérant que la CNAF a estimé que le nombre d'arbres fruitiers présents sur le parcelles d'apport B490 et B493 n'était pas suffisant pour que ces parcelles soient regardées comme supportant des vergers ; qu'elle n'a pas, ce faisant, commis d'erreur de droit ; que le moyen tiré ce qu'elle aurait sur ce point entaché sa décision d'une erreur d'appréciation n'est pas assorti des précisions permettant d'en apprécier le bien fondé ; que, dès lors que les parcelles en cause ne pouvaient être regardées comme supportant des vergers, les parcelles d'apport des consorts D...dans cette nature de culture n'atteignaient pas le seuil au-dessus duquel, en vertu des dispositions précitées, la compensation des apports par des attributions de terres dans une nature de culture différente ne peut se faire qu'avec l'accord des propriétaires ; que le moyen tiré d'une violation des dispositions du huitième alinéa précité de l'article 21 de l'ancien code rural ne saurait, dès lors, être accueilli ; que, par ailleurs, la seule circonstance que la décision ne prenne pas expressément parti sur la classe dont relève la parcelle 49 de la section 7 ne l'entache pas d'insuffisance de motivation ; <br/>
<br/>
              Sur l'aggravation des conditions d'exploitation :<br/>
<br/>
              12. Considérant qu'aux termes de l'article 19 de l'ancien code rural, dont les dispositions ont été reprises à l'article L. 123-1 du nouveau code : " Le remembrement, applicable aux propriétés rurales non bâties, se fait au moyen d'une nouvelle distribution des parcelles morcelées et dispersées. / Il a principalement pour but, par la constitution d'exploitations rurales d'un seul tenant ou à grandes parcelles bien groupées, d'améliorer l'exploitation agricole des biens qui y sont soumis. (...) / Sauf accord des propriétaires et exploitants intéressés, le nouveau lotissement ne peut allonger la distance moyenne des terres au centre d'exploitation principal, si ce n'est dans la mesure nécessaire au regroupement parcellaire. / (...) " ; que l'aggravation des conditions d'exploitation s'apprécie non parcelle par parcelle mais pour l'ensemble d'un compte de propriété ;<br/>
<br/>
              13. Considérant, en premier lieu, que, la CNAF n'a pas commis d'erreur de droit en relevant, pour conclure à l'absence d'aggravation des conditions d'exploitation des consorts D...en raison des difficultés d'accès à leurs parcelles, que les intéressés étaient déjà propriétaires de la parcelle n° 34 avant les opérations de remembrement et que les parcelles situées au lieu-dit " Ketzermatt " étaient désormais accessibles par un chemin alors qu'elles ne l'étaient que par les terres avant les opérations de remembrement ; <br/>
<br/>
              14. Considérant, en second lieu, que, pour estimer que la non réattribution de terrains situés derrière leurs bâtiments d'exploitation et la perte éventuelle d'une possibilité de procéder à l'extension de ces bâtiments n'entraînaient pas une aggravation des conditions d'exploitation, la commission nationale s'est fondée sur le fait que la taille des parcelles réattribuées laissait subsister, même en tenant compte des distances imposées pour l'implantation de bâtiments, de larges possibilités de construction, sans éloignement par rapport à l'existant, et que les consorts D...avaient bénéficié de réattributions qui rapprochaient les deux îlots principaux de leur exploitation ; que de tels motifs pouvaient légalement justifier la décision attaquée ; <br/>
<br/>
              15. Considérant, enfin, que si Mme D...et Mme C...affirment que la CNAF n'a pas pris en compte le fait que les terrains qui leur ont été réattribués comportent des surfaces non exploitables ou de moins bonne qualité, ils n'assortissent pas ce moyen des précisions permettant d'en apprécier le bien fondé ; qu'il ne ressort pas des pièces du dossier que les attributions décidées par la commission nationale aient pour conséquence, en violation des dispositions précitées de l'article 19 de l'ancien code rural, une aggravation des conditions d'exploitation ;<br/>
<br/>
              16. Considérant qu'il résulte de tout ce qui précède que Mme D...et Mme C... ne sont pas fondées à demander l'annulation de la décision qu'elles attaquent ; que leurs conclusions à fin d'injonction et d'astreinte ne peuvent par suite qu'être rejetées ;<br/>
<br/>
              Sur les conclusions de Mme D...et de Mme C...présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              17. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
		Article 1er : La requête de Mme D...et de Mme C...est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à Mme E...D..., épouse B...à Mme A...C..., veuve D...et au ministre de l'agriculture, de l'agroalimentaire et de la forêt, porte parole du Gouvernement.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
