<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039379809</ID>
<ANCIEN_ID>JG_L_2019_11_000000420299</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/37/98/CETATEXT000039379809.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 13/11/2019, 420299</TITRE>
<DATE_DEC>2019-11-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420299</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; LE PRADO</AVOCATS>
<RAPPORTEUR>Mme Louise Cadin</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2019:420299.20191113</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. A... D... et Mme F... D..., agissant en leur nom propre et pour le compte de leur fils Vladimir, Mme G... D... et M. E... D... ont demandé au tribunal administratif de Pau de condamner le centre hospitalier de Bigorre à leur verser la somme globale de 550 000 euros en réparation des préjudices qu'ils estiment avoir subis du fait du suivi de la grossesse de Mme D... et de la naissance de leur fils et frère Vladimir. Par un jugement n° 1401152 du 29 décembre 2015, le tribunal administratif de Pau a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 16BX00810 du 28 décembre 2017, la cour administrative d'appel de Bordeaux a rejeté l'appel formé contre ce jugement par M. D... et autres.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 2 mai et 2 août 2018 au secrétariat du contentieux du Conseil d'Etat, M. D... et autres demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la SCP Piwnica, Molinié, leur avocat, sur le fondement des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de la santé publique ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme C... B..., auditrice,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de M. D..., et autres et à Me Le Prado, avocat du centre hospitalier de Bigorre ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, pour le suivi de la grossesse de Mme D..., alors âgée de 40 ans, le Dr. Srour, praticien hospitalier affecté au centre hospitalier de Bigorre où il effectuait également des consultations à titre libéral, a pratiqué, le 24 avril 2008, à 16 semaines et demie d'aménorrhée, une échographie ne révélant aucune anomalie de la morphologie foetale. Mme D... a ensuite fait effectuer, le 4 juin 2008, une deuxième échographie dans un cabinet de radiologie privé. S'étant ensuite présentée le 8 juillet 2008, soit à 27 semaines d'aménorrhée, en consultation publique au centre hospitalier de Bigorre, Mme D... a bénéficié, le 21 août 2008, à 34 semaines d'aménorrhée, d'une troisième échographie, réalisée par le Dr. Bennaçar, sans qu'aucune anomalie ne soit mise en évidence. Le 1er octobre 2008, Mme D... a donné naissance, au centre hospitalier de Bigorre, à un enfant atteint de trisomie 21 et souffrant d'une malformation cardiaque. Par un jugement du 29 décembre 2015, le tribunal administratif de Pau a rejeté la demande de M. et Mme D... et de deux de leurs enfants, tendant à ce que le centre hospitalier de Bigorre soit condamné à leur verser une indemnité de 550 000 euros en réparation des préjudices qu'ils estiment avoir subis en raison de fautes commises pendant le suivi de la grossesse de Mme D.... Ils se pourvoient en cassation contre l'arrêt par lequel la cour administrative de Bordeaux a rejeté leur appel dirigé contre ce jugement.<br/>
<br/>
              Sur la responsabilité du centre hospitalier au titre de la consultation du 24 avril 2008 :<br/>
<br/>
              2. Pour juger que la consultation effectuée le 24 avril 2008 par le Dr. Srour n'avait pas été exécutée dans le cadre du service public hospitalier mais revêtait un caractère libéral, la cour, qui n'a pas dénaturé les pièces du dossier en se fondant sur l'attestation de paiement éditée le 2 mars 2011 qui mentionne le " détail du paiement libéral " de la consultation et qui n'a pas commis d'erreur de droit en jugeant que l'absence d'information ou de consentement de Mme D... quant au caractère libéral de la consultation était sans incidence sur ce caractère libéral, a exactement qualifié les faits qui lui étaient soumis.<br/>
<br/>
              3. La cour n'a, par suite, pas entaché son arrêt d'erreur de droit en jugeant que le caractère libéral de la consultation du 24 avril 2008 faisait obstacle à ce que la responsabilité du centre hospitalier de Bigorre soit engagée en raison d'éventuels manquements du praticien au cours de cette consultation.<br/>
<br/>
              Sur la responsabilité du centre hospitalier au titre de la prise en charge postérieure au 8 juillet 2008 :<br/>
<br/>
              4. D'une part, aux termes de l'article L. 2213-1 du code de la santé publique : " L'interruption volontaire d'une grossesse peut, à toute époque, être pratiquée si deux médecins membres d'une équipe pluridisciplinaire attestent, après que cette équipe a rendu son avis consultatif, soit que la poursuite de la grossesse met en péril grave la santé de la femme, soit qu'il existe une forte probabilité que l'enfant à naître soit atteint d'une affection d'une particulière gravité reconnue comme incurable au moment du diagnostic ".  <br/>
<br/>
              5. D'autre part, aux termes du II de l'article L. 2131-1 du code de la santé publique : " Toute femme enceinte reçoit, lors d'une consultation médicale, une information loyale, claire et adaptée à sa situation sur la possibilité de recourir, à sa demande, à des examens de biologie médicale et d'imagerie permettant d'évaluer le risque que l'embryon ou le foetus présente une affection susceptible de modifier le déroulement ou le suivi de sa grossesse ". Aux termes du I de l'article R. 2131-2 du même code : " Lors du premier examen médical mentionné au second alinéa de l'article R. 2122-1 ou, à défaut, au cours d'une autre consultation médicale, toute femme enceinte est informée par le médecin ou la sage-femme de la possibilité d'effectuer, à sa demande, un ou plusieurs des examens mentionnés au I de l'article R. 2131-1. / Sauf opposition de la femme enceinte, celle-ci reçoit une information claire, adaptée à sa situation personnelle, qui porte sur les objectifs des examens, les résultats susceptibles d'être obtenus, leurs modalités, leurs éventuelles contraintes, risques, limites et leur caractère non obligatoire ". Il résulte de ces dispositions que, lorsqu'un praticien d'un centre hospitalier reçoit en consultation une femme enceinte ayant auparavant été suivie dans un autre cadre, il lui appartient de vérifier que l'intéressée a, antérieurement, effectivement reçu l'information prévue à l'article L. 2131-1 du code de la santé publique et, à défaut, de lui donner cette information, y compris jusqu'aux derniers moments de la grossesse. <br/>
<br/>
              6. Il ressort des énonciations de l'arrêt attaqué que, pour écarter toute responsabilité du centre hospitalier de Bigorre au titre du suivi de la grossesse de Mme D..., la cour administrative d'appel a retenu, par adoption des motifs du jugement du tribunal administratif, que le centre hospitalier de Bigorre n'avait pas commis de faute de nature à engager sa responsabilité en n'informant pas l'intéressée du risque que son enfant soit atteint de trisomie 21 ou de l'intérêt de pratiquer des examens afin de détecter d'éventuelles affections du foetus, notamment une amniocentèse qu'il est possible de réaliser à tout moment de la grossesse même si elle est habituellement programmée entre 15 et 17 semaines d'aménorrhée.<br/>
<br/>
              7. En statuant ainsi, alors qu'il appartenait au centre hospitalier de Bigorre, ainsi qu'il a été dit au point 5, de donner à Mme D..., même à un stade avancé de sa grossesse où il est d'ailleurs encore possible de pratiquer une amniocentèse et, le cas échéant, une interruption médicale de grossesse, l'information prévue aux articles L. 2131-1 et R. 3121-2 du code de la santé publique qu'elle n'avait pas reçue auparavant, la cour a entaché son arrêt d'une erreur de droit. <br/>
<br/>
              8. M. D... et autres sont, par suite, fondés à demander l'annulation de l'arrêt qu'ils attaquent en tant qu'il se prononce sur la responsabilité du centre hospitalier de Bigorre au titre de la prise en charge postérieure au 8 juillet 2008.<br/>
<br/>
              9. M. D... et autres ont obtenu le bénéfice de l'aide juridictionnelle. Par suite, leur avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Piwnica, Molinié, avocat des requérants, renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge du centre hospitalier de Bigorre la somme de 3 000 euros à verser à cette société.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt du 28 décembre 2017 de la cour administrative d'appel de Bordeaux est annulé en tant qu'il se prononce sur la responsabilité du centre hospitalier de Bigorre au titre de la prise en charge de Mme D... postérieure au 8 juillet 2008.<br/>
<br/>
Article 2 : L'affaire est renvoyée dans cette mesure devant la cour administrative d'appel de Bordeaux.<br/>
<br/>
Article 3 : Le centre hospitalier de Bigorre versera à la SCP Piwnica, Molinié, avocat de M. D... et autres, la somme de 3 000 euros, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
<br/>
Article 4 : Le surplus des conclusions du pourvoi est rejeté.<br/>
<br/>
Article 5 : La présente décision sera notifiée à M. A... D..., premier requérant dénommé et au centre hospitalier de Bigorre. <br/>
Copie en sera adressée à la caisse primaire d'assurance maladie des Hautes-Pyrénées.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">55-03-01-02 PROFESSIONS, CHARGES ET OFFICES. CONDITIONS D'EXERCICE DES PROFESSIONS. MÉDECINS. RÈGLES DIVERSES S'IMPOSANT AUX MÉDECINS DANS L'EXERCICE DE LEUR PROFESSION. - OBLIGATION D'INFORMATION DE LA FEMME ENCEINTE (ART. L. 2131-1 DU CSP) - PRATICIEN RECEVANT EN CONSULTATION UNE FEMME ENCEINTE AYANT AUPARAVANT ÉTÉ SUIVIE DANS UN AUTRE CADRE - PRATICIEN TENU DE VÉRIFIER QUE L'INTÉRESSÉE A DÉJÀ REÇU CETTE INFORMATION ET, À DÉFAUT, DE LUI DONNER CETTE INFORMATION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-02-01-01-01-01-04 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICE PUBLIC DE SANTÉ. ÉTABLISSEMENTS PUBLICS D'HOSPITALISATION. RESPONSABILITÉ POUR FAUTE SIMPLE : ORGANISATION ET FONCTIONNEMENT DU SERVICE HOSPITALIER. EXISTENCE D'UNE FAUTE. MANQUEMENTS À UNE OBLIGATION D'INFORMATION ET DÉFAUTS DE CONSENTEMENT. - CENTRE HOSPITALIER RECEVANT EN CONSULTATION UNE FEMME ENCEINTE AYANT AUPARAVANT ÉTÉ SUIVIE DANS UN AUTRE CADRE - CENTRE HOSPITALIER NE DONNANT PAS À L'INTÉRESSÉE L'INFORMATION PRÉVUE À L'ARTICLE L. 2131-1 DU CSP QU'ELLE N'AVAIT PAS REÇUE AUPARAVANT.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">60-03-02-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. PROBLÈMES D'IMPUTABILITÉ. PERSONNES RESPONSABLES. COLLECTIVITÉ PUBLIQUE OU PERSONNE PRIVÉE. - CONSULTATION PRÉSENTANT UN CARACTÈRE LIBÉRAL - CONSÉQUENCE - IMPOSSIBILITÉ D'ENGAGER LA RESPONSABILITÉ DU CENTRE HOSPITALIER OÙ CETTE CONSULTATION A EU LIEU EN RAISON D'ÉVENTUELS MANQUEMENTS DU PRATICIEN [RJ1].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">61-06-025 SANTÉ PUBLIQUE. ÉTABLISSEMENTS PUBLICS DE SANTÉ. RESPONSABILITÉ DES ÉTABLISSEMENTS DE SANTÉ (VOIR : RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE). - CONSULTATION PRÉSENTANT UN CARACTÈRE LIBÉRAL - CONSÉQUENCE - IMPOSSIBILITÉ D'ENGAGER LA RESPONSABILITÉ DU CENTRE HOSPITALIER OÙ CETTE CONSULTATION A EU LIEU EN RAISON D'ÉVENTUELS MANQUEMENTS DU PRATICIEN [RJ1].
</SCT>
<SCT ID="8E" TYPE="PRINCIPAL">61-06-05 SANTÉ PUBLIQUE. ÉTABLISSEMENTS PUBLICS DE SANTÉ. EXERCICE D'UNE ACTIVITÉ LIBÉRALE. - CONSULTATION EFFECTUÉE PAR UN PRATICIEN HOSPITALIER AFFECTÉ À UN CENTRE HOSPITALIER OÙ IL EFFECTUE ÉGALEMENT DES CONSULTATIONS À TITRE LIBÉRAL - 1) ATTESTATION DE PAIEMENT MENTIONNANT LE DÉTAIL DU PAIEMENT LIBÉRAL - CONSULTATION PRÉSENTANT UN CARACTÈRE LIBÉRAL, SANS QU'AIT D'INCIDENCE L'ABSENCE D'INFORMATION OU DE CONSENTEMENT DU PATIENT QUANT À CE CARACTÈRE - 2) CONSÉQUENCE -  IMPOSSIBILITÉ D'ENGAGER LA RESPONSABILITÉ DU CENTRE HOSPITALIER EN RAISON D'ÉVENTUELS MANQUEMENTS DU PRATICIEN AU COURS DE CETTE CONSULTATION [RJ1].
</SCT>
<ANA ID="9A"> 55-03-01-02 Il résulte de l'article L. 2213-1, du II de l'article L. 2131-1 et du I de l'article R. 2131-2 du code de la santé publique (CSP) que, lorsqu'un praticien d'un centre hospitalier reçoit en consultation une femme enceinte ayant auparavant été suivie dans un autre cadre, il lui appartient de vérifier que l'intéressée a, antérieurement, effectivement reçu l'information prévue à l'article L. 2131-1 du CSP et, à défaut, de lui donner cette information, y compris jusqu'aux derniers moments de la grossesse.</ANA>
<ANA ID="9B"> 60-02-01-01-01-01-04 Il résulte de l'article L. 2213-1, du II de l'article L. 2131-1 et du I de l'article R. 2131-2 du code de la santé publique (CSP) que, lorsqu'un praticien d'un centre hospitalier reçoit en consultation une femme enceinte ayant auparavant été suivie dans un autre cadre, il lui appartient de vérifier que l'intéressée a, antérieurement, effectivement reçu l'information prévue à l'article L. 2131-1 du CSP et, à défaut, de lui donner cette information, y compris jusqu'aux derniers moments de la grossesse.,,,Cour administrative d'appel retenant, pour écarter toute responsabilité du centre hospitalier au titre du suivi de la grossesse de la requérante, que ce centre n'avait pas commis de faute de nature à engager sa responsabilité en n'informant pas l'intéressée du risque que son enfant soit atteint de trisomie 21 ou de l'intérêt de pratiquer des examens afin de détecter d'éventuelles affections du foetus, notamment une amniocentèse qu'il est possible de réaliser à tout moment de la grossesse même si elle est habituellement programmée entre 15 et 17 semaines d'aménorrhée.,,,En statuant ainsi, alors qu'il appartenait au centre hospitalier de donner à la requérante, même à un stade avancé de sa grossesse où il est d'ailleurs encore possible de pratiquer une amniocentèse et, le cas échéant, une interruption médicale de grossesse, l'information prévue aux articles L. 2131-1 et R. 3121-2 du CSP qu'elle n'avait pas reçue auparavant, la cour entache son arrêt d'une erreur de droit.</ANA>
<ANA ID="9C"> 60-03-02-01 Le caractère libéral d'une consultation fait obstacle à ce que la responsabilité du centre hospitalier dans lequel elle a eu lieu soit engagée en raison d'éventuels manquements du praticien au cours de cette consultation.</ANA>
<ANA ID="9D"> 61-06-025 Le caractère libéral d'une consultation fait obstacle à ce que la responsabilité du centre hospitalier dans lequel elle a eu lieu soit engagée en raison d'éventuels manquements du praticien au cours de cette consultation.</ANA>
<ANA ID="9E"> 61-06-05 1) Pour juger qu'une consultation n'avait pas été exécutée dans le cadre du service public hospitalier mais revêtait un caractère libéral, la cour, qui n'a pas dénaturé les pièces du dossier en se fondant sur l'attestation de paiement qui mentionne le détail du paiement libéral de la consultation et qui n'a pas commis d'erreur de droit en jugeant que l'absence d'information ou de consentement de l'intéressée quant au caractère libéral de la consultation était sans incidence sur ce caractère libéral, a exactement qualifié les faits qui lui étaient soumis.,,,2) La cour n'a, par suite, pas entaché son arrêt d'erreur de droit en jugeant que le caractère libéral de la consultation faisait obstacle à ce que la responsabilité du centre hospitalier soit engagée en raison d'éventuels manquements du praticien au cours de cette consultation.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., en l'absence de dommages provoqués par un mauvais fonctionnement du service public hospitalier, CE, 10 octobre 1973, Demoiselle de,et caisse primaire d'assurance maladie du Calvados, n°s 84178, 84273, p. 556. Rappr., sous cette même réserve, TC, 31 mars 2008, Mme Véronique,c/Docteur,, Centre hospitalier universitaire de Voiron, n° 3616, inédit.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
