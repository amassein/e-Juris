<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044359304</ID>
<ANCIEN_ID>JG_L_2021_11_000000455891</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/35/93/CETATEXT000044359304.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 22/11/2021, 455891, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-11-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>455891</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jonathan  Bosredon</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:455891.20211122</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. et Mme A... B..., à l'appui de leur demande tendant à la décharge de la cotisation d'impôt sur le revenu à laquelle ils ont été assujettis au titre de l'année 2016, ont produit un mémoire, enregistré le 26 mars 2021 au greffe du tribunal administratif de Paris, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958, par lequel ils soulèvent une question prioritaire de constitutionnalité.<br/>
<br/>
              Par une ordonnance n° 2106822 du 23 août 2021, enregistrée le même jour au secrétariat du contentieux du Conseil d'Etat, le président de la 10ème chambre du tribunal administratif de Montreuil, auquel la demande a été transmise par une ordonnance n° 2105435 du 18 mai 2021 du président du tribunal administratif de Paris et avant qu'il y soit statué, a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du 5 du VIII de l'article 167 bis du code général des impôts dans leur rédaction issue de l'article 42 de la loi du 29 décembre 2013 de finances rectificative pour 2013.<br/>
<br/>
              Par la question prioritaire de constitutionnalité transmise et par un nouveau mémoire, enregistré le 8 octobre 2021 au secrétariat du contentieux du Conseil d'Etat, M. et Mme B... soutiennent que ces dispositions méconnaissent les principes d'égalité devant la loi et d'égalité devant les charges publiques garantis par les articles 6 et 13 de la Déclaration des droits de l'Homme et du citoyen de 1789.  <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule et son article 61-1 ; <br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code général des impôts, notamment son article 167 bis ;<br/>
              - la loi n° 2013-1279 du 29 décembre 2013 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jonathan Bosredon, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteure publique ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. Aux termes du 5 du VIII de l'article 167 bis du code général des impôts, dans sa rédaction issue de l'article 42 de la loi du 29 décembre 2013 de finances rectificative pour 2013 : " L'impôt éventuellement acquitté par le contribuable dans son Etat de résidence dans les cas prévus au a du 1 du VII est imputable, dans la limite de l'impôt définitif dû en France : / a) Sur les prélèvements sociaux afférents à la plus-value calculée en application du premier alinéa du 2 du I et des 1 et 4 bis du présent VIII, à proportion du rapport entre, d'une part, cette même plus-value et, d'autre part, l'assiette de l'impôt acquitté hors de France ; / b) Puis, pour le reliquat, sur l'impôt sur le revenu afférent à la plus-value calculée en application des 2 à 3 du I et des 1, 3 et 4 bis du présent VIII, à proportion du rapport entre, d'une part, cette même plus-value et, d'autre part, l'assiette de l'impôt acquitté hors de France ".     <br/>
<br/>
              3. D'une part, aux termes de l'article 6 de la Déclaration des droits de l'homme et du citoyen du 26 août 1789, la loi " doit être la même pour tous, soit qu'elle protège, soit qu'elle punisse ". Le principe d'égalité ne s'oppose ni à ce que le législateur règle de façon différente des situations différentes, ni à ce qu'il déroge à l'égalité pour des raisons d'intérêt général, pourvu que dans l'un et l'autre cas, la différence de traitement qui en résulte soit en rapport direct avec l'objet de la loi qui l'établit. D'autre part, aux termes de l'article 13 de cette même déclaration : " Pour l'entretien de la force publique, et pour les dépenses d'administration, une contribution commune est indispensable : elle doit être également répartie entre tous les citoyens, en raison de leurs facultés ". Cette exigence ne serait pas respectée si l'impôt revêtait un caractère confiscatoire ou faisait peser sur une catégorie de contribuables une charge excessive au regard de leurs facultés contributives. En vertu de l'article 34 de la Constitution, il appartient au législateur de déterminer, dans le respect des principes constitutionnels et compte tenu des caractéristiques de chaque impôt, les règles selon lesquelles doivent être appréciées les facultés contributives. En particulier, pour assurer le respect du principe d'égalité, il doit fonder son appréciation sur des critères objectifs et rationnels en fonction des buts qu'il se propose. Cette appréciation ne doit cependant pas entraîner de rupture caractérisée de l'égalité devant les charges publiques.<br/>
<br/>
              4. L'article 167 bis du code général des impôts prévoit l'imposition des plus-values latentes constatées sur les droits sociaux, valeurs, titres ou droits que détiennent des contribuables à l'occasion du transfert de leur domicile fiscal hors de France. Cette imposition est assortie d'un sursis de paiement de l'impôt correspondant lorsque le transfert de domicile intervient vers un Etat membre de l'Union européenne ou un autre Etat partie à l'accord sur l'Espace économique européen ayant conclu avec la France une convention d'assistance administrative en vue de lutter contre la fraude et l'évasion fiscales ainsi qu'une convention d'assistance mutuelle en matière de recouvrement ayant une portée similaire à celle prévue par la directive du Conseil du 16 mars 2010, concernant l'assistance mutuelle en matière de recouvrement des créances relatives aux taxes, impôts, droits et autres mesures. Par les dispositions contestées du 5 du VIII de l'article 167 bis du code général des impôts, le législateur a entendu prévenir une éventuelle double imposition des gains de cession des titres grevés, à la date du transfert de domicile fiscal hors de France, d'une plus-value latente en permettant aux contribuables d'imputer l'impôt acquitté à l'étranger à l'occasion de la cession des titres sur l'impôt dû en France, le sursis de paiement prenant fin du fait de cette cession. L'impôt étranger n'est toutefois imputable sur l'impôt dû en France qu'à proportion du rapport entre le montant de la plus-value assujettie en France, le cas échéant après application de l'abattement pour durée de détention, et l'assiette de l'impôt acquitté hors de France.<br/>
<br/>
              5. En prévoyant ainsi l'imputation sur l'impôt dû en France de la seule fraction de l'impôt acquitté à l'étranger correspondant à la fraction imposable en France de l'assiette totale, le législateur a retenu un critère objectif et rationnel au regard de l'objectif consistant à éviter une double imposition d'un même revenu. <br/>
<br/>
              6. Si M. et Mme B... soutiennent en outre que les dispositions contestées créeraient une différence de traitement entre les contribuables ayant quitté le territoire français et ceux ayant conservé leur domicile fiscal en France, d'une part, et entre les contribuables selon qu'ils bénéficient ou non d'un abattement pour durée de détention, d'autre part, ces contribuables se trouvent dans des situations différentes au regard de l'objet de ces dispositions. <br/>
<br/>
              7. Il résulte de ce qui précède que la question prioritaire de constitutionnalité soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux. Par suite, il n'y a pas lieu de la renvoyer au Conseil constitutionnel. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M. et Mme B....<br/>
Article 2 : La présente décision sera notifiée à M. et Mme A... B... et au ministre de l'économie, des finances et de la relance. <br/>
Copie en sera adressée au Conseil constitutionnel et au Premier ministre.<br/>
              Délibéré à l'issue de la séance du 9 novembre 2021 où siégeaient : M. Pierre Collin, président de chambre, présidant ; M. Mathieu Herondart, conseiller d'Etat et M. Jonathan Bosredon, conseiller d'Etat-rapporteur<br/>
<br/>
              Rendu le 22 novembre 2021.<br/>
<br/>
<br/>
                 Le président : <br/>
                 Signé : M. Pierre Collin<br/>
 		Le rapporteur : <br/>
      Signé : M. Jonathan Bosredon<br/>
                 La secrétaire :<br/>
                 Signé : Mme D... C...<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
