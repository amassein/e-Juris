<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028837922</ID>
<ANCIEN_ID>J4_L_2014_03_000001201748</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/83/79/CETATEXT000028837922.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>CAA de NANTES, 3ème chambre, 27/03/2014, 12NT01748, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-03-27</DATE_DEC>
<JURIDICTION>CAA de NANTES</JURIDICTION>
<NUMERO>12NT01748</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. COIFFET</PRESIDENT>
<AVOCATS>JOSSELIN</AVOCATS>
<RAPPORTEUR>Mme Frédérique  SPECHT</RAPPORTEUR>
<COMMISSAIRE_GVT>M. DEGOMMIER</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 27 juin 2012, présentée pour M. B... C..., demeurant au..., par Me Josselin, avocat au barreau de Quimper ; M. C... demande à la cour :<br/>
<br/>
       1°) d'annuler le jugement n° 09-3055 en date du 27 avril 2012 en tant que le tribunal administratif de Rennes a rejeté les conclusions de sa demande tendant à la condamnation de l'Etat à lui verser la somme de 500 000 euros portant intérêts et capitalisation des intérêts en réparation du préjudice subi du fait du retard dans l'octroi de l'indemnisation due au titre des calamités agricoles pour la perte de son cheptel en 1976 et de l'insuffisance des sommes versées à ce titre ;<br/>
<br/>
       2°) de condamner l'Etat à lui verser la somme demandée, assortie des intérêts au taux légal et de la capitalisation des intérêts ;<br/>
<br/>
       3°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
       il soutient que :<br/>
<br/>
       - le jugement est insuffisamment motivé en ce qu'il ne précise pas les motifs pour lesquels la prescription n'a pas été considérée comme interrompue par les différents courriers postérieurs à la décision d'indemnisation de 1986 ; <br/>
<br/>
       - le moyen tiré de l'incompétence du signataire du mémoire en défense de l'administration, opposant la prescription quadriennale, a été écarté par le tribunal sans que ne soit produite la délégation de signature, en méconnaissance du respect du principe du contradictoire ; <br/>
       - la délégation de signature accordée au signataire du mémoire, invoquée par le tribunal, ne permettait pas à l'auteur de ce mémoire d'invoquer la prescription quadriennale ; si elle existe, cette délégation de signature a un caractère général et il n'est par ailleurs pas établi que la directrice des affaires juridiques aurait été empêchée de signer le mémoire en défense ;<br/>
<br/>
       - l'indemnisation accordée en 1986 était fondée sur les règles applicables aux calamités agricoles et non sur la responsabilité pour faute de l'Etat ; ainsi, contrairement à ce qu'ont estimé les premiers juges, cette indemnisation n'a pu constituer le point de départ du délai de la prescription quadriennale ;<br/>
<br/>
       - le délai de prescription a, en outre, été interrompu par l'envoi de divers courriers ultérieurs ; il a pu légitimement estimer que ses interventions avaient sauvegardé ses droits à indemnité ; <br/>
<br/>
       - la prescription ne pouvait être opposée dès lors que les préjudices ne sont pas définitivement évalués ;<br/>
<br/>
       - le délai de quatre ans applicable aux créances envers l'Etat alors que celui-ci disposait d'un délai de trente ans pour faire valoir ses créances est discriminatoire au sens de l'article 14 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; ce délai de prescription de quatre ans est également contraire au principe de l'égalité des armes entre l'administration et le contribuable et porte une atteinte disproportionnée à son droit au respect de ses biens et à l'équilibre entre la protection de la propriété et les exigences de l'intérêt général, en méconnaissance de l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
       - la responsabilité de l'Etat est engagée pour n'avoir pas reconnu immédiatement son droit à être indemnisé au titre des calamités agricoles ; c'est également à tort que sa situation n'a pas été prise en considération par la Caisse nationale de Crédit agricole, établissement public relevant, à l'époque des faits, du ministère de l'agriculture ; <br/>
<br/>
       - les préjudices subis du fait des fautes commises par l'Etat, consécutifs à la perte de son cheptel, portent sur le manque à gagner, les préjudices financiers occasionnés par un endettement accru, les difficultés d'entretien des bâtiments, l'obligation de vendre son patrimoine, et, enfin, les atteintes à son image et les troubles dans ses conditions d'existence et celles de sa famille du fait de la diminution de ses revenus ; une expertise peut être diligentée afin d'évaluer ces préjudices ; <br/>
<br/>
       Vu le jugement attaqué ;<br/>
<br/>
       Vu le mémoire en défense, enregistré le 12 novembre 2013, présenté par le ministre de l'agriculture, de l'agroalimentaire et de la forêt qui conclut au rejet de la requête ; <br/>
       il fait valoir que :<br/>
<br/>
       - le jugement est suffisamment motivé ; <br/>
<br/>
       - la décision portant délégation de signature au signataire du mémoire en défense devant le tribunal administratif a été publiée au Journal Officiel de la République française du 8 mai 2010 et était accessible à l'ensemble des parties ; sa communication ne s'imposait pas ;<br/>
<br/>
       - la délégation de signature n'a pas de caractère général et n'a pas été limitée aux cas d'absence ou d'empêchement de la directrice des affaires juridiques ; en tout état de cause le requérant n'établit pas que cette dernière n'aurait été ni absente ni empêchée ; <br/>
<br/>
       - l'application d'un délai de prescription plus court à l'égard des créanciers de l'Etat ne méconnaît pas l'exigence de sauvegarde du droit de propriété stipulée à l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; la prescription a été opposée à M. C... 22 ans après la naissance de l'éventuelle créance ; <br/>
<br/>
       - à la date de l'arrêté interministériel du 18 mars 2006, lui accordant la somme de 540 946 francs (82 468 euros), complété le 9 décembre 1986 par le versement de la somme de 197 807 francs (30 155 euros), M. C... avait connaissance des conséquences dommageables du retard qu'il invoque ; <br/>
<br/>
       - les courriers auxquels se réfère M. C... ne permettent pas d'établir qu'ils porteraient sur une demande d'indemnisation de l'intéressé en raison d'une faute commise par l'Etat et ne sauraient donc avoir un effet interruptif de prescription ; <br/>
<br/>
       - par ailleurs, si M. C... soutient que son préjudice n'avait pas de caractère certain à la date à laquelle il en a demandé l'indemnisation, alors un tel préjudice ne peut être indemnisé ; <br/>
<br/>
       - M. C... n'établit pas l'existence d'une faute lourde de l'Etat dans son pouvoir de tutelle à l'égard de la Caisse nationale de crédit agricole ; <br/>
<br/>
       - le requérant n'établit pas davantage la réalité des préjudices allégués qui incluent le préjudice résultant de la prétendue insuffisance de l'indemnisation perçue en 1986 alors que M. C... ne critique pas le jugement du tribunal administratif de Rennes en tant qu'il a reconnu l'incompétence de la juridiction administrative pour connaître de conclusions tendant à une indemnisation au titre des calamités agricoles ; le requérant ne démontre pas le lien de causalité entre les préjudices allégués et le retard imputé à l'administration ni n'établit la réalité des préjudices invoqués ; <br/>
<br/>
       Vu le mémoire, enregistré le 25 février 2014, présenté pour M. C... ; <br/>
<br/>
<br/>
       Vu les autres pièces du dossier ;<br/>
<br/>
       Vu la Convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
       Vu le code civil ; <br/>
<br/>
       Vu la loi n° 2008-651 du 17 juin 2008 portant réforme de la prescription en matière civile ; <br/>
<br/>
       Vu la loi n° 68-1250 du 31 décembre 1968 relative à la prescription des créances sur l'Etat, les départements, les communes et les établissements publics ;<br/>
<br/>
       Vu le décret n° 2005-850 du 27 juillet 2005 relatif aux délégations de signature des <br/>
membres du Gouvernement ;<br/>
<br/>
       Vu le décret n° 2008-636 du 30 juin 2008 fixant l'organisation de l'administration centrale du ministère chargé de l'agriculture, de l'alimentation et de la pêche ;<br/>
<br/>
       Vu le code de justice administrative ;<br/>
<br/>
       Les parties ayant été régulièrement averties du jour de l'audience ;<br/>
<br/>
       Après avoir entendu au cours de l'audience publique du 27 février 2014 :<br/>
       - le rapport de Mme Specht, premier conseiller ;<br/>
<br/>
       - et les conclusions de M. Degommier, rapporteur public ;<br/>
<br/>
<br/>
<br/>
       1. Considérant que M. C..., qui était éleveur de porcs, sélectionneur, à Plouvenez-Lochrist (Finistère), a été contraint en 1976 de faire procéder à l'abattage d'une partie importante de son cheptel après que celui-ci eut été touché par une maladie qui a été ultérieurement reconnue contagieuse ; qu'un premier refus a été opposé en 1976 à sa demande d'indemnisation de la perte ainsi subie sur le fondement des dispositions du code rural relatives aux calamités agricoles ; que le second refus qui lui a été opposé par une décision du 6 juin 1983 du ministre de l'agriculture a été annulé par un jugement du 7 novembre 1985 du tribunal administratif de Rennes ; qu'en exécution de ce jugement, l'Etat, par un arrêté conjoint du ministre des finances et du ministre de l'agriculture, du 18 mars 1986, a fixé l'indemnisation due à M. C... à la somme de 540 946 francs (82 468 euros), à laquelle s'est ajouté, par une décision du 9 décembre 1986, le versement de la somme de 197 807 francs (30 155 euros) soit un montant total de 738 753 francs (112 623 euros) ; que, par une demande du 10 février 2009, M. C... a sollicité de l'Etat l'indemnisation des préjudices subis du fait de l'insuffisance de l'indemnisation perçue ainsi que du retard fautif des services de l'Etat à l'indemniser de la perte de son cheptel ; que par un jugement du 27 avril 2012, le tribunal administratif de Rennes a, d'une part, rejeté les conclusions de la demande de M. C... tendant à la réévaluation des indemnités versées dans le cadre de la procédure des calamités agricoles comme portées devant une juridiction incompétente pour en connaître, et, d'autre part rejeté le surplus de sa demande indemnitaire portant sur le retard de l'Etat à réparer la perte de son cheptel ; que M. C... demande l'annulation de ce jugement en tant seulement qu'il a rejeté cette dernière demande ; <br/>
<br/>
       Sur la régularité du jugement : <br/>
<br/>
       2. Considérant, en premier lieu, qu'il ressort du jugement attaqué que le tribunal a estimé que le délai de prescription était acquis dès le 31 décembre 1990 ; que par suite, les premiers juges, qui n'étaient pas tenus, dès lors, de se prononcer sur l'effet interruptif des lettres, postérieures à cette date, dont se prévalait M. C..., n'ont pas entaché le jugement attaqué d'insuffisance de motivation ;<br/>
<br/>
       3. Considérant, en second lieu, qu'il est de l'office du juge de vérifier la portée du moyen d'ordre public tiré de l'incompétence de l'auteur de l'acte et notamment l'existence d'une délégation de signature régulièrement publiée ; que par suite, les premiers juges ont pu, à bon droit, écarter le moyen tiré de l'incompétence du signataire du mémoire en défense de première instance soulevé par M. C... dans son mémoire en réplique en se fondant sur l'existence de la décision du 5 mai 2010, régulièrement publiée au Journal officiel de la République française du 8 mai 2010, portant délégation de signature à l'auteur du mémoire en défense, alors même que cette décision n'avait pas été produite par le ministre ; que, dès lors, le moyen tiré de ce que l'absence de communication de décision portant délégation de signature méconnaîtrait le principe du contradictoire de la procédure contentieuse ne peut qu'être écarté ; <br/>
<br/>
       4. Considérant qu'il résulte de ce qui précède que M. C... n'est pas fondé à soutenir que le jugement attaqué serait entaché d'irrégularité ; <br/>
<br/>
       Sur les conclusions tendant à la condamnation de l'Etat :<br/>
<br/>
       5. Considérant, qu'aux termes de l'article 1er de la loi susvisée du 31 décembre 1968 : " Sont prescrites au profit de l'Etat, des départements et des communes, sans préjudices des déchéances particulières édictées par la loi, et sous réserve des dispositions de la présente loi, toutes créances qui n'ont pas été payées dans un délai de quatre ans à partir du premier jour de l'année suivant celle au cours de laquelle les droits ont été acquis. Sont prescrites, dans le même délai et sous la même réserve, les créances sur les établissements publics dotés d'un comptable public " ; qu'aux termes de l'article 2 de la même loi : " La prescription est interrompue par : Toute demande de paiement ou toute réclamation écrite adressée par un créancier à l'autorité administrative, dès lors que la demande ou la réclamation a trait au fait générateur, à l'existence, au montant ou au paiement de la créance, alors même que l'administration saisie n'est pas celle qui aura finalement la charge du règlement. Tout recours formé devant une juridiction, relatif au fait générateur, à l'existence, au montant ou au paiement de la créance, quel que soit l'auteur du recours et même si la juridiction saisie est incompétente pour en connaître, et si l'administration qui aura finalement la charge du règlement n'est pas partie à l'instance ; Toute communication écrite d'une administration intéressée, même si cette communication n'a pas été faite directement au créancier qui s'en prévaut, dès lors que cette communication a trait au fait générateur, à l'existence, au montant ou au paiement de la créance (...) " ; qu'aux termes de l'article 3 de la même loi : " La prescription ne court ni contre le créancier qui ne peut agir, soit par lui-même ou par l'intermédiaire de son représentant légal, soit pour une cause de force majeure, ni contre celui qui peut être légitimement regardé comme ignorant l'existence de sa créance ou de la créance de celui qu'il représente légalement " ; qu'enfin, aux termes de l'article 7 de la même loi : "L'administration doit, pour pouvoir se prévaloir, à propos d'une créance litigieuse, de la prescription prévue par la présente loi, l'invoquer avant que la juridiction saisie du litige au premier degré se soit prononcée sur le fond (...)" ;<br/>
       6. Considérant, en premier lieu, qu'en vertu de l'article R. 431-9 du code de justice administrative relatif à la représentation de l'Etat devant le tribunal administratif, et sous réserve des dispositions particulières qu'il mentionne, les mémoires en défense présentés au nom de l'Etat sont signés par le ministre intéressé et les ministres peuvent déléguer leur signature dans les conditions prévues par la réglementation en vigueur ; <br/>
<br/>
       7. Considérant qu'en vertu du 1° de l'article 1er du décret du 27 juillet 2005 relatif aux délégations de signature des membres du Gouvernement, les directeurs d'administration centrale ont reçu délégation de signature pour signer au nom du ministre l'ensemble des actes, à l'exception des décrets, relatifs aux affaires des services placés sous leur autorité ; qu'en application des articles 1er et 2 du décret du 30 juin 2008 fixant l'organisation de l'administration centrale du ministère chargé de l'agriculture, de l'alimentation et de la pêche, le secrétariat général de l'administration centrale de ce ministère comprend notamment le service des affaires juridiques, lequel, dirigé par le directeur ou la directrice des affaires juridiques, est en particulier chargé du traitement du contentieux de niveau central du ministère et représente le ministre devant les juridictions compétentes ; que, comme il a été dit au point 3, par une décision du 5 mai 2010 régulièrement publiée au Journal officiel de la République française du 8 mai 2010, MmeA..., sous-directrice des affaires juridiques, a reçu de la directrice des affaires juridiques délégation de signature à l'effet de signer, à l'exception des décrets, tous actes, arrêtés et décisions, dans la limite des attributions de la sous-direction du droit de l'administration, de la concurrence et des procédures juridiques communautaires ; qu'eu égard à l'objet du litige, la sous-directrice des affaires juridiques avait, en vertu de la délégation de signature qui lui a été régulièrement accordée, qui n'avait pas un caractère général et n'était pas subordonnée à l'absence ou à l'empêchement de la directrice des affaires juridiques, qualité pour signer au nom du ministre le mémoire en défense présenté devant le tribunal administratif de Rennes ; qu'elle était ainsi compétente, même en l'absence dans la délégation en cause d'une mention portant expressément sur la prescription, pour opposer l'exception de prescription quadriennale à la demande présentée par M. C... ; qu'enfin, si le mémoire du 8 octobre 2011 opposant la prescription quadriennale a été signé " par empêchement de la directrice des affaires juridiques ", il ne ressort pas des pièces du dossier que cette dernière n'ait pas été absente ou empêchée à la date de signature du mémoire en litige ; que, par suite, le moyen tiré de ce que la décision opposant la prescription quadriennale aurait été signée par une autorité incompétente doit être écarté ; <br/>
<br/>
       8. Considérant, en deuxième lieu, qu'il résulte de la combinaison des dispositions précitées de la loi du 31 décembre 1968 que le point de départ de la prescription quadriennale est la date à laquelle la victime est en mesure de connaître l'origine du dommage ou du moins de disposer d'indications suffisantes selon lesquelles ce dommage pourrait être imputable au fait de l'administration ;<br/>
       9. Considérant, que M. C... soutient que les dispositions précitées de la loi du 31 décembre 1968 instaurent une discrimination entre les créanciers de l'Etat qui ne disposent que d'un délai de quatre ans pour faire valoir leurs créances et le délai dont dispose l'Etat pour recouvrer ses créances et qui, avant l'entrée en vigueur de la loi du 17 juin 2008 portant réforme de la prescription en matière civile, pouvait atteindre trente ans et méconnaissent ainsi le principe de non discrimination protégé par les stipulations de l'article 14 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et le principe de l'égalité des armes découlant de l'article 6 paragraphe 1 de la même convention, emportant une atteinte disproportionnée à son droit au respect de ses biens et à l'équilibre entre la protection de la propriété et les exigences de l'intérêt général, en méconnaissance de l'article 1er du premier protocole additionnel à la même convention ; que toutefois, à la date du 10 février 2009 à laquelle M. C... a présenté sa demande d'indemnisation pour les préjudices qu'il invoque, le délai de prescription applicable aux créances détenues par l'Etat était de cinq ans en application de l'article 2224 du code civil tel que résultant de la loi du 17 juin 2008 portant réforme de la prescription en matière civile, applicable aux actions en responsabilité extra contractuelle ; que par suite, le moyen tiré du caractère disproportionné des délais de prescription applicables aux créanciers de l'Etat et à ses débiteurs n'est pas fondé ; <br/>
<br/>
       10. Considérant qu'ainsi que l'ont estimé les premiers juges, il résulte de l'instruction que M. C... était en mesure, après l'intervention de l'arrêté interministériel du 18 mars 1986, pris en exécution du jugement du tribunal administratif de Rennes du 7 novembre 1985 annulant le refus opposé à sa demande d'indemnisation par le ministre de l'agriculture, lui accordant une indemnité et la décision du 9 décembre 1986 lui accordant une indemnité supplémentaire, de percevoir la nature et la portée des préjudices qu'il invoque et disposait alors d'indications suffisantes selon lesquelles ces préjudices pouvaient être imputables au retard fautif de l'administration dans l'attribution de ces indemnités comme à la faute alléguée de l'Etat dans l'exercice de son pouvoir de tutelle exercé alors sur la Caisse nationale du Crédit agricole ; qu'ainsi, le délai de prescription a commencé à courir à compter du 1er janvier 1987 et a expiré le 31 décembre 1990 ; que les interventions dont M. C... fait état auprès d'un député en mai 1991 puis, à plusieurs reprises ensuite auprès du ministre de l'agriculture et du Médiateur de la République qui sont postérieures à la date d'expiration du délai de prescription, n'ont pu, en tout état de cause, interrompre ce délai ; que par suite, la créance dont M. C... se prévaut était prescrite à la date de sa demande d'indemnisation présentée le 10 février 2009 au ministre de l'agriculture ; <br/>
<br/>
       11. Considérant qu'il résulte de ce qui précède que M. C... n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Rennes a rejeté sa demande ;<br/>
<br/>
       Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
       12. Considérant que ces dispositions font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance, la somme que M. C... demande au titre des frais exposés et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>
                                   DÉCIDE :<br/>
<br/>
Article 1er :	La requête de M. C... est rejetée.<br/>
Article 2 :	Le présent arrêt sera notifié à M. B... C... et au ministre de l'agriculture, de l'agroalimentaire et de la forêt.<br/>
<br/>
Délibéré après l'audience du 27 février 2014, à laquelle siégeaient :<br/>
<br/>
       - M. Coiffet, président,<br/>
       - Mme Specht, premier conseiller,<br/>
       - M. Lemoine, premier conseiller.<br/>
<br/>
       Lu en audience publique, le 27 mars 2014.<br/>
<br/>
Le rapporteur,<br/>
F. SPECHTLe président,<br/>
O. COIFFET<br/>
Le greffier,<br/>
 A. MAUGENDRE<br/>
<br/>
<br/>
<br/>
La République mande et ordonne au ministre de l'agriculture, de l'agroalimentaire et de la forêt en ce qui le concerne, et à tous huissiers de justice à ce requis en ce qui concerne les voies de droit commun contre les parties privées, de pourvoir à l'exécution de la présente décision.<br/>
<br/>
''<br/>
''<br/>
''<br/>
''<br/>
N° 12NT01748                                      2<br/>
1<br/>
<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
