<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030956656</ID>
<ANCIEN_ID>JG_L_2015_07_000000382550</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/95/66/CETATEXT000030956656.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème SSJS, 27/07/2015, 382550, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-07-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>382550</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MEIER-BOURDEAU, LECUYER ; SCP VINCENT, OHL</AVOCATS>
<RAPPORTEUR>Mme Clémence Olsina</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:382550.20150727</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Par une requête et un mémoire complémentaire, enregistrés les 11 juillet et 10 octobre 2014 au secrétariat du contentieux du Conseil d'Etat, la société Turgot Asset Management demande au Conseil d'Etat d'annuler la décision du 12 mai 2014 par laquelle la commission des sanctions de l'Autorité des marchés financiers a prononcé à son encontre une sanction pécuniaire de 80 000 euros et a décidé de publier cette décision sur son site Internet et, subsidiairement, de réduire le montant de cette sanction pécuniaire.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code monétaire et financier ;<br/>
              - le règlement général de l'Autorité des marchés financiers ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Clémence Olsina, auditeur, <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Meier-Bourdeau, Lecuyer, avocat de la société Turgot Asset Management et à la SCP Vincent, Ohl, avocat de l'Autorité des marchés financiers ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que, par une décision du 12 mai 2014, dont la société Turgot Asset Management demande l'annulation, la commission des sanctions de l'Autorité des marchés financiers a prononcé à l'encontre de cette société une sanction pécuniaire de 80 000 euros et a ordonné la publication de cette décision sur le site Internet de l'Autorité des marchés financiers ; <br/>
<br/>
              Sur la régularité de la décision attaquée :<br/>
<br/>
              2. Considérant, en premier lieu, qu'aux termes de l'article R. 621-38 du code monétaire et financier, dans sa rédaction applicable au litige : " Lorsque le collège décide de l'ouverture d'une procédure de sanction, la notification des griefs est adressée, par lettre recommandée avec demande d'avis de réception, remise en main propre contre récépissé ou acte d'huissier, à la personne mise en cause, accompagnée du rapport d'enquête ou de contrôle ou de la demande formulée par le président de l'Autorité de contrôle prudentiel. / (...) / La notification des griefs est transmise au président de la commission des sanctions. / La personne mise en cause dispose d'un délai de deux mois pour transmettre au président de la commission des sanctions ses observations écrites sur les griefs qui lui ont été notifiés. La notification des griefs mentionne ce délai et précise que la personne mise en cause peut prendre connaissance et copie des autres pièces du dossier auprès de la commission des sanctions et se faire assister ou représenter par tout conseil de son choix " ; qu'aux termes de l'article R. 621-39 du même code : " I.-Le président de la commission des sanctions attribue l'affaire soit à cette dernière soit à l'une de ses sections. Il désigne le rapporteur. Celui-ci procède à toutes diligences utiles. Il peut s'adjoindre le concours des services de l'Autorité des marchés financiers. La personne mise en cause et le membre du collège mentionné au troisième alinéa du I de l'article L. 621-15 ou son représentant désigné en application de cette disposition peuvent être entendus par le rapporteur à leur demande ou si celui-ci l'estime utile. Le rapporteur peut également entendre toute personne dont l'audition lui paraît utile. / Lorsqu'il estime que les griefs doivent être complétés ou que les griefs sont susceptibles d'être notifiés à une ou plusieurs personnes autres que celles mises en cause, le rapporteur saisit le collège. Le collège statue sur cette demande du rapporteur dans les conditions et formes prévues à l'article R. 621-38. Le délai prévu au troisième alinéa de l'article R. 621-38 est applicable en cas de notification complémentaire des griefs. / (...) / III.-La personne mise en cause est convoquée devant la commission des sanctions ou la section par lettre recommandée avec demande d'avis de réception, remise en main propre contre récépissé ou acte d'huissier, dans un délai qui ne peut être inférieur à 30 jours francs. Cette lettre précise que la personne mise en cause dispose d'un délai de 15 jours francs pour faire connaître par écrit ses observations sur le rapport. " ; <br/>
<br/>
              3. Considérant que, par un courrier du 10 décembre 2012, la société Turgot Asset Management s'est vu notifier par l'Autorité des marchés financiers un grief tiré de l'absence de mise en place d'une procédure de gestion du risque de liquidité pour le fonds Turgot Multigest International, sur le fondement de l'article 313-53-5 du règlement général de l'Autorité des marchés financiers ; que si ces dispositions ne sont entrées en vigueur que le 21 octobre 2011, soit postérieurement aux faits litigieux, l'obligation pour le prestataire de mettre en place une procédure de gestion du risque de liquidité résultait déjà, à la date des faits, des dispositions de l'article 313-60 du même règlement, dans leur version en vigueur depuis le 1er novembre 2007, qui imposait notamment aux sociétés de gestion de portefeuille d'adopter des dispositifs, des processus et des mécanismes permettant de gérer efficacement les risques liés à ces activités ; que le risque de liquidité, eu égard à ses enjeux financiers, figure en effet nécessairement parmi les risques visés par ces dispositions ; qu'ainsi, en substituant, dans son rapport, la référence à l'article 313-60 à la référence à l'article 313-53-5 du règlement, le rapporteur ne peut, contrairement à ce qui est soutenu, être regardé comme ayant irrégulièrement soulevé un grief nouveau ; que, dès lors, le moyen tiré de ce que le principe du contradictoire et les droits de la défense auraient ainsi été méconnus doit être écarté ;<br/>
<br/>
              4. Considérant, en second lieu, que, pour motiver le montant et la nature des sanctions retenues, la décision attaquée relève qu'ont notamment été pris en compte la gravité des manquements, la durée limitée de certains d'entre eux, la moins-value qui a résulté, pour la société concernée, de l'opération critiquée, ainsi que l'engagement de la société à modifier ses documents et procédures ; que la décision relève également que si la société faisait état d'une création récente et d'un équilibre financier fragile, le montant des fonds qu'elle gérait était en constante augmentation depuis 2011 : que, ce faisant, la commission des sanctions a suffisamment motivé sa  décision ; <br/>
<br/>
              Sur le bien-fondé de la décision attaquée :<br/>
<br/>
              En ce qui concerne les manquements constatés :<br/>
<br/>
              5. Considérant, en premier lieu, qu'il résulte de ce qui a été dit au point 3 sur la portée et les termes du règlement général de l'Autorité des marchés financiers que le moyen tiré de ce que ces dispositions n'auraient pas défini de façon suffisamment précise l'obligation faite aux sociétés de gestion de se doter d'une procédure de gestion des risques de liquidité, en méconnaissance du principe de légalité des délits et des peines, doit être écarté ;  <br/>
<br/>
              6. Considérant, en second lieu, qu'aux termes de l'article L. 214-9 du code monétaire et financier : " Les OPCVM, le dépositaire et la société de gestion doivent agir de façon indépendante et dans le seul intérêt des porteurs de parts ou actionnaires " ; qu'aux termes de l'article L. 533-11 du même code : " Lorsqu'ils fournissent des services d'investissement et des services connexes à des clients, les prestataires de services d'investissement agissent d'une manière honnête, loyale et professionnelle, servant au mieux les intérêts des clients " ; que la substance de ces dernières dispositions est reprise par l'article           314-3 du règlement général de l'Autorité des marchés financiers ; qu'il résulte de ces dispositions qu'une société de gestion de portefeuille doit, lorsqu'elle effectue des opérations financières pour le compte d'organismes de placement collectif en valeurs mobilières (OPCVM) qu'elle gère, tenir compte du seul intérêt des porteurs de parts de chacun des OPCVM concernés, à l'exclusion d'intérêts concurrents, divergents ou antagonistes, y compris ceux de la société de gestion elle-même ou d'autres OPCVM dont elle assure la gestion ; que la méconnaissance de ces principes peut, à défaut de preuve matérielle, être établie par un faisceau d'indices concordants dont il résulte, indépendamment de l'opportunité de l'opération considérée, que le seul intérêt des porteurs de parts du fonds ne peut expliquer l'opération effectuée pour son compte par la société de gestion ;<br/>
<br/>
              7. Considérant qu'aux termes des dispositions des deuxième et troisième alinéas de l'article 411-14 du règlement général de l'Autorité des marchés financiers, dans leur version applicable au litige : " Lorsque l'actif d'une SICAV devient inférieur à 4 000 000 d'euros, aucun rachat des actions de la SICAV ne peut être effectué. / Lorsque l'actif d'un FCP devient inférieur à 300 000 euros, ou à 160 000 euros lorsque le FCP est dédié conformément au troisième alinéa de l'article 411-12, les rachats de parts sont suspendus. / Lorsque l'actif demeure pendant trente jours inférieur aux montants mentionnés aux premier et deuxième alinéas, il est procédé à la liquidation de l'OPCVM concerné, ou à l'une des opérations mentionnées à l'article 411-17. / (...) " ; <br/>
<br/>
              8. Considérant qu'il résulte de l'instruction que, le 19 octobre 2011, la Société Générale Corporate et Investment Banking a demandé le rachat de 31 282 parts du fonds Turgot Multigest International, géré par la société Turgot Asset Management, entraînant une décollecte de plus de 90 % de l'actif net de ce fonds, lequel est ainsi passé de 2 473 760 euros à 73 987 euros ; que, le lendemain, soit le 20 octobre 2011, les fonds ORCAP, TME et TAF, tous trois gérés par la société Turgot Asset Management, ont souscrit des parts du fonds Turgot Multigest International, dont l'actif net s'établissait, à la suite de ces opérations, à 309 684 euros, soit un niveau supérieur au seuil minimal réglementaire de 300 000 euros prévu par les dispositions citées ci-dessus ; que ces opérations ont entraîné, pour les trois fonds souscripteurs, une surexposition aux risques liés aux variations de valeur des actions ainsi souscrites, dans la mesure où la société Turgot Asset Management était alors dans l'obligation de vendre très rapidement les actifs du fonds Turgot Multigest International en raison de la décollecte de son actif ; que la valeur liquidative du fonds Turgot Multigest International a d'ailleurs diminué de 7,5 % entre les 20 et 21 octobre ; qu'il résulte de tous ces éléments que, ainsi que l'a estimé la commission des sanctions de l'Autorité des marchés financiers, la souscription de parts du fonds Turgot Multigest International par les fonds ORCAP, TME et TAF doit être regardée comme découlant de la volonté de la société Turgot Asset Management de faire remonter l'actif net du fonds au-dessus du seuil réglementaire de 300 000 euros afin d'éviter sa liquidation, et non par la prise en compte du seul intérêt des porteurs des trois fonds souscripteurs ; que, par suite, la commission des sanctions n'a pas entaché sa décision d'erreur d'appréciation en retenant à l'encontre la société Turgot Asset Management le grief tiré du manquement à l'obligation d'agir dans le seul intérêt des porteurs de parts des fonds souscripteurs ; <br/>
<br/>
<br/>
              En ce qui concerne les sanctions prononcées :<br/>
<br/>
              9. Considérant qu'aux termes du quatrième alinéa du III de l'article L. 621-15 du code monétaire et financier : " Le montant de la sanction doit être fixé en fonction de la gravité des manquements commis et en relation avec les avantages ou les profits éventuellement tirés de ces manquements. " ; qu'il ne résulte pas de l'instruction qu'en infligeant à la société Turgot Asset Management une sanction pécuniaire d'un montant de 80 000 euros et en décidant la publication de cette décision sur le site Internet de l'Autorité des marchés financiers, la commission des sanctions ait, dans les circonstances de l'espèce, retenu des sanctions disproportionnées au regard de la gravité de la nature et de la durée des manquements commis, de la situation financière de la société et des autres éléments mentionnés au point 4 ; <br/>
<br/>
              10. Considérant qu'il résulte de tout ce qui précède que la société Turgot Asset Management n'est pas fondée à demander l'annulation de la décision qu'elle attaque ;<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              11. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Autorité des marchés financiers, qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de la société Turgot Asset Management la somme de 3 000 euros à verser à l'Autorité des marchés financiers, au titre des mêmes dispositions ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
---------<br/>
Article 1er : La requête de la société Turgot Asset Management est rejetée.<br/>
Article 2 : La société Turgot Asset Management versera à l'Autorité des marchés financiers une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société Turgot Asset Management, à l'Autorité des marchés financiers et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
