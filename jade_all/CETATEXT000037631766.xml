<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037631766</ID>
<ANCIEN_ID>JG_L_2018_11_000000416203</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/63/17/CETATEXT000037631766.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 21/11/2018, 416203, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-11-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>416203</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>M. Sylvain Monteillet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:416203.20181121</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Leroy Merlin France a demandé au tribunal administratif d'Orléans la décharge de la cotisation de taxe d'enlèvement des ordures ménagères à laquelle elle a été assujettie au titre de l'année 2014 pour son établissement situé 1, rue Victor de Broglie à Tours (Indre-et-Loire), ainsi que la restitution de la somme en cause. Par un jugement n° 1601086 du 3 octobre 2017, le tribunal administratif d'Orléans a rejeté cette demande.<br/>
<br/>
              Par un pourvoi, enregistré le 4 décembre 2017 au secrétariat du contentieux du Conseil d'Etat, la société Leroy Merlin France demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Sylvain Monteillet, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de la société Leroy Merlin France ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. La société Leroy Merlin France se pourvoit en cassation contre le jugement du 3 octobre 2017 par lequel le tribunal administratif d'Orléans a rejeté sa demande de décharge de la cotisation de taxe d'enlèvement des ordures ménagères à laquelle elle a été assujettie au titre de l'année 2014 pour son établissement situé 1, rue Victor de Broglie à Tours (Indre-et-Loire), ainsi que la restitution de la somme en cause.<br/>
<br/>
              2. Aux termes du I de l'article 1520 du code général des impôts, dans sa rédaction en vigueur au titre de l'année d'imposition en litige : " Les communes qui assurent au moins la collecte des déchets des ménages peuvent instituer une taxe destinée à pourvoir aux dépenses du service dans la mesure où celles-ci ne sont pas couvertes par des recettes ordinaires n'ayant pas le caractère fiscal. (...) ". La taxe d'enlèvement des ordures ménagères n'a pas le caractère d'un prélèvement opéré sur les contribuables en vue de pourvoir à l'ensemble des dépenses budgétaires de la commune mais a exclusivement pour objet de couvrir les dépenses exposées par la commune pour assurer l'enlèvement et le traitement des ordures ménagères et non couvertes par des recettes non fiscales. Ces dépenses sont constituées de la somme de toutes les dépenses de fonctionnement réelles exposées pour le service public de collecte et de traitement des déchets ménagers et des dotations aux amortissements des immobilisations qui lui sont affectées, telle qu'elle peut être estimée à la date du vote de la délibération fixant le taux de la taxe.<br/>
<br/>
              3. Le tribunal administratif d'Orléans a jugé que la légalité de la délibération et du taux qu'elle fixe doit s'apprécier en comparant le produit de la taxe d'enlèvement des ordures ménagères au montant estimé des dépenses, tant en section de fonctionnement qu'en section d'investissement, non couvertes par des recettes non fiscales, à l'exclusion du report du produit de la section de fonctionnement résultant des années précédentes qui n'a pas à être intégré dans le calcul et des montants se rapportant aux opérations d'ordre, lesquelles correspondent à des jeux d'écriture entre sections. Il résulte de ce qui a été dit au point 2 qu'en jugeant qu'il y avait lieu de prendre en compte les dépenses réelles d'investissement affectées au service et non les dotations aux amortissements des immobilisations, le tribunal administratif a commis une erreur de droit. Par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, le jugement attaqué doit être annulé.<br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à la société Leroy Merlin France de la somme de 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif d'Orléans du 3 octobre 2017 est annulé.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif d'Orléans.<br/>
Article 3 : L'Etat versera une somme de 500 euros à la société Leroy Merlin France au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la société Leroy Merlin France et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
