<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039112463</ID>
<ANCIEN_ID>JG_L_2019_08_000000433284</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/11/24/CETATEXT000039112463.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 27/08/2019, 433284, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-08-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>433284</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP COUTARD, MUNIER-APAIRE</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2019:433284.20190827</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 5 août 2019 et 20 août 2019 au secrétariat du contentieux du Conseil d'Etat, la société AC Environnement demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
              1°) de suspendre l'exécution des dispositions des articles 4 et 13 de l'arrêté interministériel du 16 juillet 2019 relatif au repérage de l'amiante avant certaines opérations réalisées dans les immeubles bâtis en tant qu'elles font obligation aux opérateurs de repérage d'amiante de disposer de la certification avec mention prévue à l'article 2 de l'arrêté du 25 juillet 2016 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Elle soutient que : <br/>
              - la condition d'urgence est remplie dès lors que les dispositions contestées ont pour effet, en l'absence d'un nombre suffisant d'opérateurs de repérage d'amiante disposant des qualifications requises, de lui faire renoncer à l'exécution de nombreux contrats, conduisant à une baisse significative de son chiffre d'affaires et de son résultat ;<br/>
              - il existe un doute sérieux quant à la légalité des dispositions contestées ;<br/>
              - elles portent une atteinte disproportionnée au principe de sécurité juridique en ne prévoyant aucune disposition transitoire ;<br/>
              - elles méconnaissent l'impératif de clarté et d'intelligibilité de la norme ;<br/>
              - elles entraînent une rupture d'égalité entre le domaine des immeubles bâtis et celui des navires, bateaux, engins flottants et autres constructions flottantes dès lors que l'arrêté règlementant le domaine des navires, bateaux, engins flottants et autres constructions flottantes dispose d'une période transitoire et d'une méthode de certification différente ;<br/>
              - l'arrêté du 16 juillet 2019 doit être annulé par voie de conséquence de l'annulation de l'arrêté du 25 juillet 2016 par la décision du Conseil d'Etat statuant au contentieux du 24 juillet 2019.<br/>
<br/>
<br/>
              Par deux mémoires en défense, enregistrés les 9 août 2019 et 21 août 2019, la ministre du travail conclut au rejet de la requête. Elle soutient que la condition d'urgence n'est pas remplie et qu'aucun des moyens soulevés n'est propre à créer un doute sérieux quant à la légalité de la décision attaquée.<br/>
<br/>
<br/>
	Vu les autres pièces des dossiers ;<br/>
	Vu :<br/>
	- le code la construction et de l'habitation ;<br/>
	- le code de la santé publique ;<br/>
	- le code du travail ;<br/>
	- le code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, la société<br/>
AC Environnement et, d'autre part, la ministre du travail, la ministre de la transition écologique et solidaire et la ministre de la cohésion des territoires et des relations avec les collectivités locales ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 22 août 2019 à 14 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Madeleine Munier-Apaire, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la société AC Environnement ;<br/>
<br/>
              - le représentant de la société AC Environnement ;<br/>
<br/>
- les représentants de la ministre du travail ;<br/>
- les représentants de la ministre de la transition écologique et solidaire ;<br/>
              - les représentants de la ministre de la cohésion des territoires et des relations avec les collectivités locales ;<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". <br/>
<br/>
<br/>
<br/>
<br/>
              Sur le cadre juridique :<br/>
              2. D'une part, des dispositions des articles R. 1334-20 et suivants du code de la santé publique, prises sur le fondement des articles L. 1334-12-1 et L. 1334-17 de ce code, font obligation, pour certaines opérations concernant des immeubles, d'opérer un repérage d'amiante. Un arrêté du 25 juillet 2016 a mis en oeuvre un dispositif de certification des personnes effectuant ces repérages, en créant deux niveaux de certification, l'un " sans mention ", l'autre " avec mention ", exigé pour les repérages les plus délicats. Un arrêté du 2 juillet 2018, modifié en mars 2019, a prévu l'abrogation et le remplacement de ces dispositions à compter du<br/>
1er janvier 2020. Toutefois, l'arrêté du 25 juillet 2016 a été annulé par une décision du Conseil d'Etat statuant au contentieux du 24 juillet 2019.<br/>
<br/>
              3. D'autre part, l'article L. 4412-2 du code du travail, issu de la loi du 8 août 2016, a étendu ces obligations de repérage en prévoyant que " le donneur d'ordre, le maître d'ouvrage ou le propriétaire d'immeubles par nature ou par destination, d'équipements, de matériels ou d'articles y font rechercher la présence d'amiante préalablement à toute opération comportant des risques d'exposition des travailleurs à l'amiante ". Pour l'application de ces dispositions, l'article R. 4412-97 de ce code, dispose que " II. La recherche d'amiante est assurée par un repérage préalable à l'opération, adapté à sa nature, à son périmètre et au niveau de risque qu'elle présente. / Les conditions dans lesquelles la mission de repérage est conduite, notamment s'agissant de ses modalités techniques et des méthodes d'analyse des matériaux susceptibles de contenir de l'amiante, sont précisées par arrêtés du ministre chargé du travail et, chacun en ce qui le concerne, des ministres chargés de la santé, de la construction, des transports et de la mer, pour les domaines d'activité suivants : 1° Immeubles bâtis ;<br/>
2° Autres immeubles tels que terrains, ouvrages de génie civil et infrastructures de transport ;<br/>
3° Matériels roulants ferroviaires et autres matériels roulants de transports ; 4° Navires, bateaux et autres engins flottants ; 5° Aéronefs ; 6° Installations, structures ou équipements concourant à la réalisation ou la mise en oeuvre d'une activité ". Sur le fondement de ces dispositions, a été pris l'arrêté du 16 juillet 2019 relatif au repérage de l'amiante avant certaines opérations réalisées dans les immeubles bâtis, entré en vigueur le lendemain du jour de sa publication au Journal officiel, soit le 19 juillet dernier. <br/>
<br/>
<br/>
              Sur le litige :<br/>
<br/>
              4. La société AC Environnement a saisi le juge des référés du Conseil d'Etat d'une demande, fondée sur les dispositions de l'article L. 521-1 du code de justice administrative, tendant à la suspension de l'exécution de cet arrêté en tant que ses articles 4 et 13 font obligation à l'opérateur de repérage de disposer de la certification " avec mention " prévue à l'article 3 de l'arrêté du 25 juillet 2016 mentionné au point 2.<br/>
<br/>
<br/>
              En ce qui concerne l'urgence : <br/>
<br/>
              5. L'urgence justifie que soit prononcée la suspension d'un acte administratif, lorsque l'exécution de celui-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre. Il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte litigieux sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue. L'urgence doit être appréciée objectivement et compte tenu de l'ensemble des circonstances de l'affaire, à la date à laquelle le juge des référés se prononce.<br/>
<br/>
              6. La société requérante fait valoir que les opérations de repérage d'amiante avant travaux représentent 80 % de son activité, pour laquelle elle ne dispose que de<br/>
64 opérateurs ayant obtenu la certification " avec mention " sur un effectif total d'environ<br/>
200 salariés. De manière plus générale, elle observe que sur environ 8250 opérateurs certifiés recensés en France, seuls 3153 sont titulaires de la certification " avec mention ". L'administration estime, pour sa part, que la protection de la santé des travailleurs constitue un intérêt public impérieux justifiant l'exécution des dispositions litigieuses.<br/>
<br/>
              7. Il n'est pas contesté que la certification avec mention, dont le processus est au demeurant bloqué à la suite de l'annulation des dispositions de l'arrêté du 25 juillet 2016, dure de deux à trois mois et n'est accessible qu'aux opérateurs justifiant de qualifications préexistantes. Il n'est ainsi pas douteux que l'exigence immédiate de cette mention pour le repérage éventuel d'amiante avant de nombreux travaux alors que l'état du droit antérieur ne la requérait pas a pour effet de perturber ou de retarder l'exécution des contrats déjà passés et dans certains cas d'obliger la société requérante ainsi, vraisemblablement que d'autres entreprises spécialisées, à renoncer à soumissionner à de futurs contrats faute de disposer des opérateurs qualifiés. En outre, s'il n'est pas contestable que la prévention des risques liés à l'amiante constitue un impératif de santé publique, il n'est pas établi, dans les circonstances de l'espèce, que le maintien de l'exécution de ces dispositions y participe, alors que, d'une part, le dispositif de certification auxquelles elles se réfèrent a été annulé par la décision mentionnée au point 2. et, d'autre part, ainsi qu'il a été dit au même point, qu'elles seront, à compter du 1er janvier prochain, remplacées par un régime, dont il est admis qu'il sera moins exigeant s'agissant des qualifications requises des opérateurs de repérage.  <br/>
<br/>
              8. Il résulte de ce qui précède que la condition d'urgence doit être regardée comme remplie.<br/>
<br/>
<br/>
              En ce qui concerne les moyens de nature à créer un doute sérieux sur la légalité des dispositions contestées :<br/>
<br/>
              9. En l'état de l'instruction, les moyens tirés de ce que les dispositions litigieuses méconnaîtraient, en l'absence de régime transitoire, le principe de sécurité juridique et seraient illégales du fait de l'annulation de l'arrêté du 25 juillet 2016 sont de nature à créer un doute sérieux sur leur légalité. <br/>
<br/>
              10. Il résulte de ce qui précède que les conditions posées par l'article L. 521-1 du code de justice administrative, pour que le juge des référés puisse prononcer la suspension de l'exécution d'un acte, étant remplies en l'espèce, il y a lieu de suspendre l'exécution des dispositions des articles 4 et 13 l'arrêté interministériel du 16 juillet 2019 relatif au repérage de l'amiante avant certaines opérations réalisées dans les immeubles bâtis en tant qu'ils prévoient que l'opérateur de repérage de l'amiante doit disposer de la certification avec mention prévue à l'article 2 de l'arrêté du 25 juillet 2016. <br/>
<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              11. Il y a lieu, dans les circonstances de l'espèce, de faire application de ces dispositions et de mettre à la charge de l'Etat la somme de 3000 euros à verser à la société<br/>
AC Environnement. <br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : L'exécution des articles 4 et 13 l'arrêté interministériel du 16 juillet 2019 relatif au repérage de l'amiante avant certaines opérations réalisées dans les immeubles bâtis, en tant qu'ils prévoient que l'opérateur de repérage de l'amiante doit disposer de la certification avec mention prévue à l'article 2 de l'arrêté du 25 juillet 2016 est suspendue.<br/>
Article 2 : L'Etat versera à la société AC Environnement une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente ordonnance sera notifiée à la société AC Environnement, à la ministre du travail, à la ministre de la transition écologique et solidaire, à la ministre des solidarités et de la santé et à la ministre de la cohésion des territoires et des relations avec les collectivités locales. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
