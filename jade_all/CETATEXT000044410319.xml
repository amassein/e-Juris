<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044410319</ID>
<ANCIEN_ID>JG_L_2021_12_000000448305</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/41/03/CETATEXT000044410319.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 03/12/2021, 448305, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>448305</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ZRIBI, TEXIER</AVOCATS>
<RAPPORTEUR>Mme Pauline Hot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier  Fuchs</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:448305.20211203</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              I. Sous les nos 448305 et 455519, par deux mémoires distincts et deux mémoires en réplique enregistrés le 6 septembre et le 12 novembre 2021 au secrétariat du contentieux du Conseil d'Etat, l'association des avocats pour la défense des droits des étrangers et l'association Infomie demandent au Conseil d'Etat, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 et à l'appui de leurs requêtes tendant à l'annulation pour excès de pouvoir, sous le n° 448305, du décret n° 2020-1370 du 10 novembre 2020 relatif à la légalisation des actes publics établis par une autorité étrangère et, sous le n° 455519, de la décision implicite par laquelle le Premier ministre a rejeté leur demande tendant à l'abrogation de ce décret, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du II de l'article 16 de la loi n° 2019-222 du 23 mars 2019 de programmation 2018-2022 et de réforme de la justice. <br/>
<br/>
<br/>
<br/>
              II. Sous le n° 454144, par un mémoire distinct et des observations en réplique enregistrés le 7 septembre et le 10 novembre 2021 au secrétariat du contentieux du Conseil d'Etat, le Groupe d'information et de soutien des immigrés, le syndicat des avocats de France et le Conseil national des barreaux demandent au Conseil d'État, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 et à l'appui de leur requête tendant à l'annulation pour excès de pouvoir de la décision implicite par laquelle le Premier ministre a rejeté leur demande tendant à l'abrogation du décret n° 2020-1370 du 10 novembre 2020 relatif à la légalisation des actes publics établis par une autorité étrangère, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du II de l'article 16 de la loi n° 2019-222 du 23 mars 2019 de programmation 2018-2022 et de réforme de la justice. <br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - le code civil ;<br/>
              - la loi n° 2019-222 du 23 mars 2019 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - l'ordonnance n° 2006-460 du 21 avril 2006 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Hot, auditrice,  <br/>
<br/>
              - les conclusions de M. Olivier Fuchs, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Zribi et Texier, avocat de l'association des avocats pour la défense des droits des étrangers et autre ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel susvisée : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé, y compris pour la première fois en cassation, à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de cet article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. Le II de l'article 16 de la loi du 23 mars 2019 de programmation 2018-2022 et de réforme pour la justice dispose que : " Sauf engagement international contraire, tout acte public établi par une autorité étrangère et destiné à être produit en France doit être légalisé pour y produire effet. / La légalisation est la formalité par laquelle est attestée la véracité de la signature, la qualité en laquelle le signataire de l'acte a agi et, le cas échéant, l'identité du sceau ou timbre dont cet acte est revêtu. / Un décret en Conseil d'Etat précise les actes publics concernés par le présent II et fixe les modalités de la légalisation ". A l'appui de leurs recours dirigés contre le décret du 10 novembre 2020 relatif à la légalisation des actes publics établis par une autorité étrangère, les associations requérantes demandent au Conseil d'État de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution de ces dispositions, notamment au motif qu'elles ne prévoient aucune dérogation, en cas d'urgence, à l'obligation de légalisation, ni de voie de recours contre les décisions de refus de légalisation. <br/>
<br/>
              3. Les dispositions ainsi contestées sont applicables au litige et n'ont pas été déclarées conformes à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel. Le moyen tiré de ce qu'elles portent atteinte aux droits et libertés garantis par la Constitution, notamment au droit à un recours juridictionnel effectif garanti par l'article 16 de la Déclaration des droits de l'homme et du citoyen du 26 août 1789, soulève une question présentant un caractère sérieux. <br/>
<br/>
              4. Il y a lieu, dès lors, de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La question de la conformité à la Constitution des dispositions du II de l'article 16 de la loi du 23 mars 2019 de programmation 2018-2022 et de réforme de la justice est renvoyée au Conseil constitutionnel.<br/>
Article 2 : Il est sursis à statuer sur les requêtes de l'association des avocats pour la défense des droits des étrangers et autre et du Groupe d'information et de soutien des immigrés et autres jusqu'à ce que le Conseil constitutionnel ait tranché la question de constitutionnalité ainsi soulevée.<br/>
Article 3 : La présente décision sera notifiée à l'association des avocats pour la défense du droit des étrangers, à l'association Infomie, au Groupe d'information et de soutien des immigrés, au syndicat des avocats de France, au conseil national des barreaux, au Premier ministre, au garde des sceaux, ministre de la justice et au ministre de l'Europe et des affaires étrangères.<br/>
<br/>
<br/>
              Délibéré à l'issue de la séance du 22 novembre 2021 où siégeaient : M. Rémy Schwartz, président adjoint de la Section du contentieux, présidant ; M. Fabien Raynaud, président de chambre ; Mme H... D..., M. F... B..., Mme C... E..., M. Cyril Roger-Lacan, conseillers d'Etat et Mme Pauline Hot, Auditrice-rapporteure. <br/>
<br/>
              Rendu le 3 décembre 2021.<br/>
<br/>
                 Le président : <br/>
                 Signé : M. Rémy Schwartz<br/>
 		La rapporteure : <br/>
      Signé : Mme Pauline Hot<br/>
                 La secrétaire :<br/>
                 Signé : Mme G... A...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
