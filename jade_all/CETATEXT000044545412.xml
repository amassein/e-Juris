<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044545412</ID>
<ANCIEN_ID>JG_L_2021_12_000000457618</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/54/54/CETATEXT000044545412.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 21/12/2021, 457618, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>457618</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO ; SCP GUÉRIN - GOUGEON</AVOCATS>
<RAPPORTEUR>Mme Thalia Breton</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Raphaël Chambon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:457618.20211221</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° Sous le numéro 457618, Mme D... C... a demandé au juge des référés du tribunal administratif de Melun, sur le fondement de l'article L. 521-1 du code de justice administrative, d'ordonner, à titre principal, la suspension de la décision d'ajournement définitif en filière médecine de la deuxième année des études de santé la concernant et, à titre subsidiaire, la suspension de la délibération du jury de licence option accès santé de l'Université Paris-Est-Créteil (UPEC) se prononçant sur l'admission des candidats en deuxième année et leur classement en filière médecine pour l'année 2020/2021. Par une ordonnance n° 2107880 du 4 octobre 2021, le juge de référés du tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un pourvoi, enregistré le 18 octobre 2021 au secrétariat du contentieux du Conseil d'Etat, Mme C... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant au titre de la procédure de référé engagée, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'UPEC la somme de 1 800 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le code de l'éducation ;<br/>
              - l'arrêté du 4 novembre 2019 relatif à l'accès aux formations de médecine, de pharmacie, d'odontologie et de maïeutique ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Thalia Breton, auditrice,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Guérin - Gougeon, avocat de Mme C... et de Mme F... et à Me Le Prado, avocat de l'Université Paris-Est Créteil ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". <br/>
<br/>
              2. Il ressort des pièces des dossiers soumis au juge des référés du tribunal administratif de Melun que Mme C... et Mme F..., étudiantes inscrites en licence accès santé (LAS) au titre de l'année universitaire 2020-2021 à l'université de Paris-Est-Créteil (UPEC), ont demandé, par deux requêtes distinctes, d'ordonner, à titre principal, la suspension de la décision d'ajournement définitif en filière médecine de la deuxième année des études de santé les concernant et à titre subsidiaire la suspension de la délibération du jury de licence option accès santé de cette université se prononçant sur l'admission des candidats en deuxième année et leur classement en filière médecine pour l'année 2020-2021. Par deux pourvois distincts qu'il y a lieu de joindre, Mme C... et Mme F... demandent l'annulation des ordonnances du 4 octobre 2021, par lesquelles le juge des référés a rejeté leurs demandes respectives.<br/>
<br/>
              3. Il ressort des termes mêmes des ordonnances attaquées que le juge des référés du tribunal administratif de Melun a estimé que les demandes des requérantes étaient manifestement mal fondées en raison de ce qu'elles se bornaient à soutenir que le règlement de l'UPEC concernant les modalités d'accès aux études de santé était entaché d'illégalité, dès lors que ce règlement n'avait pas fait l'objet de recours contentieux tendant à son annulation pour excès de pouvoir et était devenu définitif. En statuant ainsi, alors qu'en raison de la permanence de l'acte réglementaire, la légalité des règles qu'il fixe, comme la compétence de son auteur et l'existence d'un détournement de pouvoir, peuvent être mises en cause à tout moment, de telle sorte que puissent toujours être sanctionnées les atteintes illégales que cet acte est susceptible de porter à l'ordre juridique, le juge des référés du tribunal administratif de Melun a entaché ses ordonnances d'erreur de droit.<br/>
<br/>
              4. Il résulte de ce qui précède que, sans qu'il soit besoin de se prononcer sur les autres moyens des pourvois, Mme C... et Mme F... sont fondées à demander l'annulation des ordonnances du 4 octobre 2021 du juge des référés du tribunal administratif de Melun qu'elles attaquent.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de régler les affaires au titre des procédures de référé engagées par Mme C... et Mme F..., en application des dispositions de l'article L. 821-1 du code de justice administrative.<br/>
<br/>
              6. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
<br/>
              7. L'urgence justifie que soit prononcée la suspension d'un acte administratif lorsque l'exécution de celui-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre. Il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte litigieux sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue. L'urgence s'apprécie objectivement et compte tenu de l'ensemble des circonstances de l'espèce.<br/>
<br/>
              8. Au soutien de leurs demandes de suspension, pour justifier de ce que la condition d'urgence prévue par l'article L. 521-1 du code de justice administrative est satisfaite, Mme F... et Mme C... font principalement valoir que le refus de les admettre en deuxième année des études de médecine obère leurs chances d'y être admises à l'avenir et qu'il y a urgence à ce qu'il soit remédié aux irrégularités ayant entaché la procédure d'admission des étudiants en deuxième année des études de santé à l'UPEC. Toutefois, il ressort des pièces du dossier, en premier lieu, que, dès lors que le nombre d'étudiants admis en deuxième année des études de santé (dit numerus apertus) fixé par l'UPEC est limitatif, la suspension de l'exécution des décisions de non-admission concernant Mme C... et Mme F... et leur éventuelle admission en filière médecine, aurait nécessairement pour effet d'affecter la situation d'étudiants admis en deuxième année des études de santé. En second lieu, la suspension de l'exécution de la délibération du jury de licence option accès santé se prononçant sur l'admission des candidats et leur classement en filière médecine aurait pour effet, d'une part, de remettre en cause les décisions d'admission notifiées aux étudiants de première année du premier cycle des études de santé, qui ont déjà réalisé leur stage infirmier au cours de l'été 2021 et commencé à suivre les enseignements de deuxième année, et, d'autre part, de rendre nécessaire l'organisation de nouvelles épreuves orales et l'établissement d'un nouveau classement, ce qui perturberait significativement l'organisation de la filière santé de l'UPEC. Par suite, l'intérêt public s'oppose à ce que soit ordonnée la suspension de l'exécution des décisions contestées. Dans ces conditions, la condition d'urgence requise par les dispositions de l'article L. 521-1 du code de justice administrative ne saurait être regardée comme remplie.<br/>
<br/>
              9. Il résulte de ce qui précède que, sans qu'il soit besoin de se prononcer sur l'existence d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité des décisions contestées, les demandes de suspension de Mme C... et de Mme F... doivent être rejetées, y compris leurs conclusions à fin d'injonction.<br/>
<br/>
              10. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'il soit fait droit aux conclusions présentées à ce titre par Mme F... et Mme C... à l'encontre de l'UPEC dès lors que cette dernière n'est pas dans la présente instance la partie perdante. Dans les circonstances de l'espèce, il n'y a pas lieu de faire droit aux conclusions présentées au même titre par l'UPEC. <br/>
<br/>
<br/>
<br/>
<br/>
              D E C I D E :<br/>
              --------------<br/>
Article 1er : Les ordonnances n° 2107880 et n° 2107879 du 4 octobre 2021 du juge de référés du tribunal administratif de Melun sont annulées.<br/>
Article 2 : Les conclusions des demandes de Mme C... et de Mme F... devant le juge des référés du tribunal administratif de Melun et le surplus de leurs conclusions de cassation sont rejetées.<br/>
Article 3 : Les conclusions présentées par l'université de Paris-Est-Créteil au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 4 : La présente décision sera notifiée à Mme D... C..., à Mme A... F... et à l'université de Paris-Est-Créteil.<br/>
Copie en sera adressée à la ministre de l'enseignement supérieur, de la recherche et de l'innovation.<br/>
<br/>
Délibéré à l'issue de la séance du 25 novembre 2021 où siégeaient : Mme Maud Vialettes, présidente de chambre, présidant ; Mme Carine Soulay, conseillère d'Etat et Mme Thalia Breton, auditrice-rapporteure. <br/>
<br/>
<br/>
              Rendu le 16 décembre 2021.<br/>
              La présidente : <br/>
              Signé : Mme Maud Vialettes<br/>
<br/>
<br/>
La rapporteure: <br/>
Signé : Mme Thalia Breton<br/>
<br/>
      La secrétaire :<br/>
      Signé : Mme B... E...<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
