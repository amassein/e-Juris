<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038704080</ID>
<ANCIEN_ID>JG_L_2019_06_000000417409</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/70/40/CETATEXT000038704080.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 28/06/2019, 417409, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-06-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417409</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Dominique Chelle </RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:417409.20190628</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Bordeaux d'annuler la décision implicite du ministre de l'intérieur rejetant son recours préalable tendant, d'une part, à l'inscription du poste de chef de la division de sécurité de proximité de Pessac sur la liste des postes ouvrant droit à l'allocation de service puis au plafond réglementaire de la part fonctionnelle de l'indemnité de responsabilité et de performance et, d'autre part, à l'indemnisation du préjudice résultant pour lui de l'absence de versement de ces avantages, d'enjoindre au ministre de l'intérieur d'inscrire ce poste sur la liste des postes y ouvrant droit, de condamner l'Etat à l'indemniser du préjudice subi et de mettre à la charge de l'Etat la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative. Par un jugement n° 1401874 du 6 juin 2016, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 16BX02660 du 8 janvier 2018, enregistré au secrétariat du contentieux du Conseil d'Etat le 17 janvier 2018, la cour administrative d'appel de Bordeaux a annulé ce jugement et a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête présentée par M. A...devant le tribunal administratif. <br/>
<br/>
              Par un mémoire enregistré le 8 décembre 2017 au greffe de la cour administrative d'appel de Bordeaux, M. A...a déclaré se désister de ses conclusions tendant, d'une part, à l'annulation du refus du ministre de l'intérieur d'inscrire le poste de chef de la division de sécurité de proximité de Pessac sur la liste des postes ouvrant droit à l'allocation de service et, d'autre part, à ce qu'il soit enjoint au ministre d'inscrire ce poste sur cette liste.<br/>
<br/>
              Par deux courriers du 6 février 2018 et du 15 mai 2018, régulièrement notifiés, M. A...a été invité à régulariser sa requête.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 84-16 du 11 janvier 1984 ; <br/>
              - le décret n° 2004-455 du 27 mai 2004 ;	<br/>
              - le décret n° 2013-1144 du 11 décembre 2013 ; <br/>
              - l'arrêté du 16 juin 2011 fixant la liste des postes de chef de service ou d'unité organique prévue par le décret n° 2004-455 du 27 mai 2004 ;<br/>
              - l'arrêté du 17 janvier 2014 fixant la liste des postes de chef de circonscription de sécurité publique et de chef de service ou d'unité organique bénéficiant du plafond réglementaire de la part fonctionnelle de l'indemnité de responsabilité et de performance allouée aux fonctionnaires du corps de commandement de la police nationale ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Dominique Chelle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
              Sur la compétence du Conseil d'Etat :<br/>
<br/>
              1. Aux termes de l'article R. 311-1 du code de justice administrative : " Le Conseil d'Etat est compétent pour connaître en premier et dernier ressort : (...) 2° Des recours dirigés contre les actes réglementaires des ministres (...) ". Aux termes de l'article R. 341-1 du même code : " Lorsque le Conseil d'Etat est saisi de conclusions relevant de sa compétence de premier ressort, il est également compétent pour connaître de conclusions connexes relevant normalement de la compétence de premier ressort d'un tribunal administratif ou d'une cour administrative d'appel ".<br/>
<br/>
              2. La demande présentée par M. A...devant le tribunal administratif de Bordeaux et transmise au Conseil d'Etat, après annulation du jugement du 6 juin 2016 de ce tribunal, par l'arrêt du 8 janvier 2018 de la cour administrative d'appel de Bordeaux, doit être regardée comme tendant, en premier lieu, à l'annulation du refus du ministre de l'intérieur d'inscrire le poste de chef de la division de sécurité de proximité de Pessac sur la liste des postes ouvrant droit à l'allocation de service prévue par le décret visé ci-dessus du 27 mai 2004 puis au plafond réglementaire de la part fonctionnelle de l'indemnité de responsabilité et de performance prévue par le décret du 11 décembre 2013, en deuxième lieu, à ce qu'il soit enjoint au ministre de procéder à cette inscription et, enfin, à ce que l'Etat soit condamné à indemniser le requérant du préjudice subi du fait de l'absence de versement de ces avantages. Les conclusions à fin d'annulation, étant dirigées contre un acte d'un ministre présentant un caractère réglementaire, relèvent, ainsi que les conclusions à fin d'injonction, de la compétence de premier et dernier ressort du Conseil d'Etat en vertu des dispositions du 2° de l'article R. 311-1 du code de justice administrative. Les conclusions indemnitaires, étant connexes aux précédentes, relèvent également de cette compétence en vertu des dispositions de l'article R. 341-1 du même code. <br/>
<br/>
              Sur les conclusions à fin d'annulation et d'injonction :<br/>
<br/>
              3. Par son mémoire enregistré le 8 décembre 2017 au greffe de la cour administrative d'appel de Bordeaux, M. A...a déclaré se désister de ses conclusions tendant, d'une part, à l'annulation du refus du ministre de l'intérieur d'inscrire le poste de chef de la division de sécurité de proximité de Pessac sur la liste des postes ouvrant droit à l'allocation de service puis au plafond réglementaire de la part fonctionnelle de l'indemnité de responsabilité et de performance et, d'autre part, à ce qu'il soit enjoint au ministre de procéder à cette inscription. Ce désistement est pur et simple. Rien ne s'oppose à ce qu'il en soit donné acte.<br/>
<br/>
              Sur les conclusions indemnitaires : <br/>
<br/>
              4. Aux termes de l'article R. 432-1 du code de justice administrative: " La requête et les mémoires des parties doivent, à peine d'irrecevabilité, être présentés par un avocat au Conseil d'Etat ". Aux termes de l'article R. 432-2 du même code : " Toutefois, les dispositions de l'article R. 432-1 ne sont pas applicables : 1°) Aux recours pour excès de pouvoir contre les actes des diverses autorités administratives ; 2°) Aux recours en appréciation de légalité ; 3°) Aux litiges en matière électorale ; 4°) Aux litiges concernant la concession ou le refus de pension ".<br/>
<br/>
              5. Les conclusions de M. A...tendant à la réparation du préjudice subi du fait de l'absence de versement de l'allocation de service puis du plafond réglementaire de la part fonctionnelle de l'indemnité de responsabilité et de performance, qui ne sont pas au nombre de celles que l'article R. 432-2 du code de justice administrative dispense de l'obligation de ministère d'avocat, ont été présentées sans le ministère d'un avocat au Conseil d'Etat et à la Cour de cassation et n'ont pas été régularisées malgré les invitations qui ont été adressées au requérant les 6 février et 15 mai 2018. Par suite, ces conclusions ne sont pas recevables et doivent être rejetées. <br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              6. Ces dispositions font obstacle à ce que la somme que M. A...demande sur leur fondement soit mise à la charge de l'Etat qui n'est pas, dans la présente espèce, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Il est donné acte du désistement des conclusions de M. A...tendant, d'une part, à l'annulation du refus du ministre de l'intérieur d'inscrire le poste de chef de la division de sécurité de proximité de Pessac sur la liste des postes ouvrant droit à l'allocation de service puis au plafond réglementaire de la part fonctionnelle de l'indemnité de responsabilité et de performance et, d'autre part, à ce qu'il soit enjoint au ministre de procéder à cette inscription.<br/>
Article 2 : Le surplus des conclusions de la requête de M. A...est rejeté.<br/>
Article 3 : La présente décision sera notifiée à M. B...A...et  au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
