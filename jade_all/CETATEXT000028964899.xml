<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028964899</ID>
<ANCIEN_ID>JG_L_2014_05_000000370820</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/96/48/CETATEXT000028964899.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère / 6ème SSR, 20/05/2014, 370820</TITRE>
<DATE_DEC>2014-05-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>370820</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère / 6ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Julia Beurton</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:370820.20140520</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le mémoire, enregistré le 26 février 2014 au secrétariat du contentieux du Conseil d'Etat, présenté par la SELARL Tant d'M, dont le siège est 2 750, boulevard Paul Valéry à Montpellier (34070), représentée par son gérant en exercice, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 ; la société requérante demande au Conseil d'Etat, à l'appui de sa requête tendant à l'annulation pour excès de pouvoir de l'arrêté du 20 juin 2013 du ministre des affaires sociales et de la santé relatif aux bonnes pratiques de dispensation des médicaments par voie électronique, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du I et du 3° du II de l'article 4 de la loi du 24 février 2014 portant diverses dispositions d'adaptation au droit de l'Union européenne dans le domaine de la santé et de l'article L. 5125-34 du code de la santé publique, dans sa rédaction antérieure à la date d'entrée en vigueur de cette loi ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 7 mai 2014, présentée par la SELARL Tant d'M ; <br/>
<br/>
              Vu la Constitution, notamment son Préambule et son article 61-1 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
              Vu le code de la santé publique ;<br/>
<br/>
              Vu la loi n° 2014-201 du 24 février 2014 ;<br/>
<br/>
              Vu la décision n° 365317 du Conseil d'Etat, statuant au contentieux, du 17 juillet 2013 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Julia Beurton, Auditeur,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) " ; qu'il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2. Considérant que l'article 7 de l'ordonnance du 19 décembre 2012 relative au renforcement de la sécurité de la chaîne d'approvisionnement des médicaments, à l'encadrement de la vente de médicaments sur internet et à la lutte contre la falsification de médicaments a inséré dans le code de la santé publique un article L. 5125-34 ainsi rédigé : " Seuls peuvent faire l'objet de l'activité de commerce électronique les médicaments de médication officinale qui peuvent être présentés en accès direct au public en officine, ayant obtenu l'autorisation de mise sur le marché mentionnée à l'article L. 5121-8 ou un des enregistrements mentionnés aux articles L. 5121-13 et L. 5121-14-1 " ; que cet article a été annulé par la décision n° 365317 du Conseil d'Etat, statuant au contentieux, du 17 juillet 2013, en tant qu'il ne limite pas aux seuls médicaments soumis à prescription obligatoire l'interdiction de faire l'objet de l'activité de commerce électronique ; que l'article 4 de la loi du 24 février 2014 portant diverses dispositions d'adaptation au droit de l'Union européenne dans le domaine de santé a, par son I, ratifié l'ordonnance du 19 décembre 2012 et, par son II, modifié certaines dispositions de la cinquième partie du code de la santé publique, dont l'article L. 5125-34, qui dispose désormais que : " Seuls peuvent faire l'objet de l'activité de commerce électronique les médicaments qui ne sont pas soumis à prescription obligatoire " ;<br/>
<br/>
              En ce qui concerne le I de l'article 4 de la loi du 24 février 2014 et l'article L. 5125-34 du code de la santé publique dans sa rédaction antérieure à la date d'entrée en vigueur de cette loi :<br/>
<br/>
              3. Considérant, en premier lieu, que le I de l'article 4 de la loi du 24 février 2014 ratifie l'ordonnance du 19 décembre 2012 dans sa rédaction seule applicable de la date de son entrée en vigueur jusqu'au 25 février 2014, laquelle résulte de la décision du Conseil d'Etat, statuant au contentieux, du 17 juillet 2013 ; que, par suite, la société requérante ne saurait sérieusement soutenir que les dispositions qu'elle critique méconnaîtraient le principe de séparation des pouvoirs ou le droit à un recours juridictionnel effectif garantis par l'article 16 de la Déclaration des droits de l'homme et du citoyen de 1789 ;<br/>
<br/>
              4. Considérant, en second lieu, que les dispositions critiquées, qui ne portent pas atteinte à des situations légalement acquises et ne remettent pas en cause les effets qui peuvent légitimement être attendus de telles situations, ne méconnaissent pas la garantie des droits proclamée par l'article 16 de la Déclaration de 1789 ; que, par ailleurs, si le I de l'article 4 de la loi du 24 février 2014 ne mentionne pas la décision du Conseil d'Etat, statuant au contentieux, du 17 juillet 2013, il n'en résulte pas, en tout état de cause, que les dispositions critiquées méconnaîtraient l'objectif de valeur constitutionnelle d'accessibilité et d'intelligibilité de la loi, qui découle des articles 4, 5, 6 et 16 de la Déclaration de 1789 ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède, en ce qui concerne le I de l'article 4 de la loi du 24 février 2014 et l'article L. 5125-34 du code de la santé publique dans sa rédaction antérieure à la date d'entrée en vigueur de cette loi, que la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux ; <br/>
<br/>
              En ce qui concerne le 3° du II de l'article 4 de la loi du 24 février 2014 :<br/>
<br/>
              6. Considérant que le 3° du II de l'article 4 de la loi du 24 février 2014, qui modifie l'article L. 5125-34 du code de la santé publique, est entré en vigueur le lendemain de la publication de cette loi au Journal officiel de la République française, le 25 février 2014 ; que, par suite, il n'est pas applicable au litige soulevé par la société requérante, qui tend à l'annulation pour excès de pouvoir de l'arrêté du 20 juin 2013 relatif aux bonnes pratiques de dispensation des médicaments par voie électronique ;<br/>
<br/>
              7. Considérant qu'ainsi, sans qu'il soit besoin de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée, doit être écarté le moyen tiré de ce que les dispositions du I et du 3° du II de l'article 4 de la loi du 24 février 2014 portant diverses dispositions d'adaptation au droit de l'Union européenne dans le domaine de la santé et de l'article L. 5125-34 du code de la santé publique, dans sa rédaction antérieure à la date d'entrée en vigueur de cette loi, portent atteinte aux droits et libertés garantis par la Constitution ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la SELARL Tant d'M.<br/>
Article 2 : La présente décision sera notifiée à la SELARL Tant d'M et à la ministre des affaires sociales et de la santé.<br/>
Copie en sera adressée au Conseil constitutionnel et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-045 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. - ORDONNANCE AYANT FAIT L'OBJET D'UNE ANNULATION CONTENTIEUSE PARTIELLE - LOI DE RATIFICATION - PORTÉE - ORDONNANCE RATIFIÉE DANS SA RÉDACTION RÉSULTANT DE L'ANNULATION CONTENTIEUSE - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-10-05-04-02 PROCÉDURE. - QUESTION NE PRÉSENTANT PAS UN CARACTÈRE SÉRIEUX - DISPOSITION LÉGISLATIVE PROCÉDANT À LA RATIFICATION D'UNE ORDONNANCE AYANT FAIT L'OBJET D'UNE ANNULATION CONTENTIEUSE PARTIELLE - GRIEFS TIRÉS DE LA MÉCONNAISSANCE DE LA SÉPARATION DES POUVOIRS ET DU DROIT À UN RECOURS JURIDICTIONNEL EFFECTIF.
</SCT>
<ANA ID="9A"> 01-01-045 Une ordonnance que le Conseil d'Etat, statuant au contentieux, a partiellement annulée, est ratifiée dans la rédaction résultant de cette annulation.</ANA>
<ANA ID="9B"> 54-10-05-04-02 Une ordonnance que le Conseil d'Etat, statuant au contentieux, a partiellement annulée, est ratifiée dans la rédaction résultant de cette annulation.,, ...Par suite, les moyens tirés de ce que la loi de ratification en cause porterait, eu égard à son effet rétroactif, atteinte au principe de séparation des pouvoirs ou au droit à un recours juridictionnel effectif garantis par l'article 16 de la Déclaration des droits de l'homme et du citoyen de 1789 ne sauraient être regardés comme sérieux.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
