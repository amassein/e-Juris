<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031640686</ID>
<ANCIEN_ID>JG_L_2015_12_000000371486</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/64/06/CETATEXT000031640686.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème SSJS, 16/12/2015, 371486, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-12-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>371486</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>HAAS</AVOCATS>
<RAPPORTEUR>M. Matias de Sainte Lorette</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:371486.20151216</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure :<br/>
<br/>
              La société Gurin Holding Ltd a demandé au tribunal administratif de Nice la décharge des cotisations supplémentaires d'impôt sur les sociétés et de contribution sur cet impôt, et des pénalités correspondantes, auxquelles elle a été assujettie au titre des exercices clos en 2002 et 2003. Par un jugement n° 0603439 du 13 avril 2010, le tribunal a réduit les bases imposables de 4 263 euros au titre de chacun de ces exercices, prononcé la décharge des cotisations d'impôt correspondantes et rejeté le surplus des conclusions de la société requérante.<br/>
<br/>
              Par un arrêt n° 10MA02726 du 25 juin 2013, la cour administrative d'appel de Marseille, après avoir annulé ce jugement, a réduit les bases imposables d'une somme de 13 064 euros au titre de l'exercice 2002 et d'une somme de 13 131 euros au titre de l'exercice 2003, prononcé la décharge des cotisations d'impôt correspondantes et rejeté le surplus des conclusions de la société requérante. <br/>
<br/>
Procédure devant le Conseil d'Etat :<br/>
<br/>
              1° Sous le n° 371486 :<br/>
<br/>
              Par un pourvoi, enregistré le 21 août 2013 au secrétariat du contentieux du Conseil d'Etat, le ministre délégué, chargé du budget, demande au Conseil d'Etat d'annuler les articles 2 et 3 de cet arrêt.<br/>
<br/>
<br/>
              2° Sous le n° 371628 :<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 26 août et 26 novembre 2013 au secrétariat du contentieux du Conseil d'Etat, la société Gurin Holding Ltd demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'article 4 de cet arrêt rejetant le surplus de sa requête d'appel ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Matias de Sainte Lorette, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Haas, avocat de la société Gurin Holding Ltd ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Les pourvois visés ci-dessus sont dirigés contre le même arrêt. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que la société Gurin Holding Ltd, dont le siège est à Dublin en Irlande, a été assujettie à des cotisations supplémentaires d'impôt sur les sociétés et de contribution additionnelle sur cet impôt au titre des exercices clos en 2002 et 2003, au motif qu'elle avait effectué un acte anormal de gestion en mettant gratuitement à la disposition de ses associés un bien immobilier dont elle est propriétaire à Mandelieu-la-Napoule (Alpes-Maritimes). Par un jugement du 13 avril 2010, le tribunal administratif de Nice a réduit les bases imposables de la société Gurin Holding Ltd de 4 263 euros au titre de chacun des exercices en litige, prononcé la décharge des cotisations d'impôt correspondantes et rejeté le surplus des conclusions de cette société. Le ministre délégué, chargé du budget, se pourvoit en cassation contre l'arrêt du 25 juin 2013 en tant que, par les articles 1er et 2 de cet arrêt, la cour administrative d'appel de Marseille, après avoir annulé ce jugement, a réduit les bases imposables de la société Gurin Holding Ltd d'une somme de 13 064 euros au titre de l'exercice 2002 et d'une somme de 13 131 euros au titre de l'exercice 2003 et prononcé la décharge des cotisations d'impôt correspondantes. La société se pourvoit en cassation contre l'article 4 de l'arrêt du 25 juin 2013, par lequel la cour a rejeté le surplus de sa requête d'appel contre ce jugement.<br/>
<br/>
              Sur le pourvoi du ministre délégué, chargé du budget : <br/>
<br/>
              3. Il ressort des énonciations de l'arrêt attaqué que la cour a annulé le jugement du tribunal administratif de Nice pour vice de forme puis a évoqué l'affaire. Elle n'avait dès lors pas à prendre en compte, pour le calcul des montants des sommes dont elle a prononcé la décharge, la réduction de la base imposable prononcée par les premiers juges, contrairement à ce que soutient le ministre. Le ministre délégué, chargé du budget, n'est donc pas fondé, par le moyen qu'il invoque, à demander l'annulation des articles 2 et 3 de l'arrêt qu'il attaque.<br/>
<br/>
              Sur le pourvoi de la société Gurin Holding Ltd :<br/>
<br/>
              4. Il ressort des énonciations de l'arrêt attaqué que la cour a omis de répondre au moyen, invoqué par la société requérante devant elle et qui n'était pas inopérant, tiré de ce que la proposition de rectification était insuffisamment motivée en ce qui concerne la détermination du taux de rendement locatif retenu par l'administration fiscale.<br/>
<br/>
              5. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, que la société Gurin Holding Ltd est fondée à demander l'annulation de l'article 4 de l'arrêt qu'elle attaque.<br/>
<br/>
              Sur les conclusions de la société Gurin Holding Ltd présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 500 euros à verser à la société Gurin Holding Ltd, au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : Le pourvoi du ministre délégué, chargé du budget, est rejeté.<br/>
Article 2 : L'article 4 de l'arrêt du 25 juin 2013 de la cour administrative d'appel de Marseille est annulé.<br/>
Article 3 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Marseille.<br/>
Article 4 : L'Etat versera à la société Gurin Holding Ltd une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : La présente décision sera notifiée au ministre des finances et des comptes publics et à la société Gurin Holding Ltd.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
