<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042699900</ID>
<ANCIEN_ID>JG_L_2020_12_000000443382</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/69/99/CETATEXT000042699900.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 11/12/2020, 443382</TITRE>
<DATE_DEC>2020-12-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>443382</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. François Weil</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHR:2020:443382.20201211</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un jugement n° 1908486 du 13 février 2020, enregistré le 26 août 2020 au secrétariat du contentieux du Conseil d'Etat, le tribunal administratif de Nantes, avant de statuer sur la requête de M. A... B... tendant d'une part à l'annulation de la décision par laquelle la commission de recours contre les décisions de refus de visa d'entrée en France a implicitement rejeté le recours dirigé contre la décision du 6 février 2019 par laquelle les autorités consulaires à Fès (Maroc) ont abrogé le visa de court séjour à entrées multiples qui lui avait été délivré le 2 février 2018 ainsi que la décision des autorités consulaires, d'autre part à ce qu'il soit enjoint au ministre de l'intérieur de lui délivrer le visa sollicité ou, à défaut, de procéder au réexamen de sa demande dans un délai de quarante-huit heures à compter de la notification du jugement à intervenir, a décidé, en application des dispositions de l'article L. 113-1 du code de justice administrative, de transmettre le dossier de cette requête au Conseil d'Etat, en soumettant à son examen les questions suivantes : <br/>
<br/>
              1°) Les litiges relatifs aux décisions portant abrogation d'un visa d'entrée en France prises par les autorités diplomatiques ou consulaires relèvent-ils de la compétence territoriale du tribunal administratif de Nantes en application des dispositions de l'article R. 312-18 du code de justice administrative '<br/>
<br/>
              2°) Les décisions d'abrogation de visa prises par les autorités diplomatiques ou consulaires sont-elles soumises au recours administratif préalable devant la commission de recours contre les décisions de refus de visa d'entrée en France prévu par les dispositions de l'article D. 211-5 du code de l'entrée et du séjour des étrangers et du droit d'asile.<br/>
<br/>
              .........................................................................<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le règlement (CE) n° 810/2009 du 13 juillet 2009, notamment son article 34 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative, notamment son article L. 113-1, l'ordonnance 2020-1402 du 18 novembre 2020 et le décret 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Weil, conseiller d'Etat,<br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              REND L'AVIS SUIVANT :<br/>
<br/>
<br/>
              1.	En premier lieu, selon l'article 34 du règlement (CE) du 13 juillet 2009 du Parlement européen et du Conseil modifié établissant un code communautaire des visas : " 1. Un visa est annulé s'il s'avère que les conditions de délivrance du visa n'étaient pas remplies au moment de la délivrance, notamment s'il existe des motifs sérieux de penser que le visa a été obtenu de manière frauduleuse. Un visa est en principe annulé par les autorités compétentes de l'Etat membre de délivrance. (...)2. Un visa est abrogé s'il s'avère que les conditions de délivrance ne sont plus remplies. Un visa est en principe abrogé par les autorités compétentes de l'Etat membre de délivrance. (...) 7. Les titulaires dont le visa a été annulé ou abrogé peuvent former un recours contre cette décision, à moins que le visa n'ait été abrogé à la demande de son titulaire, (...) ". Le dernier alinéa de l'article R. 311-3 du code de l'entrée et du séjour des étrangers et du droit d'asile dispose : " Lorsqu'un étranger est autorisé à séjourner en France sous couvert d'un titre de voyage revêtu du visa requis pour un séjour d'une durée supérieure à trois mois et au plus égale à un an, ce visa peut être abrogé par le préfet du département où séjourne l'étranger qui en est titulaire, ou par le préfet du département où la situation de cet étranger est contrôlée, s'il existe des indices concordants permettant de présumer que l'intéressé a obtenu son visa frauduleusement ou qu'il est entré en France pour s'y établir à d'autres fins que celles qui ont justifié la délivrance du visa, ou si le comportement de l'intéressé trouble l'ordre public. Le préfet qui a prononcé l'abrogation en avertit sans délai l'autorité qui a délivré le visa ". Selon l'article R. 321-6 du même code : " Lorsqu'un étranger est autorisé à séjourner en France sous couvert d'un titre de voyage revêtu d'un visa requis pour les séjours n'excédant pas trois mois, ce visa peut être abrogé si l'étranger titulaire de ce visa exerce en France une activité lucrative sans y avoir été régulièrement autorisé, s'il existe des indices concordants permettant de présumer que l'intéressé est venu en France pour s'y établir ou si son comportement trouble l'ordre public. " Aux termes de l'article R. 321-7 du même code : " " L'abrogation du visa mentionnée à l'article R. 321-6 est décidée par le préfet du département où séjourne l'étranger qui en est titulaire ou du département où la situation de cet étranger est contrôlée. Le préfet qui a prononcé l'abrogation en avertit sans délai le ministre des affaires étrangères. " En vertu du premier alinéa de l'article R. 312-18 du code de justice administrative : " Les litiges relatifs au rejet des demandes de visa d'entrée sur le territoire de la République française relevant des autorités consulaires ressortissent à la compétence du tribunal administratif de Nantes. " L'article R. 312-19 du même code dispose : " Les litiges qui ne relèvent de la compétence d'aucun tribunal administratif par application des dispositions des articles R. 312-1 et R. 312-6 à R. 312-18 sont attribués au tribunal administratif de Paris. "<br/>
<br/>
              2.	S'agissant des étrangers se trouvant sur le territoire français sous couvert d'un visa requis pour le séjours n'excédant pas trois mois, les dispositions des articles R. 321-6 et R. 321-7 du code de l'entrée et du séjour des étrangers et du droit d'asile citées au point précédent fixent les conditions permettant à l'autorité administrative d'abroger ce visa et désignent, pour le prononcé de cette abrogation, le préfet du département où séjourne l'étranger titulaire de ce visa ou du département où sa situation est contrôlée en qualité d'autorité décisionnaire. La contestation de la décision préfectorale d'abrogation relève alors du tribunal administratif territorialement compétent pour connaître des décisions de ce préfet. En revanche, lorsque l'abrogation du visa est décidée par les autorités diplomatiques ou consulaires, l'intérêt d'une bonne administration de la justice conduit à attribuer au tribunal administratif de Nantes, dont relèvent déjà en vertu de l'article R. 312-18 du code de justice administrative précité les litiges relatifs au rejet par les autorités diplomatiques et consulaires des demandes de visa d'entrée sur le territoire français, les litiges relatifs à l'abrogation d'un visa par ces autorités.<br/>
<br/>
              3.	En second lieu, l'article D. 211-5 du code de l'entrée et du séjour des étrangers et du droit d'asile dispose : " Une commission placée auprès du ministre des affaires étrangères et du ministre chargé de l'immigration est chargée d'examiner les recours contre les décisions de refus de visa d'entrée en France prises par les autorités diplomatiques ou consulaires. La saisine de cette commission est un préalable obligatoire à l'exercice d'un recours contentieux, à peine d'irrecevabilité de ce dernier. " <br/>
<br/>
              4.	Ainsi qu'il résulte de ces dispositions, une personne dont le visa d'entrée en France est abrogé, que ce soit par le préfet ou par une autorité diplomatique ou consulaire, peut exercer un recours contentieux dans les conditions du droit commun, sans saisir la commission mentionnée à l'article D. 211-5 du code de l'entrée et du séjour des étrangers et du droit d'asile, lequel n'institue de recours préalable obligatoire à un recours contentieux qu'à l'égard des décisions refusant un visa et non de celles qui l'abrogent ou le retirent.<br/>
<br/>
<br/>
<br/>5.	Le présent avis sera notifié au tribunal administratif de Nantes, à M. A... B..., et au ministre de l'intérieur. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-05-01-02 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE EN PREMIER RESSORT DES TRIBUNAUX ADMINISTRATIFS. COMPÉTENCE TERRITORIALE. - RECOURS DIRIGÉ CONTRE LA DÉCISION ABROGEANT UN VISA D'ENTRÉE EN FRANCE - 1) DÉCISION PRISE PAR LE PRÉFET - TA COMPÉTENT POUR CONNAÎTRE DES DÉCISIONS DE CE PRÉFET - 2) DÉCISION PRISE PAR UNE AUTORITÉ DIPLOMATIQUE OU CONSULAIRE - TA DE NANTES.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">335-005-01 ÉTRANGERS. ENTRÉE EN FRANCE. VISAS. - ABROGATION - RÉGIME CONTENTIEUX - 1) COMPÉTENCE TERRITORIALE DES TA - A) DÉCISION PRISE PAR LE PRÉFET - TA COMPÉTENT POUR CONNAÎTRE DES DÉCISIONS DE CE PRÉFET - B) DÉCISION PRISE PAR UNE AUTORITÉ DIPLOMATIQUE OU CONSULAIRE - TA DE NANTES - 2) RECEVABILITÉ - RECOURS PRÉALABLE OBLIGATOIRE DEVANT LA COMMISSION DE RECOURS CONTRE LES REFUS DE VISA D'ENTRÉE EN FRANCE (ART. D. 211-5 DU CESEDA) - ABSENCE [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-01-02-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. LIAISON DE L'INSTANCE. RECOURS ADMINISTRATIF PRÉALABLE. - RECOURS PRÉALABLE OBLIGATOIRE DEVANT LA COMMISSION DE RECOURS CONTRE LES REFUS DE VISA D'ENTRÉE EN FRANCE (ART. D. 211-5 DU CESEDA) - ABSENCE - ABROGATION D'UN VISA D'ENTRÉE EN FRANCE [RJ1].
</SCT>
<ANA ID="9A"> 17-05-01-02 1) S'agissant des étrangers se trouvant sur le territoire français sous couvert d'un visa requis pour le séjour n'excédant pas trois mois, les dispositions des articles R. 321-6 et R. 321-7 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA) fixent les conditions permettant à l'autorité administrative d'abroger ce visa et désignent, pour le prononcé de cette abrogation, le préfet du département où séjourne l'étranger titulaire de ce visa ou du département où sa situation est contrôlée en qualité d'autorité décisionnaire.... ,,La contestation de la décision préfectorale d'abrogation relève alors du tribunal administratif (TA) territorialement compétent pour connaître des décisions de ce préfet.,,,2) En revanche, lorsque l'abrogation du visa est décidée par les autorités diplomatiques ou consulaires, l'intérêt d'une bonne administration de la justice conduit à attribuer au TA de Nantes, dont relèvent déjà en vertu de l'article R. 312-18 du code de justice administrative (CJA) les litiges relatifs au rejet par les autorités diplomatiques et consulaires des demandes de visa d'entrée sur le territoire français, les litiges relatifs à l'abrogation d'un visa par ces autorités.</ANA>
<ANA ID="9B"> 335-005-01 1) a) S'agissant des étrangers se trouvant sur le territoire français sous couvert d'un visa requis pour le séjour n'excédant pas trois mois, les dispositions des articles R. 321-6 et R. 321-7 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA) fixent les conditions permettant à l'autorité administrative d'abroger ce visa et désignent, pour le prononcé de cette abrogation, le préfet du département où séjourne l'étranger titulaire de ce visa ou du département où sa situation est contrôlée en qualité d'autorité décisionnaire.... ,,La contestation de la décision préfectorale d'abrogation relève alors du tribunal administratif (TA) territorialement compétent pour connaître des décisions de ce préfet.,,,b) En revanche, lorsque l'abrogation du visa est décidée par les autorités diplomatiques ou consulaires, l'intérêt d'une bonne administration de la justice conduit à attribuer au TA de Nantes, dont relèvent déjà en vertu de l'article R. 312-18 du code de justice administrative (CJA) les litiges relatifs au rejet par les autorités diplomatiques et consulaires des demandes de visa d'entrée sur le territoire français, les litiges relatifs à l'abrogation d'un visa par ces autorités.,,,2) Ainsi qu'il résulte de l'article D. 211-5 du CESEDA, une personne dont le visa d'entrée en France est abrogé, que ce soit par le préfet ou par une autorité diplomatique ou consulaire, peut exercer un recours contentieux dans les conditions du droit commun, sans saisir la commission mentionnée à cet article, lequel n'institue de recours préalable obligatoire à un recours contentieux qu'à l'égard des décisions refusant un visa et non de celles qui l'abrogent ou le retirent.</ANA>
<ANA ID="9C"> 54-01-02-01 Ainsi qu'il résulte de l'article D. 211-5 du CESEDA, une personne dont le visa d'entrée en France est abrogé, que ce soit par le préfet ou par une autorité diplomatique ou consulaire, peut exercer un recours contentieux dans les conditions du droit commun, sans saisir la commission mentionnée à cet article, lequel n'institue de recours préalable obligatoire à un recours contentieux qu'à l'égard des décisions refusant un visa et non de celles qui l'abrogent ou le retirent (M. Janati, avis, 2 / 7 CHR, 443382, 11 décembre 2020, B, M. Schwartz, pdt., M. Weil, rapp., Mme Roussel, rapp. publ.).</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp., s'agissant des recours contre les refus de visas d'entrée, CE, 19 février 2001,,, n° 228994, p. 78.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
