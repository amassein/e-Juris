<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037600024</ID>
<ANCIEN_ID>JG_L_2018_11_000000420654</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/60/00/CETATEXT000037600024.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 09/11/2018, 420654, Publié au recueil Lebon</TITRE>
<DATE_DEC>2018-11-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420654</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI ; SCP ROCHETEAU, UZAN-SARANO ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>Mme Cécile Renault</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:420654.20181109</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure <br/>
<br/>
              La société Groupement des laboratoires de biologie médicale (GLBM), la société Exalab, la société Reunilab, la société Synergibio et la société Beckman Coulter France ont demandé au tribunal administratif de Paris, d'une part, d'annuler, ou, à défaut, de résilier le marché public conclu le 19 décembre 2014 par la Caisse nationale d'assurance maladie des travailleurs salariés (CNAMTS) avec la société Cerba, relatif à la fourniture de kits de dépistage immunologique du cancer colorectal et à la gestion de la solution d'analyse des tests immunologiques quantitatifs de dépistage, et, d'autre part, d'enjoindre à la CNAMTS de communiquer les documents relatifs à la procédure de passation et au marché signé. Par un jugement n° 1503085 du 30 septembre 2016, le tribunal administratif de Paris a rejeté leur demande.<br/>
<br/>
              Le groupement d'intérêt économique (GIE) Labco Gestion a demandé au tribunal administratif de Paris d'annuler, ou, à défaut, de résilier le même marché. Par un jugement n° 1503071 du 30 septembre 2016, le tribunal administratif de Paris a rejeté sa demande.<br/>
<br/>
              Par un arrêt n°s 16PA03554, 16PA03573 du 24 avril 2018, la cour administrative d'appel de Paris a, sur appel de la société GLBM et du GIE Labco Gestion, annulé ces jugements ainsi que le marché public conclu le 19 décembre 2014 entre la CNAMTS et la société Cerba, avec effet au 1er août 2018. <br/>
<br/>
Procédures devant le Conseil d'Etat<br/>
<br/>
              1° Sous le numéro 420654, par un pourvoi et un mémoire en réplique, enregistrés les 15 mai et 8 octobre 2018 au secrétariat du contentieux du Conseil d'Etat, les sociétés Cerba et Delapack Europe B.V. demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter les requêtes de la société GLBM et du GIE Labco Gestion ;<br/>
<br/>
              3°) de mettre solidairement à la charge de la société GLBM et du GIE Labco Gestion la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              2° Sous le numéro 420663, par un pourvoi et un mémoire en réplique, enregistrés les 15 mai et 5 octobre 2018 au secrétariat du contentieux du Conseil d'Etat, la Caisse nationale d'assurance maladie demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter les requêtes de la société GLBM et du GIE Labco Gestion ;<br/>
<br/>
              3°) de mettre solidairement à la charge de la société GLBM et du GIE Labco Gestion la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code civil ;<br/>
              - le code général des impôts ;<br/>
              - le code des marchés publics ;<br/>
              - l'arrêté du 23 septembre 2014 portant introduction du test immunologique dans le programme de dépistage organisé du cancer colorectal ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Renault, auditeur,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, avocat de la société Cerba et de la société Daklapack Europe B.V., à la SCP Rocheteau, Uzan-Sarano, avocat de la société Groupement des laboratoires de biologie médicale et du groupement d'intérêt économique Labco Gestion, à la SCP Foussard, Froger, avocat de la Caisse nationale d'assurance maladie, à la SCP Rocheteau, Uzan-Sarano, avocat du groupement d'intérêt économique Babco Gestion et à la SCP Gatineau, Fattaccini, avocat de la société Daklapack Europe BV.<br/>
<br/>
<br/>
<br/>1. Considérant que les pourvois des sociétés Cerba et Daklapack Europe B.V., d'une part, et de la Caisse nationale d'assurance maladie, d'autre part, sont dirigés contre le même arrêt ; qu'il y a lieu de les joindre pour statuer par une seule décision ; <br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la Caisse nationale d'assurance maladie des travailleurs salariés (CNAMTS, devenue CNAM le 1er janvier 2018), agissant en qualité de coordonnateur d'un groupement de commandes constitué d'organismes d'assurance maladie, a, par un avis publié au Journal officiel de l'Union européenne du 28 janvier 2014, lancé un appel d'offres en vue de l'attribution d'un marché consistant, pour une durée de quatre ans, en la fourniture aux médecins et aux structures de gestion de " kits de dépistage immunologique " du cancer colorectal destinés à la population cible du programme de dépistage de ce cancer et en la " gestion de la solution d'analyse des tests ", de la réception des prélèvements jusqu'à la transmission des résultats ; que les sociétés Groupement des laboratoires de biologie médicale (GLBM), Beckman Coulter France, Exalab, RéuniLab, Synergibio et l'Institut inter régional pour la santé, composant un groupement momentané d'entreprises ayant pour mandataire la société GLBM, ont présenté une offre ; que le groupement d'intérêt économique (GIE) Labco Gestion, les sociétés CSP et Epiconcept et les laboratoires Barla, Bioliance, Labco midi et Biopaj, composant un groupement momentané d'entreprises ayant pour mandataire le GIE Labco Gestion, ont également présenté une offre ; que la CNAMTS a rejeté ces offres ainsi que celle d'un groupement constitué autour de l'Institut Pasteur de Lille comme irrégulières par des décisions du 6 octobre 2014 et a attribué le marché le 19 décembre 2014 au groupement constitué par la société Cerba, mandataire, et par la société Daklapack Europe BV, anciennement Minigrip Nederland B.V. ; que, saisi par la société GLBM et les autres membres de son groupement, d'une part, et le GIE Labco Gestion, d'autre part, d'un recours contestant la validité de ce contrat, le tribunal administratif de Paris a, par deux jugements du 30 septembre 2016, rejeté les requêtes tendant à l'annulation ou, à défaut, à la résiliation du marché ; que, par un arrêt du 24 avril 2018, la cour administrative d'appel de Paris a, sur appel des sociétés GLBM et du GIE Labco Gestion,  prononcé l'annulation du marché avec effet au 1er août 2018 ; que, par la décision n°s 420656, 420665 du 12 juillet 2018, le Conseil d'Etat, statuant au contentieux a ordonné le sursis à exécution de cet arrêt jusqu'à ce qu'il soit statué sur les présents pourvois, dirigés contre l'arrêt de la cour administrative d'appel de Paris   en tant qu'il  annule le marché en litige ;<br/>
<br/>
              Sur le cadre juridique du litige :<br/>
<br/>
              3. Considérant qu'indépendamment des actions dont disposent les parties à un contrat administratif et des actions ouvertes devant le juge de l'excès de pouvoir contre les clauses réglementaires d'un contrat ou devant le juge du référé contractuel sur le fondement des articles L. 551-13 et suivants du code de justice administrative, tout tiers à un contrat administratif susceptible d'être lésé dans ses intérêts de façon suffisamment directe et certaine par sa passation ou ses clauses est recevable à former devant le juge du contrat un recours de pleine juridiction contestant la validité du contrat ou de certaines de ses clauses non réglementaires qui en sont divisibles ; que cette action devant le juge du contrat est également ouverte aux membres de l'organe délibérant de la collectivité territoriale ou du groupement de collectivités territoriales concerné ainsi qu'au représentant de l'Etat dans le département dans l'exercice du contrôle de légalité ; que si le représentant de l'Etat dans le département et les membres de l'organe délibérant de la collectivité territoriale ou du groupement de collectivités territoriales concerné, compte tenu des intérêts dont ils ont la charge, peuvent invoquer tout moyen à l'appui du recours ainsi défini, les autres tiers ne peuvent invoquer que des vices en rapport direct avec l'intérêt lésé dont ils se prévalent ou ceux d'une gravité telle que le juge devrait les relever d'office ; qu'un concurrent évincé ne peut ainsi invoquer, outre les vices d'ordre public dont serait entaché le contrat, que les manquements aux règles applicables à la passation de ce contrat qui sont en rapport direct avec son éviction ;<br/>
<br/>
              4. Considérant que, saisi par un tiers dans les conditions définies ci-dessus, de conclusions contestant la validité du contrat ou de certaines de ses clauses, il appartient au juge du contrat, après avoir vérifié que l'auteur du recours autre que le représentant de l'Etat dans le département ou qu'un membre de l'organe délibérant de la collectivité territoriale ou du groupement de collectivités territoriales concerné se prévaut d'un intérêt susceptible d'être lésé de façon suffisamment directe et certaine et que les irrégularités qu'il critique sont de celles qu'il peut utilement invoquer, lorsqu'il constate l'existence de vices entachant la validité du contrat, d'en apprécier l'importance et les conséquences ; qu'ainsi, il lui revient, après avoir pris en considération la nature de ces vices, soit de décider que la poursuite de l'exécution du contrat est possible, soit d'inviter les parties à prendre des mesures de régularisation dans un délai qu'il fixe, sauf à résilier ou résoudre le contrat ; qu'en présence d'irrégularités qui ne peuvent être couvertes par une mesure de régularisation et qui ne permettent pas la poursuite de l'exécution du contrat, il lui revient de prononcer, le cas échéant avec un effet différé, après avoir vérifié que sa décision ne portera pas une atteinte excessive à l'intérêt général, soit la résiliation du contrat, soit, si le contrat a un contenu illicite ou s'il se trouve affecté d'un vice du consentement ou de tout autre vice d'une particulière gravité que le juge doit ainsi relever d'office, l'annulation totale ou partielle de celui-ci ; qu'il peut enfin, s'il en est saisi, faire droit, y compris lorsqu'il invite les parties à prendre des mesures de régularisation, à des conclusions tendant à l'indemnisation du préjudice découlant de l'atteinte à des droits lésés ;<br/>
<br/>
              Sur le pourvoi :<br/>
<br/>
              5. Considérant, en premier lieu, qu'une erreur conduisant à une appréciation inexacte du coût d'un achat par le pouvoir adjudicateur n'est pas, en elle-même, constitutive d'un vice du consentement ; qu'il ressort des pièces du dossier soumis aux juges du fond que l'offre de la société Cerba était présentée, pour les kits de dépistage,  pour un prix hors taxes et indiquait que la TVA n'était pas applicable, alors que cette taxe avait seulement pour redevable, en application de l'article 283 du code général des impôts, non la société néerlandaise membre du groupement qui réalisait les dispositifs, mais la CNAM, qui les achetait ; que, toutefois, à supposer même que, ainsi que le relève l'arrêt attaqué, la CNAM se soit, du fait de cette ambiguïté, méprise sur le coût total de l'offre pour elle et ait estimé à tort qu'il ne dépassait pas le montant des crédits budgétaires alloués au marché, la cour administrative d'appel de Paris a inexactement qualifié les faits qui lui étaient soumis en retenant, au vu des circonstances de fait qu'elle avait relevées, l'existence d'un vice du consentement de nature à affecter la validité du marché ; <br/>
<br/>
              6. Considérant, en second lieu, que la cour a estimé que, compte tenu de l'existence d'autres tests de dépistage aisément accessibles et de la circonstance que la campagne de prévention arrivait à son terme sans avoir obtenu les résultats escomptés, l'annulation du contrat ne pouvait être regardée comme portant une atteinte excessive à l'intérêt général et a prononcé son annulation, eu égard à l'intérêt s'attachant dans l'immédiat à la poursuite de l'exécution du contrat, avec effet différé au 1er août 2018 ; que, toutefois, compte tenu de l'enjeu majeur de santé publique que représente le dépistage du cancer colorectal, qui est l'un des cancers les plus meurtriers en France, à l'objet du marché litigieux, qui s'inscrit dans le cadre d'un vaste programme de santé publique, et aux conséquences de l'interruption du service sur l'efficacité du programme de dépistage, la cour administrative d'appel de Paris a inexactement qualifié les faits qui lui étaient soumis en jugeant que l'annulation du contrat litigieux ne portait pas une atteinte excessive à l'intérêt général ;  <br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens des pourvois, que la CNAM et les sociétés Cerba et Daklapack Europe B.V. sont fondées à demander l'annulation de l'arrêt qu'elles attaquent en tant qu'il annule le marché contesté avec effet au 1er août 2018 ;<br/>
<br/>
              8. Considérant que, dans les circonstances de l'espèce, il y a lieu, par application de l'article L. 821-2 du code de justice administrative, de régler l'affaire au fond dans cette mesure ;<br/>
<br/>
              Sur les fins de non recevoir opposées par la société Cerba :<br/>
<br/>
              9. Considérant que, contrairement à ce qui est soutenu, la circonstance qu'un concurrent évincé ait d'abord formé un référé précontractuel afin d'obtenir l'annulation de la procédure de passation ne fait pas obstacle à ce qu'il saisisse ensuite le juge administratif d'un recours en contestation de la validité du contrat ; que la circonstance que son offre ait été rejetée comme irrégulière n'est pas non plus de nature à le priver de la possibilité de faire un tel recours ; que, par suite, la société Cerba n'est pas fondée à soutenir que les requêtes de la société GLBM et du GIE Labco Gestion seraient, pour de tels motifs, irrecevables ; <br/>
<br/>
              Sur les conclusions aux fins d'annulation :<br/>
<br/>
              En ce qui concerne la régularité du jugement :<br/>
<br/>
              10. Considérant que la société et le GIE requérants soutiennent que le jugement attaqué est irrégulier, faute pour le tribunal administratif de Paris, d'avoir examiné tous leurs moyens ; qu'ainsi qu'il a été rappelé au point 3, un concurrent évincé ne peut invoquer, outre les vices d'ordre public dont serait entaché le contrat, que les manquements aux règles applicables à la passation de ce contrat qui sont en rapport direct avec son éviction ; qu'après avoir écarté les moyens dirigés contre les décisions rejetant leurs offres comme irrégulières, le tribunal administratif de Paris a écarté tous les autres manquements dont se prévalaient les requérants au motif qu'eu égard au caractère irrégulier de leurs offres, ces manquements n'étaient pas susceptibles de les avoir lésés ; qu'il a ainsi, implicitement mais nécessairement, jugé aussi qu'aucun de ces manquements n'entachait le contrat litigieux d'un vice d'ordre public ; que si les requérants contestent l'appréciation portée sur ce point par le tribunal,  il n'en résulte en tout état de cause  pas que le jugement serait entaché d'une insuffisance de motivation ;<br/>
<br/>
              En ce qui concerne le bien-fondé du jugement :<br/>
<br/>
              11. Considérant que les requérants soutiennent, d'une part, que leurs offres n'auraient pas dû être rejetées comme irrégulières et, d'autre part, que l'offre présentée par la société Cerba était entachée de graves irrégularités et aurait dû être rejetée comme irrégulière ou inacceptable ; qu'ainsi qu'il a été dit au point 3, un concurrent évincé ne peut invoquer, outre les vices d'ordre public dont serait entaché le contrat, que des manquements aux règles applicables à la passation de ce contrat en rapport direct avec son éviction ; qu'au titre de tels manquements, le concurrent évincé peut contester la décision par laquelle son offre a été écartée comme irrégulière ; qu'un candidat dont l'offre a été à bon droit écartée comme irrégulière ou inacceptable ne saurait en revanche soulever un moyen critiquant l'appréciation des autres offres ; qu'il ne saurait notamment soutenir que ces offres auraient dû être écartées comme irrégulières ou inacceptables, un tel manquement n'étant pas en rapport direct avec son éviction et n'étant pas, en lui-même, de ceux que le juge devrait relever d'office ;  qu'il en va ainsi y compris dans l'hypothèse où, comme en l'espèce, toutes les offres ont été écartées comme irrégulières ou inacceptables, sauf celle de l'attributaire, et qu'il est soutenu que celle-ci aurait dû être écartée comme irrégulière ou inacceptable ; <br/>
<br/>
              Quant au rejet des offres des requérants comme irrégulières :<br/>
<br/>
              12. Considérant que pour soutenir que leurs offres étaient conformes aux documents de la consultation et n'auraient pas dû être rejetées comme irrégulières, les requérants se bornent à reprendre les arguments qu'ils ont présentés en première instance et que les premiers juges ont écartés par un jugement suffisamment motivé ; qu'il y a lieu, par adoption des motifs retenus par le tribunal administratif de Paris, d'écarter ce moyen ;<br/>
<br/>
              Quant aux autres moyens : <br/>
<br/>
              13. Considérant que, pour les motifs exposés au point 5, aucun vice du consentement ne saurait être retenu ;<br/>
<br/>
              14. Considérant qu'il résulte de ce qui a été dit au point 11 que les requérants, dont les offres ont été à bon droit écartées comme irrégulières, ne peuvent soutenir que l'offre de la société Cerba aurait dû être écartée comme irrégulière ou inacceptable ;  <br/>
<br/>
              15. Considérant que si les requérants entendent soutenir que, du fait des irrégularités de l'offre de la société Cerba, attributaire du marché, qui la rendaient, selon eux, irrégulière et inacceptable, le contenu du contrat litigieux est lui-même entaché d'un vice, il résulte de ce qui a été dit au point 11 qu'ils ne peuvent soulever un tel moyen que si le vice ainsi allégué est d'ordre public, c'est-à-dire si le contenu du contrat est illicite ; que le contenu d'un contrat ne présente un caractère illicite que si l'objet même du contrat, tel qu'il a été formulé par la personne publique contractante pour lancer la procédure de passation du contrat ou tel qu'il résulte des stipulations convenues entre les parties qui doivent être regardées comme le définissant, est, en lui-même, contraire à la loi, de sorte qu'en s'engageant pour un tel objet, le cocontractant de la personne publique la méconnaît nécessairement ; <br/>
<br/>
              16. Considérant que si les requérants entendent à cet égard soutenir, en premier lieu, que les stipulations du contrat litigieux, telles qu'elles résultent de l'acte d'engagement signé après l'attribution du marché à la société Cerba, sont entachées de vices d'ordre public, d'une part, parce que le prix des kits de dépistage est fixé hors taxes alors que la CNAM devra en outre, en vertu de l'article 283 du code général des impôts, verser directement au Trésor le montant de la taxe sur la valeur ajoutée afférente, d'autre part, parce que le prix TTC de ces prestations dépasse les crédits budgétaires alloués au marché, de tels vices, à les supposer établis, ne sauraient en tout état de cause caractériser un contenu du contrat illicite ; <br/>
<br/>
              17. Considérant que si les requérants entendent soutenir, en second lieu, que le contenu du contrat litigieux est entaché d'un vice d'ordre public, au motif qu'il prévoit que son titulaire, conformément à ce que proposait son offre, devra faire procéder à la lecture des tests immunologiques dans un seul laboratoire, en méconnaissance de la réglementation en matière de santé publique, notamment de l'arrêté du 23 septembre 2014 portant introduction du test immunologique dans le programme de dépistage organisé du cancer colorectal, un tel moyen ne peut qu'être écarté dès lors qu'en tout état de cause, la réglementation dont se prévalent les requérants n'était pas applicable au marché en litige et n'impose d'ailleurs pas un nombre minimum de centres de lecture des tests ;<br/>
<br/>
              18. Considérant qu'il résulte de tout ce qui précède que la société GLBM et le GIE Labco Gestion ne sont pas fondés à soutenir que c'est à tort que le tribunal administratif de Paris a rejeté leurs demandes ; que leurs demandes d'annulation ou, à défaut, de résiliation du marché litigieux doivent être rejetées ; <br/>
<br/>
              19. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge respectivement de la société GLBM et du GIE Labco Gestion les sommes de 4 000 euros à verser, d'une part, à la société Cerba et à la société Daklapack Europe BV à parts égales et, d'autre part, à la CNAM au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que les mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge des sociétés Cerba et Dalkapack Europe B.V. et de la CNAM qui ne sont pas, dans la présente instance, les parties perdantes ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 1er à 4 de l'arrêt de la cour administrative d'appel de Paris du 24 avril 2018 sont annulés. <br/>
Article 2 : Les conclusions d'appel de la société GLBM et du groupement d'intérêt économique Labco Gestion tendant à l'annulation des jugements du tribunal administratif de Paris du 30 septembre 2016 ainsi qu'à l'annulation ou, à défaut, à la résiliation du marché conclu le 19 décembre par la CNAM pour la fourniture de " kits de dépistage immunologique " du cancer colorectal  et la " gestion de la solution d'analyse des tests " sont rejetées.<br/>
Article 3 : La société GLBM et le groupement d'intérêt économique Labco Gestion verseront aux sociétés Cerba et Daklapack Europe B.V. à parts égales, d'une part, et à la Caisse nationale d'assurance maladie, d'autre part, des sommes de 4 000 euros chacune, au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par la société GLBM et le GIE Labco Gestion au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la société Cerba, à la société Daklapack Europe B.V., à la Caisse nationale d'assurance maladie, à la société groupement des laboratoires de biologie médicale, et au groupement d'intérêt économique Labco Gestion.<br/>
Copie sera adressée à la ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-02 MARCHÉS ET CONTRATS ADMINISTRATIFS. FORMATION DES CONTRATS ET MARCHÉS. - VICE DE CONSENTEMENT - ERREUR CONDUISANT À UNE APPRÉCIATION INEXACTE DU COÛT D'UN ACHAT - ABSENCE EN ELLE-MÊME.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-02-005 MARCHÉS ET CONTRATS ADMINISTRATIFS. FORMATION DES CONTRATS ET MARCHÉS. FORMALITÉS DE PUBLICITÉ ET DE MISE EN CONCURRENCE. - RECOURS DÉFINI PAR LA JURISPRUDENCE TARN-ET-GARONNE [RJ1] - MOYENS INVOCABLES PAR LE CONCURRENT ÉVINCÉ - 1) MANQUEMENTS AUX RÈGLES DE PASSATION DU CONTRAT EN RAPPORT AVEC SON ÉVICTION [RJ2] - MOYEN TIRÉ DE CE QUE SON OFFRE A ÉTÉ ÉCARTÉE À TORT COMME IRRÉGULIÈRE OU INACCEPTABLE - INCLUSION - CAS DU CONCURRENT ÉVINCÉ DONT L'OFFRE A ÉTÉ ÉCARTÉE À BON DROIT COMME IRRÉGULIÈRE OU INACCEPTABLE - MOYEN CRITIQUANT L'APPRÉCIATION DES AUTRES OFFRES, NOTAMMENT EN CE QU'ELLES AURAIENT DÛ ÊTRE ÉCARTÉES COMME IRRÉGULIÈRES OU INACCEPTABLES - EXCLUSION - 2) MOYENS D'ORDRE PUBLIC - ILLICÉITÉ DU CONTENU DU CONTRAT - NOTION.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">39-04-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. FIN DES CONTRATS. NULLITÉ. - ILLICÉITÉ DU CONTENU DU CONTRAT - NOTION.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">39-08-01-03 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. RECEVABILITÉ. - RECOURS DÉFINI PAR LA JURISPRUDENCE TARN-ET-GARONNE [RJ1] - MOYENS INVOCABLES PAR LE CONCURRENT ÉVINCÉ - 1) MANQUEMENTS AUX RÈGLES DE PASSATION DU CONTRAT EN RAPPORT AVEC SON ÉVICTION [RJ2] - MOYEN TIRÉ DE CE QUE SON OFFRE A ÉTÉ ÉCARTÉE À TORT COMME IRRÉGULIÈRE - INCLUSION - CAS DU CONCURRENT ÉVINCÉ DONT L'OFFRE A ÉTÉ ÉCARTÉE À BON DROIT COMME IRRÉGULIÈRE OU INACCEPTABLE - MOYEN CRITIQUANT L'APPRÉCIATION DES AUTRES OFFRES, NOTAMMENT EN CE QU'ELLES AURAIENT DÛ ÊTRE ÉCARTÉES COMME IRRÉGULIÈRES OU INACCEPTABLES - EXCLUSION - 2) MOYENS D'ORDRE PUBLIC - ILLICÉITÉ DU CONTENU DU CONTRAT - NOTION.
</SCT>
<ANA ID="9A"> 39-02 Une erreur conduisant à une appréciation inexacte du coût d'un achat par le pouvoir adjudicateur n'est pas, en elle-même, constitutive d'un vice du consentement.</ANA>
<ANA ID="9B"> 39-02-005 1) Un concurrent évincé ne peut invoquer, outre les vices d'ordre public dont serait entaché le contrat, que des manquements aux règles applicables à la passation de ce contrat en rapport direct avec son éviction. Au titre de tels manquements, le concurrent évincé peut contester la décision par laquelle son offre a été écartée comme irrégulière. Un candidat dont l'offre a été à bon droit écartée comme irrégulière ou inacceptable ne saurait en revanche soulever un moyen critiquant l'appréciation des autres offres. Il ne saurait notamment soutenir que ces offres auraient dû être écartées comme irrégulières ou inacceptables, un tel moyen n'étant pas de ceux que le juge devrait relever d'office. Il  en va ainsi y compris dans l'hypothèse où toutes les offres ont été écartées comme irrégulières ou inacceptables, sauf celle de l'attributaire, et qu'il est soutenu que celle-ci aurait dû être écartée comme irrégulière ou inacceptable.,,,2) Requérants soutenant que, du fait des irrégularités de l'offre de la société attributaire du marché, qui la rendent, selon eux, irrégulière et inacceptable, le contenu du contrat litigieux est lui-même entaché d'un vice.,,Il résulte de ce qui a été précédemment dit au point 1) qu'ils ne peuvent soulever un tel moyen que si le vice ainsi allégué est d'ordre public, c'est-à-dire si le contenu du contrat est illicite. Le contenu d'un contrat ne présente un caractère illicite que si l'objet même du contrat, tel qu'il a été formulé par la personne publique contractante pour lancer la procédure de passation du contrat ou tel qu'il résulte des stipulations convenues entre les parties qui doivent être regardées comme le définissant, est, en lui-même, contraire à la loi, de sorte qu'en s'engageant pour un tel objet le cocontractant de la personne publique la méconnaît nécessairement.</ANA>
<ANA ID="9C"> 39-04-01 Le contenu d'un contrat ne présente un caractère illicite que si l'objet même du contrat, tel qu'il a été formulé par la personne publique contractante pour lancer la procédure de passation du contrat ou tel qu'il résulte des stipulations convenues entre les parties qui doivent être regardées comme le définissant, est, en lui-même, contraire à la loi, de sorte qu'en s'engageant pour un tel objet le cocontractant de la personne publique la méconnaît nécessairement.</ANA>
<ANA ID="9D"> 39-08-01-03 1) Un concurrent évincé ne peut invoquer, outre les vices d'ordre public dont serait entaché le contrat, que des manquements aux règles applicables à la passation de ce contrat en rapport direct avec son éviction. Au titre de tels manquements, le concurrent évincé peut contester la décision par laquelle son offre a été écartée comme irrégulière. Un candidat dont l'offre a été à bon droit écartée comme irrégulière ou inacceptable ne saurait en revanche soulever un moyen critiquant l'appréciation des autres offres. Il ne saurait notamment soutenir que ces offres auraient dû être écartées comme irrégulières ou inacceptables, un tel moyen n'étant pas de ceux que le juge devrait relever d'office. Il  en va ainsi y compris dans l'hypothèse où toutes les offres ont été écartées comme irrégulières ou inacceptables, sauf celle de l'attributaire, et qu'il est soutenu que celle-ci aurait dû être écartée comme irrégulière ou inacceptable.,,,2) Requérants soutenant que, du fait des irrégularités de l'offre de la société attributaire du marché, qui la rendent, selon eux, irrégulière et inacceptable, le contenu du contrat litigieux est lui-même entaché d'un vice.,,Il résulte de ce qui a été précédemment dit au point 1) qu'ils ne peuvent soulever un tel moyen que si le vice ainsi allégué est d'ordre public, c'est-à-dire si le contenu du contrat est illicite. Le contenu d'un contrat ne présente un caractère illicite que si l'objet même du contrat, tel qu'il a été formulé par la personne publique contractante pour lancer la procédure de passation du contrat ou tel qu'il résulte des stipulations convenues entre les parties qui doivent être regardées comme le définissant, est, en lui-même, contraire à la loi, de sorte qu'en s'engageant pour un tel objet le cocontractant de la personne publique la méconnaît nécessairement.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 4 avril 2014, Département de Tarn-et-Garonne, n° 358994, p. 70.,,[RJ2] Cf. CE, Section, 5 février 2016, Syndicat mixte des transports en commun Hérault transport, n° 383149, p.10.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
