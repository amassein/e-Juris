<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044945857</ID>
<ANCIEN_ID>JG_L_2021_12_000000459289</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/94/58/CETATEXT000044945857.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 24/12/2021, 459289, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>459289</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:459289.20211224</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
              Par une requête, enregistrée le 9 décembre 2021 au secrétariat du contentieux du Conseil d'Etat, Mme A... B... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) de suspendre l'exécution de l'arrêté du 29 novembre 2021 modifiant l'arrêté du 1er juin 2021 prescrivant les mesures générales nécessaires à la gestion de la sortie de crise sanitaire en tant qu'il limite la validité du passe sanitaire à vingt-quatre heures ; <br/>
<br/>
              2°) de suspendre l'exécution de l'obligation de présenter un " QR code ". <br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - la condition d'urgence est satisfaite ;<br/>
              - il est porté une atteinte grave et manifestement illégale à plusieurs libertés fondamentales ; <br/>
              - les mesures contestées méconnaissent le principe d'égalité et créent une discrimination entre les personnes vaccinées et les personnes non vaccinées dès lors que les personnes vaccinées ne sont pas soumises à l'obligation de réaliser des tests RT-PCR et antigéniques alors qu'il n'est pas établi qu'elles seraient moins protégées ou plus contagieuses que les personnes vaccinées ; <br/>
              - la limitation de la validité temporelle des tests nasopharyngés augmente les risques médicaux liés à la pratique de ces tests ; <br/>
              - le fait d'imposer la présentation d'un " QR code " comme unique mode de preuve de validité du passe sanitaire méconnaît le décret n° 2021-1059 du 7 août 2021 et constitue une discrimination en raison de l'état de santé. <br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu : <br/>
              - la Constitution, et notamment son Préambule ; <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - le code de la santé publique ; <br/>
              - la loi n° 2021-689 du 31 mai 2021 ; <br/>
              - la loi n° 2021-1040 du 5 août 2021 ; <br/>
              - la loi n° 2021-1465 du 10 novembre 2021 ;<br/>
              - le décret n° 2021-699 du 1er juin 2021 ; <br/>
              - le décret n° 2021-1059 du 7 août 2021 ; <br/>
              - le décret n° 2021-1521 du 25 novembre 2021 ; <br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. En premier lieu, par la disposition de l'arrêté du 29 novembre 2021 modifiant l'arrêté du 1er juin 2021 prescrivant les mesures générales nécessaires à la gestion de la sortie de crise sanitaire dont la requérante demande la suspension, le ministre de la santé s'est borné à transcrire dans les dispositions de l'arrêté de l'arrêté du 1er juin la réduction de 72 à 24 heures de la durée de validité des tests résultant du 5° de l'article 1er du décret du 25 novembre 2021 modifiant le décret n° 2021-699 du 1er juin 2021 prescrivant les mesures générales nécessaires à la gestion de la sortie de crise sanitaire. Les dispositions contestées de cet arrêté ne sont ainsi, par elles-mêmes, pas susceptibles de porter une atteinte grave et manifestement illégale à une liberté fondamentale. Au demeurant, le juge des référés du Conseil d'Etat a jugé par une ordonnance du 13 décembre que les mêmes moyens que ceux soulevés par Mme B... contre l'arrêté n'étaient pas de nature à caractériser de la part du décret du 25 novembre 2021 une atteinte grave et manifestement illégale aux libertés fondamentales.<br/>
<br/>
              3. En second lieu, le moyen tiré de ce que la vérification du passe sanitaire par QR code exclusivement méconnaîtrait les dispositions du décret du 7 août 2021 qui ne l'envisageraient que comme une faculté n'est, en tout état de cause, pas de nature à caractériser une atteinte grave et manifestement illégale à une liberté fondamentale. <br/>
<br/>
              4. Il résulte de ce qui précède que, sans qu'il soit besoin de se prononcer sur la condition d'urgence, la requête de Mme B... doit être rejetée par application des dispositions L. 522-3 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de Mme B... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à Mme A... B.... <br/>
Fait à Paris, le 24 décembre 2021<br/>
Signé : Gilles Pellissier<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
