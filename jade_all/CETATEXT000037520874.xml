<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037520874</ID>
<ANCIEN_ID>JG_L_2018_10_000000419286</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/52/08/CETATEXT000037520874.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 22/10/2018, 419286, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-10-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>419286</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Thibaut Félix</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:419286.20181022</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société en nom collectif Cannes Esterel a demandé au tribunal administratif de Nice de condamner conjointement et solidairement la commune de Cannes et l'Etat, d'une part, à lui verser la somme de 87 577 000 euros et, d'autre part, à verser à ses associés la somme de 10 602 000 euros en réparation des préjudices qu'elle estime avoir subis en raison du refus illégal de permis de construire que la commune de Cannes lui a opposé le 28 juillet 1994. Par un jugement n° 0302663 du 16 juillet 2015, le tribunal administratif de Nice a condamné la commune de Cannes à verser à la société Cannes Esterel la somme de 8 217 675 euros, assortie des intérêts au taux légal et de leur capitalisation et a rejeté le surplus de cette demande.<br/>
<br/>
              Par un arrêt n° 15MA03990 du 24 janvier 2018, la cour administrative d'appel de Marseille a, sur l'appel de la société Cannes Esterel et l'appel incident de la commune de Cannes, par son article 1er, ramené à 6 144 467,89 euros la somme, portant intérêts à compter du 5 février 2002, à verser par la commune de Cannes à la société Cannes Esterel en réparation des préjudices subis par cette société du fait des conséquences dommageables de l'arrêté du 28 juillet 1994, par son article 2, porté à 183 396,44 euros la somme des frais d'expertise supportés par la commune de Cannes, par son article 3, réformé le jugement du tribunal administratif de Nice du 16 juillet 2015 en ce qu'il a de contraire et, par son article 4, rejeté le surplus des conclusions de la société Cannes Esterel et de la commune de Cannes.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 26 mars et 27 juin 2018 au secrétariat du contentieux du Conseil d'Etat, la société Cannes Esterel demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler les articles 1er et 3 de cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond dans cette mesure, de faire droit à son appel et de rejeter l'appel incident de la commune de Cannes ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Cannes la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thibaut Félix, auditeur,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de la SNC Cannes Esterel.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
<br/>
              2. Pour demander l'annulation de l'arrêt qu'elle attaque, la société requérante soutient que :<br/>
              - la cour a commis une erreur de droit et dénaturé le jugement du tribunal administratif de Nice du 16 juillet 2015 en jugeant que celui-ci n'avait pas omis de statuer sur les conclusions de la société requérante tendant à l'engagement de la responsabilité de la commune de Cannes du fait du délai que celle-ci avait pris pour instruire la demande de permis de construire modificatif présentée le 19 juin 1992 ;<br/>
              - elle a insuffisamment motivé son arrêt et inexactement qualifié les faits en jugeant que la durée de l'instruction par la commune de cette demande ne revêtait pas un caractère fautif ;<br/>
              - elle a insuffisamment motivé son arrêt, commis une erreur de droit et dénaturé les faits de l'espèce en jugeant que la période d'indemnisation du préjudice résultant de l'absence d'exploitation du bâtiment B courait jusqu'au 1er juillet 2004 ;<br/>
              - elle a dénaturé le rapport d'expertise en jugeant que celui-ci avait évalué le coût nécessaire à la remise en état du bâtiment B à 1 322 967 francs.<br/>
<br/>
              3. Eu égard aux moyens soulevés, il y a lieu d'admettre les conclusions du pourvoi qui sont dirigées contre l'arrêt attaqué en tant qu'il s'est prononcé sur l'indemnisation du préjudice subi par la société Cannes Esterel au titre des frais de remise en état du bâtiment B. En revanche, aucun des moyens soulevés n'est de nature à permettre l'admission du surplus des conclusions du pourvoi.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les conclusions du  pourvoi de la société Cannes Esterel qui sont dirigées contre l'arrêt attaqué en tant qu'il s'est prononcé sur l'indemnisation du préjudice subi par la société Cannes Esterel au titre des frais de remise en état du bâtiment B sont admises.<br/>
Article 2 : Le surplus des conclusions du pourvoi de la société Cannes Esterel n'est pas admis.<br/>
Article 3 : La présente décision sera notifiée à la société en nom collectif Cannes Esterel.<br/>
Copie en sera adressée à la commune de Cannes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
