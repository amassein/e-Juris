<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043246463</ID>
<ANCIEN_ID>JG_L_2021_03_000000450151</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/24/64/CETATEXT000043246463.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 04/03/2021, 450151, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-03-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>450151</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:450151.20210304</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au juge des référés du tribunal administratif de Strasbourg, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'une part, de suspendre l'exécution de l'arrêté du préfet de la Moselle du 5 février 2021 en tant qu'il prévoit l'obligation du port du masque en extérieur sur l'ensemble du territoire du département de la Moselle et, d'autre part, d'enjoindre au préfet de la Moselle de prendre un nouvel arrêté dans un délai de vingt-quatre heures à compter de l'ordonnance à intervenir, sous astreinte de 50 euros par jours de retard, et précisant que le port du masque en extérieur ne concernera que les zones de forte densité de personnes ou celles présentant une difficulté à assurer le respect de la distance physique, que l'obligation du port du masque en extérieur ne concernera pas les utilisateurs de moyens de transport individuel et que cette obligation ne concernera pas les personnes en train de fumer, boire et manger dans l'espace public ouvert. Par une ordonnance n° 2100994 du 19 février 2021, le juge des référés du tribunal administratif de Strasbourg a rejeté ses demandes.<br/>
<br/>
              Par une requête, enregistrée le 25 février 2021 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de faire droit à ses demandes de première instance ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - il justifie d'un intérêt pour agir ;<br/>
              - l'ordonnance du juge des référés de première instance, s'agissant de l'appréciation de la condition d'urgence, est entachée, d'une part, d'erreur de droit, en ce qu'elle a retenu que l'arrêté contesté s'appliquait déjà depuis deux semaines à la date de saisine de la juridiction, l'absence d'urgence ne pouvant résulter de la seule circonstance du temps écoulé entre la décision contestée et la saisine, et, d'autre part, d'erreur d'appréciation, en ce qu'il fait état d'élément précis et circonstanciés de nature à établir que la condition d'urgence est remplie, à supposer qu'elle ne soit pas présumée en raison de la crise sanitaire ;<br/>
              - le juge des référés de première instance, s'agissant de l'appréciation du caractère grave et manifestement illégal porté aux libertés fondamentales invoquées, a méconnu le principe selon lequel une mesure doit être strictement proportionnée pour atteindre le but légitime qu'elle s'est fixé ;<br/>
              - la condition d'urgence est remplie eu égard, en premier lieu, aux atteintes portées à de nombreuses libertés fondamentales, en deuxième lieu, à la prolongation de l'état d'urgence sanitaire jusqu'au 1er juin 2021, les effets de l'arrêté contesté ayant ainsi vocation à être reconduits, bien qu'il prenne fin au 28 février 2021, en troisième lieu, à son caractère général et absolu, dès lors qu'il s'applique de manière uniforme sur tout le département, en quatrième lieu, à l'absence de prise en compte des circonstances de temps et de lieu en lien notamment avec la densité de population, l'importance des moyens de transport, la présence de tourisme et d'activité estudiantine et le caractère urbain ou rural des communes concernées, en cinquième lieu, à l'imminente reconduction des effets de l'arrêté contesté, en sixième lieu, au caractère inutile de la mesure contestée et aux nombreux inconvénients résultant de l'imposition du port du masque à des personnes en bonne santé, en septième lieu, à la nécessité d'encadrer l'action du préfet, la mesure de port du masque devant se cantonner aux lieux de forte fréquentation, en huitième lieu, à l'absence de lisibilité et de compréhension de la mesure contestée, et, en dernier lieu, à l'absence d'étude d'impact ;<br/>
              - il est porté une atteinte grave et manifestement illégale à plusieurs libertés fondamentales ; <br/>
              - il méconnaît la liberté d'aller et venir dès lors qu'il ne peut plus se déplacer sans devoir se conformer à l'obligation du port du masque auquel il ne réussit toujours pas à s'accoutumer, ce qui le conduit à limiter ses déplacements dans l'espace public ;<br/>
              - il méconnaît le droit à la vie privée et familiale et à ses libertés corollaires, lesquelles sont, d'une part, le droit à l'autodétermination, dès lors qu'il est contraint de porter le masque en milieu rural, ce qui le prive de son libre arbitre, d'autre part, le droit à une vie normale, l'absence de vie sociale créant un préjudice à son encontre, et, enfin, le droit à la santé, la mesure contestée, assortie de plusieurs autres mesures restrictives, ayant des effets nocifs sur sa santé mentale ;<br/>
              - il méconnaît le principe de fraternité, lequel doit permettre une exigence minimale de civilité nécessaire à la relation sociale ;<br/>
              - il méconnaît la liberté individuelle, laquelle comporte la liberté de refuser ou d'accepter de porter un masque, notamment dans un lieu de faible densité où les gestes barrières sont en mesure d'être mis en oeuvre ;<br/>
              - il méconnaît la liberté de réunion, laquelle est anéantie par l'obligation constante du port du masque et le respect scrupuleux des gestes barrières ;<br/>
              - l'arrêté contesté est entaché d'erreur de droit en ce qu'il méconnaît, d'une part, le principe de légalité des délits et des peines, dès lors que l'obligation fixée, laquelle est sanctionnée pénalement, est imprécise, notamment s'agissant de la possibilité d'exonération du port du masque dans le cadre d'activités sportives et durant l'exercice du droit de fumer dans l'espace public et, d'autre part, le secret médical, en raison de l'exigence d'un certificat médical pour justifier de la dérogation au port du masque pour les personnes en situation de handicap ; <br/>
              - l'arrêté contesté est entaché d'une erreur manifeste d'appréciation dès lors que la mesure de police administrative qu'il édicte n'est pas justifiée, nécessaire et proportionnée eu égard, en premier lieu, à l'absence de preuve de l'utilité du masque grand public, en deuxième lieu, à l'absence de prise en charge du coût de cette mesure pour les personnes fragiles, l'imposition du port du masque venant grever considérablement les finances des foyers les plus modestes, en troisième lieu, à la faible efficacité du masque grand public, en quatrième lieu, à l'absence de prise en compte de la létalité du virus, le risque pandémique ne pouvant se mesurer à sa seule contagiosité, en cinquième lieu, à la disproportion résultant d'une absence de référentiel absolu, les critères permettant de restreindre les libertés variant selon les périodes, en sixième lieu, à la généralisation et à la banalisation du port du masque en l'absence de justification sur la lisibilité et l'efficacité de la mesure, notamment en zone rurale, l'obligation de port du masque sur la voie publique s'appliquant de façon uniforme à toutes les communes du département, sans distinction en fonction des circonstances de temps et de lieu et, enfin, en septième lieu, à la balance avantage inconvénient, le nombre de personnes sauvées grâce à cette mesure ne pourra jamais être démontré tandis que ses effets négatifs relatifs au délitement du lien social peuvent être mesurés.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - le décret n°°2020-1257 du 14 octobre 2020 ;<br/>
              - le décret n° 2020-1310 du 29 octobre 2020 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée. Il appartient au juge des référés saisi en appel de porter son appréciation sur ce point au regard de l'ensemble des pièces du dossier, et notamment des éléments recueillis par le juge de première instance dans le cadre de la procédure qu'il a diligentée.<br/>
<br/>
              2. La liberté d'aller et venir, le droit au respect de la vie privée et familiale, le droit à une vie familiale normale, le droit à la santé, au principe de fraternité, à la liberté individuelle et à la liberté de réunion constituent des libertés fondamentales au sens de l'article L. 521-2 du code de justice administrative.<br/>
<br/>
              3. Une nouvelle progression de l'épidémie de covid-19 sur le territoire national a conduit le Président de la République à prendre, le 14 octobre 2020, sur le fondement des articles L. 3131-12 et L. 3131-13 du code de la santé publique, un décret déclarant l'état d'urgence sanitaire à compter du 17 octobre 2020 sur l'ensemble du territoire de la République. Les 16 et 29 octobre 2020, le Premier ministre a pris, sur le fondement de l'article L. 3131-15 du code de la santé publique, deux décrets prescrivant les mesures générales nécessaires pour faire face à l'épidémie de Covid-19 dans le cadre de l'état d'urgence sanitaire. Aux termes du II de l'article 1er du décret du 29 octobre 2020 : " Dans les cas où le port du masque n'est pas prescrit par le présent décret, le préfet de département est habilité à le rendre obligatoire, sauf dans les locaux d'habitation, lorsque les circonstances locales l'exigent ".<br/>
<br/>
              4. Le caractère proportionné d'une mesure de police s'apprécie nécessairement en tenant compte de ses conséquences pour les personnes concernées et de son caractère approprié pour atteindre le but d'intérêt général poursuivi. Sa simplicité et sa lisibilité, nécessaires à sa bonne connaissance et à sa correcte application par les personnes auxquelles elle s'adresse, sont un élément de son effectivité qui doivent, à ce titre, être prises en considération. Il en résulte que le préfet, lorsqu'il détermine les lieux dans lesquels il rend obligatoire le port du masque, est en droit de délimiter des zones suffisamment larges pour englober de façon cohérente les points du territoire caractérisés par une forte densité de personnes ou une difficulté à assurer le respect de la distance physique, de sorte que les personnes qui s'y rendent puissent avoir aisément connaissance de la règle applicable et ne soient pas incitées à enlever puis remettre leur masque à plusieurs reprises au cours d'une même sortie. Il peut, de même, définir les horaires d'application de cette règle de façon uniforme dans l'ensemble d'une même commune, voire d'un même département, en considération des risques encourus dans les différentes zones couvertes par la mesure qu'il adopte. Il doit, toutefois, tenir compte de la contrainte que représente, même si elle reste mesurée, le port d'un masque par les habitants des communes concernées, qui doivent également respecter cette obligation dans les transports en commun et, le plus souvent, dans leur établissement scolaire ou universitaire ou sur leur lieu de travail.<br/>
<br/>
              5. Par un arrêté du 5 février 2021, le préfet de la Moselle a imposé le port du masque aux personnes de onze ans et plus, dans toutes les communes de Moselle et à l'occasion de rassemblements, de 6 heures à minuit. Cet arrêté prévoit toutefois que l'obligation du port du masque ne s'applique ni aux personnes en situation de handicap munies d'un certificat médical justifiant de cette dérogation, ni à la pratique d'activités physiques. M. A... relève appel de l'ordonnance du 19 février 2021 par laquelle le juge des référés du tribunal administratif de Strasbourg a rejeté ses demandes, présentées sur le fondement de l'article L. 521-2 du code de justice administrative, tendant à ce que soit suspendue l'exécution de cet arrêté et qu'un nouvel arrêté soit édicté, précisant que le port du masque en extérieur ne concernera que les zones de forte densité de personnes ou celles présentant une difficulté à assurer le respect de la distance physique, que l'obligation du port du masque en extérieur ne concernera pas les utilisateurs de moyens de transport individuel et que cette obligation ne concernera pas les personnes en train de fumer, boire et manger dans l'espace public ouvert.<br/>
<br/>
              6. Pour rejeter la demande de M. A..., le juge des référés du tribunal administratif de Strasbourg, après avoir rappelé les principes et dispositions cités aux points 2, 3 et 4, a relevé, en premier lieu, qu'en l'état actuel des connaissances, le virus peut se transmettre par gouttelettes respiratoires, par contacts et par voie aéroportée et que les personnes peuvent être contagieuses sans le savoir. En deuxième lieu, il a constaté que la circulation virale, en forte augmentation dans le département de la Moselle, ne se limite pas aux agglomérations mosellanes à forte densité de population, bien que ces dernières soient particulièrement touchées, mais s'étend à la quasi-totalité du territoire, et pourrait être en lien avec une prévalence inexpliquée des variants de virus dans le département. En troisième lieu, il a relevé que l'aggravation de la situation sanitaire sur l'ensemble du département, malgré l'application depuis le 2 janvier 2021 d'un couvre-feu à partir de 18 heures, impose aux pouvoirs publics de prendre les mesures adaptées pour contenir la propagation de l'épidémie, notamment au regard du développement important de variants plus contagieux de ce virus depuis quelques semaines. En quatrième lieu, il a estimé qu'en imposant le port du masque sur l'ensemble du territoire du département de la Moselle pour une durée d'un peu plus de trois semaines et en prévoyant des dérogations pour les personnes en situation de handicap et pratiquant une activité physique, l'arrêté, qui est adapté et proportionné à l'objectif de santé publique qu'il poursuit, ne peut être regardé comme portant une atteinte grave et manifestement illégale aux libertés fondamentales invoquées par le requérant. En cinquième lieu, il a considéré qu'aucune atteinte au principe à valeur constitutionnelle de légalité des délits et des peines n'est établie dès lors qu'il n'appartient pas au préfet de prévoir, dans son arrêté, l'ensemble des dérogations à l'obligation du port du masque afin de permettre notamment aux personnes circulant sur la voie publique et dans les lieux ouverts au public de manger ou fumer, ni de déterminer précisément ce que revêtent les notions d'activités physiques et sportives qu'il autorise. En sixième lieu, il a estimé qu'il n'est porté aucune atteinte au secret médical dès lors que l'arrêté contesté, qui prévoit une dérogation pour les personnes en situation de handicap munies d'un certificat médical, n'impose pas de préciser sur le certificat médical la nature des pathologies. <br/>
<br/>
              7. M. A..., qui, au demeurant, ne conteste pas les données épidémiologiques du département, notamment celles relatives au nombre d'hospitalisation liées à la Covid-19 et au développement important des variants plus contagieux du virus, lesquelles font état d'une aggravation de la situation sanitaire, n'apporte aucun élément nouveau en appel susceptible d'infirmer l'appréciation ainsi retenue par le juge des référés de première instance.<br/>
<br/>
              8. Il résulte de ce qui précède qu'il est manifeste que l'appel de M. A... ne peut être accueilli. Sa requête ne peut, dès lors, qu'être rejetée, selon la procédure prévue par l'article L. 522-3 du code de justice administrative. <br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de M. A... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. A... et au ministre de l'intérieur.<br/>
Copie pour information en sera adressée au préfet de la Moselle.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
