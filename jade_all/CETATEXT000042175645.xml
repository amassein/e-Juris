<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042175645</ID>
<ANCIEN_ID>JG_L_2020_07_000000424146</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/17/56/CETATEXT000042175645.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 29/07/2020, 424146, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-07-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>424146</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; SCP L. POULET-ODENT ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Arno Klarsfeld</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Anne Iljic</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:424146.20200729</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° Sous le n° 424146, M. B... A... a demandé au tribunal administratif de Toulouse d'annuler pour excès de pouvoir l'arrêté du 28 décembre 2016 par lequel le maire de Pinsaguel (Haute-Garonne) a accordé à la SARL " Les Terrains du lac " et à la SA d'habitations à loyer modéré " Colomiers habitat " un permis de construire en vue de l'implantation d'un ensemble immobilier de 69 logements route de Lacroix-Falgarde, ainsi que la décision du 22 mars 2017 par laquelle le maire de Pinsaguel a rejeté son recours gracieux. Par un jugement n° 1702312 du 12 juillet 2018, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 13 septembre et 6 décembre 2018 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge des sociétés " Les Terrains du lac " et " Colomiers habitat " la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Sous le n° 439749, par une requête enregistrée le 24 mars 2020 au secrétariat du contentieux du Conseil d'Etat, M. B... A... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 20 janvier 2020 par lequel le maire de Pinsaguel (Haute-Garonne) a accordé à la SARL " Les Terrains du lac " et à la SA d'habitations à loyer modéré Alteal un permis modifiant le permis de construire accordé le 28 décembre 2016 à la Sarl " Les Terrains du lac " et à la SA d'habitations à loyer modéré " Colomiers Habitat " ;<br/>
<br/>
              2°) de mettre à la charge de la commune de Pinsaguel la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 62 ;<br/>
              - le code de l'urbanisme ;<br/>
              - la loi n° 2017-86 du 27 janvier 2017 ;<br/>
              - la loi n° 2018-1021 du 23 novembre 2018 ;<br/>
              - la décision du Conseil constitutionnel n° 2019-777 QPC du 19 avril 2019 statuant sur la question prioritaire de constitutionnalité soulevée par M. A... ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arno Klarsfeld, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Anne Iljic, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de M. B... A..., à la SCP Piwnica, Molinié, avocat de la commune de Pinsaguel, et à la SCP L. Poulet, Odent, avocat de la SARL " Les terrains du lac " et de la SA HLM " Colomiers habitat " ; <br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 600-5-2 du code de l'urbanisme issu de la loi du 23 novembre 2018 portant évolution du logement, de l'aménagement et du numérique : " Lorsqu'un permis modificatif, une décision modificative ou une mesure de régularisation intervient au cours d'une instance portant sur un recours dirigé contre le permis de construire, de démolir ou d'aménager initialement délivré ou contre la décision de non-opposition à déclaration préalable initialement obtenue et que ce permis modificatif, cette décision modificative ou cette mesure de régularisation ont été communiqués aux parties à cette instance, la légalité de cet acte ne peut être contestée par les parties que dans le cadre de cette même instance ". <br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que, par un jugement du 12 juillet 2018, le tribunal administratif de Toulouse, faisant application de l'article L. 600-13 du code de l'urbanisme dans sa version issue de la loi du 27 janvier 2017 relative à l'égalité et à la citoyenneté, a déclaré caduque la requête de M. A... tendant à l'annulation pour excès de pouvoir de l'arrêté du 28 décembre 2016 par lequel le maire de Pinsaguel (Haute-Garonne) a accordé à la SARL " Les Terrains du lac " et à la SA d'habitations à loyer modéré " Colomiers habitat " un permis de construire en vue de l'implantation d'un ensemble immobilier et de la décision du 22 mars 2017 par laquelle il a rejeté son recours gracieux. Sous le n° 424146, M. A... se pourvoit en cassation contre ce jugement. <br/>
<br/>
              3. La demande enregistrée sous le n° 439749 tendant à l'annulation pour excès de pouvoir de l'arrêté du 20 janvier 2020 du maire de Pinsaguel modifiant le permis de construire du 28 décembre 2016 constitue en réalité un mémoire présenté, en application de l'article L. 600-5-2 du code de l'urbanisme, dans l'hypothèse du règlement au fond de l'affaire enregistrée sous le n°424146. Par suite, il y a lieu de rayer la demande n° 439749 des registres du secrétariat du contentieux du Conseil d'Etat et d'enregistrer sous le n° 424146 les productions enregistrées sous le n° 439749.<br/>
<br/>
              4. Aux termes de l'article L. 600-13 du code de l'urbanisme issu de la loi du 27 janvier 2017 relative à l'égalité et à la citoyenneté et abrogé par la loi du 23 novembre 2018 portant évolution du logement, de l'aménagement et du numérique : " La requête introductive d'instance est caduque lorsque, sans motif légitime, le demandeur ne produit pas les pièces nécessaires au jugement de l'affaire dans un délai de trois mois à compter du dépôt de la requête ou dans le délai qui lui a été imparti par le juge. / La déclaration de caducité peut être rapportée si le demandeur fait connaître au greffe, dans un délai de quinze jours, le motif légitime qu'il n'a pas été en mesure d'invoquer en temps utile ".<br/>
<br/>
              5. Le Conseil constitutionnel, par sa décision n° 2019-777 du 19 avril 2019, a déclaré les dispositions de l'article L. 600-13 du code de l'urbanisme contraires à la Constitution. Il résulte des termes du point 12 de cette décision que cette déclaration d'inconstitutionnalité est applicable à toutes les affaires non jugées définitivement à la date de sa publication.<br/>
<br/>
              6. L'instance engagée par M. A... n'étant pas définitivement jugée à la date de la décision du Conseil constitutionnel, celui-ci peut se prévaloir dans la présente instance, y compris devant le Conseil d'Etat, juge de cassation, de la déclaration d'inconstitutionnalité des dispositions de l'article L. 600-13 du code de l'urbanisme sur lesquelles est fondé le jugement qu'il attaque. Le moyen tiré de ce que le tribunal administratif de Toulouse a commis une erreur de droit en faisant application de ces dispositions est fondé. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, que M. A... est fondé à demander l'annulation du jugement du 12 juillet 2018 du tribunal administratif de Toulouse.<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la SARL " Les Terrains du lac " et de la SA d'habitations à loyer modéré Altéal venant aux droits de la SA d'habitations à loyer modéré " Colomiers habitat " la somme de 3 000 euros à verser à M. A... au titre de l'article L. 761-1 du code de justice administrative. Ces dispositions font obstacle à ce qu'une somme soit mise à la charge de M. A..., qui n'est pas la partie perdante dans la présente instance.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                         --------------<br/>
<br/>
Article 1er : Les productions enregistrées sous le n° 439749 seront rayées des registres du secrétariat du contentieux du Conseil d'Etat pour être enregistrées sous le n° 424146.<br/>
Article 2 : Le jugement du 12 juillet 2018 du tribunal administratif de Toulouse est annulé.<br/>
Article 3 : L'affaire est renvoyée au tribunal administratif de Toulouse.<br/>
Article 4 : La SARL " Les Terrains du lac " et la SA d'habitations à loyer modéré Altéal verseront la somme de 3 000 euros à M. A... au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : Les conclusions présentées par la SARL " Les Terrains du lac " et la SA d'habitations à loyers modérés Altéal ainsi que par la commune de Pinsaguel au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 6 : La présente décision sera notifiée à M. B... A..., à la SARL " Les terrains du lac ", à la SA d'habitations à loyer modéré Altéal et à la commune de Pinsaguel.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
