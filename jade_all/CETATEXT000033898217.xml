<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033898217</ID>
<ANCIEN_ID>JG_L_2017_01_000000405990</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/89/82/CETATEXT000033898217.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 05/01/2017, 405990, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-01-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>405990</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2017:405990.20170105</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 15 décembre 2016 et 3 janvier 2017 au secrétariat du contentieux du Conseil d'Etat, la société Aéroport du Golfe de Saint-Tropez demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution de la décision du gouvernement, révélée par la liste publiée au Journal officiel de l'Union européenne du 29 octobre 2016, d'exclure l'aéroport de La Môle - Saint-Tropez de la liste des points de passage frontaliers visés à l'article 2, paragraphe 8, du règlement UE n° 2016/399 du Parlement européen et du Conseil du 9 mars 2016 ; <br/>
<br/>
              2°) d'enjoindre au gouvernement, sur le fondement des articles L. 911-1 et L. 911-3 du code de justice administrative, de notifier à la Commission européenne une nouvelle liste en application de l'article 5, paragraphe 1 du règlement UE n° 2016/399 du Parlement européen et du Conseil du 9 mars 2016 dans laquelle figurera l'aéroport de La Môle, dans les sept jours qui suivront la notification de l'ordonnance de référé et sous astreinte de 1 000 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              La société requérante soutient que : <br/>
              - la condition d'urgence est remplie dès lors que la déqualification de l'aéroport, qui a pris effet le 30 octobre 2016, porte atteinte à sa situation sociale et économique en ce qu'elle lui inflige une perte de 31% de son chiffre d'affaires et remet en cause ses projets de développement de lignes régulières internationales, ainsi qu'à l'intérêt public qui s'attache au développement et à l'économie touristique du territoire ;<br/>
              - il existe un doute sérieux quant à la légalité de la décision contestée ;<br/>
              - elle est entachée d'incompétence ;<br/>
              - elle est entachée d'une erreur de droit en ce qu'elle méconnaît les dispositions de l'arrêté du 20 avril 1998 portant ouverture des aérodromes au trafic aérien international ;<br/>
              - elle porte une atteinte injustifiée et disproportionnée à la liberté de circulation aérienne en ce qu'elle fait obstacle à la réalisation de près de 300 mouvements annuels en provenance ou à destination de pays qui se situent hors de l'espace Schengen et qu'aucune économie substantielle n'est susceptible de résulter de la décision contestée ;<br/>
              - d'application immédiate, elle méconnaît le principe de sécurité juridique tandis que la société requérante n'a pas été informée, préalablement à la publication de la liste des points de passage frontaliers, de ce que l'aéroport qu'elle exploite ne figurerait plus sur cette liste ;<br/>
<br/>
              Par un mémoire en intervention enregistré le 22 décembre 2016, la région Provence-Alpes-Côte d'Azur demande au Conseil d'Etat de faire droit aux conclusions de la requête. Elle fait siens les moyens présentés par la société Aéroport du Golfe de Saint-Tropez ;<br/>
<br/>
              Par un mémoire en défense, enregistré le 3 janvier 2017, le ministre de l'intérieur conclut au rejet de la requête. Il soutient que la condition d'urgence n'est pas remplie et que les moyens soulevés par la société requérante ne sont pas fondés. <br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, la société Aéroport du Golfe de Saint-Tropez, d'autre part, le ministre de l'intérieur ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du mardi 3 janvier 2017 à 16 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Piwnica, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la société Aéroport du Golfe de Saint-Tropez ;<br/>
<br/>
- la représentante du ministre de l'intérieur ;<br/>
                          - la représentante de la région Provence-Alpes-Côte d'Azur ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a différé la clôture de l'instruction au 5 janvier 2017 à 12 heures ;<br/>
<br/>
              Vu les mémoires, enregistrés le 5 janvier 2017, présentés par le ministre de l'intérieur ; <br/>
<br/>
              Vu le mémoire, enregistré le 5 janvier 2017, présenté par la société Aéroport du Golfe de Saint-Tropez ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
              Vu : <br/>
              - le règlement UE n° 2016/399 du Parlement européen et du Conseil du 9 mars 2016 ;<br/>
              - l'arrêté du 20 avril 1998 portant ouverture des aérodromes au trafic aérien international ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". <br/>
<br/>
              2. Il résulte de ces dispositions que le prononcé de la suspension d'un acte administratif est subordonné notamment à une condition d'urgence. L'urgence justifie la suspension de l'exécution d'un acte administratif lorsque celui-ci porte atteinte de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre. Il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte contesté sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue. L'urgence doit être appréciée objectivement et compte tenu de l'ensemble des circonstances de l'affaire.<br/>
<br/>
              3. Aux termes de l'article 2, paragraphe 8, du règlement (UE) 2016/399 du Parlement européen et du Conseil du 9 mars 2016 : " Aux fins du présent règlement, on entend par : 8) " point de passage frontalier" : tout point de passage autorisé par les autorités compétentes pour le franchissement des frontière extérieures ". Aux termes de l'article 5, paragraphe 1, du même règlement : " Les frontières extérieures ne peuvent être franchies qu'aux points de passage frontaliers et durant les heures d'ouverture fixées. Les heures d'ouverture sont indiquées clairement aux points de passage frontaliers qui ne sont pas ouverts 24 heures sur 24. Les États membres notifient la liste de leurs points de passage frontaliers à la Commission conformément à l'article 39. ". Enfin, aux termes de l'article 39 du règlement précité : " Les États membres communiquent à la Commission [...] b) la liste de leurs points de passage frontaliers [...]. La Commission rend les informations notifiées conformément au paragraphe 1 accessibles aux États membres et au public par le biais d'une publication au Journal officiel de l'Union européenne, série C, et par tout autre moyen approprié. ".<br/>
<br/>
              4. La liste des points de passage frontaliers, visés à l'article 2, paragraphe 8, du règlement (UE) n° 2016/399 du Parlement européen et du Conseil du 9 mars 2016, retenue par le gouvernement français et notifiée à la Commission européenne a été mise à jour et publiée au Journal officiel de l'Union européenne le 29 octobre 2016. Cette liste ne faisant pas mention de l'aéroport de La Môle - Saint-Tropez, la société Aéroport du Golfe de Saint-Tropez, exploitante de l'aéroport, a demandé au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre la décision du gouvernement d'exclure l'aéroport de La Môle - Saint-Tropez de ladite liste des points de passage frontaliers. <br/>
<br/>
              5. Il résulte de l'instruction et des débats à l'audience que l'essentiel du trafic international hors de l'espace Schengen que réalise l'aéroport concerne une part significative du résultat et de l'activité mais se concentre sur un faible nombre de passagers essentiellement durant les mois d'été. Les perspectives d'ouverture d'une liaison permanente avec la Grande Bretagne dont se prévaut la société demeurent.à ce stade hypothétiques Une décision au fond est susceptible d'être prise par une formation collégiale avant l'été. Les vols concernés sont ceux d'avions privés qui ne nécessitent pas, contrairement à des liaisons commerciales régulières, que la possibilité d'atterrir à Saint-Tropez soit établie dès aujourd'hui. A cet égard, si la société gestionnaire de l'aéroport fait valoir dans une note en délibéré que l'attractivité commerciale de l'aéroport pour la période estivale dépend de la certitude pour ses utilisateurs qu'il sera ouvert au trafic international pendant l'été, rendant urgente que cette ouverture soit d'ores et déjà connue, le gouvernement ne s'oppose pas à ce qu'une ouverture saisonnière permanente au trafic international soit mise en place, que la société requérante pourrait demander d'instaurer dès maintenant si, comme elle le soutient, ses installations répondent aux exigences d'un tel fonctionnement. Aucune urgence ne s'attache dans ces conditions à la suspension demandée par la société requérante.<br/>
<br/>
              6. Les conclusions de suspension de la société ne peuvent donc qu'être rejetées, ainsi que, par voie de conséquence, ses conclusions d'exécution et celles tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative, ses dispositions faisant obstacle à ce que l'Etat, qui n'est pas la partie perdante, verse à la société la somme qu'elle demande sur leur fondement.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : L'intervention de la région Provence-Alpes-Côte d'Azur est admise. <br/>
Article 2 : La requête de la société Aéroport du Golfe de Saint-Tropez est rejetée. <br/>
Article 3 : La présente ordonnance sera notifiée à la société Aéroport du Golfe de Saint-Tropez, au ministre de l'intérieur et à la région Provence-Alpes-Côte d'Azur. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
