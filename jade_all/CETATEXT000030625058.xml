<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030625058</ID>
<ANCIEN_ID>JG_L_2015_05_000000371623</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/62/50/CETATEXT000030625058.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème / 2ème SSR, 22/05/2015, 371623</TITRE>
<DATE_DEC>2015-05-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>371623</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème / 2ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO ; SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>M. Frédéric Dieu</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:371623.20150522</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Cergy-Pontoise d'annuler la décision du 27 juillet 2009 par laquelle le ministre du travail, des relations sociales, de la famille, de la solidarité et de la ville a autorisé la société Air France à rompre son contrat de travail.<br/>
<br/>
              Par un jugement n° 0909066 du 5 décembre 2011, le tribunal administratif de Cergy-Pontoise a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 12VE00522 du 25 juin 2013, la cour administrative d'appel de Versailles, sur l'appel de M.A..., a annulé ce jugement ainsi que la décision du 27 juillet 2009.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 26 août et 25 novembre 2013 et le 24 octobre 2014 au secrétariat du contentieux du Conseil d'Etat, la société Air France demande au Conseil d'Etat d'annuler cet arrêt. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la directive 2000/78/CE du 27 novembre 2000 ; <br/>
              - le code de l'aviation civile ; <br/>
              - le code du travail ; <br/>
              - la loi n° 95-116 du 4 février 1995 ;<br/>
              - la loi n° 2008-1330 du 17 décembre 2008 ;<br/>
              - les arrêts C-229/08 du 12 janvier 2010 et C-447/09 du 13 septembre 2011 de la Cour de justice de l'Union européenne ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Dieu, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Le Prado, avocat de la société Air France, et à la SCP Didier, Pinet, avocat de M. A...;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 6 mai 2015, présentée pour la société Air France ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que M. B...A..., né le 23 mai 1949, exerçait les fonctions de pilote de ligne au sein de la société Air France et avait la qualité de délégué syndical ; qu'ayant atteint l'âge de soixante ans, il n'était plus autorisé à poursuivre son activité de pilote en application des dispositions de l'article L. 421-9 du code de l'aviation civile alors en vigueur ; qu'à défaut d'avoir pu le reclasser dans un emploi au sol, la société Air France a demandé à l'inspection du travail l'autorisation de rompre son contrat de travail ; que cette autorisation lui a été refusée par une décision du 2 mars 2009 ; que, toutefois, par une décision du 27 juillet 2009, le ministre chargé du travail a annulé la décision de l'inspecteur du travail et autorisé la rupture du contrat de travail de M. A...; que, par l'arrêt attaqué, la cour administrative d'appel de Versailles a annulé le jugement du tribunal administratif de Cergy-Pontoise ayant rejeté la demande de M. A...tendant à l'annulation de cette décision ainsi que cette décision elle-même ; <br/>
<br/>
              Sur le droit applicable : <br/>
<br/>
              2. Considérant qu'aux termes de l'article 91 de la loi du 17 décembre 2008 : " I. - L'article L. 421-9 du code de l'aviation civile est ainsi modifié : 1° La première phrase est précédée de la mention : " I. - " ; 2° Il est ajouté un II ainsi rédigé : " II. - Le personnel navigant de la section A du registre qui remplit les conditions nécessaires à la poursuite de son activité de navigant est toutefois maintenu en activité au-delà de soixante ans pour une année supplémentaire sur demande formulée au plus tard trois mois avant son soixantième anniversaire, uniquement dans le cas des vols en équipage avec plus d'un pilote, à la condition qu'un seul des pilotes soit âgé de plus de soixante ans. Cette demande peut être renouvelée dans les mêmes conditions les quatre années suivantes. / Le personnel navigant de la section A du registre peut de droit et à tout moment, à partir de soixante ans, demander à bénéficier d'un reclassement dans un emploi au sol. / Lorsqu'il ne demande pas à poursuivre son activité de navigant ou atteint l'âge de soixante-cinq ans, le contrat n'est pas rompu de ce seul fait, sauf impossibilité pour l'entreprise de proposer un reclassement dans un emploi au sol ou refus de l'intéressé d'accepter l'emploi qui lui est proposé. " / II. - Le II de l'article L. 421-9 du code de l'aviation civile entre en vigueur à compter du 1er janvier 2010. Les textes réglementaires relatifs à l'aptitude physique et mentale du personnel navigant technique professionnel de l'aéronautique civile seront adaptés, après consultation des organisations syndicales représentatives des personnels navigants techniques, pour tenir compte de ces nouvelles dispositions. Jusqu'au 1er janvier 2010, le contrat de travail du personnel navigant de la section A n'est pas rompu du seul fait que la limite d'âge de soixante ans est atteinte, sauf impossibilité pour l'entreprise de proposer un reclassement dans un emploi au sol ou refus de l'intéressé d'accepter l'emploi qui lui est proposé. " ; qu'il résulte de ces dispositions que l'article L. 421-9 du code de l'aviation civile, dans sa rédaction antérieure à celle issue du I de l'article 91 de la loi du 17 décembre 2008, est resté en vigueur jusqu'au 1er janvier 2010 ; <br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 421-9 du code de l'aviation civile dans cette rédaction applicable à la date de la décision attaquée, issue de l'article 90 de la loi du 4 février 1995 portant diverses dispositions d'ordre social : " Le personnel navigant de l'aéronautique civile de la section A du registre prévu à l'article L. 421-3 ne peut exercer aucune activité en qualité de pilote ou de copilote dans le transport aérien public au-delà de l'âge de soixante ans. Le personnel navigant de l'aéronautique civile de la section D du registre prévu au même article ne peut exercer aucune activité en qualité de personnel de cabine dans le transport aérien public au-delà de l'âge de soixante ans. (...) Toutefois, le contrat de travail du navigant n'est pas rompu du seul fait que cette limite d'âge est atteinte sauf impossibilité pour l'entreprise de proposer un reclassement dans un emploi au sol ou refus de l'intéressé d'accepter l'emploi qui lui est offert. " ;<br/>
<br/>
              4. Considérant que la directive 2000/78/CE du Conseil du 27 novembre 2000 portant création d'un cadre général en faveur de l'égalité de traitement en matière d'emploi et de travail a pour objet, en vertu de ses articles 1 et 2, de proscrire les discriminations professionnelles directes et indirectes, y compris les discriminations fondées sur l'âge ; que toutefois, aux termes du paragraphe 5 de son article 2 : " (...) la présente directive ne porte pas atteinte aux mesures prévues par la législation nationale qui, dans une société démocratique, sont nécessaires à la sécurité publique, à la défense de l'ordre et à la prévention des infractions pénales, à la protection de la santé et à la protection des droits et libertés d'autrui " ; qu'aux termes du paragraphe 1 de l'article 6 de la même directive : " (...) les Etats membres peuvent prévoir que des différences de traitement fondées sur l'âge ne constituent pas une discrimination lorsqu'elles sont objectivement et raisonnablement justifiées, dans le cadre du droit national, par un objectif légitime, notamment par des objectifs légitimes de politique de l'emploi, du marché du travail et de la formation professionnelle, et que les moyens de réaliser cet objectif sont appropriés et nécessaires (...) " ; <br/>
<br/>
              5. Considérant qu'une limite d'âge inférieure au droit commun constitue une différence de traitement selon l'âge affectant les conditions d'emploi et de travail au sens des dispositions précitées des articles 1 et 2 de la directive 2000/78 CE du Conseil du 27 novembre 2000, ainsi que l'a notamment jugé la Cour de justice de l'Union européenne par son arrêt du 12 janvier 2010 (aff. C-229/08) ; qu'une telle mesure peut cependant être justifiée si elle est nécessaire, aux termes du paragraphe 5 de l'article 2 de la directive, notamment à la sécurité publique ou si, en vertu du paragraphe 1 de l'article 6 de la directive, elle est objectivement et raisonnablement justifiée par des objectifs légitimes de politique sociale ou de l'emploi et constitue un moyen approprié et nécessaire pour atteindre ces objectifs ; <br/>
<br/>
              Sur le litige :<br/>
<br/>
              6. Considérant, en premier lieu, qu'aucune des pièces soumises au juge du fond ne permettait de justifier une interdiction totale de pilotage à compter de soixante ans au motif tiré de considérations de sécurité publique ; qu'il ressort en particulier des pièces du dossier soumis au juge du fond que l'Organisation de l'aviation civile, dans ses recommandations encadrant les conditions dans lesquelles les pilotes de plus de soixante ans pouvaient continuer à exercer jusqu'à l'âge de soixante-cinq ans, n'excluait pas tout pilotage au-delà de l'âge de soixante ans en l'absence de considérations de sécurité publique susceptibles de fonder une telle limitation ; que dans ces conditions, la cour n'a pas commis d'erreur de droit en jugeant que les dispositions de l'article L. 421-9 du code de l'aviation civile alors en vigueur, posant le principe de la limite d'âge de soixante ans pour les pilotes, n'étaient pas nécessaires à la sécurité publique au sens des dispositions du paragraphe 5 de l'article 2 de la directive précitée du 27 novembre 2000 et qu'elles étaient par suite incompatibles avec son objectif de non discrimination en fonction de l'âge ;  <br/>
<br/>
              7. Considérant, en deuxième lieu, qu'il ressort également des pièces du dossier soumis au juge du fond qui si la requérante soutenait que la limite d'âge posée par les dispositions alors en vigueur de l'article L. 421-9 du code de l'aviation civile était objectivement et raisonnablement justifiée par un objectif de politique de l'emploi tenant au recrutement de jeunes pilotes à l'issue de leur formation et que, lors de l'adoption de ces dispositions par le Parlement, était envisagé un recrutement, sur une année donnée, de cent trente à cent cinquante jeunes pilotes à l'issue de leur formation, la société Air France n'avait présenté aucun élément justifiant que cette mesure avait un tel effet ; qu'ainsi, en l'absence d'éléments précis permettant de justifier que cette mesure avait permis de répondre effectivement au motif avancé de recrutement de jeunes pilotes à l'issue de leur formation, la cour n'a pas commis d'erreur de droit en jugeant qu'elle n'était pas justifiée au regard des dispositions du paragraphe 1 de l'article 6 de la directive du 27 novembre 2000 et qu'elle était, à ce titre également, incompatible avec son objectif de non discrimination en fonction de l'âge ;  <br/>
<br/>
              8. Considérant, enfin, qu'en écartant, dans le cadre de son office, l'application des dispositions de l'article L. 421-9 du code de l'aviation civile au motif qu'elles n'étaient pas compatibles avec les objectifs de la directive du 27 novembre 2000, la cour n'a pas méconnu le principe de sécurité juridique ;<br/>
<br/>
              9. Considérant qu'il résulte de tout ce qui précède que le pourvoi de la société Air France doit être rejeté ; qu'il y a lieu, en application des dispositions de l'article L. 761-1 du code de justice administrative et dans les circonstances de l'espèce, de mettre à la charge de la société Air France une somme de 3 000 euros à verser à M. A...;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société Air France est rejeté.<br/>
Article 2 : La société Air France versera une somme de 3 000 euros à M. A...en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société Air France, à M. B...A..., au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social et à la ministre de l'écologie, du développement durable et de l'énergie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">15-05-002 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. RÈGLES APPLICABLES. - DIRECTIVE 2000/78/CE - DISCRIMINATION EN FONCTION DE L'ÂGE - EXISTENCE - LIMITE D'ÂGE DES PILOTES À 60 ANS (L. 421-9 DU CODE DE L'AVIATION CIVILE) [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">65-03-01-01-02 TRANSPORTS. TRANSPORTS AÉRIENS. PERSONNELS. PERSONNELS DES COMPAGNIES AÉRIENNES. PILOTES. - LIMITE D'ÂGE DES PILOTES À 60 ANS (L. 421-9 DU CODE DE L'AVIATION CIVILE) - DISCRIMINATION EN FONCTION DE L'ÂGE PROHIBÉE PAR LA DIRECTIVE 2000/78/CE - EXISTENCE [RJ1].
</SCT>
<ANA ID="9A"> 15-05-002 Directive 2000/78/CE, du 27 novembre 2000, portant création d'un cadre général en faveur de l'égalité de traitement en matière d'emploi et de travail.... ,,Dès lors qu'aucune pièce du dossier ne permet de justifier une interdiction totale de pilotage à compter de soixante ans au motif tiré de considérations de sécurité publique ou d'objectifs de politique de l'emploi, et qu'au contraire l'Organisation de l'aviation civile, dans ses recommandations encadrant les conditions dans lesquelles les pilotes de plus de soixante ans peuvent continuer à exercer jusqu'à l'âge de soixante-cinq ans, n'exclut pas tout pilotage au-delà de l'âge de soixante ans en l'absence de considérations de sécurité publique, une cour administrative d'appel juge sans erreur de droit que les dispositions de l'article L. 421-9 du code de l'aviation civile posant le principe de la limite d'âge de soixante ans pour les pilotes ne sont pas nécessaires à la sécurité publique au sens des dispositions du paragraphe 5 de l'article 2 de la directive du 27 novembre 2000 et sont, par suite, incompatibles avec son objectif de non discrimination en fonction de l'âge.</ANA>
<ANA ID="9B"> 65-03-01-01-02 Directive 2000/78/CE, du 27 novembre 2000, portant création d'un cadre général en faveur de l'égalité de traitement en matière d'emploi et de travail.... ,,Dès lors qu'aucune pièce du dossier ne permet de justifier une interdiction totale de pilotage à compter de soixante ans au motif tiré de considérations de sécurité publique ou d'objectifs de politique de l'emploi, et qu'au contraire l'Organisation de l'aviation civile, dans ses recommandations encadrant les conditions dans lesquelles les pilotes de plus de soixante ans peuvent continuer à exercer jusqu'à l'âge de soixante-cinq ans, n'exclut pas tout pilotage au-delà de l'âge de soixante ans en l'absence de considérations de sécurité publique, une cour administrative d'appel juge sans erreur de droit que les dispositions de l'article L. 421-9 du code de l'aviation civile posant le principe de la limite d'âge de soixante ans pour les pilotes ne sont pas nécessaires à la sécurité publique au sens des dispositions du paragraphe 5 de l'article 2 de la directive du 27 novembre 2000 et sont, par suite, incompatibles avec son objectif de non discrimination en fonction de l'âge.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp. CE, Assemblée, 4 avril 2014, n°s 362785 et autres, Ministre de l'écologie, du développement durable et de l'énergie c/ M. Lambois, p. 63. Cf. CJUE, 13 septembre 2011, Reinhard Prigge et autres contre Deutsche Lufthansa AG, aff. C-447/09 ; Cass. soc., 3 juillet 2012, n° 11-13.795, Bull. 2012 V n° 205.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
