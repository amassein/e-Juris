<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036791231</ID>
<ANCIEN_ID>JG_L_2018_04_000000413302</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/79/12/CETATEXT000036791231.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 11/04/2018, 413302, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-04-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>413302</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>Mme Dominique Chelle </RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:413302.20180411</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. et Mme D...C...ont demandé au tribunal administratif de Cergy-Pontoise de condamner l'Etat à leur verser une indemnité de 40 638,13 euros en réparation des préjudices ayant résulté pour eux, pour la période comprise entre le 28 octobre 2014 et le 31 décembre 2016, du refus du préfet des Hauts-de-Seine de leur accorder le concours de la force publique pour procéder, en exécution d'une décision de justice, à l'expulsion de l'occupante d'un logement leur appartenant situé 2, avenue Louise-de-Bettignies à Colombes. Par un jugement n° 1600922 du 28 juin 2017, le tribunal administratif a condamné l'Etat à leur verser la somme de 3 379,44 euros au titre des pertes de loyers subies du 28 octobre 2014 au 28 février 2015 et rejeté le surplus de leurs conclusions. <br/>
<br/>
              Par un pourvoi et un nouveau mémoire, enregistrés les 11 août et 12 octobre 2017 au secrétariat du contentieux du Conseil d'Etat, M. et Mme C...demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement en tant qu'il ne fait que partiellement droit à leur demande ; <br/>
<br/>
              2°) réglant l'affaire au fond, de condamner l'Etat à leur verser une somme de 29 118, 07 euros, assortie des intérêts et de la capitalisation des intérêts ;    <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu :<br/>
<br/>
              - le code des procédures civiles d'exécution ;<br/>
<br/>
	- le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Dominique Chelle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de M. C...et de Mme A...B...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que M. et Mme C...ont demandé au préfet des Hauts-de-Seine de leur accorder le concours de la force publique en vue de l'exécution d'un jugement du 30 mai 2014 du tribunal d'instance de Colombes ordonnant l'expulsion de l'occupante d'un logement leur appartenant ; qu'ils ont demandé au tribunal administratif de Cergy-Pontoise de condamner l'Etat à réparer les préjudices ayant résulté pour eux du rejet de cette demande par une décision implicite née le 28 octobre 2014 ; que, par un jugement du 28 juin 2017, le tribunal a retenu que la responsabilité de l'Etat était engagée à compter de ce refus et jusqu'au 31 décembre 2016, date du dernier décompte présenté par les requérants, mais s'est borné à mettre à sa charge une indemnité de 3 379, 44 euros correspondant aux pertes de loyers subies entre le 28 octobre 2014 et le 28 février 2015 ; que M. et Mme C...demandent que ce jugement soit annulé en tant qu'il rejette le surplus de leur demande ; <br/>
<br/>
              2. Considérant, en premier lieu, qu'il ressort des pièces du dossier soumis au juge du fond que M. et Mme C...ont demandé au tribunal administratif de condamner l'Etat à les indemniser au titre des frais afférents à l'intervention d'huissiers les 9 septembre, 16 octobre et 14 novembre 2014 et le 17 février 2015 ; que le jugement attaqué doit être annulé en tant qu'il omet de se prononcer sur ces conclusions ; <br/>
<br/>
              3. Considérant, en second lieu, que pour refuser aux requérants toute indemnité au titre des pertes de loyers subies pendant la période comprise entre le 1er mars 2015 et le 31 décembre 2016, le tribunal administratif s'est borné à relever qu'ils ne produisaient " aucune pièce justificative " ; qu'en statuant ainsi, alors qu'il était constant que l'occupation irrégulière du logement avait perduré pendant cette période et que, s'il estimait ne pas disposer de tous les éléments nécessaires pour évaluer le préjudice subi du fait de la perte de revenus locatifs, il lui appartenait de faire usage de ses pouvoirs d'instruction, le juge du fond a entaché son jugement d'une erreur de droit ; qu'il y a lieu, par suite, d'annuler également son jugement en tant qu'il statue sur les conclusions de M. et Mme C...tendant à la réparation de pertes de loyers subies à compter du 1er mars 2015 ; <br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à M. et Mme C...de la somme de 2 500 euros qu'ils demandent au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 28 juin 2017 du tribunal administratif de Cergy-Pontoise est annulé en tant qu'il omet de statuer sur les conclusions de M. et Mme C...relatives à l'indemnisation de frais d'huissier et en tant qu'il statue sur leurs conclusions tendant à la réparation de pertes de loyers subies à compter du 1er mars 2015.<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, au tribunal administratif de Cergy-Pontoise.<br/>
Article 3 : L'Etat versera à M. et Mme C...la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Le surplus des conclusions du pourvoi de M. et Mme C...est rejeté. <br/>
Article 5 : La présente décision sera notifiée à M. et Mme D...C...et au ministre d'Etat, ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
