<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042499879</ID>
<ANCIEN_ID>JG_L_2020_11_000000442577</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/49/98/CETATEXT000042499879.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 04/11/2020, 442577, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-11-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>442577</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Catherine Moreau</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier  Fuchs</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:442577.20201104</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par deux mémoires, enregistrés les 7 août et 29 septembre 2020 au secrétariat du contentieux du Conseil d'État, le Conseil national des greffiers des tribunaux de commerce demande au Conseil d'État, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 et à l'appui de sa requête tendant à l'annulation du décret n° 2020-179 du 28 février 2020 relatif aux tarifs réglementés applicables à certains professionnels du droit et de l'arrêté du même jour fixant les tarifs réglementés des greffiers des tribunaux de commerce, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des articles L. 444-2 et L. 444-7 du code de commerce, dans leur rédaction résultant de l'article 20 de la loi n° 2019-222 du 23 mars 2019 de programmation 2018-2022 et de réforme pour la justice.  <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ; <br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code de commerce ;<br/>
              - la loi n° 2019-222 du 23 mars 2019 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme B... A..., conseillère d'Etat en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Olivier Fuchs, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat du Conseil national des greffiers des tribunaux de commerce ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. Le premier alinéa de l'article L. 444-2 du code de commerce prévoit que les tarifs réglementés applicables aux prestations de certains professionnels du droit, dont les greffiers de tribunal de commerce, prennent en compte les coûts pertinents du service rendu et une rémunération raisonnable, définie sur la base de critères objectifs. Le deuxième alinéa du même article autorise une péréquation des tarifs applicables à l'ensemble des prestations servies. Aux termes du troisième alinéa de l'article L. 444-2 du code de commerce, dans sa rédaction issue de l'article 20 de la loi du 23 mars 2019 de programmation 2018-2022 et de réforme pour la justice : " En application des deux premiers alinéas du présent article, l'arrêté conjoint mentionné au même article L. 444-3 fixe les tarifs sur la base d'un objectif de taux de résultat moyen, dont les modalités de détermination sont définies par un décret en Conseil d'Etat, et dont le montant est estimé globalement pour chaque profession pour l'ensemble des prestations tarifées en application de l'article L. 444-1 ". Aux termes du 1° de l'article L. 444-7 du même code, dans sa rédaction également issue de l'article 20 de la loi du 23 mars 2019 : " Un décret en Conseil d'Etat, pris après avis de l'Autorité de la concurrence, précise les modalités d'application du présent titre, notamment : 1° Les modalités selon lesquelles les coûts pertinents et la rémunération raisonnable, mentionnés au premier alinéa de l'article L. 444-2, sont évalués globalement pour l'ensemble des prestations tarifées en application de l'article L. 444-1 ".<br/>
<br/>
              3. Le Conseil national des greffiers des tribunaux de commerce soutient que le troisième alinéa de l'article L. 444-2 et le 1° de l'article L. 444-7 du code de commerce sont entachés d'incompétence négative dans des conditions affectant la liberté d'entreprendre résultant de l'article 4 de la Déclaration des droits de l'homme et du citoyen de 1789. <br/>
<br/>
              4. Les dispositions critiquées prévoient que le pouvoir réglementaire fixe les tarifs des prestations des professions concernées en prenant en compte les coûts pertinents du service rendu, une rémunération raisonnable et la péréquation entre les tarifs des prestations des professionnels du droit concernés à travers la détermination d'un objectif de taux de résultat moyen pour l'ensemble des prestations servies par chaque profession. Ces dispositions précisent ainsi de manière suffisante les conditions dans lesquelles seront fixés les tarifs réglementés des professionnels du droit concernés et pouvaient, dès lors, sans être entachées d'une incompétence négative de nature à porter atteinte à la liberté d'entreprendre, renvoyer au pouvoir réglementaire l'édiction des modalités de détermination de ce taux de résultat moyen. Par suite, la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux.<br/>
<br/>
              5. Il résulte de ce qui précède qu'il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par Conseil national des greffiers des tribunaux de commerce.<br/>
Article 2 : La présente décision sera notifiée au Conseil national des greffiers des tribunaux de commerce, au Premier ministre, au garde des sceaux, ministre de la justice, au ministre de l'économie, des finances et de la relance et au ministre des outre-mer.<br/>
Copie en sera adressée au Conseil constitutionnel.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
