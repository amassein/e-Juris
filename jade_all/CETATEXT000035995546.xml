<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035995546</ID>
<ANCIEN_ID>JG_L_2017_11_000000409207</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/99/55/CETATEXT000035995546.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 09/11/2017, 409207, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-11-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>409207</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Yves Doutriaux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:409207.20171109</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 24 mars 2017 au secrétariat du contentieux du Conseil d'Etat, M. B...A...demande au Conseil d'Etat d'annuler pour excès de pouvoir le décret du 25 novembre 2016 rapportant le décret du 27 mai 2014 qui lui avait accordé la nationalité française.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code civil ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yves Doutriaux, conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'aux termes de l'article 27-2 du code civil : " Les décrets portant acquisition, naturalisation ou réintégration peuvent être rapportés sur avis conforme du Conseil d'Etat dans le délai de deux ans à compter de leur publication au Journal officiel si le requérant ne satisfait pas aux conditions légales ; si la décision a été obtenue par mensonge ou fraude, ces décrets peuvent être rapportés dans le délai de deux ans à partir de la découverte de la fraude " ; <br/>
<br/>
              2.	Considérant qu'il ressort des pièces du dossier que M.A..., ressortissant algérien, a déposé une demande de naturalisation le 10 juin 2010 dans laquelle il a indiqué comme conjoint MmeC..., de nationalité française résidant en France ; qu'au vu de ces déclarations, il a été naturalisé par décret du 27 mai 2014 ; que, par une lettre du 22 novembre 2014, il a toutefois signalé au ministre de l'intérieur qu'il s'était marié en Algérie le 24 août 2008 avec une ressortissante algérienne résidant habituellement en Algérie avec laquelle il avait eu deux enfants ; que, par le décret attaqué, le Premier ministre a rapporté le décret du 27 mai 2014 prononçant la naturalisation de M. A...au motif qu'il avait été pris au vu d'informations mensongères délivrées par l'intéressé quant à sa situation familiale ;<br/>
<br/>
              3.	Considérant qu'aux termes de l'article 21-6 du code civil : " Nul ne peut être naturalisé s'il n'a en France sa résidence au moment de la signature du décret de naturalisation " ; qu'il résulte de ces dispositions que la demande de naturalisation n'est pas recevable lorsque l'intéressé n'a pas fixé en France de manière durable le centre de ses intérêts ; que, pour apprécier si cette condition se trouve remplie, l'autorité administrative peut notamment prendre en compte, sous le contrôle du juge de l'excès de pouvoir, la situation familiale en France de l'intéressé ; que, par suite, la circonstance que l'intéressé ait dissimulé s'être marié en Algérie était de nature à modifier l'appréciation qui a été portée par l'autorité administrative sur la fixation du centre de ses intérêts ; <br/>
<br/>
              4.	Considérant qu'il ressort des pièces du dossier que, dans la demande visant à l'obtention de la nationalité française qu'il a déposée le 10 juin 2010, M. A...a indiqué être divorcé depuis 2007 d'une ressortissante algérienne résidant en Algérie et avoir désormais pour conjoint une ressortissante française résidant en France ; qu'il n'a pas mentionné qu'il était marié, depuis le 24 août 2008, avec une autre ressortissante algérienne résidant en Algérie ; que de cette union sont nés deux enfants en 2010 et 2012 qu'il n'a pas portés à la connaissance de l'administration chargée de l'instruction de sa demande de naturalisation ; qu'au cours de l'entretien d'assimilation qu'il a eu le 4 janvier 2011, il a déclaré que les seuls liens familiaux l'attachant à l'Algérie étaient sa mère et sa fratrie ; qu'il n'a pas davantage mentionné son mariage et la naissance de ses enfants dans la lettre qu'il a adressée le 27 novembre 2012 au préfet des Alpes-Maritimes pour demander le réexamen de sa demande de naturalisation ; <br/>
<br/>
              5.	Considérant qu'il ressort des pièces du dossier que l'intéressé maîtrise la langue française, comme en atteste le procès-verbal d'assimilation établi le 4  janvier 2011, et ne pouvait se méprendre sur la portée de la déclaration sur l'honneur qu'il a signée le 10 juin 2010 en présentant sa demande de naturalisation ; qu'il doit être regardé comme ayant sciemment dissimulé la réalité de sa situation familiale ; que, par suite, en rapportant le décret prononçant sa naturalisation, le Premier ministre n'a pas fait une inexacte application des dispositions de l'article 27-2 du code civil ;<br/>
<br/>
              6.	Considérant qu'il résulte de ce qui précède que M. A...n'est pas fondé à demander l'annulation pour excès de pouvoir du décret qu'il attaque ;  <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de M. A...est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. B...A...et au ministre d'Etat, ministre de l'intérieur.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
