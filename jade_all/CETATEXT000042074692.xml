<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042074692</ID>
<ANCIEN_ID>JG_L_2020_07_000000423600</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/07/46/CETATEXT000042074692.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 01/07/2020, 423600</TITRE>
<DATE_DEC>2020-07-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>423600</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BAUER-VIOLAS, FESCHOTTE-DESBOIS, SEBAGH ; SCP SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR>M. Damien Pons</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie Sirinelli</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:423600.20200701</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° Mme A... D... a demandé au tribunal administratif de Grenoble de condamner le département de la Drôme à lui verser une indemnité de 445 645,84 euros, assortie des intérêts au taux légal et de leur capitalisation, en réparation des préjudices qu'elle estime avoir subis en raison de l'illégalité fautive des décisions par lesquelles le département de la Drôme a mis fin à l'accueil des deux enfants qui lui avaient été confiés puis l'a licenciée de son emploi d'assistante familiale. Par un jugement n° 1503095 du 22 mars 2016, le tribunal administratif de Grenoble a condamné le département de la Drôme à lui verser une indemnité de 4 000 euros intérêts compris au titre du préjudice moral résultant du retrait des enfants qu'elle accueillait et rejeté le surplus des conclusions de sa demande.<br/>
<br/>
              Par un arrêt n° 16LY01553 du 26 juin 2018, la cour administrative d'appel de Lyon a fait partiellement droit à l'appel de Mme D... contre ce jugement et a condamné le département de la Drôme à lui payer une indemnité de 26 217 euros, assortie des intérêts au taux légal et de la capitalisation des intérêts, réformé en ce sens le jugement du tribunal administratif de Grenoble et rejeté le surplus des conclusions de l'appel de Mme D... et l'appel incident du département.<br/>
<br/>
              Sous le n° 423600, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 27 août et 27 novembre 2018 au secrétariat du contentieux du Conseil d'Etat, le département de la Drôme demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt n° 16LY01553 de la cour administrative d'appel de Lyon ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de Mme D... et de faire droit à son appel incident ;<br/>
<br/>
              3°) de mettre la somme de 3 000 euros à la charge de Mme D... au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Mme D... a demandé au tribunal administratif de Grenoble d'annuler pour excès de pouvoir la décision du 27 mars 2014 par laquelle le président du conseil général de la Drôme l'a licenciée de son emploi d'assistante familiale. Par un jugement n° 1403309 du 22 mars 2016, le tribunal administratif de Grenoble a annulé cette décision. <br/>
<br/>
              Par un arrêt n° 16LY01539 du 26 juin 2018, la cour administrative d'appel de Lyon a rejeté l'appel formé par le département de la Drôme contre ce jugement. <br/>
<br/>
              Sous le n° 423603, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 27 août et 27 novembre 2018 au secrétariat du contentieux du Conseil d'Etat, le département de la Drôme demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt n° 16LY01539 de la cour administrative d'appel de Lyon ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre la somme de 3 000 euros à la charge de Mme D... sur le fondement de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Damien Pons, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Marie Sirinelli, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Bauer-Violas, Feschotte-Desbois, Sebagh, avocat du département de la Drôme et à la SCP Sevaux, Mathonnet, avocat de Mme D... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces des dossiers soumis aux juges du fond que, par une décision du 2 juillet 2012, le président du conseil général de la Drôme a retiré à Mme D..., assistante familiale employée par le département depuis le 30 août 2002, les deux enfants qui lui étaient confiés, puis, par une décision du 27 mars 2014, a procédé à son licenciement. Par deux jugements du 22 mars 2016, le tribunal administratif de Grenoble a annulé pour excès de pouvoir la décision du 27 mars 2014 et a condamné le département à verser à Mme D... une indemnité de 4 000 euros, intérêts compris, au titre du préjudice moral résultant du retrait des deux enfants qu'elle accueillait. Par deux arrêts du 26 juin 2018, la cour administrative d'appel de Lyon a rejeté l'appel formé par le département de la Drôme contre le premier de ces jugements et a partiellement fait droit à l'appel de Mme D... contre le second en portant à 26 217 euros, assortis des intérêts au taux légal et de leur capitalisation, la somme que le département de la Drôme devait lui verser, dont 5 000 euros au titre du préjudice moral résultant du retrait des deux enfants qu'elle accueillait et 21 217 euros au titre du préjudice matériel résultant de son licenciement. Par deux pourvois qu'il y a lieu de joindre pour statuer par une même décision, le département de la Drôme demande l'annulation de ces deux arrêts. <br/>
<br/>
              Sur l'arrêt statuant sur la légalité de la décision de licenciement du 27 mars 2014 :<br/>
<br/>
              2. D'une part, aux termes de l'article L. 421-3 du code de l'action sociale et des familles, dans sa rédaction applicable à la date de la décision attaquée : " L'agrément nécessaire pour exercer la profession d'assistant maternel ou d'assistant familial est délivré par le président du conseil général du département où le demandeur réside. / Un référentiel approuvé par décret en Conseil d'Etat fixe les critères d'agrément (...) ". Aux termes de l'article L. 421-6 du même code, dans sa rédaction applicable à la même date : " (...) Si les conditions de l'agrément cessent d'être remplies, le président du conseil général peut, après avis d'une commission consultative paritaire départementale, modifier le contenu de l'agrément ou procéder à son retrait. En cas d'urgence, le président du conseil général peut suspendre l'agrément. Tant que l'agrément reste suspendu, aucun enfant ne peut être confié. / Toute décision de retrait de l'agrément, de suspension de l'agrément ou de modification de son contenu doit être dûment motivée et transmise sans délai aux intéressés (...) ".<br/>
<br/>
              3. D'autre part, aux termes de l'article L. 423-32 de ce code, applicable aux assistants familiaux employés par des personnes morales de droit public conformément à son article L. 422-1 : " L'employeur qui n'a pas d'enfant à confier à un assistant familial pendant une durée de quatre mois consécutifs est tenu de recommencer à verser la totalité du salaire à l'issue de cette période s'il ne procède pas au licenciement de l'assistant familial fondé sur cette absence d'enfants à lui confier ". Aux termes de l'article L. 423-35 du même code, applicable aux mêmes assistants familiaux en vertu du même article : " Dans le cas prévu à l'article L .423-32, si l'employeur décide de procéder au licenciement, il convoque l'assistant familial par lettre recommandée avec demande d'avis de réception et le reçoit en entretien dans les conditions prévues aux articles L. 1232-2 à L. 1232-4 du code du travail. La lettre de licenciement ne peut être expédiée moins d'un jour franc après la date pour laquelle le salarié a été convoqué à l'entretien. L'employeur doit indiquer à l'assistant familial, au cours de l'entretien et dans la lettre recommandée, le motif pour lequel il ne lui confie plus d'enfants ". <br/>
<br/>
              4. Les dispositions citées au point 3 permettent à un employeur de droit public de procéder au licenciement d'un assistant familial s'il n'a pas d'enfant à lui confier pendant une durée d'au moins quatre mois consécutifs. Un tel licenciement, qui ne peut être motivé par le fait que l'assistant familial ne remplit plus les conditions de l'agrément, situation régie par d'autres dispositions du code de l'action sociale et des familles, citées au point 2, doit être justifié soit par l'absence de tout enfant à confier à l'assistant familial, soit par la circonstance que le département a été conduit, par une appréciation soumise au contrôle du juge, pour assurer la meilleure prise en charge des enfants, au regard notamment, de leur âge, de leur situation familiale et de leur santé, des conditions définies par l'agrément de l'assistant familial concerné et des disponibilités d'autres assistants familiaux, à ne pas confier d'enfant pendant cette période à l'assistant familial dont le licenciement est envisagé. En revanche, il ne résulte ni de ces dispositions ni d'aucun principe qu'un tel licenciement ne pourrait être légalement motivé que par la circonstance que l'employeur public serait contraint de ne plus confier d'enfant à l'assistant maternel concerné par des raisons d'intérêt général dont il devrait justifier. Par suite, le département de la Drôme est fondé à soutenir que la cour administrative d'appel de Lyon a commis une erreur de droit en se fondant sur l'absence de justification, par le département, de ce qu'il aurait été contraint de ne pas confier d'enfant à Mme D... par des raisons d'intérêt général pour juger illégale la décision de licenciement du 27 mars 2014. Ce moyen suffisant à entraîner l'annulation de l'arrêt attaqué sous le n° 423603, il n'est pas nécessaire de statuer sur l'autre moyen de ce pourvoi.<br/>
<br/>
              Sur l'arrêt statuant sur la responsabilité du département :<br/>
<br/>
              En ce qui concerne le retrait des deux enfants confiés à Mme D... :<br/>
<br/>
              5. En premier lieu, la décision par laquelle le président du conseil général, devenu conseil départemental, procède à son initiative au retrait d'un enfant confié à un assistant familial doit être motivée par les besoins ou l'intérêt de l'enfant. La cour administrative d'appel de Lyon a jugé, au vu de l'ensemble des circonstances qui lui étaient soumises, que si Mme D... n'avait pas, en méconnaissance des dispositions de l'article R. 42138 du code de l'action sociale et des familles, immédiatement informé le département de la modification de sa situation familiale, cette omission n'était pas de nature à justifier le retrait des enfants qui lui avaient été confiés. En statuant ainsi, la cour n'a pas commis d'erreur de droit.<br/>
<br/>
              6. En second lieu, en jugeant que le président du conseil général avait commis une erreur manifeste d'appréciation en décidant le 2 juillet 2012 de retirer du domicile de Mme D..., à compter des 12 et 20 août 2012, l'enfant B..., âgée de douze ans, qui lui avait été confiée en 2004, et l'enfant C..., âgé de vingt et un mois, qui lui avait été confié en avril 2011, au motif que sa nouvelle situation personnelle engendrait de la confusion chez les enfants accueillis, la cour, qui a relevé à titre surabondant que Mme D... n'avait pas été consultée avant cette décision, a porté sur les pièces du dossier qui lui était soumis une appréciation souveraine exempte de dénaturation.<br/>
<br/>
              7. Par suite, le département de la Drôme n'est pas fondé à demander l'annulation de l'arrêt attaqué sous le n° 423600 en tant que, par cet arrêt, la cour juge qu'il a commis une faute de nature à engager sa responsabilité en retirant les enfants confiés à Mme D... et le condamne à verser à celle-ci une somme de 5 000 euros au titre de son préjudice moral.<br/>
<br/>
              En ce qui concerne le licenciement de Mme D... :<br/>
<br/>
              8. La cour s'est fondée sur la circonstance que, par un arrêt du même jour, elle rejetait l'appel du département de la Drôme dirigé contre le jugement annulant la décision du 27 mars 2014, par laquelle le président du conseil général avait prononcé le licenciement de Mme D..., pour juger que cette décision était entachée d'une illégalité fautive, de nature à engager la responsabilité du département. Il résulte de ce qui a été dit au point 4 ci-dessus que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt doit être annulé en tant qu'il juge que le département a commis une faute de nature à engager sa responsabilité en prononçant le licenciement de Mme D... et le condamne à verser à celle-ci une somme de de 21 217 euros au titre de son préjudice matériel.<br/>
<br/>
              Sur les frais liés au litige :<br/>
<br/>
              9. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre une somme à la charge de Mme D... au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces dispositions font en outre obstacle à ce qu'il soit fait droit à la demande de Mme D... présentée au même titre, le département de la Drôme n'étant pas la partie perdante dans la présente instance.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : L'arrêt n° 16LY01539 de la cour administrative d'appel de Lyon du 26 juin 2018 et l'arrêt n° 16LY01553 de cette cour du même jour, en tant qu'il condamne le département de la Drôme à verser à Mme D... la somme de 21 217 euros, assortie des intérêts et de la capitalisation des intérêts, sont annulés.<br/>
Article 2 : Les affaires sont renvoyées à la cour administrative d'appel de Lyon dans la mesure de la cassation prononcée.<br/>
Article 3 : Le surplus des conclusions du département de la Drôme est rejeté.<br/>
Article 4 : Les conclusions de Mme D... présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 5 : La présente décision sera notifiée au département de la Drôme et à Mme A... D....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-02-02-02-01 AIDE SOCIALE. DIFFÉRENTES FORMES D'AIDE SOCIALE. AIDE SOCIALE À L'ENFANCE. PLACEMENT DES MINEURS. PLACEMENT FAMILIAL. - ASSISTANT FAMILIAL - LICENCIEMENT (ART. L. 423-32 ET L. 423-35 DU CASF) - MOTIFS.
</SCT>
<ANA ID="9A"> 04-02-02-02-01 Les articles L. 423-32 et L. 423-35 du code de l'action sociale et des familles (CASF) permettent à un employeur de droit public de procéder au licenciement d'un assistant familial s'il n'a pas d'enfant à lui confier pendant une durée d'au moins quatre mois consécutifs. Un tel licenciement, qui ne peut être motivé par le fait que l'assistant familial ne remplit plus les conditions de l'agrément, situation régie par les articles L. 421-3 et L. 421-6 du même code, doit être justifié soit par l'absence de tout enfant à confier à l'assistant familial, soit par la circonstance que le département a été conduit, par une appréciation soumise au contrôle du juge, pour assurer la meilleure prise en charge des enfants, au regard notamment, de leur âge, de leur situation familiale et de leur santé, des conditions définies par l'agrément de l'assistant familial concerné et des disponibilités d'autres assistants familiaux, à ne pas confier d'enfant pendant cette période à l'assistant familial dont le licenciement est envisagé.... ,,En revanche, il ne résulte ni de ces dispositions ni d'aucun principe qu'un tel licenciement ne pourrait être légalement motivé que par la circonstance que l'employeur public serait contraint de ne plus confier d'enfant à l'assistant maternel concerné par des raisons d'intérêt général dont il devrait justifier.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
