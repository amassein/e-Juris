<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041541107</ID>
<ANCIEN_ID>JG_L_2020_02_000000437575</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/54/11/CETATEXT000041541107.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 03/02/2020, 437575, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-02-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>437575</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:437575.20200203</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme C... A... a demandé au juge des référés du tribunal administratif de Lille, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre au préfet du Nord de procéder à l'enregistrement de sa demande d'asile en procédure normale et de lui délivrer l'attestation correspondante ainsi que le dossier destiné à l'Office français de protection des réfugiés et apatrides, dans un délai de quarante-huit heures à compter de la notification de l'ordonnance à intervenir, sous astreinte de 150 euros par jour de retard. Par une ordonnance n° 1910487 du 13 décembre 2019, le juge des référés du tribunal administratif de Lille a rejeté cette demande.<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés au secrétariat du contentieux du Conseil d'Etat les 13 et 15 janvier 2020, Mme A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) de faire droit à sa demande de première instance ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros à verser à Me B..., son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. <br/>
<br/>
<br/>
<br/>
              Elle soutient que le refus du préfet du Nord de procéder à l'enregistrement de sa demande d'asile en procédure normale porte une atteinte grave et manifestement illégale, d'une part, à son droit à l'asile dès lors qu'elle ne peut être regardée comme s'étant soustraite à l'exécution de la mesure de transfert prononcée à son égard ni par, suite, comme étant en fuite, d'autre part, au droit de mener une vie familiale normale.<br/>
              Par un mémoire en défense, enregistré le 31 janvier 2020, le ministre de l'intérieur conclut à ce qu'il n'y ait lieu de statuer sur la requête. Il soutient que, postérieurement à l'introduction de sa requête, Mme A... a été convoquée par le préfet du Nord pour l'enregistrement de sa demande d'asile en procédure normale le 13 février 2020.<br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, Mme A..., d'autre part, le ministre de l'intérieur ;<br/>
<br/>
              Vu la lettre informant les parties de la radiation de l'affaire du rôle de l'audience publique du 3 février 2020 ;<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales<br/>
              - le règlement (UE) n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
Considérant ce qui suit :<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ".<br/>
<br/>
              2. Il résulte de l'instruction que Mme A..., ressortissante guinéenne née le 1er janvier 1999, a déposé une demande d'asile auprès des services de la préfecture du Nord le 10 janvier 2019. Par un arrêté du 4 mars 2019, le préfet du Nord a décidé de remettre l'intéressée aux autorités espagnoles, responsables de l'examen de sa demande d'asile, à la suite de leur accord de prise en charge. Par un jugement du 17 avril 2019, le tribunal administratif de Lille a rejeté la demande d'annulation pour excès de pouvoir de cet arrêté présentée par l'intéressée. Le 4 novembre 2019, Mme A... s'est présentée auprès des services de la préfecture du Nord en vue de l'enregistrement de sa demande d'asile en procédure normale, en invoquant l'expiration du délai de six mois au cours duquel la France pouvait procéder à son transfert. Cette demande a fait l'objet d'un refus au motif que l'intéressée était en fuite, faute pour elle de s'être présentée à une convocation le 22 juillet 2019 pour l'exécution de son transfert vers l'Espagne. Mme A... relève appel de l'ordonnance du 13 décembre 2019 par laquelle le juge des référés du tribunal administratif de Lille a rejeté sa demande tendant à ce qu'il soit enjoint au préfet du Nord, sur le fondement de l'article L. 521-2 du code de justice administrative, d'enregistrer sa demande d'asile en procédure normale et de lui délivrer l'attestation correspondante ainsi que le dossier destiné à l'Office français de protection des réfugiés et apatrides, dans un délai de quarante-huit heures à compter de la notification de l'ordonnance à intervenir, sous astreinte de 150 euros par jour de retard.<br/>
<br/>
              3. Postérieurement à l'introduction de la requête de Mme A..., le préfet du Nord a convoqué l'intéressée à un rendez-vous fixé le 13 février 2020 afin de procéder à l'enregistrement de sa demande d'asile en procédure normale. Le ministre de l'intérieur est par suite fondé à soutenir que les conclusions de Mme A... tendant aux mêmes fins sont devenues sans objet. Il n'y a, dès lors, plus lieu d'y statuer.<br/>
<br/>
              4. Dans les circonstances de l'espèce, il n'y a pas lieu de faire droit aux conclusions de la requérante présentées au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : Il n'y a pas lieu de statuer sur les conclusions de Mme A... présentées au titre de l'article L. 521-2 du code de justice administrative.<br/>
Article 2 : Les conclusions présentées par Mme A... au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
Article 3 : La présente ordonnance sera notifiée à Mme C... A... et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
