<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038444236</ID>
<ANCIEN_ID>JG_L_2019_05_000000409285</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/44/42/CETATEXT000038444236.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 1ère chambres réunies, 06/05/2019, 409285</TITRE>
<DATE_DEC>2019-05-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>409285</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>Mme Sara-Lou Gerber</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:409285.20190506</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Nancy d'annuler pour excès de pouvoir la décision du 16 décembre 2011 par laquelle l'inspectrice du travail de l'unité territoriale de la Meuse a autorisé la société Miler Gaz à le licencier. Par un jugement n°s 1200288, 1200289, 1200290, 1200291 du 31 décembre 2015, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 16NC00382 du 26 janvier 2017, la cour administrative d'appel de Nancy a, sur appel de M.B..., d'une part, annulé ce jugement et d'autre part, dit qu'il n'y avait pas lieu de statuer sur la demande présentée par M. B...devant le tribunal administratif.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire enregistrés les 27 mars et 27 juin 2017 au secrétariat du contentieux du Conseil d'Etat, la société Barrois Gaz, venant aux droits de la société Miler Gaz, demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de M. B... la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sara-Lou Gerber, auditeur,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de la société Barrois Gaz venant aux droits de la société Miler Gaz ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 16 décembre 2011, l'inspectrice du travail de l'unité territoriale de la Meuse a autorisé la société Miler Gaz à licencier M.B..., salarié protégé. Sur le fondement de cette autorisation, M. B...a été licencié le 19 décembre 2011. Toutefois, par une seconde décision du 6 avril 2012, l'inspectrice du travail a retiré sa décision du 16 décembre 2011 et refusé l'autorisation sollicitée. Saisi par la société Miler Gaz d'un recours hiérarchique contre cette décision du 6 avril 2012, le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social a, par une décision du 8 octobre 2012, d'une part, annulé la décision du 6 avril 2012 pour méconnaissance du caractère contradictoire de la procédure, d'autre part, décidé que, la société Miler Gaz n'étant plus l'employeur de M.B..., sa demande d'autorisation de licenciement ne pouvait qu'être rejetée.<br/>
<br/>
<br/>
              2. Il appartenait au ministre, saisi d'un recours hiérarchique de la société Miler Gaz contre la décision du 6 avril 2012 de l'inspectrice du travail retirant une précédente décision d'autorisation et refusant l'autorisation demandée, de statuer d'abord, ainsi qu'il l'a d'ailleurs fait, sur la légalité de la décision litigieuse en tant qu'elle retirait l'autorisation précédemment délivrée à la société Miler Gaz. Ayant, à la suite de cet examen, prononcé l'annulation de la décision du 6 avril 2012 en tant qu'elle retirait la décision du 19 décembre 2011 autorisant le licenciement, il incombait alors au ministre, puisqu'il rétablissait ainsi cette décision d'autorisation créatrice de droits, d'annuler par voie de conséquence le refus d'autorisation également prononcé par la décision 6 avril 2012 qui lui était déférée. En décidant, au contraire, de rejeter ensuite la demande d'autorisation de licenciement présentée par la société Miler Gaz, le ministre chargé du travail s'est prononcé sur une demande dont il n'était pas saisi et dont il ne pouvait légalement se saisir. Par suite, l'article 2 de sa décision du 8 octobre 2012, par lequel il a rejeté la demande de l'employeur, etait dépourvu de toute portée juridique.<br/>
<br/>
              3. Par le présent pourvoi, la société Barrois Gaz, venant aux droits de la société Miler Gaz, demande l'annulation de l'arrêt du 26 janvier 2017 par lequel la cour administrative d'appel de Nancy a, d'une part, annulé le jugement du 31 décembre 2015 du tribunal administratif de Nancy rejetant la demande d'annulation de la décision du 16 décembre 2011 de l'inspectrice du travail présentée par M. B...et, d'autre part, dit n'y avoir lieu de statuer sur cette demande.<br/>
<br/>
              4. Il ressort des termes mêmes de l'arrêt attaqué que, pour juger que le litige introduit par M. B...contre la décision du 16 décembre 2011 autorisant son licenciement avait perdu son objet, la cour s'est fondée sur ce que la décision du ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social du 8 octobre 2012 avait " implicitement mais nécessairement annulé la décision initiale du 16 décembre 2011 autorisant le licenciement ". Il résulte de ce qui a été dit ci-dessus qu'en statuant ainsi, au lieu de constater que l'article 2 de la décision du 8 octobre 2012 était dépourvu de toute portée juridique, la cour administrative d'appel a commis une erreur de droit.<br/>
<br/>
              5. Il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, la société Barrois Gaz est fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative et de statuer sur la requête par laquelle M. B...demande l'annulation du jugement du 31 décembre 2015 du tribunal administratif de Nancy.<br/>
<br/>
              7. Pour rejeter la demande d'annulation présentée par M. B..., le tribunal administratif s'est fondé sur ce que M. B...n'avait pas, au terme du délai d'un mois qui lui avait été imparti par un précédent jugement avant-dire droit du 24 juin 2014 du même tribunal, justifié de sa diligence à saisir le juge compétent de la question préjudicielle soulevée par ce jugement, qui portait sur le point de savoir si, à la date du 19 décembre 2011, l'activité de la société Miler Gaz avait été ou non transférée à la société Distrinord Gaz.<br/>
<br/>
              8. Eu égard à la question renvoyée par le jugement du 24 juin 2014, M. B... n'est pas fondé à soutenir que l'existence d'une procédure en contestation de son licenciement du 19 décembre 2011, pendante devant le conseil de prud'hommes d'Amiens, était de nature à tenir lieu des diligences requises par le jugement du 24 juin 2014. Par suite, M. B..., ne saurait utilement soutenir, à l'encontre du jugement qu'il attaque, que le jugement rendu par le conseil de prud'hommes serait frappé d'appel. <br/>
<br/>
              9. Il résulte de tout ce qui précède que M. B...n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Nancy a rejeté sa demande.<br/>
<br/>
              10. Les dispositions de l'article L.761-1 du code de justice administrative font obstacle à ce soit mise à la charge de la société Barrois Gaz, qui n'est pas la partie perdante dans la présente instance, la somme que demande à ce titre M.B.... Par ailleurs, il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de M. B...la somme demandée au même titre par la société Barrois Gaz.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 26 janvier 2017 de la cour administrative d'appel de Nancy est annulé.<br/>
Article 2 : La demande présentée par M. B...devant le tribunal administratif de Nancy est rejetée.<br/>
Article 3 : Le surplus des conclusions de la société Barrois Gaz, présenté au titre de l'article L. 761-1 du code de justice administrative, est rejeté.<br/>
Article 3 : La présente décision sera notifiée à la Société Barrois Gaz et à M. A... B....<br/>
Copie en sera adressée à la ministre du travail. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
