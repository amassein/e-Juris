<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028812903</ID>
<ANCIEN_ID>JG_L_2014_03_000000368003</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/81/29/CETATEXT000028812903.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème / 3ème SSR, 31/03/2014, 368003, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-03-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>368003</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème / 3ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>RICARD ; SCP BORE, SALVE DE BRUNETON</AVOCATS>
<RAPPORTEUR>M. Patrick Quinqueton</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:368003.20140331</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 22 avril et 7 mai 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune de Mios, représentée par son maire ; la commune demande au Conseil d'Etat : <br/>
<br/>
               1°) d'annuler l'ordonnance n°1301021 du 4 avril 2013 par laquelle le juge des référés du tribunal administratif de Bordeaux, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, a suspendu, à la demande de Mme C...B...et M. E...D..., l'exécution de la délibération du 31 mai 2012 de son conseil municipal décidant d'incorporer la parcelle AP173 dans le domaine communal ; <br/>
<br/>
              2°) statuant en référé, de rejeter la demande de suspension présentée par Mme B...et M.D... ; <br/>
<br/>
              3°) de mettre à la charge de Mme B...et M. D...une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général de la propriété des personnes publiques ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Patrick Quinqueton, Conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Ricard, avocat de la commune de Mios et à la SCP Boré, Salve de Bruneton, avocat de Mme C...B...et de M. E...D...;<br/>
<br/>
<br/>
<br/>
              1.  Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 1123-1 du code général de la propriété des personnes publiques : " Sont considérés comme n'ayant pas de maître les biens autres que ceux relevant de l'article L. 1122-1 et qui : / 1° Soit font partie d'une succession ouverte depuis plus de trente ans et pour laquelle aucun successible ne s'est présenté ; / 2° Soit sont des immeubles qui n'ont pas de propriétaire connu et pour lesquels depuis plus de trois ans les taxes foncières n'ont pas été acquittées ou ont été acquittées par un tiers. (...) " ; qu'aux termes de l'article L. 1123-3 du même code : " L'acquisition des immeubles mentionnés au 2° de l'article L. 1123-1 est opérée selon les modalités suivantes. / Un arrêté du maire (...) constate que l'immeuble satisfait aux conditions mentionnées au 2° de l'article L. 1123-1. Il est procédé par les soins du maire à une publication et à un affichage de cet arrêté et, s'il y a lieu, à une notification aux derniers domicile et résidence du dernier propriétaire connu. (...) / Dans le cas où un propriétaire ne s'est pas fait connaître dans un délai de six mois à dater de l'accomplissement de la dernière des mesures de publicité mentionnées au deuxième alinéa, l'immeuble est présumé sans maître. La commune dans laquelle est situé ce bien peut, par délibération du conseil municipal, l'incorporer dans le domaine communal. Cette incorporation est constatée par arrêté du maire. (...) " ;<br/>
<br/>
              3 . Considérant que par un arrêté du 23 juin 2011, le maire de la commune de Mios a constaté que la parcelle cadastrée AP173 n'avait pas de propriétaire connu et que les contributions foncières n'avaient pas été acquittées depuis plus de trois ans ; qu'en application de cet arrêté, un avis au public a été affiché afin de rechercher un propriétaire ; que le 21 décembre 2011, Mme B...et M. D...ont signifié à la commune de Mios un acte déclaratif établi par huissier de justice aux termes duquel ils se disaient tous les deux propriétaires ; que par courrier du 22 mars 2012, la commune leur a demandé de fournir l'acte de propriété ; qu'ils ont alors transmis un acte de donation établi le 13 novembre 1903 au profit de M. A...D..., dont les frères sont les grands-parents des requérants ; que par délibération du 31 mai 2012, la commune a toutefois considéré la parcelle comme étant sans maître et l'a incorporée dans le domaine communal ; que Mme B...et M. D...ont formé un recours pour excès de pouvoir contre cette décision ; que par délibération du 26 février 2013, la commune a décidé de céder la parcelle litigieuse à d'autres personnes et a autorisé le maire à signer l'acte de cession ; que, saisi par Mme B...et M.D..., sur le fondement de l'article L. 521-1 du code de justice administrative, d'une demande de suspension de la délibération du 31 mai 2012, le juge des référés du tribunal administratif de Bordeaux a prononcé la  suspension de l'exécution de cette délibération ;<br/>
<br/>
              4. Considérant, en premier lieu, que le moyen tiré de ce que la minute de l'ordonnance attaquée ne serait pas signée par le magistrat qui l'a rendue manque en fait ; <br/>
<br/>
              5. Considérant, en deuxième lieu, que la condition d'urgence doit être considérée comme remplie lorsque la décision administrative contestée préjudicie de manière suffisamment grave et immédiate à un intérêt public, à la situation des requérants ou aux intérêts qu'ils entendent défendre ; que, si cette condition s'apprécie au regard des effets de la décision attaquée, l'existence, au moment où le juge des référés statue, d'une autre décision, rendue possible par la première et préjudiciant à la situation des requérants, est au nombre des éléments dont il lui appartient de tenir compte à cet effet ; que c'est par suite, et en tout état de cause, sans erreur de droit et sans dénaturer les pièces du dossier que le juge des référés du tribunal administratif de Bordeaux a, pour décider de suspendre l'exécution de la délibération du conseil municipal de Mios du 31 mai 2012, apprécié la situation d'ensemble née de cette délibération, et notamment la décision ultérieure de la commune de mettre en vente le bien qui avait été déclaré sans maître et incorporé dans le domaine communal par la délibération attaquée ;<br/>
<br/>
              6. Considérant que, si les documents fournis par Mme B...et M.D..., et notamment l'acte de donation établi le 13 novembre 1903 ne suffisent pas par eux-mêmes à établir leur propriété sur le bien en cause, et s'il n'appartient qu'au juge civil d'apprécier la portée de la prescription acquisitive revendiquée par Mme B...et M. D...et contestée par la commune, c'est sans commettre d'erreur de droit et sans dénaturer les pièces du dossier que, dans le cadre d'une procédure de référé, le juge des référés s'est fondé sur ces deux circonstances pour juger qu'il existait un doute sérieux quant à la légalité de la délibération litigieuse ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que le pourvoi de la commune de Mios à l'encontre de l'ordonnance du juge des référés du tribunal administratif de Bordeaux ne peut qu'être rejeté ; que les dispositions de l'article L. 761-1 du code de justice administrative font, par suite, obstacle à ce que soit mise à la charge des défendeurs la somme demandée par la commune de Mios au titre de ces dispositions ; qu'il y a lieu, en revanche, de mettre à ce titre à la charge de la commune de Mios le versement  d'une somme de 1 500 euros chacun à Mme B... et M.D... ;<br/>
<br/>
<br/>
<br/>		D E C I D E :<br/>
              		--------------<br/>
<br/>
 Article 1er : Le pourvoi de la commune de Mios est rejeté.<br/>
<br/>
 Article 2 : La commune de Mios versera à Mme B... et à M. D...une somme de 1 500 euros chacun en application de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
 Article 3 : La présente décision sera notifiée à la commune de Mios,  à Mme C...B...et à M. E... D....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
