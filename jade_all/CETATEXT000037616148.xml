<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037616148</ID>
<ANCIEN_ID>JG_L_2018_11_000000414635</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/61/61/CETATEXT000037616148.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 15/11/2018, 414635, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-11-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>414635</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUIN-PALAT, BOUCARD</AVOCATS>
<RAPPORTEUR>M. Olivier Rousselle</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:414635.20181115</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société anonyme Omnium de gestion immobilière d'Ile-de-France (OGIF) a demandé au tribunal de Cergy-Pontoise de condamner l'Etat à lui verser une indemnité de 4 726,78 euros en réparation des préjudices ayant résulté pour elle du refus du préfet des Hauts-de-Seine de lui accorder le concours de la force publique pour l'exécution de la décision de justice prononçant l'expulsion des occupants d'un logement lui appartenant sis 4, rue Paul Verlaine à Gennevilliers. Par un jugement n°1609573 du 27 juillet 2017, le tribunal administratif a rejeté sa demande. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 27 septembre et 27 décembre 2017 au secrétariat du contentieux du Conseil d'Etat, la société OGIF demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le code civil ; <br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Rousselle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouin-Palat, Boucard, avocat de la société Omnium de gestion immobilière d'Ile-de-France.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que le juge des référés du tribunal d'instance d'Asnières-sur-Seine a, le 24 septembre 2013, ordonné l'expulsion de M. A...et Mme B...du logement qu'ils occupaient au 4, rue Paul Verlaine à Gennevilliers ; que la société Omnium de gestion immobilière d'Ile-de-France (OGIF), propriétaire de ce logement, a requis le concours de la force publique le 13 décembre 2013 ; que ce concours ne lui a été prêté que le 4 septembre 2014 ; que la société a vainement demandé au préfet des Hauts-de-Seine de l'indemniser des préjudices ayant résulté pour elle des loyers impayés et des frais de procédure exposés pour la période comprise entre le 1er avril et le 4 septembre 2014 ; qu'elle se pourvoit en cassation contre le jugement du 27 juillet 2017 par lequel le tribunal administratif de Cergy-Pontoise a rejeté sa demande tendant à ce que l'Etat soit condamné à réparer ces mêmes préjudices ; que le ministre de l'intérieur conteste, par la voie d'un pourvoi incident, le principe de la responsabilité de l'Etat et demande l'annulation de ce jugement pour ce motif ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 1256 du code civil, alors applicable : " Lorsque la quittance ne porte aucune imputation, le paiement doit être imputé sur la dette que le débiteur avait pour lors le plus d'intérêt d'acquitter entre celles qui sont pareillement échues ; sinon, sur la dette échue, quoique moins onéreuse que celles qui ne le sont point./ Si les dettes sont d'égale nature, l'imputation se fait sur la plus ancienne... " ; qu'il résulte de ces dispositions que les versements de M. A...et Mme B...intervenus au cours de la période de responsabilité de l'Etat, comprise entre le 1er avril et le 4 septembre 2014, devaient s'imputer en priorité, le cas échéant, sur le montant dont les intéressés étaient redevables au début de cette période ; qu'ainsi, en rejetant la demande de la société OGIF au motif que le versement de 4 000 euros intervenu le 28 mai 2014 était supérieur à la somme, évaluée à 3 535,75 euros, due au titre de la période de responsabilité de l'Etat sans vérifier l'existence d'une dette locative au 1er avril 2014, le tribunal administratif a commis une erreur de droit qui justifie l'annulation de son jugement ; que cette annulation prive d'objet le pourvoi incident du ministre de l'intérieur, sur lequel il n'y a pas lieu de statuer ; <br/>
<br/>
              3. Considérant qu'il résulte de ce qui précède que la société OGIF est fondée à demander l'annulation du jugement qu'elle attaque ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la société OGIF au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Cergy-Pontoise du 27 juillet 2017 est annulé.<br/>
<br/>
		Article 2 :   Il n'y a pas lieu de statuer sur le pourvoi incident du ministre de l'intérieur.<br/>
<br/>
		Article 3 : L'affaire est renvoyée au tribunal administratif de Cergy-Pontoise.<br/>
<br/>
Article 4 : L'Etat versera à la société Omnium de gestion immobilière d'Ile-de-France la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 5 : La présente décision sera notifiée à la société anonyme Omnium de Gestion Immobilière d'Ile-de-France et au  ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
