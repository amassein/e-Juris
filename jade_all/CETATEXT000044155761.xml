<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044155761</ID>
<ANCIEN_ID>JG_L_2021_10_000000448993</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/15/57/CETATEXT000044155761.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 01/10/2021, 448993, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-10-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>448993</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Edouard Solier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Raphaël Chambon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:448993.20211001</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M.  J... N..., d'une part, Mme A... O..., d'autre part, ont demandé au tribunal administratif de Montpellier, à titre principal, d'annuler les opérations électorales qui se sont déroulées le 15 mars 2020 pour l'élection des conseillers municipaux et communautaires dans la commune de Béziers (Hérault). Par un jugement n°s 2001449, 2002128 du 21 décembre 2020, le tribunal administratif a rejeté leurs protestations.<br/>
<br/>
              Par une requête, un mémoire en réplique et un nouveau mémoire, enregistrés les 22 janvier, 18 mai et 2 juillet 2021 au secrétariat du contentieux du Conseil d'Etat, M. M...-E... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) d'annuler ces opérations électorales ;<br/>
<br/>
              3°) à titre subsidiaire, de réformer les résultats des opérations électorales et de constater l'inéligibilité de M. H... E... ;<br/>
<br/>
              4°) en tout état de cause, de rejeter les comptes de campagne des candidats ayant bénéficié de l'aide d'une personne morale ou ayant enfreint d'autres règles en matière de financement électoral, de déclarer inéligibles les candidats concernés et, à défaut de rejet, de réformer les comptes de campagne de M. C... L... et de M. G... F... et de recalculer le montant du remboursement dû par l'Etat à chacun d'eux.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code électoral ;<br/>
              - la loi n° 2020-290 du 23 mars 2020 ;<br/>
              - le décret n° 2019-928 du 4 septembre 2019 ;<br/>
              - l'arrêté du ministre des solidarités et de la santé du 13 mars 2020 portant diverses mesures relatives à la lutte contre la propagation du virus covid-19 ;<br/>
              - l'arrêté du ministre des solidarités et de la santé du 14 mars 2020 portant diverses mesures relatives à la lutte contre la propagation du virus covid-19 ;<br/>
              - la décision du Conseil d'Etat, statuant au contentieux, n° 448993 du 31 mai 2021 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Edouard Solier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. A l'issue des opérations électorales qui se sont déroulées le 15 mars 2020 dans la commune de Béziers (Hérault), les quarante-neuf sièges de conseillers municipaux et les vingt-sept sièges de conseillers communautaires ont été pourvus. Quarante-quatre sièges de conseillers municipaux et vingt-cinq sièges de conseillers communautaires ont été attribués à des candidats de la liste " Choisir Béziers " conduite par M. D... B..., qui a obtenu 68,74 % des suffrages exprimés. Trois sièges de conseillers municipaux et un siège de conseiller communautaire ont été attribués à des candidats de la liste " De l'audace pour notre ville ", conduite par M. C... L..., qui a obtenu 11,53 % des suffrages exprimés. Un siège de conseiller municipal et un siège de conseiller communautaire ont été attribués à la liste " A gauche Béziers " conduite par M. G... F..., qui a recueilli 6,11 % des suffrages exprimés. Enfin, un siège de conseiller municipal a été attribué à la liste " Béziers en commun " conduite par M. H... E..., qui a obtenu 5,36 % des suffrages exprimés. M. J... N... fait appel du jugement du 21 décembre 2020 par lequel le tribunal administratif de Montpellier a rejeté sa protestation. A l'appui de sa requête, il soulève treize questions prioritaires de constitutionnalité.<br/>
<br/>
              Sur les questions prioritaires de constitutionnalité :<br/>
<br/>
              2. Il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que le Conseil constitutionnel est saisi de la question de la conformité à la Constitution de la disposition législative contestée à la triple condition que cette disposition soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux. Et aux termes du premier alinéa de l'article 23-5 de la même ordonnance: " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution (...) est présenté, à peine d'irrecevabilité, dans un mémoire distinct et motivé (...) ".<br/>
<br/>
              3. Les treize questions prioritaires de constitutionnalité soulevées par M. M...-E..., qui sont relatives à des dispositions de l'ordonnance du 7 novembre 1958, du code de justice administrative et du code électoral, ne sont assorties d'aucun élément permettant d'apprécier leur applicabilité au litige, ni d'aucune précision permettant d'apprécier leur caractère nouveau ou sérieux. Par suite, elles ne peuvent être regardées comme présentées dans un mémoire motivé. Dès lors, il n'y a pas lieu de les renvoyer au Conseil constitutionnel. <br/>
<br/>
              Sur la régularité du jugement attaqué :<br/>
<br/>
              4. En premier lieu, en se bornant à soutenir devant le Conseil d'Etat que le principe d'impartialité aurait été méconnu du fait de la présence dans la formation de jugement de M. K... et de M. I..., M. M...-E..., qui s'est d'ailleurs abstenu de demander la récusation de ces magistrats, n'apporte pas les précisions nécessaires à l'appréciation du bien-fondé du moyen qu'il soulève.<br/>
<br/>
              5. En deuxième lieu, il résulte des dispositions combinées de l'article R. 773-1 du code de justice administrative et des articles R. 119 et R. 120 du code électoral, qui ne méconnaissent pas l'article 16 de la Déclaration de 1789 et à l'encontre desquelles M. M...-E... ne peut utilement invoquer les stipulations de l'article 6, paragraphe 1, de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, d'une part que les protestations ne sont communiquées qu'aux conseillers élus, d'autre part que, par dérogation aux dispositions de l'article R. 611-1 du code de justice administrative, les tribunaux administratifs ne sont pas tenus d'ordonner la communication des mémoires en défense des conseillers municipaux dont l'élection est contestée aux auteurs des protestations, ni des autres mémoires ultérieurement enregistrés et qu'il appartient seulement aux parties, si elles le jugent utile, de prendre connaissance de ces défenses et mémoires ultérieurs au greffe du tribunal administratif. Par suite, le moyen tiré par M. N... du défaut de communication de sa protestation aux candidats non élus et du défaut de communication aux candidats élus de son mémoire du 6 juillet 2020 et de divers autres pièces du dossier n'est pas de nature à entacher d'irrégularité la procédure suivie devant le tribunal administratif, alors, d'ailleurs, qu'il n'est pas contesté que ces pièces étaient effectivement à la disposition des parties qui pouvaient en prendre connaissance. <br/>
<br/>
              6. En troisième lieu, le moyen tiré de ce que le jugement ne vise pas le mémoire produit par M. N... le 8 décembre 2020 manque en fait.<br/>
<br/>
              7. En quatrième lieu, contrairement à ce que soutient M. N..., le tribunal administratif, qui n'était pas tenu de répondre à l'ensemble des arguments présentés à l'appui des griefs soulevés au soutien de sa protestation, a suffisamment répondu aux griefs tirés de la méconnaissance de l'arrêté du 14 mars 2020 du ministre des solidarités et de la santé, de l'illégalité de la circulaire du ministre de l'intérieur du 9 mars 2020, de la nullité des bulletins de vote irréguliers au regard des dispositions de l'article R. 66-2 du code électoral, de l'absence de toute signature sur la feuille récapitulative des résultats par bureau de vote et sur le document de proclamation des résultats, et de l'impact de l'abstention sur le résultat des deux listes n'ayant pas atteint 5% des suffrages exprimés.<br/>
<br/>
              8. En cinquième lieu, le tribunal administratif a statué, au point 41 de son jugement, sur l'ensemble des conclusions présentées par M. N....<br/>
<br/>
              9. En sixième lieu, d'une part, il résulte des dispositions de l'article R. 773-1 du code de justice administrative et des articles R. 119 et R. 120 du code électoral que le caractère contradictoire de la procédure n'impose pas, compte tenu des règles et délais propres au contentieux électoral, que soient communiquées aux parties les décisions de la Commission nationale des comptes de campagne et des financements politiques relatives aux comptes de campagne des listes des candidats dont l'élection est contestée. D'autre part, alors que M. M...-E... soutenait devant les premiers juges que les comptes de campagne de plusieurs candidats devaient être rejetés du fait de l'utilisation de moyens publics prohibés par l'article L. 52-8 du code électoral, il ne résulte d'aucune règle ou principe général du droit que le tribunal administratif, qui a répondu sur ce grief tel qu'il était argumenté par le protestataire, était tenu de faire préalablement usage de ses pouvoirs d'instruction et d'ordonner à la Commission nationale des comptes de campagne et des financements politiques de communiquer l'ensemble des pièces en sa possession. Il en est de même des griefs, auxquels le tribunal a répondu, tirés de l'irrégularité des listes électorales, de l'établissement des procurations, de la fiabilité des listes d'émargement et de la régularité des procès-verbaux des bureaux de vote.<br/>
<br/>
              Sur le bien-fondé du jugement attaqué :<br/>
<br/>
              En ce qui concerne le contexte sanitaire :<br/>
<br/>
              10. L'émergence d'un nouveau coronavirus, responsable de la maladie à coronavirus 2019 ou covid-19 et particulièrement contagieux, a été qualifiée d'urgence de santé publique de portée internationale par l'Organisation mondiale de la santé le 30 janvier 2020, puis de pandémie le 11 mars 2020. La propagation du virus sur le territoire français a conduit le ministre des solidarités et de la santé à prendre, à compter du 4 mars 2020, des mesures de plus en plus strictes destinées à réduire les risques de contagion. Dans ce contexte, le Premier ministre a adressé à l'ensemble des maires le 7 mars 2020 une lettre présentant les mesures destinées à assurer le bon déroulement des élections municipales et communautaires prévues les 15 et 22 mars 2020. Ces mesures ont été précisées par une circulaire du ministre de l'intérieur du 9 mars 2020 relative à l'organisation des élections municipales des 15 et 22 mars 2020 en situation d'épidémie de coronavirus covid-19, formulant des recommandations relatives à l'aménagement des bureaux de vote et au respect des consignes sanitaires, et par une instruction de ce ministre, du même jour, destinée à faciliter l'exercice du droit de vote par procuration. Après consultation par le Gouvernement du conseil scientifique mis en place pour lui donner les informations scientifiques utiles à l'adoption des mesures nécessaires pour faire face à l'épidémie de covid-19, les 12 et 14 mars 2020, le premier tour des élections municipales a eu lieu comme prévu le 15 mars 2020. A l'issue du scrutin, les conseils municipaux ont été intégralement renouvelés dans 30 143 communes ou secteurs. Le taux d'abstention a atteint 55,34 % des inscrits, contre 36,45 % au premier tour des élections municipales de 2014. Au vu de la situation sanitaire, l'article 19 de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19 a reporté le second tour des élections, initialement fixé au 22 mars 2020, au plus tard en juin 2020 et prévu que : " Dans tous les cas, l'élection régulière des conseillers municipaux et communautaires, des conseillers d'arrondissement, des conseillers de Paris et des conseillers métropolitains de Lyon élus dès le premier tour organisé le 15 mars 2020 reste acquise, conformément à l'article 3 de la Constitution ". Ainsi que le Conseil constitutionnel l'a jugé dans sa décision n° 2020-849 du 17 juin 2020, ces dispositions n'ont ni pour objet ni pour effet de valider rétroactivement les opérations électorales du premier tour ayant donné lieu à l'attribution de sièges et ne font ainsi pas obstacle à ce que ces opérations soient contestées devant le juge de l'élection.<br/>
<br/>
              11. Aux termes de l'article L. 262 du code électoral, applicable aux communes de mille habitants et plus : " Au premier tour de scrutin, il est attribué à la liste qui a recueilli la majorité absolue des suffrages exprimés un nombre de sièges égal à la moitié du nombre des sièges à pourvoir, arrondi, le cas échéant, à l'entier supérieur lorsqu'il y a plus de quatre sièges à pourvoir et à l'entier inférieur lorsqu'il y a moins de quatre sièges à pourvoir. Cette attribution opérée, les autres sièges sont répartis entre toutes les listes à la représentation proportionnelle suivant la règle de la plus forte moyenne, sous réserve de l'application des dispositions du troisième alinéa ci-après. / Si aucune liste n'a recueilli la majorité absolue des suffrages exprimés au premier tour, il est procédé à un deuxième tour (...) ".<br/>
<br/>
              12. En premier lieu, il résulte des dispositions mêmes de la loi du 23 mars 2020 citées au point 10 que M. N... ne peut en tout état de cause utilement soutenir que le maintien des opérations électorales le 15 mars 2020 serait illégal, ni que l'incertitude sur l'organisation du second tour de scrutin aurait porté atteinte à la sincérité des opérations électorales du 15 mars 2020. Il ne peut davantage utilement se prévaloir de la méconnaissance de divers arrêtés comportant des prescriptions relatives aux rassemblements et réunions, qui ne sont pas applicables à la tenue des opérations électorales, ni exciper de leur illégalité.<br/>
<br/>
              13. En deuxième lieu, ni par ces dispositions, ni par celles de la loi du 23 mars 2020, le législateur n'a subordonné à un taux de participation minimal la répartition des sièges au conseil municipal à l'issue du premier tour de scrutin dans les communes de mille habitants et plus, lorsqu'une liste a recueilli la majorité absolue des suffrages exprimés. Le niveau de l'abstention n'est ainsi, par lui-même, pas de nature à remettre en cause les résultats du scrutin, s'il n'a pas altéré, dans les circonstances de l'espèce, sa sincérité. Alors que le taux d'abstention s'est élevé à 56,02 % des inscrits, soit un niveau proche de celui constaté au niveau national, et que M. N... fait seulement valoir que la crainte de la contamination aurait dissuadé certains électeurs d'aller voter, il ne résulte pas de l'instruction que des circonstances particulières lors de la tenue du scrutin au premier tour auraient été de nature à porter atteinte au libre exercice du droit de vote ou à altérer la sincérité du scrutin. <br/>
<br/>
              En ce qui concerne la campagne électorale :<br/>
<br/>
              14. Il y a lieu, par adoption des motifs retenus par le tribunal administratif, dont le jugement est suffisamment motivé sur chacun de ces points, d'écarter les griefs relatifs aux réunions organisées par M. B..., aux événements festifs organisés par la commune, à la mise à disposition par le centre communal d'action sociale d'un service de transport gratuit vers les bureaux de vote pour les personnes à mobilité réduite et les personnes âgées ou isolées, au traitement dont M. B... a fait l'objet dans certains journaux, à l'organisation d'un débat télévisé auquel n'ont été conviés que quatre des six candidats, aux soutiens politiques dont ont bénéficié les listes conduites par M. L... et M. F... et à la brochure de campagne de M. B.... <br/>
<br/>
              En ce qui concerne l'inéligibilité de M. H... E... :<br/>
<br/>
              15. Aux termes de l'article L. 231 du code électoral : " Ne peuvent être élus conseillers municipaux dans les communes situées dans le ressort où ils exercent ou ont exercé leurs fonctions depuis moins de six mois : / (...) 8° Les personnes exerçant, au sein (...) d'un établissement public de coopération intercommunale à fiscalité propre ou de leurs établissements publics, les fonctions de directeur général des services, directeur général adjoint des services, directeur des services, directeur adjoint des services ou chef de service, ainsi que les fonctions de directeur de cabinet, directeur adjoint de cabinet ou chef de cabinet en ayant reçu délégation de signature du président, du président de l'assemblée ou du président du conseil exécutif (...) ". Il y a lieu d'écarter le grief tiré de l'inéligibilité de M. E... par adoption des motifs du jugement du tribunal administratif. <br/>
<br/>
              En ce qui concerne les inscriptions sur les listes électorales : <br/>
<br/>
              16. Il n'appartient pas au juge de l'élection, hors le cas de manœuvres, d'apprécier le bien-fondé des inscriptions et radiations opérées sur les listes électorales. Il y a lieu, par adoption des motifs du jugement du tribunal administratif, d'écarter les griefs tirés de l'irrégularité dans l'établissement des listes électorales.<br/>
<br/>
              En ce qui concerne la régularité des opérations de vote :<br/>
<br/>
              17. En premier lieu, le grief tiré, par la voie de l'exception, de l'illégalité de la circulaire du ministre de l'intérieur du 9 mars 2020 relative aux modalités d'exercice du droit de vote par procuration doit être écarté par adoption des motifs retenus par le tribunal administratif. En outre, le grief tiré de ce que certaines procurations recueillies dans la commune de Béziers n'auraient pas satisfait à la condition de désignation d'un délégué de l'officier de police judiciaire n'est assorti d'aucune précision permettant d'en apprécier le bien-fondé.<br/>
<br/>
              18. En deuxième lieu, aux termes de l'article R. 117-4 du code électoral : " Dans les communes de 1 000 habitants et plus, les bulletins de vote doivent comporter, sur leur partie gauche, précédé des termes " Liste des candidats au conseil municipal ", le titre de la liste des candidats au mandat de conseiller municipal, ainsi que le nom de chaque candidat composant la liste dans l'ordre de présentation et, pour tout candidat ressortissant d'un Etat membre de l'Union européenne autre que la France, l'indication de sa nationalité. / Les bulletins de vote doivent également comporter sur la partie droite de la même page, précédée des termes " Liste des candidats au conseil communautaire ", la liste des candidats au mandat de conseiller communautaire mentionnant, dans l'ordre de présentation, leurs noms ". Eu égard à l'objet de ces dispositions, qui est d'éviter toute confusion, dans l'esprit de l'électeur, entre les candidats au mandat de conseiller municipal et les candidats au mandat de conseiller communautaire, les circonstances que le nom des listes conduites par MM. E..., L..., About et F... apparaissait au milieu des bulletins et que celui de la liste conduite par M. L... figurait également au verso du bulletin de vote n'ont pas été de nature à affecter la régularité des bulletins. <br/>
<br/>
              19. En troisième lieu, aux termes du dernier alinéa de l'article L. 62-1 du code électoral : " Le vote de chaque électeur est constaté par sa signature apposée à l'encre en face de son nom sur la liste d'émargement ". Aux termes de l'article L. 74 du même code : " Le ou la mandataire participe au scrutin dans les conditions prévues à l'article L. 62. / (...) Son vote est constaté par sa signature apposée à l'encre sur la liste d'émargement en face du nom du mandant ". Si M. N... invoque l'irrégularité des votes par procuration, faute de justification de la signature des mandataires conformément aux dispositions rappelées ci-dessus, il n'assortit pas ce grief de précisions suffisantes, à l'exception de quatre votes dans le bureau n° 7. En tout état de cause, ainsi que l'a relevé le tribunal administratif, une telle irrégularité ne saurait être de nature à altérer la sincérité du scrutin, compte tenu de l'écart de voix séparant la liste " Choisir Bézier " des autres listes.<br/>
<br/>
              20. En quatrième lieu, il ne résulte pas de l'instruction que l'existence de discordances dans la comptabilisation des électeurs dans les procès-verbaux des bureaux de vote n° 8, 18, 19, 30, 34 et 50 soit établie. Par ailleurs, les autres discordances invoquées par M. N..., qui ne portent, à supposer qu'elles soient établies, que sur un bulletin pour le bureau de vote n° 10, un bulletin pour le bureau de vote n° 30 et un autre bulletin pour le bureau de vote n° 44, ne peuvent être regardées comme ayant été de nature, en l'espèce, à avoir altéré la sincérité du scrutin eu égard à l'écart de voix constaté entre les listes.<br/>
<br/>
              21. En cinquième lieu, aux termes du premier alinéa de l'article L. 63 du code électoral : " L'urne (...) doit, avant le commencement du scrutin, avoir été fermée à deux serrures dissemblables, dont les clefs restent, l'une entre les mains du président, l'autre entre les mains d'un assesseur tiré au sort parmi l'ensemble des assesseurs ". La circonstance qu'il n'ait pas été procédé à un tirage au sort parmi l'ensemble des assesseurs du bureau de vote n° 50 pour désigner le porteur de la seconde clé, et que celle-ci ait été placée sous l'urne doit être regardée, en l'absence de toute allégation de manipulation irrégulière de l'urne, comme ayant été, en l'espèce, sans incidence sur la sincérité du scrutin.<br/>
<br/>
              22. En sixième lieu, aux termes de l'article R. 62 du code électoral : " Dès la clôture du scrutin, la liste d'émargement est signée par tous les membres du bureau. Il est aussitôt procédé au dénombrement des émargements ". Il ne résulte pas de l'instruction, en l'absence de manœuvre, que la circonstance, d'une part, que les membres des bureaux de vote n° 8, 18, 19, 30, 34 et 50 aient entendu apposer leur signature en une seule fois sur les listes d'émargement, en signant soit la liste principale, soit la liste complémentaire, et, d'autre part, qu'aucune signature n'apparaît sur les procès-verbaux des bureaux de vote n° 31, 32 et 44, ait été de nature à altérer la sincérité du scrutin ainsi que l'a relevé à bon droit le tribunal administratif, sans entacher son jugement d'une contradiction de motifs. Par ailleurs, si M. N... soutient que le procès-verbal du bureau centralisateur ne contient ni le recensement des résultats par bureau de vote, ni la signature des présidents des cinquante-quatre bureaux de vote, il résulte de l'instruction que ces informations et ces signatures figurent dans des annexes à ce procès-verbal, sans que celui-ci s'en trouve entaché d'une irrégularité de nature à porter atteinte à la sincérité du scrutin. <br/>
<br/>
              En ce qui concerne les comptes de campagne de MM. B..., L... et F... :<br/>
<br/>
              23. D'une part, il résulte des décisions du 1er octobre 2020 de la Commission nationale des comptes campagne et des financements politiques qu'elle a approuvé les comptes de campagne déposés par M. B..., M. L... et M. F.... D'autre part, il résulte de ce qui est dit au point 14 que le grief tiré de la méconnaissance par M. B..., M. L... et M. F... des dispositions de l'article L. 52-8 du code électoral ne peut qu'être écarté.  En particulier, pour ce qui concerne l'élaboration de la brochure de campagne de M. B..., il résulte de l'instruction que les factures afférentes aux frais d'impression et aux photographies utilisées ont déjà été imputées sur le compte de campagne de ce candidat. En outre, il ne résulte de l'instruction ni que les montants de ces factures seraient sous-évalués, ni que des services municipaux auraient contribué sur d'autres aspects à l'élaboration de cette brochure. Par suite, les conclusions tendant à la réformation des comptes de campagne des intéressés ne peuvent, en tout état de cause, qu'être rejetées.  Il n'y a dès lors pas lieu de réformer les comptes de campagne de MM. B..., L... et F....<br/>
<br/>
              24. Il résulte de tout ce qui précède, sans qu'il soit besoin de se prononcer sur les fins de non-recevoir opposées par M. B... et autres, et sans qu'il y ait lieu d'ordonner les mesures d'instruction sollicitées par M. N..., que ce dernier n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, qui est suffisamment motivé, le tribunal administratif de Montpellier a rejeté les conclusions de sa protestation.<br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              25. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par M. B... et autres au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel les questions prioritaires de constitutionnalité soulevées par M. N....<br/>
Article 2 : La requête de M. N... est rejetée.<br/>
Article 3 : Les conclusions présentées par M. B... et autres au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à M. J... N..., à M.  D... B..., représentant unique pour l'ensemble de ses cosignataires, à M. C... L..., à M. H... E..., à M. G... F... et au ministre de l'intérieur. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
