<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037783343</ID>
<ANCIEN_ID>JG_L_2018_12_000000414899</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/78/33/CETATEXT000037783343.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 4ème chambres réunies, 07/12/2018, 414899, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-12-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>414899</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Marie Sirinelli</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:414899.20181207</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              1° Sous le n° 414899, par une requête, un mémoire en réplique et un nouveau mémoire, enregistrés le 9 octobre 2017 et les 5 octobre et 20 novembre 2018 au secrétariat du contentieux du Conseil d'Etat, la Chambre syndicale des cabarets artistiques et discothèques, devenue la Chambre syndicale des lieux musicaux festifs et nocturnes, demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2017-1244 du 7 août 2017 relatif à la prévention des risques liés aux bruits et aux sons amplifiés ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Sous le n° 414964, par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 10 octobre et 21 décembre 2017 et le 10 octobre 2018, la Fédération française de motocyclisme demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le même décret ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule ;<br/>
              - le code de l'environnement ;<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Sirinelli, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat de la Fédération française de motocyclisme.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 1336-1 du code de la santé publique : " Les activités impliquant la diffusion de sons à un niveau sonore élevé, dans tout lieu public ou recevant du public, clos ou ouvert, sont exercées de façon à protéger l'audition du public et la santé des riverains. / Les modalités d'application du présent article sont fixées par décret en Conseil d'Etat ". La Chambre syndicale des cabarets artistiques et discothèques, désormais dénommée Chambre syndicale des lieux musicaux festifs et nocturnes, et la Fédération française de motocyclisme demandent l'annulation pour excès de pouvoir du décret du 7 août 2017 relatif à la prévention des risques liés aux bruits et aux sons amplifiés, pris pour l'application de ces dispositions. Leurs requêtes étant dirigées contre le même acte,  il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2. En premier lieu, il ressort des mentions de l'ampliation du décret attaqué, certifiée conforme par le secrétaire général du Gouvernement, que le moyen tiré de ce que ce décret n'aurait pas été signé par le Premier ministre et contresigné par le ministre des solidarités et de la santé, le ministre d'Etat, ministre de la transition écologique et solidaire, le garde des sceaux, ministre de la justice et le ministre de la culture manque en fait.<br/>
<br/>
              3. En deuxième lieu, aux termes de l'article L. 1311-1 du code de la santé publique : " Sans préjudice de l'application de législations spéciales et des pouvoirs reconnus aux autorités locales, des décrets en Conseil d'Etat, pris après consultation du Haut Conseil de la santé publique et, le cas échéant, du Conseil supérieur de la prévention des risques professionnels, fixent les règles générales d'hygiène et toutes autres mesures propres à préserver la santé de l'homme, notamment en matière : (...) de lutte contre les nuisances sonores et la pollution atmosphérique (...) ". L'organisme dont une disposition législative ou réglementaire prévoit la consultation avant l'intervention d'une décision doit être mis à même d'exprimer son avis sur l'ensemble des questions soulevées par cette décision. En l'espèce, il ressort des pièces du dossier que le projet de décret en cause, sur lequel le Haut Conseil de la santé publique a rendu un avis le 20 octobre 2016, renvoyait à un arrêté, notamment, la fixation des " niveaux de pression acoustique ". Le Haut Conseil a fait état de son regret qu'il procède à un tel renvoi et recommandé la reprise des seuils qu'il avait préconisés dans un rapport publié en septembre 2013. Dans ces conditions, si la Chambre syndicale des lieux musicaux festifs et nocturnes fait valoir que le projet a été modifié après cette consultation pour fixer lui-même des niveaux de pression acoustique, une telle modification ne posait aucune question nouvelle sur laquelle le Haut Conseil de la santé publique aurait dû être de nouveau consulté. Par suite, le moyen tiré de la méconnaissance de l'obligation de consultation prévue par l'article L. 1311-1 précité du code de la santé publique doit être écarté. <br/>
<br/>
              4. En troisième lieu, il résulte des termes mêmes de l'article L. 1336-1 de la santé publique, cité au point 1, qu'il couvre l'ensemble des activités impliquant la diffusion de sons à un niveau sonore élevé, dans tout lieu public ou recevant du public, clos ou ouvert. En prévoyant, au I de l'article R. 1336-1 qu'il rétablit dans le code de la santé publique, que ses dispositions " s'appliquent aux lieux ouverts au public ou recevant du public, clos ou ouverts, accueillant des activités impliquant la diffusion de sons amplifiés dont le niveau sonore est supérieur à la règle d'égale énergie fondée sur la valeur de 80 décibels pondérés A équivalents sur 8 heures ", sans limiter cette application aux seules activités ayant pour objet la diffusion de sons amplifiés, le décret attaqué n'a pas méconnu le champ d'application de la loi et ne saurait ainsi être regardé comme étant, sur ce point, entaché d'une erreur manifeste d'appréciation ou comme portant une atteinte disproportionnée à la liberté individuelle ou à la liberté du commerce et de l'industrie.<br/>
<br/>
              5. En quatrième lieu, le décret attaqué impose, au II du même article R. 1336-1, aux exploitants de lieux ouverts au public ou recevant du public accueillant des activités impliquant la diffusion de sons amplifiés, ainsi qu'aux producteurs ou aux diffuseurs, des prescriptions destinées à éviter l'exposition du public à des niveaux sonores susceptibles de présenter un risque pour sa santé, en renvoyant à un arrêté des ministres chargés de la santé, de l'environnement et de la culture les conditions de mise en oeuvre de ces dispositions.<br/>
<br/>
              6. D'une part, en procédant à un tel renvoi après avoir déterminé chacune des prescriptions par la fixation de niveaux de pression acoustique continus maximum et la définition d'obligations tenant à l'enregistrement et à l'affichage des niveaux sonores ainsi qu'à l'information du public, à la mise à disposition de protections auditives et à la création de zones de repos auditif, le décret attaqué, qui a fixé avec une précision suffisante la teneur de ces mesures et leur périmètre d'application,  n'opère pas, contrairement à ce qui est soutenu, une subdélégation illégale.<br/>
<br/>
              7. D'autre part, il ressort des pièces du dossier que les obligations ainsi fixées, qui s'inspirent des recommandations figurant dans le rapport publié en septembre 2013 par le Haut Conseil de la santé publique mentionné ci-dessus, poursuivent un objectif de santé publique. En outre, les obligations qui s'ajoutent au seul respect des niveaux de pression acoustique continus maximum ne s'appliquent, à l'exception des festivals, qu'aux lieux diffusant des sons amplifiés à titre habituel et, pour celles qui tiennent à l'obligation d'enregistrement et d'affichage en continu de ces niveaux, qu'à ceux d'entre eux, à l'exception des discothèques, dont la capacité d'accueil est supérieure à 300 personnes. Enfin, il ne ressort pas des pièces du dossier que ces obligations impliqueraient des investissements et des adaptations excessifs, celle qui prévoit la création de zones de repos auditif pouvant, à supposer qu'elle se révèle trop contraignante, être remplacée par le respect de périodes de repos auditif. Dans ces conditions, les requérantes ne sont pas fondées à soutenir que ces obligations seraient entachées d'une erreur manifeste d'appréciation, au motif, notamment, qu'elles seraient inapplicables, ou qu'elles porteraient une atteinte disproportionnée à la liberté du commerce et de l'industrie ou, en tout état de cause, à la liberté individuelle. <br/>
<br/>
              8. En cinquième lieu, les obligations définies par le décret attaqué s'appliquent à la diffusion de sons amplifiés, ainsi qu'il résulte, pour celles qui sont insérées dans le code de la santé publique, de l'article R. 1336-1 de ce code mentionné au point 4 et, pour celles qui sont insérées dans le code de l'environnement, de l'article R. 571-25 de ce code. Elles ne sont susceptibles de concerner les événements ou compétitions sportives impliquant des véhicules que dans la mesure où des sons amplifiés y seraient diffusés. Par suite, la Fédération française de motocyclisme ne peut utilement soutenir que le décret attaqué serait entaché d'une erreur manifeste d'appréciation en ce qu'il introduit, à l'article R. 1336-1 du code de la santé publique et à l'article R. 571-27 du code de l'environnement, des limitations de bruit qui ne seraient pas techniquement susceptibles d'être imposées à des véhicules et qui ne pourraient raisonnablement encadrer les compétitions motocyclistes ou la circulation de motocyclettes. <br/>
<br/>
              9. En sixième lieu, aux termes de l'article L. 131-16 du code du sport : " Les fédérations délégataires édictent : / 1° Les règles techniques propres à leur discipline ainsi que les règles ayant pour objet de contrôler leur application et de sanctionner leur non-respect par les acteurs des compétitions sportives ; / 2° Les règlements relatifs à l'organisation de toute manifestation ouverte à leurs licenciés ; (...) ". L'article R. 1336-6 du code de la santé publique, dans sa rédaction issue du II de l'article 1er du décret litigieux, prévoit que : " Lorsque le bruit mentionné à l'article R. 1336-5 a pour origine une activité professionnelle autre que l'une de celles mentionnées à l'article R. 1336-10 ou une activité sportive, culturelle ou de loisir, organisée de façon habituelle ou soumise à autorisation, l'atteinte à la tranquillité du voisinage ou à la santé de l'homme est caractérisée si l'émergence globale de ce bruit perçu par autrui, telle que définie à l'article R. 1336-7, est supérieure aux valeurs limites fixées au même article ". En fixant ainsi, dans un but de santé et de tranquillité publiques, des valeurs limites à respecter en toute hypothèse en matière de bruit de voisinage, notamment dans le cadre des activités sportives, le décret attaqué, qui n'est pas entaché d'erreur manifeste d'appréciation sur ce point, ne méconnaît pas les dispositions précitées de l'article L. 131-16 du code du sport qui définissent la compétence des fédérations sportives pour édicter les règles techniques propres à chaque discipline.<br/>
<br/>
              10. En septième lieu, aux termes de l'article L. 111-1 du code de la propriété intellectuelle : " L'auteur d'une oeuvre de l'esprit jouit sur cette oeuvre, du seul fait de sa création, d'un droit de propriété incorporelle exclusif et opposable à tous. / Ce droit comporte des attributs d'ordre intellectuel et moral ainsi que des attributs d'ordre patrimonial, qui sont déterminés par les livres Ier et III du présent code ". Le décret attaqué, ayant pour seul objet de réglementer la diffusion de sons amplifiés dans des lieux ouverts au public, sans interdire la diffusion ni encadrer la création des oeuvres musicales susceptibles d'être ainsi diffusées, ne saurait être regardé comme portant atteinte à la liberté de création artistique des auteurs de telles oeuvres. En outre, à supposer même que les dispositions en cause soient susceptibles de porter atteinte au droit moral de certains auteurs en imposant, pour le respect des niveaux de pression acoustiques qu'elles fixent, des adaptations ou des altérations, une telle atteinte, qui est limitée à certains types d'établissements et à certaines circonstances, est justifiée par des impératifs de santé publique et ne méconnaît pas, en tout état de cause, les dispositions précitées de l'article L. 111-1 du code de la propriété intellectuelle.<br/>
<br/>
              11. En dernier lieu, l'article R. 1336-14 du code de la santé publique prévoit, dans sa rédaction issue du décret attaqué, qu'" est puni de l'amende prévue pour les contraventions de 5e classe le fait pour toute personne visée au deuxième alinéa de l'article R. 1336-1 de ne pas respecter les prescriptions mentionnées aux 1°, 2° et 3° de ce même article ". L'article R. 1336-3 du même code prévoit, en outre, dans sa rédaction issue de ce décret, que : " Lorsqu'il constate l'inobservation des dispositions prévues à l'article R. 1336-1, le préfet ou, à Paris, le préfet de police met en oeuvre les mesures définies à l'article L. 171-8 du code de l'environnement ". Enfin, aux termes de l'article L. 171-8 du code de l'environnement auquel il est ainsi renvoyé, dans sa rédaction en vigueur à la date du décret attaqué : " I. - Indépendamment des poursuites pénales qui peuvent être exercées, en cas d'inobservation des prescriptions applicables en vertu du présent code aux installations, ouvrages, travaux, aménagements, opérations, objets, dispositifs et activités, l'autorité administrative compétente met en demeure la personne à laquelle incombe l'obligation d'y satisfaire dans un délai qu'elle détermine. En cas d'urgence, elle fixe les mesures nécessaires pour prévenir les dangers graves et imminents pour la santé, la sécurité publique ou l'environnement. / II. - Si, à l'expiration du délai imparti, il n'a pas été déféré à la mise en demeure, l'autorité administrative compétente peut arrêter une ou plusieurs des sanctions administratives suivantes : / 1° L'obliger à consigner entre les mains d'un comptable public avant une date qu'elle détermine une somme correspondant au montant des travaux ou opérations à réaliser. La somme consignée est restituée au fur et à mesure de l'exécution des travaux ou opérations. (...) / 2° Faire procéder d'office, en lieu et place de la personne mise en demeure et à ses frais, à l'exécution des mesures prescrites ; les sommes consignées en application du 1° sont utilisées pour régler les dépenses ainsi engagées ; / 3° Suspendre le fonctionnement des installations et ouvrages, la réalisation des travaux et des opérations ou l'exercice des activités jusqu'à l'exécution complète des conditions imposées et prendre les mesures conservatoires nécessaires, aux frais de la personne mise en demeure ; / 4° Ordonner le paiement d'une amende au plus égale à 15 000 euros et une astreinte journalière au plus égale à 1 500 euros applicable à partir de la notification de la décision la fixant et jusqu'à satisfaction de la mise en demeure. (...) ".<br/>
<br/>
              12. S'il résulte des dispositions précitées que la méconnaissance de certaines des règles fixées par le code de la santé publique est punie de l'amende prévue pour les contraventions de cinquième classe, cette sanction ne revêt pas, eu égard à l'objectif de protection de la santé publique poursuivi par les dispositions en cause, un caractère manifestement disproportionné. Par suite, et alors qu'il appartient en tout état de cause à l'autorité qui prononce la sanction de respecter les principes de nécessité et de proportionnalité des peines résultant de l'article 8 de la Déclaration des droits de l'homme et du citoyen, notamment en veillant à ce que le montant global des sanctions éventuellement prononcées de façon cumulée ne dépasse pas le montant le plus élevé de l'une des sanctions encourues, la Fédération française de motocyclisme n'est pas fondée à soutenir que les dispositions du décret attaqué porteraient atteinte à ces principes. Enfin, les dispositions de l'article L. 171-8 du code de l'environnement, auquel renvoie l'article R. 1336-3 du code de la santé publique, visent principalement à obtenir la mise en conformité des installations et poursuivent ainsi un objet distinct de celui de la sanction pénale prévue à l'article R. 1336-14 précité du code de la santé publique. Par suite, la fédération requérante n'est pas davantage fondée à soutenir que cette sanction pénale serait entachée d'une erreur manifeste d'appréciation au motif qu'elle viendrait inutilement compléter le dispositif de sanction administrative prévu par l'article L. 171-8 du code de l'environnement.<br/>
<br/>
              13. Il résulte de tout ce qui précède que la Chambre syndicale des cabarets artistiques et discothèques, désormais dénommée Chambre syndicale des lieux musicaux festifs et nocturnes, et la Fédération française de motocyclisme ne sont pas fondées à demander l'annulation du décret qu'elles attaquent. Par suite, leurs conclusions présentées au titre de l'article L. 761-1 du code de justice administrative doivent être également rejetées. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er :	Les requêtes de la Chambre syndicale des cabarets artistiques et discothèques et de la Fédération française de motocyclisme sont rejetées. <br/>
Article 2 : La présente décision sera notifiée à la Chambre syndicale des lieux musicaux festifs et nocturnes, à la Fédération française de motocyclisme et à la ministre des solidarités et de la santé.<br/>
Copie en sera adressée au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
