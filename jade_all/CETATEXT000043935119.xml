<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043935119</ID>
<ANCIEN_ID>JG_L_2021_08_000000455131</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/93/51/CETATEXT000043935119.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 06/08/2021, 455131, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-08-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>455131</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLEZ</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:455131.20210806</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
               M. D... A... B... a demandé au juge des référés du tribunal administratif de Versailles, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, de prendre toutes les mesures nécessaires pour empêcher son expulsion des locaux de son habitation principale située 7, rue Saint-Honoré, à Versailles en ordonnant le retrait, l'annulation ou la suspension de la décision du préfet des Yvelines du 24 juin 2021 accordant le concours de la force publique pour cette expulsion. Par une ordonnance n° 2106363 du 29 juillet 2021, le juge des référés du tribunal administratif de Versailles a rejeté sa demande.<br/>
<br/>
               Par une requête, enregistrée le 2 août 2021 au secrétariat du contentieux du Conseil d'Etat, M. A... B... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
               1°) d'annuler cette ordonnance ; <br/>
<br/>
               2°) de faire droit à sa demande en référé en suspendant au besoin la décision du 24 juin 2021 par laquelle le préfet des Yvelines a accordé le concours de la force publique ; <br/>
<br/>
               3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
               Il soutient que :<br/>
               - la condition d'urgence est satisfaite dès lors que l'expulsion deviendra effective à partir du 2 août 2021 ;  <br/>
               - il est porté une atteinte grave et manifestement illégale à son droit de mener une vie familiale normale et au droit à l'inviolabilité du domicile ; <br/>
               - c'est à tort que le juge des référés a considéré que sa requête tendait à apprécier la validité du commandement de quitter les lieux délivré par l'huissier de justice ;<br/>
               - la décision du préfet des Yvelines du 24 juin 2021 ne permet pas de s'assurer que le titre exécutoire et le commandement de libérer les lieux ont bien été notifiés au préfet des Yvelines ;<br/>
               - la décision fait référence à une décision rendue le 29 janvier 2019 par le juge des contentieux de la protection judiciaire de Versailles alors que la décision a été rendue par la cour d'appel de Versailles ;<br/>
               - la décision méconnaît les dispositions de l'article R. 153-1 du code des procédures civiles d'exécution dès lors qu'elle n'est accompagnée ni d'une copie du dispositif du titre exécutoire, ni de l'exposé des diligences auxquelles l'huissier de justice a procédé et des difficultés d'exécution ;<br/>
               - la décision méconnaît les dispositions des articles L. 411-1 et R. 153-1 du code des procédures civiles d'exécution en ne faisant pas référence à un commandement de quitter les lieux ;<br/>
               - l'arrêt du 29 janvier 2019 ne pouvait plus être regardé comme valant titre exécutoire, Me C... étant dépourvu de mandat légal à la suite de l'arrêt de la cour d'appel de Versailles du 31 janvier 2019 annulant l'ordonnance du 5 avril 2016 l'ayant désigné en qualité d'administrateur judiciaire ;<br/>
               - les commandements de payer ne pouvaient être regardés comme valables dès lors qu'ils ont été signifiés à la demande d'un administrateur judiciaire dépourvu de mandat légal.<br/>
<br/>
<br/>
               Vu les autres pièces du dossier ;<br/>
<br/>
               Vu : <br/>
               - le code des procédures civiles d'exécution ;<br/>
               - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
               1. Aux termes de l'article L. 521-2 du même code : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
               2. Il résulte de l'instruction menée devant le juge des référés du tribunal administratif de Versailles que, par un arrêt du 29 janvier 2019, la cour d'appel de Versailles a confirmé le jugement du 5 avril 2012 du tribunal d'instance de Versailles en ce qu'il a ordonné à M. et Mme A... B... de restituer l'appartement sis 7, rue Saint Honoré à Versailles, et qu'il a ordonné, à défaut de départ volontaire, l'expulsion immédiate de M. et Mme A... B... au besoin avec l'assistance de la force publique, cet arrêt fixant également l'indemnité d'occupation par M. et Mme A... B... à 1 500euros pour la période allant du 1er mars 2008 au 1er février 2012 et à 1 800 euros pour la période allant du 2 février 2012 à la libération effective des lieux. Trois commandements de quitter les lieux ont été signifiés par huissier à M. et Mme A... B... les 29 juillet, 28 octobre et 27 décembre 2019 à la demande de Me C... en qualité d'administrateur des indivisions successorales de Jacques et Edmée Blanchard de la Brosse, parents de l'épouse de M. A... B.... Par une décision du 24 juin 2021, le préfet des Yvelines a accordé le concours de la force publique pour faire procéder à l'expulsion à compter du 2 août 2021 en rappelant à M. et Mme A... B... qu'ils restaient redevables d'une dette s'élevant à 274 890 euros au 15 juin 2021. M. A... B... a saisi le juge des référés du tribunal administratif de Versailles, sur le fondement de l'article L. 521-2 du code de justice administrative, pour obtenir la suspension de de l'exécution de cette décision. Il relève appel de l'ordonnance du juge des référés du tribunal administratif de Versailles du 29 juillet 2021 rejetant sa demande.<br/>
<br/>
               3. Aux termes de l'article L. 153-1 du code des procédures civiles d'exécution : " L'Etat est tenu de prêter son concours à l'exécution des jugements et des autres titres exécutoires. Le refus de l'Etat de prêter son concours ouvre droit à réparation ". Aux termes de l'article L. 153-2 du même code : " L'huissier de justice chargé de l'exécution peut requérir le concours de la force publique ". Aux termes de l'article L. 411-1 du même code : " Sauf disposition spéciale, l'expulsion d'un immeuble ou d'un lieu habité ne peut être poursuivie qu'en vertu d'une décision de justice ou d'un procès-verbal de conciliation exécutoire et après signification d'un commandement d'avoir à libérer les locaux ". Aux termes de l'article R. 153-1 du même code : " Si l'huissier de justice est dans l'obligation de requérir le concours de la force publique, il s'adresse au préfet / La réquisition contient une copie du dispositif du titre exécutoire. Elle est accompagnée d'un exposé des diligences auxquelles l'huissier de justice a procédé et des difficultés d'exécution ".<br/>
<br/>
               4. Il résulte des dispositions de l'article L. 153-1 du code des procédures civiles d'exécution citées au point 3 que le représentant de l'Etat, saisi d'une demande en ce sens, doit prêter le concours de la force publique en vue de l'exécution des décisions de justice ayant force exécutoire. Seules des considérations impérieuses tenant à la sauvegarde de l'ordre public, ou des circonstances postérieures à une décision de justice ordonnant l'expulsion d'occupants d'un local, faisant apparaître que l'exécution de cette décision serait de nature à porter atteinte à la dignité de la personne humaine, peuvent légalement justifier, sans qu'il soit porté atteinte au principe de la séparation des pouvoirs, le refus de prêter le concours de la force publique.<br/>
<br/>
               5. En premier lieu, le requérant fait valoir que l'arrêt du 29 janvier 2019 confirmant le jugement ordonnant son expulsion ne peut être regardé comme valant titre exécutoire à la suite de l'arrêt de la cour d'appel de Versailles du 31 janvier 2019 annulant l'ordonnance du 5 avril 2016 du président du tribunal de grande instance de Versailles désignant Me C... comme administrateur des indivisions successorales de Jacques et Edmée Blanchard de la Brosse. Il résulte néanmoins de l'instruction que cet arrêt ne statue pas sur l'expulsion de M. et Mme A... B... et qu'il confirme la désignation de Me C... comme administrateur des indivisions successorales en faisant injonction, en vertu de l'effet dévolutif de l'appel, à deux indivisionnaires, dont Mme A... B..., de remettre à Me C... les fonds perçus sur les biens appartenant auxdites indivisions, de lui remettre une reddition des comptes et de lui restituer les dossiers locatifs des successions et de cesser toute gestion locative.<br/>
<br/>
               6. En deuxième lieu, la circonstance que, par une erreur de plume, la décision du préfet des Yvelines du 24 juin 2021 mentionne que la décision du 29 janvier 2019 a été rendue par le juge des contentieux de la protection du tribunal judiciaire de Versailles et non par la cour d'appel de Versailles ne peut faire regarder cette décision comme manifestement illégale.<br/>
<br/>
               7. En troisième lieu, les dispositions du deuxième alinéa de l'article R. 153-1 du code des procédures civiles d'exécution citées au point 3, qui concernent la réquisition adressée par l'huissier de justice au préfet pour obtenir le concours de la force publique, n'imposent pas à la décision préfectorale accordant le concours de la force publique d'être accompagnée d'une copie du dispositif du titre exécutoire et de l'exposé des diligences auxquelles l'huissier de justice a procédé et des difficultés d'exécution. De même, ni les dispositions de cet article, ni celles de l'article L. 411-1 du même code citées au point 3 n'imposent que la décision accordant le concours de la force publique comprenne la références des commandements de quitter les lieux adressés dans le cadre de la procédure d'expulsion. <br/>
<br/>
               8. En quatrième lieu, il ne résulte d'aucune disposition qu'il appartient au préfet, saisi d'une demande d'octroi du concours de la force publique pour l'exécution d'un jugement d'expulsion, d'apprécier la validité du commandement de quitter les lieux délivré par l'huissier de justice. <br/>
<br/>
               9. Il résulte de ce qui précède que M. A... B... n'est pas fondé à demander l'annulation de l'ordonnance du juge des référés du tribunal administratif de Versailles du 29 juillet 2021. Il s'ensuit que sa requête doit être rejetée, y compris les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative, selon la procédure prévue par l'article L. 522-3 du code de justice administrative.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de M. A... B... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. D... A... B....<br/>
Copie en sera adressée au préfet des Yvelines.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
