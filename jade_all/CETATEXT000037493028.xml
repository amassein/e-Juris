<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037493028</ID>
<ANCIEN_ID>JG_L_2018_10_000000419505</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/49/30/CETATEXT000037493028.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 12/10/2018, 419505, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-10-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>419505</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Céline  Guibé</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Astrid Nicolazo de Barmon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:419505.20181012</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société T2S a demandé au juge des référés du tribunal administratif de Nancy d'ordonner, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution des décisions des 14 et 20 novembre 2017 par lesquelles la société Electricité de France (EDF) a refusé le rattachement de son installation photovoltaïque située à Jarville-la-Malgrange au périmètre d'équilibre EDF OA en vue de bénéficier d'un contrat d'obligation d'achat de l'électricité produite et d'enjoindre à la société EDF d'y procéder dans un délai de trois jours. Par une ordonnance n° 1800493 du 16 mars 2018, le juge des référés du tribunal administratif de Nancy a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 3 et 18 avril 2018 au secrétariat du contentieux du Conseil d'Etat, la société T2S demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande de suspension ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'énergie ;<br/>
              - la loi n° 2000-108 du 10 février 2000 ;<br/>
              - le décret n° 2015-233 du 27 février 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Céline Guibé, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Marie-Astrid Nicolazo de Barmon, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de la société T2S et à la SCP Piwnica, Molinié, avocat de la société EDF.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 27 septembre 2018, présentée par la société EDF ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Par une demande du 7 novembre 2017, la société T2S a sollicité auprès de la société EDF le rattachement d'une centrale photovoltaïque située à Jarville-la-Malgrange (Meurthe-et-Moselle) au périmètre d'équilibre dédié aux obligations d'achat de la société EDF, en vue de préserver son droit à conclure un contrat d'achat de l'électricité produite selon les conditions tarifaires prévues par l'arrêté du 12 janvier 2010. Par deux lettres des 14 et 20 novembre 2017, la société EDF a rejeté cette demande au motif qu'elle avait suspendu la signature de tout contrat d'achat au tarif fixé par l'arrêté du 12 janvier 2010. La société T2S se pourvoit en cassation contre l'ordonnance du 16 mars 2018 par laquelle le juge des référés du tribunal administratif de Nancy a rejeté sa demande tendant à la suspension de cette décision.<br/>
<br/>
              2. Aux termes de l'article 35 du décret du 27 février 2015 : " Lorsqu'une juridiction est saisie d'un litige qui présente à juger, soit sur l'action introduite, soit sur une exception, une question de compétence soulevant une difficulté sérieuse et mettant en jeu la séparation des ordres de juridiction, elle peut, par une décision motivée qui n'est susceptible d'aucun recours, renvoyer au Tribunal des conflits le soin de décider sur cette question de compétence. ".<br/>
<br/>
              3. Aux termes de l'article L. 321-15 du code de l'énergie : " Chaque producteur d'électricité raccordé aux réseaux publics de transport ou de distribution et chaque consommateur d'électricité, pour les sites pour lesquels il a exercé son droit prévu à l'article L. 331-1, est responsable des écarts entre les injections et les soutirages d'électricité auxquels il procède. Il peut soit définir les modalités selon lesquelles lui sont financièrement imputés ces écarts par contrat avec le gestionnaire du réseau public de transport, soit contracter à cette fin avec un responsable d'équilibre qui prend en charge les écarts ou demander à l'un de ses fournisseurs de le faire ". <br/>
<br/>
              4. Le litige né de l'action de la société T2S tendant à la contestation du refus de la société Electricité de France de conclure avec un producteur d'électricité un contrat de rattachement à un périmètre d'équilibre présente à juger une question de compétence soulevant une difficulté sérieuse et de nature à justifier le recours à la procédure prévue par l'article 35 du décret du 27 février 2015. Par suite, il y a lieu de renvoyer au Tribunal des conflits la question de savoir si l'action introduite par la société requérante devant le juge des référés relève ou non de compétence de la juridiction administrative et de surseoir à statuer sur le pourvoi de la société T2S jusqu'à la décision de ce tribunal.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'affaire est renvoyée au Tribunal des conflits.<br/>
Article 2 : Il est sursis à statuer sur le pourvoi de la société T2S jusqu'à ce que le Tribunal des conflits ait tranché la question de savoir quel est l'ordre de juridiction compétent pour statuer sur sa demande.<br/>
Article 3 : La présente décision sera notifiée à la société T2S et à la société EDF et au ministre d'Etat, ministre de la transition écologique et solidaire.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
