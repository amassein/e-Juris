<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030484879</ID>
<ANCIEN_ID>JG_L_2015_04_000000370309</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/48/48/CETATEXT000030484879.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 15/04/2015, 370309, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-04-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>370309</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER, PINATEL ; FOUSSARD ; SCP DIDIER, PINET ; SCP ROGER, SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR>M. Lionel Collet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:370309.20150415</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif de Paris de mettre à la charge de l'Assistance publique-Hôpitaux de Paris (AP-HP) la réparation des préjudices ayant résulté pour elle d'une intervention chirurgicale pratiquée le 4 avril 2003 à l'hôpital Bichat-Claude Bernard. Par un jugement n° 0701584/6-1 du 12 mars 2010, le tribunal administratif a rejeté sa demande ainsi que les conclusions de la caisse primaire d'assurance maladie (CPAM) de l'Essonne tendant au remboursement de ses frais. <br/>
<br/>
              Par un arrêt n° 11PA02104 du 31 janvier 2013, la cour administrative d'appel de Paris a, sur la requête de MmeB..., annulé ce jugement et condamné l'AP-HP à verser à l'intéressée la somme de 61 342,80 euros, à son fils mineur la somme de 500 euros et à la CPAM de l'Essonne la somme de 11 679,59 euros et rejeté le surplus des conclusions de Mme B... et de la caisse.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 18 juillet et 18 octobre 2013 au secrétariat du contentieux, Mme B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt de la cour administrative d'appel de Paris en tant qu'il a rejeté le surplus de ses conclusions d'appel ; <br/>
<br/>
              2°) de renvoyer l'affaire devant une cour administrative d'appel de Paris ; <br/>
<br/>
              3°) de mettre à la charge de l'AP-HP et de l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) le versement d'une somme de 5 000 euros à la SCP Fabiani, Luc-Thaler, son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de santé publique ;<br/>
<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Lionel Collet, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de MmeB..., à la SCP Didier, Pinet, avocat de l'Assistance publique-Hôpitaux de Paris, à la SCP Roger, Sevaux, Mathonnet, avocat de l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales, et à Me Foussard, avocat de la caisse primaire d'assurance maladie de l'Essonne ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme B... a subi le 4 avril 2003 à l'hôpital Bichat-Claude Bernard, dépendant de l'Assistance publique-Hôpitaux de Paris (AP-HP), une opération de gastroplastie sous coelioscopie avec mise en place d'un anneau gastrique, destinée à traiter l'obésité dont elle était atteinte ; qu'en raison d'une péritonite apparue le 12 avril 2003, une nouvelle intervention chirurgicale avec une résection oeso-gastrique et gastrectomie partielle a dû être pratiquée, entraînant des séquelles importantes ; que le tribunal administratif de Paris, saisi d'une demande indemnitaire de l'intéressée et de conclusions de la caisse primaire d'assurance maladie de l'Essonne tendant au remboursement de ses frais, a estimé, d'une part, que le dommage n'était pas la conséquence d'une faute médicale de nature à engager la responsabilité de l'AP-HP et, d'autre part, qu'il ne présentait pas le caractère d'anormalité auquel les dispositions du II de l'article L. 1142-1 du code de la santé publique subordonnent le droit à indemnisation des accidents médicaux par l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) au titre de la solidarité nationale ; que, par un arrêt du 31 janvier 2013, la cour administrative d'appel de Paris, estimant que la responsabilité de l'AP-HP était engagée à hauteur de 50 % des préjudices au titre d'un manquement des médecins à leur obligation d'informer la patiente des risques de l'opération pratiquée le 4 avril 2003, a annulé la décision des premiers juges, mis à la charge de l'AP-HP le versement de 61 342, 80 euros à l'intéressée, de 500 euros à son fils mineur et de 11 679,59 euros à la CPAM de l'Essonne et rejeté le surplus des conclusions de l'intéressée et de la caisse primaire ; que Mme B...et la caisse primaire demandent que cet arrêt soit annulé en tant qu'il ne retient pas la responsabilité de l'AP-HP au titre d'une faute médicale et n'accorde aucune indemnité au titre de la nécessité de recourir à l'assistance d'une tierce personne ; que Mme B...demande également qu'il soit annulé en tant qu'il ne lui reconnaît pas un droit à réparation au titre de la solidarité nationale ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 1142-1 du code de la santé publique, dans sa rédaction applicable au litige : " I. - Hors le cas où leur responsabilité est encourue en raison d'un défaut d'un produit de santé, les professionnels de santé mentionnés à la quatrième partie du présent code, ainsi que tout établissement, service ou organisme dans lesquels sont réalisés des actes individuels de prévention, de diagnostic ou de soins ne sont responsables des conséquences dommageables d'actes de prévention, de diagnostic ou de soins qu'en cas de faute. / (... )/ II. - Lorsque la responsabilité d'un professionnel, d'un établissement, service ou organisme mentionné au I ou d'un producteur de produits n'est pas engagée, un accident médical, une affection iatrogène ou une infection nosocomiale ouvre droit à la réparation des préjudices du patient au titre de la solidarité nationale, lorsqu'ils sont directement imputables à des actes de prévention, de diagnostic ou de soins et qu'ils ont eu pour le patient des conséquences anormales au regard de son état de santé comme de l'évolution prévisible de celui-ci et présentent un caractère de gravité, fixé par décret, apprécié au regard de la perte de capacités fonctionnelles et des conséquences sur la vie privée et professionnelle mesurées en tenant notamment compte du taux d'incapacité permanente ou de la durée de l'incapacité temporaire de travail. / Ouvre droit à réparation des préjudices au titre de la solidarité nationale un taux d'incapacité permanente supérieur à un pourcentage d'un barème spécifique fixé par décret ; ce pourcentage, au plus égal à 25 %, est déterminé par ledit décret " ; que l'article D. 1142-1 du même code définit le seuil de gravité prévu par ces dispositions législatives ;<br/>
<br/>
              Sur la responsabilité de l'AP-HP au titre d'une faute médicale :<br/>
<br/>
              3. Considérant, d'une part, que l'arrêt attaqué énonce que la péritonite constatée le 12 avril 2003 est due à une perforation minime de l'estomac survenue lors de l'intervention pratiquée le 4 avril 2003, qui s'est ensuite agrandie pour atteindre deux centimètres, qu'une telle perforation peut trouver son origine dans le geste de dissection au contact de la paroi gastrique nécessaire pour passer la languette de l'anneau, sans qu'aucune erreur ou maladresse soit imputable au praticien qui l'effectue, et que si la plaie instrumentale a pu être considérée par l'expert comme résultant d'une " faute technique ", il ne résulte pas de l'instruction que le chirurgien ait méconnu les règles de l'art ; qu'en jugeant, en conséquence, que les médecins n'avaient pas commis de faute de nature à engager la responsabilité de l'AP-HP lors de l'intervention pratiquée le 4 avril 2003, la cour administrative d'appel, dont l'arrêt est suffisamment motivé sur ce point, n'a pas inexactement qualifié les faits qui lui étaient soumis ; <br/>
<br/>
              4. Considérant, d'autre part, que, pour écarter l'existence d'une faute dans la surveillance post-opératoire, l'arrêt attaqué retient qu'il résulte de l'instruction que l'évolution de l'état de santé de la patiente entre le 5 et le 12 avril 2003, marquée par un état fébrile et des douleurs, a fait l'objet d'une surveillance étroite, que le délai entre les premiers symptômes et la deuxième intervention, délai pendant lequel des examens radiographiques ont été entrepris, permettant d'écarter certaines hypothèses avant qu'un scanner et une coelioscopie soient réalisés, ne révèle pas un retard fautif et que, dès que l'anomalie intra-abdominale a été détectée, une nouvelle intervention en urgence a été décidée ; que l'arrêt n'est pas entaché sur ce point d'une erreur de qualification juridique des faits ;<br/>
<br/>
              Sur la prise en charge du dommage au titre de la solidarité nationale :<br/>
<br/>
              5. Considérant qu'il résulte du II de l'article L. 1142-1 précité du code de la santé publique que l'ONIAM doit assurer, au titre de la solidarité nationale, la réparation des dommages résultant directement d'actes de prévention, de diagnostic ou de soins à la double condition qu'ils présentent un caractère d'anormalité au regard de l'état de santé du patient comme de l'évolution prévisible de cet état et que leur gravité excède le seuil défini à l'article D. 1142-1 du même code ; que la condition d'anormalité du dommage prévue par ces dispositions doit toujours être regardée comme remplie lorsque l'acte médical a entraîné des conséquences notablement plus graves que celles auxquelles le patient était exposé de manière suffisamment probable en l'absence de traitement ; que, lorsque les conséquences de l'acte médical ne sont pas notablement plus graves que celles auxquelles le patient était exposé par sa pathologie en l'absence de traitement, elles ne peuvent être regardées comme anormales sauf si, dans les conditions où l'acte a été accompli, la survenance du dommage présentait une probabilité faible ; qu'ainsi, elles ne peuvent être regardées comme anormales au regard de l'état du patient lorsque la gravité de cet état a conduit à pratiquer un acte comportant des risques élevés dont la réalisation est à l'origine du dommage ; <br/>
<br/>
              6. Considérant que, pour juger que l'accident survenu lors de l'intervention pratiquée le 4 avril 2003 n'avait pas eu des conséquences anormales au regard de l'état de santé initial de Mme B...comme de l'évolution prévisible de cet état, la cour a estimé que les troubles résultant de cet accident n'étaient pas notablement plus graves que ceux auxquels l'intéressée était exposée par l'obésité sévère dont elle était atteinte ; que, toutefois, une telle circonstance n'était pas, à elle seule, de nature à exclure un droit à réparation au titre de la solidarité nationale ; qu'en rejetant les conclusions tendant à une indemnisation par l'ONIAM sans vérifier que le dommage ne résultait pas de la réalisation d'un risque de l'intervention présentant une faible probabilité, la cour a commis une erreur de droit ; <br/>
<br/>
              Sur l'évaluation des préjudices :<br/>
<br/>
              7. Considérant qu'en estimant qu'il n'était pas établi que la mère de Mme B... avait dû l'assister à son domicile de manière permanente du 27 avril au 27 mai 2003 et que son père avait dû l'assister sept heures par jour du 27 mai au 18 juin suivants, la cour administrative d'appel a porté une appréciation souveraine sur les pièces du dossier qui lui était soumis, sans les dénaturer ;<br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que l'arrêt attaqué doit être annulé en tant seulement qu'il rejette les conclusions de Mme B...dirigées contre l'ONIAM ;<br/>
<br/>
              Sur l'application des dispositions de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991 relative à l'aide juridique : <br/>
<br/>
              9. Considérant que Mme B...a obtenu le bénéfice de l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Fabiani, Luc-Thaler, avocat de Mme B..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'ONIAM le versement à la SCP Fabiani, Luc-Thaler de la somme de 3 000 euros ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de Mme B..., qui n'est pas la partie perdante dans la présente instance, la somme demandée sur leur fondement par l'ONIAM ; <br/>
<br/>
              10. Considérant, en revanche, que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que les sommes demandées par l'avocat de Mme B... et par la CPAM de l'Essonne soient mises à la charge de l'AP-HP, qui n'est pas, dans la présente instance, la partie perdante ; qu'elles font de même obstacle à ce que la somme demandée par l'ONIAM soit mise à la charge de MmeB..., qui obtient la cassation de l'arrêt en tant qu'il concerne l'indemnisation par la solidarité nationale ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme B...et de la CPAM de l'Essonne les sommes que l'AP-HP demande au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 31 janvier 2013 est annulé en tant qu'il rejette les conclusions de Mme B...dirigées contre l'ONIAM.<br/>
<br/>
Article 2 : L'affaire est renvoyée dans cette mesure à la cour administrative d'appel de Paris.<br/>
<br/>
Article 3 : L'ONIAM versera à la SCP Fabiani, Luc-Thaler, avocat de MmeB..., la somme de 3 000 euros en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
<br/>
Article 4 : Le surplus des conclusions du pourvoi de Mme B...est rejeté.<br/>
<br/>
		Article 5 : Le pourvoi de la CPAM de l'Essonne est rejeté.<br/>
<br/>
Article 6 : Les conclusions présentées par l'ONIAM et par l'AP-HP au titre de l'article L.  761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 7 : La présente décision sera notifiée à Mme A...B..., à l'Assistance publique-Hôpitaux de Paris, à l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales et la caisse primaire d'assurance maladie de l'Essonne.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
