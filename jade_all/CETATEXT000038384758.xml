<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038384758</ID>
<ANCIEN_ID>JG_L_2019_04_000000428401</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/38/47/CETATEXT000038384758.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 16/04/2019, 428401, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-04-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428401</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Charles-Emmanuel Airy</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:428401.20190416</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 25 février 2019 au secrétariat du contentieux du Conseil d'Etat, M. et Mme A...B...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir les paragraphes numéros 1, 20 et 30 des commentaires administratifs publiés les 12 septembre 2012 et 11 février 2014 au Bulletin officiel des finances publiques (BOFiP)  impôts sous la référence BOI-RPPM-RCM-20-10-20-80 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code général des impôts, notamment le 2° du 7 de l'article 158 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Charles-Emmanuel Airy, auditeur,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. M. et Mme B...demandent au Conseil d'Etat d'annuler les paragraphes numéros 1, 20 et 30 des commentaires administratifs publiés les 12 septembre 2012 et 11 février 2014 au Bulletin officiel des finances publiques (BOFiP) - impôts sous la référence BOI-RPPM-RCM-20-10-20-80 au motif que ces énonciations réitèrent des dispositions elles mêmes contraires aux droits et libertés garantis par la Constitution. <br/>
<br/>
              Sur la recevabilité de la requête :<br/>
<br/>
              2. Pour justifier de la recevabilité de leur requête, les requérants font état de ce qu'ils ont fait l'objet de rehaussements d'impositions au titre des années 2014 et 2015 sur le fondement des dispositions du c de l'article 111 et du 1° du 1 de l'article 109 du code général des impôts et que la majoration d'assiette prévue par les dispositions du 2° du 7 de l'article 158 du même code leur a été appliquée. L'intérêt dont ils se prévalent ainsi ne leur donne toutefois qualité pour demander l'annulation des commentaires administratifs qu'ils contestent qu'en tant qu'ils concernent l'application des dispositions du 2° du 7 de l'article 158 du code général des impôts aux revenus taxés sur le fondement du c de l'article 111 du code général des impôts et du 1° du 1 de l'article 109 du même code. Il en résulte que leur requête n'est recevable que dans cette mesure. <br/>
<br/>
              Sur la question prioritaire de constitutionnalité :<br/>
<br/>
               3. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé, y compris pour la première fois en cassation, à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
               4. Aux termes du 7 de l'article 158 du code général des impôts : " Le montant des revenus et charges énumérés ci-après, retenu pour le calcul de l'impôt selon les modalités prévues à l'article 197, est multiplié par 1, 25. Ces dispositions s'appliquent : / (...) 2° Aux revenus distribués mentionnés aux c à e de l'article 111, aux bénéfices ou revenus mentionnés à l'article 123 bis et aux revenus distribués mentionnés à l'article 109 résultant d'une rectification des résultats de la société distributrice ". Aux termes du 1 de l'article 109 du même code : " Sont considérés comme revenus distribués : / 1° Tous les bénéfices ou produits qui ne sont pas mis en réserve ou incorporés au capital ; / 2° Toutes les sommes ou valeurs mises à la disposition des associés, actionnaires ou porteurs de parts et non prélevées sur les bénéfices (...) ". Aux termes de l'article 111 du même code : " Sont notamment considérés comme revenus distribués:/  (....) c. les rémunérations et avantages occultes (...) ".<br/>
<br/>
               5. Les dispositions précitées du 7 de l'article 158 du code général des impôts ont été introduites par le législateur dans le cadre d'une réforme globale de l'impôt sur le revenu qui prévoyait la suppression, à compter du 1er janvier 2006, de l'abattement de 20 % dont bénéficiaient les traitements, salaires, pensions et rentes viagères, en application du a du 5 de l'article 158 du code général des impôts, ainsi que les revenus professionnels des adhérents d'un centre de gestion ou d'une association bénéficiant d'un agrément, en application du 4 bis du même article, et la compensation de cette suppression par une réduction équivalente des taux du barème de l'impôt sur le revenu pour tous les contribuables. Compte tenu de la portée générale de cette mesure, le législateur a prévu, pour réserver aux seuls revenus précités le bénéfice de règles fiscales plus favorables, des mécanismes de majoration pour les contribuables ou les catégories de revenus imposables qui ne bénéficiaient pas jusqu'alors de l'abattement de 20 %, notamment pour les revenus de capitaux mobiliers. A cette fin, le 2° du 7 de l'article 158 du code général des impôts prévoit notamment, pour déterminer l'assiette imposable, l'application d'un coefficient de 1,25 pour les avantages occultes mentionnés aux c) à e) de l'article 111, les revenus distribués mentionnés à l'article 109 résultant d'une rectification des résultats de la société distributrice et les revenus de l'article 123 bis du même code, qui sont imposés dans la catégorie des revenus de capitaux mobiliers. <br/>
<br/>
               6. Aux termes de l'article 13 de la Déclaration des droits de l'homme et du citoyen de 1789 : " Pour l'entretien de la force publique, et pour les dépenses d'administration, une contribution commune est indispensable : elle doit être également répartie entre tous les citoyens, en raison de leurs facultés ". Cette exigence ne serait pas respectée si l'impôt revêtait un caractère confiscatoire ou faisait peser sur une catégorie de contribuables une charge excessive au regard de leurs facultés contributives. En vertu de l'article 34 de la Constitution, il appartient au législateur de déterminer, dans le respect des principes constitutionnels et compte tenu des caractéristiques de chaque impôt, les règles selon lesquelles doivent être appréciées les facultés contributives. En particulier, pour assurer le respect du principe d'égalité devant les charges publiques, il doit fonder son appréciation sur des critères objectifs et rationnels en fonction des buts qu'il se propose. Cette appréciation ne doit cependant pas entraîner de rupture caractérisée de l'égalité devant les charges publiques.<br/>
<br/>
               7. M. et Mme B...soutiennent notamment qu'en prévoyant l'imposition des revenus visés au c de l'article 111 du code général des impôts et, lorsqu'ils résultent d'une rectification des résultats de la société distributrice, au 1° du 1 de l'article 109 du même code à l'impôt sur le revenu sur une base de 125 % de leur montant, les dispositions du 2° du 7 de l'article 158 du code général des impôts sont susceptibles, compte tenu du cumul de l'impôt sur le revenu, de la contribution exceptionnelle sur les hauts revenus et des autres prélèvements assis sur les mêmes revenus, de faire peser sur eux une imposition revêtant un caractère confiscatoire, portant ainsi atteinte au principe d'égalité devant les charges publiques. La question ainsi soulevée revêt un caractère sérieux. <br/>
<br/>
              8. Les dispositions en cause étant applicables au litige et n'ayant pas été déclarées conformes à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, il y a lieu de renvoyer au Conseil constitutionnel, dans cette mesure, la question prioritaire de constitutionnalité invoquée.<br/>
<br/>
<br/>
<br/>                        D E C I D E :<br/>
                                     --------------<br/>
<br/>
Article 1er : La question de la conformité aux droits et libertés garantis par la Constitution, notamment au principe d'égalité devant les charges publiques, des dispositions du 2° du 7 de l'article 158 du code général des impôts, en tant qu'elles s'appliquent aux revenus taxés sur le fondement du c de l'article 111 du code général des impôts et du 1° du 1 de l'article 109 du même code, est renvoyée au Conseil constitutionnel.<br/>
<br/>
Article 2 : Il est sursis à statuer sur les conclusions de la requête de M. et MmeB.... <br/>
<br/>
Article 3 : La présente décision sera notifiée à M. et Mme A...B...et au ministre de l'action et des comptes publics.<br/>
Copie en sera adressée au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
