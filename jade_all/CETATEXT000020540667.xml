<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000020540667</ID>
<ANCIEN_ID>J0_L_2009_03_000000703189</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/20/54/06/CETATEXT000020540667.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Cour Administrative d'Appel de Versailles, 4ème Chambre, 03/03/2009, 07VE03189, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2009-03-03</DATE_DEC>
<JURIDICTION>Cour Administrative d'Appel de Versailles</JURIDICTION>
<NUMERO>07VE03189</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème Chambre</FORMATION>
<TYPE_REC>excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>Mme CHELLE</PRESIDENT>
<AVOCATS>TERREL</AVOCATS>
<RAPPORTEUR>Mme Emmanuelle  BORET</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme JARREAU</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 19 décembre 2007, présentée pour M. Youcef X, demeurant chez Mme Y ..., par Me Terrel ; M. X demande à la Cour :<br/>
<br/>
       1°) d'annuler le jugement n° 0707833 du 20 novembre 2007 par lequel le Tribunal administratif de Cergy-Pontoise a rejeté sa demande tendant à l'annulation de l'arrêté du 7 juin 2007 par lequel le préfet de la Seine-Saint-Denis a refusé de lui délivrer un certificat de résidence et a assorti son refus d'une obligation de quitter le territoire français à destination de l'Algérie ;<br/>
<br/>
       2°) d'annuler pour excès de pouvoir cet arrêté ; <br/>
<br/>
       3°) d'enjoindre au préfet de la Seine-Saint-Denis de lui délivrer un certificat de résidence et, à titre subsidiaire, de procéder au réexamen de sa situation ;<br/>
<br/>
       4°) de mettre à la charge de l'Etat la somme de 1 500 euros en application des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
       Il soutient que l'arrêté contesté n'est pas suffisamment motivé ; que la gravité de son état de santé nécessite une prise en charge médicale qui ne peut être assurée dans son pays d'origine ; qu'ainsi, ledit arrêté a été pris en violation de l'article 6-7 de l'accord franco-algérien du 27 décembre 1968 modifié et de l'article 3 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; que le préfet de la Seine-Saint-Denis a également méconnu son droit au respect de sa vie privée et familiale, garanti par l'article 6-5 de l'accord franco-algérien et l'article 8 de la convention précitée ; qu'enfin, compte tenu de l'ensemble des éléments rappelés ci-dessus, l'appréciation de sa situation est entachée d'une erreur manifeste ;<br/>
       ..................................................................................................<br/>
<br/>
       Vu les autres pièces du dossier ;<br/>
<br/>
       Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
       Vu l'accord franco-algérien du 27 décembre 1968 modifié ;<br/>
<br/>
       Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
       Vu la loi n° 79-587 du 11 juillet 1979 ;<br/>
<br/>
       Vu le code de justice administrative ;<br/>
<br/>
       Les parties ayant été régulièrement averties du jour de l'audience ;<br/>
<br/>
       Après avoir entendu au cours de l'audience publique du 10 février 2009 :<br/>
<br/>
       - le rapport de Mme Boret, premier conseiller,<br/>
       - et les conclusions de Mme Jarreau, rapporteur public ;<br/>
<br/>
       Considérant, en premier lieu, que l'arrêté attaqué comporte les éléments de droit et de fait qui, au regard des stipulations de l'accord franco-algérien, justifient le refus de séjour opposé à M. X,ainsi que le visa des dispositions de l'article L. 511-1 I du code de l'entrée et du séjour des étrangers et du droit d'asile qui fonde l'obligation de quitter le territoire ; que, dès lors, cet arrêté satisfait à l'exigence de motivation prévue par la loi du 11 juillet 1979 ; <br/>
<br/>
       Considérant, en deuxième lieu, qu'aux termes de l'article 6 de l'accord franco-algérien du 27 décembre 1968 modifié : « (...) Le certificat de résidence d'un an portant la mention &#147;vie privée et familiale&#148; est délivré de plein droit : (...) 7. Au ressortissant algérien, résidant habituellement en France, dont l'état de santé nécessite une prise en charge médicale dont le défaut pourrait entraîner pour lui des conséquences d'une exceptionnelle gravité, sous réserve qu'il ne puisse pas effectivement bénéficier d'un traitement approprié dans son pays (...) » ; <br/>
<br/>
       Considérant que si M. X fait valoir qu'il souffre de plusieurs pathologies, en particulier d'un diabète non insulinodépendant, et qu'en conséquence son état de santé nécessite un suivi médical régulier, il ressort cependant des pièces du dossier et, notamment, de l'avis motivé du médecin inspecteur de santé publique en date du 4 avril 2007, que ne contredisent pas les certificats médicaux produits par l'intéressé, que, d'une part, le défaut d'une prise en charge médicale n'entraînerait pas des conséquences d'une exceptionnelle gravité et, d'autre part, qu'il peut bénéficier d'un traitement approprié dans son pays d'origine ; qu'ainsi, en opposant à M. X une décision de refus de séjour, assortie d'une obligation de quitter le territoire, le préfet de la Seine-Saint-Denis n'a pas méconnu les stipulations de l'article 6-7 de l'accord franco-algérien du 27 décembre 1968 modifié ;<br/>
<br/>
       Considérant, en troisième lieu, qu'aux termes de l'article 6 de l'accord franco-algérien du 27 décembre 1968 susvisé : « (...) Le certificat de résidence d'un an portant la mention « vie privée et familiale » est délivré de plein droit : (...) 5. Au ressortissant algérien (...) dont les liens personnels et familiaux en France sont tels que le refus d'autoriser son séjour porterait à son droit au respect de sa vie privée et familiale une atteinte disproportionnée au regard des motifs du refus (...) » ; qu'aux termes de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : « 1. Toute personne a droit au respect de sa vie privée et familiale, de son domicile et de sa correspondance. 2. Il ne peut y avoir ingérence dans l'exercice de ce droit que pour autant que cette ingérence est prévue par la loi et qu'elle constitue une mesure qui, dans une société démocratique, est nécessaire à la sécurité nationale, à la sûreté publique, au bien-être économique du pays, à la défense de l'ordre et à la prévention des infractions pénales, à la protection de la santé ou de la morale, ou à la protection des droits et libertés d'autrui » ; <br/>
<br/>
       Considérant que M. X fait valoir que son père étant décédé en Algérie, ses liens familiaux sont en France où il vit avec sa mère, titulaire d'une carte de résident, et son beau-père, handicapé, auquel il apporte une aide indispensable, et où se trouvent également ses deux soeurs en situation régulière ; que, toutefois, il ne ressort pas des pièces du dossier qu'une autre personne ne pourrait apporter au beau-père de M. X l'assistance nécessaire ; que le requérant, qui est entré en France en 2003 à l'âge de 40 ans, n'établit pas être dépourvu d'attaches familiales dans son pays d'origine ; qu'ainsi, compte tenu de l'ensemble des circonstances de l'espèce, l'arrêté du préfet de la Seine-Saint-Denis n'a pas porté au droit de l'intéressé au respect de sa vie privée et familiale une atteinte disproportionnée aux buts en vue desquels il a été pris ; que cet arrêté n'a donc méconnu ni les stipulations de l'article 6-5 de l'accord franco-algérien ni celles de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
<br/>
       Considérant, en quatrième lieu, que pour les mêmes motifs que ceux exposés ci-dessus, il ne ressort pas des pièces du dossier qu'en prenant une mesure d'éloignement à l'encontre de M. X, le préfet de la Seine-Saint-Denis ait fait une appréciation manifestement erronée des conséquences de la gravité de cette mesure sur la situation personnelle de l'intéressé ; qu'il n'est pas davantage établi qu'en raison de son état de santé, un retour en Algérie l'exposerait à des traitements contraires à l'article 3 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
       Considérant qu'il résulte de ce qui précède que M. X n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le Tribunal administratif de Cergy-Pontoise a rejeté sa demande ; que les conclusions à fin d'injonction et celles tendant en application des dispositions de l'article L. 761-1 du code de justice administrative doivent être rejetées par voie de conséquence ; <br/>
<br/>
<br/>
DECIDE :<br/>
<br/>
<br/>
       Article 1er : La requête de M. X est rejetée.<br/>
<br/>
''<br/>
''<br/>
''<br/>
''<br/>
N° 07VE03189		2<br/>
<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
