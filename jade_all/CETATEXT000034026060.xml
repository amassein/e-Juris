<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034026060</ID>
<ANCIEN_ID>JG_L_2017_02_000000387713</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/02/60/CETATEXT000034026060.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 10/02/2017, 387713, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-02-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>387713</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX ; BALAT</AVOCATS>
<RAPPORTEUR>M. Sylvain Monteillet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:387713.20170210</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Besançon de condamner la commune de Frotey-lès-Vesoul (Haute-Saône) à l'indemniser de divers préjudices qu'il a subis, en raison de fautes commises par cette commune dans la gestion de sa carrière de fonctionnaire territorial.<br/>
<br/>
              Par un jugement n° 1201646 du 15 octobre 2013, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par une ordonnance n° 14NC00394 du 30 janvier 2015, enregistrée le 5 février 2015 au secrétariat du contentieux du Conseil d'Etat, le président de la cour administrative d'appel de Nancy a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi enregistré le 17 février 2014 au greffe de cette cour, présenté par M.B....<br/>
<br/>
              Par ce pourvoi et deux nouveaux mémoires, enregistrés le 13 avril 2015 et 14 avril 2016 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'attribuer le jugement de sa requête à la cour administrative d'appel de Nancy ;<br/>
<br/>
              2°) subsidiairement, d'annuler le jugement n° 1201646 du 15 octobre 2013 du tribunal administratif de Besançon ;<br/>
<br/>
              3°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              4°) de mettre à la charge de la commune de Frotey-les-Vesoul une somme de 3 000 euros au titre des dispositions de l'article L.761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Sylvain Monteillet, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de M. B...et à Me Balat, avocat de la commune de Frotey-les-Vesoul ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. B..., adjoint technique de seconde classe affecté dans les services techniques de la commune de Frotey-lès-Vesoul, a été victime d'un accident à son domicile le 30 septembre 2006. Après avoir bénéficié de congés de maladie pendant un an, il a été placé d'office, par arrêtés successifs, en disponibilité à partir du 2 octobre 2007 jusqu'à une courte période de reprise d'activité, du 3 au 9 juin 2009. A l'issue de cette période d'activité et après épuisement de ses droits à congés de maladie ordinaire, il a de nouveau été placé d'office en disponibilité par arrêtés successifs, du 10 juin 2010 au 7 avril 2014, date de sa mise à la retraite pour invalidité. Le 3 décembre 2012, M. B... a saisi le tribunal administratif de Besançon de conclusions tendant à ce que la commune de Frotey-lès-Vesoul soit condamnée à l'indemniser des divers préjudices qu'il estime avoir subi, en raison des agissements fautifs de cette commune. Le tribunal administratif a rejeté ses demandes par un jugement du 15 octobre 2013, que M. B...a contesté devant la cour administrative d'appel de Nancy. Par une ordonnance du 30 janvier 2015, la présidente de cette cour, estimant que ce jugement avait été rendu en premier et dernier ressort et que la requête formée par M. B...devant cette cour revêtait dès lors le caractère d'un pourvoi en cassation, a transmis cette requête au Conseil d'Etat.<br/>
<br/>
              2. Il résulte des dispositions du deuxième alinéa de l'article R. 811-1 du code de justice administrative, combinées avec celles de l'article R. 222-13 du même code, dans leur rédaction applicable au litige, que le tribunal administratif statue en premier et dernier ressort sur les litiges relatifs à la situation individuelle des agents publics, à l'exception de ceux concernant l'entrée au service, la discipline ou la sortie du service, sauf pour les recours comportant des conclusions tendant au versement ou à la décharge de sommes d'un montant supérieur au montant déterminé par les articles R. 222-14 et R. 222-15 de ce code. L'article R. 222-14 fixe ce montant à 10 000 euros. Enfin, l'article R. 222-15 précise que ce montant est déterminé par la valeur totale des sommes demandées dans la requête introductive d'instance.<br/>
<br/>
              3. Dans sa requête introductive d'instance présentée le 3 décembre 2012 devant le tribunal administratif de Besançon, M. B...concluait à ce que la commune de Frotey-lès-Vesoul fût condamnée à lui verser " quatre années de salaires bruts (1 467,78 &#128;) ". Compte-tenu de la formulation ainsi adoptée par M.B..., précisant le montant de son salaire mensuel ainsi que la durée au titre de laquelle il demandait à être indemnisé, par simple multiplication du montant ainsi énoncé, l'intéressé devait en tout état de cause être regardé comme sollicitant une  indemnisation nécessairement supérieure à 10 000 euros. Dès lors, le Conseil d'Etat n'est pas compétent pour connaître des conclusions de la demande de M.B..., laquelle doit être regardée comme revêtant le caractère d'un appel dirigé contre le jugement du tribunal administratif de Besançon du 15 octobre 2013. Il y a lieu, par suite, de transmettre cette requête à la cour administrative d'appel de Nancy.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement des conclusions de la requête de M. B...est attribué à la cour administrative d'appel de Nancy. <br/>
Article 2 : La présente décision sera notifiée à M. A...B..., à la commune de Frotey-lès-Vesoul et à la présidente de la cour administrative d'appel de Nancy.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
