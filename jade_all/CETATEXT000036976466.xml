<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036976466</ID>
<ANCIEN_ID>JG_L_2018_06_000000414513</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/97/64/CETATEXT000036976466.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  4ème chambre jugeant seule, 01/06/2018, 414513, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-06-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>414513</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 4ème chambre jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>Mme Sara-Lou Gerber</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:414513.20180601</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés les 22 septembre et 26 décembre 2017 au secrétariat du contentieux du Conseil d'Etat, M. A... B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 18 juillet 2017 du Conseil national de l'ordre des médecins, statuant en formation restreinte, le suspendant à titre temporaire du droit d'exercer la médecine ; <br/>
<br/>
              2°) de mettre à la charge du Conseil national de l'ordre des médecins la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sara-Lou Gerber, auditeur,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de M. B...et à la SCP Matuchansky, Poupot, Valdelièvre, avocat du Conseil national de l'ordre des médecins ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article R. 4124-3 du code de la santé publique : " I. - Dans le cas d'infirmité ou d'état pathologique rendant dangereux l'exercice de la profession, la suspension temporaire du droit d'exercer est prononcée par le conseil régional ou interrégional pour une période déterminée, qui peut, s'il y a lieu, être renouvelée. (...) / II. - La suspension ne peut être ordonnée que sur un rapport motivé établi à la demande du conseil régional ou interrégional par trois médecins désignés comme experts, le premier par l'intéressé, le deuxième par le conseil régional ou interrégional et le troisième par les deux premiers experts. /  III. - En cas de carence de l'intéressé lors de la désignation du premier expert ou de désaccord des deux experts lors de la désignation du troisième, la désignation est faite à la demande du conseil par ordonnance du président du tribunal de grande instance dans le ressort duquel se trouve la résidence professionnelle de l'intéressé. Cette demande est dispensée de ministère d'avocat. / (...) VI. - Si le conseil régional ou interrégional n'a pas statué dans le délai de deux mois à compter de la réception de la demande dont il est saisi, l'affaire est portée devant le Conseil national de l'ordre " ; <br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier que le conseil départemental de la Sarthe a saisi le conseil régional des Pays de la Loire de l'ordre des médecins de la situation de M.B..., médecin qualifié spécialiste en psychiatrie, sur le fondement des dispositions de l'article R. 4124-3 du code de la santé publique qui viennent d'être citées ; que, le conseil régional n'ayant pas statué dans le délai de deux mois, le dossier a été transmis au Conseil national de l'ordre des médecins, lequel a, en formation restreinte, suspendu, par une décision du 18 juillet 2017, le droit de M. B... d'exercer la médecine pendant un an et subordonné la reprise de son exercice professionnel, à l'issue de ce délai, à la réalisation d'une nouvelle expertise médicale ; que M. B...demande l'annulation pour excès de pouvoir de cette décision ; <br/>
              3. Considérant, en premier lieu, qu'il résulte des dispositions de l'article R. 4124-3 du code de la santé publique citées au point 1 que lorsque le praticien dont l'aptitude à exercer la médecine est mise en doute ne désigne pas d'expert, il est suppléé à cette carence par une désignation de l'expert par ordonnance du président du tribunal de grande instance territorialement compétent ; qu'il ressort des pièces du dossier que, par un courrier du 2 décembre 2016, M. B...a été informé de ce qu'il disposait d'un délai de huit jours pour désigner un expert de son choix ; qu'il n'est pas contesté qu'il n'a apporté aucune réponse à ce courrier ; que, par suite, M. B..., qui n'a pas procédé aux diligences qui lui incombaient, n'est pas fondé à soutenir que la décision attaquée a été prise à l'issue d'une procédure irrégulière, faute, pour lui, d'avoir été mis à même de désigner l'un des trois experts ;  <br/>
<br/>
              4. Considérant, en deuxième lieu, que la décision attaquée, qui énonce, au-delà de la seule reprise de citations du rapport des experts, les éléments de droit et de fait sur lesquels s'est fondé le Conseil national de l'ordre des médecins, est suffisamment motivée ;<br/>
<br/>
              5. Considérant, en dernier lieu, qu'en estimant, au vu de l'ensemble des pièces du dossier, notamment du rapport des experts et des témoignages concordants de collègues et de patients sur le comportement de M.B..., que celui-ci présentait un état pathologique rendant dangereux l'exercice de la profession, le Conseil national de l'ordre des médecins a fait une exacte application des dispositions de l'article R. 4124-3 du code de la santé publique ; qu'en prononçant sa suspension du droit d'exercer la médecine pour une durée d'un an, le Conseil national de l'ordre des médecins, dont la décision n'avait pas à être plus motivée sur ce point, n'a pas entaché sa décision d'erreur manifeste d'appréciation ; qu'est, à ce titre, sans incidence la circonstance qu'eu égard à l'âge de l'intéressé, cette durée équivaudrait à une cessation définitive de son activité ;<br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que la requête de M. B... doit être rejetée, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. B... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. A...B...et au Conseil national de l'ordre des médecins.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
