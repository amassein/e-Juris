<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029800113</ID>
<ANCIEN_ID>JG_L_2014_10_000000381923</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/80/01/CETATEXT000029800113.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème SSJS, 23/10/2014, 381923, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-10-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>381923</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Esther de Moustier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:381923.20141023</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique enregistrés les 30 juin et 12 août 2014, M. B... A...demande l'annulation pour excès de pouvoir du décret n° 2014-180 du 18 février 2014, modifié par le décret n° 2014-351 du 19 mars 2014, portant délimitation des cantons dans le département de l'Isère.<br/>
<br/>
              Il soutient que : <br/>
              - la nouvelle délimitation des cantons  a été décidée sans concertation avec les élus ;<br/>
              - la consultation des conseillers généraux a été irrégulière dès lors que la carte présentée par le préfet au conseil général était incomplète en ce qui concerne le canton de Grenoble 2 ; <br/>
              - la délimitation des cantons est arbitraire dès lors que sa seule logique est démographique et politique, qu'elle ne tient compte ni des structures intercommunales, ni des " bassins de vie ", ni des difficultés de communication et qu'elle ne respecte pas l'intégrité des communes ;<br/>
              - la délimitation du canton de Matheysine-Trièves regroupe deux territoires qui n'ont rien en commun sur le plan géographique et constituent des bassins de vie et économiques distincts et elle ne tient pas compte des temps de trajet à l'intérieur du canton et de leur difficulté, notamment en hiver ; <br/>
              - la délimitation du canton du Pont-de-Claix est arbitraire dès lors qu'elle n'intègre pas la commune de Claix, membre de l'ancien canton, mais s'étend à des communes appartenant à une autre circonscription législative située de l'autre côté d'une barrière naturelle, au mépris de la continuité géographique, administrative et historique ainsi que des " bassins de vie " existants, qu'elle n'inclut pas la commune de Fontaine et que l'ancien canton de Vif aurait pu être maintenu en totalité dans le nouveau canton en respectant le critère démographique ; <br/>
              - la délimitation du canton de Grenoble 2 est arbitraire dès lors qu'elle a été faite au mépris des limites des structures intercommunales et circonscriptions législatives, ainsi que des entités géographiques et topographiques, qu'elle regroupe des communes de plaine, de montagne et du centre-ville de Grenoble qui n'ont aucun point commun, et que la population de ce canton est susceptible de dépasser 49 600 habitants en raison d'un projet de construction ; <br/>
              - le nouveau canton de Grenoble 1, d'une population de 49 756 habitants,  dépassera prochainement le seuil démographique de 49 920 habitants en raison de  projets d'urbanisation.<br/>
<br/>
              Par un mémoire en défense, enregistré le 21 juillet 2014, le ministre de l'intérieur conclut au rejet de la requête. <br/>
<br/>
              Il soutient que :<br/>
              - le Gouvernement n'est tenu par aucune disposition législative ou réglementaire de consulter les maires ou les élus du département ;<br/>
              - l'assemblée départementale a été informée et mise à même de se prononcer utilement sur le projet ; <br/>
              - aucun principe législatif ou réglementaire n'impose le respect des limites des circonscriptions législatives et des structures intercommunales, de la cohérence géographique, historique, économique, sociale et culturelle des territoires, alors que le Gouvernement, seulement tenu de respecter les critères de l'article L. 3113-2 du code général des collectivités territoriales, s'est attaché à tenir compte de ces considérations ;<br/>
              - le canton de Matheysine-Trièves, défini dans un périmètre géographique cohérent caractérisé par une très faible densité de population, se situe dans le cadre des exceptions démographiques justifiées par le relief, la topographie, l'enclavement et la superficie ; <br/>
              - le canton du Pont-au-Claix présente un caractère continu que ne saurait remettre en cause la difficulté de certaines liaisons routières, a une cohérence géographique et l'intégration de la commune de Claix au canton de Fontaine-Vercors était nécessaire pour des raisons d'équilibre démographique ;  <br/>
              - les cantons de Grenoble 1 et 2 respectent les exigences démographiques, qui s'apprécient au regard des chiffres authentifiés de population du décret du 27 décembre 2012, et le canton de Grenoble 2 est au surplus défini de manière cohérente au sein de l'agglomération grenobloise à partir de l'intégralité des communes de l'ancien canton de Saint-Egrève ;<br/>
              - le détournement de pouvoir allégué n'est pas établi.<br/>
<br/>
<br/>
<br/>
              Vu :<br/>
<br/>
              - les autres pièces du dossier ;<br/>
<br/>
              - le code électoral ;<br/>
<br/>
              - le code général des collectivités territoriales ;<br/>
<br/>
              - la loi n° 2013-403 du 17 mai 2013 ;<br/>
<br/>
              - le décret n° 2013-938 du 18 octobre 2013, modifié par le décret n° 2014-112 du 6 février 2014 ;<br/>
<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Esther de Moustier, auditeur,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public. <br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que l'article L. 3113-2 du code général des collectivités territoriales, dans sa version issue de la loi du 17 mai 2013, prévoit que : " I. - Les modifications des limites territoriales des cantons, les créations et suppressions de cantons et le transfert du siège de leur chef-lieu sont décidés par décret en Conseil d'Etat après consultation du conseil départemental qui se prononce dans un délai de six semaines à compter de sa saisine. A l'expiration de ce délai, son avis est réputé rendu. (...) III. - La modification des limites territoriales des cantons effectuée en application du I est conforme aux règles suivantes :     a) Le territoire de chaque canton est défini sur des bases essentiellement démographiques ; / b) Le territoire de chaque canton est continu ; / c) Est entièrement comprise dans le même canton toute commune de moins de 3 500 habitants ; / IV. - Il n'est apporté aux règles énoncées au III que des exceptions de portée limitée, spécialement justifiées, au cas par cas, par des considérations géographiques ; ou par d'autres impératifs d'intérêt général " ;<br/>
<br/>
              2. Considérant, en premier lieu, que le décret attaqué a été pris sur avis du conseil général de l'Isère, rendu le 22 novembre 2013 après examen du projet présenté par le préfet de l'Isère, dont il est constant qu'il était accompagné d'un rapport de présentation, de tableaux mentionnant la composition des nouveaux cantons et leurs populations, de diverses cartes comprenant notamment une carte relative au nouveau canton de Grenoble 2 ; qu'ainsi,  l'assemblée départementale a été mise à même d'émettre un avis sur les modalités de mise en oeuvre de la nouvelle délimitation des cantons et de faire des propositions spécifiques notamment pour le canton de Grenoble 2 ; que, par suite, le requérant n'est pas fondé à soutenir que la consultation du conseil général aurait été irrégulière ; <br/>
<br/>
              3. Considérant, en deuxième lieu, qu'aucune disposition n'imposait au Gouvernement de consulter l'ensemble des maires et élus du département ; <br/>
<br/>
              4. Considérant, en troisième lieu, qu'il résulte des dispositions du III de l'article L. 3113-2 du code général des collectivités territoriales que le territoire de chaque canton doit être établi sur des bases essentiellement démographiques et qu'il doit être continu ; que l'article 1er du décret attaqué procède à une nouvelle délimitation de l'ensemble des cantons du département de l'Isère compte tenu de l'exigence de réduction du nombre des cantons de ce département de cinquante-huit à vingt-neuf résultant de l'application de l'article L. 191-1 du code électoral en se fondant sur une population moyenne et en rapprochant la population de chaque canton de cette moyenne ; que, si le requérant fait valoir que la nouvelle délimitation des cantons ne répond à aucune autre logique que démographique, il n'est pas soutenu que cette nouvelle délimitation méconnaîtrait les critères définis au III de l'article L. 3113-2 du code général des collectivités territoriales ; <br/>
<br/>
              5. Considérant, en quatrième lieu, qu'aucune disposition n'impose de prévoir que les limites des cantons, qui sont des circonscriptions électorales, coïncident avec les limites des circonscriptions législatives et les périmètres des établissements publics de coopération intercommunale figurant dans le schéma départemental de coopération intercommunale ou des bassins économiques et sociaux ; que la proximité géographique des communes d'un même canton n'est pas au nombre des critères définis à l'article L. 3113-2 du code général des collectivités territoriales ; que, par suite, le requérant ne saurait utilement soutenir que le Gouvernement a commis une erreur de droit en ne tenant pas compte de ces différents éléments pour délimiter les nouveaux cantons ; <br/>
<br/>
              6. Considérant, en cinquième lieu, que s'il soutient que la délimitation du canton de Matheysine-Trièves (n° 15) serait arbitraire au motif que la seule prise en compte du critère démographique a conduit à regrouper en un seul canton de cent vingt-six kilomètres carrés soixante-douze communes réparties entre deux territoires aux caractéristiques géographiques très différentes, constituant des " bassins de vie " et des bassins économiques distincts, et que les temps de trajet entre certaines communes peuvent être très longs et difficiles, ces circonstances ne sont pas de nature à établir l'existence d'une erreur manifeste d'appréciation, alors que ce canton, qui comprend deux ensembles géographiques cohérents, réunit six anciens cantons et cinq établissements publics de coopération intercommunale et que sa superficie importante s'explique par la faible population de cette zone et la volonté de limiter les écarts de population entre les cantons ; <br/>
<br/>
              7. Considérant, en sixième lieu, que le canton périurbain du Pont-de-Claix, situé au sud de l'agglomération grenobloise, qui inclut les communes de l'ancien canton de Vif à l'exception de la commune de Claix, auxquelles s'ajoutent les communes de la partie occidentale de l'ancien canton de Vizille, comprend des communes de la communauté d'agglomération Grenoble Alpes Méditerranée et du sud grenoblois ; qu'il a une cohérence géographique assurée par sa localisation à la convergence des vallées du Sud-Isère, alors même qu'il existe à l'intérieur de ce canton une barrière naturelle séparant en deux parties le canton ; que la commune de Claix, malgré ses liens avec la commune du Pont-de-Claix, pouvait être rattachée au canton de Fontaine-en-Vercors afin que soient respectés les équilibres démographiques entre cantons ; que si le requérant soutient que certaines voies de communication du canton du Pont-de-Claix seraient parfois impraticables, cette circonstance n'est pas de nature à caractériser l'existence d'une rupture de la continuité territoriale exigée par les dispositions du b) du III de l'article L. 3113-2 du code général des collectivités territoriales  ; que le moyen tiré de ce que d'autres délimitations auraient été possibles est sans influence sur la légalité du décret attaqué ;<br/>
<br/>
              8. Considérant, en septième lieu, que l'article 71 du décret du 18 octobre 2013, dans sa rédaction applicable à la date du décret attaqué, dispose que : "  (...) Pour la première délimitation générale des cantons opérée en application de l'article L. 3113-2  du code général des collectivités territoriales, dans sa rédaction résultant de l'article 46 de la loi n° 2013-403 du 17 mai 2013 relative à l'élection des conseillers départementaux, (...), le chiffre de la population municipale auquel il convient de se référer est celui authentifié par le décret n° 2012-1479 du 27 décembre 2012 authentifiant les chiffres des populations (...) " ; qu'il n'est pas contesté que les nouveaux cantons du département de l'Isère ont été délimités sur la base des données figurant dans le décret du 27 décembre 2012 authentifiant les chiffres des populations de métropole, des départements d'outre-mer de la Guadeloupe, de la Guyane, de la Martinique et de la Réunion, de Saint-Barthélemy, de Saint-Martin et de Saint-Pierre-et-Miquelon ; que, pour l'application des dispositions énoncées au a du III de l'article L. 3113-2 du code général des collectivités territoriales, il appartenait au pouvoir réglementaire, pour la prise en compte des bases démographiques, de retenir les chiffres de population constatés et non de simples prévisions ; que, par suite, le moyen tiré de ce que le décret attaqué aurait fait une inexacte application des dispositions rappelées ci-dessus en ne tenant pas compte des perspectives d'évolution démographique des cantons de Grenoble 1 et 2 en raison de projets d'aménagements et de construction ne peut qu'être écarté ;  <br/>
<br/>
              9. Considérant, en huitième lieu, qu'il n'est pas contesté que la nouvelle délimitation des cantons respecte les critères définis à l'article L. 3113-2 du code général des collectivités territoriales ; que, par suite, la circonstance que la commune de Fontaine est restée rattachée à deux cantons distincts que sont ceux de Fontaine-Seyssinet et de Fontaine-Vercors, alors que cette commune, qui excède le seuil de 3 500 habitants, n'est pas soumise à l'exigence du b) du III de ce même article prévoyant qu'elle doit être entièrement comprise dans un même canton, ne caractérise pas une erreur manifeste d'appréciation ;  <br/>
<br/>
              10. Considérant, enfin, que le détournement de pouvoir allégué n'est pas établi ;<br/>
<br/>
              11. Considérant qu'il résulte de ce qui précède que la requête de M. A...doit être rejetée ;<br/>
<br/>
<br/>
<br/>
<br/>
              		D E C I D E :<br/>
              		--------------<br/>
<br/>
Article  1er : La requête de M. A...est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. B...A...et au ministre de l'intérieur.<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
