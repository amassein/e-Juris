<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035179862</ID>
<ANCIEN_ID>JG_L_2017_07_000000395313</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/17/98/CETATEXT000035179862.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 1ère chambres réunies, 12/07/2017, 395313</TITRE>
<DATE_DEC>2017-07-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>395313</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Stéphane Decubber</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:395313.20170712</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire en réplique et de nouveaux mémoires, enregistrés les 15 décembre 2015, 3 février 2016, et les 27 février, 9 mai,  29 mai et 16 juin 2017 au secrétariat du contentieux du Conseil d'Etat, M. A... B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2015-1269 du 13 octobre 2015 portant publication de l'accord entre le Gouvernement de la République française et le Gouvernement de la République italienne relatif au transfert transfrontalier des déchets issus des travaux de construction du tunnel de Tende (ensemble un accord sous forme d'échange de notes verbales portant modification, signées à Paris le 13 mars et le 10 avril 2015), signé à Ajaccio le 26 octobre 2013 ;<br/>
<br/>
              2°) d'annuler pour excès de pouvoir le décret n° 2008-1128 du 3 novembre 2008 portant publication de l'accord entre le Gouvernement de la République française et le Gouvernement de la République italienne relatif à la mise en place d'une gestion unifiée du tunnel de Tende et la construction d'un nouveau tunnel, signé à Paris le 12 mars 2007, l'arrêté du 22 octobre 2007 du préfet des Alpes-Maritimes déclarant d'utilité publique le projet d'aménagement du tunnel neuf de Tende, enfin l'arrêté du 7 août 2012 du même préfet prorogeant les effets de l'arrêté du 22 octobre 2007 déclarant d'utilité publique le projet d'aménagement du tunnel neuf de Tende ;<br/>
<br/>
              3°) de surseoir à statuer afin de poser une question préjudicielle à la Cour de justice de l'Union européenne sur l'interprétation de la convention alpine et de divers règlements et directives ; <br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ;<br/>
              - le protocole d'application de la convention alpine de 1991 dans le domaine des transports, fait à Lucerne le 31 octobre 2000 ;<br/>
              - la convention sur l'évaluation de l'impact de l'environnement dans un contexte transfrontière, signée à Espoo le 25 février 1991 ; <br/>
              - le règlement (CE) n° 1013/2006 du 14 juin 2006 ;<br/>
              - la directive 2002/49/CE du 25 juin 2002 ;<br/>
              - la directive 2008/50/CE du 21 mai 2008 ;<br/>
              - la directive 2011/92/UE du 13 décembre 2011 ;<br/>
              - le code de l'environnement ;<br/>
              - la loi n° 2007-1485 du 18 octobre 2007 ;<br/>
              - le décret n° 53-192 du 14 mars 1953 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Stéphane Decubber, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 28 juin 2017, présentée par M.B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que, par un décret du 3 novembre 2008, le Gouvernement a publié l'accord signé à Paris le 12 mars 2007 avec le Gouvernement de la République italienne relatif à la mise en place d'une gestion unifiée du tunnel de Tende et la construction d'un nouveau tunnel, dont l'approbation avait été autorisée par la loi du 18 octobre 2007 ; que, par un arrêté du 22 octobre 2007 du préfet des Alpes-Maritimes, dont les effets ont été prorogés par un autre arrêté du 7 août 2012 de la même autorité, le projet d'aménagement du tunnel neuf de Tende a été déclaré d'utilité publique ; que, par un décret du 13 octobre 2015, le Président de la République a publié l'accord signé à Ajaccio le 26 octobre 2013 avec le Gouvernement de la République italienne relatif au transfert transfrontalier des déchets issus des travaux de construction du tunnel de Tende et modifié par un échange de notes verbales signées à Paris les 13 mars et 10 avril 2015 ; que M. B... demande l'annulation pour excès de pouvoir du décret du 13 octobre 2015, du décret du 3 novembre 2008 et des arrêtés des 22 octobre 2007 et 7 août 2012 ;<br/>
<br/>
              Sur l'intervention :<br/>
<br/>
              2. Considérant que, eu égard à la nature et à l'objet des litiges, les associations Roya expansion nature et France nature environnement PACA justifient d'un intérêt suffisant pour demander l'annulation des actes attaqués ; qu'ainsi, leur intervention est recevable ;<br/>
<br/>
              Sur les conclusions dirigées contre les arrêtés des 22 octobre 2007 et 7 août 2012 : <br/>
<br/>
              3. Considérant que la décision par laquelle le représentant de l'Etat dans le département déclare d'utilité publique la réalisation d'un projet ou d'une opération n'est pas au nombre des actes mentionnés par l'article R. 311-1 du code de justice administrative ; qu'aucune autre disposition du code de justice administrative ne donne compétence au Conseil d'Etat pour connaître en premier et dernier ressort des conclusions de M. B... tendant à l'annulation pour excès de pouvoir de l'arrêté du 22 octobre 2007 du préfet des Alpes-Maritimes déclarant d'utilité publique le projet d'aménagement du tunnel neuf de Tende et de l'arrêté du 7 août 2012 du même préfet prorogeant les effets de l'arrêté du 22 octobre 2007 déclarant d'utilité publique le projet d'aménagement du tunnel neuf de Tende ; qu'il y a lieu, en application de l'article R. 351-1 du code de justice administrative, d'en attribuer le jugement au tribunal administratif de Nice, compétent pour en connaître en vertu de l'article R. 312-1 du même code ;<br/>
<br/>
              Sur les conclusions dirigées contre le décret du 3 novembre 2008 : <br/>
<br/>
              4. Considérant qu'aux termes de l'article R. 421-1 du code de justice administrative, dans sa rédaction applicable à la date d'introduction du recours : " Sauf en matière de travaux publics, la juridiction ne peut être saisie que par voie de recours formé contre une décision, et ce, dans les deux mois à partir de la notification ou de la publication de la décision attaquée. (...) " ; que le décret du 3 novembre 2008 a été publié au Journal officiel de la République française du 5 novembre 2008 ; que la requête de M. B... n'a été enregistrée au secrétariat du contentieux du Conseil d'Etat que le 15 décembre 2015, soit après l'expiration du délai de recours de deux mois imparti par l'article R. 421-1 du code de justice administrative ; que, dès lors, les conclusions de sa requête tendant à l'annulation pour excès de pouvoir du décret du 3 novembre 2008 doivent être rejetées comme irrecevables ; <br/>
<br/>
              Sur les conclusions dirigées contre le décret du 13 octobre 2015 :<br/>
<br/>
              En ce qui concerne les moyens dirigés par la voie de l'exception contre le décret du 3 novembre 2008 et les arrêtés préfectoraux des 22 octobre 2007 et 7 août 2012 :<br/>
<br/>
              5. Considérant que l'illégalité d'un acte administratif, qu'il soit ou non réglementaire, ne peut être utilement invoquée à l'appui de conclusions dirigées contre une décision administrative que si cette dernière a été prise pour son application ou s'il en constitue la base légale ; que ni les arrêtés préfectoraux des 22 octobre 2007 et 7 août 2012 portant, respectivement, déclaration d'utilité publique du projet d'aménagement du tunnel neuf de Tende et prorogation des effets de cette déclaration, ni le décret du 3 novembre 2008 par lequel le Gouvernement a publié l'accord signé à Paris le 12 mars 2007 avec le Gouvernement de la République italienne relatif à la mise en place d'une gestion unifiée du tunnel de Tende et la construction d'un nouveau tunnel, ne constituent la base légale du décret du 13 octobre 2015 ; que ce dernier décret n'a pas davantage été pris pour l'application des arrêtés préfectoraux des 22 octobre 2007 et 7 août 2012 et du décret du 3 novembre 2008 ; que, dès lors, le requérant ne saurait utilement exciper de leur illégalité à l'encontre du décret du 13 octobre 2015  attaqué ;<br/>
<br/>
              En ce qui concerne les autres moyens :<br/>
<br/>
              6. Considérant qu'aux termes de l'article 53 de la Constitution : " Les traités de paix, les traités de commerce, les traités ou accords relatifs à l'organisation internationale, ceux qui engagent les finances de l'Etat, ceux qui modifient des dispositions de nature législative, ceux qui sont relatifs à l'état des personnes, ceux qui comportent cession, échange ou adjonction de territoire, ne peuvent être ratifiés ou approuvés qu'en vertu d'une loi. / Ils ne prennent effet qu'après avoir été ratifiés ou approuvés. (...) " ; que l'article 55 de la Constitution dispose : " Les traités ou accords régulièrement ratifiés ou approuvés ont, dès leur publication, une autorité supérieure à celle des lois, sous réserve, pour chaque accord ou traité, de son application par l'autre partie " ;<br/>
<br/>
              7. Considérant, en premier lieu, qu'aux termes de l'article 1er du décret du 14 mars 1953 relatif à la ratification et la publication des engagements internationaux souscrits par la France : " Le ministre des affaires étrangères est seul chargé de pourvoir à la ratification et la publication des conventions, accords, protocoles et règlements internationaux dont la France est signataire ou par lesquels la France se trouve engagée. (...) " ; qu'un décret portant publication d'un accord international n'appelle par lui-même aucune mesure d'exécution ; que, par suite, le moyen tiré de ce que le décret attaqué serait irrégulier, faute d'être revêtu du contreseing du ministre chargé de l'environnement, doit être écarté ;<br/>
<br/>
              8. Considérant, en deuxième lieu, qu'il résulte de la combinaison des dispositions citées au point 6 que les traités ou accords entrant dans le champ de l'article 53 de la Constitution et dont la ratification ou l'approbation est intervenue sans avoir été autorisée par la loi ne peuvent être regardés comme régulièrement ratifiés ou approuvés au sens de l'article 55 précité ; qu'il appartient au Conseil d'Etat, statuant au contentieux, en cas de recours pour excès de pouvoir contre un décret publiant un traité ou un accord, de connaître de moyens tirés, d'une part, de vices propres à ce décret, d'autre part, de ce qu'en vertu de l'article 53 de la Constitution, la ratification ou l'approbation de l'engagement international en cause aurait dû être autorisée par la loi ; qu'en revanche, il n'appartient pas au Conseil d'Etat statuant au contentieux de se prononcer sur la conformité du traité ou de l'accord à la Constitution ou à d'autres engagements internationaux ; <br/>
<br/>
              9. Considérant qu'il résulte de ce qui a été dit au point précédent que l'ensemble des moyens tirés de la méconnaissance de divers accords internationaux et directives européennes, de la Charte de l'environnement et, en tout état de cause, de dispositions législatives, qui ne mettent pas en cause des vices propres du décret de publication attaqué, sont sans influence sur l'issue du présent litige et ne peuvent dès lors qu'être écartés comme inopérants  ;<br/>
              10. Considérant, en troisième lieu, que les traités ou accords qui " engagent les finances de l'État ", au sens de l'article 53 de la Constitution, sont ceux qui créent une charge financière certaine et directe pour l'État ; que, toutefois, lorsque les charges financières impliquées par un accord n'excèdent pas, compte tenu de leur nature et de leur montant limité, les dépenses de fonctionnement courant incombant normalement à l'administration, elles ne peuvent pas être regardées comme " engageant les finances de l'État ", au sens de l'article 53 de la Constitution ; que l'accord publié par le décret attaqué, qui a pour objet de prévoir des assouplissements aux règles de procédure prévues par le règlement n° 1013/2006 du Parlement européen et du Conseil du 14 juin 2006 concernant les transferts de déchets, ne crée aucune charge financière certaine et directe pour l'Etat ; que, dès lors, le moyen tiré de ce que l'accord publié par le décret attaqué devait être ratifié par une loi, au motif qu'il engagerait les finances de l'Etat, ne peut qu'être écarté ;<br/>
<br/>
              11. Considérant, en quatrième lieu, que constitue un traité ou un accord " modifiant des dispositions de nature législative ", au sens de l'article 53 de la Constitution, un engagement international dont les stipulations touchent à des matières réservées à la loi par la Constitution ou énoncent des règles qui diffèrent de celles posées par des dispositions de forme législative ; qu'aux termes de l'article 30 du règlement (CE) n° 1013/2006 du Parlement européen et du Conseil du 14 juin 2006 concernant les transferts de déchets : " 1. Dans des cas exceptionnels et si une situation géographique ou démographique particulière le justifie, les États membres peuvent, pour le transfert transfrontalier vers les installations appropriées les plus proches situées dans l'espace frontalier situé entre les deux États membres concernés, conclure des accords bilatéraux prévoyant des assouplissements de la procédure de notification pour le transfert de flux spécifiques de déchets. (...) " ; qu'aux termes de l'article L. 541-40 du code de l'environnement : " I. - L'importation, l'exportation et le transit de déchets sont soumis aux dispositions du règlement (CE) n° 1013/2006 du Parlement européen et du Conseil du 14 juin 2006 concernant les transferts de déchets. / II.   En cas d'exportation de déchets soumise à notification, le notifiant est établi en France. Il en va de même pour la personne, visée au 1 de l'article 18 du règlement mentionné ci-dessus, qui organise un transfert de déchets dispensé de notification en application du 2 et du 4 de l'article 3 du même règlement. / La notification couvre le transfert des déchets depuis un lieu d'expédition unique. / Le notifiant est défini à l'article 2.15 du règlement mentionné ci-dessus. / Le présent article et l'article L. 541-42-2 peuvent être adaptés par la prise d'un accord bilatéral entre les Gouvernements des Etats d'expédition et de destination des déchets, dans les limites prévues par le règlement (CE) n° 1013/2006 du Parlement européen et du Conseil, du 14 juin 2006, concernant les transferts de déchets " ; <br/>
<br/>
              12. Considérant qu'il est soutenu que l'accord publié par le décret attaqué dérogerait illégalement à des dispositions de forme législative, au motif que le deuxième alinéa de son article 1er stipule : " Pour l'application du présent accord, l'obligation d'établissement en France du notifiant de l'article L. 541-40-II du code de l'environnement français ne s'applique pas " ; qu'il résulte toutefois de l'échange de notes verbales signées les 13 mars  et 10 avril 2015 et publiées, avec le décret attaqué, au Journal officiel de la République française du 15 octobre 2015, que cette stipulation a été remplacée par une autre stipulation aux termes de laquelle : " Le présent accord est conclu conformément à l'article 30, paragraphe 1, du règlement 1013/2006. Les assouplissements de la procédure de notification du transfert de déchets qu'il prévoit sont conformes aux dérogations expressément prévues par les dispositions de ce règlement 1013/2006. Nonobstant ces assouplissements, le présent accord n'affecte pas la mise en oeuvre du règlement 1013/2006 "  ; que la stipulation initiale dont il est soutenu qu'elle modifierait une disposition de nature législative n'est, ainsi, jamais entrée en vigueur ; qu'ainsi, l'accord publié par le décret attaqué ne peut être regardé comme constituant, au sens de l'article 53 de la Constitution, un traité ou un accord " modifiant des dispositions de nature législative " ; que le moyen tiré de ce que cet accord aurait dû, pour ce motif, être ratifié par une loi ne peut, par suite, qu'être écarté ;<br/>
<br/>
              13. Considérant, en cinquième lieu, qu'il ressort des pièces du dossier, ainsi qu'il a été dit au point précédent, que les notes verbales des 13 mars et 10 avril 2015 ont été publiées, avec le décret attaqué, au Journal officiel de la République française du 15 octobre 2015 ; que, par suite, le moyen tiré de ce que ces notes verbales auraient dû être signées par le ministre des affaires étrangères ne peut qu'être écarté ;<br/>
<br/>
              14. Considérant, en sixième lieu, que l'accord publié par le décret attaqué ne comporte ni cession, ni échange, ni adjonction de territoire ; que, dès lors, le moyen tiré de ce que cet accord aurait dû être ratifié par une loi au motif qu'il porterait atteinte à l'intégrité du territoire national ne peut être accueilli ;<br/>
<br/>
              15. Considérant, en septième lieu, que le requérant ne peut utilement exciper de ce que le décret attaqué serait incompatible avec les dispositions de la directive 2002/49/CE du Parlement européen et du Conseil du 25 juin 2002 relative à l'évaluation et à la gestion du bruit dans l'environnement, de la directive 2008/50/CE du Parlement européen et du Conseil du 21 mai 2008 concernant la qualité de l'air ambiant et un air pur pour l'Europe et de la directive 2011/92/UE du Parlement européen et du Conseil du 13 décembre 2011 concernant l'évaluation des incidences de certains projets publics et privés sur l'environnement, qui ont été toutes trois transposées, et dont, au demeurant, il ne soutient pas qu'elles l'auraient été incomplètement ;<br/>
<br/>
              16. Considérant, en huitième et dernier lieu, que le moyen tiré de ce que le décret attaqué serait illégal, au motif qu'il aurait été pris sans qu'ait été créée d'installation classée afin de procéder au tri des roches anhydrites, n'est pas assorti des précisions permettant d'en apprécier le bien-fondé ; qu'il ne peut, dès lors, qu'être écarté ;<br/>
<br/>
              17. Considérant qu'il résulte de tout ce qui précède, sans qu'il soit besoin de statuer sur la fin de non-recevoir opposée en défense par le ministre chargé de l'environnement, que les conclusions de la requête de M. B... tendant à l'annulation du décret du 13 octobre 2015 doivent être rejetées de même que, par voie de conséquence, les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative par l'intéressé ainsi, en tout état de cause, que celles présentées par les intervenants ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'intervention des associations Roya expansion nature et France nature environnement PACA est admise.<br/>
Article 2 : : Le jugement des conclusions de la requête de M. B... dirigées contre l'arrêté du 22 octobre 2007 du préfet des Alpes-Maritimes déclarant d'utilité publique le projet d'aménagement du tunnel neuf de Tende et contre l'arrêté du 7 août 2012 du même préfet prorogeant les effets de l'arrêté du 22 octobre 2007 déclarant d'utilité publique le projet d'aménagement du tunnel neuf de Tende est attribué au tribunal administratif de Nice.<br/>
<br/>
Article 3 : Le surplus des conclusions de la requête de M. B... est rejeté.<br/>
Article 4 : Les conclusions présentées par les associations Roya expansion nature et France nature environnement PACA au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 5 : La présente décision sera notifiée à M. A... B..., au Premier ministre, au ministre d'Etat, ministre de la transition écologique et solidaire, au ministre de l'Europe et des affaires étrangères, à l'association Roya expansion nature, première intervenante dénommée, et au président du tribunal administratif de Nice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-02-005 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. ACCORDS INTERNATIONAUX. - TRAITÉS QUI ENGAGENT LES FINANCES DE L'ETAT - 1) NOTION [RJ1] - 2) ESPÈCE.
</SCT>
<ANA ID="9A"> 01-01-02-005 1) Les traités ou accords qui engagent les finances de l'État, au sens de l'article 53 de la Constitution, sont ceux qui créent une charge financière certaine et directe pour l'État. Toutefois, lorsque les charges financières impliquées par un accord n'excèdent pas, compte tenu de leur nature et de leur montant limité, les dépenses de fonctionnement courant incombant normalement à l'administration, elles ne peuvent pas être regardées comme engageant les finances de l'État, au sens de l'article 53 de la Constitution.... ,,2) Accord entre le Gouvernement de la République française et le Gouvernement de la République italienne relatif au transfert transfrontalier des déchets issus des travaux de construction du tunnel de Tende. L'accord publié par le décret n° 2015-1269 du 13 octobre 2015 attaqué, qui a pour objet de prévoir des assouplissements aux règles de procédure prévues par le règlement n° 1013/2006 du Parlement européen et du Conseil du 14 juin 2006 concernant les transferts de déchets, ne crée aucune charge financière certaine et directe pour l'Etat. Dès lors, le moyen tiré de ce que l'accord publié par le décret attaqué devait être ratifié par une loi, au motif qu'il engagerait les finances de l'Etat, ne peut qu'être écarté.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp., s'agissant des critères relatifs à la nature de la charge pour l'Etat, Assemblée générale (section des finances), 22 mars 2011, avis n° 385018, Rapport public 2012, p. 212.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
