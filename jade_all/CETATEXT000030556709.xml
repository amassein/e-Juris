<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030556709</ID>
<ANCIEN_ID>JG_L_2015_05_000000362617</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/55/67/CETATEXT000030556709.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 06/05/2015, 362617, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-05-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>362617</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>Mme Anne Iljic</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:362617.20150506</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi et le nouveau mémoire, enregistrés les 10 et 12 septembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés par le ministre des affaires sociales et de la santé ; le ministre des affaires sociales et de la santé demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler le jugement n° 0917874/5-2 du 5 juillet 2012 par lequel le tribunal administratif de Paris, faisant droit à la demande de l'UNSA-ITEFA, a annulé l'arrêté du ministre chargé des affaires sociales du 16 octobre 2009, modifié le 25 novembre 2009, portant détachement de conseillers techniques de service social dans le corps des attachés d'administration des affaires sociales ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de l'UNSA-ITEFA ; <br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi du n° 83-634 du 13 juillet 1983 ; <br/>
<br/>
              Vu le décret n° 91-783 du 1er août 1991 ;<br/>
<br/>
              Vu le décret n° 91-784 du 1er août 1991 ; <br/>
<br/>
              Vu le décret n° 2005-1215 du 26 septembre 2005 ; <br/>
<br/>
              Vu le décret n° 2006-1818 du 23 décembre 2006 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anne Iljic, auditeur,  <br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de la société UNSA-ITEFA ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'à la suite de l'avis favorable rendu le 19 juin 2009 par la commission administrative paritaire du ministère chargé des affaires sociales sur plusieurs demandes de détachement d'agents du corps des conseillers techniques de service social dans celui des attachés d'administration des affaires sociales, l'Union nationale des syndicats autonomes - ITEFA (UNSA-ITEFA), dont l'objet social consiste en la défense des intérêts des attachés d'administration des affaires sociales, a, par courrier du 2 juillet 2009, demandé au ministre de renoncer à ces détachements  ; que, par un arrêté du 16 octobre 2009, modifié le 25 novembre 2009, ce dernier a néanmoins procédé au détachement de seize agents du corps des conseillers techniques de service social dans le corps des attachés d'administration des affaires sociales ; qu'il se pourvoit en cassation contre le jugement par lequel le tribunal administratif de Paris a fait droit à la demande de l'UNSA-ITEFA tendant à l'annulation de cet arrêté ; <br/>
<br/>
              Sur la régularité du jugement attaqué : <br/>
<br/>
              2. Considérant que le ministre soutient que la circonstance que les seize agents du corps des conseillers techniques de service social bénéficiaires de l'arrêté en litige aient été mis en cause en première instance afin de produire des observations, alors qu'ils auraient dû être regardés comme des défendeurs, constitue une irrégularité de nature à entraîner l'annulation du jugement qu'il attaque ; que, toutefois, il ressort des pièces de la procédure devant le tribunal administratif de Paris et des visas du jugement attaqué que les observations produites par ces seize agents ont été à la fois visées et analysées et qu'ils ont été convoqués à l'audience au cours de laquelle ils ont pu, pour certains d'entre eux, présenter des observations orales ; qu'ainsi, dans les circonstances de l'espèce, l'irrégularité commise par le tribunal administratif de Paris, qui n'a privé les seize agents bénéficiaires de l'arrêté en litige d'aucune des garanties attachées à la qualité de partie, n'est pas de nature à entraîner l'annulation du jugement attaqué ; qu'il suit de là que ce moyen ne peut, en tout état de cause, qu'être écarté ; <br/>
<br/>
              Sur le bien-fondé du jugement attaqué : <br/>
<br/>
              3. Considérant qu'aux termes de l'article 13 bis de la loi du 13 juillet 1983 portant droits et obligations des fonctionnaires : " Le détachement ou l'intégration directe s'effectue entre corps et cadres d'emplois appartenant à la même catégorie et de niveau comparable, apprécié au regard des conditions de recrutement ou de la nature des missions. " ; <br/>
<br/>
              4. Considérant, en premier lieu, que, pour juger que le corps des conseillers techniques de service social et celui des attachés d'administration des affaires sociales, tous deux classés dans la catégorie A, n'étaient pas de niveau comparable au regard de leurs conditions de recrutement, le tribunal administratif de Paris a en particulier relevé que, à la différence du corps des conseillers techniques de service social, le corps des attachés d'administration des affaires sociales est, en vertu du décret du 26 septembre 2005, recruté à titre principal par la voie des instituts régionaux d'administration, au sein desquels les élèves reçoivent une formation spécifique, à l'issue de laquelle ils n'accèdent au corps que s'ils ont un niveau adéquat ; qu'en se fondant ainsi sur le type de formation et les modalités de sélection des agents en cause, le tribunal administratif, qui n'a pas, contrairement à ce qui est soutenu, établi de hiérarchisation entre les différentes voies de concours, n'a pas commis d'erreur de droit et a exactement qualifié les faits qui lui étaient soumis ; <br/>
<br/>
              5. Considérant, en second lieu, que pour juger que le corps des conseillers techniques de service social et celui des attachés d'administration des affaires sociales n'étaient pas de niveau comparable au regard de la nature des missions exercées, le tribunal administratif de Paris a comparé les missions définies par les statuts de ces deux corps ; qu'en se fondant ainsi sur les missions statutaires  et non sur les missions effectivement exercées par les intéressés, le tribunal administratif de Paris n'a pas commis d'erreur de droit;  qu'en relevant notamment que ces missions n'étaient pas comparables, dès lors que les conseillers techniques de service social, qui sont chargés de mener des actions en faveur de familles et de groupes connaissant des difficultés sociales, ne sont pas, à la différence des attachés d'administration des affaires sociales, chargés à titre principal de participer à la conception et à l'élaboration des politiques publiques, il a exactement qualifié les faits qui lui étaient soumis; <br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que le ministre n'est pas fondé à demander l'annulation du jugement qu'il attaque ; que son pourvoi doit, par suite, être rejeté ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3000 euros à verser à l'UNSA-ITEFA au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du ministre des affaires sociales et de la santé est rejeté.  <br/>
Article 2 : L'Etat versera à l'UNSA-ITEFA une somme de 3000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 2 : La présente décision sera notifiée à la ministre des affaires sociales et de la santé, à l'UNSA-ITEFA, à Mme AB...V..., à Mme X...C..., à Mme Z...O..., à Mme P...G..., à Mme L...AF..., à Mme A...I..., à Mme U...Y..., à Mme T...Q..., à Mme B...K..., à Mme H...E..., à M. N... F..., à Mme AA...M..., à Mme AC...N..., à Mme L...R..., à Mme AE...AD..., à Mme J... S...et à Mme W...D....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
