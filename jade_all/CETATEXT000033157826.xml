<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033157826</ID>
<ANCIEN_ID>JG_L_2016_09_000000371862</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/15/78/CETATEXT000033157826.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 21/09/2016, 371862</TITRE>
<DATE_DEC>2016-09-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>371862</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Jean-Luc Matt</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:371862.20160921</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              Procédure contentieuse antérieure<br/>
<br/>
              La SA Compagnie de Saint-Gobain a demandé au tribunal administratif de Montreuil la décharge des cotisations supplémentaires d'impôt sur les sociétés auxquelles elle a été assujettie au titre des exercices 2001 à 2003. Par un jugement n° 0902633 du 10 mars 2011, le tribunal administratif de Montreuil a partiellement fait droit à sa demande.<br/>
<br/>
              Par un arrêt n° 11VE02480 du 2 juillet 2013, la cour administrative d'appel de Versailles a rejeté l'appel formé par le ministre de l'économie et des finances contre les articles 1er et 2 de ce jugement par lesquels le tribunal a fait droit à la demande de la SA Compagnie de Saint-Gobain concernant la réintégration d'une quote-part de frais et charges sur les dividendes perçus de ses filiales et rejeté l'appel incident formé par cette société contre l'article 3 du même jugement rejetant le surplus des conclusions de sa demande relatif à la reprise de provisions pratiquées sur des sociétés absorbées ou cédées.<br/>
<br/>
              Procédures devant le Conseil d'Etat<br/>
<br/>
              1° Par un pourvoi et un mémoire en réplique, enregistrés les 3 septembre 2013 et 15 décembre 2014 au secrétariat du contentieux du Conseil d'Etat, le ministre délégué, chargé du budget demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il a rejeté son appel ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel.<br/>
<br/>
<br/>
              2° Par un pourvoi sommaire, un mémoire complémentaire, un mémoire en réplique et un nouveau mémoire, enregistrés les 12 septembre 2013, 12 décembre 2013, 21 avril 2015 et 17 février 2015 au secrétariat du contentieux du Conseil d'Etat, la SA Compagnie de Saint-Gobain demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il a rejeté son appel incident ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel incident ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - la loi n° 92-1376 du 30 décembre 1992 ;<br/>
              - la loi n° 98-1266 du 30 décembre 1998 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Matt, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de la SA Compagnie de Saint-Gobain ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Les pourvois du ministre délégué, chargé du budget et de la SA Compagnie de Saint-Gobain sont dirigés contre le même arrêt. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2. Aux termes de l'article 209 quinquies du code général des impôts, alors en vigueur : " Les sociétés françaises agréées à cet effet par le ministre de l'économie et des finances peuvent retenir l'ensemble des résultats de leurs exploitations directes ou indirectes, qu'elles soient situées en France ou à l'étranger, pour l'assiette des impôts établis sur la réalisation et la distribution de leurs bénéfices. / Les conditions d'application des dispositions qui précèdent sont fixées par décret en Conseil d'Etat ". Aux termes de l'article 118 de l'annexe II au même code, alors en vigueur : " Le résultat de la société agréée (...) est, le cas échéant, rectifié de manière à éliminer les opérations qui font double emploi. La liste de  ces opérations est arrêtée par le ministre de l'économie et des finances ". Aux termes de l'article 23 bis A de l'annexe IV au code général des impôts, créé par un arrêté du 6 février 1992 du ministre de l'économie, des finances et du budget : " Les opérations mentionnées à l'article 118 de l'annexe II au code général des impôts s'entendent : / a) De la distribution de dividendes entre sociétés dont les résultats sont pris en compte dans le résultat consolidé, pour la quote-part de frais et charges réintégrée en application du II de l'article 216 du code général des impôts ; b) De la dotation aux provisions constituées par des sociétés dont les résultats sont pris en compte dans le résultat consolidé en vue de faire face aux dépréciations des créances sur d'autres sociétés comprises dans le périmètre de consolidation ou de participation dans de telles sociétés, ainsi que des risques qu'elles encourent du fait de ces mêmes sociétés ; (...) ".<br/>
<br/>
              Sur le pourvoi du ministre délégué, chargé du budget :<br/>
<br/>
              3. Les dispositions du I et du II de l'article 216 du code général des impôts prévoyant la réintégration, dans le bénéfice net total de la société mère, d'une quote-part de frais et charges fixée à 5 % du produit total des participations, crédit d'impôt compris, ont été abrogées par l'article 104 de la loi du 30 décembre 1992 de finances pour 1993, privant de ce fait d'application, pour les sociétés relevant du régime du bénéfice consolidé prévu par l'article 209 quinquies du même code, les dispositions du a de l'article 23 bis A de l'annexe IV à ce code. Toutefois, l'article 43 de la loi du 31 décembre 1998 de finances pour 1999 a rétabli, au I de l'article 216 du code général des impôts, des dispositions prévoyant la réintégration d'une quote-part de frais et charges dans le bénéfice net total de la société mère. Le a de l'article 23 bis A de l'annexe IV à ce code, qui n'avait pas entre temps été abrogé, est, ainsi, redevenu applicable. La circonstance que les dispositions du a de l'article 23 bis A de l'annexe IV au code général des impôts fassent référence à la réintégration opérée " en application du II de l'article 216 du code général des impôts ", et non en application du I de cet article, est à cet égard sans incidence.<br/>
<br/>
              4. Il résulte de ce qui précède que la cour n'a pas commis d'erreur de droit en se fondant sur les dispositions du a de l'article 23 bis A de l'annexe IV au code général des impôts pour faire droit à la demande de la SA Compagnie de Saint-Gobain tendant à ce que ne soient pas réintégrées dans son résultat consolidé imposable à l'impôt sur les sociétés, au titre des exercices 2001 à 2003, les quotes-parts de frais et charges attachées aux dividendes perçus de ses filiales. Par suite, le pourvoi du ministre doit être rejeté.<br/>
<br/>
              Sur le pourvoi incident de la SA Compagnie de Saint-Gobain :<br/>
<br/>
              5. Il ressort des pièces du dossier soumis aux juges du fond que l'administration a remis en cause la déduction par la SA Compagnie de Saint-Gobain, de ses résultats consolidés des exercices clos en 2002 et 2003, des reprises des provisions pour risque ou pour dépréciation qui avaient été constituées par des sociétés comprises dans son périmètre de consolidation sur des titres des sociétés Novatech BV, Novatech SA, Securit Véhicules Industriels, Jiangsu Donghai et SG Diamant Winter GmbH, provisions qui étaient devenues sans objet à l'occasion de l'absorption de ces sociétés par d'autres sociétés du groupe consolidé ou de la cession des titres concernés à des sociétés extérieures au groupe.<br/>
<br/>
              6. En premier lieu, il résulte des termes mêmes des dispositions précitées du b de l'article 23 bis A de l'annexe IV au code général des impôts, pris pour l'application de l'article 118 de l'annexe II au même code, que les dotations aux provisions constituées par des sociétés dont les résultats sont pris en compte dans le résultat consolidé en vue de faire face aux dépréciations des participations dans d'autres sociétés comprises dans le périmètre de consolidation et aux risques qu'elles encourent du fait de ces mêmes sociétés doivent être neutralisées pour la détermination du résultat consolidé de la société agréée. L'objectif d'élimination des opérations faisant double emploi énoncé à l'article 118 de l'annexe II au code implique que, symétriquement, la reprise de telles provisions soit neutralisée pour la détermination du résultat consolidé, y compris lorsque cette reprise est justifiée par l'absorption des sociétés en cause par des sociétés qui demeurent.membres du groupe consolidé Par suite, la cour a méconnu les articles 209 quinquies du code général des impôts, 118 de l'annexe II et 23 bis A de l'annexe IV à ce code en refusant la neutralisation des reprises de provisions litigieuses.<br/>
<br/>
              7. En second lieu, s'agissant des reprises de provisions au titre de la cession des titres concernés à des sociétés extérieures au groupe consolidé, la SA Compagnie de Saint-Gobain se prévalait devant la cour, sur le fondement de l'article L. 80 A du livre des procédures fiscales, du bénéfice des paragraphes 95 et 97 de l'instruction 4 H-4-95, publiée au bulletin officiel des impôts du 31 mars 1995. D'une part, la cour a omis de statuer au regard du paragraphe 97 de cette instruction. D'autre part, elle a commis une erreur de droit en jugeant que le paragraphe 95 de cette instruction ne s'applique pas à des filiales de sociétés du groupe qui ont la personnalité juridique, alors que de telles filiales peuvent constituer des exploitations indirectes étrangères au sens des dispositions du 1 de l'article 114 de l'annexe II au code général des impôts.<br/>
<br/>
              8. Il résulte de tout ce qui précède que sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, la SA Compagnie de Saint-Gobain est fondée à demander l'annulation de l'article 1er de l'arrêt attaqué en tant qu'il a rejeté son appel incident.<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              9. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 6 000 euros à verser à la SA Compagnie de Saint-Gobain au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du ministre délégué, chargé du budget est rejeté.<br/>
Article 2 : L'article 1er de l'arrêt du 2 juillet 2013 de la cour administrative d'appel de Versailles est annulé en tant qu'il a rejeté l'appel incident de la SA Compagnie de Saint-Gobain.<br/>
Article 3 : L'affaire est renvoyée dans cette mesure à la cour administrative d'appel de Versailles.<br/>
Article 4 : L'Etat versera à la SA Compagnie de Saint-Gobain une somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : La présente décision sera notifiée au ministre de l'économie et des finances et à la SA Compagnie de Saint-Gobain.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-08-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. APPLICATION DANS LE TEMPS. TEXTE APPLICABLE. - DISPOSITIONS RÉGLEMENTAIRES RENVOYANT À DES DISPOSITIONS LÉGISLATIVES ABROGÉES PUIS RÉTABLIES - DISPOSITIONS DEVENANT INAPPLICABLES PUIS REDEVENANT APPLICABLES.
</SCT>
<ANA ID="9A"> 01-08-03 L'abrogation des dispositions législatives auxquelles renvoyaient des dispositions réglementaires a privé ces dernières d'application. Une loi postérieure a toutefois rétabli, presque à l'identique, ces dispositions législatives. Les dispositions réglementaires, qui n'avaient pas entre temps été abrogées sont, ainsi, redevenues applicables.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
