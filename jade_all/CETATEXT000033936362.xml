<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033936362</ID>
<ANCIEN_ID>JG_L_2017_01_000000396404</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/93/63/CETATEXT000033936362.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 27/01/2017, 396404</TITRE>
<DATE_DEC>2017-01-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>396404</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BALAT ; SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>Mme Sophie Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:396404.20170127</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Tahitienne de construction (STAC) a demandé au tribunal administratif de la Polynésie française de condamner l'Etablissement d'aménagement et de développement, aux droits duquel est venu l'Etablissement public Tahiti Nui Aménagement et Développement, à lui verser la somme de 273 099 046 F CFP en règlement du solde du marché de travaux relatif au lot n° 12-2 " revêtements sols souples " conclu pour la construction du centre hospitalier du Taaone. <br/>
<br/>
              Par un jugement n° 1300557 du 15 juillet 2014, le tribunal administratif de la Polynésie française a fixé le solde de ce marché à 16 072 263 F CFP et a condamné l'Etablissement public Tahiti Nui Aménagement et Développement à verser à la STAC les intérêts au taux légal majoré de deux points sur cette somme, à compter du 30 avril 2011 et jusqu'au versement effectif de celle-ci. Le tribunal a rejeté le surplus des conclusions de la société.<br/>
<br/>
              Par un arrêt n° 14PA04342 du 26 octobre 2015, la cour administrative d'appel de Paris a rejeté l'appel de la société dirigé contre le jugement du 15 juillet 2014 du tribunal administratif de la Polynésie française.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 26 janvier, 26 avril et 14 novembre 2016 au secrétariat du contentieux du Conseil d'Etat, la STAC demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il juge irrecevable sa demande de première instance en ce qu'elle contestait le décompte général qui lui avait été notifié ;<br/>
<br/>
              2°) réglant dans cette mesure l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etablissement public Tahiti Nui Aménagement et Développement la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi organique n° 2004-192 du 27 février 2004 modifiée, ensemble la loi n° 2004-193 du 27 février 2004 ;<br/>
              - la délibération n° 84-20 du 1er mars 1984 portant code des marchés publics de toute nature passés au nom de la Polynésie française et de ses établissements publics ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public. <br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de la société Tahitienne de construction et à Me Balat, avocat de l'Etablissement public Tahiti Nui Aménagement et Développement. <br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des énonciations de l'arrêt attaqué que, par un marché de travaux conclu le 18 avril 2006, la société Tahitienne de construction (STAC) s'est vu confier par l'Etablissement d'aménagement et de développement, agissant en qualité de maître d'ouvrage délégué de la Polynésie française, l'exécution du lot n° 12-2 relatif aux revêtements de sols souples dans le cadre de la construction du centre hospitalier du Taaone ; que la STAC a saisi le maître d'ouvrage délégué d'une réclamation contestant le décompte général de ce marché ; que cette réclamation a été rejetée le 29 avril 2011 ; que la société a saisi, le 7 juin suivant, le juge des référés du tribunal administratif de la Polynésie française afin d'obtenir le versement d'une provision ; qu'elle a ensuite saisi, le 16 octobre 2013, le tribunal administratif d'un recours au fond tendant à la condamnation de l'Etablissement d'aménagement et de développement, aux droits duquel vient l'Etablissement public Tahiti Nui Aménagement et Développement, à lui verser la somme de 273 099 046 F CFP, assortie des intérêts à compter du 22 novembre 2010 ; que par un jugement du 15 juillet 2014, le tribunal administratif de la Polynésie française a fixé le solde du marché à 16 072 263 F CFP et rejeté le surplus des conclusions de la société ; que par un arrêt du 26 octobre 2015, la cour administrative d'appel de Paris a rejeté l'appel formé par la STAC contre ce jugement ; que la société se pourvoit en cassation contre cet arrêt en tant qu'il juge irrecevable sa contestation du décompte général qui lui a été notifié ; <br/>
<br/>
              2. Considérant, d'une part, qu'aux termes de l'article 7.2.3. du cahier des clauses administratives générales (CCAG) applicable au marché litigieux : " Si, dans le délai de six mois à partir de la notification à l'entrepreneur de la décision prise conformément au 3 de l'article 7.2.2. sur les réclamations auxquelles a donné lieu le décompte général du marché, l'entrepreneur n'a pas porté ses réclamations devant le tribunal administratif compétent, il est considéré comme ayant accepté ladite décision et toute réclamation est irrecevable. / Toutefois, le délai de six mois est suspendu en cas de saisine du comité consultatif de règlement amiable dans les conditions du 1 de l'article 7.2.4 " ; <br/>
<br/>
              3. Considérant, d'autre part, qu'aux termes de l'article R. 541-1 du code de justice administrative : " Le juge des référés peut, même en l'absence d'une demande au fond, accorder une provision au créancier qui l'a saisi lorsque l'existence de l'obligation n'est pas sérieusement contestable. Il peut, même d'office, subordonner le versement de la provision à la constitution d'une garantie. " ; <br/>
<br/>
              4 Considérant qu'il résulte de ces dernières dispositions que le titulaire du marché peut obtenir du juge des référés qu'il ordonne au pouvoir adjudicateur le versement d'une indemnité provisionnelle et qu'il n'est pas tenu de saisir, par ailleurs, le juge du contrat d'une demande au fond ; que, dans ces conditions, la saisine du juge des référés sur le fondement des articles R. 541-1 et suivants du code de justice administrative doit être regardée comme la saisine du tribunal administratif compétent au sens de l'article 7.2.3. du CCAG cité au point 2 ; <br/>
<br/>
              5. Considérant que pour juger tardif, et donc irrecevable, le recours de la STAC contre la décision prise par l'établissement public sur sa réclamation relative au décompte général, la cour administrative d'appel de Paris a estimé que la demande de provision présentée par cette société au juge des référés du tribunal administratif de la Polynésie française le 7 juin 2011 ne pouvait être regardée comme la saisine du tribunal administratif compétent au sens des stipulations de l'article 7.2.3. du CCAG cité ci-dessus ; qu'il résulte de ce qui précède qu'en statuant ainsi, la cour a commis une erreur de droit ; <br/>
<br/>
              6. Considérant que la STAC est par suite fondée à demander l'annulation de l'arrêt qu'elle attaque en tant qu'il juge irrecevable sa contestation du décompte général qui lui a été notifié ; <br/>
<br/>
              7. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etablissement public Tahiti Nui Aménagement et Développement la somme de 3 000 euros à verser à la STAC sur le fondement de l'article L. 761-1 du code de justice administrative ; que ces dispositions font en revanche obstacle à ce qu'une somme soit mise à la charge de cette société qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 26 octobre 2015 de la cour administrative d'appel de Paris est annulé en tant qu'il juge irrecevable la contestation par la société Tahitienne de construction du décompte général qui lui a été notifié. <br/>
Article 2 : L'affaire est renvoyée dans cette mesure à la cour administrative d'appel de Paris. <br/>
Article 3 : L'Etablissement public Tahiti Nui Aménagement et Développement versera à la STAC une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : Les conclusions présentées par l'Etablissement public Tahiti Nui Aménagement et Développement au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 5 : La présente décision sera notifiée à la société Tahitienne de construction et à l'Etablissement public Tahiti Nui Aménagement et Développement.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-05-02-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. EXÉCUTION FINANCIÈRE DU CONTRAT. RÈGLEMENT DES MARCHÉS. DÉCOMPTE GÉNÉRAL ET DÉFINITIF. - DÉCISION PRISE SUR LES RÉCLAMATIONS AUXQUELLES A DONNÉ LIEU LE DÉCOMPTE GÉNÉRAL DU MARCHÉ - DÉLAI DE SIX MOIS POUR PORTER LES RÉCLAMATIONS DEVANT LE TRIBUNAL ADMINISTRATIF COMPÉTENT (ART. 7.2.3 DU CCAG-TRAVAUX) - NOTION DE SAISINE DU TRIBUNAL ADMINISTRATIF COMPÉTENT - RÉFÉRÉ-PROVISION - INCLUSION [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-08-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. RECEVABILITÉ. - DÉCISION PRISE SUR LES RÉCLAMATIONS AUXQUELLES A DONNÉ LIEU LE DÉCOMPTE GÉNÉRAL DU MARCHÉ - DÉLAI DE SIX MOIS POUR PORTER LES RÉCLAMATIONS DEVANT LE TRIBUNAL ADMINISTRATIF COMPÉTENT (ART. 7.2.3 DU CCAG-TRAVAUX) - NOTION DE SAISINE DU TRIBUNAL ADMINISTRATIF COMPÉTENT - INCLUSION - RÉFÉRÉ-PROVISION [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-03-015 PROCÉDURE. PROCÉDURES DE RÉFÉRÉ AUTRES QUE CELLES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ-PROVISION. - INTRODUCTION D'UN RÉFÉRÉ-PROVISION - SAISINE DU TRIBUNAL ADMINISTRATIF COMPÉTENT AU SENS DE L'ART. 7.2.3 DU CCAG-TRAVAUX (DÉLAI DE SIX MOIS POUR PORTER LES RÉCLAMATIONS RELATIVES AU DÉCOMPTE GÉNÉRAL DU MARCHÉ DEVANT LE TRIBUNAL ADMINISTRATIF COMPÉTENT) - EXISTENCE [RJ1].
</SCT>
<ANA ID="9A"> 39-05-02-01 Article 7.2.3 du cahier des clauses administratives générales applicable aux marchés publics de travaux (CCAG-Travaux) prévoyant que si l'entrepreneur n'a pas porté ses réclamations devant le tribunal administratif compétent dans un délai de six mois à partir de la notification de la décision prise sur ses réclamations relatives au décompte général du marché, il est considéré comme ayant accepté cette décision et toute réclamation est irrecevable.,,,Il résulte de l'article R. 541-1 du code de justice administrative (CJA) que le titulaire du marché peut obtenir du juge des référés qu'il ordonne au pouvoir adjudicateur le versement d'une indemnité provisionnelle et qu'il n'est pas tenu de saisir, par ailleurs, le juge du contrat d'une demande au fond. Dans ces conditions, la saisine du juge des référés sur le fondement des articles R. 541-1 et suivants du CJA doit être regardée comme la saisine du tribunal administratif compétent au sens de l'article 7.2.3. du CCAG-Travaux.</ANA>
<ANA ID="9B"> 39-08-01 Article 7.2.3 du cahier des clauses administratives générales applicable aux marchés publics de travaux (CCAG-Travaux) prévoyant que si l'entrepreneur n'a pas porté ses réclamations devant le tribunal administratif compétent dans un délai de six mois à partir de la notification de la décision prise sur ses réclamations relatives au décompte général du marché, il est considéré comme ayant accepté cette décision et toute réclamation est irrecevable.,,,Il résulte de l'article R. 541-1 du code de justice administrative (CJA) que le titulaire du marché peut obtenir du juge des référés qu'il ordonne au pouvoir adjudicateur le versement d'une indemnité provisionnelle et qu'il n'est pas tenu de saisir, par ailleurs, le juge du contrat d'une demande au fond. Dans ces conditions, la saisine du juge des référés sur le fondement des articles R. 541-1 et suivants du CJA doit être regardée comme la saisine du tribunal administratif compétent au sens de l'article 7.2.3. du CCAG-Travaux.</ANA>
<ANA ID="9C"> 54-03-015 Article 7.2.3 du cahier des clauses administratives générales applicable aux marchés publics de travaux (CCAG-Travaux) prévoyant que si l'entrepreneur n'a pas porté ses réclamations devant le tribunal administratif compétent dans un délai de six mois à partir de la notification de la décision prise sur ses réclamations relatives au décompte général du marché, il est considéré comme ayant accepté cette décision et toute réclamation est irrecevable.,,,Il résulte de l'article R. 541-1 du code de justice administrative (CJA) que le titulaire du marché peut obtenir du juge des référés qu'il ordonne au pouvoir adjudicateur le versement d'une indemnité provisionnelle et qu'il n'est pas tenu de saisir, par ailleurs, le juge du contrat d'une demande au fond. Dans ces conditions, la saisine du juge des référés sur le fondement des articles R. 541-1 et suivants du CJA doit être regardée comme la saisine du tribunal administratif compétent au sens de l'article 7.2.3. du CCAG-Travaux.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Comp., s'agissant d'un référé-expertise, CE, 18 septembre 2015, Société Avena BTP, n° 384523, inédite au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
