<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042543730</ID>
<ANCIEN_ID>JG_L_2020_11_000000439424</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/54/37/CETATEXT000042543730.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 19/11/2020, 439424, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-11-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>439424</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLOCHE</AVOCATS>
<RAPPORTEUR>M. Bruno Bachini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Stéphane Hoynck</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:439424.20201119</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un mémoire, enregistré le 21 août 2020 au secrétariat du contentieux du Conseil d'État, Mme F... B..., Mme G... B..., M. C... E... et Mme D... A... demandent au Conseil d'État, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 et à l'appui de leur pourvoi formé contre l'arrêt n° 18MA01492 de la cour administrative d'appel de Marseille du 14 janvier 2020, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution, d'une part, des dispositions de l'article L. 1321-2 du code de la santé publique dans leur version antérieure à l'entrée en vigueur de la loi du 24 juillet 2019 relative à l'organisation et à la transformation du système de santé, d'autre part, des dispositions combinées de l'article L. 1321-2 du code de la santé publique dans sa version actuellement en vigueur et du IX de l'article 61 de la loi du 24 juillet 2019. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
- la Constitution, notamment son Préambule et son article 61-1 ; <br/>
- l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
- le code de l'expropriation pour cause d'utilité publique ;<br/>
- l'article L. 1321-2 du code de la santé publique, dans sa version issue de la loi n° 2010-788 du 12 juillet 2010 ;<br/>
- l'article L. 1321-2 du code de la santé publique, dans sa version issue de la loi n° 2019-774 du 24 juillet 2019 ;<br/>
- le IX de l'article 61 de la loi n° 2019-774 du 24 juillet 2019 ; <br/>
- le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Bachini, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Stéphane Hoynck, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boulloche, avocat de Mme B... et autres ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              Sur les dispositions de l'article L. 1321-2 du code de la santé publique dans leur version issue de la loi du 12 juillet 2010 :<br/>
<br/>
              2. L'article L. 1321-2 du code de la santé publique, dans sa version issue de la loi du 12 juillet 2010 portant engagement national pour l'environnement, prévoit qu'en vue d'assurer la protection de la qualité des eaux destinées à la consommation humaine, l'acte portant déclaration d'utilité publique des travaux de prélèvement d'eau destinée à l'alimentation des collectivités humaines détermine autour du point de prélèvement un périmètre de protection immédiate dont les terrains sont à acquérir en pleine propriété, un périmètre de protection rapprochée à l'intérieur duquel peuvent être interdits ou réglementés toutes sortes d'installations, travaux, activités, dépôts, ouvrages, aménagement ou occupation des sols de nature à nuire directement ou indirectement à la qualité des eaux et, le cas échéant, un périmètre de protection éloignée à l'intérieur duquel peuvent être réglementés les installations, travaux, activités, dépôts, ouvrages, aménagement ou occupation des sols ci-dessus mentionnés.<br/>
<br/>
              3. Aux termes de l'article 17 de la Déclaration des droits de l'homme et du citoyen de 1789 : " La propriété étant un droit inviolable et sacré, nul ne peut en être privé, si ce n'est lorsque la nécessité publique, légalement constatée, l'exige évidemment et sous la condition d'une juste et préalable indemnité ". En vertu de l'article 34 de la Constitution, la loi détermine les principes fondamentaux du régime de la propriété. <br/>
<br/>
              4. La méconnaissance par le législateur de sa propre compétence ne peut être invoquée dans le cadre d'une question prioritaire de constitutionnalité que dans le cas où est affecté un droit ou une liberté que la Constitution garantit. <br/>
<br/>
              5. Les requérants soutiennent, d'une part, que les dispositions précédemment mentionnées de l'article L. 1321-2 du code de la santé publique méconnaissent le droit de propriété constitutionnellement garanti par l'article 17 de la Déclaration des droits de l'homme et du citoyen en ce que celles-ci imposent l'acquisition en pleine propriété des terrains situés dans le périmètre de protection immédiate autour du point de prélèvement d'eau pour assurer la protection de la qualité des eaux destinées à l'alimentation humaine alors que celle-ci n'est pas indispensable à la protection du captage d'eau en cause et, d'autre part, que le législateur a méconnu l'étendue de sa compétence en s'abstenant de désigner le bénéficiaire de l'expropriation, de fixer des critères pour la détermination des périmètres et de prévoir une procédure contradictoire permettant aux intéressés d'être informés des motifs de l'acquisition et de présenter des observations.  <br/>
<br/>
              6. D'une part, la protection de la qualité des eaux destinées à l'alimentation des collectivités humaines constitue un objectif d'intérêt général justifiant l'acquisition en pleine propriété, au bénéfice de la personne publique responsable du captage, des terrains situés à proximité immédiate du point de prélèvement en vue de constituer le périmètre de protection immédiate, d'une superficie limitée et dont l'utilité publique est légalement constatée sous le contrôle du juge. D'autre part, en l'absence de dispositions spécifiques définissant la procédure qui leur est applicable, les actes portant déclaration d'utilité publique des travaux de prélèvement d'eau pris sur le fondement des dispositions de l'article L. 1321-2 du code de la santé publique sont régis par les dispositions des articles L. 121-1 et suivants du code de l'expropriation pour cause d'utilité publique, qui déterminent les principes régissant la procédure d'acquisition et garantissent les droits des propriétaires concernés, en fixant notamment les règles relatives à la constatation de l'utilité publique de l'opération ainsi qu'à l'identification des propriétaires et à la détermination des parcelles.<br/>
<br/>
              7. Il résulte de ce qui précède que les dispositions contestées ne méconnaissent pas le droit de propriété garanti par l'article 17 de la Déclaration des droits de l'homme et du citoyen de 1789, ni ne sont entachées sur ce point d'une incompétence négative du législateur. Par suite, la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux. <br/>
<br/>
Sur les dispositions du IX de l'article 61 de la loi du 24 juillet 2019 relative à l'organisation et à la transformation du système de santé :<br/>
<br/>
              8. D'une part, l'article L. 1321-2 du code de la santé publique, dans sa version issue de la loi du 24 juillet 2019 relative à l'organisation et à la transformation du système de santé, dispose, à ses deuxième et troisième alinéas, que l'instauration d'un périmètre de protection rapprochée n'est pas nécessaire lorsque les conditions hydrologiques et hydrogéologiques permettent d'assurer efficacement la préservation de la qualité de l'eau par des mesures de protection limitées au voisinage immédiat du captage ainsi qu'en cas de captages d'eau d'origine souterraine dont le débit exploité est inférieur, en moyenne annuelle, à 100 mètres cubes par jour. D'autre part, il résulte du IX de l'article 61 de la loi du 24 juillet 2019 que ces nouvelles dispositions ne s'appliquent pas aux captages d'eau pour lesquels un arrêté d'ouverture d'une enquête publique relative à l'instauration d'un périmètre de protection a été publié à la date de publication de cette loi.<br/>
<br/>
              9. Ces dispositions du IX de l'article 61 de la loi du 24 juillet 2019, qui ont pour effet de maintenir l'instauration de périmètres de protection rapprochée dans des situations pour lesquelles le législateur a désormais prévu que celle-ci n'était plus requise dans les cas où un arrêté d'ouverture d'une enquête publique relative à l'instauration d'un périmètre de protection a déjà été publié à la date de publication de la loi introduisant ces dérogations nouvelles, sont applicables au litige dont est saisi la cour administrative d'appel de Marseille au sens et pour l'application de l'article 23-4 et n'ont pas déjà été déclarées conformes à la Constitution par le Conseil constitutionnel. Les moyens tirés de ce qu'elles portent atteinte aux droits et libertés garantis par la Constitution, et notamment au droit de propriété ainsi qu'au principe d'égalité devant la loi, soulèvent une question présentant un caractère sérieux. Ainsi, il y a lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée. <br/>
<br/>
              10. Il résulte de tout ce qui précède, d'une part, qu'il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée contre les dispositions de l'article L. 1321-2 du code de la santé publique dans leur version issue de la loi n° 2010-788 du 12 juillet 2010, d'autre part, qu'il y a lieu, à l'inverse, de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée contre les dispositions du IX de l'article 61 de la loi n° 2019-774 du 24 juillet 2019. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par Mme B... et autres contre les dispositions de l'article L. 1321-2 du code de la santé publique dans leur version issue de la loi n° 2010-788 du 12 juillet 2010.<br/>
Article 2 : La question de la conformité à la Constitution des dispositions du IX de l'article 61 de la loi n° 2019-774 du 24 juillet 2019 est renvoyée au Conseil constitutionnel.<br/>
Article 3 : La présente décision sera notifiée à Mme F... B..., Mme G... B... épouse E..., M. C... E..., Mme D... A..., au ministre des solidarités et de la santé, à la communauté de communes du Sartenais Valinco et à la secrétaire générale du gouvernement.<br/>
Copie en sera adressée au Conseil constitutionnel et au Premier ministre.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
