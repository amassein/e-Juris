<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030249862</ID>
<ANCIEN_ID>JG_L_2015_02_000000362781</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/24/98/CETATEXT000030249862.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème / 9ème SSR, 16/02/2015, 362781</TITRE>
<DATE_DEC>2015-02-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>362781</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème / 9ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Isabelle Lemesle</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:362781.20150216</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, le mémoire en réplique et les autres mémoires, enregistrés les 14 septembre 2012, 8 avril 2013, 28 et 31 mars 2014 au secrétariat du contentieux du Conseil d'Etat, présentés par M. et Mme A...B...et M. C...D...; M. et MmeB..., ainsi que M. D...demandent au Conseil d'Etat :<br/>
<br/>
              - d'annuler, pour excès de pouvoir, la délibération n° 2012 184 du 7 juin 2012 par laquelle la Commission nationale de l'informatique et des libertés (CNIL) a dispensé de déclaration les traitements automatisés de données personnelles relatifs à la gestion administrative, comptable et pédagogique des écoles et des établissements d'enseignement secondaire des secteurs public et privé ;<br/>
              - de mettre à la charge de l'Etat la somme de 1 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu la directive n° 95/46/CE du 24 octobre 1995 ;<br/>
<br/>
              Vu la loi n° 78-17 du 6 janvier 1978 ;<br/>
<br/>
              Vu le décret n° 2005-1309 du 20 octobre 2005 ;<br/>
<br/>
              Vu le règlement intérieur de la CNIL adopté par la délibération n° 2006-147 du 23 mai 2006, modifiée par la délibération n° 2011-249 du 8 septembre 2011 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Isabelle Lemesle, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que, par une délibération du 7 juin 2012, la Commission nationale de l'informatique et des libertés (CNIL) a dispensé de déclaration les traitements automatisés de données personnelles relatifs à la gestion administrative, comptable et pédagogique des écoles et des établissements d'enseignement secondaire des secteurs public et privé ; que M.et Mme B...ainsi que M. D...demandent l'annulation pour excès de pouvoir de cette délibération ;<br/>
<br/>
              2. Considérant, en premier lieu, que la circonstance que la délibération attaquée a été prise en application des dispositions citées ci-après du II de l'article 24 de la loi du 6 janvier 1978 alors qu'elle se substitue à la délibération n° 86-116 du 2 décembre 1986 qui avait été prise sur le fondement de l'article 17 de la même loi, dans sa version alors en vigueur, est sans incidence sur sa légalité ;<br/>
<br/>
              3. Considérant, en deuxième lieu, qu'aux termes du dernier alinéa de l'article 13 de la loi du 6 janvier 1978 : " La commission établit un règlement intérieur. Ce règlement fixe les règles relatives à l'organisation et au fonctionnement de la commission. Il précise notamment les règles relatives aux délibérations, à l'instruction des dossiers et à leur présentation devant la commission, ainsi que les modalités de mise en oeuvre de la procédure de labellisation prévue au c du 3° de l'article 11 " ; que si les requérants peuvent utilement se prévaloir de la méconnaissance du règlement intérieur de la CNIL, auquel renvoient les dispositions précitées de la loi et qui a été adopté, dans sa rédaction applicable à la date de la décision attaquée, par la délibération n° 2006-147 du 23 mai 2006, modifiée par la délibération n° 2011-249 du 8 septembre 2011, ni l'absence d'indication, au procès-verbal de la séance plénière de la CNIL du 7 juin 2012, sur la présence ou l'absence d'un représentant du Défenseur des droits, ni la circonstance que ce procès-verbal ne permettrait pas de s'assurer de la qualité de son signataire ni de ce que la délibération a été adoptée à la majorité ne méconnaissent les prescriptions du règlement intérieur alors en vigueur ;<br/>
<br/>
              4. Considérant, par ailleurs, qu'il ressort des pièces du dossier que, contrairement à ce qui est soutenu, les membres de la CNIL ont disposé des éléments nécessaires pour délibérer en connaissance de cause ; que la circonstance, qui ne méconnaît par elle-même ni le principe de loyauté, ni le caractère équitable de la procédure juridictionnelle, que ce procès-verbal a été produit en défense par la CNIL dans une version comportant certains passages occultés, ne révèle pas que la délibération attaquée n'aurait pas été adoptée à la majorité des voix ; <br/>
<br/>
              5 Considérant qu'il résulte de ce qui précède que les moyens tirés de ce que la délibération litigieuse de la CNIL aurait été adoptée à l'issue d'une procédure irrégulière ne peuvent qu'être écartés ;<br/>
<br/>
              6. Considérant, en troisième lieu, qu'aux termes de l'article 24 de la loi du 6 janvier 1978, dans sa version applicable à la date de la délibération attaquée : " I. - Pour les catégories les plus courantes de traitements de données à caractère personnel, dont la mise en oeuvre n'est pas susceptible de porter atteinte à la vie privée ou aux libertés, la Commission nationale de l'informatique et des libertés établit et publie, après avoir reçu le cas échéant les propositions formulées par les représentants des organismes publics et privés représentatifs, des normes destinées à simplifier l'obligation de déclaration./ Ces normes précisent :/ 1° Les finalités des traitements faisant l'objet d'une déclaration simplifiée ;/ 2° Les données à caractère personnel ou catégories de données à caractère personnel traitées ;/ 3° La ou les catégories de personnes concernées ;/ 4° Les destinataires ou catégories de destinataires auxquels les données à caractère personnel sont communiquées ; / 5° La durée de conservation des données à caractère personnel./ Les traitements qui correspondent à l'une de ces normes font l'objet d'une déclaration simplifiée de conformité envoyée à la commission, le cas échéant par voie électronique./ II. - La commission peut définir, parmi les catégories de traitements mentionnés au I, celles qui, compte tenu de leurs finalités, de leurs destinataires ou catégories de destinataires, des données à caractère personnel traitées, de la durée de conservation de celles-ci et des catégories de personnes concernées, sont dispensées de déclaration./ Dans les mêmes conditions, la commission peut autoriser les responsables de certaines catégories de traitements à procéder à une déclaration unique selon les dispositions du II de l'article 23 " ; qu'il résulte de ces dispositions qu'il appartient à la CNIL de décider si les traitements les plus courants de données à caractère personnel, dont la mise en oeuvre n'est pas susceptible de porter atteinte à la vie privée ou aux libertés, doivent relever de la procédure de déclaration simplifiée de conformité à des normes qu'elle a précédemment établies, de la dispense de déclaration, ou encore, pour certaines catégories, de la déclaration unique ;<br/>
<br/>
              7. Considérant, d'une part, que les traitements litigieux ont pour objet de permettre une meilleure gestion administrative, comptable et pédagogique des écoles et des établissements d'enseignement secondaire des secteurs public et privé ; qu'au regard de cet objectif, la collecte des données personnelles qu'ils prévoient est pertinente ; qu'en effet, les données relatives aux absences et aux sanctions disciplinaires, ainsi qu'aux voeux d'orientation se rapportent à la scolarité des élèves ; que celles qui concernent les remises et réductions tarifaires permettent d'appréhender la situation financière du foyer auquel appartiennent les élèves ; que la collecte des données relatives à la catégorie socioprofessionnelle du ou des responsables légaux de l'élève permet quant à elle l'établissement, par le ministère de l'éducation nationale, de statistiques anonymes ; que, d'autre part, la CNIL a, conformément aux dispositions précitées du II de l'article 24 de la loi du 6 janvier 1978, subordonné la dispense de déclaration des traitements litigieux au strict respect de prescriptions relatives à leurs finalités, à leurs destinataires ou catégories de destinataires ainsi qu'à la durée de conservation des données traitées ; que, dans ces conditions et alors même que les données collectées concernent principalement des personnes mineures, la CNIL a fait une exacte application des dispositions précitées en décidant d'exonérer de déclaration les traitements en cause ; <br/>
<br/>
              8. Considérant, en quatrième lieu, qu'eu égard, d'une part, aux finalités des traitements litigieux, rappelées au point précédent, et à la nature des données collectées, qui sont, ainsi qu'il a été dit, en adéquation avec ces finalités, auxquelles elles sont proportionnées, et compte tenu, d'autre part, tant des conditions de leur collecte, qui suppose, s'agissant des données relatives à la catégorie socio-professionnelle et au numéro de téléphone professionnel, le consentement du ou des responsables légaux des élèves concernés, que des restrictions d'accès qui sont prévues, la dispense de déclaration préalable de ces traitements ne porte pas au droit au respect de la vie privée et familiale des intéressés une atteinte excessive de nature à caractériser une méconnaissance de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; qu'il suit de là que le moyen tiré de ce que la collecte de ces données méconnaîtrait l'article 14 de la convention est inopérant, dès lors que la méconnaissance d'aucun autre droit garanti par la convention n'est invoquée ; <br/>
<br/>
              9. Considérant, en cinquième lieu, que le a) de l'article 3 de la délibération du 7 juin 2012 dispose que n'est autorisé, s'agissant de l'identité des élèves, que le traitement des données suivantes : " nom, prénoms, sexe, date et lieu de naissance, adresse, adresse électronique de l'élève fournie par l'établissement, nombre de frères et soeurs scolarisés, et, à titre facultatif et unique si l'intéressé y consent : la nationalité (uniquement en vue de l'établissement par le ministère de traitements statistiques anonymes), l'adresse électronique personnelle de l'élève, le numéro de téléphone portable de l'élève " ; que, contrairement à ce qui est soutenu, la circonstance qu'en application du g) de l'article 2 de la délibération attaquée, les traitements litigieux puissent avoir pour finalité l'interfaçage technique, notamment avec " le traitement BE1D (base élèves premier degré), pour faciliter la gestion administrative, pédagogique et financière des élèves du premier degré ", traitement qui comporte le " numéro national identifiant élève ", n'implique pas nécessairement que la mention de ce numéro figure parmi les données relatives à l'" identité de l'élève "; qu'il suit de là que le moyen tiré de ce que la délibération attaquée serait, du fait de la contradiction qui l'entacherait sur ce point, illégale ne peut qu'être écarté ;<br/>
<br/>
              10. Considérant, en sixième lieu, que s'il est soutenu que le ministère de l'éducation nationale confie la gestion des traitements en cause à des personnels qui n'entrent pas dans les catégories de destinataires des informations limitativement énumérées à l'article 5 de la délibération attaquée, les modalités selon lesquelles ces traitements sont gérés sont sans incidence sur la légalité de la délibération ; qu'il en est de même de la circonstance que M. D... n'aurait pas obtenu de l'établissement qui accueille ses enfants que les données facultatives à caractère personnel qui les concernent soient effacées, en vertu du droit de rectification ouvert par la loi du 6 janvier 1978 ;<br/>
<br/>
              11. Considérant qu'il résulte de tout ce qui précède que la requête de M. et Mme B...et de M. D...doit être rejetée, y compris les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. et Mme B...et de M. D...est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. et Mme A...B..., à M. C...D..., à la Commission nationale de l'informatique et des libertés et à la ministre de l'éducation nationale.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-035-04 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. ACTES RÉGLEMENTAIRES. VIOLATION D'UN ACTE ÉMANANT D'UNE AUTRE AUTORITÉ. - MOYEN TIRÉ DE LA MÉCONNAISSANCE DU RÈGLEMENT INTÉRIEUR DE LA CNIL AUQUEL LA LOI RENVOIE - OPÉRANCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">26-07-10 DROITS CIVILS ET INDIVIDUELS. - 1) MOYEN TIRÉ DE LA MÉCONNAISSANCE DU RÈGLEMENT INTÉRIEUR DE LA CNIL AUQUEL RENVOIE LA LOI - OPÉRANCE [RJ1] - 2) DISPENSE DE DÉCLARATION D'UN TRAITEMENT DE DONNÉES À CARACTÈRE PERSONNEL - CONTRÔLE DU JUGE - CONTRÔLE NORMAL.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-07-01-04-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. MOYENS INOPÉRANTS. - MOYEN TIRÉ DE LA MÉCONNAISSANCE DU RÈGLEMENT INTÉRIEUR DE LA CNIL AUQUEL LA LOI RENVOIE - OPÉRANCE [RJ1].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">54-07-02-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR. APPRÉCIATIONS SOUMISES À UN CONTRÔLE NORMAL. - DÉCISION DE LA CNIL DE DISPENSER DE DÉCLARATION UN TRAITEMENT DE DONNÉES À CARACTÈRE PERSONNEL.
</SCT>
<ANA ID="9A"> 01-04-035-04 Un requérant peut utilement se prévaloir de la méconnaissance du règlement intérieur de la Commission nationale de l'informatique et des libertés (CNIL), auquel renvoient les dispositions du dernier alinéa de l'article 13 de la loi n° 78-17 du 6 janvier 1978, selon lesquelles la CNIL établit un règlement intérieur qui fixe les règles relatives à l'organisation et au fonctionnement de la commission et précise notamment les règles relatives aux délibérations, à l'instruction des dossiers et à leur présentation devant la commission.</ANA>
<ANA ID="9B"> 26-07-10 1) Un requérant peut utilement se prévaloir de la méconnaissance du règlement intérieur de la Commission nationale de l'informatique et des libertés (CNIL), auquel renvoient les dispositions du dernier alinéa de l'article 13 de la loi n° 78-17 du 6 janvier 1978, selon lesquelles la CNIL établit un règlement intérieur qui fixe les règles relatives à l'organisation et au fonctionnement de la commission et précise notamment les règles relatives aux délibérations, à l'instruction des dossiers et à leur présentation devant la commission.,,,2) Le juge de l'excès de pouvoir exerce un contrôle normal sur la décision de la CNIL de dispenser de déclaration, sur le fondement  du II de l'article 24 de la loi du 6 janvier 1978,  un traitement de données à caractère personnel.</ANA>
<ANA ID="9C"> 54-07-01-04-03 Un requérant peut utilement se prévaloir de la méconnaissance du règlement intérieur de la Commission nationale de l'informatique et des libertés (CNIL), auquel renvoient les dispositions du dernier alinéa de l'article 13 de la loi n° 78-17 du 6 janvier 1978, selon lesquelles la CNIL établit un règlement intérieur qui fixe les règles relatives à l'organisation et au fonctionnement de la commission et précise notamment les règles relatives aux délibérations, à l'instruction des dossiers et à leur présentation devant la commission.</ANA>
<ANA ID="9D"> 54-07-02-03 Le juge de l'excès de pouvoir exerce un contrôle normal sur la décision de la Commission nationale de l'informatique et des libertés (CNIL) de dispenser de déclaration, sur le fondement  du II de l'article 24 de la loi n° 78-17 du 6 janvier 1978,  un traitement de données à caractère personnel.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., dans le cas de la consultation d'un organisme par une autorité avant l'adoption d'un acte, CE, 25 janvier 2012, Association nationale des psychologues de la petite enfance et autres, n° 342210 342296, T. pp. 548-937-997 ; CE, 10 février 2014, Syndicat viticole de Cussac-Fort-Médoc, n°356113, p. 25.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
