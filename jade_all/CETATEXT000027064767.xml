<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027064767</ID>
<ANCIEN_ID>JG_L_2013_02_000000363533</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/06/47/CETATEXT000027064767.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 13/02/2013, 363533</TITRE>
<DATE_DEC>2013-02-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>363533</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. David Gaudillère</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CESSR:2013:363533.20130213</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'arrêt n° 12NT00517 du 18 octobre 2012, enregistré le 24 octobre 2012 au secrétariat du contentieux du Conseil d'Etat, par lequel la cour administrative d'appel de Nantes, avant de statuer sur la requête de M. B...A...tendant, d'une part, à l'annulation du jugement n° 11-3926,11-3927 en date du 19 janvier 2012 par lequel le tribunal administratif de Rennes a rejeté sa demande tendant à l'annulation pour excès de pouvoir de l'arrêté du 16 septembre 2011 du préfet des Côtes-d'Armor portant obligation de quitter le territoire français dans un délai de trente jours et interdiction de retour pendant deux ans, d'autre part, à l'annulation pour excès de pouvoir de cet arrêté et à ce qu'il soit enjoint au préfet de lui délivrer une autorisation provisoire de séjour lui permettant de travailler et de réexaminer sa situation dans un délai d'un mois, a décidé, par application des dispositions de l'article L. 113-1 du code de justice administrative, de transmettre le dossier de cette demande au Conseil d'Etat, en soumettant à son examen la question de savoir si, dans le cadre des dispositions du code de l'entrée et du séjour des étrangers et du droit d'asile issues de la loi n° 2011-672 du 16 juin 2011, le refus implicite opposé, par application de l'article R. 311-12 du même code, à une demande de titre de séjour formulée par un étranger peut désormais servir de base légale à une décision portant obligation de quitter le territoire prise sur le fondement du 3° du I de l'article L. 511-1 de ce code ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              Vu le code de justice administrative, notamment son article L. 113-1 ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. David Gaudillère, Auditeur,  <br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              REND L'AVIS SUIVANT <br/>
<br/>
<br/>
              1.	Dans sa rédaction résultant de la loi du 24 juillet 2006 relative à l'intégration et à l'immigration, le I de l'article L. 511-1 du code de l'entrée et du séjour des étrangers et du droit d'asile prévoyait que l'autorité administrative qui refusait la délivrance ou le renouvellement d'un titre de séjour à un étranger, ou qui lui retirait son titre de séjour, son récépissé de demande de carte de séjour ou son autorisation provisoire de séjour, pour un motif autre que l'existence d'une menace à l'ordre public, pouvait assortir sa décision d'une obligation de quitter le territoire français, laquelle fixait aussi le pays à destination duquel l'étranger pouvait être renvoyé d'office faute de respecter le délai de départ volontaire d'une durée d'un mois qui lui était laissé. Parallèlement, le II de l'article L. 511-1 permettait à l'autorité administrative, par arrêté motivé, de décider qu'un étranger serait reconduit à la frontière dans différents cas, tenant notamment à l'entrée irrégulière en France, au maintien irrégulier sur le territoire français, au prononcé de certaines condamnations ou à des menaces à l'ordre public.<br/>
<br/>
              Le Conseil d'Etat, par un avis contentieux n° 311893 du 28 mars 2008, avait estimé qu'il résultait de ces dispositions que le préfet ne pouvait prendre une décision obligeant un étranger à quitter le territoire français sans lui avoir dans la même décision refusé, de manière explicite, un titre de séjour. Il avait, dès lors, été d'avis qu'un refus implicite résultant du silence gardé par l'administration sur une demande de titre de séjour dans le délai de quatre mois prévu par l'article R. 311-12 du code de l'entrée et du séjour des étrangers et du droit d'asile ne pouvait permettre de prendre une obligation de quitter le territoire français et qu'une telle mesure ne pouvait intervenir qu'après que l'administration eut opposé, à nouveau et de manière explicite, un refus à la demande de titre de séjour.<br/>
<br/>
              2.	La loi du 16 juin 2011 relative à l'immigration, à l'intégration et à la nationalité a substantiellement modifié les dispositions de l'article L. 511-1 du code de l'entrée et du séjour des étrangers et du droit d'asile.<br/>
<br/>
              Alors que, selon les dispositions issues de la loi du 24 juillet 2006, l'obligation de quitter le territoire français constituait une mesure particulière d'éloignement, qui n'était susceptible d'être prononcée que pour assortir un refus de délivrance ou de renouvellement d'un titre de séjour, ou d'un retrait d'un tel titre pour un autre motif que la menace à l'ordre public, le I de l'article L. 511-1 dans sa rédaction issue de la loi du 16 juin 2011 permet désormais à l'autorité administrative de prononcer une obligation de quitter le territoire français dans les différents cas suivants : " 1° Si l'étranger ne peut justifier être entré régulièrement sur le territoire français, à moins qu'il ne soit titulaire d'un titre de séjour en cours de validité ; / 2° Si l'étranger s'est maintenu sur le territoire français au-delà de la durée de validité de son visa ou, s'il n'est pas soumis à l'obligation du visa, à l'expiration d'un délai de trois mois à compter de son entrée sur le territoire sans être titulaire d'un premier titre de séjour régulièrement délivré ; / 3° Si la délivrance ou le renouvellement d'un titre de séjour a été refusé à l'étranger ou si le titre de séjour qui lui avait été délivré lui a été retiré ; / 4° Si l'étranger n'a pas demandé le renouvellement de son titre de séjour temporaire et s'est maintenu sur le territoire français à l'expiration de ce titre ; / 5° Si le récépissé de la demande de carte de séjour ou l'autorisation provisoire de séjour qui avait été délivré à l'étranger lui a été retiré ou si le renouvellement de ces documents lui a été refusé ".<br/>
<br/>
              Il résulte de ces nouvelles dispositions que l'administration est désormais susceptible de prononcer une obligation de quitter le territoire français dans différents cas, tenant à l'entrée irrégulière en France, au maintien irrégulier sur le territoire national, au refus de délivrance ou de renouvellement d'un titre de séjour ou de retrait d'un tel titre, au retrait ou au refus de renouvellement d'un récépissé ou d'une autorisation provisoire de séjour. Cette mesure d'éloignement n'est ainsi plus spécifiquement liée au cas du refus de séjour.<br/>
<br/>
              Si rien ne s'oppose à ce que l'administration, comme elle le fait de façon générale, permette le maintien sur le territoire d'un étranger qui a demandé un titre de séjour jusqu'à ce qu'elle ait statué par une décision explicite sur la demande de séjour, ni les dispositions de l'article L. 511-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, telles qu'issues de la loi du 16 juin 2011, ni aucune disposition du code ne font obstacle à ce que, en application des dispositions de l'article 21 de la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations et de l'article R. 311-12 du code de l'entrée et du séjour des étrangers et du droit d'asile, le silence gardé par l'administration pendant plus de quatre mois sur une demande de titre de séjour vaille décision de rejet et à ce que ce rejet implicite permette directement de prononcer une obligation de quitter le territoire français.<br/>
<br/>
              Il s'ensuit que l'autorité administrative peut prononcer une obligation de quitter le territoire français lorsque le silence gardé pendant quatre mois sur une demande de délivrance ou de renouvellement d'un titre de séjour a fait naître une décision implicite de rejet, sans qu'il lui soit impératif d'opposer au préalable un refus explicite de titre de séjour. <br/>
<br/>
              3.	En vertu du septième alinéa du I de l'article L. 511-1, la décision énonçant l'obligation de quitter le territoire français doit être motivée.<br/>
<br/>
              Si la loi prévoit que cette décision n'a pas à faire l'objet d'une motivation distincte de celle de la décision relative au séjour dans les cas où la mesure d'éloignement fait suite à un refus de délivrance ou de renouvellement d'un titre de séjour ou retrait d'un tel titre, ou au retrait ou au refus de renouvellement d'un récépissé de demande de carte de séjour ou d'autorisation provisoire de séjour, cette exception à l'obligation de motivation ne peut trouver à s'appliquer que si la mesure d'éloignement assortit une décision relative au séjour elle-même explicite et motivée.<br/>
<br/>
              Il incombe ainsi à l'autorité administrative, dans le cas où elle prononcerait une obligation de quitter le territoire français à la suite d'un refus implicite de délivrance ou de renouvellement de titre de séjour, de motiver sa décision en indiquant les circonstances de fait et les considérations de droit qui la justifient, sans qu'elle puisse se borner à motiver sa décision par référence à l'existence d'un refus implicite de titre de séjour. <br/>
<br/>
<br/>
<br/>
              Le présent avis sera notifié à la cour administrative d'appel de Nantes, à M. B... A...et au ministre de l'intérieur.<br/>
<br/>
              Il sera publié au Journal officiel de la République française.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
