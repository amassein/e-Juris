<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036845237</ID>
<ANCIEN_ID>JG_L_2018_04_000000409021</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/84/52/CETATEXT000036845237.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 6ème chambres réunies, 26/04/2018, 409021, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-04-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>409021</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Florian Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:409021.20180426</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'Office public d'aménagement et de construction de l'Oise (OPAC de l'Oise) a demandé au tribunal administratif de Paris d'annuler la décision du 12 février 2014 par laquelle la Caisse de garantie du logement locatif social (CGLLS) a rejeté sa demande de dégrèvement de cotisation additionnelle pour les années 2010, 2011 et 2012. Par un jugement n° 1405990 du 12 juin 2015, le tribunal administratif de Paris a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 15PA03100 du 23 janvier 2017, la cour administrative d'appel de Paris a, sur appel de l'OPAC de l'Oise, annulé ce jugement et fait droit à la demande de décharge de l'OPAC.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 17 mars et 15 juin 2017 au secrétariat du contentieux du Conseil d'Etat, la CGLLS demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel formé par l'OPAC de l'Oise contre le jugement ;<br/>
<br/>
              3°) de mettre à la charge de l'OPAC de l'Oise la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le code de la construction et de l'habitation ;<br/>
<br/>
              - le livre des procédures fiscales ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la Caisse de garantie du logement locatif social et à la SCP Piwnica, Molinié, avocat de l'Office public d'aménagement et de construction de l'Oise.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 11 avril 2018, présentée par l'OPAC de l'Oise ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, le 3 juin 2013, la Caisse de garantie du logement locatif social (CGLLS) a adressé à l'Office public de l'habitat (OPAC) de l'Oise une proposition de rectification portant sur la cotisation additionnelle due par l'établissement au titre des années 2010, 2011 et 2012 ; que, le 7 novembre 2013, elle a mis en recouvrement une somme de 337 634 euros correspondant au rappel de cotisations ; que sa réclamation ayant été rejetée le 12 février 2014, l'OPAC a saisi le tribunal administratif de Paris d'une demande tendant à la décharge du rappel de la cotisation additionnelle ; que, par un jugement du 12 juin 2015, le tribunal administratif de Paris a rejeté sa demande ; que, par un arrêt du 23 janvier 2017, la cour administrative d'appel de Paris a, sur appel de l'OPAC, annulé ce jugement et fait droit à sa demande de décharge ; que la CGLLS se pourvoit en cassation contre cet arrêt ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 452-4-1 du code de la construction et de l'habitation, dans sa rédaction alors en vigueur : " Les organismes d'habitations à loyer modéré (...)  versent, au premier trimestre de chaque année, une cotisation additionnelle à la Caisse de garantie du logement locatif social. Elle est due pour l'année entière par le redevable qui exerce l'activité assujettie à cette cotisation le 1er janvier. La cotisation additionnelle comprend :/ (...)  b) Une part variable qui a pour assiette l'autofinancement net de l'organisme établi à partir des comptes annuels de l'avant-dernier exercice clos. L'autofinancement net est calculé en déduisant les remboursements d'emprunts liés à l'activité locative, à l'exception des remboursements anticipés, de la différence entre les produits et les charges de l'exercice. (...) Le montant de la part variable est calculé en appliquant à la base ainsi déterminée un taux fixé, dans les limites de 15 %, par un arrêté pris dans les mêmes formes " ; qu'en vertu de l'article L. 452-5, dans sa rédaction alors applicable, la cotisation additionnelle est versée par les redevables à la Caisse de garantie du logement locatif social (CGLLS) sur la base d'une déclaration dont le modèle est fixé par l'autorité administrative ; <br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions que pour calculer l'autofinancement net, qui constitue l'assiette de la part variable de la cotisation additionnelle, il y a lieu de déduire de la différence entre les produits et les charges de l'exercice les seuls remboursements d'emprunts liés à des opérations immobilières ayant généré des revenus locatifs au titre de cet exercice, à l'exception des remboursements anticipés ; que ne peuvent, en conséquence, être déduits de l'autofinancement net les remboursements d'emprunts afférents à des immeubles ou parties d'immeubles démolis, cédés ou sortis de l'actif ;<br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la CGLLS a motivé le rappel de la cotisation additionnelle due par l'OPAC de l'Oise au titre des exercices 2010, 2011 et 2012 par la circonstance que l'établissement avait déduit à tort de l'autofinancement net les remboursements d'emprunts se rapportant à des immeubles ou parties d'immeubles démolis, cédés ou sortis de l'actif ; que, pour faire droit à la demande de l'OPAC tendant à la décharge du rappel de cotisations dont il a fait l'objet, la cour a retenu qu'en réintégrant ces catégories d'emprunts dans l'assiette de la cotisation, la CGLLS avait méconnu les dispositions de l'article L. 452-4-1 du code de la construction et de l'habitation ; qu'il résulte de ce qui précède qu'en statuant ainsi, la cour a commis une erreur de droit ;<br/>
<br/>
              5. Considérant que, par suite, et sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, la CGLLS est fondée à demander l'annulation de l'arrêt attaqué ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de la CGLLS une somme au titre des frais exposés par l'OPAC de l'Oise et non compris dans les dépens ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'OPAC de l'Oise le versement à la CGLLS d'une somme de 3 000 euros au titre de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 23 janvier 2017 est annulé.<br/>
<br/>
		Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br/>
<br/>
Article 3 : L'OPAC de l'Oise versera à la Caisse de garantie du logement locatif social une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la Caisse de garantie du logement locatif social et à l'Office public d'aménagement et de construction de l'Oise. <br/>
Copie en sera adressée au ministre de la cohésion des territoires.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
