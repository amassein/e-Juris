<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034017903</ID>
<ANCIEN_ID>JG_L_2017_02_000000393311</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/01/79/CETATEXT000034017903.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème chambres réunies, 08/02/2017, 393311</TITRE>
<DATE_DEC>2017-02-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>393311</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE ; SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>M. Frédéric Puigserver</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:393311.20170208</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Polyclinique Saint-Jean a demandé au tribunal administratif de Nice d'annuler un titre de recettes et cinquante-quatre avis de sommes à payer valant titres exécutoires émis à son encontre par le centre hospitalier universitaire (CHU) de Nice pour un montant total de 48 551,62 euros. Par un jugement n°s 1302258, 1302260 du 21 novembre 2014, le tribunal d'administratif de Nice a annulé les titres contestés et a déchargé la société du paiement des sommes correspondantes.<br/>
<br/>
              Par un arrêt n° 15MA00226 du 16 juillet 2015, la cour administrative d'appel de Marseille, saisie par le CHU de Nice, a annulé l'article 1er du jugement du tribunal administratif de Nice du 21 novembre 2014 en tant qu'il prononce l'annulation de certains de ces titres et qu'il décharge la société Polyclinique Saint-Jean du paiement des sommes correspondantes et rejeté, dans cette limite, la demande de première instance de la société Polyclinique Saint-Jean.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 9 septembre et 8 décembre 2015 au secrétariat du contentieux du Conseil d'Etat, la société Polyclinique Saint-Jean demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt de la cour administrative d'appel de Marseille du 16 juillet 2015 en tant qu'il a partiellement annulé l'article 1er du jugement du tribunal administratif de Nice et rejeté les conclusions correspondantes ;<br/>
<br/>
              2°) de mettre à la charge du centre hospitalier universitaire de Nice la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de la santé publique ;<br/>
              - le décret n° 62-1587 du 29 décembre 1962 ;<br/>
              - le décret n° 2012-1246 du 7 novembre 2012 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Puigserver, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de la société Polyclinique Saint-Jean et à la SCP Célice, Soltner, Texidor, Perier, avocat du centre hospitalier universitaire de Nice.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que le centre hospitalier universitaire (CHU) de Nice, qui dispose d'une structure mobile d'urgence et de réanimation (SMUR), a assuré, à la demande de la polyclinique Saint-Jean, établissement de santé privé autorisé à assurer un service d'urgence, le transport de certains de ses patients vers d'autres établissements de santé et a facturé à la polyclinique les prestations correspondantes. Cette dernière a demandé au tribunal administratif de Nice, qui a fait droit à sa demande par un jugement du 21 novembre 2014, d'annuler un titre de recettes et cinquante-quatre avis de sommes à payer valant titre exécutoire émis entre février 2012 et janvier 2013, correspondant à des transports effectués entre décembre 2011 et décembre 2012, pour un montant total de 48 551,62 euros. Elle se pourvoit en cassation contre l'arrêt de la cour administrative d'appel de Marseille du 16 juillet 2015 en tant que, accueillant partiellement l'appel du CHU de Nice, cet arrêt rejette ses demandes portant sur trente-trois titres et réforme le jugement du tribunal administratif de Nice en conséquence. <br/>
<br/>
              Sur le cadre juridique du litige :<br/>
<br/>
              2. Aux termes de l'article L. 6112-1 du code de la santé publique, dans sa rédaction applicable au litige : " Les établissements de santé peuvent être appelés à assurer, en tout ou partie, une ou plusieurs des missions de service public suivantes : (...) 8° L'aide médicale urgente (...) ". Aux termes de l'article L. 6311-1 du même code : " L'aide médicale urgente a pour objet, en relation notamment avec les dispositifs communaux et départementaux d'organisation des secours, de faire assurer aux malades, blessés et parturientes, en quelque endroit qu'ils se trouvent, les soins d'urgence appropriés à leur état ". Aux termes de l'article L. 6311-2 du même code : " Seuls les établissements de santé peuvent être autorisés (...) à comporter une ou plusieurs unités participant au service d'aide médicale urgente, dont les missions et l'organisation sont fixées par voie réglementaire. / Un centre de réception et de régulation des appels est installé dans les services d'aide médicale urgente. (...) Les services d'aide médicale urgente (...) sont tenus d'assurer le transport des patients pris en charge dans le plus proche des établissements offrant des moyens disponibles adaptés à leur état, sous réserve du respect du libre choix ". Il résulte de l'article R. 6311-2 de ce code qu'à cette fin, ils " organisent, le cas échéant, le transport dans un établissement public ou privé en faisant appel à un service public ou à une entreprise privée de transports sanitaires ".<br/>
<br/>
              3. Aux termes de l'article R. 6123-1 du code de la santé publique : " L'exercice par un établissement de santé de l'activité de soins de médecine d'urgence (...) est autorisé selon une ou plusieurs des trois modalités suivantes : / 1° La régulation des appels adressés au service d'aide médicale urgente mentionné à l'article L. 6112-5 ; / 2° La prise en charge des patients par la structure mobile d'urgence et de réanimation, appelée SMUR (...) ; / 3° La prise en charge des patients accueillis dans la structure des urgences (...) ". Aux termes de son article R. 6123-15 : " Dans le cadre de l'aide médicale urgente, la structure mobile d'urgence et de réanimation mentionnée à l'article R. 6123-1 a pour mission : / 1° D'assurer, en permanence, en tous lieux et prioritairement hors de l'établissement de santé auquel il est rattaché, la prise en charge d'un patient dont l'état requiert de façon urgente une prise en charge médicale et de réanimation, et, le cas échéant, et après régulation par le SAMU [service d'aide médicale urgente], le transport de ce patient vers un établissement de santé ; / 2° D'assurer le transfert entre deux établissements de santé d'un patient nécessitant une prise en charge médicale pendant le trajet. / Pour l'exercice de ces missions, l'équipe d'intervention de la structure mobile d'urgence et de réanimation comprend un médecin ". Aux termes de son article R. 6123-16 : " Les interventions des SMUR (...) sont déclenchées et coordonnées par le SAMU ". A cette fin, le médecin régulateur du service d'aide médicale urgente (SAMU) peut, en vertu de l'article L. 1111-17 du même code, accéder, sauf opposition expresse précédemment manifestée par le patient, au dossier médical personnel de celui-ci. <br/>
<br/>
              4. Enfin, en vertu de l'article R. 6123-18 du code de la santé publique, tout établissement autorisé à exercer une prise en charge des patients dans une structure des urgences est tenu d'y accueillir en permanence toute personne qui s'y présente en situation d'urgence ou qui lui est adressée, notamment par le SAMU. L'article R. 6123-19 de ce code précise que : " Pour assurer, postérieurement à son accueil, l'observation, les soins et la surveillance du patient jusqu'à son orientation, l'établissement organise la prise en charge diagnostique et thérapeutique selon le cas : (...) 5° En liaison avec le SAMU, en l'orientant vers un autre établissement de santé apte à le prendre en charge et, si nécessaire, en assurant ou en faisant assurer son transfert (...) ".  <br/>
<br/>
              5. Il résulte des dispositions du code de la santé publique mentionnées ci-dessus que les établissements de santé autorisés à prendre en charge des patients accueillis dans une structure des urgences sont responsables, lorsqu'elle est médicalement nécessaire, de l'orientation de ces personnes vers l'établissement de santé apte à les prendre en charge, en liaison avec le SAMU. Dans un tel cas, le transport du patient vers cet établissement peut être assuré, conformément à l'article R. 6311-2 de ce code, en faisant appel, selon les besoins du patient, à une entreprise privée de transport sanitaire ou à un service public, notamment à leur propre structure mobile d'urgence et de réanimation s'ils en ont une ou celle d'un autre établissement. La décision de transporter un patient par une structure mobile d'urgence et de réanimation, qui ne peut agir que dans le cadre de sa mission de service public d'aide médicale urgente, limitativement définie à l'article R. 6123-15 du code de la santé publique, est prise, sous sa responsabilité, par le médecin régulateur du SAMU, qui a estimé cette intervention médicalement justifiée au regard de l'état du patient. <br/>
<br/>
              Sur l'arrêt de la cour : <br/>
<br/>
              6. La cour administrative d'appel de Marseille a jugé que le transfert d'un patient entre deux établissements assuré par une structure mobile d'urgence et de réanimation ne relevait pas nécessairement de l'aide médicale urgente telle qu'elle est définie par l'article L. 6311-1 du code de la santé publique, ce dont elle a déduit que certaines des interventions d'une telle structure, au seul motif que le transfert n'avait pas pour objet de faire assurer au patient des soins d'urgence dans l'établissement de destination, ne relevaient pas d'un financement par la dotation nationale de financement des missions d'intérêt général et d'aide à la contractualisation, mentionnée aux articles L. 162-22-13 et D. 162-6 du code de la sécurité sociale, au titre de l'aide médicale urgente. En statuant ainsi, la cour a commis une erreur de droit dès lors, ainsi qu'il résulte du cadre juridique précisé ci-dessus, qu'une structure mobile d'urgence n'intervient que dans le cadre de sa mission de service public d'aide médicale urgente, sur décision du médecin régulateur du SAMU. <br/>
<br/>
              7. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que la polyclinique Saint-Jean est fondée à demander l'annulation des articles 1er et 2 de l'arrêt attaqué, sauf en ce qui concerne les avis de sommes à payer n°s 0411583, 0411853 et 0053313, pour lesquels la cour a annulé le jugement du tribunal administratif de Nice et rejeté les conclusions de la demande présentée à ce tribunal par des motifs que la polyclinique ne critique pas.<br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du CHU de Nice la somme de 3 000 euros à verser à la polyclinique Saint-Jean au titre de l'article L. 761-1 du code de justice administrative. Ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la polyclinique Saint-Jean, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 1er et 2 de l'arrêt de la cour administrative d'appel de Marseille du 16 juillet 2015, sauf en ce qui concerne les avis de sommes à payer n°s 0411583, 0411853 et 0053313, sont annulés.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille dans la mesure de la cassation prononcée.<br/>
Article 3 : Le CHU de Nice versera à la polyclinique Saint-Jean la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions du CHU de Nice présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la société Polyclinique Saint-Jean et au centre hospitalier universitaire de Nice.<br/>
Copie en sera adressé à la ministre des affaires sociales et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">61-01-02 SANTÉ PUBLIQUE. PROTECTION GÉNÉRALE DE LA SANTÉ PUBLIQUE. TRANSPORTS SANITAIRES. - TRANSPORT PAR UNE STRUCTURE MOBILE D'URGENCE ET DE RÉANIMATION -TRANSPORT RELEVANT NÉCESSAIREMENT DE L'AIDE MÉDICALE URGENTE - EXISTENCE -  DÉCISIONNAIRE - MÉDECIN RÉGULATEUR DU SAMU.
</SCT>
<ANA ID="9A"> 61-01-02 Les établissements de santé autorisés à prendre en charge des patients accueillis dans une structure des urgences sont responsables, lorsqu'elle est médicalement nécessaire, de l'orientation de ces personnes vers l'établissement de santé apte à les prendre en charge, en liaison avec le SAMU. Dans un tel cas, le transport du patient vers cet établissement peut être assuré en faisant appel, selon les besoins du patient, à une entreprise privée de transport sanitaire ou à un service public, notamment une structure mobile d'urgence et de réanimation.... ,,La décision de transporter un patient par une structure mobile d'urgence et de réanimation, qui ne peut agir que dans le cadre de sa mission de service public d'aide médicale urgente, limitativement définie à l'article R. 6123-15 du code de la santé publique, est prise, sous sa responsabilité, par le médecin régulateur du SAMU, qui a estimé cette intervention médicalement justifiée au regard de l'état du patient. Commet ainsi une erreur de droit la cour qui juge que le transfert d'un patient entre deux établissements assuré par une structure mobile d'urgence et de réanimation ne relève pas nécessairement de l'aide médicale urgente telle qu'elle est définie par l'article L. 6311-1 du code de la santé publique.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
