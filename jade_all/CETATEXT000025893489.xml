<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025893489</ID>
<ANCIEN_ID>JG_L_2012_05_000000325875</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/89/34/CETATEXT000025893489.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 16/05/2012, 325875, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-05-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>325875</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Philippe Martin</PRESIDENT>
<AVOCATS>SCP COUTARD, MUNIER-APAIRE</AVOCATS>
<RAPPORTEUR>M. Jean-Claude Hassan</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Delphine Hedary</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 9 mars et 5 juin 2009 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme Christiane A, demeurant ... ; Mme A demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 07VE02008 du 11 décembre 2008 par lequel la cour administrative d'appel de Versailles a rejeté sa requête tendant à l'annulation du jugement du 21 juin 2007 du tribunal administratif de Versailles rejetant sa demande tendant à la décharge des cotisations supplémentaires d'impôt sur le revenu auxquelles elle a été assujettie au titre des années 2003 et 2004 et des pénalités dont elles ont été assorties ;<br/>
<br/>
              2°) réglant l'affaire au fond, de prononcer la décharge de ces impositions ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code civil ;<br/>
<br/>
              Vu le code général des impôts ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Claude Hassan, Conseiller d'Etat,<br/>
<br/>
              - les observations de la SCP Coutard, Munier-Apaire, avocat de Mme A,<br/>
<br/>
              - les conclusions de Mme Delphine Hedary, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Coutard, Munier-Apaire, avocat de Mme A ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le jugement de divorce prononcé le 30 septembre 1997 par le tribunal de grande instance d'Evry a notamment pris en compte la circonstance non contestée que Mme A assumait seule la charge du fils majeur handicapé né de son union avec M. B ; que les époux ont conservé, après le divorce, la propriété indivise et à parts égales du bien immobilier qu'ils avaient acquis ensemble pendant leur union ; que, par une convention d'indivision signée en 1998 pour cinq ans, qui a ensuite été renouvelée, ils sont convenus que Mme A percevrait l'intégralité des loyers de ce bien immobilier dont elle assumerait l'intégralité des charges, en contrepartie de ce qu'elle assumait seule l'entretien de leur fils handicapé ; que Mme A se pourvoit en cassation contre l'arrêt du 11 décembre 2008 par lequel la cour administrative d'appel de Versailles, confirmant le jugement rendu par le tribunal administratif de Versailles, a rejeté sa requête tendant à la décharge des cotisations supplémentaires d'impôt sur le revenu auxquelles elle a été assujettie au titre des années 2003 et 2004 du fait de la réintégration dans ses revenus fonciers du montant des charges acquittées au-delà de sa part dans l'indivision ; <br/>
<br/>
              Considérant, en premier lieu, que si Mme A fait valoir que la cour administrative d'appel de Versailles s'est bornée à constater que la convention d'indivision par laquelle M. B lui a abandonné sa part des loyers dans l'immeuble indivis qu'ils possédaient ensemble n'est pas intervenue en exécution du jugement de divorce prononcé le 30 septembre 1997 pour qualifier cet abandon de libéralité, sans rechercher si M. B ne s'acquittait pas ainsi de son obligation d'aliments née des articles 205 et 207 du code civil envers son fils handicapé, cette qualification est sans effet sur le caractère imposable des loyers supplémentaires que l'administration fiscale n'a d'ailleurs pas inclus dans les bases d'imposition de Mme A ; que le moyen est, par suite, inopérant ; <br/>
<br/>
              Considérant, en deuxième lieu, que les charges de l'immeuble correspondant à la part de M. B et que Mme A a acquittées en même temps qu'elle percevait directement la part de loyers revenant à son ancien mari constituaient des charges foncières ; que de telles charges ne peuvent être déduites, en vertu des dispositions des articles 28 et 31 du code général des impôts, que des revenus fonciers du contribuable et ne peuvent s'imputer sur des revenus résultant du versement d'une pension alimentaire ; qu'il résulte de la décision n° 325876 rendue le 16 mai 2012 par le Conseil d'Etat statuant au contentieux, que les loyers perçus par Mme A au-delà de ses propres droits dans l'indivision constituée avec son ancien époux présentaient le caractère d'une pension alimentaire, répondant aux conditions fixées par les articles 205 à 211 du code civil, versée au fils handicapé de Mme A, rattaché à son foyer fiscal ; que, par suite, Mme A n'est pas fondée à demander l'annulation de l'arrêt attaqué, qui est suffisamment motivé, par lequel la cour administrative d'appel de Versailles a rejeté sa requête tendant à obtenir la déduction de ces charges de la pension alimentaire versée par son ancien époux ; <br/>
<br/>
              Considérant qu'il résulte de ce qui précède que les conclusions du pourvoi de Mme A tendant à ce qu'il soit fait application des dispositions de l'article L. 761-1 du code de justice administrative doivent être rejetées ;<br/>
<br/>
<br/>
<br/>				D E C I D E  :<br/>
              				--------------<br/>
<br/>
Article 1er : Le pourvoi de Mme A est rejeté.<br/>
Article 2 : La présente décision sera notifiée à Mme Christiane A et à la ministre du budget, des comptes publics et de la réforme de l'Etat, porte-parole du Gouvernement.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
