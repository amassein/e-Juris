<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042353552</ID>
<ANCIEN_ID>JG_L_2020_09_000000426376</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/35/35/CETATEXT000042353552.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 21/09/2020, 426376</TITRE>
<DATE_DEC>2020-09-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>426376</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BAUER-VIOLAS, FESCHOTTE-DESBOIS, SEBAGH</AVOCATS>
<RAPPORTEUR>M. Christian Fournier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:426376.20200921</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU> Vu la procédure suivante :<br/>
<br/>
              Le préfet de la Seine-Saint-Denis a demandé au tribunal administratif de Montreuil d'annuler la délibération du conseil de territoire de l'établissement public territorial Plaine Commune n° CC-16/1374 du 19 janvier 2016 accordant des indemnités de fonctions aux conseillers territoriaux de l'établissement. Par un jugement n° 1604449 du 2 février 2017, le tribunal administratif de Montreuil a annulé la délibération. Par un arrêt n° 17VE01022 du 18 octobre 2018, la cour administrative d'appel de Versailles a rejeté l'appel formé par l'établissement public territorial Plaine Commune contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 18 décembre 2018 et 18 mars 2019 au secrétariat du contentieux du Conseil d'Etat, l'établissement public territorial Plaine Commune demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre des dispositions des articles L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christian Fournier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Bauer-Violas, Feschotte-Desbois, Sebagh, avocat de l'établissement public territorial Plaine Commune ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juges du fond que par une délibération du 19 janvier 2016, le conseil de territoire de l'établissement public territorial Plaine Commune a fixé le montant de l'indemnité mensuelle des élus territoriaux. Le 11 mars 2016, le préfet de la Seine-Saint-Denis a demandé le retrait de cette décision. Le président de l'établissement l'ayant informé que la délibération ne serait pas rapportée, le préfet de la Seine-Saint-Denis a demandé au tribunal administratif de Montreuil l'annulation de cette délibération. Par un jugement du 2 février 2017, le tribunal administratif de Montreuil a annulé cette délibération. L'établissement public territorial Plaine Commune se pourvoit en cassation contre l'arrêt du 18 octobre 2018 par lequel la cour administrative d'appel de Versailles a rejeté l'appel qu'il a formé contre ce jugement.<br/>
<br/>
              2. Aux termes de l'article L. 5211-12 du code général des collectivités territoriales : " Les indemnités maximales votées par le conseil ou comité d'un syndicat de communes (...), d'une communauté de communes, d'une communauté urbaine, d'une communauté d'agglomération, d'une métropole et d'un syndicat d'agglomération nouvelle pour l'exercice effectif des fonctions de président et de vice-président sont déterminées par un décret en Conseil d'Etat par référence au montant du traitement correspondant à l'indice brut terminal de l'échelle indiciaire de la fonction publique. / Le montant total des indemnités versées ne doit pas excéder celui de l'enveloppe indemnitaire globale, déterminée en additionnant l'indemnité maximale pour l'exercice effectif des fonctions de président et les indemnités maximales pour l'exercice effectif des fonctions de vice-président, correspondant soit au nombre maximal de vice-présidents qui résulterait de l'application des deuxième et troisième alinéas de l'article L. 5211-10 à l'organe délibérant qui comporterait un nombre de membres déterminé en application des III à VI de l'article L. 5211-6-1, soit au nombre existant de vice-présidences effectivement exercées, si celui-ci est inférieur. / De manière dérogatoire, l'indemnité versée à un vice-président peut dépasser le montant de l'indemnité maximale prévue au premier alinéa du présent article, à condition qu'elle ne dépasse pas le montant de l'indemnité maximale susceptible d'être allouée au président et que le montant total des indemnités versées n'excède pas l'enveloppe indemnitaire globale définie au deuxième alinéa. / (...) / Toute délibération de l'organe délibérant d'un établissement public de coopération intercommunale concernant les indemnités de fonction d'un ou de plusieurs de ses membres est accompagnée d'un tableau annexe récapitulant l'ensemble des indemnités allouées aux membres de l'assemblée concernée. (...) ".<br/>
<br/>
              3. Aux termes de l'article L. 5219-2 du même code : " Dans le périmètre de la métropole du Grand Paris, sont créés, au 1er janvier 2016, des établissements publics de coopération intercommunale dénommés " établissements publics territoriaux ". Sous réserve du présent chapitre, ces établissements publics sont soumis aux dispositions applicables aux syndicats de communes. D'un seul tenant et sans enclave, d'au moins 300 000 habitants, ces établissements regroupent l'ensemble des communes membres de la métropole du Grand Paris, à l'exception de la commune Paris. Les communes appartenant à un même établissement public de coopération intercommunale à fiscalité propre à la date de promulgation de la loi n° 2015-991 du 7 août 2015 portant nouvelle organisation territoriale de la République ne peuvent appartenir à des établissements publics territoriaux distincts. / Dans chaque établissement public territorial, il est créé un conseil de territoire composé des délégués des communes incluses dans le périmètre de l'établissement, désignés au conseil de la métropole du Grand Paris en application de l'article L. 5219-9 (...). / Le président du conseil de territoire est élu en son sein. Le conseil de territoire désigne également en son sein un ou plusieurs vice-présidents. Le nombre de ceux-ci ne peut excéder 20 % du nombre total des membres du conseil de territoire (...) ".<br/>
<br/>
              4. Enfin aux termes de l'article L. 5219-2-1 du même code : " Les indemnités votées par le conseil de territoire pour l'exercice effectif des fonctions de président d'un établissement public territorial sont inférieures ou égales à 110 % du terme de référence mentionné au I de l'article L. 2123-20. / Les indemnités votées par le conseil de territoire pour l'exercice effectif des fonctions de vice-président d'un établissement public territorial sont inférieures ou égales à 44 % du terme de référence mentionné au même I. / Les indemnités votées par le conseil de territoire pour l'exercice effectif des fonctions de conseiller d'un établissement public territorial sont inférieures ou égales à 6 % du terme de référence mentionné audit I. / L'article L. 5211-12, à l'exception de son premier alinéa, est applicable aux indemnités des élus des établissements publics territoriaux. / Les indemnités de fonctions pour l'exercice des fonctions de président, de vice-président et de conseiller des établissements publics territoriaux ne peuvent être cumulées avec les indemnités de fonctions perçues au titre des fonctions de président, de vice-président et de conseiller de la métropole du Grand Paris ".<br/>
<br/>
              5. Le quatrième alinéa de l'article L. 5219-2-1 du code général des collectivités territoriales cité au point 4, qui prévoit que les indemnités des élus des établissements publics territoriaux sont régies par les dispositions de l'article L. 5211-12 du même code à l'exception de celles de son premier alinéa, doit être interprété comme rendant applicable le dispositif de l'enveloppe indemnitaire globale, prévu au deuxième alinéa de ce dernier article et qui constitue, pour les autres établissements publics de coopération intercommunale, un plafond uniquement pour les indemnités attribuées aux présidents, vice-présidents et, lorsqu'elles sont facultatives, à certains conseillers, aux indemnités votées par le conseil de territoire pour l'exercice effectif des fonctions de président et de vice-président d'un établissement public territorial, mais non aux indemnités prévues de droit par le code général des collectivités territoriales pour l'exercice effectif des fonctions de conseiller d'un établissement public territorial.<br/>
<br/>
              6. Dès lors, en jugeant qu'il résulte de la combinaison des dispositions des articles L. 5211-12, L. 5219-2 et L. 5219-2-1 du code général des collectivités territoriales que l'enveloppe indemnitaire globale, définie par le deuxième alinéa de l'article L. 5211-12 et correspondant à la somme de l'indemnité maximale pour l'exercice effectif des fonctions de président et des indemnités maximales pour l'exercice effectif des fonctions de vice-président, doit être répartie entre l'ensemble des élus, président, vice-présidents et conseillers de l'établissement public territorial, la cour administrative d'appel a commis une erreur de droit. Par suite, l'établissement public territorial Plaine Commune est fondé, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, à demander l'annulation de l'arrêt qu'il attaque.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L.821-2 du code de justice administrative.<br/>
<br/>
              8. Il résulte de ce qui a été dit ci-dessus que l'établissement public territorial Plaine Commune est fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Montreuil a jugé qu'il résulte de la combinaison des dispositions des articles L. 5211-12, L. 5219-2 et L. 5219-2-1 du code général des collectivités territoriales que l'enveloppe globale des indemnités de fonctions du président, des vice-présidents et des conseillers de l'établissement public territorial est égale à la somme de l'indemnité maximale pour l'exercice effectif des fonctions de président et des indemnités maximales pour l'exercice effectif des fonctions de vice-président et a annulé, à la demande du préfet de la Seine-Saint-Denis, la délibération de l'établissement public territorial Plaine Commune n° CC-16/1374 du 19 janvier 2016.<br/>
<br/>
              9. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 2 500 euros à verser à l'établissement public territorial Plaine Commune au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt du 18 octobre 2018 de la cour administrative d'appel de Versailles et le jugement du 2 février 2017 du tribunal administratif de Montreuil sont annulés.<br/>
<br/>
Article 2 : La demande présentée par le préfet de la Seine-Saint-Denis devant le tribunal administratif de Montreuil est rejetée.<br/>
<br/>
Article 3 : L'Etat est condamné à verser à l'établissement public Plaine Commune la somme de 2 500 euros en application de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à l'établissement public territorial Plaine Commune et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-05-01-01 COLLECTIVITÉS TERRITORIALES. COOPÉRATION. ÉTABLISSEMENTS PUBLICS DE COOPÉRATION INTERCOMMUNALE - QUESTIONS GÉNÉRALES. DISPOSITIONS GÉNÉRALES ET QUESTIONS COMMUNES. - ENVELOPPE INDEMNITAIRE GLOBALE POUR L'EXERCICE DE CERTAINES FONCTIONS EXÉCUTIVES AU SEIN DU CONSEIL DE TERRITOIRE D'UN ÉTABLISSEMENT PUBLIC TERRITORIAL (ART. L. 5219-2-1 DU CGCT) - APPLICABILITÉ AUX INDEMNITÉS ALLOUÉES POUR L'EXERCICE DES FONCTIONS DE CONSEILLER - ABSENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-08-03 FONCTIONNAIRES ET AGENTS PUBLICS. RÉMUNÉRATION. INDEMNITÉS ET AVANTAGES DIVERS. - ENVELOPPE INDEMNITAIRE GLOBALE POUR L'EXERCICE DE CERTAINES FONCTIONS EXÉCUTIVES AU SEIN DU CONSEIL DE TERRITOIRE D'UN ÉTABLISSEMENT PUBLIC TERRITORIAL (ART. L. 5219-2-1 DU CGCT) - APPLICABILITÉ AUX INDEMNITÉS ALLOUÉES POUR L'EXERCICE DES FONCTIONS DE CONSEILLER - ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 135-05-01-01 Le quatrième alinéa de l'article L. 5219-2-1 du code général des collectivités territoriales (CGCT), qui prévoit que les indemnités des élus des établissements publics territoriaux sont régies par l'article L. 5211-12 du même code à l'exception de celles de son premier alinéa, doit être interprété comme ne rendant applicable le dispositif de l'enveloppe indemnitaire globale, prévu au deuxième alinéa de ce dernier article et qui constitue, pour les autres établissements publics de coopération intercommunale, un plafond uniquement pour les indemnités attribuées aux présidents, vice-présidents et, lorsqu'elles sont facultatives, à certains conseillers, qu'aux indemnités votées par le conseil de territoire pour l'exercice effectif des fonctions de président et de vice-président d'un établissement public territorial, mais non aux indemnités prévues de droit par le CGCT pour l'exercice effectif des fonctions de conseiller d'un établissement public territorial.</ANA>
<ANA ID="9B"> 36-08-03 Le quatrième alinéa de l'article L. 5219-2-1 du code général des collectivités territoriales (CGCT), qui prévoit que les indemnités des élus des établissements publics territoriaux sont régies par l'article L. 5211-12 du même code à l'exception de celles de son premier alinéa, doit être interprété comme ne rendant applicable le dispositif de l'enveloppe indemnitaire globale, prévu au deuxième alinéa de ce dernier article et qui constitue, pour les autres établissements publics de coopération intercommunale, un plafond uniquement pour les indemnités attribuées aux présidents, vice-présidents et, lorsqu'elles sont facultatives, à certains conseillers, qu'aux indemnités votées par le conseil de territoire pour l'exercice effectif des fonctions de président et de vice-président d'un établissement public territorial, mais non aux indemnités prévues de droit par le CGCT pour l'exercice effectif des fonctions de conseiller d'un établissement public territorial.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, décision du même jour, Etablissement public territorial Plaine Commune, n° 426393, inédite au Recueil ; CE, décision du même jour, Etablissement public territorial Grand Orly Seine Bièvre, 431880, inédite au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
