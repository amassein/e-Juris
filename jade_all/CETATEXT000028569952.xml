<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028569952</ID>
<ANCIEN_ID>JG_L_2014_02_000000371121</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/56/99/CETATEXT000028569952.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème / 2ème SSR, 05/02/2014, 371121, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-02-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>371121</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème / 2ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>Mme Natacha Chicot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:371121.20140205</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et les mémoires complémentaires, enregistrés les 12, 26 et 29 août 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société Equalia, dont le siège est 2 rue du 19 mars 1962 à Clichy (92110), et la société Polyxo, dont le siège est 36 avenue de Verdun à Saint-Dizier (52100), représentées par leur gérant en exercice ; les sociétés Equalia et Polyxo demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1301173 du 26 juillet 2013 par laquelle le juge des référés du tribunal administratif de Châlons-en-Champagne, statuant sur le fondement de l'article L. 521-3 du code de justice administrative, à la demande de la communauté de communes de Saint-Dizier, Der et Blaise, leur a enjoint de restituer à cette dernière les matériels jusqu'alors installés dans la salle de " fitness " et les " aquabikes " dans un délai de huit jours à compter de la notification de l'ordonnance et ce, sous astreinte de 100 euros par jour de retard ; <br/>
<br/>
              2°) statuant en référé, de rejeter la demande présentée par la communauté de communes de Saint-Dizier, Der et Blaise devant le juge des référés du tribunal administratif de Châlons-en-Champagne ;<br/>
<br/>
              3°) de mettre à la charge de la communauté de communes de Saint-Dizier, Der et Blaise le versement de la somme de 4 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Natacha Chicot, Auditeur,  <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat des sociétés Equalia et Polyxo, et à la SCP Odent, Poulet, avocat de la communauté de communes de Saint-Dizier, Der et Blaise ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 521-3 du code de justice administrative : " En cas d'urgence et sur simple requête qui sera recevable même en l'absence de décision administrative préalable, le juge des référés peut ordonner toutes autres mesures utiles sans faire obstacle à l'exécution d'aucune décision administrative " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Châlons-en-Champagne que, par une convention du 23 juin 2008, la communauté de communes de Saint-Dizier, Der et Blaise a confié à la société financière Sport et Loisirs, devenue la société Equalia, l'exploitation du service public d'un centre nautique, pour une durée de cinq ans à compter du 1er juillet 2008 ; qu'au cours de cette période, le délégataire, après avoir obtenu l'accord de la communauté de communes, a diversifié l'offre proposée aux usagers en créant une activité de remise en forme (" fitness ") et d'aquacycle (" aquabiking ") ; qu'à l'échéance de la délégation, le délégataire a retiré du centre nautique les équipements de la salle de remise en forme ainsi que ceux nécessaires à l'activité d'aquacycle ; que, par une ordonnance du 26 juillet 2013, le juge des référés du tribunal administratif de Châlons-en-Champagne a, à la demande de la communauté de communes de Saint-Dizier, Der et Blaise, ordonné la restitution de ces équipements ; que les sociétés Equalia et Polyxo se pourvoient en cassation contre cette ordonnance ;<br/>
<br/>
              Sur la régularité de l'ordonnance attaquée :<br/>
<br/>
              3. Considérant, en premier lieu, que si la société Equalia et la société Polyxo, laquelle se présentant comme ayant été chargée de l'exploitation de la délégation, a le même gérant et est représentée par le même mandataire que la société Equalia, soutiennent que la demande de la communauté de communes de Saint-Dizier, Der et Blaise n'aurait pas été notifiée à la société Polyxo, il ressort, en tout état de cause, des pièces du dossier soumis au juge des référés que le mémoire en défense transmis au greffe du tribunal administratif était présenté pour le compte des deux sociétés en réponse à cette notification ; qu'en deuxième lieu, aucun principe ni aucune disposition n'imposait au juge des référés d'attendre le retour de l'accusé de réception du courrier de communication de la requête adressé à la société Polyxo pour statuer sur la demande de la communauté de communes ; qu'en troisième lieu, si la communication aux parties de la requête doit être établie par les pièces du dossier, aucun texte ni aucun principe n'impose de mentionner les dates de communication et de réception par les parties de cette requête dans les visas de la décision ; que si les sociétés Equalia et Polyxo soutiennent, en dernier lieu, que le greffe du tribunal administratif leur aurait indiqué, par téléphone, qu'elles pouvaient produire un mémoire en défense jusqu'au 26 juillet 2013 inclus alors que le délai qui leur était imparti expirait le 24 juillet 2013, elles n'apportent, à l'appui de cette allégation, aucune précision permettant d'en apprécier la réalité ; qu'il s'ensuit que les requérantes ne sont pas fondées à soutenir que l'ordonnance attaquée aurait été rendue au terme d'une procédure irrégulière ;<br/>
<br/>
              Sur le bien fondé de l'ordonnance attaquée :<br/>
<br/>
              En ce qui concerne la recevabilité de la demande :<br/>
<br/>
              4. Considérant qu'il n'appartenait pas au juge des référés de soulever d'office le moyen tiré de ce que la demande de la communauté de communes de Saint-Dizier, Der et Blaise était irrecevable au motif que la procédure de conciliation prévue à l'article 41 de la convention de délégation de service public n'aurait pas été respectée ; que les requérantes ne sont, par suite, pas fondées à soutenir que le juge des référés aurait dénaturé les pièces du dossier en ne rejetant pas la demande de la communauté de communes comme irrecevable ;<br/>
<br/>
              En ce qui concerne l'injonction prononcée par le juge des référés :<br/>
<br/>
              5. Considérant, d'une part, que, s'il n'appartient pas au juge administratif d'intervenir dans la gestion d'un service public en adressant des injonctions à ceux qui ont contracté avec l'administration, lorsque celle-ci dispose à l'égard de ces derniers des pouvoirs nécessaires pour assurer l'exécution du contrat, il en va autrement quand l'administration ne peut user de moyens de contrainte à l'encontre de son cocontractant qu'en vertu d'une décision juridictionnelle ; qu'en pareille hypothèse, le juge du contrat est en droit de prononcer, à l'encontre du cocontractant, une condamnation, éventuellement sous astreinte, à une obligation de faire ; qu'en cas d'urgence, le juge des référés peut, de même, sur le fondement des dispositions de l'article L. 521-3 du code de justice administrative, ordonner au cocontractant, éventuellement sous astreinte, de prendre à titre provisoire toute mesure nécessaire pour assurer la continuité du service public ou son bon fonctionnement, à condition que cette mesure soit utile, justifiée par l'urgence, ne fasse obstacle à l'exécution d'aucune décision administrative et ne se heurte à aucune contestation sérieuse ;<br/>
<br/>
              6. Considérant, d'autre part, que, dans le cadre d'une délégation de service public ou d'une concession de travaux mettant à la charge du cocontractant les investissements correspondant à la création ou à l'acquisition des biens nécessaires au fonctionnement du service public, l'ensemble de ces biens, meubles ou immeubles, appartient, dans le silence de la convention, dès leur réalisation ou leur acquisition à la personne publique ; qu'à l'expiration de la convention, les biens qui sont entrés dans la propriété de la personne publique et ont été amortis au cours de l'exécution du contrat font nécessairement retour à celle-ci gratuitement, sous réserve des clauses contractuelles permettant à la personne publique, dans les conditions qu'elles déterminent, de faire reprendre par son cocontractant les biens qui ne seraient plus nécessaires au fonctionnement du service public ;<br/>
<br/>
              7. Considérant, en premier lieu, que contrairement à ce que soutiennent les requérantes, il résulte de ce qui précède que le juge des référés n'a pas commis d'erreur de droit ni dénaturé les pièces du dossier en jugeant que, la communauté de communes ne disposant pas de pouvoirs de contrainte à l'égard de la société Equalia, la restitution par le délégataire de biens de retour est au nombre des mesures utiles et urgentes qui peuvent être prises sur le fondement de l'article L. 521-3 du code de justice administrative afin d'assurer la continuité du service public et son bon fonctionnement ;<br/>
<br/>
              8. Considérant, en second lieu, qu'il ressort des pièces du dossier soumis au juge des référés que la création d'activités de remise en forme et d'aquacycle visait, conformément aux termes de l'article 3 de la convention de délégation de service public du 23 juin 2008, à améliorer le service offert aux usagers ; qu'elle a été approuvée par la communauté de communes, qui a notamment pris en charge les travaux d'aménagement de la salle de remise en forme et décidé, par deux délibérations des 17 janvier et 30 juin 2011, d'augmenter en conséquence les tarifs proposés aux usagers du centre nautique ; que, contrairement à ce que soutiennent les requérantes, le juge des référés pouvait, dès lors, dans les circonstances de l'espèce, juger, sans commettre d'erreur de droit ni entacher son ordonnance d'une erreur de qualification juridique, que les activités de remise en forme et d'aquacycle relevaient du périmètre de la délégation de service public consentie et que les équipements utilisés pour l'accomplissement de ces activités pouvaient être regardés comme des biens de retour quand bien même ils ne figuraient pas à l'annexe 1 de la convention de délégation qui établissait, à la date de sa signature, la liste des ouvrages et équipements devant être remis gratuitement à la collectivité au terme du contrat ; <br/>
<br/>
              9. Considérant qu'il résulte de tout ce qui précède que le pourvoi des sociétés Equalia et Polyxo et, par voie de conséquence, leurs conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent qu'être rejetés ; qu'il y lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de chacune de ces sociétés le versement à la communauté de communes de Saint-Dizier, Der et Blaise de la somme de 1 500 euros en application des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi des sociétés Equalia et Polyxo est rejeté.<br/>
Article 2 : Les sociétés Equalia et Polyxo verseront chacune à la communauté de communes de Saint-Dizier, Der et Blaise la somme de 1 500 euros en application de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée aux sociétés Equalia et Polyxo et à la communauté de communes de Saint-Dizier, Der et Blaise.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
