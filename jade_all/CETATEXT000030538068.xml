<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030538068</ID>
<ANCIEN_ID>JG_L_2015_04_000000374076</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/53/80/CETATEXT000030538068.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  3ème sous-section jugeant seule, 29/04/2015, 374076, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-04-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>374076</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 3ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>M. Christian Fournier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:374076.20150429</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Paris la décharge des cotisations supplémentaires à l'impôt sur le revenu auxquelles il a été assujetti au titre des années 2004, 2005 et 2006 ainsi que des pénalités dont elles sont assorties. Par un jugement n° 0916248-0916250-0916251 du 9 mai 2011, le tribunal administratif de Paris a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 11PA03133 du 24 octobre 2013, la cour administrative d'appel de Paris a rejeté l'appel formé par M. B...contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 18 décembre 2013 et 18 mars 2014 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 11PA03133 du 24 octobre 2013 de la cour administrative d'appel de Paris ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              	Vu les autres pièces du dossier ; <br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christian Fournier, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattacinii, avocat de M. A...B... ; <br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que l'administration fiscale a réintégré dans les revenus imposables de M. B... à l'impôt sur le revenu des années 2004, 2005 et 2006 des sommes qu'il avait déduites de ses revenus au titre de pensions alimentaires, versées à la mère de ses trois enfants, dont il vivait séparé et qui en avait la garde, en vue de pourvoir à leur entretien ; que M. B... se pourvoit en cassation contre l'arrêt du 24 octobre 2013  par lequel la cour administrative d'appel de Paris a rejeté l'appel qu'il avait interjeté du jugement du 9 mai 2011 du tribunal administratif de Paris rejetant sa demande tendant à la décharge des cotisations supplémentaires d'impôt sur le revenu et de contribution sociale, résultant de ces réintégrations, auxquelles il a été assujetti au titre de ces années ; <br/>
<br/>
              2. Considérant qu'en vertu des dispositions de l'article 156 du code général des impôts, dans sa rédaction applicable aux impositions en litige, une pension alimentaire versée par un contribuable en vue de pourvoir aux besoins de toute nature de ses enfants mineurs est déductible du revenu imposable à l'impôt sur le revenu ; <br/>
<br/>
              3. Considérant qu'en jugeant que M. B... ne pouvait en tout état de cause se prévaloir sur le fondement de l'article L. 80 A du livre des procédures fiscales de l'interprétation de l'administration fiscale référencée 5 B-2421 en date du 1er septembre 1999 au motif qu'il ne vivait pas en union libre avec la mère des enfants alors que le paragraphe 74 de cette instruction concerne le cas de la séparation des parents non mariés dont celui qui n'assure pas la garde de l'enfant contribue à son entretien en versant une pension alimentaire déductible du revenu imposable, la cour administrative d'appel a dénaturé les pièces du dossier qui lui étaient soumis ;<br/>
<br/>
              4. Considérant, au surplus, qu'en jugeant que le contribuable ne pouvait utilement se prévaloir de la table de référence des contributions aux frais d'éducation et d'entretien des enfants de parents séparés publiée dans la circulaire du 12 avril 2010 du garde des sceaux, ministre de la justice, alors qu'il ressort des écritures de M. B...devant les juges du fond que celui-ci entendait s'appuyer sur cette table de référence pour démontrer que les pensions alimentaires en cause pouvaient être regardées comme proportionnées à ses ressources et à celle de la mère ainsi qu'aux besoins des enfants, la cour administrative d'appel s'est méprise sur le sens et la portée de l'argumentation de l'appelant ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que M. B...est fondé à demander l'annulation de l'arrêt attaqué ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à M. B... au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 24 octobre 2013 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br/>
Article 3 : L'Etat versera à M. B... une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. A...B...et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
