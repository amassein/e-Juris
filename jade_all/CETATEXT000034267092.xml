<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034267092</ID>
<ANCIEN_ID>JG_L_2017_03_000000400449</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/26/70/CETATEXT000034267092.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 22/03/2017, 400449, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-03-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>400449</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Charles Touboul</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:400449.20170322</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Nice, d'une part, d'annuler la décision du ministre de l'intérieur constatant la perte de validité de son permis de conduire pour solde de points nul, ainsi que les décisions de retrait de points ayant concouru à cette perte de validité et la décision rejetant son recours gracieux et,  d'autre part, d'enjoindre au ministre de rétablir les points retirés. Par un jugement n° 1502307 du 19 avril 2016, le tribunal administratif a annulé la décision constatant la perte de validité du permis et les décisions de retrait de points.<br/>
<br/>
              Par un pourvoi, enregistré le 7 juin 2016 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de M.A....<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
<br/>
              Vu : <br/>
<br/>
              - le code de la route ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Charles Touboul, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il incombe à l'administration, lorsqu'elle oppose une fin de non-recevoir tirée de la tardiveté d'une action introduite devant une juridiction administrative, d'établir la date à laquelle la décision attaquée a été régulièrement notifiée à l'intéressé ; qu'en cas de retour à l'administration, au terme du délai de mise en instance, du pli recommandé contenant la décision, la notification est réputée avoir été régulièrement accomplie à la date à laquelle ce pli a été présenté à l'adresse de l'intéressé, dès lors du moins qu'il résulte soit de mentions précises, claires et concordantes portées sur l'enveloppe, soit, à défaut, d'une attestation du service postal ou d'autres éléments de preuve, que le préposé a, conformément à la réglementation en vigueur, déposé un avis d'instance informant le destinataire que le pli était à sa disposition au bureau de poste ; que, compte tenu des modalités de présentation des plis recommandés prévues par la réglementation postale, doit être regardé comme portant des mentions précises, claires et concordantes suffisant à constituer la preuve d'une notification régulière le pli recommandé retourné à l'administration auquel est rattaché un volet " avis de réception " sur lequel a été apposée par voie de duplication la date de vaine présentation du courrier et qui porte, sur l'enveloppe ou l'avis de réception, l'indication du motif pour lequel il n'a pu être remis ; <br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que l'avis de réception attaché au pli recommandé contenant la décision constatant la perte de validité du permis de conduire de M.A..., adressé à celui-ci et retourné à l'administration, comporte la mention " présenté/avisé le 29 avril 2011 " et que la case " pli non réclamé ", correspondant au motif de non-distribution, y est cochée ; qu'en estimant que la notification ne pouvait être regardée comme régulière, faute d'une mention expresse du dépôt d'un avis d'instance informant le destinataire que le pli était à sa disposition au bureau de poste, pour en déduire que la demande d'annulation de cette décision et des décisions de retrait de points qu'elle récapitulait, enregistrée le 9 juin 2015 au greffe du tribunal administratif,  n'était pas tardive, l'auteur du jugement attaqué a commis une erreur de droit ; que, par suite, le ministre de l'intérieur est fondé à demander l'annulation de ce jugement ;<br/>
<br/>
              3. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que la décision du ministre de l'intérieur constatant la perte de validité du permis de conduire de M. A... doit être regardée comme ayant été valablement notifiée à l'intéressé le 29 avril 2011 ; qu'il ressort des pièces du dossier que la demande de M.A..., tendant à l'annulation de cette décision ainsi que des décisions de retrait de points qu'elle récapitulait et à ce qu'il soit enjoint au ministre de l'intérieur de restituer le permis litigieux, a été enregistrée au greffe du tribunal administratif de Nice le 9 juin 2015 ; qu'à cette date, le délai de recours contentieux était expiré ; que les conclusions à fin d'annulation sont dès lors irrecevables et doivent être rejetées pour ce motif ; que les conclusions à fin d'injonction doivent être rejetées par voie de conséquence ;<br/>
<br/>
              5. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que la somme demandée sur leur fondement par M.A..., au titre des frais qu'il a exposés devant le tribunal administratif, soit mise à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante ; que le ministre de l'intérieur, qui n'a pas recouru au ministère d'un avocat et ne fait pas état de frais spécifiques exposés par l'Etat pour présenter sa défense devant le tribunal administratif, n'est pas fondé à demander qu'une somme soit mise au même titre à la charge de M.A... ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 19 avril 2016 du tribunal administratif de Nice est annulé.<br/>
<br/>
Article 2 : La demande présentée par M. A...devant le tribunal administratif de Nice est rejetée.<br/>
<br/>
Article 3 : Les conclusions présentées par le ministre de l'intérieur devant le tribunal administratif de Nice sur le fondement de dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 4 : La présente décision sera notifiée au ministre de l'intérieur et à M. B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
