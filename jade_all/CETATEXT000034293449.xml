<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034293449</ID>
<ANCIEN_ID>JG_L_2017_03_000000390594</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/29/34/CETATEXT000034293449.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 5ème chambres réunies, 27/03/2017, 390594, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-03-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>390594</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>Mme Tiphaine Pinault</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:390594.20170327</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique enregistrés les 1er juin et 19 novembre 2015 au secrétariat du contentieux du Conseil d'Etat, le Syndicat interprofessionnel de la montagne demande au Conseil d'Etat d'annuler pour excès de pouvoir l'arrêté du 11 mars 2015 du ministre de la ville, de la jeunesse et des sports relatif au contenu et aux modalités d'organisation du recyclage des titulaires des diplômes de guide de haute montagne. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du sport ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Tiphaine Pinault, maître des requêtes en service extraordinaire, <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat du Syndicat interprofessionnel de la montagne ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 212-1 du code du sport : " I. - Seuls peuvent, contre rémunération, enseigner, animer ou encadrer une activité physique ou sportive ou entraîner ses pratiquants (...) les titulaires d'un diplôme, titre à finalité professionnelle ou certificat de qualification : / 1° Garantissant la compétence de son titulaire en matière de sécurité des pratiquants et des tiers dans l'activité considérée " ; que l'article L. 212-2 du même code prévoit que, lorsque ces activités d'enseignement, d'animation ou d'encadrement s'exercent dans un environnement impliquant le respect de mesures de sécurité particulières, le diplôme permettant son exercice " est délivré par l'autorité administrative dans le cadre d'une formation coordonnée par les services du ministre chargé des sports et assurée par des établissements relevant de son contrôle pour les activités considérées " ; qu'à ce titre, l'article R. 212-7 du même code mentionne, au nombre des activités impliquant le respect de mesures de sécurité particulières, " le ski, l'alpinisme et leurs activités assimilées " ; qu'enfin, l'article R. 212-9 dispose que " L'arrêté du ministre créant (...) la spécialité du diplôme relative à l'une des activités prévues à l'article R. 212-7 (...) comporte : 1° Le programme de formation et les modalités d'évaluation (...)/ Cet arrêté précise les éléments du programme ou des activités qui ne peuvent être délégués à d'autres établissements ou organismes de formation [que ceux placés sous sa tutelle] " ; <br/>
<br/>
              2. Considérant que, pour l'application de ces dernières dispositions, l'arrêté attaqué du 11 mars 2015 du ministre de la ville, de la jeunesse et des sports a fixé les modalités d'organisation d'une formation continue dite " de recyclage " à laquelle doivent se soumettre les titulaires du diplôme de guide de haute montagne, du brevet d'Etat d'alpinisme et du diplôme d'Etat d'alpinisme-guide de haute montagne ; que cet arrêté dispose notamment que, si cette formation incombe à l'Ecole nationale des sports de montagne, placée sous la tutelle du ministre chargé des sports, elle peut également être organisée par un ou plusieurs organismes de formation ayant passé convention avec cette école, suivant un cahier des charges établi par celle-ci ; que le même arrêté fixe, dans son annexe, le " cadre général " imposé à ce cahier des charges ;<br/>
<br/>
              3. Considérant que la requête du syndicat interprofessionnel de la montagne doit être regardée comme demandant l'annulation de l'arrêté du 11 mars 2015 en tant seulement qu'il fixe ce " cadre général ", applicable, ainsi qu'il vient d'être dit, au futur cahier des charges auquel les organismes de formation devront satisfaire pour être retenus par l'Ecole nationale des sports de montagne, si cette dernière décide de déléguer tout ou partie de l'organisation de la formation " de recyclage " dont elle a la charge ;<br/>
<br/>
              Sur les moyens tirés de la méconnaissance de la liberté du commerce et de l'industrie et des règles de la concurrence :<br/>
<br/>
              4. Considérant que lorsque l'exercice de pouvoirs de police administrative est susceptible d'affecter des activités de production, de distribution ou de services, la circonstance que les mesures envisagées aient pour objectif la protection de l'ordre ou de la sécurité publique n'exonère pas l'autorité compétente de l'obligation de prendre en compte également la liberté du commerce et de l'industrie et les règles de concurrence ; qu'il appartient au juge de l'excès de pouvoir d'apprécier la légalité de ces mesures en recherchant si elles ont été prises en tenant compte de l'ensemble de ces objectifs et de ces règles et si elles en ont fait, en les combinant, une exacte application ; <br/>
<br/>
              5. Considérant, en premier lieu, que si le " cadre général " du cahier des charges annexé à l'arrêté du 11 mars 2015 prévoit que les organismes de formation devront : " inscrire leur objet, à titre principal, dans le champ des activités professionnelles des guides de haute montagne : expérience dans le domaine des métiers de la montagne ", ces dispositions, qui ne sauraient avoir légalement pour effet de limiter la contractualisation aux seuls organismes de formation qui consacrent l'essentiel de leur activité aux métiers de la montagne, doivent être regardées comme ayant seulement pour objet d'exiger des organismes susceptibles d'être conventionnés que, pour la part de leur activité de formation qui s'adresse aux guides de haute montagne, ils dispensent, à titre principal, des formations visant à acquérir de l'expérience dans les métiers de la montagne ; que le syndicat requérant n'est, par suite, pas fondé à soutenir que cette mesure ne serait pas nécessaire et proportionnée à l'objectif de sécurité dans la pratique des sports de montagne ; <br/>
<br/>
              6. Considérant, en deuxième lieu, que le même " cadre général " prévoit que les organismes de formation devront : " présenter les garanties structurelles et financières permettant d'assurer en toutes circonstances le bon déroulement des sessions : capacité de gestion administrative de plusieurs centaines de dossiers par an, capacité financière permettant de faire face à l'irrégularité des flux de formation (...) " ; qu'il ressort des pièces du dossier qu'eu égard au nombre important de formations de " recyclage " à assurer chaque année et compte tenu de la nécessité de pouvoir le cas échéant confier ces formations à un seul des organismes susceptibles d'être retenus, soit à des fins de commodité d'organisation, soit en raison de la défaillance d'un ou de plusieurs organismes initialement choisis, ces conditions doivent, contrairement à ce que soutient le syndicat requérant, être regardées comme nécessaires et proportionnées à l'objectif visant à assurer, dans des conditions suffisantes de continuité et de sécurité, les formations en cause ;<br/>
<br/>
              7. Considérant, enfin, qu'il ressort également des pièces du dossier qu'en exigeant des organismes de formation une " capacité à autofinancer l'ingénierie de formation et l'ingénierie pédagogique ", le " cadre général " litigieux a, eu égard aux exigences de bon fonctionnement du dispositif de " recyclage " et alors même que, ainsi que le soutient le syndicat requérant, les formations feraient en principe l'objet d'une avance de paiement par leurs bénéficiaires, fixé une obligation qui doit, pour les raisons déjà mentionnées de continuité et de sécurité du processus de " recyclage ", être regardée comme nécessaire et proportionnée aux objectifs poursuivis ;<br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que les moyens tirés de ce que le " cadre général " porterait, en raison des exigences qu'il fait peser sur les organismes de formation susceptibles d'être sélectionnés, une atteinte excessive à la liberté du commerce et de l'industrie et au principe d'égalité devant la commande publique, ou qu'il méconnaîtrait les règles de la concurrence en favorisant la constitution d'un monopole au profit d'un opérateur particulier, ne peuvent qu'être écartés ;<br/>
<br/>
              Sur les autres moyens de la requête :<br/>
<br/>
              9. Considérant, en premier lieu, que les moyens tirés de ce que le " cadre général " du cahier des charges annexé à l'arrêté méconnaîtrait le principe de neutralité du service public, ainsi qu'un principe de " transparence " des services publics, ne sont, en tout état de cause, pas assortis des précisions permettant d'en apprécier le bien-fondé ; <br/>
<br/>
              10. Considérant, en deuxième lieu, que le " cadre général " litigieux n'est relatif ni à l'enseignement ni à la scolarité ; qu'ainsi, les moyens tirés de ce qu'il porterait atteinte aux principes de gratuité et de neutralité de l'enseignement ne sauraient être utilement invoqués ;<br/>
<br/>
              11. Considérant, enfin, que le détournement de pouvoir allégué n'est pas établi ;<br/>
<br/>
              12. Considérant qu'il résulte de tout ce qui précède que le syndicat interprofessionnel de la montagne n'est pas fondé à demander l'annulation du " cadre général " du cahier des charges annexé à l'arrêté du 11 mars 2015 ; que sa requête doit, par suite, être rejetée ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La requête du Syndicat interprofessionnel de la montagne est rejetée.<br/>
Article 2 : La présente décision sera notifiée au Syndicat interprofessionnel de la montagne et au ministre de la ville, de la jeunesse et des sports.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
