<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026258470</ID>
<ANCIEN_ID>JG_L_2012_07_000000360788</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/25/84/CETATEXT000026258470.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 13/07/2012, 360788, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-07-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>360788</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLEZ</AVOCATS>
<RAPPORTEUR>M. Mattias Guyomar</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2012:360788.20120713</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête enregistrée le 6 juillet 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par M. Binda B domicilié chez ... ; M. B demande au juge des référés du Conseil d'Etat :<br/>
<br/>
              - d'annuler l'ordonnance n° 1203625 du 6 juin 2012 par laquelle le juge des référés du tribunal administratif de Lille, statuant sur le fondement des dispositions de l'article L. 521-2 du code de justice administrative, a rejeté sa demande dirigée contre l'arrêté du 2 juin 2012 par lequel le préfet de la région Nord-Pas-de-Calais, préfet du Nord a décidé son éloignement à destination du Congo et ordonné son placement en rétention ;<br/>
              - de mettre à la charge de l'Etat le versement de la somme de 3 000 euros à la SCP Boullez au titre de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991 ;<br/>
<br/>
<br/>
              il soutient que c'est à tort que le juge des référés du tribunal administratif de Lille a décidé que la condition d'urgence n'était plus satisfaite du seul fait que la mesure d'éloignement de M. B avait été exécutée à la date à laquelle il statuait alors qu'une telle exigence était remplie au jour du dépôt de la requête ; <br/>
<br/>
<br/>
              Vu l'ordonnance attaquée ;<br/>
<br/>
              Vu le mémoire en défense, enregistré le 12 juillet 2012, présenté par le ministre de l'intérieur qui conclut au rejet de la requête ; <br/>
<br/>
              il soutient que :<br/>
              - la condition d'urgence n'est pas remplie ; <br/>
              - la mesure d'interdiction du territoire français étant devenue définitive, le préfet était tenu de l'exécuter ; en l'absence des garanties suffisantes, le préfet était tenu d'ordonner le placement en rétention administrative du requérant pour l'organisation matérielle de son départ dès lors que M. B devait être reconduit à la frontière en exécution d'une interdiction judiciaire ;<br/>
              - l'interdiction judiciaire du territoire ayant été exécutée, la requête est dépourvue d'objet ; <br/>
              - aucune atteinte grave et manifestement illégale n'a été portée à une liberté fondamentale ;<br/>
<br/>
              Vu le mémoire complémentaire, enregistré le 13 juillet 2012, présenté pour M. B qui reprend les conclusions et moyen de sa requête ; <br/>
<br/>
              il soutient en outre que : <br/>
              - l'exécution de la mesure d'éloignement ne rend pas sans objet la demande ;<br/>
              - en ne laissant pas à M. B  de disposer d'un délai suffisant pour formuler des observations écrites et pouvoir se faire assister par un mandataire de son choix, le préfet a porté une atteinte grave et manifestement illégale aux droits de la défense ; <br/>
              - l'exécution de la mesure d'éloignement porte une atteinte grave et manifestement illégale au droit à la vie privée et familiale ;<br/>
<br/>
              Vu la décision du bureau d'aide juridictionnelle en date du 21 juin 2012 accordant l'aide juridictionnelle à M. B ;<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              Vu le code pénal ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. B et, d'autre part, le ministre de l'intérieur ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 13 juillet 2012 à 14 heures 30 au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Boullez, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. B ;<br/>
<br/>
              - le représentant du ministre de l'intérieur ;<br/>
<br/>
              et à l'issue de laquelle l'instruction a été close ; <br/>
<br/>
<br/>
<br/>
              1. Considérant que M. B, de nationalité congolaise, né en 1960, a été condamné par un arrêt du 28 février 2009, devenu définitif, de la Cour d'appel de Douai, à 15 mois d'emprisonnement et à une interdiction définitive du territoire ; que, pour l'exécution de cette interdiction judiciaire du territoire, le préfet de la région Nord-Pas-de-Calais, préfet du Nord a, le 2 juin 2012, pris un arrêté décidant l'éloignement de M. B à destination du Congo et ordonnant son placement en rétention ; que M. B relève appel de l'ordonnance du 6 juin 2012 par laquelle le juge des référés du tribunal administratif de Lille a rejeté sa demande présentée sur le fondement de l'article L. 521-2 du code de justice administrative ;<br/>
<br/>
              Sur les conclusions dirigées contre l'arrêté du 2 juin en tant qu'il ordonne le placement en rétention de M. B :<br/>
<br/>
              2. Considérant que la circonstance que M. B a effectivement été reconduit à la frontière le 5 juin 2002 a pour effet qu'à la date où le premier juge s'est prononcé sur les conclusions tendant à ce que soit ordonnée la suspension de l'arrêté préfectoral ordonnant le placement en rétention de l'intéressé, cette mesure avait produit l'intégralité de ses effets ; qu'il suit de là, que le ministre de l'intérieur est fondé à soutenir que les conclusions de la requête tendant à la suspension d'une décision administrative entièrement exécutée avant même son introduction, se trouvaient dépourvues d'objet ; qu'en ne constatant pas qu'il n'y avait pas lieu de statuer, dans cette mesure, sur les conclusions dont il était saisi, le juge des référés du tribunal administratif de Lille a commis une erreur de droit ; que son ordonnance doit être annulée dans cette mesure ; <br/>
<br/>
              Sur les conclusions dirigées contre l'arrêté du 2 juin en tant qu'il ordonne la reconduite à la frontière de M. B :<br/>
<br/>
              3. Considérant en revanche que l'entière exécution de l'arrêté du 2 juin 2012 ne prive pas d'objet les conclusions dirigées à son encontre en tant qu'il ordonne la reconduite à la frontière de M. Lemebe Li ;<br/>
<br/>
              4. Considérant que, dans son principe, la reconduite à la frontière de M. B est la conséquence nécessaire de l'interdiction définitive du territoire français prononcée par le juge pénal, qui emporte de plein droit cette mesure, que le préfet était ainsi tenu de prononcer alors même qu'une demande de relèvement de cette interdiction était pendante devant la cour d'appel de Douai ; qu'ainsi, l'arrêté préfectoral ne porte pas lui-même atteinte au droit de M. B au respect de sa vie privée et familiale, les effets attachés à la mesure d'éloignement litigieuse découlant en réalité du prononcé par le juge pénal de la peine d'interdiction du territoire ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que M. B n'est pas fondé à se plaindre de ce que, par l'ordonnance du 6 juin 2012, le juge des référés du tribunal administratif de Lille a rejeté ses conclusions dirigées contre l'arrêté du 2 juin 2012 en tant qu'il ordonne sa reconduite à la frontière ;<br/>
<br/>
<br/>
<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :  <br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de l'Etat, qui n'est pas dans la présente instance la partie perdante, le versement de la somme demandée par M. B ;<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
 Article 1er : L'ordonnance n° 1203625 du 6 juin 2012 du juge des référés du tribunal administratif de Lille est annulée en tant qu'elle statue sur les conclusions de M. B  dirigées contre l'arrêté préfectoral du 2 juin 2012 en tant qu'il ordonne son placement en rétention.<br/>
<br/>
 Article 2 : Il n'y pas lieu de statuer sur les conclusions de M. B dirigées contre l'arrêté préfectoral du 2 juin 2012 en tant qu'il ordonne son placement en rétention.<br/>
<br/>
 Article 3 : Le surplus des conclusions de M. B est rejeté.<br/>
<br/>
 Article 4 : La présente ordonnance sera notifiée à M. B et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
