<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026768122</ID>
<ANCIEN_ID>JG_L_2012_12_000000342076</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/76/81/CETATEXT000026768122.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 12/12/2012, 342076, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-12-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>342076</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR>M. Matthieu Schlesinger</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Claire Legras</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:342076.20121212</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 2 août et 2 novembre 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour le Commissariat à l'énergie atomique (CEA), dont le siège est bâtiment " Le Ponant D ", 25 rue Leblanc à Paris (75015) ; le Commissariat à l'énergie atomique demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n°s 07LY01256, 09LY00920 et 09LY01528 du 3 juin 2010 de la cour administrative d'appel de Lyon en tant qu'il a rejeté sa requête tendant à l'annulation du jugement n°s 0500308-0500435 du 29 mars 2007 du tribunal administratif de Dijon en tant qu'il a rejeté sa demande tendant à la décharge de la cotisation de taxe professionnelle à laquelle il a été assujetti au titre de l'année 1998 dans les rôles de la commune de Salives ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 6 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Matthieu Schlesinger, Auditeur,  <br/>
<br/>
              - les observations de la SCP Delaporte, Briard, Trichet, avocat du Commissariat à l'énergie Atomique,<br/>
<br/>
              - les conclusions de Mme Claire Legras, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Delaporte, Briard, Trichet, avocat du Commissariat à l'énergie Atomique ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le Commissariat à l'énergie atomique développe sur le site du centre d'études de Valduc, situé sur la commune de Salives, des activités militaires et civiles ; que, par un jugement du 27 novembre 2001, confirmé par un arrêt de la cour administrative d'appel de Lyon du 23 novembre 2006 devenu définitif, le tribunal administratif de Dijon, statuant sur la demande de la commune de Salives, a annulé la décision implicite du directeur des services fiscaux de la Côte d'Or refusant d'assujettir cet établissement public à la taxe professionnelle au titre des années 1996 à 1998 à raison de certaines de ses activités et a enjoint à l'administration de procéder, au plus tard le 31 décembre 2001, à cet assujettissement au titre de l'année 1998, seule année non prescrite à la date de son jugement ; que le Commissariat à l'énergie atomique a déposé le 20 décembre 2002 une réclamation contre la cotisation de taxe professionnelle à laquelle il a été assujetti au titre de l'année 1998 en exécution du jugement du 27 novembre 2001 ; qu'à la suite du rejet de cette réclamation, il a introduit une demande en décharge de cette cotisation devant le tribunal administratif de Dijon, puis devant la cour administrative d'appel de Lyon ; qu'il se pourvoit en cassation contre l'arrêt du 3 juin 2010 par lequel cette cour a rejeté ses conclusions en jugeant que l'autorité absolue de chose jugée qui s'attache à son arrêt du 23 novembre 2006 et aux motifs qui en sont le support nécessaire s'opposait à ce qu'il soumette à la juridiction administrative des conclusions tendant à la décharge de la cotisation de taxe professionnelle à laquelle il a été assujetti au titre de l'année 1998 ;<br/>
<br/>
              2. Considérant qu'un jugement annulant, dans le cadre d'un recours pour excès de pouvoir, une décision de refus d'assujettir un contribuable à un impôt, même s'il est assorti d'une injonction de procéder à cet assujettissement, ne fait pas obstacle à ce que ce contribuable conteste devant le juge de l'impôt le bien-fondé de l'imposition mise à sa charge en exécution du jugement ; que l'autorité absolue de chose jugée dont sont revêtus le dispositif du jugement d'annulation et les motifs qui en constituent le soutien nécessaire justifie seulement que soient écartés, dans le cadre de l'instance de plein contentieux fiscal tendant à la décharge de l'imposition, les moyens qui soulèvent des contestations identiques à celles qui ont été tranchées dans le litige d'excès de pouvoir ;<br/>
<br/>
              3. Considérant qu'il résulte de ce qui précède que, en se fondant sur l'autorité de chose jugée dont était revêtu son arrêt du 23 novembre 2006 pour rejeter comme irrecevables les conclusions aux fins de décharge de la cotisation de taxe professionnelle litigieuse dont elle était saisie par le Commissariat à l'énergie atomique, la cour a commis une erreur de droit ; que, dès lors, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'article 5 de son arrêt doit être annulé ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser au Commissariat à l'énergie atomique, au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : L'article 5 de l'arrêt de la cour administrative d'appel de Lyon du 3 juin 2010 est annulé.<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Lyon.<br/>
Article 3 : L'Etat versera une somme de 3 000 euros au Commissariat à l'énergie atomique au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée au Commissariat à l'énergie atomique et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
