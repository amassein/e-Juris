<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043121082</ID>
<ANCIEN_ID>JG_L_2021_02_000000445165</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/12/10/CETATEXT000043121082.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 10/02/2021, 445165, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-02-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>445165</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Arnaud Skzryerbak</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:445165.20210210</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme O... A... a demandé au tribunal administratif de Dijon d'annuler l'élection de M. C... L... en qualité de conseiller municipal lors des opérations électorales qui se sont déroulées le 15 mars 2020 dans la commune d'Étevaux (Côte-d'Or) et de la proclamer élue. Par un jugement n° 2001110 du 22 septembre 2020, le tribunal administratif de Dijon a fait droit à cette protestation.<br/>
<br/>
<br/>
              M. L... soutient que :<br/>
              - la procédure suivie par le tribunal administratif a été irrégulière, faute que la commune d'Etevaux ait été mise en cause ; <br/>
              - le suffrage irrégulier devait être déduit du nombre de voix obtenu par chacun des candidats et non comme l'a jugé le tribunal de celui des seuls élus.<br/>
<br/>
              M. J... I..., M. M... G..., M. W... B..., M. P... N..., M. T... D..., M. Q... E..., M. K... F..., Mme R... S..., M. V... H... et M. X... U... ont présenté des observations, enregistrées le 16 novembre 2020.<br/>
<br/>
              La requête a été communiquée à Mme A..., qui n'a pas présenté de mémoire.<br/>
<br/>
              La requête a été communiquée au préfet de la région Bourgogne Franche-Comté, préfet de la Côte-d'Or et au ministre de l'intérieur, qui n'ont pas présenté d'observations.<br/>
<br/>
<br/>
<br/>
              2° Sous le n° 446019, par une ordonnance n° 2002874 du 22 octobre 2020 enregistrée au secrétariat du contentieux du Conseil d'Etat le 6 novembre 2020, le président du tribunal administratif de Dijon a transmis au Conseil d'Etat la requête de M.  L..., enregistrée le 1er octobre 2020 au greffe de ce tribunal. Par cette requête, M. L... demande au Conseil d'Etat d'annuler le même jugement du 22 septembre 2020 et de valider son élection.<br/>
<br/>
              Il soutient que :<br/>
              - la procédure suivie par le tribunal administratif a été irrégulière, faute que la commune d'Etevaux ait été mise en cause ; <br/>
              - la protestation de Mme A... n'était pas recevable, faute d'avoir été inscrite au procès-verbal de l'élection ;<br/>
              - le suffrage irrégulier devait être déduit du nombre de voix obtenu par chacun des candidats et non comme l'a jugé le tribunal de celui des seuls élus.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arnaud Skzryerbak, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Les requêtes présentées par M. L... sont dirigées contre le même jugement. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2. À l'issue des élections municipales qui se sont déroulées le 15 mars 2020 dans la commune d'Étevaux, les onze sièges de conseiller municipal ont été pourvus. M. L... relève appel du jugement du 22 septembre 2020 par lequel le tribunal administratif de Dijon a annulé son élection en qualité de conseiller municipal et a proclamé Mme A... élue à sa place.<br/>
<br/>
              3. En premier lieu, aux termes de l'article R. 119 du code électoral : " Les réclamations contre les opérations électorales doivent être consignées au procès-verbal, sinon être déposées, à peine d'irrecevabilité, au plus tard à dix-huit heures le cinquième jour qui suit l'élection, à la sous-préfecture ou à la préfecture. (...) / Les protestations peuvent également être déposées directement au greffe du tribunal administratif dans le même délai. (...) / (...) la notification est faite, dans les trois jours de l'enregistrement de la protestation, aux conseillers dont l'élection est contestée qui sont avisés en même temps qu'ils ont cinq jours pour tout délai à l'effet de déposer leurs défenses au greffe (bureau central ou greffe annexe) du tribunal administratif et de faire connaître s'ils entendent ou non user du droit de présenter des observations orales (...)." Il résulte de ces dispositions, d'une part, que la protestation de Mme A... ne peut être regardée comme irrecevable du fait qu'elle a été présentée par l'intéressée au greffe du tribunal administratif sans être consignée au procès-verbal et, d'autre part, que cette protestation n'avait pas à être communiquée par le tribunal à la commune, mais seulement à M. L..., unique conseiller municipal dont l'élection était contestée. Par suite, M. L... n'est pas fondé à soutenir que le jugement du tribunal administratif devrait être annulé pour ces motifs.<br/>
<br/>
              4. En second lieu, il résulte de l'instruction que, lors des opérations de dépouillement du scrutin qui s'est déroulé le 15 mars 2020, le nombre de bulletins trouvés dans l'urne excédait d'une unité le nombre des émargements. C'est par suite à bon droit que le tribunal administratif en a déduit qu'il y avait lieu, quelle que fût l'origine de cette erreur, de regarder ce suffrage comme irrégulier et de le retrancher du nombre des suffrages exprimés et, hypothétiquement, des voix obtenues par chacun des candidats proclamés élus, afin de déterminer s'ils demeuraient, même dans l'hypothèse où ce suffrage irrégulier se serait porté sur eux, en position de se voir attribuer un siège de conseiller municipal. <br/>
<br/>
              5. Après déduction du suffrage irrégulier, M. L..., dernier candidat proclamé élu, n'obtient plus que 79 voix, à égalité de voix avec Mme A..., première candidate non élue, sans qu'il puisse, dès lors qu'il est plus jeune que Mme A..., demeurer élu dans cette hypothèse au bénéfice de l'âge, l'élection étant, à égalité de suffrages, acquise, en vertu de l'article 253 du code électoral, au candidat le plus âgé. M. L... n'est dès lors pas fondé à soutenir que c'est à tort que le tribunal administratif de Dijon a annulé son élection. Il est, en revanche, fondé à soutenir que c'est à tort que le tribunal a proclamé élue Mme A... à sa place au bénéfice de l'âge, dès lors que celle-ci ne demeurerait pas en position d'être élue dans l'hypothèse où le suffrage irrégulier se serait porté sur elle.<br/>
<br/>
              6. Il résulte de ce qui précède que, le dernier candidat élu ne pouvant, dans la présente hypothèse, être déterminé, M. L... est seulement fondé à demander l'annulation de l'article 2 du jugement qu'il attaque, proclamant Mme A... élue en qualité de conseillère municipale d'Etevaux.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'article 2 du jugement du tribunal administratif de Dijon du 22 septembre 2020 est annulé.<br/>
Article 2 : Le surplus des conclusions de la requête de M. L... est rejeté.<br/>
Article 3 : La présente décision sera notifiée à M. C... L... et à Mme O... A....<br/>
Copie en sera adressée à M. J... I..., premier dénommé, pour l'ensemble des co-signataires de leurs observations, au préfet de la région Bourgogne - Franche-Comté, préfet de la Côte-d'Or et au ministre de l'intérieur.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
