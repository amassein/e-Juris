<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029986030</ID>
<ANCIEN_ID>JG_L_2014_12_000000371674</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/98/60/CETATEXT000029986030.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 29/12/2014, 371674, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-12-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>371674</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>M. Alain Méar</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:371674.20141229</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et les mémoires complémentaires, enregistrés les 27 août et 15 octobre 2013 et le 20 janvier 2014 au secrétariat du contentieux du Conseil d'Etat, présentés pour la Confédération CFE-CGC, dont le siège est au 59, rue du Rocher à Paris (75008) ; la confédération demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'instruction interministérielle n° 13-000955-I du 27 juin 2013 relative aux modalités du renouvellement des conseils économiques, sociaux et environnementaux régionaux (CESER) ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative, ainsi qu'une somme de 35 euros au titre de l'article R. 761-1 du même code.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alain Méar, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, avocat de la Confédération CFE-CGC ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article L. 4134-2 du code général des collectivités territoriales : " La composition des conseils économiques, sociaux et environnementaux régionaux, les conditions de nomination de leurs membres (...) sont fixées par un décret en Conseil d'Etat. " ; qu'en application de l'article R. 4134-1 du même code : " Les membres du conseil économique, social et environnemental régional sont répartis en quatre collèges composés comme suit: / (...) 2° Le deuxième collège comprend des représentants des organisations syndicales de salariés les plus représentatives ; / (...) Un tableau, constituant l'annexe XI du présent code, précise, pour chaque conseil économique, social et environnemental régional, le nombre de ses membres et la répartition de ces derniers entre les collèges. " ; qu'aux termes du deuxième alinéa de l'article R. 4134-3 du même code : " Les représentants des organisations syndicales de salariés sont désignés par les unions, fédérations et comités régionaux ou départementaux compte tenu notamment de leur représentativité dans la région. " ; qu'en vertu de l'article R. 4134-4 : " I.- Un arrêté du préfet de région fixe, par application des règles définies aux articles R. 4134-1 et R. 4134-3, la liste des organismes de toute nature représentés au conseil économique, social et environnemental régional, le nombre de leurs représentants (...) / II.-Un arrêté du préfet de région constate la désignation des représentants (...) des organisations syndicales de salariés (...) " ;<br/>
<br/>
              2. Considérant que le principe général de la représentativité implique notamment que la représentativité d'une organisation syndicale s'apprécie, pour la composition d'un organisme, au niveau territorial ou professionnel auquel il siège ; qu'ainsi, dans le cas d'un organisme régional, il appartient aux autorités administratives de mesurer la représentativité des syndicats appelés à y siéger en fonction de leurs résultats aux diverses élections professionnelles au niveau régional ;<br/>
<br/>
              3. Considérant que le décret du 27 janvier 2011 a modifié les dispositions du 2° de l'article R. 4134-1 du code général des collectivités territoriales pour prévoir que le deuxième collège du conseil économique, social et environnemental régional comprend " des représentants des organisations syndicales de salariés les plus représentatives " et non plus "  des représentants des organisations syndicales de salariés représentatives au niveau national, de l'Union nationale des syndicats autonomes et de la Fédération syndicale unitaire " ; que le pouvoir réglementaire a ainsi entendu n'admettre au sein du deuxième collège du conseil économique, social et environnemental régional, conformément au principe général de représentativité, que les organisations syndicales les plus représentatives au niveau régional ; qu'il suit de là que les dispositions du deuxième alinéa de l'article R. 4134-3 doivent être interprétées comme ayant pour seul objet de prévoir, d'une part, que le nombre de sièges attribués à chaque organisation syndicale parmi celles qui sont les plus représentatives dans la région considérée sont en principe proportionnels à ses résultats électoraux régionaux et, d'autre part, que les représentants des organisations sont désignés par leurs instances locales, départementales ou régionales ; qu'elles n'ont pas pour effet, contrairement à ce que soutient la Confédération CFE-CGC, d'imposer la prise en compte, pour la répartition des sièges du deuxième collège du conseil économique, social et environnemental régional, de la représentativité d'une organisation syndicale sur le plan national ;<br/>
<br/>
              4. Considérant que, par l'instruction interministérielle du 27 juin 2013, les ministres concernés ont indiqué aux préfets de région, dans la perspective du prochain renouvellement des conseils économiques, sociaux et environnementaux régionaux de métropole, la méthodologie qu'ils leur demandaient de suivre pour apprécier la représentativité des organisations syndicales de salariés au niveau de chaque région métropolitaine continentale, en vue de la répartition des sièges au sein du deuxième collège de ces assemblées consultatives ; que ce texte prescrit aux préfets d'apprécier la représentativité des organisations syndicales de salariés au niveau régional, après s'être assurés qu'elles répondaient aux critères découlant du principe général de représentativité, en mesurant leur audience en fonction des résultats, à l'échelon régional, des élections professionnelles les plus récentes du secteur privé et des trois fonctions publiques, et de répartir les sièges du deuxième collège à la proportionnelle, à partir de ces résultats appréciés tous secteurs confondus ; qu'il indique également que, si cette répartition conduit à écarter la représentation d'une organisation syndicale de salariés lorsque celle-ci recueille une audience significative uniquement dans l'un des secteurs considérés, le préfet doit veiller à lui attribuer un siège ; qu'il résulte de ce qui a été dit au point 3 qu'en énonçant ces prescriptions, les auteurs de l'instruction attaquée n'ont ni méconnu les dispositions législatives et réglementaires précitées, ni ajouté à ces dernières ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que la Confédération CFE-CGC n'est pas fondée à demander l'annulation de l'instruction interministérielle attaquée ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la Confédération CFE-CGC est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la Confédération CFE-CGC et au ministre de l'intérieur. <br/>
Copie en sera adressée à la ministre de la décentralisation et de la fonction publique et au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
