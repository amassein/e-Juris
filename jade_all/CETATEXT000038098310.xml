<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038098310</ID>
<ANCIEN_ID>JG_L_2019_02_000000417047</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/09/83/CETATEXT000038098310.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 6ème chambres réunies, 04/02/2019, 417047</TITRE>
<DATE_DEC>2019-02-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417047</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET BRIARD ; SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>M. Alain Seban</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:417047.20190204</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme D...B...a demandé au tribunal administratif de Rennes de condamner la commune de La Ville-ès-Nonais à lui verser, ainsi qu'à Mme C...A..., la somme de 220 000 euros en réparation du préjudice subi en raison de l'illégalité de l'arrêté de péril imminent du 25 mai 2009 du maire de La Ville-ès-Nonais. Par un jugement n° 1305023 du 22 juillet 2016, le tribunal administratif a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 16NT03249 du 20 octobre 2017, la cour administrative d'appel de Nantes a rejeté l'appel présenté contre ce jugement par Mmes B...etA....<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés au secrétariat du contentieux du Conseil d'Etat les 3 janvier et 3 avril 2018, Mme B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de la commune de La Ville-ès-Nonais une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de la construction et de l'habitation ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alain Seban, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, au Cabinet Briard, avocat de Mme B...et à la SCP Gaschignard, avocat de la commune de la Ville-ès-Nonais.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 511-1 du code de la construction et de l'habitation : " Le maire peut prescrire la réparation ou la démolition des murs, bâtiments ou édifices quelconques lorsqu'ils menacent ruine et qu'ils pourraient, par leur effondrement, compromettre la sécurité ou lorsque, d'une façon générale, ils n'offrent pas les garanties de solidité nécessaires au maintien de la sécurité publique, dans les conditions prévues à l'article L. 511-2. Toutefois, si leur état fait courir un péril imminent, le maire ordonne préalablement les mesures provisoires indispensables pour écarter ce péril, dans les conditions prévues à l'article L. 511-3 ".<br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 25 mai 2009, pris sur le fondement des articles L. 511-1 et L. 511-3 du code de la construction et de l'habitation, le maire de la commune de La Ville-ès-Nonais a prescrit aux consortsA..., dont MmeB..., de procéder à la démolition d'une dépendance d'une propriété leur appartenant située 6 et 8 impasse des Petits Clos. Le 16 juin 2009, le maire a fait procéder d'office à une démolition partielle. L'arrêté a ensuite été suspendu, à la demande des propriétaires, par une ordonnance du 23 juin 2009 du juge des référés du tribunal administratif de Rennes puis annulé par un jugement du 1er octobre 2009 du même tribunal. En décembre 2013, les consorts A...ont demandé que la commune soit condamnée à leur verser une indemnité de 220 000 euros en réparation des préjudices qu'ils estiment avoir subis du fait de l'exécution d'office de l'arrêté. Le tribunal administratif a rejeté cette demande par un jugement du 22 juillet 2016, que la cour administrative d'appel de Nantes a confirmé par un arrêt du 20 octobre 2017 contre lequel Mme B...se pourvoit en cassation. <br/>
<br/>
              3. Aux termes de l'article R. 421-1 du code de justice administrative, dans sa rédaction applicable au litige : " Sauf en matière de travaux publics, la juridiction ne peut être saisie que par voie de recours formé contre une décision, et ce, dans les deux mois à partir de la notification ou de la publication de la décision attaquée ". L'action engagée par les consorts A...devant le tribunal administratif tendait à la réparation des préjudices qu'ils estiment avoir subis du fait de travaux de démolition exécutés d'office par l'administration sur leur propriété. De tels travaux revêtant le caractère de travaux publics, la demande devait être regardée comme présentée " en matière de travaux publics " au sens des dispositions précitées. En jugeant qu'il en allait autrement, au motif que les requérants soutenaient que les travaux engageaient la responsabilité de la commune en raison de l'illégalité de l'arrêté ayant fait l'objet de l'exécution d'office et en en déduisant que la demande était irrecevable faute d'avoir été précédée d'une réclamation ayant fait naître une décision de l'administration, la cour administrative d'appel de Nantes a commis une erreur de droit. Son arrêt doit dès lors être annulé, sans qu'il soit besoin d'examiner les autres moyens du pourvoi.<br/>
<br/>
              4. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de Mme B...qui n'est pas, dans la présente instance, la partie perdante. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de La Ville-ès-Nonais la somme de 3 500 euros que demande Mme B... au même titre.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nantes du 20 octobre 2017 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nantes.<br/>
Article 3 : La commune de La Ville-ès-Nonais versera à Mme B...une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par la commune de La Ville-ès-Nonais au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à Mme B...et à la commune de La Ville-ès-Nonais.<br/>
Copie en sera adressée à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-01-07-01-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉLAIS. ABSENCE DE DÉLAIS. DEMANDES PRÉSENTÉES EN MATIÈRE DE TRAVAUX PUBLICS. - ACTION TENDANT À LA RÉPARATION DES PRÉJUDICES AYANT RÉSULTÉ POUR LES REQUÉRANTS DE TRAVAUX DE DÉMOLITION EXÉCUTÉS D'OFFICE PAR L'ADMINISTRATION SUR LEUR PROPRIÉTÉ - INCLUSION (ART. R. 421-1 DU CJA DANS SA VERSION ANTÉRIEURE AU DÉCRET DU 2 NOVEMBRE 2016) - CIRCONSTANCE QUE LES REQUÉRANTS SOUTENAIENT QUE LES TRAVAUX ENGAGEAIENT LA RESPONSABILITÉ DE LA COMMUNE EN RAISON DE L'ILLÉGALITÉ DE L'ARRÊTÉ AYANT FAIT L'OBJET DE L'EXÉCUTION D'OFFICE - INCIDENCE - ABSENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">67-05-01-01 TRAVAUX PUBLICS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. INTRODUCTION DE L'INSTANCE. DÉLAIS. - ACTION TENDANT À LA RÉPARATION DES PRÉJUDICES AYANT RÉSULTÉ POUR LES REQUÉRANTS DE TRAVAUX DE DÉMOLITION EXÉCUTÉS D'OFFICE PAR L'ADMINISTRATION SUR LEUR PROPRIÉTÉ - INCLUSION (ART. R. 421-1 DU CJA DANS SA VERSION ANTÉRIEURE AU DÉCRET DU 2 NOVEMBRE 2016) - CIRCONSTANCE QUE LES REQUÉRANTS SOUTENAIENT QUE LES TRAVAUX ENGAGEAIENT LA RESPONSABILITÉ DE LA COMMUNE EN RAISON DE L'ILLÉGALITÉ DE L'ARRÊTÉ AYANT FAIT L'OBJET DE L'EXÉCUTION D'OFFICE - INCIDENCE - ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 54-01-07-01-01 Une action tendant à la réparation des préjudices ayant résulté pour les requérants de travaux de démolition exécutés d'office par l'administration sur leur propriété doit, dès lors que de tels travaux revêtent le caractère de travaux publics, être regardée comme présentée en matière de travaux publics au sens de l'article R. 421-1 du code de justice administrative (CJA) dans sa rédaction antérieure au décret n° 2016-1480 du 2 novembre 2016. Commet une erreur de droit la cour qui juge qu'il en allait autrement, au motif que les requérants soutenaient que les travaux engageaient la responsabilité de la commune en raison de l'illégalité de l'arrêté ayant fait l'objet de l'exécution d'office, et en déduit que la demande était irrecevable faute d'avoir été précédée d'une réclamation ayant fait naître une décision de l'administration.</ANA>
<ANA ID="9B"> 67-05-01-01 Une action tendant à la réparation des préjudices ayant résulté pour les requérants de travaux de démolition exécutés d'office par l'administration sur leur propriété doit, dès lors que de tels travaux revêtent le caractère de travaux publics, être regardée comme présentée en matière de travaux publics au sens de l'article R. 421-1 du code de justice administrative (CJA) dans sa rédaction antérieure au décret n° 2016-1480 du 2 novembre 2016. Commet une erreur de droit la cour qui juge qu'il en allait autrement, au motif que les requérants soutenaient que les travaux engageaient la responsabilité de la commune en raison de l'illégalité de l'arrêté ayant fait l'objet de l'exécution d'office, et en déduit que la demande était irrecevable faute d'avoir été précédée d'une réclamation ayant fait naître une décision de l'administration.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp. CE, 17 novembre 2008, Entreprise Aubelec et Ahmed, n° 294215, T. pp. 844-957.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
