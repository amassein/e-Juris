<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034542405</ID>
<ANCIEN_ID>JG_L_2017_04_000000392699</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/54/24/CETATEXT000034542405.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 28/04/2017, 392699, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-04-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>392699</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>M. Jean-Marc Anton</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:392699.20170428</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société à responsabilité limitée H2I  a demandé au tribunal administratif de Châlons-en-Champagne la réduction de la cotisation de taxe foncière sur les propriétés bâties à laquelle elle a été assujettie au titre de l'année 2013 à raison de locaux situés sur le territoire de la commune de Biesles (Haute-Marne). Par un jugement n° 1401001 du 16 juin 2015, ce tribunal a rejeté cette demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 17 août et 17 novembre 2015 au secrétariat du contentieux du Conseil d'Etat, la société H2I demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier.<br/>
<br/>
              Vu : <br/>
              - le code civil ;<br/>
              - le code de commerce ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Anton, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Soltner, Texidor, Perier, avocat de la société H21.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que, par une convention du 24 décembre 1993, conclue pour une durée de neuf ans avec effet rétroactif au 1er décembre 1993, la société H2I a donné à bail commercial à la société Conversat Forges, devenue la société Forgeavia Fav Conversat Forges, un bâtiment situé sur le territoire de la commune de Biesles (Haute-Marne). Au cours de l'année 2012, l'administration fiscale a indiqué à la société qu'elle entendait modifier ses bases d'imposition à la taxe foncière par l'ajout de terrains et de bâtiments. La société H2I se pourvoit en cassation contre le jugement du 16 juin 2015 par lequel le tribunal administratif de Châlons-en-Champagne a rejeté sa demande tendant à la réduction de la cotisation de taxe foncière sur les propriétés bâties à laquelle elle a été assujettie au titre de l'année 2013.<br/>
<br/>
              Sur le bien-fondé du jugement en ce qui concerne la cotisation de taxe foncière sur les propriétés bâties mise à la charge de la société H2I à raison des constructions réalisées par la société locataire : <br/>
<br/>
              2. Aux termes du I de l'article 1400 du code général des impôts : " Sous réserve des dispositions des articles 1403 et 1404, toute propriété bâtie ou non bâtie doit être imposée au nom du propriétaire actuel (...) ". Aux termes de l'article 555 du code civil : " Lorsque les plantations, constructions et ouvrages ont été faits par un tiers et avec des matériaux appartenant à ce dernier, le propriétaire du fonds a le droit, sous réserve des dispositions de l'alinéa 4, soit d'en conserver la propriété, soit d'obliger le tiers à les enlever (...) ". <br/>
<br/>
              3. En application des dispositions citées ci-dessus du code civil, l'accession à la propriété des biens construits par un tiers sur le terrain que lui loue son propriétaire ne peut intervenir qu'à l'expiration du bail conclu avec ce tiers, sauf stipulations contraires.<br/>
<br/>
              4. Pour juger que la société H2I était redevable, au 1er janvier 2013, de l'imposition en litige à concurrence des constructions et aménagements réalisés par le preneur du bail, le tribunal administratif a estimé que la convention de location conclue le 24 décembre 1993 était parvenue à échéance le 30 novembre 2002 et que l'acte du 28 décembre 2002, conclu postérieurement au terme de cette convention et qui en modifiait au demeurant l'objet et le prix, ne pouvait s'analyser comme un avenant à cette convention mais devait être regardé comme un nouveau contrat. Il a estimé que ce nouveau contrat avait également une durée de neuf ans et avait donc expiré le 27 décembre 2011. Il en a déduit qu'en l'absence de stipulations contractuelles contraires, la société H2I était devenue propriétaire, le 30 novembre 2002 et le 27 décembre 2011, des constructions nouvelles et des aménagements sur le bâtiment loué réalisés, au cours de la période d'application de chacune de ces conventions de location, par la société Forgeavia Fav Conversat Forges. <br/>
<br/>
              5. Toutefois, le bail commercial conclu le 24 décembre 1993 comporte une clause selon laquelle " si, à l'expiration de la durée du bail, ci-dessus convenue, le preneur reste et est laissé en possession, les rapports entre bailleur et preneur continueront à être réglés par les conventions résultant du présent bail, et ce par dérogation aux dispositions de l'article 5 alinéa 2 du décret du 30 septembre 1953 ", tandis que l'avenant du 28 décembre 2002 indique que les " autres termes du bail initial du 24 décembre 1993 " demeurent.inchangés Dès lors, en estimant que la convention du 24 décembre 1993 était parvenue à échéance le 30 novembre 2002, qu'un nouveau contrat avait été conclu le 28 décembre 2002 et que celui-ci était lui-même parvenu à échéance le 27 décembre 2011, de sorte que le bailleur devait être regardé comme ayant accédé à la propriété des constructions et des aménagements constatés aux dates du 30 novembre 2002 et du 27 décembre 2011, alors qu'il ressortait des pièces du dossier que la société Forgeavia Fav Conversat Forges était restée en possession du bâtiment loué, au sens des stipulations de la clause mentionnée ci-dessus, après le 30 novembre 2002 et qu'ainsi la convention du 24 décembre 1993 était prolongée pour une durée indéterminée après cette date, le tribunal administratif a dénaturé les clauses de cette convention. La société est par suite fondée à soutenir que c'est à tort que le tribunal a jugé qu'elle était, pour le motif qu'il a retenu, redevable de l'imposition en litige à concurrence des constructions et aménagements réalisés par le preneur du bail.<br/>
<br/>
              Sur le bien-fondé du jugement en ce qui concerne la cotisation de taxe foncière sur les propriétés bâties mise à la charge de la société H2I à raison des travaux qu'elle a réalisés sur d'autres constructions : <br/>
<br/>
              6. Aux termes de l'article 1499 du code général des impôts : " La valeur locative des immobilisations industrielles passibles de la taxe foncière sur les propriétés bâties est déterminée en appliquant au prix de revient de leurs différents éléments, revalorisé à l'aide des coefficients qui avaient été prévus pour la révision des bilans, des taux d'intérêt fixés par décret en Conseil d'Etat (...) ".<br/>
<br/>
              7. Pour l'application de ces dispositions, le prix de revient des immobilisations industrielles passibles de la taxe foncière, évalué selon la méthode comptable, est celui qui est inscrit à l'actif du bilan et l'administration peut se fonder sur les énonciations comptables opposables à la société pour inclure dans la valeur locative des immobilisations le montant des travaux inscrits en tant qu'immobilisations, sauf pour la société à démontrer que ces travaux constitueraient en réalité des charges déductibles. <br/>
<br/>
              8. Le tribunal administratif n'a pas dénaturé les pièces du dossier en jugeant que la société n'apportait aucune justification de nature à établir que les travaux d'entretien réalisés et inscrits par elle en comptabilité en tant qu'immobilisations présentaient en réalité le caractère de charges déductibles et n'a pas entaché son jugement d'erreur de droit en en déduisant que ces travaux avaient à bon droit été pris en compte dans les bases d'imposition.<br/>
<br/>
              9. Il résulte de tout ce qui précède que la société H2I est seulement fondée à demander l'annulation du jugement du tribunal administratif en tant qu'il statue sur la cotisation de taxe foncière sur les propriétés bâties mise à sa charge au titre de l'année 2013 à raison des constructions et aménagements réalisés par la société locataire. En revanche, son pourvoi dirigé contre ce jugement en tant qu'il  statue sur la cotisation de taxe foncière sur les propriétés bâties mise à sa charge à raison des travaux qu'elle a réalisés sur d'autres constructions doit être rejeté.<br/>
<br/>
              10. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 2 000 euros à verser à la société H2I au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Châlons-en-Champagne du 16 juin 2015 est annulé en tant qu'il statue sur la cotisation de taxe foncière sur les propriétés bâties mise à la charge de la société H2I au titre de l'année 2013 à raison des constructions et aménagements réalisés par la société locataire. <br/>
<br/>
Article 2 : L'affaire est renvoyée, dans cette  mesure, au tribunal administratif de Châlons-en-Champagne. <br/>
Article 3 : L'Etat versera à la société H2I la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Le surplus des conclusions du pourvoi est rejeté.<br/>
<br/>
Article 5 : La présente décision sera notifiée à la société à responsabilité limitée H2I et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
