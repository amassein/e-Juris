<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000024755410</ID>
<ANCIEN_ID>JG_L_2011_10_000000335755</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/24/75/54/CETATEXT000024755410.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 5ème sous-sections réunies, 26/10/2011, 335755</TITRE>
<DATE_DEC>2011-10-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>335755</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 5ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI ; SCP DELVOLVE, DELVOLVE</AVOCATS>
<RAPPORTEUR>M. Bruno Bachini</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2011:335755.20111026</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 20 janvier et 22 mars 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SOCIETE TOTAL, dont le siège social est 2 place de la Coupole La Défense 6 à Courbevoie (92400) ; la SOCIETE TOTAL demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 07PA01459 du 16 novembre 2009 par lequel la cour administrative d'appel de Paris a annulé, d'une part, le jugement n° 0305340 du 17 janvier 2007 du tribunal administratif de Paris et, d'autre part, la décision du 27 mars 2003 du ministre des affaires sociales, du travail et de la solidarité lui accordant l'autorisation de mise à la retraite de M. B...A...; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M.A...; <br/>
<br/>
              3°) de mettre à la charge de ce dernier une somme de 4 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code du travail ;<br/>
<br/>
              Vu le code de justice administrative <br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Bachini, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Gatineau, Fattaccini, avocat de la SOCIETE TOTAL et de la SCP Delvolvé, Delvolvé, avocat de M.A..., <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Gatineau, Fattaccini, avocat de la SOCIETE TOTAL et à la SCP Delvolvé, Delvolvé, avocat de M.A..., <br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'en vertu des dispositions du code du travail, les salariés légalement investis de fonctions représentatives bénéficient, dans l'intérêt de l'ensemble des travailleurs qu'ils représentent, d'une protection exceptionnelle ; que dans le cas où la demande de rupture du contrat de travail d'un salarié protégé par l'employeur est motivée par la survenance de l'âge, il appartient à l'inspecteur du travail, et le cas échéant au ministre, de vérifier sous le contrôle du juge de l'excès de pouvoir, d'une part, que la mesure envisagée n'est pas en rapport avec les fonctions représentatives exercées ou l'appartenance syndicale de l'intéressé et, d'autre part, que les conditions légales de mise à la retraite sont remplies ; que cette rupture doit suivre la procédure prévue en cas de licenciement ; qu'il s'ensuit que la mise à la retraite, envisagée par l'employeur au titre des dispositions, alors applicables, de l'article L. 122-14-13 du même code, désormais reprises à l'article L. 1237-5, d'un salarié élu délégué du personnel ou membre du comité d'entreprise, en qualité de titulaire ou de suppléant, est obligatoirement soumise à l'avis du comité d'entreprise prévu par les dispositions des articles L. 425 -1 et L. 436-1, alors en vigueur, du code du travail, reprises à l'article L. 2421-3 ; qu'à cette fin, il appartient à l'employeur de mettre le comité d'entreprise à même d'émettre son avis, en toute connaissance de cause, sur la procédure dont fait l'objet le salarié protégé, en lui transmettant des informations précises et écrites sur les motifs de celle-ci, ainsi que le prescrivent les dispositions, alors applicables, de l'article L. 431-5, reprises à l'article L. 2323-4 ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que la cour administrative d'appel de Paris n'a pas commis d'erreur de droit en jugeant que l'absence de communication au comité d'établissement, à l'occasion de sa séance du 30 juillet 2002 au cours de laquelle devait être examiné le projet de mise à la retraite de M.A..., titulaire de plusieurs mandats représentatifs au sein de la société Elf Exploration Production, de toute pièce lui permettant de vérifier que l'intéressé bénéficiait d'un nombre de trimestres validés suffisant pour recevoir une pension à taux plein, avait été de nature à entacher d'irrégularité la procédure de mise à la retraite litigieuse ; <br/>
<br/>
              Considérant qu'en retenant que le ministre chargé du travail n'avait pas vérifié la teneur du dossier soumis au comité d'établissement, la cour, qui a suffisamment motivé sa décision sur ce point, n'a pas commis d'erreur de fait ni dénaturé les pièces du dossier ; que le moyen tiré de ce qu'elle aurait entaché son arrêt d'erreur de fait et de dénaturation en estimant que l'inspecteur du travail s'était fondé sur le caractère incomplet des informations fournies au comité d'établissement pour refuser l'autorisation de mise à la retraite demandée est, en tout état de cause, inopérant à l'appui de conclusions relatives à la décision du ministre ayant annulé cette décision de refus et autorisé la mise à la retraite du salarié ;  <br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que la SOCIETE TOTAL, venant aux droits de la société Elf Exploration Production, n'est pas fondée à demander l'annulation de l'arrêt attaqué ; <br/>
<br/>
              Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. A...qui n'est pas, dans la présente instance, la partie perdante ; qu'en revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société requérante la somme de 3 000 euros à verser à M. A...au titre de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : Le pourvoi de la SOCIETE TOTAL est rejeté.<br/>
<br/>
Article 2 : La SOCIETE TOTAL versera à M. A...une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 3 : La présente décision sera notifiée à la SOCIETE TOTAL, à M. B...A...et au ministre du travail, de l'emploi et de la santé.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">66-04-01-03 TRAVAIL ET EMPLOI. INSTITUTIONS REPRÉSENTATIVES DU PERSONNEL. COMITÉS D'ENTREPRISE. ATTRIBUTIONS. - CONSULTATION OBLIGATOIRE SUR LA DÉCISION DE MISE À LA RETRAITE D'UN SALARIÉ PROTÉGÉ - EXISTENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">66-07-01-02-02 TRAVAIL ET EMPLOI. LICENCIEMENTS. AUTORISATION ADMINISTRATIVE - SALARIÉS PROTÉGÉS. PROCÉDURE PRÉALABLE À L'AUTORISATION ADMINISTRATIVE. CONSULTATION DU COMITÉ D'ENTREPRISE. - DÉCISION DE MISE À LA RETRAITE D'UN SALARIÉ PROTÉGÉ (ANC. ART. L. 122-14-13, DÉSORMAIS L. 1237-5, DU CODE DU TRAVAIL) - DÉCISION ASSIMILABLE À UN LICENCIEMENT - CONSÉQUENCE - PROCÉDURE DEVANT SUIVRE CELLE APPLICABLE EN CAS DE LICENCIEMENT, DONT LA CONSULTATION DU COMITÉ D'ENTREPRISE (ANC. ART. L. 425-1 ET L. 436-1, DÉSORMAIS L. 2421-3 DU CODE DU TRAVAIL) [RJ1].
</SCT>
<ANA ID="9A"> 66-04-01-03 La rupture par l'employeur du contrat de travail d'un salarié protégé motivée par la survenance de l'âge permettant, ainsi que le prévoyaient les dispositions de l'ancien article L. 122-14-13 du code du travail, aujourd'hui reprises à l'article L. 1237-5, du même code, sa mise à la retraite, doit être autorisée par l'inspecteur du travail et suivre la procédure prévue en cas de licenciement d'un salarié. L'avis du comité d'entreprise, prévu par les dispositions des anciens articles L. 425-1 et L. 436-1 du même code, aujourd'hui reprises à l'article L. 2421-3, est donc obligatoire et il appartient à l'employeur de transmettre à ce comité les informations précises et écrites sur les motifs de la procédure, ainsi que le prescrivaient les dispositions de l'article L. 431-5 du code du travail, reprises à l'article L. 2323-4.</ANA>
<ANA ID="9B"> 66-07-01-02-02 La rupture par l'employeur du contrat de travail d'un salarié protégé motivée par la survenance de l'âge permettant, ainsi que le prévoyaient les dispositions de l'ancien article L. 122-14-13 du code du travail, aujourd'hui reprises à l'article L. 1237-5, du même code, sa mise à la retraite, doit être autorisée par l'inspecteur du travail et suivre la procédure prévue en cas de licenciement d'un salarié. L'avis du comité d'entreprise, prévu par les dispositions des anciens articles L. 425-1 et L. 436-1 du même code, aujourd'hui reprises à l'article L. 2421-3, est donc obligatoire et il appartient à l'employeur de transmettre à ce comité les informations précises et écrites sur les motifs de la procédure, ainsi que le prescrivaient les dispositions de l'article L. 431-5 du code du travail, reprises à l'article L. 2323-4.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., à propos de l'entretien préalable, CE, 17 juin 2009, Société du Crédit du Nord, n° 304027, T. p. 977.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
