<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028349188</ID>
<ANCIEN_ID>JG_L_2013_12_000000361593</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/34/91/CETATEXT000028349188.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème / 1ère SSR, 13/12/2013, 361593, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-12-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>361593</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème / 1ère SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Samuel Gillis</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:361593.20131213</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 2 août 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par M. A...B..., demeurant ...; M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 23 juillet 2012 par laquelle le garde des sceaux, ministre de la justice, a rejeté sa demande tendant à l'abrogation des articles 2 à 5 du décret n° 72-785 du 25 août 1972 relatif au démarchage et à la publicité en matière de consultation et de rédaction d'actes juridiques, des mots " dès lors qu'elle est exclusive de toute forme de démarchage " et du troisième alinéa de l'article 15 du décret n° 2005-790 du 12 juillet 2005 relatif aux règles de déontologie de la profession d'avocat ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              3°) d'ordonner toutes les mesures de publicité nécessaires ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la directive 2006/123/CE du 12 décembre 2006 ;<br/>
<br/>
              Vu la loi n° 71-1130 du 31 décembre 1971 ;<br/>
<br/>
              Vu le décret n° 72-785 du 25 août 1972 ;<br/>
<br/>
              Vu le décret n° 2005-790 du 12 juillet 2005 ;<br/>
<br/>
              Vu l'arrêt de la Cour de justice de l'Union européenne C-119/09 du 5 avril 2011 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Samuel Gillis, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que M. B...demande l'annulation pour excès de pouvoir de la décision du 23 juillet 2012 par laquelle le garde des sceaux, ministre de la justice, a rejeté sa demande tendant à l'abrogation de diverses dispositions du décret du 25 août 1972 relatif au démarchage et à la publicité en matière de consultation et de rédaction d'actes juridiques et du décret du 12 juillet 2005 relatif aux règles de déontologie de la profession d'avocat en ce qu'elles édictent des interdictions de certaines formes de communication commerciale qu'il estime contraires au droit de l'Union européenne ; qu'eu égard aux termes de sa requête, M. B...doit être regardé comme demandant l'annulation de cette décision dans la seule mesure où les dispositions dont l'abrogation est demandée concernent les avocats ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article 24 de la directive 2006/123/CE du 12 décembre 2006 relative aux services dans le marché intérieur : " 1. Les Etats membres suppriment toutes les interdictions totales visant les communications commerciales des professions réglementées. / 2. Les Etats membres veillent à ce que les communications commerciales faites par les professions réglementées respectent les règles professionnelles conformes au droit communautaire, qui visent notamment l'indépendance, la dignité et l'intégrité de la profession ainsi que le secret professionnel, en fonction de la spécificité de chaque profession. Les règles professionnelles en matière de communications commerciales doivent être non discriminatoires, justifiées par une raison impérieuse d'intérêt général et proportionnées " ; que le paragraphe 12 de l'article 4 de la même directive définit la communication commerciale comme " toute forme de communication destinée à promouvoir directement ou indirectement, les biens, les services ou l'image d'une (...) personne exerçant une profession réglementée " mais précise, dans son a), que " les informations permettant l'accès direct à l'activité (...) de la personne, notamment un nom de domaine ou une adresse de courrier électronique ne constituent pas en tant que telles des communications commerciales " ; que ces dispositions s'opposent à une réglementation nationale qui interdit totalement aux membres d'une profession réglementée de recourir au démarchage ou de proposer à leurs clients une offre personnalisée de services, quels que soient leur forme, leur contenu et les moyens employés, ou prohibe de manière générale le recours à la publicité dans les médias ; <br/>
<br/>
              3. Considérant qu'aux termes de l'article 66-4 de la loi du 31 décembre 1971 portant réforme de certaines professions judiciaires : " Sera puni des peines prévues à l'article 72 quiconque se sera livré au démarchage en vue de donner des consultations ou de rédiger des actes en matière juridique. Toute publicité aux mêmes fins est subordonnée au respect de conditions fixées par le décret visé à l'article 66-6 " ; que les articles 2 et 3 du décret du 25 août 1972 précité interdisent la publicité en vue de donner des consultations, de rédiger des actes ou de proposer son assistance en matière juridique par voie de tracts, affiches, films cinématographiques, émissions radiophoniques ou télévisées, tout en exonérant de cette prohibition certains organismes ; qu'aux termes de l'article 4 du même décret : " La publicité faite, par quelque moyen que ce soit, aux fins mentionnées à l'article 2 ne doit contenir aucune indication contraire à la loi. / Elle doit s'abstenir, notamment, de toute mention méconnaissant la discrétion professionnelle ou portant atteinte à la vie privée. /Toute publicité mensongère ou contenant des renseignements inexacts ou fallacieux est prohibée " ; que l'article 5 du même décret sanctionne d'une peine d'amende la pratique du démarchage et les infractions à ses articles 2, 3 et 4 ; que l'article 15 du décret du 12 juillet 2005 précité dispose : " La publicité est permise à l'avocat si elle procure une information au public et si sa mise en oeuvre respecte les principes essentiels de la profession. / La publicité inclut la diffusion d'informations sur la nature des prestations de services proposées, dès lors qu'elle est exclusive de toute forme de démarchage./Toute offre de service personnalisée adressée à un client potentiel est interdite à l'avocat " ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que, comme le soutient le requérant, les dispositions de l'article 66-4 de la loi du 31 décembre 1971 portant réforme de certaines professions judiciaires et juridiques, comme celles du second alinéa de l'article 5 du décret du 25 août 1972 et celles des deuxième et troisième alinéas de l'article 15 du décret du 12 juillet 2005 qui en font application, prohibent pour les avocats, toute activité de démarchage ou offre personnalisée de services juridiques ; qu'en vertu des dispositions des articles 2, 3 et 5 du décret du 25 août 1972, il est interdit aux avocats de recourir à la publicité dans les médias en vue de donner des consultations, de rédiger des actes ou de proposer leur assistance en matière juridique ; qu'il résulte de ce qui a été dit au point 2 que de telles dispositions sont incompatibles avec les articles 4 et 24 de la directive du Parlement européen et du Conseil du 12 décembre 2006 relative aux services dans le marché intérieur ; qu'en revanche, l'article 4 du décret du 25 août 1972, qui se borne à rappeler que la publicité ne doit contenir aucune indication contraire à la loi, et notamment ne pas comporter de mention méconnaissant la discrétion professionnelle ou portant atteinte à la vie privée, et à prohiber la publicité mensongère ou contenant des renseignements inexacts ou fallacieux, n'est pas contraire aux dispositions de la directive ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que M. B...est fondé à demander l'annulation de la décision qu'il attaque en tant seulement qu'elle refuse d'abroger les mots " dès lors qu'elle est exclusive de toute forme de démarchage " figurant au deuxième alinéa de l'article 15 du décret du 12 juillet 2005 ainsi que le troisième alinéa de cet article, et, en tant qu'ils s'appliquent aux avocats, les articles 2 et 3 du décret du 25 août 1972, la référence à ces deux articles figurant au premier alinéa de l'article 5 du même décret ainsi que le second alinéa de cet article 5, qui sont divisibles des autres dispositions de ces décrets ; qu'il y a lieu de rejeter le surplus de ses conclusions dirigées contre la décision attaquée ;<br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 500 euros qui sera versée à M. B...au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
<br/>
Article 1er : La décision du 23 juillet 2012 du garde des sceaux, ministre de la justice, est annulée en tant qu'elle refuse d'abroger les mots " dès lors qu'elle est exclusive de toute forme de démarchage " figurant au second alinéa de l'article 15 du décret n° 2005-790 du 12 juillet 2005 relatif aux règles de déontologie de la profession d'avocat et le troisième alinéa de cet article et, en tant qu'ils s'appliquent aux avocats, les articles 2 et 3 du décret n° 72-785 du 25 août 1972 relatif au démarchage et à la publicité en matière de consultation et de rédaction d'actes juridiques, les chiffres " 2, 3 " figurant au premier alinéa de l'article 5 de ce même décret et le second alinéa de ce dernier article.<br/>
Article 2 : L'Etat versera à M. B...une somme de 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Le surplus des conclusions de la requête de M. B...est rejeté.<br/>
Article 4 : La présente décision sera notifiée à M. A...B..., au Premier ministre et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
