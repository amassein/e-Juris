<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027656276</ID>
<ANCIEN_ID>JG_L_2013_07_000000368854</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/65/62/CETATEXT000027656276.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 03/07/2013, 368854, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-07-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>368854</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SPINOSI</AVOCATS>
<RAPPORTEUR>M. Vincent Montrieux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:368854.20130703</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le jugement n° 1202463 du 23 mai 2013, enregistré le 27 mai 2013 au secrétariat du contentieux du Conseil d'Etat, par lequel le tribunal administratif de Strasbourg, avant de statuer sur la demande de Mme B...A..., tendant, d'une part, à l'annulation des décisions du 11 avril 2012 par lesquelles le préfet du Bas-Rhin a refusé de l'admettre provisoirement au séjour en qualité de demandeur d'asile et a décidé sa réadmission vers la Suisse, et, d'autre part, à ce qu'il soit enjoint au préfet de lui délivrer une autorisation provisoire de séjour ainsi qu'un formulaire de demande d'asile à l'Office français de protection des réfugiés et des apatrides selon la procédure normale, subsidiairement de réexaminer sa demande, sous astreinte de 200 euros par jour de retard à compter de la notification du jugement à intervenir, a transmis au Conseil d'Etat, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel, la question de la conformité aux droits et libertés garantis par la Constitution des dispositions des articles L. 723-1 alinéa 1, L. 741-4 1°, L. 742-4 et L. 742-5 du code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              Vu le mémoire, enregistré le 26 février 2013 au greffe du tribunal administratif de Strasbourg, présenté pour Mme B...A..., demeurant..., en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment ses articles 61-1 et 62 ;<br/>
<br/>
              Vu la convention de Genève relative au statut des réfugiés ;<br/>
<br/>
              Vu le règlement (CE) du Conseil n° 343/2003 du 18 février 2003 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
<br/>
              Vu les décisions du Conseil constitutionnel n° 2003-485 DC du 4 décembre 2003 et n° 2011-120 QPC du 8 avril 2011 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Montrieux, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Spinosi, avocat de Mme A...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2. Considérant qu'aux termes du quatrième alinéa du préambule de la Constitution de 1946 : " Tout homme persécuté en raison de son action en faveur de la liberté a droit d'asile sur les territoires de la République. " ; que l'article 53-1 de la Constitution dispose que : " La République peut conclure avec les Etats européens qui sont liés par des engagements identiques aux siens en matière d'asile et de protection des Droits de l'homme et des libertés fondamentales, des accords déterminant leurs compétences respectives pour l'examen des demandes d'asile qui leur sont présentées. / Toutefois, même si la demande n'entre pas dans leur compétence en vertu de ces accords, les autorités de la République ont toujours le droit de donner asile à tout étranger persécuté en raison de son action en faveur de la liberté ou qui sollicite la protection de la France pour un autre motif " ; qu'aux termes de l'article L. 723-1 du code de l'entrée et du séjour des étrangers et du droit d'asile : " L'office statue sur les demandes d'asile dont il est saisi. Il n'est toutefois pas compétent pour connaître d'une demande présentée par une personne à laquelle l'admission au séjour a été refusée pour le motif prévu au 1° de l'article L. 741-4. ( ...) " ; que cet article dispose que : " Sous réserve du respect des stipulations de l'article 33 de la convention de Genève du 28 juillet 1951 relative au statut des réfugiés, l'admission en France d'un étranger qui demande à bénéficier de l'asile ne peut être refusée que si : / 1° L'examen de la demande d'asile relève de la compétence d'un autre Etat en application des dispositions du règlement (CE) n° 343/2003 du Conseil du 18 février 2003 établissant les critères et mécanismes de détermination de l'Etat membre responsable de l'examen d'une demande d'asile présentée dans l'un des Etats membres par un ressortissant d'un pays tiers, ou d'engagements identiques à ceux prévus par ledit règlement avec d'autres Etats ; (...) " ; qu'au termes de l'article L. 742-4 de ce code : " Dans le cas où l'admission au séjour a été refusée pour le motif mentionné au 1° de l'article L. 741-4, l'intéressé n'est pas recevable à saisir la Cour nationale du droit d'asile. " ; qu'enfin, l'article L. 742-5 prévoit que : " Dans le cas où l'admission au séjour a été refusée pour l'un des motifs mentionnés aux 2° à 4° de l'article L. 741-4, l'étranger qui souhaite bénéficier de l'asile peut saisir l'office de sa demande. Celle-ci est examinée dans les conditions prévues au second alinéa de l'article L. 723-1. " ;<br/>
<br/>
              3. Considérant que, par la décision n° 2003-485 DC du 4 décembre 2003, le Conseil constitutionnel a, dans ses motifs et son dispositif, déclaré conforme à la Constitution l'article 5 de la loi n° 2003-1176 du 10 décembre 2003 modifiant la loi n° 52-893 du 25 juillet 1952 relative au droit d'asile, d'où sont issus les articles L. 723-1, L. 741-4, L. 742-4 et L. 742-5 du code de l'entrée et du séjour des étrangers et du droit d'asile ; qu'aucun changement de circonstances survenu depuis lors n'est de nature à justifier que la conformité de ces dispositions à la Constitution soit à nouveau examinée par le Conseil constitutionnel, alors même que cette décision ne s'est pas expressément prononcée sur le moyen tiré du quatrième alinéa du préambule de la Constitution du 27 octobre 1946 ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède qu'il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er: Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité transmise par le tribunal administratif de Strasbourg.  <br/>
Article 2 : La présente décision sera notifiée à Mme B...A...et au ministre de l'intérieur.<br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre et au tribunal administratif de Strasbourg. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
