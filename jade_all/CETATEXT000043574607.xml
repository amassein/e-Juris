<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043574607</ID>
<ANCIEN_ID>JG_L_2021_05_000000450670</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/57/46/CETATEXT000043574607.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 31/05/2021, 450670, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-05-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>450670</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. François Weil</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:450670.20210531</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 12 mars 2021 au secrétariat du contentieux du Conseil d'Etat, M. B... A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du 17 décembre 2020 accordant son extradition aux autorités macédoniennes ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne d'extradition du 13 décembre 1957 ;<br/>
              - le code de procédure pénale ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Weil, conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public,<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Par le décret attaqué du 17 décembre 2020, le Premier ministre a accordé aux autorités macédoniennes l'extradition de M. B... A..., de nationalité macédonienne, pour l'exécution de deux décisions rendues par le tribunal de première instance de Kumanovo, l'une le 25 septembre 2014 pour des faits qualifiés d'abus de pouvoir et de falsification d'un document officiel et l'autre le 31 août 2016 pour des faits qualifiés d'escroquerie et de falsification de documents officiels, assorties toutes les deux d'un mandat d'arrêt décerné par le juge de l'exécution des sanctions près la même juridiction.<br/>
<br/>
              2. En premier lieu, aux termes de l'article 12, paragraphe 2, de la convention européenne d'extradition du 13 décembre 1957 : " Il sera produit à l'appui de la requête : / a) L'original ou l'expédition authentique soit d'une décision de condamnation exécutoire, soit d'un mandat d'arrêt ou de tout autre acte ayant la même force, délivré dans les formes prescrites par la loi de la Partie requérante / b) Un exposé des faits pour lesquels l'extradition est demandée. (...) ; et / c) Une copie des dispositions légales applicables ou, si cela n'est pas possible, une déclaration sur le droit applicable (...) ". Aux termes de l'article 23 de la même convention : " Les pièces à produire seront rédigées soit dans la langue de la Partie requérante, soit dans celle de la Partie requise. Cette dernière pourra réclamer une traduction dans la langue officielle du Conseil de l'Europe qu'elle choisira ". Par déclaration annexée à la convention, la France a indiqué qu'elle choisissait le français.<br/>
<br/>
              3. Il ressort des pièces du dossier que, contrairement à ce qui est soutenu, la demande d'extradition était accompagnée des textes d'incrimination et de répression applicables, traduits en français. Si le requérant met en cause la mauvaise qualité de la traduction des décisions du tribunal de première instance de Kumanovo, cette circonstance n'a pas privé les autorités françaises des éléments nécessaires à l'examen de la demande dont elles étaient saisies. Ainsi, le requérant n'est pas fondé à soutenir que les stipulations précitées de la convention européenne d'extradition n'auraient pas été respectées.<br/>
<br/>
              4. En second lieu, si, aux termes du second alinéa de l'article 1er des réserves et déclarations émises par la France lors de la ratification de la convention européenne d'extradition, l'extradition peut être refusée si la remise est susceptible d'avoir des conséquences d'une gravité exceptionnelle pour la personne réclamée, ces conséquences ne sauraient s'apprécier qu'au regard de cette dernière. Si l'intéressé fait valoir les graves conséquences que l'exécution du décret attaqué est susceptible d'avoir sur sa vie familiale en France, notamment du fait de l'état de santé dégradé de son épouse à laquelle a été délivré un titre de séjour pour ce motif, une telle décision trouve, en principe, sa justification dans la nature même de la procédure d'extradition, qui est de permettre, dans l'intérêt de l'ordre public et sous les conditions fixées par les dispositions qui la régissent, tant le jugement de personnes se trouvant en France poursuivies à l'étranger pour des crimes ou des délits commis hors du territoire national que l'exécution, par les mêmes personnes, des condamnations pénales prononcées contre elles à l'étranger pour de tels crimes ou délits. Ainsi, dans les circonstances de l'espèce, en autorisant l'extradition de M. A..., le Premier ministre n'a pas commis d'erreur manifeste au regard des exigences résultant du second alinéa des réserves émises par la France à la ratification de la convention européenne d'extradition. <br/>
<br/>
              5. Il résulte de tout ce qui précède que M. A... n'est pas fondé à demander l'annulation pour excès de pouvoir du décret attaqué. Ses conclusions présentées sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, en conséquence, qu'être rejetées.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. A... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. B... A... et au garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
