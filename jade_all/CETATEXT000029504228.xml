<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029504228</ID>
<ANCIEN_ID>JG_L_2014_09_000000379994</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/50/42/CETATEXT000029504228.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème SSJS, 26/09/2014, 379994, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-09-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>379994</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Didier Ribes</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:379994.20140926</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 28 avril 2014 au secrétariat du contentieux du Conseil d'Etat, présentée par Mme A...B..., demeurant ... ; Mme B...demande au Conseil d'Etat d'annuler pour excès de pouvoir le décret n° 2014-262 du 26 février 2014 portant délimitation des cantons dans le département des Pyrénées-Orientales ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code électoral ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu la loi n° 2013-403 du 17 mai 2013 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier Ribes, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 191-1 du code électoral, résultant de la loi du 17 mai 2013 relative à l'élection des conseillers départementaux, des conseillers municipaux et des conseillers communautaires et modifiant le calendrier électoral : " Le nombre de cantons dans lesquels seront élus les conseillers départementaux est égal, pour chaque département, à la moitié du nombre de cantons existant au 1er janvier 2013, arrondi à l'unité impaire supérieure si ce nombre n'est pas entier impair. / Le nombre de cantons dans chaque département comptant plus de 500 000 habitants ne peut être inférieur à dix-sept. Il ne peut être inférieur à treize dans chaque département comptant entre 150 000 et 500 000 habitants " ; qu'aux termes de l'article L. 3113-2 du code général des collectivités territoriales, dans sa rédaction résultant de la même loi du 17 mai 2013, applicable à la date du décret attaqué : " I.- Les modifications des limites territoriales des cantons, les créations et suppressions de cantons et le transfert du siège de leur chef-lieu sont décidés par décret en Conseil d'Etat après consultation du conseil général qui se prononce dans un délai de six semaines à compter de sa saisine. A l'expiration de ce délai, son avis est réputé rendu. / II. - La qualité de chef-lieu de canton est maintenue aux communes qui la perdent dans le cadre d'une modification des limites territoriales des cantons, prévue au I, jusqu'au prochain renouvellement général des conseils généraux. / III. La modification des limites territoriales des cantons effectuée en application du I est conforme aux règles suivantes : / a) Le territoire de chaque canton est défini sur des bases essentiellement démographiques ; / b) Le territoire de chaque canton est continu ; / c) Est entièrement comprise dans le même canton toute commune de moins de 3 500 habitants. / IV. Il n'est apporté aux règles énoncées au III que des exceptions de portée limitée, spécialement justifiées, au cas par cas, par des considérations géographiques ou par d'autres impératifs d'intérêt général " ;<br/>
<br/>
              2. Considérant que le décret attaqué a, sur le fondement de l'article L. 3113-2 du code général des collectivités territoriales, procédé à une nouvelle délimitation des cantons du département des Pyrénées-Orientales, compte tenu de l'exigence de réduction du nombre des cantons de ce département de 31 à 17 résultant de l'article L. 191-1 du code électoral ; <br/>
<br/>
              Sur la légalité externe du décret attaqué : <br/>
<br/>
              3. Considérant, en premier lieu, qu'il n'est pas contesté que le conseil général des Pyrénées-Orientales a été consulté dans les conditions prévues par les dispositions du I de l'article L. 3113-2 du code général des collectivités territoriales cité ci-dessus ; que la circonstance que cette consultation n'ait fait l'objet que de deux réunions préalables auxquelles auraient assisté un faible nombre d'élus, est, en tout état de cause, sans incidence sur la régularité de la consultation ; <br/>
<br/>
              4. Considérant, en deuxième lieu, qu'aux termes de l'article L. 3121-19 du code général des collectivités territoriales : " Douze jours au moins avant la réunion du conseil général, le président adresse aux conseillers généraux un rapport, sous quelque forme que ce soit, sur chacune des affaires qui doivent leur être soumises. (...) / Sans préjudice des dispositions de l'article L. 3121-18, en cas d'urgence, le délai prévu au premier alinéa peut être abrégé par le président sans pouvoir être toutefois inférieur à un jour franc. " ; qu'il ressort des pièces du dossier que la présidente du conseil général des Pyrénées-Orientales a adressé le rapport mentionné à cet article le 24 septembre 2013 à l'ensemble des conseillers généraux, lesquels ont, par délibération du 7 octobre 2013, rendu un avis sur le projet de délimitation des cantons du département ; qu'est sans incidence la circonstance que la commission permanente du conseil général des Pyrénées-Orientales a rendu le 30 septembre 2013, soit six jours après la saisine par sa présidente de l'ensemble des conseillers généraux, un avis sur le projet de modification territoriale des cantons ; que, par suite, le moyen tiré de ce que la procédure de consultation du conseil général aurait méconnu les dispositions de l'article L. 3121-19 du code général des collectivités territoriales ne peut qu'être écarté ; que la requérante n'est pas fondée à soutenir qu'ont été méconnues les dispositions de l'article 7 du règlement intérieur du conseil général des Pyrénées-Orientales, selon lesquelles le rapport est adressé aux membres de la commission permanente " au moins 8 jours avant la date de la réunion, dans toute la mesure du possible dès lors qu'il résulte de leurs termes mêmes qu'un délai inférieur à huit jours n'est pas par lui-même irrégulier " ;<br/>
<br/>
              5. Considérant, en troisième lieu, que les dispositions du I de l'article              L. 3113-2 du code général des collectivités territoriales prévoient que le conseil général se prononce dans un délai de six semaines à compter de sa saisine par le représentant de l'État ; que la circonstance que la commission permanente du conseil général des Pyrénées-Orientales ait rendu son avis le 30 septembre 2013, soit six jours seulement après la saisine par sa présidente de l'ensemble des conseillers généraux n'est, en tout état de cause, pas contraire à ces dispositions ; <br/>
<br/>
              6. Considérant, en quatrième lieu, que, contrairement à ce que soutient la requérante, la délibération du 7 octobre 2013 du conseil général relative au projet portant délimitation des cantons dans le département des Pyrénées-Orientales est intervenue en séance publique ; que la circonstance que la commission permanente se soit réunie à huis-clos n'entache pas la régularité du décret attaqué, la commission permanente n'étant pas tenue à une obligation de publicité des débats ; <br/>
<br/>
              7. Considérant, en cinquième lieu, que la circonstance que l'avis rendu le 7 octobre 2013 par le conseil général sur le projet de décret portant délimitation des cantons dans le département des Pyrénées-Orientales n'aurait pas fait l'objet d'une mesure de publicité est sans influence sur la légalité du décret attaqué ;<br/>
<br/>
              8. Considérant, en sixième lieu, qu'aucune disposition législative n'imposait la motivation du décret attaqué ;<br/>
<br/>
              Sur la légalité interne du décret attaqué :<br/>
<br/>
              9. Considérant, en premier lieu, que s'il est soutenu que le décret attaqué a été élaboré sans tenir compte des données démographiques les plus récentes, l'article 71 du décret du 18 octobre 2013, dans sa rédaction applicable à la date du décret attaqué et dont la légalité n'est pas contestée, dispose toutefois que : " (...) Pour la première délimitation générale des cantons opérée en application de l'article L. 3113-2  du code général des collectivités territoriales, dans sa rédaction résultant de l'article 46 de la loi n° 2013-403 du 17 mai 2013 relative à l'élection des conseillers départementaux, (...), le chiffre de la population municipale auquel il convient de se référer est celui authentifié par le décret n° 2012-1479 du 27 décembre 2012 authentifiant les chiffres des populations (...). " ; qu'il est constant que les nouveaux cantons du département des Pyrénées-Orientales ont été délimités sur la base des données authentifiées par le décret du 27 décembre 2012 ; qu'en outre, aucune disposition législative, ni aucun principe n'imposaient au Premier ministre de prendre en compte les prévisions d'évolution démographique élaborées par l'Institut national de la statistique et des études économiques pour procéder à la délimitation des nouveaux cantons ; que, par suite, les moyens tirés de ce que les données retenues pour la délimitation des cantons du département ne correspondraient pas à la réalité démographique ne peuvent qu'être écartés ;<br/>
<br/>
              10. Considérant, en deuxième lieu, que, pour mettre en oeuvre les critères définis au III de l'article L. 3113-2 du code général des collectivités territoriales, le décret attaqué a procédé à la délimitation des dix-sept nouveaux cantons du département des Pyrénées-Orientales en se fondant sur une population moyenne de 26 385 habitants et en rapprochant la population de chaque canton de cette moyenne ; que si, comme le soutient la requérante, le canton du Canigou a une population inférieure à la moyenne départementale de 21,92 %, il ressort des pièces du dossier que cet écart limité est justifié par des considérations géographiques, notamment le relief et la topographie de la haute montagne dans ce canton ; que de telles considérations, qui sont dépourvues de caractère arbitraire, n'ont pas conduit, en l'espèce, à méconnaître l'obligation, énoncée au a) du III de l'article L. 3113-2 mentionnée ci-dessus, de définir le territoire de chaque canton sur des bases essentiellement démographiques ; <br/>
<br/>
              11. Considérant, en troisième lieu, que le b) du III de l'article L. 3113-2 du code général des collectivités territoriales se borne à prévoir que le territoire de chaque canton doit être continu ; que, contrairement à ce que soutient la requérante, ces dispositions ne font pas obstacle à ce que certaines communes non limitrophes de la commune de Perpignan soient incluses dans un canton comprenant une partie de cette commune ; que s'il existe un écart de 31,93 % entre le canton le plus peuplé et le canton le moins peuplé de la commune de Perpignan, la nouvelle délimitation des cantons n'a pas eu pour effet d'accroître cet écart ; qu'en conséquence, le moyen tiré de l'incohérence du découpage démographique de la commune de Perpignan doit être écarté ;<br/>
<br/>
              12. Considérant, en quatrième lieu, que ni les dispositions de l'article L. 3113-2 du code général des collectivités territoriales, ni aucun autre texte non plus qu'aucun principe n'imposent au pouvoir réglementaire de prévoir que les limites des cantons, d'une part, prennent en compte la précédente délimitation cantonale, le temps de trajet entre les communes ou leurs topographies respectives ou les prévisions d'évolution des populations, et, d'autre part, s'agissant de circonscriptions électorales, coïncident avec les périmètres des établissements publics de coopération intercommunale ou les limites des " bassins de vie " définis par l'Institut national de la statistique et des études économiques ;<br/>
<br/>
              13. Considérant, en cinquième lieu, que si la requérante critique les choix opérés par le décret attaqué dans la délimitation de certains cantons, et alors qu'il n'est pas contesté que les délimitations retenues respectent les critères définis par l'article L. 3113-2 du code général des collectivités territoriales, elle n'apporte pas d'élément de nature à établir que les choix auxquels il a été procédé reposeraient sur une erreur manifeste d'appréciation ;<br/>
<br/>
              14. Considérant, en sixième et dernier lieu, que le détournement de pouvoir allégué par la requérante, relatif à la délimitation de certains cantons, n'est pas établi ;<br/>
<br/>
              15. Considérant qu'il résulte de tout ce qui précède que Mme B...n'est pas fondée à demander l'annulation pour excès de pouvoir du décret qu'elle attaque ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
      Article 1er : La requête de Mme B...est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à Mme A...B...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
