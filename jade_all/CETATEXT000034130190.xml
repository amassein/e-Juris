<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034130190</ID>
<ANCIEN_ID>JG_L_2017_02_000000399584</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/13/01/CETATEXT000034130190.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 08/02/2017, 399584</TITRE>
<DATE_DEC>2017-02-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>399584</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Stéphane Hoynck</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:399584.20170208</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 5 mai et 14 novembre 2016 au secrétariat du contentieux du Conseil d'Etat, la préfète déléguée de Saint-Barthélemy et de Saint-Martin demande au Conseil d'Etat d'annuler la délibération du conseil territorial de la collectivité de Saint-Martin n° CT27-6a-2016 du 31 mars 2016 portant adaptation des modalités de versement du revenu de solidarité active.<br/>
<br/>
              Vu :<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de l'action sociale et des familles ;<br/>
              - la loi n° 2015-1268 du 14 octobre 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Stéphane Hoynck, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article L.O. 6351-5 du code général des collectivités territoriales   : " I. - Le conseil territorial peut, lorsqu'il y a été habilité à sa demande par la loi ou par le décret, selon le cas, adapter aux caractéristiques et aux contraintes particulières de la collectivité les dispositions législatives ou réglementaires en vigueur. / La demande d'habilitation tendant à adapter une disposition législative ou réglementaire est adoptée par délibération motivée du conseil territorial. / Cette délibération mentionne les dispositions législatives ou réglementaires en cause (...)/  Elle expose les caractéristiques et contraintes particulières justifiant la demande d'habilitation et précise la nature et la finalité des dispositions que le conseil territorial envisage de prendre. ".<br/>
<br/>
              2. Le pouvoir d'adaptation dont dispose le conseil territorial de la collectivité de Saint-Martin sur le fondement de l'article L.O. 6351-5 du code général des collectivités territoriales cité au point 1 ci-dessus autorise l'adoption de dispositions spécifiques au territoire, dans la mesure où l'ampleur de ces adaptations est proportionnée aux caractéristiques particulières de la situation locale, en rapport avec l'objet des textes législatifs ou réglementaires que la collectivité entend adapter.<br/>
<br/>
              3. Par une délibération du 26 juin 2014, le conseil territorial de Saint-Martin a demandé, en application de ces dispositions, à être habilité à adapter les règles législatives en matière de revenu de solidarité active. A cette fin, l'article 83 de la loi du 14 octobre 2015 d'actualisation du droit des outre-mer dispose : " A compter de la promulgation de la présente loi, le conseil territorial de Saint-Martin est habilité, en application des articles LO. 6351-5 à LO. 6351-10 du code général des collectivités territoriales, à adapter les dispositions législatives portant sur le revenu de solidarité active, dans les conditions prévues par la délibération n° CT 18-1-2014 du 26 juin 2014 du conseil territorial de Saint-Martin portant demande d'habilitation en matière de revenu de solidarité active. /Cette habilitation doit permettre au conseil territorial de Saint-Martin d'adapter les conditions d'accès à cette prestation, ses modalités de versement et son montant, pour tenir compte des spécificités du territoire. / Cette habilitation est accordée, conformément à l'article LO. 6351-8 du code général des collectivités territoriales, pour une durée de deux ans. ". <br/>
<br/>
              4. Sur le fondement de cette habilitation législative, le conseil territorial a adopté la délibération du 8 avril 2016 que le préfet délégué de Saint-Barthélemy et de Saint-Martin défère à la censure du Conseil d'Etat en application de l'article LO. 6351-9 du code général des collectivités territoriales.<br/>
<br/>
              5. Aux termes de l'article L. 262-2 du code de l'action sociale et des familles : " Toute personne résidant en France de manière stable et effective, dont le foyer dispose de ressources inférieures à un montant forfaitaire, a droit au revenu de solidarité active dans les conditions définies au présent chapitre. / Le revenu de solidarité active est une allocation qui porte les ressources du foyer au niveau du montant forfaitaire. (...) ". Il résulte de ces dispositions que le revenu de solidarité active a le caractère d'une ressource dont les bénéficiaires ont la libre disposition. <br/>
<br/>
              6. La délibération attaquée, en prévoyant que ces dispositions entrent en vigueur à compter des allocations dues au titre du mois de janvier 2017, complète la liste des éléments que doit préciser la convention prévue à l'article L. 262-25 du code de l'action sociale et des familles, en disposant que le versement du revenu de solidarité active " s'effectue pour partie sous une forme démonétisée garantissant l'utilisation des sommes correspondantes dans un Etat membre de l'UE, dans un autre Etat partie à l'accord sur l'EEE ou de la Confédération suisse " , et en indiquant que " le montant de la partie démonétisée est fixé par délibération du conseil territorial ". <br/>
<br/>
              7. Il ressort des pièces du dossier et notamment des motifs de la délibération attaquée qu'en prévoyant la démonétisation partielle du revenu de solidarité active, la collectivité de Saint-Martin a entendu limiter les territoires dans lesquels les bénéficiaires du revenu de solidarité active seront susceptibles d'effectuer des dépenses à l'aide des ressources octroyées à ce titre. Si la collectivité fait valoir qu'une proportion importante des sommes octroyées à ce titre sont dépensées en dehors du territoire de la collectivité et que cette situation préjudicie à l'économie de Saint-Martin, une telle circonstance ne constitue pas une caractéristique particulière justifiant, au regard de l'objet des dispositions instituant le revenu de solidarité active, de priver les bénéficiaires de la libre disposition de la ressource qui leur est ainsi allouée. <br/>
<br/>
              8. Il suit de là que la collectivité de Saint-Martin est allée, par la délibération litigieuse, au-delà du pouvoir d'adaptation de la loi qu'elle tire de l'article 83 de la loi du 14 octobre 2015 et que le préfet délégué de Saint-Barthélemy et de Saint-Martin est fondé, sans qu'il soit besoin d'examiner les autres moyens de sa requête, à en demander l'annulation. <br/>
<br/>
              9. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La délibération du conseil territorial de la collectivité de Saint-Martin n° CT27-6a-2016 du 31 mars 2016 est annulée.<br/>
Article 2 : Les conclusions présentées par la collectivité de Saint-Martin sur le fondement de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à la Préfète déléguée de Saint-Barthélemy et de Saint-Martin et à la collectivité de Saint-Martin. <br/>
Copie en sera adressée à la ministre des outre-mer. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-02-06 AIDE SOCIALE. DIFFÉRENTES FORMES D'AIDE SOCIALE. REVENU MINIMUM D'INSERTION (RMI). - HABILITATION DU CONSEIL TERRITORIAL DE SAINT-MARTIN À ADAPTER LES DISPOSITIONS LÉGISLATIVES PORTANT SUR LE RSA (ART. 83 DE LA LOI DU 14 OCTOBRE 2015 ) - POSSIBILITÉ DE PRÉVOIR UNE DÉMONÉTISATION PARTIELLE DU RSA DU FAIT D'UNE PROPORTION IMPORTANTE DE BÉNÉFICIAIRES DU RSA DÉPENSANT LES SOMMES OCTROYÉES À CE TITRE EN DEHORS DU TERRITOIRE DE LA COLLECTIVITÉ - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">46-01-02-07 OUTRE-MER. DROIT APPLICABLE. STATUTS. - HABILITATION DU CONSEIL TERRITORIAL DE SAINT-MARTIN À ADAPTER LES DISPOSITIONS LÉGISLATIVES PORTANT SUR LE RSA (ART. 83 DE LA LOI DU 14 OCTOBRE 2015 ) - POSSIBILITÉ DE PRÉVOIR UNE DÉMONÉTISATION PARTIELLE DU RSA DU FAIT D'UNE PROPORTION IMPORTANTE DE BÉNÉFICIAIRES DU RSA DÉPENSANT LES SOMMES OCTROYÉES À CE TITRE EN DEHORS DU TERRITOIRE DE LA COLLECTIVITÉ - ABSENCE.
</SCT>
<ANA ID="9A"> 04-02-06 Article 83 de la loi n° 2015-1268 du 14 octobre 2015 habilitant, dans les conditions prévues par l'article L.O. 6351-5 du code général des collectivités territoriales, le conseil territorial de Saint-Martin à adapter les dispositions législatives relatives au revenu de solidarité active (RSA) aux caractéristiques et contraintes particulières de son territoire.... ,,En prévoyant la démonétisation partielle du RSA, la collectivité de Saint-Martin a entendu limiter les territoires dans lesquels les bénéficiaires du RSA seront susceptibles d'effectuer des dépenses à l'aide des ressources octroyées à ce titre. Si la collectivité fait valoir qu'une proportion importante des sommes octroyées à ce titre sont dépensées en dehors du territoire de la collectivité et que cette situation préjudicie à l'économie de Saint-Martin, une telle circonstance ne constitue pas une caractéristique particulière justifiant, au regard de l'objet des dispositions instituant le RSA, de priver les bénéficiaires de la libre disposition de la ressource qui leur est ainsi allouée. Il suit de là que la collectivité de Saint-Martin est allée, par la délibération litigieuse, au-delà du pouvoir d'adaptation de la loi qu'elle tire de l'article 83 de la loi du 14 octobre 2015.</ANA>
<ANA ID="9B"> 46-01-02-07 Article 83 de la loi n° 2015-1268 du 14 octobre 2015 habilitant, dans les conditions prévues par l'article L.O. 6351-5 du code général des collectivités territoriales, le conseil territorial de Saint-Martin à adapter les dispositions législatives relatives au revenu de solidarité active (RSA) aux caractéristiques et contraintes particulières de son territoire.... ,,En prévoyant la démonétisation partielle du RSA, la collectivité de Saint-Martin a entendu limiter les territoires dans lesquels les bénéficiaires du RSA seront susceptibles d'effectuer des dépenses à l'aide des ressources octroyées à ce titre. Si la collectivité fait valoir qu'une proportion importante des sommes octroyées à ce titre sont dépensées en dehors du territoire de la collectivité et que cette situation préjudicie à l'économie de Saint-Martin, une telle circonstance ne constitue pas une caractéristique particulière justifiant, au regard de l'objet des dispositions instituant le RSA, de priver les bénéficiaires de la libre disposition de la ressource qui leur est ainsi allouée. Il suit de là que la collectivité de Saint-Martin est allée, par la délibération litigieuse, au-delà du pouvoir d'adaptation de la loi qu'elle tire de l'article 83 de la loi du 14 octobre 2015.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
