<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032112587</ID>
<ANCIEN_ID>JG_L_2016_02_000000382910</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/11/25/CETATEXT000032112587.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème SSJS, 26/02/2016, 382910, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-02-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>382910</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>M. Pierre Lombard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2016:382910.20160226</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Le 1er juin 2010, la société American Telegraph et Telephone a demandé au tribunal administratif de Montreuil de condamner l'Etat à lui verser les intérêts moratoires afférents au remboursement du crédit de taxe sur la valeur ajoutée prononcé le 26 août 2009, calculés à compter du 27 juin 2003, assortis des intérêts au taux légal et eux-mêmes capitalisés. Par un jugement n° 1005534 du 2 janvier 2012, le tribunal administratif de Montreuil a rejeté la demande de la société.<br/>
<br/>
              Par un arrêt n° 12VE01614 du 20 mars 2014, la cour administrative d'appel de Versailles a rejeté l'appel formé par la société American Telegraph et Telephone contre le jugement du tribunal administratif de Montreuil.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 21 juillet et 20 octobre 2014 et le 17 décembre 2015 au secrétariat du contentieux du Conseil d'Etat, la société American Telegraph et Telephone demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la directive n° 86/650/CEE du 17 novembre 1986 ;<br/>
              - le code général des impôts, notamment son annexe II ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pierre Lombard, auditeur,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, avocat de la Société American Telegraph et Telephone ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. En vertu des dispositions des articles 242-0 M et 242-0 O de l'annexe II au code général des impôts, dans leur rédaction en vigueur lors de la période du 1er janvier au 31 décembre 2002, un assujetti établi à l'étranger et qui n'avait pas d'établissement stable en France au cours de cette période et n'y avait pas réalisé d'autres livraisons de biens ou prestations de service entrant dans le champ d'application de la taxe sur la valeur ajoutée que, le cas échéant, celles qui étaient énumérées au 2 de l'article 242-0 M, parmi lesquelles figurent les prestations de télécommunications, pouvait, sous conditions, demander le remboursement de la taxe sur la valeur ajoutée ayant grevé des biens livrés ou importés ou des services rendus en vue de la commercialisation en France de produits imposables ou de la réalisation d'opérations mentionnées au 2 de l'article 242-0 M. A...termes de l'article 242-0 Q de la même annexe, dans sa rédaction en vigueur lors de la période litigieuse, une telle demande de remboursement, qui devait être présentée avant la fin du sixième mois suivant l'année civile au cours de laquelle la taxe est devenue exigible, devait être " accompagnée des originaux des factures, des documents d'importation et de toutes pièces justificatives " établissant que les biens avaient été livrés ou importés ou que les services avaient été rendus en vue de la commercialisation en France de produits imposables ou de la réalisation d'opérations mentionnées au 2 de l'article 242-0 M . <br/>
<br/>
              2. Il ressort des pièces du dossier soumis A...juges du fond que, le 27 juin 2003, la société American Telegraph et Telephone, qui n'avait pas d'établissement stable en France, a présenté une demande de remboursement de la taxe sur la valeur ajoutée ayant grevé les prestations de pose et d'entretien de câbles sous-marins réalisées en 2002 sur le territoire français. A l'appui de sa demande de remboursement, la société American Telegraph et Telephone a produit une copie d'un contrat avec la société France Télécom, par lequel les deux sociétés s'engageaient à se fournir mutuellement, pour la période du 1er février au 30 septembre 2002, des prestations de télécommunications sous la forme de la mise à disposition de minutes de téléphonie ainsi qu'une copie des états récapitulatifs (International Traffic Management Settlements) du coût des prestations que se sont rendues les deux sociétés au cours de la période, lesquels font apparaître que le coût des prestations rendues par la société France Télécom l'a emporté sur le coût des prestations rendues par la société American Telegraph et Telephone, de sorte que c'est la société France Télécom qui a émis à destination de la société American Telegraph et Telephone une facture correspondant au coût net des prestations de télécommunications ainsi échangées. Ces documents étaient de nature à établir que les prestations de pose et d'entretien de câbles sous-marins avaient été rendues en vue de la réalisation de prestations de télécommunications. Par suite, en jugeant, par un arrêt suffisamment motivé sur ce point, que la demande de remboursement de la taxe sur la valeur ajoutée qu'elle avait présentée devait être regardée comme incomplète au motif que ces documents n'établissaient pas la réalisation par la société American Telegraph et Telephone de prestations de télécommunications, la cour administrative d'appel a dénaturé les pièces du dossier qui lui était soumis. La société American Telegraph et Telephone est fondée, pour ce motif, à demander l'annulation de l'arrêt attaqué.<br/>
<br/>
              3. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros, à verser à la société American Telegraph et Telephone au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Versailles du 20 mars 2014 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Versailles.<br/>
Article 3 : L'Etat versera à la société American Telegraph et Telephone la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la société American Telegraph et Telephone et au ministre des finances et des comptes publics.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
