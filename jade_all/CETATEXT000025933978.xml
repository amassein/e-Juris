<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025933978</ID>
<ANCIEN_ID>JG_L_2012_05_000000344589</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/93/39/CETATEXT000025933978.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 22/05/2012, 344589, Publié au recueil Lebon</TITRE>
<DATE_DEC>2012-05-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>344589</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP DE CHAISEMARTIN, COURJON</AVOCATS>
<RAPPORTEUR>M. Raphaël Chambon</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CESSR:2012:344589.20120522</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 29 novembre 2010 et 28 février 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. Brice A, demeurant ... ; M. A demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision du 12 novembre 2009 par laquelle la commission des sanctions de l'Autorité des Marchés Financiers a décidé de prononcer à son encontre une sanction pécuniaire de 60 000 euros, assortie d'une mesure de publication au Bulletin des annonces légales obligatoires, ainsi que sur le site internet de l'Autorité des Marchés Financiers et dans le recueil annuel des décisions de la commission des sanctions ;<br/>
<br/>
              2°) subsidiairement, de réformer la décision du 12 novembre 2009 en réduisant la sanction pécuniaire prononcée à 10 000 euros et en supprimant la mesure de publication ;<br/>
<br/>
              3°) de mettre à la charge de l'Autorité des Marchés Financiers la somme de 4 500 euros sur le fondement de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code monétaire et financier ;<br/>
<br/>
              Vu le règlement général de l'Autorité des marchés financiers ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Raphaël Chambon, Maître des Requêtes,<br/>
<br/>
              - les observations de la SCP Rocheteau, Uzan-Sarano, avocat de M. A et de la SCP de Chaisemartin, Courjon, avocat de l'Autorité des Marchés Financiers,<br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Rocheteau, Uzan-Sarano, avocat M. A et à la SCP de Chaisemartin, Courjon, avocat de l'Autorité des Marchés Financiers ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant que, par la décision du 12 novembre 2009 que M. A attaque, la commission des sanctions de l'Autorité des marchés financiers a infligé à ce dernier une sanction pécuniaire de 60 000 euros pour avoir utilisé une information privilégiée relative à l'imminence de l'annonce d'une forte baisse du chiffre d'affaires de la société Infogrames, en vendant à découvert 85 300 actions de cette société en utilisant le compte ouvert au nom de son père avant que cette baisse ne soit rendue publique, puis en les rachetant quelques jours plus tard en réalisant une forte plus-value ; qu'elle a également décidé que sa décision serait publiée au Bulletin des annonces légales obligatoires ainsi que sur le site internet et dans la revue mensuelle de l'Autorité des marchés financiers ;<br/>
<br/>
              Sur la régularité de la procédure et de la décision attaquée :<br/>
<br/>
              Considérant, en premier lieu, qu'il résulte de l'instruction que le collège de l'Autorité des Marchés Financiers (AMF) a notifié les griefs à M. A par lettre du 25 janvier 2008 ; que cette lettre contient l'énoncé des griefs adressés à celui-ci, précisant les considérations de fait comme de droit en constituant le fondement ; qu'elle indique que les manquements en cause sont susceptibles de donner lieu à une sanction sur le fondement des dispositions des articles L. 621-14 et L. 621-15 du code monétaire et financier ; que la circonstance qu'elle ne précise pas au titre de quel paragraphe de ce dernier article une telle sanction pourrait être prise ne constitue pas une irrégularité de nature à vicier la procédure de sanction ;<br/>
<br/>
              Considérant, en second lieu, que, contrairement à ce qui est soutenu, la décision attaquée, qui comporte l'énoncé des considérations de droit et de fait qui constituent le fondement des sanctions prononcées, est suffisamment motivée ;<br/>
<br/>
              Sur le bien-fondé de la décision attaquée :<br/>
<br/>
              En ce qui concerne le fondement juridique de la sanction prononcée :<br/>
<br/>
              Considérant qu'aux termes du II de l'article L. 621-15 du code monétaire et financier, dans sa rédaction applicable : " La commission des sanctions peut, après une procédure contradictoire, prononcer une sanction à l'encontre des personnes suivantes : (...) / b) Les personnes physiques placées sous l'autorité ou agissant pour le compte de l'une des personnes mentionnées aux 1° à 8° et 11° à 15° du II de l'article L. 621-9 au titre de tout manquement à leurs obligations professionnelles définies par les lois, règlements et règles professionnelles approuvées par l'Autorité des marchés financiers en vigueur, sous réserve des dispositions de l'article L. 613-21 ; / c) Toute personne qui, sur le territoire français ou à l'étranger, s'est livrée ou a tenté de se livrer à une opération d'initié ou s'est livrée à une manipulation de cours, à la diffusion d'une fausse information ou à tout autre manquement mentionné au premier alinéa du I de l'article L. 621-14, dès lors que ces actes concernent un instrument financier admis aux négociations sur un marché réglementé ou sur un système multilatéral de négociation qui se soumet aux dispositions législatives ou réglementaires visant à protéger les investisseurs contre les opérations d'initiés, les manipulations de cours et la diffusion de fausses informations, ou pour lequel une demande d'admission aux négociations sur de tels marchés a été présentée, dans les conditions déterminées par le règlement général de l'Autorité des marchés financiers " ; qu'aux termes du III du même article : " Les sanctions applicables sont : (...) / b) Pour les personnes physiques placées sous l'autorité ou agissant pour le compte de l'une des personnes mentionnées aux 1° à 8°, 11°, 12° et 15° du II de l'article L. 621-9, l'avertissement, le blâme, le retrait temporaire ou définitif de la carte professionnelle, l'interdiction à titre temporaire ou définitif de l'exercice de tout ou partie des activités ; la commission des sanctions peut prononcer soit à la place, soit en sus de ces sanctions une sanction pécuniaire dont le montant ne peut être supérieur à 1,5 million d'euros ou au décuple du montant des profits éventuellement réalisés en cas de pratiques mentionnées aux c et d du II ou à 300 000 euros ou au quintuple des profits éventuellement réalisés dans les autres cas (...) ; / c) Pour les personnes autres que l'une des personnes mentionnées au II de l'article L. 621-9, auteurs des faits mentionnés aux c et d du II, une sanction pécuniaire dont le montant ne peut être supérieur à 10 millions d'euros ou au décuple du montant des profits éventuellement réalisés ; les sommes sont versées au Trésor public " ;<br/>
<br/>
              Considérant qu'il résulte de l'instruction que M. A, analyste financier travaillant pour la société Exane et en charge du suivi de la société Infogrames, était au nombre des personnes physiques placées sous l'autorité ou agissant pour le compte de l'une des personnes mentionnées au 1° du II de l'article L. 621-9 du code monétaire et financier ; qu'ainsi, il ne pouvait être sanctionné sur le fondement du c du III de l'article L. 621-15 de ce code ;<br/>
<br/>
              Considérant, toutefois, que lorsque, saisi d'une requête dirigée contre une sanction prononcée par la commission des sanctions de l'AMF, il constate que la décision contestée devant lui aurait pu être prise, en vertu du même pouvoir d'appréciation, sur le fondement d'un autre texte que celui sur lequel s'est fondée la commission des sanctions, le juge administratif peut substituer ce fondement à celui qui a servi de base légale à la décision attaquée, sous réserve que l'intéressé ait disposé des garanties dont est assortie l'application du texte sur le fondement duquel la décision aurait dû être prononcée ; qu'une telle substitution relevant de l'office du juge, celui-ci peut y procéder soit à la demande des parties soit de sa propre initiative, au vu des pièces du dossier, mais sous réserve, dans ce dernier cas, d'avoir au préalable mis les parties à même de présenter des observations sur ce point ;<br/>
<br/>
              Considérant qu'en l'espèce, comme le soutient l'AMF en défense, la sanction attaquée, motivée par le manquement d'initié reproché à M. A, trouve son fondement légal dans les dispositions du b du III du même article L. 621-15 qui peuvent être substituées à celles du c du III dès lors, en premier lieu, que, M. A pouvait être sanctionné sur ce fondement, en deuxième lieu, que cette substitution de base légale n'a pour effet de priver l'intéressé d'aucune garantie et, en troisième lieu, que la commission des sanctions dispose du même pouvoir d'appréciation pour appliquer l'une ou l'autre de ces deux dispositions ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède qu'il y a lieu de faire droit à la substitution de base légale demandée en défense par l'Autorité des marchés financiers ;<br/>
<br/>
              En ce qui concerne la détention et l'utilisation d'une information privilégiée :<br/>
<br/>
              Considérant qu'aux termes de l'article 621-1 du règlement général de l'AMF : " Une information privilégiée est une information précise qui n'a pas été rendue publique, qui concerne, directement ou indirectement, un ou plusieurs émetteurs d'instruments financiers, ou un ou plusieurs instruments financiers, et qui si elle était rendue publique, serait susceptible d'avoir une influence sensible sur le cours des instruments financiers concernés ou le cours d'instruments financiers qui leur sont liés. / Une information est réputée précise si elle fait mention d'un ensemble de circonstances ou d'un événement qui s'est produit ou qui est susceptible de se produire et s'il est possible d'en tirer une conclusion quant à l'effet possible de ces circonstances ou de cet événement sur le cours des instruments financiers concernés ou des instruments financiers qui leur sont liés. / Une information, qui si elle était rendue publique, serait susceptible d'avoir une influence sensible sur le cours des instruments financiers concernés ou le cours d'instruments financiers dérivés qui leur sont liés est une information qu'un investisseur raisonnable serait susceptible d'utiliser comme l'un des fondements de ses décisions d'investissement " ; qu'en vertu des articles 622-1 et 622-2 du même règlement général, toute personne qui détient une information privilégiée en raison de son accès à l'information du fait de son travail, de sa profession ou de ses fonctions " doit s'abstenir d'utiliser l'information privilégiée qu'elle détient en acquérant ou en cédant, ou en tentant d'acquérir ou de céder, pour son propre compte ou pour le compte d'autrui, soit directement soit indirectement, les instruments financiers auxquels se rapporte cette information ou les instruments financiers auxquels ces instruments sont liés " ;<br/>
<br/>
              Considérant qu'il résulte de l'instruction que M. A a eu une conversation téléphonique avec M. B, trésorier de la société Exane, le 7 février 2006 au matin ; que quelques minutes après cette conversation, il a procédé à la vente à découvert, sur le compte inscrit au nom de son père dans les livres de la société Boursorama, de 85 300 titres Infogrames ; que le 9 février au soir la société Infogrames a annoncé une forte baisse de son chiffre d'affaires ; que le lendemain 10 février le cours du titre a fortement baissé ; que M. A a racheté les actions vendues à découvert, dégageant une plus-value estimée à plus de 25 000 euros ;<br/>
<br/>
              Considérant que si M. A soutient que la baisse du chiffre d'affaires d'Infogrames était prévisible dès lors que les difficultés d'Infogrames étaient déjà connues du marché et que, par suite, les informations supposément délivrées par M. B lors de la conversation du 7 février 2006 au matin ne sauraient revêtir un caractère d'information privilégiée, il résulte de l'instruction que ni le montant du chiffre d'affaires pour le troisième trimestre de l'exercice d'Infogrames ni sa baisse par rapport à celui de l'exercice précédent n'étaient connus du public avant la publication d'un communiqué de presse les révélant par Infogrames le 9 février ; que ces informations étaient de nature à influencer le cours du titre en cause ;<br/>
<br/>
              Considérant qu'à défaut de preuve matérielle à l'encontre d'une personne mentionnée aux articles 622-1 et 622-2 du règlement général de l'AMF, la détention d'une information privilégiée peut être établie par un faisceau d'indices concordants, desquels il résulte que seule la détention d'une information privilégiée peut expliquer les opérations litigieuses auxquelles la personne mise en cause a procédé, sans que la commission des sanctions de l'AMF n'ait l'obligation d'établir précisément les circonstances dans lesquelles l'information est parvenue jusqu'à la personne qui l'a utilisée ; que dès lors, et alors que le caractère nécessairement secret et volontairement dissimulé des opérations fautives ne permet généralement pas de disposer de preuves directes à l'encontre des personnes mentionnées aux articles 622-1 et 622-2 de ce règlement, la commission des sanctions pouvait légalement réunir un faisceau d'indices concordants en vue d'établir, à l'égard de M. A, un manquement aux dispositions du règlement général de l'AMF ;<br/>
<br/>
              Considérant que M. A soutient que la commission des sanctions ne disposait pas d'indices précis et concordants pour établir la détention d'une information privilégiée ; que toutefois il ne résulte pas de l'instruction que la commission des sanctions se soit fondée sur des faits matériellement inexacts en estimant que l'entretien téléphonique que M. A a eu avec M. B dans la matinée du 7 février 2006, dans le cadre de la préparation de l'estimation qu'il devait faire de l'évolution du chiffre d'affaires de la société Infogrames, avait eu lieu quelques minutes avant la vente à découvert de titres Infogrames par M. A ; qu'il résulte également de l'instruction que M. A a indiqué qu'il anticipait un chiffre d'affaires de 210 millions d'euros pour le trimestre en cause, soit un chiffre de plus de 39 millions d'euros supérieur au chiffre d'affaires réalisé par la société Infogrames ; qu'au regard de l'écart existant entre l'estimation initiale de M. A et le chiffre d'affaires réalisé, c'est à bon droit que la commission des sanctions a pu estimer que M. B devait être regardé comme ayant donné à M. A une information inattendue et suffisamment précise pour être privilégiée au sens de l'article 621-1 du règlement général de l'AMF alors même que M. B se bornait à affiner les prévisions des analystes financiers avec lesquels il discutait, au nombre desquels figurait M. A, afin de les amener à dire eux-mêmes le chiffre d'affaires qui se rapprochait de la réalité, sans leur donner lui-même d'informations sur le montant exact du chiffre d'affaires ; qu'il résulte enfin de l'instruction que M. A est intervenu sur ce titre, en méconnaissance de l'interdiction qui lui était faite tant par le règlement général de l'AMF que par celui de son employeur, et en utilisant un compte ouvert par son père, sur lequel il n'avait pas procuration ; que la commission des sanctions a pu à bon droit en déduire que l'utilisation par M. A, dans ces circonstances, du compte de son père révélait une volonté de dissimulation ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que la commission des sanctions n'a pas commis d'erreur d'appréciation ni d'erreur de droit en estimant, en l'absence de toute autre explication plausible avancée par le requérant, que seule la détention d'une information privilégiée et son usage prohibé pouvaient expliquer les ventes à découvert du 7 février 2006 sur la valeur Infogrames réalisées par M. A ; que contrairement à ce qui est soutenu, la commission des sanctions n'a pas retenu comme indice de la détention et de l'exploitation d'une information privilégiée la méconnaissance par M. A de ses obligations professionnelles en tant que telle mais seulement la volonté de dissimulation révélée par les circonstances de la vente à découvert à laquelle celui-ci a procédé ;<br/>
<br/>
              En ce qui concerne le montant de la sanction :<br/>
<br/>
              Considérant, en premier lieu, que si les dispositions du code monétaire et financier selon lesquelles le montant d'une sanction peut être fixé en fonction des profits éventuellement réalisés ne permettent de prendre en compte que les profits qui ont été personnellement appréhendés, à un titre ou un autre, par la personne sanctionnée, la commission des sanctions n'a pas commis d'erreur de droit en prenant en compte la plus-value réalisée lors de la vente à découvert par M. A de titres Infogrames sur le compte ouvert au nom de son père, dès lors qu'il résulte de l'instruction que M. A était le véritable gestionnaire de ce compte ;<br/>
<br/>
              Considérant, en deuxième lieu, que si M. A soutient que la commission des sanctions aurait dû prendre en compte le montant net de la plus-value réalisée, c'est-à-dire le montant brut duquel il fallait selon lui retrancher le coût de l'ordre et l'impôt sur la plus-value, il ne résulte pas des dispositions précitées de l'article L. 621-15 du code monétaire et financier que la commission des sanctions aurait dû adopter ce mode de calcul ;<br/>
<br/>
              Considérant, en troisième lieu, qu'il ne résulte pas de l'instruction que la commission des sanctions se soit fondée sur des faits matériellement inexacts pour estimer le montant de la plus-value réalisée à l'occasion de la vente à découvert à laquelle a procédé M. A ;<br/>
<br/>
              Considérant, enfin, qu'en infligeant à M. A une sanction pécuniaire de 60 000 euros et en décidant la publication de sa décision, la commission des sanctions n'a pas, dans les circonstances de l'espèce, infligé de sanctions disproportionnées au regard de la gravité et de la nature des manquements reprochés ;<br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que M. A n'est pas fondé à demander l'annulation ni la réformation de la sanction qui lui a été infligée ; que, par voie de conséquence, ses conclusions tendant au bénéfice des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent qu'être rejetées ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à sa charge le versement à l'Autorité des marchés financiers d'une somme de 2 000 euros au titre des frais exposés par elle et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>				D E C I D E  :<br/>
              				--------------<br/>
<br/>
Article 1er : La requête de M. A est rejetée.<br/>
Article 2 : M. A versera à l'Autorité des marchés financiers une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à M. Brice A et à l'Autorité des Marchés Financiers.<br/>
Copie en sera adressée au ministre de l'économie, des finances et du commerce extérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">13-01-02-01 CAPITAUX, MONNAIE, BANQUES. CAPITAUX. OPÉRATIONS DE BOURSE. AUTORITÉ DES MARCHÉS FINANCIERS. - COMMISSION DES SANCTIONS - RECOURS DE PLEIN CONTENTIEUX - SUBSTITUTION DE BASE LÉGALE - 1) FACULTÉ - A) EXISTENCE - B) CONDITIONS - 2) POSSIBILITÉ POUR LE JUGE D'Y PROCÉDER DE SA PROPRE INITIATIVE - A) EXISTENCE - B) CONDITIONS - OBLIGATION DE METTRE LES PARTIES À MÊME DE PRÉSENTER LEURS OBSERVATIONS [RJ1].
</SCT>
<ANA ID="9A"> 13-01-02-01 1) Lorsqu'il constate qu'une sanction prononcée par la commission des sanctions de l'Autorité des marchés financiers contestée devant lui aurait pu être prise, en vertu du même pouvoir d'appréciation, sur le fondement d'un autre texte que celui sur lequel s'est fondée la commission, a) le juge administratif peut opérer une substitution de base légale b) sous réserve que la personne sanctionnée ait disposé des garanties dont est assortie l'application du texte sur le fondement duquel la décision aurait dû être prononcée. 2) Une telle substitution relevant de l'office du juge, a) celui-ci peut y procéder d'office, b) mais sous réserve, dans ce cas, d'avoir au préalable mis les parties à même de présenter des observations sur ce point.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., en excès de pouvoir, CE, Section, 3 décembre 2003, Préfet de la Seine-Maritime c/ El Bahi, n° 240267, p. 479 ; en matière de sanction contre les médecins, CE, Assemblée, 2 juillet 1993, Milhaud, n° 124960, p. 194 ; en matière de contravention de grande voirie, CE, Section, 20 décembre 1968, Ministre des Postes et Télécommunications/ Mme Daude, p. 681.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
