<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030788012</ID>
<ANCIEN_ID>JG_L_2015_06_000000380422</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/78/80/CETATEXT000030788012.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème SSJS, 24/06/2015, 380422, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-06-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>380422</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>M. Guillaume Odinet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:380422.20150624</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. et Mme B...ont demandé au tribunal administratif de Paris la décharge de la cotisation supplémentaire d'impôt sur le revenu à laquelle ils ont été assujettis au titre de l'année 2005. Par un jugement n° 0903877 du 2 février 2012, le tribunal administratif de Bordeaux a rejeté leur demande. <br/>
<br/>
              Par un arrêt n° 12BX01199 du 18 mars 2014, la cour administrative d'appel de Bordeaux a rejeté l'appel formé par M. et Mme B...contre ce jugement. <br/>
<br/>
              Par un pourvoi et un mémoire complémentaire, un nouveau mémoire et un mémoire en réplique, enregistrés les 19 mai, 19 août et 12 novembre 2014 et le 18 mai 2015 au secrétariat du contentieux du Conseil d'Etat, M. et Mme B...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt du 28 novembre 2013 de la cour administrative d'appel de Bordeaux ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code civil ;<br/>
              - le code de commerce ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - la loi n° 90-1258 du 31 décembre 1990 ; <br/>
              - la loi n° 2004-804 du 9 août 2004 ;<br/>
              - la loi n° 2004-1485 du 30 décembre 2004 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Odinet, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de M. et Mme B...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant, d'une part, qu'aux termes de l'article 3 de la loi du 31 décembre 1990 relative à l'exercice sous forme de sociétés des professions libérales soumises à un statut législatif ou règlementaire ou dont le titre est protégé : "  La société ne peut exercer la ou les professions constituant son objet social qu'après son agrément par l'autorité ou les autorités compétentes ou son inscription sur la liste ou les listes ou au tableau de l'ordre ou des ordres professionnels. (...) L'immatriculation de la société ne peut intervenir qu'après l'agrément de celle-ci par l'autorité compétente ou son inscription sur la liste ou au tableau de l'ordre professionnel " ;<br/>
<br/>
              2. Considérant, d'autre part, qu'aux termes de l'article 238 quaterdecies du code général des impôts, dans sa rédaction issue de l'article 13 de la loi du 9 août 2004 relative au soutien de la consommation et de l'investissement en vigueur jusqu'au 31 décembre 2004 : " I. - Les plus-values soumises au régime des articles 39 duodecies à 39 quindecies et réalisées dans le cadre d'une activité commerciale, industrielle, artisanale ou libérale sont exonérées lorsque les conditions suivantes sont simultanément satisfaites : / 1° Le cédant est soit : / a) Une entreprise dont les résultats sont soumis à l'impôt sur le revenu (...) ; / 2° La cession est réalisée à titre onéreux et porte sur une branche complète d'activité ; / 3° La valeur des éléments de cette branche complète d'activité servant d'assiette aux droits d'enregistrement exigibles en application des articles 719, 720 ou 724 n'excède pas 300 000 euros (...) / / III. - Les dispositions des I et II s'appliquent aux cessions intervenues entre le 16 juin 2004 et le 31 décembre 2005 " ; que le I de l'article 52 de la loi du 30 décembre 2004 de finances rectificatives pour 2004 a restreint à compter du 1er janvier 2005 cette mesure d'exonération en excluant de son bénéfice notamment les situations dans lesquelles le cédant détient, directement ou indirectement, plus de 50 % de droits de vote ou des droits dans les bénéficies sociaux de la société ou exerce en droit ou en fait, directement ou indirectement, la direction effective de la société ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un acte du 1er octobre 2004, M.B..., médecin anesthésiste, a cédé la clientèle ainsi que les autres éléments incorporels et corporels de son cabinet à la société d'exercice libéral à responsabilité limitée (SELARL) Anesthésistes Saint-Antoine dont il était le co-gérant ; qu'il a entendu bénéficier de l'exonération, prévue par l'article 238 quaterdecies du code général des impôts dans sa rédaction en vigueur jusqu'au 31 décembre 2004, de la plus-value de cession qu'il a réalisée à cette occasion ; que l'administration fiscale a remis en cause l'application du régime d'exonération au motif que la cession n'est intervenue qu'à la date à laquelle elle a été agréée par le conseil de l'ordre des médecins, le 3 mars 2005, et qu'à cette date s'appliquaient les restrictions prévues par la loi de finances rectificative pour 2004 dans le champ desquelles entrait la cession ; que l'imposition litigieuse procède de cette remise en cause ;<br/>
<br/>
              4. Considérant qu'il ressort des énonciations de l'arrêt attaqué que, pour prononcer la décharge de cette imposition, la cour administrative d'appel a relevé que, conformément aux prescriptions du code de la santé publique, l'article 9 de l'acte de cession du 1er octobre 2004 prévoyait la communication de cet acte à l'ordre des médecins pour agrément et que les statuts de la société cessionnaire subordonnait sa constitution à l'agrément de l'ordre des médecins ; que cet agrément n'a été délivré que le 3 mars 2005 ; que la cour a en outre mentionné que la circonstance que M. B... aurait exercé à compter du 1er octobre 2004 son activité dans le cadre d'une société de fait était sans incidence sur le fait que la cession n'aurait pu intervenir avant le 3 mars 2005 ;<br/>
<br/>
              5. Considérant que si, en vertu des dispositions de l'article 1583 du code civil, la vente est parfaite entre les parties et la propriété acquise de droit à l'acheteur à l'égard du vendeur dès lors qu'on est convenu de la chose et du prix, l'opération de cession en cause n'est devenue opposable à l'administration fiscale en vue de l'application des dispositions de l'article 238 quaterdecies du code général des impôts qu'à compter de l'accomplissement de la formalité d'agrément de la société cessionnaire par le conseil de l'ordre des médecins, conformément aux dispositions de l'article 3 de la loi du 31 décembre 1990 ; qu'il suit de là qu'en statuant comme il est dit au point 4, la cour administrative d'appel, qui a suffisamment motivé son arrêt, n'a pas commis d'erreur de droit ; que M. et Mme B...ne sont donc pas fondés à demander l'annulation de l'arrêt qu'ils attaquent ;<br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. et Mme B...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. et Mme A...B...et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
