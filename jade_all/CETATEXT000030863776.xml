<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030863776</ID>
<ANCIEN_ID>JG_L_2015_07_000000388466</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/86/37/CETATEXT000030863776.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère SSJS, 09/07/2015, 388466, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-07-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>388466</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Frédéric Puigserver</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:388466.20150709</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              La Commission nationale des comptes de campagne et des financements politiques a saisi, sur le fondement de l'article L. 52-15 du code électoral, le tribunal administratif de Versailles de la décision du 10 novembre 2014 par laquelle elle a rejeté le compte de campagne de M. A...B..., candidat tête de liste lors des élections municipales et communautaires de Longjumeau (Essonne) organisées les 23 et 30 mars 2014.<br/>
<br/>
              Par un jugement n° 1408121 du 3 février 2015, le tribunal administratif de Versailles a rejeté sa saisine et fixé à dix mille cinquante euros le montant du remboursement dû par l'Etat à M. B...en application de l'article L. 52-11-1 du code électoral.<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 4 mars et 27 mai 2015 au secrétariat du contentieux du Conseil d'Etat, la Commission nationale des comptes de campagne et des financements politiques demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement du tribunal administratif de Versailles du 3 février 2015 ; <br/>
<br/>
              2°) de dire que le compte de campagne de M. B...a été rejeté à bon droit et de statuer sur l'éligibilité de M. B...au titre des dispositions de l'article L. 118-3 du code électoral.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Puigserver, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Par sa décision du 10 novembre 2014, la Commission nationale des comptes de campagne et des financements politiques a rejeté le compte de campagne de M.B..., candidat tête de liste aux élections municipales et communautaires organisées les 23 et 30 mars 2014 dans la commune de Longjumeau, au motif qu'un concours en nature lui avait été consenti par une personne morale autre qu'un parti ou groupement politique. <br/>
<br/>
              2. Selon le deuxième alinéa de l'article L. 52-8 du code électoral : " Les personnes morales, à l'exception des partis ou groupements politiques, ne peuvent participer au financement de la campagne électorale d'un candidat, ni en lui consentant des dons sous quelque forme que ce soit, ni en lui fournissant des biens, services ou autres avantages directs ou indirects à des prix inférieurs à ceux qui sont habituellement pratiqués ". Le premier alinéa de l'article L. 52-12 du même code dispose que : " Chaque candidat ou candidat tête de liste soumis au plafonnement prévu à l'article L. 52-11 et qui a obtenu au moins 1 % des suffrages exprimés est tenu d'établir un compte de campagne retraçant, selon leur origine, l'ensemble des recettes perçues et, selon leur nature, l'ensemble des dépenses engagées ou effectuées en vue de l'élection, hors celles de la campagne officielle par lui-même ou pour son compte, au cours de la période mentionnée à l'article L. 52-4 (...) Le candidat estime et inclut, en recettes et en dépenses, les avantages directs ou indirects, les prestations de services et dons en nature dont il a bénéficié (...) ". Enfin, selon l'article L. 52-15 du même code : " La Commission nationale des comptes de campagne et des financements politiques approuve et, après procédure contradictoire, rejette ou réforme les comptes de campagne. (...) Lorsque la commission a constaté que le compte de campagne n'a pas été déposé dans le délai prescrit, si le compte a été rejeté ou si, le cas échéant après réformation, il fait apparaître un dépassement du plafond des dépenses électorales, la commission saisit le juge de l'élection (...) ".<br/>
<br/>
              3. Les dispositions précitées de l'article L. 52-8 du code électoral ont pour effet d'interdire aux personnes morales, qu'il s'agisse de personnes publiques ou de personnes morales de droit privé, à l'exception des partis ou groupements politiques, de consentir à un candidat des dons en nature ou en espèces, sous quelque forme et de quelque montant que ce soit. Toutefois, ni l'article L. 52-15 de ce code ni aucune autre disposition législative n'obligent la Commission nationale des comptes de campagne et des financements politiques à rejeter le compte d'un candidat faisant apparaître qu'il a bénéficié de la part de personnes morales d'un avantage prohibé par l'article L. 52-8. Il lui appartient, sous le contrôle du juge de l'élection, d'apprécier si, compte tenu notamment des circonstances dans lesquelles le don a été consenti et de son montant, sa perception doit entraîner le rejet du compte et la saisine du juge de l'élection afin qu'il prononce, le cas échéant, et par application de l'article L. 118-3 du code électoral, l'inéligibilité du candidat concerné.<br/>
<br/>
              4. Il résulte de l'instruction que la société civile immobilière Parclé, dont les deux seuls associés sont des personnes physiques, a mis à disposition de M.B..., à titre gracieux, un local de vingt mètres carrés lui appartenant pour une durée de quatre mois, en vue de la campagne électorale. M. B... a ainsi bénéficié, de la part d'une personne morale de droit privé autre qu'un parti ou groupement politique, d'un avantage en nature prohibé par les dispositions citées ci-dessus de l'article L. 52-8 du code électoral. Toutefois, il est constant que cet avantage doit être évalué à un montant de mille deux cents euros, soit 3,9 % du plafond des dépenses autorisées. Eu égard à son montant limité et aux circonstances dans lesquelles il a été consenti, cet avantage n'est pas, à lui seul, de nature à justifier le rejet par la Commission nationale des comptes de campagne et des financements politiques du compte de campagne de M. B....<br/>
<br/>
              5. Il résulte de ce qui précède que la Commission nationale des comptes de campagne et des financements politiques n'est pas fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Versailles a rejeté sa saisine.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la Commission nationale des comptes de campagne et des financements politiques est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la Commission nationale des comptes de campagne et des financements politiques, à M. A...B...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
