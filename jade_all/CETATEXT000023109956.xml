<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000023109956</ID>
<ANCIEN_ID>JG_L_2010_11_000000316587</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/23/10/99/CETATEXT000023109956.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 17/11/2010, 316587, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2010-11-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>316587</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Arrighi de Casanova</PRESIDENT>
<AVOCATS>SCP BOUZIDI, BOUHANNA</AVOCATS>
<RAPPORTEUR>Mme Anne  Egerszegi</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Geffray Edouard</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 28 mai et 13 août 2008 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. Georges A, demeurant ... ; M. A demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt du 27 mars 2008 par lequel la cour administrative d'appel de Versailles a rejeté l'appel qu'il a interjeté du jugement du 24 avril 2007 du tribunal administratif de Cergy-Pontoise ayant rejeté sa demande tendant à la décharge des suppléments d'impôt sur le revenu et de contributions sociales auxquels il avait été assujetti au titre de l'année 1997 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anne Egerszegi, Maître des Requêtes,<br/>
<br/>
              - les observations de la SCP Bouzidi, Bouhanna, avocat de M. A,<br/>
<br/>
              - les conclusions de M. Edouard Geffray, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Bouzidi, Bouhanna, avocat de M. A ; <br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la Sarl Immobilière Parisienne GE, qui exerce l'activité de marchand de biens et dont M. A est le gérant, a acquis le 25 avril 1990 un appartement situé 38, quai d'Orléans à Paris pour un prix de 23 517 649 francs, dans lequel elle a entrepris d'importants travaux de rénovation, jusqu'en 1992, d'un montant total de 7 130 953 francs, dont 756 590 francs pour l'installation d'équipements de cuisine (appareils électro-ménagers et mobilier de cuisine) et de matériels audiovisuels ; qu'à compter de l'année 1992, l'appartement a été loué à M. A pour un loyer mensuel de 45 000 francs, puis revendu à celui-ci en 1997 au prix de 16 000 000 francs ; qu'à la suite d'une vérification de comptabilité portant sur les années 1995 à 1997, l'administration fiscale a estimé que la société Immobilière Parisienne GE était sans intérêt à engager les dépenses d'équipement ci-dessus mentionnées et que, par suite, celles-ci constituaient une charge anormale qui n'aurait pas dû venir grever le coût de revient de l'immeuble ; qu'elle a ainsi rejeté la valorisation des travaux en cause lors de la cession du bien en 1997 et regardé la somme de 756 590 francs comme une libéralité consentie par la société au profit de son gérant, imposable sur le fondement du 2° du 1 de l'article 109 du code général des impôts ; que M. A se pourvoit en cassation contre l'arrêt du 27 mars 2008 par lequel la cour administrative d'appel de Versailles a confirmé le jugement du 24 avril 2007 du tribunal administratif de Cergy-Pontoise rejetant sa demande tendant à la décharge des impositions supplémentaires consécutives à ce redressement ;<br/>
<br/>
              Sans qu'il soit besoin d'examiner les autres moyens du pourvoi ; <br/>
<br/>
              Considérant que, lorsqu'une résidence appartenant à une société est mise à la disposition d'un dirigeant puis vendue à ce dernier, les dépenses afférentes à ce bien relèvent d'une gestion normale et, par suite, ne peuvent être regardées comme une libéralité imposable entre les mains du dirigeant si un loyer normal a été versé à la société par celui-ci au titre de la jouissance de ce bien et si sa cession ultérieure a été effectuée à un prix normal ; que, par suite, en jugeant que devait être regardé comme une libéralité, imposable entre les mains de M. A dans la catégorie des revenus de capitaux mobiliers, le montant des dépenses d'équipement mobilier engagées par la société Immobilière Parisienne GE à l'occasion de la rénovation de l'appartement en cause, au seul motif que celui-ci avait été acquis par la société pour l'usage personnel et exclusif de son gérant, sans rechercher si le loyer versé par celui-ci au titre de la jouissance de ce bien, puis le prix versé pour son acquisition ultérieure étaient susceptibles de constituer, pour la société, la contrepartie normale des dépenses litigieuses, la cour administrative d'appel de Versailles a commis une erreur de droit ; que par suite, M. A est fondé à demander l'annulation de l'arrêt qu'il attaque ; <br/>
<br/>
              Considérant qu'il y lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              Considérant qu'il résulte de l'instruction que l'administration fiscale ne conteste pas le caractère normal du montant de loyer versé par M. A, gérant de la société Immobilière Parisienne GE, pour l'occupation de l'appartement dont elle était propriétaire, ni le caractère normal du prix auquel la société a cédé cet appartement à l'intéressé en 1997 ; que, dès lors qu'il existait une contrepartie propre à justifier l'intérêt de la société, les dépenses d'équipement engagées par celle-ci pour l'aménagement de ce bien n'avaient pas le caractère d'un acte anormal de gestion, constitutif d'une libéralité au profit du gérant ; que, par suite, l'administration fiscale ne pouvait regarder le montant de ces dépenses comme un revenu distribué entre les mains de M. A, imposable dans la catégorie des revenus de capitaux mobiliers sur le fondement des dispositions du 2° du 1 de l'article 109 du code général des impôts ; que, dès lors, M. A est fondé à soutenir que c'est à tort que, par le jugement attaqué du 24 avril 2007, le tribunal administratif de Cergy-Pontoise a rejeté sa demande tendant à la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles il a été assujetti au titre de l'année 1997 ; <br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de faire application des dispositions de l'article L. 761-1 du code de justice administrative  et de mettre à la charge de l'Etat le versement à M. A de la somme de 6 000 euros, au titre des frais exposés tant devant le Conseil d'Etat que devant les juges du fond ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Versailles du 27 mars 2008 et le jugement du tribunal administratif de Cergy-Pontoise du 24 avril 2007 sont annulés.<br/>
Article 2 : M. A est déchargé des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles il a été assujetti au titre de l'année 1997.<br/>
Article 3 : L'Etat versera à M. A la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. Georges A et au ministre du budget, des comptes publics, de la fonction publique et de la réforme de l'Etat, porte-parole du Gouvernement.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
