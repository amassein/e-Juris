<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034310629</ID>
<ANCIEN_ID>JG_L_2017_03_000000407057</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/31/06/CETATEXT000034310629.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 29/03/2017, 407057, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-03-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>407057</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Laurent Domingo</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:407057.20170329</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 23 janvier 2017 au secrétariat du contentieux du Conseil d'Etat, la société par actions simplifiée Sintex France demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le deuxième tiret du paragraphe n° 60 de l'instruction BOI-IS-AUT-30-20140306. <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code général des impôts ;<br/>
              - la loi n° 2012-958 du 16 août 2012 ;<br/>
              - la loi n° 2015-1786 du 29 décembre 2015 ;<br/>
              - le code de justice administrative ;		<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Domingo, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du I de l'article 235 ter ZCA du code général des impôts, dans sa rédaction antérieure à la loi du 29 décembre 2015 de finances rectificative pour 2015 : " Les sociétés ou organismes français ou étrangers passibles de l'impôt sur les sociétés en France, à l'exclusion des organismes de placement collectif mentionnés au II de l'article L. 214-1 du code monétaire et financier ainsi que de ceux qui satisfont à la définition des micro, petites et moyennes entreprises donnée à l'annexe I au règlement (CE) n° 800/2008 de la Commission du 6 août 2008 déclarant certaines catégories d'aides compatibles avec le marché commun en application des articles 87 et 88 du traité (Règlement général d'exemption par catégorie), sont assujettis à une contribution additionnelle à cet impôt au titre des montants qu'ils distribuent au sens des articles 109 à 117 du présent code (...) ".<br/>
<br/>
              2. La société Sintex France, qui est redevable de la contribution prévue par les dispositions précitées de l'article 235 ter ZCA, demande l'annulation pour excès de pouvoir du § 60 de l'instruction BOI-IS-AUT-30-20140306, par laquelle l'administration a fait connaître son interprétation de ces dispositions, en ce qu'il indique que " sont exonérées de la contribution additionnelle : / (...) - les personnes morales qui satisfont à la définition des micro, petites et moyennes entreprises donnée à l'annexe I au règlement (CE) n° 800/2008 de la Commission du 6 août 2008 déclarant certaines catégories d'aides compatibles avec le marché commun en application des articles 87 et 88 du traité (Règlement général d'exemption par catégorie) ", et qui ajoute que " sur la qualité d'entreprise au sens communautaire, il convient notamment de se reporter au II-A-1 § 60 à 140 du BOI-BIC-RICI-10-10-50 ".<br/>
<br/>
              3. Au soutien de sa requête, la société Sintex France soulève un moyen unique, présenté dans un mémoire distinct, tiré de l'absence de conformité aux droits et libertés garantis par la Constitution des mots " ainsi que de ceux qui satisfont à la définition des micro, petites et moyennes entreprises donnée à l'annexe I au règlement (CE) n° 800/2008 de la Commission du 6 août 2008 déclarant certaines catégories d'aides compatibles avec le marché commun en application des articles 87 et 88 du traité (Règlement général d'exemption par catégorie) " figurant au premier alinéa du I de l'article 235 ter ZCA du code général des impôts.<br/>
<br/>
              4. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              5. La société Sintex France soutient que les mots précités du premier alinéa du I de l'article 235 ter ZCA du code général des impôts sont contraires aux principes d'égalité devant la loi et d'égalité devant les charges publiques garantis par les articles 6 et 13 de la Déclaration de 1789.<br/>
<br/>
              6. D'une part, le principe d'égalité ne s'oppose ni à ce que le législateur règle de façon différente des situations différentes, ni à ce qu'il déroge à l'égalité pour des raisons d'intérêt général, pourvu que dans l'un et l'autre cas, la différence de traitement qui en résulte soit en rapport direct avec l'objet de la loi qui l'établit. D'autre part, en vertu de l'article 34 de la Constitution, il appartient au législateur de déterminer, dans le respect des principes constitutionnels et compte tenu des caractéristiques de chaque impôt, les règles selon lesquelles doivent être appréciées les facultés contributives. En particulier, pour assurer le respect du principe d'égalité, il doit fonder son appréciation sur des critères objectifs et rationnels en fonction des buts qu'il se propose. Cette appréciation ne doit cependant pas entraîner de rupture caractérisée de l'égalité devant les charges publiques garantie par l'article 13 de la  Déclaration de 1789.<br/>
<br/>
              7. La société Sintex France fait valoir que la différence de traitement entre les sociétés qui remplissent les critères de définition des micro, petites et moyennes entreprises donnée à l'annexe I au règlement (CE) n° 800/2008 du 6 août 2008 et celles qui ne remplissent pas l'un de ces critères et dont les distributions se trouvent ainsi soumises à la contribution prévue par l'article 235 ter ZCA du code général des impôts, alors même qu'elles distribuent le même montant de dividendes, n'est pas justifiée par une différence de situation entre ces sociétés au regard de l'objet de la contribution, ni par un motif d'intérêt général. Toutefois, lors de l'adoption de l'article 235 ter ZCA du code général des impôts, le législateur a entendu prendre en considération les difficultés rencontrées par les micro, petites et moyennes entreprises pour accéder à des financements en capital. En excluant ces entreprises du champ de la contribution instaurée par ces dispositions, afin de ne pas accroître ces difficultés d'accès par une imposition de 3% des distributions auxquelles ces entreprises procèdent, le législateur a tenu compte de la situation différente dans laquelle ces entreprises se trouvent, au regard de l'objet de l'imposition instituée par l'article 235 ter ZCA, par rapport aux autres sociétés passibles de l'impôt sur les sociétés. Par suite, les dispositions critiquées du premier alinéa du I de l'article 235 ter ZCA du code général des impôts ne méconnaissent pas le principe d'égalité devant la loi. Ces dispositions n'entraînent pas non plus de rupture caractérisée de l'égalité devant les charges publiques.<br/>
<br/>
              8. Il résulte de ce qui précède que la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux. Par suite, il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée. La société Sintex France ne présentant aucun autre moyen à l'appui de sa requête, celle-ci doit donc être rejetée.<br/>
<br/>
              9. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
                               D E C I D E :<br/>
                                --------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la société Sintex France.<br/>
<br/>
Article 2 : La requête de la société Sintex France est rejetée.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la société par actions simplifiée Sintex France et au ministre de l'économie et des finances.<br/>
Copie en sera adressée au Conseil constitutionnel et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
