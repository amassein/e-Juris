<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032739893</ID>
<ANCIEN_ID>JG_L_2016_06_000000386978</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/73/98/CETATEXT000032739893.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 1ère chambres réunies, 20/06/2016, 386978</TITRE>
<DATE_DEC>2016-06-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>386978</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP MATUCHANSKY, VEXLIARD, POUPOT</AVOCATS>
<RAPPORTEUR>M. Didier Ribes</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:386978.20160620</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C...F..., la société Nawak et Ventilo, M. A...B...et Mme E... G...D...ont demandé au tribunal administratif de Montreuil l'annulation pour excès de pouvoir de l'arrêté du 21 mars 2011 et de l'arrêté du 21 mai 2012, rectifié le 13 septembre 2012, par lesquels le maire de Saint-Denis (Seine-Saint-Denis) a accordé à la société Logis Transports un permis de construire un immeuble de vingt logements et des équipements techniques liés au tramway, puis un permis de construire modificatif tendant notamment à la modification de l'alignement de la façade, à la ventilation haute de locaux techniques de la RATP et à des accès à ces locaux techniques. Par un jugement nos 1104157, 1206056 du 7 février 2013, le tribunal administratif de Montreuil a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 13VE01115 du 23 octobre 2014, la cour administrative d'appel de Versailles a rejeté l'appel formé contre ce jugement par M. F...et autres.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire, deux nouveaux mémoires et un mémoire en réplique, enregistrés les 7 janvier, 7 avril, 18 mai, et 11 septembre 2015 et le 23 mai 2016 au secrétariat du contentieux du Conseil d'Etat, M. F...et autres demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Saint-Denis la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier Ribes, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de M. F...et autres, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la commune de Saint-Denis et à la SCP Matuchansky, Vexliard, Poupot avocat de la société Logis Transports ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 21 mars 2011 et un arrêté du 21 mai 2012, rectifié le 13 septembre 2012, le maire de Saint-Denis (Seine-Saint-Denis) a accordé à la société Logis Transports, filiale immobilière de la RATP, un permis de construire un immeuble de vingt logements et un poste de redressement électrique de la RATP, équipement technique lié au tramway, puis un permis de construire modificatif ; que le projet litigieux occupe la totalité d'une parcelle qui appartenait au domaine public communal et avait été grevée d'une servitude d'emplacement réservé par le plan d'occupation des sols de la commune pour la réalisation d'un poste de redressement de la RATP ; que cette parcelle a fait l'objet d'un déclassement du domaine public communal puis a été cédée par la commune à la RATP ; que, par un jugement du 7 février 2013, le tribunal administratif de Montreuil a rejeté les demandes de M.F..., de M.B..., de Mme D... et de la société Nawak et Ventilo tendant à l'annulation pour excès de pouvoir de ces arrêtés ; que, par un arrêt du 29 octobre 2014 contre lequel M. F...et autres se pourvoient en cassation, la cour administrative d'appel de Versailles a rejeté leur appel contre ce jugement ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 123-1-5 du code de l'urbanisme, dans sa rédaction alors en vigueur : " Le règlement [du plan local d'urbanisme] fixe, en cohérence avec le projet d'aménagement et de développement durables, les règles générales et les servitudes d'utilisation des sols permettant d'atteindre les objectifs mentionnés à l'article L. 121-1, qui peuvent notamment comporter l'interdiction de construire, délimitent les zones urbaines ou à urbaniser et les zones naturelles ou agricoles et forestières à protéger et définissent, en fonction des circonstances locales, les règles concernant l'implantation des constructions. / À ce titre, le règlement peut : (...) / 8° Fixer les emplacements réservés aux voies et ouvrages publics, aux installations d'intérêt général ainsi qu'aux espaces verts. (...) " ; qu'en contrepartie de cette servitude, le propriétaire concerné par un emplacement réservé bénéficie, en vertu de l'article L. 123-17 du code de l'urbanisme, dans sa rédaction alors applicable, d'un droit de délaissement lui permettant d'exiger de la collectivité publique au bénéfice de laquelle le terrain a été réservé qu'elle procède à son acquisition, dans les conditions fixées par les articles L. 230-1 et suivants du même code, faute de quoi les limitations au droit à construire et la réserve ne sont plus opposables ;<br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions que l'autorité administrative chargée de délivrer le permis de construire est tenue de refuser toute demande, même émanant de la personne bénéficiaire de la réserve, dont l'objet ne serait pas conforme à la destination de l'emplacement réservé, tant qu'aucune modification du plan local d'urbanisme emportant changement de la destination n'est intervenue ; qu'en revanche, un permis de construire portant à la fois sur l'opération en vue de laquelle l'emplacement a été réservé et sur un autre projet peut être légalement délivré, dès lors que ce dernier projet est compatible avec la destination assignée à l'emplacement réservé ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède qu'en jugeant, pour écarter le moyen tiré de l'illégalité du permis de construire autorisant la construction d'un immeuble comprenant le poste de redressement en vue duquel l'emplacement avait été réservé et vingt logements, qu'aucune disposition n'interdisait de réaliser sur la même parcelle d'autres projets compatibles avec la destination qui lui était assignée, la cour n'a pas commis d'erreur de droit ;<br/>
<br/>
              5. Considérant que les requérants soutiennent, pour le surplus, en premier lieu, que la cour administrative d'appel de Versailles a entaché son arrêt d'insuffisance de motivation et de dénaturation des pièces du dossier en se bornant à affirmer la compatibilité des logements avec le poste de redressement électrique, en deuxième lieu, que la cour a entaché son arrêt d'erreur de droit en ne relevant pas le détournement de procédure commis par le maire de la commune, en troisième lieu, que la cour a insuffisamment motivé son arrêt en ne répondant pas aux moyens tirés de ce que le terrain d'assiette du projet appartenait encore au domaine public lorsque le permis a été accordé, de l'absence d'habilitation du pétitionnaire et de l'absence de consultation de l'autorité gestionnaire de la voie publique ainsi que du service des domaines, et, en dernier lieu, que la cour a entaché son arrêt d'erreur de droit et de dénaturation des pièces du dossier en se bornant à constater qu'un arrêté avait désaffecté et déclassé la parcelle sans rechercher si celle-ci avait fait l'objet d'une désaffectation matérielle avant l'octroi par le maire du permis de construire des logements sociaux ;<br/>
<br/>
              6. Considérant qu'aucun de ces moyens n'est de nature à justifier l'annulation de l'arrêt attaqué ;<br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que M.F..., M. B..., Mme D...et la société Nawak et Ventilo ne sont pas fondés à demander l'annulation de l'arrêt attaqué ; que leurs conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ; qu'en revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M.F..., de M.B..., de Mme D...et de la société Nawak et Ventilo la somme de 500 euros à verser chacun, d'une part, à la commune de Saint-Denis et, d'autre part, à la société Logis Transports, au titre des mêmes dispositions ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
      Article 1er : Le pourvoi de M. F...et autres est rejeté.<br/>
<br/>
Article 2 : M.F..., M.B..., Mme D...et la société Nawak et Ventilo verseront chacun une somme de 500 euros, d'une part, à la commune de Saint-Denis et, d'autre part, à la société Logis Transports, au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. C...F..., premier requérant dénommé, à la commune de Saint-Denis et à la société Logis Transports. Les autres requérants seront informés de la présente décision par la SCP Waquet, Farge, Hazan, avocat au Conseil d'Etat et à la Cour de cassation, qui les représente devant le Conseil d'Etat.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-04-01-04 DROITS CIVILS ET INDIVIDUELS. DROIT DE PROPRIÉTÉ. SERVITUDES. EFFETS DE L'ÉTABLISSEMENT DES SERVITUDES. - SERVITUDE D'EMPLACEMENT RÉSERVÉ (8° DE L'ART. L. 123-1-5 DU CODE DE L'URBANISME) - POSSIBILITÉ DE DÉLIVRER UN PERMIS PORTANT À LA FOIS SUR L'OPÉRATION JUSTIFIANT LA SERVITUDE ET UN AUTRE PROJET COMPATIBLE - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-01-01-02-02-17 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PLANS D'AMÉNAGEMENT ET D'URBANISME. PLANS D`OCCUPATION DES SOLS (POS) ET PLANS LOCAUX D'URBANISME (PLU). APPLICATION DES RÈGLES FIXÉES PAR LES POS OU LES PLU. RÈGLES DE FOND. SERVITUDES D'UTILITÉ PUBLIQUE AFFECTANT L'UTILISATION DU SOL. - SERVITUDE D'EMPLACEMENT RÉSERVÉ (8° DE L'ART. L. 123-1-5 DU CODE DE L'URBANISME) - POSSIBILITÉ DE DÉLIVRER UN PERMIS PORTANT À LA FOIS SUR L'OPÉRATION JUSTIFIANT LA SERVITUDE ET UN AUTRE PROJET COMPATIBLE - EXISTENCE.
</SCT>
<ANA ID="9A"> 26-04-01-04 Servitude d'emplacement pour les voies et ouvrages publics, les installations d'intérêt général et les espaces verts (8° de l'article L. 123-1-5 du code de l'urbanisme).... ,,L'autorité administrative chargée de délivrer le permis de construire est tenue de refuser toute demande, même émanant de la personne bénéficiaire de la réserve, dont l'objet ne serait pas conforme à la destination de l'emplacement réservé, tant qu'aucune modification du plan local d'urbanisme emportant changement de la destination n'est intervenue. En revanche, un permis de construire portant à la fois sur l'opération en vue de laquelle l'emplacement a été réservé et sur un autre projet peut être  légalement délivré, dès lors que ce dernier projet est compatible avec la destination assignée à l'emplacement réservé.</ANA>
<ANA ID="9B"> 68-01-01-02-02-17 Servitude d'emplacement pour les voies et ouvrages publics, les installations d'intérêt général et les espaces verts (8° de l'article L. 123-1-5 du code de l'urbanisme).... ,,L'autorité administrative chargée de délivrer le permis de construire est tenue de refuser toute demande, même émanant de la personne bénéficiaire de la réserve, dont l'objet ne serait pas conforme à la destination de l'emplacement réservé, tant qu'aucune modification du plan local d'urbanisme emportant changement de la destination n'est intervenue. En revanche, un permis de construire portant à la fois sur l'opération en vue de laquelle l'emplacement a été réservé et sur un autre projet peut être  légalement délivré, dès lors que ce dernier projet est compatible avec la destination assignée à l'emplacement réservé.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
