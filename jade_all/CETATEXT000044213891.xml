<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044213891</ID>
<ANCIEN_ID>JG_L_2021_10_000000451835</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/21/38/CETATEXT000044213891.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 15/10/2021, 451835, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-10-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>451835</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP OHL, VEXLIARD ; SCP BORE, SALVE DE BRUNETON, MEGRET ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Rozen Noguellou</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier  Fuchs</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:451835.20211015</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par deux mémoires, enregistrés le 19 juillet 2021 au secrétariat du contentieux du Conseil d'État, la société Mazars et M. A... H... demandent au Conseil d'État, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 et à l'appui de leur requête tendant à l'annulation de la décision du Haut conseil du commissariat aux comptes du 19 février 2021 prononçant à l'encontre de la société Mazars une interdiction d'exercer la fonction de commissaire aux comptes pendant douze mois assortie du sursis pour la totalité de sa durée ainsi qu'une sanction pécuniaire de 400 000 euros et à l'encontre de M. H... une interdiction d'exercer la fonction de commissaire aux comptes pendant dix-huit mois, assortie du sursis pour la totalité de sa durée ainsi qu'une sanction pécuniaire de 50 000 euros, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des articles L. 821-1, L. 821-2, L. 821-3-1 al. 1, L. 824-5, L. 824-8, al. 1 et 3, et L. 824-11, al. 7 et 8 du code de commerce. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ; <br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - les articles L. 821-1, L. 821-2, L. 821-3-1 al. 1, L. 824-5, L. 824-8, al. 1 et 3, et L. 824-11, al. 7 et 8 du code de commerce ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Rozen Noguellou, conseillère d'Etat,  <br/>
<br/>
              - les conclusions de M. Olivier Fuchs, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Boré, Salve de Bruneton, Mégret, avocat de la société Mazars et autre, à la SCP Ohl, Vexliard, avocat du Haut conseil du commissariat aux comptes et à la SCP Piwnica, Molinié, avocat de la société Pricewaterhousecoopers Audit SAS et autres ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. La société Mazars et M. H... soulèvent, à l'appui des conclusions de leur requête tendant à l'annulation de la décision du Haut conseil du commissariat aux comptes du 19 février 2021 prononçant à l'encontre de la société Mazars une interdiction d'exercer la fonction de commissaire aux comptes pendant douze mois assortie du sursis pour la totalité de sa durée et une sanction pécuniaire de 400 000 euros et à l'encontre de M. H... une interdiction d'exercer la fonction de commissaire aux comptes pendant dix-huit mois, assortie du sursis pour la totalité de sa durée et une sanction pécuniaire de 50 000 euros, la question de la conformité aux droits et libertés garantis par la Constitution des articles L. 821-1, L. 821-2 du code de commerce, d'une part, des articles L. 821-3-1 alinéa 1, L. 824-5, L. 824-8, alinéas 1 et 3, et L. 824-11, alinéa 7 et 8 du code de commerce, d'autre part. <br/>
<br/>
              Sur la question prioritaire de constitutionnalité relative aux articles L. 821-1 et L. 821-2 du code de commerce :<br/>
<br/>
              3. L'article L. 821-1 du code de commerce prévoit que le Haut conseil du commissariat aux comptes, qui est une autorité publique indépendante, exerce les missions qu'il énumère parmi lesquelles figurent notamment l'adoption, dans les conditions prévues à l'article L. 821-14, des normes relatives à la déontologie des commissaires aux comptes, au contrôle interne de qualité et à l'exercice professionnel ainsi que le prononcé de sanctions. L'article L. 821-2 du code de commerce précise l'organisation du Haut conseil, qui comporte un collège de quatorze membres, présidé par un magistrat membre de la Cour de cassation, et une formation restreinte, chargée du prononcé des sanctions, présidée par l'un des deux autres magistrats de l'ordre judiciaire qui siègent au collège et par quatre autres membres élus par le collège en son sein, à l'exception des membres du bureau et du directeur général du Trésor ou de son représentant, et prévoit qu'une commission placée auprès du Haut conseil, composée à parité de quatre membres du collège et de quatre commissaires aux comptes désignés par la Compagnie nationale des commissaires aux comptes, est compétente pour élaborer, dans un délai fixé par décret, les projets des normes prévues à l'article L. 821-1 du code de commerce, lesquels doivent être adoptés par le Haut conseil ou, à défaut d'élaboration dans ce délai, élaborés par lui.<br/>
<br/>
              4. A l'appui de leur demande tendant à l'annulation des sanctions prononcées à leur encontre par le Haut conseil du commissariat aux comptes, la société Mazars et M. H... soutiennent que ces dispositions sont contraires au principe de séparation des pouvoirs découlant de l'article 16 de la Déclaration des droits de l'homme et du citoyen en ce qu'elles confient au Haut conseil à la fois le pouvoir d'adopter une norme et d'en assurer la sanction et en ce qu'elles permettent à des membres de la formation restreinte, en charge du prononcé des sanctions, de participer aux instances en charge de l'élaboration des normes dont la méconnaissance est sanctionnée.<br/>
<br/>
              5. Aux termes de l'article 16 de la Déclaration des droits de l'homme et du citoyen du 26 août 1789 : " Toute société dans laquelle la garantie des droits n'est pas assurée, ni la séparation des pouvoirs déterminée, n'a point de Constitution ". L'attribution par la loi à une autorité administrative du pouvoir de fixer les règles dans un domaine déterminé et d'en assurer elle-même le respect, par l'exercice d'un pouvoir de contrôle des activités exercées et de sanction des manquements constatés, ne contrevient pas aux exigences découlant de l'article 16 de la Déclaration des droits de l'homme et du citoyen dès lors que ce pouvoir de sanction est aménagé de telle façon que soient assurés le respect des droits de la défense, le caractère contradictoire de la procédure et les principes d'indépendance et d'impartialité.<br/>
<br/>
              6. Les articles L. 821-1 et suivants du code de commerce prévoient notamment, ainsi qu'il a été dit plus haut, que le collège du Haut conseil du commissariat aux comptes adopte des normes relatives à la déontologie des commissaires aux comptes, au contrôle interne de qualité et à l'exercice professionnel et que sa formation restreinte exerce le pouvoir disciplinaire à l'égard des commissaires aux comptes. Les articles L. 824-1 et suivants du même code précisent notamment la nature des manquements et des sanctions dont sont passibles les commissaires aux comptes ainsi que la procédure disciplinaire applicable, qui prévoit l'engagement de la procédure par la saisine du rapporteur général ; une phase d'enquête conduite par le rapporteur général, qui entend notamment la personne intéressée et qui se conclut par un rapport adressé par le rapporteur général au collège du Haut conseil lequel, hors la présence des membres de la formation restreinte, délibère pour arrêter les griefs reprochés à la personne en cause, qui sont alors notifiés à la personne intéressée par le rapporteur général ; enfin, sur la base d'un rapport final du rapporteur général avec les observations de la personne intéressée, une saisine de la formation restreinte, qui, lors d'une audience publique, entend notamment la personne poursuivie, qui peut être assistée ou représentée par la personne de son choix - et qui peut, lorsqu'il existe une raison sérieuse de mettre en doute l'impartialité d'un membre de la formation, en obtenir la récusation -  ainsi que les conclusions du rapporteur général ou de son représentant, avant de délibérer hors la présence des parties et du rapporteur général.<br/>
<br/>
              7. Eu égard à ce qui précède, d'une part, le principe du cumul au sein du Haut conseil d'un pouvoir d'élaboration de normes et de sanction de leur méconnaissance n'est pas, par lui-même, de nature à méconnaître les exigences découlant de l'article 16 de la Déclaration des droits de l'homme et du citoyen, le pouvoir de sanction confié à cette autorité étant organisé dans des conditions qui assurent le respect des droits de la défense, du caractère contradictoire de la procédure et des principes d'indépendance et d'impartialité. D'autre part, si aucune disposition du code de commerce ne fait obstacle à ce que des membres de la formation restreinte du collège du Haut conseil aient par ailleurs siégé dans les instances de ce Haut conseil chargées d'élaborer ou d'adopter les normes dont la formation restreinte est amenée à faire application lorsqu'elle se prononce sur les procédures individuelles dont elle est saisie, cette circonstance n'est pas non plus par elle-même de nature à méconnaître les exigences découlant de l'article 16 de la Déclaration des droits de l'homme et du citoyen, la formation restreinte n'ayant à connaître que de litiges individuels. <br/>
<br/>
              8. Il résulte de ce qui précède que la question soulevée, qui n'est pas nouvelle, ne présente pas de caractère sérieux. Il n'y a, par suite, pas lieu de la renvoyer au Conseil constitutionnel.<br/>
<br/>
              Sur la question prioritaire de constitutionnalité relative aux articles L. 821-3-1 alinéa 1, L. 824-5, L. 824-8 alinéas 1 et 3 et L. 824-11, alinéas 7 et 8 du code de commerce :<br/>
<br/>
              9. Aux termes de l'alinéa 1 de l'article L. 821-3-1 du code de commerce : " Le Haut conseil dispose d'un service chargé de procéder aux enquêtes préalables à l'ouverture des procédures prévues au chapitre IV du présent titre. Ce service est dirigé par un rapporteur général et composé d'enquêteurs habilités par ce dernier ". Aux termes de l'article L. 824-5 : " Le rapporteur général procède à une enquête. Il peut désigner des enquêteurs pour l'assister. Le rapporteur général et les enquêteurs peuvent à cet effet : / 1° Obtenir du commissaire aux comptes, sans que celui-ci puisse opposer le secret professionnel, tout document ou information, sous quelque forme que ce soit ; ils peuvent en exiger une copie ; / 2° Obtenir de toute personne tout document ou information utile à l'enquête ; ils peuvent en exiger une copie ; / 3° Convoquer et entendre toute personne susceptible de leur fournir des informations ; / 4° Accéder aux locaux à usage professionnel ; / 5° Demander à des commissaires aux comptes inscrits sur une liste établie par le Haut conseil après avis de la compagnie nationale des commissaires aux comptes, de procéder à des vérifications ou d'effectuer des actes d'enquête sous leur contrôle ; / 6° Faire appel à des experts. / Toute personne entendue pour les besoins de l'enquête peut se faire assister par un conseil de son choix ". Les alinéas 1 et 3 de l'article L. 824-8 disposent que : " A l'issue de l'enquête et après avoir entendu la personne intéressée, le rapporteur général établit un rapport d'enquête qu'il adresse au Haut conseil. Lorsque les faits justifient l'engagement d'une procédure de sanction, le Haut conseil délibérant hors la présence des membres de la formation restreinte arrête les griefs qui sont notifiés par le rapporteur général à la personne intéressée. La notification expose les faits passibles de sanction. Elle est accompagnée des principaux éléments susceptibles de fonder les griefs. (...) Le rapporteur général établit un rapport final qu'il adresse à la formation restreinte avec les observations de la personne intéressée ". Enfin, les alinéas 7 et 8 de l'article L. 824-11 prévoient que : " Le rapporteur général ou la personne qu'il désigne pour le représenter assiste à l'audience. Il expose ses conclusions oralement. Il peut proposer une ou plusieurs des sanctions prévues aux articles L. 824-2 et L. 824-3 ". <br/>
<br/>
              10. La société Mazars et M. H... soutiennent que ces dispositions sont contraires à l'article 16 de la Déclaration des droits de l'homme et du citoyen en ce qu'elles ne garantissent pas suffisamment l'impartialité du rapporteur général.  <br/>
<br/>
              11. Ces dispositions font du rapporteur général, qui est nommé par le président du Haut conseil et dirige un service spécifique du Haut conseil, l'organe en charge de l'instruction. A ce titre, il lui revient d'établir le rapport d'enquête transmis au Haut conseil et sur le fondement duquel ce dernier, statuant hors la présence des membres de la formation restreinte, arrête les griefs. Il lui revient également d'établir le rapport final, transmis à la formation restreinte avec les observations de la personne intéressée, laquelle peut consulter le dossier, présenter ses observations et se faire assister par un conseil à toutes les étapes de la procédure. Il peut assister à l'audience où il expose ses conclusions mais ne participe pas au délibéré. Dans ces conditions, l'organisation de la procédure de sanction devant le Haut conseil du commissariat aux comptes n'opère pas de confusion entre, d'une part, les fonctions de poursuite et d'instruction et, d'autre part, les pouvoirs de sanction.<br/>
<br/>
              12. Il résulte de ce qui précède que la question soulevée, qui n'est pas nouvelle, ne présente pas de caractère sérieux. Il n'y a, par suite, pas lieu de la renvoyer au Conseil constitutionnel.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel les questions prioritaires de constitutionnalité soulevées par la société Mazars.<br/>
Article 2  : La présente décision sera notifiée à la société Mazars, à M A... H..., au Haut conseil du commissariat aux comptes, au garde des sceaux, ministre de la justice et au ministre de l'économie, des finances et de la relance.<br/>
Copie en sera adressée au Conseil constitutionnel et au Premier ministre. <br/>
<br/>
<br/>
<br/>
              Délibéré à l'issue de la séance du 8 octobre 2021 où siégeaient : Mme Christine Maugüé, présidente adjointe de la Section du contentieux, présidant ; M. B... G..., M. Fabien Raynaud, présidents de chambre ; Mme N... J..., M. L... D..., Mme F... K..., M. E... I..., M. Cyril Roger-Lacan, conseillers d'Etat et Mme Rozen Noguellou, conseillère d'Etat-rapporteure. <br/>
<br/>
              Rendu le 15 octobre 2021.<br/>
<br/>
<br/>
                 La présidente : <br/>
                 Signé : Mme Christine Maugüé<br/>
 		La rapporteure : <br/>
      Signé : Mme Rozen Noguellou<br/>
                 La secrétaire :<br/>
                 Signé : Mme M... C...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
