<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028854919</ID>
<ANCIEN_ID>JG_L_2014_03_000000358821</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/85/49/CETATEXT000028854919.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème / 5ème SSR, 31/03/2014, 358821</TITRE>
<DATE_DEC>2014-03-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>358821</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème / 5ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP HEMERY, THOMAS-RAQUIN ; SCP BARTHELEMY, MATUCHANSKY, VEXLIARD ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Philippe Orban</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:358821.20140331</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 24 avril et 8 juin 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. C...A..., demeurant ...; M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision n° 10445/10446 du 24 février 2012 de la chambre disciplinaire nationale de l'ordre des médecins en tant qu'elle a d'une part, rejeté sa requête tendant à l'annulation de la décision n° 49 du 14 avril 2009 par laquelle la chambre disciplinaire de première instance du Centre, statuant sur les plaintes de Mme E...et du conseil départemental de l'ordre des médecins de l'Indre, lui avait infligé un blâme et, d'autre part, lui a interdit d'exercer la médecine pendant un mois et a décidé que cette sanction prendrait effet à compter du 1er juillet 2012 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel et de rejeter les appels de Mme E...et du conseil départemental de l'ordre des médecins de l'Indre ;<br/>
<br/>
              3°) de mettre à la charge du conseil départemental de l'ordre des médecins de l'Indre la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la santé publique ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Philippe Orban, maître des requêtes en service extraordinaire, <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de M.A..., à la SCP Hémery, Thomas-Raquin, avocat du conseil départemental de l'ordre des médecins de l'Indre et à la SCP Barthélemy, Matuchansky, Vexliard, avocat du Conseil national de l'ordre des médecins ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 4124-2 du code de la santé publique, dans sa rédaction alors applicable : " Les médecins (...) chargés d'un service public et inscrits au tableau de l'ordre ne peuvent être traduits devant la chambre disciplinaire de première instance, à l'occasion des actes de leur fonction publique, que par le ministre chargé de la santé, le représentant de l'Etat dans le département, le procureur de la République ou, lorsque lesdits actes ont été réalisés dans un établissement public de santé, le directeur de l'agence régionale de l'hospitalisation. " ; qu'en vertu du premier alinéa de l'article L. 6146-5-1 du même code, les praticiens chefs de service " assurent la mise en oeuvre des missions assignées à la structure dont ils ont la responsabilité et la coordination de l'équipe médicale qui s'y trouve affectée. " ; qu'il résulte de ces dispositions que, lorsque les auteurs de la plainte ne sont pas au nombre des personnes limitativement énumérées par l'article L. 4124-2 du code de la santé publique, ils ne sont pas recevables à invoquer devant les juridictions disciplinaires des griefs qui se rattachent à des actes commis par les praticiens chargés d'un service public à l'occasion de leur fonction publique ;<br/>
<br/>
              2. Considérant que, pour confirmer la sanction infligée à M. A...chef du service d'oto-rhino-laryngologie au centre hospitalier de Châteauroux, la chambre disciplinaire nationale a jugé qu'en acceptant que M. D...exerce, au sein de ce service, une mission étrangère à l'objet de celui-ci, M. A...avait méconnu les dispositions précitées de l'article L. 146-5-1 du code de la santé publique ; qu'il résulte cependant des dispositions de l'article L. 4124-2 du même code qu'il ne pouvait être traduit devant la juridiction disciplinaire à raison des actes accomplis dans ses fonctions de chef de service hospitalier, notamment dans l'exercice des pouvoirs qu'il tient de l'article L. 6146-5-1, que par les autorités mentionnées par cet article ; que ni MmeE..., ni le conseil départemental de l'Indre, auteurs de la plainte contre M.A..., n'étaient au nombre des personnes pouvant engager contre lui une action disciplinaire au titre de ses fonctions publiques ; qu'ainsi, en accueillant ce grief de Mme E...et du conseil départemental de l'Indre, alors que seuls étaient recevables les griefs de leurs plaintes se rapportant à l'activité privée du praticien, la chambre disciplinaire nationale a commis une erreur de droit ; que, dès lors, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, M. A... est fondé à en demander l'annulation ;<br/>
<br/>
              3. Considérant que l'affaire faisant l'objet d'un second recours en cassation, il y a lieu de la régler au fond par application des dispositions du deuxième alinéa de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Sur les griefs se rapportant aux actes de fonction publique :<br/>
<br/>
              4. Considérant qu'il résulte de ce qui a été dit ci-dessus que le grief tiré de ce que M. A...aurait méconnu les dispositions de l'article L. 6146-5-1 du code de la santé publique en tolérant des activités libérales sans rapport avec l'objet du service, n'est pas recevable dès lors que les plaintes n'émanent pas d'une des personnes habilitées à engager une action disciplinaire à l'encontre du requérant à l'occasion de ses actes de fonction publique ; <br/>
<br/>
              Sur les griefs se rapportant aux activités libérales :<br/>
<br/>
              5. Considérant que si Mme E...et le conseil départemental de l'ordre des médecins de l'Indre soutiennent que M. A...aurait manqué aux obligations de moralité, de probité et de dévouement résultant de l'article R. 4127-3 du code de la santé publique pour avoir fait pratiquer des actes d'exploration fonctionnelle sur des patients reçus en consultation privée par des auxiliaires médicaux et s'être ainsi fait rémunérer pour des actes qu'il n'accomplissait pas lui-même, il résulte des dispositions de l'article R. 4311-10 du code de la santé publique que le personnel infirmier était habilité à réaliser ces actes et il ressort des pièces du dossier que M. A... en assurait lui-même l'interprétation ; qu'ainsi ce moyen doit être écarté ;<br/>
<br/>
              6. Considérant qu'en vertu des dispositions combinées des articles L. 4113-9 et R. 6154-4 du code de la santé publique, il appartient aux médecins de communiquer aux conseils compétents de leur ordre les contrats relatifs à l'exercice de leur activité libérale dans le mois suivant leur conclusion ; que les dispositions de l'article L. 6154-5 du code de la santé publique, qui prévoient qu'une commission d'activité libérale " est chargée de veiller au bon déroulement de cette activité ", n'ont ni pour objet ni pour effet d'exonérer les praticiens hospitaliers de cette obligation ; qu'il ressort des pièces du dossier que M. A...avait communiqué son contrat d'activité libérale avec sept années de retard, alors qu'aucune circonstance ne le dispensait de respecter cette formalité ; <br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que seuls les manquements aux dispositions combinées des articles L. 4113-9 et R. 6154-4 du code de la santé publique, reprochés à M.A..., sont de nature à justifier une sanction ; qu'il sera fait une juste appréciation de ces manquements en lui infligeant la sanction de l'avertissement ; <br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              8. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge du conseil départemental de l'ordre des médecins de l'Indre la somme que demande M. A...au titre de ces dispositions ; qu'il n'y a pas lieu non plus, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par le conseil départemental de l'ordre sur le fondement des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>
              				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Les articles 3, 5 et 7 de la décision du 24 février 2012 de la chambre disciplinaire nationale de l'ordre des médecins sont annulés, en tant qu'ils concernent M.A....<br/>
Article 2 : La décision du 14 avril 2009 de la chambre disciplinaire de première instance du Centre est annulée en tant qu'elle a infligé un blâme à M.A....<br/>
Article 3 : Il est infligé un avertissement à M.A....<br/>
Article 4 : Le surplus des conclusions du pourvoi de M. A...est rejeté.<br/>
Article 5 : Les conclusions présentées par le conseil départemental de l'ordre des médecins de l'Indre au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 6 : La présente décision sera notifiée à M. C...A..., au conseil départemental de l'ordre des médecins de l'Indre et au Conseil national de l'ordre des médecins. <br/>
Copie en sera adressée pour information à Mme B...F....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">55-04-01-01 PROFESSIONS, CHARGES ET OFFICES. DISCIPLINE PROFESSIONNELLE. PROCÉDURE DEVANT LES JURIDICTIONS ORDINALES. INTRODUCTION DE L'INSTANCE. - PLAINTES FORMÉES PAR UN PARTICULIER ET PAR UN CONSEIL DÉPARTEMENTAL QUI LA TRANSMET EN S'Y ASSOCIANT - GRIEFS PORTANT D'UNE PART SUR LES ACTES ACCOMPLIS PAR LE PRATICIEN DANS SES FONCTIONS DE CHEF DE SERVICE HOSPITALIER ET D'AUTRE PART SUR SON ACTIVITÉ PRIVÉE - IRRECEVABILITÉ DE LA PREMIÈRE SÉRIE DE GRIEFS (ART. L. 4124-2 DU CSP) - CONSÉQUENCE - RECEVABILITÉ PARTIELLE DES PLAINTES - EXISTENCE.
</SCT>
<ANA ID="9A"> 55-04-01-01 Plaintes formées par un particulier et par un conseil départemental qui la transmet en s'y associant portant, d'une part, sur les actes accomplis par le praticien dans ses fonctions de chef de service hospitalier et, d'autre part, sur son activité privée.... ,,Il résulte des dispositions de l'article L. 4124-2 du code de la santé publique (CSP) que le praticien ne pouvait être traduit devant la juridiction disciplinaire à raison des actes accomplis dans ses fonctions de chef de service hospitalier, notamment dans l'exercice des pouvoirs qu'il tient de l'article L. 6146-5-1, que par les autorités mentionnées à cet article. Dès lors que ni le particulier, ni le conseil départemental, n'étaient au nombre des personnes pouvant engager contre le praticien une action disciplinaire au titre de ces fonctions publiques, seuls étaient recevables les griefs de leurs plaintes se rapportant à l'activité privée du praticien.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
