<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030713408</ID>
<ANCIEN_ID>JG_L_2015_06_000000373602</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/71/34/CETATEXT000030713408.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème SSJS, 10/06/2015, 373602, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-06-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>373602</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR>M. Jacques Reiller</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:373602.20150610</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
<br/>
              M. et Mme B...A...ont demandé au tribunal administratif de Châlons-en-Champagne la décharge des suppléments d'impôt sur le revenu auxquels ils ont été assujettis au titre de l'année 2000.<br/>
<br/>
              Par un jugement n° 0901785 du 24 mai 2012, le tribunal administratif de Châlons-en-Champagne a fait droit à leur demande.<br/>
<br/>
              Par un arrêt n° 12NC01614 du 3 octobre 2013, la cour administrative d'appel de Nancy a rejeté l'appel du ministre délégué, chargé du budget, dirigé contre ce jugement.<br/>
<br/>
              Par un pourvoi, enregistré le 29 novembre 2013 au secrétariat du contentieux du Conseil d'Etat, le ministre délégué, chargé du budget, demande au Conseil d'Etat d'annuler cet arrêt.<br/>
<br/>
<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jacques Reiller, conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delaporte, Briard, Trichet, avocat de M. et Mme A...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que l'article 163 tervicies du code général des impôts alors applicable dispose : " I. - Les contribuables peuvent déduire de leur revenu net global une somme égale au montant hors taxes des investissements productifs (...) qu'ils réalisent dans les départements et territoires d'outre-mer (...), dans le cadre d'une entreprise exerçant une activité dans les secteurs (...) de l'agriculture ; (...) IV. - Les dispositions du présent article (...) ne sont applicables qu'aux investissements neufs réalisés au plus tard le 31 décembre 2002 " ; que l'article 91 sexies de l'annexe II au même code pris pour l'application de l'article 163 tervicies précise : " Les investissements productifs réalisés dans les départements et territoires d'outre-mer et dans les collectivités territoriales de Mayotte et de Saint-Pierre-et-Miquelon dont le montant peut être déduit du revenu net global des contribuables en application du premier alinéa du I de l'article 163 tervicies du code général des impôts s'entendent des acquisitions ou créations d'immobilisations corporelles amortissables, affectées aux activités relevant des secteurs mentionnés au même alinéa " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'à la suite d'une vérification de comptabilité de la société en nom collectif Château-Gaillard (SNC), dont M. A...est l'un des associés, et d'un contrôle sur pièces des revenus déclarés par M. et MmeA..., l'administration a remis en cause les déductions que les contribuables, en se prévalant de l'article 163 tervicies du code général des impôts, avaient pratiquées sur leur revenu global de l'année 2000 correspondant à leur quote-part des dépenses réalisées par la SNC en Guadeloupe pour financer des travaux de plantation de bananeraies ; que, par jugement du 24 mai 2012, le tribunal administratif de Châlons-en-Champagne a accordé aux contribuables la décharge des cotisations supplémentaires d'impôt sur le revenu auxquelles ils ont été assujettis au titre de 2000 ; que, par arrêt du 3 octobre 2013, la cour administrative d'appel de Nancy a rejeté le recours du ministre délégué chargé du budget dirigé contre ce jugement ; que le ministre chargé du budget se pourvoit en cassation contre cet arrêt ; <br/>
<br/>
              3. Considérant, en premier lieu, d'une part, que les dispositions précitées de l'article 163 tervicies du code général des impôts, dès lors qu'elles se réfèrent sans restriction au secteur d'activité de l'agriculture, ne sauraient être interprétées, en l'absence d'indication de la volonté du législateur dans les travaux parlementaires ayant précédé leur adoption, comme limitant le bénéfice de l'avantage fiscal qu'elles prévoient aux investissements réalisés par des entreprises appartenant au secteur de la production agricole et par suite aux seuls exploitants agricoles ; que, d'autre part, la circonstance que l'investissement productif réalisé, à caractère immobilier, donne lieu à location ou sous-location est sans incidence sur le droit à bénéfice de la déduction au titre de l'impôt sur le revenu prévu par ces dispositions ; que, dès lors, c'est sans entacher son arrêt d'erreur de droit ni d'inexacte qualification juridique des faits que la cour, après avoir relevé que la SNC Château-Gaillard avait financé les travaux de plantation de bananeraies et avait donné en location ces bananeraies à des entreprises agricoles chargées d'en assurer l'exploitation, a jugé que les immobilisations ainsi réalisées par cette société entraient dans le champ d'application du I de l'article 163 tervicies et que la circonstance que les investissements productifs réalisés avaient donné lieu à location était sans incidence sur le droit au bénéfice de leur déduction sur le revenu global net des associés à proportion de leurs droits dans la SNC Château-Gaillard ;  <br/>
<br/>
              4. Considérant, en second lieu, que la cour n'a pas entaché son arrêt de dénaturation des pièces du dossier en estimant, au vu des factures établies au titre de l'année 2000 lors des travaux de réalisation des plantations, que les dépenses de préparation du sol, d'arrachage, de nettoyage, de parage et sortie des plants, d'amendements, de préémergence et de traitement par insecticides et nématicides étaient en relation directe avec la création des plantations ; qu'elle a pu en déduire, sans erreur de qualification juridique sur la nature de ces dépenses, qu'elles pouvaient donner lieu au bénéfice des dispositions de l'article 163 tervicies précitées ;<br/>
<br/>
              5. Considérant qu'il suit de ce qui précède que le ministre chargé du budget n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du ministre chargé du budget est rejeté.<br/>
Article 2 : L'Etat versera à M. et Mme A...la somme de 3 000 euros en application de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée au ministre des finances et des comptes publics et à M. et Mme B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
