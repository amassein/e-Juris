<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032865670</ID>
<ANCIEN_ID>JG_L_2016_07_000000386606</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/86/56/CETATEXT000032865670.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème chambres réunies, 08/07/2016, 386606, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-07-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>386606</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Marc Thoumelou</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:386606.20160708</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 19 décembre 2014 et 10 septembre 2015 au secrétariat du contentieux du Conseil d'Etat, le Syndicat national des médecins français spécialistes de l'appareil digestif demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du ministre des finances et des comptes publics et du ministre des affaires sociales, de la santé et des droits des femmes du 30 octobre 2014 modifiant la liste des spécialités pharmaceutiques agréées à l'usage des collectivités et divers services publics ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - le code de la sécurité sociale ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Thoumelou, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes des premier et dernier alinéas de l'article L. 5123-2 du code de la santé publique : " L'achat, la fourniture, la prise en charge et l'utilisation par les collectivités publiques des médicaments définis aux articles L. 5121-8, L. 5121-9-1, L. 5121-13 et L. 5121-14-1 (...) sont limités (...) aux produits agréés dont la liste est établie par arrêté des ministres chargés de la santé et de la sécurité sociale. Cette liste précise les seules indications thérapeutiques ouvrant droit à la prise en charge des médicaments. / (...) / L'inscription d'un médicament sur la liste (...) peut, au vu des exigences de qualité et de sécurité des soins mettant en oeuvre ce médicament, énoncées le cas échéant par la commission prévue à l'article L. 5123-3, être assortie de conditions concernant la qualification ou la compétence des prescripteurs, l'environnement technique ou l'organisation de ces soins et d'un dispositif de suivi des patients traités " ; que, sur le fondement de ces dispositions, les ministres chargés de la santé et de la sécurité sociale ont, par un arrêté du 30 octobre 2014, dont le Syndicat national des médecins français spécialistes de l'appareil digestif demande l'annulation pour excès de pouvoir, inscrit la spécialité Sovaldi sur la liste des médicaments agréés à l'usage des collectivités et divers services publics, fixé les indications thérapeutiques ouvrant droit à sa prise en charge par l'assurance maladie et assorti cette inscription d'une condition relative à l'organisation des soins, en subordonnant l'engagement du traitement à " la tenue, dans les pôles de référence hépatites, d'une réunion de concertation pluridisciplinaire " ;<br/>
<br/>
              Sur la fin de non-recevoir opposée par le ministre des affaires sociales, de la santé et des droits des femmes :<br/>
<br/>
              2. Considérant que le Syndicat national des médecins français spécialistes de l'appareil digestif a, contrairement à ce que soutient le ministre des affaires sociales, de la santé et des droits des femmes, intérêt à agir contre l'arrêté attaqué, qui est susceptible d'affecter les conditions d'exercice des médecins hépato-gastro-entérologues dont il a notamment pour objet de défendre les intérêts matériels et moraux ;<br/>
<br/>
              Sur la légalité de l'arrêté attaqué :<br/>
<br/>
              3. Considérant, en premier lieu, que l'arrêté attaqué a pour seul objet, en application de l'article L. 5123-2 du code de la santé publique, de réglementer l'achat, la fourniture, la prise en charge et l'utilisation de la spécialité Sovaldi par les collectivités et divers services publics ; qu'il n'a ni pour objet, ni pour effet de réglementer la prise en charge de cette spécialité dans le cadre d'une dispensation en officine, laquelle relève de la liste mentionnée à l'article L. 162-17 du code de la sécurité sociale ; que, par suite, le syndicat requérant ne peut utilement se prévaloir, pour contester la légalité de l'arrêté qu'il attaque, de ses conséquences sur les médecins hépato-gastro-entérologues exerçant à titre libéral ;<br/>
<br/>
              4. Considérant, en deuxième lieu, qu'il résulte des dispositions précitées du dernier alinéa de l'article L. 5123-2 du code de la santé publique que l'inscription d'un médicament sur la liste des spécialités pharmaceutiques agréées à l'usage des collectivités et divers services publics précise les indications thérapeutiques ouvrant droit à sa prise en charge et peut, au vu des exigences de qualité et de sécurité des soins mettant en oeuvre ce médicament, être assortie de conditions concernant notamment l'organisation de ces soins ; que, dans sa recommandation relative à la prise en charge de l'hépatite C par les médicaments anti-viraux à action directe, au nombre desquels figure la spécialité Sovaldi, le collège de la Haute Autorité de santé a indiqué que " la décision de traiter les patients ainsi que les modalités de leur suivi devraient être décidées en réunion de concertation de praticiens, comprenant au minimum un virologue et un hépatologue " ; qu'en outre, il a souligné les incertitudes entourant les potentialités et la place dans la stratégie thérapeutique de ces médicaments, ainsi que la nécessaire adaptation du traitement au stade de fibrose ; que, s'agissant du traitement des malades atteints d'une fibrose de " stade 2 ", le collège de la Haute Autorité de santé a précisé que " ce stade (...) est de diagnostic difficile " et que " l'évolution de ce stade de fibrose vers une cirrhose hépatique n'est pas constante " et en a conclu que " divers arguments justifient (...) de traiter les malades au stade F2 ", en invitant à " documenter au mieux le stade réel de fibrose avant de traiter " ; <br/>
<br/>
              5. Considérant que, dans ces conditions, il ne ressort pas des pièces du dossier qu'en soumettant l'engagement de l'ensemble des traitements par Sovaldi à la tenue préalable d'une réunion de concertation pluridisciplinaire et en limitant la prise en charge de cette spécialité, s'agissant des malades atteints d'une fibrose de " stade 2 ", aux seuls cas pouvant être qualifiés de " sévères ", les ministres auteurs de l'arrêté attaqué auraient entaché leur appréciation d'erreur manifeste ; <br/>
<br/>
              6. Considérant, toutefois, que le syndicat requérant conteste également la limitation de la tenue des réunions de concertation pluridisciplinaire dans les seuls " pôles de référence hépatites ", au nombre de trente-et-un pour l'ensemble du territoire, en faisant valoir qu'une telle restriction est manifestement excessive au regard du nombre élevé de porteurs du virus de l'hépatite C susceptibles d'être concernés par ce traitement ; que le ministre des affaires sociales, de la santé et des droits des femmes n'apporte en défense aucune justification à cette limitation tenant aux caractéristiques propres de l'organisation des soins au sein de ces pôles, aux qualifications ou compétences particulières des prescripteurs y exerçant, ou au suivi des patients traités qui y aurait été mis en place, alors même que le collège de la Haute Autorité de santé, dans sa recommandation précitée de juin 2014, n'avait pas mis en évidence de nécessité particulière tenant à la tenue, au sein des seuls pôles de référence hépatites, des réunions de concertation de praticiens qu'il préconisait ; que, dans ces conditions, compte tenu de la précision des critiques, de la teneur de la recommandation de la Haute Autorité de santé et, en revanche, de l'absence de toute justification apportée en défense, le syndicat requérant est fondé à soutenir que les ministres auteurs de l'arrêté attaqué ont, en réservant à ces seuls pôles la possibilité de tenir de telles réunions, porté une appréciation manifestement erronée des conditions relatives à l'organisation des soins auxquelles la prise en charge du traitement devait être subordonnée ; que, par suite, les mots " , dans les pôles de référence hépatites, " figurant dans l'annexe à l'arrêté attaqué, qui sont divisibles des autres dispositions de cet arrêté et ont été, au demeurant, ultérieurement abrogés par un arrêté des mêmes ministres du 29 avril 2015, doivent être annulés ;<br/>
<br/>
              7. Considérant, en troisième lieu, qu'aux termes du premier alinéa de l'article R. 4127-8 du code de la santé publique : " Dans les limites fixées par la loi et compte tenu des données acquises de la science, le médecin est libre de ses prescriptions qui seront celles qu'il estime les plus appropriées en la circonstance " ; que la possibilité de subordonner l'inscription d'un médicament sur la liste des spécialités pharmaceutiques agréées à l'usage des collectivités et divers services publics à une condition tenant à l'organisation des soins résulte directement des dispositions précitées de l'article L. 5123-2 du même code ; que les ministres n'ayant pas fait une application manifestement erronée de la loi, ainsi qu'il a été dit ci-dessus, en prévoyant la tenue préalable d'une réunion de concertation pluridisciplinaire, l'arrêté ne porte pas une atteinte illégale à la liberté de prescription des médecins, laquelle s'exerce dans les limites fixées par la loi, en imposant une telle condition ;<br/>
<br/>
              8. Considérant, en quatrième lieu, que les dispositions attaquées, en tant qu'elles subordonnent l'engagement d'un traitement à des réunions de concertation pluridisciplinaires, ne peuvent être regardées comme privant certains patients de la possibilité d'accéder à un traitement ; que cette modalité particulière d'organisation des soins ne méconnaît pas davantage, en tout état de cause, par elle-même, le principe d'égalité entre les praticiens exerçant au sein des établissements de santé ;<br/>
<br/>
              9. Considérant, en dernier lieu, que la circonstance que les conditions mises par les dispositions attaquées à la prise en charge par l'assurance maladie de la spécialité Sovaldi aient notamment été motivées par son coût particulièrement élevé n'est pas constitutive d'un détournement de pouvoir ;<br/>
<br/>
              10. Considérant qu'il résulte de tout ce qui précède que le Syndicat national des médecins français spécialistes de l'appareil digestif est fondé à demander l'annulation des seuls mots ", dans les pôles de référence hépatites, " de l'annexe à l'arrêté qu'il attaque ;<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              11. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par le Syndicat national des médecins français spécialistes de l'appareil digestif au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêté du 30 octobre 2014 modifiant la liste des spécialités pharmaceutiques agréées à l'usage des collectivités et divers services publics est annulé en tant que le dernier alinéa de son annexe comporte les mots ", dans les pôles de référence hépatites, ".<br/>
Article 2 : Le surplus des conclusions de la requête du Syndicat national des médecins français spécialistes de l'appareil digestif est rejeté.<br/>
Article 3 : La présente décision sera notifiée au Syndicat national des médecins français spécialistes de l'appareil digestif, au ministre des finances et des comptes publics et à la ministre des affaires sociales et de la santé.<br/>
Copie en sera adressée à la Haute Autorité de santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
