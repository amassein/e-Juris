<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035701546</ID>
<ANCIEN_ID>JG_L_2017_10_000000407121</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/70/15/CETATEXT000035701546.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 02/10/2017, 407121, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-10-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>407121</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MEIER-BOURDEAU, LECUYER</AVOCATS>
<RAPPORTEUR>M. Jean-Baptiste de Froment</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:407121.20171002</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Ferme éolienne de Seigny a demandé au juge des référés du tribunal administratif de Dijon, sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de la décision du 28 octobre 2016 par laquelle le préfet de la Côte-d'Or a refusé de faire droit à sa demande d'autorisation unique en vue de l'implantation d'un parc éolien de cinq machines sur le territoire de la commune de Seigny.<br/>
<br/>
               Par une ordonnance n° 1603506 du 6 janvier 2017, le juge des référés du tribunal administratif de Dijon a suspendu l'exécution de cette décision.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 23 janvier et 7 février 2017 au secrétariat du contentieux du Conseil d'Etat, la ministre de l'environnement, de l'énergie et de la mer demande au Conseil d'Etat d'annuler cette ordonnance. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
              Vu : <br/>
              - le code de l'environnement ;<br/>
              - l'ordonnance n° 2014-355 du 20 mars 2014 ;<br/>
              - le décret n° 2014-450 du 2 mai 2014; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Baptiste de Froment, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Meier-Bourdeau, Lecuyer, avocat de la société Ferme éolienne de Seigny.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 15 septembre 2017, présentée par la société Ferme éolienne de Seigny.<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que, par une décision du 28 octobre 2016, le préfet de la Côte-d'Or a refusé de faire droit à la demande d'autorisation unique présentée par la société Ferme éolienne de Seigny en vue de l'implantation d'un parc éolien de cinq machines sur le territoire de la commune de Seigny, au motif que le projet ne permettait pas de respecter les objectifs fixés par l'article 3 de l'ordonnance du 20 mars 2014 relative à l'expérimentation d'une autorisation unique en matière d'installations classées pour la protection de l'environnement, en raison notamment des atteintes portées aux paysages et à divers sites classés se trouvant à proximité du projet, en particulier celui d'Alésia ; que la société Ferme éolienne de Seigny a demandé au juge des référés du tribunal administratif de Dijon, sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de cette décision de refus ; que, par une ordonnance du 6 janvier 2017, contre laquelle le ministre chargé de l'environnement et de l'énergie se pourvoit en cassation, le juge des référés a suspendu l'exécution de cette décision ; <br/>
<br/>
              2. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision. " ;  <br/>
<br/>
              3. Considérant que l'urgence justifie que soit prononcée la suspension d'un acte administratif lorsque l'exécution de celui-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ; qu'il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte litigieux sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue ; qu'il lui appartient également, l'urgence s'appréciant objectivement et compte tenu de l'ensemble des circonstances de chaque espèce, de faire apparaître dans sa décision tous les éléments qui, eu égard notamment à l'argumentation des parties, l'ont conduit à considérer que la suspension demandée revêtait un caractère d'urgence ;<br/>
<br/>
              4. Considérant que, pour prononcer la suspension de la décision litigieuse, le juge des référés s'est borné à relever que la Ferme éolienne de Seigny justifiait de l'existence d'une situation d'urgence, dans la mesure où la décision attaquée préjudiciait à ses intérêts, notamment eu égard au retard apporté à son projet en l'absence d'une instruction complète de sa demande ; qu'en statuant ainsi, en termes particulièrement succincts, sans répondre à l'argumentation en défense présentée par l'administration, qui critiquait l'absence de justification concrète et précise des atteintes portées à la situation, en particulier économique, de la société requérante, le juge des référés a entaché son ordonnance d'insuffisance de motivation ; que le ministre est par suite fondé, pour ce motif, à en demander l'annulation ; <br/>
<br/>
              5. Considérant qu'il y a lieu, en application des dispositions de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée ; <br/>
<br/>
              6. Considérant que les moyens tirés de ce que la décision attaquée serait entachée d'incompétence négative et d'erreur manifeste d'appréciation, ne sont pas, en l'état de l'instruction, propres à créer un doute sérieux sur sa légalité ; <br/>
<br/>
              7. Considérant que l'une des conditions posées par l'article L. 521-1 du code de justice administrative n'étant pas remplie, la demande présentée par la société Ferme éolienne de Seigny tendant à ce que soit ordonnée la suspension de l'exécution de la décision du préfet de la Côte d'Or en date du 28 octobre 2016 refusant de faire droit à sa demande doit être rejetée ; <br/>
<br/>
              8. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance, la somme que demande la société Ferme éolienne de Seigny au titre des frais exposés par elle et non compris dans les dépens ;  <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 6 janvier 2017 du juge des référés du tribunal administratif de Dijon est annulée.<br/>
Article 2 : La demande présentée par la société Ferme Eolienne de Seigny devant le juge des référés du tribunal administratif de Dijon est rejetée. <br/>
Article 3 : La présente décision sera notifiée au ministre d'Etat, ministre de la transition écologique et solidaire et à la société Ferme éolienne de Seigny.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
