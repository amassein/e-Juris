<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042499871</ID>
<ANCIEN_ID>JG_L_2020_11_000000437888</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/49/98/CETATEXT000042499871.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 04/11/2020, 437888, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-11-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>437888</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Pierre Vaiss</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Raphaël Chambon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:437888.20201104</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée 21 janvier 2020 au secrétariat du contentieux du Conseil d'Etat, Mme B... A... demande au Conseil d'Etat : <br/>
<br/>
              l°) d'annuler pour excès de pouvoir la décision du 21 novembre 2019 par laquelle le Conseil national de l'ordre des médecins, statuant en formation restreinte, l'a suspendue, sur le fondement de l'article R. 4124-3 du code de la santé publique, du droit d'exercer la médecine pour une durée de dix-huit mois et a subordonné la reprise de son activité professionnelle aux résultats d'une nouvelle expertise ; <br/>
<br/>
              2°) d'enjoindre au Conseil national de l'ordre des médecins de ramener la durée de la suspension d'exercice professionnel à douze mois.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pierre Vaiss, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat du Conseil national de l'ordre des médecins ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article R. 4124-3 du code de la santé publique : " I. - Dans le cas d'infirmité ou d'état pathologique rendant dangereux l'exercice de la profession, la suspension temporaire du droit d'exercer est prononcée par le conseil régional ou interrégional pour une période déterminée, qui peut, s'il y a lieu, être renouvelée. // Le conseil est saisi à cet effet soit par le directeur général de l'agence régionale de santé soit par une délibération du conseil départemental ou du conseil national. Ces saisines ne sont pas susceptibles de recours. / II. - La suspension ne peut être ordonnée que sur un rapport motivé établi à la demande du conseil régional ou interrégional par trois médecins désignés comme experts, le premier par l'intéressé, le deuxième par le conseil régional ou interrégional et le troisième par les deux premiers experts. / III. - En cas de carence de l'intéressé lors de la désignation du premier expert ou de désaccord des deux experts lors de la désignation du troisième, la désignation est faite à la demande du conseil par ordonnance du président du tribunal de grande instance dans le ressort duquel se trouve la résidence professionnelle de l'intéressé. Cette demande est dispensée de ministère d'avocat. / IV. - Les experts procèdent ensemble, sauf impossibilité manifeste, à l'expertise. Le rapport d'expertise est déposé au plus tard dans le délai de six semaines à compter de la saisine du conseil. / Si les experts ne peuvent parvenir à la rédaction de conclusions communes, le rapport comporte l'avis motivé de chacun d'eux. / VI. - Si le conseil régional ou interrégional n'a pas statué dans le délai de deux mois à compter de la réception de la demande dont il est saisi, l'affaire est portée devant le Conseil national de l'ordre. / VII. - La notification de la décision de suspension mentionne que la reprise de l'exercice professionnel par le praticien ne pourra avoir lieu sans qu'au préalable ait été diligentée une nouvelle expertise médicale, dont il lui incombe de demander l'organisation au conseil régional ou interrégional au plus tard deux mois avant l'expiration de la période de suspension. ".<br/>
<br/>
              2. Mme A..., médecin qualifiée spécialiste en médecine générale, demande l'annulation pour excès de pouvoir de la décision du 21 novembre 2019 par laquelle le Conseil national de l'ordre des médecins, statuant en formation restreinte, l'a, en application des dispositions qui viennent d'être citées, suspendue du droit d'exercer la médecine pour une durée de dix-huit mois et a subordonné la reprise de son activité professionnelle aux résultats d'une nouvelle expertise.<br/>
<br/>
              3. Il ressort des pièces du dossier et notamment du rapport de l'expertise diligentée que Mme A... présente un état pathologique de nature à faire courir un danger à ses patients et à elle-même et qui nécessite l'engagement et le suivi prolongé de soins spécialisés, avant que ne puisse être envisagée une éventuelle reprise de l'exercice de la médecine. Dès lors, en la suspendant, dans ces circonstances, du droit d'exercer la médecine pendant une durée de dix-huit mois, le Conseil national de l'ordre des médecins, statuant en formation restreinte, n'a pas fait une inexacte application des dispositions citées dans l'article R. 4124-3 du code de la santé publique. <br/>
<br/>
              4. Il résulte de tout ce qui précède que la requête de Mme A... ne peut être accueillie. <br/>
<br/>
              5. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire application des dispositions de l'article L. 761-1 du code de justice administrative et de mettre à la charge de Mme A... le versement de la somme que demande le Conseil national de l'ordre des médecins au titre des frais exposés par lui et non compris dans les dépens.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de Mme A... est rejetée.<br/>
Article 2 : Les conclusions présentées par le Conseil national de l'ordre des médecins au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à Mme B... A... et au Conseil national de l'ordre des médecins.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
