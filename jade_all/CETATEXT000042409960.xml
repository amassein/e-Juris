<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042409960</ID>
<ANCIEN_ID>JG_L_2020_10_000000430101</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/40/99/CETATEXT000042409960.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 07/10/2020, 430101, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-10-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>430101</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP RICHARD ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>Mme Catherine Brouard-Gallet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Raphaël Chambon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:430101.20201007</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés au secrétariat du contentieux du Conseil d'Etat les 23 avril, 22 juillet et 27 novembre 2019, Mme B... A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 8 janvier 2019 du Conseil national de l'ordre des médecins, statuant en formation restreinte, la suspendant pour une durée d'un an du droit d'exercer tout geste technique obstétrical, notamment les accouchements ou les césariennes, et tout acte chirurgical et subordonnant la reprise de l'ensemble de son activité à la justification du respect d'obligations de formation ; <br/>
<br/>
              2°) de mettre à la charge du Conseil national de l'ordre des médecins et du conseil départemental de la Guyane de l'ordre des médecins la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme C... D..., conseillère d'Etat en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Richard, avocat de Mme A... et à la SCP Matuchansky, Poupot, Valdelièvre, avocat du Conseil national de l'ordre des médecins ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article R. 4124-3-5 du code de la santé publique : " I.- En cas d'insuffisance professionnelle rendant dangereux l'exercice de la profession, la suspension temporaire, totale ou partielle, du droit d'exercer est prononcée par le conseil régional ou interrégional pour une période déterminée qui peut, s'il y a lieu, être renouvelée. / Le conseil régional ou interrégional est saisi à cet effet soit par le directeur général de l'agence régionale de santé, soit par une délibération du conseil départemental ou du conseil national. Ces saisines ne sont pas susceptibles de recours. / II. - La suspension ne peut être ordonnée que sur un rapport motivé établi à la demande du conseil régional ou interrégional dans les conditions suivantes : / 1° Pour les médecins, le rapport est établi par trois médecins qualifiés dans la même spécialité que celle du praticien concerné désignés comme experts, le premier par l'intéressé, le deuxième par le conseil régional ou interrégional et le troisième par les deux premiers experts. Ce dernier est choisi parmi les personnels enseignants et hospitaliers titulaires de la spécialité. (...). / IV.- Les experts procèdent ensemble, sauf impossibilité manifeste, à l'examen des connaissances théoriques et pratiques du praticien. Le rapport d'expertise est déposé au plus tard dans le délai de six semaines à compter de la saisine du conseil. Il indique les insuffisances relevées au cours de l'expertise, leur dangerosité et préconise les moyens de les pallier par une formation théorique et, si nécessaire, pratique. / Si les experts ne peuvent parvenir à la rédaction de conclusions communes, le rapport comporte l'avis motivé de chacun d'eux. / (...) / VI. Si le conseil régional ou interrégional n'a pas statué dans le délai de deux mois à compter de la réception de la demande dont il est saisi, l'affaire est portée devant le Conseil national de l'ordre. / VII. - La décision de suspension temporaire du droit d'exercer pour insuffisance professionnelle définit les obligations de formation du praticien. (...) ".<br/>
<br/>
              2. En application des dispositions de l'article R. 4124-3-5 du code de la santé publique cité ci-dessus, la formation restreinte du Conseil national de l'ordre des médecins, auquel l'affaire avait été renvoyée en application des dispositions du VI du même article, a, par la décision attaquée du 8 janvier 2019, suspendu Mme A..., médecin spécialiste qualifié en gynécologie médicale et obstétrique, du droit de pratiquer tout geste technique obstétrical, notamment les accouchements ou les césariennes, et tout acte chirurgical pendant une durée d'un an et a subordonné la reprise de cette activité à la justification du suivi d'une formation dans le domaine obstétrical. <br/>
<br/>
              3. En premier lieu, il ressort des pièces du dossier que le moyen tiré de ce que Mme A... n'aurait pas eu accès à la délibération par laquelle le conseil départemental de la Guyane de l'ordre des médecins a saisi de sa situation le conseil interrégional de la Guyane de l'ordre des médecins sur le fondement des dispositions du second alinéa du I de l'article R. 4124-3-5 du code de la santé publique manque en fait. <br/>
<br/>
              4. En second lieu, il ressort des pièces du dossier qu'en estimant, au vu notamment du rapport d'expertise établi conformément aux dispositions précitées du code de la santé publique, lequel concluait défavorablement à l'exercice par Mme A... d'une activité obstétricale en salle de naissance et en chirurgie gynécologique, et alors que, depuis 2012, année lors de laquelle ses fonctions hospitalières ont cessé, Mme A... a exclusivement une activité libérale de gynécologie médicale et n'a pas suivi de formation de maintien de ses compétences obstétricales, que Mme A... présente une insuffisance professionnelle rendant dangereux qu'elle pratique les actes techniques obstétricaux, notamment les accouchements ou les césariennes, et les actes chirurgicaux relevant de sa spécialité, la formation restreinte du Conseil national de l'ordre des médecins a fait une exacte application des dispositions du code de la santé publique citées au point 1. <br/>
<br/>
              5. Il résulte de tout ce qui précède que la requête de Mme A... ne peut qu'être rejetée, y compris en ce qu'elle comporte des conclusions présentées au titre de l'article L. 761-1 du code de justice administrative. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par le Conseil national de l'ordre des médecins au titre des mêmes dispositions. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de Mme A... est rejetée.<br/>
Article 2 : Les conclusions présentées par le Conseil national de l'ordre des médecins au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à Mme B... A... et au Conseil national de l'ordre des médecins.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
