<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037599988</ID>
<ANCIEN_ID>JG_L_2018_11_000000413206</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/59/99/CETATEXT000037599988.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 6ème chambres réunies, 09/11/2018, 413206</TITRE>
<DATE_DEC>2018-11-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>413206</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SEVAUX, MATHONNET ; SCP ROCHETEAU, UZAN-SARANO ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Guillaume Leforestier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:413206.20181109</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La Mutualité Finistère et Morbihan a demandé au tribunal administratif de Rennes de condamner l'Etablissement français du sang (EFS) à la garantir des condamnations prononcées à son encontre par la juridiction judiciaire du fait de la contamination transfusionnelle par les virus de l'hépatite B et C dont Mme A...B...a été victime au cours de l'intervention qu'elle a subie à la clinique Saint-Michel Sainte-Anne de Quimper le 4 novembre 1976 et lors de sa prise en charge jusqu'au 13 novembre 1976. Par un jugement n° 1202879 du 11 juin 2015, le tribunal administratif de Rennes a fait droit à sa demande en condamnant l'Etablissement français du sang à lui verser la somme de 323 810 euros.<br/>
<br/>
              Par un arrêt n° 15NT02361 du 9 juin 2017, la cour administrative d'appel de Nantes a, sur appel de l'EFS, annulé ce jugement et rejeté la demande de la Mutualité Finistère et Morbihan.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 9 août 2017, 2 novembre 2017 et 7 mai 2018 au secrétariat du contentieux du Conseil d'Etat, la Mutualité Finistère et Morbihan demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'EFS la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le code civil ;<br/>
<br/>
              - le code de la santé publique ;<br/>
<br/>
              - la loi n° 85-677 du 5 juillet 1985 ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Leforestier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de la Mutualité Finistère et Morbihan, à la SCP Piwnica, Molinié, avocat de l'Etablissement français du sang et à la SCP Sevaux Mathonnet, avocat de l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme B..., attribuant sa contamination par les virus de l'hépatite B et C à des transfusions sanguines subies en 1976 et 1977 à la clinique Saint-Michel Sainte-Anne de Quimper, a recherché la responsabilité civile de cet établissement ; que la Mutualité Finistère et Morbihan, en qualité de gestionnaire de la clinique, a été condamnée par un jugement du tribunal de grande instance de Quimper du 20 mai 2014, confirmé par un arrêt de la cour d'appel de Rennes du 17 février 2016, à verser à l'intéressée la somme de 264 985,49 euros et à la caisse primaire d'assurance maladie du Finistère la somme de 117 574,09 euros en remboursement de ses débours et de l'indemnité forfaitaire de gestion ; que la Mutualité Finistère et Morbihan a, le 17 juillet 2012, saisi le tribunal administratif de Rennes d'une demande tendant à ce que l'Établissement français du sang, venu aux droits et obligations de l'établissement de transfusion sanguine qui avait fourni les produits contaminés, la garantisse de ces sommes ; que, par un jugement du 11 juin 2015, le tribunal administratif a partiellement fait droit à sa demande en condamnant l'EFS à lui rembourser la somme de 206 235 euros au titre des préjudices de Mme B... et la somme de 117 574,09 euros au titre des débours de la CPAM du Finistère ; que, par un arrêt du 9 juin 2017, la cour administrative d'appel de Nantes, statuant sur appel de l'EFS, a annulé ce jugement et rejeté la demande de la Mutualité Finistère et Morbihan ; que la Mutualité Finistère et Morbihan se pourvoit en cassation contre cet arrêt ;<br/>
<br/>
              2. Considérant que l'article L. 1221-14 du code de la santé publique, dans sa rédaction applicable au litige, permet aux victimes d'une contamination par le virus de l'hépatite B ou C résultant de la transfusion de produits sanguins d'être indemnisées par l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) au titre de la solidarité nationale ; que le même article prévoit que l'ONIAM et les tiers payeurs qui ont, le cas échéant, également indemnisé la victime peuvent, même en l'absence de faute, exercer une action subrogatoire contre l'Etablissement français du sang, venu aux droits et obligations des établissements de transfusion sanguine, sauf " si l'établissement de transfusion sanguine n'est pas assuré, si sa couverture d'assurance est épuisée ou encore dans le cas où le délai de validité de sa couverture est expiré " ; que ces dispositions ne font pas obstacle à ce qu'un établissement de santé condamné par la juridiction judiciaire à indemniser la victime d'une contamination transfusionnelle exerce contre l'Etablissement français du sang, en tant que co-auteur du dommage, l'action subrogatoire dont il est détenteur  en vertu des dispositions de l'article 1251 du code civil qui, dans sa rédaction applicable au litige, disposait que : " La subrogation a lieu de plein droit :/ (...) 3° Au profit de celui qui, étant tenu avec d'autres ou pour d'autres au paiement de la dette, avait intérêt de l'acquitter " ; <br/>
<br/>
              3. Considérant que, pour rejeter la demande de la Mutualité Finistère et Morbihan, la cour administrative d'appel de Nantes a retenu que le recours subrogatoire prévu par l'article L. 1221-14 du code de la santé publique à l'encontre de l'Etablissement français du sang n'était ouvert qu'au bénéfice des tiers payeurs énumérés à l'article 29 de la loi du 5 juillet 1985 visée ci-dessus, au nombre desquels la requérante ne figure pas ; qu'en statuant ainsi, alors que la Mutualité Finistère et Morbihan, qui avait indemnisé la victime de la contamination transfusionnelle, pouvait à ce titre exercer un recours subrogatoire contre l'Etablissement français du sang en application de l'article 1251 du code civil cité ci-dessus, la cour a commis une erreur de droit ; que, par suite, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, son arrêt doit être annulé ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etablissement français du sang une somme de 4 000 euros à verser à la Mutualité Finistère et Morbihan au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que les mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la Mutualité Finistère et Morbihan qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 9 juin 2017 de la cour administrative d'appel de Nantes est annulé.<br/>
<br/>
		Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nantes.<br/>
<br/>
Article 3 : L'Etablissement français du sang versera à la Mutualité Finistère et Morbihan la somme de 4 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions présentées par l'Etablissement français du sang au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à la Mutualité Finistère et Morbihan, à l'Etablissement français du sang, à l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales et à la caisse primaire d'assurance maladie du Finistère Morbihan.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-02-01-02 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICE PUBLIC DE SANTÉ. DONS DU SANG. - ACTION SUBROGATOIRE CONTRE L'ETABLISSEMENT FRANÇAIS DU SANG - ACTION OUVERTE, DANS CERTAINES CONDITIONS, À L'ONIAM (ART. L. 1221-14 DU CSP, DANS SA RÉDACTION ALORS EN VIGUEUR) - ACTION OUVERTE À UN ÉTABLISSEMENT DE SANTÉ CONDAMNÉ À INDEMNISER LA VICTIME D'UNE CONTAMINATION TRANSFUSIONNELLE - EXISTENCE, EN VERTU DE L'ARTICLE 1251 DU CODE CIVIL (DANS SA RÉDACTION ALORS EN VIGUEUR) [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-05-03 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RECOURS OUVERTS AUX DÉBITEURS DE L'INDEMNITÉ, AUX ASSUREURS DE LA VICTIME ET AUX CAISSES DE SÉCURITÉ SOCIALE. SUBROGATION. - INDEMNISATION D'UNE VICTIME D'UNE CONTAMINATION TRANSFUSIONNELLE - ACTION SUBROGATOIRE CONTRE L'EFS - ACTION OUVERTE, DANS CERTAINES CONDITIONS, À L'ONIAM (ART. L. 1221-14 DU CSP ALORS EN VIGUEUR) - ACTION OUVERTE À UN ÉTABLISSEMENT DE SANTÉ CONDAMNÉ À INDEMNISER LA VICTIME - EXISTENCE, EN VERTU DE L'ARTICLE 1251 DU CODE CIVIL (DANS SA RÉDACTION ALORS EN VIGUEUR) [RJ1].
</SCT>
<ANA ID="9A"> 60-02-01-02 L'article L. 1221-14 du code de la santé publique (CSP), dans sa rédaction alors en vigueur, permet aux victimes d'une contamination par le virus de l'hépatite B ou C résultant de la transfusion de produits sanguins d'être indemnisées par l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) au titre de la solidarité nationale. Le même article prévoit que l'ONIAM et les tiers payeurs qui ont, le cas échéant, également indemnisé la victime peuvent, même en l'absence de faute, exercer une action subrogatoire contre l'Etablissement français du sang (EFS), venu aux droits et obligations des établissements de transfusion sanguine, sauf si l'établissement de transfusion sanguine n'est pas assuré, si sa couverture d'assurance est épuisée ou encore dans le cas où le délai de validité de sa couverture est expiré.... ...Ces dispositions ne font pas obstacle à ce qu'un établissement de santé condamné par la juridiction judiciaire à indemniser la victime d'une contamination transfusionnelle exerce contre l'EFS en tant que co-auteur du dommage, l'action subrogatoire dont il est détenteur en vertu des dispositions de l'article 1251 du code civil qui, dans sa rédaction alors en vigueur, disposait que : La subrogation a lieu de plein droit :/ (&#133;) 3° Au profit de celui qui, étant tenu avec d'autres ou pour d'autres au paiement de la dette, avait intérêt de l'acquitter.</ANA>
<ANA ID="9B"> 60-05-03 L'article L. 1221-14 du code de la santé publique (CSP), dans sa rédaction alors en vigueur, permet aux victimes d'une contamination par le virus de l'hépatite B ou C résultant de la transfusion de produits sanguins d'être indemnisées par l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) au titre de la solidarité nationale. Le même article prévoit que l'ONIAM et les tiers payeurs qui ont, le cas échéant, également indemnisé la victime peuvent, même en l'absence de faute, exercer une action subrogatoire contre l'Etablissement français du sang (EFS), venu aux droits et obligations des établissements de transfusion sanguine, sauf si l'établissement de transfusion sanguine n'est pas assuré, si sa couverture d'assurance est épuisée ou encore dans le cas où le délai de validité de sa couverture est expiré.... ...Ces dispositions ne font pas obstacle à ce qu'un établissement de santé condamné par la juridiction judiciaire à indemniser la victime d'une contamination transfusionnelle exerce contre l'EFS en tant que co-auteur du dommage, l'action subrogatoire dont il est détenteur en vertu des dispositions de l'article 1251 du code civil qui, dans sa rédaction alors en vigueur, disposait que : La subrogation a lieu de plein droit :/ (&#133;) 3° Au profit de celui qui, étant tenu avec d'autres ou pour d'autres au paiement de la dette, avait intérêt de l'acquitter.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 31 décembre 2008, Société Foncière Ariane, n° 294078, p. 498.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
