<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043751439</ID>
<ANCIEN_ID>JG_L_2021_07_000000437608</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/75/14/CETATEXT000043751439.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 01/07/2021, 437608, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>437608</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>HAAS</AVOCATS>
<RAPPORTEUR>M. Jean-Luc Prévoteau</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Céline  Guibé</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:437608.20210701</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              I. M. B... A... a demandé au tribunal administratif de Lille, en premier lieu, de prononcer la décharge, en droits et pénalités, des cotisations supplémentaires à l'impôt sur le revenu auxquelles il a été assujetti au titre des années 2010 et 2011, en deuxième lieu, de lui accorder le sursis de paiement des impositions contestées et de le dispenser de constituer une garantie à cet effet, et en troisième et dernier lieu, d'ordonner une expertise en vue de déterminer la part de chiffre d'affaires réalisée sur le territoire français par l'entreprise de droit belge Transport Rapide, et de procéder au calcul de l'impôt dû sur le montant de cette seule part de chiffre d'affaires ainsi déterminée. Par une ordonnance n° 1807777 du 1er juillet 2019, le président de la 4ème chambre du tribunal administratif de Lille a déclaré qu'il n'y avait pas lieu de statuer sur les conclusions à fin de sursis de paiement et rejeté le surplus des conclusions de sa demande.<br/>
<br/>
              Par une ordonnance n°19DA02013 du 14 novembre 2019, le président de la 4ème chambre de la cour administrative d'appel de Douai a rejeté l'appel formé par M. A... contre cette ordonnance.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés, sous le n° 437608, les 14 janvier 2019 et 20 mai 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance du président de la 4ème chambre de la cour administrative d'appel de Douai ;<br/>
     2°) de mettre à la charge l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              II. M. B... A... a demandé au tribunal administratif de Lille, en premier lieu, de prononcer la décharge, en droits et pénalités, des cotisations supplémentaires à l'impôt sur le revenu auxquelles il a été assujetti au titre des années 2012, 2014 et 2015, en deuxième lieu, de lui accorder le sursis de paiement des impositions contestées et de le dispenser de constituer une garantie à cet effet, et en troisième et dernier lieu, d'ordonner une expertise en vue de déterminer la part de chiffre d'affaires réalisée sur le territoire français par l'entreprise de droit belge Transport Rapide, et de procéder au calcul de l'impôt dû sur le montant de cette seule part de chiffre d'affaires ainsi déterminée. Par une ordonnance n° 1807758 du 1er juillet 2019, le président de la 4ème chambre du tribunal administratif de Lille a déclaré qu'il n'y avait pas lieu de statuer sur les conclusions à fin de sursis de paiement et rejeté le surplus des conclusions de sa demande.<br/>
<br/>
              Par une ordonnance n°19DA02020 du 14 novembre 2019, le président de la 4ème chambre de la cour administrative d'appel de Douai a rejeté l'appel formé par M. A... contre cette ordonnance.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés, sous le n° 437609, les 14 janvier 2019 et 20 mai 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance du président de la 4ème chambre de la cour administrative d'appel de Douai ;<br/>
<br/>
              2°) de mettre à la charge l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              -la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code général des impôts et le livre des procédures fiscales ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Prévoteau, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Céline Guibé, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à Me Haas, avocat de M. B... A... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Les pourvois visés ci-dessus présentent à juger les mêmes questions. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2. Aux termes de l'article R. 222-1 du code de justice administrative, dans sa rédaction applicable au litige : " (...) les présidents des formations de jugement des tribunaux peuvent (...) par ordonnance : / (...) 4° Rejeter les requêtes manifestement irrecevables, lorsque la juridiction n'est pas tenue d'inviter leur auteur à les régulariser ou qu'elles n'ont pas été régularisées à l'expiration du délai imparti par une demande en ce sens ; / (...) " ;<br/>
<br/>
              3. Il ressort des pièces des procédures devant le tribunal administratif de Lille que, dans le mémoire en défense qu'elle a présenté en réponse à la communication de chacune des requêtes aux fins de décharges présentées par M. A..., l'administration a opposé à celles-ci une fin de non-recevoir tirée de ce que les courriers qui lui avaient été adressés le 3 avril 2018 ne pouvaient être regardés comme des réclamations contentieuses. Ces mémoires en défense ont été communiqués à l'intéressé avec les indications selon lesquelles : " Dans le cas où ce mémoire appellerait des observations de votre part, celles-ci devront être produites en deux exemplaires dans les meilleurs délais " et " Afin de ne pas retarder la mise en état d'être jugé de votre dossier, vous avez tout intérêt, si vous l'estimez utile, à produire ces observations aussi rapidement que possible ". De telles indications ne permettaient pas à M. A..., en l'absence de date déterminée, de connaître de façon certaine le délai dans lequel il était invité à produire ses observations en réplique et, en l'absence d'audience, il n'a, en outre, pas été mis en mesure de les faire éventuellement valoir avant que le président de la 4ème chambre du tribunal administratif de Lille ne rejette ses requêtes à raison de l'irrecevabilité précitée. Par suite, en jugeant que les ordonnances ainsi rendues n'étaient pas entachées d'une méconnaissance du principe du contradictoire, le président de la 4ème chambre de la cour administrative d'appel de Douai a commis une erreur de droit. <br/>
<br/>
              4. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur les autres moyens des pourvois, que M. A... est fondé à demander l'annulation des ordonnances qu'il attaque. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions de M. A... présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Les ordonnances du 14 novembre 2019 de la cour administrative d'appel de Douai sont annulées.<br/>
Articles 2 : Les affaires sont renvoyées devant la cour administrative d'appel de Douai.<br/>
Article 3 : Les conclusions présentées par M. A... au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à M. B... A... et au ministre de l'économie, des finances et de la relance.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
