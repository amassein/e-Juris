<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042143138</ID>
<ANCIEN_ID>JG_L_2020_07_000000436905</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/14/31/CETATEXT000042143138.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 22/07/2020, 436905, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>436905</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Liza Bellulo</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:436905.20200722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU> Vu la procédure suivante :<br/>
<br/>
              La société civile immobilière (SCI) La Rampe a demandé au tribunal administratif de Montpellier de prononcer la décharge des cotisations de taxe foncière sur les propriétés bâties auxquelles elle a été assujettie au titre des années 2015, 2016, 2017 et 2018 dans les rôles de la commune de Béziers (Hérault). Par un jugement n° 1806067 du 28 octobre 2019, le magistrat désigné par le président du tribunal administratif de Montpellier a accordé à la société la décharge de la cotisation de taxe foncière sur les propriétés bâties établie au titre de l'année 2018 et rejeté le surplus des conclusions de la demande.<br/>
<br/>
              Par un pourvoi, enregistré le 19 décembre 2019 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'action et des comptes publics demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'article 1er de ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond dans cette mesure, de rejeter la demande de la société La Rampe.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Liza Bellulo, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société La Rampe a formé une réclamation tendant à ce que lui soit accordé, au titre d'un immeuble dont elle est propriétaire à Béziers (Hérault), le bénéfice de l'exonération de taxe foncière sur les propriétés bâties prévue par l'article 1383 C ter du code général des impôts au profit des immeubles situés dans les quartiers prioritaires de la politique de la ville et rattachés à un établissement remplissant les conditions pour bénéficier de l'exonération de cotisation foncière des entreprises prévue au I septies de l'article 1466 A du même code. Le ministre de l'action et des comptes publics se pourvoit en cassation contre l'article 1er du jugement du 28 octobre 2019 par lequel le tribunal administratif de Montpellier a prononcé la décharge de la cotisation de taxe foncière sur les propriétés bâties mise à la charge de la société La Rampe au titre de l'année 2018 à raison de cet immeuble.<br/>
<br/>
              2. Aux termes de l'article 1383 C ter du code général des impôts, dans sa rédaction applicable aux impositions en litige : " Sauf délibération contraire de la collectivité territoriale ou de l'établissement public de coopération intercommunale doté d'une fiscalité propre, prise dans les conditions prévues au I de l'article 1639 A bis, les immeubles situés dans les quartiers prioritaires de la politique de la ville définis à l'article 5 de la loi n° 2014-173 du 21 février 2014 de programmation pour la ville et la cohésion urbaine sont exonérés de taxe foncière sur les propriétés bâties pour une durée de cinq ans. (...) L'exonération s'applique aux immeubles existant au 1er janvier 2017 et rattachés à cette même date à un établissement remplissant les conditions pour bénéficier de l'exonération de cotisation foncière des entreprises prévue au I septies de l'article 1466 A (...) ". Selon le 7ème alinéa du I septies de l'article 1466 A du même code : " (...) L'exonération s'applique lorsque les conditions suivantes sont remplies : / 1° L'entreprise exerce une activité commerciale (...) ". Pour l'application de ces dispositions, la condition mentionnée au 1° du 7ème alinéa du I septies de l'article 1466 A du code général des impôts s'apprécie, s'agissant d'un immeuble donné en location, au niveau du preneur et non du bailleur.<br/>
<br/>
              3. Pour faire droit à la demande de la société La Rampe, le magistrat désigné par le président du tribunal administratif de Montpellier en application de l'article R. 222-13 du code de justice administrative s'est fondé sur ce que les locaux en litige étaient, d'une part, situés dans un quartier prioritaire de la politique de la ville de Béziers et, d'autre part, donnés en location à la société de fait de Michel et Xavier de Clock, laquelle exerçait une activité d'agent d'assurances, qu'il a regardée comme entrant dans le champ des dispositions du 1° du 7ème alinéa du I septies de l'article 1466 A du code général des impôts. En regardant cette activité comme commerciale au seul motif qu'elle était liée à des opérations d'assurance, alors que les revenus que perçoivent les agents généraux d'assurance, provenant des commissions qui leur sont versées par les compagnies d'assurance qu'ils représentent, ont la nature de bénéfices non commerciaux, et sans rechercher si la société de fait en cause exerçait son activité dans des conditions lui conférant un caractère commercial, notamment en fournissant des services de courtage, le magistrat désigné du tribunal administratif de Montpellier a commis une erreur de droit. <br/>
<br/>
              4. Par suite, sans qu'il soit besoin de se prononcer sur l'autre moyen de son pourvoi, le ministre de l'action et des comptes publics est fondé à demander l'annulation de l'article 1er du jugement qu'il attaque.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 1er du jugement du 28 octobre 2019 du tribunal administratif de Montpellier est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée dans cette mesure au tribunal administratif de Montpellier.<br/>
Article 3 : La présente décision sera notifiée au ministre de l'économie, des finances et de la relance et à la société civile immobilière La Rampe.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
