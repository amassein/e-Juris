<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029626758</ID>
<ANCIEN_ID>JG_L_2014_10_000000380928</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/62/67/CETATEXT000029626758.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème SSJS, 24/10/2014, 380928, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-10-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>380928</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Guillaume Odinet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:380928.20141024</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Vu la requête, enregistrée le 4 juin 2014 au secrétariat du contentieux du Conseil d'Etat, présentée par M. F...D..., demeurant ...; demande au Conseil d'Etat, d'une part, d'annuler l'élection de M. B... A...en qualité de conseiller consulaire dans la circonscription électorale d'Annaba (Algérie) à l'issue du scrutin qui s'est tenu le 25 mai 2014 et, d'autre part, de déclarer élu le candidat de la liste " Ensemble, ouvrons les portes des consulats et des écoles françaises " venant en deuxième position.<br/>
<br/>
              Elle soutient qu'en mentionnant leur qualité d'agent consulaire et de retraitée du consulat de France à Annaba sur les bulletins de vote ainsi que sur leur circulaire électorale, M. B... A..., tête de la liste " Union des Français en Algérie ", et Mme E...C..., candidate sur cette liste en quatrième position, ont méconnu les dispositions du mémento établi à l'attention des candidats par le ministère des affaires étrangères et européennes dans son édition du 7 mars 2014 et que cette méconnaissance a vicié le résultat des élections.<br/>
<br/>
              Le ministre des affaires étrangères et du développement international a présenté des observations, enregistrées le 17 juillet 2014.<br/>
<br/>
              Par un mémoire en défense, enregistré le 2 août 2014, M. A... conclut au rejet de la requête. Il déclare qu'il reprend à son compte les arguments avancés par le ministre des affaires étrangères et du développement international.<br/>
<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - le code électoral ;<br/>
              - la loi n° 2013-659 du 22 juillet 2013 ;<br/>
              - le décret n° 2014-290 du 4 mars 2014 ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Odinet, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. A l'issue des opérations électorales qui se sont déroulées le 25 mai 2014 dans la 2ème circonscription d'Algérie, M. B... A..., tête de la liste " Union des Français en Algérie ", a été proclamé élu en qualité de conseiller consulaire. Mme F...D..., tête de la liste " Ensemble, ouvrons les portes des consulats et des écoles françaises ", demande l'annulation de l'élection de M. B... A...et que soit déclaré élu le candidat venant en deuxième position sur sa liste.<br/>
<br/>
              Sur le grief tiré de la méconnaissance des règles relatives aux bulletins de vote :<br/>
<br/>
              2. Aux termes du II de l'article 21 de la loi du 22 juillet 2013 relative à la représentation des Français établis hors de France : " Les candidats ou listes de candidats remettent leurs bulletins de vote au chef-lieu de leur circonscription électorale. / (...) / Dans le respect des dispositions du III de l'article 19 et sous réserve des dispositions du second alinéa du II de l'article 20, le bulletin de vote comporte, dans les circonscriptions électorales où plus d'un siège est à pourvoir, le titre de la liste et les noms des candidats, dans l'ordre de leur présentation. ". Aux termes de l'article 6 du décret du 4 mars 2014 portant dispositions électorales relatives à la représentation des Français établis hors de France : " (...) / Dans les circonscriptions où l'élection a lieu à la représentation proportionnelle, le bulletin de vote doit comporter le titre de la liste, tel qu'il a été indiqué dans la déclaration de candidature, et les noms et prénoms des candidats cités dans l'ordre de ladite déclaration. Le nom de chacun des candidats est précédé de son numéro d'ordre. ". Aux termes de l'article R. 30 du code électoral, rendu applicable par l'article 3 du décret du 4 mars 2014 : " (...) Les bulletins ne peuvent pas comporter d'autres noms de personne que celui du ou des candidats ou de leurs remplaçants éventuels. (...) ". <br/>
<br/>
              3. Ces dispositions, qui répondent notamment à la nécessité d'éviter une éventuelle confusion dans l'esprit des électeurs sur l'identité du candidat et les enjeux du scrutin, s'appliquent aux patronymes. Elles n'ont pas été méconnues par la liste " Union des Français en Algérie " au seul motif qu'elle a fait figurer sur ses bulletins de vote l'indication des fonctions des candidats, en particulier celles d'agent consulaire de M. A... et de retraitée du consulat de France à Annaba de MmeC..., candidate en quatrième position. De telles indications, dont il n'est pas soutenu qu'elles seraient erronées, ne peuvent être regardées, en l'espèce, comme une manoeuvre susceptible d'avoir altéré la sincérité du scrutin du 25 mai 2014.<br/>
<br/>
              Sur le grief tiré de la méconnaissance des règles relatives aux circulaires électorales :<br/>
<br/>
              4. Aux termes de l'article 4 du décret du 4 mars 2014 : " Les circulaires dématérialisées prévues au I de l'article 21 de la loi du 22 juillet 2013 susvisée sont transmises au ministre des affaires étrangères au plus tard le quatrième lundi qui précède le jour de l'élection. Un arrêté du ministre des affaires étrangères fixe les caractéristiques techniques auxquelles elles doivent se conformer ainsi que leurs modalités de transmission (...) ". Aux termes de l'article 1er de l'arrêté du 4 mars 2014 fixant les caractéristiques techniques et les modalités de transmission des circulaires dématérialisées prévues aux articles 4 et 25 du décret n° 2014-290 du 4 mars 2014 portant dispositions électorales relatives à la représentation des Français établis hors de France : " Les circulaires dématérialisées prévues au I de l'article 21 de la loi du 22 juillet 2013 susvisée doivent être fournies au format " pdf " et leur volume ne peut excéder deux mégaoctets. / Elles ne peuvent pas contenir de lien hypertexte actif. ". Ni ces dispositions, ni aucun autre texte législatif ou réglementaire, ne font obstacle à ce que la profession ou l'ancienne profession d'un candidat aux élections consulaires soit mentionnée sur une circulaire électorale. Une telle indication ne peut être regardée, en l'espèce, comme une manoeuvre susceptible d'avoir altéré la sincérité du scrutin du 25 mai 2014. Enfin, Mme D...ne peut utilement se prévaloir, sur ce point, d'indications contenues dans la brochure dénommée " Elections des conseillers consulaires - Mémento à l'usage des candidats ", diffusée par le ministre des affaires étrangères, qui sont dépourvues de valeur réglementaire.<br/>
<br/>
              5. Il résulte de tout ce qui précède que la protestation de Mme D...doit être rejetée.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La protestation de Mme D...est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à Mme F...D...et à M. B... A....<br/>
Copie en sera adressée pour information au ministre des affaires étrangères et du développement international.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
