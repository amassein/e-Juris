<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026504645</ID>
<ANCIEN_ID>JG_L_2012_10_000000354874</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/50/46/CETATEXT000026504645.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  7ème sous-section jugeant seule, 17/10/2012, 354874, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-10-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>354874</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 7ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Rémy Schwartz</PRESIDENT>
<AVOCATS>BERTRAND</AVOCATS>
<RAPPORTEUR>M. Fabrice Aubert</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:354874.20121017</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 14 décembre 2011 et 9 mars 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. Hocine A, demeurant ... ; M. A demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09PA01013 du 7 juin 2010 par lequel la cour administrative d'appel de Paris a rejeté sa requête tendant, d'une part, à l'annulation de l'ordonnance n° 0801671 du 15 mai 2008 par laquelle la présidente du tribunal administratif de Paris a rejeté sa demande tendant à l'annulation de la décision du 22 octobre 2007 du préfet de la région Ile-de-France, préfet de Paris, lui refusant le bénéfice de la carte du combattant, d'autre part, à l'annulation de ladite décision et, enfin, à ce qu'il soit enjoint au préfet de lui délivrer une carte du combattant ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat, au profit de Me Bertrand, son avocat, la somme de 3 000 euros en application des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution et, notamment, ses articles 61-1 et 62 ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu le code des pensions militaires d'invalidité et des victimes de guerre ;<br/>
<br/>
              Vu la décision n° 2010-18 QPC du 23 juillet 2010 du Conseil constitutionnel ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Fabrice Aubert, Auditeur,  <br/>
<br/>
              - les observations de Me Bertrand, avocat de M. A,<br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à Me Bertrand, avocat de M. A ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. A, de nationalité algérienne et résidant en Algérie, a saisi le préfet de la région <br/>
Ile-de-France, préfet de Paris, d'une demande d'attribution de la carte du combattant au titre de son appartenance aux forces françaises supplétives durant la guerre d'Algérie ; qu'il se pourvoit en cassation contre l'arrêt du 7 juin 2010 par lequel la cour administrative d'appel de Paris a rejeté son appel tendant à l'annulation de l'ordonnance du 15 mai 2008 de la présidente du tribunal administratif de Paris rejetant sa demande d'annulation de la décision du 22 octobre 2007 par laquelle le préfet de la région Ile-de-France, préfet de Paris, a refusé de lui accorder le bénéfice de la carte du combattant, au motif qu'il ne remplissait pas les conditions de nationalité française ou de résidence sur le territoire français prévues à l'article L. 253 bis du code des pensions militaires d'invalidité et des victimes de guerre pour l'attribution de cette carte ;<br/>
<br/>
              2. Considérant qu'aux termes du premier alinéa de l'article 61-1 de la Constitution : " Lorsque, à l'occasion d'une instance en cours devant une juridiction, il est soutenu qu'une disposition législative porte atteinte aux droits et libertés que la Constitution garantit, le Conseil constitutionnel peut être saisi de cette question sur renvoi du Conseil d'Etat ou de la Cour de cassation. " ; qu'aux termes du deuxième alinéa de son article 62 : " Une disposition déclarée inconstitutionnelle sur le fondement de l'article 61-1 est abrogée à compter de la publication de la décision du Conseil constitutionnel ou d'une date ultérieure fixée par cette décision. Le Conseil constitutionnel détermine les conditions et limites dans lesquelles les effets que la disposition a produits sont susceptibles d'être remis en cause " ; qu'enfin, aux termes du troisième alinéa du même article : " Les décisions du Conseil constitutionnel ne sont susceptibles d'aucun recours. Elles s'imposent aux pouvoirs publics et à toutes les autorités administratives et juridictionnelles. " ;<br/>
<br/>
              3. Considérant qu'il résulte des dispositions précitées de l'article 62 de la Constitution qu'une disposition législative déclarée contraire à la Constitution sur le fondement de l'article 61-1 n'est pas annulée rétroactivement mais abrogée pour l'avenir à compter de la publication de la décision du Conseil constitutionnel ou d'une date ultérieure fixée par cette décision ; que par sa décision n° 2010-108 QPC en date du 25 mars 2011, le Conseil constitutionnel a jugé qu' " en principe, la déclaration d'inconstitutionnalité doit bénéficier à l'auteur de la question prioritaire de constitutionnalité et la disposition déclarée contraire à la Constitution ne peut être appliquée dans les instances en cours à la date de la publication de la décision du Conseil constitutionnel " ; qu'il appartient dès lors au juge, saisi d'un litige relatif aux effets produits par la disposition déclarée inconstitutionnelle, de les remettre en cause en écartant, pour la solution de ce litige, le cas échéant d'office, cette disposition, dans les conditions et limites éventuellement fixées par le Conseil constitutionnel ou le législateur ;<br/>
<br/>
              4. Considérant que, par sa décision n° 2010-18 QPC du 23 juillet 2010, le Conseil constitutionnel a déclaré contraires à la Constitution les mots " possédant la nationalité française à la date de la présentation de leur demande ou domiciliés en France à la même date " du troisième alinéa de l'article 235 bis du code des pensions militaires d'invalidité et des victimes de guerre, au motif que le législateur ne pouvait établir, au regard de l'objet de la loi, une différence de traitement à raison de la nationalité ou du domicile entre les membres des forces supplétives pour l'octroi de la carte du combattant ; <br/>
<br/>
              5. Considérant que pour rejeter les demandes de M. A, la cour administrative d'appel de Paris s'est fondée sur les dispositions de l'article L. 253 bis citées <br/>
ci-dessus ; qu'afin de préserver l'effet utile de la décision n° 2010-18 QPC du Conseil constitutionnel pour la solution de l'instance ouverte par M. A, il y a lieu d'annuler l'arrêt attaqué ; <br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond au titre de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              7. Considérant que, pour rejeter la demande d'attribution de la carte du combattant présentée par M. A, le préfet de la région Ile-de-France, préfet de Paris, s'est seulement fondé sur la circonstance que l'intéressé ne remplissait pas les conditions de nationalité et de résidence définies par les dispositions déclarées inconstitutionnelles de l'article L. 253 bis du code des pensions militaires d'invalidité et des victimes de guerre alors qu'il ressort des pièces du dossier, et notamment du " mémoire des états de services " produit par M. A, que ce dernier a servi en qualité de harki entre le 1er février 1961 et le 31 mars 1962 au sein des forces supplétives françaises ; qu'il n'est plus contesté devant le juge que M. A remplit ainsi les conditions d'obtention de la carte du combattant définies aux articles L. 253 et R. 224 du code des pensions militaires d'invalidité et des victimes de guerre ; que, par suite, M. A est fondé à soutenir que c'est à tort que par l'ordonnance attaquée la présidente du tribunal administratif a rejeté ses conclusions tendant à l'annulation de la décision du préfet du 22 octobre 2007 ; que, par suite, il y a lieu d'annuler la décision du préfet de la région <br/>
Ile-de-France du 22 octobre 2007 refusant de lui attribuer la carte du combattant ;<br/>
<br/>
              8. Considérant que l'annulation de la décision préfectorale du 22 octobre 2007 implique nécessairement d'attribuer la carte du combattant à M. A ; qu'il y a lieu d'enjoindre à l'administration de lui délivrer cette carte dans un délai de quatre mois à compter de la présente décision ; <br/>
<br/>
              9. Considérant que M. A a obtenu le bénéfice de l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce et sous réserve que Me Bertrand, avocat de M. A, renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat le versement à Me Bertrand de la somme de 3 000 euros au titre de ces dispositions ;  <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 7 juin 2010 de la cour administrative d'appel de Paris et l'ordonnance du 15 mai 2008 de la présidente du tribunal administratif de Paris sont annulés.<br/>
Article 2 : La décision du préfet de la région Ile-de-France, préfet de Paris, du 22 octobre 2007 est annulée.<br/>
Article 3 : Il est enjoint au ministre de la défense de faire délivrer à M. A, dans un délai de quatre mois à compter de la notification de la présente décision, la carte du combattant.<br/>
Article 4 : L'Etat versera à Me Bertrand, avocat de M. A, une somme de 3 000 euros en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve de renoncer à percevoir la somme correspondant à la part contributive de l'Etat. <br/>
Article 5 : La présente décision sera notifiée à M. Hocine A et au ministre de la défense.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
