<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041757074</ID>
<ANCIEN_ID>JG_L_2020_03_000000421149</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/75/70/CETATEXT000041757074.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 25/03/2020, 421149, Publié au recueil Lebon</TITRE>
<DATE_DEC>2020-03-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421149</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Céline Roux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Raphaël Chambon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:421149.20200325</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 31 mai et 27 août 2018, et le 27 février 2019 au secrétariat du contentieux du Conseil d'Etat, le Syndicat de la juridiction administrative demande au Conseil d'Etat d'annuler pour excès de pouvoir la décision du 16 mars 2018 par laquelle le vice-président du Conseil d'Etat a modifié la charte de déontologie des membres de la juridiction administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - la loi n° 2016-483 du 20 avril 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Céline Roux, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Par une décision du 14 mars 2017, le vice-président du Conseil d'Etat, après avoir recueilli l'avis du collège de déontologie de la juridiction administrative, a adopté la charte de déontologie des membres de la juridiction administrative. Il a modifié cette charte par une autre décision du 16 mars 2018, en remplaçant, au sein du chapitre IV de la charte intitulé " Devoir de réserve dans l'expression publique ", dans sa rubrique " Bonnes pratiques ", le paragraphe 47 par de nouveaux paragraphes numérotés 47 à 47-6 portant sur l'usage des réseaux sociaux sur Internet. Le Syndicat de la juridiction administrative demande l'annulation pour excès de pouvoir de la décision modificative du 16 mars 2018.<br/>
<br/>
              Sur la portée de la charte de déontologie des membres de la juridiction administrative :<br/>
<br/>
              2. La loi du 20 avril 2016 relative à la déontologie et aux droits et obligations des fonctionnaires a introduit dans le code de justice administrative des dispositions, figurant à l'article L. 131-2 pour les membres du Conseil d'Etat et à l'article L. 231-1-1 pour les magistrats des tribunaux administratifs et des cours administratives d'appel, aux termes desquelles les membres de la juridiction administrative " exercent leurs fonctions en toute indépendance, dignité, impartialité, intégrité et probité et se comportent de façon à prévenir tout doute légitime à cet égard. / Ils s'abstiennent de tout acte ou comportement à caractère public incompatible avec la réserve que leur imposent leurs fonctions (...) ". <br/>
<br/>
              3. La même loi a inséré dans le code de justice administrative un article L. 131-4, aux termes duquel : " Le vice-président du Conseil d'Etat établit, après avis du collège de déontologie de la juridiction administrative, une charte de déontologie énonçant les principes déontologiques et les bonnes pratiques propres à l'exercice des fonctions de membre de la juridiction administrative ". <br/>
<br/>
              4. Aux termes de l'article L. 131-6 du même code, issu de la même loi : " Le collège de déontologie de la juridiction administrative est chargé : / 1° De rendre un avis préalable à l'établissement de la charte de déontologie mentionnée à l'article L. 131-4 ; / 2° De rendre des avis sur toute question déontologique concernant personnellement un membre de la juridiction administrative (...) ; / 3° De formuler des recommandations de nature à éclairer les membres de la juridiction administrative sur l'application des principes déontologiques et de la charte de déontologie (...); / 4 ° De rendre des avis sur les déclarations d'intérêts qui lui sont transmises dans les conditions prévues aux articles L. 131-7 et L. 231-4-1 (...) ". <br/>
<br/>
              5. Il résulte de l'ensemble de ces dispositions que la charte de déontologie des membres de la juridiction administrative, qui n'a pas pour objet de se substituer aux principes et dispositions textuelles, notamment statutaires, régissant l'exercice de leurs fonctions, a vocation, outre à rappeler les principes et obligations d'ordre déontologique qui leur sont applicables, à préconiser des bonnes pratiques propres à en assurer le respect. Pour apprécier si le comportement d'un membre de la juridiction administrative traduit un manquement aux obligations déontologiques qui lui incombent, les bonnes pratiques ainsi recommandées sont susceptibles d'être prises en compte, sans pour autant que leur méconnaissance ne soit, en elle-même, constitutive d'un manquement disciplinaire. <br/>
<br/>
              Sur la légalité de la décision attaquée modifiant la charte de déontologie des membres de la juridiction administrative :<br/>
<br/>
              En ce qui concerne la légalité externe :<br/>
<br/>
              6. En premier lieu, les dispositions précédemment citées de l'article L. 131-4 du code de justice administrative, issues de la loi du 20 avril 2016, donnent, en tout état de cause, compétence au vice-président du Conseil d'Etat pour établir une charte de déontologie des membres de la juridiction administrative comportant l'énoncé des principes déontologiques et de bonnes pratiques propres à en assurer le respect. En vertu de ces dispositions, le vice-président peut rappeler les principes applicables et préconiser l'observation de bonnes pratiques non seulement aux membres du Conseil d'Etat et aux membres des tribunaux administratifs et des cours administratives d'appel en exercice, mais aussi, afin d'éviter que leur comportement affecte l'indépendance et le fonctionnement des juridictions administratives ou la dignité de leurs anciennes fonctions, aux membres honoraires des deux corps, pouvant se prévaloir de l'honorariat dans les conditions prévues à l'article 71 de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat, et, plus généralement, à tous les anciens membres.<br/>
<br/>
              7. Conformément à ce qu'énoncent les dispositions des articles L. 131-2 et L. 231-1-1 du code de justice administrative, citées au point 2, les membres du Conseil d'Etat et les magistrats des tribunaux administratifs et des cours administratives d'appel " s'abstiennent de tout acte ou comportement à caractère public incompatible avec la réserve que leur imposent leurs fonctions ". En adoptant, par la décision attaquée, des recommandations de bonnes pratiques destinées à assurer le respect, par les membres de la juridiction administrative lorsqu'ils s'expriment sur les réseaux sociaux, des exigences qui s'attachent à l'obligation de réserve à laquelle ils sont tenus, le vice-président du Conseil d'Etat n'a pas excédé sa compétence. <br/>
<br/>
              8. En second lieu, si le Syndicat de la juridiction administrative soutient que la charte aurait été irrégulièrement modifiée dans la mesure où la commission supérieure du Conseil d'Etat n'aurait pas été consultée préalablement à l'adoption de la décision attaquée, il ressort des pièces versées au dossier que le projet de modification de la charte de déontologie en cause a été examiné par la commission supérieure du Conseil d'Etat lors de sa séance du 11 décembre 2017. Par suite, le moyen tiré du défaut de consultation ne peut, en tout état de cause, qu'être écarté comme manquant en fait. <br/>
<br/>
              En ce qui concerne la légalité interne :<br/>
<br/>
              9. En premier lieu, les paragraphes critiqués de la charte de déontologie recommandent notamment aux membres de la juridiction administrative d'observer " la plus grande retenue (...) dans l'usage des réseaux sociaux sur Internet lorsque l'accès à ces réseaux n'est pas exclusivement réservé à un cercle privé aux accès protégés ". Ils précisent aussi que : " dans tous les cas, il convient de s'abstenir de prendre part à toute polémique qui, eu égard à son objet ou à son caractère, serait de nature à rejaillir sur l'institution ". S'agissant de l'actualité juridique et administrative, ils énoncent que les membres de la juridiction administrative doivent faire preuve, dans les propos qu'ils sont conduits à tenir sur les réseaux sociaux " d'une vigilance équivalente à celle qu'impliquerait leur publication dans une revue scientifique ". <br/>
<br/>
              10. Ces recommandations, formulées à titre de bonnes pratiques, visent, s'agissant de l'expression sur les réseaux sociaux et eu égard aux caractéristiques techniques de ces modes d'expression, à assurer le respect de l'obligation de réserve à laquelle les membres de la juridiction administrative sont tenus, laquelle vise à éviter que la diffusion de leurs propos porte atteinte à la nature et à la dignité des fonctions qu'ils exercent et à garantir l'indépendance, l'impartialité et le bon fonctionnement de la juridiction administrative. Ce faisant, elles ne portent pas à la liberté d'expression une atteinte qui méconnaîtrait les exigences découlant de l'article 11 de la Déclaration des droits de l'homme et du citoyen de 1789 ou celles qui résultent de l'article 10 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales.<br/>
<br/>
              11. En deuxième lieu, les paragraphes critiqués rappellent aussi que les informations diffusées sur le compte d'un réseau social ne sont susceptibles de constituer des correspondances privées que lorsque l'utilisateur a préalablement et correctement paramétré ce compte afin d'en contrôler l'accessibilité et de s'assurer du nombre restreint et de la fiabilité des contacts. Ce faisant, la charte recommande aux membres de la juridiction administrative qui utilisent les réseaux sociaux de régler les paramètres de leur compte de telle sorte que leur profil ne figure pas dans les résultats des moteurs de recherche et leur recommande de ne pas mentionner leur qualité de magistrat ou de membre du Conseil d'État lorsqu'ils renseignent leur profil sur un réseau social à vocation non professionnelle. <br/>
<br/>
              12. Ces recommandations de prudence n'ont ni pour objet ni pour effet d'interdire l'inscription et l'expression des membres de la juridiction administrative sur des réseaux sociaux et leur méconnaissance ne saurait, ainsi qu'il a été dit au point 5, en elle-même constituer un manquement disciplinaire. Elles visent seulement à prémunir les membres de la juridiction administrative contre le risque que des propos publiés sur les réseaux sociaux reçoivent une diffusion excédant celle qui avait été initialement envisagée par leur auteur et puissent exposer ce dernier, dans le cas où leur diffusion rejaillirait sur l'institution, à devoir répondre d'un éventuel manquement à leur obligation de réserve. Dans ces conditions, les recommandations de bonnes pratiques ainsi énoncées, destinées à garantir le respect de l'obligation de réserve sur les réseaux sociaux, ne portent pas à la liberté d'expression des membres de la juridiction administrative une atteinte disproportionnée.<br/>
<br/>
              13. En troisième lieu, les paragraphes critiqués de la charte recommandent aux membres de la juridiction administrative, " compte tenu du caractère présumé public et de la spécificité des réseaux sociaux numériques ", " de ne pas utiliser ces supports aux fins de commenter l'actualité politique et sociale ". Une telle recommandation de prudence tient compte des caractéristiques techniques des réseaux de communication au public en ligne en général et des réseaux sociaux en particulier et de la difficulté pour l'utilisateur qui y publie des propos de s'assurer de leur caractère privé ou de leur diffusion restreinte, d'en garantir l'intégrité ou d'en maîtriser la portée, eu égard notamment aux réactions auxquelles ils sont susceptibles de donner lieu, parfois presque instantanément. Dans ces conditions, eu égard à l'obligation de réserve à laquelle les membres de la juridiction administrative sont tenus, de telles recommandations ne portent pas d'atteinte disproportionnée à leur liberté d'expression.<br/>
<br/>
              14. Il résulte de ce qui précède que les moyens tirés de ce que les paragraphes 47 à 47-6 de la charte de déontologie de la juridiction administrative, issus de la décision attaquée, méconnaîtraient l'article 11 de la Déclaration des droits de l'homme et du citoyen de 1789 et seraient incompatibles avec les exigences qui résultent de l'article 10 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales doivent être écartés.<br/>
<br/>
              15. Il résulte de tout ce qui précède que le Syndicat de la juridiction administrative n'est pas fondé à demander l'annulation pour excès de pouvoir de la décision qu'il attaque.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La requête du Syndicat de la juridiction administrative est rejetée.<br/>
Article 2 : La présente décision sera notifiée au Syndicat de la juridiction administrative, à la garde des sceaux, ministre de la justice. <br/>
Copie en sera adressée au secrétaire général du Conseil d'Etat.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">37-04-01 JURIDICTIONS ADMINISTRATIVES ET JUDICIAIRES. MAGISTRATS ET AUXILIAIRES DE LA JUSTICE. MAGISTRATS DE L'ORDRE ADMINISTRATIF. - CHARTE DE DÉONTOLOGIE [RJ1] - BONNES PRATIQUES RELATIVES À L'USAGE DES RÉSEAUX SOCIAUX - MÉCONNAISSANCE DE LA LIBERTÉ D'EXPRESSION (ART. 11 DE LA DDHC ET 10 DE LA CONV. EDH) - 1) RECOMMANDATIONS GÉNÉRALES DE RETENUE DANS L'EXPRESSION ET LES PRISES DE POSITION - ABSENCE - 2) RECOMMANDATIONS RELATIVES À LA PUBLICITÉ DES PROFILS ET À LA MENTION DE LA QUALITÉ DE MEMBRE DE LA JURIDICTION - ABSENCE - 3) RECOMMANDATION DE NE PAS UTILISER LES RÉSEAUX SOCIAUX POUR COMMENTER L'ACTUALITÉ POLITIQUE ET SOCIALE - ABSENCE.
</SCT>
<ANA ID="9A"> 37-04-01 Paragraphes 47 à 47-6 du chapitre IV de la charte de déontologie portant sur l'usage des réseaux sociaux sur Internet.... ,,1) Recommandations générales relatives à l'usage des réseaux sociaux et incitant à la retenue dans l'expression et les prises de position.... ... ...Ces recommandations, formulées à titre de bonnes pratiques, visent, s'agissant de l'expression sur les réseaux sociaux et eu égard aux caractéristiques techniques de ces modes d'expression, à assurer le respect de l'obligation de réserve à laquelle les membres de la juridiction administrative sont tenus, laquelle vise à éviter que la diffusion de leurs propos porte atteinte à la nature et à la dignité des fonctions qu'ils exercent et à garantir l'indépendance, l'impartialité et le bon fonctionnement de la juridiction administrative. Ce faisant, elles ne portent pas à la liberté d'expression une atteinte qui méconnaîtrait les exigences découlant de l'article 11 de la Déclaration des droits de l'homme et du citoyen de 1789 (DDHC) ou celles qui résultent de l'article 10 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales (convention EDH).,,,2) Recommandations relatives à la publicité des profils et à l'absence de mention de la qualité de membre de la juridiction sur les réseaux sociaux non professionnels.... ,,Ces recommandations de prudence n'ont ni pour objet ni pour effet d'interdire l'inscription et l'expression des membres de la juridiction administrative sur des réseaux sociaux et leur méconnaissance ne saurait en elle-même constituer un manquement disciplinaire. Elles visent seulement à prémunir les membres de la juridiction administrative contre le risque que des propos publiés sur les réseaux sociaux reçoivent une diffusion excédant celle qui avait été initialement envisagée par leur auteur et puissent exposer ce dernier, dans le cas où leur diffusion rejaillirait sur l'institution, à devoir répondre d'un éventuel manquement à l'obligation de réserve. Dans ces conditions, les recommandations de bonnes pratiques ainsi énoncées, destinées à garantir le respect de l'obligation de réserve sur les réseaux sociaux, ne portent pas à la liberté d'expression des membres de la juridiction administrative une atteinte disproportionnée.,,,3) Recommandation de ne pas utiliser les réseaux sociaux pour commenter l'actualité politique et sociale.... ,,Une telle recommandation de prudence tient compte des caractéristiques techniques des réseaux de communication au public en ligne en général et des réseaux sociaux en particulier et de la difficulté pour l'utilisateur qui y publie des propos de s'assurer de leur caractère privé ou de leur diffusion restreinte, d'en garantir l'intégrité ou d'en maîtriser la portée, eu égard notamment aux réactions auxquelles ils sont susceptibles de donner lieu, parfois presque instantanément. Dans ces conditions, eu égard à l'obligation de réserve à laquelle les membres de la juridiction administrative sont tenus, de telles recommandations ne portent pas d'atteinte disproportionnée à leur liberté d'expression.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur la portée, la justiciabilité et le champ d'application de cette charte, CE, décision du même jour, M.,, n° 411070, à publier au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
