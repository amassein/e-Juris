<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038279179</ID>
<ANCIEN_ID>JG_L_2019_03_000000428995</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/27/91/CETATEXT000038279179.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 21/03/2019, 428995, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-03-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428995</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2019:428995.20190321</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 20 mars 2019 au secrétariat du contentieux du Conseil d'Etat, M. C... A...B...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'enjoindre au maire de la commune d'Audierne de rétablir les aisances de voierie et accès aux garages de sa propriété, située boulevard Normand, n° 1, à Audierne, en faisant notamment retirer tout mobilier urbain gênant l'accès ;<br/>
<br/>
              2°) d'enjoindre au maire de la commune d'Audierne de remettre en état les accès à sa propriété ;<br/>
<br/>
              3°) d'enjoindre au maire de la commune d'Audierne de restituer cinquante tonnes de roches assurant la sécurité de sa propriété ;<br/>
<br/>
              4°) de condamner la commune au paiement de la somme de 25 000 euros en réparation au préjudice qu'il estime avoir subi ;<br/>
<br/>
              5°) d'assortir sa décision d'une astreinte d'un montant de 500 euros par jour de retard.<br/>
<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - l'urgence résulte de l'ouverture prochaine d'un établissement commercial dans cette propriété ;<br/>
              - l'urgence résulte également de l'impossibilité de réaliser des travaux de sécurisation des lieux, exposant les tiers à des risques ;<br/>
              - la fermeture des aisances de voirie porte une atteinte grave et manifestement illégale à la liberté d'entreprendre, en l'espèce une activité touristique ;<br/>
              - l'impossibilité de procéder aux travaux permettant de mettre fin aux risques d'accident mortel pour les tiers constitue également une atteinte grave et manifestement illégale à une liberté fondamentale.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              Sur les conclusions aux fins d'injonction :<br/>
<br/>
              2. Le juge des référés du Conseil d'Etat ne peut être régulièrement saisi, en premier et dernier ressort, d'une requête tendant à la mise en oeuvre de l'une des procédures régies par le livre V du code de justice administrative que pour autant que le litige principal auquel se rattache ou est susceptible de se rattacher la mesure d'urgence qu'il lui est demandé de prendre, ressortit lui-même à la compétence directe du Conseil d'Etat. L'article R. 522-8-1 du même code prévoit que, par dérogation aux dispositions du titre V du livre III relatif au règlement des questions de compétence au sein de la juridiction administrative, le juge des référés qui entend décliner la compétence de la juridiction rejette les conclusions dont il est saisi par voie d'ordonnance, sans qu'il ait à les transmettre à la juridiction compétente.<br/>
<br/>
              3. M. A... B...demande au juge des référés du Conseil d'Etat d'enjoindre au maire de la commune d'Audierne, en premier lieu, de rétablir les aisances de voierie et accès aux garages de sa propriété, située boulevard Normand, n° 1, à Audierne, et notamment de retirer le mobilier urbain gênant l'accès, en deuxième lieu, de remettre en état les accès à sa propriété et, en troisième lieu, de restituer cinquante tonnes de rochers assurant la sécurité de sa propriété.<br/>
<br/>
              4. Les décisions du maire de la commune d'Audierne, à supposer leur existence établie, de réaliser les travaux litigieux et d'autoriser le passage des véhicules ayant déplacé les rochers, ne sont manifestement pas au nombre de celles dont il appartient au Conseil d'Etat de connaître en premier ressort en vertu des dispositions de l'article R. 311-1 du code de justice administrative. Par suite, les conclusions aux fins d'injonction doivent être rejetées selon la procédure prévue par l'article L. 522-2 du code de justice administrative.<br/>
<br/>
              Sur les conclusions indemnitaires :<br/>
<br/>
              5. Il n'appartient pas au juge des référés de se prononcer sur des conclusions à fin d'indemnité, qui ne peuvent être utilement soumises qu'au juge du fond. Par suite, les conclusions indemnitaires présentées par M. A... B...ne peuvent qu'être rejetées selon la procédure prévue par l'article L. 522-3 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. A... B...est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. C... A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
