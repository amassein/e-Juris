<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035995548</ID>
<ANCIEN_ID>JG_L_2017_11_000000409538</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/99/55/CETATEXT000035995548.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 09/11/2017, 409538, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-11-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>409538</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Yves Doutriaux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:409538.20171109</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 4 avril 2017 au secrétariat du contentieux du Conseil d'Etat, Mme A...B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du 21 octobre 2016 rapportant le décret du 13 novembre 2014 qui lui avait accordé la nationalité française ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre des dispositions de l'article 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code civil ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yves Doutriaux, conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'aux termes de l'article 27-2 du code civil : " Les décrets portant acquisition, naturalisation ou réintégration peuvent être rapportés sur avis conforme du Conseil d'Etat dans le délai de deux ans à compter de leur publication au Journal officiel si le requérant ne satisfait pas aux conditions légales ; si la décision a été obtenue par mensonge ou fraude, ces décrets peuvent être rapportés dans le délai de deux ans à partir de la découverte de la fraude " ;<br/>
<br/>
              2.	Considérant qu'il ressort des pièces de dossier que MmeB..., ressortissante marocaine, a déposé une demande de naturalisation le 11 mars 2014 en indiquant être divorcée ; qu'au vu de ses déclarations, elle a été naturalisée par décret du 13 novembre 2014 ; que le préfet du Gard a, toutefois, informé le 18 novembre 2014 le ministre chargé des naturalisations que Mme B...avait épousé un ressortissant marocain le 10 septembre 2014 au Maroc ; que, par bordereau reçu le 13 janvier 2016, le ministre des affaires étrangères et du développement international a, pour sa part, informé le ministre chargé des naturalisations que Mme B...avait sollicité la transcription de son acte de mariage sur les registres de l'état civil français et que, de son union, était né un enfant le 15 juin 2015 ; que, par le décret attaqué, le Premier ministre a rapporté le décret du 13 novembre 2014 prononçant la naturalisation de Mme B... au motif qu'il avait été pris au vu d'informations mensongères délivrées par l'intéressée sur sa situation familiale ; <br/>
<br/>
              3.	Considérant qu'aux termes de l'article 21-6 du code civil : " Nul ne peut être naturalisé s'il n'a en France sa résidence au moment de la signature du décret de naturalisation " ; qu'il résulte de ces dispositions que la demande de naturalisation n'est pas recevable lorsque l'intéressé n'a pas fixé en France de manière durable le centre de ses intérêts ; que, pour apprécier si cette condition se trouve remplie, l'autorité administrative peut notamment prendre en compte, sous le contrôle du juge de l'excès de pouvoir, la situation familiale en France de l'intéressé ; que, par suite, ainsi que l'énonce le décret attaqué, la circonstance que l'intéressé ait dissimulé s'être mariée au Maroc était de nature à modifier l'appréciation qui a été portée par l'autorité administrative sur la fixation du centre de ses intérêts ; <br/>
<br/>
              4.	Considérant que le mariage contracté le 10 septembre 2014 a constitué un changement de sa situation personnelle et familiale que Mme B...aurait dû porter à la connaissance des services instruisant sa demande de naturalisation, ce qu'elle n'a pas fait avant l'intervention du décret lui accordant la nationalité française le 13 novembre 2014 ; que, si elle soutient qu'elle était de bonne foi, qu'elle n'était pas mariée à la date du dépôt de sa demande de naturalisation, qu'elle a sollicité elle-même la transcription de son acte de mariage et qu'elle a informé la préfecture du Gard le 18 novembre 2014, il ressort des pièces du dossier que Mme B... était en mesure de faire part de son changement de situation familiale avant l'intervention du décret lui accordant la nationalité française, ce qu'elle n'a pas fait ; que l'intéressée, qui maîtrise la langue française, ne pouvait se méprendre sur la teneur de l'engagement qu'elle avait pris sur l'honneur, en soumettant sa demande de naturalisation, de faire connaître toute modification de sa situation familiale survenant au cours de l'instruction de sa demande de naturalisation ; qu'elle doit être regardée comme ayant sciemment dissimulé le changement de sa situation familiale avant que ne soit prononcée sa naturalisation par l'effet du décret du 13 novembre 2014 ; que, par suite, en rapportant la naturalisation de MmeB..., le Premier ministre n'a pas fait une inexacte application des dispositions de l'article 27-2 du code civil ;<br/>
<br/>
              5.	Considérant qu'il résulte de ce qui précède que Mme B...n'est pas fondée à demander l'annulation pour excès de pouvoir du décret du 21 octobre 2016 rapportant le décret du 13 novembre 2014 lui accordant la nationalité française ; que ses conclusions présentées sur le fondement de l'article L. 761-1 du code de justice administrative ne peuvent, en conséquence, qu'être rejetées ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de Mme B...est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à Mme A...B...et au ministre d'Etat, ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
