<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000020868271</ID>
<ANCIEN_ID>JG_L_2008_12_000000294078</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/20/86/82/CETATEXT000020868271.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 31/12/2008, 294078, Publié au recueil Lebon</TITRE>
<DATE_DEC>2008-12-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>294078</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Vigouroux</PRESIDENT>
<AVOCATS>SPINOSI ; SCP PEIGNOT, GARREAU</AVOCATS>
<RAPPORTEUR>M. Alexandre  Lallet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mlle Courrèges Anne</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 5 juin et 5 octobre 2006 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SOCIETE FONCIERE ARIANE, dont le siège est 6, rue de Brindejonc des Moulinais - BP 55823 à Toulouse Cedex 5 (31505) ; la SOCIETE FONCIERE ARIANE demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt du 30 mars 2006 par lequel la cour administrative d'appel de Nancy a, d'une part, annulé les jugements des 9 septembre 1999 et 3 octobre 2002 du tribunal administratif de Besançon déclarant l'Etat partiellement responsable des conséquences dommageables, pour la SA Guiraudie et Auffeve (SA G.A.), devenue la SOCIETE FONCIERE ARIANE, des inondations subies par la société Equipement et Fournitures pour l'Imprimerie (E.F.I.) dans les bâtiments situés à Baume-les-Dames que la SA G.A. a construits pour son compte et le condamnant à verser à cette dernière la moitié des sommes qu'elle a été condamnée à verser à la société E.F.I. et, d'autre part, rejeté sa requête tendant à la réformation de ces jugements ainsi que l'ensemble des demandes présentées par la SA G.A. devant le tribunal administratif de Besançon ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 10 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
<br/>
              Vu le code civil, notamment son article 1251 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alexandre Lallet, Auditeur, <br/>
<br/>
              - les observations de Me Spinosi, avocat de la SOCIETE FONCIERE ARIANE et de la SCP Peignot, Garreau, avocat du ministre d'Etat, ministre de l'écologie, de l'énergie, du développement durable et de l'aménagement du territoire, <br/>
<br/>
              - les conclusions de Mlle Anne Courrèges, Commissaire du gouvernement ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un arrêt de la cour d'appel de Besançon du 31 mai 1995 devenu définitif sur ce point, la société anonyme Guiraudie Auffève (SA G.A.), devenue la SOCIETE FONCIERE ARIANE, a été reconnue responsable, sur le fondement de l'article 1792 du code civil, des dommages causés par la crue du Doubs aux installations qu'elle a construites pour le compte de la société E.F.I. à Baumes-les-Dames, et a été condamnée à réparer les préjudices qui en ont résulté pour cette dernière, pour un montant définitivement fixé, après cassation partielle de cet arrêt, par un arrêt de la cour d'appel de Dijon du 17 mars 1998 ; que la SOCIETE FONCIERE ARIANE demande l'annulation de l'arrêt du 30 mars 2006 par lequel la cour administrative d'appel de Nancy, après avoir annulé les jugements des 9 septembre 1999 et 3 octobre 2002 du tribunal administratif de Besançon, a rejeté les requêtes d'appel de la SA G.A. ainsi que ses demandes présentées à ce tribunal et tendant à ce que l'Etat soit condamné à l'indemniser à hauteur des condamnations prononcées à son encontre ;<br/>
<br/>
              Considérant que lorsque l'auteur d'un dommage, condamné, comme en l'espèce, par le juge judiciaire à en indemniser la victime, saisit la juridiction administrative d'un recours en vue de faire supporter la charge de la réparation par la collectivité publique co-auteur de ce dommage, sa demande, quel que soit le fondement de sa responsabilité retenu par le juge judiciaire, n'a pas le caractère d'une action récursoire par laquelle il ferait valoir des droits propres à l'encontre de cette collectivité mais d'une action subrogatoire fondée sur les droits de la victime à l'égard de ladite collectivité ; qu'ainsi subrogé, s'il peut utilement se prévaloir des fautes que la collectivité publique aurait commises à son encontre ou à l'égard de la victime et qui ont concouru à la réalisation du dommage, il ne saurait avoir plus de droits que cette dernière et peut donc se voir opposer l'ensemble des moyens de défense qui auraient pu l'être à la victime ; qu'en outre, eu égard à l'objet d'une telle action, qui vise à assurer la répartition de la charge de la réparation du dommage entre ses co-auteurs, sa propre faute lui est également opposable ;<br/>
<br/>
              Considérant qu'après avoir relevé, sans dénaturer ses écritures, que la société requérante recherchait la responsabilité de l'Etat  pour ne pas l'avoir informée que la construction endommagée devait être réalisée au-dessus de la cote 267,40 NGF  afin d'être préservée du risque d'inondation et qu'elle demandait la réparation du préjudice résultant du versement à la société E.F.I de l'indemnité qu'elle a été condamnée à lui verser, la cour administrative d'appel, qui n'était pas tenue par la qualification donnée par la SA G.A. à sa demande, en a exactement déduit que l'action de cette société à l'encontre de l'Etat, dont les agissements reprochés auraient, selon la société requérante, contribué à la réalisation du dommage subi par la société E.F.I., avait le caractère d'une action subrogatoire ; qu'elle n'a donc commis aucune erreur de droit ni, en tout état de cause, privé la SA G.A. de son droit à un recours effectif garanti par les stipulations de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, en lui opposant la faute que la société E.F.I a commise en s'abstenant de lui communiquer les informations nécessaires à la sécurité des constructions au regard du risque d'inondation ;<br/>
<br/>
              Considérant, en second lieu, qu'en estimant qu'il ressortait des pièces du dossier que la société E.F.I avait eu connaissance de la cote de 267,40 NGF en temps utile pour faire surélever la construction en cours de réalisation, la cour administrative d'appel a porté sur les faits de l'espèce une appréciation souveraine exempte de dénaturation ; <br/>
<br/>
              Considérant qu'il résulte de ce qui précède que la SOCIETE FONCIERE ARIANE n'est pas fondée à demander l'annulation de l'arrêt attaqué, qui est suffisamment motivé ; que les conclusions qu'elle présente au titre de l'article L. 761-1 du code de justice administrative doivent, par voie de conséquence, être rejetées ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à sa charge, au profit de l'Etat, la somme de 3 000 euros à ce même titre ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la SOCIETE FONCIERE ARIANE est rejeté.<br/>
Article 2 : La SOCIETE FONCIERE ARIANE versera à l'Etat une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la SOCIETE FONCIERE ARIANE et au ministre d'Etat, ministre de l'écologie, de l'énergie, du développement durable et de l'aménagement du territoire.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-05-02 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RECOURS OUVERTS AUX DÉBITEURS DE L'INDEMNITÉ, AUX ASSUREURS DE LA VICTIME ET AUX CAISSES DE SÉCURITÉ SOCIALE. ACTION RÉCURSOIRE. - ABSENCE - RECOURS DE L'AUTEUR D'UN DOMMAGE CONTRE LA COLLECTIVITÉ PUBLIQUE CO-AUTEUR DU DOMMAGE [RJ1] - CONSÉQUENCES.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-05-03 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RECOURS OUVERTS AUX DÉBITEURS DE L'INDEMNITÉ, AUX ASSUREURS DE LA VICTIME ET AUX CAISSES DE SÉCURITÉ SOCIALE. SUBROGATION. - EXISTENCE - RECOURS DE L'AUTEUR D'UN DOMMAGE CONTRE LA COLLECTIVITÉ PUBLIQUE CO-AUTEUR DU DOMMAGE [RJ1] - CONSÉQUENCES.
</SCT>
<ANA ID="9A"> 60-05-02 L'auteur d'un dommage, condamné par le juge judiciaire à en indemniser la victime, qui saisit la juridiction administrative d'un recours en vue de faire supporter la charge de la réparation par la collectivité publique co-auteur du dommage, exerce une action subrogatoire et non une action récursoire. S'il peut utilement se prévaloir des fautes que la collectivité publique aurait commises à son encontre ou à l'égard de la victime et qui ont concouru à la réalisation du dommage, il ne saurait avoir plus de droits que la victime et peut donc se voir opposer l'ensemble des moyens de défense qui auraient pu l'être à la victime.</ANA>
<ANA ID="9B"> 60-05-03 L'auteur d'un dommage, condamné par le juge judiciaire à en indemniser la victime, qui saisit la juridiction administrative d'un recours en vue de faire supporter la charge de la réparation par la collectivité publique co-auteur du dommage, exerce une action subrogatoire et non une action récursoire. S'il peut utilement se prévaloir des fautes que la collectivité publique aurait commises à son encontre ou à l'égard de la victime et qui ont concouru à la réalisation du dommage, il ne saurait avoir plus de droits que la victime et peut donc se voir opposer l'ensemble des moyens de défense qui auraient pu l'être à la victime.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. Section, 13 octobre 1972, Caisse régionale de réassurance mutuelle agricole de l'Est, n° 82202-82203, p. 635. Conf. CAA Paris, 18 mai 1989, Commune de Dourdan, n° 89PA00399, p. 307.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
