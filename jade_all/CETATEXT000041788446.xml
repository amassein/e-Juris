<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041788446</ID>
<ANCIEN_ID>JG_L_2020_04_000000439762</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/78/84/CETATEXT000041788446.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 01/04/2020, 439762, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-04-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>439762</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:439762.20200401</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 24 et 31 mars 2020 au secrétariat du contentieux du Conseil d'Etat, la Fédération nationale des marchés de France demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
              1°) de suspendre l'exécution des dispositions du III de l'article 8 du décret n° 2020-293 du 23 mars 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire ;<br/>
<br/>
              2°) d'enjoindre à l'Etat de rétablir le principe d'autorisation de la tenue des marchés alimentaires, couverts et de plein air, sur l'ensemble du territoire national ;<br/>
<br/>
              3°) d'enjoindre à l'Etat de modifier les dispositions du III de l'article 8 du décret du 23 mars 2020, d'autoriser par principe la tenue des marchés couverts et de plein air, sur l'ensemble du territoire national, sans préjudice de la faculté qui pourrait être utilement accordée aux préfets sur avis des maires concernés, ou directement aux maires, d'édicter les mesures de toute nature propres à garantir la santé de la clientèle des marchés et des commerçant non-sédentaires, pour faire face efficacement à la propagation du covid-19, dans le cadre de l'état d'urgence sanitaire déclaré par la loi n° 2020-290 du 23 mars 2020 ;<br/>
<br/>
<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - sa requête est recevable, dès lors qu'en tant qu'organisation professionnelle qui assure la défense des commerçants non sédentaires, elle dispose d'un intérêt à agir contre le décret attaqué ;<br/>
              - la condition d'urgence est remplie ;<br/>
              - il est porté une atteinte grave et manifestement illégale à la liberté d'exercice d'une profession, à la liberté d'entreprendre et la liberté de commerce et d'industrie, à la libre administration des collectivités territoriales, à l'égalité de traitement entre les commerçant non sédentaires exerçant sur le domaine public et les commerces sédentaires ;<br/>
              - en effet, l'interdiction générale et absolue de la tenue de marchés résultant des dispositions litigieuses est excessive et disproportionnée au regard de l'objectif poursuivi ;<br/>
              - ces mêmes dispositions méconnaissent l'article L. 3131-15 du code de la santé publique ;<br/>
              - elles méconnaissent le principe d'égalité entre les commerces non sédentaires exerçant sur le domaine public et les commerces sédentaires en instaurant une différence de traitement qui n'est pas justifiée par l'objectif de protection de la santé publique, dès lors que les premiers sont, au même titre que les seconds, en charge de l'approvisionnement de biens de première nécessité et à même de respecter les mesures de sécurité sanitaire et, qu'en outre, la fédération requérante a diffusé un guide des bonnes pratiques imposant aux commerçants non sédentaires, et proposant aux élus, une série de mesures propres à garantir la santé publique sur les marchés de plein air ou couverts ;<br/>
              - elles méconnaissent les dispositions des articles L. 2122-1 et L. 2224-18 du code général des collectivités territoriales en permettant aux préfets d'accorder des dérogations à l'interdiction générale alors que la compétence pour autoriser ou refuser l'occupation du domaine public communal relève du maire et que celle d'autoriser ou supprimer un marché relève du conseil municipal ;<br/>
              - la mise en oeuvre de l'interdiction contestée aura pour effet d'accroître l'affluence de la population dans les commerces sédentaires d'alimentation et, par là-même, les risques de propagation du covid-19 en méconnaissance de l'objectif de protection de la santé publique ;<br/>
              - l'interdiction des marchés aura pour effet de priver les personnes les plus vulnérables, âgées et à la mobilité réduite, d'un mode d'approvisionnement de proximité.<br/>
<br/>
<br/>
              Par une intervention et trois mémoires, enregistrés les 25 et 31 mars 2020, la Confédération des petites et moyennes entreprises (CPME), la Fédération Saveurs Commerces, la Fédération des fromagers de France et l'Organisation des poissonniers écaillers de France déclarent intervenir au soutien de la requête de la Fédération nationale des marchés de France. <br/>
<br/>
              Par une intervention, enregistrée le 31 mars 2020, la Confédération des commerçants de France déclare intervenir au soutien de la requête de la Fédération nationale des marchés de France. <br/>
              Par un mémoire en défense, enregistré le 28 mars 2020, le ministre des solidarités et de la santé conclut au rejet de la requête. Il soutient qu'aucune atteinte grave et manifestement illégale aux libertés fondamentales invoquées n'est caractérisée.<br/>
<br/>
              Par un mémoire, enregistré le 31 mars 2020, le ministre de l'agriculture et de l'alimentation déclare s'associer aux conclusions du ministre des solidarités et de la santé.<br/>
<br/>
              Par un mémoire, enregistré le 1er avril 2020, le ministre de l'économie et des finances déclare s'associer aux conclusions du ministre des solidarités et de la santé.<br/>
              La requête a été communiquée au Premier ministre et au ministre de l'intérieur qui n'ont pas produit de mémoire.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution ; <br/>
              - le code général des collectivités territoriales ; <br/>
              - le code de la santé publique ;<br/>
              - la loi n° 2020-290 du 23 mars 2020 ;<br/>
              - le décret n° 2020-293 du 23 mars 2020 ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
<br/>
<br/>
              Les parties ont été informées, sur le fondement de l'article 9 de l'ordonnance du 25 mars 2020 portant adaptation des règles applicables devant les juridictions de l'ordre administratif, de ce qu'aucune audience ne se tiendrait et de ce que la clôture de l'instruction serait fixée le 1er avril 2020 à 12 heures.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
<br/>
              1. L'article L. 511-1 du code de justice administrative dispose que : " Le juge des référés statue par des mesures qui présentent un caractère provisoire. Il n'est pas saisi du principal et se prononce dans les meilleurs délais. " Aux termes de l'article L. 521-2 du même code : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures. ". <br/>
<br/>
              Sur les interventions :<br/>
<br/>
              2. La Confédération des petites et moyennes entreprises (CPME), la Fédération Saveurs Commerces, la Fédération des fromagers de France et l'Organisation des poissonniers écaillers de France justifient d'un intérêt suffisant pour intervenir au soutien de la requête de la Fédération nationale des marchés de France. La Confédération des commerçants de France justifie d'un intérêt suffisant pour intervenir au soutien de cette requête. Leurs interventions sont, par suite, recevables.<br/>
              Sur l'office du juge des référés :<br/>
<br/>
              3. Il résulte de la combinaison des dispositions des articles L. 511-1 et L. 521-2 du code de justice administrative qu'il appartient au juge des référés, lorsqu'il est saisi sur le fondement de l'article L. 521-2 et qu'il constate une atteinte grave et manifestement illégale portée par une personne morale de droit public à une liberté fondamentale, résultant de l'action ou de la carence de cette personne publique, de prescrire les mesures qui sont de nature à faire disparaître les effets de cette atteinte, dès lors qu'existe une situation d'urgence caractérisée justifiant le prononcé de mesures de sauvegarde à très bref délai. Ces mesures doivent, en principe, présenter un caractère provisoire, sauf lorsque aucune mesure de cette nature n'est susceptible de sauvegarder l'exercice effectif de la liberté fondamentale à laquelle il est porté atteinte. Sur le fondement de l'article L. 521-2, le juge des référés peut ordonner à l'autorité compétente de prendre, à titre provisoire, des mesures d'organisation des services placés sous son autorité, dès lors qu'il s'agit de mesures d'urgence qui lui apparaissent nécessaires pour sauvegarder, à très bref délai, la liberté fondamentale à laquelle il est gravement, et de façon manifestement illégale, porté atteinte. Le caractère manifestement illégal de l'atteinte doit s'apprécier notamment en tenant compte des moyens dont dispose l'autorité administrative compétente et des mesures qu'elle a déjà prises. <br/>
              Sur les circonstances :<br/>
<br/>
              4. L'émergence d'un nouveau coronavirus (covid-19), de caractère pathogène et particulièrement contagieux et sa propagation sur le territoire français a conduit le ministre des solidarités et de la santé à prendre, par plusieurs arrêtés à compter du 4 mars 2020, des mesures sur le fondement des dispositions de l'article L. 3131-1 du code de la santé publique. En particulier, par un arrêté du 14 mars 2020, un grand nombre d'établissements recevant du public ont été fermés au public, les rassemblements de plus de 100 personnes ont été interdits et l'accueil des enfants dans les établissements les recevant et les établissements scolaires et universitaires a été suspendu. Puis, par un décret du 16 mars 2020 motivé par les circonstances exceptionnelles découlant de l'épidémie de covid-19, modifié par décret du 19 mars, le Premier ministre a interdit le déplacement de toute personne hors de son domicile, sous réserve d'exceptions limitativement énumérées et devant être dûment justifiées, à compter du 17 mars à 12h, sans préjudice de mesures plus strictes susceptibles d'être ordonnées par le représentant de l'Etat dans le département. Le ministre des solidarités et de la santé a pris des mesures complémentaires par des arrêtés des 17, 19, 20, 21 mars 2020. <br/>
<br/>
              5. Par l'article 4 de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19, a été déclaré l'état d'urgence sanitaire pour une durée de deux mois sur l'ensemble du territoire national. Par un nouveau décret du 23 mars 2020 pris sur le fondement de l'article L. 3131-15 du code de la santé publique issu de la loi du 23 mars 2020, le Premier ministre a réitéré les mesures qu'il avait précédemment ordonnées tout en leur apportant des précisions ou restrictions complémentaires.<br/>
<br/>
              Sur la demande en référé :<br/>
<br/>
              6. Aux termes de l'article L. 3131-15 du code de la santé publique, créé par la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de Covid-19 : " Dans les circonscriptions territoriales où l'état d'urgence sanitaire est déclaré, le Premier ministre peut, par décret réglementaire pris sur le rapport du ministre chargé de la santé, aux seules fins de garantir la santé publique : [...] 5° Ordonner la fermeture provisoire d'une ou plusieurs catégories d'établissements recevant du public ainsi que des lieux de réunion, à l'exception des établissements fournissant des biens ou des services de première nécessité ; 6° Limiter ou interdire les rassemblements sur la voie publique ainsi que les réunions de toute nature ; [...] 10° En tant que de besoin, prendre par décret toute autre mesure réglementaire limitant la liberté d'entreprendre, dans la seule finalité de mettre fin à la catastrophe sanitaire mentionnée à l'article L. 3131-12 du présent code. / Les mesures prescrites en application des 1° à 10° du présent article sont strictement proportionnées aux risques sanitaires encourus et appropriées aux circonstances de temps et de lieu. Il y est mis fin sans délai lorsqu'elles ne sont plus nécessaires. ".<br/>
<br/>
              7. L'article 1er du décret du 23 mars 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire dispose que : " Eu égard à la situation sanitaire résultant de l'épidémie de covid-19, le présent décret fixe les mesures propres à garantir la santé publique mentionnées à l'article L.3131-15 du code de la santé publique. ". L'article 2 du même décret dispose que : " Afin de ralentir la propagation du virus, les mesures d'hygiène et de distanciation sociale, dites " barrières ", définies au niveau national, doivent être observées en tout lieu et en toute circonstance. Les rassemblements, réunions, activités, accueils et déplacements ainsi que l'usage des moyens de transports qui ne sont pas interdits en vertu du présent décret sont organisés en veillant au strict respect de ces mesures. ". Aux termes de l'article 7 du même décret : " Tout rassemblement, réunion ou activité mettant en présence de manière simultanée plus de 100 personnes en milieu clos ou ouvert, est interdit sur le territoire de la République jusqu'au 15 avril 2020. Les rassemblements, réunions ou activités indispensables à la continuité de la vie de la Nation peuvent être maintenus à titre dérogatoire par le représentant de l'Etat dans le département, par des mesures réglementaires ou individuelles, sauf lorsque les circonstances locales s'y opposent. / Le représentant de l'Etat dans le département est habilité aux mêmes fins à interdire ou à restreindre, par des mesures réglementaires ou individuelles, les rassemblements, réunions ou activités ne relevant pas du premier alinéa lorsque les circonstances locales l'exigent. ". <br/>
<br/>
              8. Par sa requête, la fédération demande au juge des référés du Conseil d'Etat, d'ordonner, sur le fondement de l'article L. 521-2 du code de justice administrative, la suspension de l'exécution des dispositions du III de l'article 8 du décret du 23 mars 2020 aux termes desquelles : " La tenue des marchés, couverts ou non et quel qu'en soit l'objet, est interdite. Toutefois, le représentant de l'Etat dans le département peut, après avis du maire, accorder une autorisation d'ouverture des marchés alimentaires qui répondent à un besoin d'approvisionnement de la population si les conditions de leur organisation ainsi que les contrôles mis en place sont propres à garantir le respect des dispositions de l'article 1er et de l'article 7. " et d'enjoindre à l'Etat d'autoriser la tenue des marchés couverts et de plein air, sur l'ensemble du territoire national.<br/>
<br/>
              9. En premier lieu, dans l'actuelle période d'état d'urgence sanitaire, il appartient aux différentes autorités compétentes, en particulier au Premier ministre, de prendre, en vue de sauvegarder la santé de la population, toutes dispositions de nature à prévenir ou à limiter les effets de l'épidémie. Ces mesures, qui peuvent limiter l'exercice des droits et libertés fondamentaux, comme la liberté d'exercice d'une profession ou la liberté d'entreprendre doivent, dans cette mesure, être nécessaires, adaptées et proportionnées à l'objectif de sauvegarde de la santé publique qu'elles poursuivent.  <br/>
<br/>
              10. D'une part, il résulte de l'instruction que l'interdiction de tenue des marchés, posée par les dispositions litigieuses, repose sur le constat que l'insuffisance des mesures d'organisation rendait, dans une large mesure, difficile voire impossible le respect des règles de sécurité sanitaire, en particulier les règles de distance minimale entre les personnes, qu'impose la situation actuelle. D'autre part, cette interdiction générale s'accompagne de la faculté pour le représentant de l'Etat d'autoriser, au terme d'un examen des circonstances locales, l'ouverture d'un marché alimentaire à la double condition que doive être satisfait un besoin d'approvisionnement de la population et que les conditions de son organisation ainsi que les contrôles mis en place permettent de garantir le respect des règles de sécurité sanitaire requises pour assurer la protection tant de la population que des personnes y travaillant. <br/>
<br/>
              11. La fédération requérante soutient que les dispositions litigieuses méconnaissent l'article L. 3131-15 du code de la santé publique précité sur le fondement duquel elles ont été prises qui permet au Premier ministre d'ordonner la fermeture provisoire d'une ou plusieurs catégories d'établissements à l'exception de ceux fournissant des biens ou des services de première nécessité. Il découle toutefois de l'économie générale du dispositif présenté au point précédent, qui prévoit que le représentant de l'Etat dans le département prenne en compte, dans l'exercice de son pouvoir d'autorisation, l'obligation de pourvoir aux besoins de première nécessité de la population, que les dispositions litigieuses ne méconnaissent pas l'article L. 3131-15 du code de la santé publique qui excepte des mesures susceptibles d'être prescrites pour garantir la santé publique celles qui auraient pour effet de faire obstacle à la fourniture à la population des biens ou des services de première nécessité.<br/>
<br/>
              12. Il résulte enfin de l'instruction, en particulier des éléments produits en défense, que le nombre des autorisations préfectorales augmente régulièrement depuis l'entrée en vigueur du décret du 23 mars 2020 et a vocation à continuer de s'élever au fur et à mesure qu'il apparaîtra que l'ouverture d'un marché alimentaire peut s'accompagner des mesures d'organisation permettant de prévenir le risque sanitaire, notamment en ce qui concerne la gestion des flux et le respect des distances. Dans ces conditions, les dispositions litigieuses, dont il ne résulte pas de l'instruction qu'elles entraîneraient un phénomène d'afflux dans les magasins d'alimentation sédentaires, ménagent un juste équilibre entre la nécessité de garantir la santé publique et la satisfaction des besoins d'approvisionnement de la population. Il s'ensuit qu'il ne résulte pas de l'instruction que serait méconnue l'obligation posée par l'article L. 3131-15 du code de la santé publique précité que les mesures prises sur son fondement soient strictement proportionnées aux risques sanitaires encourus et appropriées aux circonstances de temps et de lieu.  <br/>
<br/>
              13. Il résulte des motifs énoncés aux points 10 à 12 qu'eu égard à l'objectif de sauvegarde de la santé publique que poursuit l'interdiction contestée, à son caractère provisoire, à la perspective que se développent sur l'ensemble du territoire, dans le respect des règles de sécurité sanitaire, les autorisations d'ouverture des marchés alimentaires ainsi qu'aux mesures de soutien mises en place par l'Etat, notamment le fonds de solidarité à destination des entreprises particulièrement touchées par les conséquences économiques, financières et sociales de la propagation de l'épidémie de covid-19 et des mesures prises pour limiter cette propagation créé par l'ordonnance du 25 mars 2020, auxquelles sont éligibles les entreprises de commerce et d'artisanat non sédentaires, les dispositions litigieuses ne caractérisent, en l'état de l'instruction, aucune atteinte grave et manifestement illégale à la liberté d'entreprendre, à la liberté du commerce et de l'industrie qui en est une composante et la liberté d'exercice d'une profession.<br/>
<br/>
<br/>
              14. En deuxième lieu, les dispositions litigieuses qui confient, ainsi qu'il a été dit, au représentant de l'Etat dans le département la faculté d'autoriser, après un avis du maire de la commune concernée qui ne le lie pas, l'ouverture d'un marché alimentaire, par dérogation à l'interdiction générale qu'elles édictent, n'affectent ni la compétence du conseil municipal pour créer ou supprimer les halles et marchés communaux ni celle du maire pour autoriser ou refuser l'occupation du domaine public communal. Il s'ensuit qu'elles ne portent aucune atteinte à la libre administration des collectivités territoriales.<br/>
<br/>
<br/>
              15. En troisième et dernier lieu, la méconnaissance du principe d'égalité ne saurait révéler, par elle-même, une atteinte à une liberté fondamentale au sens de l'article L. 521-2 du code de justice administrative. Au demeurant, si la fédération requérante soutient que les dispositions litigieuses introduisent une différence de traitement entre les marchés alimentaires non sédentaires et les établissements de commerce alimentaire sédentaires, il résulte de l'instruction que les premiers sont placés, au regard de l'objectif de protection de sauvegarde de la santé publique et de la nécessité de respecter les règles de sécurité sanitaire qui en découlent, dans une situation différente de celle des seconds eu égard aux caractéristiques et spécificités de leur topographie, de leurs jours et heures d'ouverture et de la densité de leur personnel.<br/>
<br/>
<br/>
              16. Il résulte de tout ce qui précède que, sans qu'il soit besoin de se prononcer sur la condition d'urgence, la requête de la Fédération nationale des marchés de France doit être rejetée.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : Les interventions de la Confédération des petites et moyennes entreprises, la Fédération Saveurs Commerces, la Fédération des fromagers de France et l'Organisation des poissonniers écaillers de France ainsi que de la Confédération des commerçants de France sont admises.<br/>
Article 2 : La requête de la Fédération nationale des marchés de France est rejetée.<br/>
Article 3 : La présente ordonnance sera notifiée à la Fédération nationale des marchés de France, au ministre des solidarités et de la santé, au ministre de l'économie et des finances, au ministre de l'agriculture et de l'alimentation, à la Confédération des petites et moyennes entreprises, à la Fédération Saveurs Commerces, à la Fédération des fromagers de France, à l'Organisation des poissonniers écaillers de France et à la Confédération des commerçants de France.<br/>
Copie en sera adressée au Premier ministre et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
