<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032821135</ID>
<ANCIEN_ID>JG_L_2016_06_000000390157</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/82/11/CETATEXT000032821135.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 30/06/2016, 390157, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-06-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>390157</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>M. Tristan Aureau</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:390157.20160630</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire complémentaire, enregistrés les 13 mai et 13 août 2015, M. B...A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du 6 mars 2015 accordant son extradition aux autorités turques ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - la convention européenne d'extradition du 13 décembre 1957 ;<br/>
              - le code pénal ;<br/>
              - le code de procédure pénale ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Tristan Aureau, maître des requêtes, <br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de M. A...; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant, en premier lieu, qu'il ressort des mentions de l'ampliation du décret attaqué, figurant au dossier et certifiée conforme par le secrétaire général du Gouvernement, que ce décret a été signé par le Premier ministre et contresigné par le garde des sceaux, ministre de la justice ; que l'ampliation notifiée à M. A...n'avait pas à être revêtue de ces signatures ;<br/>
<br/>
              2.	Considérant, en deuxième lieu, qu'aux termes de l'article 12 de la convention européenne d'extradition du 13 décembre 1957 : " (...) / 2- Il sera produit à l'appui de la requête : / a) L'original ou l'expédition authentique soit d'une décision de condamnation exécutoire, soit d'un mandat d'arrêt ou de tout autre acte ayant la même force, délivré dans les formes prescrites par la loi de la Partie requérante (...) " ; qu'il ressort des pièces du dossier que la demande d'extradition était accompagnée de toutes les pièces requises par ces stipulations et que ces pièces présentaient une authenticité suffisante ; qu'ainsi, le moyen tiré de ce que le décret attaqué aurait fait droit à une demande d'extradition alors que les autorités françaises ne disposaient pas des éléments que l'Etat requérant devait leur présenter en vertu de ces stipulations doit être écarté ; <br/>
<br/>
              3.	Considérant, en troisième lieu, qu'aux termes de l'article 3 de la convention européenne d'extradition : " 1. L'extradition ne sera pas accordée si l'infraction pour laquelle elle est demandée est considérée par la Partie requise comme une infraction politique ou comme un fait connexe à une telle infraction. / 2. La même règle s'appliquera si la Partie requise a des raisons sérieuses de croire que la demande d'extradition motivée par une infraction de droit commun a été présentée aux fins de poursuivre ou de punir un individu pour des considérations de race, de religion, de nationalité ou d'opinions politiques ou que la situation de cet individu risque d'être aggravée pour l'une ou l'autre de ces raisons " ; que l'extradition de M. A...a été accordée aux fins d'exécuter une peine prononcée pour des faits de violences volontaires et de port d'arme prohibé qui ne sont pas politiques par nature ; qu'il ne ressort d'aucun élément versé au dossier que la demande d'extradition aurait été présentée dans un but politique par les autorités turques ; que le moyen tiré de la méconnaissance de l'article 3 de la convention européenne d'extradition ne peut, par suite, qu'être écarté ; <br/>
<br/>
              4.	Considérant, en quatrième lieu, qu'aux termes des stipulations de l'article 10 de la convention européenne d'extradition : " L'extradition ne sera pas accordée si la prescription (...) de la peine est acquise d'après la législation soit de la partie requérante, soit de la partie requise " ; que ces stipulations ne précisant pas la date à laquelle il y a lieu de se placer pour apprécier la prescription de la peine, les dispositions applicables sur ce point sont celles du 5° de l'article 696-4 du code de procédure pénale, selon lesquelles la prescription de la peine s'apprécie à la date de l'arrestation de la personne réclamée ;<br/>
<br/>
              5.	Considérant que M. A...a été condamné par un jugement de la cour d'assises d'Agri (Turquie) du 29 juin 2006 à une peine de quatre ans et deux mois d'emprisonnement pour des faits relevant des articles 765 et 6136 du code pénal turc, lesquels ont respectivement trait au fait de porter à autrui une souffrance corporelle sans intention de tuer et au port illégal d'une arme et que M. A...a fait l'objet, le 22 janvier 2008, puis le 22 février 2012, de deux mandats d'arrêts émis par le parquet général d'Agri, lesquels constituent des actes interruptifs du délai de prescription ; qu'il a été arrêté par les autorités françaises le 31 mai 2014 à Marignane (Bouches-du-Rhône) ; <br/>
<br/>
              6.	Considérant que le délai de prescription de la peine dont M. A...a fait l'objet est de dix ans selon la législation turque ; qu'à supposer, ainsi qu'il est allégué dans la requête, que les faits pour lesquels M. A...a été condamné seraient susceptibles d'être qualifiés de délit, le délai de prescription de cette peine en droit français est de cinq ans en vertu des dispositions de l'article 133-3 du code pénal ; qu'il résulte de ce qui précède que la prescription de la peine prononcée par la cour d'assises d'Agri contre M. A...par le jugement du 29 juin 2006 n'était acquise ni d'après la législation turque, ni, en tout état de cause, d'après la législation française à la date de son arrestation par les autorités françaises ; que le moyen tiré de ce que le décret attaqué est entaché d'une erreur de droit tirée de ce que la peine sur laquelle il se fonde était prescrite ne peut, dès lors, qu'être écarté ; <br/>
<br/>
              7.	Considérant, en cinquième lieu, qu'il est constant que dans l'avis favorable à l'extradition de M. A...qu'elle a rendu le 12 août 2014, la chambre de l'instruction de la cour d'appel d'Aix-en-Provence s'est fondée sur un mandat d'arrêt émis par le parquet général d'Agri le 22 janvier 2008 ; que le décret attaqué se fonde sur un autre mandat d'arrêt émis par le parquet général d'Agri le 22 février 2012 ; qu'il ressort des pièces du dossier que ces deux mandats d'arrêts successifs ont été émis pour l'exécution du même jugement de la cour d'assises d'Agri (Turquie) ; que, dès lors, la circonstance que le décret attaqué mentionne un autre mandat d'arrêt que celui sur lequel s'est fondée la chambre de l'instruction de la cour d'appel <br/>
d'Aix-en-Provence n'est pas de nature à révéler que le décret attaqué serait entaché d'une erreur de droit ;<br/>
<br/>
              8.	Considérant, en sixième lieu, que la seule circonstance que le requérant a déposé une demande de statut de réfugié et s'est vu remettre une autorisation provisoire de séjour en application de l'article L. 742-1 du code de l'entrée et du séjour des étrangers et du droit d'asile ne fait pas obstacle à ce que le Gouvernement français procède à son extradition ; qu'il appartient au Conseil d'Etat d'apprécier, au vu des éléments qui lui sont soumis et en faisant, le cas échéant, usage de ses pouvoirs d'instruction, si le requérant peut se prévaloir de la qualité de réfugié pour s'opposer à l'exécution du décret ; qu'en l'espèce, par une décision du 3 mars 2016, le Conseil d'Etat statuant au contentieux n'a pas admis le pourvoi de M. A...dirigé contre la décision du 31 août 2015 par laquelle la Cour nationale du droit d'asile a rejeté sa demande tendant au réexamen de sa demande d'asile ; que l'intéressé ne fait état d'aucun élément nouveau qui serait intervenu depuis cette décision ; que, par suite, M. A...ne peut se prévaloir de la qualité de réfugié pour s'opposer à l'exécution du décret attaqué ;<br/>
<br/>
              9.	Considérant qu'il résulte de tout ce qui précède que M. A...n'est pas fondé à demander l'annulation pour excès de pouvoir du décret attaqué ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de M. A...est rejetée. <br/>
<br/>
Article 2 : La présente décision sera notifiée à M. B...A...et au garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
