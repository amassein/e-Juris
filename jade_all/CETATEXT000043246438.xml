<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043246438</ID>
<ANCIEN_ID>JG_L_2021_03_000000445257</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/24/64/CETATEXT000043246438.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 10/03/2021, 445257</TITRE>
<DATE_DEC>2021-03-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>445257</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET BRIARD ; SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>M. Martin Guesdon</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:445257.20210310</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu les procédures suivantes :<br/>
<br/>
              M. I... N... a demandé au tribunal administratif de Montreuil, d'une part, d'annuler les opérations électorales qui se sont déroulées le 28 juin 2020 pour l'élection des conseillers municipaux et communautaires de la commune de L'Ile-Saint-Denis et, d'autre part, de prononcer la suspension du mandat de Mme AR... H.... Par un jugement n° 2006224 du 2 octobre 2020, le tribunal administratif de Montreuil a annulé les opérations électorales et rejeté le surplus de la protestation.<br/>
<br/>
              1° Sous le n° 445257, par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 12 octobre et 12 novembre 2020 et le 8 février 2021 au secrétariat du contentieux du Conseil d'Etat, M. AA... X..., Mme AS...-Marie AJ..., M. AI... Q..., Mme AQ..., M. Y... Paris, Mme C... AO..., M. Nabil E..., Mme Marie P..., M. David AC..., Mme T... AH..., M. AI... AF..., Mme AR... H..., M. AM... D..., M. AE... AP..., Mme Marie-AT... B..., M. AN... V..., Mme O... U..., M. AD... Z..., Mme S... R..., M. F... AG..., Mme L... AB... demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ; <br/>
<br/>
              2°) de rejeter la protestation de M. N... ; <br/>
<br/>
              3°) de mettre à la charge de M. N... la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              2° Sous le n° 445450, par une requête et un mémoire complémentaire, enregistrés les 19 octobre et 12 novembre au secrétariat du contentieux du Conseil d'Etat, Mme AK... W... présente les mêmes conclusions et les mêmes moyens que ceux de la requête enregistrée sous le n° 445257. <br/>
<br/>
              La requête a été communiquée à M. N..., M. Abid et Mme AL..., qui n'ont pas produit de mémoire. <br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - la loi n° 2020-290 du 23 mars 2020 ;<br/>
              - le décret n° 2020-642 du 27 mai 2020 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Martin Guesdon, auditeur,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat de M. X..., au Cabinet Briard, avocat de M. N... et à la SCP Thouvenin, Coudray, Grevy, avocat de Mme W... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. A l'issue du second tour des élections municipales s'étant déroulé le 28 juin 2020 à L'Ile-Saint-Denis, commune de plus de 1 000 habitants située dans le département de la Seine-Saint-Denis, la liste conduite par le maire sortant M. X... a recueilli 1 005 voix, soit 45,93 % des suffrages exprimés, celle menée par M. N... 781 voix, soit 35,69 % des suffrages, celle de M. Abid 268 voix, soit 12,25 % des suffrages, et celle de Mme AL... 134 voix, soit 6,12 % des suffrages. M. X... et ses colistiers font appel du jugement du 2 octobre 2020 par lequel le tribunal administratif de Montreuil a annulé les opérations électorales des 15 mars et 28 juin 2020 et rejeté le surplus de la protestation formée par M. N.... Ils doivent être regardés comme demandant l'annulation de ce jugement en tant qu'il fait droit aux conclusions de M. N....<br/>
<br/>
              2. La requête de M. X... et autres et celle de Mme W... sont dirigées contre le même jugement. Il y a lieu de les joindre pour statuer par une seule décision. <br/>
<br/>
              Sur les griefs retenus par le tribunal administratif : <br/>
<br/>
              3. Aux termes du second alinéa de l'article L. 52-1 du code électoral : " A compter du premier jour du sixième mois précédant le mois au cours duquel il doit être procédé à des élections générales, aucune campagne de promotion publicitaire des réalisations ou de la gestion d'une collectivité ne peut être organisée sur le territoire des collectivités intéressées par le scrutin. (...) ". En vertu du 2° du XII de l'article 19 de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19, l'interdiction mentionnée à l'article L. 52-1 du code électoral court à compter du 1er septembre 2019. Aux termes du deuxième alinéa de l'article L. 52-8 du code électoral : " Les personnes morales (...) ne peuvent participer au financement de la campagne électorale d'un candidat, ni en lui consentant des dons sous quelque forme que ce soit, ni en lui fournissant des biens, services ou autres avantages directs ou indirects à des prix inférieurs à ceux qui sont habituellement pratiqués ".<br/>
<br/>
              4. En premier lieu, si M. N... soutient que M. X... a promu les réalisations de son mandat en publiant au sein du bulletin municipal mensuel, dans les six mois précédant les élections, un éditorial accompagné de sa photo, placé à côté de la présentation des actions réalisées, il résulte de l'instruction que les éditoriaux critiqués, situés au milieu des bulletins municipaux d'une vingtaine de pages chacun et n'occupant que la moitié d'une page, ont été publiés à la même fréquence et sous le même format qu'habituellement. S'ils ont pu, à certaines occasions, traiter de thèmes non dénués de lien avec la campagne électorale, ils sont rédigés dans des termes comparables à ceux des années précédentes et ne peuvent, contrairement à ce qu'a jugé le tribunal administratif pour les éditoriaux des numéros de février et juin 2020, être regardés comme participant d'une campagne de promotion publicitaire au sens des dispositions du second alinéa de l'article L. 52-1 du code électoral. <br/>
<br/>
              5. En deuxième lieu, il résulte de l'instruction que s'il était initialement prévu que les travaux de rénovation du stade municipal Robert César débutent en mars 2020 et se terminent en août de la même année, ce calendrier ayant fait l'objet d'une communication officielle dans le bulletin municipal de l'été 2019, ces travaux ont été reportés une première fois au mois d'avril 2020, ainsi qu'en a fait état le bulletin municipal du mois de février 2020, l'objectif étant de permettre une utilisation des infrastructures sportives à la rentrée scolaire 2020. Lors du premier conseil municipal qui s'est tenu après la fin du confinement décidé le 16 mars 2020 pour lutter contre l'épidémie de covid-19, soit le 10 juin 2020, les élus ont été informés que les travaux ne débuteraient finalement que le 22 juin 2020. Dans ces conditions, le fait que les travaux de réfection de la piste d'athlétisme et de la pelouse du stade, d'ampleur limitée et dont il n'appartient pas au juge de l'élection d'apprécier la légalité, aient commencé pendant la semaine précédant le second tour de scrutin, alors même que M. X... s'en est fait l'écho le 23 juin sur sa page sur le réseau social Facebook et que M. N... critiquait, dans le cadre de la campagne, l'absence de rénovation du stade, n'est pas, dans les circonstances de l'espèce, de nature à caractériser l'existence d'une manoeuvre ayant eu une incidence sur les résultats du scrutin. Par ailleurs, l'annonce du début des travaux sur la page Facebook du bulletin municipal de la commune, purement informative et dénuée de caractère polémique, ne saurait être regardée comme méconnaissant les dispositions de l'article L. 52-1 du code électoral. <br/>
<br/>
              6. En troisième lieu, il résulte de l'instruction qu'après que la métropole du Grand Paris eut décidé d'attribuer une dotation de solidarité exceptionnelle à la commune de L'Ile-Saint-Denis, qui en a reçu la notification le 28 mai 2020, le conseil municipal de la commune a décidé, par une délibération adoptée le 10 juin 2020 lors du premier conseil municipal qui a suivi la fin du confinement, de faire usage de ces fonds pour distribuer des chèques alimentaires aux familles dont les enfants étaient inscrits dans les restaurants scolaires, dont la valeur, comprise entre 30 euros et 100 euros, était fonction du quotient familial. Les chèques alimentaires ont été commandés à la société prestataire le 12 juin et ont été reçus à la trésorerie d'Epinay-sur-Seine le 18 juin, avant d'être remis à la ville le 23 juin. Il n'est pas établi que la distribution des chèques, intervenue au gymnase municipal entre le mercredi 24 juin et le vendredi 26 juin, aurait pu être mise en oeuvre plus tôt. Les familles concernées, averties qu'elles pouvaient venir retirer les chèques alimentaires leur étant destinés par des messages électroniques ainsi que par une information mise en ligne sur la page Facebook de la commune, représentaient 252 électeurs inscrits sur les listes électorales. Seuls 355 carnets de chèques ont toutefois été distribués sur cette période, concernant 125 électeurs inscrits. Dans ces circonstances, cette action, qui répondait à un besoin urgent des familles les plus modestes dont les enfants n'avaient pu se restaurer dans les établissements scolaires pendant la période de confinement et qui s'inscrivait dans le cadre d'autres actions menées par la commune pour venir en aide aux personnes les plus vulnérables du fait de l'épidémie, ne peut être regardée, pour regrettable qu'ait été la diffusion sur le compte Facebook personnel de M. X... d'un message se prévalant des dotations obtenues pour financer l'opération, comme une manoeuvre de nature à altérer la sincérité du scrutin. <br/>
<br/>
              7. Il résulte de ce qui précède que c'est à tort que le tribunal administratif s'est fondé sur ces motifs pour annuler les opérations électorales qui se sont déroulées les 15 mars et 28 juin 2020 dans la commune de L'Ile-Saint-Denis. Il appartient toutefois au Conseil d'Etat, saisi de l'ensemble du litige par l'effet dévolutif de l'appel, d'examiner les autres griefs soulevés par M. N... devant le tribunal administratif de Montreuil.<br/>
<br/>
              Sur les autres griefs de la protestation : <br/>
<br/>
              8. En premier lieu, il résulte de l'instruction que le tract visant personnellement M. N..., distribué selon lui l'avant-veille du scrutin mais dont l'ampleur de la diffusion reste inconnue, n'apporte aucun élément nouveau dans le débat électoral et ne revêt pas de caractère diffamatoire de nature à altérer la sincérité du scrutin. <br/>
<br/>
              9. En deuxième lieu, si M. N... soutient que la diffusion d'un tract mettant en avant la distribution par la commune de masques aux habitants ainsi que l'envoi, le 12 juin 2020, d'une lettre informant les habitants du quartier sud de la ville de l'avancée d'un projet de rénovation les concernant, méconnaissent les dispositions de l'article L. 52-1 du code électoral, ces documents, dont l'ampleur de la diffusion n'est pas établie, n'excèdent pas les limites d'une information institutionnelle et ne sauraient être qualifiés de campagne de promotion publicitaire prohibée par les dispositions de cet article.<br/>
<br/>
              10. En troisième lieu, il résulte de l'instruction que les vidéos par lesquelles M. X... évoque les actions mises en oeuvre par la municipalité pour lutter contre l'épidémie de covid-19, mises en ligne chaque semaine sur la page Facebook du bulletin municipal entre le 23 mars et le 18 juin, consultées par un nombre élevé d'internautes, visaient à informer la population sur les mesures prises pour lutter contre l'épidémie et les conséquences de cette dernière pour la commune. Elles ne contiennent pas d'éléments de polémique électorale, à l'exception de celle du 4 juin 2020, sans lien avec la crise sanitaire, le maire sortant y évoquant le thème de la tranquillité publique en réponse à une proposition formulée par M. N... dans le cadre de la campagne. Cette communication, ponctuelle, ne saurait toutefois constituer une campagne de promotion publicitaire des réalisations de la commune au sens des dispositions du second alinéa de l'article L. 52-1 du code électoral. <br/>
<br/>
              11. En quatrième lieu, aux termes de l'article L. 228 du code électoral : " Sont éligibles au conseil municipal tous les électeurs de la commune et les citoyens inscrits au rôle des contributions directes ou justifiant qu'ils devaient y être inscrits au 1er janvier de l'année de l'élection ". Par ailleurs, l'article L. 11 du code électoral dispose : " I.- Sont inscrits sur la liste électorale de la commune, sur leur demande : / 1° Tous les électeurs qui ont leur domicile réel dans la commune ou y habitent depuis six mois au moins et leurs enfants de moins de 26 ans ". Enfin, le douzième alinéa de l'article L. 231 du code électoral dispose que " Les agents salariés communaux ne peuvent être élus au conseil municipal de la commune qui les emploie (...) ".<br/>
<br/>
              12. D'une part, si M. N... soutient que l'inscription de Mme H... sur les listes électorales revêt le caractère d'une manoeuvre dès lors qu'elle ne réside pas dans la commune de L'Ile-Saint-Denis, il n'apporte, à l'exception de son propre témoignage, aucun élément de nature à établir ses allégations, alors que Mme H... fournit une attestation d'hébergement sur le territoire de la commune depuis juillet 2019 ainsi qu'une facture téléphonique datant de février 2020 à l'adresse mentionnée sur l'attestation. D'autre part, il résulte de l'instruction que Mme H..., adjointe administrative à la ville de L'Ile-Saint-Denis, a été placée en disponibilité à sa demande à compter du 21 février par un arrêté du maire du 20 février 2020. Ainsi, dès lors qu'il n'est pas établi qu'elle aurait continué d'exercer ses fonctions au-delà du 21 février et que M. N... n'apporte aucun élément de nature à caractériser l'existence d'une manoeuvre, le grief tiré de ce que l'inscription de Mme H... sur les listes électorales et sa participation à la campagne constitueraient une manoeuvre de nature à altérer la sincérité du scrutin ne peut qu'être écarté. Il en va de même pour le grief tiré de la méconnaissance des dispositions de l'article L. 52-8 du code électoral.<br/>
<br/>
              13. Il résulte de tout ce qui précède que les requérants sont fondés à soutenir que c'est à tort que, par le jugement qu'ils attaquent, le tribunal administratif de Montreuil a annulé les opérations électorales qui se sont déroulées les 15 mars et 28 juin 2020 pour l'élection des conseillers municipaux et communautaires de la commune de l'Ile-Saint-Denis. <br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              14. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge des requérants qui ne sont pas, dans la présente instance, la partie perdante. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par ceux-ci au titre de ces mêmes dispositions. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Les articles 1er et 3 du jugement du tribunal administratif de Montreuil du 2 octobre 2020 sont annulés.<br/>
<br/>
Article 2 : Les opérations électorales des 15 mars et 28 juin 2020 pour l'élection des conseillers municipaux et communautaires de la commune de l'Ile-Saint-Denis sont validées. <br/>
<br/>
Article 3 : La protestation de M. N... est rejetée. <br/>
Article 4 : Les conclusions présentées par M. X... et autres, par Mme W... et par M. N... au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">28-04-04-01 ÉLECTIONS ET RÉFÉRENDUM. ÉLECTIONS MUNICIPALES. CAMPAGNE ET PROPAGANDE ÉLECTORALES. CAMPAGNE ÉLECTORALE. - MAN&#140;UVRE DE NATURE À ALTÉRER LA SINCÉRITÉ DU SCRUTIN - DISTRIBUTION DE CHÈQUES ALIMENTAIRES - ABSENCE, EN L'ESPÈCE, COMPTE TENU 1) DE L'OBJET DE L'OPÉRATION, 2) DE SON CALENDRIER, ET 3) DE SES MODALITÉS.
</SCT>
<ANA ID="9A"> 28-04-04-01 Commune d'environ 8 000 habitants qui, dans le contexte de la crise sanitaire liée à l'épidémie de covid-19, a distribué entre les deux tours de l'élection municipale des 15 mars et 28 juin 2020 des chèques alimentaires.,,,1) Ces chèques ont été prévus pour les familles dont les enfants sont inscrits dans les restaurants scolaires. Leur valeur, comprise entre 30 et 100 euros, est fonction du quotient familial. Cette action a répondu à un besoin urgent des familles les plus modestes dont les enfants n'avaient pu se restaurer dans les établissements scolaires pendant la période de confinement. Elle s'est inscrite dans le cadre d'autres actions menées par la commune pour venir en aide aux personnes les plus vulnérables du fait de l'épidémie.,,,2) Distribution des chèques alimentaires décidée le 10 juin 2020 lors du premier conseil municipal qui a suivi la fin du confinement, afin de faire usage d'une dotation de solidarité exceptionnelle, consentie par la métropole dont la commune est membre et qui lui avait été notifiée le 28 mai. Chèques commandés à la société prestataire le 12 juin, reçus à la trésorerie le 18, et remis à la ville le 23. Il n'est pas établi que leur distribution, intervenue au gymnase municipal entre le mercredi 24 et le vendredi 26 juin, aurait pu être mise en oeuvre plus tôt.... ,,3) Les familles concernées, averties qu'elles pouvaient venir retirer les chèques alimentaires leur étant destinés par des messages électroniques ainsi que par une information mise en ligne sur la page Facebook de la commune, représentaient 252 électeurs inscrits sur les listes électorales. Seuls 355 carnets de chèques ont toutefois été distribués sur cette période, concernant 125 électeurs inscrits.,,,Dans ces circonstances, et pour regrettable qu'ait été la diffusion sur le compte Facebook personnel du maire sortant, candidat à sa réélection, d'un message se prévalant des dotations obtenues pour financer l'opération, cette action ne peut être regardée comme une manoeuvre de nature à altérer la sincérité du scrutin.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
