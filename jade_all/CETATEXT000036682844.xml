<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036682844</ID>
<ANCIEN_ID>JG_L_2018_03_000000406980</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/68/28/CETATEXT000036682844.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 07/03/2018, 406980, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-03-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>406980</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Yannick Faure</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:406980.20180307</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 18 janvier 2017 et 9 février 2018 au secrétariat du contentieux du Conseil d'Etat, la société Massis distribution demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir, d'une part, la réponse donnée par le ministère des affaires sociales et de la santé le 5 décembre 2016 à sa demande d'information et l'avertissement du sous-directeur des droits indirects du ministère de l'économie et des finances du 23 décembre 2016 relatifs aux délais de mise en conformité des produits du tabac avec les exigences résultant de l'ordonnance n° 2016-623 du 19 mai 2016 portant transposition de la directive 2014/40/UE et de ses textes d'application et, d'autre part, la décision du ministre des affaires sociales et de la santé rejetant sa demande du 5 janvier 2017 tendant à l'octroi d'un délai supplémentaire pour l'écoulement de ses stocks non conformes, par dérogation à l'arrêté du 19  mai 2016 ou, à défaut, à l'abrogation de cet arrêté ;<br/>
<br/>
              2°) à titre subsidiaire, de sursoir à statuer dans l'attente de la réponse de la Cour de justice de l'Union européenne aux questions préjudicielles posées par la décision du Conseil d'Etat, statuant au contentieux, nos 401536, 401561, 401611, 401632 et 401668 et de lui poser une nouvelle question préjudicielle relative à l'interprétation et à la validité de l'article 13 de la directive 2014/40/UE en tant qu'il concerne le tabac à pipe à eau ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 15 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la directive 2014/40/UE du Parlement européen et du Conseil du 3 avril 2014 ;<br/>
              - le code général des impôts ; <br/>
              - le code des relations entre le public et l'administration ; <br/>
              - le code de la santé publique ;<br/>
              - l'ordonnance n° 2016-623 du 19 mai 2016 ;<br/>
              - le décret n° 2016-1117 du 11 août 2016 ;<br/>
              - l'arrêté du 19 mai 2016 relatif aux modalités d'inscription des avertissements sanitaires sur les unités de conditionnement des produits du tabac, des produits du vapotage, des produits à fumer à base de plantes autres que le tabac et du papier à rouler les cigarettes ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yannick Faure, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat du ministre de l'action et des comptes publics.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              Sur les conclusions dirigées contre le courriel adressé à la société Massis distribution le 5 décembre 2016 :<br/>
<br/>
              1. Le courriel adressé par un agent de la direction générale de la santé du ministère des affaires sociale et de la santé à la société Massis distribution le 5 décembre 2016 se borne, en réponse à la demande de la société, à la renseigner sur la date limite de commercialisation des paquets de tabac à pipe à eau non conformes aux dispositions de l'ordonnance du 19 mai 2016 et des textes pris pour son application. Il n'a ainsi pas le caractère d'acte faisant grief et les conclusions de la requête tendant à son annulation pour excès de pouvoir ne sont, par suite, pas recevables.<br/>
<br/>
              Sur les conclusions dirigées contre la mise en demeure de la société Massis distribution de se conformer à la réglementation et le refus opposé à sa demande de dérogation à l'arrêté du 19 mai 2016 ou, à défaut, d'abrogation de cet arrêté :<br/>
<br/>
              2. La directive 2014/40/UE du 3 avril 2014 relative au rapprochement des dispositions législatives, réglementaires et administratives des Etats membres en matière de fabrication, de présentation et de vente des produits du tabac et des produits connexes a renforcé les obligations pesant sur les Etats membres en ce qui concerne notamment l'étiquetage et le conditionnement des produits du tabac, dont le tabac à pipe à eau, en précisant, à ses articles 8 à 12, les dispositions relatives aux avertissements et messages d'information à apposer sur les unités de conditionnement et emballages extérieurs et, à son article 13, les mentions incitatives dont l'usage est interdit sur les unités de conditionnement, les emballages extérieurs et les produits eux-mêmes, y compris lorsqu'il s'agit de noms ou de marques commerciales. Son article 30 dispose que : " Les Etats membres peuvent autoriser jusqu'au 20 mai 2017 la mise sur le marché des produits suivants dès lors qu'ils ne sont pas conformes à la présente directive : / a) les produits du tabac fabriqués ou mis en libre circulation et étiquetés conformément à la directive 2011/37/CE avant le 20 mai 2016 (...) ". <br/>
<br/>
              3. Pour la transposition de la première catégorie d'obligations, l'ordonnance du 19 mai 2016 portant transposition de la directive 2014/40/UE sur la fabrication, la présentation et la vente des produits du tabac et des produits connexes a imposé, par l'article L. 3512-22 qu'elle insère dans le code de la santé publique, l'apposition d'avertissements et d'un message d'information. Les dispositions d'application de cette mesure ont été adoptées par un arrêté du même jour relatif aux modalités d'inscription des avertissements sanitaires sur les unités de conditionnement des produits du tabac, des produits du vapotage, des produits à fumer à base de plantes autres que le tabac et du papier à rouler les cigarettes, publié comme l'ordonnance au Journal officiel de la République française du 20 mai 2016. Cet arrêté précise que les fichiers comportant les images des avertissements peuvent être demandés au ministère chargé de la santé, à une adresse électronique et à une adresse postale qu'il mentionne. Aux termes du II de l'article 6 de l'ordonnance du 19 mai 2016 : " Excepté les cigares, les produits non conformes aux dispositions de la présente ordonnance peuvent être mis à la consommation jusqu'au 20 novembre 2016 et commercialisés jusqu'au 1er janvier 2017. / Pour les cigares, les produits non conformes aux dispositions de la présente ordonnance peuvent être commercialisés jusqu'au 20 mai 2017 ". L'article 17 de l'arrêté du même jour comporte des dispositions transitoires identiques.<br/>
<br/>
              4. Pour la transposition de la seconde catégorie d'obligations, l'ordonnance du 19 mai 2016 a interdit, par l'article L. 3512-21 qu'elle insère dans le code de la santé publique, l'usage de certaines mentions susceptibles d'inciter à la consommation du tabac. Elle a, en outre, modifié l'article 572 du code général des impôts pour prévoir que l'arrêté homologuant le prix de détail d'un produit mentionne sa marque et sa dénomination commerciale à la condition qu'elles respectent les dispositions de l'article L. 3512-21 du code de la santé publique. Les dispositions d'application de cet article L. 3512-21, précisant les mentions interdites, ont été adoptées par le décret du 11 août 2016 relatif à la fabrication, à la présentation, à la vente et à l'usage des produits du tabac, des produits du vapotage et des produits à fumer à base de plantes autres que le tabac, publié au Journal officiel de la République française du 14 août 2016. Les conditions d'entrée en vigueur de ces dispositions ont été fixées par le II de l'article 6 de l'ordonnance du 19 mai 2016 cité au point 3. Toutefois, par une lettre circulaire en date du 22 novembre 2016 relative à l'homologation des prix de vente au détail des tabacs manufacturés, le ministre des affaires sociales et de la santé et le ministre de l'économie et des finances ont accordé aux fabricants et fournisseurs agrées de tabacs manufacturés une tolérance administrative, en prévoyant, pour les produits déjà homologués dont la marque ou la dénomination commerciale n'étaient pas conformes aux nouvelles exigences, qu'ils pourraient bénéficier d'une nouvelle homologation d'une durée d'un an, afin de faciliter l'écoulement des produits en stock.  <br/>
<br/>
              5. En premier lieu, d'une part, s'il incombait au Gouvernement de prendre, dans la mesure où des motifs de sécurité juridique l'exigeaient, des mesures transitoires pour l'entrée en vigueur des dispositions de transposition de la directive du 3 avril 2014, une période transitoire ne pouvait, en principe, légalement avoir pour effet de donner aux opérateurs des délais excédant ceux prévus par la directive elle-même. En fixant au 20 novembre 2016 la date butoir pour la mise à la consommation des produits du tabac non conformes à la directive, le Gouvernement a adopté des dispositions plus favorables aux opérateurs que celles prévues par l'article 30 de la directive précitée, applicables aux seuls produits du tabac fabriqués ou mis en libre circulation avant le 20 mai 2016. Il ne ressort pas des pièces du dossier que les difficultés invoquées par la société Massis distribution pour concevoir et fabriquer les nouveaux conditionnements de ses produits, alors même que les dispositions relatives aux mentions incitatives interdites n'ont été précisées que par le décret du 11 août 2016, seraient, eu égard aux délais nécessaires à cette fin, de nature à caractériser l'existence d'un motif impérieux susceptible de justifier un délai allant au-delà de celui retenu par le Gouvernement pour la mise à la consommation de produits de tabac à pipe à eau non conformes à la directive. A cet égard, la société ne peut utilement se prévaloir ni de la date à laquelle elle a reçu les informations nécessaires à la reproduction des avertissements requis, en réponse à une demande, dont elle ne précise pas la date, qu'elle aurait pu former dès le 20 mai 2016, ni de la circonstance qu'elle attendait l'homologation des prix de détail de ses produits, prévue par l'article 572 du code général des impôts, laquelle ne faisait pas obstacle à l'adaptation des conditionnements de ses produits, ni, en tout état de cause, de la tolérance administrative accordée par la lettre-circulaire du 22 novembre 2016 pour la mise en oeuvre de l'interdiction des marques et dénominations commerciales incitatives. <br/>
<br/>
              6. D'autre part, les dispositions de l'article 30 de la directive 2014/40/UE du 3 avril 2014 citées au point 2 ne prévoient pas un délai supplémentaire d'un an qui serait de droit au bénéfice des fabricants concernés pour se conformer à la réglementation nouvelle, mais se bornent à ouvrir aux Etats la faculté d'impartir aux fabricants et importateurs un délai maximal d'un an pour écouler le stock de produits fabriqués ou importés avant le 20 mai 2016 non conformes à leurs nouvelles obligations. En retenant, au titre des dispositions transitoires, la date du 1er janvier 2017, antérieure à celle du 20 mai 2017, pour la commercialisation du tabac à pipe à eau, le Gouvernement n'a pas méconnu, contrairement à ce que soutient la société requérante, l'objectif d'intelligibilité et d'accessibilité de la norme et a poursuivi, eu égard à la nocivité des produits en cause, dont la consommation s'est fortement accrue durant la dernière décennie, en particulier chez les jeunes, un objectif de protection de la santé publique en relation avec les objectifs retenus par la directive. Il ne ressort pas des pièces du dossier qu'au regard de l'objet et des effets de la nouvelle réglementation, et alors qu'il n'est pas établi que le délai d'écoulement du tabac à pipe à eau par les buralistes serait plus proche de celui des cigares que de celui des cigarettes, une atteinte excessive ait été portée aux intérêts de la société requérante. Par suite, le moyen tiré de la méconnaissance du principe de sécurité juridique doit être écarté.<br/>
<br/>
              7. En deuxième lieu, il ne ressort pas des pièces du dossier, pour les mêmes raisons, que le calendrier retenu pour l'entrée en vigueur des nouvelles obligations applicables aux produits du tabac à pipe à eau aurait, par ses incidences sur la commercialisation de ces produits, méconnu la liberté d'entreprendre.<br/>
<br/>
              8. En dernier lieu, ni l'arrêté du 19 mai 2016 relatif aux modalités d'inscription des avertissements sanitaires, auquel la société requérante a demandé à pouvoir déroger et dont elle a sollicité, à défaut, l'abrogation, ni la mise en demeure qui lui a été faite le 23 décembre 2016 de se conformer à la réglementation applicable à la commercialisation des tabacs manufacturés ne concernent l'interdiction d'utiliser certaines mentions incitatives prévue par l'article 13 de la directive 2014/40/UE du 3 avril 2014. Par suite, la société requérante, qui, au demeurant, a bénéficié de la tolérance administrative mentionnée au point 4 s'agissant des marques et dénominations commerciales, ne peut utilement soutenir, à l'appui de ses conclusions dirigées contre la mise en demeure du 23 décembre 2016 et le rejet de sa demande du 5 janvier 2017, que les dispositions de l'article 13 de la directive méconnaîtraient la liberté d'entreprise, le principe de proportionnalité et le principe de sécurité juridique.<br/>
<br/>
              9. Dès lors, sans qu'il soit besoin de statuer sur les fins de non-recevoir opposées par le ministre de l'action et des comptes publics et le ministre des solidarités et de la santé, la société Massis distribution n'est pas fondée à demander l'annulation de la mise en demeure du 23 décembre 2016 et du refus opposé à sa demande du 5 janvier 2017.<br/>
<br/>
              Sur les frais liés au litige :<br/>
<br/>
              10. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par le ministre de l'action et des comptes publics au titre de ces mêmes dispositions<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la société Massis distribution est rejetée.<br/>
Article 2 : Les conclusions du ministre de l'action et des comptes publics présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à la société Massis distribution, à la ministre des solidarités et de la santé et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
