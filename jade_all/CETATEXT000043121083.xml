<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043121083</ID>
<ANCIEN_ID>JG_L_2021_02_000000449319</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/12/10/CETATEXT000043121083.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 09/02/2021, 449319, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-02-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>449319</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:449319.20210209</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. D... A... a demandé au juge des référés du tribunal administratif de Nantes, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, de suspendre l'exécution de l'arrêté du préfet de la Loire-Atlantique du 22 janvier 2020 portant obligation de quitter le territoire français ainsi que de la mesure d'éloignement prévue le 3 février 2021 et d'enjoindre au préfet de la Loire-Atlantique de renoncer à toute tentative ultérieure de mise à exécution de cet arrêté. Par une ordonnance n° 2101095 du 2 février 2021, le juge des référés du tribunal administratif de Nantes a rejeté sa demande.<br/>
<br/>
              Par une requête et un nouveau mémoire, enregistrés les 2 février et 5 février 2021 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de faire droit à sa demande de première instance ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 000 euros à verser à son conseil au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - la condition de l'urgence est satisfaite dès lors que l'exécution de la mesure d'éloignement du territoire français doit intervenir le 3 février 2021 à 4h25 ;<br/>
              - l'ordonnance attaquée est entachée d'une erreur manifeste d'appréciation  dès lors que la décision contestée du préfet de la Loire-Atlantique porte une atteinte grave et manifestement illégale à son droit de mener une vie familiale normale à la liberté de se marier dès lors, d'une part, que le préfet de la Loire-Atlantique, qui avait connaissance de son projet imminent de mariage avec Mme C..., ressortissante française, a commis un détournement de pouvoir en notifiant une décision d'éloignement dans la précipitation et, d'autre part, que son exécution conduira à une séparation de plusieurs mois et à une impossibilité pour les futurs époux de se marier en raison des restrictions de déplacements vers l'étranger en vigueur afin de lutter contre l'épidémie de Covid-19 ;<br/>
              - la seule circonstance que la relation qu'il entretient avec Mme C... est récente ne saurait être regardée comme de nature à faire naître un doute sur la sincérité et la stabilité de la relation amoureuse, qui sont attestées par la vie commune qu'il mène avec l'intéressée ; <br/>
              - le préfet de la Loire-Atlantique ne saurait se fonder sur l'absence de concrétisation à ce jour du projet de mariage dès lors que des démarches ont été entreprises afin de réunir l'ensemble des pièces nécessaires pour constituer le dossier à l'appui de la demande de mariage déposée en mairie de Couëron le 20 janvier 2020 ;<br/>
              - en se fondant sur la circonstance qu'il n'a pas fait l'objet d'une décision portant interdiction de séjour en France pour en déduire qu'il n'était pas fondé à soutenir qu'il lui serait impossible d'obtenir un visa pour se marier sur le territoire français, le préfet de la Loire-Atlantique n'a pas tenu compte du décret n° 2020-1310 du 29 octobre 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de Covid-19 dans le cadre de l'état d'urgence sanitaire, qui ne mentionnent pas la célébration d'un mariage au nombre des motifs impérieux pouvant justifier un déplacement à destination de la France ; <br/>
              - le juge des référés du tribunal administratif de Nantes a entaché son ordonnance d'une erreur manifeste d'appréciation en estimant que les difficultés actuellement rencontrées pour les déplacements internationaux sont sans incidence sur son projet de mariage avec Mme C..., dès lors que l'intéressée ne peut se rendre en Azerbaïdjan, les frontières de ce pays étant actuellement fermées.<br/>
<br/>
              Par un mémoire en défense, enregistré le 3 février 2021, le ministre de l'intérieur conclut, à titre principal, au rejet de la requête pour irrecevabilité dès lors que le juge administratif s'est prononcé sur la demande d'annulation de l'arrêté litigieux du 18 octobre 2017 sur le fondement des dispositions du III de l'article L. 512-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, en l'absence de changements dans les circonstances de droit ou de fait survenus depuis l'intervention de la mesure d'éloignement et de son jugement. Il conclut, à titre subsidiaire, au rejet de la requête en faisant valoir que la condition d'urgence n'est pas remplie et qu'il n'est porté aucune atteinte grave et manifestement illégale aux libertés fondamentales invoquées. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. A..., et d'autre part, le ministre de l'intérieur ;<br/>
<br/>
              Ont été entendus lors de l'audience publique du 4 février 2021 à 15 heures :<br/>
<br/>
              - Me Rameix, avocat au Conseil d'Etat et à la Cour de Cassation, avocat de M. A... ;<br/>
              - les représentants du ministre de l'intérieur ; <br/>
<br/>
              à l'issue de laquelle le juge des référés a différé la clôture de l'instruction au vendredi 5 février à 12 heures. <br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ".<br/>
<br/>
              2. M. D... A..., ressortissant azerbaïdjanais né le 3 septembre 1995, a déclaré être entré sur le territoire français en provenance d'Italie le 9 août 2017, muni d'un visa de court séjour. La demande d'asile qu'il a formée a été rejetée par décision de l'Office français de protection des réfugiés et des apatrides du 17 avril 2018, et ce rejet a été confirmé par un arrêt de la Cour nationale du droit d'asile du 13 septembre 2018. Par une décision du 17 avril 2018, le préfet de la Loire-Atlantique a refusé de délivrer un titre de séjour à M. A... et lui a notifié l'obligation de quitter le territoire français. L'intéressé a contesté ce refus de délivrance d'un titre de séjour devant le tribunal administratif de Nantes qui a rejeté sa demande par un jugement n° 1811342 du 30 janvier 2019. Par un arrêté du 22 janvier 2020, le préfet de la Loire-Atlantique lui a notifié une nouvelle décision portant obligation de quitter le territoire français, suivie d'une décision portant assignation à résidence pour une durée de 6 mois prononcée par arrêté du 20 octobre 2020, dans l'attente d'une réouverture des frontières permettant son éloignement effectif. Par un arrêté du 17 décembre 2020, dont M. A... a demandé l'annulation devant le tribunal administratif de Nantes, le préfet de la Loire-Atlantique lui a notifié un nouveau refus de délivrance d'un titre de séjour et lui a rappelé qu'il faisait l'objet d'une décision portant obligation de quitter le territoire français. Le 29 janvier 2021, M. A... a été convoqué par la gendarmerie de Couëron et s'est vu notifier la mise à exécution de la décision d'éloignement pour le 3 février 2021. Il relève appel de l'ordonnance du 2 février 2021 par laquelle le juge des référés du tribunal administratif de Nantes, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, a rejeté sa demande tendant à la suspension de l'exécution de l'arrêté du préfet de la Loire-Atlantique du 22 janvier 2020 portant obligation de quitter le territoire français ainsi que de la mesure d'éloignement prévue le 3 février 2021 et à ce qu'il soit enjoint au préfet de renoncer à toute tentative ultérieure de mise à exécution de cet arrêté.<br/>
<br/>
              Sur les conclusions tendant à la suspension de la mesure d'éloignement prévue le 3 février 2021 :<br/>
<br/>
              3. Il résulte de l'instruction que M. A... s'est présenté le 3 février 2021 à 4h30 à l'aéroport de Nantes, mais que la mesure d'éloignement dont il devait faire l'objet n'a pu être exécutée en raison de l'annulation du vol prévu à destination de l'Azerbaïdjan. Dans ces conditions, les conclusions de M. A... tendant à ce que le juge des référés fasse usage des pouvoirs qu'il tient de l'article L. 521-2 du code de justice administrative afin de suspendre cette mesure d'éloignement sont devenues sans objet. Il n'y a donc pas lieu d'y statuer.<br/>
<br/>
              Sur les conclusions tendant à la suspension de l'obligation de quitter le territoire français :<br/>
<br/>
              4. En premier lieu, si M. A... a fait valoir que les liens affectifs noués depuis le mois de septembre 2019 avec Mme B... C..., ressortissante française, ont conduit à une vie commune à partir du mois de mars 2020 et à un projet de mariage devant se concrétiser en 2021, qui a amené les intéressés à entreprendre auprès de la mairie de Couëron et du consulat d'Azerbaïdjan les démarches nécessaires à la constitution du dossier d'Etat civil, il ne résulte pas de l'instruction que la célébration de son mariage était imminente. Lors de l'entretien qui s'est tenu le 4 février 2021 avec l'officier d'Etat civil, au cours duquel les futurs époux ont fait part de leur souhait de célébrer leur union le 20 mars 2021, ce dernier a constaté que le dossier était incomplet en l'absence de production de l'acte de naissance apostillé de M. A.... En outre, il n'est pas contesté que M. A... n'a fait l'objet d'aucune mesure lui interdisant un retour en France en vue de la célébration de cette union. Enfin, il résulte de l'instruction que les restrictions mises en place dans le cadre de l'état d'urgence sanitaire en matière de transport aérien pour faire face à l'épidémie de Covid-19 et à la propagation des nouveaux variants de ce virus présentent, par nature, un caractère temporaire, et sont appelées à être levées dès que la crise aura pris fin. Par suite, le requérant n'est pas fondé à soutenir que la décision du préfet de la Loire-Atlantique du 22 janvier 2020 portant obligation de quitter le territoire français dont il fait l'objet porterait une atteinte disproportionnée, d'une part, à son droit au respect de sa vie privée et familiale garanti par les stipulations de l'article 8 de la Convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et, d'autre part, à son droit au mariage.<br/>
<br/>
              5. En second lieu, si M. A... soutient que le préfet de la Loire-Atlantique avait connaissance de son projet de mariage avec Mme C... et que la décision d'éloignement du territoire français est, ainsi, entachée de détournement de pouvoir dès lors qu'elle a été prise dans la précipitation et avait pour seul objet de s'opposer à la célébration de son mariage, il résulte des circonstances de l'espèce, rappelées au point 4, qu'à la date à laquelle le préfet de la Loire-Atlantique a notifié à M. A... l'arrêté portant obligation de quitter le territoire français dont la suspension est demandée, ce projet de mariage ne présentait pas un caractère imminent. Par suite, ce moyen ne peut qu'être écarté. <br/>
<br/>
              6. Il résulte de ce qui précède que la condition tenant à l'existence d'une atteinte grave et manifestement illégale à la situation de M. A... ne peut être regardée comme satisfaite. Par suite, sans qu'il soit besoin de se prononcer sur la fin de non-recevoir opposée par le ministre de l'intérieur et sur la condition d'urgence prévue par l'article L. 521-2 du code de justice administrative, les conclusions tendant à la suspension de l'obligation de quitter le territoire français et à ce qu'une injonction soit prononcée doivent être rejetées. <br/>
<br/>
              Sur les frais de l'instance : <br/>
<br/>
              7. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées au titre des dispositions des articles 37 de la loi du 10 juillet 1991 et L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de statuer sur les conclusions de la requête de M. A... présentées sur le fondement de l'article L. 521-2 du code de justice administrative et tendant à la suspension de la mesure d'éloignement prévue le 3 février 2021.<br/>
Article 2 : Le surplus des conclusions de la requête de M. A... est rejeté. <br/>
Article 3 : La présente ordonnance sera notifiée à M. D... A... et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
