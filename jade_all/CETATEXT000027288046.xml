<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027288046</ID>
<ANCIEN_ID>JG_L_2013_04_000000346001</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/28/80/CETATEXT000027288046.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 08/04/2013, 346001, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-04-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>346001</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON</AVOCATS>
<RAPPORTEUR>Mme Séverine Larere</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Claire Legras</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:346001.20130408</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 24 janvier et 26 avril 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B... A..., demeurant... ; M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09PA02619 du 18 novembre 2010 par lequel la cour administrative d'appel de Paris a rejeté sa requête tendant, d'une part, à l'annulation du jugement n° 0506346, 0600680, 0601459 du tribunal administratif de Melun du 18 mars 2009 en tant qu'il a rejeté ses conclusions tendant à la décharge de l'obligation de payer la somme de 219 641,41 euros correspondant à des compléments d'impôt sur le revenu établis au titre des années 1990 et 1991, résultant d'un commandement de payer du 27 juin 2005 et d'avis à tiers détenteur émis le 20 septembre 2005 et, d'autre part, au prononcé de la décharge demandée et à la condamnation de l'Etat à lui rembourser les sommes versées depuis l'acquisition de la prescription et les intérêts de retard y afférents ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement d'une somme de 3 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu la loi n° 87-502 du 8 juillet 1987 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Séverine Larere, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Boré, Salve de Bruneton, avocat de M. A...,<br/>
<br/>
              - les conclusions de Mme Claire Legras, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Boré, Salve de Bruneton, avocat de M. A... ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'à la suite d'une vérification de la comptabilité de son activité de conseil en entreprise portant sur les années 1989, 1990 et 1991, M. A...s'est vu assigner des cotisations supplémentaires d'impôt sur le revenu qui ont été mises en recouvrement le 30 septembre 1994 ; qu'il a formé une réclamation préalable, assortie d'une demande de sursis de paiement, à l'encontre de ces impositions, reçue par l'administration le 17 octobre 1994 ; que, par courrier du 18 janvier 1995, le comptable lui a demandé de constituer des garanties dans un délai de quinze jours en lui précisant qu'à défaut, le sursis de paiement lui serait refusé et que des poursuites seraient exercées à son encontre ; que, par lettre du 30 juin 1995, M. A... a proposé de garantir les impositions contestées par l'affectation hypothécaire d'un bien immobilier situé à Montigny-sur-Loing et de s'acquitter de la somme de 200 000 francs par des versements mensuels de 10 000 francs à compter du 25 juillet 1995 ; que le comptable a accepté l'échéancier de paiement ainsi proposé et a procédé, le 1er juillet 1995, à l'inscription de son hypothèque légale sur le bien immobilier de Montigny-sur-Loing ; qu'à la suite du rejet, par un jugement du tribunal administratif de Melun du 4 novembre 2004, de la demande de décharge formée par M. A... à l'encontre des impositions supplémentaires mises à sa charge, l'administration lui a adressé, le 27 juin 2005, un commandement de payer la somme de 219 641,41 euros correspondant aux impositions restant dues au titre des années 1990 et 1991 puis a émis, le 20 septembre 2005, plusieurs avis à tiers détenteur aux fins de recouvrer cette somme ; que M. A... a contesté ces actes de poursuites en invoquant la prescription de l'action en recouvrement ; que, par jugement du 18 mars 2009, le tribunal administratif de Melun a rejeté ses demandes tendant à la décharge de l'obligation de payer les sommes réclamées ; que M. A... se pourvoit en cassation contre l'arrêt du 18 novembre 2010 par lequel la cour administrative d'appel de Paris a confirmé ce jugement ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 274 du livre des procédures fiscales, dans sa rédaction applicable au litige : " Les comptables du Trésor qui n'ont fait aucune poursuite contre un contribuable retardataire pendant quatre années consécutives, à partir du jour de la mise en recouvrement du rôle perdent leur recours et sont déchus de tous droits et de toute action contre ce redevable. / Le délai de quatre ans mentionné au premier alinéa, par lequel se prescrit l'action en vue du recouvrement, est interrompu par tous actes comportant reconnaissance de la part des contribuables et par tous autres actes interruptifs de la prescription. " ; que selon l'article L. 277 du même livre, dans sa rédaction alors applicable, issue de la loi du 8 juillet 1987 : "  Le contribuable qui conteste le bien-fondé ou le montant des impositions mises à sa charge peut, s'il en a expressément formulé la demande dans sa réclamation et précisé le montant ou les bases du dégrèvement auquel il estime avoir droit, être autorisé à différer le paiement de la partie contestée de ces impositions et des pénalités y afférentes. Le sursis de paiement ne peut être refusé au contribuable que s'il n'a pas constitué auprès du comptable les garanties propres à assurer le recouvrement de la créance du Trésor. / (...) / A défaut de constitution de garanties ou si les garanties offertes sont estimées insuffisantes, le comptable peut prendre des mesures conservatoires pour les impôts contestés, jusqu'à la saisie inclusivement. (...) " ; qu'aux termes, enfin, de l'article R. 277-1 du même livre, dans sa rédaction alors applicable : " Le comptable compétent invite le contribuable qui a demandé à différer le paiement des impositions à constituer les garanties prévues à l'article L. 277. Le contribuable dispose d'un délai de quinze jours à compter de la réception de l'invitation formulée par le comptable pour faire connaître les garanties qu'il s'engage à constituer. / (...) / Si le comptable estime ne pas pouvoir accepter les garanties offertes par le contribuable parce qu'elles ne répondent pas aux conditions prévues au deuxième alinéa, il lui notifie sa décision par lettre recommandée " ;<br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions que, lorsque le redevable a assorti sa réclamation d'une demande de sursis de paiement et qu'il n'a pas déféré à la demande du comptable de constituer des garanties dans le délai de quinze jours fixé par l'article R. 277-1 du livre des procédures fiscales, le délai de prescription de l'action en recouvrement, qui avait été suspendu à compter de la réception, par l'administration, de sa demande de sursis de paiement, recommence à courir à l'expiration du délai imparti au contribuable pour constituer des garanties ; qu'il en résulte qu'en jugeant que le délai de prescription de l'action en recouvrement des impositions complémentaires mises à la charge de M. A... avait été suspendu du 17 octobre 1994, date de réception, par l'administration, de sa réclamation préalable assortie d'une demande de sursis de paiement, jusqu'au jugement du tribunal administratif de Melun du 4 novembre 2004 rejetant sa demande de décharge, alors qu'il ressortait des pièces de son dossier et qu'il n'était pas contesté que M. A... n'avait pas constitué de garanties dans le délai de quinze jours qui lui avait été imparti par le comptable, conformément aux dispositions de l'article R. 277-1 du livre des procédures fiscales, dans son courrier du 18 janvier 1995, la cour a entaché son arrêt d'une erreur de droit ; que M. A... est, par suite et sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, fondé à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui a été dit précédemment que le délai de prescription de l'action en recouvrement, qui avait été suspendu à compter du 17 octobre 1994 du fait de la demande de sursis de paiement présentée par M. A..., a recommencé à courir à l'expiration du délai de quinze jours qui lui avait été imparti par l'administration pour constituer des garanties ;<br/>
<br/>
              6. Considérant, toutefois, que l'acceptation, par l'administration, le 30 juin 1995, de l'échéancier de paiement proposé par le contribuable portant sur la somme de 200 000 francs a eu pour effet d'interrompre le cours de la prescription en ce qui concerne le recouvrement de cette somme ; que, par ailleurs, en inscrivant, le 1er juillet 1995, son hypothèque légale sur le bien immobilier appartenant à M. et Mme A..., l'administration doit être regardée comme ayant accepté, à cette date, la garantie proposée par le contribuable et comme lui ayant accordé le bénéfice du sursis de paiement ; que le délai de prescription de l'action en recouvrement s'est, ainsi, trouvé interrompu en ce qui concerne le recouvrement du surplus des impositions jusqu'à la date du 4 novembre 2004 à laquelle le tribunal administratif a rejeté sa demande de décharge ; qu'il n'était, dès lors, pas expiré à la date à laquelle l'administration a décerné le commandement de payer du 27 juin 2005 et les avis à tiers détenteurs du 30 septembre 2005 ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin de statuer sur les fins de non-recevoir opposées en défense par l'administration, M. A... n'est pas fondé à se plaindre de ce que, par jugement du 18 mars 2009, le tribunal administratif de Melun a rejeté ses demandes tendant à la décharge de l'obligation de payer résultant de ces actes de poursuites ;<br/>
<br/>
              8. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce l'Etat, qui n'est pas la partie perdante dans la présente instance, verse à M. A... la somme qu'il réclame à ce titre ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 18 novembre 2010 est annulé.<br/>
Article 2 : La requête présentée par M. A... devant la cour administrative d'appel de Paris et le surplus des conclusions de son pourvoi sont rejetés.<br/>
Article 3 : La présente décision sera notifiée à M. B... A...et au ministre de l'économie et des finances.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
