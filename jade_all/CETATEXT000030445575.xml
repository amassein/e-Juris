<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030445575</ID>
<ANCIEN_ID>JG_L_2015_03_000000368093</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/44/55/CETATEXT000030445575.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème / 4ème SSR, 20/03/2015, 368093</TITRE>
<DATE_DEC>2015-03-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>368093</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème / 4ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:368093.20150320</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Grenoble d'annuler la décision 48 SI du ministre de l'intérieur, de l'outre-mer et des collectivités territoriales du 23 décembre 2009 récapitulant les retraits de points réalisés sur son permis de conduire, l'informant de la perte de validité de ce titre pour solde de points nul et lui enjoignant de le restituer. Par un jugement n° 1000258 du 21 février 2012, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 12LY00982 du 28 février 2013, la cour administrative d'appel de Lyon a rejeté l'appel formé contre ce jugement par M.A.... <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire enregistrés les 26 avril et 26 juillet 2013 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de la route ;<br/>
<br/>
- le code de procédure pénale ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Le Prado, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 223-1 du code de la route, dans sa rédaction antérieure au 31 décembre 2007 : " Le permis de conduire est affecté d'un nombre de points. Celui-ci est réduit de plein droit si le titulaire du permis a commis une infraction pour laquelle cette réduction est prévue. / A la date d'obtention du permis de conduire, celui-ci est affecté, pendant un délai probatoire de trois ans, de la moitié du nombre maximal de points. Ce délai probatoire est réduit à deux ans lorsque le titulaire du permis de conduire a suivi un apprentissage anticipé de la conduite. A l'issue de ce délai probatoire, le permis de conduire est affecté du nombre maximal de points, si aucune infraction ayant donné lieu au retrait de points n'a été commise... " ; qu'aux termes de l'article R. 223-1 du même code, dans sa rédaction applicable au litige : " ...II. - A la date d'obtention du permis de conduire, celui-ci est affecté d'un nombre initial de six points. / III. - Pendant le délai probatoire défini à l'article L. 223-1, le permis de conduire ne peut être affecté d'un nombre de points supérieur à six. / IV. - A l'issue de ce délai probatoire, si aucune infraction ayant donné lieu à retrait de points n'a été commise, le permis de conduire est affecté du nombre maximal de douze points. / En cas de commission d'infraction ayant donné lieu à retrait de points au cours du délai probatoire, l'affectation du nombre maximal de points intervient dans les conditions définies à l'article L. 223-6 " ; qu'aux termes de ce dernier article : " Si le titulaire du permis de conduire n'a pas commis, dans le délai de trois ans à compter de la date du paiement de la dernière amende forfaitaire, de l'émission du titre exécutoire de la dernière amende forfaitaire majorée, de l'exécution de la dernière composition pénale ou de la dernière condamnation définitive, une nouvelle infraction ayant donné lieu au retrait de points, son permis est affecté du nombre maximal de points... " ; <br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond, notamment du relevé intégral d'information, qu'à la suite d'infractions commises les 10 et 11 mai 2008 et le 22 octobre 2009, l'administration a procédé au retrait de huit points du permis de conduire de M.A... ; qu'en retenant que ces trois infractions avaient entraîné la perte de dix points, la cour administrative d'appel de Lyon a dénaturé les pièces qui lui étaient soumises ; qu'il y a lieu, par suite, d'annuler son arrêt ;<br/>
<br/>
              3. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-1 du code de justice administrative ; <br/>
<br/>
              4. Considérant, en premier lieu, qu'il ressort des pièces du dossier que le capital de points du permis de M. A...était de quatre points au terme de la période probatoire, dès lors que deux points avaient été retirés à la suite d'une infraction commise le 11 mai 2007 ; que, le 17 juin 2008, ce solde a été porté à huit points à la suite de l'accomplissement par l'intéressé d'un stage de sensibilisation à la sécurité routière ; que ces huit points lui ont été ultérieurement retirés à la suite de trois infractions constatées les 10 et 11 mai 2008 et le 22 octobre 2009 ; qu'ainsi, son solde de points s'est trouvé nul après le retrait de points consécutif à cette dernière infraction ; <br/>
<br/>
              5. Considérant, en deuxième lieu, que les conditions de la notification au conducteur des retraits de points de son permis de conduire, prévue par les dispositions de l'article L. 223-3 du code de la route, ne conditionnent pas la régularité de la procédure suivie ni, partant, la légalité de ces retraits ; que M. A...ne saurait, en tout état de cause, invoquer utilement l'irrégularité de cette notification ; que la décision " 48 SI ", dont M. A...a eu notification, récapitule les retraits de points antérieurs ; que, par suite et contrairement à ce qu'il soutient, ces retraits lui sont opposables ;<br/>
<br/>
              6. Considérant, en troisième lieu, que le ministre de l'intérieur produit les procès-verbaux de contravention établis lors des infractions des 11 mai 2007, 10 et 11 mai 2008 et 22 octobre 2009 ; que M. A... a signé ces procès-verbaux sous la mention par laquelle il reconnaît avoir reçu la carte de paiement et l'avis de contravention ; que les formulaires utilisés sont conformes aux dispositions des articles A. 37 à A. 37-4 du code de procédure pénale et mentionnent qu'un retrait de points est susceptible d'affecter le permis de conduire du conducteur ; que M.A...  n'établit pas que les avis qui lui ont été remis seraient inexacts ou incomplets ; qu'eu égard aux mentions dont les avis de contraventions sont réputés être revêtus, l'administration doit ainsi être regardée comme s'étant acquittée envers lui de l'obligation de lui délivrer l'information prévue par les article L. 223-3 et R. 223-3 du code de la route ; <br/>
<br/>
              7. Considérant, en quatrième lieu,  qu'aux termes du quatrième alinéa de l'article L. 223-1 du code de la route : " La réalité d'une infraction entraînant retrait de points est établie par le paiement d'une amende forfaitaire ou l'émission du titre exécutoire de l'amende forfaitaire majorée, l'exécution d'une composition pénale ou par une condamnation définitive " ; qu'il n'est pas établi ni même allégué que M.A..., qui a été personnellement identifié lors de l'interception de son véhicule lorsqu'a été constatée l'infraction du 11 mai 2007 et auquel, ainsi qu'il a été dit ci-dessus, l'agent verbalisateur a délivré l'information prévue par les article L. 223-3 et R. 223-3 du code de la route, aurait élevé une contestation au titre de cette infraction dans le délai qui lui était imparti ; qu'il ne peut dès lors contester la matérialité de celle-ci en se prévalant de ce que l'amende forfaitaire correspondant à cette infraction a été acquittée par la société titulaire du certificat d'immatriculation du véhicule et non par lui-même ;<br/>
<br/>
              8 Considérant, en cinquième lieu, qu'il résulte des dispositions des articles L. 223-3 et R. 223-3 du code de la route que la décision de réduction du nombre de points intervient seulement lorsque la réalité de l'infraction est établie, par le paiement de l'amende forfaitaire, l'émission du titre exécutoire de l'amende majorée, l'exécution d'une condamnation pénale ou la condamnation définitive prononcée par un juge pénal qui statue sur tous les éléments de droit et de fait portés à sa connaissance ; que le conducteur est informé par l'autorité administrative, dès la constatation de l'infraction, de la perte de points qu'il peut encourir ; qu'ainsi, le retrait de points ne peut intervenir qu'en cas de reconnaissance de responsabilité pénale, le cas échéant après appréciation par le juge judiciaire de la réalité de l'infraction et son imputabilité à la demande de l'intéressé ; que, dès lors, le moyen tiré de ce que les dispositions du code de la route relatives au permis à points méconnaîtraient les stipulations du paragraphe 1 de l'article 6 de la convention de sauvegarde des droits de l'homme et des libertés fondamentales  doit, compte tenu des garanties accordées à l'auteur de l'infraction et alors même qu'elles prévoient que le retrait de points est prononcé par une autorité administrative, être écarté ; <br/>
<br/>
              9. Considérant, enfin, que le principe de non cumul des peines garanti, notamment, par l'article 4 du protocole additionnel n° 7 à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ne s'oppose pas à ce que puissent être infligées à raison des mêmes faits des sanctions distinctes dès lors que celles-ci visent à assurer le respect de réglementations distinctes ou à protéger des intérêts spécifiques ; qu'ainsi, ce principe ne fait pas obstacle à ce qu'un conducteur déjà sanctionné pénalement pour une infraction au code de la route dont la réalité a été établie se voie infliger la sanction administrative de retrait de points de son permis ; <br/>
<br/>
              10. Considérant qu'il résulte de tout ce qui précède que M. A...n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Grenoble a rejeté sa demande dirigée contre la décision du ministre de l'intérieur du 23 septembre 2009 ; que, par voie de conséquence, ses conclusions à fin d'injonction doivent elles-mêmes être rejetées ;<br/>
<br/>
              11. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance ; que si l'Etat, qui n'a pas eu recours au ministère d'avocat, demande qu'une somme soit mise au même titre à la charge de M.A..., eu égard au surcroît de travail imposé par le traitement de l'ensemble des litiges relatifs aux permis de conduire, le ministre  ne fait état d'aucun frais spécifiquement exposé par ses services pour assurer la défense de l'Etat ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon du 28 février 2013 est annulé. <br/>
<br/>
		Article 2 : L'appel de M. A...est rejeté. <br/>
<br/>
Article 3 : Les conclusions présentées par M. A...et par l'Etat au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. B...A...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">49-04-01-04-025 POLICE. POLICE GÉNÉRALE. CIRCULATION ET STATIONNEMENT. PERMIS DE CONDUIRE. - RÉALITÉ DE L'INFRACTION ÉTABLIE PAR LE PAIEMENT DE L'AMENDE FORFAITAIRE (ART. L. 223-1 DU CODE DE LA ROUTE) - PAIEMENT PAR UN TIERS - CIRCONSTANCE SANS INCIDENCE, DÈS LORS QUE LE CONDUCTEUR N'A PAS ÉLEVÉ DE CONTESTATION EN TEMPS UTILE.
</SCT>
<ANA ID="9A"> 49-04-01-04-025 Conducteur personnellement identifié lors de l'interception de son véhicule, auquel l'agent verbalisateur a délivré l'information prévue par les articles L. 223-3 et R. 223-3 du code de la route. Faute d'avoir élevé une contestation au titre de cette infraction dans le délai qui lui était imparti, il ne peut dès lors contester la matérialité de celle-ci en se prévalant de ce que l'amende forfaitaire correspondant à cette infraction a été acquittée par la société titulaire du certificat d'immatriculation du véhicule et non par lui-même.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 25 février 2011, Brouard, n° 338692, T. p. &#133;.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
