<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036927183</ID>
<ANCIEN_ID>JG_L_2018_05_000000417841</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/92/71/CETATEXT000036927183.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Formation spécialisée, 04/05/2018, 417841, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-05-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417841</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Formation spécialisée</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Emmanuelle Prada Bordenave</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CEFSP:2018:417841.20180504</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une ordonnance n° 1719085 du 23 janvier 2018, enregistrée au secrétariat du contentieux du Conseil d'Etat le 1er février 2018, la présidente du tribunal administratif de Paris a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête que Mme A...B...avait présentée à ce tribunal.<br/>
<br/>
              Par cette requête, enregistrée au greffe du tribunal administratif le 14 décembre 2017, et un mémoire en réplique, enregistré au secrétariat du contentieux du Conseil d'Etat le<br/>
13 mars 2018 Mme B...demande :<br/>
<br/>
              1°) de condamner l'État à lui verser une provision de 50 000 euros en réparation du préjudice moral subi en raison de la faute commise par la direction du renseignement militaire qui a illégalement détenu dans ses traitements de données des informations la concernant ;<br/>
<br/>
              2°) de condamner l'État à lui verser une provision de 25 000 euros en réparation du préjudice subi du fait de l'application de la loi n° 2015-912 du 24 juillet 2015 ;<br/>
<br/>
              3°) de condamner l'État à lui verser une provision de 25 000 euros en réparation du préjudice subi du fait de la décision n° 396550 du Conseil d'Etat statuant au contentieux du 8 novembre 2017 ;<br/>
<br/>
              4°) d'écarter des débats le mémoire en défense de la ministre des armées ; <br/>
<br/>
              5°) de mettre à la charge de l'Etat une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - le code de la sécurité intérieure ; <br/>
              - la loi n° 78-17 du 6 janvier 1978 ;<br/>
              - le décret n° 2005-1309 du 20 octobre 2005 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir convoqué à une séance à huis-clos, d'une part, Mme A...B..., et d'autre part, la ministre des armées et la Commission nationale de l'informatique et des libertés, qui ont été mis à même de prendre la parole avant les conclusions ;<br/>
<br/>
              Et après avoir entendu en séance :<br/>
              - le rapport de Mme Emmanuelle Prada Bordenave, conseillère d'Etat, <br/>
              - et, hors la présence des parties, les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1.  Aux termes de l''article L. 841-2 du code de la sécurité intérieure dispose que : " Le Conseil d'Etat est compétent pour connaître, dans les conditions prévues au chapitre III bis du titre VII du livre VII du code de justice administrative, des requêtes concernant la mise en oeuvre de l'article 41 de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, pour les traitements ou parties de traitements intéressant la sûreté de l'Etat dont la liste est fixée par décret en Conseil d'Etat " . L'article R. 841-2 du même code prévoit que : " Relèvent des dispositions de l'article L. 841-2 du présent code les traitements ou parties de traitements automatisés de données à caractère personnel intéressant la sûreté de l'Etat autorisés par les actes réglementaires ou dispositions suivants : (...) 4° Décret portant application des dispositions de l'article 31 de la loi n° 78/17 du 6 janvier 1978 au fichier d'informations nominatives mis en oeuvre par la direction du renseignement militaire ; (...) ".<br/>
<br/>
              2. L'article L. 773-1 du code de justice administrative dispose que : "  Le Conseil d'Etat examine les requêtes présentées sur le fondement des articles L. 841-1 et L. 841-2 du code de la sécurité intérieure conformément aux règles générales du présent code sous réserves des dispositions particulières du présent chapitre et du chapitre IV du titre V du livre VIII du code de sécurité intérieure " . L'article L. 773-8 du même code prévoit que : <br/>
" Lorsqu'elle traite des requêtes relatives à la mise en oeuvre de l'article 41 de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, la formation de jugement se fonde sur les éléments contenus, le cas échéant, dans le traitement sans les révéler ni révéler si le requérant figure ou non dans le traitement. Toutefois, lorsqu'elle constate que le traitement ou la partie de traitement faisant l'objet du litige comporte des données à caractère personnel le concernant qui sont inexactes, incomplètes, équivoques ou périmées, ou dont la collecte, l'utilisation, la communication ou la conservation est interdite, elle en informe le requérant, sans faire état d'aucun élément protégé par le secret de la défense nationale. Elle peut ordonner que ces données soient, selon les cas, rectifiées, mises à jour ou effacées. Saisie de conclusions en ce sens, elle peut indemniser le requérant ".<br/>
<br/>
              3. L'article L. 311-4-1 du code de justice administrative prévoit que : "  Le Conseil d'Etat est compétent pour connaître, en premier et dernier ressort, des requêtes concernant (...) la mise en oeuvre de l'article 41 de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, pour certains traitements ou parties de traitements intéressant la sûreté de l'État. / Le Conseil d'Etat peut être saisi, en premier et dernier ressort, comme juge des référés ". L'article R. 541-1 du même code prévoit que : "  Le juge des référés peut, même en l'absence d'une demande au fond, accorder une provision au créancier qui l'a saisi lorsque l'existence de l'obligation n'est pas sérieusement contestable (...) ".<br/>
<br/>
              4. Par une décision n° 396550 du 8 novembre 2017, le Conseil d'Etat statuant au contentieux a ordonné l'effacement de données concernant Mme B...qui étaient illégalement contenues dans les traitements de données de la direction du renseignement militaire. Par la présente requête, Mme B...demande que l'Etat soit condamné à lui verser, à titre de provision, des sommes d'un montant total de 100 000 euros à raison du préjudice moral qu'elle estime avoir subi en raison tant de la faute commise par la direction du renseignement militaire que, sur le terrain de la responsabilité sans faute, de la mise en oeuvre de la loi du <br/>
24 juillet 2015 sur le renseignement et de l'intervention de la décision du Conseil d'Etat précitée.<br/>
<br/>
              5. La circonstance que le mémoire produit par la ministre des armées été signé par un membre du Conseil d'Etat en détachement n'est pas, par elle-même, de nature à méconnaître l'égalité des armes entre les parties ni à mettre en cause l'impartialité des membres de la formation de jugement du Conseil d'Etat. Par suite, les conclusions de Mme B...tendant à ce que ce mémoire en défense soit écarté des débats ne peuvent qu'être rejetées.<br/>
<br/>
              6. La détention illégale d'informations nominatives concernant Mme B...dans les traitements de données de la direction du renseignement militaire constitue une faute de nature à engager la responsabilité de l'État. A raison du préjudice moral causé par cette faute, Mme B...détient une obligation à l'égard de l'Etat dont le montant non sérieusement contestable peut être évalué à 1 000 euros.<br/>
<br/>
              7. La loi du 24 juillet 2015 a prévu que la formation spécialisée du Conseil d'Etat pouvait avoir accès aux traitements de données nominatives intéressant la sûreté de l'État afin d'assurer le contrôle de leur conformité aux exigences de la loi du 6 janvier 1978. A la demande de MmeB..., il a été procédé à ce contrôle sur les traitements de données de la direction du renseignement militaire. L'obligation dont Mme B...se prévaut à l'encontre de l'État sur le fondement de l'égalité devant les charges publiques à raison de l'intervention de cette loi et de cette décision du Conseil d'Etat apparaît sérieusement contestable. Sa demande de provision ne peut donc, s'agissant de ces chefs de préjudice, qu'être rejetée.<br/>
<br/>
              8. Il résulte de ce qui précède qu'il y a lieu de fixer à 1 000 euros la provision au versement de laquelle l'État doit être condamné.<br/>
<br/>
              9. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'État la somme de 1 000 euros à verser à Mme B...au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'État versera une provision de 1 000 euros à MmeB....<br/>
Article 2 : L'Etat versera une somme de 1 000 euros à Mme B...au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 3. Le surplus des conclusions de la requête de Mme B...est rejeté. <br/>
Article 4 : La présente décision sera notifiée à Mme A...B...et à la ministre des armées.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
