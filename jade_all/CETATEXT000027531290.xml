<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027531290</ID>
<ANCIEN_ID>JG_L_2013_06_000000336596</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/53/12/CETATEXT000027531290.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 10/06/2013, 336596, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-06-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>336596</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER</AVOCATS>
<RAPPORTEUR>M. Nicolas Labrune</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Delphine Hedary</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:336596.20130610</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi du ministre du budget, des comptes publics, de la fonction publique et de la réforme de l'Etat, enregistré le 12 février 2010 au secrétariat du contentieux du Conseil d'Etat ; le ministre demande au Conseil d'Etat d'annuler l'arrêt n° 07LY01065 du 17 décembre 2009 de la cour administrative d'appel de Lyon en tant qu'il fait partiellement droit à l'appel de M. A...B...en réformant le jugement du 15 février 2007 du tribunal administratif de Dijon et en déchargeant l'intéressé des droits correspondants à la réduction de 45 666 euros de sa base d'imposition à l'impôt sur le revenu dans la catégorie des bénéfices industriels et commerciaux au titre de l'année 2002 ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Nicolas Labrune, Auditeur,  <br/>
<br/>
              - les conclusions de Mme Delphine Hedary, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Blancpain, Soltner, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M.B..., concessionnaire automobile à Beaune, a vu son contrat de concession avec la SA Rover France dénoncé unilatéralement en 1995 ; qu'il a obtenu du juge judiciaire la condamnation de cette société a lui verser une indemnité de 137 000 euros ; que l'administration a remis en cause la qualification de plus-value à long terme mentionnée par le contribuable dans sa déclaration de revenus pour l'année 2002 et réintégré l'indemnité litigieuse dans la catégorie des bénéfices industriels et commerciaux ; que la demande en décharge de la cotisation supplémentaire d'impôt sur le revenu en résultant a été rejetée par un jugement du 15 février 2007 du tribunal administratif de Dijon ; que, par un arrêt du 17 décembre 2009, la cour administrative d'appel de Lyon a déchargé le contribuable des droits correspondants au tiers de l'indemnité litigieuse, soit 45 666 euros ; que le ministre chargé du budget demande l'annulation de cet arrêt dans cette mesure ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 38 du code général des impôts, dans sa rédaction applicable à l'année d'imposition en litige : " 1. (...) le bénéfice imposable est le bénéfice net, déterminé d'après les résultats d'ensemble des opérations de toute nature effectuées par les entreprises, y compris notamment les cessions d'éléments quelconques de l'actif, soit en cours, soit en fin d'exploitation. (...) " ; qu'en vertu de l'article 39 duodecies du même code : " 1. Par dérogation aux dispositions de l'article 38, les plus-values provenant de la cession d'éléments de l'actif immobilisé sont soumises à des régimes distincts suivant qu'elles sont réalisées à court ou à long terme. (...) " ; qu'il résulte de ces dispositions que les indemnités versées à un commerçant en vertu d'une obligation de réparation incombant à la partie versante constituent soit des recettes concourant à la formation de son bénéfice imposable si elles ont pour objet de compenser un préjudice résultant d'une perte de recettes commerciales, soit des plus-values si elles ont pour objet de compenser les pertes résultant d'une cession d'un élément de l'actif immobilisé à un tiers ou d'un événement ayant pour effet de retirer définitivement toute valeur à cet élément ;<br/>
<br/>
              3. Considérant qu'en jugeant que la part de l'indemnité accordée par le juge judiciaire à M. B...pour compenser la perte de chance de vendre son fonds de commerce à un prix avantageux en raison de la résiliation abusive du contrat de concession automobile qui le liait à la SA Rover France constituait une plus-value de même nature que celle qui résulte de la cession d'un élément d'actif immobilisé, alors qu'elle a estimé que ce contrat de concession ne constituait pas un élément incorporel de l'actif immobilisé, la cour administrative d'appel de Lyon a commis une erreur de droit ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, le ministre chargé du budget est fondé à demander l'annulation des articles 1 à 4 de l'arrêt attaqué ;<br/>
<br/>
              4. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>				D E C I D E  :<br/>
              				--------------<br/>
<br/>
Article 1er : Les articles 1 à 4 de l'arrêt du 17 décembre 2009 de la cour administrative d'appel de Lyon sont annulés.<br/>
<br/>
Article 2 : L'affaire est renvoyée, dans la limite de la cassation ainsi prononcée, à la cour administrative d'appel de Lyon.<br/>
<br/>
Article 3 : Les conclusions présentées par M. B...au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée au ministre de l'économie et des finances et à M. A... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
