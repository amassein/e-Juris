<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042499854</ID>
<ANCIEN_ID>JG_L_2020_11_000000432656</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/49/98/CETATEXT000042499854.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 04/11/2020, 432656, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-11-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>432656</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Réda Wadjinny-Green</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:432656.20201104</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire en réplique et deux nouveaux mémoires, enregistrés le 15 juillet 2019, le 2 août 2020, 7 octobre 2020 et le 8 octobre 2020 au secrétariat du contentieux du Conseil d'Etat, l'association " La Quadrature du net " demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2019-452 du 13 mai 2019 autorisant la création d'un moyen d'identification électronique dénommé " Authentification en ligne certifiée sur mobile " ;<br/>
<br/>
              2°) à titre subsidiaire, de poser les questions préjudicielles suivantes à la Cour de justice de l'Union européenne et de surseoir à statuer dans l'attente de la réponse de la Cour à ces questions :<br/>
              " 1 L'appréciation de la validité du consentement, au sens du point 4 de l'article 7 du règlement (UE) 2016/679 du Parlement européen et du Conseil relatif à la protection de personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données, doit-elle se faire au niveau de chaque service faisant l'objet d'un traitement de données personnelles, indépendamment de l'existence d'un autre service " équivalent " ou au niveau de l'ensemble des " services équivalents " '<br/>
              2 Les données biométriques collectées et traitées, par une application mobile recourant à une technologie de reconnaissance faciale à des fins d'authentification auprès de certains services publics et de leurs partenaires sont-elles adéquates, pertinentes et non excessives au regard des finalités pour lesquelles elles sont collectées et traitées, au sens de l'article 5 du règlement (UE) 2016/679 du Parlement européen et du Conseil relatif à la protection de personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données ' " ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 4 096 euros au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
<br/>
             Vu les autres pièces du dossier ; <br/>
<br/>
             Vu :<br/>
             - le règlement (UE) n°910/2014 du Parlement européen et du Conseil du 23 juillet 2014 ; <br/>
             - le règlement (UE) n°2016/679 du Parlement européen et du Conseil du 27 avril 2016 ; <br/>
             - le règlement d'exécution (UE) 2015/1502 de la Commission du 8 septembre 2015 ;<br/>
             - la loi n° 78-17 du 6 janvier 1978 ;<br/>
             - le décret n° 2019-452 du 13 mai 2019 ; <br/>
             - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Réda Wadjinny-Green, auditeur,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Le décret du 13 mai 2019 dont l'association " La Quadrature du net " demande l'annulation pour excès de pouvoir autorise la mise en oeuvre par le ministre de l'intérieur d'un traitement automatisé de données à caractère personnel dénommé " authentification en ligne certifiée sur mobile " (Alicem). En vertu de l'article 1er de ce décret, ce traitement a pour finalité de proposer aux ressortissants français titulaires d'un passeport biométrique et aux ressortissants étrangers titulaires d'un titre de séjour biométrique " la délivrance d'un moyen d'identification électronique leur permettant de s'identifier électroniquement et de s'authentifier auprès d'organismes publics ou privés, au moyen d'un équipement terminal de communications électroniques doté d'un dispositif permettant la lecture sans contact du composant électronique de ces titres, en respectant les dispositions prévues par le règlement (UE) n° 910/2014 du Parlement européen et du Conseil du 23 juillet 2014 susvisé, notamment les exigences relatives au niveau de garantie requis par le téléservice concerné. / L'enrôlement dans ce traitement par le titulaire de l'un des titres mentionnés à l'article 2 du présent décret donne lieu à l'ouverture d'un compte ". L'article 4 de ce décret dispose que " Le traitement mentionné à l'article 1er utilise un système de reconnaissance faciale statique et de reconnaissance faciale dynamique ". L'article 10 du décret prévoit que les données collectées par le système de reconnaissance faciale le sont à cette seule fin et sont " effacées sitôt ces reconnaissances terminées ". Aux termes de l'article 13 de ce décret : " L'Agence nationale des titres sécurisés procède, au moment de la demande d'ouverture du compte mentionné à l'article 1er, à l'information de l'usager concernant l'utilisation d'un dispositif de reconnaissance faciale statique et de reconnaissance faciale dynamique et au recueil de son consentement au traitement de ses données biométriques ". <br/>
<br/>
              2. Il ressort des pièces du dossier et des dispositions citées au point précédent que l'ouverture d'un compte " Alicem " permet aux titulaires d'un passeport ou d'une carte de séjour biométrique de s'identifier en ligne auprès d'organismes publics ou privés partenaires et d'accéder à leurs téléservices. Ce service, qui prend la forme d'une application disponible sur le système d'exploitation Android, vise à offrir aux usagers un niveau de garantie élevé au sens du règlement du 23 juillet 2014 et de proposer ainsi une protection renforcée contre l'utilisation abusive ou l'usurpation de leur identité dans le cadre de leurs démarches en ligne. Aux termes du 2.1.2. du règlement d'exécution de la Commission du 8 septembre 2015, ce niveau de garantie peut être atteint s'il est vérifié que la personne physique : " est en possession d'un élément d'identification biométrique ou photographique reconnu par l'État membre dans lequel est déposée la demande relative au moyen d'identité électronique et que cet élément correspond à l'identité alléguée, l'élément fait l'objet d'une vérification visant à déterminer sa validité selon une source faisant autorité / et / le demandeur est identifié comme ayant l'identité alléguée par comparaison d'une ou de plusieurs caractéristiques physiques de la personne auprès d'une source faisant autorité ". Pour créer un compte " Alicem ", l'usager doit, entre autres démarches, consentir à un traitement de données biométriques collectées à travers un système de reconnaissance faciale. S'il y consent, il est invité à enregistrer une courte vidéo à partir de laquelle un algorithme de reconnaissance faciale vérifie qu'il est le titulaire légitime du titre biométrique sur lequel l'identité numérique est fondée, tandis qu'un algorithme de reconnaissance du vivant analyse les actions effectuées sur la vidéo pour détecter toute tentative d'attaque informatique ou de tromperie. Une fois son identité authentifiée, l'usager peut finaliser son inscription. Des identifiants électroniques sont alors associés à son compte. Ils lui permettent de se connecter sur l'application et d'effectuer des démarches sur les téléservices partenaires. Les données biométriques collectées à l'occasion de la création du compte sont quant à elles détruites. Si un usager ne consent pas au traitement par reconnaissance faciale, il ne peut pas créer de compte Alicem ni, par suite, accéder à l'application. <br/>
<br/>
              Sur la légalité externe du décret attaqué :<br/>
<br/>
              3. En premier lieu, l'article 27 de la loi du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés dispose, dans sa rédaction applicable au litige, que : " Sont autorisés par décret en Conseil d'Etat, pris après avis motivé et publié de la Commission nationale de l'informatique et des libertés, les traitements de données à caractère personnel mis en oeuvre pour le compte de l'Etat, agissant dans l'exercice de ses prérogatives de puissance publique, qui portent sur des données génétiques ou sur des données biométriques nécessaires à l'authentification ou au contrôle de l'identité des personnes ". Lorsque, comme en l'espèce, un décret doit être pris en Conseil d'Etat, le texte retenu par le gouvernement ne peut être différent à la fois du projet qu'il avait soumis au Conseil d'Etat et du texte adopté par ce dernier. <br/>
<br/>
              4. Il ressort de la copie de la minute de l'avis émis par la section de l'intérieur du Conseil d'Etat sur le projet de décret, versée au dossier par le ministre de l'intérieur, que le texte retenu par le gouvernement est identique au texte adopté par le Conseil d'Etat, sous la réserve de la simple rectification d'une erreur de plume dans l'intitulé du décret aux articles 15 et 16 de ce texte. Dans ces conditions, le moyen tiré de ce que le décret attaqué n'aurait pas été pris en Conseil d'Etat doit être écarté. <br/>
<br/>
              5. En second lieu, aux termes de l'article 22 de la Constitution : " Les actes du Premier ministre sont contresignés, le cas échéant, par les ministres chargés de leur exécution ". Les ministres chargés de l'exécution d'un acte réglementaire sont ceux qui ont compétence pour signer ou contresigner les mesures réglementaires ou individuelles que comporte nécessairement l'exécution de cet acte. Le décret attaqué n'appelant pas de mesure d'exécution de la part du garde des sceaux, ministre de la justice, de la ministre de la cohésion des territoires et des relations avec les collectivités territoriales ou du ministre des outre-mer, le moyen tiré du défaut de contreseing de ces ministres doit être écarté.<br/>
<br/>
              Sur la légalité interne du décret attaqué :<br/>
<br/>
              6. Dans sa rédaction applicable au litige, l'article 8 de la loi du 6 janvier 1978 dispose que : " I.- Il est interdit de traiter (...) des données biométriques aux fins d'identifier une personne physique de manière unique (...) II.- Dans la mesure où la finalité du traitement l'exige pour certaines catégories de données, ne sont pas soumis à l'interdiction prévue au I : / 1° Les traitements pour lesquels la personne concernée a donné son consentement exprès (...) IV. - De même, ne sont pas soumis à l'interdiction prévue au I les traitements, automatisés ou non, justifiés par l'intérêt public et autorisés dans les conditions prévues au II de l'article 26. ". Aux termes du 2 de l'article 9 du règlement du 27 avril 2016, dit règlement général sur la protection des données, cette interdiction " ne s'applique pas si l'une des conditions suivantes est remplie : / a) la personne concernée a donné son consentement explicite au traitement de ces données à caractère personnel pour une ou plusieurs finalités spécifiques (...) g) le traitement est nécessaire pour des motifs d'intérêt public important, sur la base du droit de l'Union ou du droit d'un État membre qui doit être proportionné à l'objectif poursuivi, respecter l'essence du droit à la protection des données et prévoir des mesures appropriées et spécifiques pour la sauvegarde des droits fondamentaux et des intérêts de la personne concernée ". <br/>
<br/>
              7. Par ailleurs, l'article 7 de la loi du 6 janvier 1978 dispose, dans sa rédaction applicable au litige, que : " Un traitement de données à caractère personnel doit avoir reçu le consentement de la personne concernée, dans les conditions mentionnées au 11) de l'article 4 et à l'article 7 du règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 précité ". L'article 4 (11) du règlement général sur la protection des données définit le consentement comme " toute manifestation de volonté, libre, spécifique, éclairée et univoque par laquelle la personne concernée accepte, par une déclaration ou par un acte positif clair, que des données à caractère personnel la concernant fassent l'objet d'un traitement ". En outre, l'article 7 du même règlement précise que : " 4. Au moment de déterminer si le consentement est donné librement, il y a lieu de tenir le plus grand compte de la question de savoir, entre autres, si l'exécution d'un contrat, y compris la fourniture d'un service, est subordonnée au consentement au traitement de données à caractère personnel qui n'est pas nécessaire à l'exécution dudit contrat ". Il est indiqué au considérant 42 du même règlement que " Le consentement ne devrait pas être considéré comme ayant été donné librement si la personne concernée ne dispose pas d'une véritable liberté de choix ou n'est pas en mesure de refuser ou de retirer son consentement sans subir de préjudice " tandis que son considérant 43 précise que : " Le consentement est présumé ne pas avoir été donné librement si un consentement distinct ne peut pas être donné à différentes opérations de traitement des données à caractère personnel bien que cela soit approprié dans le cas d'espèce, ou si l'exécution d'un contrat, y compris la prestation d'un service, est subordonnée au consentement malgré que celui-ci ne soit pas nécessaire à une telle exécution ". <br/>
<br/>
              8. D'une part, il ne ressort pas des pièces du dossier que, pour la création d'identifiants électroniques, il existait à la date du décret attaqué d'autres moyens d'authentifier l'identité de l'usager de manière entièrement dématérialisée en présentant le même niveau de garantie que le système de reconnaissance faciale. Il s'ensuit que le recours au traitement de données biométriques autorisé par le décret attaqué doit être regardé comme exigé par la finalité de ce traitement. <br/>
<br/>
              9. D'autre part, il ressort des pièces du dossier que les téléservices accessibles via l'application " Alicem " l'étaient également, à la date du décret attaqué, à travers le dispositif FranceConnect, dont l'utilisation ne présuppose pas le consentement à un traitement de reconnaissance faciale. Dès lors que les usagers qui ne consentiraient pas au traitement prévu dans le cadre de la création d'un compte Alicem peuvent accéder en ligne, grâce à un identifiant unique, à l'ensemble des téléservices proposés, ils ne sauraient être regardés comme subissant un préjudice au sens du règlement général sur la protection des données précité. Il s'ensuit que l'association requérante n'est pas fondée à soutenir que le consentement des utilisateurs de l'application Alicem ne serait pas librement recueilli ni, par suite, que le décret attaqué méconnaîtrait pour ce motif les dispositions du règlement général sur la protection des données et de la loi du 6 janvier 1978. <br/>
<br/>
              10. Par ailleurs, l'article 6 de la loi du 6 janvier 1978 dispose, dans sa rédaction applicable à la date du litige, que les données à caractère personnel doivent être " 3° (...) adéquates, pertinentes et non excessives au regard des finalités pour lesquelles elles sont collectées et de leurs traitements ultérieurs ". Pour l'application de ces dispositions, les données pertinentes au regard de la finalité d'un traitement sont celles qui sont en adéquation avec la finalité du traitement et qui sont proportionnées à cette finalité. En vertu de l'article 7 du décret attaqué est prévue la collecte de données relatives, en premier lieu, à l'identification de l'usager, en deuxième lieu à l'identification de son titre biométrique, en troisième lieu à l'équipement terminal de communications électroniques qu'il utilise et, enfin, à l'historique des transactions associées à son compte, ces dernières données ne pouvant être communiquées aux fournisseurs de téléservices en vertu de l'article 9. Eu égard à leur objet et aux finalités du traitement rappelées au point 1, le recueil de ces données doit être regardé comme adéquat et proportionné à cette finalité. <br/>
<br/>
              11. Il résulte de tout ce qui précède, sans qu'il soit besoin de poser des questions préjudicielles à la Cour de justice de l'Union européenne, que la requête de La Quadrature du net doit être rejetée, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
     --------------<br/>
<br/>
Article 1er : La requête de La Quadrature du net est rejetée.<br/>
Article 2 : La présente décision sera notifiée à La Quadrature du net et au ministre de l'intérieur. <br/>
Copie en sera adressée à la Commission nationale de l'informatique et des libertés.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
