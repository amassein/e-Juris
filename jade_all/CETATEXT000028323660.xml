<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028323660</ID>
<ANCIEN_ID>JG_L_2013_12_000000342504</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/32/36/CETATEXT000028323660.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 13/12/2013, 342504, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-12-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>342504</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>M. Olivier Japiot</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Claire Legras</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:342504.20131213</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 17 août et 17 novembre 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B... A..., demeurant... ; M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 06PA00904 du 17 juin 2010 par lequel la cour administrative d'appel de Paris a rejeté sa requête tendant à l'annulation du jugement n° 0310642/7 du 13 janvier 2006 par lequel le tribunal administratif de Paris a rejeté sa demande tendant à la condamnation de l'Etat à lui verser une somme de 163 000 000 euros, augmentée des intérêts au taux légal à compter du 4 avril 2003 avec capitalisation de ces intérêts, en réparation du préjudice résultant des décisions prises par la commission de contrôle des assurances et annulées par le Conseil d'Etat ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des assurances ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Japiot, Conseiller d'Etat, <br/>
<br/>
              - les conclusions de Mme Claire Legras, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, avocat de M. A...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'à la suite d'un contrôle des sociétés ICD SA et ICD Vie, dont M. A... était le fondateur et le dirigeant, la commission de contrôle des assurances a, par une décision du 19 avril 2000, infligé à M. A...un blâme et une sanction pécuniaire de 100 000 francs sur le fondement de l'article L. 310-18 du code des assurances ; que, par une décision du 21 août 2000, elle a mis en oeuvre la procédure de recours au fonds de garantie prévue par l'article L. 423-2 du code des assurances et lancé un appel d'offres en vue du transfert du portefeuille de contrats de la société ICD Vie ; que, par une décision du 6 octobre 2000, elle a engagé la procédure de transfert du portefeuille de contrats de la société ICD SA ; que, par deux décisions du 7 novembre 2000, elle a procédé au retrait des agréments de la société ICD SA et décidé le transfert d'un contrat collectif d'assurances de cette même société ; que, par une décision du 19 décembre 2000, elle a procédé au retrait des agréments de la société ICD Vie ; que, toutefois, par deux décisions des 28 octobre 2002 et 10 mars 2003, le Conseil d'Etat, statuant au contentieux, a annulé les décisions mentionnées ci-dessus de la commission de contrôle des assurances, au motif d'un vice de procédure ; que M. A...a demandé, le 4 avril 2003, au ministre de l'économie et des finances de lui verser une indemnité de 163 millions d'euros, augmentée des intérêts au taux légal à compter de cette même date avec capitalisation de ces intérêts, en  raison du préjudice qu'il estime avoir subi du fait de ces décisions illégales ; que le tribunal administratif de Paris a rejeté sa demande tendant à la condamnation de l'Etat par un jugement du 13 janvier 2006 ; que M. A...se pourvoit en cassation contre l'arrêt du 17 juin 2010 par lequel la cour administrative d'appel de Paris a rejeté son appel dirigé contre ce jugement ;<br/>
<br/>
              Sur la fin de non-recevoir opposée par le ministre de l'économie et des finances :<br/>
<br/>
              2. Considérant que si le ministre soutient que M.A..., en qualité d'actionnaire et de dirigeant des sociétés mentionnées ci-dessus, ne justifie pas d'un intérêt à agir suffisant pour demander la condamnation de l'Etat, cette circonstance est, en tout état de cause, dépourvue d'incidence sur la recevabilité du pourvoi de l'intéressé, qui était partie à l'instance devant la cour administrative d'appel ;<br/>
<br/>
              Sur l'arrêt attaqué :<br/>
<br/>
              3. Considérant qu'il ressort des pièces de la procédure devant la cour administrative d'appel de Paris que M. A...  soutenait devant celle-ci que les opérations d'expertise qu'elle avait ordonnées, par un arrêt avant-dire droit du 5 juin 2008, avaient été conduites en méconnaissance du principe du contradictoire ; qu'il incombait à la cour de répondre à ce moyen, soit en l'écartant , soit - si elle estimait l'expertise irrégulière - en ordonnant une nouvelle expertise ou en indiquant qu'elle disposait désormais, en dépit de cette irrégularité, d'éléments d'information nécessaires à la solution du litige, dont il lui appartenait d'apprécier la pertinence au vu des observations des parties ; qu'en omettant de répondre à ce moyen, qui n'était pas inopérant, la cour a entaché son arrêt d'insuffisance de motivation ; qu'elle a, en outre, insuffisamment motivé son arrêt sur la méthode utilisée par 1'expert pour actualiser les flux de trésorerie et pour apprécier la valeur de la société ICD Vie, en ne répondant pas aux nombreuses critiques formulées par le requérant sur ce point ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que M. A... est fondé à demander l'annulation de l'arrêt attaqué ; <br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à M. A..., au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 17 juin 2010 de la cour administrative d'appel de Paris est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br/>
Article 3 : L'Etat versera à M. A... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. B... A...et au ministre de l'économie et des finances.<br/>
Copie en sera transmise pour information à l'Autorité de contrôle prudentiel.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
