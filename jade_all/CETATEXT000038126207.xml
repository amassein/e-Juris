<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038126207</ID>
<ANCIEN_ID>JG_L_2019_02_000000419662</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/12/62/CETATEXT000038126207.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 13/02/2019, 419662, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-02-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>419662</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Bertrand Mathieu</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:419662.20190213</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire complémentaire et deux mémoires en réplique enregistrés les 9 avril, 23 avril, 22 août et 29 août 2018 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le décret du 30 janvier 2018 portant refus d'acquisition de la nationalité française ; <br/>
<br/>
              2°) de lui accorder la nationalité française ou à défaut, d'enjoindre au Premier ministre de réexaminer sa situation ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement d'une somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code civil ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bertrand Mathieu, conseiller d'Etat en service extraordinaire,<br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1.	Aux termes de l'article 21-2 du code civil : " L'étranger (...) qui contracte mariage avec un conjoint de nationalité française peut (...) acquérir la nationalité française par déclaration. (...) ". L'article 21-4 du même code dispose que : " Le Gouvernement peut s'opposer par décret en Conseil d'Etat, pour indignité ou défaut d'assimilation, autre que linguistique, à l'acquisition de la nationalité française par le conjoint étranger dans un délai de deux ans à compter de la date du récépissé prévu au deuxième alinéa de l'article 26 (...) " . <br/>
<br/>
              2.	Il ressort des pièces du dossier que M. A...B..., de nationalité tunisienne, a épousé le 25 avril 2009 une ressortissante française, à Reims. Le 8 décembre 2015, il a souscrit une déclaration en vue d'acquérir la nationalité française à raison de ce mariage. Toutefois, par un décret du 30 janvier 2018, le Premier ministre s'est opposé à l'acquisition de la nationalité française au motif que M. B...participait activement et adhérait aux idées d'un courant qui promeut des principes contraires aux valeurs essentielles de la société française, en particulier à l'égalité entre les sexes et ne pouvait être considéré comme assimilé à la société française. M. B...demande l'annulation pour excès de pouvoir de ce décret. <br/>
<br/>
              3.	Conformément au principe du caractère contradictoire de l'instruction, le juge administratif ne peut statuer qu'au vu des seules pièces du dossier qui ont été communiquées entre les parties au litige. Pour se prononcer sur une requête, assortie d'allégations sérieuses, dirigée contre un décret s'étant opposé à l'acquisition de la nationalité française sur le fondement de l'article 21-4 du code civil, le Conseil d'Etat doit être en mesure d'apprécier, à partir d'éléments précis, le bien-fondé du motif qui fonde le décret attaqué. Il appartient en conséquence à l'administration de verser au dossier, dans le respect des secrets garantis par la loi et des exigences liées à la sécurité nationale, les éléments nécessaires pour permettre au juge de l'excès de pouvoir de statuer en pleine connaissance de cause.<br/>
<br/>
              4.	En l'espèce, alors que la requête de M. B...conteste de façon sérieuse le motif tiré du défaut d'assimilation qui fonde le décret attaqué, le ministre de l'intérieur se borne en défense à faire état, pour établir le motif du décret, de pièces réunies par le sous-préfet de Reims et de deux notes du service départemental du renseignement territorial de Reims, documents qu'il se refuse à communiquer dans le cadre de l'instruction contradictoire conduite par le Conseil d'Etat, au motif que cette communication serait susceptible de porter atteinte à la sûreté de l'Etat, à la sécurité publique ou à la sécurité des personnes. Si le ministre soutient qu'il a reproduit dans un de ses mémoires les informations utiles figurant dans les documents qu'il se refuse à communiquer, il ne ressort pas des éléments généraux, peu circonstanciés et non corroborés qui sont retracés dans ces écritures que M. B...participerait activement et adhérerait, comme l'indique le décret attaqué, aux idées d'un courant qui promeut des principes contraires aux valeurs essentielles de la société française. Dans ces conditions, le motif sur lequel repose le décret attaqué ne peut, au vu des pièces versées au dossier dans le cadre de l'instruction contradictoire, être tenu pour établi.<br/>
<br/>
              5.	Il résulte de ce qui précède que M. B...est fondé à demander l'annulation pour excès de pouvoir du décret qu'il attaque. <br/>
<br/>
              6.	La présente décision, qui annule le décret qui avait fait opposition à l'acquisition de la nationalité française par M. B...au vu de la déclaration qu'il avait souscrite à raison de son mariage, n'implique par elle-même aucune mesure d'exécution. Les conclusions à fin d'injonction de la requête ne peuvent, par suite, qu'être rejetées.<br/>
<br/>
              7.	Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement d'une somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le décret du 8 février 2018 s'opposant à l'acquisition par M. B...de la nationalité française est annulé. <br/>
<br/>
Article 2 : L'Etat versera à M. B...la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Le surplus des conclusions de M.B...  est rejeté. <br/>
<br/>
Article 4 : La présente décision sera notifiée à M. A...B...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
