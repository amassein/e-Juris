<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036976460</ID>
<ANCIEN_ID>JG_L_2018_06_000000406106</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/97/64/CETATEXT000036976460.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème chambres réunies, 01/06/2018, 406106, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-06-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>406106</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>M. Yves Doutriaux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:406106.20180601</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. et Mme B...et Françoise A...ont demandé au tribunal administratif de Marseille d'annuler pour excès de pouvoir l'arrêté du 16 décembre 2010 par lequel le maire de Marseille a accordé à la SA Unimo un permis de construire pour la réalisation d'un immeuble d'habitation sur un terrain situé 70 boulevard Hilarion Boeuf sur le territoire de la commune. Par un jugement n° 1104096 du 22 novembre 2012, le tribunal administratif a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 13MA00370 du 21 juillet 2014, la cour administrative d'appel de Marseille a, sur appel de M. et MmeA..., annulé ce jugement ainsi que l'arrêté du maire du 16 décembre 2010.<br/>
<br/>
              Par une décision n° 384786-384790 du 16 mars 2016, le Conseil d'Etat, statuant au contentieux a, sur les pourvois formés, d'une part, par la ville de Marseille et, d'autre part, par la société Crédit agricole immobilier entreprise venant aux droits de la société Unimo, annulé cet arrêt et renvoyé le jugement de l'affaire à la cour administrative d'appel de Marseille.<br/>
<br/>
              Par un nouvel arrêt n° 16MA01219 du 18 octobre 2016, la cour administrative d'appel de Marseille a annulé le jugement du tribunal administratif de Marseille du 22 novembre 2012 et l'arrêté du maire de Marseille du 16 décembre 2010.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 19 décembre 2016, 20 mars 2017 et 27 février 2018 au secrétariat du contentieux du Conseil d'Etat, la société Crédit agricole immobilier entreprise demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce dernier arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M. et Mme A...;<br/>
<br/>
              3°) de mettre à la charge de M. et Mme A...la somme de 5 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yves Doutriaux, conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Soltner, Texidor, Périer, avocat de la société Crédit agricole immobilier entreprise, et à la SCP Waquet, Farge, Hazan, avocat de M. A...; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant que le maire de Marseille a, par un arrêté du 16 décembre 2010, accordé à la société Unimo, aux droits de laquelle vient la société Crédit agricole immobilier entreprise, un permis de construire en vue de l'édification d'un immeuble d'habitation de six étages, comportant 65 logements, sur un terrain situé 70 boulevard Hilarion Boeuf sur le territoire de la commune ; que, par un jugement du 22 novembre 2012, le tribunal administratif de Marseille a rejeté la demande présentée par M. et Mme A...tendant à l'annulation pour excès de pouvoir de ce permis de construire ; que si, par un arrêt du 21 juillet 2014, la cour administrative d'appel de Marseille a annulé ce jugement ainsi que le permis de construire, le Conseil d'Etat, statuant au contentieux a, par une décision en date du 16 mars 2016, prononcé l'annulation de cet arrêt et renvoyé l'affaire devant la cour administrative d'appel de Marseille ; que cette dernière, par un nouvel arrêt du 18 octobre 2016, a de nouveau prononcé l'annulation du jugement de première instance et de l'arrêté du 16 décembre 2010 accordant le permis de construire ;<br/>
<br/>
              2.	Considérant que l'article R. UA 11 du règlement du plan d'occupation des sols de Marseille, relatif à l'aspect extérieur des constructions, tel qu'applicable à la date de délivrance du permis de construire contesté, expose à titre de dispositions générales que les " constructions à édifier s'inscrivent dans la perspective du renouvellement du tissu urbain et/ou de celle de sa valorisation ou dans la perspective de sa valorisation " ; que, s'agissant des constructions nouvelles, l'article prévoit, en son paragraphe 2.3.1. relatif à l'échelle et l'ordonnancement, que " Les constructions à édifier tiennent compte de l'échelle du bâti environnant et de l'ordonnancement des rythmes et dimensions des percements pour permettre, dans la mesure du possible, un rapport cohérent des étages entre les immeubles " ; <br/>
<br/>
              3.	Considérant qu'eu égard à la teneur des dispositions de cet article R. UA 11, il appartient au juge de l'excès de pouvoir, saisi d'un moyen en ce sens, d'apprécier si l'autorité administrative a pu légalement autoriser la construction projetée, compte tenu de ses caractéristiques et de celles du bâti environnant, sans méconnaître les exigences résultant de cet article ; que, dans l'exercice de ce contrôle, le juge doit tenir compte de l'ensemble des dispositions de l'article et de la marge d'appréciation qu'elles laissent à l'autorité administrative pour accorder ou refuser de délivrer une autorisation d'urbanisme ; qu'à cet égard, si les dispositions relatives aux constructions nouvelles indiquent que ces constructions " tiennent compte " de l'échelle du bâti environnant et de l'ordonnancement des rythmes et percements afin de permettre un rapport cohérent des étages entre les immeubles, elles n'assignent cette finalité que " dans la mesure du possible " et s'inscrivent dans une perspective générale de renouvellement du tissu urbain ; que ces dispositions ne font, par suite, pas obstacle à ce qu'un projet de construction présente, dans le respect des autres prescriptions fixées par le règlement du plan d'occupation des sols, et notamment celles relatives à la hauteur des constructions, une différence d'échelle avec les constructions immédiatement avoisinantes ; <br/>
<br/>
              4.	Considérant que, pour annuler le permis de construire litigieux, la cour administrative d'appel a retenu que les dispositions de l'article R. UA 11 imposaient de limiter la prise en compte de l'échelle du bâti environnant, en comparant l'emprise au sol et la hauteur de la construction à édifier à celles des bâtiments situés dans son voisinage, au seul bâti de l'environnement  immédiat de la construction; qu'en se fondant sur une telle interprétation du règlement du plan d'occupation des sols, la cour administrative d'appel a commis une erreur de droit ; que la société Crédit agricole immobilier entreprise est, dès lors, fondée, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, à demander pour ce motif l'annulation de l'arrêt qu'elle attaque ; <br/>
<br/>
              5.	Considérant qu'aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire " ; qu'il incombe en conséquence au Conseil d'Etat de régler l'affaire au fond ; <br/>
<br/>
              6.	Considérant, en premier lieu, que les dispositions de l'article R. UA 11 du règlement du plan d'occupation des sols de Marseille, ainsi qu'il a été dit au point 3, ne font pas obstacle à ce qu'un projet de construction présente, dans le respect des autres prescriptions du règlement du plan d'occupation des sols, et notamment celles relatives à la hauteur, une différence d'échelle avec les constructions immédiatement avoisinantes et n'imposent pas que soit refusée une autorisation de construire pour un bâtiment qui, par sa hauteur, ne correspondrait pas à l'échelle de ces constructions voisines ; qu'il ressort des pièces du dossier que, si les constructions immédiatement voisines de la construction projetée sont des maisons ne comportant qu'un voire deux étages, l'environnement proche de la construction comporte aussi des immeubles collectifs d'habitation dont certains ont une hauteur importante ; qu'eu égard à la marge d'appréciation laissée à l'autorité administrative par les dispositions de l'article R. UA 11, qui retiennent pour perspective le renouvellement urbain, le maire de Marseille a pu légalement accorder le permis de construire litigieux sans méconnaître ces dispositions du règlement du plan d'occupation des sols ; <br/>
<br/>
              7.	Considérant, en deuxième lieu, que le règlement du plan d'occupation des sols de Marseille définit la zone UA comme correspondant aux " tissus centraux ", soit un " tissu constitué, continu et aligné le long de rues, à l'intérieur duquel des constructions en retrait ou en interruption de façade pourront être admises le long de certaines voies dans un souci d'aération et de modernisation du tissu " ; qu'aux termes de l'article R. UA 7 de ce règlement, relatif à l'implantation des constructions par rapport aux limites séparatives : " Les constructions à édifier sont implantées : / 1. ... sur une profondeur, mesurée à compter de la limite de l'alignement (...) et égale à la plus grande profondeur de la parcelle, diminuée de 4 mètres, sans être supérieure à 17 mètres, / 1.1. par rapport aux limites latérales, (...) / 1.1.2. ... sans nécessaire continuité en UA (...) à condition que la distance mesurée horizontalement de tout point desdites constructions au point le plus proche de la limite concernée soit au moins égale à la moitié de la hauteur de la construction à édifier diminuée de 3 mètres, sans être inférieure à 4 mètres (...) / 1.2. par rapport aux limites arrière, sans prescription particulière (...) " ; <br/>
<br/>
              8.	Considérant que ces dispositions permettent, en zone UA, une implantation des constructions sur une profondeur qui peut atteindre, si la parcelle a au moins 21 mètres de profondeur, 17 mètres à compter de l'alignement avec la voie, soit en continuité par rapport aux limites séparatives latérales, soit dans le respect d'une règle de prospect ; que, dans le cas d'un terrain situé à l'angle de deux voies, en l'absence de règle particulière dans le règlement du plan d'occupation des sols, peuvent être délimitées à partir de l'alignement de ces voies deux bandes d'une profondeur maximale de 17 mètres, se recoupant pour partie, à l'intérieur desquelles la construction doit être édifiée ; <br/>
<br/>
              9.	Considérant qu'il ressort des pièces du dossier que le projet de construction doit être implanté sur un terrain situé à l'angle des deux voies que constituent le boulevard Hilarion Boeuf et la petite voie située au nord du terrain d'implantation du projet, qui permet la desserte d'une autre parcelle et qui doit être regardée comme une voie au sens du règlement du plan d'occupation des sols ; que le projet de construction s'inscrit dans les bandes d'une profondeur de 17 mètres pouvant être délimitées à partir de l'alignement de chacune de ces voies ; que dès lors, le moyen tiré de ce que le permis de construire méconnaîtrait les dispositions de l'article R. UA 7 du règlement du plan d'occupation des sols ne peut qu'être écarté ; <br/>
<br/>
              10.	Considérant, en troisième lieu, qu'aux termes de l'article R. UA3 du règlement du plan d'occupation des sols de la commune de Marseille, relatif à la voirie, alors applicable : " 1. Les constructions sont desservies par des voies publiques ou privées dont les caractéristiques, telles qu'elles se présentent au moment de l'exécution du projet, correspondent à leur destination. 2. Les accès sur les voies publiques sont aménagés de façon à éviter toute perturbation et tout danger pour la circulation générale (...) " ; qu'il ressort des pièces du dossier que le projet de construction est desservi par le boulevard Hilarion Boeuf, d'une largeur d'au moins 6 mètres, sur lequel la circulation s'effectue à sens unique et qui présente des caractéristiques permettant une desserte suffisante de l'immeuble à construire ; que le moyen tiré de la méconnaissance de ces dispositions doit, par suite, être écarté ; <br/>
<br/>
              11.	Considérant qu'il résulte de tout ce qui précède que M. et Mme A...ne sont pas fondés à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Marseille a rejeté leur demande ; <br/>
<br/>
              12.	Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la Société crédit agricole immobilier entreprise qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. et Mme A...une somme de 3 000 euros à verser à la société Crédit agricole immobilier entreprise, en application des dispositions de l'article L. 761-1 du code de justice administrative au titre de la procédure d'appel et de cassation ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la commune de Marseille devant la cour administrative d'appel de Marseille au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt en date du 18 octobre 2016 de la cour administrative d'appel de Marseille est annulé.<br/>
Article 2 : La requête présentée par M. et Mme A...devant la cour administrative d'appel de Marseille et les conclusions qu'ils ont présentées devant le Conseil d'Etat sont rejetées.<br/>
Article 3 : M. et Mme A...verseront à la société Crédit agricole immobilier entreprise une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de la commune de Marseille présentées devant la cour administrative d'appel de Marseille au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la société Crédit agricole immobilier entreprise, à M. et Mme B...A...et à la commune de Marseille.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
