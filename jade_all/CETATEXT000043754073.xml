<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043754073</ID>
<ANCIEN_ID>JG_L_2021_07_000000439121</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/75/40/CETATEXT000043754073.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 01/07/2021, 439121, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>439121</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON, MEGRET ; SCP BAUER-VIOLAS, FESCHOTTE-DESBOIS, SEBAGH</AVOCATS>
<RAPPORTEUR>Mme Pauline Hot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Stéphane Hoynck</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:439121.20210701</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Madame B... C... a demandé au tribunal administratif de Nice d'annuler pour excès de pouvoir l'arrêté du 2 décembre 2015 par lequel le maire d'Antibes (Alpes-Maritimes) a refusé de lui délivrer un permis de construire pour la réalisation d'une maison individuelle sur une parcelle cadastrée section AK n°67 située route de la Badine. Par un jugement n° 1602282 du 31 mai 2018, le tribunal administratif a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 18MA03255 du 26 décembre 2019, la cour administrative d'appel de Marseille a rejeté l'appel formé par Mme C... contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 26 février et 19 mai 2020 et le 29 janvier 2021 au secrétariat du contentieux du Conseil d'Etat, Mme C... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune d'Antibes la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme D... A..., auditrice,  <br/>
<br/>
              - les conclusions de M. Stéphane Hoynck, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Boré, Salve de Bruneton, Mégret, avocat de Mme C... et à la SCP Bauer-Violas, Feschotte-Desbois, Sebagh, avocat de la commune d'Antibes ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 2 décembre 2015, le maire d'Antibes a refusé de délivrer à Mme C... un permis de construire une maison individuelle. Par un arrêt du 26 décembre 2019, contre lequel Mme C... se pourvoit en cassation, la cour administrative d'appel de Marseille a rejeté son appel contre le jugement du tribunal administratif de Nice ayant rejeté sa demande tendant à l'annulation de ce refus. Il ressort des énonciations de l'arrêt attaqué que la cour a jugé que le maire d'Antibes pouvait légalement refuser le permis de construire en se fondant uniquement sur la méconnaissance de l'article UD10 du règlement du plan local d'urbanisme de la commune. <br/>
<br/>
              2. Aux termes de l'article L. 123-1-9 du code de l'urbanisme dans sa rédaction applicable à la date de l'arrêté litigieux : " Les règles et servitudes définies par un plan local d'urbanisme ne peuvent faire l'objet d'aucune dérogation, à l'exception des adaptations mineures rendues nécessaires par la nature du sol, la configuration des parcelles ou le caractère des constructions avoisinantes. " Il résulte de l'article UD 10 du règlement du plan local d'urbanisme de la commune d'Antibes que la hauteur absolue des constructions se trouvant dans le périmètre où se situe le terrain d'assiette du projet est mesurée du sol naturel ou excavé, à l'exception de l'excavation nécessaire à l'aménagement de la rampe d'accès au sous-sol, à l'aplomb de la façade au point le plus haut à l'égout du toit, et que les hauteurs maximales ne doivent pas dépasser 9 mètres. <br/>
<br/>
              3. Il appartient à l'autorité administrative, saisie d'une demande de permis de construire, de déterminer si le projet qui lui est soumis ne méconnaît pas les dispositions du plan local d'urbanisme applicables, y compris telles qu'elles résultent le cas échéant d'adaptations mineures lorsque la nature particulière du sol, la configuration des parcelles d'assiette du projet ou le caractère des constructions avoisinantes l'exige. A l'appui de sa contestation devant le juge de l'excès de pouvoir, du refus opposé à sa demande, le pétitionnaire peut se prévaloir de la conformité de son projet aux règles d'urbanisme applicables, le cas échéant assorties d'adaptations mineures dans les conditions précisées ci-dessus, alors même qu'il n'a pas fait état, dans sa demande à l'autorité administrative, de la nécessité de telles adaptations. <br/>
<br/>
              4. Pour rejeter le recours de Mme C..., la cour a relevé, par une appréciation souveraine exempte de dénaturation, que le projet de permis de construire refusé prévoyait une hauteur de 9,2 mètres par rapport au terrain naturel en certains points en méconnaissance de l'article UD 10 du règlement du plan local d'urbanisme de la commune. Mme C... faisait toutefois valoir devant les juges du fond que le terrain d'assiette du projet se caractérisait à la fois par sa déclivité et par la proximité immédiate du château de Laval, identifié par le plan local d'urbanisme comme un élément remarquable du patrimoine paysager et architectural, et que les adaptations intégrées au projet visaient à tenir compte de la première, tout en harmonisant la construction projetée avec le second, s'agissant notamment des toitures. En se bornant à relever que la seule circonstance que le terrain soit en pente ne rendait pas nécessaire une adaptation mineure aux dispositions du règlement du plan local d'urbanisme sans rechercher si, ainsi que le soutenait la requérante, le caractère des constructions environnantes, combiné avec la déclivité du terrain, ne justifiait pas une telle adaptation, la cour administrative d'appel a entaché son arrêt d'une erreur de droit. <br/>
<br/>
              5. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que Mme C... est fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune d'Antibes une somme de 3 000 euros à verser à Mme C... au titre de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font obstacle à ce qu'une somme soit mise à la charge de Mme C... qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 26 décembre 2019 de la cour administrative d'appel de Marseille est annulé. <br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.  <br/>
<br/>
Article 3 : La commune d'Antibes versera à Mme C... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à Mme B... C... et à la commune d'Antibes. <br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
