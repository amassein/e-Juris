<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042828461</ID>
<ANCIEN_ID>JG_L_2020_12_000000427201</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/82/84/CETATEXT000042828461.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 29/12/2020, 427201, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>427201</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Nicolas Agnoux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:427201.20201229</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° Sous le n° 427201, par une requête, un mémoire et un mémoire en réplique, enregistrés les 18 janvier et 28 juin 2019 et le 10 décembre 2020 au secrétariat du contentieux du Conseil d'Etat, la société ACI demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision du 19 novembre 2018 par laquelle le ministre d'Etat, ministre de la transition écologique et solidaire a rejeté l'ensemble de ses demandes de certificats d'économies d'énergie, l'a privée de la possibilité d'obtenir des certificats d'économies d'énergie pour une durée de vingt-quatre mois et a prononcé l'annulation des certificats d'économies d'énergie d'un volume égal à celui des opérations concernées par les non-conformités constatées dans le cadre du contrôle, soit 46 481 510 kWh cumac " hors précarité énergétique " et 4 108 300 kWh cumac " précarité énergétique " ;<br/>
<br/>
              2°) à titre subsidiaire, d'ordonner une médiation en application des articles L. 114-1, L. 213-1 et suivants du code de justice administrative ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              2° Sous le n° 432672, par deux mémoires et un mémoire en réplique enregistrés les 16 juillet et 16 octobre 2019 et le 10 décembre 2020 au secrétariat du contentieux du Conseil d'Etat, la société ACI demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette même décision du 19 novembre 2018 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le code de l'énergie ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le décret n° 2005-850 du 27 juillet 2005 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Nicolas Agnoux, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme A... D..., rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de la société Aci et à la SCP Lyon-Caen, Thiriez, avocat de la société Aci Fusion ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Les documents enregistrés sous le n° 432672 constituent en réalité des mémoires présentés par la société ACI, faisant suite à sa requête enregistrée sous le n° 427201. Par suite, ces documents doivent être rayés des registres du secrétariat du contentieux du Conseil d'Etat et être joints à la requête enregistrée sous le n° 427201. Dans ces conditions, la fin de non-recevoir opposée par la ministre en défense contre la requête enregistrée sous le n° 432672 doit être écartée. <br/>
<br/>
              2. Il résulte de l'instruction que la société ACI était délégataire d'obligations d'économie d'énergie en application de l'article R. 221-5 du code de l'énergie au titre de la troisième période d'économies d'énergie comprise entre 1er janvier 2015 et le 31 décembre 2017, dont elle s'acquittait à travers la réalisation d'opérations donnant lieu à la délivrance de certificats d'économies d'énergie (CEE). Par un courrier du 10 mai 2017, le ministre d'Etat, ministre de la transition écologique et solidaire a engagé un contrôle sur pièces de la conformité des opérations ayant fait l'objet de la délivrance de CEE sous le numéro 1041OB/25047 puis, par un courrier du 27 novembre 2017, l'a mise en demeure de lui transmettre dans un délai d'un mois les preuves de la conformité réglementaire de ces opérations. Par une décision du 19 novembre 2018, le ministre, après avoir constaté que les éléments produits par la société ne permettaient pas de lever les non-conformités relevées, a prononcé à son encontre une sanction comportant le rejet de ses neuf demandes de certificats d'économies d'énergie référencées sous les numéros 1041OB/25468, 25569, 25770, 26017, 26230, 26609, 27839, 28280 et 28907, la privation de la possibilité d'obtenir des certificats pour une durée de vingt-quatre mois et l'annulation des certificats d'un volume égal à celui des opérations concernées par les non-conformités constatées dans le cadre du contrôle, soit 46 481 510 kWh cumac " hors précarité énergétique " et 4 108 300 kWh cumac " précarité énergétique ". <br/>
<br/>
              3. D'une part, aux termes de l'article L. 222-1 du code de l'énergie : " Dans les conditions définies aux articles suivants, le ministre chargé de l'énergie peut sanctionner les manquements qu'il constate, de la part des personnes mentionnées à l'article L. 221-1, aux dispositions des articles L. 221-1 à L. 221-5 ou aux dispositions réglementaires prises pour leur application ". Aux termes de l'article L. 222-2 du même code : " Le ministre met l'intéressé en demeure de se conformer à ses obligations dans un délai déterminé. Il peut rendre publique cette mise en demeure. / Lorsque l'intéressé ne se conforme pas dans les délais fixés à cette mise en demeure, le ministre chargé de l'énergie peut : /(...) 2° Le priver de la possibilité d'obtenir des certificats d'économies d'énergie selon les modalités prévues au premier alinéa de l'article L. 221-7 et à l'article L. 221-12 ; / 3° Annuler des certificats d'économies d'énergie de l'intéressé, d'un volume égal à celui concerné par le manquement ; / 4° Suspendre ou rejeter les demandes de certificats d'économies d'énergie faites par l'intéressé (...) ". Aux termes de l'article R. 222-8 du même code, relatif aux opérations de contrôle de la régularité de la délivrance des certificats d'économies d'énergie : " La conformité de l'échantillon s'apprécie à partir de la somme des volumes de certificats d'économies d'énergie de chacune de ses opérations, établis conformément aux dispositions des deux premiers alinéas du présent article. L'échantillon est réputé conforme si le rapport entre la somme des volumes de certificats d'économies d'énergie établis pour les opérations de l'échantillon et la somme des volumes de certificats d'économies d'énergie délivrés pour les mêmes opérations est : (...) / 2° Pour les opérations engagées à partir du 1er janvier 2013, supérieur à 95 % ". Selon l'article R. 222-9 du même code dans sa rédaction applicable au litige : " Lorsque l'échantillon n'est pas réputé conforme, le ministre chargé de l'énergie met en demeure l'intéressé de transmettre, dans un délai d'un mois, les preuves de la conformité réglementaire des opérations d'économies d'énergie pour lesquelles des manquements ont été constatés. / Simultanément, le délai prévu par l'article R. 221-22 est suspendu pour les demandes de certificats d'économies d'énergies déposées par l'intéressé et n'ayant pas encore fait l'objet d'une délivrance de certificats ".   Enfin, aux termes de l'article R. 222-12 du même code : " Les décisions du ministre chargé de l'énergie prononçant les sanctions prévues à l'article L. 222-2 peuvent faire l'objet d'un recours de pleine juridiction et d'une demande de référé tendant à la suspension de leur exécution devant le Conseil d'Etat (...) ". <br/>
<br/>
              4. D'autre part, aux termes de l'article L. 231-1 du code des relations entre le public et l'administration : " Le silence gardé pendant deux mois par l'administration sur une demande vaut décision d'acceptation ". Aux termes de l'article R. 221-22 du code de l'énergie dans sa rédaction applicable au litige : " La demande de certificats d'économies d'énergie est adressée au ministre chargé de l'énergie. / (...) A compter de la date de réception d'un dossier complet, le ministre chargé de l'énergie délivre les certificats dans un délai de : / 1° Six mois pour les demandes relatives à des opérations spécifiques et pour les demandes relatives à des opérations standardisées de longue durée définies par arrêté du ministre chargé de l'énergie, ne relevant pas d'un plan d'actions et engagées avant le 31 décembre 2014 ; / 2° Deux mois pour les autres demandes ".<br/>
<br/>
              5. En premier lieu, l'article 1er du décret du 27 juillet 2005 relatif aux délégations de signature des membres du Gouvernement dispose que : " A compter du jour suivant la publication au Journal officiel de la République française de l'acte les nommant dans leurs fonctions (...) peuvent signer, au nom du ministre ou du secrétaire d'Etat et par délégation, l'ensemble des actes, à l'exception des décrets, relatifs aux affaires des services placés sous leur autorité : / 1° Les secrétaires généraux des ministères, les directeurs d'administration centrale (...) que le décret d'organisation du ministère rattache directement au ministre ou au secrétaire d'Etat ". La décision du 19 novembre 2018 en litige a été signée par M. C... B..., directeur général de l'énergie et du climat, pour le ministre d'Etat, ministre de la transition écologique et solidaire et par délégation de celui-ci. Le directeur général de l'énergie et du climat est un directeur d'administration centrale, directement rattaché au ministre chargé de la transition écologique et solidaire. M. C... B... ayant été nommé par un décret du 19 décembre 2012 publié au Journal officiel de la République française le 21 décembre 2012, il avait, du fait de cette publication, qualité pour signer cette décision. Le moyen tiré de ce que cette dernière aurait été prise par une autorité incompétente doit, par suite, être écarté. <br/>
<br/>
              6. En deuxième lieu, la décision du 19 novembre 2018, notifiée à la société par un courrier réceptionné le 21 novembre suivant, est suffisamment circonstanciée en fait comme en droit. La société requérante ne peut utilement soutenir que le ministre n'aurait pas motivé sa décision en se prévalant des seules mentions figurant dans l'extrait publié au Journal officiel du 16 mai 2019.<br/>
<br/>
              7. En troisième lieu, il résulte des dispositions citées au point 4 ci-dessus que le silence gardé par le ministre chargé de l'énergie sur une demande de CEE autre que celles relatives à des opérations spécifiques fait naître une décision implicite d'acceptation à l'issue d'un délai de deux mois suivant la date de réception par le ministre du dossier de demande. Lorsque, dans le cadre du contrôle de la régularité de la délivrance des CEE, le ministre chargé de l'énergie notifie une mise en demeure en application de l'article R. 222-9 du code de l'énergie, ce délai est suspendu au titre des demandes de certificats déposées avant la mise en demeure et n'ayant pas donné lieu à décision implicite et il ne peut commencer à courir pour toutes les demandes présentées ultérieurement. La mise en demeure cesse de produire ses effets lorsque le ministre décide de prononcer l'une ou plusieurs des sanctions prévues à l'article L. 222-2 du même code ou informe le demandeur qu'il renonce à faire usage de ces dispositions.<br/>
<br/>
              8. Il résulte de l'instruction que, par un courrier du 27 novembre 2017, le ministre a mis en demeure la société requérante de lui transmettre les preuves de la conformité réglementaire des opérations contrôlées, en l'informant qu'il suspendait, en application de l'article R. 222-9 du code de l'énergie, le délai au terme duquel est susceptible d'intervenir une décision implicite d'acceptation pour l'ensemble des demandes de CEE déposées par la société et de toute nouvelle demande faisant l'objet d'un dépôt. Par conséquent, ce délai a été suspendu, d'une part, au titre des cinq demandes de CEE pour lesquelles le ministre avait prononcé le 23 octobre 2017 le retrait des décisions implicites d'acceptation en vue du réexamen des demandes et, d'autre part, au titre des quatre demandes réceptionnées par le ministre les 2 octobre, 16 novembre, 12 et 28 décembre 2017. La société requérante n'est donc pas fondée à soutenir que le ministre ne pouvait régulièrement prononcer le rejet des neuf dossiers de demande au motif qu'ils auraient fait l'objet, à la date de la sanction, d'une décision implicite d'acceptation.<br/>
<br/>
              9. En quatrième lieu, si la société soutient que le ministre a commis une erreur manifeste, un abus et un détournement de pouvoir en se fondant, pour prononcer les sanctions litigieuses, sur la non-conformité des opérations contrôlées résultant de la falsification de plusieurs documents, de l'absence des mentions et éléments requis pour établir le rôle actif et incitatif de la société dans la réalisation des opérations, du caractère non authentique des signatures et cachets apposés sur les contrats de partenariat conclus avec les professionnels et des irrégularités affectant les déclarations sur l'honneur, elle n'assortit pas ces moyens de précisions suffisantes pour en apprécier le bien-fondé, à défaut, notamment, de produire les pièces utiles des dossiers en litige. En outre, le moyen tiré de ce que le ministre se serait fondé sur l'absence de certification " Qualibat RGE ", à l'exclusion de toute autre certification, pour retenir que les entreprises partenaires de la société ne disposaient pas de la qualification requise manque en fait. Enfin, la société requérante n'est pas fondée à soutenir qu'en lui demandant de fournir la preuve de la conformité des opérations contrôlées, le ministre aurait de ce seul fait commis un abus ou un détournement de pouvoir.<br/>
<br/>
              10. En dernier lieu, la société ne peut utilement soutenir que les sanctions qu'elle conteste seraient disproportionnées au motif qu'elle aurait fait la preuve de sa bonne foi en proposant au ministre de retirer les opérations reconnues comme non-conformes, alors, au surplus, que le ministre avait établi que le taux de non-conformité des opérations contrôlées était de 100 %. En outre, la période de vingt-quatre mois au titre de laquelle la société a été privée de la possibilité d'obtenir de nouveaux certificats n'apparaît pas, en dépit de la durée durant laquelle le délai implicite d'acceptation de ses demandes de certificats a été suspendu en application de la mise en demeure du 27 novembre 2017, disproportionnée au regard des manquements constatés. Enfin, la circonstance qu'elle ne serait pas à l'origine des falsifications relevées par l'administration et portant sur des documents transmis par ses partenaires ne peut être de nature à atténuer le manquement de la société requérante aux obligations, qui n'incombent qu'à elle, prescrites pour la délivrance des CEE obtenus au titre d'opérations réalisées par l'intermédiaire de tiers.<br/>
<br/>
              11. Il résulte de tout ce qui précède que la société ACI n'est pas fondée à demander l'annulation de la décision qu'elle attaque ni, en tout état de cause, à demander au Conseil d'Etat d'ordonner une médiation pour parvenir à un accord entre les parties au litige en application de l'article L. 114-1 du code de justice administrative.<br/>
<br/>
              12. La requête de la société ACI doit, par suite, être rejetée, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les productions enregistrées sous le n° 432672 seront rayées du registre du secrétariat du contentieux du Conseil d'Etat pour être jointes à la requête n° 427201.<br/>
Article 2 : La requête de la société ACI est rejetée.<br/>
Article 3 : La présente décision sera notifiée à la société ACI et à la ministre de la transition écologique.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
