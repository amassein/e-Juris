<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043246452</ID>
<ANCIEN_ID>JG_L_2021_03_000000447504</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/24/64/CETATEXT000043246452.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 12/03/2021, 447504, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-03-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>447504</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Bruno Bachini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier  Fuchs</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:447504.20210312</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un mémoire et deux mémoires en réplique, enregistrés le 22 décembre 2020, le 19 janvier et le 5 février 2021 au secrétariat du contentieux du Conseil d'État, Mme A... B... demande au Conseil d'État, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 et à l'appui de sa requête tendant à l'annulation pour excès de pouvoir des énonciations du 1.3 de la fiche n° 1 " champ d'application des obligations LCBFT " du guide pratique " lutte contre le blanchiment de capitaux et le financement du terrorisme " publié, dans sa 3ème édition, par le Conseil national des barreaux, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution du 3° du I de l'article L. 561-3 du code monétaire et financier. <br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ; <br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - la directive (UE) 2015/849 du Parlement européen et du Conseil du 20 mai 2015 ;<br/>
              - la directive (UE) 2018/843 du Parlement européen et du Conseil du 30 mai 2018 ;<br/>
              - l'article L. 561-3 du code monétaire et financier ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Bachini, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Olivier Fuchs, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Piwnica, Molinié, avocat du Conseil national des barreaux ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 25 février 2021, présentée par Mme B... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. Aux termes de l'article L. 561-3 du code monétaire et financier, modifié par les dispositions du b) du 3° de l'article 2 de l'ordonnance du 12 février 2020 renforçant le dispositif national de lutte contre le blanchiment de capitaux et le financement du terrorisme, prises pour la transposition de la directive (UE) 2018/843 du 30 mai 2018 en tant que le a) du 1. de son article 1er a modifié le a) du 3) du 1. de l'article 2 de la directive (UE) 2015/849 du 20 mai 2015 : " I. - Les personnes mentionnées au 13° de l'article L. 561-2 sont soumises aux dispositions du présent chapitre lorsque, dans le cadre de leur activité professionnelle : / (...) 2° Elles assistent leur client dans la préparation ou la réalisation des transactions concernant : / a) L'achat et la vente de biens immeubles ou de fonds de commerce ;  / b) La gestion de fonds, titres ou autres actifs appartenant au client ; / c) L'ouverture de comptes bancaires, d'épargne ou de titres ou de contrats d'assurance ; / d) L'organisation des apports nécessaires à la création des sociétés ; / e) La constitution, la gestion ou la direction des sociétés ; /f) La constitution, la gestion ou la direction de fiducies, régies par les articles 2011 à 2031 du code civil ou de droit étranger, ou de toute autre structure similaire ; / g) La constitution ou la gestion de fonds de dotation ou de fonds de pérennité ; / 3° Elles fournissent, directement ou par toute personne interposée à laquelle elles sont liées, des conseils en matière fiscale. / II. - Les avocats au Conseil d'Etat et à la Cour de cassation, les avocats et les personnes mentionnées au 18° de l'article L. 561-2 dans l'exercice d'une activité mentionnée au I ne sont pas soumis aux dispositions de la section 4 du présent chapitre et de l'article L. 561-25 lorsque l'activité se rattache à une procédure juridictionnelle, que les informations dont ils disposent soient reçues ou obtenues avant, pendant ou après cette procédure, y compris dans le cadre de conseils relatifs à la manière d'engager ou d'éviter une telle procédure, non plus que lorsqu'ils donnent des consultations juridiques, à moins qu'elles n'aient été fournies à des fins de blanchiment de capitaux ou de financement du terrorisme ou en sachant que le client les demande aux fins de blanchiment de capitaux ou de financement du terrorisme (...) ".<br/>
<br/>
              3. Aux termes de l'article L. 561-2 du code monétaire et financier relatif aux personnes assujetties aux obligations de lutte contre le blanchiment des capitaux et le financement du terrorisme : " Sont assujettis aux obligations prévues par les dispositions des sections 2 à 7 du présent chapitre : / (...) 13° Les avocats au Conseil d'Etat et à la Cour de cassation, les avocats, les notaires, les huissiers de justice, les administrateurs judiciaires, les mandataires judiciaires et les commissaires-priseurs judiciaires, dans les conditions prévues à l'article L. 561-3 (...) ". <br/>
<br/>
              4. Aux termes de l'article 8 de la Déclaration des droits de l'homme et du citoyen de 1789 : " La loi ne doit établir que des peines strictement et évidemment nécessaires, et nul ne peut être puni qu'en vertu d'une loi établie et promulguée antérieurement au délit, et légalement appliquée ". Les principes ainsi énoncés ne concernent pas seulement les peines prononcées par les juridictions pénales mais s'étendent à toute sanction ayant le caractère d'une punition. Appliquée en dehors du droit pénal, l'exigence d'une définition des infractions sanctionnées se trouve satisfaite, en matière administrative, dès lors que les textes applicables font référence aux obligations auxquelles les intéressés sont soumis en raison de l'activité qu'ils exercent, de la profession à laquelle ils appartiennent, de l'institution dont ils relèvent ou de la qualité qu'ils revêtent.<br/>
<br/>
              5. Il résulte des termes des dispositions du 3° du I de l'article L. 561-3 du code monétaire et financier introduites par l'article 2 de l'ordonnance du 12 février 2020 pour transposer les dispositions du a) du 1. de l'article 1er de la directive du 30 mai 2018 modifiant le a) du 3) du 1. de l'article 2 de la directive du 20 mai 2015 relative à la prévention de l'utilisation du système financier aux fins du blanchiment de capitaux ou du financement du terrorisme que les prestations d'assistance ou de conseil en matière fiscale délivrées, directement ou par toute personne intermédiaire à laquelle ils sont liés, par les avocats au Conseil d'Etat et à la Cour de cassation, avocats, notaires, huissiers de justice, administrateurs judiciaires, mandataires judiciaires et commissaires-priseurs judiciaires entrent dans leur ensemble dans le champ d'application des obligations relatives à la lutte contre le blanchiment de capitaux et le financement du terrorisme posées par le chapitre premier du titre VI du livre V de la partie législative du code monétaire et financier, sans préjudice de l'application aux avocats de la dispense, prévue par le II de ce même article, de l'obligation de déclaration et d'information posée par la section 4 du même chapitre ainsi que de l'obligation de communication à la cellule de renseignement financier nationale prescrite par l'article L. 561-25, pour ce qui concerne les informations obtenues avant, pendant ou après une procédure judiciaire ou lors de l'évaluation de la situation juridique d'un client, y compris pour ces prestations de conseil en matière fiscale qui participent de la mission générale de consultation juridique inhérente à la profession d'avocat. Dès lors, la requérante n'est pas fondée à soutenir que ces dispositions méconnaîtraient le principe de légalité des délits et des peines découlant de l'article 8 de la Déclaration des droits de l'homme et du citoyen de 1789. Par suite, la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux.6. Il résulte de ce qui précède qu'il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée.   <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par Mme B....<br/>
Article 2 : La présente décision sera notifiée à Mme A... B..., au Conseil national des barreaux, au garde des sceaux, ministre de la justice, et au ministre de l'économie, des finances et de la relance. <br/>
Copie en sera adressée au Conseil constitutionnel et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
