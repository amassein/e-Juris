<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032916591</ID>
<ANCIEN_ID>JG_L_2016_07_000000380905</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/91/65/CETATEXT000032916591.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 19/07/2016, 380905, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-07-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>380905</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE ; SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>Mme Anne Egerszegi</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:380905.20160719</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Melun d'annuler pour excès de pouvoir la décision du 26 mars 2009 par laquelle le maire d'Ivry-sur-Seine a prononcé son licenciement pour inaptitude physique, d'enjoindre au maire de le réintégrer dans son poste, de lui remettre une attestation Assedic ainsi qu'une fiche de paie conforme à la décision à intervenir et de condamner la commune d'Ivry-sur-Seine à lui verser les salaires qu'il estime lui être dus durant la période d'éviction ainsi qu'une indemnité de 50 000 euros, une somme de 25 300 euros au titre des salaires du 1er février 2008 au 26 mars 2009, les congés payés correspondants, une indemnité compensatrice de préavis, un rappel d'indemnité de licenciement et une somme de 40 000 euros en réparation du préjudice résultant de son recrutement comme vacataire. Par un jugement n° 0904143/5 du 2 octobre 2012, le tribunal administratif a rejeté sa demande. <br/>
<br/>
              	Par un arrêt n° 12PA05054 du 1er avril 2014, la cour administrative d'appel de Paris a rejeté l'appel formé par M. A...contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 3 juin et 23 décembre 2014 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de la commune d'Ivry-sur-Seine la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - la loi n° 2005-843 du 26 juillet 2005 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anne Egerszegi, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de M. A...et à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la commune d'Ivry-sur-Seine ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. M. B...A..., employé en tant qu'éducateur des activités physiques et sportives de la commune d'Ivry-sur-Seine par un contrat à durée déterminée courant du 3 octobre 2005 au 4 juillet 2006, a été victime d'un accident de service le 25 avril 2006. Son état de santé a été consolidé à la date du 31 janvier 2008 avec une incapacité permanente partielle de 15 %. A la suite de plusieurs propositions de reclassement déclinées par l'intéressé, le maire d'Ivry-sur-Seine a prononcé son licenciement pour inaptitude physique par un arrêté du 26 mars 2009 contre lequel M. A...a formé une requête en annulation, en demandant également l'indemnisation de divers chefs de préjudice. M. A... se pourvoit en cassation contre l'arrêt de la cour administrative d'appel de Paris du 1er avril 2014 qui a rejeté son appel contre le jugement du 2 octobre 2012 du tribunal administratif de Melun rejetant ces demandes. <br/>
<br/>
              2. Il résulte des dispositions de l'article 3 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale, dans sa version résultant de l'article 4 de la loi du 26 juillet 2005 portant diverses mesures de transposition du droit communautaire à la fonction publique, que les collectivités territoriales ne peuvent recruter par contrat à durée déterminée des agents non titulaires que, d'une part, au titre des premier et deuxième alinéas de cet article, en vue d'assurer des remplacements momentanés ou d'effectuer des tâches à caractère temporaire ou saisonnier définies à ces alinéas, et, d'autre part, au titre des quatrième, cinquième et sixième alinéas du même article, lorsqu'il n'existe pas de cadre d'emplois de fonctionnaires susceptibles d'assurer certaines fonctions, lorsque, pour des emplois de catégorie A, la nature des fonctions ou les besoins des services le justifient et, dans les communes de moins de 1 000 habitants, lorsque la durée de travail de certains emplois n'excède pas la moitié de celle des agents publics à temps complet. Aux termes des septième et huitième alinéas de cet article, dans sa rédaction issue de la loi du 26 juillet 2005 : " Les agents recrutés conformément aux quatrième, cinquième, et sixième alinéas sont engagés par des contrats à durée déterminée, d'une durée maximale de trois ans. Ces contrats sont renouvelables, par reconduction expresse. La durée des contrats successifs ne peut excéder six ans. / Si, à l'issue de la période maximale de six ans mentionnée à l'alinéa précédent, ces contrats sont reconduits, ils ne peuvent l'être que par décision expresse et pour une durée indéterminée". Aux termes du I de l'article 15 de la loi du 26 juillet 2005 : " Lorsque l'agent, recruté sur un emploi permanent, est en fonction à la date de publication de la présente loi (...), le renouvellement de son contrat est soumis aux conditions prévues aux septième et huitième alinéas de l'article 3 [de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale]. / Lorsque, à la date de publication de la présente loi, l'agent est en fonction depuis six ans au moins, de manière continue, son contrat ne peut, à son terme, être reconduit que par décision expresse pour une durée indéterminée." <br/>
<br/>
              3. Il résulte de l'ensemble de ces dispositions que, pour les agents contractuels de la fonction publique territoriale recrutés sur un emploi permanent, en fonction au moment de l'entrée en vigueur de la loi du 26 juillet 2005, le renouvellement de contrat régi par le I de l'article 15 de cette loi doit intervenir selon les règles fixées par les septième et huitième alinéas de l'article 3 de la loi du 26 janvier 1984 et ne peut donc concerner que les titulaires de contrats entrant dans les catégories énoncées aux quatrième, cinquième et sixième alinéas de ce même article. Cette disposition ne saurait toutefois s'appliquer aux contrats passés au titre du remplacement momentané de titulaires ou pour faire face temporairement et pour une durée maximale d'un an à la vacance d'un emploi, tels que visés par le premier alinéa du même article 3 de la loi du 26 janvier 1984. Dès lors, seuls les agents bénéficiant de contrats entrant dans les catégories prévues par les quatrième, cinquième et sixième alinéas de ce même article peuvent se voir proposer, par décision expresse et après six années de fonction au moins, un contrat à durée indéterminée. En revanche, la durée hebdomadaire du travail effectué est sans incidence sur l'appréciation du caractère permanent ou non de l'emploi occupé, résultant seulement de la nature du besoin auquel répond cet emploi. <br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond que M. A... a été recruté en 1997 et a exercé successivement les fonctions de surveillant de cantine, puis de contrôleur de théâtre à compter du 18 juin 1998, et enfin d'éducateur des activités physiques et sportives non titulaire pour le mois de juillet 2004 puis sous contrat à durée déterminée à compter du 3 octobre 2005 pour une durée de 10 mois jusqu'au 4 juillet 2006 et une durée hebdomadaire de travail de 8 heures. A la suite de l'accident de service dont il a été victime le 25 avril 2006, il a été placé en congé pour maladie jusqu'à la consolidation de son état de santé le 31 janvier 2008. M. A...a fait valoir devant la cour administrative d'appel de Paris qu'il était devenu titulaire d'un contrat à durée indéterminée à la suite du renouvellement de ses contrats à durée déterminée depuis 1997. Toutefois, et alors même que les juges d'appel ont relevé, par un motif surabondant de leur arrêt, qu'il n'apportait pas la preuve qu'il exerçait ses fonctions à temps complet, la cour a pu déduire, sans commettre d'erreur de droit ni d'erreur de qualification juridique des faits, qu'il résultait des constatations ci-dessus rappelées que, faute d'avoir été recruté sur un emploi permanent, M. A...ne remplissait pas les conditions exigées par les dispositions combinées des articles 15 de la loi du 26 juillet 2005 et 3 de la loi du 26 janvier 1984 permettant la transformation de son contrat en contrat à durée indéterminée et que, dès lors, il n'était pas titulaire d'un tel contrat. En outre, les postes proposés pouvant être considérés comme équivalents à ses anciennes fonctions, les moyens du pourvoi tirés de ce que la cour a commis une erreur de droit et une erreur de qualification juridique des faits en jugeant que la commune avait satisfait à son obligation de reclassement ne peuvent qu'être écartés. <br/>
<br/>
              5. M. A...soutient enfin que la cour aurait commis une erreur de qualification juridique des faits en jugeant, par adoption des motifs des premiers juges, qu'il n'occupait pas un emploi permanent. Il ressort cependant des termes mêmes de l'arrêt attaqué que la cour n'a pas adopté les motifs du jugement du tribunal administratif sur ce point, mais a jugé que les conclusions indemnitaires présentées par l'intéressé tendant au remboursement d'un préjudice résultant de son maintien en situation précaire pendant plusieurs années n'étaient pas assorties de précisions suffisantes permettant d'en apprécier le bien-fondé. M. A...ne critiquant pas une telle motivation, ce moyen ne peut qu'être écarté. <br/>
<br/>
              6. Il résulte de ce qui précède que le pourvoi de M. A...doit être rejeté, y compris ses conclusions formées au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. B...A...et à la commune d'Ivry-sur-Seine.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
