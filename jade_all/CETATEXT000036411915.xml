<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036411915</ID>
<ANCIEN_ID>JG_L_2017_12_000000410139</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/41/19/CETATEXT000036411915.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 27/12/2017, 410139, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-12-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>410139</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Marc Lambron</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:410139.20171227</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. et Mme C...ont demandé au tribunal administratif de Cergy-Pontoise de condamner l'Etat à les indemniser du préjudice résultant du refus implicite du préfet des Hauts-de-Seine de leur accorder le concours de la force publique pour l'exécution d'une décision de justice ordonnant l'expulsion de MmeB..., occupante d'un logement situé 17, rue Ferdinand Buisson à Clichy dont ils sont propriétaires. Par un jugement n° 1507420 du 24 février 2017, le tribunal administratif a partiellement fait droit à leur demande.<br/>
<br/>
              Par un pourvoi, enregistré le 28 avril 2017 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de subroger l'Etat  dans les droits de M. et Mme C....<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Lambron, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il appartient au juge administratif, lorsqu'il détermine le montant et la forme des indemnités allouées par lui, de prendre, au besoin d'office, les mesures nécessaires pour que sa décision n'ait pas pour effet de procurer à la victime d'un dommage, par les indemnités qu'elle a pu ou pourrait obtenir en raison des mêmes faits, une réparation supérieure au préjudice subi ; que, par suite, lorsqu'il condamne l'Etat à indemniser le propriétaire auquel le préfet a refusé le concours de la force publique pour exécuter un jugement ordonnant l'expulsion des occupants d'un local, le juge doit, au besoin d'office, subroger l'Etat, dans la limite de l'indemnité mise à sa charge, dans les droits que le propriétaire peut détenir sur les occupants au titre de l'occupation irrégulière de son bien pendant la période de responsabilité de l'Etat ;<br/>
<br/>
              2. Considérant que, par le jugement attaqué, le tribunal administratif de Cergy-Pontoise a condamné l'Etat à verser à M. et Mme C...une indemnité destinée à réparer les préjudices par eux subis du fait du refus du préfet des Hauts-de-Seine de leur accorder le concours de la force publique pour exécuter une ordonnance du juge des référés du tribunal d'instance d'Asnières prononçant l'expulsion de Mme B...du logement situé 17, rue Ferdinand Buisson à Clichy, sans subordonner le versement de cette indemnité à la subrogation mentionnée au point 1 ; qu'il a ainsi commis une erreur de droit ; que son jugement doit être annulé en tant qu'il omet de subordonner le versement de l'indemnité mise à la charge de l'Etat à la subrogation de ce dernier dans les droits que M. et Mme C...peuvent détenir sur Mme B...au titre de l'occupation irrégulière de leur bien pendant la période de responsabilité de l'Etat ; <br/>
<br/>
              3. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative dans la mesure de la cassation prononcée ;<br/>
<br/>
              4. Considérant que pour les motifs exposés au point 1, il y a lieu de subordonner le versement de l'indemnité que le jugement du 24 février 2017 accorde à M. et Mme C...à la subrogation de l'Etat, dans la limite du montant de cette indemnité, dans les droits que ceux-ci peuvent détenir sur Mme B...au titre de l'occupation irrégulière, entre le 5 mai 2013 et le 30 mars 2015, du logement leur appartenant situé 17, rue Ferdinand Buisson à Clichy ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 24 février 2017 du tribunal administratif de Cergy-Pontoise est annulé en tant qu'il omet de subordonner le paiement de l'indemnité qu'il met à la charge de l'Etat à la subrogation de ce dernier, dans la limite du montant de cette indemnité, dans les droits que M. et Mme C...peuvent détenir sur Mme B...au titre de l'occupation irrégulière de leur bien pendant la période de responsabilité de l'Etat.<br/>
<br/>
Article 2 : Le paiement de l'indemnité que le jugement du 24 février 2017 du tribunal administratif de Cergy-Pontoise accorde à M. et Mme C...est subordonné à la subrogation de l'Etat dans les droits qu'ils peuvent détenir sur Mme B...au titre de l'occupation irrégulière, entre le 5 mai 2013 et le 30 mars 2015, du logement leur appartenant situé 17, rue Ferdinand Buisson à Clichy.<br/>
<br/>
Article 3 : La présente décision sera notifiée au ministre d'Etat, ministre de l'intérieur, à M. D...C..., à Mme E...C...et à Mme A...B....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
