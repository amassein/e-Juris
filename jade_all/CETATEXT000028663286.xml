<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028663286</ID>
<ANCIEN_ID>JG_L_2014_02_000000356006</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/66/32/CETATEXT000028663286.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 26/02/2014, 356006, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-02-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>356006</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>M. Alain Méar</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Astrid Nicolazo de Barmon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:356006.20140226</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 20 janvier et 20 avril 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour l'Association des viticulteurs d'Alsace, dont le siège est situé à la Maison des vins d'Alsace, 12 avenue de la foire aux vins, BP 91225, à Colmar (68012 cedex) ; l'association demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2011-1621 du 23 novembre 2011 relatif à l'appellation d'origine contrôlée " Gaillac " ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le règlement (CE) n° 1234/2007 du Conseil, du 22 octobre 2007, portant organisation commune des marchés dans le secteur agricole et dispositions spécifiques en ce qui concerne certains produits de ce secteur (règlement " OCM unique ") ;<br/>
<br/>
              Vu le règlement (CE) n° 607/2009 de la Commission, du 14 juillet 2009, fixant certaines modalités d'application du règlement (CE) n° 479/2008 du Conseil en ce qui concerne les appellations d'origine protégées et les indications géographiques protégées, les mentions traditionnelles, l'étiquetage et la présentation de certains produits du secteur vitivinicole ;<br/>
<br/>
              Vu le code rural et de la pêche maritime ;<br/>
<br/>
              Vu l'arrêté interministériel du 8 février 2007 fixant la composition du comité national des vins, eaux-de-vie et autres boissons alcoolisées de l'Institut national de l'origine et de la qualité ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alain Méar, Conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Marie-Astrid Nicolazo de Barmon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de l'Association des viticulteurs d'Alsace ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 641-6 du code rural et de la pêche maritime : " La reconnaissance d'une appellation d'origine contrôlée est proposée par l'Institut national de l'origine et de la qualité (...)./ La proposition  de l'Institut porte sur l'aire géographique de production (...), ainsi que sur la détermination des conditions de production qui figurent dans un cahier des charges " ; qu'aux termes de l'article L. 641-7 du même code : " La reconnaissance d'une appellation d'origine contrôlée est prononcée par un décret qui homologue un cahier des charges où figurent notamment la délimitation de l'aire géographique de production de cette appellation ainsi que ses conditions de production " ;<br/>
<br/>
              2. Considérant que, par le décret attaqué du 23 novembre 2011, le Premier ministre a homologué un nouveau cahier des charges de l'appellation d'origine contrôlée " Gaillac " en vertu duquel le nom de l'appellation peut être complété par la mention " vendanges tardives " pour les vins répondant aux conditions qu'il fixe pour cette mention ; que l'Association des viticulteurs d'Alsace demande au Conseil d'État l'annulation pour excès de pouvoir de ce décret ;<br/>
<br/>
              Sur la légalité externe :<br/>
<br/>
              3. Considérant, en premier lieu, qu'en vertu des dispositions de l'article L. 641-9 du code rural et de la pêche maritime, les comités nationaux de l'Institut national de l'origine et de la qualité (INAO), dont le comité national compétent en matière d'appellations d'origine relatives aux vins, sont composés de professionnels, de représentants des administrations et de personnalités qualifiées assurant notamment la représentation des consommateurs ; que l'article R. 642-10 du même code dispose, dans sa version applicable au litige issue du décret du 7 octobre 2009 : " I. - Chaque comité national comprend, outre son président : / 1° Un membre de chacun des autres comités nationaux et du conseil chargé des agréments et contrôles ; / 2° Des représentants des secteurs de la production, de la transformation ou du négoce des produits relevant de la compétence du comité ; / 3° Des représentants de l'administration ; / 4° Des personnalités qualifiées, notamment, en matière d'exportation et de distribution ou par leurs capacités d'expertise ainsi que des représentants des consommateurs./ II. - Les représentants des secteurs de la production, de la transformation ou du négoce sont choisis:/ 1° Pour le comité national des appellations d'origine relatives aux vins et aux boissons alcoolisées, et des eaux-de-vie : parmi les membres des comités régionaux (...) " ; que l'article R. 642-11 du même code prévoit : " La composition des comités nationaux est fixée par arrêté des ministres chargés de l'agriculture et de la consommation dans le respect des règles suivantes:/ - le nombre de représentants des secteurs professionnels mentionnés au 2° de l'article R. 642-10 et des personnalités qualifiées mentionnées au 4° du même article ne peut excéder cinquante ;/ - les représentants des secteurs professionnels constituent au moins la moitié des membres du comité ;/ - les représentants de l'administration constituent le quart au plus des membres du comité. " ; qu'enfin, en vertu de l'arrêté interministériel du 8 février 2007, le comité national, dénommé alors " comité national des vins, eaux-de-vie et autres boissons alcoolisées ", est composé de quatre représentants des autres comités nationaux et du conseil chargé des agréments et contrôles, de quarante représentants des secteurs de la production, de la transformation ou du négoce, de six représentants de l'administration et d'un maximum de dix personnalités qualifiées, notamment, en matière d'exportation et de distribution ou par leurs capacités d'expertise, et au titre desquelles figurent des représentants des consommateurs ; <br/>
<br/>
              4. Considérant que si, depuis l'intervention de cet arrêté interministériel, le comité national compétent en matière d'appellations d'origine relatives aux vins est devenu " comité national des appellations d'origine relatives aux vins et aux  boissons alcoolisées, et des eaux-de-vie ", cette modification n'a pas affecté les règles gouvernant la répartition des sièges entre les différentes catégories de membres du comité ; que, par suite, l'association requérante n'est pas fondée à soutenir que le décret attaqué serait entaché d'un vice de procédure au motif que les membres du comité national des appellations d'origine relatives aux vins et aux boissons alcoolisées, et des eaux-de-vie, qui a proposé, en application de l'article R. 642-7 cité plus haut, l'homologation du cahier des charges de l'appellation d'origine contrôlée " Gaillac ", ayant été désignés conformément à l'arrêté interministériel du 8 février 2007, ce comité était irrégulièrement composé ;<br/>
<br/>
              5. Considérant, en deuxième lieu, que l'article R. 642-8 du code rural et de la pêche maritime prévoit : " Une commission permanente, composée de vingt membres au plus, est constituée par chaque comité lors de sa première réunion (...)./ La commission permanente a compétence pour traiter les affaires courantes du comité national et exercer les attributions qui lui ont, le cas échéant, été déléguées par le comité " ; que si l'association requérante soutient que la commission permanente du comité national des appellations d'origine relatives aux vins et aux boissons alcoolisées, et des eaux-de-vie, ainsi que ce comité national, n'auraient pas siégé dans une composition régulière lors de leurs séances, respectivement, des 24 mars et 28 septembre 2011, au cours desquelles a été approuvé le cahier des charges de l'appellation d'origine contrôlée " Gaillac " homologué par le décret attaqué, ils n'apportent à l'appui de ce moyen aucune précision permettant d'en apprécier la portée ; qu'au surplus, il ressort des pièces du dossier et notamment des procès-verbaux de ces séances et du règlement intérieur de l'INAO, produits par le ministre chargé de l'agriculture, que les règles de quorum ont été respectées et que les membres présents avaient été régulièrement nommés à la commission permanente et au comité national ; que ce moyen doit donc être écarté ;<br/>
<br/>
              6. Considérant, en troisième lieu, que lorsqu'il se prononce sur la reconnaissance d'une appellation d'origine contrôlée, l'INAO prend une décision administrative et n'a pas le caractère d'un tribunal au sens de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; que, par suite, le moyen tiré de la méconnaissance du droit à un tribunal impartial garanti par ces stipulations ne peut qu'être écarté ;<br/>
<br/>
              7. Considérant, en quatrième lieu, que le comité national des appellations d'origine relatives aux vins et aux boissons alcoolisées, et des eaux-de-vie a constitué une commission d'enquête pour l'éclairer dans le cadre de l'instruction du dossier de reconnaissance de l'appellation d'origine contrôlée " Gaillac " ; que le comité national a choisi les membres de cette commission d'enquête en son sein parmi ceux de ses membres qui représentent les producteurs et les négociants en veillant à ce qu'ils n'aient aucune activité ou intérêt économique dans l'aire viticole de Gaillac ; que l'impartialité de ces professionnels ne peut, en tout état de cause, être contestée au seul motif qu'ils seraient issus de régions dans lesquelles sont produits des vins blancs liquoreux pour lesquels une demande d'attribution de la mention " vendanges tardives " pourrait être éventuellement présentée, dans l'avenir, comme pour les vins blancs liquoreux de l'appellation " Gaillac " ; <br/>
<br/>
              Sur la légalité interne :<br/>
<br/>
              8. Considérant que le règlement (CE) n° 1234/2007 du Conseil, du 22 octobre 2007, portant organisation commune des marchés dans le secteur agricole et dispositions spécifiques en ce qui concerne certains produits de ce secteur (règlement " OCM unique "), prévoit à son article 118 duovicies : " 1. On entend par " mention traditionnelle " une mention employée de manière traditionnelle dans un État membre (...):/ (...) b) pour désigner la méthode de production ou de vieillissement ou la qualité, la couleur, le type de lieu ou un événement particulier lié à l'histoire du produit bénéficiant d'une appellation d'origine protégée ou d'une indication géographique protégée./ 2. Les mentions traditionnelles sont répertoriées, définies et protégées par la Commission " ;<br/>
<br/>
              9. Considérant que, pour l'application de ces dispositions, le règlement (CE) n° 607/2009 de la Commission, du 14 juillet 2009, fixant certaines modalités d'application du règlement (CE) n° 479/2008 du Conseil en ce qui concerne les appellations d'origine protégées et les indications géographiques protégées, les mentions traditionnelles, l'étiquetage et la présentation de certains produits du secteur vitivinicole, a prévu dans ses articles 29 à 48 les modalités selon lesquelles les mentions traditionnelles sont définies, reconnues, protégées et modifiées par la Commission à la demande des autorités compétentes des Etats membres ou des pays tiers ou des organisations professionnelles représentatives établies dans les pays tiers ; qu'en vertu de l'article 40 du règlement, les mentions traditionnelles protégées sont répertoriées et définies dans la base de données électronique E-Bacchus ; que cette base énonce, pour ce qui concerne la mention traditionnelle " vendanges tardives " : " AOP " Alsace ", " Alsace grand cru ", " Jurançon " : expression liée à un type de vin et à une méthode particulière de production, réservée aux vins issus de vendanges surmûries qui respectent des conditions définies de densité et de titre alcoométrique " ;<br/>
<br/>
              10. Considérant, en premier lieu, que l'article 42 bis du règlement (CE) n° 607/2009 de la Commission prévoit que les demandeurs de la protection peuvent solliciter " l'approbation de la modification d'une mention traditionnelle, de la langue indiquée, du ou des vins concernés ou du résumé de la définition ou des conditions d'utilisation de la mention traditionnelle concernée. / Les articles 33 à 39 s'appliquent mutatis mutandis aux demandes de modification " ; <br/>
<br/>
              11. Considérant, d'une part, qu'il résulte de ces dispositions que l'association requérante n'est pas fondée à soutenir que l'énumération des vins bénéficiaires de la mention traditionnelle " vendanges tardives " ne pourrait être modifiée ;<br/>
<br/>
              12. Considérant, d'autre part,  que l'article 35 du même règlement, relatif aux conditions de validité des mentions traditionnelles, prévoit au b) de son paragraphe 1, au nombre des conditions d'acceptation d'une demande de reconnaissance, que la mention qui en fait l'objet doit consister exclusivement, soit en " une dénomination traditionnellement utilisée dans le commerce sur une grande partie du territoire de la Communauté ", soit en " une dénomination réputée, traditionnellement utilisée dans le commerce au moins sur le territoire de l'État membre " ; qu'en vertu du paragraphe 2 du même article, on entend par " utilisation traditionnelle " au sens du paragraphe 1 " une utilisation d'au moins cinq ans dans le cas de mentions apparaissant " dans les langues officielles ou régionales de l'État, ou " d'au moins quinze ans dans le cas de mentions apparaissant " dans une autre langue utilisée dans le commerce pour cette mention ; que, toutefois, si l'article 42 bis du règlement renvoie à l'article 35, il n'en résulte pas que cette condition d'antériorité à laquelle est subordonnée la reconnaissance d'une nouvelle mention traditionnelle dusse s'appliquer à une modification de la liste des vins bénéficiaires d'une telle mention préexistante dès lors que le renvoi à l'article 35 est fait sous la réserve de la locution latine " mutatis mutandis " et que, par construction, les dispositions protectrices des mentions traditionnelles faisant obstacle à ce qu'un vin fasse un usage commercial d'une telle mention avant d'y être autorisé, il ne pourrait remplir de manière licite une condition d'antériorité avant que la liste des vins bénéficiaires de la mention soit modifiée ; qu'ainsi l'application aux demandes de modification des mentions traditionnelles de la condition tenant à l'utilisation préalable d'une mention traditionnelle pendant au moins cinq ans conduirait à priver de leur portée les dispositions du premier alinéa de l'article 42 bis ; qu'il suit de là que le décret attaqué a pu légalement homologuer un cahier des charges conduisant les autorités françaises à demander une modification de la liste des vins bénéficiant de la mention traditionnelle " vendanges tardives " même si le vin concerné ne remplit pas une condition d'usage antérieur de la mention ;<br/>
<br/>
              13. Considérant, en second lieu, qu'il ressort des pièces du dossier que le cahier des charges de l'appellation d'origine contrôlée " Gaillac " subordonne l'utilisation de la mention " vendanges tardives " au respect de conditions de densité des parcelles et de titre alcoométrique équivalentes à celles requises dans les appellations d'origine contrôlée " Alsace ", " Alsace grand cru " et " Jurançon " ; qu'il suit de là que le décret attaqué n'a pas, contrairement à ce que soutient l'association requérante, méconnu les conditions d'utilisation de cette mention relative à la méthode de production utilisée ; que les critères d'appréciation autres que la densité des parcelles et le titre alcoométrique, invoqués par l'association requérante, ne sont pas au nombre de ceux auxquels les dispositions communautaires rappelées plus haut subordonnent la licéité de l'utilisation de la mention traditionnelle " vendanges tardives " ; qu'ainsi le moyen tiré de ce qu'en homologuant le cahier des charges de l'appellation " Gaillac " comportant le bénéfice de la mention traditionnelle " vendanges tardives ", le décret serait entaché d'erreur d'appréciation ne peut qu'être écarté ;<br/>
<br/>
              14. Considérant qu'il résulte de tout ce qui précède que l'association requérante n'est pas fondée à demander l'annulation du décret qu'elle attaque ; <br/>
<br/>
              15. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête présentée par l'Association des viticulteurs d'Alsace est rejetée.<br/>
Article 2 : La présente décision sera notifiée à l'Association des viticulteurs d'Alsace, au Syndicat de défense des vins à appellation d'origine contrôlée " Gaillac ", au Premier ministre, au ministre de l'économie et des finances, au ministre de l'agriculture, de l'agroalimentaire et de la forêt et à l'Institut national de l'origine et de la qualité.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
