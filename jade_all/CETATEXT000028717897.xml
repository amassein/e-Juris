<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028717897</ID>
<ANCIEN_ID>JG_L_2014_03_000000373949</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/71/78/CETATEXT000028717897.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 12/03/2014, 373949, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-03-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>373949</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER</AVOCATS>
<RAPPORTEUR>M. Denis Rapone</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:373949.20140312</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance n° 1307381 du 12 décembre 2013, enregistrée le 13 décembre 2013 au secrétariat du contentieux du Conseil d'Etat, par laquelle la présidente de la 9ème chambre du tribunal administratif de Melun, avant qu'il soit statué sur la demande de M. B... M'A..., tendant, d'une part, à l'annulation de la décision de l'inspecteur du travail du 25 janvier 2013 autorisant son licenciement et de la décision implicite par laquelle le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social a rejeté son recours hiérarchique contre cette décision et, d'autre part, à ce qu'il soit enjoint de le réintégrer au sein de la société Euro Disney, a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution des dispositions de l'article L. 2325-5 du code du travail ;<br/>
<br/>
              Vu le mémoire, enregistré le 4 octobre 2013 au greffe du tribunal administratif de Melun, présenté par M. M'A..., demeurant..., en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule et son article 61-1 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
              Vu le code du travail, notamment son article L. 2325-5 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Denis Rapone, Conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Blancpain, Soltner, avocat de la société Euro Disney Associés ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2. Considérant que M. M'A... soutient que les dispositions du second alinéa de l'article L. 2325-5 du code du travail sont contraires au droit syndical et au droit de tout travailleur de participer par l'intermédiaire de ses délégués à la gestion des entreprises, garantis respectivement par le sixième et par le huitième alinéa du Préambule de la Constitution du 27 octobre 1946, en ce qu'elles ne subordonnent pas la présentation d'une information comme confidentielle à la condition que cette exigence soit légitime et conforme aux droits et libertés des travailleurs et en ce qu'elles confèreraient un pouvoir arbitraire et discrétionnaire à l'employeur ;<br/>
<br/>
              3. Considérant qu'aux termes du sixième alinéa du Préambule de la Constitution du 27 octobre 1946 : " Tout homme peut défendre ses droits et ses intérêts par l'action syndicale et adhérer au syndicat de son choix " ; qu'aux termes du huitième alinéa du même Préambule : " Tout travailleur participe, par l'intermédiaire de ses délégués, à la détermination collective des conditions de travail ainsi qu'à la gestion des entreprises " ;<br/>
<br/>
              4. Considérant qu'aux termes du second alinéa de l'article L. 2325-5 du code du travail : " Les membres du comité d'entreprise et les représentants syndicaux sont tenus à une obligation de discrétion à l'égard des informations revêtant un caractère confidentiel et présentées comme telles par l'employeur " ; qu'en permettant aux membres du comité d'entreprise de bénéficier de toutes les informations utiles à l'exercice de leur mandat et à leur participation à la gestion de l'entreprise, y compris de celles qui ont un caractère confidentiel et exigent, par suite, le respect d'une obligation de discrétion, le législateur a entendu assurer l'effectivité du droit de tout travailleur de participer à la gestion des entreprises, dont le huitième alinéa du Préambule de la Constitution de 1946 prévoit qu'il s'exerce par l'intermédiaire des délégués du travailleur ; qu'en conditionnant, par ailleurs, la possibilité de qualifier une information de confidentielle à la double exigence qu'elle revête effectivement un caractère confidentiel et qu'elle soit présentée comme telle par l'employeur, tout abus dans la qualification opérée par celui-ci étant susceptible d'être soumis au contrôle du juge judiciaire ou à l'appréciation de l'inspecteur du travail, sous le contrôle du juge administratif, en cas de demande par l'employeur d'une autorisation de licenciement d'un membre du comité d'entreprise pour violation de l'obligation de discrétion, le législateur n'a pas méconnu les principes de liberté syndicale et de participation des travailleurs à la gestion de l'entreprise ; qu'il suit de là que la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède qu'il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité transmise par la présidente de la 9ème chambre du tribunal administratif de Melun.<br/>
Article 2 : La présente décision sera notifiée à M. B...M'A..., à la société Euro Disney Associés et au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre, ainsi qu'au tribunal administratif de Melun.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
