<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032491579</ID>
<ANCIEN_ID>JG_L_2016_05_000000371973</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/49/15/CETATEXT000032491579.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 04/05/2016, 371973, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-05-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>371973</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT</AVOCATS>
<RAPPORTEUR>Mme Esther de Moustier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:371973.20160504</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société civile immobilière (SCI) Euro Invest a demandé au tribunal administratif de Cergy-Pontoise de prononcer la réduction des cotisations de taxe foncière sur les propriétés bâties auxquelles elle a été assujettie au titre des années 2008 à 2011 à raison de locaux dont elle est propriétaire dans la commune de Clichy-la-Garenne (Hauts-de-Seine). Par un jugement n°s 1002675, 1106568, 1204094 du 3 juillet 2013, le tribunal administratif a rejeté ces demandes.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 5 septembre et 6 décembre 2013 au secrétariat du contentieux du Conseil d'Etat, la SCI Euro Invest demande au Conseil d'Etat :<br/>
<br/>
              	1°) d'annuler ce jugement ;<br/>
<br/>
              	2°) réglant l'affaire au fond, de faire droit à ses demandes ;<br/>
<br/>
              	3°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le code de la construction et de l'habitation ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Esther de Moustier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Potier de la Varde, Buk Lament, avocat de la SCI Euro Invest ;<br/>
<br/>
<br/>
<br/>
              1. Considérant que la société Euro Invest a demandé au tribunal administratif de Cergy-Pontoise de prononcer la réduction des cotisations de taxe foncière sur les propriétés bâties auxquelles elle a été assujettie au titre des années 2008 à 2011 à raison de locaux dont elle est propriétaire dans la commune de Clichy-la-Garenne (Hauts-de-Seine) ; que, par le jugement attaqué du 3 juillet 2013, le tribunal administratif a rejeté ces demandes ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article 1494 du code général des impôts : " La valeur locative des biens passibles de la taxe foncière sur les propriétés bâties (...) est déterminée, conformément aux règles définies par les articles 1495 à 1508, pour chaque propriété ou fraction de propriété normalement destinée à une utilisation distincte " ; qu'aux termes de l'article 1498 du même code : " La valeur locative de tous les biens autres que les locaux visés au I de l'article 1496 et que les établissements industriels visés à l'article 1499 est déterminée au moyen de l'une des méthodes indiquées ci-après : (...) / 2° a. Pour les biens loués à des conditions de prix anormales ou occupés par leur propriétaire, occupés par un tiers à un autre titre que la location, vacants ou concédés à titre gratuit, la valeur locative est déterminée par comparaison. / Les termes de comparaison sont choisis dans la commune. Ils peuvent être choisis hors de la commune pour procéder à l'évaluation des immeubles d'un caractère particulier ou exceptionnel ; / b. La valeur locative des termes de comparaison est arrêtée : / - soit en partant du bail en cours à la date de référence de la révision lorsque l'immeuble type était loué normalement à cette date ; / - soit, dans le cas contraire, par comparaison avec des immeubles similaires situés dans la commune ou dans une localité présentant, du point de vue économique, une situation analogue à celle de la commune en cause et qui faisaient l'objet à cette date de locations consenties à des conditions de prix normales ; /  3° A défaut de ces bases, la valeur locative est déterminée par voie d'appréciation directe " ; qu'en vertu des dispositions de l'article 324 AA de l'annexe III à ce code : " La valeur locative cadastrale des biens loués à des conditions anormales ou occupés par leur propriétaire, occupés par un tiers à un titre autre que celui de locataire, vacants ou concédés à titre gratuit est obtenue en appliquant aux données relatives à leur consistance - telles que superficie réelle, nombre d'éléments - les valeurs unitaires arrêtées pour le type de la catégorie correspondante (...) " ; qu'en vertu des dispositions de l'article 324 Z de la même annexe : " I. L'évaluation par comparaison consiste à attribuer à un immeuble ou à un local donné une valeur locative proportionnelle à celle qui a été adoptée pour d'autres biens de même nature pris comme types. / II. Les types dont il s'agit doivent correspondre aux catégories dans lesquelles peuvent être rangés les biens de la commune visés aux articles 324 Y à 324 AC, au regard de l'affectation, de la situation, de la nature de la construction, de son importance, de son état d'entretien et de son aménagement. / Ils sont inscrits au procès-verbal des opérations de la révision " ;<br/>
<br/>
              3. Considérant qu'en vertu de l'article R. 122-2 du code de la construction et de l'habitation : " Constitue un immeuble de grande hauteur, pour l'application du présent chapitre, tout corps de bâtiment dont le plancher bas du dernier niveau est situé, par rapport au niveau du sol le plus haut utilisable pour les engins des services publics de secours et de lutte contre l'incendie : / - à plus de 50 mètres pour les immeubles à usage d'habitation, tels qu'ils sont définis par l'article R. 111-1 ; / - à plus de 28 mètres pour tous les autres immeubles (...) " ;<br/>
<br/>
              4. Considérant que, pour l'application de la méthode d'évaluation de la valeur locative des locaux commerciaux et biens divers prévue au 2° de l'article 1498 du code général des impôts, les immeubles de grande hauteur, eu égard à leurs spécificités, ne peuvent être évalués que par comparaison avec d'autres immeubles de grande hauteur ou, à défaut, par voie d'appréciation directe en application du 3° du même article ; qu'à la date du 1er janvier 1970 retenue pour l'évaluation des valeurs locatives cadastrales servant de base à la taxe foncière sur les propriétés bâties, les seuls immeubles que leur hauteur exceptionnelle rendait spécifiques en France, pour l'évaluation de leur valeur locative, se situaient dans le quartier de La Défense en banlieue parisienne et avaient une hauteur de 100 mètres ; qu'en conséquence, doit être regardé comme un immeuble de grande hauteur, pour l'application de la règle mentionnée ci-dessus, un immeuble dont la hauteur est proche de cette hauteur ou lui est supérieure ; qu'il n'y a, en revanche, pas lieu de se référer à la catégorie des immeubles de grande hauteur définie par le code de la construction et de l'habitation, notamment par son article R. 122-2 précité, qui inclut des immeubles qui ne présentent pas, par la nature de leur construction, de spécificité telle, au regard de la loi fiscale, qu'elle empêche la comparaison avec un immeuble n'appartenant pas à cette catégorie ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède qu'en jugeant que la règle rappelée au point 4 devait être appliquée en se référant à l'article R. 122-2 du code de la construction et de l'habitation et en en faisant, par suite, application, dès lors qu'il a jugé que les locaux à évaluer présentaient le caractère d'un immeuble de grande hauteur au sens de ce code, le tribunal administratif a commis une erreur de droit ; <br/>
<br/>
              6. Considérant qu'aux termes de l'article 1494 du code général des impôts : " La valeur locative des biens passibles de la taxe foncière sur les propriétés bâties, de la taxe d'habitation ou d'une taxe annexe établie sur les mêmes bases est déterminée, conformément aux règles définies par les articles 1495 à 1508, pour chaque propriété ou fraction de propriété normalement destinée à une utilisation distincte " ; qu'aux termes de l'article 324 A de l'annexe III au même code : " Pour l'application de l'article 1494 du code général des impôts on entend : / 2° Par fraction de propriété normalement destinée à une utilisation distincte lorsqu'ils sont situés dans un immeuble collectif ou un ensemble immobilier : a. Le local normalement destiné à raison de son agencement à être utilisé par un même occupant ; (...) / Est également considéré comme une fraction de propriété l'ensemble des sols terrains bâtiments et parties de bâtiment réservés à l'usage commun des occupants. L'immeuble collectif s'entend de toute propriété bâtie normalement aménagée pour recevoir au moins deux occupants " ;<br/>
<br/>
              7. Considérant qu'il résulte de ces dispositions que, hors le cas des immeubles de grande hauteur, tels qu'ils sont définis au point 4, les parties d'un ensemble immobilier constituent des " fractions de propriété normalement destinées à une utilisation distincte " au sens de l'article 1494 du code général des impôts lorsqu'elles sont susceptibles de faire l'objet chacune d'une utilisation distincte par un même occupant ; que les aires de stationnement et les parkings d'un immeuble de bureaux doivent être regardés comme des fractions de propriété destinées à une utilisation distincte des bureaux ; qu'à cet égard est sans incidence la circonstance qu'ils fassent ou non l'objet d'une exploitation commerciale autonome ; qu'ainsi, en jugeant que les parkings de l'ensemble immobilier de la société ne pouvaient être regardés comme une fraction de propriété destinée à une utilisation autonome de l'activité de bureaux, dès lors qu'ils ne faisaient l'objet d'aucune exploitation distincte, le tribunal administratif a commis une seconde erreur de droit ;<br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la société Euro Invest est fondée à demander l'annulation du jugement qu'elle attaque ;<br/>
<br/>
              9. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à la société Euro Invest de la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>               D E C I D E :<br/>
                              --------------<br/>
<br/>
Article 1er : Le jugement du 3 juillet 2013 du tribunal administratif de Cergy-Pontoise est annulé. <br/>
<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Cergy-Pontoise.<br/>
<br/>
Article 3 : L'Etat versera à la société Euro Invest la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 4 : La présente décision sera notifiée à la société civile immobilière Euro Invest et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
