<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042494716</ID>
<ANCIEN_ID>JG_L_2020_10_000000445449</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/49/47/CETATEXT000042494716.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 28/10/2020, 445449, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-10-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>445449</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:445449.20201028</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
              Par une requête, enregistrée le 19 octobre 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... B... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'ordonner la suspension de l'exécution du décret n° 2020-1262 du 16 octobre 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire.<br/>
<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - le décret contesté est disproportionné et injustifié dès lors, en premier lieu, que la population n'est pas en danger eu égard au nombre très peu élevé de personne gravement atteinte du covid-19, en deuxième lieu, que les soins de conservations sur le corps des défunts atteints ou probablement atteints du covid-19 au moment de leur décès sont interdits alors même qu'il n'existe aucun risque de contamination, en troisième lieu, que le port du masque ne protège pas contre le virus mais participe à sa propagation et, en dernier lieu, que l'instauration du couvre-feu et les mesures de distanciation sociale ont des conséquences grave sur la santé mentale de la population ;<br/>
              - le décret contesté est entaché d'illégalité en ce qu'il habilite le préfet de département à rendre obligatoire des mesures de lutte contre l'épidémie.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, et notamment son Préambule ;<br/>
              - le code de la santé publique ; <br/>
              - le décret n° 2020-1257 du 14 octobre 2020 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
		     Considérant ce qui suit :<br/>
<br/>
              1. L'article L. 511-1 du code de justice administrative dispose que : " Le juge des référés statue par des mesures qui présentent un caractère provisoire. Il n'est pas saisi du principal et se prononce dans les meilleurs délais ". Aux termes de l'article L. 521-2 du même code : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". Enfin, en vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
<br/>
              Sur le cadre juridique :<br/>
<br/>
              2. L'émergence d'un nouveau coronavirus, responsable de la maladie à coronavirus 2019 ou covid-19 et particulièrement contagieux, a été qualifiée d'urgence de santé publique de portée internationale par l'Organisation mondiale de la santé le 30 janvier 2020, puis de pandémie le 11 mars 2020. La propagation du virus sur le territoire français a conduit le ministre des solidarités et de la santé puis le Premier ministre à prendre, à compter du 4 mars 2020, des mesures de plus en plus strictes destinées à réduire les risques de contagion. Pour faire face à l'aggravation de l'épidémie, le législateur, par l'article 4 de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19, a déclaré l'état d'urgence sanitaire pour une durée de deux mois à compter du 24 mars 2020, puis, par l'article 1er de la loi du 11 mai 2020 prorogeant l'état d'urgence sanitaire et complétant ses dispositions, a prorogé cet état d'urgence sanitaire jusqu'au 10 juillet 2020 inclus. Au vu de l'évolution de la situation sanitaire, les mesures générales adoptées par décret ont assoupli progressivement les sujétions imposées afin de faire face à l'épidémie. Enfin, par un décret du 10 juillet 2020, pris sur le fondement de la loi du 9 juillet 2020 organisant la sortie de l'état d'urgence sanitaire, le Premier ministre a prescrit les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans les territoires sortis de l'état d'urgence sanitaire et dans ceux au sein desquels il a été prorogé.  <br/>
<br/>
              3. Par un décret n° 2020-1257 du 14 octobre 2020, pris en conseil des ministres et sur le rapport du ministre des solidarités et de la santé, le Président de la République a déclaré l'état d'urgence sanitaire sur l'ensemble du territoire de la République à compter du 17 octobre 2020 à 0 heure, sur le fondement des dispositions de l'article L. 3131-13 du code de la santé publique.  <br/>
<br/>
              4. Aux termes de l'article L. 3131-15 du code de la santé publique dispose, dans sa rédaction issue de la loi du 11 mai 2020 : " I. - Dans les circonscriptions territoriales où l'état d'urgence sanitaire est déclaré, le Premier ministre peut, par décret réglementaire pris sur le rapport du ministre chargé de la santé, aux seules fins de garantir la santé publique : (...) 2° Interdire aux personnes de sortir de leur domicile, sous réserve des déplacements strictement indispensables aux besoins familiaux ou de santé ; (...) III. - Les mesures prescrites en application du présent article sont strictement proportionnées aux risques sanitaires encourus et appropriées aux circonstances de temps et de lieu. Il y est mis fin sans délai lorsqu'elles ne sont plus nécessaires. ". <br/>
<br/>
              5. Dans ce cadre, par un décret n° 2020-1262 du 16 octobre 2020, le Premier ministre a prescrit de nouvelles mesures générales nécessaires pour faire face à l'épidémie de covid-19. En particulier, le I de l'article 51 de ce décret prévoit que " I. - Dans les départements mentionnés à l'annexe 2, le préfet de département interdit, dans les zones qu'il définit, aux seules fins de lutter contre la propagation du virus, les déplacements de personnes hors de leur lieu de résidence entre 21 heures et 6 heures du matin à l'exception des déplacements pour les motifs suivants, en évitant tout regroupement de personnes : 1° Déplacements entre le domicile et le lieu d'exercice de l'activité professionnelle ou le lieu d'enseignement et de formation ; 2° Déplacements pour des consultations et soins ne pouvant être assurés à distance et ne pouvant être différés ou pour l'achat de produits de santé ; 3° Déplacements pour motif familial impérieux, pour l'assistance aux personnes vulnérables ou précaires ou pour la garde d'enfants ; 4° Déplacements des personnes en situation de handicap et de leur accompagnant ; 5° Déplacements pour répondre à une convocation judiciaire ou administrative ; 6° Déplacements pour participer à des missions d'intérêt général sur demande de l'autorité administrative ; 7° Déplacements liés à des transferts ou transits vers ou depuis des gares ou aéroports dans le cadre de déplacements de longue distance ; 8° Déplacements brefs, dans un rayon maximal d'un kilomètre autour du domicile pour les besoins des animaux de compagnie. Les personnes souhaitant bénéficier de l'une des exceptions mentionnées au présent I se munissent, lors de leurs déplacements hors de leur domicile, d'un document leur permettant de justifier que le déplacement considéré entre dans le champ de l'une de ces exceptions. Les mesures prises en vertu du présent I ne peuvent faire obstacle à l'exercice d'une activité professionnelle sur la voie publique dont il est justifié dans les conditions prévues à l'alinéa précédent ".  <br/>
<br/>
<br/>
              Sur la demande en référé : <br/>
<br/>
              6. M. B... doit être regardé comme demandant au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'ordonner la suspension de l'exécution du décret n° 2020-1262 du 16 octobre 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire. <br/>
<br/>
              7. Si le requérant soutient que le décret contesté est disproportionné et injustifié pour poursuivre l'objectif de lutte contre l'épidémie covid-19 dès lors, en premier lieu, que la population n'est pas en danger eu égard au nombre très peu élevé de personne gravement atteinte du covid-19, en deuxième lieu, que les soins de conservations sur le corps des défunts atteints ou probablement atteints du covid-19 au moment de leur décès sont interdits alors même qu'il n'existe aucun risque de contamination, en troisième lieu, que le port du masque ne protège pas contre le virus mais participe à sa propagation et, en dernier lieu, que l'instauration d'un couvre-feu et les mesures de distanciation sociale ont des conséquences graves sur la santé mentale de la population, il n'apporte, compte tenu du caractère très général de ses écritures, aucun élément de nature à établir que, au regard à la nette aggravation de la crise sanitaire en France ces dernières semaines, le décret contesté porterait une atteinte grave et manifestement illégale aux libertés fondamentales, dont au demeurant il n'indique même pas lesquelles auraient été méconnues par ces mesures.<br/>
<br/>
              8. Il suit de là que, sans qu'il soit besoin de statuer sur la condition d'urgence, la requête de M. B... doit être rejetée, selon la procédure prévue à l'article L. 522-3 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. B... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. A... B....<br/>
Copie en sera adressée au Premier ministre et au ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
