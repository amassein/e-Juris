<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030646504</ID>
<ANCIEN_ID>JG_L_2015_05_000000385876</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/64/65/CETATEXT000030646504.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème SSJS, 22/05/2015, 385876, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-05-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>385876</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Alain Méar</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:385876.20150522</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une protestation enregistrée sous le n° 1401277, M. D...H...a demandé au tribunal administratif de Toulon d'annuler les opérations électorales qui se sont déroulées les 23 et 30 mars 2014 dans la commune de Roquebrune-sur-Argens (Var) en vue de l'élection des conseillers municipaux et communautaires.<br/>
<br/>
              Par une protestation, enregistrée sous le n° 1401316, M. C...E...a demandé au tribunal administratif de Toulon, à titre principal, de prononcer l'inéligibilité de MM. G...A...et B...F...et de réformer dans cette mesure les résultats du scrutin et, à titre subsidiaire, d'annuler dans leur ensemble les opérations électorales qui se sont déroulées les 23 et 30 mars 2014 dans la commune de Roquebrune-sur-Argens en vue de l'élection des conseillers municipaux et communautaires.<br/>
<br/>
              Par un jugement n° 1401277-1401316 du 17 octobre 2014, le tribunal administratif de Toulon a rejeté ces protestations.<br/>
<br/>
              Par une requête et un mémoire complémentaire, enregistrés les 21 novembre 2014 et 30 avril 2015 au secrétariat du contentieux du Conseil d'Etat, M.E..., agissant en son nom et au nom des élus et candidats de la liste " Ensemble, Roquebrune sortira la tête de l'eau ", demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) d'annuler les opérations électorales en vue de l'élection des conseillers municipaux et des conseillers communautaires qui se sont déroulées les 23 et 30 mars 2014 dans la commune de Roquebrune-sur-Argens ;<br/>
<br/>
              3°) de prononcer l'inéligibilité de M. B...F....<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alain Méar, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              - Vu la note en délibéré, enregistrée le 6 mai 2015, présentée pour M.E....<br/>
<br/>
<br/>
<br/>
<br/>1. Il résulte de l'instruction qu'à l'issue du second tour du scrutin organisé le 30 mars 2014 dans la commune de Roquebrune-sur-Argens (Var) en vue de l'élection des conseillers municipaux et des conseillers communautaires, la liste " Poursuivons ensemble avec Luc F...pour toujours mieux vivre à Roquebrune ", conduite par M. B...F..., maire sortant, a recueilli 3 639 voix, la liste " Ensemble, Roquebrune sortira la tête de l'eau ", conduite par M. C... E..., a recueilli 3 477 voix, la liste " Roquebrune-sur-Argens Rassemblement bleu marine ", conduite par M. D...H..., a obtenu 738 voix et la liste " Roquebrune en grand, Roquebrune ensemble ", conduite par M. G...A..., a obtenu 467 voix. La liste conduite par M. F...s'est vu attribuer 24 sièges sur les 33 sièges à pourvoir au conseil municipal et 6 sièges sur les 7 sièges à pourvoir au conseil communautaire. M. E...relève appel du jugement du 17 octobre 2014 par lequel le tribunal administratif de Toulon a rejeté sa protestation tendant, à titre principal, à ce qu'il constate l'inéligibilité de M. F...et rectifie en conséquence les résultats du scrutin et, à titre subsidiaire, à l'annulation des opérations électorales dans leur ensemble, ainsi que la protestation formée par M. H...tendant à l'annulation de ces opérations électorales.<br/>
<br/>
              Sur les griefs relatifs à l'établissement de la liste électorale :<br/>
<br/>
              2. Aux termes de l'article L. 11 du code électoral : " Sont inscrits sur la liste électorale, sur leur demande : / 1° Tous les électeurs qui ont leur domicile réel dans la commune ou y habitent depuis six mois au moins ; / 2° Ceux qui figurent pour la cinquième fois sans interruption, l'année de la demande d'inscription, au rôle d'une des contributions directes communales et, s'ils ne résident pas dans la commune, ont déclaré vouloir y exercer leurs droits électoraux. Tout électeur ou toute électrice peut être inscrit sur la même liste que son conjoint au titre de la présente disposition ; / 3° Ceux qui sont assujettis à une résidence obligatoire dans la commune en qualité de fonctionnaires publics (...) ".<br/>
<br/>
              3. Si M. E...soutient que plusieurs électeurs ont été inscrits sur la liste électorale au moyen d'une domiciliation fictive 21, place Alfred Perrin, dans des locaux où M. F... aurait précédemment exercé une activité médicale, et produit, en ce qui concerne certains de ces électeurs seulement, des lettres de l'administration fiscale mentionnant que les intéressés ne sont pas inscrits au rôle d'une des contributions directes communales, ces éléments ne sont pas de nature à établir que les électeurs concernés n'auraient pas rempli l'une des deux autres conditions prévues à l'article L. 11 du code électoral et, notamment, qu'ils n'auraient pas eu leur domicile réel dans la commune de Roquebrune-sur-Argens. M. E...n'apporte pas davantage d'éléments de nature à établir que le maintien de M. F...sur la liste électorale de la commune serait le fruit d'une manoeuvre, en se bornant à faire valoir que ce dernier n'avait pas la qualité de contribuable local et à produire un extrait de jugement du tribunal correctionnel de Draguignan, rendu postérieurement au scrutin, mentionnant une adresse dans la commune de Saint-Aygulf. Enfin, M. E... n'apporte pas d'éléments de nature à établir que le maintien sur la liste électorale de personnes non inscrites au rôle de l'une des contributions directes communales, qui n'ont d'ailleurs pas toutes pris part au scrutin et dont le nombre exact est au demeurant indéterminé, serait le fruit d'une manoeuvre de nature à altérer la sincérité du scrutin.<br/>
<br/>
              4. M.E... soulève, en outre, dans son mémoire complémentaire, un grief plus général tiré des " nombreuses irrégularités " dont serait entachée la liste électorale de la commune et un grief particulier tiré de ce qu'une personne aurait voté, au premier tour, sous le nom d'un électeur de la commune décédé à cette date. Ces griefs sont nouveaux en appel et, par suite, irrecevables.<br/>
<br/>
              Sur les griefs tirés d'une violation des articles L. 52-1 et L. 52-8 du code électoral :<br/>
<br/>
              5. Aux termes de l'article L. 52-1 du code électoral : " Pendant les six mois précédant le premier jour du mois d'une élection et jusqu'à la date du tour de scrutin où celle-ci est acquise, l'utilisation à des fins de propagande électorale de tout procédé de publicité commerciale par la voie de la presse ou par tout moyen de communication audiovisuelle est interdite. A compter du premier jour du sixième mois précédant le mois au cours duquel il doit être procédé à des élections générales, aucune campagne de promotion publicitaire des réalisations ou de la gestion d'une collectivité ne peut être organisée sur le territoire des collectivités intéressées par le scrutin. (...) cette interdiction ne s'applique pas à la présentation, par un candidat ou pour son compte, dans le cadre de l'organisation de sa campagne, du bilan de la gestion des mandats qu'il détient ou qu'il a détenus. Les dépenses afférentes sont soumises aux dispositions relatives au financement et au plafonnement des dépenses électorales contenues au chapitre V bis du présent titre ". L'article L. 52-8 du même code dispose que : " Les personnes morales, à l'exception des partis ou groupements politiques, ne peuvent participer au financement de la campagne électorale d'un candidat, ni en lui consentant des dons sous quelque forme que ce soit, ni en lui fournissant des biens, services ou autres avantages directs ou indirects à des prix inférieurs à ceux qui sont habituellement pratiqués ".<br/>
<br/>
              6. Eu égard à la date des opérations électorales litigieuses, il résulte des dispositions de l'article L. 52-1 du code électoral que seules les campagnes de promotion publicitaire réalisées à compter du 1er septembre 2013 doivent être prises en considération pour l'application de ces dispositions.<br/>
<br/>
              7. En premier lieu, si M. E...soutient que M. F...aurait " diffusé très largement " aux habitants de la commune, sur support papier mais aussi par voie électronique et sous forme de vidéo-clip, un bilan de sa gestion et des réalisations de la municipalité au cours de la période 2001-2011, il résulte toutefois de l'instruction que toutes les factures relatives à la conception, la fabrication, l'impression et la diffusion de ce bilan ont été émises et réglées au cours de l'année 2011. Si M. E...établit par ailleurs que, postérieurement au 1er septembre 2013, le bilan pouvait être téléchargé librement par les internautes fréquentant le site Internet de la commune et que certains exemplaires du document étaient à la disposition du public, parmi d'autres documents, dans au moins un service municipal, cette diffusion ne constitue pas, eu égard à son ampleur très limitée, une campagne de promotion publicitaire au sens de l'article L. 52-1 du code électoral.<br/>
<br/>
              8. En deuxième lieu, il résulte de l'instruction que le numéro de février 2014 du bulletin municipal d'informations se borne à traiter en des termes mesurés et non polémiques, alors même qu'un démenti express est apporté à des allégations diffusées dans des tracts électoraux, du choix de la commune de conclure un nouveau contrat de collecte des déchets, sans valoriser de manière excessive l'action de la municipalité et sans excéder l'objet habituel d'une telle publication. <br/>
<br/>
              9. En troisième lieu, l'agenda de l'année 2014 distribué aux habitants de la commune, s'il tend à présenter sous un jour globalement favorable l'action de la municipalité, n'a pas constitué, eu égard à son contenu dépourvu d'esprit polémique et à la circonstance qu'il est diffusé chaque année depuis 2001, l'utilisation à des fins de propagande électorale d'un procédé de publicité commerciale.<br/>
<br/>
              10. Enfin, si M. E...soutient que plusieurs photographies insérées dans deux documents électoraux diffusés par la liste conduite par M. F... auraient été réalisées par les services de la commune de Roquebrune-sur-Argens, aux frais de cette dernière, il n'apporte pas d'élément de nature à l'établir en se bornant à observer certaines similitudes avec des photographies figurant dans des bulletins municipaux ou dans le bilan 2001-2011 précité.<br/>
<br/>
              En ce qui concerne les autres griefs : <br/>
<br/>
              11. Si M. E...soutient, enfin, que M. F...aurait accompli une manoeuvre portant atteinte à la sincérité du scrutin, d'une part, en diffusant dans une circulaire électorale un droit de réponse à un article de l'hebdomadaire " L'Express " ayant mis en cause sa gestion en tant que maire et, d'autre part, en adressant au cours du mois de février 2014 à différents groupes d'électeurs des lettres signées en sa qualité de maire de Roquebrune-sur-Argens et de vice-président de la communauté d'agglomération Var-Estérel-Méditerranée, les invitant à voter pour la liste qu'il conduisait lors des élections municipales et communautaires de mars 2014, il résulte de l'instruction que les électeurs n'ont pu être induits en erreur par ces documents dont les mentions indiquaient clairement qu'ils émanaient de M. F...en tant que candidat aux élections municipales et communautaires.<br/>
<br/>
              12. Il résulte de tout ce qui précède que M. E...n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Toulon a rejeté sa protestation.<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              13. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de M. E... une somme à verser à M. A... et à M. F...au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. E...est rejetée.<br/>
Article 2 : Les conclusions présentées par MM. A...et F...et autres au titre de l'article <br/>
L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. C...E..., à M. B...F..., à M. G...A..., à M. D...H...et au ministre de l'intérieur. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
