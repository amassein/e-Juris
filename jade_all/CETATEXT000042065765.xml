<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042065765</ID>
<ANCIEN_ID>JG_L_2020_06_000000426546</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/06/57/CETATEXT000042065765.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 29/06/2020, 426546, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-06-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>426546</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER, PINATEL ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>Mme Catherine Moreau</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Stéphane Hoynck</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:426546.20200629</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              La société Innoclair a demandé au tribunal administratif d'Orléans d'annuler pour excès de pouvoir la délibération adoptée le 30 octobre 2014 par le conseil d'administration de l'Agence de l'eau Loire-Bretagne (AELB) et la décision du 16 décembre 2015 portant refus d'abroger cette décision. Par un jugement n° 1600461 du 4 juillet 2017, le tribunal administratif a annulé cette délibération et cette décision.<br/>
<br/>
              Par un arrêt n° 17NT02712 du 22 octobre 2018, la cour administrative d'appel de Nantes a, sur appel de l'Agence de l'eau Loire-Bretagne, annulé ce jugement et rejeté la demande présentée par la société Innoclair devant le tribunal administratif d'Orléans.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 24 décembre 2018 et le 1er mars 2019 au secrétariat du contentieux du Conseil d'Etat, la société Innoclair demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Agence de l'eau Loire-Bretagne la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le règlement communautaire n° 305/2011 du 9 mars 2011 ;<br/>
              - le code de l'environnement ;<br/>
              - l'arrêté du 7 septembre 2009 modifié du ministre chargé de l'environnement fixant les prescriptions techniques applicables aux installations d'assainissement non collectif recevant une charge brute de pollution organique inférieure ou égale à 1,2 kg/j de DBOS ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 modifiée ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Catherine Moreau, conseiller d'Etat en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Stéphane Hoynck, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de la société Innoclair et à la SCP Waquet, Farge, Hazan, avocat de l'Agence de l'eau Loire-Bretagne ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par une délibération du 30 octobre 2014, le conseil d'administration de l'Agence de l'eau Loire-Bretagne (AELB) a modifié la " fiche action " 1-2-c1 de son programme pluriannuel d'intervention approuvé pour la période 2013-2018, portant sur les conditions d'éligibilité aux aides relatives aux études de sol et de filières d'assainissement non collectif réalisées soit préalablement à des réhabilitations de ces installations dans le cadre d'un service public d'assainissement non collectif, soit à l'occasion de réhabilitations d'habitations neuves. Il a ensuite confirmé cette modification par une délibération du 29 octobre 2015. Saisi par la société Innoclair, spécialisée dans la recherche, le développement, la fabrication, la commercialisation et le service après-vente de micro stations d'assainissement et détentrice d'un agrément ministériel, le tribunal administratif d'Orléans a annulé la délibération du 30 octobre 2014 en tant qu'elle modifie la " fiche action " mentionnée ci-dessus. Par un arrêt du 22 octobre 2018, la cour administrative d'appel de Nantes a, sur appel de l'AELB, annulé ce jugement et a rejeté les demandes de la société Innoclair.<br/>
<br/>
              2. En premier lieu, aux termes de l'article R. 711-3 du code de justice administrative : " Si le jugement de l'affaire doit intervenir après le prononcé de conclusions du rapporteur public, les parties ou leurs mandataires sont mis en mesure de connaître, avant la tenue de l'audience, le sens de ces conclusions sur l'affaire qui les concerne ". La communication aux parties du sens des conclusions, prévue par ces dispositions, a ainsi pour objet de mettre les parties en mesure d'apprécier l'opportunité d'assister à l'audience publique, de préparer, le cas échéant, les observations orales qu'elles peuvent y présenter, après les conclusions du rapporteur public, à l'appui de leur argumentation écrite et d'envisager, si elles l'estiment utile, la production, après la séance publique, d'une note en délibéré. Il en résulte qu'à peine d'irrégularité de la décision, les parties ou leurs mandataires doivent être mis en mesure de connaître, dans un délai raisonnable avant l'audience, l'ensemble des éléments du dispositif de la décision que le rapporteur public compte proposer à la formation de jugement d'adopter, à l'exception de la réponse aux conclusions qui revêtent un caractère accessoire, notamment celles qui sont relatives à l'application de l'article L. 761-1 du code de justice administrative et que le rapporteur public qui, après avoir communiqué le sens de ses conclusions, envisage de modifier sa position doit mettre les parties à même de connaître ce changement.<br/>
<br/>
              3. Il ressort des pièces de la procédure devant la cour administrative d'appel, notamment du relevé de l'application " Sagace ", que le rapporteur public ayant modifié le sens de ses conclusions la veille de l'audience, cette information a été portée à la connaissance des parties par un courrier dématérialisé le 4 octobre 2018 à 12h34, dont l'avocate de la société Innoclair a accusé réception le même jour à 13h36, pour une audience fixée le lendemain à 9h15. La société Innoclair peut, dans les circonstances de l'espèce, être regardée comme ayant été mise en mesure de connaître, dans un délai suffisant avant l'audience, la modification du sens des conclusions du rapporteur public. Par suite, le moyen tiré de ce que l'arrêt aurait été rendu au terme d'une procédure irrégulière doit être écarté. <br/>
<br/>
              4. En deuxième lieu, aux termes de l'article L. 213-8-1 du code de l'environnement dans sa rédaction alors applicable, les agences de l'eau ont pour mission de " favoris[er] une gestion équilibrée et économe de la ressource en eau et des milieux aquatiques, l'alimentation en eau potable, la régulation des crues et le développement durable des activités économiques ".  Aux termes de l'article L. 213-9-1 du même code alors applicable : " Pour l'exercice des missions définies à l'article L. 213-8-1, le programme pluriannuel d'intervention de chaque agence de l'eau détermine les domaines et les conditions de son action et prévoit le montant des dépenses et des recettes nécessaires à sa mise en oeuvre. Le Parlement définit les orientations prioritaires du programme pluriannuel d'intervention des agences de l'eau et fixe le plafond global de leurs dépenses sur la période considérée ainsi que celui des contributions des agences à l'Office national de l'eau et des milieux aquatiques (...) ". Cet article est complété par l'article L. 213-9-2 du même code, aux termes duquel : " I.- Dans le cadre de son programme pluriannuel d'intervention, l'agence de l'eau apporte directement ou indirectement des concours financiers sous forme de subventions, de primes de résultat ou d'avances remboursables aux personnes publiques ou privées pour la réalisation d'actions ou de travaux d'intérêt commun au bassin ou au groupement de bassins qui contribuent à la gestion équilibrée de la ressource en eau et des milieux aquatiques. (...) ". Enfin, les conditions d'attribution de concours financiers sont précisées par les dispositions, d'une part, de l'article R. 213-32 du même code, en vertu duquel : " I.- Pour l'exercice de ses missions définies aux articles L. 213-8-1 et L. 213-9-2 : 1° L'agence peut attribuer des subventions, des primes de résultat et consentir des avances remboursables aux personnes publiques ou privées, dans la mesure où les études, recherches, travaux ou ouvrages exécutés par ces personnes et leur exploitation entrent dans le cadre de ses attributions. Elle s'assure de la bonne utilisation et de l'efficacité des aides versées (...) ", d'autre part, de l'article R. 213-39 du même code, en vertu duquel le conseil d'administration de l'agence délibère sur " 2° Les programmes généraux d'activité, et notamment les programmes pluriannuels d'intervention prévus à l'article L. 213-9-1 (...) ; / 7° Les conditions générales d'attribution des subventions et des concours financiers aux personnes publiques et privées (...) ". <br/>
<br/>
              5. Il résulte de ces dispositions que les agences de l'eau disposent d'un pouvoir règlementaire pour déterminer, dans la limite des missions qui leur sont fixées par la loi, les domaines et conditions de leur action et définir les conditions générales d'attribution des concours financiers qu'elles peuvent apporter aux personnes publiques et privées sous forme de subventions, primes de résultat ou avances remboursables. Cette compétence doit être exercée, en vertu de l'article R. 213-39 du code de l'environnement cité ci-dessus, par leur conseil d'administration. <br/>
<br/>
              6. Après avoir estimé, par des appréciations souveraines non contestées en cassation, que le dispositif mis en place par l'AELB par la délibération du 30 octobre 2014 impose, au nombre des conditions d'éligibilité des personnes publiques ou privées aux aides relatives aux études de sol et de filières d'assainissement non collectif, la réalisation de ces études conformément à un cahier des charges type qui, en son point 4, exige que soit étudiée en priorité la solution d'une installation d'assainissement non collectif traditionnelle, dite " par le sol ", la possibilité d'un assainissement par un dispositif alternatif agréé n'étant étudiée qu'au cas où il est justifié qu'un dispositif traditionnel n'est pas techniquement réalisable, la cour a pu en déduire, sans commettre d'erreur de droit, que le conseil d'administration de l'AELB, en adoptant cette délibération, n'avait pas excédé sa compétence.<br/>
<br/>
              7. En troisième lieu, il appartient au juge de l'excès de pouvoir de former sa conviction sur les points en litige au vu des éléments versés au dossier par les parties. S'il peut écarter des allégations qu'il jugerait insuffisamment étayées, il ne saurait exiger de l'auteur du recours que ce dernier apporte la preuve des faits qu'il avance. Le cas échéant, il revient au juge, avant de se prononcer sur une requête assortie d'allégations sérieuses non démenties par les éléments produits par l'administration en défense, de mettre en oeuvre ses pouvoirs généraux d'instruction et de prendre toutes mesures propres à lui procurer, par les voies de droit, les éléments de nature à lui permettre de former sa conviction, en particulier en exigeant de l'administration compétente la production de tout document susceptible de permettre de vérifier les allégations du demandeur. Il ressort des pièces du dossier soumis au juge du fond que la société requérante s'était bornée devant lui à une affirmation selon laquelle il n'était aucunement acquis que les membres du conseil d'administration aient été régulièrement convoqués. En relevant que ce moyen n'était assorti d'aucune précision suffisante, la cour ne s'est pas méprise sur les écritures de la requérante et n'a pas méconnu son office. <br/>
<br/>
              8. En quatrième lieu,  en relevant que la délibération contestée se borne à fixer les conditions dans lesquelles sont menées les études préalables à l'installation d'un assainissement non collectif et n'a pour objet ni pour effet de s'opposer à l'installation des dispositifs de traitement agréés et bénéficiant donc d'un marquage CE au sens du point 4 de l'article 7 du règlement (UE) n° 305/2011 du Parlement européen et du Conseil du 9 mars 2011 établissant des conditions harmonisées de commercialisation pour les produits de construction et abrogeant la directive 89/106/CEE du Conseil, la cour n'a pas commis d'erreur de droit, ni insuffisamment motivé son arrêt. <br/>
<br/>
              9. En cinquième lieu, le cahier des charges type faisant partie du dispositif mis en place par la délibération du 30 octobre 2014 a pour seul objet de fixer le cadre dans lequel l'AELB financera les études diagnostiques préalables à la réhabilitation des dispositifs d'assainissement non collectif. Par suite, le moyen tiré de ce que la cour aurait commis une erreur de droit en se fondant sur des motifs inopérants pour écarter le moyen tiré de ce que le cahier des charges type méconnaitrait l'article 6 de l'arrêté ministériel du 7 septembre 2009 ne peut qu'être écarté.  <br/>
<br/>
              10. Il résulte de tout ce qui précède que le pourvoi de la société Innoclair doit être rejeté. <br/>
<br/>
              11. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'AELB qui n'est pas, dans la présente instance, la partie perdante. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions de l'AELB présentées au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société Innoclair est rejeté.<br/>
Article 2 : Les conclusions présentées par l'Agence de l'eau Loire-Bretagne au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à la société Innoclair et à l'Agence de l'eau Loire-Bretagne. <br/>
Copie en sera adressée à la ministre de la transition écologique et solidaire.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
