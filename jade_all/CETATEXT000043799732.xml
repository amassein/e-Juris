<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043799732</ID>
<ANCIEN_ID>JG_L_2021_07_000000437391</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/79/97/CETATEXT000043799732.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 09/07/2021, 437391, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>437391</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>Mme Ophélie Champeaux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:437391.20210709</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... C... a demandé au tribunal administratif de Paris d'annuler les décisions du ministre de la culture rejetant ses demandes tendant à la liquidation et au paiement des dix-huit jours déposés sur son compte épargne temps et des congés annuels qu'il n'avait pas été en mesure de prendre en raison d'un congé maladie à compter de l'année 2011. Mme B..., venant aux droits de son époux M. C... décédé en cours d'instance, a demandé à ce tribunal d'annuler l'arrêté du 7 août 2018 portant indemnisation des jours de congés annuels non pris au titre des années 2014, 2015 et 2016, en tant qu'il ne fait que partiellement droit à la demande de son époux. Par un jugement nos 1700069, 170070, 1817921 du 6 novembre 2019, ce tribunal a prononcé un non-lieu à statuer à concurrence de la somme de 2 804,4 euros et rejeté le surplus des demandes.<br/>
<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 6 janvier et 7 avril 2020 au secrétariat du Conseil d'Etat, Mme B... demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler l'article 3 de ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond dans cette mesure, de faire droit à ses demandes ;<br/>
<br/>
              3°) de mettre à la charge de l'État la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la directive 2003/88/CE du Parlement européen et du Conseil du 4 novembre 2003 relative à certains aspects de l'aménagement du temps de travail ;<br/>
              - le décret n° 84-972 du 26 octobre 1984 relatif aux congés annuels des fonctionnaires de l'Etat ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Ophélie Champeaux, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat de Mme B... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article R. 811-1 du code de justice administrative, dans sa rédaction alors applicable : " Toute partie présente dans une instance devant le tribunal administratif ou qui y a été régulièrement appelée (...) peut interjeter appel contre toute décision juridictionnelle rendue dans cette instance. / Toutefois, le tribunal administratif statue en premier et dernier ressort : / (...) 8° Sauf en matière de contrat de la commande publique sur toute action indemnitaire ne relevant pas des dispositions précédentes, lorsque le montant des indemnités demandées est inférieur au montant déterminé par les articles R. 222-14 et R. 222-15 ; / (...) / Par dérogation aux dispositions qui précèdent, en cas de connexité avec un litige susceptible d'appel, les décisions portant sur les actions mentionnées au 8° peuvent elles-mêmes faire l'objet d'un appel (...) ". En vertu de l'article R. 222-14 de ce code, le montant des indemnités visées par le 8° de l'article R. 811-1, déterminé conformément à ce que prévoit l'article R. 222-15, est fixé à 10 000 euros. <br/>
<br/>
              2. La demande d'un fonctionnaire ou d'un agent public tendant seulement au versement de traitements, rémunérations, indemnités, avantages ou soldes impayés, sans chercher la réparation d'un préjudice distinct du préjudice matériel objet de cette demande pécuniaire, ne revêt pas le caractère d'une action indemnitaire au sens du 8° de l'article R. 811-1 du code de justice administrative. Par suite, une telle demande n'entre pas, quelle que soit l'étendue des obligations qui pèseraient sur l'administration au cas où il y serait fait droit, dans le champ de l'exception, prévue à ce 8°, en vertu de laquelle le tribunal administratif statue en dernier ressort. <br/>
<br/>
              3. Il ressort des termes des requêtes soumises au tribunal administratif qu'elles tendaient, d'une part, à l'indemnisation des jours de congés non pris par M. C... au titre des années 2012 à 2016 et des jours attribués au titre de la réduction du temps de travail et inscrits sur son compte épargne temps à la date de sa radiation des cadres et, d'autre part, à l'annulation de l'arrêté du 7 août 2018 de la ministre de la culture, en tant qu'il n'a fait que partiellement droit à cette demande. Elles ne visaient ainsi pas la réparation d'un préjudice distinct du préjudice matériel objet de cette demande pécuniaire. <br/>
<br/>
              4. Il en résulte de que la requête de Mme B... a le caractère d'un appel qui ne ressortit pas à la compétence du Conseil d'Etat, juge de cassation, mais à celle de la cour administrative d'appel de Paris. Il y a lieu, dès lors, d'en attribuer le jugement à cette cour.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement de la requête de Mme B... est attribué à la cour administrative d'appel de Paris<br/>
Article 2 : La présente décision sera notifiée à Mme D... B... venant aux droits de M. A... C..., à la ministre de la culture et au président de la cour administrative d'appel de Paris.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
