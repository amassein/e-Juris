<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034900566</ID>
<ANCIEN_ID>JG_L_2017_06_000000410913</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/90/05/CETATEXT000034900566.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 01/06/2017, 410913, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-06-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>410913</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2017:410913.20170601</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
         Par une requête, enregistrée le 28 mai 2017 au secrétariat du contentieux du Conseil d'Etat, M.B... A... demande au juge des référés du Conseil d'Etat, sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
         1°) de suspendre l'exécution de la décision par laquelle le vice-président du Conseil d'Etat a adopté la charte de déontologie de la juridiction administrative datée du 14 mars 2017, plus particulièrement du dernier alinéa de son  paragraphe 16 ; <br/>
<br/>
         2°) d'ordonner que la mesure de suspension soit mentionnée sur les sites internet et intranet du Conseil d'Etat en marge de la disposition suspendue tant que celle-ci n'aura pas été retirée ou annulée ; <br/>
<br/>
         3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de des articles L. 761-1 et R. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
         Il soutient que :<br/>
         - la condition d'urgence est remplie dès lors que le dernier alinéa du paragraphe 16 de la charte de déontologie litigieuse porte atteinte à sa liberté d'entreprendre, fait obstacle à l'exercice de sa mission d'avocat et méconnaît le principe du libre choix de l'avocat ; <br/>
         - la décision litigieuse a été prise sur le fondement de l'article L 131-4 du code de justice administrative, lequel méconnaît l'article 16 de la Déclaration des droits de l'homme et du citoyen ;<br/>
<br/>
         - le dernier alinéa du paragraphe 16 de la charte de déontologie de la juridiction administrative porte une atteinte au principe de libre choix de l'avocat ;  <br/>
         - la décision litigieuse méconnaît le droit à un recours effectif et le droit à un tribunal impartial garantis par les articles 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et  47 de la Charte des droits fondamentaux de l'Union européenne ;<br/>
         - elle méconnaît l'article 41 de la Charte des droits fondamentaux de l'Union européenne ;<br/>
         - le vice-président du Conseil d'Etat n'est compétent ni pour réglementer l'activité des anciens membres de la juridiction administrative ni pour réglementer l'exercice de la profession d'avocat ; <br/>
         - le dernier alinéa du paragraphe 16 de la charte de déontologie de la juridiction administrative méconnaît l'article 5 de la loi du 31 décembre 1971.<br/>
<br/>
<br/>
<br/>
         Vu les autres pièces du dossier ;<br/>
<br/>
         Vu :<br/>
- la Constitution, notamment son Préambule ;<br/>
	                        - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
- la Charte des droits fondamentaux de l'Union européenne ;<br/>
- le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
         1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée. <br/>
<br/>
         2. M. A...demande au juge des référés du Conseil d'Etat, sur le fondement de l'article L 521-2 du code de justice administrative, de suspendre l'exécution de la décision par laquelle le vice-président du Conseil d'Etat a adopté la charte de déontologie de la juridiction administrative datée du 14 mars 2017, en critiquant plus particulièrement les dispositions du dernier alinéa de son point 16.  <br/>
<br/>
         3. Ces dispositions prévoient, au titre des " bonnes pratiques " destinées à garantir les principes fondamentaux d'indépendance et d'impartialité des membres de la juridiction administrative rappelés au point 8 de la charte, qu'il convient, pour les anciens chefs de juridiction devenus avocats, de s'abstenir de présenter des requêtes ou mémoires ou de paraître à l'audience devant la juridiction qu'ils ont présidée, pendant une durée de dix ans à compter de la fin de leur présidence .<br/>
<br/>
         4. Si M.A..., qui a présidé le tribunal administratif de Nice jusqu'au 8 juillet 2008 et est inscrit au Barreau de cette ville depuis le 15 septembre 2016, soutient que la " bonne pratique " mentionnée ci-dessus porte atteinte à sa liberté d'entreprendre, fait obstacle à l'exercice de sa mission d'avocat et méconnaît le principe du libre choix de l'avocat, il ne saurait être regardé comme caractérisant ainsi une situation d'urgence qui, sous réserve que les autres conditions posées par les dispositions précitées de l'article L 521-2 du code de justice administrative soient remplies, justifierait que la juge des référés fasse usage à très bref délai des pouvoirs qu'il tient de ces dispositions.<br/>
<br/>
         5. Il résulte de ce qui précède que la requête de M.A..., y compris sa demande d'injonction et ses conclusions présentées au titre des articles L 761-1 et R 761-1 du code de justice administrative, doit être rejetée selon la procédure prévue par l'article L. 522-3 du même code.<br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. A...est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M.B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
