<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039420587</ID>
<ANCIEN_ID>JG_L_2019_11_000000418025</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/42/05/CETATEXT000039420587.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 25/11/2019, 418025, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-11-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418025</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELVOLVE ET TRICHET ; SCP POTIER DE LA VARDE, BUK LAMENT, ROBILLOT</AVOCATS>
<RAPPORTEUR>Mme Tiphaine Pinault</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:418025.20191125</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au tribunal administratif de Toulouse d'annuler pour excès de pouvoir la décision du 25 octobre 2012 par laquelle l'inspectrice du travail de la 1ère section de l'unité territoriale de la Haute-Garonne a autorisé la société Continental Automotive France à le licencier. Par un jugement n° 12005476 du 9 avril 2015, le tribunal administratif a annulé cette décision.<br/>
<br/>
              Par un arrêt n° 15BX01957 du 11 décembre 2017, la cour administrative d'appel de Bordeaux a, sur appel de la société Continental Automotive France, annulé ce jugement et rejeté la demande de M. A....<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 9 février et 9 mai 2018 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la société Continental Automotive France ;<br/>
<br/>
              3°) de mettre à la charge la société Continental Automotive France la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Tiphaine Pinault, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delvolvé et Trichet, avocat de M. A... et à la SCP Potier de la Varde, Buk Lament, Robillot, avocat de la société Continental Automotive France.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'après le rejet, le 20 juin 2012, d'une demande de licenciement pour faute de M. A..., salarié protégé, la société Continental Automotive France a sollicité l'autorisation de licencier ce même salarié pour insuffisance professionnelle. Saisie d'un recours gracieux formé contre sa décision du 13 septembre 2012 rejetant cette seconde demande, l'inspectrice du travail de la 1ère section de l'unité territoriale de la Haute-Garonne a autorisé la société Continental Automotive France à procéder à ce licenciement par une décision du 25 octobre 2012. Par un jugement du 9 avril 2015, le tribunal administratif de Toulouse a annulé pour excès de pouvoir cette décision. M. A... se pourvoit en cassation contre l'arrêt du 11 décembre 2017 par lequel la cour administrative d'appel de Bordeaux a annulé ce jugement et rejeté sa demande.<br/>
<br/>
              2. En vertu des dispositions du code du travail, les salariés légalement investis de fonctions représentatives bénéficient, dans l'intérêt de l'ensemble des salariés qu'ils représentent, d'une protection exceptionnelle. Lorsque le licenciement de l'un de ces salariés est envisagé, ce licenciement ne doit pas être en rapport avec les fonctions représentatives normalement exercées ou l'appartenance syndicale de l'intéressé. Dans le cas où la demande de licenciement est motivée par l'insuffisance professionnelle, il appartient à l'inspecteur du travail et le cas échéant au ministre, de rechercher, sous le contrôle du juge de l'excès de pouvoir, si cette insuffisance est telle qu'elle justifie le licenciement, compte tenu de l'ensemble des règles applicables au contrat de travail de l'intéressé, des caractéristiques de l'emploi exercé à la date à laquelle elle est constatée, des exigences propres à l'exécution normale du mandat dont il est investi, et de la possibilité d'assurer son reclassement dans l'entreprise. En outre, pour refuser l'autorisation sollicitée, l'autorité administrative a la faculté de retenir des motifs d'intérêt général relevant de son pouvoir d'appréciation de l'opportunité, sous réserve qu'une atteinte excessive ne soit pas portée à l'un ou l'autre des intérêts en présence. A l'effet de concourir à la mise en oeuvre de la protection ainsi instituée, les articles R. 2421-4 et R. 2121-11 du code du travail disposent que l'inspecteur du travail, saisi d'une demande d'autorisation de licenciement d'un salarié protégé, " procède à une enquête contradictoire au cours de laquelle le salarié peut, sur sa demande, se faire assister d'un représentant de son syndicat ".<br/>
<br/>
              3. Le caractère contradictoire de l'enquête menée conformément aux dispositions du code du travail qui viennent d'être citées impose à l'autorité administrative, saisie d'une demande d'autorisation de licenciement d'un salarié protégé fondée sur un motif d'insuffisance professionnelle, d'informer le salarié concerné des agissements qui lui sont reprochés et de l'identité des personnes qui en ont témoigné. Il implique, en outre, que le salarié protégé soit mis à même de prendre connaissance de l'ensemble des pièces produites par l'employeur à l'appui de sa demande, dans des conditions et des délais lui permettant de présenter utilement sa défense, sans que la circonstance que le salarié soit susceptible de connaître le contenu de certaines de ces pièces puisse exonérer l'inspecteur du travail de cette obligation. Ce n'est que lorsque l'accès à certains de ces éléments serait de nature à porter gravement préjudice à leurs auteurs que l'inspecteur du travail doit se limiter à informer le salarié protégé, de façon suffisamment circonstanciée, de leur teneur. <br/>
<br/>
              4. Il ressort des termes mêmes de l'arrêt attaqué que, pour juger que le caractère contradictoire de l'enquête conduite par l'inspectrice du travail n'avait pas été méconnu, la cour administrative d'appel de Bordeaux s'est fondée sur la circonstance que M. A... avait été informé par l'inspectrice du travail de l'ensemble des éléments déterminants recueillis lors de cette enquête, sans rechercher si, ainsi qu'il était discuté devant elle, il avait été mis à même de prendre connaissance de l'ensemble des pièces produites par l'employeur à l'appui de sa demande d'autorisation de licenciement et ce, alors même qu'il était susceptible de connaître le contenu de certaines de ces pièces. La cour a ainsi entaché son arrêt d'une erreur de droit. M. A... est, par suite, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, fondé à en demander l'annulation.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Continental Automotive France la somme de 3 000 euros à verser à M. A... au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font obstacle à ce que soit mise à la charge de M. A..., qui dans la présente instance n'est pas la partie perdante, la somme demandée par la société Continental Automotive France au titre de ses frais exposés et non compris dans les dépens.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Bordeaux du 11 décembre 2017 est annulé.<br/>
Article 2 : L'affaire est renvoyée devant la cour administrative d'appel de Bordeaux.<br/>
Article 3 : La société Continental Automotive France versera la somme de 3 000 euros à M. A... au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par la société Continental Automotive France au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à M. B... A... et à la société Continental Automotive France.<br/>
Copie en sera adressée à la ministre du travail.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
