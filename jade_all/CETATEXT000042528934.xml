<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042528934</ID>
<ANCIEN_ID>JG_L_2020_11_000000433370</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/52/89/CETATEXT000042528934.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 16/11/2020, 433370, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-11-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>433370</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE ; LE PRADO</AVOCATS>
<RAPPORTEUR>Mme Bénédicte Fauvarque-Cosson</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:433370.20201116</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'association syndicale libre Giroval Sud a demandé au tribunal administratif de Nice d'annuler pour excès de pouvoir l'arrêté du 26 octobre 2012 par lequel le maire de Roquefort-les-Pins s'est opposé à la déclaration préalable qu'elle avait déposée le 1er août 2012 et complétée le 10 septembre 2012 en vue de l'installation de deux portails destinés à fermer le lotissement qu'elle gère. Par un jugement n° 1301249 du 15 juin 2017, le tribunal administratif de Nice a fait droit à cette demande.<br/>
<br/>
              Par un arrêt n° 17MA03552 du 6 juin 2019, la cour administrative d'appel de Marseille a rejeté l'appel formé par la commune de Roquefort-les-Pins contre ce jugement et enjoint au maire de la commune de prendre une décision de non-opposition à cette déclaration préalable dans un délai de deux mois à compter de la notification de son arrêt.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 6 août et 6 novembre 2019 au secrétariat du contentieux du Conseil d'Etat, la commune de Roquefort-les-Pins demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'association syndicale libre Giroval Sud la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'environnement ;<br/>
              - le code de l'urbanisme ;<br/>
              - la loi n° 2015-990 du 6 août 2015 et notamment son article 108 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme A... B..., conseillère d'Etat,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat de la commune de Roquefort-les-Pins ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que l'association syndicale libre Giroval Sud, chargée de gérer le lotissement du même nom situé sur le territoire de la commune de Roquefort-les-Pins, a déposé le 1er août 2012 et complété le 10 septembre 2012 une déclaration préalable portant sur la pose de deux portails permettant l'accès à ce lotissement, situé chemin des trois feuillets. Le maire de Roquefort-les-Pins s'est opposé à cette déclaration préalable par un arrêté du 26 octobre 2012, en se fondant sur la circonstance que la fermeture de ce lotissement avait recueilli un avis défavorable du service départemental d'incendie et de secours selon lequel il réduirait la défense contre les incendies de forêt du secteur. Par un jugement du 15 juin 2017, le tribunal administratif de Nice, saisi par l'association syndicale libre Giroval Sud, a annulé cet arrêté au motif qu'il était insuffisamment motivé. Après avoir procédé au réexamen de cette déclaration préalable, la commune de Roquefort-les-Pins a renouvelé son opposition par une décision du 29 août 2017. Par un arrêt du 6 juin 2019, la cour administrative d'appel de Marseille a rejeté l'appel formé par la commune contre le jugement du tribunal administratif de Nice, en jugeant que si l'arrêté du 26 octobre 2012 était suffisamment motivé, il reposait toutefois sur des motifs, tirés des risques pour la sécurité incendie et de l'absence de construction d'un local pour la collecte des ordures ménagères en bordure de voie publique, tous deux entachés d'illégalité. Sur l'appel incident de l'association syndicale libre, elle a enjoint au maire de prendre une décision de non-opposition à la déclaration préalable dans un délai de deux mois. <br/>
<br/>
              Sur l'arrêt attaqué, en tant qu'il statue sur la légalité de l'arrêté du 26 octobre 2012 :<br/>
<br/>
              2. Aux termes de l'article R. 111-2 du code de l'urbanisme, dans sa rédaction en vigueur à la date de la décision contestée : " Le projet peut être refusé ou n'être accepté que sous réserve de l'observation de prescriptions spéciales s'il est de nature à porter atteinte à la salubrité ou à la sécurité publique du fait de sa situation, de ses caractéristiques, de son importance ou de son implantation à proximité d'autres installations ". Aux termes de l'article L. 562-4 du code de l'environnement, dans sa rédaction alors en vigueur : " Le plan de prévention des risques naturels prévisibles approuvé vaut servitude d'utilité publique. Il est annexé au plan d'occupation des sols, conformément à l'article L. 126-1 du code de l'urbanisme (...) ".<br/>
<br/>
              3. Les prescriptions d'un plan de prévention des risques naturels prévisibles, destinées notamment à assurer la sécurité des personnes et des biens exposés à certains risques naturels et valant servitude d'utilité publique, s'imposent directement aux déclarations préalables à la réalisation de travaux, sans que l'autorité administrative soit tenue de les reprendre dans une décision de non-opposition. L'autorité compétente pour s'opposer à la déclaration préalable peut aussi, si elle estime, au vu d'une appréciation concrète de l'ensemble des caractéristiques de la situation d'espèce qui lui est soumise et du projet pour lequel la déclaration préalable est déposée, y compris d'éléments déjà connus lors de l'élaboration du plan de prévention des risques naturels, que les risques d'atteinte à la salubrité ou à la sécurité publique le justifient, s'opposer, sur le fondement de l'article R. 111-2 du code de l'urbanisme et sous le contrôle du juge de l'excès de pouvoir, à une déclaration préalable, alors même que le plan n'aurait pas classé le terrain d'assiette du projet en zone à risques ni prévu de prescriptions particulières qui lui soient applicables. <br/>
<br/>
              4. La cour administrative d'appel de Marseille a relevé, au terme d'une appréciation qui n'est pas arguée de dénaturation et par un arrêt suffisamment motivé, que le lotissement Giroval Sud n'était pas classé en zone rouge du plan de prévention des risques d'incendies de forêts, que le service départemental d'incendie et de secours avait délivré le 27 août 2012 un avis favorable à ce projet, avant son avis défavorable du 19 octobre 2012, et que le risque d'incendie résultant pour le lotissement de sa proximité avec un boisement n'était pas aggravé par le projet de fermeture du lotissement. En statuant ainsi, la cour, qui ne peut être regardée comme s'étant fondée sur les seules circonstances que le lotissement n'était pas classé en zone rouge et que le premier avis du service départemental d'incendie et de secours était favorable mais s'est prononcée au vu de l'ensemble des pièces du dossier qui lui était soumis, n'a pas commis d'erreur de droit.<br/>
<br/>
              Sur l'arrêt attaqué, en tant qu'il enjoint à la commune de prendre une décision de non-opposition à déclaration préalable :<br/>
<br/>
              5. Aux termes de l'article L. 911-1 du code de justice administrative, dans sa rédaction en vigueur à la date de l'arrêt attaqué : " Lorsque sa décision implique nécessairement qu'une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public prenne une mesure d'exécution dans un sens déterminé, la juridiction, saisie de conclusions en ce sens, prescrit, par la même décision, cette mesure assortie, le cas échéant, d'un délai d'exécution. (...) ". Aux termes de l'article L. 911-2 du même code : " Lorsque sa décision implique nécessairement qu'une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public prenne à nouveau une décision après une nouvelle instruction, la juridiction, saisie de conclusions en ce sens, prescrit, par la même décision juridictionnelle, que cette nouvelle décision doit intervenir dans un délai déterminé. (...) ".<br/>
<br/>
              6. La circonstance que le maire de Roquefort-les-Pins, pour l'exécution du jugement du tribunal administratif de Nice du 15 juin 2017 annulant son arrêté pour insuffisance de motivation, ait réexaminé la déclaration préalable de l'association syndicale libre Giroval Sud et pris une nouvelle décision d'opposition le 29 août 2017, fondée sur la méconnaissance de l'article R. 111-2 du code de l'urbanisme, ne dispensait pas la commune de prendre la nouvelle décision qu'impliquait nécessairement l'arrêt de la cour administrative d'appel de Marseille, dont les motifs faisaient obstacle à ce qu'elle puisse se fonder sur une telle méconnaissance. Par suite, la cour n'a pas commis d'erreur de droit en jugeant que la décision du 29 août 2017 ne s'opposait pas à ce qu'elle prononce une injonction à l'égard de la commune.<br/>
<br/>
              7. Toutefois, aucune règle ni aucun principe n'imposant, avant l'entrée en vigueur de l'article L. 424-3 du code de l'urbanisme dans sa rédaction issue de l'article 108 de la loi du 6 août 2015 pour la croissance, l'activité et l'égalité des chances économiques, que l'autorité compétente énonce dans sa décision l'intégralité des motifs justifiant son opposition à une déclaration préalable, l'annulation d'une telle opposition ne faisait pas obstacle à ce que la même décision puisse être légalement prise pour d'autres motifs. Dès lors, en jugeant que lorsque le juge annule une opposition préalable à une déclaration après avoir censuré l'ensemble des motifs que l'autorité compétente a énoncés dans sa décision ainsi que, le cas échéant, les motifs qu'elle a pu invoquer en cours d'instance, il doit, s'il est saisi de conclusions à fin d'injonction, ordonner à l'autorité compétente de prendre une décision de nonopposition, la cour a commis une erreur de droit. Par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi tendant aux mêmes fins, la commune de Roquefort-les-Pins est fondée à demander l'annulation de l'arrêt de la cour en tant qu'il lui enjoint de prendre une décision de non-opposition à la déclaration préalable présentée par l'association syndicale libre Giroval Sud.  <br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond, dans la mesure de la cassation prononcée, en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              9. Il résulte de ce qui a été dit ci-dessus que l'annulation de l'arrêté du 26 octobre 2012 par lequel le maire de Roquefort-les-Pins s'est opposé à la déclaration préalable déposée par l'association syndicale libre Giroval Sud, pour les motifs retenus par la cour administrative d'appel de Marseille dans son arrêt, implique que le maire de Roquefort-les-Pins prenne à nouveau une décision après une nouvelle instruction, dans le respect de l'autorité absolue de chose jugée s'attachant aux motifs qui sont le soutien nécessaire de cette annulation. Il y a lieu, par suite, d'enjoindre à ce maire de procéder à ce réexamen dans un délai de deux mois à compter de la notification de la présente décision. <br/>
<br/>
              Sur les frais liés au litige :<br/>
<br/>
              10. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Roquefort-les-Pins une somme de 2 000 euros à verser à l'association syndicale libre Giroval Sud au titre de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de cette association syndicale libre, qui n'est pas, pour l'essentiel, la partie perdante dans la présente instance.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 2 de l'arrêt de la cour administrative d'appel de Marseille du 6 juin 2019 est annulé. <br/>
Article 2 : Il est enjoint au maire de Roquefort-les-Pins de procéder au réexamen de la déclaration préalable de l'association syndicale libre Giroval Sud dans un délai de deux mois à compter de la notification de la présente décision. <br/>
Article 3 : Le surplus des conclusions du pourvoi de la commune de Roquefort-les-Pins est rejeté.<br/>
Article 4 : La commune de Roquefort-les-Pins versera une somme de 2 000 euros à l'association syndicale libre Giroval Sud au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : La présente décision sera notifiée à la commune de Roquefort-les-Pins et à l'association syndicale libre Giroval Sud.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
