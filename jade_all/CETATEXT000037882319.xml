<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037882319</ID>
<ANCIEN_ID>JG_L_2018_12_000000421547</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/88/23/CETATEXT000037882319.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 4ème chambres réunies, 26/12/2018, 421547, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-12-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421547</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 4ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>HAAS</AVOCATS>
<RAPPORTEUR>M. Thibaut Félix</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:421547.20181226</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La société La Poste a demandé au juge des référés du tribunal administratif de Nantes de suspendre, sur le fondement de l'article L. 521-1 du code de justice administrative, l'exécution des décisions du 13 mars 2018 par lesquelles le directeur régional des entreprises, de la concurrence, de la consommation, du travail et de l'emploi de la région Pays de la Loire lui a infligé des amendes de respectivement 17 500 euros, 4 000 euros et 10 000 euros. Par trois ordonnances n° 1804287, n° 1804288 et n° 1804289 du 30 mai 2018, le juge des référés du tribunal administratif de Nantes a suspendu l'exécution de ces décisions. <br/>
<br/>
              Par un pourvoi et un mémoire en réplique, enregistrés les 15 juin et 5 octobre 2018 au secrétariat du contentieux du Conseil d'Etat, le ministre du travail demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ces ordonnances ; <br/>
<br/>
              2°) statuant en référé, de rejeter les demandes de suspension présentées par la société La Poste ;<br/>
<br/>
              3°) de prononcer la suppression de certains passages du mémoire en défense de la société La Poste sur le fondement de l'article L. 741-2 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thibaut Félix, auditeur,  <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Haas, avocat de la société La poste.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1.	Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
<br/>
              2. Aux termes de l'article L. 8115-1 du code du travail, dans sa rédaction alors applicable : " L'autorité administrative compétente peut, sur rapport de l'agent de contrôle de l'inspection du travail mentionné à l'article L. 8112-1, et sous réserve de l'absence de poursuites pénales, prononcer à l'encontre de l'employeur une amende en cas de manquement : (...) 3° A l'article L. 3171-2 relatif à l'établissement d'un décompte de la durée de travail et aux dispositions réglementaires prises pour son application (...) ". Aux termes de l'article L. 8115-3 du même code, dans sa rédaction alors applicable : " Le montant maximal de l'amende est de 2 000 euros et peut être appliqué autant de fois qu'il y a de travailleurs concernés par le manquement. / Le plafond de l'amende est porté au double en cas de nouveau manquement constaté dans un délai d'un an à compter du jour de la notification de l'amende concernant un précédent manquement ".<br/>
<br/>
              3. Il ressort des pièces du dossier soumis au juge des référés que, sur le fondement des dispositions des articles L. 8115-1 et L. 8115-3 du code du travail, le directeur régional des entreprises, de la concurrence, de la consommation, du travail et de l'emploi (DIRECCTE) de la région Pays de la Loire a infligé à la société La Poste, le 13 mars 2018, trois sanctions administratives, d'un montant total de 31 500 euros, pour des manquements à l'obligation de tenue de documents de décompte individuel du temps de travail sur différents sites de plate-forme de distribution de courriers. Le ministre du travail se pourvoit en cassation contre trois ordonnances par lesquelles le juge des référés du tribunal administratif de Nantes, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, a suspendu l'exécution de ces sanctions à la demande de la société La Poste. <br/>
<br/>
              Sur l'urgence :<br/>
<br/>
              4. L'urgence justifie que soit prononcée la suspension d'un acte administratif lorsque l'exécution de celui-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre. Il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte litigieux sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue. Il lui appartient également, l'urgence s'appréciant objectivement et compte tenu de l'ensemble des circonstances de chaque espèce, de faire apparaître dans sa décision tous les éléments qui, eu égard notamment à l'argumentation des parties, l'ont conduit à considérer que la suspension demandée revêtait un caractère d'urgence.<br/>
<br/>
              5. Pour suspendre les sanctions qui avaient été prononcées, le juge des référés du tribunal administratif de Nantes a jugé que ces sanctions avaient nécessairement pour effet de contraindre La Poste à modifier l'organisation du temps de travail dans tous ses sites de distribution sur l'ensemble du territoire national, en modifiant en particulier les accords d'entreprise applicables en la matière, pour ne pas s'exposer à de nouvelles sanctions. En se fondant sur les conséquences des sanctions prononcées que la société pouvait être amenée à tirer, sur l'ensemble du territoire national et par des mesures dont les délais de mise en oeuvre excéderaient nécessairement celui dans lequel les juges du fond seraient amenés à statuer, alors qu'il ressort des pièces qui lui étaient soumises que l'exécution des sanctions litigieuses n'emportait pas nécessairement de telles conséquences et que les effets allégués des décisions attaquées sur l'organisation du travail et le climat social du groupe étaient trop éventuels pour caractériser l'urgence à suspendre les décisions litigieuses, le juge des référés du tribunal administratif de Nantes a dénaturé les faits de l'espèce.<br/>
<br/>
              6. Par suite, le ministre du travail est fondé à demander l'annulation des ordonnances qu'il attaque. Le moyen retenu suffisant à entraîner cette annulation, il n'est pas nécessaire d'examiner les autres moyens de son pourvoi.<br/>
<br/>
              Sur les procédures de référé :<br/>
<br/>
              7. Il y a lieu, par application de l'article L. 821-2 du code de justice administrative, de régler les affaires au titre des procédures de référé engagées par la société La Poste.<br/>
<br/>
              8. Il ressort des pièces du dossier que les trois décisions attaquées visaient à sanctionner la méconnaissance, par La Poste, dans les sites contrôlés, de l'obligation de décompte journalier et hebdomadaire du temps de travail des salariés qui découle des articles L. 3171-2 et D. 3171-8 du code du travail. D'une part, si La Poste soutient qu'elle se trouve contrainte de modifier l'organisation du travail des agents de distribution, ce qui aurait de nombreuses conséquences opérationnelles et sociales pour l'ensemble de cette activité sur le territoire, il ne résulte pas de l'instruction que l'exécution des sanctions litigieuses emporte nécessairement de telles conséquences et que la société ne puisse, pour éviter de nouvelles amendes dans l'attente du jugement de ses requêtes en annulation, ainsi que le fait valoir le ministre du travail, prendre des mesures conservatoires relatives au décompte individuel du temps de travail effectif sans bouleverser l'organisation du travail de ses agents. Les effets allégués des décisions attaquées sur l'organisation du travail et le climat social du groupe sont ainsi trop éventuels pour caractériser l'urgence à suspendre les décisions litigieuses. D'autre part, les sommes dues ou même encourues ne sont pas susceptibles de mettre en cause l'équilibre économique et financier de la société La Poste et ne constituent pas pour elle un préjudice grave et immédiat de nature à caractériser l'urgence. Par suite, le ministre du travail est fondé à soutenir que l'urgence ne justifie pas que l'exécution des sanctions attaquées soit suspendue. <br/>
<br/>
              9. Il suit de là que l'une des conditions auxquelles l'article L. 521-1 du code de justice administrative subordonne la suspension de l'exécution d'un acte n'est pas remplie. Dès lors, sans qu'il soit besoin de rechercher s'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de ces décisions, les conclusions de la société La Poste tendant à la suspension de l'exécution des sanctions du 13 mars 2018 doivent être rejetées.<br/>
<br/>
              Sur les frais liés au litige :<br/>
<br/>
              10. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'il soit fait droit aux conclusions présentées par la société La Poste à ce titre.<br/>
<br/>
              Sur les conclusions tendant à l'application de l'article L. 741-2 du code de justice administrative :<br/>
<br/>
              11. Le mémoire en défense de la société La Poste ne contient pas, contrairement à ce qui est soutenu, d'imputation à caractère injurieux, outrageant ou diffamatoire, au sens de l'article L. 741-2 du code de justice administrative, de nature à en faire prononcer la suppression. Il n'y a pas lieu, par suite, de faire droit aux conclusions du ministre tendant à une telle suppression.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Les ordonnances du juge des référés du tribunal administratif de Nantes du 30 mai 2018 sont annulées.<br/>
Article 2 : Les demandes présentées par la société La Poste devant le juge des référés du tribunal administratif  de Nantes sont rejetées.<br/>
Article 3 : Les conclusions de la société La Poste présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : Les conclusions de la ministre du travail tendant à l'application de l'article L. 741-2 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la ministre du travail et à la société La Poste.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
