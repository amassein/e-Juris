<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033894337</ID>
<ANCIEN_ID>JG_L_2017_01_000000404621</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/89/43/CETATEXT000033894337.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème chambres réunies, 19/01/2017, 404621, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-01-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>404621</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Dorothée Pradines</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:404621.20170119</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B..., à l'appui de sa demande tendant à l'annulation de la décision implicite du 3 juillet 2016 par laquelle la caisse d'allocations familiales de la Haute-Savoie a rejeté son recours formé contre la décision du 7 mars 2016 fixant le montant de sa prime d'activité, a produit un mémoire, enregistré le 5 octobre 2016 au greffe du tribunal administratif de Grenoble, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958, par lequel il soulève une question prioritaire de constitutionnalité.<br/>
<br/>
              Par une ordonnance n° 1604207 du 18 octobre 2016, enregistrée le 21 octobre 2016 au secrétariat du contentieux du Conseil d'Etat, le vice-président du tribunal administratif de Grenoble, avant qu'il soit statué sur la demande de M.B..., a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution du 5° de l'article L. 842-4 du code de la sécurité sociale.<br/>
<br/>
              Dans la question prioritaire de constitutionnalité transmise, M. B...soutient que ces dispositions, applicables au litige, portent atteinte au principe d'égalité devant la loi, affirmé par l'article 6 de la Déclaration des droits de l'homme et du citoyen.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ; <br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code général des impôts ;<br/>
              - le code de la sécurité sociale, notamment son article L. 842-4 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Dorothée Pradines, auditeur,  <br/>
<br/>
- les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
<br/>1. Considérant qu'il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 841-1 du code de la sécurité sociale : " La prime d'activité a pour objet d'inciter les travailleurs aux ressources modestes, qu'ils soient salariés ou non salariés, à l'exercice ou à la reprise d'une activité professionnelle et de soutenir leur pouvoir d'achat " ; qu'aux termes de l'article L. 842-3 du même code : " La prime d'activité est égale à la différence entre : / 1° Un montant forfaitaire dont le niveau varie en fonction de la composition du foyer et du nombre d'enfants à charge, augmenté d'une fraction des revenus professionnels des membres du foyer, et qui peut faire l'objet d'une ou de plusieurs bonifications ; / 2° Les ressources du foyer, qui sont réputées être au moins égales au montant forfaitaire mentionné au 1°. / La bonification mentionnée au 1° est établie pour chaque travailleur, membre du foyer, compte tenu de ses revenus professionnels. Son montant est une fonction croissante des revenus situés entre un seuil et un plafond. Au-delà de ce plafond, ce montant est fixe (...) " ; qu'aux termes de l'article L. 842-4 du même code : " Les ressources mentionnées au 2° de l'article L. 842-3 prises en compte pour le calcul de la prime d'activité sont : / 1° Les ressources ayant le caractère de revenus professionnels ou qui en tiennent lieu ; / 2° Les revenus de remplacement des revenus professionnels ; / 3° L'avantage en nature que constitue la disposition d'un logement à titre gratuit, déterminé de manière forfaitaire ; / 4° Les prestations et les aides sociales, à l'exception de certaines d'entre elles en raison de leur finalité sociale particulière ; / 5° Les autres revenus soumis à l'impôt sur le revenu " ;<br/>
<br/>
              3. Considérant que M. B...soutient que les dispositions du 5° de l'article L. 842-4 du code de la sécurité sociale méconnaissent l'article 6 de la Déclaration des droits de l'homme et du citoyen, en ce qu'elles instaurent, sans rapport direct avec leur objet, une différence de traitement entre les personnes tirant des revenus de leur patrimoine selon que ces revenus sont ou non soumis à l'impôt sur le revenu ; <br/>
<br/>
              4. Considérant qu'aux termes de l'article 6 de la Déclaration des droits de l'homme et du citoyen de 1789 : " La loi (...) doit être la même pour tous, soit qu'elle protège, soit qu'elle punisse " ; que le principe d'égalité ne s'oppose ni à ce que le législateur règle de façon différente des situations différentes ni à ce qu'il déroge à l'égalité pour des raisons d'intérêt général, pourvu que, dans l'un et l'autre cas, la différence de traitement qui en résulte soit en rapport direct avec l'objet de la loi qui l'établit ;<br/>
<br/>
              5. Considérant que l'application combinée des articles L. 842-3 et L. 842-4 du code de la sécurité sociale a pour conséquence, s'agissant des travailleurs percevant des revenus du patrimoine soumis à l'impôt sur le revenu, de réduire d'un montant égal à ces revenus le montant de la prime d'activité susceptible de leur être attribuée, alors que les revenus du patrimoine non soumis à l'impôt sur le revenu ne sont pas pris en compte pour la détermination du montant de prime d'activité auquel leur bénéficiaire a droit ; qu'il résulte des dispositions de l'article 157 du code général des impôts que les revenus ainsi exclus correspondent à des revenus soumis à une période d'indisponibilité ou à la rémunération des sommes placées sur des comptes ou livrets dont le législateur a choisi de ne pas soumettre les revenus, limités par l'encadrement des plafonds et des taux, à l'impôt sur le revenu ; qu'il ressort des travaux préparatoires de la loi du 17 août 2015 relative au dialogue social et à l'emploi, dont ces dispositions sont issues, qu'elles ont pour objet de remplacer la prime pour l'emploi et la part du revenu de solidarité active correspondant à une fraction des revenus professionnels du foyer par une prestation unique et plus simple, pour améliorer le recours des travailleurs aux ressources modestes à ce dispositif d'incitation à l'exercice ou à la reprise d'une activité professionnelle ; qu'à cette fin, le législateur a fait le choix de restreindre les revenus du patrimoine pris en compte à ceux déclarés par les intéressés au titre de l'impôt sur le revenu, de façon à limiter, s'agissant de cette catégorie de revenus, les obligations déclaratives nécessaires au calcul de la prime d'activité et à l'actualisation de son montant ; que la différence de traitement qui en résulte est en lien direct avec l'objet de la loi qui l'établit ; que, par suite, la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède qu'il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité transmise par le tribunal administratif de Grenoble.<br/>
Article 2 : La présente décision sera notifiée à M. A...B...et à la ministre des affaires sociales et de la santé.<br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre, à la caisse d'allocations familiales de la Haute-Savoie, ainsi qu'au tribunal administratif de Grenoble.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
