<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043358793</ID>
<ANCIEN_ID>JG_L_2021_04_000000445582</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/35/87/CETATEXT000043358793.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 12/04/2021, 445582, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-04-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>445582</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAMARRE, JEHANNIN</AVOCATS>
<RAPPORTEUR>M. Laurent-Xavier Simonel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:445582.20210412</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme B... A... a demandé au tribunal administratif de Nantes de condamner l'Ecole supérieure des beaux-arts de Nantes Métropole (ESBANM) à lui verser la somme de 151 144,14 euros en réparation du préjudice subi du fait de l'absence de prise en compte du travail qu'elle estimait avoir effectué pour le compte de cet établissement entre octobre 2010 et avril 2014. Par un jugement n° 1602934 du 27 juin 2018, le tribunal administratif de Nantes a condamné l'ESBANM à lui verser une somme de 2 000 euros en réparation du préjudice subi.<br/>
<br/>
              Par un arrêt n° 18NT02956 du 13 octobre 2020, la cour administrative d'appel de Nantes a rejeté l'appel formé par Mme A... contre ce jugement en tant qu'il ne fait pas entièrement droit à sa demande et, sur appel incident de l'ESBANM, a annulé ce jugement et rejeté la demande de première instance de Mme A....  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés le 22 octobre 2020 et le 8 janvier 2021 au secrétariat du contentieux du Conseil d'Etat, Mme A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel et de rejeter l'appel incident de l'ESBANM ;<br/>
<br/>
              3°) de mettre à la charge de l'ESBANM la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              -  la loi n° 83-634 du 13 juillet 1983 ;<br/>
              -  la loi n° 84-53 du 26 janvier 1984;<br/>
              -  le décret n° 85-733 du 17 juillet 1985 ;<br/>
              -  le décret n° 91-857 du 2 septembre 1991 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent-Xavier Simonel, conseiller d'Etat en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Delamarre, Jéhannin, avocat de Mme B... A... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ". <br/>
<br/>
              2. Pour demander l'annulation de l'arrêt qu'elle attaque, Mme A... soutient que la cour administrative d'appel de Nantes :<br/>
              - a inexactement qualifié les faits ou, à tout le moins, a dénaturé les pièces du dossier en écartant le moyen tiré de ce qu'elle avait exercé, en réalité, des fonctions de professeur d'enseignement artistique ou des fonctions de maître de conférence ou de professeur des universités associé ou invité au sein de l'ESBANM ;<br/>
              - a commis une erreur de droit ou, à tout le moins, a inexactement qualifié les faits en écartant le moyen tiré de ce que la responsabilité de l'ESBANM était engagée du fait qu'elle l'avait employée sans la rémunérer pour préparer un projet de publication d'une revue au-delà de l'année au cours de laquelle elle bénéficiait d'une bourse d'études ;<br/>
              - a inexactement qualifié les faits ou, à tout le moins, a dénaturé les pièces du dossier en jugeant que les éléments qu'elle avait produits ne permettaient pas de faire présumer de l'existence d'un harcèlement moral.<br/>
<br/>
              3. Eu égard aux moyens soulevés, il y a lieu d'admettre les conclusions du pourvoi dirigées contre l'arrêt attaqué en tant qu'il s'est prononcé sur la relation d'emploi établie entre l'ESBANM et Mme A... pendant sa période d'activité en qualité d'artiste-chercheur. En revanche, aucun des moyens soulevés n'est de nature à permettre l'admission des conclusions dirigées contre l'arrêt attaqué en tant qu'il s'est prononcé, d'une part, sur la responsabilité de l'ESBANM à l'égard des activités de Mme A... pour la mise au point d'une publication scientifique et, d'autre part, sur l'allégation par Mme A... d'une situation de harcèlement moral.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Les conclusions du pourvoi de Mme A... dirigées contre l'arrêt attaqué en tant qu'il s'est prononcé sur la relation d'emploi établie entre l'ESBANM et Mme A... pendant sa période d'activité en qualité d'artiste-chercheur, sont admises.<br/>
Article 2 : Le surplus des conclusions du pourvoi de Mme A... n'est pas admis.<br/>
Article 3 : La présente décision sera notifiée à Madame B... A....<br/>
Copie en sera adressée à l'Ecole supérieure des beaux-arts de Nantes Métropole.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
