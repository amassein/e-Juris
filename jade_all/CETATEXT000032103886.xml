<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032103886</ID>
<ANCIEN_ID>JG_L_2016_02_000000380116</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/10/38/CETATEXT000032103886.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème SSR, 24/02/2016, 380116</TITRE>
<DATE_DEC>2016-02-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>380116</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ODENT, POULET ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>Mme Florence Marguerite</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean Lessi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:380116.20160224</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif de Poitiers d'annuler la décision implicite de rejet opposée par le président de la région Poitou-Charentes à sa demande du 6 avril 2010 tendant au versement d'allocations de chômage à compter du 1er février 2010. Par un jugement n° 1001647 du 23 janvier 2013, le tribunal administratif de Poitiers a annulé cette décision en tant que le bénéfice des allocations de chômage lui a été refusé pour la période du 1er au 3 février 2010 et a rejeté le surplus des conclusions de sa demande. <br/>
<br/>
              Par un arrêt n° 13BX01652 du 11 mars 2014, la cour administrative d'appel de  Bordeaux, à la demande de MmeB..., a annulé ce jugement du tribunal administratif de Poitiers en tant qu'il rejette ses conclusions tendant à l'annulation de l'intégralité de la décision du président de la région Poitou-Charentes, ainsi que cette décision, et a enjoint au président de la région de lui verser les allocations d'assurance chômage auxquelles elle a droit, pour la période du 1er février au 31 août 2010. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire, un mémoire en réplique et un nouveau mémoire, enregistrés les 12 mai 2014, 25 juillet 2014, 22 juin 2015 et 7 août 2015 au secrétariat du contentieux du Conseil d'Etat, la région Poitou-Charentes demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de MmeB.... <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le décret n° 86-68 du 13 janvier 1986 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Florence Marguerite, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Jean Lessi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Odent, Poulet, avocat de la région Poitou-Charentes, et à la SCP Foussard, Froger, avocat de MmeB... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant, d'une part, qu'il résulte des dispositions de l'article 72 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale que le fonctionnaire territorial qui, demandant sa réintégration après avoir été placé en disponibilité pour convenances personnelles pendant une durée n'ayant pas excédé trois années, doit se voir proposer une des trois premières vacances d'emploi dans sa collectivité d'origine et que, s'il refuse successivement trois postes qui lui sont proposés dans le ressort territorial de son cadre d'emploi, emploi ou corps en vue de cette réintégration, la collectivité peut le licencier après avis de la commission administrative paritaire mais n'est pas tenue de le faire ; <br/>
<br/>
              2. Considérant, d'autre part, qu'aux termes des dispositions de l'article L. 5421-1 du code du travail, " en complément des mesures tendant à faciliter leur reclassement ou leur conversion, les travailleurs involontairement privés d'emploi, (...) aptes au travail et recherchant un emploi, ont droit à un revenu de remplacement dans les conditions fixées au présent titre " ; qu'aux termes de l'article L. 5424-1 du même code : " Ont droit à une allocation d'assurance dans les conditions prévues aux articles L. 5422-2 et L. 5422-3 : / 1° Les agents fonctionnaires et non fonctionnaires de l'Etat et de ses établissements publics administratifs, les agents titulaires des collectivités territoriales (...) " ; <br/>
<br/>
              3. Considérant qu'il résulte de la combinaison de ces dispositions qu'un fonctionnaire territorial qui, à l'expiration de la période pendant laquelle il a été placé, sur sa demande, en disponibilité, est maintenu d'office dans cette position, ne peut prétendre au bénéfice des allocations d'assurance chômage que si ce maintien résulte de motifs indépendants de sa volonté ; que tel n'est pas le cas du fonctionnaire qui a refusé un emploi, répondant aux conditions définies par les dispositions statutaires applicables, qui lui a été proposé par la collectivité en vue de sa réintégration ;<br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que MmeB..., ingénieur territorial de la région Poitou-Charentes, a sollicité sa réintégration à compter du 1er février 2010 à l'issue de sa période de disponibilité pour convenances personnelles ; que, maintenue d'office en disponibilité dans l'attente de sa réintégration, elle n'a pas donné suite aux premières propositions de poste qui lui ont été adressées par la collectivité entre le 3 et le 8 février 2010 ; qu'en jugeant que la circonstance que Mme B...avait, à l'expiration de sa période de disponibilité pour convenances personnelles, été maintenue d'office dans cette position en dépit de sa demande de réintégration suffisait à établir qu'elle était involontairement privée d'emploi durant toute la période allant du 1er février au 31 août 2010, sans qu'il soit besoin d'apprécier la nature des emplois qui lui avaient été proposés et les motifs de ses refus, la cour administrative d'appel de Bordeaux a commis une erreur de droit ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, la région Poitou-Charentes est fondée à demander l'annulation de l'arrêt qu'elle attaque ;<br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de la région Poitou-Charentes, qui n'est pas la partie perdante dans le présent litige, la somme que Mme B...demande au titre des frais exposés par elle et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Bordeaux du 11 mars 2014 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Bordeaux.<br/>
Article 3 : Les conclusions de Mme B...présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la région Aquitaine, Limousin et Poitou-Charentes et à Mme A...B....<br/>
Copie en sera adressée à la ministre de la décentralisation et de la fonction publique. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-10-06-04 FONCTIONNAIRES ET AGENTS PUBLICS. CESSATION DE FONCTIONS. LICENCIEMENT. ALLOCATION POUR PERTE D'EMPLOI. - CONDITIONS D'OUVERTURE DES DROITS (ART. L. 5424-1 DU CODE DU TRAVAIL) - FONCTIONNAIRE TERRITORIAL AYANT REFUSÉ, À L'ISSUE D'UNE PÉRIODE DE DISPONIBILITÉ, UN EMPLOI PROPOSÉ DANS LE RESPECT DE SON STATUT - ABSENCE D'ALLOCATION [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">66-10-02 TRAVAIL ET EMPLOI. POLITIQUES DE L'EMPLOI. INDEMNISATION DES TRAVAILLEURS PRIVÉS D'EMPLOI. - DROITS DES AGENTS DE L'ETAT ET DES COLLECTIVITÉS TERRITORIALES (ART. L. 5424-1 DU CODE DU TRAVAIL) - FONCTIONNAIRE TERRITORIAL AYANT REFUSÉ, À L'ISSUE D'UNE PÉRIODE DE DISPONIBILITÉ, UN EMPLOI PROPOSÉ DANS LE RESPECT DE SON STATUT - ABSENCE D'ALLOCATION [RJ1].
</SCT>
<ANA ID="9A"> 36-10-06-04 Un fonctionnaire territorial qui, à l'expiration de la période pendant laquelle il a été placé, sur sa demande, en disponibilité, est maintenu d'office dans cette position, ne peut prétendre au bénéfice des allocations d'assurance chômage que si ce maintien résulte de motifs indépendants de sa volonté. Tel n'est pas le cas du fonctionnaire qui a refusé un emploi, répondant aux conditions définies par les dispositions statutaires applicables, qui lui a été proposé par la collectivité en vue de sa réintégration.</ANA>
<ANA ID="9B"> 66-10-02 Un fonctionnaire territorial qui, à l'expiration de la période pendant laquelle il a été placé, sur sa demande, en disponibilité, est maintenu d'office dans cette position, ne peut prétendre au bénéfice des allocations d'assurance chômage que si ce maintien résulte de motifs indépendants de sa volonté. Tel n'est pas le cas du fonctionnaire qui a refusé un emploi, répondant aux conditions définies par les dispositions statutaires applicables, qui lui a été proposé par la collectivité en vue de sa réintégration.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Comp., pour un agent ne se voyant pas proposer d'emploi à l'issue de la disponibilité faute de poste vacant, CE, 30 septembre 2002, Mme,, n° 216912, T. p. 954 ; CE, 28 juillet 2004, Office public d'aménagement et de construction Sarthe habitat, n° 243387, T. pp. 748-901.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
