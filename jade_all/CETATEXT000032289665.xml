<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032289665</ID>
<ANCIEN_ID>JG_L_2016_03_000000393959</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/28/96/CETATEXT000032289665.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème SSJS, 23/03/2016, 393959, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-03-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>393959</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER, PINATEL ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Bruno Bachini</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2016:393959.20160323</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au juge des référés du tribunal administratif de Besançon, d'une part, d'ordonner, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de la décision du 7 juillet 2015 par laquelle le président de l'université de Franche-Comté a refusé de l'admettre en troisième et quatrième semestres du master " psychologie cognitive et neuropsychologie " au titre de l'année 2015-2016, d'autre part, d'enjoindre au président de l'université de procéder à son inscription. Par une ordonnance n° 1501252 du 21 août 2015, le juge des référés a rejeté sa demande. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 7 et 22 octobre 2015 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande ; <br/>
<br/>
              3°) de mettre à la charge de l'université de Franche-Comté la somme de 1 500 euros sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991 relative à l'aide juridique.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'éducation ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ; <br/>
              - l'arrêté du 25 avril 2002 relatif au diplôme national de master ; <br/>
              - l'arrêté du 22 janvier 2014 fixant le cadre national des formations conduisant à la délivrance des diplômes nationaux de licence, de licence professionnelle et de master ;  <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Bachini, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de M. A...et à la SCP Lyon-Caen, Thiriez, avocat de l'université de Franche-Comté ; <br/>
<br/>
<br/>
<br/>
<br/>1. Considérant, d'une part, qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ;<br/>
<br/>
              2. Considérant, d'autre part, qu'aux termes de l'article L. 612-1 du code de l'éducation : " Le déroulement des études supérieures est organisé en cycles (...) / Les grades de licence, de master et de doctorat sont conférés respectivement dans le cadre du premier, du deuxième et du troisième cycle (...) " ; qu'aux termes de l'article L. 612-6 du même code : " L'admission dans les formations du deuxième cycle est ouverte à tous les titulaires des diplômes sanctionnant les études de premier cycle (...) / La liste limitative des formations dans lesquelles cette admission peut dépendre des capacités d'accueil des établissements et, éventuellement, être subordonnée au succès à un concours ou à l'examen du dossier du candidat, est établie par décret après avis du Conseil national de l'enseignement supérieur et de la recherche (...) " ; qu'aux termes de l'article 3 de l'arrêté du 25 avril 2002 relatif au diplôme national de master : " Le diplôme de master sanctionne un niveau correspondant à l'obtention de 120 crédits européens au-delà du grade de licence " ; qu'aux termes de l'article 11 du même arrêté : " Lorsqu'une université est habilitée à délivrer le diplôme de master, l'accès de l'étudiant titulaire de la licence, dans le même domaine, est de droit pour les 60 premiers crédits européens. / L'admission ultérieure dans un parcours type de formation débouchant sur le master recherche s'effectue dans les conditions prévues à l'article 16 de l'arrêté du 25 avril 2002 susvisé. L'admission ultérieure dans un parcours type de formation débouchant sur le master professionnel est prononcée par le chef d'établissement sur proposition du responsable de la formation " ; <br/>
<br/>
              3. Considérant qu'il résulte des dispositions des articles L. 612-1 et L. 612-6 du code de l'éducation citées ci-dessus que l'admission à une formation de deuxième cycle au terme de laquelle est délivré le grade de master, en première comme en deuxième année, ne peut dépendre des capacités d'accueil d'un établissement ou être subordonnée au succès à un concours ou à l'examen du dossier des candidats que si cette formation figure sur la liste qu'elles mentionnent ; qu'il s'ensuit que, pour une formation de deuxième cycle qui n'a pas été inscrite à cette fin sur cette liste, aucune limitation à l'admission des candidats du fait des capacités d'accueil d'un établissement ou par une condition de réussite à un concours ou d'examen du dossier ne peut être introduite après l'obtention des 60 premiers crédits européens, c'est-à-dire après la première année du deuxième cycle ; que les dispositions de l'article 11 de l'arrêté du 25 avril 2002 citées ci-dessus ne sauraient, en tout état de cause, avoir eu légalement pour objet ou pour effet de permettre de limiter l'admission des candidats, sauf à ce que la formation en cause ait été inscrite à ce titre sur une liste établie par décret pris après avis du Conseil national de l'enseignement supérieur et de la recherche ;<br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Besançon que, par une décision du 7 juillet 2015, le président de l'université de Franche-Comté a refusé d'autoriser M.A..., ayant validé son année de master 1, à s'inscrire en année de master 2 dans la spécialité " psychologie cognitive et neuropsychologie " pour l'année 2015-2016, au motif que son dossier de candidature n'avait pas été retenu ;  <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède qu'en jugeant que le moyen tiré de la méconnaissance, en l'absence d'une liste de formations établie par décret dans les conditions prévues par cet article, des dispositions de l'article L. 612-6 du code de l'éducation n'était pas propre à créer, en l'état de l'instruction, un doute sérieux sur la légalité de la décision contestée, le juge des référés a commis une erreur de droit ; que, par suite, et sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, son ordonnance doit être annulée ; <br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de statuer sur la demande de suspension en application des dispositions de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              7. Considérant que l'admission de M. A...en master 2 n'est plus susceptible d'intervenir avant la rentrée universitaire 2016-2017 ; que, dès lors, les effets de la décision de refus contestée ne sont pas de nature à caractériser une situation d'urgence justifiant sa suspension ; <br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que M. A...n'est pas fondé à demander la suspension de la décision du président de l'université de Franche-Comté du 7 juillet 2015 ; que ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991 relative à l'aide juridique ne peuvent, par suite, qu'être rejetées ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par l'université de Franche-Comté au titre de ces mêmes dispositions du code de justice administrative ; <br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Besançon du 21 août 2015 est annulée. <br/>
<br/>
Article 2 : Le surplus des conclusions du pourvoi et la demande de suspension de M. A...sont rejetés. <br/>
<br/>
Article 3 : Les conclusions de l'université de Franche-Comté présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 4 : La présente décision sera notifiée à M. B...A...et à l'université de Franche-Comté.<br/>
Copie en sera adressée à la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
