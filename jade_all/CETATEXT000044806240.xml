<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044806240</ID>
<ANCIEN_ID>JG_L_2021_12_000000450845</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/80/62/CETATEXT000044806240.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 30/12/2021, 450845, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>450845</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Ségolène Cavaliere</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:450845.20211230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. D... A... a demandé au tribunal administratif de Cergy-Pontoise d'annuler les opérations électorales qui se sont déroulées le 28 juin 2020 en vue de la désignation des conseillers municipaux et conseillers communautaires de la commune de Sarcelles (Val d'Oise). Par un jugement n° 2006003 du 18 février 2021, le tribunal administratif a rejeté sa protestation.<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés les 19 mars et 19 avril 2021 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de faire droit à sa protestation.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code électoral ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Ségolène Cavaliere, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il résulte de l'instruction qu'à l'issue du second tour des opérations électorales organisé le 28 juin 2020 dans la commune de Sarcelles (Val-d'Oise) pour la désignation des conseillers municipaux et communautaires, la liste " Rassemblés pour Sarcelles " conduite par M. C... a recueilli 5584 voix représentant 57,85 % des suffrages exprimés et la liste " Pour que vive Sarcelles " conduite par M. F... a recueilli 4068 voix représentant 42,14 % des suffrages exprimés. M. A... fait appel du jugement du 18 février 2021 par lequel le tribunal administratif de Cergy-Pontoise a rejeté sa protestation dirigée contre ces opérations électorales.<br/>
<br/>
              Sur les moyens relatifs à l'utilisation des moyens municipaux :<br/>
<br/>
              2. Aux termes de l'article L. 52-8 du code électoral : " Les personnes morales, à l'exception des partis ou groupements politiques, ne peuvent participer au financement de la campagne électorale d'un candidat, ni en lui consentant des dons sous quelque forme que ce soit, ni en lui fournissant des biens, services ou autres avantages directs ou indirects à des prix inférieurs à ceux qui sont habituellement pratiqués ".<br/>
<br/>
              3. En premier lieu, il résulte de l'instruction que, si la police municipale de Sarcelles a été présente aux abords de rassemblements électoraux organisés par M. C... dans l'espace public de la commune de Sarcelles, cette présence s'inscrivait dans le cadre normal de la mission de la police municipale et n'a pas revêtu le caractère d'une mise à la disposition de ce dernier les moyens de la municipalité.<br/>
<br/>
              4. En deuxième lieu, il ne résulte pas de l'instruction que, contrairement à ce que soutient le requérant en produisant une photo qui ne permet pas de l'établir, M. C... aurait fait usage de son véhicule de fonction dans le cadre de sa campagne électorale.<br/>
<br/>
              5. Enfin, il ne résulte pas de l'instruction que la directrice de la communication de la ville de Sarcelles aurait, en cette qualité, ou sur son temps de travail, assuré la fonction de conseil en communication de M. C... pendant sa campagne.<br/>
<br/>
              6. Il résulte de ce qui précède que le requérant n'est pas fondé à soutenir que M. C... a bénéficié de moyens municipaux en violation des dispositions de l'article L. 52-8 du code électoral.<br/>
<br/>
              Sur le moyen tiré de la méconnaissance des dispositions des articles L. 48-1 et L. 52-1 du code électoral :<br/>
<br/>
              7. Aux termes de l'article L. 52-1 du code électoral : " Pendant les six mois précédant le premier jour du mois d'une élection et jusqu'à la date du tour de scrutin où celle-ci est acquise, l'utilisation à des fins de propagande électorale de tout procédé de publicité commerciale par la voie de la presse ou par tout moyen de communication audiovisuelle est interdite. / A compter du premier jour du sixième mois précédant le mois au cours duquel il doit être procédé à des élections générales, aucune campagne de promotion publicitaire des réalisations ou de la gestion d'une collectivité ne peut être organisée sur le territoire des collectivités intéressées par le scrutin. Sans préjudice des dispositions du présent chapitre, cette interdiction ne s'applique pas à la présentation, par un candidat ou pour son compte, dans le cadre de l'organisation de sa campagne, du bilan de la gestion des mandats qu'il détient ou qu'il a détenus. Les dépenses afférentes sont soumises aux dispositions relatives au financement et au plafonnement des dépenses électorales contenues au chapitre V bis du présent titre ". <br/>
<br/>
              8. En premier lieu, il résulte de l'instruction que la réunion de quartier tenue par M. C... le 26 juin 2020 dans le quartier Watteau Koening à Sarcelles, qui s'inscrivait dans l'exercice normal de ses fonctions de maire, n'a pas revêtu le caractère d'une campagne de promotion publicitaire au sens des dispositions citées ci-dessus.<br/>
<br/>
              9. En deuxième lieu, la publication sur la page Facebook privée de M. C... d'informations relatives à une opération de dépistage de la covid-19 organisée par la municipalité de Sarcelles n'est pas davantage constitutive d'une campagne de promotion publicitaire au sens de ces mêmes dispositions.<br/>
<br/>
              10. En troisième lieu, l'installation dans une des rues de Sarcelles, pendant la campagne électorale, d'un panneau mentionnant la réalisation par la commune et le syndicat intercommunal d'aménagement hydraulique de travaux de rénovation du réseau des eaux usées pour une durée de quatre mois, qui avait pour seul objet d'en avertir les riverains, ne saurait davantage être regardée comme revêtant le caractère d'une campagne de promotion publicitaire au sens de ces dispositions.<br/>
<br/>
              11. Il résulte de ce qui précède que le requérant n'est pas fondé à soutenir que M. C... aurait organisé des campagnes de promotion publicitaire prohibées par les dispositions de l'article L. 52-1 du code électoral. <br/>
<br/>
              Sur les autres moyens :<br/>
<br/>
              12. En premier lieu, aux termes de l'article L. 106 du code électoral : " Quiconque, par des dons ou libéralités en argent ou en nature, par des promesses de libéralités, de faveurs, d'emplois publics ou privés ou d'autres avantages particuliers, faits en vue d'influencer le vote d'un ou de plusieurs électeurs aura obtenu ou tenté d'obtenir leur suffrage, soit directement, soit par l'entremise d'un tiers, quiconque, par les mêmes moyens, aura déterminé ou tenté de déterminer un ou plusieurs d'entre eux à s'abstenir, sera puni de deux ans d'emprisonnement et d'une amende de 15 000 euros ".<br/>
<br/>
              13. Il résulte de l'instruction qu'une distribution de bons alimentaires a été organisée à l'initiative du centre communal d'action sociale à destination de plusieurs familles modestes résidant dans la commune trois semaines avant le second tour des élections municipales. Contrairement à ce que soutient le requérant, une telle action, dont l'objectif était de répondre à un besoin urgent de ces familles né de la crise sanitaire ne saurait, alors même que le nom de M. C... figurait sur ces bons en sa qualité de président du centre communal d'action sociale, être regardée comme une libéralité octroyée en vue d'obtenir des suffrages, en méconnaissance des dispositions de l'article L. 106 du code électoral.<br/>
<br/>
              14. En deuxième lieu, il résulte de l'instruction que si un tract distribué dans la semaine ayant précédé le scrutin alléguait des liens entre M. F... et l'extrême droite, la diffusion de ce document, dont les termes n'excédaient pas les limites de la polémique électorale et auquel il a été possible pour M. F... de répondre utilement, n'a pas été de nature à altérer la sincérité du scrutin.<br/>
<br/>
              15. En troisième lieu, il résulte de l'instruction que ni la publication, sur la page Facebook de M. B... invitant la veille du scrutin à voter pour M. C..., ni celle, le jour du scrutin, sur la page Facebook de l'association Eternal Victory, d'un remerciement adressé au maire pour avoir mis à sa disposition la salle des fêtes de la commune n'ont été de nature à altérer la sincérité du scrutin.<br/>
<br/>
              16. En quatrième lieu, il ne résulte de l'instruction, ni que M. C... aurait offert un emploi à un électeur venu voter dans le bureau de vote dont il était le président, ni, ainsi que se borne à le soutenir le requérant, qu'un électeur aurait voté à la fois au bureau de vote n°22 et au bureau de vote n°24.<br/>
<br/>
              17. Enfin, il résulte de l'instruction, notamment des procès-verbaux des bureaux de vote n°s 7, 9, 13, 18 et 21 que dans ces bureaux, des écarts ont été constatés entre le nombre de bulletins trouvés dans l'urne et le nombre d'émargements, pour un total de sept suffrages. Si, en raison de ces erreurs, dont il ne résulte pas de l'instruction qu'elles révéleraient l'existence d'une fraude, sept suffrages doivent être retranchés hypothétiquement du total des voix obtenues par la liste de M. C... arrivée en tête, cette soustraction est, eu égard à l'importance de l'écart de voix entre les deux listes pour l'attribution du dernier siège au conseil municipal et au conseil communautaire, sans incidence sur le résultat des opérations électorales.<br/>
<br/>
              18. Il résulte de tout ce qui précède que M. A... n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif a rejeté sa protestation.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de M. A... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. D... A..., à M. E... C... et au ministre de l'intérieur.<br/>
Copie en sera adressée à la Commission nationale des comptes de campagne et des financements politiques.<br/>
              Délibéré à l'issue de la séance du 25 novembre 2021 où siégeaient : M. Denis Piveteau, président de chambre, présidant ; M. Jean-Philippe Mochon, conseiller d'Etat et Mme Ségolène Cavaliere, maître des requêtes en service extraordinaire-rapporteure. <br/>
<br/>
              Rendu le 30 décembre 2021.<br/>
<br/>
                 Le Président : <br/>
                 Signé : M. Denis Piveteau<br/>
 		La rapporteure : <br/>
      Signé : Mme Ségolène Cavaliere<br/>
                 La secrétaire :<br/>
                 Signé : Mme H... G...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
