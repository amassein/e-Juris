<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043574589</ID>
<ANCIEN_ID>JG_L_2021_05_000000434576</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/57/45/CETATEXT000043574589.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 31/05/2021, 434576, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-05-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>434576</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP RICARD, BENDEL-VASSEUR, GHNASSIA ; SCP JEAN-PHILIPPE CASTON</AVOCATS>
<RAPPORTEUR>Mme Airelle Niepce</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Stéphane Hoynck</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:434576.20210531</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'association Agir pour la Crau, l'association Nature et citoyenneté Crau Camargue Alpilles (NACICCA) et l'association France nature environnement Bouches-du-Rhône (FNE 13) ont demandé au tribunal administratif de Marseille d'annuler l'arrêté du préfet des Bouches-du-Rhône du 25 janvier 2013 autorisant la société Logiprest à exploiter une installation de stockage de matières, produits ou substances combustibles en entrepôts couverts d'une capacité maximale de 1 677 600 m3, sur le territoire de la commune de Saint-Martin-de-Crau, dans la zone industrielle du Bois de Leuze, au lieu-dit Mas de Leuze.<br/>
<br/>
              Par un jugement n° 1400631 du 12 janvier 2017, le tribunal administratif de Marseille a annulé cet arrêté.<br/>
<br/>
              Par un arrêt nos 17MA00990, 17MA00996 du 12 juillet 2019, la cour administrative d'appel de Marseille a rejeté les appels formés par la société Logiprest et le ministre d'Etat, ministre de la transition écologique et solidaire contre ce jugement.<br/>
<br/>
              1° Sous le n° 434576, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 12 septembre et 12 décembre 2019 au secrétariat du contentieux du Conseil d'Etat, la société Logiprest demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'association Agir pour la Crau, l'association Nature et citoyenneté Crau Camargue Alpilles et l'association France nature environnement Bouches-du-Rhône la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              2° Sous le n° 434604, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 13 septembre et 16 décembre 2019 au secrétariat du contentieux du Conseil d'Etat, la ministre de la transition écologique et solidaire demande au Conseil d'Etat d'annuler cet arrêt.<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code de l'environnement ;<br/>
              - le code de l'urbanisme ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ; <br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Airelle Niepce, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Stéphane Hoynck, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Jean-Philippe Caston, avocat de la société Logiprest et à la SCP Ricard, Bendel-Vasseur, Ghnassia, avocat de l'association Agir pour la Crau et autres ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par arrêté en date du 25 janvier 2013 pris au titre la législation des installations classées pour la protection de l'environnement, le préfet des Bouches-du-Rhône a autorisé la société Logiprest à exploiter une installation de stockage de matières, produits ou substances combustibles d'une capacité maximale de 1 677 600 m3 sur le territoire de la commune de Saint-Martin-de-Crau, dans la zone industrielle du Bois de Leuze. Parallèlement, par deux arrêtés respectivement des 18 et 20 juillet 2012, la ministre chargée de l'environnement et le préfet des Bouches-du-Rhône ont délivré à la SCI Boussard sud, aux droits de laquelle vient la société Logiprest, des dérogations aux interdictions de destruction et de perturbation des espèces protégées et de leurs habitats. A la demande des associations Agir pour la Crau, Nature et citoyenneté Crau Camargue Alpilles et France nature environnement Bouches-du-Rhône, par jugement du 12 janvier 2017, le tribunal administratif de Marseille a annulé l'arrêté du 25 juillet 2013 portant autorisation d'exploiter. Par un arrêt du 12 juillet 2019, la cour administrative d'appel de Marseille rejeté les appels formés par la société Logiprest et le ministre d'Etat, ministre de la transition écologique et solidaire contre ce jugement. Par deux pourvois qu'il y a lieu de joindre pour statuer par une seule décision, la société Logiprest et la ministre de la transition écologique et solidaire se pourvoient en cassation contre cet arrêt. <br/>
<br/>
              2. Il appartient au juge du plein contentieux des installations classées pour la protection de l'environnement d'apprécier le respect des règles relatives à la forme et la procédure régissant la demande d'autorisation au regard des circonstances de droit et de fait en vigueur à la date de délivrance de l'autorisation et celui des règles de fond régissant le projet en cause au regard des circonstances de droit et de fait en vigueur à la date à laquelle il se prononce, sous réserve du respect des règles d'urbanisme qui s'apprécie au regard des circonstances de droit et de fait applicables à la date de l'autorisation.<br/>
<br/>
              3. Aux termes de l'article L. 511-1 du code de l'environnement : " Sont soumis aux dispositions du présent titre les usines, ateliers, dépôts, chantiers et, d'une manière générale, les installations exploitées ou détenues par toute personne physique ou morale, publique ou privée, qui peuvent présenter des dangers ou des inconvénients soit pour la commodité du voisinage, soit pour la santé, la sécurité, la salubrité publiques, soit pour l'agriculture, soit pour la protection de la nature, de l'environnement et des paysages, soit pour l'utilisation rationnelle de l'énergie, soit pour la conservation des sites et des monuments ainsi que des éléments du patrimoine archéologique ". Aux termes de l'article L. 512-1 du même code, dans sa rédaction issue de l'ordonnance du 26 janvier 2017 relative à l'autorisation environnementale, applicable en l'espèce : " Sont soumises à autorisation les installations qui présentent de graves dangers ou inconvénients pour les intérêts mentionnés à l'article L. 511-1. / L'autorisation, dénommée autorisation environnementale, est délivrée dans les conditions prévues au chapitre unique du titre VIII du livre Ier. " Par ailleurs, aux termes de l'article L. 181-3 du même code, créé par la même ordonnance, dont les dispositions ont été rendues applicables aux autorisations d'exploiter délivrées avant son entrée en vigueur en vertu des dispositions de l'article 15 de l'ordonnance du 26 janvier 2017 : " I. - L'autorisation environnementale ne peut être accordée que si les mesures qu'elle comporte assurent la prévention des dangers ou inconvénients pour les intérêts mentionnés aux articles L. 211-1 et L. 511-1, selon les cas. / II. - L'autorisation environnementale ne peut être accordée que si les mesures qu'elle comporte assurent également : / (...) 4° Le respect des conditions, fixées au 4° de l'article L. 411-2, de délivrance de la dérogation aux interdictions édictées pour la conservation de sites d'intérêt géologique, d'habitats naturels, des espèces animales non domestiques ou végétales non cultivées et de leurs habitats, lorsque l'autorisation environnementale tient lieu de cette dérogation ; / (...) "<br/>
<br/>
              4. En premier lieu, dans l'exercice de ses pouvoirs de police administrative en matière d'installations classées pour la protection de l'environnement, il appartient à l'autorité administrative d'assortir l'autorisation d'exploiter délivrée en application de l'article L. 512-1 du code de l'environnement des prescriptions de nature à assurer la protection des intérêts mentionnés à l'article L. 511-1 du même code, en tenant compte des conditions d'installation et d'exploitation précisées par le pétitionnaire dans le dossier de demande, celles-ci comprenant notamment les engagements qu'il prend afin d'éviter, réduire et compenser les dangers ou inconvénients de son exploitation pour ces intérêts.<br/>
<br/>
              5. Il ressort des pièces du dossier soumis aux juges du fond, d'une part, que la demande d'autorisation d'exploiter présentée par la SCI Boussard sud, aux droits de laquelle vient la société Logiprest, comporte  un certain nombre de mesures destinées à atténuer les atteintes du projet sur l'environnement, y compris celles faisant suite à une demande d'éléments complémentaires de la préfecture des Bouches-du-Rhône, d'autre part, que l'article 1.3.1 de l'arrêté en litige prévoit que les installations et leurs annexes doivent être disposées, aménagées et exploités conformément aux éléments figurant dans les différents dossiers déposés par le pétitionnaire et respecter les éventuels arrêtés complémentaires ainsi que les autres réglementations en vigueur. Par suite, en relevant que le préfet, qui n'avait imposé aucune prescription à l'exploitant en rapport avec ses activités et avec les atteintes qu'elles sont susceptibles de porter aux intérêts mentionnés à l'article L. 511-1 du code de l'environnement, ne pouvait utilement invoquer, pour apprécier le respect par l'installation de ces intérêts, des engagements pris par la société pétitionnaire dans son dossier de demande pour réduire l'impact de son exploitation sur l'environnement, la cour a commis une erreur de droit. <br/>
<br/>
              6. En second lieu, en vertu du I de l'article L. 411-1 du code de l'environnement, " lorsqu'un intérêt scientifique particulier, le rôle essentiel dans l'écosystème ou les nécessités de la préservation du patrimoine naturel justifient la conservation de sites d'intérêt géologique, d'habitats naturels, d'espèces animales non domestiques ou végétales non cultivées et de leurs habitats ", sont notamment interdites la destruction et la perturbation intentionnelle des espèces animales protégées, la destruction de végétaux protégés ainsi que la destruction, l'altération ou la dégradation de leurs habitats. Toutefois, les dispositions du 4° du l'article L. 411-2 du même code permettent de déroger à ces interdictions dans les strictes conditions qu'elles précisent, parmi lesquelles figure dans tous les cas celle que " la dérogation ne nuise pas au maintien, dans un état de conservation favorable, des populations des espèces concernées dans leur aire de répartition naturelle ".<br/>
<br/>
              7. Il résulte de ces dispositions combinées avec celles citées des articles L. 511-1, L. 512-1 et L. 181-3 du code de l'environnement que lorsque la construction et la fonctionnement d'une installation classée pour la protection de l'environnement nécessite la délivrance d'une dérogation au titre de l'article L. 411-2 du même code, les conditions d'octroi de cette dérogation contribuent à l'objectif de protection de la nature mentionné à son article L. 511-1. Pour autant, lorsqu'elles lui apparaissent nécessaires pour assurer la protection des intérêts mentionnés à cet article, le préfet doit assortir l'autorisation d'exploiter qu'il délivre de prescriptions additionnelles. A cet égard, ce n'est que dans le cas où il estime, au vu d'une appréciation concrète de l'ensemble des caractéristiques de la situation qui lui est soumise et du projet pour lequel l'autorisation d'exploitation est sollicitée, que même l'édiction de telles prescriptions additionnelles ne permet pas d'assurer la conformité de l'exploitation aux dispositions de l'article L. 511-1 du code de l'environnement, qu'il ne peut légalement délivrer cette autorisation. <br/>
<br/>
              8. Par suite, en jugeant que le projet devait être regardé comme portant aux intérêts protégés par l'article L. 511-1 du code de l'environnement des atteintes qu'aucune prescription additionnelle ne permettrait d'éviter, sans préciser la teneur de ces atteintes, ni caractériser en quoi les prescriptions prévues par les arrêtés pris sur le fondement de l'article L. 411-2 du même code, complétées le cas échéant par des prescriptions supplémentaires, seraient insuffisantes pour les prévenir, la cour a commis une erreur de droit. <br/>
<br/>
              9. Il résulte de tout ce qui précède que la société Logiprest et la ministre de la transition écologique et solidaire sont fondées à demander l'annulation de l'arrêt de la cour administrative d'appel de Marseille qu'elles attaquent.<br/>
<br/>
              10. Dans les circonstances de l'espèce, il n'y a pas lieu de faire droit aux conclusions de la société Logiprest présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative. Par ailleurs, ces mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société Logiprest et de l'Etat qui ne sont pas, dans la présente instance, les parties perdantes.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 12 juillet 2019 de la cour administrative d'appel de Marseille est annulé.<br/>
Article 2 : Les affaires sont renvoyées à la cour administrative d'appel de Marseille.<br/>
Article 3 : Le surplus des conclusions du pourvoi de la société Logiprest est rejeté.<br/>
Article 4 : Les conclusions présentées par l'association Agir pour la Crau et autres au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la société Logiprest, à l'association Agir pour la Crau, première défenderesse dénommée, et à la ministre de la transition écologique.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
