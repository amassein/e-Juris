<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032629912</ID>
<ANCIEN_ID>JG_L_2014_12_000000370203</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/62/99/CETATEXT000032629912.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème SSJS, 18/12/2014, 370203, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-12-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>370203</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO</AVOCATS>
<RAPPORTEUR>M. Romain Victor</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:370203.20141218</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 15 juillet 2013 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre de l'éducation nationale, qui demande au Conseil d'Etat d'annuler le jugement n° 1202495 du 14 mai 2013 par lequel le tribunal administratif de Lille a annulé la décision du 9 février 2012 du recteur de l'académie de Lille refusant à M. A... le report sur l'année 2012 de ses congés annuels du 7 juillet 2011 au 27 août 2011 qu'il n' a pas pris du fait d'une mesure de suspension de ses fonctions ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 83-634 du 13 juillet 1983 ;<br/>
<br/>
              Vu la loi n° 84-16 du 11 janvier 1984 ; <br/>
<br/>
              Vu le décret n° 84-972 du 26 octobre 1984 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Romain Victor, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Le Prado, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. A..., attaché d'administration de l'éducation nationale et de l'enseignement supérieur, affecté au collège Paul Machy de Dunkerque, a été suspendu de ses fonctions par un arrêté du 21 juin 2011 du recteur de l'académie de Lille prenant effet le 4 juillet 2011 ; que par lettre du 9 février 2012, le recteur de l'académie de Lille a rejeté la demande de M. A... tendant au report en 2012 de ses jours de congés annuels initialement prévus du 7 juillet au 27 août 2011 mais non pris car intervenant pendant la période au cours de laquelle il était suspendu de l'exercice de ses fonctions ; que le ministre de l'éducation nationale se pourvoit en cassation contre le jugement du 14 mai 2013 par lequel le tribunal administratif de Lille a annulé la décision du recteur du 9 février 2012 ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 30 de la loi du 13 juillet 1983 portant droits et obligations des fonctionnaires : " En cas de faute grave commise par un fonctionnaire, qu'il s'agisse d'un manquement à ses obligations professionnelles ou d'une infraction de droit commun, l'auteur de cette faute peut être suspendu par l'autorité ayant pouvoir disciplinaire qui saisit, sans délai, le conseil de discipline. / Le fonctionnaire suspendu conserve son traitement, l'indemnité de résidence, le supplément familial de traitement et les prestations familiales obligatoires. Sa situation doit être définitivement réglée dans le délai de quatre mois. Si, à l'expiration de ce délai, aucune décision n'a été prise par l'autorité ayant pouvoir disciplinaire, l'intéressé, sauf s'il est l'objet de poursuites pénales, est rétabli dans ses fonctions (...) " ; qu'aux termes de l'article 34 de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat : " Le fonctionnaire en activité a droit : / 1° A un congé annuel avec traitement (...) " ; qu'aux termes du premier alinéa de l'article 5 du décret du 26 octobre 1984 relatif aux congés annuels des fonctionnaires de l'Etat : " Le congé dû pour une année de service accompli ne peut se reporter sur l'année suivante, sauf autorisation exceptionnelle donnée par le chef de service  " ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le recteur de l'académie de Lille a refusé de reporter en 2012 les congés annuels de M. A... non pris du 7 juillet au 27 août 2011 au motif que " la mesure de suspension dont il avait fait l'objet n'entrait pas dans le champ d'application de l'article 34 de la loi n° 84-16 du 11 janvier 1984 [...] autorisant une possible récupération ou report de congé non pris " ; qu'en jugeant qu'un tel motif ne pouvait légalement fonder cette décision de refus au regard des dispositions citées au point 2, le tribunal administratif de Lille n'a commis aucune erreur de droit ; que le ministre ne critique pas utilement le motif de droit retenu par le tribunal administratif en se bornant à faire valoir que M. A... avait bénéficié de ses congés annuels pendant la période de fermeture estivale du collège Paul Machy du 7 juillet au 27 août 2011 et qu'il n'avait pas perdu le bénéfice de ses congés en raison de la mesure de suspension dont il a fait l'objet ; que, par suite, il n'est pas fondé à demander l'annulation du jugement qu'il attaque ; <br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à M. A... au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du ministre de l'éducation nationale est rejeté.<br/>
Article 2 : L'Etat versera à M. A... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche et à M. B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
