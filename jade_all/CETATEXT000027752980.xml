<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027752980</ID>
<ANCIEN_ID>JG_L_2013_07_000000359654</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/75/29/CETATEXT000027752980.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  9ème sous-section jugeant seule, 25/07/2013, 359654, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-07-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>359654</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 9ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARTHELEMY, MATUCHANSKY, VEXLIARD</AVOCATS>
<RAPPORTEUR>M. Jean-Luc Matt</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:359654.20130725</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 24 mai et 24 août 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B... A..., demeurant... ; M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09MA01891 du 27 mars 2012 par lequel la cour administrative d'appel de Marseille a rejeté sa requête tendant à l'annulation du jugement n° 0701727 du 25 mars 2009 par lequel le tribunal administratif de Montpellier a rejeté sa demande tendant à la restitution des droits de taxe sur la valeur ajoutée qu'il a acquittés au titre de la période du 1er janvier 2004 au 27 décembre 2006 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement d'une somme de 3 000 euros au titre de l'article L.761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la directive n° 77/388/CEE du Conseil des Communautés européennes du 17 mai 1977 ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu la loi n° 2002-303 du 4 mars 2002 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Matt, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Barthélemy, Matuchansky, Vexliard, avocat de M. A...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. A..., qui exerce l'activité de chiropracteur, a demandé la restitution des droits de taxe sur la valeur ajoutée qu'il a spontanément acquittés au titre de la période du 1er janvier 2004 au 27 décembre 2006, en estimant pouvoir bénéficier des dispositions de l'article 261 du code général des impôts relatives à l'exonération de cette taxe ; qu'il se pourvoit en cassation contre l'arrêt du 27 mars 2012 par lequel la cour administrative d'appel de Marseille a rejeté sa requête tendant à l'annulation du jugement du 25 mars 2009 du tribunal administratif de Montpellier rejetant cette demande de restitution ;<br/>
<br/>
              2. Considérant, en premier lieu, qu'aux termes de l'article R. 194-1 du livre des procédures fiscales : " Lorsque, ayant donné son accord à la rectification ou s'étant abstenu de répondre dans le délai légal à la proposition de rectification, le contribuable présente cependant une réclamation faisant suite à une procédure contradictoire de rectification, il peut obtenir la décharge ou la réduction de l'imposition, en démontrant son caractère exagéré. / Il en est de même lorsqu'une imposition a été établie d'après les bases indiquées dans la déclaration souscrite par un contribuable (...) " ; qu'il résulte de ces dispositions qu'un contribuable ne peut obtenir la restitution de droits de taxe sur la valeur ajoutée qu'il a déclarés et spontanément acquittés conformément à ses déclarations qu'à la condition d'en établir le mal-fondé ;<br/>
<br/>
              3. Considérant, en deuxième lieu, qu'aux termes de l'article 13, A, paragraphe 1 de la sixième directive du Conseil du 17 mai 1977 en matière d'harmonisation des législations des Etats membres relatives aux taxes sur le chiffre d'affaires : " Sans préjudice d'autres dispositions communautaires, les Etats membres exonèrent, dans les conditions qu'ils fixent en vue d'assurer l'application correcte et simple des exonérations prévues ci-dessous et de prévenir toute fraude, évasion et abus éventuels : / (...) c) les prestations de soins à la personne effectuées dans le cadre de l'exercice des professions médicales et paramédicales telles qu'elles sont définies par l'État membre concerné (...) " ; qu'en vertu du 1° du 4 de l'article 261 du code général des impôts, dans sa rédaction applicable à la période d'imposition en litige, sont exonérés de la taxe sur la valeur ajoutée : " Les soins dispensés aux personnes par les membres des professions médicales et paramédicales réglementées (...) " ; qu'en limitant l'exonération qu'elles prévoient aux soins dispensés par les membres des professions médicales et paramédicales soumises à réglementation, ces dispositions ne méconnaissent pas l'objectif poursuivi par l'article 13, A, paragraphe 1, sous c) de la sixième directive, précité, qui est de garantir que l'exonération s'applique uniquement aux prestations de soins à la personne fournies par des prestataires possédant les qualifications professionnelles requises ; qu'en effet, la directive renvoie à la réglementation interne des États membres la définition de la notion de professions paramédicales, des qualifications requises pour exercer ces professions et des activités spécifiques de soins à la personne qui relèvent de telles professions ;<br/>
<br/>
              4. Considérant toutefois que, conformément à l'interprétation des dispositions de la sixième directive qui résulte de l'arrêt rendu le 27 avril 2006 par la Cour de justice des Communautés européennes dans les affaires C-443/04 et C-444/04, l'exclusion d'une profession ou d'une activité spécifique de soins à la personne de la définition des professions paramédicales retenue par la réglementation nationale aux fins de l'exonération de la taxe sur la valeur ajoutée prévue à l'article 13, A, paragraphe 1, sous c) de cette directive serait contraire au principe de neutralité fiscale inhérent au système commun de taxe sur la valeur ajoutée s'il pouvait être démontré que les personnes exerçant cette profession ou cette activité disposent, pour la fourniture de telles prestations de soins, de qualifications professionnelles propres à assurer à ces prestations un niveau de qualité équivalente à celles fournies par des personnes bénéficiant, en vertu de la réglementation nationale, de l'exonération ;<br/>
<br/>
              5. Considérant, en troisième lieu, qu'aux termes de l'article 75 de la loi du 4 mars 2002 relative aux droits des malades et à la qualité du système de santé, dans sa version applicable au présent litige : " L'usage professionnel du titre d'ostéopathe ou de chiropracteur est réservé aux personnes titulaires d'un diplôme sanctionnant une formation spécifique à l'ostéopathie ou à la chiropraxie délivrée par un établissement de formation agréé par le ministre chargé de la santé dans des conditions fixées par décret. Le programme et la durée des études préparatoires et des épreuves après lesquelles peut être délivré ce diplôme sont fixés par voie réglementaire. (...) / Les praticiens en exercice, à la date d'application de la présente loi, peuvent se voir reconnaître le titre d'ostéopathe ou de chiropracteur s'ils satisfont à des conditions de formation ou d'expérience professionnelle analogues à celles des titulaires du diplôme mentionné au premier alinéa. Ces conditions sont déterminées par décret. (...) / Un décret établit la liste des actes que les praticiens justifiant du titre d'ostéopathe ou de chiropracteur sont autorisés à effectuer, ainsi que les conditions dans lesquelles ils sont appelés à les accomplir. / Ces praticiens ne peuvent exercer leur profession que s'ils sont inscrits sur une liste dressée par le représentant de l'État dans le département de leur résidence professionnelle, qui enregistre leurs diplômes, certificats, titres ou autorisations " ;<br/>
<br/>
              6. Considérant que le décret du 7 janvier 2011 relatif aux actes et aux conditions d'exercice de la chiropraxie n'a été publié que le 9 janvier 2011 et que le décret du 20 septembre 2011 relatif à la formation des chiropracteurs et à l'agrément des établissements de formation en chiropraxie, ainsi que l'arrêté du même jour pris en application de ces deux décrets, n'ont été publiés que le 21 septembre 2011 ; que, durant la période allant du 1er janvier 2004 au 27 décembre 2006, les actes dits de chiropraxie ne pouvaient être pratiqués que par les docteurs en médecine, et le cas échéant, pour certains actes seulement et sur prescription médicale, par les autres professionnels de santé habilités à les réaliser ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que, pour statuer sur la restitution des droits de taxe sur la valeur ajoutée acquittés par M. A... sur ses prestations de chiropraxie, la cour devait vérifier que celui-ci démontrait disposer, pour la fourniture de ces prestations, de qualifications professionnelles propres à leur assurer un niveau de qualité équivalente à celles fournies, selon le cas, par un médecin ou par un membre d'une profession de santé réglementée ; qu'une telle appréciation ne peut être portée qu'au vu de la nature des actes accomplis sous la dénomination d'actes de chiropraxie et, s'agissant des actes susceptibles de comporter des risques en cas de contre-indication médicale, en considération des conditions dans lesquelles ils ont été effectués ;<br/>
<br/>
              8. Considérant qu'il appartenait, dès lors, à M. A..., pour mettre le juge à même de s'assurer que la condition tenant à la qualité des actes était remplie, de produire, d'une part, et sous réserve de l'occultation des noms des patients, des éléments relatifs à sa pratique permettant d'appréhender, sur une période significative, la nature des actes accomplis et les conditions dans lesquelles ils l'ont été et, d'autre part, tous éléments utiles relatifs à ses qualifications professionnelles ;<br/>
<br/>
              9. Considérant que la cour administrative d'appel de Marseille, qui a relevé que M. A... était titulaire d'un diplôme de chiropracteur, a jugé que les " fiches patients " qu'il produisait ne comportaient qu'une description de l'état général des intéressés sans apporter aucune précision sur la nature des actes accomplis pendant la période en litige ou les conditions dans lesquelles ils l'ont été, et que, par suite, il n'établissait pas que ces actes pouvaient être considérés comme d'une qualité équivalente à ceux qui, s'ils avaient été effectués par un médecin pratiquant la chiropraxie, auraient été exonérés de taxe sur la valeur ajoutée ; qu'en statuant ainsi alors que les copies anonymisées de " fiches patients " produites comprenaient une description succincte des manipulations pratiquées, la cour a dénaturé les pièces du dossier qui lui était soumis ; que pour ce motif et sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, M. A... est fondé à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              10. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              11. Considérant qu'il résulte de tout ce qui a été dit aux points 1 à 8 que, pour obtenir la restitution des droits de taxe sur la valeur ajoutée qu'il a spontanément acquittés au titre de la période du 1er janvier 2004 au 27 décembre 2006 à raison de ses prestations de chiropraxie, M. A... doit, pour mettre le juge à même de s'assurer que la condition de l'exonération de taxe tenant à la qualité de ces prestations était remplie, produire, d'une part, et sous réserve de l'occultation des noms des patients, des éléments relatifs à sa pratique permettant d'appréhender, sur une période significative, la nature des actes accomplis et les conditions dans lesquelles ils l'ont été et, d'autre part, tous éléments utiles relatifs à ses qualifications professionnelles ;<br/>
<br/>
              12. Considérant que M. A... produit des éléments pouvant être regardés comme attestant, de manière suffisante, la qualité de la formation qu'il a suivie et du diplôme qu'il a obtenu ; que pour permettre au juge de se prononcer sur la nature des actes accomplis et les conditions dans lesquelles ils ont été effectués, il lui appartient également de fournir des éléments permettant, sur une période significative d'au moins deux mois, de s'assurer que ces actes n'étaient pas interdits ou n'avaient pas été accomplis sans avis médical préalable lorsque celui-ci était requis ; qu'en l'espèce, M. A... se borne à produire trente-huit " fiches patients ", devant être regardées, faute d'être assorties de précisions suffisantes sur ce point, comme correspondant à un nombre équivalent de consultations, réparties sur une période de trente-quatre mois, alors que son chiffre d'affaires s'élevait au titre de la période en litige à plus de 280 000 euros ; qu'ainsi M. A... n'établit pas, par les pièces qu'il produit, que les actes de chiropraxie qu'il a accomplis au cours de la période litigieuse puissent être regardés comme d'une qualité équivalente à ceux qui, s'ils avaient été effectués par un médecin pratiquant la chiropraxie, auraient été exonérés de taxe sur la valeur ajoutée ;<br/>
<br/>
              13. Considérant qu'il résulte de tout ce qui précède que M. A... n'est pas fondé à soutenir que c'est à tort que, par le jugement qu'il attaque, le tribunal administratif de Dijon a rejeté sa demande ; que par suite, les conclusions qu'il présente au titre de l'article L. 761-1 du code de justice administrative ne peuvent qu'être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 27 mars 2012 est annulé.<br/>
Article 2 : La requête de M. A... présentée devant la cour administrative d'appel de Marseille et le surplus des conclusions du pourvoi de M. A... sont rejetés.<br/>
Article 3 : La présente décision sera notifiée à M. B... A...et au ministre délégué auprès du ministre de l'économie et des finances, chargé du budget.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
