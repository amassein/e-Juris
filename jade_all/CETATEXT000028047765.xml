<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028047765</ID>
<ANCIEN_ID>JG_L_2013_10_000000354125</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/04/77/CETATEXT000028047765.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère sous-section jugeant seule, 07/10/2013, 354125, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-10-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>354125</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>M. Rémi Decout-Paolini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:354125.20131007</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>VU LA PROCEDURE SUIVANTE :<br/>
<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 17 novembre 2011, 16 février 2012 et 16 octobre 2012 au secrétariat du contentieux du Conseil d'Etat, la fédération nationale des personnels des sociétés d'études de conseil et de prévention-CGT (FNPSECP-CGT), le syndicat national des professions judiciaires (SNPJ-CFDT), le syndicat national des techniciens et cadres du notariat (SNTCN-CFE-CGC) et la fédération des syndicats CFTC commerce, service et force de vente (CFTC-CSFV), représentés par la SCP Didier, Pinet, demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2011-1112 du 16 septembre 2011 relatif au régime spécial de retraite des clercs et employés de notaires ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Par un mémoire en défense, enregistré le 6 juin 2012, le ministre des affaires sociales et de la santé conclut au rejet de la requête.<br/>
<br/>
              Par un mémoire, enregistré le 7 juin 2012, le Premier ministre déclare faire siennes les observations présentées en défense par le ministre des affaires sociales et de la santé.<br/>
<br/>
              Vu : <br/>
              - les autres pièces du dossier ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
              Ont été entendus en séance publique :<br/>
<br/>
              - le rapport de M. Rémi Decout-Paolini, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public.<br/>
<br/>
              La parole a été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de la FNPSECP-CGT et autres.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>CONSIDERANT CE QUI SUIT :<br/>
<br/>
              Sur la légalité externe du décret attaqué :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 1 du code du travail : " Tout projet de réforme envisagé par le Gouvernement qui porte sur les relations individuelles et collectives du travail, l'emploi et la formation professionnelle et qui relève du champ de la négociation nationale et interprofessionnelle fait l'objet d'une concertation préalable avec les organisations syndicales de salariés et d'employeurs représentatives au niveau national et interprofessionnel en vue de l'ouverture éventuelle d'une telle négociation ". Le décret attaqué ne relevant pas du champ de la négociation nationale et interprofessionnelle, les organisations requérantes ne peuvent utilement invoquer la méconnaissance des dispositions de cet article.<br/>
<br/>
              Sur la légalité interne du décret attaqué :<br/>
<br/>
              2. Le décret n° 2008-147 du 15 février 2008 relatif au régime spécial de retraite des clercs et employés de notaires a fixé à 60 ans l'âge d'ouverture du droit à pension. Toutefois, pour les assurés nés avant le 1er janvier 1958 et justifiant de vingt-cinq années de versement de cotisation à la caisse de retraite et de prévoyance des clercs et employés de notaires (CRPCEN), des dispositions particulières étaient prévues, l'âge d'ouverture du droit à pension étant fixé à 55 ans pour les assurés nés avant le 1er juillet 1953 et relevé, à raison de six mois par semestre de naissance, pour les assurés nés entre le 1er juillet 1953 et le 31 décembre 1957. Le décret attaqué a porté à 62 ans l'âge d'ouverture du droit à pension pour les assurés de ce régime spécial nés à compter du 1er janvier 1962. Il a prévu des dispositions particulières pour les assurés nés entre le 1er janvier 1955 et le 31 décembre 1956 et justifiant de vingt-cinq années de versement de cotisation, dont l'âge d'ouverture, porté progressivement à 60 ans, est relevé à raison non plus de six mois mais de neuf mois par semestre de naissance, ainsi que pour les assurés nés entre le 1er janvier 1957 et le 31 décembre 1961, dont l'âge d'ouverture est porté progressivement de 60 à 62 ans, à raison de quatre mois par année de naissance.<br/>
<br/>
              En ce qui concerne la méconnaissance du principe d'égalité :<br/>
<br/>
              3. Le principe d'égalité ne s'oppose pas à ce que l'autorité investie du pouvoir réglementaire règle de façon différente des situations différentes ni à ce qu'elle déroge à l'égalité pour des raisons d'intérêt général, pourvu que, dans l'un comme l'autre cas, la différence de traitement qui en résulte soit en rapport direct avec l'objet de la norme qui l'établit et ne soit pas manifestement disproportionnée au regard des motifs susceptibles de la justifier.<br/>
<br/>
              4. En premier lieu, les régimes de retraite forment chacun un ensemble dont les dispositions ne peuvent être envisagées isolément. Par conséquent, le principe d'égalité ne s'oppose pas à ce que les personnels affiliés à un régime de retraite soient soumis à des mesures de relèvement d'âge différentes de celles auxquelles sont soumis les personnels relevant d'un autre régime.<br/>
<br/>
              5. En second lieu, la différence de traitement qui résulte de la succession de régimes juridiques dans le temps n'est pas, en elle-même, contraire au principe d'égalité. En outre, le critère de la date de naissance est en rapport direct avec l'objet des dispositions litigieuses, destinées à assurer la pérennité du régime spécial de retraite des clercs et employés de notaires en relevant à 62 ans l'âge d'ouverture du droit à pension pour des assurés du régime. Enfin, le respect du principe d'égalité n'obligeait pas à traiter différemment, en les exceptant de la nouvelle réforme, les assurés sociaux dont la situation avait déjà été modifiée par le décret du 15 février 2008.  Par suite, en adoptant les dispositions critiquées, qui relèvent l'âge d'ouverture du droit à pension en assurant une entrée en vigueur progressive de la réforme en fonction du semestre ou de l'année de naissance, le pouvoir réglementaire n'a pas porté atteinte au principe d'égalité devant la loi ni, en tout état de cause, au principe d'égalité devant les charges publiques.<br/>
<br/>
              En ce qui concerne la méconnaissance du principe de sécurité juridique :<br/>
<br/>
              6. L'article 2 du décret attaqué, publié au Journal officiel du 18 septembre 2011, produit ses effets à compter du 1er janvier 2012. Il prévoit, ainsi qu'il a été indiqué, un relèvement progressif de l'âge d'ouverture du droit à pension, qui passe de 57 ans à 57 ans et trois mois pour les assurés nés au premier semestre 1955 et justifiant de vingt-cinq années de cotisations, qui pouvaient espérer prendre leur retraite au premier semestre 2012, et qui est porté à 62 ans à compter du 1er janvier 2024, pour les assurés nés à compter du 1er janvier 1962. Les organisations requérantes ne sont pas fondées à soutenir que le pouvoir réglementaire, en prévoyant de telles modalités d'entrée en vigueur, aurait méconnu le principe de sécurité juridique.<br/>
<br/>
              En ce qui concerne les autres moyens :<br/>
<br/>
              7. Les organisations requérantes ne peuvent utilement invoquer à l'encontre du décret attaqué les dispositions de la loi n° 2010-1330 du 9 novembre 2010 portant réforme des retraites, qui ne régit pas l'âge d'ouverture du droit à pension dans les régimes spéciaux, non plus que l'" engagement ", sans portée normative, figurant dans le " document d'orientation " sur la réforme des retraites de 2010 et relatif au calendrier de cette réforme.<br/>
<br/>
              8. Il résulte de tout ce qui précède que la fédération nationale des personnels des sociétés d'études de conseil et de prévention-CGT et autres ne sont pas fondés à demander l'annulation du décret attaqué. Leurs conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la fédération nationale des personnels des sociétés d'études de conseil et de prévention-CGT (FNPSECP-CGT), du syndicat national des professions judiciaires (SNPJ-CFDT), du syndicat national des techniciens et cadres du notariat (SNTCN-CFE-CGC) et de la fédération des syndicats CFTC commerce, service et force de vente (CFTC-CSFV) est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la fédération nationale des personnels des sociétés d'études de conseil et de prévention-CGT (FNPSECP-CGT), première requérante dénommée, au Premier ministre et à la ministre des affaires sociales et de la santé.<br/>
Les autres requérants seront informés de la présente décision par la SCP Didier, Pinet, avocat au Conseil d'Etat et à la Cour de cassation, qui les représente devant le Conseil d'Etat.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
