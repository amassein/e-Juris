<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025822321</ID>
<ANCIEN_ID>JG_L_2012_05_000000341110</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/82/23/CETATEXT000025822321.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 07/05/2012, 341110</TITRE>
<DATE_DEC>2012-05-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>341110</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Christian Vigouroux</PRESIDENT>
<AVOCATS/>
<RAPPORTEUR>M. Alexandre Aïdara</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:341110.20120507</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le recours, enregistré le 2 juillet 2010 au secrétariat du contentieux du Conseil d'Etat, présenté par l'ORDRE DES AVOCATS DU BARREAU DE VERSAILLES, domicilié au Palais de justice, 3 place André Mignot à Versailles (78000), représenté par son bâtonnier en exercice ; il demande au Conseil d'Etat d'annuler pour excès de pouvoir la circulaire référencée SJ-09-451-AB4 du 24 décembre 2009 du ministre d'Etat, garde des sceaux, ministre de la justice et des libertés relative aux conséquences des dispositions du code général de la propriété des personnes publiques sur la situation des tiers occupants dans les palais de justice, ensemble la décision du 7 mai 2010 du ministre de la justice rejetant son recours gracieux en date du 22 février 2010 tendant au retrait de cette circulaire ; <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général de la propriété des personnes publiques ;<br/>
<br/>
              Vu la loi n° 71-1130 du 31 décembre 1971 ;<br/>
<br/>
              Vu le décret n° 2005-850 du 27 juillet 2005 ;<br/>
<br/>
              Vu l'arrêté du 19 novembre 2008 relatif à la protection contre les risques d'incendie et de panique des établissements de l'ordre judiciaire ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alexandre Aïdara, Maître des Requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant, en premier lieu, que, par arrêté du 26 juin 2009 publié au Journal officiel du 1er juillet 2009, le directeur de cabinet du garde des sceaux, ministre de la justice et des libertés, a reçu délégation de signature pour signer, au nom du ministre,  tous actes, arrêtés et décisions, à l'exclusion des décrets en ce qui concerne les affaires pour lesquelles délégation n'a pas été donnée aux personnes mentionnées à l'article 1er du décret du 27 juillet 2005 ; que par suite le moyen de l'ORDRE DES AVOCATS DU BARREAU DE VERSAILLES, qui se borne à soutenir que le signataire de la circulaire attaquée ne justifie pas d'une délégation de signature, n'est pas fondé ;<br/>
<br/>
              Considérant, en deuxième lieu, que la circulaire attaquée du 24 décembre 2009, qui contient des dispositions impératives, a pour objet de préciser les conséquences des dispositions du code général de la propriété des personnes publiques sur la situation des tiers occupants dans les palais de justice ; <br/>
<br/>
              Considérant que le requérant soutient que cette circulaire, qui modifie la situation juridique des ordres des avocats à l'égard des locaux qu'ils occupent dans les palais de justice,  est illégale dès lors qu'elle a pour effet de prévoir que le domaine public ne peut être occupé qu'à titre onéreux  et qu'en ajoutant à la loi, elle revêt par suite un caractère réglementaire et serait en conséquence entachée d'incompétence ;<br/>
<br/>
              Considérant qu'aux termes de l'article L. 2125-1 du code général de la propriété des personnes publiques dans sa rédaction applicable au litige : " Toute occupation ou utilisation du domaine public d'une personne publique mentionnée à l'article L. 1 donne lieu au paiement d'une redevance sauf lorsque l'occupation ou l'utilisation concerne l'installation par l'Etat des équipements visant à améliorer la sécurité routière. / Par dérogation aux dispositions de l'alinéa précédent, l'autorisation d'occupation ou d'utilisation du domaine public peut être délivrée gratuitement : / 1° Soit lorsque l'occupation ou l'utilisation est la condition naturelle et forcée de l'exécution de travaux ou de la présence d'un ouvrage, intéressant un service public qui bénéficie gratuitement à tous ; / 2° Soit lorsque l'occupation ou l'utilisation contribue directement à assurer la conservation du domaine public lui-même. / En outre, l'autorisation d'occupation ou d'utilisation du domaine public peut être délivrée gratuitement aux associations à but non lucratif qui concourent à la satisfaction d'un intérêt général. " ; que même si la circulaire attaquée ne réitère pas les exceptions, relatives à l'occupation à titre gratuit, énumérées par cet article, il ne résulte d'aucun de ses termes qu'elle affirme le principe selon lequel le domaine public ne peut être concédé qu'à titre onéreux et qu'elle aurait ainsi ajouté à la loi ;  <br/>
<br/>
              Considérant, en troisième lieu,  que le requérant fait valoir que la circulaire est entachée d'erreur de droit pour ne pas avoir précisé que l'occupation des locaux des palais de justice par les ordres des avocats était gratuite dès lors que des missions d'intérêt général leurs sont confiées par la loi ; que si l'ordre des avocats concourt à certaines missions d'intérêt général qui lui ont été dévolues par la loi du 31 décembre 1971 portant réforme de certaines professions judiciaires et juridiques modifiée, il n'est pas au nombre des " associations à but non lucratif " auxquelles les dispositions de l'article L. 2125-1 du code général de la propriété des personnes publiques réservent la possibilité de bénéficier d'une autorisation d'occupation du domaine public à titre gratuit ;   <br/>
<br/>
              Considérant, enfin, que le requérant soutient que la circulaire est illégale dès lors que la convention-type de répartition des charges avec les tiers occupants qui y est annexée prévoit au point 5.2 que la totalité des locaux concédés doit être libre d'accès à tout moment au chef d'établissement ou à son représentant et qu'elle porterait ainsi atteinte aux principes d'indépendance et du secret professionnel, consubstantiels à la profession d'avocat ; que toutefois, il n'assortit pas ce moyen de précisions suffisante permettant d'en apprécier le bien-fondé ; qu'au demeurant les dispositions de cette convention ont pour objet le respect des prescriptions de l'arrêté du 19 novembre 2008 relatif à la protection contre les risques d'incendie et de panique des établissements de l'ordre judiciaire ; <br/>
<br/>
              Considérant qu'il suit de là que l'ORDRE DES AVOCATS DU BARREAU DE VERSAILLES n'est pas fondé à demander l'annulation de cette circulaire, ensemble la décision du ministre du 7 mai 2010 rejetant son recours gracieux en date du 22 février 2010 tendant à son retrait ;<br/>
<br/>
<br/>
<br/>		D E C I D E :<br/>
              		--------------<br/>
<br/>
Article 1er : La requête de l'ORDRE DES AVOCATS DU BARREAU DE VERSAILLES en date du 22 février 2010 est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à l'ORDRE DES AVOCATS DU BARREAU DE VERSAILLES et au garde des sceaux, ministre de la justice et des libertés.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-02-05-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - COMPÉTENCE. DÉLÉGATIONS, SUPPLÉANCE, INTÉRIM. DÉLÉGATION DE SIGNATURE. - DÉLÉGATION DONNÉE À UN DIRECTEUR DE CABINET POUR SIGNER, AU NOM DU MINISTRE, TOUS ACTES, À L'EXCLUSION DES DÉCRETS, EN CE QUI CONCERNE LES AFFAIRES POUR LESQUELLES DÉLÉGATION N'A PAS ÉTÉ DONNÉE AUX PERSONNES MENTIONNÉES À L'ARTICLE 1ER DU DÉCRET DU 27 JUILLET 2005 - CONSÉQUENCE - COMPÉTENCE DE CE DIRECTEUR POUR SIGNER UNE CIRCULAIRE PORTANT SUR UNE MATIÈRE INTÉRESSANT PLUS D'UNE SEULE DIRECTION D'ADMINISTRATION CENTRALE - EXISTENCE [RJ1].
</SCT>
<ANA ID="9A"> 01-02-05-02 Un directeur de cabinet ministériel ayant, en vertu de l'article 2 du décret n° 2005-850 du 27 juillet 2005 relatif aux délégations de signature des membres du Gouvernement, reçu régulièrement délégation de signature pour signer, au nom du ministre, tous actes, à l'exclusion des décrets, en ce qui concerne les affaires pour lesquelles délégation n'a pas été donnée aux personnes mentionnées à l'article 1er du même décret, peut légalement signer une circulaire qui porte sur une matière qui intéresse plus d'une seule direction d'administration centrale (sol. impl.).</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. sol. contr. CE, 4 juin 2007, Ligue de l'Enseignement et autres, n°s 289792 290183, T. pp. 643-998.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
