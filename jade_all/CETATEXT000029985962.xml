<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029985962</ID>
<ANCIEN_ID>JG_L_2014_12_000000369027</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/98/59/CETATEXT000029985962.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  4ème sous-section jugeant seule, 29/12/2014, 369027, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-12-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>369027</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 4ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Benjamin de Maillard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Keller</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:369027.20141229</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 4 juin 2013 au secrétariat du contentieux du Conseil d'Etat, présentée par la société Forum Kinepolis, dont le siège est 130 rue Michel Debré, ZAC Mas des Abeilles, à Nîmes (30900) ; la société Forum Kinepolis demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision n° 200 du 7 février 2013 par laquelle la Commission nationale d'aménagement commercial, statuant en matière cinématographique, a, d'une part, rejeté son recours dirigé contre la décision du 25 septembre 2012 de la commission départementale d'aménagement commercial du Gard statuant en matière cinématographique, et d'autre part, a accordé à la société Cap cinéma Nîmes l'autorisation préalable requise en vue de créer 10 salles et 1 662 places à l'enseigne "Cap Cinéma" à Nîmes (Gard) ; <br/>
<br/>
              2°) de mettre à la charge de la société Cap cinéma Nîmes le versement de la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de commerce ;<br/>
<br/>
              Vu le code du cinéma et de l'image animée ;<br/>
<br/>
              Vu la loi n° 2008-776 du 4 août 2008 ;<br/>
<br/>
              Vu le décret n° 2008-1212 du 24 novembre 2008 ;<br/>
<br/>
              Vu le décret n° 2009-1393 du 11 novembre 2009 ;<br/>
<br/>
              Vu le décret n° 2011-921 du 1er août 2011 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Benjamin de Maillard, auditeur,  <br/>
<br/>
              - les conclusions de M. Rémi Keller, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Sur la légalité externe de la décision attaquée :<br/>
<br/>
              1. Considérant que si la société requérante soutient que l'avis conjoint du ministre chargé de l'égalité des territoires et du ministre chargé de l'écologie n'est pas signé et que seul le courrier de transmission de cet avis est signé, il ressort clairement des termes de ce courrier, signé par une personne ayant reçu régulièrement délégation de signature, que l'avis joint est celui de ces deux ministres ; <br/>
<br/>
              Sur la légalité interne de la décision attaquée :<br/>
<br/>
              En ce qui concerne la composition du dossier de la demande d'autorisation :<br/>
<br/>
              2. Considérant, d'une part, qu'aux termes de l'article R. 752-6 du code de commerce : " La demande d'autorisation prévue à l'article L. 752-1 et à l'article 30-2 du code de l'industrie cinématographique est présentée (...) par une personne justifiant d'un titre l'habilitant à construire sur le terrain ou à exploiter commercialement l'immeuble " ; qu'il ressort des pièces du dossier que la société d'aménagement des territoires, propriétaire de la parcelle d'implantation du projet, a autorisé la société Cap cinéma, dont la société Cap cinéma Nîmes est une filiale, à déposer un dossier de demande ; qu'ainsi, contrairement à ce que soutient la société requérante, la société pétitionnaire justifiait d'un titre lui permettant de présenter sa demande ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier que le pétitionnaire a délimité une zone d'influence cinématographique correspondant à un temps d'accès en voiture de trente minutes ; que la société requérante n'apporte aucun élément probant de nature à établir que la délimitation de la zone par le pétitionnaire aurait été erronée et que cette délimitation aurait été de nature à fausser l'appréciation portée par la Commission nationale d'aménagement commercial ; <br/>
<br/>
              4. Considérant, que, contrairement à ce que soutient la société requérante, le dossier soumis par la société pétitionnaire comportaient des éléments suffisants, notamment s'agissant de la desserte par les " modes de déplacement doux ", des transports en commun, de la desserte routière et des stationnements, pour permettre à la commission nationale de se prononcer de façon éclairée sur la demande dont elle était saisie ; <br/>
<br/>
              En ce qui concerne l'appréciation de la Commission nationale d'aménagement commercial :<br/>
<br/>
              5. Considérant qu'aux termes de l'article 30-1 du code de l'industrie cinématographique, résultant de l'article 105 de la loi du 4 août 2008 de modernisation de l'économie, dans la rédaction applicable à la date de la décision attaquée, depuis lors codifié à l'article L. 212-6 du code du cinéma et de l'image animée : " Les créations, extensions et réouvertures au public d'établissements de spectacles cinématographiques doivent répondre aux exigences de diversité de l'offre cinématographique, d'aménagement culturel du territoire, de protection de l'environnement et de qualité de l'urbanisme, en tenant compte de la nature spécifique des oeuvres cinématographiques. Elles doivent contribuer à la modernisation des établissements de spectacles cinématographiques et à la satisfaction des intérêts du spectateur tant en ce qui concerne la programmation d'une offre diversifiée que la qualité des services offerts " ; qu'aux termes de l'article 30-3 du même code, désormais codifié à l'article L. 212-9 du code du cinéma et de l'image animée : " Dans le cadre des principes définis à l'article 30-1, la Commission d'aménagement commercial statuant en matière cinématographique se prononce sur les deux critères suivants : / 1° L'effet potentiel sur la diversité cinématographique offerte aux spectateurs dans la zone d'influence cinématographique concernée, évalué au moyen des indicateurs suivants : / a) Le projet de programmation envisagé pour l'établissement de spectacles cinématographiques objet de la demande d'autorisation et, le cas échéant, le respect des engagements de programmation éventuellement contractés en application de l'article 90 de la loi n° 82-52 du 29 juillet 1982 sur la communication audiovisuelle ; / b) La nature et la diversité culturelle de l'offre cinématographique proposée dans la zone concernée, compte tenu de la fréquentation cinématographique ; / c) La situation de l'accès des oeuvres cinématographiques aux salles et des salles aux oeuvres cinématographiques pour les établissements de spectacles cinématographiques existants ; / 2° L'effet du projet sur l'aménagement culturel du territoire, la protection de l'environnement et la qualité de l'urbanisme, évalué au moyen des indicateurs suivants : / a) L'implantation géographique des établissements de spectacles cinématographiques dans la zone d'influence cinématographique et la qualité de leurs équipements ; / b) La préservation d'une animation culturelle et le respect de l'équilibre des agglomérations ; / c) La qualité environnementale appréciée en tenant compte des différents modes de transports publics, de la qualité de la desserte routière, des parcs de stationnement ; / d) L'insertion du projet dans son environnement ; / e) La localisation du projet. " ;<br/>
<br/>
              6. Considérant qu'il résulte de ces dispositions que l'autorisation d'aménagement commercial ne peut être refusée que si, eu égard à ses effets, le projet d'équipement cinématographique contesté compromet la réalisation des objectifs et principes énoncés par la loi ; qu'il appartient aux commissions d'aménagement commercial statuant en matière cinématographique, lorsqu'elles se prononcent sur les dossiers de demande d'autorisation, d'apprécier la conformité du projet à ces objectifs et principes, au vu des critères d'évaluation et indicateurs mentionnés à l'article 30-3 du code de l'industrie cinématographique, codifié ainsi qu'il a été dit ci-dessus à l'article L. 212-9 du code du cinéma et de l'image animée, parmi lesquels ne figure plus la densité d'équipements en salles de spectacles cinématographiques dans la zone d'attraction du projet ;<br/>
<br/>
              7. Considérant qu'il ressort des pièces du dossier que l'impact du projet pour le cinéma " le Sémaphore ", classé " art et essai ", sera limité, compte tenu du caractère essentiellement généraliste de la programmation du nouveau cinéma, et que le projet permettra de diversifier l'offre cinématographique dans l'agglomération nîmoise, en renforçant la part aujourd'hui faible de cette offre située près du centre-ville ; que, par suite, le moyen tiré de ce que le projet compromettrait l'objectif de diversité cinématographique peut être écarté ;<br/>
<br/>
              8. Considérant que la circonstance que le projet devra comprendre une modification technique en raison des prescriptions posées par le plan de prévention du risque d'inondation de Nîmes est sans influence sur la légalité de la décision d'autorisation prise par la commission nationale ; qu'il ressort des pièces du dossier que l'implantation du projet à proximité du centre-ville de Nîmes contribuera à redynamiser le quartier de la gare centrale de Nîmes et que son insertion dans les réseaux de transport est satisfaisante ; qu'il contribuera ainsi à l'aménagement culturel du territoire ;<br/>
<br/>
              9. Considérant qu'il résulte de ce qui précède que la société Forum Kinepolis n'est pas fondée à demander l'annulation de la décision attaquée ; <br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              10. Considérant que ces dispositions font obstacle à ce que la somme que demande la société Forum Kinepolis soit mise à la charge de la société Cap cinéma Nîmes qui n'est pas, dans la présente instance, la partie perdante ; qu'en revanche , il y a lieu de mettre à la charge de la société Forum Kinepolis le versement de la somme de 5 000 euros à la société Cap cinéma Nîmes au titre de ces mêmes dispositions ; <br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La requête de la société Forum Kinepolis est rejetée.<br/>
Article 2 : La société Forum Kinepolis versera la somme de 5 000 euros à la société Cap cinéma Nîmes au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société Forum Kinepolis, à la société Cap cinéma Nîmes et à la Commission nationale d'aménagement commercial.<br/>
Copie en sera adressée pour information au médiateur du cinéma et à la ministre de la culture et de la communication.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
