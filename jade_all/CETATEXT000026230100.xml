<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026230100</ID>
<ANCIEN_ID>JG_L_2012_07_000000338536</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/23/01/CETATEXT000026230100.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  9ème sous-section jugeant seule, 27/07/2012, 338536, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-07-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>338536</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 9ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Jean-Pierre Jouguelet</PRESIDENT>
<AVOCATS/>
<RAPPORTEUR>M. Matthieu Schlesinger</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:338536.20120727</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi du ministre du budget, des comptes publics, de la fonction publique et de la réforme de l'Etat, porte-parole du Gouvernement, enregistré le 9 avril 2010 au secrétariat du contentieux du Conseil d'Etat ; le ministre du budget, des comptes publics, de la fonction publique et de la réforme de l'Etat, porte-parole du Gouvernement demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09NC01133 du 4 mars 2010 par lequel la cour administrative d'appel de Nancy a rejeté son recours tendant, d'une part, à l'annulation du jugement n° 0800799 du 26 mars 2009 par lequel le tribunal administratif de Besançon a prononcé la réduction de taxe professionnelle à laquelle la SA Technotime Microtechnique a été assujettie au titre de l'année 2006 dans les rôles de la commune de Valdahon (Doubs) et, d'autre part, à ce que l'imposition contestée soit remise à la charge de cette société ;<br/>
<br/>
              2°) réglant l'affaire au fond, de remettre à la charge de la SA Technotime Microtechnique les cotisations de taxe professionnelle en litige ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre de procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Matthieu Schlesinger, Auditeur,  <br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article 1465 du code général des impôts, dans sa rédaction applicable au litige : " Dans les zones définies par l'autorité compétente où l'aménagement du territoire le rend utile, les collectivités locales et leurs groupements dotés d'une fiscalité propre peuvent, par une délibération de portée générale, exonérer de la taxe professionnelle en totalité ou en partie les entreprises qui procèdent sur leur territoire, soit à des décentralisations, extensions ou créations d'activités industrielles ou de recherche scientifique et technique, ou de services de direction, d'études, d'ingénierie et d'informatique, soit à une reconversion dans le même type d'activités, soit à la reprise d'établissements en difficulté exerçant le même type d'activités. Cette délibération ne peut avoir pour effet de reporter de plus de cinq ans l'application du régime d'imposition de droit commun (...) " ; que par une délibération du 25 mars 1996, le conseil régional de Franche-Comté a décidé d'accorder, en application de ces dispositions, aux entreprises éligibles établies sur son territoire une exonération de la totalité de la part régionale de la taxe professionnelle ; que cette délibération précisait que l'exonération s'appliquait " pour une durée de 5 ans (année de droit comprise) " ;<br/>
<br/>
              2. Considérant qu'il résulte des pièces du dossier soumis aux juges du fond que, consécutivement à un plan de cession arrêté par jugement du tribunal de commerce de Besançon du 13 juin 2000, l'activité de la SAS France Ebauches a été reprise en mai 2001 par la SAS Technotime, créée à cette fin ; que cette dernière a bénéficié, pour les années 2002 à 2005, d'une exonération de la part régionale de la taxe professionnelle en application des dispositions précitées de l'article 1465 du code général des impôts et de la délibération du 25 mars 1996 du conseil régional de Franche-Comté ; qu'au titre de l'année 2006, la société a été assujettie à une cotisation de taxe professionnelle comportant la part régionale ; que le ministre du budget, des comptes publics et de la réforme de l'Etat se pourvoit en cassation contre l'arrêt du 9 avril 2010 par lequel la cour administrative de Nancy a confirmé le jugement du tribunal administratif de Besançon du 26 mars 2009 accordant à la société la décharge, qu'elle avait sollicitée, de la part régionale de sa cotisation de taxe professionnelle pour 2006 ;<br/>
<br/>
              3. Considérant qu'il résulte des mentions de la délibération du 26 mars 1996 précitée qu'en disposant que l'exonération porterait sur une durée de 5 ans " année de droit comprise ", le conseil régional, qui s'est ainsi borné à reconduire le système d'exonération de taxe professionnelle antérieur régi par une délibération du 3 décembre 1982, a entendu octroyer l'exonération en cause pour une durée de cinq années incluant celle au cours de laquelle l'établissement a été créé ou repris, au titre de laquelle l'entreprise n'est pas redevable de la taxe professionnelle en application des règles de droit commun fixées à l'article 1478 du code général des impôts ; que l'exonération découlant des dispositions combinées de l'article 1465 du code général des impôts et de la délibération du 26 mars 1996 cesse ainsi de produire ses effets à l'issue de la quatrième année suivant celle de la création ou de la reprise de l'établissement ; qu'il en résulte qu'en jugeant que le conseil régional avait entendu exonérer les entreprises éligibles pour une durée de cinq ans à compter du premier janvier de l'année suivant l'évènement justifiant l'exonération, à savoir la création ou la reprise de l'établissement, la cour administrative d'appel de Nancy a commis une erreur de droit ; que son arrêt doit, pour ce motif, être annulé ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce de faire application des dispositions de l'article L. 821-2 du code de justice administrative et de régler l'affaire au fond ;<br/>
<br/>
              5. Considérant que l'interprétation de la délibération du conseil régional de Franche-Comté du 25 mars 1996 sur laquelle se sont fondés les premiers juges et que la cour administrative de Nancy a adoptée, ne saurait, pour les motifs précités, justifier la décharge des cotisations de taxe professionnelle en litige ;<br/>
<br/>
              6. Considérant que si la société Technotime Microtechnique se prévaut, sur le fondement de l'article L. 80 B du livre des procédures fiscales, des indications que lui aurait fournies l'administration dans un courrier que celle-ci lui a adressé le 30 mars 2001 et dans la décision du 19 décembre 2002 par laquelle l'agrément nécessaire au bénéfice de l'exonération temporaire de taxe professionnelle lui a été accordé, il ressort de ces documents, dont le premier se borne à donner une information générale sur la durée globale des exonérations de taxe professionnelle décidées par les collectivités territoriales et le second à rappeler la date à laquelle prend effet l'exonération, qu'ils ne constituent pas une prise de position formelle sur l'appréciation de la situation de fait de l'entreprise au regard d'un texte fiscal, au sens de l'article L. 80 B ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que le ministre du budget, des comptes publics et de la fonction publique est fondé à demander l'annulation du jugement qu'il attaque ; que la demande de la SAS Technotime Microtechnique doit, par suite, être rejetée ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt du 4 mars 2010 de la cour administrative d'appel de Nancy est annulé.<br/>
Article 2 : Le jugement du 26 mars 2009 du tribunal administratif de Besançon est annulé.<br/>
Article 3 : La demande de la SAS Technotime Microtechnique est rejetée.<br/>
Article 4 : La présente décision sera notifiée au ministre de l'économie et des finances et à la société Technotime Microtechnique.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
