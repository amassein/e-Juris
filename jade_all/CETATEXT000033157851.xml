<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033157851</ID>
<ANCIEN_ID>JG_L_2016_09_000000393252</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/15/78/CETATEXT000033157851.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 26/09/2016, 393252, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-09-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>393252</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Grégory Rzepski</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:393252.20160926</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés au secrétariat du contentieux du Conseil d'Etat les 7 septembre, 7 décembre 2015 et 6 septembre 2016, la Fédération CGT de l'éducation, de la recherche et de la culture (FERC-CGT), le Syndicat national des travailleurs de la recherche scientifique CGT (SNTRS-CGT), la FERC SUP CGT, le syndicat CGT de la Recherche Agronomique (CGT-INRA), la Confédération générale du travail Educ'action (CGT Educ'action), la Confédération générale du travail (CGT), l'Union nationale des syndicats CGT des CROUS (UN-CGT-CROUS) et l'Union générale des fédérations de fonctionnaires CGT (UGFF-CGT) demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2015-825 du 6 juillet 2015 relatif aux modalités de calcul et de répartition du crédit de temps syndical au ministère de l'éducation nationale, de l'enseignement supérieur et de la recherche ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 7 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule ;<br/>
              - la Charte sociale européenne ; <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - le décret n° 82-447 du 28 mai 1982 ;<br/>
              - le décret n° 2011-184 du 15 février 2011 ;<br/>
              - le décret n° 2012-225 du 16 février 2012 ;<br/>
              - le décret n° 2014-402 du 16 avril 2014 ;<br/>
              - le décret du 26 août 2014 relatif à la composition du gouvernement ;<br/>
              - le décret n° 2014-1092 du 26 septembre 2014 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Grégory Rzepski, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de la Fédération CGT de l'éducation, de la recherche et de la culture (FERC-CGT), du Syndicat national des travailleurs de la recherche scientifique CGT, (SNTRS-CGT), du syndicat CGT de la recherche agronomique (CGT-INRA), de la Confédération générale du travail Educ'action, de la Confédération générale du travail, de l'Union nationale des syndicats CGT des Crous et l'Union Générale des Fédérations de Fonctionnaires CGT (UGFF-CGT) ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du I de l'article 16 du décret du 28 mai 1982 : " Un crédit de temps syndical, utilisable sous forme de décharges de service ou de crédits d'heure selon les besoins de l'activité syndicale, est déterminé, au sein de chaque département ministériel, à l'issue du renouvellement général des comités techniques. Son montant global, exprimé en effectifs décomptés en équivalents temps plein, est calculé en fonction d'un barème appliqué aux effectifs (...) " ; qu'aux termes du dernier alinéa du II du même article : " Les effectifs pris en compte correspondent au nombre des électeurs inscrits sur les listes électorales pour l'élection au comité technique ministériel " ; qu'aux termes du premier alinéa de l'article 3 du décret du 15 février 2011 : " Dans chaque département ministériel, un comité technique ministériel est créé auprès du ministre par arrêté du ministre intéressé " ; qu'aux termes du I de l'article 1er du décret du 26 septembre 2014 : " Par dérogation au premier alinéa de l'article 3 du décret du 15 février 2011 susvisé, il est institué auprès de la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche : 1° Un comité technique ministériel, dénommé comité technique ministériel de l'éducation nationale, compétent pour examiner les questions intéressant les services centraux et les services déconcentrés relevant de l'éducation nationale ainsi que les services d'administration centrale relevant conjointement de l'éducation nationale et de l'enseignement supérieur et de la recherche./ (...) 2° Un comité technique ministériel, dénommé comité technique ministériel de l'enseignement supérieur et de la recherche, compétent pour examiner les questions intéressant les services relevant de l'enseignement supérieur et de la recherche " ; qu'aux termes de l'article 1er du décret attaqué : " Par dérogation aux dispositions de l'article 16 du décret du 28 mai 1982 susvisé, les effectifs pris en compte pour le calcul du contingent de crédit de temps syndical au ministère de l'éducation nationale, de l'enseignement supérieur et de la recherche correspondent au cumul du nombre des électeurs inscrits sur les listes électorales pour l'élection au comité technique ministériel de l'éducation nationale et du nombre des électeurs inscrits sur les listes électorales pour l'élection au comité technique ministériel de l'enseignement supérieur et de la recherche, créés par le décret du 26 septembre 2014 (...) " ; <br/>
<br/>
              Sur la légalité externe :<br/>
<br/>
              2. Considérant qu'aux termes du I de l'article 2 du décret du 16 février 2012 : " Le Conseil supérieur de la fonction publique de l'Etat est saisi pour avis :/ (...) 4° Des projets de décret relatifs à la situation de l'ensemble des agents publics de l'Etat ;/ 5° Des projets de décret comportant des dispositions statutaires communes à plusieurs corps de fonctionnaires de l'Etat lorsque ces projets relèvent de la compétence de plusieurs comités techniques ;/ (...) " ; que le décret attaqué a pour objet de préciser les règles de calcul du contingent global de crédit de temps syndical au ministère de l'éducation nationale, de l'enseignement supérieur et de la recherche ; qu'il n'est pas relatif à la situation de l'ensemble des agents publics de l'Etat et ne comporte aucune disposition statutaire ; que, par suite, il n'avait pas à être pris après avis du Conseil supérieur de la fonction publique de l'Etat, alors même qu'il déroge aux dispositions du décret précité du 28 mai 1982 modifié, pris lui-même après avis de ce conseil ;<br/>
<br/>
              Sur la légalité interne :<br/>
<br/>
              3. Considérant, en premier lieu, que, contrairement à ce que soutiennent les syndicats requérants, le ministère de l'éducation nationale, de l'enseignement supérieur et de la recherche constitue, à la date du décret attaqué, un seul département ministériel au sens du décret du 28 mai 1982 modifié, cité au même point, alors même que deux comités techniques ministériels ont été institués auprès de ce ministre ; qu'il ne résulte d'aucune disposition législative, ni d'aucun principe général du droit que les moyens nécessaires à l'exercice de l'activité syndicale doivent être déterminés au  regard du nombre d'électeurs inscrits sur les listes électorales pour l'élection au comité technique ministériel ; que, par suite, le décret attaqué n'a pas introduit une dérogation illégale aux dispositions du décret du 28 mai 1982 en prévoyant que l'effectif de référence pour la détermination du contingent global de crédit de temps syndical au ministère de l'éducation nationale, de l'enseignement supérieur et de la recherche est le nombre cumulé des électeurs aux deux comités techniques ministériels placés auprès de ce ministre ;<br/>
<br/>
              4. Considérant, en deuxième lieu, que dès lors qu'il confère aux organisations syndicales du ministère de l'éducation nationale, de l'enseignement supérieur et de la recherche le droit à un crédit de temps syndical identique lorsqu'il est apprécié au niveau de chaque électeur aux comités techniques ministériels de ce ministère, le décret attaqué ne saurait méconnaître le principe d'égalité entre les agents publics relevant de chacun de ces deux comités ; <br/>
<br/>
              5. Considérant, en dernier lieu, que, si les syndicats requérants soutiennent que les dispositions combinées du décret attaqué ont pour effet de réduire le contingent global de crédit de temps syndical au ministère de l'éducation nationale, de l'enseignement supérieur et de la recherche, il ne ressort pas des pièces du dossier que le décret attaqué aurait pour effet de porter atteinte à l'exercice de la liberté syndicale, ni qu'il serait entaché d'une erreur manifeste d'appréciation ; <br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède, que les syndicats requérants ne sont pas fondés à demander l'annulation du décret qu'ils attaquent ; que, par suite, leurs conclusions présentées sur le fondement de l'article L. 761-1 du code de justice administrative ne peuvent qu'être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la Fédération CGT de l'éducation, de la recherche et de la culture (FERC-CGT) et autres est rejetée. <br/>
Article 2 : La présente décision sera notifiée à la Fédération CGT de l'éducation, de la recherche et de la culture (FERC-CGT), premier requérant dénommé et à la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche. Les autres requérants seront informés de la présente décision par la SCP Lyon-Caen et Thiriez, avocat au Conseil d'Etat et à la Cour de cassation, qui les représente devant le Conseil d'Etat.<br/>
Copie en sera adressée pour information au Premier Ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
