<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031447519</ID>
<ANCIEN_ID>JG_L_2015_11_000000384241</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/44/75/CETATEXT000031447519.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère / 6ème SSR, 04/11/2015, 384241</TITRE>
<DATE_DEC>2015-11-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384241</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère / 6ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON ; SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR>M. Yannick Faure</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:384241.20151104</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
<br/>
              M. et Mme A...et Raihana B...ont demandé au tribunal administratif de Versailles : <br/>
              - d'annuler, d'une part, la décision de la caisse d'allocations familiales de l'Essonne leur refusant le bénéfice de l'allocation de logement à compter du 1er mars 2011 et, d'autre part, la décision du président du conseil général de l'Essonne du 27 juillet 2012 rejetant leur recours préalable dirigé contre la décision de cette même caisse du 24 décembre 2011 mettant fin à leurs droits au revenu de solidarité active (RSA) ;<br/>
              - de mettre à la charge de cette même caisse le versement de 2 860 euros de prestations non perçues au titre de l'année 2011, de  8 724 euros au titre de l'année 2012, de 8 868 euros au titre de l'année 2013, de 3 740 euros au titre de l'année 2014, ainsi que de 960,42 euros au titre de l'aide exceptionnelle de fin d'année pour 2011, 2012 et 2013 ;<br/>
              - de condamner cette même caisse à leur verser la somme de 6 000 euros en réparation du préjudice qu'ils estiment avoir subi.<br/>
<br/>
              Par une ordonnance n° 1200509 du 3 juillet 2014, le président de la première chambre du tribunal administratif de Versailles a rejeté leur demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 5 septembre et 5 décembre 2014 au secrétariat du contentieux du Conseil d'Etat, M. et Mme B... demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yannick Faure, auditeur,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de M. et Mme A...B...et à la SCP Boré, Salve de Bruneton, avocat du département de l'Essonne ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des termes de leur pourvoi que M. et Mme B...doivent être regardés comme demandant l'annulation de l'ordonnance du 3 juillet 2014 du président de la première chambre du tribunal administratif de Versailles en tant seulement que leurs conclusions dirigées contre la décision du président du conseil général de l'Essonne du 27 juillet 2012 leur refusant le versement du revenu de solidarité active et de l'aide exceptionnelle de fin d'année et tendant à la condamnation du département à leur verser les sommes ainsi dues ont été rejetées pour irrecevabilité manifeste, au motif qu'ils n'avaient pas formé leur recours administratif devant le président du conseil général préalablement à l'introduction de leur requête devant le tribunal administratif  ;<br/>
<br/>
              2. Considérant qu'aux termes du premier alinéa de l'article L. 262-47 du code de l'action sociale et des familles, dans sa rédaction applicable au litige : " Toute réclamation dirigée contre une décision relative au revenu de solidarité active fait l'objet, préalablement à l'exercice d'un recours contentieux, d'un recours administratif auprès du président du conseil général (...) " ; qu'aux termes du premier alinéa de l'article R. 262-88 du même code : " Le recours administratif préalable mentionné à l'article L. 262-47 est adressé par le bénéficiaire au président du conseil général dans un délai de deux mois à compter de la notification de la décision contestée (...) " ;<br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions qu'une réclamation dirigée contre une décision relative au revenu de solidarité active ne peut, à peine d'irrecevabilité, faire l'objet d'un recours contentieux sans qu'ait été préalablement exercé un recours administratif auprès du président du conseil général ; que, toutefois, le juge administratif ne peut rejeter pour irrecevabilité des conclusions nouvelles, présentées en cours d'instance, dirigées contre la décision du président du conseil général rendue sur le recours administratif formé en application de l'article R. 262-88 du code de l'action sociale et des familles, dès lors que ce recours administratif a été exercé dans le délai requis par cet article et que ces conclusions nouvelles sont elles-mêmes présentées dans le délai de recours contentieux ;<br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que si, le 4 janvier 2012, date d'enregistrement de leur requête au greffe du tribunal administratif de Versailles, M. et Mme B...n'avaient pas exercé de recours administratif préalable obligatoire auprès du président du conseil général de l'Essonne contre la décision du 24 décembre 2011 de la caisse d'allocations familiales de ce département, ils ont néanmoins exercé ce recours le 16 février 2012, dans le délai de deux mois prévu par l'article R. 262-88 du code de l'action sociale et des familles ; que le président du conseil général a rejeté ce recours par une décision du 27 juillet 2012 ; que, par un mémoire enregistré le 31 octobre 2013 au greffe de ce tribunal, l'avocat de M. et Mme B...désigné au titre de l'aide juridictionnelle a présenté des conclusions nouvelles dirigées contre cette décision ; que, dès lors, en jugeant manifestement irrecevables les conclusions de la requête de M. et Mme B...relatives au refus de versement du revenu de solidarité active et de l'aide exceptionnelle de fin d'année, sans rechercher si ces conclusions nouvelles avaient été présentées avant l'expiration du délai de recours contentieux, eu égard à la prorogation de celui-ci par la demande d'aide juridictionnelle formée par les requérants, le président de la première chambre du tribunal a commis une erreur de droit ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens de leur pourvoi, M. et Mme B...sont fondés à demander l'annulation de l'ordonnance du président de la première chambre du tribunal administratif de Versailles qu'ils attaquent en tant qu'elle rejette leurs conclusions tendant à l'annulation de la décision du président du conseil général de l'Essonne du 27 juillet 2012 leur refusant le versement du revenu de solidarité active et de l'aide exceptionnelle de fin d'année et à la condamnation du département à leur verser les sommes ainsi dues ; <br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas partie à la présente instance, et de M. et Mme B..., qui ne sont pas, dans cette même instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du président de la première chambre du tribunal administratif de Versailles du 3 juillet 2014 est annulée en tant qu'elle rejette les conclusions de M. et Mme B...tendant à l'annulation de la décision du président du conseil général de l'Essonne du 27 juillet 2012 leur refusant le versement du revenu de solidarité active et de l'aide exceptionnelle de fin d'année et à la condamnation du département à leur verser les sommes ainsi dues.<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, au tribunal administratif de Versailles.<br/>
Article 3 : Les conclusions présentées par les parties au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à M. et Mme A... et Raihana B...et au département de l'Essonne.<br/>
Copie en sera adressée à la ministre des affaires sociales, de la santé et des droits des femmes et à la caisse d'allocations familiales de l'Essonne.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-04 AIDE SOCIALE. CONTENTIEUX DE L'AIDE SOCIALE ET DE LA TARIFICATION. - RECOURS ADMINISTRATIF PRÉALABLE OBLIGATOIRE (RAPO) EN CONTENTIEUX DU RSA - EXERCICE DU RAPO APRÈS INTRODUCTION DE LA REQUÊTE - RECEVABILITÉ DES CONCLUSIONS NOUVELLES DIRIGÉES CONTRE LA DÉCISION RENDUE SUR RAPO - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-02-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. LIAISON DE L'INSTANCE. RECOURS ADMINISTRATIF PRÉALABLE. - RECOURS ADMINISTRATIF PRÉALABLE OBLIGATOIRE (RAPO) -  EXERCICE DU RAPO APRÈS INTRODUCTION DE LA REQUÊTE - RECEVABILITÉ DES CONCLUSIONS NOUVELLES DIRIGÉES CONTRE LA DÉCISION RENDUE SUR RAPO - EXISTENCE.
</SCT>
<ANA ID="9A"> 04-04 En vertu de l'article L. 262-47 du code de l'action sociale et des familles (CASF), une réclamation dirigée contre une décision relative au revenu de solidarité active (RSA) ne peut, à peine d'irrecevabilité, faire l'objet d'un recours contentieux sans qu'ait été préalablement exercé un recours administratif auprès du président du conseil général.... ,,Cas d'un requérant ayant introduit ce recours administratif après l'introduction d'une requête juridictionnelle et formé des conclusions nouvelles, présentées en cours d'instance, dirigées contre la décision du président du conseil général rendue sur son recours administratif. Le juge administratif ne peut rejeter pour irrecevabilité ces conclusions nouvelles dès lors que ce recours administratif a été exercé dans le délai requis par l'article R. 262-88 du CASF et que ces conclusions nouvelles sont elles-mêmes présentées dans le délai de recours contentieux.</ANA>
<ANA ID="9B"> 54-01-02-01 Recours administratif préalable obligatoire (RAPO) au contentieux du RSA (art. L. 262-47 du code de l'action sociale et des familles (CASF)). Cas d'un requérant ayant introduit ce recours administratif après l'introduction d'une requête juridictionnelle et formé des conclusions nouvelles, présentées en cours d'instance, dirigées contre la décision rendue sur son recours administratif. Le juge administratif ne peut rejeter pour irrecevabilité ces conclusions nouvelles dès lors que ce recours administratif a été exercé dans le délai requis par l'article R. 262-88 du CASF et que ces conclusions nouvelles sont elles-mêmes présentées dans le délai de recours contentieux.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
