<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036164734</ID>
<ANCIEN_ID>JG_L_2017_12_000000405685</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/16/47/CETATEXT000036164734.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 06/12/2017, 405685, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-12-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>405685</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>M. Jean Sirinelli</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:405685.20171206</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Lyon, d'une part, d'annuler pour excès de pouvoir les deux arrêtés du 28 septembre 2011 par lesquels le ministre de la défense l'a rayé du personnel navigant de l'armée de l'air et l'a nommé d'office dans le corps des officiers des bases de l'air dans la spécialité " communication et relations internationales " à compter du 2 octobre 2011, la décision implicite de rejet née du silence gardé par le ministre sur son recours administratif préalable obligatoire dirigé contre ces deux arrêtés, les deux décisions du 14 février 2012 par lesquelles le ministre de la défense a rejeté ses recours administratifs préalables obligatoires dirigés contre les deux arrêté précités du 28 septembre 2011 et, d'autre part, d'enjoindre sous astreinte au ministre de la défense de le réintégrer dans le corps des officiers de l'air au titre du personnel navigant, de l'affecter dans un emploi de pilote en unité navigante et de reconstituer sa carrière à compter du 1er octobre 2011. Par un jugement n° 1201572 du 16 juillet 2014, le tribunal administratif de Lyon a rejeté cette demande. <br/>
<br/>
              Par un arrêt n° 14LY03031 du 4 octobre 2016, la cour administrative d'appel de Lyon  a rejeté l'appel formé par M. B...contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 5 décembre 2016 et 6 mars 2017 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L.761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la défense ;<br/>
              - le décret du 27 décembre 1929 fixant les conditions de classement et de maintien dans le personnel militaire navigant ;<br/>
              - le décret n° 2008-943 du 12 septembre 2008 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean Sirinelli, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de M.B....<br/>
<br/>
<br/>
<br/>1. Considérant qu'il résulte des énonciations de l'arrêt attaqué que M. B..., capitaine du corps des officiers de l'air exerçant les fonctions de pilote de transport, a été placé sur sa demande en congé parental du 10 novembre 2008 au 30 septembre 2011 ; que, par un arrêté du 27 septembre 2011, le ministre de la défense a mis fin à son congé parental et l'a affecté à compter du 1er octobre 2011 à l'état-major de soutien de la défense de Lyon ; que, par deux arrêtés du 28 septembre 2011, le ministre l'a rayé du personnel navigant de l'armée de l'air et l'a nommé d'office dans le corps des officiers des bases de l'air dans la spécialité " communication et relations internationales " à compter du 2 octobre 2011 ; que le requérant a saisi la commission de recours des militaires de recours dirigés contre ces trois arrêtés ; que, par deux décisions du 14 février 2012, le ministre de la défense a rejeté ces recours ; que, par un jugement du 16 juillet 2014, le tribunal administratif de Lyon a rejeté la demande de M. B...dirigée contre ces trois arrêtés et les deux décisions de rejet qui lui ont été opposées ; que, par l'arrêt attaqué, la cour administrative d'appel de Lyon a rejeté l'appel formé par M. B...contre ce jugement ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 4133-1 du code de la défense : " Les militaires de carrière peuvent, pour les besoins du service, être admis sur leur demande ou affectés d'office dans d'autres corps de la force armée ou de la formation rattachée à laquelle ils appartiennent (...) " ; qu'aux termes du second alinéa de l'article L. 4111-2 du même code : " Les statuts particuliers des militaires sont fixés par décret en Conseil d'Etat. Ils peuvent déroger aux dispositions du présent livre qui ne répondraient pas aux besoins propres d'un corps particulier, à l'exception de celles figurant au titre II et de celles relatives au recrutement, aux conditions d'avancement et aux limites d'âge " ; qu'aux termes de l'article R. 4133-8 du même code : " Les changements d'office de corps au sein d'une même armée ou d'une même formation rattachée sont prononcés après avis d'une commission mixte composée des membres de la commission d'avancement du corps d'origine et de la commission d'avancement du corps d'accueil prévues à l'article L. 4136-3 ou par les statuts particuliers: / 1° Par décret du Président de la République, pour les officiers; / (...) " ; qu'aux termes de l'article R. 4133-9 : " Les militaires pour lesquels il est envisagé de recourir à la procédure du changement d'office de corps sont convoqués par lettre recommandée avec demande d'avis de réception au moins quinze jours francs avant la réunion de la commission prévue à l'article R. 4133-8 et peuvent se faire assister d'un militaire de leur choix (...) " ; que l'article R. 4133-4 prévoit que " Le militaire de carrière (...) classé dans le personnel navigant peut être admis, dans les conditions fixées aux articles R. 4133-5 à R. 4133-9 : / 1° Sur sa demande ou d'office, dans un autre corps de l'armée ou de la formation rattachée à laquelle il appartient (...) " ; qu'aux termes du troisième alinéa de l'article 4 du décret du 27 décembre 1929 fixant les conditions de classement et de maintien dans le personnel militaire navigant : " Le militaire classé dans le personnel navigant en est rayé automatiquement lorsque, pendant deux années consécutives, il n'a pas accompli [les] épreuves " prévues au premier alinéa du même article ; qu'aux termes du premier alinéa de l'article 38 du décret du 12 septembre 2008 portant statut particulier des corps des officiers de l'air, des officiers mécaniciens de l'air et des officiers des bases de l'air : " Les officiers de l'air rayés du personnel navigant pour l'une des raisons énoncées aux articles 4 et 5 du décret du 27 décembre 1929 fixant les conditions de classement dans le personnel militaire navigant sont affectés d'office soit dans le corps des officiers mécaniciens de l'air, soit dans le corps des officiers des bases de l'air. Le choix du corps d'accueil est fixé par arrêté du ministre de la défense en fonction des besoins du service et de l'aptitude des intéressés (...) " ;<br/>
<br/>
              3. Considérant, en premier lieu, qu'aux termes du troisième alinéa de l'article L. 4138-14 du code de la défense, dans sa rédaction applicable au litige : " (...) A l'expiration de son congé, il est réintégré de plein droit, au besoin en surnombre, dans son corps d'origine. Il peut, sur sa demande, être réaffecté dans un poste le plus proche possible de sa résidence, sous réserve des nécessités du service " ; que la cour administrative d'appel de Lyon n'a pas commis d'erreur de droit en jugeant qu'il résultait des dispositions du second alinéa de l'article L. 4111-2 du code de la défense, citées au point 2, que le statut particulier du corps des officiers de l'air, fixé par le décret du 12 septembre 2008, pouvait, eu égard aux besoins propres du corps des officiers de l'air appartenant au personnel navigant, déroger aux dispositions précitées de l'article L. 4138-14 du même code relatives au congé parental ; <br/>
<br/>
              4. Considérant, en deuxième lieu, que le moyen tiré de ce que les dispositions de l'article 38 du décret du 12 septembre 2008 et de l'article 4 du décret du 27 septembre 1929 fixant les conditions de classement et de maintien dans le personnel militaire navigant méconnaitraient les stipulations du paragraphe 2 de l'article 33 de la charte des droits fondamentaux de l'Union européenne et de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, qui n'est pas d'ordre public, est en tout état de cause  nouveau en cassation et, par suite, inopérant ; <br/>
<br/>
              5. Considérant, en troisième lieu, qu'il résulte de la combinaison des dispositions des articles R. 4133-8 et R. 4133-9 du code de la défense et de l'article 38 du décret du 12 septembre 2008, citées au point 2, que s'il appartient au ministre de la défense de choisir le futur corps d'affectation d'un officier de l'air radié du personnel navigant en application des dispositions des articles 4 et 5 du décret du 27 décembre 1929, seul le Président de la République est compétent pour procéder au changement d'office de corps de l'intéressé, à l'issue de la procédure prévue par les articles R. 4133-8 et R. 4133-9 du code de la défense ; qu'il suit de là que la cour a commis une erreur de droit en jugeant que le ministre de la défense était compétent pour procéder à un tel changement et qu'il n'était pas tenu de respecter cette procédure ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, que M. B...est seulement fondé à demander l'annulation de l'arrêt qu'il attaque en tant qu'il a rejeté ses conclusions dirigées contre le jugement du tribunal administratif de Lyon tendant à l'annulation de la décision du 14 février 2012 du ministre de la défense en tant qu'elle prononce son changement de corps d'office ; <br/>
<br/>
              7. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler, dans cette mesure, l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              8. Considérant, ainsi qu'il a été dit au point 5, que seul le Président de la République est compétent pour procéder au changement d'office de corps de M.B..., à l'issue de la procédure prévue par les articles R. 4133-8 et R. 4133-9 du code de la défense ; que, par suite, la décision du ministre de la défense du 14 février 2012 confirmant le changement de corps d'office de M. B...a été prise par une autorité incompétente ; <br/>
<br/>
              9. Considérant qu'il résulte de ce qui précède, et sans qu'il soit besoin d'examiner les autres moyens de la requête sur ce point, que M. B...est fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Lyon a rejeté ses conclusions tendant à l'annulation de la décision du ministre de la défense du 14 février 2012 en tant qu'elle prononce son changement de corps d'office ;<br/>
<br/>
              10. Considérant que l'exécution de la présente décision implique seulement que la ministre des armées, dans un délai de deux mois à compter de la notification de la présente décision, réintègre M. B...dans le corps des officiers de l'air à compter du 2 octobre 2011, avec reconstitution de sa carrière, et saisisse le Président de la République en vue que celui-ci prononce d'office son changement de corps, compte tenu de la décision ministérielle le rayant du personnel naviguant, après mise en oeuvre de la procédure prévue par les articles R. 4133-8 et R. 4133-9 du code de la défense, ; <br/>
<br/>
              11. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à M. B...au titre de l'article L. 761-1 du code de justice administrative pour l'ensemble de la procédure ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 4 octobre 2016 de la cour administrative d'appel de Lyon et le jugement du 16 juillet 2014 du tribunal administratif de Lyon sont annulés en tant qu'ils ont rejeté les conclusions de M. B...tendant à l'annulation de la décision du 14 février 2012 du ministre de la défense en tant qu'elle prononce son changement de corps d'office.<br/>
Article 2 : La décision du 14 février 2012 du ministre de la défense est annulée en tant qu'elle prononce le changement de corps d'office de M.B.... <br/>
Article 3 : Il est enjoint à la ministre des armées, dans un délai de deux mois à compter de la notification de la présente décision de réintégrer M. B...dans le corps des officiers de l'air à compter du 2 octobre 2011, avec reconstitution de sa carrière, et de saisir le Président de la République en vue que celui-ci prononce d'office son changement de corps. <br/>
Article 4 : L'Etat versera à M. B...une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : Le surplus des conclusions de la requête de M. B...est rejeté. <br/>
Article 6 : La présente décision sera notifiée à M. A...B...et à la ministre des armées.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
