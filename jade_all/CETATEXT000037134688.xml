<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037134688</ID>
<ANCIEN_ID>JG_L_2018_06_000000417686</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/13/46/CETATEXT000037134688.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 20/06/2018, 417686, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-06-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417686</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>HAAS ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Thomas Odinot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:417686.20180620</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              La société Cercis a demandé au juge des référés du tribunal administratif de Paris, statuant en application de l'article L. 551-1 du code de justice administrative ou, à titre subsidiaire, sur le fondement de l'article L. 551-13 du même code, d'enjoindre avant dire droit à la ville de Paris de lui communiquer les motifs détaillés du rejet de son offre et de surseoir à statuer dans cette attente, d'annuler la procédure lancée par la ville de Paris pour la passation du lot n° 2 d'un marché portant sur le nettoiement, l'entretien et l'aménagement des espaces verts du boulevard périphérique extérieur, à compter de la phase d'examen des offres et d'enjoindre à la ville de Paris de reprendre la procédure à compter de la phase d'examen des offres.<br/>
<br/>
              Par une ordonnance n° 1719239 du 12 janvier 2018, le juge des référés du tribunal administratif de Paris a prononcé un non lieu à statuer sur les conclusions de la demande en tant qu'elles sont présentées sur le fondement de l'article L. 551-1 du code de justice administrative et a rejeté le surplus des conclusions de la requête.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire, et un nouveau mémoire enregistrés les 29 janvier, 14 février et 19 mars 2018 au secrétariat du contentieux du Conseil d'Etat, la société Cercis demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) de mettre à la charge de la ville de Paris la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - l'ordonnance n° 2015-899 du 23 juillet 2015 ;<br/>
              - le décret n° 2016-360 du 25 mars 2016 ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Odinot, auditeur,<br/>
<br/>
              - les conclusions de M. GillesPellissier, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Haas, avocat de la société Cercis et à la SCP Foussard, Froger, avocat de la ville de Paris.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des énonciations de l'ordonnance attaquée que la ville de Paris a, par un avis de marché publié le 21 août 2017, engagé une procédure d'appel d'offres ouvert pour la passation d'un accord-cadre à bons de commandes relatif à des prestations de nettoiement, d'entretien et d'aménagement des espaces végétalisés du boulevard périphérique parisien ; que la société Cercis s'est portée candidate pour l'attribution des deux lots constituant le marché ; que, par un courrier électronique du 8 décembre 2017, la ville de Paris a informé la société Cercis du rejet de son offre et du nom de la société attributaire ; que le marché a été signé le 19 décembre 2017 ; que, par une ordonnance du 12 janvier 2018 contre laquelle la société Cercis se pourvoit en cassation, le juge des référés du tribunal administratif de Paris a prononcé un non lieu à statuer sur les conclusions de la requête en tant qu'elles sont présentées sur le fondement de l'article L. 551-1 du code de justice administrative et a rejeté le surplus des conclusions de la demande ; que le pourvoi doit être regardé comme dirigé contre l'ordonnance attaquée en tant qu'elle a rejeté les conclusions présentées par la société requérante sur le fondement de l'article L. 551-13 du code de justice administrative ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 551-13 du code de justice administrative : " Le président du tribunal administratif, ou le magistrat qu'il délègue, peut être saisi, une fois conclu l'un des contrats mentionnés aux articles L. 551-1 et L. 551-5, d'un recours régi par la présente section. " ; qu'aux termes de l'article L. 551-14 du même code, le recours en référé contractuel " n'est pas ouvert au demandeur ayant fait usage du recours prévu à l'article L. 551-1 ou à l'article L. 551-5 dès lors que le pouvoir adjudicateur ou l'entité adjudicatrice a respecté la suspension prévue à l'article L. 551-4 (...) " ; que l'article L. 551-4 du même code dispose que : " Le contrat ne peut être signé à compter de la saisine du tribunal administratif et jusqu'à la notification au pouvoir adjudicateur de la décision juridictionnelle " ; qu'enfin, l'article R. 551 1 du même code dispose que : " Le représentant de l'Etat ou l'auteur du recours est tenu de notifier son recours au pouvoir adjudicateur. / Cette notification doit être faite en même temps que le dépôt du recours et selon les mêmes modalités. / Elle est réputée accomplie à la date de sa réception par le pouvoir adjudicateur " ;<br/>
<br/>
              3. Considérant qu'il résulte des dispositions citées au point 2 que l'obligation de suspendre la signature du contrat qui pèse sur le pouvoir adjudicateur lorsqu'est introduit un recours en référé précontractuel dirigé contre la procédure de passation du contrat court à compter, soit de la notification au pouvoir adjudicateur du recours par le représentant de l'Etat ou par son auteur agissant conformément aux dispositions de l'article R. 551-1 du code de justice administrative, soit de la communication de ce recours par le greffe du tribunal administratif ; que lorsque l'auteur d'un référé précontractuel établit l'avoir notifié au pouvoir adjudicateur dans les conditions prévues par cet article, le pouvoir adjudicateur qui signe le contrat postérieurement à la réception du recours doit être regardé comme ayant méconnu les dispositions de l'article L. 551-4 du même code ; que s'agissant d'un recours envoyé au service compétent du pouvoir adjudicateur par des moyens de communication permettant d'assurer la transmission d'un document en temps réel, le délai de suspension court à compter non de la prise de connaissance effective du recours par le pouvoir adjudicateur, mais de la réception de la notification qui lui a été faite ;<br/>
<br/>
              4. Considérant qu'il ressort des énonciations de l'ordonnance attaquée que, pour apprécier le respect de l'obligation de suspension prévue à l'article L. 551-4 du code de justice administrative, le juge des référés s'est fondé sur l'heure de la prise de connaissance effective par la ville de Paris du recours de la société Cercis, et non sur l'heure de la réception par la ville de la notification qui lui a été faite par le greffe du tribunal administratif par l'intermédiaire de l'application Télérecours le 19 décembre 2017 à 15h09 ; que le juge des référés a écarté comme irrecevable le recours en référé contractuel de la société Cercis au motif que le pouvoir adjudicateur n'avait pas, en raison de cette référence erronée à l'heure de la prise de connaissance effective du recours en référé précontractuel, signé le marché en méconnaissance des dispositions de l'article L. 551-4 du code de justice administrative ; qu'en statuant ainsi, le juge des référés du tribunal administratif de Paris a commis une erreur de droit ; que, par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son ordonnance doit être annulée en tant qu'elle s'est prononcée sur les conclusions présentées par la société Cercis sur le fondement des dispositions de l'article L. 551-13 du code de justice administrative ; <br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, en application des dispositions de l'article L. 821-2 du code de justice administrative, de régler l'affaire, dans cette mesure, au titre de la procédure de référé engagée ;<br/>
<br/>
              6. Considérant qu'en vertu des dispositions du troisième alinéa de l'article L. 551-18 du code de justice administrative, le juge du référé contractuel prononce la nullité du contrat " lorsque celui-ci a été signé avant l'expiration du délai exigé après l'envoi de la décision d'attribution aux opérateurs économiques ayant présenté une candidature ou une offre ou pendant la suspension prévue à l'article L. 551-4 ou à l'article L. 551-9 si, en outre, deux conditions sont remplies : la méconnaissance de ces obligations a privé le demandeur de son droit d'exercer le recours prévu par les articles L. 551-1 et L. 551-5, et les obligations de publicité et de mise en concurrence auxquelles sa passation est soumise ont été méconnues d'une manière affectant les chances de l'auteur du recours d'obtenir le contrat. " ;<br/>
<br/>
              7. Considérant, en premier lieu, que la société Cercis ne peut utilement contester l'insuffisance de la motivation de la notification du rejet de son offre dès lors qu'une telle irrégularité, à la supposer établie, n'a pas affecté ses chances d'obtenir le marché en litige ;<br/>
<br/>
              8. Considérant, en second lieu, qu'aux termes du I de l'article 60 du décret du 25 mars 2016 relatif aux marchés publics : " L'acheteur exige que le soumissionnaire justifie le prix ou les coûts proposés dans son offre lorsque celle-ci semble anormalement basse eu égard aux travaux, fournitures ou services, y compris pour la part du marché public qu'il envisage de sous-traiter./ (...) " ; qu'il résulte de ces dispositions que, quelle que soit la procédure de passation mise en oeuvre, il incombe au pouvoir adjudicateur qui constate qu'une offre paraît anormalement basse de solliciter auprès de son auteur toutes précisions et justifications de nature à expliquer le prix proposé ; que si les précisions et justifications apportées ne sont pas suffisantes pour que le prix proposé ne soit pas regardé comme manifestement sous-évalué et de nature, ainsi, à compromettre la bonne exécution du marché, il appartient au pouvoir adjudicateur de rejeter l'offre, sauf à porter atteinte à l'égalité entre les candidats à l'attribution d'un marché public ; <br/>
<br/>
              9. Considérant que si la société Cercis soutient que la ville de Paris a attribué le lot en litige à une entreprise ayant présenté une offre anormalement basse, il résulte de l'instruction que, par un courrier du 19 octobre 2017, la ville a demandé des justifications à la société Robert Paysage au titre des dispositions précitées du décret du 25 mars 2016 ; que si la société requérante fait valoir l'important écart de prix proposé entre son offre et celle de la société Robert Paysage, il ne résulte pas de l'instruction que le prix proposé par la société attributaire serait manifestement sous-évalué et de nature, ainsi, à compromettre la bonne exécution du marché ; qu'ainsi, les obligations de publicité et de mise en concurrence n'ont pas été méconnues d'une manière affectant les chances de la société Cercis d'obtenir le contrat ; <br/>
<br/>
              10. Considérant qu'il résulte de tout ce qui précède, sans qu'il soit besoin de statuer sur la fin de non recevoir opposée par la ville de Paris, que la société Cercis n'est pas fondée à demander que soit annulée la procédure de la passation du lot en litige et qu'il soit enjoint à la ville de Paris de reprendre la procédure à compter de la phase d'examen des offres ; que ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ;<br/>
<br/>
              11. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Cercis la somme de 3 000 euros à verser à la ville de Paris au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 12 janvier 2018 du tribunal administratif de Paris est annulée en tant qu'elle s'est prononcée sur les conclusions présentées par la société Cercis sur le fondement des dispositions de l'article L. 551-13 du code de justice administrative.<br/>
Article 2 : Les conclusions de la société Cercis présentées sur le fondement des dispositions des articles L. 551-13 et L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La société Cercis versera à la ville de Paris la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la société Cercis et à la ville de Paris.<br/>
Copie en sera adressée à la société Robert Paysage.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
