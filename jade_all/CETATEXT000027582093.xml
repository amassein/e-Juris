<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027582093</ID>
<ANCIEN_ID>JG_L_2013_06_000000356862</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/58/20/CETATEXT000027582093.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 19/06/2013, 356862, Publié au recueil Lebon</TITRE>
<DATE_DEC>2013-06-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>356862</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Sophie-Caroline de Margerie</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Damien Botteghi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:356862.20130619</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu, 1° sous le n° 356862, la requête, enregistrée le 17 février 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par la Commission nationale des comptes de campagne et des financements politiques, dont le siège est 33, avenue de Wagram à Paris (75176) Cedex 17 ; la commission demande au Conseil d'Etat d'annuler le jugement n° 1109704 du 2 février 2012 du tribunal administratif de Cergy-Pontoise en tant qu'il a rejeté sa saisine consécutive au rejet du compte de campagne de M. A...B..., candidat aux fonctions de conseiller général du canton de Bourg-la-Reine (Hauts-de-Seine) lors des opérations électorales qui se sont déroulées les 20 et 27 mars 2011 ;<br/>
<br/>
<br/>
<br/>
              Vu, 2° sous le n° 357277, la requête, enregistrée le 1er mars 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par M. A...B..., demeurant ... ; M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le même jugement du tribunal administratif de Cergy-Pontoise en tant qu'il juge que la Commission nationale des comptes de campagne et des financements politiques a rejeté à bon droit son compte de campagne ;<br/>
<br/>
              2°) de mettre à la charge de la Commission nationale des comptes de campagne et des financements politiques la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu le code électoral ;<br/>
<br/>
              Vu la loi n° 2011-412 du 14 avril 2011 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie-Caroline de Margerie, Conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Damien Botteghi, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant que les requêtes de la Commission nationale des comptes de campagne et des financements politiques et de M. B...sont dirigées contre le même jugement ; qu'il y a lieu de les joindre pour statuer par une seule décision ; <br/>
<br/>
              2.	Considérant qu'aux termes du premier alinéa de l'article L. 52-11-1 du code électoral, dans sa version applicable aux opérations électorales des 20 et 27 mars 2011 : " Les dépenses électorales des candidats aux élections auxquelles l'article L. 52-4 est applicable font l'objet d'un remboursement forfaitaire de la part de l'Etat égal à 50 % de leur plafond de dépenses. Ce remboursement ne peut excéder le montant des dépenses réglées sur l'apport personnel des candidats et retracées dans leur compte de campagne " ; qu'aux termes des deux derniers alinéas du même article, dans leur version issue de la loi du 14 avril 2011 portant simplification du code électoral et relative à la transparence financière de la vie politique : " Le remboursement forfaitaire n'est pas versé aux candidats qui ont obtenu moins de 5 % des suffrages exprimés au premier tour de scrutin, qui ne se sont pas conformés aux prescriptions de l'article L. 52-11, qui n'ont pas déposé leur compte de campagne dans le délai prévu au deuxième alinéa de l'article L. 52-12 ou dont le compte de campagne est rejeté pour d'autres motifs ou qui n'ont pas déposé leur déclaration de situation patrimoniale, s'ils sont astreints à cette obligation. / Dans les cas où les irrégularités commises ne conduisent pas au rejet du compte, la décision concernant ce dernier peut réduire le montant du remboursement forfaitaire en fonction du nombre et de la gravité de ces irrégularités " ;<br/>
<br/>
              3.	Considérant que l'article L. 52-15 du même code dispose : " La Commission nationale des comptes de campagne et des financements politiques approuve et, après procédure contradictoire, rejette ou réforme les comptes de campagne. Elle arrête le montant du remboursement forfaitaire prévu à l'article L. 52-11-1. / Hors le cas prévu à l'article L. 118-2, elle se prononce dans les six mois du dépôt des comptes. Passé ce délai, les comptes sont réputés approuvés. / Lorsque la commission a constaté que le compte de campagne n'a pas été déposé dans le délai prescrit, si le compte a été rejeté ou si, le cas échéant après réformation, il fait apparaître un dépassement du plafond des dépenses électorales, la commission saisit le juge de l'élection / (...) Le remboursement total ou partiel des dépenses retracées dans le compte de campagne, quand la loi le prévoit, n'est possible qu'après l'approbation du compte de campagne par la commission. (...) " ; <br/>
<br/>
              4.	Considérant qu'aux termes du second alinéa de l'article L. 118-2 du même code, issu de la même loi du 14 avril 2011 : " Sans préjudice de l'article L. 52-15, lorsqu'il constate que la commission instituée par l'article L. 52-14 n'a pas statué à bon droit, le juge de l'élection fixe le montant du remboursement dû au candidat en application de l'article L. 52-11-1 " ; que, selon l'article L. 118-3 du même code ,dans sa rédaction applicable en l'espèce : " Saisi par la commission instituée par l'article L. 52-14, le juge de l'élection peut déclarer inéligible pendant un an le candidat dont le compte de campagne, le cas échant après réformation, fait apparaître un dépassement du plafond des dépenses électorales. / Saisi dans les mêmes conditions, le juge de l'élection peut déclarer inéligible le candidat qui n'a pas déposé son compte de campagne dans les conditions et le délai prescrits à l'article L. 52-12. / Il prononce également l'inéligibilité du candidat dont le compte de campagne a été rejeté à bon droit en cas de volonté de fraude ou de manquement d'une particulière gravité aux règles relatives au financement des campagnes électorales. (...) " ;<br/>
<br/>
              5.	Considérant que le compte de campagne de M.B..., candidat aux élections qui se sont déroulées les 20 et 27 mars 2011 en vue de la désignation du conseiller général du canton de Bourg-la-Reine (Hauts-de-Seine), a été rejeté par la Commission nationale des comptes de campagne et des financements politiques par une décision du 3 novembre 2011 ; que le tribunal administratif de Cergy-Pontoise, saisi par la commission, après avoir estimé que le compte de campagne avait été rejeté à bon droit mais que le manquement commis par M. B... ne pouvait être regardé comme présentant le caractère d'une fraude ou d'un manquement d'une particulière gravité susceptible de justifier que ce dernier soit déclaré inéligible, a rejeté la saisine de la commission par un jugement du 2 février 2012 ; que la Commission nationale des comptes de campagne et des financements politiques et M. B...relèvent appel de ce jugement ;<br/>
<br/>
              Sur l'appel de M. B...: <br/>
<br/>
              6.	Considérant qu'il résulte des dispositions du second alinéa de l'article L. 118-2 du code électoral, issu de la loi du 14 avril 2011, que le législateur a entendu que le juge de l'élection, lorsqu'il se prononce sur un compte de campagne et sur l'éligibilité d'un candidat, fixe, au besoin d'office, le montant du remboursement dû par l'Etat au candidat s'il constate que la Commission nationale des comptes de campagne et des financements politiques n'a pas statué à bon droit ; qu'il s'ensuit que, lorsque la commission, après avoir rejeté le compte d'un candidat, saisit régulièrement le juge de l'élection, cette saisine conduit nécessairement le juge, avant de rechercher s'il y a lieu ou non de prononcer l'inéligibilité du candidat et, s'il s'agit d'un candidat proclamé élu, d'annuler son élection ou de le déclarer démissionnaire d'office, à apprécier si le compte de campagne a été rejeté à bon droit par la commission ; que si le juge de l'élection estime que le compte n'a pas été rejeté à bon droit, il lui appartient alors, qu'il soit ou non saisi de conclusions en ce sens, de fixer le montant du remboursement dû par l'Etat au candidat, sans qu'il puisse toutefois ordonner au candidat de rembourser des sommes qu'il aurait déjà perçues à ce titre ; que si le juge de l'élection estime, en revanche, que le compte de campagne a été rejeté à bon droit, ce qui fait alors obstacle à ce que le candidat obtienne le remboursement des dépenses retracées dans son compte, le candidat justifie d'un intérêt lui donnant qualité à relever appel du jugement dans cette mesure, quand bien même le juge de l'élection ne l'aurait finalement pas déclaré inéligible en application de l'article L. 118-3 du code électoral ; <br/>
<br/>
              7.	Considérant que, si le tribunal administratif de Cergy-Pontoise n'a pas déclaré dans son dispositif, avant de statuer sur l'inéligibilité du candidat, que le compte de campagne de M. B...avait été rejeté à bon droit - ainsi qu'il aurait dû le faire dès lors qu'il entendait confirmer la position de la commission - il s'est néanmoins prononcé en ce sens ; qu'il résulte de ce qui précède que M. B...est recevable, alors même que le jugement ne l'a pas déclaré inéligible, à relever appel de ce jugement en tant qu'il a jugé que son compte de campagne avait été rejeté à bon droit ;<br/>
<br/>
              8.	Considérant que, selon le premier alinéa de l'article L. 52-1 du code électoral, dans sa version applicable à l'élection en cause, l'utilisation à des fins de propagande électorale de tout procédé de publicité commerciale par la voie de la presse est interdite pendant les trois mois précédant le premier jour du mois d'une élection et jusqu'à la date du tour de scrutin où celle-ci est acquise ; que la Commission nationale des comptes de campagne et des financements politiques a rejeté le compte de campagne de M. B...au motif que ce compte comportait une dépense irrégulière d'un montant de 2 392 euros, pour des encarts publicitaires regardés comme contraires aux prescriptions du premier alinéa de l'article L. 52-1 ; <br/>
<br/>
              9.	Considérant que, si la méconnaissance de l'interdiction résultant du premier alinéa de l'article L. 52-1 du code électoral constitue une irrégularité susceptible d'altérer la sincérité du scrutin et de justifier, en fonction de son incidence sur les résultats, l'annulation de l'élection, et si le caractère irrégulier d'une telle dépense fait obstacle à ce qu'elle puisse faire l'objet d'un remboursement de la part de l'Etat, cette méconnaissance ne peut, par elle-même, justifier le rejet du compte de campagne du candidat qui y a porté cette dépense faite en vue de l'élection ; que, par suite, la Commission nationale des comptes de campagne et des financements politiques ne pouvait rejeter le compte de campagne de M. B...pour ce motif ;<br/>
<br/>
              10.	Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens de sa requête, M. B...est fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Cergy-Pontoise a jugé que son compte de campagne avait été rejeté à bon droit ;<br/>
<br/>
              Sur l'appel de la Commission nationale des comptes de campagne et des financements politiques :<br/>
<br/>
              11.	Considérant qu'ainsi qu'il a été dit, le compte de campagne de M. B... ne pouvait être rejeté à bon droit au motif qu'il comportait une dépense irrégulière exposée en méconnaissance de l'interdiction résultant du premier alinéa de l'article L. 52-1 du code électoral ; que, dès lors, la Commission nationale des comptes de campagne et des financements politiques n'est pas fondée à se plaindre de ce que, par le jugement attaqué, le tribunal administratif de Cergy-Pontoise a rejeté sa saisine ;  <br/>
<br/>
              Sur le remboursement des dépenses électorales dû par l'Etat : <br/>
<br/>
              12.	Considérant qu'il résulte des dispositions, citées au point 2, de l'article L. 52-11-1 du code électoral que les candidats ayant obtenu au moins 5 % des suffrages exprimés au premier tour de scrutin ont droit à un remboursement forfaitaire de la part de l'Etat égal à 50 % de leur plafond de dépenses, sans que ce remboursement ne puisse excéder le montant des dépenses réglées sur leur apport personnel et retracées dans leur compte de campagne ; que le caractère irrégulier d'une dépense exposée en méconnaissance du premier alinéa de l'article L. 52-1 du code électoral fait obstacle à ce qu'elle puisse faire l'objet d'un remboursement de la part de l'Etat ; <br/>
<br/>
              13.	Considérant qu'il résulte de l'instruction que M. B...a fait publier dans le journal " Antony Hebdo ", les 21 et 28 janvier et les 18 et 25 février 2011, des encarts présentant sa candidature aux élections cantonales, ses soutiens et les thèmes de sa campagne ; que ces publications ont fait l'objet d'une facture établie par le journal à titre d'" insertions publicitaires " pour un montant de 2 392 euros ; que, si M. B...soutient que la référence à des insertions publicitaires aurait été portée par erreur sur cette facture et produit à cet effet une attestation du directeur de la publication, ces éléments, dépourvus de précisions, ne sont pas de nature à établir que la mention portée sur la facture serait inexacte et que la prestation réglée aurait correspondu, non à un achat d'espace, mais à l'achat d'un grand nombre d'exemplaires du journal ; que les publications en cause, payées par le candidat, doivent ainsi être regardées comme constituant un procédé de publicité commerciale à des fins de propagande électorale par la voie de la presse ; qu'en y ayant recours, M. B...a méconnu l'interdiction résultant de l'article L. 52-1 du code électoral ; que la dépense correspondante, qui présente ainsi un caractère irrégulier, ne peut donner lieu à remboursement de la part de l'Etat ; <br/>
<br/>
              14.	Considérant qu'il résulte de l'instruction que M.B..., dont le compte de campagne n'a pas été rejeté à bon droit, a obtenu plus de 5 % des suffrages exprimés au premier tour de scrutin ; qu'il a droit, en application de l'article L. 52-11-1 du code électoral, à un remboursement forfaitaire égal à la moitié du plafond légal des dépenses, laquelle s'établit à 12 260 euros, le remboursement ne pouvant toutefois excéder le montant des dépenses réglées sur son apport personnel et retracées dans son compte de campagne ; que les dépenses de M. B... réglées sur son apport personnel se sont élevées à 5 554 euros ; qu'ainsi qu'il a été dit, la dépense de 2 392 euros correspondant aux insertions publicitaires faites en méconnaissance de l'article L. 52-1 du code électoral ne peut donner lieu à remboursement ; qu'ainsi, c'est à la somme de 3 162 euros que doit être fixé le montant du remboursement forfaitaire auquel a droit M. B...;<br/>
<br/>
              Sur les conclusions relatives à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              15.	Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire  droit aux conclusions présentées par M. B...au titre des frais exposés et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le montant du remboursement dû par l'Etat à M. B...en application de l'article L. 52-11-1 du code électoral est fixé à 3 162 euros.<br/>
<br/>
Article 2 : Le jugement du tribunal administratif de Cergy-Pontoise du 2 février 2012 est réformé en ce qu'il a de contraire à la présente décision.<br/>
<br/>
Article 3 : La requête de la Commission nationale des comptes de campagne et des financements politiques est rejetée.<br/>
<br/>
Article 4 : Les conclusions présentées par M. B...au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 5 : La présente décision sera notifiée à M. A...B...et à la Commission nationale des comptes de campagne et des financements politiques. <br/>
                       Copie en sera adressée au ministre de l'intérieur.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">28-005-02 ÉLECTIONS ET RÉFÉRENDUM. DISPOSITIONS GÉNÉRALES APPLICABLES AUX ÉLECTIONS. CAMPAGNE ET PROPAGANDE ÉLECTORALES. - MÉCONNAISSANCE PAR UN CANDIDAT DE L'INTERDICTION D'UTILISER À DES FINS DE PROPAGANDE ÉLECTORALE TOUT PROCÉDÉ DE PUBLICITÉ COMMERCIALE PAR LA VOIE DE LA PRESSE DANS LES TROIS MOIS PRÉCÉDANT L'ÉLECTION (ART. L. 52-1, 1ER AL.) - CONSÉQUENCES - IRRÉGULARITÉ SUSCEPTIBLE D'ALTÉRER LA SINCÉRITÉ DU SCRUTIN - EXISTENCE - CIRCONSTANCE FAISANT OBSTACLE AU REMBOURSEMENT DES DÉPENSES EN QUESTION - EXISTENCE - CIRCONSTANCE POUVANT JUSTIFIER PAR ELLE-MÊME LE REJET DU COMPTE - ABSENCE [RJ3].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">28-005-04-02 ÉLECTIONS ET RÉFÉRENDUM. DISPOSITIONS GÉNÉRALES APPLICABLES AUX ÉLECTIONS. FINANCEMENT ET PLAFONNEMENT DES DÉPENSES ÉLECTORALES. COMPTE DE CAMPAGNE. - MÉCONNAISSANCE PAR UN CANDIDAT DE L'INTERDICTION D'UTILISER À DES FINS DE PROPAGANDE ÉLECTORALE TOUT PROCÉDÉ DE PUBLICITÉ COMMERCIALE PAR LA VOIE DE LA PRESSE DANS LES TROIS MOIS PRÉCÉDANT L'ÉLECTION (ART. L. 52-1, 1ER AL.) - CONSÉQUENCES - IRRÉGULARITÉ SUSCEPTIBLE D'ALTÉRER LA SINCÉRITÉ DU SCRUTIN - EXISTENCE - CIRCONSTANCE FAISANT OBSTACLE AU REMBOURSEMENT DES DÉPENSES EN QUESTION - EXISTENCE - CIRCONSTANCE POUVANT JUSTIFIER PAR ELLE-MÊME LE REJET DU COMPTE - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">28-005-04-03 ÉLECTIONS ET RÉFÉRENDUM. DISPOSITIONS GÉNÉRALES APPLICABLES AUX ÉLECTIONS. FINANCEMENT ET PLAFONNEMENT DES DÉPENSES ÉLECTORALES. COMMISSION NATIONALE DES COMPTES DE CAMPAGNE ET DES FINANCEMENTS POLITIQUES (CNCCFP). - MODIFICATIONS DU CODE ÉLECTORAL OPÉRÉES PAR LA LOI DU 14 AVRIL 2011 - 1) OFFICE DU JUGE DE L'ÉLECTION LORSQU'IL SE PRONONCE SUR UN COMPTE DE CAMPAGNE ET SUR L'ÉLIGIBILITÉ D'UN CANDIDAT (ART. L. 118-2, 2ND ALINÉA) - OBLIGATION DE FIXER, Y COMPRIS D'OFFICE, LE MONTANT DU REMBOURSEMENT DÛ PAR L'ETAT - EXISTENCE [RJ1] - 2) CONSÉQUENCES SUR L'OFFICE DU JUGE DE L'ÉLECTION SAISI PAR LA CNCCFP SUR LE FONDEMENT DE L'ARTICLE L. 118-3 - OBLIGATION DE SE PRONONCER SUR LE BIEN-FONDÉ DES MOTIFS RETENUS PAR LA CNCCFP POUR RÉFORMER OU REJETER LE COMPTE - EXISTENCE - 3) JUGE DE L'ÉLECTION DÉCIDANT N'Y AVOIR PAS LIEU DE PRONONCER L'INÉLIGIBILITÉ (ART. L. 118-3 DU CODE ÉLECTORAL) - A) OBLIGATION DE LE DÉCLARER DANS LE DISPOSITIF DU JUGEMENT - EXISTENCE - B) INTÉRÊT DU CANDIDAT À FAIRE APPEL - EXISTENCE [RJ2] - CIRCONSTANCE QUE LE JUGE NE L'AURAIT PAS MENTIONNÉ DANS LE DISPOSITIF DE SON JUGEMENT - INCIDENCE - ABSENCE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">28-08 ÉLECTIONS ET RÉFÉRENDUM. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. - MODIFICATIONS DU CODE ÉLECTORAL OPÉRÉES PAR LA LOI DU 14 AVRIL 2011 - 1) OFFICE DU JUGE DE L'ÉLECTION LORSQU'IL SE PRONONCE SUR UN COMPTE DE CAMPAGNE ET SUR L'ÉLIGIBILITÉ D'UN CANDIDAT (ART. L. 118-2, 2ND ALINÉA) - OBLIGATION DE FIXER, Y COMPRIS D'OFFICE, LE MONTANT DU REMBOURSEMENT DÛ PAR L'ETAT - EXISTENCE [RJ1] - 2) CONSÉQUENCES SUR L'OFFICE DU JUGE DE L'ÉLECTION SAISI PAR LA CNCCFP SUR LE FONDEMENT DE L'ARTICLE L. 118-3 - OBLIGATION DE SE PRONONCER SUR LE BIEN-FONDÉ DES MOTIFS RETENUS PAR LA CNCCFP POUR RÉFORMER OU REJETER LE COMPTE - EXISTENCE - 3) JUGE DE L'ÉLECTION DÉCIDANT N'Y AVOIR PAS LIEU DE PRONONCER L'INÉLIGIBILITÉ (ARTICLE L. 118-3 DU CODE ÉLECTORAL) - A) OBLIGATION DE LE DÉCLARER DANS LE DISPOSITIF DU JUGEMENT - EXISTENCE - B) INTÉRÊT DU CANDIDAT À FAIRE APPEL - EXISTENCE [RJ2] - CIRCONSTANCE QUE LE JUGE NE L'AURAIT PAS MENTIONNÉ DANS LE DISPOSITIF DE SON JUGEMENT - INCIDENCE - ABSENCE.
</SCT>
<ANA ID="9A"> 28-005-02 Si la méconnaissance de l'interdiction d'utiliser à des fins de propagande électorale tout procédé de publicité commerciale par la voie de la presse  dans les trois mois précédant l'élection résultant du premier alinéa de l'article L. 52-1 du code électoral constitue une irrégularité susceptible d'altérer la sincérité du scrutin et de justifier, en fonction de son incidence sur les résultats, l'annulation de l'élection, et si le caractère irrégulier d'une telle dépense fait obstacle à ce qu'elle puisse faire l'objet d'un remboursement de la part de l'Etat, cette méconnaissance ne peut, par elle-même, justifier le rejet du compte de campagne du candidat qui y a porté cette dépense faite en vue de l'élection.</ANA>
<ANA ID="9B"> 28-005-04-02 Si la méconnaissance de l'interdiction d'utiliser à des fins de propagande électorale tout procédé de publicité commerciale par la voie de la presse  dans les trois mois précédant l'élection résultant du premier alinéa de l'article L. 52-1 du code électoral constitue une irrégularité susceptible d'altérer la sincérité du scrutin et de justifier, en fonction de son incidence sur les résultats, l'annulation de l'élection, et si le caractère irrégulier d'une telle dépense fait obstacle à ce qu'elle puisse faire l'objet d'un remboursement de la part de l'Etat, cette méconnaissance ne peut, par elle-même, justifier le rejet du compte de campagne du candidat qui y a porté cette dépense faite en vue de l'élection.</ANA>
<ANA ID="9C"> 28-005-04-03 1) Il résulte des dispositions du second alinéa de l'article L. 118-2 du code électoral, issu de la loi n° 2011-412 du 14 avril 2011, que le législateur a entendu que le juge de l'élection, lorsqu'il se prononce sur un compte de campagne et sur l'éligibilité d'un candidat, fixe, au besoin d'office, le montant du remboursement dû par l'Etat au candidat s'il constate que la Commission nationale des comptes de campagne et des financements politiques (CNCCFP) n'a pas statué à bon droit.... ,,2) Il s'ensuit que, lorsque la commission, après avoir rejeté le compte d'un candidat, saisit régulièrement le juge de l'élection, cette saisine conduit nécessairement le juge, avant de rechercher s'il y a lieu ou non de prononcer l'inéligibilité du candidat et, s'il s'agit d'un candidat proclamé élu, d'annuler son élection ou de le déclarer démissionnaire d'office, à apprécier si le compte de campagne a été rejeté à bon droit par la commission. Si le juge de l'élection estime que le compte n'a pas été rejeté à bon droit, il lui appartient alors, qu'il soit ou non saisi de conclusions en ce sens, de fixer le montant du remboursement dû par l'Etat au candidat, sans qu'il puisse toutefois ordonner au candidat de rembourser des sommes qu'il aurait déjà perçues à ce titre.,,,3) Si le juge de l'élection estime que le compte de campagne a été rejeté à bon droit, ce qui fait alors obstacle à ce que le candidat obtienne le remboursement des dépenses retracées dans son compte,... ...a) il doit le déclarer dans son dispositif ;,,b) le candidat justifie d'un intérêt lui donnant qualité à relever appel du jugement dans cette mesure, quand bien même le juge de l'élection ne l'aurait finalement pas déclaré inéligible en application de l'article L. 118-3 du code électoral, et alors même que le juge aurait méconnu l'obligation énoncée au a) ci-dessus..</ANA>
<ANA ID="9D"> 28-08 1) Il résulte des dispositions du second alinéa de l'article L. 118-2 du code électoral, issu de la loi n° 2011-412 du 14 avril 2011, que le législateur a entendu que le juge de l'élection, lorsqu'il se prononce sur un compte de campagne et sur l'éligibilité d'un candidat, fixe, au besoin d'office, le montant du remboursement dû par l'Etat au candidat s'il constate que la Commission nationale des comptes de campagne et des financements politiques (CNCCFP) n'a pas statué à bon droit.... ,,2) Il s'ensuit que, lorsque la commission, après avoir rejeté le compte d'un candidat, saisit régulièrement le juge de l'élection, cette saisine conduit nécessairement le juge, avant de rechercher s'il y a lieu ou non de prononcer l'inéligibilité du candidat et, s'il s'agit d'un candidat proclamé élu, d'annuler son élection ou de le déclarer démissionnaire d'office, à apprécier si le compte de campagne a été rejeté à bon droit par la commission. Si le juge de l'élection estime que le compte n'a pas été rejeté à bon droit, il lui appartient alors, qu'il soit ou non saisi de conclusions en ce sens, de fixer le montant du remboursement dû par l'Etat au candidat, sans qu'il puisse toutefois ordonner au candidat de rembourser des sommes qu'il aurait déjà perçues à ce titre.,,,3) Si le juge de l'élection estime que le compte de campagne a été rejeté à bon droit, ce qui fait alors obstacle à ce que le candidat obtienne le remboursement des dépenses retracées dans son compte,... ...a) il doit le déclarer dans son dispositif ;,,b) le candidat justifie d'un intérêt lui donnant qualité à relever appel du jugement dans cette mesure, quand bien même le juge de l'élection ne l'aurait finalement pas déclaré inéligible en application de l'article L. 118-3 du code électoral, et alors même que le juge aurait méconnu l'obligation énoncée au a) ci-dessus.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 23 juillet 2012, M.,, n° 356623, à publier au Recueil.,,[RJ2] Comp., dans l'état du droit antérieur à cette modification législative, CE, Section, 13 décembre 2002,,, n° 243109, p. 457.,,[RJ3] Cf. CE, Section, 7 mai 1993,,et autres (élections régionales de la Réunion) et Commission nationale des comptes de campagne et des financements politiques, n° 135815 et autres, p. 146 ; CE, Section, 8 juin 2009, Elections municipales de Corbeil-Essonnes, n° 322236 322237, p. 222. Ab. jur. sur ce point CE, 2 mars 2011, Commission nationale des comptes de campagne et des financements politiques c/ M.,, n° 341743, inédite au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
