<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042854715</ID>
<ANCIEN_ID>JG_L_2020_12_000000428835</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/85/47/CETATEXT000042854715.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 31/12/2020, 428835, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428835</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:428835.20201231</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B... A... a demandé au tribunal administratif de Caen de condamner l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) à prendre en charge la réparation des préjudices qu'elle estime avoir subis du fait de sa prise en charge au centre hospitalier de Lisieux. Par un jugement n° 1502458 du 27 décembre 2016, le tribunal administratif a condamné l'ONIAM à lui verser la somme de 234 840,40 euros.<br/>
<br/>
              Par un arrêt n° 17NT00789 du 11 janvier 2019, la cour administrative d'appel de Nantes a, sur appel de Mme A... et de l'ONIAM, réformé ce jugement, condamné l'ONIAM à verser à Mme A... une somme de 98 196,99 euros en capital et une rente annuelle de 11 400 euros, et rejeté le surplus de ses conclusions.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 14 mars et 14 juin 2019 au secrétariat du contentieux du Conseil d'Etat, Mme A... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il rejette le surplus de ses conclusions ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel et de rejeter l'appel de l'ONIAM ; <br/>
<br/>
              3°) de mettre à la charge de l'ONIAM la somme de 5 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ; <br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de Mme A... et à la SCP Sevaux, Mathonnet, avocat de l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme A... a subi le 6 avril 2010 au centre hospitalier de Lisieux une opération dont les séquelles ont été regardées comme résultant d'un accident médical non fautif, ouvrant droit à prise en charge au titre de la solidarité nationale. Mme A... se pourvoit en cassation contre l'arrêt du 11 janvier 2019 par lequel la cour administrative d'appel de Nantes a, sur son appel dirigé contre le jugement du 27 décembre 2016 du tribunal administratif de Caen, fixé le montant de l'indemnisation due par l'ONIAM. Par la voie du pourvoi incident, l'ONIAM demande également l'annulation de cet arrêt en tant qu'il statue sur les frais d'assistance par une tierce personne de Mme A....<br/>
<br/>
              Sur le pourvoi principal de Mme A... :<br/>
<br/>
              En ce qui concerne les frais liés à l'hospitalisation de son conjoint et à l'assistance apportée à celui-ci : <br/>
<br/>
              2. Aux termes du II de l'article L. 1142-1 du code de la santé publique : " Lorsque la responsabilité d'un professionnel, d'un établissement, service ou organisme mentionné au I ou d'un producteur de produits n'est pas engagée, un accident médical, une affection iatrogène ou une infection nosocomiale ouvre droit à la réparation des préjudices du patient et, en cas de décès, de ses ayants droit au titre de la solidarité nationale, lorsqu'ils sont directement imputables à des actes de prévention, de diagnostic ou de soins et qu'ils ont eu pour le patient des conséquences anormales au regard de son état de santé comme de l'évolution prévisible de celui-ci (...) ". En vertu des articles L. 1142-17 et L. 1142-22 du même code, la réparation au titre de la solidarité nationale est assurée par l'ONIAM. <br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond que Mme A... assurait, avant l'intervention qu'elle a subie, une assistance quotidienne auprès de son conjoint. Devant la cour administrative d'appel, elle soutenait que les conséquences de l'intervention du 6 avril 2010 l'avaient placée temporairement dans l'incapacité d'assurer cette assistance et qu'elle avait exposé une partie des dépenses liées à la nécessité de faire, durant sa propre convalescence, hospitaliser M. A..., puis de lui faire assurer une assistance à domicile à titre onéreux. En se fondant, pour écarter la prise en charge de ces frais par l'ONIAM, sur la circonstance qu'ils portaient sur des préjudices de M. A..., alors que ces préjudices, bien que liés à des soins apportés à ce dernier, étaient propres à la requérante et susceptibles d'être en lien direct avec l'accident médical dont elle avait été victime, la cour a entaché son arrêt d'une erreur de droit.<br/>
<br/>
              En ce qui concerne les frais d'assistance de Mme A... par une tierce personne :<br/>
<br/>
              4. Il résulte des termes de l'arrêt attaqué que la cour administrative d'appel, qui avait évalué le besoin d'assistance par une tierce personne découlant de l'accident médical subi par Mme A... à deux heures par jour à compter du 22 juillet 2010, n'a, pour la période antérieure à son arrêt, mis à la charge de l'ONIAM que les seuls frais d'assistance salariée effectivement exposés par la victime. En statuant ainsi, alors qu'il lui appartenait de fixer le droit à indemnisation de Mme A... au vu des seuls besoins résultant pour celle-ci des conséquences de l'intervention du 6 avril 2010, elle a entaché son arrêt d'une erreur de droit. <br/>
<br/>
              Sur le pourvoi incident de l'ONIAM :<br/>
<br/>
              5. Il ressort des pièces du dossier soumis aux juges du fond que Mme A... n'avait, à la date de l'arrêt attaqué, perçu une allocation personnalisée d'autonomie qu'entre le 29 septembre 2010 et le 30 septembre 2015. La cour administrative d'appel a pu, par suite, sans erreur de droit, ne déduire de la somme accordée à Mme A... au titre de l'assistance par une tierce personne que les sommes correspondant au montant de cette prestation versées entre ces deux dates. Par ailleurs, il ressort des termes de son arrêt qu'elle a également jugé que, pour l'avenir, de tels versement viendraient à nouveau en déduction de la rente mise à la charge de l'ONIAM, dans l'hypothèse où Mme A... obtiendrait de nouveau le bénéfice de l'allocation personnalisée d'autonomie. Le moyen tiré de ce qu'elle aurait, sur ce point, commis une erreur de droit doit donc être écarté.<br/>
<br/>
              6. Il résulte de tout ce qui précède que l'arrêt de la cour administrative d'appel de Nantes doit être annulé en tant seulement qu'il statue sur la prise en charge des frais occasionnés à Mme A... par l'hospitalisation et l'assistance apportée à son conjoint et en tant qu'il statue sur la prise en charge des frais d'assistance de la victime par une tierce personne pour la période antérieure à la date de cet arrêt. <br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'ONIAM une somme de 3 000 euros à verser à Mme A... au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces dispositions font obstacle à ce que soit mise à la charge de Mme A..., qui n'est pas, dans la présente instance, la partie perdante, la somme que demande l'ONIAM au même titre.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nantes du 11 janvier 2019 est annulé en tant qu'il statue sur la prise en charge des frais occasionnés à Mme A... par l'hospitalisation et l'assistance apportée à M. A... et en tant qu'il statue sur la prise en charge des frais d'assistance par une tierce personne de Mme A... pour la période antérieure à la date de cet arrêt.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nantes dans la limite de la cassation prononcée.<br/>
<br/>
Article 3 : L'ONIAM versera à Mme A... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Le pourvoi incident de l'ONIAM et le surplus des conclusions des parties sont rejetés.<br/>
<br/>
Article 5 : La présente décision sera notifiée à Mme B... A... et à l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
