<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029255217</ID>
<ANCIEN_ID>JG_L_2014_07_000000372699</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/25/52/CETATEXT000029255217.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  2ème sous-section jugeant seule, 16/07/2014, 372699, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-07-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>372699</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 2ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP HEMERY, THOMAS-RAQUIN</AVOCATS>
<RAPPORTEUR>Mme Airelle Niepce</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:372699.20140716</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 8 octobre 2013 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre de l'égalité des territoires et du logement ; le ministre de l'égalité des territoires et du logement demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 11MA04645 du 30 juillet 2013 par lequel la cour administrative d'appel de Marseille a, d'une part, annulé le jugement n° 0900570 du 20 octobre 2011 par lequel le tribunal administratif de Marseille a, sur déféré du préfet des Bouches-du-Rhône, annulé l'arrêté en date du 9 septembre 2008 par lequel le maire de la commune de Peynier avait accordé à Mme B...A...un permis de construire, d'autre part, rejeté le déféré du préfet des Bouches-du-Rhône ; <br/>
<br/>
              2°) réglant l'affaire au fond de rejeter l'appel de la commune de Peynier devant la cour administrative d'appel ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Airelle Niepce, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Hemery, Thomas-Raquin, avocat de la commune de Peynier ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que le ministre de l'égalité des territoires et du logement demande l'annulation de l'arrêt du 30 juillet 2013 par lequel la cour administrative d'appel de Marseille a, d'une part, annulé le jugement du 20 octobre 2011 par lequel le tribunal administratif de Marseille a, sur déféré du préfet des Bouches-du-Rhône, annulé l'arrêté en date du 9 septembre 2008 par lequel le maire de la commune de Peynier avait accordé à Mme B...A...un permis de construire, d'autre part, rejeté le déféré du préfet des Bouches-du-Rhône ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 611-7 du code de justice administrative : " Lorsque la décision lui paraît susceptible d'être fondée sur un moyen relevé d'office, le président de la formation de jugement ou, au Conseil d'Etat, la sous-section chargée de l'instruction en informe les parties avant la séance de jugement et fixe le délai dans lequel elles peuvent, sans qu'y fasse obstacle la clôture éventuelle de l'instruction, présenter leurs observations sur le moyen communiqué. " ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier des juges du fond et des énonciations de l'arrêt attaqué que, le 26 juin 2013, soit la veille de la clôture de l'instruction, la cour a informé les parties de ce qu'elle était susceptible de relever d'office un moyen tiré de ce qu'en raison de l'effet rétroactif qui s'attache à l'annulation pour excès de pouvoir du plan local d'urbanisme de la commune de Peynier prononcée par un jugement du 24 février 2011 du tribunal administratif de Marseille, confirmé par arrêt de la cour administrative d'appel du 21 février 2013, le tribunal ne pouvait prononcer l'annulation du permis de construire en litige pour méconnaissance du règlement du document d'urbanisme annulé ; qu'un délai de six jours a été accordé aux parties pour présenter leurs observations ; que le préfet des Bouches-du-Rhône a reçu le 26 juin cette communication de la cour ; <br/>
<br/>
              4. Considérant que pour rejeter le déféré du préfet des Bouches-du-Rhône après avoir annulé le jugement du tribunal administratif, la cour administrative d'appel a écarté comme irrecevable le moyen soulevé par le préfet à la suite de l'information donnée aux parties en application de l'article R. 611-7, tiré de ce que le permis de construire litigieux méconnaissait les dispositions du règlement de la zone NC du plan d'occupation des sols antérieur remis en vigueur par l'effet de l'annulation du plan local d'urbanisme, au motif que ce moyen avait été soulevé après la clôture de l'instruction fixée au 27 juin 2013 ; <br/>
<br/>
              5. Considérant toutefois que la cour ne pouvait, sans méconnaître en l'espèce les exigences de la procédure contradictoire et les dispositions de l'article R. 611-7 du code de justice administrative, s'abstenir de rouvrir l'instruction afin de permettre aux parties de débattre du moyen nouveau qui avait été soulevé par le préfet en conséquence du moyen d'ordre public communiqué par la cour ; que, par suite, le ministre de l'égalité des territoires et du logement est fondé à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative de Marseille  du 30 juillet 2013 est annulé. <br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
Article 3 : La présente décision sera notifiée à la ministre du logement et de l'égalité des territoires, à la commune de Peynier et à Mme B...A....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
