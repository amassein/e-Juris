<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032409024</ID>
<ANCIEN_ID>JG_L_2016_04_000000393104</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/40/90/CETATEXT000032409024.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 15/04/2016, 393104, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-04-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>393104</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Cécile Barrois de Sarigny</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:393104.20160415</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 3 septembre 2015 au secrétariat du contentieux du Conseil d'Etat, l'Amicale Laïque Aplemont Le Havre Basket demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 3 juillet 2015 par laquelle le bureau fédéral de la Fédération française de basketball a décidé de ne pas pourvoir les places vacantes en Ligue Féminine 2 à l'issue de la saison 2014-2015 ;<br/>
<br/>
              2°) d'enjoindre sa réintégration dans le championnat de Ligue Féminine 2 pour la saison 2015-2016. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du sport ;<br/>
              - les règlements sportifs généraux de la Fédération française de basketball ;<br/>
              - le règlement sportif particulier de la Ligue féminine 2 de basketball ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Barrois de Sarigny, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'aux termes de l'article R. 311-1 du code de justice administrative : " Le Conseil d'Etat est compétent pour connaître en premier et dernier ressort :/ (...) 2° Des recours dirigés contre les actes réglementaires des ministres et des autres autorités à compétence nationale et contre leurs circulaires et instructions de portée générale " ; <br/>
<br/>
              2.	Considérant qu'aux termes de l'article 19 des règlements sportifs généraux de la Fédération française de basketball : " (...) Dans l'hypothèse où, pour la saison sportive suivante, le nombre d'équipes ayant gagné sportivement le droit de s'engager dans une division est inférieur au nombre de places prévues pour l'organisation du championnat eu égard aux présents règlements sportifs, notamment pour cause de rétrogradation, de refus d'engagement, de liquidation ou toute autre cause, le Bureau Fédéral est compétent afin de se prononcer sur un éventuel besoin de remplacement total ou partiel jusqu'au 15 juillet. Au-delà de cette date, toute place vacante ne sera plus pourvue " ; qu'aux termes de l'article 1er du règlement sportif particulier de la Ligue féminine 2 de basketball, alors en vigueur : " Les 14 équipes sont groupées en une poule unique (...) Les équipes classées 13ème et 14ème de la phase 1 sont reléguées en NF1 pour la saison suivante. Si l'équipe du CFBB se trouve en situation de relégation en NF1 pour la saison suivante, elle sera maintenue en LF2 et l'équipe classée 12ème de la phase 1 sera reléguée en NF1 pour la saison suivante " ; <br/>
<br/>
              3.	Considérant qu'il ressort des pièces du dossier que l'Amicale Laïque Aplemont Le Havre Basket était, à l'issue de la saison 2014 -2015 du championnat de Ligue féminine 2, en situation d'être reléguée dans le championnat national 1 dès lors que, si elle était classée 12ème, l'équipe du Centre fédéral de basketball (CFBB) classée en 13ème position se trouvait en situation de relégation ; qu'à l'issue de la même saison deux places restaient vacantes dans le championnat de Ligue féminine 2 en raison de la rétrogradation pour raisons financières d'un club qui ne devait pas être relégué en application des dispositions précitées de l'article 1er du règlement sportif particulier de cette Ligue et du refus d'un club de National Féminine 1 de participer à ce championnat alors qu'il avait gagné sportivement le droit de s'y engager ; que par une délibération du 3 juillet 2015, le bureau fédéral de la Fédération française de basketball a décidé, en application des dispositions précitées de l'article 19 des règlements sportifs généraux de cette fédération, de ne pas pourvoir les deux places restant vacantes ; <br/>
<br/>
              4.	Considérant que, même si elle a eu pour conséquence de limiter à douze, au lieu de quatorze, le nombre des équipes participant au championnat de Ligue Féminine 2 pour la saison 2015-2016, la délibération attaquée du 3 juillet 2015 par laquelle le bureau fédéral de la Fédération française de basketball a décidé, comme le lui permettaient les règlements applicables, de ne pas pourvoir les deux places vacantes du championnat de Ligue féminine 2 ne constitue pas un acte réglementaire ; que, par suite, cette délibération n'entre pas dans le champ du 2° de l'article R. 311-1 du code de justice administrative ; qu'aucune autre disposition du code de justice administrative ne donne compétence au Conseil d'Etat pour connaître en premier et dernier ressort des conclusions de l'Amicale Laïque Aplemont Le Havre Basket tendant à l'annulation de cette délibération ; qu'il y a lieu, en application de l'article R. 351-1 du code de justice administrative, d'en attribuer le jugement au tribunal administratif de Paris, compétent pour en connaître en vertu de l'article R. 312-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le jugement de la requête de l'Amicale Laïque Aplemont Le Havre Basket est attribué au tribunal administratif de Paris.<br/>
<br/>
Article 2 : La présente décision sera notifiée à l'Amicale Laïque Aplemont Le Havre Basket, à la Fédération française de basketball et au ministre de la ville, de la jeunesse et des sports. Copie en sera adressée au président du tribunal administratif de Paris. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
