<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029471749</ID>
<ANCIEN_ID>JG_L_2014_08_000000382511</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/47/17/CETATEXT000029471749.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 26/08/2014, 382511, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-08-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>382511</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LESOURD ; SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2014:382511.20140826</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête enregistrée le 15 juillet 2014 au secrétariat du contentieux du Conseil d'Etat, présentée pour M. C...A..., demeurant... ; le requérant demande au juge des référés du Conseil d'Etat :<br/>
<br/>
              1°) d'ordonner, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution des arrêtés n° 2014-367 et n° 2014-370 du 26 mai 2014 par lesquels la présidente de l'université des Antilles et de la Guyane, d'une part, l'a suspendu de ses fonctions d'enseignant-chercheur et de toutes les activités qui y sont attachées pour une durée de douze mois, dans l'intérêt du service, à titre conservatoire et sans privation de traitement, et, d'autre part, lui a interdit l'accès à l'établissement pendant la durée de sa suspension de fonctions, sauf autorisation écrite expresse accordée par le chef d'établissement ;<br/>
<br/>
              2°) de mettre à la charge de l'université des Antilles et de la Guyane la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              il soutient que :<br/>
<br/>
              - la condition d'urgence est remplie dès lors que les arrêtés litigieux portent gravement atteinte, d'une part, à sa situation personnelle, et, d'autre part, à un intérêt public, en perturbant le bon fonctionnement du service ;<br/>
<br/>
              - les arrêtés contestés revêtent un caractère manifestement excessif, dès lors que les faits qui lui sont reprochés ne sont pas constitutifs d'une faute, ni d'une atteinte grave au fonctionnement du service, et ne justifient pas que la suspension prononcée soit étendue au-delà des activités relatives à la gestion du laboratoire CEREGMIA ;<br/>
<br/>
              - la présidente de l'université des Antilles et de la Guyane, par les arrêtés contestés, a entendu paralyser ses activités, pour des motifs étrangers à l'intérêt du service, commettant ainsi un détournement de pouvoir ;<br/>
<br/>
              - les arrêtés contestés, en l'absence de poursuites disciplinaires ou pénales, revêtent le caractère d'une sanction déguisée et, dès lors, portent atteinte à l'inamovibilité, à l'indépendance et à la liberté d'expression des membres de l'enseignement supérieur ;<br/>
<br/>
              - la présidente de l'université des Antilles et de la Guyane n'établit pas l'existence de menaces graves à l'ordre public justifiant la mesure d'interdiction de l'accès à l'établissement ;<br/>
<br/>
<br/>
              Vu le mémoire en défense, enregistré le 12 août 2014, présenté pour l'université des Antilles et de la Guyane, qui conclut au rejet de la requête et à ce que la somme de 4 000 euros soit mise à la charge de M. A...au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              elle soutient que :<br/>
<br/>
              - la condition d'urgence n'est pas satisfaite, dès lors que le requérant n'établit le caractère grave et immédiat ni du préjudice dont il se prévaut, ni d'une atteinte à un intérêt public ;<br/>
<br/>
              - aucun des moyens soulevés par le requérant n'est de nature à faire naître un doute sérieux quant à la légalité des arrêtés contestés ;<br/>
<br/>
              Vu le mémoire en réplique, enregistré le 18 août 2014, présenté pour M. C...A..., qui conclut aux mêmes fins par les mêmes moyens ; il soutient en outre que :<br/>
<br/>
              - la condition d'urgence est remplie, dès lors que les mesures mises en oeuvre par l'université des Antilles et de la Guyane ne sont pas de nature à mettre un terme à l'atteinte portée, d'une part, au bon fonctionnement des enseignements, d'autre part, à la situation des doctorants ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. A..., d'autre part, l'université des Antilles et de la Guyane ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 19 août 2014 à 10 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Lesourd, avocat au Conseil et à la Cour de cassation, avocat de M. C...A... ; <br/>
              - M. C...A... ; <br/>
              - Me Garreau, avocat au Conseil et à la Cour de cassation, avocat de l'université des Antilles et de la Guyane ;<br/>
              - la présidente de l'université des Antilles et de la Guyane ;<br/>
              et à l'issue de laquelle le juge des référés a prolongé l'instruction jusqu'au 21 août à 18 heures ;<br/>
<br/>
              Vu les observations complémentaires, enregistrées le 20 août 2014, présentées pour l'université des Antilles et de la Guyane, qui persiste dans ses précédentes conclusions ;<br/>
<br/>
              Vu les observations complémentaires, enregistrées le 21 août 2014, présentées pour M.A..., qui persiste dans ses précédentes conclusions ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'éducation ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ;<br/>
<br/>
              Sur les conclusions tendant à la suspension de l'exécution de l'arrêté de la présidente de l'université des Antilles et de la Guyane décidant de suspendre M. A...de ses fonctions d'enseignant-chercheur :<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 951-4 du code de l'éducation : " Le ministre chargé de l'enseignement supérieur peut prononcer la suspension d'un membre du personnel de l'enseignement supérieur pour un temps qui n'excède pas un an, sans suspension de traitement " ; que l'article L. 951-3 de ce code autorise le ministre chargé de l'enseignement supérieur à déléguer aux présidents des universités tout ou partie de ses pouvoirs en matière de recrutement et de gestion des personnels relevant de son autorité ; qu'en application de ces dernières dispositions, l'article 2 de l'arrêté du 10 février 2012 portant délégation de pouvoirs en matière de recrutement et de gestion de certains personnels enseignants des établissements publics d'enseignement supérieur et de recherche, prévoit que " les présidents et les directeurs des établissements publics d'enseignement supérieur dont la liste est fixée à l'article 3 du présent arrêté reçoivent délégation des pouvoirs du ministre chargé de l'enseignement supérieur pour le recrutement et la gestion des personnels enseignants mentionnés à l'article 1er du présent arrêté en ce qui concerne : [...] 24. La suspension " ; <br/>
<br/>
              3. Considérant que M.A..., professeur des universités, qui exerce les fonctions de professeur d'économie à l'université des Antilles et de la Guyane et de directeur-adjoint du centre d'étude et de recherche en économie, gestion modélisation et informatique appliquée (CEREGMIA) de cette université, a fait l'objet d'une mesure de suspension de l'ensemble de ses fonctions d'enseignant-chercheur, pour une durée de douze mois, prononcée dans l'intérêt du service par la présidente de l'université ;<br/>
<br/>
              4. Considérant que la suspension d'un professeur est une mesure de caractère conservatoire, prononcée dans le souci de préserver le bon fonctionnement du service public universitaire ; qu'elle permet, en particulier, d'écarter à titre provisoire un enseignant, dans l'attente de l'issue d'une procédure disciplinaire ou de poursuites pénales engagées à son encontre, lorsque la continuation des activités de l'intéressé au sein de l'établissement présente des inconvénients suffisamment sérieux pour le service ou pour le déroulement des procédures en cours ; que, si l'autorité hiérarchique dispose à cet égard d'un large pouvoir d'appréciation, le principe et la durée de la suspension ne doivent pas être manifestement excessifs au regard de l'ensemble des circonstances de l'espèce ;<br/>
<br/>
              5. Considérant qu'il résulte tant de l'instruction écrite que des débats au cours de l'audience publique que la mesure de suspension de fonctions contestée a été prise en raison de la gravité des irrégularités qui auraient été commises depuis plusieurs années dans le cadre de la gestion du CEREGMIA et auxquelles le requérant aurait pris part en tant que directeur-adjoint de ce centre, en particulier en ce qui concerne les procédures de dépenses, la passation de marchés de prestations de services et l'utilisation de fonds européens, et dans celui de la mise en oeuvre, dans laquelle il a joué un rôle important, d'une convention de collaboration entre l'Agence universitaire de la Francophonie et l'université des Antilles et de la Guyane ; que si le requérant conteste la réalité des faits, ceux-ci ont fait l'objet de constatations dans un rapport de la Cour des comptes remis le 11 janvier 2013 à l'administrateur provisoire de l'université, dans un rapport d'information du Sénat sur la situation et l'avenir du système universitaire aux Antilles et en Guyane publié le 16 avril 2014 et, enfin, dans un rapport conjoint de l'inspection générale de l'administration de l'éducation nationale et de la recherche et du contrôle général économique et financier remis en avril 2014 ; qu'en outre une information judiciaire a été ouverte sur les mêmes faits ; qu'ainsi le moyen tiré de ce que la mesure contestée ne reposerait pas sur des justifications suffisantes n'est pas, en l'état de l'instruction, de nature à créer un doute sérieux sur sa légalité ;<br/>
<br/>
              6. Considérant qu'il ressort des rapports mentionnés ci-dessus que les faits relevés à l'encontre de M. A... constitueraient, s'ils étaient avérés, des fautes professionnelles et sont, en l'état où ils sont formulés, de nature à mettre en doute sa probité et, par suite, l'autorité morale et l'exemplarité attendues d'un enseignant-chercheur ; que la mission conjointe de l'inspection générale de l'administration de l'éducation nationale et de la recherche et du contrôle général économique et financier a d'ailleurs préconisé l'engagement de poursuites disciplinaires à l'égard du requérant et la suspension de ses fonctions " afin de restaurer des conditions normales de gestion dans le laboratoire CEREGMIA et d'enrayer un climat délétère indigne de la communauté universitaire " ; que la présidente de l'université a saisi le Conseil national de l'enseignement supérieur et de la recherche d'une demande de dépaysement de poursuites disciplinaires devant être engagées à l'encontre de M. A... sur le fondement des faits constatés dans les rapports de la Cour des comptes et de la mission conjointe ; qu'enfin, si l'information judiciaire ouverte le 7 avril 2014 étant, à ce stade, une information contre X, ne vise pas nominativement le requérant, lequel a été entendu dans le cadre de cette procédure sans être mis pénalement en cause, cette information se base sur les mêmes faits et a été ouverte avec les qualifications pénales de favoritisme, escroquerie et détournement de fonds publics ; que, dans ces conditions, le moyen selon lequel la mesure de suspension prise à l'encontre de M. A... serait manifestement excessive tant dans son champ, pour ne pas avoir été limitée aux seules fonctions de gestion du CEREGMIA, que dans sa durée, n'est pas, en l'état de l'instruction, de nature à faire naître un doute sérieux sur sa légalité ; <br/>
<br/>
              7. Considérant, enfin, que les moyens tirés de ce que cette mesure de suspension serait constitutive d'une sanction déguisée et serait entachée de détournement de pouvoir ne peuvent pas être regardés comme étant de nature à susciter un doute sérieux sur la légalité de l'arrêté litigieux ; <br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin de se prononcer sur la condition d'urgence, les conclusions de la requête de M. A...tendant à la suspension de l'exécution de l'arrêté prononçant la suspension de ses fonctions d'enseignant-chercheur doivent être rejetées ;<br/>
<br/>
              Sur les conclusions tendant à la suspension de l'exécution de l'arrêté de la présidente de l'université des Antilles et de la Guyane décidant d'interdire à M. A...l'accès à l'université pendant la durée de la suspension de ses fonctions d'enseignant-chercheur :<br/>
<br/>
              9. Considérant que la mesure d'interdiction d'accès à l'université pendant la durée de la suspension de ses fonctions d'enseignant-chercheur décidée à l'encontre de M. A... a été prise sur le fondement de l'article R. 712-8 du code de l'éducation aux termes duquel : " En cas de désordre ou de menace de désordre dans les enceintes et locaux définis à l'article R. 712-1, l'autorité responsable désignée à cet article en informe immédiatement le recteur chancelier. Dans les cas mentionnés au premier alinéa : 1° La même autorité peut interdire à toute personne et, notamment, à des membres du personnel et à des usagers de l'établissement ou des autres services ou organismes qui y sont installés l'accès de ces enceintes et locaux. Cette interdiction ne peut être décidée pour une durée supérieure à trente jours. Toutefois, au cas où des poursuites disciplinaires ou judiciaires seraient engagées, elle peut être prolongée jusqu'à la décision définitive de la juridiction saisie " ;<br/>
<br/>
              10. Considérant que, si la décision d'interdiction d'accès à l'université constitue une mesure de police, elle a été conçue comme le prolongement de la décision de suspension des fonctions d'enseignant-chercheur du requérant sur la durée de laquelle sa durée a été alignée ; qu'ainsi, en tout état de cause et eu égard à l'impératif de célérité qui caractérise la procédure de référé, la demande tendant à sa suspension doit être regardée comme connexe à celle tendant à la demande de suspension des fonctions d'enseignant-chercheur ; qu'il appartient au Conseil d'Etat statuant en référé d'en connaître en premier ressort ;<br/>
<br/>
              11. Considérant que la condition d'urgence à laquelle est subordonné le prononcé d'une décision de suspension de l'exécution d'un acte administratif doit être regardée comme remplie lorsque cet acte préjudicie de manière suffisamment grave et immédiate à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ; qu'en l'espèce, la mesure interdisant à un professeur d'université d'accéder aux locaux de l'université dans laquelle il a été nommé ne peut être regardée comme préjudiciant de manière suffisamment grave et immédiate à sa situation d'enseignant chercheur ou à un intérêt public dès lors que cette mesure n'est applicable que durant la période au cours de laquelle il fait l'objet d'une mesure de suspension de l'ensemble de ses fonctions d'enseignant-chercheur ; que, en effet, durant cette période, l'intéressé, qui continue à percevoir son traitement, n'assure aucune activité d'enseignement, d'encadrement de doctorants et de recherche ; que, si l'interdiction d'accès à la bibliothèque universitaire située sur le campus peut être de nature à rendre plus difficile la poursuite d'une activité de recherche personnelle et le maintien du niveau de ses connaissances scientifiques, cet inconvénient peut être pallié dans une large mesure par l'accès aux informations disponibles en ligne ; qu'il n'est pas établi que l'interdiction d'accès à l'université préjudicierait gravement, par elle-même, dans les circonstances ci-dessus mentionnées dans lesquelles elle a été prononcée, au fonctionnement de l'université ; <br/>
<br/>
              12. Considérant que, dans ces conditions, la condition d'urgence ne peut être regardée comme remplie ; qu'il en résulte que, sans qu'il soit besoin de se prononcer sur l'existence d'un moyen propre à créer, en l'état de l'instruction, un doute sur la légalité de la décision contestée, les conclusions de M. A...tendant à la suspension de l'exécution de l'arrêté de la présidente de l'université des Antilles et de la Guyane décidant de lui interdire l'accès à l'université pendant la durée de la suspension de ses fonctions d'enseignant-chercheur doivent être rejetées ; <br/>
<br/>
              13. Considérant qu'il résulte de tout ce qui précède que M. A...n'est pas fondé à demander la suspension de l'exécution des arrêtés du 26 mai 2014 par lesquels la présidente de l'université des Antilles et de la Guyane l'a suspendu de l'ensemble de ses fonctions d'enseignant-chercheur et lui a interdit l'accès à l'université pendant la durée de sa suspension de fonctions ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de de l'université des Antilles et de la Guyane à ce titre ; que, dans les circonstances de l'espèce, il y a lieu de mettre à la charge de M. A... une somme de 3 000 euros à ce titre ;<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. C...A...est rejetée.<br/>
Article 2 : M. C...A...versera à l'université des Antilles et de la Guyane une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente ordonnance sera notifiée à M. C...A...et à l'université des Antilles et de la Guyane.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
