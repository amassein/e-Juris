<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044153753</ID>
<ANCIEN_ID>JG_L_2021_09_000000443763</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/15/37/CETATEXT000044153753.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 30/09/2021, 443763, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-09-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>443763</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE CHAISEMARTIN, DOUMIC-SEILLER</AVOCATS>
<RAPPORTEUR>M. Arno Klarsfeld</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Domingo</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:443763.20210930</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au tribunal administratif de Lille de condamner l'Etat à lui payer la somme de 1 704,20 euros à raison d'arriérés de salaires.<br/>
<br/>
              Par une ordonnance n° 1907565 du 5 août 2020, le président de la 8ème chambre du tribunal administratif de Lille lui a donné acte du désistement de sa demande en application de l'article R. 612-5-1 du code de justice administrative.<br/>
<br/>
              Par un pourvoi, deux mémoires complémentaires et un mémoire en réplique, enregistrés les 4 et 7 septembre 2020, 4 décembre 2020 et 26 mai 2021 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la SCP de Chaisemartin, Doumic-Seiller, son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arno Klarsfeld, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Laurent Domingo, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP de Chaisemartin, Doumic-Seiller, avocat de M. A... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. L'article R. 612-5-1 du code de justice administrative dispose que : " Lorsque l'état du dossier permet de s'interroger sur l'intérêt que la requête conserve pour son auteur, le président de la formation de jugement ou le président de la chambre chargée de l'instruction, peut inviter le requérant à confirmer expressément le maintien de ses conclusions. La demande qui lui est adressée mentionne que, à défaut de réception de cette confirmation à l'expiration du délai fixé, qui ne peut être inférieur à un mois, il sera réputé s'être désisté de l'ensemble de ses conclusions ". <br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que M. A... a demandé au tribunal administratif de Lille de condamner l'Etat à lui verser une somme de 1 704,20 euros au titre des arriérés de la rémunération due à raison du travail effectué au centre de détention de Bapaume pour la période d'avril 2015 à octobre 2018. Cette requête était assortie d'une demande de référé provision portant sur la même somme. Par une ordonnance du 20 mars 2019, le juge des référés du tribunal administratif a rejeté la demande de provision au motif que le requérant avait accepté, en cours d'instance, la proposition de l'administration faisant intégralement droit à sa demande. Par un courrier daté du 9 juin 2020, mis à disposition du conseil du requérant, par la voie de l'application informatique Télérecours le même jour et dont il a été accusé réception le 26 juin 2020, le président de la 8ème chambre du tribunal administratif de Lille a demandé à M. A... de confirmer le maintien des conclusions de sa requête au fond, en précisant qu'à défaut de réception de cette confirmation dans un délai d'un mois, il serait réputé s'être désisté de ses conclusions en application  des dispositions de 1'article R. 612-5-1 du code de justice administrative. Par une ordonnance du 5 août 2020, il a pris acte du désistement du requérant, compte tenu de l'absence de réponse à la demande qui lui avait été faite le 9 juin 2020. M. A... se pourvoit en cassation contre cette ordonnance. <br/>
<br/>
              3. A l'occasion de la contestation de l'ordonnance donnant acte d'un désistement par application de l'article R. 612-5-1 du code de justice administrative en l'absence de réponse du requérant à la demande de confirmation de ses conclusions dans le délai qui lui a été imparti, il incombe au juge de cassation, saisi de moyens en ce sens, de vérifier que l'intéressé a reçu la demande de confirmation du maintien de ses conclusions, que cette demande laissait au requérant un délai d'au moins un mois pour y répondre et l'informait des conséquences d'un défaut de réponse dans ce délai et que le requérant s'est abstenu de répondre en temps utile. Le juge n'est tenu d'indiquer ces motifs ni dans la demande de confirmation du maintien des conclusions qu'il adresse au requérant, ni dans l'ordonnance par laquelle il prend acte, le cas échéant, de son désistement. Si les motifs pour lesquels le signataire de l'ordonnance, auquel il incombe de veiller à une bonne administration de la justice, estime que l'état du dossier permet de s'interroger sur l'intérêt que la requête conserve pour son auteur ne peuvent en principe être utilement discutés devant le juge de cassation, il appartient néanmoins à ce dernier de censurer l'ordonnance qui lui est déférée dans le cas où il juge, au vu de l'ensemble des circonstances de l'espèce, qu'il a été fait un usage abusif de la faculté ouverte par l'article R. 612-5-1 du code de justice administrative. <br/>
<br/>
              4. En premier lieu, d'une part, le signataire de l'ordonnance donnant acte d'un désistement en application de l'article R. 612-5-1 du code de justice administrative n'a pas à mentionner les motifs pour lesquels il estime que l'état du dossier permet de s'interroger sur l'intérêt que la requête conserve pour son auteur. D'autre part, la mention, dans le courrier du tribunal informant le requérant qu'à défaut de confirmation du maintien de ses conclusions il serait réputé s'être désisté de l'ensemble de celles-ci, de ce qu'il lui appartenait de produire dans le délai d'un mois soit un mémoire, soit une lettre indiquant qu'il estimait inutile de répliquer mais qu'il maintenait sa requête, soit une lettre de désistement n'était pas de nature, alors même qu'en l'espèce l'administration n'avait pas produit de mémoire en défense, à empêcher ce dernier de comprendre la portée de son absence de réponse à ce courrier. M. A... n'est dès lors pas fondé à se prévaloir de l'irrégularité de l'ordonnance attaquée lui donnant acte de son désistement sur le fondement de l'article R. 612-5-1 du code de justice administrative. <br/>
<br/>
              5. En second lieu, dès lors que M. A... avait accepté, dans le cadre de la procédure de référé provision dont il avait aussi saisi le tribunal, la proposition d'indemnisation de l'administration qui faisait droit à l'intégralité de sa demande au fond, l'auteur de l'ordonnance attaquée, qui est suffisamment motivée, a pu, sans faire un usage abusif de la faculté ouverte par les dispositions de l'article R. 612-5-1 du code de justice administrative, demander à M. A... s'il maintenait les conclusions de sa requête au fond. Contrairement à ce que soutient le requérant, cette faculté ne méconnaît pas, par elle-même, les exigences découlant du respect du droit à un recours juridictionnel effectif et du principe du caractère contradictoire de la procédure, même dans l'hypothèse où l'administration n'a pas produit de mémoire en défense.<br/>
<br/>
              6. Il résulte de tout ce qui précède que M. A... n'est pas fondé à demander l'annulation de l'ordonnance qu'il attaque. Son pourvoi doit, par suite, être rejeté, y compris, par voie de conséquence, les conclusions présentées au titre des articles L.761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A... est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. B... A... et au garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
