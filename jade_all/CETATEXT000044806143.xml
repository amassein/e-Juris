<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044806143</ID>
<ANCIEN_ID>JG_L_2021_12_000000437426</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/80/61/CETATEXT000044806143.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 30/12/2021, 437426, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>437426</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BAUER-VIOLAS, FESCHOTTE-DESBOIS, SEBAGH ; LE PRADO</AVOCATS>
<RAPPORTEUR>Mme Pearl Nguyên Duy</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:437426.20211230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une décision du 29 décembre 2020, le Conseil d'Etat statuant au contentieux a prononcé l'admission des conclusions de Mme L... B... et autres dirigées contre l'arrêt n° 17MA01875 du 7 novembre 2019 de la cour administrative d'appel de Marseille en tant qu'il se prononce sur le préjudice moral de Mme I... B....<br/>
<br/>
              Par un mémoire en défense enregistré le 5 mars 2021 au secrétariat du contentieux du Conseil d'Etat, le centre hospitalier de Cannes conclut au rejet du pourvoi. Il soutient que ses moyens ne sont pas fondés.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pearl Nguyên Duy, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteure publique.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Bauer-Violas, Feschotte-Desbois, Sebagh, avocat de Mme B... et autres, et à Me Le Prado, avocat du centre hospitalier de Cannes.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. A la suite du décès de M. H... B... au centre hospitalier de Cannes le 5 août 2013, ses ayants droit ont demandé au tribunal administratif de Nice la condamnation de l'établissement hospitalier à les indemniser des préjudices qu'ils estiment avoir subis du fait des fautes commises dans la prise en charge du défunt. Par un jugement du 28 février 2017, le tribunal a rejeté leur demande. Sur appel des intéressés, la cour administrative d'appel de Marseille a, par un arrêt du 7 novembre 2019, jugé qu'un retard de diagnostic fautif a été à l'origine d'une perte de chance, évaluée à 65 %, d'éviter le décès de M. B.... Elle a, par suite, après avoir annulé le jugement du tribunal administratif, condamné le centre hospitalier de Cannes à verser aux héritiers de Thierry B... la somme de 2 000 euros, à Mme L... B... la somme de 16 250 euros, à Mme I... B..., M. C... B..., Mme A... B... et Mme K... G..., la somme de 4 225 euros chacun et à Mme J... E... la somme de 1 000 euros. Mme B... et autres demandent l'annulation de cet arrêt en tant qu'il se prononce sur l'indemnisation du préjudice moral de Mme I... B....<br/>
<br/>
              2. En évaluant à 4 225 euros, après application du taux de perte de chance de 65 %, le préjudice moral subi par Mme I... B..., alors que celle-ci était encore mineure à la date du décès de son père, la cour a dénaturé les pièces du dossier. Les requérants sont par suite fondés à demander l'annulation de l'arrêt attaqué en tant qu'il se prononce sur les conclusions tendant à l'indemnisation du préjudice d'affection subi par Mme I... B.... <br/>
<br/>
              3. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              4. Il sera fait une juste appréciation du préjudice d'affection subi par Mme I... B... en mettant à ce titre à la charge du centre hospitalier de Cannes, après application du taux de perte de chance de 65%, une somme de 10 000 euros.<br/>
<br/>
              5. Il résulte de ce qui précède que les requérants sont fondés à demander la condamnation du centre hospitalier de Cannes à leur verser la somme de 10 000 euros.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du centre hospitalier de Cannes une somme de 500 euros à verser à Mme L... B..., une somme de 500 euros à verser à Mme I... B..., une somme de 500 euros à verser à M. C... B..., une somme de 500 euros à verser à Mme A... B..., une somme de 500 euros à verser à Mme K... G... et une somme de 500 euros à verser à Mme J... E... au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 7 novembre 2019 de la cour administrative d'appel de Marseille est annulé en tant qu'il se prononce sur le préjudice moral de Mme I... B....<br/>
Article 2 : Le centre hospitalier de Cannes est condamné à verser à Mme I... B... la somme de 10 000 euros. <br/>
Article 3 : Le centre hospitalier de Cannes versera à Mme L... B..., Mme I... B..., M. C... B..., Mme A... B..., Mme K... G... et Mme J... E... la somme de 500 euros chacun au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : Le surplus des conclusions de Mme B... et autres est rejeté.<br/>
Article 5 : La présente décision sera notifiée à Mme L... B..., première requérante dénommée, et au centre hospitalier de Cannes. <br/>
              Délibéré à l'issue de la séance du 14 décembre 2021 où siégeaient : M. Jean-Philippe Mochon, assesseur, présidant ; M. Olivier Yeznikian, conseiller d'Etat et Mme Pearl Nguyên Duy, maître des requêtes-rapporteure. <br/>
<br/>
              Rendu le 30 décembre 2021.<br/>
                 Le président : <br/>
                 Signé : M. N... D...<br/>
 		La rapporteure : <br/>
      Signé : Mme Pearl Nguyên Duy<br/>
                 Le secrétaire :<br/>
                 Signé : M. F... M...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
