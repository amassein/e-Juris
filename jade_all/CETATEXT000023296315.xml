<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000023296315</ID>
<ANCIEN_ID>JG_L_2010_12_000000324181</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/23/29/63/CETATEXT000023296315.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 23/12/2010, 324181, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2010-12-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>324181</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR>M. Jérôme  Michel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olléon Laurent</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2010:324181.20101223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi, enregistré le 16 janvier 2009 au secrétariat du contentieux du Conseil d'Etat, présenté par le MINISTRE DU BUDGET, DES COMPTES PUBLICS ET DE LA FONCTION PUBLIQUE ; le MINISTRE DU BUDGET, DES COMPTES PUBLICS ET DE LA FONCTION PUBLIQUE demande au Conseil d'Etat d'annuler l'article 3 de l'arrêt n° 07BX01203 en date du 14 novembre 2008 par lequel la cour administrative d'appel de Bordeaux a rejeté le surplus des conclusions de son recours tendant à l'annulation du jugement n° 0402742 du 30 janvier 2007 par lequel le tribunal administratif de Toulouse a prononcé la décharge des rappels de taxe sur la valeur ajoutée mis à la charge de la société anonyme Michel Thierry au titre de la période du 1er janvier 1995 au 31 décembre 1997 ainsi que des intérêts de retard correspondants ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la sixième directive 77/388/CEE du Conseil, du 17 mai 1977, en matière d'harmonisation des législations des Etats membres relatives aux taxes sur le chiffre d'affaires ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jérôme Michel, Maître des Requêtes, <br/>
<br/>
              - les observations de la SCP Delaporte, Briard, Trichet, avocat de la société Michel Thierry, <br/>
<br/>
              - les conclusions de M. Laurent Olléon, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Delaporte, Briard, Trichet, avocat de la société Michel Thierry ; <br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'aux termes de l'article 271 du code général des impôts : "I. 1. La taxe sur la valeur ajoutée qui a grevé les éléments du prix d'une opération imposable est déductible de la taxe sur la valeur ajoutée applicable à cette opération. (...)" ; qu'il résulte de ces dispositions, interprétées à la lumière des paragraphes 1 et 2, 3 et 5 de l'article 17 de la sixième directive du Conseil du 17 mai 1977, que l'existence d'un lien direct et immédiat entre une opération particulière en amont et une ou plusieurs opérations en aval ouvrant droit à déduction est, en principe, nécessaire pour qu'un droit à déduction de la taxe sur la valeur ajoutée en amont soit reconnu à l'assujetti et pour déterminer l'étendue d'un tel droit ; que le droit à déduction de la taxe sur la valeur ajoutée grevant l'acquisition de biens ou de services en amont suppose que les dépenses effectuées pour acquérir ceux-ci fassent partie des éléments constitutifs du prix des opérations taxées en aval ouvrant droit à déduction ; qu'en l'absence d'un tel lien, un assujetti est toutefois fondé à déduire l'intégralité de la taxe sur la valeur ajoutée ayant grevé des biens et services en amont, lorsque les dépenses liées à l'acquisition de ces biens et services font partie de ses frais généraux et sont, en tant que telles, des éléments constitutifs du prix des biens produits ou des services fournis par cet assujetti ; <br/>
<br/>
              Considérant que, lorsqu'une société holding, qui se livre à une activité économique à raison de laquelle elle est assujettie à la taxe sur la valeur ajoutée, envisage de céder tout ou partie des titres de la participation qu'elle détient dans une filiale et expose à cette fin des dépenses en vue de préparer cette cession, elle est en droit, sous réserve de produire des pièces justificatives, de déduire la taxe sur la valeur ajoutée ayant grevé ces dépenses, qui sont réputées faire partie de ses frais généraux et se rattacher aux éléments constitutifs du prix des opérations relevant de cette activité économique ; qu'il en va ainsi lorsque l'opération de cession des titres ne se réalise pas ; que, lorsque cette cession est intervenue, que cette opération soit en dehors du champ d'application de la taxe sur la valeur ajoutée ou dans le champ mais exonérée, l'administration est toutefois fondée à remettre en cause la déductibilité de la taxe ayant grevé de telles dépenses quand, compte tenu des éléments portés à sa connaissance et au vu des pièces qu'il appartient le cas échéant à la société qui les détient de produire, elle établit que cette opération a revêtu un caractère patrimonial dès lors que le produit de cette cession a été distribué, quelles que soient les modalités de cette distribution, ou que, en l'absence d'éléments contraires produits par la société, ces dépenses ont été incorporées dans le prix de cession des titres ; <br/>
<br/>
              Considérant que si la taxe sur la valeur ajoutée ayant grevé les dépenses inhérentes à la transaction elle-même n'est en principe pas déductible dès lors qu'elles présentent un lien direct et immédiat avec l'opération de cession des titres, cette société est néanmoins en droit de déduire cette taxe si, compte tenu de la nature des titres cédés ou par tous éléments probants tels que sa comptabilité analytique, elle établit que ces dépenses n'ont pas été incorporées dans leur prix de cession et que, par suite, elles doivent être regardées comme faisant partie de ses frais généraux et se rattachant ainsi aux éléments constitutifs du prix des opérations relevant des activités économiques qu'elle exerce comme assujettie ; que les mêmes règles s'appliquent dans le cas où les dépenses ont été payées à un même intermédiaire, chargé à la fois de préparer cette cession et de réaliser la transaction, dès lors que ces deux catégories de prestations n'ont pas donné lieu à une rémunération distincte et qu'elles doivent alors être regardées comme un tout indissociable se rattachant à la transaction ;<br/>
<br/>
              Considérant que la cour administrative d'appel de Bordeaux a relevé qu'au cours de la période couverte par la vérification de comptabilité, la société anonyme Michel Thierry, spécialisée dans la confection de tissus pour sièges d'automobiles, avait cédé l'ensemble des titres qu'elle détenait dans une filiale, la société financière Michel Thierry, antérieurement constituée en vue d'acquérir des participations dans le capital d'un équipementier automobile qui était son principal client ; que, pour admettre la déduction de la taxe sur la valeur ajoutée dont ont été grevées les dépenses de conseil payées en 1996 et en 1997 à une banque et à deux cabinets en vue de préparer la cession de cette participation décidée dans le cadre d'une réorientation stratégique de la société, la cour, en jugeant par une appréciation souveraine non arguée de dénaturation qu'il ne résultait pas de l'instruction que ces dépenses auraient été mises à la charge des cessionnaires, n'a pas commis d'erreur de droit ou d'erreur de qualification juridique des faits en jugeant, par un arrêt qui est suffisamment motivé, que ces dépenses avaient revêtu, en dépit de leur affectation à l'opération de cession des titres elle-même exonérée de la taxe sur la valeur ajoutée, le caractère de frais généraux de cette société et qu'elles étaient ainsi des éléments constitutifs du prix des produits ou services que celle-ci commercialisait ; <br/>
<br/>
              Considérant qu'il résulte de ce qui précède que le MINISTRE DU BUDGET, DES COMPTES PUBLICS ET DE LA FONCTION PUBLIQUE n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de faire application des dispositions de l'article L. 761-1 du code de justice administrative et de mettre à la charge de l'Etat, qui est la partie perdante dans la présente instance, une somme de 3 000 euros au titre des frais engagés par la société anonyme Michel Thierry et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du MINISTRE DU BUDGET, DES COMPTES PUBLICS ET DE LA FONCTION PUBLIQUE est rejeté.<br/>
Article 2 : L'Etat versera à la société anonyme Michel Thierry une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée au MINISTRE DU BUDGET, DES COMPTES PUBLICS, DE LA FONCTION PUBLIQUE ET DE LA REFORME DE L'ETAT, PORTE-PAROLE DU GOUVERNEMENT et à la société anonyme Michel Thierry.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
