<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026512058</ID>
<ANCIEN_ID>JG_L_2012_10_000000360790</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/51/20/CETATEXT000026512058.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 19/10/2012, 360790</TITRE>
<DATE_DEC>2012-10-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>360790</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>M. Frédéric Dieu</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:360790.20121019</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et les mémoires complémentaires, enregistrés les 5 juillet, 19 juillet et 18 septembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune de Loupian, représentée par son maire ; la commune de Loupian demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1202363 du 22 juin 2012 par laquelle le juge des référés du tribunal administratif de Montpellier, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, à la demande de M. B...A..., a, en premier lieu, suspendu l'exécution de l'arrêté du 30 avril 2012 par lequel le maire de la commune de Loupian a radié ce dernier des cadres jusqu'à ce qu'il soit statué sur la légalité de cet arrêté, et a, en second lieu, enjoint au maire de la commune de Loupian de réintégrer M. A...dans les effectifs de la commune jusqu'à ce qu'il soit statué sur la légalité de la décision de révocation ;<br/>
<br/>
              2°) de mettre à la charge de M. A...le versement d'une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des communes ;<br/>
<br/>
              Vu la loi n° 84-53 du 26 janvier 1984 ;<br/>
<br/>
              Vu l'ordonnance n° 2012-351 du 12 mars 2012 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Dieu, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Didier, Pinet, avocat de la commune de Loupian,<br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Didier, Pinet, avocat de la commune de Loupian ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; <br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Montpellier que M. B...A...a été recruté en octobre 2010 en qualité d'agent de police municipale par la commune de Loupian (Hérault) ; qu'à la suite de sa condamnation à une peine d'emprisonnement avec sursis par un jugement du tribunal correctionnel de Montpellier du 2 mai 2011, le préfet de l'Hérault, par un arrêté du 14 septembre 2011, lui a retiré l'agrément qui lui avait été délivré le 29 octobre 2010 pour exercer cette fonction ; que, par une première décision du 30 avril 2012, le maire de la commune de Loupian a substitué à la sanction de révocation prononcée initialement à l'encontre de M. A...une sanction d'exclusion temporaire de cinq mois sans traitement pour la période du 30 novembre 2011 au 29 avril 2012 ; que, par une seconde décision du même jour, le maire a prononcé sa radiation des cadres ; que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Montpellier a suspendu l'exécution de cette seconde décision et enjoint au maire de la commune de Loupian de réintégrer provisoirement M. A...dans les effectifs de la commune jusqu'à ce qu'il soit statué sur la légalité de cette décision ;<br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 412-49 du code des communes, dans sa version issue de l'ordonnance du 12 mars 2012 relative à la partie législative du code de la sécurité intérieure : " Lorsque l'agrément d'un agent de police municipale est retiré ou suspendu dans les conditions prévues au troisième alinéa de l'article L. 511-2 du code de la sécurité intérieure, le maire ou le président de l'établissement public de coopération intercommunale peut proposer un reclassement dans un autre cadre d'emplois dans les mêmes conditions que celles prévues à la section 3 du chapitre VI de la loi n° 84-53 du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale, à l'exception de celles mentionnées au second alinéa de l'article 81 " ; que ces dispositions accordent au maire la faculté de rechercher les possibilités de reclassement dans un autre cadre d'emplois de l'agent de police municipale dont l'agrément a été retiré ou suspendu et qui n'a fait l'objet ni d'une mesure disciplinaire d'éviction du service ni d'un licenciement pour insuffisance professionnelle ; qu'elles n'instituent pas au bénéfice des agents de police municipale un droit à être reclassés ;  que, par suite, en jugeant que le moyen tiré ce que M. A...n'avait pas bénéficié de la " garantie de reclassement instituée par les dispositions de l'article L. 412-49 du code des communes dans sa rédaction issue de l'ordonnance n° 2012-351 du 12 mars 2012 " était de nature à faire naître un doute sérieux quant à la légalité de la décision de radiation des cadres du 30 avril 2012, le juge des référés du tribunal administratif de Montpellier a commis une erreur de droit ; que, dès lors, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'ordonnance attaquée doit être annulée ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de statuer sur la demande de suspension en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              5. Considérant que pour demander la suspension de l'exécution de la décision du maire le radiant des cadres, M. A...soutient que la décision du 14 septembre 2011 du préfet de l'Hérault portant retrait de l'agrément dont il bénéficiait en qualité d'agent de police municipale est illégale, dès lors qu'il n'a pu présenter d'observations préalablement à son intervention, que le maire de la commune de Loupian n'a pas été consulté, que les faits qui lui ont été reprochés sont inexacts et que le préfet a commis une erreur manifeste d'appréciation, que la décision de radiation des cadres du 30 avril 2012 est insuffisamment motivée et que la commune de Loupian ne l'a pas fait bénéficier de la garantie de reclassement prévue à l'article L. 412-49 du code des communes ; qu'aucun de ces moyens n'est de nature à créer, en l'état de l'instruction, un doute sérieux sur la légalité de la décision du 30 avril 2012 du maire de la commune de Loupian portant radiation des cadres de M.A... ; que, par suite, et sans qu'il soit besoin de statuer sur la condition d'urgence, il y a lieu de rejeter la demande présentée par M.  A...ainsi que, par voie de conséquence, ses conclusions à fin d'injonction et celles présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de M. A...la somme demandée par la commune de Loupian au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Montpellier du 22 juin 2012 est annulée.<br/>
<br/>
Article 2 : La demande présentée par M. A...devant le juge des référés du tribunal administratif de Montpellier est rejetée.<br/>
Article 3 : Les conclusions présentées par la commune de Loupian au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la commune de Loupian et à M. B... A....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">49-025 POLICE. PERSONNELS DE POLICE. - AGENTS DE POLICE MUNICIPALE DONT L'AGRÉMENT A ÉTÉ RETIRÉ OU SUSPENDU - RECLASSEMENT PAR LE MAIRE (ART. L. 412-49 DU CODE DES COMMUNES, DANS SA VERSION ISSUE DE L'ORDONNANCE DU 12 MARS 2012) - FACULTÉ, ET NON OBLIGATION.
</SCT>
<ANA ID="9A"> 49-025 Si les dispositions de l'article L. 412-49 du code des communes, dans sa version issue de l'ordonnance n° 2012-351 du 12 mars 2012 relative à la partie législative du code de la sécurité intérieure, accordent au maire la faculté de rechercher les possibilités de reclassement dans un autre cadre d'emplois de l'agent de police municipale dont l'agrément a été retiré ou suspendu et qui n'a fait l'objet ni d'une mesure disciplinaire d'éviction du service, ni d'un licenciement pour insuffisance professionnelle, elles n'instituent pas au bénéfice des agents de police municipale un droit à être reclassés.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
