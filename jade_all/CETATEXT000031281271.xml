<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031281271</ID>
<ANCIEN_ID>JG_L_2015_10_000000375906</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/28/12/CETATEXT000031281271.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  9ème sous-section jugeant seule, 05/10/2015, 375906, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-10-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>375906</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 9ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARTHELEMY, MATUCHANSKY, VEXLIARD, POUPOT</AVOCATS>
<RAPPORTEUR>M. Bastien Lignereux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Astrid Nicolazo de Barmon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:375906.20151005</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M.  A...B...a demandé au tribunal administratif de Rennes de prononcer la restitution des droits de taxe sur la valeur ajoutée qu'il a acquittés au titre de la période du 1er janvier 2007 au 31 décembre 2008. Par un jugement n° 1001112 du 22 novembre 2012, le tribunal administratif de Rennes a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 12NT03330 du 26 décembre 2013, la cour administrative d'appel de Nantes a rejeté l'appel formé par M. B... contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 28 février et 28 mai 2014 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la directive 2006/112/CE du Conseil du 28 novembre 2006 ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - la loi n° 2002-303 du 4 mars 2002 ;<br/>
              - le décret n° 2011-32 du 7 janvier 2011 ;<br/>
              - le décret n° 2011-1127 du 20 septembre 2011 ;<br/>
              - l'arrêté du 20 septembre 2011 relatif à la formation des chiropracteurs et à l'agrément des établissements de formation en chiropraxie ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bastien Lignereux, auditeur,  <br/>
<br/>
              - les conclusions de Mme Marie-Astrid Nicolazo de Barmon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Barthélemy, Matuchansky, Vexliard, Poupot, avocat de M. B...;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M.B..., qui exerce l'activité de chiropracteur, a demandé la restitution de droits de taxe sur la valeur ajoutée qu'il a spontanément acquittés au titre de la période du 1er janvier 2007 au 31 décembre 2008, en estimant pouvoir bénéficier des dispositions de l'article 261 du code général des impôts relatives à l'exonération de cette taxe. Il se pourvoit en cassation contre l'arrêt du 26 décembre 2013 par lequel la cour administrative d'appel de Nantes a rejeté sa requête tendant à l'annulation du jugement du 22 novembre 2012 du tribunal administratif de Rennes rejetant cette demande de restitution.<br/>
<br/>
              2. En premier lieu, aux termes de l'article R. 194-1 du livre des procédures fiscales : " Lorsque, ayant donné son accord à la rectification ou s'étant abstenu de répondre dans le délai légal à la proposition de rectification, le contribuable présente cependant une réclamation faisant suite à une procédure contradictoire de rectification, il peut obtenir la décharge ou la réduction de l'imposition, en démontrant son caractère exagéré. / Il en est de même lorsqu'une imposition a été établie d'après les bases indiquées dans la déclaration souscrite par un contribuable (...) ". Il résulte de ces dispositions qu'un contribuable ne peut obtenir la restitution de droits de taxe sur la valeur ajoutée qu'il a déclarés et spontanément acquittés conformément à ses déclarations qu'à la condition d'en établir le mal-fondé.<br/>
<br/>
              3. En deuxième lieu, aux termes de l'article 132, 1 de la directive du 28 novembre 2006 relative au système commun de taxe sur la valeur ajoutée, qui reprend les dispositions de l'article 13, A de la sixième directive du Conseil du 17 mai 1977 en matière d'harmonisation des législations des Etats membres relatives aux taxes sur le chiffre d'affaires : " Les Etats membres exonèrent les opérations suivantes : / (...) c) les prestations de soins à la personne effectuées dans le cadre de l'exercice des professions médicales et paramédicales telles qu'elles sont définies par l'État membre concerné ; / (...) ". En vertu du 1° du 4 de l'article 261 du code général des impôts, dans sa rédaction applicable à la période d'imposition en litige, sont exonérés de la taxe sur la valeur ajoutée : " Les soins dispensés aux personnes par les membres des professions médicales et paramédicales réglementées (...) ". En limitant l'exonération qu'elles prévoient aux soins dispensés par les membres des professions médicales et paramédicales soumises à réglementation, ces dispositions ne méconnaissent pas l'objectif poursuivi par l'article 13, A, paragraphe 1, sous c) de la sixième directive, précité, qui est de garantir que l'exonération s'applique uniquement aux prestations de soins à la personne fournies par des prestataires possédant les qualifications professionnelles requises. En effet, la directive renvoie à la réglementation interne des États membres la définition de la notion de professions paramédicales, des qualifications requises pour exercer ces professions et des activités spécifiques de soins à la personne qui relèvent de telles professions.<br/>
<br/>
              4. Toutefois, conformément à l'interprétation des dispositions de la sixième directive qui résulte de l'arrêt rendu le 27 avril 2006 par la Cour de justice des Communautés européennes dans les affaires C-443/04 et C-444/04, l'exclusion d'une profession ou d'une activité spécifique de soins à la personne de la définition des professions paramédicales retenue par la réglementation nationale aux fins de l'exonération de la taxe sur la valeur ajoutée prévue à l'article 13, A, paragraphe 1, sous c) de cette directive serait contraire au principe de neutralité fiscale inhérent au système commun de taxe sur la valeur ajoutée s'il pouvait être démontré que les personnes exerçant cette profession ou cette activité disposent, pour la fourniture de telles prestations de soins, de qualifications professionnelles propres à assurer à ces prestations un niveau de qualité équivalent à celles fournies par des personnes bénéficiant, en vertu de la réglementation nationale, de l'exonération.<br/>
<br/>
              5. En troisième lieu, aux termes de l'article 75 de la loi du 4 mars 2002 relative aux droits des malades et à la qualité du système de santé, dans sa version applicable au présent litige : " L'usage professionnel du titre d'ostéopathe ou de chiropracteur est réservé aux personnes titulaires d'un diplôme sanctionnant une formation spécifique à l'ostéopathie ou à la chiropraxie délivrée par un établissement de formation agréé par le ministre chargé de la santé dans des conditions fixées par décret. Le programme et la durée des études préparatoires et des épreuves après lesquelles peut être délivré ce diplôme sont fixés par voie réglementaire. /  (...) Les praticiens en exercice, à la date d'application de la présente loi, peuvent se voir reconnaître le titre d'ostéopathe ou de chiropracteur s'ils satisfont à des conditions de formation ou d'expérience professionnelle analogues à celles des titulaires du diplôme mentionné au premier alinéa. Ces conditions sont déterminées par décret. / (...) Un décret établit la liste des actes que les praticiens justifiant du titre d'ostéopathe ou de chiropracteur sont autorisés à effectuer, ainsi que les conditions dans lesquelles ils sont appelés à les accomplir. / Ces praticiens ne peuvent exercer leur profession que s'ils sont inscrits sur une liste dressée par le représentant de l'Etat dans le département de leur résidence professionnelle, qui enregistre leurs diplômes, certificats, titres ou autorisations. ".<br/>
<br/>
              6. Le décret du 7 janvier 2011 relatif aux actes et aux conditions d'exercice de la chiropraxie n'a été publié que le 9 janvier 2011 et le décret du 20 septembre 2011 relatif à la formation des chiropracteurs et à l'agrément des établissements de formation en chiropraxie, ainsi que l'arrêté du même jour pris en application de ces deux décrets, n'ont été publiés que le 21 septembre 2011. Durant la période litigieuse, les actes dits de chiropraxie ne pouvaient être pratiqués que par les docteurs en médecine, et le cas échéant, pour certains actes seulement et sur prescription médicale, par les autres professionnels de santé habilités à les réaliser.<br/>
<br/>
              7. Il résulte de ce qui précède que, pour statuer sur la restitution des droits de taxe sur la valeur ajoutée acquittés par M. B...sur ses prestations de chiropraxie, la cour devait vérifier que celui-ci démontrait disposer, pour la fourniture de ces prestations, de qualifications professionnelles propres à leur assurer un niveau de qualité équivalent à celles fournies, selon le cas, par un médecin ou par un membre d'une profession de santé réglementée habilité à les réaliser. Une telle appréciation ne peut être portée qu'au vu de la nature des actes accomplis sous la dénomination d'actes de chiropraxie et, s'agissant des actes susceptibles de comporter des risques en cas de contre-indication médicale, en considération des conditions dans lesquelles ils ont été effectués.<br/>
<br/>
              8. Il appartenait, dès lors, à M.B..., pour mettre le juge à même de s'assurer que la condition tenant à la qualité des actes était remplie, de produire, d'une part, et sous réserve de l'occultation des noms des patients, des éléments relatifs à sa pratique permettant d'appréhender, sur une période significative, la nature des actes accomplis et les conditions dans lesquelles ils l'ont été et, d'autre part, tous éléments utiles relatifs à ses qualifications professionnelles.<br/>
<br/>
              9. Il ressort des pièces du dossier soumis aux juges du fond que M. B...a produit, notamment pour la période litigieuse, 750 " fiches patients " par an, correspondant à deux mois de consultations, chaque fiche comptant en moyenne quatre consultations, soit un total de 3 000 consultations par an. Il a également versé au dossier une notice explicative des " fiches patients " et un lexique des abréviations qu'elles comportaient. En se bornant, pour juger que ces documents n'étaient pas de nature à établir que les actes de chiropraxie que M. B... a accomplis au cours de la période en cause pourraient être regardés comme d'une qualité équivalente à ceux qui, s'ils avaient été effectués par un médecin pratiquant la chiropraxie, auraient ouvert droit au bénéfice de l'exonération en cause, à relever, d'une manière générale, le caractère difficilement lisible de ces documents, sans expliquer en quoi les mentions que les " fiches patients " comportaient ne suffisaient pas à apporter la preuve incombant au contribuable, la cour a insuffisamment motivé son arrêt. Pour ce motif, M. B...est fondé à en demander l'annulation. Le moyen retenu suffisant à entraîner cette annulation, il n'est pas nécessaire d'examiner les autres moyens du pourvoi.<br/>
<br/>
              10. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 2 000 euros à verser à M. B...au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nantes du 26 décembre 2013 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nantes.<br/>
Article 3 : L'Etat versera la somme de 2 000 euros à M. B...au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. A... B...et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
