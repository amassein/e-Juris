<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042386652</ID>
<ANCIEN_ID>JG_L_2020_09_000000439789</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/38/66/CETATEXT000042386652.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 30/09/2020, 439789, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-09-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>439789</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Alexis Goin</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:439789.20200930</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée au secrétariat du contentieux du Conseil d'Etat le 25 mars 2020, M. A... B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite par laquelle le Premier ministre a rejeté sa demande tendant à ce que soit édicté un décret d'application de l'article 432-14 du code pénal ;<br/>
<br/>
              2°) d'enjoindre au Premier ministre de prendre ce décret dans un délai de trente jours, sous astreinte de 500 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - la loi n° 95-127 du 8 février 1995 ;<br/>
              - le code pénal ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alexis Goin, auditeur,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteur public ;<br/>
<br/>
              Vu les deux notes en délibéré, enregistrées le 25 septembre 2020, présentées par M. B... ; <br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article 21 de la Constitution : " Le Premier ministre (...) assure l'exécution des lois. (...) il exerce le pouvoir règlementaire ". Selon son article 34 : " La loi fixe les règles concernant : / (...) la détermination des crimes et délits (...) ". Aux termes du premier alinéa de son article 37 : " Les matières autres que celles qui sont du domaine de la loi ont un caractère réglementaire ".<br/>
<br/>
              2. Aux termes de l'article 432-14 du code pénal : " Est puni de deux ans d'emprisonnement et d'une amende de 200 000 euros, dont le montant peut être porté au double du produit tiré de l'infraction, le fait par une personne dépositaire de l'autorité publique ou chargée d'une mission de service public ou investie d'un mandat électif public ou exerçant les fonctions de représentant, administrateur ou agent de l'Etat, des collectivités territoriales, des établissements publics, des sociétés d'économie mixte d'intérêt national chargées d'une mission de service public et des sociétés d'économie mixte locales ou par toute personne agissant pour le compte de l'une de celles susmentionnées de procurer ou de tenter de procurer à autrui un avantage injustifié par un acte contraire aux dispositions législatives ou réglementaires ayant pour objet de garantir la liberté d'accès et l'égalité des candidats dans les marchés publics et les contrats de concession ".<br/>
<br/>
              3. M. B... a demandé au Premier ministre de prendre un décret définissant la notion d'" avantage injustifié " pour l'application des dispositions de l'article 432-14 du code pénal et conteste le rejet implicite de sa demande.<br/>
<br/>
              Sur la question prioritaire de constitutionnalité :<br/>
<br/>
              4. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              5. La détermination des crimes et délits ressortissant au domaine réservé à la loi par l'article 34 de la Constitution, le Premier ministre était tenu de rejeter la demande de M. B.... Ainsi, la question de la conformité de l'article 432-14 du code pénal aux droits et libertés garantis par la Constitution est insusceptible d'avoir une incidence sur l'appréciation de la légalité de la décision attaquée. Par suite, les dispositions de l'article 432-14 du code pénal ne sont pas applicables au litige au sens de l'article 23-5 de l'ordonnance du 7 novembre 1958. Il suit de là qu'il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité.<br/>
<br/>
              Sur l'autre moyen de la requête :<br/>
<br/>
              6. Compte tenu de ce qui a été dit au point 5, le moyen tiré de ce que le Premier ministre était tenu de prendre un décret d'application de l'article 432-14 du code pénal ne peut être utilement invoqué à l'encontre du refus qui a été opposé au requérant.<br/>
<br/>
              7. Il résulte de ce qui précède que la requête de M. B... doit être rejetée, y compris ses conclusions tendant au prononcé d'une injonction et celles présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M. B....<br/>
Article 2 : La requête de M. B... est rejetée.<br/>
Article 3 : La présente décision sera notifiée à M. A... B... et au garde des sceaux, ministre de la justice.<br/>
Copie en sera adressée au Premier ministre, au ministre de l'économie, des finances et de la relance et au Conseil constitutionnel.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
