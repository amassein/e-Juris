<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037022286</ID>
<ANCIEN_ID>JG_L_2018_06_000000406849</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/02/22/CETATEXT000037022286.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème chambres réunies, 06/06/2018, 406849, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-06-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>406849</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>M. Vincent Uher</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:406849.20180606</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société civile immobilière Florella a demandé au tribunal administratif de Versailles de prononcer la décharge des cotisations de taxe locale d'équipement, de taxe départementale pour le financement des conseils d'architecture, de l'urbanisme et de l'environnement et de taxe départementale sur les espaces naturels et sensibles auxquelles elle a été assujettie à raison d'un permis de construire délivré le 13 février 2009. Par un jugement n° 1103417 du 4 décembre 2015, ce tribunal a rejeté cette demande.	<br/>
<br/>
              Par un arrêt n° 16VE00344 du 29 décembre 2016, enregistré le 16 janvier 2017 au secrétariat du contentieux du Conseil d'Etat, la cour administrative d'appel de Versailles a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi, enregistré le 3 février 2016 au greffe de cette cour, présenté par la société Florella. Par ce pourvoi et par un mémoire complémentaire et un nouveau mémoire enregistrés le 21 mars 2017 et le 14 mai 2018 au secrétariat du contentieux du Conseil d'Etat, la société Florella demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des collectivités territoriales ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de l'urbanisme ;<br/>
              - la loi n° 67-1253 du 30 décembre 1967 modifiée, notamment le IV de son article 64 ;<br/>
              - la loi n° 71-581 du 16 juillet 1971, notamment le III de son article 16 ;<br/>
              - la loi n° 82-213 du 2 mars 1982 modifiée, notamment son article 45 ;<br/>
              - la loi n° 85-729 du 18 juillet 1985, notamment le III de son article 22 et le VII de son article 23 ;<br/>
              - le décret n° 72-988 du 5 octobre 1972 ;<br/>
              - le décret n° 73-741 du 26 juillet 1973 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Uher, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de la société Florella.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Florella a été assujettie à la taxe locale d'équipement, à la taxe départementale pour le financement des conseils d'architecture, de l'urbanisme et de l'environnement et à la taxe départementale sur les espaces naturels et sensibles à raison d'un permis de construire délivré le 13 février 2009 pour l'extension d'une construction existante à usage de club sportif. Elle a contesté ces impositions, estimant devoir en être exonérée en vertu d'une convention conclue le 23 décembre 1975 entre la commune de Fontenay-le-Vicomte et M. A..., précédent propriétaire du terrain d'assise de la construction litigieuse. Par un jugement du 4 décembre 2015, le tribunal administratif de Versailles a rejeté sa demande de décharge de ces impositions. La société Florella se pourvoit en cassation contre ce jugement. <br/>
<br/>
              Sur la taxe locale d'équipement :<br/>
<br/>
              2. Aux termes de l'article 1585 A du code général des impôts, dans sa rédaction applicable au litige : " Une taxe locale d'équipement, établie sur la construction, la reconstruction et l'agrandissement des bâtiments de toute nature, est instituée : / 1° De plein droit : / a) Dans les communes de 10.000 habitants et au-dessus ; / b) Dans les communes de la région parisienne figurant sur une liste arrêtée par décret (...) ".<br/>
<br/>
              3. Aux termes du IV de l'article 64 de la loi d'orientation foncière du 30 décembre 1967, dans sa rédaction issue du III de l'article 16 de la loi du 16 juillet 1971 portant dispositions diverses en matière d'urbanisme et d'action foncière : " Le conseil municipal peut exempter de la taxe les bâtiments à usage agricole. Il peut en exempter également toute construction à usage industriel ou commercial qui, par sa situation ou son importance, nécessite la réalisation d'équipements publics exceptionnels. / Dans ces cas, les dispositions de l'article 72 ci-dessous ne sont pas applicables. Un arrêté du préfet précise les conditions dans lesquelles le constructeur est appelé à participer aux dépenses impliquées par la réalisation de ces équipements ". Il résulte de ces dispositions que le conseil municipal peut exempter de taxe locale d'équipement, outre les bâtiments à usage agricole, toute construction à usage industriel ou commercial, en contrepartie de la participation du constructeur aux dépenses afférentes à la réalisation des équipements publics exceptionnels rendus nécessaires par cette construction. La circonstance que la seconde phrase du premier alinéa du IV de l'article 64, qui avait été codifiée au IV de l'article 1585 C du code général des impôts par le décret du 26 juillet 1973, a été abrogée par l'effet du III de l'article 22 de la loi du 18 juillet 1985, n'est pas de nature, par elle-même, à rendre caduques les exemptions qu'une commune a accordées sur le fondement de ce IV antérieurement à l'entrée en vigueur de cette loi.<br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond que, en application des dispositions du IV de l'article 64 précité, M. A...et la commune de Fontenay-le-Vicomte ont conclu le 23 décembre 1975 une " convention " relative à un projet de construction sur un terrain cadastré ZA n° 33 d'une superficie de 22 417 m², dont l'article 2 stipule que la commune exempte le constructeur de la taxe locale d'équipement à raison d'un bâtiment à usage de commerce de bois de 880 m² à construire sur la parcelle et dont l'article 9 précise que, dans la limite d'une surface hors oeuvre de 8 966 m², les " tranches futures " seront également exemptées du paiement de cette taxe. Pour déterminer si la société Florella pouvait bénéficier de cette exemption, le tribunal administratif de Versailles a relevé qu'il résultait des stipulations des articles 2 et 7 de cette convention que l'exemption litigieuse concernait le projet de construire un bâtiment à usage de commerce de bois et restait soumise à la condition que le pétitionnaire s'engage à participer aux dépenses impliquées par la réalisation de divers équipements. Il en a déduit que la société Florella ne pouvait prétendre au bénéfice de la convention, aux motifs que son projet portait sur l'extension d'une construction à usage de club sportif et qu'elle ne justifiait ni avoir contribué aux dépenses d'équipement ni avoir été subrogée dans les droits de M.A.... <br/>
<br/>
              5. En premier lieu, en statuant ainsi, sans répondre à l'argumentation de la société Florella qui se prévalait des stipulations de l'article 9 de la convention selon lesquelles les tranches futures de construction seraient exemptées, sans que cette exemption ne se limitât aux constructions destinées à un usage de commerce de bois, le tribunal administratif a insuffisamment motivé son jugement. <br/>
<br/>
              6. En second lieu, en se fondant également sur la circonstance que la société Florella ne démontrait pas avoir été subrogée dans les droits de M.A..., alors que l'exemption prévue par le IV de l'article 64 précité s'applique à raison des constructions visées dans la délibération du conseil municipal que ces dispositions prévoient, quel que soit le titulaire des autorisations de construire, le tribunal administratif a méconnu cet article.<br/>
<br/>
              7. Par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la société Florella est fondée à demander l'annulation du jugement attaqué en tant qu'il statue sur ses conclusions relatives à la taxe locale d'équipement.<br/>
<br/>
              Sur la taxe départementale pour le financement des conseils d'architecture, de l'urbanisme et de l'environnement et la taxe départementale sur les espaces naturels et sensibles :<br/>
<br/>
              8. Aux termes de l'article 1599 B du code général des impôts, dans sa rédaction applicable au litige : " Pour le financement des dépenses des conseils d'architecture, d'urbanisme et de l'environnement prévus à l'article 8 de la loi n° 77-2 du 3 janvier 1977, les départements peuvent établir, par délibération du conseil général, une taxe qui s'applique dans toutes les communes du département (...) ".<br/>
<br/>
              9. Aux termes de l'article L. 142-2 du code de l'urbanisme : " Pour mettre en oeuvre la politique prévue à l'article L. 142-1, le département peut instituer, par délibération du conseil général, une taxe départementale des espaces naturels sensibles (...) ".<br/>
<br/>
              10. Aux termes de l'article 45 de la loi du 2 mars 1982, codifié à l'article L. 3131-1 du code général des collectivités territoriales : " Les actes pris par les autorités départementales sont exécutoires de plein droit dès qu'il a été procédé à leur publication ou à leur notification aux intéressés ainsi qu'à leur transmission au représentant de l'Etat dans le département. (...) "<br/>
<br/>
               11. En premier lieu, en jugeant que la société Florella ne contestait pas la portée et le caractère exécutoire des délibérations par lesquelles le conseil général de l'Essonne a institué la taxe départementale pour le financement des conseils d'architecture, de l'urbanisme et de l'environnement et la taxe départementale sur les espaces naturels et sensibles, alors qu'il ressort des visas de son jugement eux-mêmes que la société requérante soutenait qu'il n'était pas établi que ces deux taxes avaient été instituées par des délibérations exécutoires du conseil général, le tribunal administratif a méconnu la portée des écritures de la requérante. <br/>
<br/>
               12. En second lieu, en estimant que le conseil général de l'Essonne avait fixé le taux de la taxe départementale pour le financement des conseils d'architecture, d'urbanisme et d'environnement par une délibération du 23 avril 1992, alors qu'il ressort des pièces du dossier qui lui était soumis que la copie de la délibération produite par l'administration était incomplète et que la délibération ne mentionnait au demeurant pas, dans ses visas, l'article 1599 B du code général des impôts relatif à cette taxe, le tribunal administratif a dénaturé les pièces du dossier qui lui était soumis.<br/>
<br/>
              13. Par suite et sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, la société Florella est fondée à demander l'annulation du jugement attaqué en tant qu'il statue sur ses conclusions relatives à la taxe départementale pour le financement des conseils d'architecture, de l'urbanisme et de l'environnement et à la taxe départementale sur les espaces naturels et sensibles. <br/>
<br/>
              Sur les conclusions de la société Florella présentées au titre de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
               14. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la société Florella au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>                        D E C I D E :<br/>
                                     --------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Versailles du 4 décembre 2015 est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Versailles.<br/>
Article 3 : L'Etat versera une somme de 3 000 euros à la société Florella au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la société civile immobilière Florella et au ministre de la cohésion des territoires.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
