<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029724755</ID>
<ANCIEN_ID>JG_L_2014_11_000000373065</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/72/47/CETATEXT000029724755.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème / 8ème SSR, 05/11/2014, 373065, Publié au recueil Lebon</TITRE>
<DATE_DEC>2014-11-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>373065</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème / 8ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ODENT, POULET ; SCP CELICE, BLANCPAIN, SOLTNER ; SCP BARADUC, DUHAMEL, RAMEIX ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Angélique Delorme</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:373065.20141105</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 30 octobre 2013 au secrétariat du contentieux du Conseil d'Etat, présentée pour la société Wienerberger, dont le siège est 8, rue du Canal à Achenheim (67204) ; la société Wienerberger demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision n° 13-DCC-101 du 26 juillet 2013 de l'Autorité de la concurrence relative à la prise de contrôle exclusif des actifs " matériaux de structure " de la société Imerys TC par la société Bouyer-Leroux ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 10 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de commerce ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Angélique Delorme, auditeur, <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Blancpain, Soltner, avocat de la Société Wienerberger, à la SCP Baraduc, Duhamel, Rameix, avocat de l'Autorité de la concurrence, à la SCP Piwnica, Molinié, avocat de la société Bouyer Leroux et à la SCP Odent, Poulet, avocat de la société Imerys TC ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier que la société <br/>
Bouyer-Leroux, qui produit notamment des briques de structure et de cloison, s'est engagée le <br/>
12 décembre 2012 à acquérir la totalité des titres et des droits de vote d'une société à laquelle seraient apportés les actifs " matériaux de structure " de la société Imerys TC ; que la société Wienerberger, qui fabrique elle-même des produits en terre cuite, en particulier des briques de structure, demande l'annulation pour excès de pouvoir de la décision n° 13-DCC-101 du <br/>
26 juillet 2013 par laquelle l'Autorité de la concurrence, après un examen approfondi de l'opération, a autorisé la prise de contrôle exclusif des actifs " matériaux de structure " de la société Imerys TC par la société Bouyer-Leroux, sous réserve de la réalisation effective des engagements pris par cette dernière en vue de remédier aux atteintes à la concurrence sur le marché des briques de mur en Aquitaine ; <br/>
<br/>
              Sur la légalité externe de la décision attaquée :<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier que la société <br/>
Bouyer-Leroux a proposé le 29 mars 2013 une première série d'engagements de caractère structurel, consistant principalement en la cession d'un site de production de briques de mur et de briques plâtrières situé dans la région Pays de la Loire ; que le collège de l'Autorité de la concurrence, qui a examiné ces engagements lors de sa réunion du 4 juillet 2013, a estimé qu'ils n'étaient pas susceptibles de remédier aux effets anticoncurrentiels de l'opération de concentration projetée ; que, entre le 18 et le 25 juillet 2013, la société Bouyer-Leroux a transmis à l'Autorité de la concurrence de nouveaux engagements, de caractère comportemental, consistant, pour l'entité issue de l'opération de concentration, à proposer à ses concurrents qui le souhaiteraient de leur vendre, à prix coûtant et sans marque commerciale apposée, un volume de 25 000 tonnes par an de briques de mur produites dans son usine de Gironde-sur-Dropt, pendant une période de 5 ans renouvelable, afin de leur permettre de maintenir et de développer leur activité commerciale ; que ces nouveaux engagements ont été examinés le 25 juillet 2013 par le collège de l'Autorité de la concurrence, qui les a acceptés et les a repris dans le dispositif de la décision attaquée ; <br/>
<br/>
              3. Considérant, en premier lieu, qu'aux termes du deuxième alinéa de l'article L. 430-6 du code de commerce : " La procédure applicable à cet examen approfondi de l'opération par l'Autorité de la concurrence est celle prévue au deuxième alinéa de l'article <br/>
L. 463-2 et aux articles L. 463-4, L. 463-6 et L. 463-7. Toutefois, les parties qui ont procédé à la notification et le commissaire du Gouvernement doivent produire leurs observations en réponse à la communication du rapport dans un délai de quinze jours ouvrés " ; que, contrairement à ce que soutient la société Wienerberger, il ne résulte pas de ces dispositions que, lorsque les parties à l'opération de concentration proposent de nouveaux engagements à l'Autorité de la concurrence au cours de la procédure d'examen approfondi, le rapporteur général ou le rapporteur général-adjoint soit tenu de produire un rapport complémentaire avant que le collège ne se prononce sur ces engagements ; <br/>
<br/>
              4. Considérant, en deuxième lieu, qu'il ressort des pièces du dossier, en particulier du procès-verbal de séance et des copies des courriels convoquant ses membres, que le collège de l'Autorité de la concurrence s'est réuni le 25 juillet 2013 pour délibérer sur les derniers engagements proposés par la société Bouyer-Leroux ; que si l'Autorité de la concurrence n'a pas produit les copies des courriels de convocation de deux membres permanents du collège, il ressort des mentions du procès-verbal de séance et des attestations fournies par ces deux membres qu'ils ont participé à la séance du 25 juillet 2013 ; qu'ainsi, le moyen tiré de ce que l'Autorité de la concurrence n'aurait pas délibéré collégialement des nouveaux engagements ne peut qu'être écarté ;<br/>
<br/>
              5. Considérant, en troisième lieu, qu'aux termes du dernier alinéa du III de l'article 430-7 du code de commerce : " Le projet de décision est transmis aux parties intéressées, auxquelles un délai raisonnable est imparti pour présenter leurs observations " ; que l'Autorité de la concurrence n'était pas tenue de transmettre le projet de décision à la société Wienerberger, qui n'est pas, en sa seule qualité de concurrente de la société Bouyer-Leroux, une partie intéressée au sens de ces dispositions ; que, dès lors que les engagements pris par la société Bouyer-Leroux n'étaient pas de nature à affecter ses droits, mais lui offraient au contraire l'opportunité de commercialiser un volume supplémentaire de briques de mur sur le marché pertinent, la société Wienerberger n'est pas fondée à soutenir qu'elle aurait dû être mise en mesure, en application du principe général des droits de la défense, de présenter ses observations avant que l'Autorité de la concurrence ne prenne la décision attaquée ;<br/>
<br/>
              Sur la légalité interne de la décision attaquée :<br/>
<br/>
              6. Considérant, en premier lieu, qu'il ressort des pièces du dossier que si elle a estimé que l'opération de concentration en cause avait des effets anticoncurrentiels horizontaux sur le marché aquitain des briques de mur, l'Autorité de la concurrence n'a pas retenu, à... ; <br/>
<br/>
              7. Considérant que les comportements d'opérateurs en situation oligopolistique sur un marché pertinent peuvent, en l'absence même de toute entente formelle, être implicitement coordonnés, lorsque ces opérateurs, notamment en raison de l'existence de facteurs de corrélation entre eux, ont le pouvoir d'adopter durablement une même ligne d'action sur le marché en vue de profiter d'une situation de puissance économique collective, en particulier pour vendre au-dessus des prix concurrentiels, sans que les concurrents actuels ou potentiels ou encore les clients et les consommateurs ne puissent réagir de manière effective ; qu'une telle position dominante collective peut être identifiée lorsque chacun des membres de l'oligopole est en mesure de connaître de manière suffisamment précise et immédiate l'évolution du comportement des autres, qu'il existe des menaces de représailles crédibles en cas de déviation de la ligne d'action implicitement approuvée par tous et que les réactions prévisibles des consommateurs et des concurrents actuels ou potentiels de l'oligopole ne peuvent suffire à remettre en cause les résultats attendus de la collusion tacite ; que, pour apprécier si une opération de concentration présente le risque de tels effets coordonnés, il incombe à l'autorité régulatrice de s'interroger, dans le cadre d'une analyse prospective du marché pertinent, sur sa probabilité, en appréhendant, sans s'en tenir à l'application de chacun des trois critères énumérés ci-dessus pris isolément, le mécanisme économique global d'une éventuelle coordination ;<br/>
<br/>
              8. Considérant que l'Autorité de la concurrence a relevé que les deux opérateurs majeurs sur le marché aquitain des briques de mur, l'entité issue de la concentration et la société Terreal, y détiendraient une part de marché globale d'au moins 90 % ; qu'elle a par ailleurs estimé que les entreprises qui resteraient présentes sur ce marché avaient des structures de coût différentes, résultant notamment de ce qu'une seule possède un site de production dans la région alors que les coûts de transport des briques sont très élevés et de ce que l'entité issue de l'opération de concentration sera une société coopérative et participative de taille moyenne active uniquement en France, alors que la société Terreal et la société requérante sont des groupes puissants au niveau national et au niveau international ; qu'elle a enfin insisté sur le fait que le marché pertinent se caractérise notamment par l'importance de l'innovation ; qu'elle n'a commis, sur ces différents points, aucune erreur d'appréciation ; qu'en en déduisant qu'une coordination des comportements de l'entité issue de l'opération de concentration litigieuse et de ses principaux concurrents sur le marché aquitain des briques de mur était peu probable, sans procéder à une analyse au regard de chacun des trois critères mentionnés au point 7 pris isolément, elle n'a, compte tenu de ce qui a été dit au même point, commis aucune erreur de droit ; <br/>
<br/>
              9. Considérant, en second lieu, que, lorsque lui est notifiée une opération de concentration dont la réalisation est soumise à son autorisation, il incombe à l'Autorité de la concurrence d'user des pouvoirs d'interdiction, d'injonction, de prescription ou de subordination de son autorisation à la réalisation effective d'engagements pris devant elle par les parties, qui lui sont conférés par les dispositions des articles L. 430-6 et suivants du code de commerce, à proportion de ce qu'exige le maintien d'une concurrence suffisante sur les marchés affectés par l'opération ;<br/>
<br/>
              10. Considérant que, pour prévenir les effets anticoncurrentiels horizontaux unilatéraux de l'opération projetée, la société Bouyer-Leroux s'est engagée, ainsi qu'il a été dit au point 2, à proposer, pour une durée de cinq ans, à ses concurrents, ou à défaut à un grossiste, de leur vendre un volume de 25 000 tonnes par an de briques de mur, incluant les accessoires qui leur sont liés, sans marque commerciale apposée, sur la base du coût de revient départ usine du site de production de Gironde-sur-Dropt ;<br/>
<br/>
              11. Considérant que la société Wienerberger soutient, en premier lieu, que cet engagement met en place un mécanisme d'échange d'informations sensibles entre concurrents susceptible, sur un marché oligopolistique, de produire des effets anticoncurrentiels, dès lors qu'il implique la communication du coût de revient départ usine du site de production de Gironde-sur-Dropt ;<br/>
<br/>
              12. Considérant, toutefois, qu'il ressort des pièces du dossier que, contrairement à ce que soutient la société requérante, l'engagement critiqué ne peut être regardé comme conduisant à la mise en place d'un circuit d'échanges d'informations qui serait de nature à permettre aux entreprises, sur le marché oligopolistique en cause, de connaître les positions ainsi que la stratégie commerciale de leurs concurrents et à altérer ainsi sensiblement la concurrence ; qu'en outre, eu égard notamment à l'importance des coûts de transport, le coût de revient départ usine d'une brique ne constitue pas un élément déterminant susceptible de renseigner les concurrents de la nouvelle entité sur sa politique commerciale ; <br/>
<br/>
              13. Considérant que la société Wienerberger soutient, en second lieu, que l'engagement retenu par l'Autorité de la concurrence est insuffisant pour prévenir les effets anticoncurrentiels horizontaux de l'opération et empêcher la nouvelle entité de profiter de sa position dominante sur le marché aquitain au détriment des consommateurs et qu'il est en réalité de nature à renforcer cette position dominante ;<br/>
<br/>
              14. Considérant que s'il est soutenu que l'engagement litigieux serait insuffisant pour permettre à des concurrents de la nouvelle entité de réaliser un site de production en Aquitaine afin de réduire le poids des coûts de transport et d'accroître ainsi la concurrence sur le marché aquitain, un tel moyen ne peut qu'être écarté dès lors qu'il appartient seulement à l'Autorité de la concurrence, pour apprécier si un engagement est pertinent et suffisant, de rechercher s'il est de nature à pallier les effets anticoncurrentiels de l'opération projetée et à maintenir ainsi une concurrence suffisante ; qu'à cet égard, il ressort des pièces du dossier que l'engagement pris, qui permet aux concurrents de la nouvelle entité qui le souhaitent de commercialiser des briques à des prix aussi compétitifs que ceux de la nouvelle entité, pour un volume de 25 000 tonnes qui correspond à ce qu'était la part de marché de la société <br/>
Bouyer-Leroux avant l'opération, est susceptible de pallier la disparition de la capacité concurrentielle qu'implique l'opération litigieuse et, en maintenant une pression concurrentielle sur les prix, d'éviter que, dans un avenir relativement proche, ne se produisent les effets anti-concurrentiels identifiés ; que, par ailleurs, si, ainsi que le soutient la société requérante, les caractéristiques des briques que la société Bouyer-Leroux s'est engagée à céder à ses concurrents permettent aux professionnels d'identifier, même en l'absence de marque commerciale apposée, leur origine, il ne ressort pas des pièces du dossier que cette seule circonstance serait de nature à renforcer la position dominante de la nouvelle entité ; que ni la circonstance que les briques puissent être cédées par la nouvelle entité à un grossiste plutôt qu'à ses concurrents eux-mêmes ni le fait que ceux-ci pourraient, après avoir signé des contrats de fournitures avec la nouvelle entité, ne procéder que partiellement à des achats effectifs ne sont de nature à priver l'engagement souscrit de sa portée ; <br/>
<br/>
              15. Considérant qu'il résulte de tout ce qui précède que la société Wienerberger n'est pas fondée à demander l'annulation pour excès de pouvoir de la décision qu'elle attaque ;<br/>
<br/>
              Sur les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              16. Considérant que ces dispositions font obstacle à ce qu'une somme soit mise à la charge de l'Etat et de la société Bouyer-Leroux qui ne sont pas, dans la présente instance, les parties perdantes ; qu'en revanche il y lieu de mettre à la charge de la société Wienerberger une somme de 3000 euros à verser à l'Etat, à la société Bouyer-Leroux et à la société Imerys TC ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la société Wienerberger est rejetée. <br/>
Article 2 : La société Wienerberger versera une somme de 3000 euros à l'Etat (Autorité de la concurrence), une somme de 3 000 euros à la société Bouyer-Leroux et une somme de 3 000 euros à la société Imerys TC. <br/>
Article 3 : La présente décision sera notifiée à la société Wienerberger, à l'Autorité de la concurrence, à la société Bouyer-Leroux et à la société Imerys TC.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">14-05-01-03 COMMERCE, INDUSTRIE, INTERVENTION ÉCONOMIQUE DE LA PUISSANCE PUBLIQUE. DÉFENSE DE LA CONCURRENCE. CONTRÔLE DE LA CONCENTRATION ÉCONOMIQUE. RÈGLES DE FOND. - 1) ANALYSE DES EFFETS CONCURRENTIELS - A) APPRÉCIATION DES RISQUES DE CRÉATION OU D'AGGRAVATION D'UNE POSITION DOMINANTE COLLECTIVE ENTRE LES PRINCIPAUX OPÉRATEURS - CRITÈRES D'IDENTIFICATION D'UNE TELLE POSITION [RJ1] - DEVOIR DE L'AUTORITÉ RÉGULATRICE DE S'INTERROGER SUR LA PROBABILITÉ DE LA CRÉATION D'UNE POSITION DOMINANTE COLLECTIVE - APPRÉHENSION DU MÉCANISME ÉCONOMIQUE GLOBAL D'UNE ÉVENTUELLE COORDINATION - B) APPLICATION À L'ESPÈCE - COORDINATION DES COMPORTEMENTS DE L'ENTITÉ ISSUE DE L'OPÉRATION DE CONCENTRATION ET DE SES PRINCIPAUX CONCURRENTS PEU PROBABLE - 2) ENGAGEMENT VISANT À REMÉDIER AUX EFFETS ANTICONCURRENTIELS DE L'OPÉRATION - DEVOIR DE L'AUTORITÉ RÉGULATRICE DE RECHERCHER SEULEMENT S'IL EST DE NATURE À PALLIER LES EFFETS ANTICONCURRENTIELS DE L'OPÉRATION ET À MAINTENIR AINSI UNE CONCURRENCE SUFFISANTE - CONSÉQUENCE - MOYEN TIRÉ DE CE QU'UN ENGAGEMENT SERAIT INSUFFISANT POUR PERMETTRE À DES CONCURRENTS DE LA NOUVELLE ENTITÉ D'ACCROÎTRE LA CONCURRENCE - OPÉRANCE - ABSENCE.
</SCT>
<ANA ID="9A"> 14-05-01-03 1) a) Les comportements d'opérateurs en situation oligopolistique sur un marché pertinent peuvent, en l'absence même de toute entente formelle, être implicitement coordonnés, lorsque ces opérateurs, notamment en raison de l'existence de facteurs de corrélation entre eux, ont le pouvoir d'adopter durablement une même ligne d'action sur le marché en vue de profiter d'une situation de puissance économique collective, en particulier pour vendre au-dessus des prix concurrentiels, sans que les concurrents actuels ou potentiels ou encore les clients et les consommateurs ne puissent réagir de manière effective. Une telle position dominante collective peut être identifiée lorsque chacun des membres de l'oligopole est en mesure de connaître de manière suffisamment précise et immédiate l'évolution du comportement des autres, qu'il existe des menaces de représailles crédibles en cas de déviation de la ligne d'action implicitement approuvée par tous et que les réactions prévisibles des consommateurs et des concurrents actuels ou potentiels de l'oligopole ne peuvent suffire à remettre en cause les résultats attendus de la collusion tacite. Pour apprécier si une opération de concentration présente le risque de tels effets coordonnés, il incombe à l'autorité régulatrice de s'interroger, dans le cadre d'une analyse prospective du marché pertinent, sur sa probabilité, en appréhendant, sans s'en tenir à l'application de chacun des trois critères énumérés ci-dessus pris isolément, le mécanisme économique global d'une éventuelle coordination.,,,b) En l'espèce, l'Autorité de la concurrence a relevé que les deux opérateurs majeurs sur le marché pertinent y détiendraient une part de marché globale d'au moins 90 %. Elle a par ailleurs estimé que les entreprises qui resteraient présentes sur ce marché avaient des structures de coût différentes, résultant notamment de ce qu'une seule possède un site de production dans la région alors que les coûts de transport des produits sont très élevés, et de ce que l'entité issue de l'opération de concentration sera une société coopérative et participative de taille moyenne active uniquement en France, alors que l'autre opérateur majeur et la société requérante sont des groupes puissants au niveau national et au niveau international. Elle a enfin insisté sur le fait que le marché pertinent se caractérise notamment par l'importance de l'innovation. Elle n'a commis, sur ces différents points, aucune erreur d'appréciation. En en déduisant qu'une coordination des comportements de l'entité issue de l'opération de concentration litigieuse et de ses principaux concurrents sur le marché pertinent était peu probable, sans procéder à une analyse au regard de chacun des trois critères mentionnés ci-dessus pris isolément, elle n'a, compte tenu de ce qui a été dit ci-dessus, commis aucune erreur de droit.,,,2) Un moyen tiré de ce qu'un engagement visant à remédier aux effets anticoncurrentiels de l'opération de concentration serait insuffisant pour permettre à des concurrents de la nouvelle entité d'accroître la concurrence sur le marché pertinent ne peut qu'être écarté dès lors qu'il appartient seulement à l'Autorité de la concurrence, pour apprécier si un engagement est pertinent et suffisant, de rechercher s'il est de nature à pallier les effets anticoncurrentiels de l'opération projetée et à maintenir ainsi une concurrence suffisante.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 31 juillet 2009, Société fiducial audit et Société fiducial expertise, n° 305903, p. 313.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
