<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032289660</ID>
<ANCIEN_ID>JG_L_2016_03_000000393574</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/28/96/CETATEXT000032289660.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 23/03/2016, 393574, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-03-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>393574</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR>M. Bruno Bachini</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:393574.20160323</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<p>Vu la procédure suivante :<br clear="none"/>
<br clear="none"/>
Mme B...a demandé au juge des référés du tribunal administratif de Montpellier d'ordonner, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de la décision du 25 juin 2015 par laquelle la présidente de l'université Paul Valéry Montpellier III lui a refusé l'inscription en master 2 ainsi que de la décision du 21 juillet 2015 rejetant son recours gracieux. Par une ordonnance n° 1504230 du 2 septembre 2015, le juge des référés a fait droit à cette demande.<br clear="none"/>
<br clear="none"/>
Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 17 et 29 septembre 2015 au secrétariat du contentieux du Conseil d'Etat, l'université Paul Valéry Montpellier III demande au Conseil d'Etat :<br clear="none"/>
<br clear="none"/>
1°) d'annuler cette ordonnance ;<br clear="none"/>
<br clear="none"/>
2°) statuant en référé, de rejeter la demande de Mme A...;<br clear="none"/>
<br clear="none"/>
3°) de mettre à la charge de Mme A...la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
Vu les autres pièces du dossier ;<br clear="none"/>
<br clear="none"/>
Vu :<br clear="none"/>
- le code de l'éducation ;<br clear="none"/>
- la loi n° 91-647 du 10 juillet 1991 ;<br clear="none"/>
- l'arrêté du 25 avril 2002 relatif au diplôme national de master ;<br clear="none"/>
- l'arrêté du 22 janvier 2014 fixant le cadre national des formations conduisant à la délivrance des diplômes nationaux de licence, de licence professionnelle et de master ;<br clear="none"/>
- le code de justice administrative ;<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
Après avoir entendu en séance publique :<br clear="none"/>
<br clear="none"/>
- le rapport de M. Bruno Bachini, maître des requêtes,<br clear="none"/>
<br clear="none"/>
- les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br clear="none"/>
<br clear="none"/>
La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de l'université Paul Valery Montpellier III et à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de Mme A...;<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
1. Considérant, d'une part, qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ;<br clear="none"/>
<br clear="none"/>
2. Considérant, d'autre part, qu'aux termes de l'article L. 612-1 du code de l'éducation : " Le déroulement des études supérieures est organisé en cycles (...) / Les grades de licence, de master et de doctorat sont conférés respectivement dans le cadre du premier, du deuxième et du troisième cycle (...) " ; qu'aux termes de l'article L. 612-6 du même code : " L'admission dans les formations du deuxième cycle est ouverte à tous les titulaires des diplômes sanctionnant les études de premier cycle (...) / La liste limitative des formations dans lesquelles cette admission peut dépendre des capacités d'accueil des établissements et, éventuellement, être subordonnée au succès à un concours ou à l'examen du dossier du candidat, est établie par décret après avis du Conseil national de l'enseignement supérieur et de la recherche (...) " ; qu'aux termes de l'article 3 de l'arrêté du 25 avril 2002 relatif au diplôme national de master : " Le diplôme de master sanctionne un niveau correspondant à l'obtention de 120 crédits européens au-delà du grade de licence " ; qu'aux termes de l'article 11 du même arrêté : " Lorsqu'une université est habilitée à délivrer le diplôme de master, l'accès de l'étudiant titulaire de la licence, dans le même domaine, est de droit pour les 60 premiers crédits européens. / L'admission ultérieure dans un parcours type de formation débouchant sur le master recherche s'effectue dans les conditions prévues à l'article 16 de l'arrêté du 25 avril 2002 susvisé. L'admission ultérieure dans un parcours type de formation débouchant sur le master professionnel est prononcée par le chef d'établissement sur proposition du responsable de la formation " ;<br clear="none"/>
<br clear="none"/>
3. Considérant qu'il résulte des dispositions des articles L. 612-1 et L. 612-6 du code de l'éducation citées ci-dessus que l'admission à une formation de deuxième cycle au terme de laquelle est délivré le grade de master, en première comme en deuxième année, ne peut dépendre des capacités d'accueil d'un établissement ou être subordonnée au succès à un concours ou à l'examen du dossier des candidats que si cette formation figure sur la liste qu'elles mentionnent ; qu'il s'ensuit que, pour une formation de deuxième cycle qui n'a pas été inscrite à cette fin sur cette liste, aucune limitation à l'admission des candidats du fait des capacités d'accueil d'un établissement ou par une condition de réussite à un concours ou d'examen du dossier ne peut être introduite après l'obtention des 60 premiers crédits européens, c'est-à-dire après la première année du deuxième cycle ; que les dispositions de l'article 11 de l'arrêté du 25 avril 2002 citées ci-dessus ne sauraient, en tout état de cause, avoir eu légalement pour objet ou pour effet de permettre de limiter l'admission des candidats, sauf à ce que la formation en cause ait été inscrite à ce titre sur une liste établie par décret pris après avis du Conseil national de l'enseignement supérieur et de la recherche ;<br clear="none"/>
<br clear="none"/>
4. Considérant qu'il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Montpellier que, par une décision du 25 juin 2015, la présidente de l'université Paul Valéry Montpellier III a refusé d'autoriser MmeA..., ayant validé son année de master 1 " informatique et communication ", à s'inscrire en année de master 2 dans la spécialité " information, communication parcours communication publique associative et culturelle " pour l'année 2015-2016, au motif que le niveau de ses résultats était insuffisant ; que, par une décision du 21 juillet 2015, la présidente de l'université a, après avis de la commission d'admission, rejeté le recours gracieux formé contre cette décision ;<br clear="none"/>
<br clear="none"/>
5. Considérant qu'il résulte de ce qui précède qu'en jugeant que, en l'absence d'une liste de formations établie par décret dans les conditions prévues par l'article L. 612-6 du code de l'éducation, le moyen tiré de la méconnaissance des dispositions de cet article était propre à créer, en l'état de l'instruction, un doute sérieux sur la légalité des décisions contestées, le juge des référés n'a pas commis d'erreur de droit ;<br clear="none"/>
<br clear="none"/>
6. Considérant qu'en retenant que, eu égard à la proximité de la rentrée universitaire et aux effets du refus d'admission sur la situation de l'intéressée, la condition d'urgence devait être regardée comme remplie, le juge des référés n'a commis ni erreur de droit ni dénaturation ;<br clear="none"/>
<br clear="none"/>
7. Considérant qu'il résulte de tout ce qui précède que l'université Paul Valéry Montpellier III n'est pas fondée à demander l'annulation de l'ordonnance attaquée ;<br clear="none"/>
<br clear="none"/>
8. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de Mme A... qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de l'université Paul Valéry Montpellier III la somme de 1 500 euros à verser à la SCP Fabiani, Luc-Thaler, Pinatel, qui représente Mme A...devant le Conseil d'Etat, au titre de ces dispositions et de celles de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat ;<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
D E C I D E :<br clear="none"/>
--------------<br clear="none"/>
<br clear="none"/>
Article 1er : Le pourvoi de l'université Paul Valéry Montpellier III est rejeté.<br clear="none"/>
<br clear="none"/>
Article 2 : L'université paul Valéry Montpellier III versera à la SCP Fabiani, Luc-Thaler, Pinatel, une somme de 1 500 euros en application du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br clear="none"/>
<br clear="none"/>
Article 3 : La présente décision sera notifiée à l'université Paul Valéry Montpellier III et à Mme B....<br clear="none"/>
Copie en sera adressée à la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche.</p>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP>
<CONTENU>



</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
