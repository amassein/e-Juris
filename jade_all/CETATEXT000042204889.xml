<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042204889</ID>
<ANCIEN_ID>JG_L_2020_07_000000441704</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/20/48/CETATEXT000042204889.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 16/07/2020, 441704, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-07-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>441704</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>GALY</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:441704.20200716</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 06 juillet 2020 au secrétariat du contentieux du Conseil d'Etat, Mme A... B... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, d'ordonner la suspension de la délibération du jury du concours interne de l'agrégation, section lettres modernes, arrêtant la liste des admis à ce concours telle que publiée le 22 juin 2020 à 17h sur le site PUBLINET ainsi que les nominations dans le corps des agrégés intervenues ou à intervenir en conséquence du prononcé de la liste des admis.<br/>
<br/>
<br/>
<br/>
              Elle soutient que : <br/>
              - la condition d'urgence est satisfaite dès lors que, en premier lieu, l'administration ne saurait soutenir que la continuité du service public de l'enseignement est subordonnée à la nomination au plus tôt dans le corps des agrégés des personnes déclarées admises, en deuxième lieu, il importe de ne pas laisser subsister dans le temps des situations entachées d'une illégalité que l'administration serait tentée de valider rétroactivement, en troisième lieu, les conditions de préparation des candidats à un concours s'opposent à ce que le sort des candidats soit réglé rapidement et, en dernier lieu, le jury a décidé tardivement de supprimer les oraux d'admission des concours internes sans faire état de l'impossibilité d'organiser ces oraux en septembre ou en octobre 2020 ;<br/>
              -  il existe un doute sérieux quant à la légalité des décisions contestées ;<br/>
              - dans l'hypothèse où l'ordonnance n° 2020-351 du 27 mars 2020 serait analysée comme autorisant l'adaptation des voies d'accès au corps de la fonction publique, la délibération du jury du concours interne de l'agrégation devrait être regardée comme étant entachée d'illégalité dès lors qu'elle a été prise sur le fondement de l'arrêté du 10 juin 2020 lui-même pris sur le fondement d'un décret illégal faute de respecter la procédure de consultation obligatoire du Conseil d'Etat ;<br/>
              - dans l'hypothèse où le décret n° 2020-437 du 16 avril 2020 serait analysé comme permettant l'adaptation et la diminution du nombre des épreuves du concours interne, ce décret devrait être regardé comme illégal en ce qu'il s'applique au statut particulier des agrégés ;<br/>
              - l'arrêté du 10 juin 2020 est entaché d'une erreur manifeste d'appréciation faute de démontrer le caractère nécessaire de la suppression des épreuves d'admission et porte ainsi atteinte à la régularité de la délibération du jury du concours interne de l'agrégation ;<br/>
              - la délibération du jury du concours interne de l'agrégation méconnait le principe d'égal accès aux emplois publics eu égard à l'absence d'anonymat des candidats lors de la seconde délibération du jury. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
              1. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". Aux termes de l'article R. 522-1 du même code : " A peine d'irrecevabilité, les conclusions tendant à la suspension d'une décision administrative ou de certains de ses effets doivent être présentées par requête distincte de la requête à fin d'annulation ou de réformation et accompagnées d'une copie de cette dernière ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. Mme B... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, d'ordonner la suspension de l'exécution de la délibération du jury du concours interne de l'agrégation, section lettres modernes, arrêtant la liste des admis à ce concours ainsi que les nominations dans le corps des agrégés intervenues ou à intervenir en conséquence du prononcé de la liste des admis. Toutefois, il ne résulte pas de l'instruction que l'intéressée aurait introduit devant le Conseil d'Etat une requête distincte en annulation. En l'absence de recours sur le fond, la présente requête en référé suspension, qui méconnaît les dispositions de l'article R. 522-1 du code précité, est manifestement irrecevable.<br/>
<br/>
              3. Il résulte de ce qui précède que la requête de Mme B... doit être rejetée en toutes ses conclusions, selon la procédure prévue à l'article L. 522-3 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de Mme B... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à Mme A... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
