<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037167398</ID>
<ANCIEN_ID>JG_L_2018_07_000000409715</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/16/73/CETATEXT000037167398.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 4ème chambres réunies, 09/07/2018, 409715, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-07-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>409715</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Cécile Chaduteau-Monplaisir</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:409715.20180709</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire en réplique et un nouveau mémoire, enregistrés le 11 avril 2017 et les 2 mars et 6 juin 2018 au secrétariat du Conseil d'Etat, la Fédération française de l'assurance demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2017-162 du 9 février 2017 relatif au financement et à la gestion de façon mutualisée des prestations mentionnées au IV de l'article L. 912-1 du code de sécurité sociale ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution ;<br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - le code monétaire et financier ;<br/>
              - le code de sécurité sociale ; <br/>
              - le code du travail ;<br/>
              - la loi n° 2013-1203 du 23 décembre 2013 ;<br/>
              - l'arrêt du 17 décembre 2015 C-25/14 et C-26/14 de la Cour de justice de l'Union européenne ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Chaduteau-Monplaisir, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
<br/>
              Vu la note en délibéré, enregistrée le 25 juin 2018, présentée par la Fédération française de l'assurance ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. L'article L. 911-1 du code de la sécurité sociale prévoit qu'à moins qu'elles ne soient instituées par des dispositions législatives ou réglementaires, les garanties collectives dont bénéficient les salariés, anciens salariés et ayants droit en complément de celles qui résultent de l'organisation de la sécurité sociale sont déterminées, notamment, par voie de conventions ou d'accords collectifs. Le I de l'article L. 912-1 du même code, dans sa rédaction issue de la loi du 23 décembre 2013 de financement de la sécurité sociale pour 2014, dispose que : " Les accords professionnels ou interprofessionnels mentionnés à l'article L. 911-1 peuvent, dans des conditions fixées par décret en Conseil d'Etat, prévoir l'institution de garanties collectives présentant un degré élevé de solidarité et comprenant à ce titre des prestations à caractère non directement contributif, pouvant notamment prendre la forme d'une prise en charge partielle ou totale de la cotisation pour certains salariés ou anciens salariés, d'une politique de prévention ou de prestations d'action sociale. / Dans ce cas, les accords peuvent organiser la couverture des risques concernés en recommandant un ou plusieurs organismes mentionnés à l'article 1er de la loi n° 89-1009 du 31 décembre 1989 renforçant les garanties offertes aux personnes assurées contre certains risques ou une ou plusieurs institutions mentionnées à l'article L. 370-1 du code des assurances (...) ", sous réserve du respect des conditions de mise en concurrence et d'égalité définies au II du même article. Le IV du même article dispose que : " Les accords mentionnés au I peuvent prévoir que certaines des prestations nécessitant la prise en compte d'éléments relatifs à la situation des salariés ou sans lien direct avec le contrat de travail les liant à leur employeur sont financées et gérées de façon mutualisée, selon des modalités fixées par décret en Conseil d'Etat, pour l'ensemble des entreprises entrant dans leur champ d'application ". La fédération requérante demande l'annulation pour excès de pouvoir du décret du 9 février 2017 pris pour l'application de ces dispositions.<br/>
<br/>
              Sur la légalité externe :<br/>
<br/>
              2. Le décret attaqué définit les modalités de la mise en oeuvre de la gestion mutualisée de certaines des prestations à caractère non directement contributif que les accords mentionnés au I de l'article L. 912-1 précité du code de sécurité sociale peuvent instituer au titre des garanties collectives de protection sociale complémentaire. Il ne peut être regardé, ce faisant, comme  " traitant de questions relatives au secteur de l'assurance ", au sens de l'article L. 614-2 du code monétaire et financier, alors même que tant les entreprises d'assurance relevant du code des assurances que les institutions de prévoyance relevant du code de la sécurité sociale et les mutuelles relevant du code de la mutualité sont susceptibles de se voir confier le pilotage de la gestion du fonds de financement de ces prestations que le décret prévoit. Par suite, la fédération requérante n'est pas fondée à soutenir que le projet de décret aurait dû être, à ce titre, soumis à la consultation du comité consultatif de la législation et de la réglementation financières.<br/>
<br/>
              Sur la légalité interne :<br/>
<br/>
              3. Le décret attaqué insère dans le code de la sécurité sociale un nouvel article R. 912-3 aux termes duquel : " Lorsqu'ils mettent en oeuvre les dispositions du IV de l'article L. 912-1, les accords mentionnés au premier alinéa du I du même article : / 1° Définissent les prestations gérées de manière mutualisée qui comprennent des actions de prévention ou des prestations d'action sociale mentionnées à l'article R. 912-2 ; / 2° Déterminent les modalités de financement de ces actions. Ce financement peut prendre la forme d'un montant forfaitaire par salarié, d'un pourcentage de la prime ou de la cotisation mentionnée à l'article R. 912-1, ou d'une combinaison de ces deux éléments ; / 3° Créent un fonds finançant les prestations mentionnées au 1° et percevant les ressources mentionnées au 2° ; / 4° Précisent les modalités de fonctionnement de ce fonds, notamment les conditions de choix du gestionnaire chargé de son pilotage par la commission paritaire de branche ".<br/>
<br/>
              En ce qui concerne la définition des prestations financées et gérées de façon mutualisée :<br/>
<br/>
              4. En premier lieu, il résulte des dispositions combinées du I et du IV de l'article L. 912-1 précités du code de la sécurité sociale que le choix des prestations, parmi celles présentant un degré élevé de solidarité, qui sont financées et gérées de façon mutualisée, est confié aux signataires des accords mentionnés au I de cet article, seule la fixation des modalités du financement et de la gestion mutualisés étant renvoyée à un décret en Conseil d'Etat. Par suite, la fédération requérante ne saurait utilement soutenir que le pouvoir réglementaire aurait incomplètement satisfait à l'objectif qui lui était assigné par le législateur en se bornant, au 1° de l'article R. 912-3 inséré dans le code de la sécurité sociale par le décret attaqué, à prévoir que les accords mettant en oeuvre les dispositions du IV de l'article L. 912-1 de ce code définissent les prestations qui seront financées et gérées de façon mutualisée de telle sorte qu'elles comprennent des actions de prévention ou des prestations d'action sociale propres à conférer, selon l'article R. 912-2 du même code, un degré élevé de solidarité aux garanties prévues par ces accords. <br/>
<br/>
              5. En second lieu, la fédération requérante ne peut utilement soutenir que le choix des prestations financées et gérées de façon mutualisée par les signataires des accords mentionnés au I de l'article L. 912-1 du code de la sécurité sociale, qui, comme il a été dit, résulte de la loi elle-même, porterait une atteinte disproportionnée à la liberté contractuelle et à la liberté d'entreprendre.<br/>
<br/>
              En ce qui concerne les modalités de financement de ces prestations :<br/>
<br/>
              6. En premier lieu, le pouvoir réglementaire a défini, au 2° du nouvel article R. 912-3 du code de la sécurité sociale cité au point 3, les trois modalités possibles de financement des prestations mentionnées au IV de l'article L. 912-1 précité du même code. Par suite, le moyen tiré de ce que le pouvoir réglementaire aurait insuffisamment encadré ces modalités doit être écarté.<br/>
<br/>
              7. En second lieu, en prévoyant trois modalités de financement entre lesquelles les signataires des accords mentionnés au I de l'article L. 912-1 peuvent choisir, le décret attaqué ne peut être regardé comme ayant méconnu le principe d'égalité entre salariés ou entre entreprises.<br/>
<br/>
              En ce qui concerne les modalités de gestion de ces prestations et de leur financement : <br/>
<br/>
              8. En premier lieu, aux termes du premier alinéa de l'article 56 du traité sur le fonctionnement de l'Union européenne : " Dans le cadre des dispositions ci-après, les restrictions à la libre prestation des services à l'intérieur de l'Union sont interdites à l'égard des ressortissants des États membres établis dans un État membre autre que celui du destinataire de la prestation ".<br/>
<br/>
              9. Par un arrêt C-25/14 et C-26/14 du 17 décembre 2015, la Cour de justice de l'Union européenne a rappelé, s'agissant des prestations de services qui impliquent une intervention des autorités nationales, que l'obligation de transparence découlant de l'article 56 du traité sur le fonctionnement de l'Union européenne s'applique non pas à toute opération, mais uniquement à celles qui présentent un intérêt transfrontalier certain, du fait qu'elles sont objectivement susceptibles d'intéresser des opérateurs économiques établis dans d'autres Etats membres. Elle a dit pour droit que c'est la décision d'extension, par un Etat membre, à l'ensemble des employeurs et des travailleurs salariés d'une branche d'activité, d'un accord collectif, conclu par les organisations représentatives d'employeurs et de travailleurs salariés de cette branche, confiant à un unique opérateur économique, choisi par les partenaires sociaux, la gestion d'un régime de prévoyance complémentaire obligatoire institué au profit des travailleurs salariés, qui crée un droit exclusif en faveur de cet organisme, ce dont elle a déduit que c'est cette décision administrative qui doit intervenir dans le respect de l'obligation de transparence découlant de l'article 56 du traité. <br/>
<br/>
              10. Il résulte de ce qui précède que, lorsque le choix de l'opérateur auquel est confié, dans une branche, le pilotage du fonds, prévu par le décret attaqué pour gérer le financement des prestations en cause, peut être regardé comme constituant une opération présentant un intérêt transfrontalier certain, le ministre compétent, saisi d'une demande tendant à ce qu'il étende l'accord confiant ce pilotage à cet opérateur, ne pourrait légalement y faire droit sans qu'il ait été satisfait à l'obligation de transparence découlant de l'article 56 du traité sur le fonctionnement de l'Union européenne. En revanche, la circonstance que le décret attaqué n'ait pas imposé aux partenaires sociaux eux-mêmes, lorsqu'il concluent un accord mettant en oeuvre les dispositions du IV de l'article L. 912-1 du code de la sécurité sociale, de choisir le gestionnaire chargé du pilotage au terme d'une procédure respectant l'obligation de transparence découlant de l'article 56 du traité sur le fonctionnement de l'Union européenne n'est pas de nature à l'entacher d'illégalité. Le décret attaqué n'a pas davantage méconnu les dispositions du IV de l'article L. 912-1 précité en ne prévoyant pas une telle obligation ou en ne fixant pas les règles de création du fonds.<br/>
<br/>
<br/>
              11. En deuxième lieu, en prévoyant le choix, par la commission paritaire de branche, du gestionnaire chargé du pilotage du fonds finançant les prestations considérées, le décret n'a pas méconnu les dispositions du IV de l'article L. 912-1 du code de la sécurité sociale.<br/>
<br/>
              12. En dernier lieu, l'extension à l'ensemble des employeurs et des travailleurs salariés d'une branche d'activité d'un accord collectif mettant en oeuvre les dispositions du IV de l'article L. 912-1 du code de la sécurité sociale ne résulte pas du décret attaqué mais des dispositions de l'article L. 911-3 du même code et de l'article L. 2261-15 du code du travail. Par suite, la fédération requérante ne peut utilement soutenir que le décret attaqué porterait atteinte à la liberté contractuelle et la liberté d'entreprendre garanties par l'article 4 de la Déclaration de 1789 en créant l'obligation, pour toutes les entreprises de la branche, de participer au financement d'un fonds dont elles n'auraient pas choisi le gestionnaire, selon des modalités et pour des prestations qu'elles n'auraient pas choisies, ni qu'il serait susceptible ce faisant de conduire à un abus de position dominante prohibé par les articles 102 et 106, paragraphe 1, du traité sur le fonctionnement de l'Union européenne . <br/>
<br/>
              13. Il résulte de tout ce qui précède que la Fédération française de l'assurance n'est pas fondée à demander l'annulation du décret qu'elle attaque.<br/>
<br/>
              14. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que la somme demandée sur leur fondement soit mise à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance. <br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La requête de la Fédération française de l'assurance est rejetée. <br/>
Article 2 : La présente décision sera notifiée à la Fédération française de l'assurance et à la ministre des solidarités et de la santé. <br/>
Copie en sera adressée au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
