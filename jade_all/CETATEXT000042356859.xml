<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042356859</ID>
<ANCIEN_ID>JG_L_2020_09_000000443851</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/35/68/CETATEXT000042356859.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 22/09/2020, 443851, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-09-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>443851</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:443851.20200922</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 8 septembre 2020 au secrétariat du contentieux du Conseil d'Etat, la fédération nationale des chasseurs et la fédération régionale des chasseurs de la région Provence-Alpes-Côte d'Azur demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution de la décision de la ministre de la transition écologique, révélée par ses déclarations publiques des 27 et 28 août 2020, refusant d'autoriser, pour la campagne 2020-2021, l'emploi des gluaux pour la capture des grives et des merles destinés à servir d'appelants dans les départements des Alpes-de-Haute-Provence, des Alpes-Maritimes, des Bouches-du-Rhône, du Var et du Vaucluse ; <br/>
<br/>
              2°) d'enjoindre à la ministre de la transition écologique de poursuivre la procédure engagée tendant à l'édiction de l'arrêté fixant le nombre maximum d'oiseaux pouvant être capturés pendant la campagne 2020-2021 ainsi que, le cas échéant, les spécifications techniques propres à chaque département concerné ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Elles soutiennent que :<br/>
              - leur requête est recevable, dès lors que le Conseil d'Etat est compétent, en application de l'article R. 311-1 du code de justice administrative, qu'elles justifient d'un intérêt leur donnant qualité pour agir et que les déclarations de la ministre de la transition écologique révèlent une décision faisant grief susceptible de faire l'objet d'un recours devant la juridiction administrative ;<br/>
              - la condition d'urgence est remplie eu égard, d'une part, à l'imminence de l'ouverture de la campagne de chasse pour l'année 2020-2021 qui aura lieu le 13 septembre 2020 et, d'autre part, à l'atteinte portée aux intérêts qu'elles entendent défendre ;<br/>
              - il existe un doute sérieux quant à la légalité de la décision contestée ;<br/>
              - la décision contestée a été prise au terme d'une procédure irrégulière dès lors qu'elle a été précédée d'une consultation du public sur la base d'un projet de décision comportant des dispositions substantiellement différentes de celles finalement retenues, en méconnaissance des dispositions de l'article L. 123-19-1 du code de l'environnement ;<br/>
              - elle méconnaît les dispositions de l'article L. 424-4 du code de l'environnement dès lors que la ministre de la transition écologique n'est habilitée par le législateur qu'à l'effet de déterminer les modalités de mise en oeuvre des modes et moyens de chasse consacrés par les usages traditionnels et non d'interdire l'emploi des gluaux.<br/>
<br/>
              Par un mémoire en défense, enregistré le 15 septembre 2020, la ministre de la transition écologique conclut au rejet de la requête. Elle soutient que la condition d'urgence n'est pas remplie et qu'aucun moyen soulevé n'est propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision contestée. <br/>
<br/>
              La requête a été communiquée à l'Office français de la biodiversité qui n'a pas produit d'observations.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la directive 2009/147/CE du 30 novembre 2009 du Parlement européen et du Conseil ;<br/>
              - le code de l'environnement ;<br/>
              - le décret n° 2020-869 du 15 juillet 2020 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, la fédération nationale des chasseurs et la fédération régionale des chasseurs de la région Provence-Alpes-Côte d'Azur et, d'autre part, la ministre de la transition écologique et l'Office français de la biodiversité ;<br/>
<br/>
              Ont été entendus lors de l'audience publique du 16 septembre 2020, à 10 heures : <br/>
<br/>
              - Me Spinosi, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la fédération nationale des chasseurs et de la fédération régionale des chasseurs de la région Provence-Alpes-Côte d'Azur ;<br/>
<br/>
              - le représentant de la fédération nationale des chasseurs ;<br/>
<br/>
              - les représentants de la ministre de la transition écologique ;<br/>
<br/>
              à l'issue de laquelle le juge des référés a clos l'instruction.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". Il résulte de ces dispositions que le prononcé de la suspension d'un acte administratif est subordonné notamment à une condition d'urgence. L'urgence justifie la suspension de l'exécution d'un acte administratif lorsque celui-ci porte atteinte de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre. Il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte contesté sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue. L'urgence doit être appréciée objectivement et compte tenu de l'ensemble des circonstances de l'affaire.<br/>
<br/>
              2. Aux termes de l'article de l'article L. 424-4 du code de l'environnement : " (...) Pour permettre, dans des conditions strictement contrôlées et de manière sélective, la chasse de certains oiseaux de passage en petites quantités, le ministre chargé de la chasse autorise, dans les conditions qu'il détermine, l'utilisation des modes et moyens de chasse consacrés par les usages traditionnels, dérogatoires à ceux autorisés par le premier alinéa. / (...) / Les gluaux sont posés une heure avant le lever du soleil et enlevés avant onze heures. / (...) ".<br/>
<br/>
              3. La fédération nationale des chasseurs et la fédération régionale des chasseurs de la région Provence-Alpes-Côte d'Azur demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, d'ordonner la suspension de l'exécution de la décision de la ministre de la transition écologique, révélée par ses déclarations publiques des 27 et 28 août 2020, refusant d'autoriser, pour la campagne 2020-2021, l'emploi des gluaux pour la capture des grives et des merles destinés à servir d'appelants dans les départements des Alpes-de-Haute-Provence, des Alpes-Maritimes, des Bouches-du-Rhône, du Var et du Vaucluse. <br/>
<br/>
              4. Pour caractériser l'urgence qu'il y aurait à suspendre la décision contestée, les fédérations requérantes se bornent à soutenir que le refus de la ministre de la transition écologique d'autoriser, pour la campagne 2020-2021, l'emploi du gluaux pour la capture des grives et des merles destinés à servir d'appelants dans certains départements porte une atteinte grave et immédiate aux intérêts qu'elles entendent défendre, eu égard à l'ouverture imminente de cette campagne et aux conséquences, notamment financières, de l'interdiction de ce mode de chasse traditionnel pratiqué par plus de 6 900 chasseurs de la région Provence-Alpes-Côte d'Azur. <br/>
<br/>
              5. Toutefois, il résulte de l'instruction que la décision en litige est motivée par les doutes de la ministre de la transition écologique sur la compatibilité des dispositions de l'article L. 424-4 du code de l'environnement autorisant la chasse aux gluaux avec la directive 2009/147/CE du Parlement européen et du Conseil du 30 novembre 2009 concernant la conservation des oiseaux sauvages, eu égard notamment à la décision nos 425519 et a. du 29 novembre 2019 par laquelle le Conseil d'Etat statuant au contentieux a renvoyé à la Cour de justice de l'Union européenne une question préjudicielle portant notamment sur ce point. <br/>
<br/>
              6. Les éléments invoqués par les requérantes ne sont pas de nature à caractériser une situation d'urgence au sens de l'article L. 521-1 du code de justice administrative, eu égard à l'intérêt général qui s'attache au respect du droit de l'Union européenne et à la conservation des oiseaux sauvages concernés.<br/>
<br/>
              7. Il résulte de ce qui précède que les demandes de la fédération nationale des chasseurs et de la fédération régionale des chasseurs de la région Provence-Alpes-Côte d'Azur doivent être rejetées, y compris leurs conclusions au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er: La requête de la fédération nationale des chasseurs et de la fédération régionale des chasseurs de la région Provence-Alpes-Côte d'Azur est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à la fédération nationale des chasseurs, premier requérant dénommé, et à la ministre de la transition écologique. <br/>
Copie en sera adressée à l'Office français de la bioversité.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
