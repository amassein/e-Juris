<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027170035</ID>
<ANCIEN_ID>JG_L_2013_03_000000354976</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/17/00/CETATEXT000027170035.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 13/03/2013, 354976</TITRE>
<DATE_DEC>2013-03-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>354976</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PEIGNOT, GARREAU, BAUER-VIOLAS ; RICARD</AVOCATS>
<RAPPORTEUR>Mme Chrystelle Naudan-Carastro</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:354976.20130313</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 19 décembre 2011 et 19 mars 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour le préfet de police, représentant la ville de Paris ; le préfet de police demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 09PA05817 du 18 octobre 2011 de la cour administrative d'appel de Paris en ce qu'il a annulé le jugement n° 0704753-0704745 du 1er juillet 2009 du tribunal administratif de Paris, en tant qu'il avait rejeté les conclusions de Mme B...tendant à l'annulation de la décision du 22 mai 2006 du commissaire divisionnaire de la brigade de répression de la délinquance contre la personne ordonnant son placement à l'infirmerie psychiatrique de la préfecture de police, ainsi que cette décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter sur ce point la requête de Mme B...; <br/>
<br/>
              3°) de mettre à la charge de Mme B...le versement de la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu les décisions du Conseil constitutionnel n° 2011-135/140 QPC du 9 juin 2011 et n° 2011-174 QPC du 6 octobre 2011 ; <br/>
<br/>
              Vu le code de la santé publique ; <br/>
<br/>
              Vu la loi n° 79-587 du 11 juillet 1979 ;<br/>
<br/>
              Vu la loi n° 2000-321 du 12 avril 2000 ; <br/>
	Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Chrystelle Naudan-Carastro, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de la SCP Peignot, Garreau, Bauer-Violas, avocat du préfet de police et de Me Ricard, avocat de MmeC...,<br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Peignot, Garreau, Bauer-Violas, avocat du préfet de police et à Me Ricard, avocat de MmeC... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme B...a été conduite à l'infirmerie psychiatrique de la préfecture de police par une décision du 22 mai 2006 du commissaire de police de la brigade de répression de la délinquance contre la personne ; qu'au vu du procès-verbal de police et du certificat médical établi par le médecin de l'infirmerie psychiatrique, le préfet de police a, par arrêté du 24 mai 2006 pris sur le fondement de l'article L. 3213-1 du code de la santé publique, ordonné son hospitalisation d'office pour une durée d'un mois ; que, par un jugement du 1er juillet 2009, le tribunal administratif de Paris a rejeté les demandes de Mme B...tendant à l'annulation de la décision du 22 mai 2006 et de l'arrêté du 24 mai 2006 ; que, par l'arrêt attaqué du 18 octobre 2011, la cour administrative d'appel de Paris a, d'une part, annulé ce jugement en tant qu'il a rejeté les conclusions de Mme B...tendant à l'annulation de la décision du 22 mai 2006 et, d'autre part, annulé cette décision ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 3211-3 du code de la santé publique, dans sa rédaction en vigueur à la date de la décision attaquée : " Lorsqu'une personne atteinte de troubles mentaux est hospitalisée sans son consentement en application des dispositions des chapitres II et III du présent titre ou est transportée en vue de cette hospitalisation, les restrictions à l'exercice de ses libertés individuelles doivent être limitées à celles nécessitées par son état de santé et la mise en oeuvre de son traitement. (...) / Elle doit être informée dès l'admission et par la suite, à sa demande, de sa situation juridique et de ses droits. / En tout état de cause, elle dispose du droit : / (...) 3° De prendre conseil d'un médecin ou d'un avocat de son choix " ; qu'aux termes de l'article L. 3213-2 du même code, dans sa rédaction en vigueur à la date de la décision attaquée : " En cas de danger imminent pour la sûreté des personnes, attesté par un avis médical, le maire et, à Paris, les commissaires de police arrêtent, à l'égard des personnes dont le comportement révèle des troubles mentaux manifestes, toutes les mesures provisoires nécessaires, à charge d'en référer dans les vingt-quatre heures au représentant de l'Etat dans le département qui statue sans délai et prononce, s'il y a lieu, un arrêté d'hospitalisation d'office dans les formes prévues à l'article L. 3213-1. Faute de décision du représentant de l'Etat, ces mesures provisoires sont caduques au terme d'une durée de quarante-huit heures " ; <br/>
<br/>
              3. Considérant qu'alors même que la conduite à l'infirmerie psychiatrique de la préfecture de police est une mesure de police administrative à caractère provisoire et de très courte durée, destinée principalement à l'observation des personnes souffrant de troubles mentaux manifestes et à leur protection ainsi qu'à celle des tiers, et que ce service ne relève pas des établissements de soins mentionnés aux articles L. 3214-1 et L. 3222-1 du code de la santé publique au sein desquels sont accueillis et soignés les malades faisant l'objet d'une hospitalisation sur demande d'un tiers ou d'office en application, respectivement, des articles L. 3212-1 et L. 3213-1 du même code, l'admission dans cette structure doit être regardée comme une hospitalisation sans consentement de la personne intéressée au sens et pour l'application des dispositions de l'article L. 3211-3 du même code, dont le champ d'application s'étend à toutes les mesures de cette nature décidées dans le cadre des chapitres II et III du titre I du livre II de la troisième partie du code de la santé publique ; que si la personne intéressée doit, par conséquent, dès son admission à l'infirmerie psychiatrique de la préfecture de police, être informée de son droit d'avoir recours à un avocat ou à un médecin, l'accomplissement de cette obligation n'a pas à précéder l'édiction de la décision de conduite à l'infirmerie psychiatrique mais se rapporte à l'exécution de cette décision et est donc sans incidence sur sa légalité ; que, par suite, la cour administrative d'appel a commis une erreur de droit en jugeant que la circonstance que Mme B... n'avait pas été informée de ses droits avant d'être conduite à l'infirmerie psychiatrique de la préfecture de police entachait la décision du commissaire divisionnaire du 22 mai 2006 d'un vice de procédure substantiel de nature à justifier son annulation ; qu'il suit de là que le préfet de police est fondé à demander l'annulation de l'article 1er de l'arrêt attaqué ;<br/>
<br/>
              4. Considérant que si Mme B...conteste le même arrêt, par la voie du pourvoi incident, en tant qu'il a, à l'article 2, confirmé le rejet, par les premiers juges, de ses conclusions dirigées contre l'arrêté du préfet de police du 24 mai 2006, ces conclusions soulèvent un litige distinct de celui qui fait l'objet du pourvoi principal ; qu'elles ont été enregistrées après l'expiration du délai de recours en cassation et ne sont, dès lors, pas recevables ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond, en application des dispositions de l'article L. 821-2 du code de justice administrative, dans la mesure de la cassation prononcée ;<br/>
<br/>
              6. Considérant, en premier lieu, qu'il résulte de ce qui a été dit ci-dessus que la circonstance que Mme B...n'aurait pas été informée de son droit d'avoir recours à un avocat ou à un médecin avant d'être conduite à l'infirmerie psychiatrique de la préfecture de police est sans incidence sur la légalité de la décision du 22 mai 2006 ;<br/>
<br/>
              7. Considérant, en deuxième lieu, qu'aux termes de l'article 24 de la loi du 12 avril 2000 : " Exception faite des cas où il est statué sur une demande, les décisions individuelles qui doivent être motivées en application des articles 1er et 2 de la loi n° 79-587 du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public n'interviennent qu'après que la personne intéressée a été mise à même de présenter des observations écrites et, le cas échéant, sur sa demande, des observations orales. (...). Les dispositions de l'alinéa précédent ne sont pas applicables : 1° En cas d'urgence ou de circonstances exceptionnelles ; (...) " ; qu'en vertu de l'article 1er de la loi du 11 juillet 1979 : " (...) doivent être motivées les décisions qui restreignent l'exercice des libertés publiques ou, de manière générale, constituent une mesure de police " ; que les mesures provisoires prises sur le fondement de l'article L. 3213-2 du code de la santé publique sont au nombre des mesures de police qui doivent être motivées en application de l'article 1er de la loi du 11 juillet 1979 ; qu'elles entrent ainsi dans le champ d'application de l'article 24 de la loi du 12 avril 2000 ; que toutefois, l'urgence qui s'attachait à ce que soit prise la décision de conduire Mme B...à l'infirmerie psychiatrique de la préfecture de police, en application des dispositions de l'article L. 3213-2 du code de la santé publique, était de nature à exonérer l'administration du respect de la procédure prévue par l'article 24 de la loi du 12 avril 2000 ; qu'il suit de là que Mme B...n'est pas fondée à soutenir que la décision attaquée serait intervenue en méconnaissance de ces dispositions ;<br/>
<br/>
              8. Considérant, en troisième lieu, qu'aux termes de l'article 3 de la loi du 11 juillet 1979 : " La motivation exigée par la présente loi doit (...) comporter l'énoncé des considérations de droit et de fait qui constituent le fondement de la décision " ; qu'il résulte de ces dispositions que l'autorité administrative, lorsqu'elle prend une mesure provisoire sur le fondement de l'article L. 3213-2 du code de la santé publique, doit indiquer dans sa décision les éléments de droit et de fait qui justifient cette mesure, sauf lorsque l'urgence absolue a empêché qu'une telle décision soit motivée ; que, si elle peut satisfaire à cette exigence de motivation en se référant à un avis médical, c'est à la condition de s'en approprier le contenu et de joindre cet avis à la décision ;<br/>
<br/>
              9. Considérant qu'il ressort des pièces du dossier que la décision du 22 mai 2006 a été prise au regard d'une expertise psychiatrique ordonnée par le parquet du tribunal de grande instance de Paris dont le commissaire de police a repris les conclusions aux termes desquelles Mme B...était atteinte d'un " grave trouble de la personnalité qui altère son discernement et le contrôle de ses actes " et qui indiquait qu'un " suivi clinique psychiatrique est indispensable " ; qu'il n'est pas contesté que ce rapport d'expertise était annexé à la décision litigieuse ; que l'auteur de cette décision a souligné qu'au regard de l'urgence et de l'état mental de Mme B...qui tenait un discours incohérent, il ne paraissait pas possible " de la laisser sans certitude d'un suivi psychiatrique, son état mental laissant positivement craindre qu'elle ne soit dangereuse pour elle ou pour autrui " ; que, par suite, la décision du 22 mai 2006, qui énonce les éléments de fait et de droit sur lesquels elle se fonde, est suffisamment motivée au regard des exigences de la loi du 11 juillet 1979 ;<br/>
<br/>
              10. Considérant qu'il résulte de tout ce qui précède que Mme B... n'est pas fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Paris a rejeté sa demande tendant à l'annulation de la décision du 22 mai 2006 ; <br/>
<br/>
              11. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme B...le versement à la ville de Paris de la somme que demande le préfet de police en application des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font obstacle à ce qu'il soit fait droit aux conclusions présentées au même titre par MmeB... ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 1er de l'arrêt de la cour administrative d'appel de Paris du 18 octobre 2011 est annulé.<br/>
Article 2 : Le pourvoi incident de Mme B...et sa requête devant la cour administrative d'appel de Paris, en tant qu'elle concerne la décision du 22 mai 2006 du commissaire de police de la brigade de répression de la délinquance contre la personne, sont rejetés.<br/>
Article 3 : Les conclusions du préfet de police tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au préfet de police et à Mme A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-03-01-02-01-01-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. QUESTIONS GÉNÉRALES. MOTIVATION. MOTIVATION OBLIGATOIRE. MOTIVATION OBLIGATOIRE EN VERTU DES ARTICLES 1 ET 2 DE LA LOI DU 11 JUILLET 1979. DÉCISION RESTREIGNANT L'EXERCICE DES LIBERTÉS PUBLIQUES OU, DE MANIÈRE GÉNÉRALE, CONSTITUANT UNE MESURE DE POLICE. - MESURES PROVISOIRES PRISES SUR LE FONDEMENT DE L'ARTICLE L. 3213-2 DU CSP - 1) EXIGENCE DE MOTIVATION (ART. 1ER DE LA LOI DU 11 JUILLET 1979) - EXISTENCE - EXIGENCE D'UNE PROCÉDURE CONTRADICTOIRE PRÉALABLE - EXISTENCE (ART. 24 DE LA LOI DCRA) [RJ1] - 2) EXCEPTION D'URGENCE - EXISTENCE EN L'ESPÈCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">01-03-03-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. PROCÉDURE CONTRADICTOIRE. CARACTÈRE OBLIGATOIRE. - MESURES PROVISOIRES PRISES SUR LE FONDEMENT DE L'ARTICLE L. 3213-2 DU CSP - 1) EXIGENCE DE MOTIVATION (ART. 1ER DE LA LOI DU 11 JUILLET 1979) - EXISTENCE - EXIGENCE D'UNE PROCÉDURE CONTRADICTOIRE PRÉALABLE - EXISTENCE (ART. 24 DE LA LOI DCRA) [RJ1] - 2) EXCEPTION D'URGENCE - EXISTENCE EN L'ESPÈCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">61-03-04 SANTÉ PUBLIQUE. LUTTE CONTRE LES FLÉAUX SOCIAUX. LUTTE CONTRE LES MALADIES MENTALES. - MESURES PROVISOIRES PRISES SUR LE FONDEMENT DE L'ARTICLE L. 3213-2 DU CSP - 1) EXIGENCE DE MOTIVATION (ART. 1ER DE LA LOI DU 11 JUILLET 1979) - EXISTENCE - EXIGENCE D'UNE PROCÉDURE CONTRADICTOIRE PRÉALABLE - EXISTENCE (ART. 24 DE LA LOI DCRA) [RJ1] - 2) EXCEPTION D'URGENCE - EXISTENCE EN L'ESPÈCE.
</SCT>
<ANA ID="9A"> 01-03-01-02-01-01-01 1) Les mesures provisoires prises sur le fondement de l'article L. 3213-2 du code de la santé publique (CSP) sont au nombre des mesures de police qui doivent être motivées en application de l'article 1er de la loi n° 79-587 du 11 juillet 1979. Elles entrent ainsi dans le champ d'application de l'article 24 de la loi n° 2000-321 du 12 avril 2000 dite loi DCRA.... ...2) Toutefois, en l'espèce, l'urgence qui s'attachait à ce que soit prise la décision de conduire la requérante à l'infirmerie psychiatrique de la préfecture de police, en application des dispositions de l'article L. 3213-2 du CSP, était de nature à exonérer l'administration du respect de cette procédure.</ANA>
<ANA ID="9B"> 01-03-03-01 1) Les mesures provisoires prises sur le fondement de l'article L. 3213-2 du code de la santé publique (CSP) sont au nombre des mesures de police qui doivent être motivées en application de l'article 1er de la loi n° 79-587 du 11 juillet 1979. Elles entrent ainsi dans le champ d'application de l'article 24 de la loi n° 2000-321 du 12 avril 2000 dite loi DCRA.... ...2) Toutefois, en l'espèce, l'urgence qui s'attachait à ce que soit prise la décision de conduire la requérante à l'infirmerie psychiatrique de la préfecture de police, en application des dispositions de l'article L. 3213-2 du CSP, était de nature à exonérer l'administration du respect de cette procédure.</ANA>
<ANA ID="9C"> 61-03-04 1) Les mesures provisoires prises sur le fondement de l'article L. 3213-2 du code de la santé publique (CSP) sont au nombre des mesures de police qui doivent être motivées en application de l'article 1er de la loi n° 79-587 du 11 juillet 1979. Elles entrent ainsi dans le champ d'application de l'article 24 de la loi n° 2000-321 du 12 avril 2000 dite loi DCRA.... ...2) Toutefois, en l'espèce, l'urgence qui s'attachait à ce que soit prise la décision de conduire la requérante à l'infirmerie psychiatrique de la préfecture de police, en application des dispositions de l'article L. 3213-2 du CSP, était de nature à exonérer l'administration du respect de cette procédure.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., pour les décisions de maintien d'une mesure d'hospitalisation d'office, CE, 27 mai 2011, Mme Kupferstein, n° 330267, T. p. 1056 ; pour l'obligation de motivation des arrêtés du maire ordonnant l'internement des personnes atteintes d'aliénation mentale en vertu de l'ancien article L. 344 du CSP, CE, Section, 31 mars 1989, Ministre de l'intérieur et de la décentralisation et Lambert, n°s 69547 71747, p. 110.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
