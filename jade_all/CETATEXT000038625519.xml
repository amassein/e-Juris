<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038625519</ID>
<ANCIEN_ID>JG_L_2019_06_000000408121</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/62/55/CETATEXT000038625519.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 14/06/2019, 408121, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-06-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>408121</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Sophie Baron</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:408121.20190614</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire en réplique et un nouveau mémoire, enregistrés les 17 février et 16 octobre 2017 et le 4 mai 2018 au secrétariat du contentieux du Conseil d'Etat, M. C...D...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la délibération du comité de sélection statuant sur les candidatures au poste de professeur des universités en histoire contemporaine, ainsi que, par voie de conséquence, la délibération du 2 juin 2016 du conseil d'administration de l'établissement sur le classement des candidats au concours opéré par ce comité, la décision du directeur de l'Institut d'études politiques de Lyon de communiquer au ministre de l'enseignement supérieur le nom de M. A...E..., la décision implicite du directeur de l'Institut d'études politiques de Lyon rejetant le recours gracieux formé contre ces délibérations et le décret du Président de la République du 15 décembre 2016 en tant qu'il nomme M. E... professeur des universités à l'Institut d'études politiques de Lyon ;<br/>
<br/>
              2°) d'enjoindre au directeur de l'Institut d'études politiques de Lyon d'engager une nouvelle procédure de recrutement d'un professeur d'histoire contemporaine dans un délai d'un mois à compter de la notification de sa décision sous astreinte de 200 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat et de l'Institut d'études politiques de Lyon la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'éducation ;<br/>
              - le décret n° 84-430 du 6 juin 1984 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie Baron, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. M.D..., maître de conférences à l'Institut d'études politiques de Lyon demande l'annulation, pour excès de pouvoir, de la délibération du comité de sélection statuant sur les candidatures au poste de professeur des universités en histoire contemporaine relevant de la 22ème section du Conseil national des universités à l'Institut d'études politiques de Lyon (PR0047), ainsi que, par voie de conséquence, l'annulation de la délibération du 2 juin 2016 du conseil d'administration de l'établissement sur le classement des candidats au concours opéré par ce comité, de la décision du directeur de l'Institut d'études politiques de Lyon de communiquer au ministre de l'enseignement supérieur le nom de M.E..., de la décision du directeur de l'Institut d'études politiques de Lyon rejetant le recours gracieux formé contre ces délibérations et du décret du Président de la République du 15 décembre 2016 en tant qu'il nomme M. E... professeur des universités à l'Institut d'études politiques de Lyon.<br/>
<br/>
              2. Aux termes de l'article L. 952-6-1 du code de l'éducation : " (...) lorsqu'un emploi d'enseignant-chercheur est créé ou déclaré vacant, les candidatures des personnes dont la qualification est reconnue par l'instance nationale prévue à l'article L. 952-6 sont soumises à l'examen d'un comité de sélection créé par délibération (...) du conseil d'administration, siégeant en formation restreinte aux représentants élus des enseignants-chercheurs, des chercheurs et des personnels assimilés. / Le comité est composé d'enseignants-chercheurs et de personnels assimilés, pour moitié au moins extérieurs à l'établissement, d'un rang au moins égal à celui postulé par l'intéressé (...) / Au vu de son avis motivé, (...) le conseil d'administration, siégeant en formation restreinte aux enseignants-chercheurs et personnels assimilés de rang au moins égal à celui postulé, transmet au ministre compétent le nom du candidat dont il propose la nomination ou une liste de candidats classés par ordre de préférence (...) ". Aux termes de l'article 9-2 du décret du 6 juin 1984 fixant les dispositions statutaires communes applicables aux enseignants-chercheurs et portant statut particulier du corps des professeurs des universités et du corps des maîtres de conférences : " Le comité de sélection examine les dossiers des maîtres de conférences ou professeurs postulant à la nomination dans l'emploi par mutation et des candidats à cette nomination par détachement et par recrutement au concours parmi les personnes inscrites sur la liste de qualification aux fonctions, selon le cas, de maître de conférences ou de professeur des universités. Au vu de rapports pour chaque candidat présenté par deux de ses membres, le comité établit la liste des candidats qu'il souhaite entendre (...)  / Le président du comité de sélection convoque les candidats et fixe l'ordre du jour de la réunion (...) / L'audition des candidats par le comité de sélection peut comprendre une mise en situation professionnelle, sous forme notamment de leçon ou de séminaire de présentation des travaux de recherche. (...) / Après avoir procédé aux auditions, le comité de sélection délibère sur les candidatures et, par un avis motivé unique portant sur l'ensemble des candidats, arrête la liste, classée par ordre de préférence, de ceux qu'il retient. (...) / Le comité de sélection émet un avis motivé unique portant sur l'ensemble des candidats ainsi qu'un avis motivé sur chaque candidature. Ces deux avis sont communiqués aux candidats sur leur demande (...) Le conseil d'administration, siégeant en formation restreinte aux enseignants-chercheurs et personnels assimilés de rang au moins égal à celui postulé, prend connaissance du nom du candidat sélectionné (...) / Sauf dans le cas où le conseil d'administration émet un avis défavorable motivé, le président ou directeur de l'établissement communique au ministre chargé de l'enseignement supérieur le nom du candidat sélectionné (...) ". <br/>
<br/>
              3. En premier lieu, il ressort des pièces du dossier que le comité de sélection a procédé au classement de quatre des six candidats qu'il a auditionnés. Par suite, le moyen tiré de ce que le comité de sélection n'a pas établi le classement des candidats auditionnés manque en fait. <br/>
<br/>
              4. En second lieu, la seule circonstance qu'un membre du jury d'un concours connaisse un candidat ne suffit pas à justifier qu'il s'abstienne de participer aux délibérations de ce concours. En revanche, le respect du principe d'impartialité exige que, lorsqu'un membre du jury d'un concours a avec l'un des candidats des liens, tenant à la vie personnelle ou aux activités professionnelles, qui seraient de nature à influer sur son appréciation, ce membre doit s'abstenir de participer aux interrogations et aux délibérations concernant non seulement ce candidat mais encore l'ensemble des candidats au concours.<br/>
<br/>
              5. D'une part, si M. D...reproche à M. Douzou, président du comité de sélection institué en vue de pourvoir le poste de professeur des universités en histoire contemporaine laissé vacant à la suite du départ à la retraite de M.B..., d'avoir proposé au conseil d'administration, pour siéger au sein du comité de sélection en qualité de membres extérieurs, des enseignants ayant pris parti en sa faveur dans le conflit qui l'opposait, au sein de l'institut, à M. B...et ayant pour certains d'entre eux, contribué à des ouvrages ou à des colloques dont il a assuré la direction, il ne ressort pas des pièces du dossier que ces liens, à les supposer établis, auraient été de nature à influer sur l'appréciation qu'ils pouvaient être conduits à porter sur les mérites professionnels de candidats n'appartenant pas à leur entourage professionnel. Si M. D...remet en cause le choix des rapporteurs chargés d'examiner sa candidature, la circonstance que l'un des rapporteurs ait siégé dans le jury d'habilitation à diriger des recherches du candidat classé premier par le comité de sélection, ne saurait établir, à elle seule, son manque d'impartialité. En l'espèce, il ne ressort d'ailleurs pas des pièces du dossier que les deux rapporteurs désignés aient eu une attitude discriminatoire à l'égard de ce candidat.<br/>
<br/>
              6. D'autre part, si M. D...fait valoir que le candidat classé premier par le comité de sélection a participé à l'organisation de colloques et conférences initiés par M. Douzou et qu'il est intervenu dans des séminaires auxquels ont participé certains des membres du comité de sélection, ces circonstances ne suffisent pas à caractériser une collaboration scientifique étroite de nature à faire obstacle à ce que ces membres participent régulièrement au comité de sélection. Par suite, le moyen tiré de ce que le comité de sélection aurait été irrégulièrement composé ne peut qu'être écarté.<br/>
<br/>
              7. Il résulte de tout ce qui précède, et sans qu'il soit besoin de statuer sur la fin de non-recevoir présentée par l'Institut d'études politiques de Lyon, que M. D...n'est pas fondé à demander l'annulation de la délibération du comité de sélection qu'il attaque, ni l'annulation de la délibération du conseil d'administration de l'Institut d'études politiques de Lyon, des décisions du directeur de cet Institut et du décret du Président de la République qu'il demande par voie de conséquence. Sa requête doit, par suite être rejetée, y compris, ses conclusions à fins d'injonction et ses conclusions présentées au titre de l'article L.761-1 du code de justice administrative. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par l'Institut d'études politiques de Lyon au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. D...est rejetée. <br/>
<br/>
Article 2 : Les conclusions de l'Institut d'études politiques de Lyon présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. C...D..., à l'Institut d'études politiques de Lyon, à M. A...E..., à la ministre de l'enseignement supérieur, de la recherche et de l'innovation et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
