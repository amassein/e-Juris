<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000020377614</ID>
<ANCIEN_ID>JG_L_2009_03_000000314792</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/20/37/76/CETATEXT000020377614.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 03/03/2009, 314792, Publié au recueil Lebon</TITRE>
<DATE_DEC>2009-03-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>314792</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Stirn</PRESIDENT>
<AVOCATS>SCP LYON-CAEN, FABIANI, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Jérôme  Marchand-Arvier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Lenica Frédéric</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2009:314792.20090303</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 2 avril 2008 et 30 juin 2008 au secrétariat du contentieux du Conseil d'Etat, présentés pour l'ASSOCIATION FRANÇAISE CONTRE LES MYOPATHIES, dont le siège est Institut de Myologie 47-89, boulevard de l'Hôpital à Paris Cedex 13 (75651) ; l'ASSOCIATION FRANÇAISE CONTRE LES MYOPATHIES demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêté du 18 janvier 2008 relatif à la mise en accessibilité des véhicules de transport public guidé urbain aux personnes handicapées et à mobilité réduite ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'action sociale et des familles ;<br/>
<br/>
              Vu la loi n° 2005-102 du 11 février 2005 ;<br/>
<br/>
              Vu le décret n° 2006-138 du 9 février 2006 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jérôme Marchand-Arvier, Auditeur,  <br/>
<br/>
              - les observations de la SCP Lyon-Caen, Fabiani, Thiriez, avocat de l'ASSOCIATION FRANÇAISE CONTRE LES MYOPATHIES, <br/>
<br/>
              - les conclusions de M. Frédéric Lenica, rapporteur public ;<br/>
<br/>
              - les nouvelles observations de la SCP Lyon-Caen, Fabiani, Thiriez, avocat de l'ASSOCIATION FRANÇAISE CONTRE LES MYOPATHIES,<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Sur la légalité externe de l'arrêté attaqué :<br/>
<br/>
              Considérant qu'aux termes de l'article 45 de la loi du 11 février 2005 pour l'égalité des droits et des chances, la participation et la citoyenneté des personnes handicapées : " I.- La chaîne du déplacement, qui comprend le cadre bâti, la voirie, les aménagements des espaces publics, les systèmes de transport et leur intermodalité, est organisée pour permettre son accessibilité dans sa totalité aux personnes handicapées ou à mobilité réduite. Dans un délai de dix ans à compter de la date de publication de la présente loi, les services de transport collectif devront être accessibles aux personnes handicapées et à mobilité réduite. (...) II- Tout matériel roulant acquis lors d'un renouvellement de matériel ou à l'occasion de l'extension des réseaux doit être accessible aux personnes handicapées ou à mobilité réduite. Des décrets préciseront, pour chaque catégorie de matériel, les modalités d'application de cette disposition. (...) " ; qu'aux termes de l'article 2 du décret du 9 février 2006 relatif à l'accessibilité du matériel roulant affecté aux services de transport public terrestre de voyageurs : " (...) Les dispositions et aménagements propres à assurer l'accessibilité du matériel roulant doivent satisfaire aux obligations suivantes : 1. S'il subsiste entre le véhicule ou la rame et le trottoir ou le quai des lacunes horizontales ou verticales non franchissables, elles sont comblées grâce à l'ajout d'équipements ou de dispositifs adéquats, à quai ou embarqués (...) " ; qu'aux termes de l'article 4 de ce décret : " Des arrêtés pris par le ministre chargé des transports et les ministres intéressés précisent au plus tard un an après la parution du présent décret, pour chaque catégorie de matériel roulant mentionnée à l'article 1er, les dispositions à respecter et les équipements spécifiques à mettre en place pour assurer l'accessibilité dudit matériel roulant (...) " ; <br/>
<br/>
              Considérant que, dans sa requête sommaire, enregistrée le 2 avril 2008 au secrétariat du contentieux du Conseil d'Etat, l'association requérante avait soulevé des moyens tenant à la fois à la légalité externe et à la légalité interne de l'arrêté qu'elle attaque ; que, par suite, contrairement à ce que soutient le ministre, elle était recevable à soulever, dans le mémoire complémentaire annoncé dans sa requête, le moyen tiré de l'absence de signature du ministre du travail, des relations sociales, de la famille et des solidarités, moyen, qui, relatif à la compétence des auteurs de l'arrêté attaqué, est, en outre, d'ordre public ;<br/>
<br/>
              Considérant que l'arrêté attaqué, qui a été pris en application du décret du 9 février 2006, définit les règles techniques pour garantir l'accessibilité des véhicules de transport public guidé urbain aux personnes handicapées et à mobilité réduite ; qu'il ne pouvait être légalement édicté sous la seule signature du ministre de l'écologie, du développement et de l'aménagement durables, mais nécessitait, conformément aux dispositions précitées de l'article 4 du décret du 9 février 2006, l'intervention conjointe de ce ministre et du ministre chargé des personnes handicapées, ministre intéressé ; que l'association requérante est, par suite, fondée à demander l'annulation pour incompétence de l'arrêté du 18 janvier 2008 ; <br/>
<br/>
              Sur les conséquences de l'illégalité de l'arrêté attaqué :<br/>
<br/>
              Considérant que, contrairement à ce qui est soutenu dans la requête, il ne ressort pas des pièces du dossier que la fixation des dimensions maximales des lacunes horizontales et verticales entre le nez de quai et le seuil des portes accessibles identifiées par le symbole international au niveau de 50 millimètres, pour du matériel neuf, à vide, positionné en ligne droite, centré dans l'axe de la voie et par rapport à un nez de quai théorique défini par le système de transport, soit entaché d'une erreur manifeste d'appréciation ; qu'ainsi, l'arrêté du 18 janvier 2008 n'est pas entaché d'illégalité interne du seul fait qu'il fixe les dimensions maximales des lacunes à ce niveau ; que, dans ces conditions, compte tenu de l'intérêt général qui s'attache à l'existence de dispositions de nature à assurer l'accessibilité des véhicules de transport public guidé urbain aux personnes handicapées et à mobilité réduite, il y a lieu, dans les circonstances de l'espèce, de différer l'effet de l'annulation de l'arrêté attaqué jusqu'au 1er septembre 2009 ; que, pour l'application du décret du 9 février 2006 relatif à l'accessibilité du matériel roulant affecté aux services de transport public terrestre de voyageurs, il appartient aux ministres chargés des transports et des personnes handicapées de prendre à nouveau, avant la date qui vient d'être fixée d'effet de l'annulation de l'arrêté attaqué, les mêmes dispositions ou, le cas échéant après consultation du Conseil national consultatif des personnes handicapées, d'édicter une réglementation différente ;<br/>
<br/>
              Sur les conclusions de l'ASSOCIATION FRANÇAISE CONTRE LES MYOPATHIES tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de faire application des dispositions de l'article L. 761-1 du code de justice administrative et de mettre à la charge de l'Etat le versement à l'ASSOCIATION FRANÇAISE CONTRE LES MYOPATHIES de la somme de 2 000 euros ;<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêté du 18 janvier 2008 relatif à la mise en accessibilité des véhicules de transport public guidé urbain aux personnes handicapées et à mobilité réduite est annulé. Cette annulation prendra effet le 1er septembre 2009.<br/>
Article 2 : L'Etat versera à l'ASSOCIATION FRANÇAISE CONTRE LES MYOPATHIES une somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à l'ASSOCIATION FRANÇAISE CONTRE LES MYOPATHIES et au ministre d'Etat, ministre de l'écologie, de l'énergie, du développement durable et de l'aménagement du territoire.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-02-02-01-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE AUTORITÉS DISPOSANT DU POUVOIR RÉGLEMENTAIRE. AUTORITÉS DISPOSANT DU POUVOIR RÉGLEMENTAIRE. MINISTRES. - ARRÊTÉ PRIS EN APPLICATION DU DÉCRET DU 9 FÉVRIER 2006 RELATIF À L'ACCESSIBILITÉ DU MATÉRIEL ROULANT AFFECTÉ AUX SERVICES DE TRANSPORT PUBLIC TERRESTRE DE VOYAGEURS, DÉFINISSANT LES RÈGLES TECHNIQUES D'ACCESSIBILITÉ À CE MATÉRIEL POUR LES PERSONNES À MOBILITÉ RÉDUITE - COMPÉTENCE DU MINISTRE CHARGÉ DE L'ÉCOLOGIE - EXISTENCE - COMPÉTENCE DU MINISTRE CHARGÉ DES PERSONNES HANDICAPÉES - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-023 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. - CONDITIONS [RJ1] - MOTIF D'INTÉRÊT GÉNÉRAL S'ATTACHANT À LA RÉGLEMENTATION ANNULÉE.
</SCT>
<ANA ID="9A"> 01-02-02-01-03 L'arrêté du 18 janvier 2008 relatif à la mise en accessibilité des véhicules de transport public guidé urbain aux personnes handicapées et à mobilité réduite, qui a été pris en application du décret n° 2006-138 du 9 février 2006 relatif à l'accessibilité du matériel roulant affecté aux services de transport public terrestre de voyageurs, définit les règles techniques pour garantir l'accessibilité des véhicules de transport public guidé urbain aux personnes handicapées et à mobilité réduite. Il ne pouvait être légalement édicté sous la seule signature du ministre de l'écologie, du développement et de l'aménagement durables, mais nécessitait, conformément aux dispositions de l'article 4 du décret du 9 février 2006, l'intervention conjointe de ce ministre et du ministre chargé des personnes handicapées, ministre intéressé.</ANA>
<ANA ID="9B"> 54-07-023 Compte tenu de l'intérêt général qui s'attache à l'existence de dispositions de nature à assurer l'accessibilité des véhicules de transport public guidé urbain aux personnes handicapées et à mobilité réduite, il y a lieu de différer l'effet de l'annulation de l'arrêté du 18 janvier 2008 relatif à la mise en accessibilité des véhicules de transport public guidé urbain aux personnes handicapées et à mobilité réduite jusqu'au 1er septembre 2009. Pour l'application du décret n° 2006-138 du 9 février 2006 relatif à l'accessibilité du matériel roulant affecté aux services de transport public terrestre de voyageurs, il appartient aux ministres chargés des transports et des personnes handicapées de prendre à nouveau, avant la date qui vient d'être fixée d'effet de l'annulation de l'arrêté attaqué, les mêmes dispositions ou, le cas échéant après consultation du Conseil national consultatif des personnes handicapées, d'édicter une réglementation différente.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. Assemblée, 11 mai 2004, Association AC ! et autres, n°s 255886 à 255892, p. 197.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
