<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031529588</ID>
<ANCIEN_ID>JG_L_2015_11_000000378004</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/52/95/CETATEXT000031529588.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème / 8ème SSR, 25/11/2015, 378004, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-11-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>378004</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème / 8ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>M. Christophe Pourreau</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:378004.20151125</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. A... B...a demandé au tribunal administratif de Montpellier de le décharger des cotisations d'impôt sur le revenu et de contributions sociales afférentes à la plus-value tirée de la cession de 1 555 actions de la société Etablissement B...réalisée le 2 septembre 2003, à concurrence d'une réduction de base de 1 751 465 euros.<br/>
<br/>
              Par un jugement n° 0800250 du 18 novembre 2010, le tribunal administratif de Montpellier, après avoir jugé qu'il n'y avait pas lieu de statuer sur la demande de M. B... à hauteur de 12 860 euros, a rejeté le surplus des conclusions de sa demande.<br/>
<br/>
              Par un arrêt n° 11MA00273 du 18 février 2014, la cour administrative d'appel de Marseille a rejeté l'appel formé par M. B... contre ce jugement en tant qu'il n'a pas fait entièrement droit à sa demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 18 avril, 18 juillet 2014 et 22 septembre 2015 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;	<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christophe Pourreau, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, avocat de M. A...B...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, le 2 septembre 2003, M. A... B...a cédé 1 555 actions de la société Etablissement B...qu'il avait reçues soit en contrepartie de son apport lors de la création de la société et de sa participation à des augmentations de capital, soit à la suite de diverses donations et notamment, pour 539 d'entre elles, à la suite de quatre dons manuels effectués par sa mère en mars 1994, juillet 1997, mai 1998 et juillet 2003 ; qu'à la suite de cette opération, il a déclaré une plus-value d'un montant total de 8 149 802 euros, résultant de l'écart entre un prix unitaire de cession de 5 286,76 euros et un prix unitaire d'acquisition de 45,73 euros ; que, le 27 décembre 2005, il a souscrit une déclaration de don manuel des 539 actions de la société reçues de sa mère, pour un montant total de 2 849 564 euros, correspondant également à une valeur unitaire de 5 286,76 euros ; que, le 26 décembre 2006, il a demandé à être déchargé des cotisations d'impôt sur le revenu et de contributions sociales afférentes à la plus-value réalisée le 2 septembre 2003 ; que l'administration a rejeté la réclamation de M. B... ; que, le 21 janvier 2008, M. B... a demandé au tribunal administratif de Montpellier de le décharger des cotisations d'impôt sur le revenu et de contributions sociales afférentes à la plus-value réalisée le 2 septembre 2003 à concurrence d'une réduction de base de 1 751 465 euros ; que, par un jugement du 18 novembre 2010, le tribunal administratif, après avoir jugé qu'il n'y avait pas lieu de statuer sur la demande de M. B... à hauteur d'un dégrèvement de 12 860 euros décidé par l'administration, a rejeté le surplus des conclusions de sa demande ; que M. B... se pourvoit en cassation contre l'arrêt du 18 février 2014 par lequel la cour administrative d'appel de Marseille a rejeté l'appel qu'il a formé contre ce jugement en tant qu'il n'a pas fait entièrement droit à sa demande ;<br/>
<br/>
              2. Considérant, en premier lieu, qu'aux termes du premier alinéa du 1 du I de l'article 150-0 A du code général des impôts, dans sa rédaction applicable à l'opération litigieuse : " (...) les gains nets retirés des cessions à titre onéreux (...) de valeurs mobilières, de droits sociaux (...) sont soumis à l'impôt sur le revenu lorsque le montant de ces cessions excède, par foyer fiscal, 15 000 euros par an " ; qu'aux termes du 1 de l'article 150-0 D du même code, dans sa rédaction applicable à l'opération litigieuse : " Les gains nets mentionnés au I de l'article 150-0 A sont constitués par la différence entre le prix effectif de cession des titres ou droits, net des frais et taxes acquittés par le cédant, et leur prix effectif d'acquisition par celui-ci ou, en cas d'acquisition à titre gratuit, leur valeur retenue pour la détermination des droits de mutation " ; que le fait générateur de la plus-value est le transfert de propriété des actions, lequel doit être regardé comme réalisé à la date de cession de celles-ci ; que, par suite, le montant de la plus-value doit être apprécié à la date de cession des actions, sans que puissent être invoqués des évènements qui, ne procédant pas de la cession elle-même, sont intervenus postérieurement à cette date ;<br/>
<br/>
              3. Considérant qu'il résulte de ce qui précède que si, pour le calcul de la plus-value taxable lors de la cession des actions, le prix d'acquisition des actions obtenues à titre gratuit doit être fixé à la valeur retenue pour le calcul des droits de mutation en vertu des dispositions du 1 de l'article 150-0 D du code général des impôts, sauf à l'administration d'établir que la valeur retenue pour le calcul des droits de mutation était dépourvue de toute signification, cette valeur ne peut être utilement invoquée dans l'hypothèse où elle a été retenue pour le calcul des droits de mutation dans une déclaration de don manuel déposée après la cession des actions ; que, par suite, c'est sans commettre d'erreur de droit que la cour administrative d'appel, après avoir souverainement relevé que la déclaration de don manuel de 539 actions reçues de sa mère avait été effectuée par M. B...en décembre 2005, soit après la cession de ces titres en septembre 2003, a jugé que la valeur ainsi déclarée n'avait pas à être prise en compte pour le calcul de la plus-value dégagée lors de cette cession ;<br/>
<br/>
              4. Considérant, en deuxième lieu, qu'en relevant, pour déterminer le prix unitaire des actions cédées, d'une part, que M. B... avait accepté que, pour les besoins de la clôture de la succession de son père, décédé le 9 septembre 2001, soit à une date proche de celles de trois des quatre dons manuels effectués par sa mère, la valeur unitaire des actions de la société Etablissements B...fût fixée à 770 euros et, d'autre part, que le 1er juillet 2003, soit le jour du quatrième don manuel, M. B... avait acquis deux actions de la société au prix unitaire de 45,73 euros, la cour administrative d'appel s'est livrée à une appréciation souveraine des pièces du dossier sans les dénaturer ;<br/>
<br/>
              5. Considérant, en troisième lieu, que le moyen tiré de ce que la cour administrative d'appel a dénaturé les pièces du dossier en énonçant que, dans sa réclamation préalable du 26 décembre 2006, M. B... n'avait demandé à être déchargé des cotisations d'impôt sur le revenu et de contributions sociales qu'à concurrence d'une réduction de base de 1 658 222 euros est dirigé contre un motif sans incidence sur le dispositif de l'arrêt attaqué ; qu'il doit, par suite, être écarté ;<br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que le pourvoi de M. B... doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
		Article 1er : Le pourvoi de M. B... est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. A... B...et au ministre des finances et des comptes publics<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
