<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031587374</ID>
<ANCIEN_ID>JG_L_2015_12_000000377262</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/58/73/CETATEXT000031587374.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème / 3ème SSR, 07/12/2015, 377262, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-12-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>377262</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème / 3ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLEZ ; SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>Mme Karin Ciavaldini</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:377262.20151207</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>       Vu la procédure suivante :<br/>
<br/>
              	M. A...B...a demandé au tribunal administratif de Toulouse d'annuler la délibération du conseil municipal de la commune de Saint-Beauzeil en date du 14 décembre 1989 autorisant l'échange d'un tronçon de chemin rural dit " de la Combe " contre un tronçon de terrain situé entre les parcelles A 154 et A 161 et d'ordonner à la commune de Saint-Beauzeil de saisir le juge judiciaire aux fins d'exécution des conséquences du jugement à intervenir, sous astreinte de 150 euros par jour de retard à compter de la notification du jugement. Par un jugement n° 0802980 du 12 avril 2012, le tribunal administratif de Toulouse a fait droit à sa demande. <br/>
<br/>
              	Par un arrêt n° 12BX01461 du 6 février 2014, la cour administrative d'appel de Bordeaux a, sur appel de la commune de Saint-Beauzeil, annulé ce jugement et rejeté la demande de M.B.... <br/>
<br/>
              	Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 7 avril et 7 juillet 2014 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              	1°) d'annuler cet arrêt ;<br/>
<br/>
              	2°) de mettre à la charge de la commune de Saint-Beauzeil la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;	<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Karin Ciavaldini, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boullez, avocat de M. B...et à la SCP Odent, Poulet, avocat de la commune de Saint-Beauzeil ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du premier alinéa de l'article R. 611-7 du code de justice administrative : " Lorsque la décision lui paraît susceptible d'être fondée sur un moyen relevé d'office, le président de la formation de jugement ou, au Conseil d'Etat, la sous-section chargée de l'instruction en informe les parties avant la séance de jugement et fixe le délai dans lequel elles peuvent, sans qu'y fasse obstacle la clôture éventuelle de l'instruction, présenter leurs observations sur le moyen communiqué " ; <br/>
<br/>
              2. Considérant que M. B...a demandé au tribunal administratif de Toulouse l'annulation d'une délibération du conseil municipal de la commune de Saint-Beauzeil (Tarn-et-Garonne) en date du 14 décembre 1989 autorisant l'échange d'un tronçon d'un chemin rural de la commune ; que, par un jugement du 12 avril 2012, le tribunal administratif a fait droit à cette demande ; que, sur appel de la commune, la cour administrative d'appel de Bordeaux, par l'arrêt attaqué du 6 février 2014, a jugé que M. B...ne se prévalait pas d'un intérêt suffisant lui donnant qualité pour agir contre la délibération contestée et a en conséquence annulé le jugement du 12 avril 2012 et rejeté la demande de M. B... ;<br/>
<br/>
              3. Considérant que si le tribunal administratif de Toulouse avait communiqué aux parties, sur le fondement des dispositions citées au point 1, le moyen d'ordre public tiré de l'absence d'intérêt à agir de M. B..., il a expressément jugé que M. B...justifiait d'un intérêt à agir, alors même que la commune de Saint-Beauzeil ne s'était pas approprié ce moyen ; qu'à l'appui de son appel, la commune n'a pas critiqué cette partie du jugement, ni soutenu devant la cour l'irrecevabilité de la demande de M.B..., tirée de son absence d'intérêt à agir ; que, dès lors, la cour ne pouvait se fonder sur cette irrecevabilité pour faire droit à l'appel de la commune et rejeter la demande présentée par M. B... devant le tribunal administratif sans communiquer ce moyen aux parties, ainsi que le prescrivent les dispositions de l'article R. 611-7 du code de justice administrative ; que, par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, M. B...est fondé à demander l'annulation de l'arrêt qu'il attaque ; <br/>
<br/>
              4. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. B...qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Saint-Beauzeil le versement à M. B...de la somme de 1 500 euros au même titre ;<br/>
<br/>
<br/>
<br/>                      D E C I D E :<br/>
                                    --------------<br/>
<br/>
Article 1er : L'arrêt du 6 février 2014 de la cour administrative d'appel de Bordeaux est annulé. <br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Bordeaux.<br/>
<br/>
Article 3 : La commune de Saint-Beauzeil versera à M. B...la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions présentées par la commune de Saint-Beauzeil au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à M. A...B...et à la commune de Saint-Beauzeil.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
