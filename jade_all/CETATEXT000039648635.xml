<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039648635</ID>
<ANCIEN_ID>JG_L_2019_12_000000421487</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/64/86/CETATEXT000039648635.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 19/12/2019, 421487, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-12-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421487</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU ; SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:421487.20191219</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au tribunal administratif de Paris d'annuler, d'une part, la décision du 28 mai 2015, confirmée le 29 mai 2015, et la décision de rejet implicite résultant du silence gardé sur son recours gracieux du 11 juin 2015, par lesquelles l'Assistance Publique - Hôpitaux de Paris (AP-HP) l'a déchargé de ses responsabilités au sein du service de transport des patients du groupement hospitalier Paris centre, d'autre part, la décision implicite par laquelle l'AP-HP a refusé de lui octroyer le bénéfice de la protection fonctionnelle. Par un jugement n° 1513133/2-2 du 7 novembre 2016, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 17PA00135 du 10 avril 2018, la cour administrative d'appel de Paris a rejeté l'appel dirigé par M. A... contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 13 juin et 13 septembre 2018 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'AP-HP la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de M. A... et à la SCP Didier, Pinet, avocat de l'Assistance publique - Hôpitaux de Paris.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. A..., cadre supérieur kinésithérapeute affecté au groupement des hôpitaux universitaires de Paris centre, relevant de l'Assistance Publique - Hôpitaux de Paris (AP-HP), a saisi le tribunal administratif de Paris de conclusions tendant, d'une part, à l'annulation de la décision du 28 mai 2015, confirmée le 29 mai 2015, par laquelle son employeur a mis fin à ses fonctions d'encadrement du service de transport des patients de ce groupement hospitalier pour l'affecter à temps plein dans des fonctions d'encadrement des kinésithérapeutes du pôle gériatrique de l'hôpital Broca qu'il exerçait jusqu'alors à mi-temps, d'autre part, à l'annulation de la décision implicite lui refusant le bénéfice de la protection fonctionnelle. Par un jugement du 7 novembre 2016, le tribunal administratif de Paris a rejeté sa demande. La cour administrative d'appel de Paris a rejeté l'appel qu'il a formé contre ce jugement par un arrêt du 10 avril 2018 contre lequel M. A... se pourvoit en cassation.<br/>
<br/>
              2. Aux termes de l'article R. 611-1 du code de justice administrative : " La requête et les mémoires, ainsi que les pièces produites par les parties, sont déposés ou adressés au greffe. / La requête, le mémoire complémentaire annoncé dans la requête et le premier mémoire de chaque défendeur sont communiqués aux parties avec les pièces jointes dans les conditions prévues aux articles R. 611-3, R. 611-5 et R. 611-6. / Les répliques, autres mémoires et pièces sont communiqués s'ils contiennent des éléments nouveaux ". Aux termes du premier alinéa de l'article R. 613-2 du même code : " Si le président de la formation de jugement n'a pas pris une ordonnance de clôture, l'instruction est close trois jours francs avant la date de l'audience indiquée dans l'avis d'audience prévu à l'article R. 711-2. Cet avis le mentionne ".<br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond que l'instruction de la requête de M. A... devant la cour administrative d'appel de Paris, close par une ordonnance du 6 février 2018, a été rouverte par une ordonnance du président de la formation de jugement du 6 mars 2018, qui n'a pas fixé de nouvelle date de clôture. Un avis d'audience émis le 7 mars 2018 a fixé l'audience au 27 mars 2018 à 14 heures. Par application des dispositions précitées de l'article R. 613-2 du code de justice administrative, la clôture de l'instruction a ainsi été fixée au 23 mars 2018 à minuit, ainsi que le mentionnait l'avis d'audience. <br/>
<br/>
              4. Il suit de là qu'en mentionnant, dans les visas de son arrêt, que l'ordonnance du 6 mars 2018 avait fixé la nouvelle clôture de l'instruction au 20 mars 2018 à 20 heures, la cour en a méconnu la portée et qu'en se bornant, par suite, à viser sans l'analyser le mémoire produit par M. A... le 23 mars 2018, au motif qu'il aurait été déposé postérieurement à la clôture de l'instruction, elle a entaché d'irrégularité la procédure suivie devant elle. M. A... est dès lors fondé, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, à demander l'annulation de l'arrêt attaqué. Dans les circonstances de l'espèce, il y a lieu de faire application des dispositions de l'article L. 761-1 du code de justice administrative pour mettre à la charge de l'AP-HP la somme de 3 000 euros à verser à M. A.... Ces mêmes dispositions font obstacle à ce qu'une somme soit mise à la charge de M. A..., qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt du 10 avril 2018 de la cour administrative d'appel de Paris est annulé.<br/>
Article 2 : L'affaire est renvoyée devant la cour administrative d'appel de Paris.<br/>
Article 3 : L'AP-HP versera la somme de 3 000 euros à M. A... au titre de l'article L. 761-1 du code de justice administrative. Les conclusions présentées par l'AP-HP au titre des mêmes dispositions sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à M. B... A... et à l'Assistance publique-Hôpitaux de Paris.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
