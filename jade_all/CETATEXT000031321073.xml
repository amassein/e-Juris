<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031321073</ID>
<ANCIEN_ID>JG_L_2015_10_000000367426</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/32/10/CETATEXT000031321073.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème SSJS, 15/10/2015, 367426, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-10-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>367426</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARTHELEMY, MATUCHANSKY, VEXLIARD, POUPOT</AVOCATS>
<RAPPORTEUR>Mme Marie-Gabrielle Merloz</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:367426.20151015</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une décision n° 367426 du 9 avril 2014, le Conseil d'Etat statuant au contentieux a prononcé l'admission des conclusions du pourvoi de la SARL Harmony dirigées contre l'arrêt n° 12PA01742 du 5 février 2013 de la cour administrative d'appel de Paris en tant seulement que cet arrêt a laissé à la charge de la SARL Harmony les pénalités de mauvaise foi qui ont été mises à sa charge en application des dispositions de l'article 1729 du code général des impôts. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Gabrielle Merloz, maître des requêtes,<br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Barthélemy, Matuchansky, Vexliard, Poupot, avocat de la SARL Harmony ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la SARL Harmony, qui exploite un hôtel, a fait l'objet, au titre des exercices clos en 2004, 2005 et 2006, d'une vérification de comptabilité à la suite de laquelle des cotisations supplémentaires d'impôt sur les sociétés et de contributions additionnelles, ainsi que les pénalités correspondantes, ont été mises à sa charge. Par un arrêt du 5 février 2013, la cour administrative d'appel de Paris a rejeté la requête de la SARL Harmony tendant à l'annulation du jugement du 16 février 2012 par lequel le tribunal administratif de Paris avait rejeté sa demande de décharge. Par une décision du 9 avril 2014, le Conseil d'Etat statuant au contentieux a prononcé l'admission des conclusions du pourvoi de la SARL Harmony dirigées contre l'arrêt du 5 février 2013 de la cour administrative d'appel de Paris en tant que, par cet arrêt, la cour a laissé à sa charge les pénalités pour mauvaise foi, qui lui ont été infligées en application des dispositions de l'article 1729 du code général des impôts dans sa rédaction alors applicable. <br/>
<br/>
              2. Pour demander l'annulation du jugement du tribunal administratif de Paris en tant qu'il avait laissé à sa charge les pénalités précitées, la SARL Harmony soutenait qu'en se fondant seulement sur la récurrence des omissions qui lui étaient reprochées et sur l'importance de leur montants ainsi que sur le comportement de ses gérants lors des opérations de contrôle, l'administration n'établissait pas son intention délibérée d'éluder l'impôt. Il ressort de l'arrêt attaqué que la cour a omis de répondre à ce moyen, qui n'était pas inopérant. Dès lors, pour ce motif, son arrêt doit être annulé en tant qu'il statue sur les pénalités pour mauvaise foi mise à la charge de la société requérante en application des dispositions de l'article 1729 du code général des impôts.<br/>
<br/>
              3. Il y a lieu, dans les circonstances de l'espèce, de régler, dans cette mesure, l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              4. Aux termes de l'article 1729 du code général des impôts dans sa rédaction alors applicable : " Lorsque la déclaration ou l'acte mentionné à l'article 1728 font apparaître une base d'imposition ou des éléments servant à la liquidation de l'impôt insuffisants, inexacts ou incomplets, le montant des droits mis à la charge du contribuable est assorti de l'intérêt de retard visé à l'article 1727 et d'une majoration de 40 % si la mauvaise foi de l'intéressé est établie (...) ". Il résulte de ces dispositions que la pénalité pour mauvaise foi a pour seul objet de sanctionner la méconnaissance par le contribuable de ses obligations déclaratives. Pour établir ce manquement, l'administration doit apporter la preuve, d'une part, de l'insuffisance ou du caractère incomplet des déclarations et, d'autre part, de l'intention de l'intéressé d'éluder l'impôt.<br/>
<br/>
              5. Il résulte de l'instruction que, pour justifier les pénalités contestées, l'administration a relevé, d'une part, que des omissions de recettes, allant jusqu'à 60 % des recettes déclarées, avaient été constatées au titre des exercices clos en 2004, 2005 et 2006, tant en matière de taxe sur la valeur ajoutée que d'impôt sur les sociétés, d'autre part, que compte tenu de l'ampleur de l'insuffisance de base imposable déclarée durant ces trois exercices successifs, la société ne pouvait ignorer qu'elle avait méconnu son obligation de comptabiliser toutes ses recettes. Dans les circonstances de l'espèce, c'est dès lors à bon droit que les pénalités prévues à l'article 1729 du code général des impôts ont été appliquées par l'administration fiscale à la SARL Harmony. Cette dernière n'est donc pas fondée à en demander la décharge.<br/>
<br/>
              6. Il résulte de ce qui précède que la société Harmony n'est pas fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Paris a rejeté sa demande de décharge des pénalités de mauvaise foi.<br/>
<br/>
              7. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que la somme demandée par la SARL Harmony au titre de ces dispositions soit mise à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : L'arrêt du 5 février 2013 de la cour administrative d'appel de Paris est annulé en tant qu'il a statué sur les conclusions de la SARL Harmony demandant la décharge des pénalités pour mauvaise foi qui ont été mises à sa charge au titre des exercices clos en 2004, 2005 et 2006.<br/>
Article 2 : Les conclusions de la requête de la SARL Harmony devant la cour administrative d'appel de Paris tendant à la décharge des pénalités pour mauvaise foi sont rejetées.<br/>
Article 3 : Les conclusions de la SARL Harmony au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la SARL Harmony et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
