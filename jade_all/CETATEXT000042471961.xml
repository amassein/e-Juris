<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042471961</ID>
<ANCIEN_ID>JG_L_2020_10_000000440880</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/47/19/CETATEXT000042471961.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 23/10/2020, 440880, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-10-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440880</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Récusation</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLOCHE</AVOCATS>
<RAPPORTEUR>M. Thomas Janicot</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:440880.20201023</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Par un mémoire, enregistré le 8 août 2020 au secrétariat du contentieux du Conseil d'Etat, présenté en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958, M. D... E... demande au Conseil d'Etat, à l'appui de sa demande de récusation, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions des articles L. 132-2, L. 132-4, L. 136-4 et L. 131-7 du code de justice administrative, de l'article L. 952-6 du code de l'éducation et de l'article 25 septies de la loi du 13 juillet 1983 portant droits et obligations des fonctionnaires. <br/>
<br/>
              Il soutient que ces dispositions, applicables au litige, méconnaissent le principe fondamental reconnu par les lois de la République d'indépendance de la justice administrative, le principe d'égalité garanti par l'article 6 de la Déclaration des droits de l'homme et du citoyens, la garantie des droits protégée par l'article 16 de cette déclaration, l'article 34 de la Constitution, le principe fondamental reconnu par les lois de la République d'indépendance des enseignants-chercheurs et les principes d'impartialité et d'égal accès aux emplois publics. <br/>
<br/>
              Par un nouveau mémoire, enregistré le 21 octobre 2020, M. E... indique récuser, en ce qui concerne l'audience relative à sa question prioritaire de constitutionnalité et son jugement, Mme H... C..., rapporteure public auprès de la 3ème chambre de la section du contentieux du Conseil d'Etat, M. Thomas Janicot, rapporteur à la 3ème chambre de la section du contentieux du Conseil d'Etat ainsi que tout autre membre du Conseil d'Etat, sa question prioritaire de constitutionnalité devant, par suite, être renvoyée à la Cour de Cassation ou, subsidiairement, au Tribunal des conflits, ou encore plus subsidiairement, à la Cour européenne des droits de l'homme. <br/>
<br/>
<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son article 61-1 ; <br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code de l'éducation ; <br/>
              - la loi n° 83-634 du 13 juillet 1983 ; <br/>
              - le code de justice administrative, notamment son article R. 771-15 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thomas Janicot, auditeur,  <br/>
<br/>
              - les conclusions de Mme H... C..., rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boulloche, avocat de M. E... ;<br/>
<br/>
              et après en avoir délibéré hors de la présence du rapporteur public<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              Sur la demande de renvoi du jugement de la question prioritaire de constitutionnalité à une autre juridiction : <br/>
<br/>
              1. Les conclusions présentées par M. E... tendant à la récusation de l'ensemble des membres de la section du contentieux soit pour conclure en qualité de rapporteur public sur sa question prioritaire de constitutionnalité, soit pour y statuer doivent être regardées comme tendant, en réalité, au renvoi du jugement de sa question prioritaire de constitutionnalité devant une autre juridiction, ainsi qu'il l'indique d'ailleurs lui-même. Or si tout justiciable est recevable à demander à la juridiction immédiatement supérieure qu'une affaire dont est saisie la juridiction compétente soit renvoyée devant une autre juridiction du même ordre si, pour des causes dont il appartient à l'intéressé de justifier, la juridiction compétente est suspecte de partialité, une telle demande ne peut être mise en oeuvre pour s'opposer à ce que le Conseil d'Etat, qui n'a pas de juridiction supérieure, juge une affaire portée devant lui. Par suite, la demande formée en ce sens par M. E... ne peut, en tout état de cause, qu'être rejetée ainsi que, par voie de conséquence, les conclusions accessoires dont est assortie cette demande.<br/>
<br/>
              Sur la question prioritaire de constitutionnalité : <br/>
<br/>
              2. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions du même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux. <br/>
<br/>
              3. A l'appui de sa demande tendant à la récusation de Mme B... G... et de M. F... A..., M. E... conteste la conformité aux droits et libertés garantis par la Constitution, d'une part, des dispositions des articles L. 131-7, L. 132-2, L. 132-4 et L. 136-4 du code de justice administrative relatives à la composition de la commission supérieure du Conseil d'Etat et à sa saisine, aux sanctions pouvant être prononcées à l'encontre des membres du Conseil d'Etat et aux conditions de leur déclaration d'intérêts, d'autre part, des dispositions de l'article L. 952-6 du code de l'éducation relatives à la qualification et au recrutement des enseignants-chercheurs et, enfin, des dispositions de l'article 25 septies de la loi du 13 juillet 1983 portant droits et obligations des fonctionnaires relatives aux conditions dans lesquelles les fonctionnaires peuvent exercer une activité privée lucrative. D'une part, aucun article L. 132-4 ne figure au code de justice administrative. D'autre part, il n'est pas fait application dans le cadre de la procédure de demande de récusation, dont le seul objet est de déterminer, au regard des motifs invoqués par le demandeur, s'il existe une raison sérieuse de mettre en doute l'impartialité du membre dont il est demandé la récusation, des autres dispositions mentionnées ci-dessus, qui ne peuvent donc être regardées comme applicables au litige ou à la procédure au sens et pour l'application de l'article 23-5 de l'ordonnance du 7 novembre 1958.<br/>
<br/>
              4. Il résulte de ce qui précède qu'il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M. E....<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La demande de renvoi pour cause de suspicion légitime présentée par M. E... est rejetée.<br/>
Article 2 : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M. E....<br/>
Article 3 : La présente décision sera notifiée à M. D... E.... <br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre, au garde des sceaux, ministre de la justice et à la ministre de l'enseignement supérieur, de la recherche et de l'innovation.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
