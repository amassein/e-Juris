<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042659660</ID>
<ANCIEN_ID>JG_L_2020_12_000000436660</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/65/96/CETATEXT000042659660.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 10/12/2020, 436660, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>436660</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUIN-PALAT, BOUCARD ; BALAT</AVOCATS>
<RAPPORTEUR>M. Edouard Solier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Raphaël Chambon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:436660.20201210</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au tribunal administratif de Dijon de condamner l'université de Bourgogne à lui verser la somme de 5 000 euros en réparation du préjudice moral qu'il estime avoir subi en raison d'un manquement de la part de celle-ci à ses obligations contractuelles. Par un jugement n° 1602252 du 28 décembre 2017, la tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par une ordonnance n° 18LY00982 du 5 décembre 2019, enregistrée le 11 décembre 2019 au secrétariat du contentieux du Conseil d'Etat, le président de la cour administrative d'appel de Lyon a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi formé par M. A... contre ce jugement.<br/>
<br/>
              Par ce pourvoi, enregistré au greffe de la cour administrative d'appel de Lyon le 2 mars 2018, et un mémoire complémentaire enregistré le 15 juin 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'université de Bourgogne la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Edouard Solier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Thouin-Palat, Boucard, avocat de M. A... et à Me Balat, avocat de l'université de Bourgogne ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. A... est professeur des universités en biologie végétale à l'université de Bourgogne, affecté à l'Institut universitaire de la vigne et du vin (IUVV). Ses conditions d'accueil, d'hébergement et de recherche au sein de l'IUVV ainsi que les moyens financiers qui lui sont affectés ont été précisés par une convention conclue le 1er novembre 2013. Par une décision du 17 mai 2016, le président de l'université de Bourgogne a refusé la demande de M. A... tendant à l'accueil, dans le cadre d'un stage, d'une étudiante préparant une thèse dans une université étrangère. À la suite de cette décision, M. A... a demandé au tribunal administratif de Dijon de condamner l'université de Bourgogne à lui verser une somme de 5 000 euros en réparation du préjudice moral qu'il estime avoir subi. Par un jugement du 28 décembre 2017, contre lequel M. A... se pourvoit en cassation, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              2. Pour rejeter la demande de M. A..., le tribunal administratif de Dijon a retenu que ses conclusions dirigées contre la décision du 17 mai 2016 n'étaient pas recevables, dès lors que cette décision a le caractère d'une mesure d'ordre intérieur, insusceptible de faire l'objet d'un recours pour excès de pouvoir. Il en a déduit que les conclusions indemnitaires de M. A..., " qui se fondent uniquement sur l'illégalité de la décision " du 17 mai 2016, ne pouvaient qu'être rejetées.  Il ressort toutefois des écritures présentées devant les juges du fond que, d'une part, M. A... ne demandait pas l'annulation de la décision du 17 mai 2016 et, d'autre part, qu'il soutenait notamment à l'appui de sa demande indemnitaire, que le président de l'université de Bourgogne avait méconnu tant les stipulations de l'article 7 de la convention du 1er novembre 2013 que les statuts de l'université de Bourgogne. Par suite, en s'estimant saisi notamment d'un litige d'excès de pouvoir et non uniquement d'une demande indemnitaire, le tribunal administratif de Dijon s'est mépris sur la portée des conclusions et des moyens dont il était saisi. Le jugement attaqué doit donc être annulé, sans qu'il soit besoin d'examiner les autres moyens du pourvoi de M. A....<br/>
<br/>
              3. Il n'y a pas lieu, dans les circonstances de l'espèce de faire droit aux conclusions présentées par M. A... au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              4. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de M. A... qui n'est pas, dans la présente, la partie perdante. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Dijon du 28 décembre 2017 est annulé.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Dijon.<br/>
Article 3 : Les conclusions présentées par M. A... et par l'université de Bourgogne au titre de l'article L. 761-1 du code de justice administrative sont rejetées<br/>
Article 4 : La présente décision sera notifiée à M. B... A... et à l'université de Bourgogne.<br/>
Copie en sera adressée à la ministre de l'enseignement supérieur, de la recherche et de l'innovation.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
