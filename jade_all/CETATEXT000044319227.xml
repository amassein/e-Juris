<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044319227</ID>
<ANCIEN_ID>JG_L_2021_11_000000449124</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/31/92/CETATEXT000044319227.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 10/11/2021, 449124, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-11-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>449124</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Audrey Prince</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Marc Pichon de Vendeuil</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:449124.20211110</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B... A... a demandé au tribunal administratif de Limoges d'annuler l'arrêté du 21 janvier 2019 lui concédant une pension de retraite en tant qu'il ne lui accorde pas la bonification du cinquième du temps de service prévue par les dispositions du i) de l'article L. 12 du code des pensions civiles et militaires de retraite.<br/>
<br/>
              Par un jugement n° 1900274 du 3 décembre 2020, le tribunal administratif de Limoges a annulé l'arrêté du 21 janvier 2019 concédant une pension de retraite à Mme A... en tant qu'il ne lui accorde pas la bonification du cinquième du temps de service pour ses périodes de congés maternité, parental et convenances personnelles.<br/>
<br/>
              Par un pourvoi, enregistré le 27 janvier 2021 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'économie, des finances et de la relance demande au Conseil d'Etat d'annuler ce jugement en tant qu'il a fait droit à la demande de Mme A... au titre des périodes de congé parental et de congé pour convenances personnelles. Il soutient que le tribunal administratif de Limoges a commis une erreur de droit et inexactement qualifié les faits en jugeant que les périodes de congé parental et de congé pour convenances personnelles devaient être regardées comme des services militaires effectifs ouvrant droit à la bonification du cinquième du temps de service prévue par les dispositions du i) de l'article L. 12 du code des pensions civiles et militaires de retraite.<br/>
<br/>
              Le pourvoi a été communiqué à Mme A... et à la ministre des armées qui n'ont pas produit de mémoire.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la défense ;<br/>
              - le code des pensions civiles et militaires de retraite ;<br/>
- le code de justice administrative <br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Audrey Prince, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Marc Pichon de Vendeuil, rapporteur public ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des énonciations du jugement attaqué que Mme A... est titulaire d'une pension militaire de retraite qui lui a été concédée par arrêté du 21 janvier 2019. Elle a demandé au tribunal administratif de Limoges d'annuler cet arrêté en tant qu'il n'incluait pas dans sa pension, faute notamment de tenir compte de périodes de congé maternité, parental et de congé pour convenances personnelles pour élever un enfant, la bonification du cinquième de son temps de service qui est accordée à tous les militaires qui ont accompli au moins dix-sept ans de services effectifs, en vertu du i) de l'article L. 12 du code des pensions civiles et militaires de retraite. Le ministre de l'économie, des finances et de la relance se pourvoit en cassation contre le jugement du 3 décembre 2020 du tribunal administratif de Limoges en tant qu'il a fait droit à cette demande au titre des périodes de congé parental et de congé pour convenances personnelles.<br/>
<br/>
              2. Aux termes de l'article L. 12 du code des pensions civiles et militaires de retraite : " Aux services effectif s'ajoutent, dans des conditions déterminées par décret en Conseil d'Etat, les bonifications ci-après : / ( ...) ; i) Une bonification  du  cinquième du temps de service  accompli est accordée dans la limite de cinq annuités à tous les militaires à la condition qu'ils aient accompli au moins dix-sept ans de services militaires effectifs ou qu'ils aient été rayés des cadres pour invalidité (...) Le temps passé en congé de longue durée pour maladie et en congé de longue maladie est assimilé à des services militaires effectifs. Les services accomplis dans la réserve opérationnelle durant un congé pour convenances personnelles pour élever un enfant de moins de huit ans sont pris en compte (...) ". Et aux termes du premier alinéa de l'article R. 25-1 du même code : " La bonification prévue au i de l'article L. 12 attribuée dans la limite de vingt trimestres est calculée en fonction des services militaires effectivement accomplis ". <br/>
<br/>
              3. Il résulte de ces dispositions que la bonification qu'elles prévoient est calculée en fonction de la durée des " services militaires effectifs ", lesquels excluent les services accomplis à titre civil et n'incluent par assimilation que les congés limitativement énumérés par ces dispositions. <br/>
<br/>
              4. Pour annuler l'arrêté de concession de pension en tant qu'il n'accordait pas à Mme A... la bonification du cinquième du temps de service au titre des périodes de congé parental et de congé pour convenances personnelles pour élever un enfant de moins de huit ans, le tribunal administratif de Limoges s'est fondé sur la circonstance que ces périodes de congés pouvaient être considérées comme du temps de " service effectif " au sens des articles L. 4138-14 et L. 4138-16 du code de la défense. En en déduisant qu'elles devaient également être assimilées à du temps de " services militaires effectifs " pour l'application des dispositions du i) de l'article L. 12 du code des pensions civiles et militaires de retraite, alors, d'une part, que le congé parental n'est pas au nombre des congés assimilés à des " services militaires effectifs " mentionnés par ces dispositions et, d'autre part, que durant un congé pour convenances personnelles pour élever un enfant de moins de huit ans, seuls les services accomplis dans la réserve opérationnelle sont pris en compte, le tribunal administratif de Limoges a commis une erreur de droit.<br/>
<br/>
              5. Il résulte de ce qui précède que le ministre de l'économie, des finances et de la relance est fondé à demander l'annulation du jugement qu'il attaque en tant qu'il a fait droit à la demande de Mme A... au titre des périodes de congé parental et de congé pour convenances personnelles.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond, dans la mesure de l'annulation prononcée, en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              7. Il résulte de ce qui a été dit au point 4 qu'en tant qu'il porte refus d'accorder à Mme A... la bonification du cinquième du temps de service au titre des périodes de congé parental et de congé pour convenances personnelles pour élever un enfant de moins de huit ans, l'arrêté du 21 janvier 2019 lui concédant une pension de retraite n'est entaché ni d'erreur de fait, ni d'erreur de droit. Par suite, Mme A... n'est pas fondée à en demander l'annulation à ce titre. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 3 décembre 2020 du tribunal administratif de Limoges est annulé en tant qu'il a fait droit à la demande de Mme A... au titre des périodes de congé parental et de congé pour convenances personnelles.<br/>
Article 2 : Le surplus des conclusions de la demande de Mme A... devant le tribunal administratif de Limoges est rejeté.<br/>
Article 3 : La présente décision sera notifiée au ministre de l'économie, des finances et de la relance et à Mme B... A...<br/>
Copie en sera adressée à la ministre des armées.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
