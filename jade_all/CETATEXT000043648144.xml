<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043648144</ID>
<ANCIEN_ID>JG_L_2021_06_000000429498</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/64/81/CETATEXT000043648144.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 09/06/2021, 429498, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-06-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>429498</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET BRIARD</AVOCATS>
<RAPPORTEUR>M. Matias de Sainte Lorette</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Céline  Guibé</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:429498.20210609</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société en nom collectif (SNC) Le Cap a demandé au tribunal administratif de Paris de prononcer la décharge des rappels de taxe sur la valeur ajoutée qui lui ont été réclamés au titre de la période du 1er janvier au 31 octobre 2014 pour un montant total de 8 148 593 euros, y compris les intérêts de retard. Par un jugement n° 1622257 du 23 mai 2018, le tribunal administratif de Paris a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 18PA02398 du 6 février 2019, la cour administrative d'appel de Paris a rejeté son appel formé contre ce jugement.<br/>
<br/>
              Par un pourvoi et un mémoire en réplique, enregistrés les 5 avril 2019 et 27 avril 2020 au secrétariat du contentieux du Conseil d'Etat, la société Le Cap demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) subsidiairement, de renvoyer à la Cour de justice de l'Union Européenne la question préjudicielle suivante : " Dans l'hypothèse de l'acquisition par un marchand de biens d'un immeuble construit depuis plus de cinq ans et loué nu, sous le régime de la taxe sur la valeur ajoutée jusqu'à sa revente, la directive 2006/112/CE du 28 novembre 2006, et notamment son article 168 : - permettent-ils (i) de refuser toute déduction de la taxe sur la valeur ajoutée acquittée sur l'achat de l'immeuble par le marchand de biens, qui réalise des opérations taxables de façon continue, et (ii) de reporter la naissance du droit à déduction de la totalité de cette taxe au moment de la revente dudit immeuble, en subordonnant entièrement et uniquement cette déduction à l'option par le marchand de biens, pour le paiement de la taxe sur la valeur ajoutée sur la revente ' - s'opposent-ils donc à la déduction immédiate, par ce marchand de biens, de la taxe sur la valeur ajoutée qu'il a acquittée sur l'acquisition d'un tel immeuble, alors qu'il a opté pour le paiement de la taxe sur les loyers perçus à compter de cette acquisition ' "<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la directive 2006/112/CE du 28 novembre 2006 ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Matias de Sainte Lorette, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Céline Guibé, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, au Cabinet Briard, avocat de la société en nom collectif (SNC) Le Cap ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société en nom collectif (SNC) Le Cap, qui exerce une activité de marchand de biens, a acquis le 30 janvier 2014 un immeuble achevé depuis plus de cinq ans dont l'acquisition a été assujettie à la taxe sur la valeur ajoutée, conformément à l'exercice par le vendeur de l'option prévue au 5°bis de l'article 260 du code général des impôts. À l'issue d'une vérification de la comptabilité de la SNC Le Cap, l'administration fiscale a estimé que la taxe sur la valeur ajoutée ayant grevé le coût d'acquisition de cet immeuble n'était pas immédiatement déductible et a notifié à la société un rappel de taxe sur la valeur ajoutée au titre de la période du 14 octobre 2013 au 31octobre 2014, assorti des intérêts de retard. Après avoir vainement contesté, la société Le Cap a porté le litige devant le tribunal administratif de Paris qui, par un jugement du 23 mai 2018, a rejeté sa demande. La société Le Cap se pourvoit en cassation contre l'arrêt du 6 février 2019 par lequel la cour administrative d'appel de Paris a rejeté son appel contre ce jugement.<br/>
<br/>
              2. Aux termes de l'article 271 du code général des impôts : " I. 1. La taxe sur la valeur ajoutée qui a grevé les éléments du prix d'une opération imposable est déductible de la taxe sur la valeur ajoutée applicable à cette opération. (...) ". Aux termes de l'article 261 du même code, dans sa rédaction applicable au litige : " Sont exonérés de la taxe sur la valeur ajoutée : / (...) 5. (Opérations immobilières) : / (...) 2° Les livraisons d'immeubles achevés depuis plus de cinq ans (...) ". Aux termes de l'article 260 de ce code : " Peuvent sur leur demande acquitter la taxe sur la valeur ajoutée : / (...) 5° bis Les personnes qui réalisent une opération visée au 5 de l'article 261 (...) ". Aux termes de l'article 201 quater de l'annexe II au même code : " L'option prévue au 5° bis de l'article 260 du code général des impôts s'exerce distinctement par immeuble, fraction d'immeuble ou droit immobilier mentionné au 1 du I de l'article 257 de ce code, relevant d'un même régime au regard des articles 266 et 268 du même code. Il doit être fait mention de cette option dans l'acte constatant la mutation ". Il résulte de la combinaison de ces dispositions que lorsqu'un immeuble achevé depuis plus de cinq ans est acquis en vue de sa revente, la taxe sur la valeur ajoutée ayant éventuellement grevé le prix d'acquisition n'est pas déductible sauf exercice, au moment de la revente, de l'option prévue au 5° bis de l'article 260 du code général des impôts. Par suite, la taxe acquittée lors de l'acquisition du bien n'est pas déductible avant cette date, quand bien même l'immeuble donnerait lieu, dans l'attente de sa revente, à des opérations de location soumises à la taxe sur la valeur ajoutée. <br/>
<br/>
              3. Il résulte de ce qui a été dit au point 2 ci-dessus qu'après avoir relevé, par une appréciation souveraine exempte de dénaturation, que la société Le Cap avait acquis, dans le cadre de son activité de marchand de biens, l'immeuble en litige dans l'intention de le revendre, la cour n'a pas commis d'erreur de droit, ni insuffisamment motivé sa décision, en jugeant que la taxe sur la valeur ajoutée acquittée lors de l'acquisition de cet immeuble ne pouvait être déduite qu'au moment de sa revente en cas d'exercice de l'option prévue au 5° bis de l'article 260 du code général des impôts, et non immédiatement sur la taxe collectée afférente aux loyers perçus dans l'attente de cette revente, en l'absence de lien direct et immédiat entre l'achat de l'immeuble et l'activité intercalaire de location.<br/>
<br/>
              4. Il résulte de ce qui précède que la société Le Cap n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque. Ses conclusions tendant au bénéfice des dispositions de l'article L. 761-1 du code de justice administrative doivent, par voie de conséquence, également être rejetées.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société Le Cap est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la société en nom collectif Le Cap et au ministre de l'économie, des finances et de la relance. <br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
