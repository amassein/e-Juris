<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041647203</ID>
<ANCIEN_ID>JG_L_2020_02_000000436176</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/64/72/CETATEXT000041647203.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 26/02/2020, 436176</TITRE>
<DATE_DEC>2020-02-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>436176</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>Mme Mélanie Villiers</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:436176.20200226</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme C... B... a demandé au tribunal administratif de Paris, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, d'une part, de suspendre l'exécution de la décision implicite par laquelle la ministre des armées a refusé de faire droit à sa demande de protection fonctionnelle et à celle de sa fille, Mme A... B..., d'autre part, d'enjoindre à la ministre de prendre toute mesure de nature à assurer leur sécurité immédiate, de leur accorder un visa pour la France et de prendre en charge leurs frais de voyage pour la France, dans un délai de deux semaines à compter de la notification de l'ordonnance à intervenir, sous astreinte de 200 euros par jour de retard, ou, à titre subsidiaire, de lui enjoindre de réexaminer leur demande de protection fonctionnelle dans le même délai. <br/>
<br/>
              Par une ordonnance n° 1923128 du 5 novembre 2019, le juge des référés du tribunal administratif de Paris a rejeté cette demande, après lui avoir accordé le bénéfice de l'aide juridictionnelle provisoire et admis l'intervention de l'association des interprètes et auxiliaires afghans de l'armée française.<br/>
<br/>
              Par un pourvoi et un mémoire en réplique, enregistrés les 25 novembre 2019 et 7 janvier 2020, au secrétariat du contentieux du Conseil d'Etat, Mme B... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros, à verser à la SCP Rocheteau et Uzan-Sarano, son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - le code de la défense ;<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Mélanie Villiers, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de Mme B... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
<br/>
              2. Il ressort des pièces du dossier soumis au juge des référés que Mme C... B..., ressortissante afghane née le 4 décembre 1956, est la mère de M. D... B..., qui a exercé entre 2003 et 2012 les fonctions d'interprète auprès des forces françaises alors déployées en Afghanistan, en qualité de personnel civil de recrutement local, et qui bénéficie d'une carte de résident en France depuis le 16 mars 2016 au titre de la protection fonctionnelle du fait de ses anciennes fonctions. Mme B... a sollicité l'octroi de cette même protection pour elle-même et pour sa fille, Mme A... B..., par courrier adressé à la ministre des armées le 5 juillet 2019, en raison des menaces dont elle estime faire l'objet du fait des anciennes fonctions de son fils. Elle a demandé au juge des référés du tribunal administratif de Paris, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de la décision par laquelle la ministre des armées a implicitement refusé de faire droit à cette demande de protection fonctionnelle, assortie d'une injonction tendant, à titre principal, à ce qu'il leur soit accordé la protection fonctionnelle, une mise en sécurité immédiate, un visa pour la France et la prise en charge de l'ensemble de leurs frais de voyage vers la France et, à titre subsidiaire, à ce que leur demande de protection fonctionnelle soit réexaminée. Mme B... se pourvoit en cassation contre l'ordonnance du 5 novembre 2019 en tant que le juge des référés de ce tribunal a rejeté cette demande.<br/>
<br/>
              Sur le pourvoi :<br/>
<br/>
              3. Il résulte d'un principe général du droit que, lorsqu'un agent public est mis en cause par un tiers à raison de ses fonctions, il incombe à la collectivité dont il dépend de le couvrir des condamnations civiles prononcées contre lui, dans la mesure où une faute personnelle détachable du service ne lui est pas imputable, de lui accorder sa protection dans le cas où il fait l'objet de poursuites pénales, sauf s'il a commis une faute personnelle, et, à moins qu'un motif d'intérêt général ne s'y oppose, de le protéger contre les menaces, violences, voies de fait, injures, diffamations ou outrages dont il est l'objet. Ce principe général du droit s'étend aux agents non-titulaires de l'Etat recrutés à l'étranger, alors même que leur contrat est soumis au droit local. La juridiction administrative est compétente pour connaître des recours contre les décisions des autorités de l'Etat refusant aux intéressés le bénéfice de cette protection.<br/>
<br/>
              4. Lorsqu'il s'agit, compte tenu de circonstances très particulières, du moyen le plus approprié pour assurer la sécurité d'un agent étranger employé par l'Etat, la protection fonctionnelle peut exceptionnellement conduire à la délivrance d'un visa ou d'un titre de séjour à l'intéressé et à sa famille, comprenant son conjoint, son partenaire au titre d'une union civile, ses enfants et ses ascendants directs. <br/>
<br/>
              5. Il ressort des énonciations de l'ordonnance attaquée que, pour estimer que ne paraissait pas de nature à créer un doute sur la légalité de la décision de la ministre des armées rejetant la demande présentée par Mme C... B... au titre de la protection fonctionnelle, à raison des fonctions anciennement exercées par son fils auprès de l'armée française, le moyen tiré de ce que la ministre aurait méconnu le principe général du droit mentionné aux points 3 et 4, le juge des référés du tribunal administratif de Paris s'est fondé sur le motif qu'elle n'établissait pas le caractère personnel, actuel et réel des menaces dont elle se dit faire l'objet. Toutefois, en retenant ce motif, alors qu'il ressort des pièces du dossier que la protection fonctionnelle a été accordée à son fils Abdul Azim B... à raison de ses fonctions, et que cette protection s'étend aux ascendants directs de celui-ci, le juge des référés a entaché son ordonnance d'une erreur de droit. Mme B... est dès lors fondée, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, à demander l'annulation de l'ordonnance qu'elle attaque en tant qu'elle a rejeté le surplus de ses conclusions.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire, dans cette mesure, au titre de la procédure de référé engagée, en application de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              Sur la demande de suspension :<br/>
<br/>
              7. L'urgence justifie que soit prononcée la suspension d'un acte administratif lorsque l'exécution de celui-ci porte de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre. Il ressort des pièces du dossier qu'eu égard aux menaces personnelles, actuelles et réelles dont font l'objet Mme C... B... et sa famille, ainsi qu'en atteste notamment l'assassinat du plus jeune fils de Mme B..., le 2 septembre 2019, devant le domicile familial, la condition d'urgence est remplie.<br/>
<br/>
              8. Il résulte de ce qui a été dit aux points 4 et 5 que le moyen tiré de la méconnaissance du droit à la protection fonctionnelle que sollicite Mme C... B... en tant qu'ascendante directe de son fils Abdul Azim B..., bénéficiaire de la protection fonctionnelle au titre de ses anciennes fonctions auprès des forces armées françaises en Afghanistan, est, en l'état de l'instruction, de nature à faire naître un doute sérieux sur la légalité de la décision implicite par laquelle la ministre des armées a refusé d'accorder à Mme C... B... la protection fonctionnelle qu'elle a sollicitée. <br/>
<br/>
              9. En revanche, ce même moyen n'est pas de nature à faire naître un doute sérieux sur la légalité de la décision implicite par laquelle la ministre des armées a refusé d'accorder la protection fonctionnelle à Mme A... B..., soeur de M. D... B..., dès lors que celle-ci ne fait pas partie des membres de la famille d'un agent auxquels s'étend la protection mentionnée au point 4. Il en est de même des moyens tirés, d'une part, du défaut d'examen de la situation individuelle de l'intéressée et, d'autre part, de la méconnaissance des dispositions de l'article L. 4123-10 du code de la défense et des stipulations des articles 2 et 3 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. Il appartient à Mme A... B... de solliciter, si elle s'y estime fondée, un visa d'entrée en France auprès des autorités compétentes à raison des menaces dont elle s'estime faire l'objet.<br/>
<br/>
              10. Il résulte de ce qui précède que Mme C... B... est seulement fondée à demander la suspension de l'exécution de la décision implicite de la ministre des armées refusant de lui accorder la protection fonctionnelle. Par suite, il y a lieu, dans les circonstances de l'espèce, d'enjoindre à la ministre de réexaminer la demande de protection fonctionnelle de l'intéressée dans un délai d'un mois à compter de la notification de la présente décision, sans qu'il soit nécessaire de prononcer une astreinte. <br/>
<br/>
              11. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 500 euros à verser à la SCP Rocheteau et Uzan-Sarano, avocat de Mme B..., au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 3 de l'ordonnance du 5 novembre 2019 du juge des référés du tribunal administratif de Paris est annulé. <br/>
Article 2 : L'exécution de la décision implicite par laquelle la ministre des armées a rejeté la demande de protection fonctionnelle de Mme C... B... est suspendue.<br/>
Article 3 : Il est enjoint à la ministre des armées de réexaminer la demande de protection fonctionnelle de Mme C... B... dans un délai d'un mois à compter de la présente décision.<br/>
Article 4 : L'Etat versera la somme de 3 500 euros à la SCP Rocheteau et Uzan-Sarano, avocat de Mme B..., au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 5 : Le surplus des conclusions présentées par Mme B... devant le juge des référés du tribunal administratif de Paris est rejeté.<br/>
Article 6 : La présente décision sera notifiée à Mme C... B... et à la ministre des armées.<br/>
Copie en sera adressée au ministre de l'Europe et des affaires étrangères et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-07-04 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. PRINCIPES INTÉRESSANT L'ACTION ADMINISTRATIVE. GARANTIES DIVERSES ACCORDÉES AUX AGENTS PUBLICS. - PROTECTION FONCTIONNELLE DES AGENTS NON-TITULAIRES DE L'ETAT RECRUTÉS À L'ÉTRANGER - MISE EN &#140;UVRE - DÉLIVRANCE D'UNE AUTORISATION DE SÉJOUR À L'INTÉRESSÉ AINSI QU'À SA FAMILLE [RJ1] - 1) NOTION DE FAMILLE - CONJOINT, PARTENAIRE AU TITRE D'UNE UNION CIVILE, ENFANTS ET ASCENDANTS DIRECTS - 2) PERSONNES NE FAISANT PAS PARTIE DE LA FAMILLE DE L'INTÉRESSÉ - POSSIBILITÉ DE SOLLICITER UN VISA À TITRE HUMANITAIRE, ET NON AU TITRE DE LA PROTECTION FONCTIONNELLE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">335-01-02-02 ÉTRANGERS. SÉJOUR DES ÉTRANGERS. AUTORISATION DE SÉJOUR. OCTROI DU TITRE DE SÉJOUR. - MISE EN &#140;UVRE DE LA PROTECTION FONCTIONNELLE DES AGENTS NON-TITULAIRES DE L'ETAT RECRUTÉS À L'ÉTRANGER - DÉLIVRANCE D'UNE AUTORISATION DE SÉJOUR À L'INTÉRESSÉ AINSI QU'À SA FAMILLE [RJ1] - 1) NOTION DE FAMILLE - CONJOINT, PARTENAIRE AU TITRE D'UNE UNION CIVILE, ENFANTS ET ASCENDANTS DIRECTS - 2) PERSONNES NE FAISANT PAS PARTIE DE LA FAMILLE DE L'INTÉRESSÉ - POSSIBILITÉ DE SOLLICITER UN VISA À TITRE HUMANITAIRE, ET NON AU TITRE DE LA PROTECTION FONCTIONNELLE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">36-07-10-005 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. GARANTIES ET AVANTAGES DIVERS. PROTECTION CONTRE LES ATTAQUES. - PROTECTION FONCTIONNELLE DES AGENTS NON-TITULAIRES DE L'ETAT RECRUTÉS À L'ÉTRANGER - MISE EN &#140;UVRE - DÉLIVRANCE D'UNE AUTORISATION DE SÉJOUR À L'INTÉRESSÉ AINSI QU'À SA FAMILLE [RJ1] - 1) NOTION DE FAMILLE - CONJOINT, PARTENAIRE AU TITRE D'UNE UNION CIVILE, ENFANTS ET ASCENDANTS DIRECTS - 2) PERSONNES NE FAISANT PAS PARTIE DE LA FAMILLE DE L'INTÉRESSÉ - POSSIBILITÉ DE SOLLICITER UN VISA À TITRE HUMANITAIRE, ET NON AU TITRE DE LA PROTECTION FONCTIONNELLE.
</SCT>
<ANA ID="9A"> 01-04-03-07-04 1) Lorsqu'il s'agit, compte tenu de circonstances très particulières, du moyen le plus approprié pour assurer la sécurité d'un agent étranger employé par l'Etat, la protection fonctionnelle peut exceptionnellement conduire à la délivrance d'un visa ou d'un titre de séjour à l'intéressé et à sa famille, comprenant son conjoint, son partenaire au titre d'une union civile, ses enfants et ses ascendants directs.,,,2) Agent étranger employé par l'Etat s'étant vu octroyer une autorisation de séjour au titre de la protection fonctionnelle.,,,Il appartient à sa soeur, qui ne fait pas partie des membres de la famille de cet agent auxquels s'étend la protection fonctionnelle, de solliciter, si elle s'y estime fondée, un visa d'entrée en France auprès des autorités compétentes à raison des menaces dont elle s'estime faire l'objet.</ANA>
<ANA ID="9B"> 335-01-02-02 1) Lorsqu'il s'agit, compte tenu de circonstances très particulières, du moyen le plus approprié pour assurer la sécurité d'un agent étranger employé par l'Etat, la protection fonctionnelle peut exceptionnellement conduire à la délivrance d'un visa ou d'un titre de séjour à l'intéressé et à sa famille, comprenant son conjoint, son partenaire au titre d'une union civile, ses enfants et ses ascendants directs.,,,2) Agent étranger employé par l'Etat s'étant vu octroyer une autorisation de séjour au titre de la protection fonctionnelle.,,,Il appartient à sa soeur, qui ne fait pas partie des membres de la famille de cet agent auxquels s'étend la protection fonctionnelle, de solliciter, si elle s'y estime fondée, un visa d'entrée en France auprès des autorités compétentes à raison des menaces dont elle s'estime faire l'objet.</ANA>
<ANA ID="9C"> 36-07-10-005 1) Lorsqu'il s'agit, compte tenu de circonstances très particulières, du moyen le plus approprié pour assurer la sécurité d'un agent étranger employé par l'Etat, la protection fonctionnelle peut exceptionnellement conduire à la délivrance d'un visa ou d'un titre de séjour à l'intéressé et à sa famille, comprenant son conjoint, son partenaire au titre d'une union civile, ses enfants et ses ascendants directs.,,,2) Agent étranger employé par l'Etat s'étant vu octroyer une autorisation de séjour au titre de la protection fonctionnelle.,,,Il appartient à sa soeur, qui ne fait pas partie des membres de la famille de cet agent auxquels s'étend la protection fonctionnelle, de solliciter, si elle s'y estime fondée, un visa d'entrée en France auprès des autorités compétentes à raison des menaces dont elle s'estime faire l'objet.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 1er février 2019, M.,, n° 421694, p. 13.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
