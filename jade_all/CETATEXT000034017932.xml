<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034017932</ID>
<ANCIEN_ID>JG_L_2017_02_000000403040</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/01/79/CETATEXT000034017932.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Formation spécialisée, 08/02/2017, 403040</TITRE>
<DATE_DEC>2017-02-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>403040</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Formation spécialisée</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Catherine de Salins</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CEFSP:2017:403040.20170208</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée au secrétariat du contentieux du Conseil d'Etat le 31 août 2016, M. B...A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision, révélée par le courrier de la présidente de la Commission nationale de l'informatique et des libertés (CNIL) du 1er juillet 2016, par laquelle le ministre de l'intérieur lui a refusé l'accès aux données susceptibles de le concerner figurant dans le traitement automatisé de données de la direction générale de la sécurité intérieure (DGSI) ;<br/>
<br/>
              2°) d'enjoindre au ministre de l'intérieur de lui communiquer une copie des mentions le concernant qui figurent dans ce fichier ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la sécurité intérieure ;<br/>
              - la loi n° 78-17 du 6 janvier 1978 ; <br/>
              - le décret n° 2005-1309 du 20 octobre 2005 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir convoqué à une séance à huis-clos, d'une part, M. B...A..., et d'autre part, le ministre de l'intérieur et la Commission nationale de l'informatique et des libertés, qui ont été mis à même de prendre la parole avant les conclusions ;<br/>
<br/>
              Et après avoir entendu en séance :<br/>
              - le rapport de Mme Catherine de Salins, conseiller d'Etat, <br/>
              - et, hors la présence des parties, les conclusions de Mme Béatrice <br/>
Bourgeois-Machureau, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article 41 de la loi du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés : " Par dérogation aux articles 39 et 40, lorsqu'un traitement intéresse la sûreté de l'Etat, la défense ou la sécurité publique, le droit d'accès s'exerce dans les conditions prévues par le présent article pour l'ensemble des informations qu'il contient. / La demande est adressée à la commission qui désigne l'un de ses membres appartenant ou ayant appartenu au Conseil d'Etat, à la Cour de cassation ou à la Cour des comptes pour mener les investigations utiles et faire procéder aux modifications nécessaires. Celui-ci peut se faire assister d'un agent de la commission. Il est notifié au requérant qu'il a été procédé aux vérifications. / Lorsque la commission constate, en accord avec le responsable du traitement, que la communication des données qui y sont contenues ne met pas en cause ses finalités, la sûreté de l'Etat, la défense ou la sécurité publique, ces données peuvent être communiquées au requérant. / Lorsque le traitement est susceptible de comprendre des informations dont la communication ne mettrait pas en cause les fins qui lui sont assignées, l'acte réglementaire portant création du fichier peut prévoir que ces informations peuvent être communiquées au requérant par le gestionnaire du fichier directement saisi ". Aux termes de l'article 88 du décret du 20 octobre 2005 pris pour l'application de cette loi : " Aux termes de ses investigations, la commission constate, en accord avec le responsable du traitement, celles des informations susceptibles d'être communiquées au demandeur dès lors que leur communication ne met pas en cause les finalités du traitement, la sûreté de l'Etat, la défense ou la sécurité publique. Elle transmet au demandeur ces informations (...) Lorsque le responsable du traitement s'oppose à la communication au demandeur de tout ou partie des informations le concernant, la commission l'informe qu'il a été procédé aux vérifications nécessaires. / La commission peut constater en accord avec le responsable du traitement, que les informations concernant le demandeur doivent être rectifiées ou supprimées et qu'il y a lieu de l'en informer. En cas d'opposition du responsable du traitement, la commission se borne à informer le demandeur qu'il a été procédé aux vérifications nécessaires. Lorsque le traitement ne contient aucune information concernant le demandeur, la commission informe celui-ci, avec l'accord du responsable du traitement. / En cas d'opposition du responsable du traitement, la commission se borne à informer le demandeur qu'il a été procédé aux vérifications nécessaires. / La réponse de la commission mentionne les voies et délais de recours ouverts au demandeur ".<br/>
<br/>
              2. L'article 26 de la loi du 6 janvier 1978 dispose que : " I. Sont autorisés par arrêté du ou des ministres compétents, pris après avis motivé et publié de la Commission nationale de l'informatique et des libertés, les traitements de données à caractère personnel mis en oeuvre pour le compte de l'Etat et : / 1° Qui intéressent la sûreté de l'Etat, la défense ou la sécurité publique ; (...) / L'avis de la commission est publié avec l'arrêté autorisant le traitement. / II. Ceux de ces traitements qui portent sur des données mentionnées au I de l'article 8 sont autorisés par décret en Conseil d'Etat pris après avis motivé et publié de la commission ; cet avis est publié avec le décret autorisant le traitement. / III. Certains traitements mentionnés au I et au II peuvent être dispensés, par décret en Conseil d'Etat, de la publication de l'acte réglementaire qui les autorise ; pour ces traitements, est publié, en même temps que le décret autorisant la dispense de publication de l'acte, le sens de l'avis émis par la commission (...) ".<br/>
<br/>
              3. L'article L. 841-2 du code de la sécurité intérieure, issu de la loi du 24 juillet 2015 relative au renseignement, dispose que : " Le Conseil d'Etat est compétent pour connaître, dans les conditions prévues au chapitre III bis du titre VII du livre VII du code de justice administrative, des requêtes concernant la mise en oeuvre de l'article 41 de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, pour les traitements ou parties de traitements intéressant la sûreté de l'Etat dont la liste est fixée par décret en Conseil d'Etat ".  L'article R. 841-2 du même code prévoit que : " Relèvent des dispositions de l'article L. 841-2 du présent code les traitements ou parties de traitements automatisés de données à caractère personnel intéressant la sûreté de l'Etat autorisés par les actes réglementaires ou dispositions suivants :/ 1° Décret portant création au profit de la direction générale de la sécurité intérieure d'un traitement automatisé de données personnelles dénommé CRISTINA (...) ".<br/>
<br/>
              4. L'article L. 773-8 du code de justice administrative, issu de la loi du 24 juillet 2015, dispose que : " Lorsqu'elle traite des requêtes relatives à la mise en oeuvre de l'article 41 de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, la formation de jugement se fonde sur les éléments contenus, le cas échéant, dans le traitement sans les révéler ni révéler si le requérant figure ou non dans le traitement. Toutefois, lorsqu'elle constate que le traitement ou la partie de traitement faisant l'objet du litige comporte des données à caractère personnel le concernant qui sont inexactes, incomplètes, équivoques ou périmées, ou dont la collecte, l'utilisation, la communication ou la conservation est interdite, elle en informe le requérant, sans faire état d'aucun élément protégé par le secret de la défense nationale. Elle peut ordonner que ces données soient, selon les cas, rectifiées, mises à jour ou effacées. Saisie de conclusions en ce sens, elle peut indemniser le requérant ". L'article <br/>
R. 773-20 du même code précise que : " Le défendeur indique au Conseil d'Etat, au moment du dépôt de ses mémoires et pièces, les passages de ses productions et, le cas échéant, de celles de la Commission nationale de contrôle des techniques de renseignement, qui sont protégés par le secret de la défense nationale. /Les mémoires et les pièces jointes produits par le défendeur et, le cas échéant, par la Commission nationale de contrôle des techniques de renseignement sont communiqués au requérant, à l'exception des passages des mémoires et des pièces qui, soit comportent des informations protégées par le secret de la défense nationale, soit confirment ou infirment la mise en oeuvre d'une technique de renseignement à l'égard du requérant, soit divulguent des éléments contenus dans le traitement de données, soit révèlent que le requérant figure ou ne figure pas dans le traitement. /Lorsqu'une intervention est formée, le président de la formation spécialisée ordonne, s'il y a lieu, que le mémoire soit communiqué aux parties, et à la Commission nationale de contrôle des techniques de renseignement, dans les mêmes conditions et sous les mêmes réserves que celles mentionnées à l'alinéa précédent ".<br/>
<br/>
              5. Il ressort des pièces du dossier que M. A...a saisi, le 29 avril 2016, la Commission nationale de l'informatique et des libertés (CNIL) afin de pouvoir accéder aux données le concernant contenues notamment dans le fichier de la direction générale de la sécurité intérieure (DGSI). La CNIL a désigné, en application de l'article 41 de la loi du 6 janvier 1978, un membre pour mener toutes investigations utiles et faire procéder, le cas échéant, aux modifications nécessaires. Par lettre du 1er juillet 2016, la présidente de la CNIL a informé M. A...qu'il avait été procédé à l'ensemble des vérifications demandées s'agissant du fichier de la DGSI et que la procédure était terminée, sans apporter à l'intéressé d'autres informations. M. A...doit être regardé comme demandant l'annulation pour excès de pouvoir du refus, révélé par ce courrier, de lui donner accès aux mentions susceptibles de le concerner et figurant dans le fichier de la DGSI. M. A...demande, en outre, qu'il soit enjoint au ministre de l'intérieur de lui communiquer les mentions le concernant figurant dans ce fichier. <br/>
<br/>
              6. Le ministre de l'intérieur ainsi que la CNIL ont communiqué au Conseil d'Etat les éléments relatifs à la situation de l'intéressé. Le ministre a, en outre, communiqué l'acte réglementaire autorisant la création des fichiers de la DGSI.<br/>
<br/>
              7. Il appartient à la formation spécialisée, créée par l'article L. 773-2 du code de justice administrative, saisie de conclusions dirigées contre le refus de communiquer les données relatives à une personne qui allègue être mentionnée dans un fichier figurant à l'article R. 841-2 du code de la sécurité intérieure, de vérifier, au vu des éléments qui lui ont été communiqués hors la procédure contradictoire, si le requérant figure ou non dans le fichier litigieux. Dans l'affirmative, il lui appartient d'apprécier si les données y figurant sont pertinentes au regard des finalités poursuivies par le fichier, adéquates et proportionnées. Lorsqu'il apparaît soit que le requérant n'est pas mentionné dans le fichier litigieux, soit que les données à caractère personnel le concernant qui y figurent ne sont entachées d'aucune illégalité, la formation de jugement rejette les conclusions du requérant sans autre précision. Dans le cas où des informations relatives au requérant figurent dans le fichier litigieux et apparaissent entachées d'illégalité, soit que les données à caractère personnel le concernant soient inexactes, incomplètes, équivoques ou périmées, soit que leur collecte ou leur utilisation, leur communication ou leur conservation est interdite, elle en informe le requérant, sans faire état d'aucun élément protégé par le secret de la défense nationale. Cette circonstance, le cas échéant relevée d'office par le juge dans les conditions prévues à l'article R. 773-21 du code de justice administrative, implique nécessairement que l'autorité gestionnaire du fichier rétablisse la légalité en effaçant ou en rectifiant, dans la mesure du nécessaire, les données litigieuses. En pareil cas, doit être annulée la décision implicite refusant de procéder à un tel effacement ou une telle rectification.<br/>
<br/>
              8. La formation spécialisée a procédé à l'examen de l'acte réglementaire autorisant la création du fichier de la DGSI ainsi que des éléments fournis par le ministre de l'intérieur et la CNIL. Cet examen s'est déroulé selon les modalités décrites au point précédent et n'a révélé aucune illégalité, notamment aucune méconnaissance de l'article 41 de la loi du 6 janvier 1978. Il résulte de cet examen que les conclusions à fin d'annulation de M.A..., qui ne peut utilement invoquer le défaut de motivation de la décision, révélée par le courrier de la CNIL, qu'il attaque, doivent être rejetées. Doivent être rejetées, par voie de conséquence, ses conclusions à fin d'injonction et celles présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. A...est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. B...A...et au ministre de l'intérieur. <br/>
Copie en sera adressée à la Commission nationale de l'informatique et des libertés.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-07-06 DROITS CIVILS ET INDIVIDUELS. - TRAITEMENTS AUTOMATISÉS INTÉRESSANT LA SÛRETÉ DE L'ETAT - LOI N° 2015-912 DU 24 JUILLET 2015 - CONTRÔLE JURIDICTIONNEL - FORMATION SPÉCIALISÉE - MOYENS - MOYENS INOPÉRANTS - DÉFAUT DE MOTIVATION DE LA DÉCISION RÉVÉLÉE PAR LA LETTRE ADRESSÉE PAR LA CNIL SUR LE FONDEMENT DE L'ART. 41 DE LA LOI N° 78-17 DU 6 JANVIER 1978.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-01-04-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. MOYENS INOPÉRANTS. - TRAITEMENTS AUTOMATISÉS INTÉRESSANT LA SÛRETÉ DE L'ETAT - LOI N° 2015-912 DU 24 JUILLET 2015 - RECOURS DEVANT LA FORMATION SPÉCIALISÉE (ART. L. 841-2 DU CSI) - MOYEN TIRÉ D'UN DÉFAUT DE MOTIVATION DE LA DÉCISION RÉVÉLÉE PAR LA LETTRE ADRESSÉE PAR LA CNIL SUR LE FONDEMENT DE L'ART. 41 DE LA LOI DU 6 JANVIER 1978.
</SCT>
<ANA ID="9A"> 26-07-06 Le défaut de motivation de la décision, révélée par la lettre de la CNIL qui informe l'intéressé, sur le fondement de l'article 41 de la loi n° 78-17 du 6 janvier 1978, qu'il a été procédé aux vérifications, ne peut être utilement invoqué à l'appui d'une requête concernant la mise en oeuvre de cet article et relevant de l'article L. 841-2 du code de la sécurité intérieure.</ANA>
<ANA ID="9B"> 54-07-01-04-03 Le défaut de motivation de la décision, révélée par la lettre de la CNIL qui informe l'intéressé, sur le fondement de l'article 41 de la loi n° 78-17 du 6 janvier 1978, qu'il a été procédé aux vérifications, ne peut être utilement invoqué à l'appui d'une requête concernant la mise en oeuvre de cet article et relevant de l'article L. 841-2 du code de la sécurité intérieure.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
