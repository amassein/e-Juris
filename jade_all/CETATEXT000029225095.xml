<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029225095</ID>
<ANCIEN_ID>JG_L_2014_07_000000356324</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/22/50/CETATEXT000029225095.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère / 6ème SSR, 11/07/2014, 356324</TITRE>
<DATE_DEC>2014-07-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>356324</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère / 6ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>FOUSSARD ; SCP PIWNICA, MOLINIE ; SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>M. Rémi Decout-Paolini</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:356324.20140711</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 31 janvier et 30 avril 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. C... A..., Mme D...G..., épouseA..., Mme E...A..., M. F... A...et Mme B...A..., demeurant... ; M. C...A...et autres demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10PA04303, 10 PA04344 du 1er décembre 2011 par lequel la cour administrative de Paris a rejeté leur requête tendant à l'annulation du jugement n° 07178174, 0805747, 0807666 du 24 juin 2010 par lequel le tribunal administratif de Paris a rejeté leur demande tendant à l'annulation des arrêtés du 26 octobre 2007 par lesquels le maire de Paris a délivré à l'Assistance publique - Hôpitaux de Paris (AP-HP) un permis de démolir et un permis de construire sur la parcelle située, dans le XVe arrondissement de Paris, 149-153 de la rue de Sèvres et 2-6 du boulevard de Montparnasse ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre solidairement à la charge de la Ville de Paris et de l'Assistance publique - Hôpitaux de Paris (AP-HP) la somme de 10 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Rémi Decout-Paolini, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de M. C...A..., et autres, à Me Foussard, avocat de la Ville de Paris et à la SCP Piwnica, Molinié, avocat de l'Assistance publique - Hôpitaux de Paris ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le règlement du plan local d'urbanisme de la Ville de Paris comporte, au titre des dispositions générales applicables au territoire couvert par ce document, une rubrique " VIII - Définitions " précisant que ces définitions " doivent être prises en compte pour l'application du présent règlement et de ses documents graphiques " ; qu'aux termes de l'entrée " cour couverte " figurant dans cette rubrique : " Une cour couverte est un espace, situé au niveau du sol ou en étage, sur lequel des locaux d'habitation ou de travail prennent jour et air (atrium, galerie, passage, patio couvert...). Elle doit être couverte par un matériau transparent et disposer d'une ventilation appropriée. / Les façades ou parties de façades qui bordent une cour couverte sont soumises aux dispositions des articles 7 et 10.3 (façades en vis-à-vis d'une limite séparative) ou 8 et 10.4 (façades en vis-à-vis sur un même terrain) (...) " ; qu'aux termes de l'article UGSU 8.2.1 du règlement du plan local d'urbanisme, relatif aux conditions d'" implantation des constructions les unes par rapport aux autres sur un même terrain " : " Lorsque des façades ou des parties de façade de constructions en vis-à-vis sur un même terrain comportent des baies constituant l'éclairement premier de pièces principales, elles doivent être édifiées de telle manière que la distance de l'une d'elle au point le plus proche de l'autre soit au moins égale à 6 mètres " ; qu'aux termes de l'article UGSU 10.4 du même règlement, relatif au " gabarit-enveloppe des constructions en vis-à-vis sur un même terrain " : " Le point d'attache du gabarit-enveloppe est pris sur le plancher du niveau le plus bas comportant des baies constituant l'éclairement premier de pièces principales s'éclairant sur la façade du bâtiment en vis-à-vis. / Le gabarit-enveloppe d'une construction ou partie de construction à édifier en vis-à-vis de la façade d'un bâtiment comportant des baies constituant l'éclairement premier de pièces principales se compose successivement : / a - d'une verticale de hauteur H égale au prospect P mesuré entre les constructions en vis-à-vis augmenté de 4 mètres (...) / b - d'une oblique de pente 1/1 élevée au sommet de la verticale et limitée au plafond des hauteurs " ; qu'il résulte de l'ensemble de ces dispositions et des schémas annexés au règlement du plan local d'urbanisme auxquels elles renvoient que les règles d'implantation ainsi fixées s'appliquent non seulement à des bâtiments distincts situés en vis-à-vis sur un même terrain mais aussi aux façades en vis-à-vis d'un même bâtiment qui entourent une " cour couverte " ; que, par suite, en jugeant que les requérants ne pouvaient utilement faire valoir que les règles d'implantation précitées auraient été méconnues au motif que le bâtiment principal du projet litigieux comportait deux patios couverts avec, à l'intérieur du même bâtiment, des façades en vis-à-vis, la cour administrative d'appel a commis une erreur de droit ; que son arrêt doit pour ce motif être annulé, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi ;<br/>
<br/>
              2. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de M. C...A...et autres qui ne sont pas, dans la présente instance, les parties perdantes ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de la Ville de Paris et de l'Assistance publique - Hôpitaux de Paris (AP-HP), au titre des mêmes dispositions, le versement d'une somme de 300 euros chacune à chaque requérant ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 1er décembre 2011 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br/>
Article 3 : La Ville de Paris et l'Assistance publique - Hôpitaux de Paris (AP-HP) verseront l'une et l'autre à M. C... A..., à Mme D...G..., épouseA..., à Mme E...A..., à M. F... A...et à Mme B...A...une somme de 300 euros chacun au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par la Ville de Paris et par l'Assistance publique - Hôpitaux de Paris (AP-HP) au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée M. C...A..., premier requérant dénommé, à l'Assistance publique - Hôpitaux de Paris (AP-HP) et à la Ville de Paris.<br/>
Les autres requérants seront informés de la présente décision par la SCP Masse-Dessen, Thouvenin, Coudray, avocat au Conseil d'Etat et à la Cour de cassation, qui les représente devant le Conseil d'Etat.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-01-01-02-02-08 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PLANS D'AMÉNAGEMENT ET D'URBANISME. PLANS D`OCCUPATION DES SOLS (POS) ET PLANS LOCAUX D'URBANISME (PLU). APPLICATION DES RÈGLES FIXÉES PAR LES POS OU LES PLU. RÈGLES DE FOND. IMPLANTATION DES CONSTRUCTIONS LES UNES PAR RAPPORT AUX AUTRES SUR UNE MÊME PROPRIÉTÉ - RÈGLES DE DISTANCE ET DE GABARIT-ENVELOPPE DÉFINIES PAR LE PLU DE PARIS - APPLICABILITÉ AUX FAÇADES EN VIS-À-VIS D'UN MÊME BÂTIMENT ENTOURANT UNE  COUR COUVERTE  - EXISTENCE.
</SCT>
<ANA ID="9A"> 68-01-01-02-02-08 Les règles de distance et de gabarit-enveloppe encadrant l'implantation des constructions les unes par rapport aux autres sur un même terrain, définies aux articles UGSU 8.2.1 et 10.4 du règlement du PLU de la Ville de Paris, s'appliquent non seulement à des bâtiments distincts situés en vis-à-vis sur un même terrain mais aussi aux façades en vis-à-vis d'un même bâtiment qui entourent une  cour couverte .</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
