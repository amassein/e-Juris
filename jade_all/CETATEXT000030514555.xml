<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030514555</ID>
<ANCIEN_ID>JG_L_2015_04_000000388776</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/51/45/CETATEXT000030514555.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 14/04/2015, 388776, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-04-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>388776</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2015:388776.20150414</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête enregistrée le 17 mars 2015 au secrétariat du contentieux du Conseil d'Etat et par un mémoire en réplique, enregistré le 9 avril 2015, la société nationale d'exploitation industrielle des tabacs et allumettes (SEITA) demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution de l'article 3 de l'arrêté du 24 février 2015 de la ministre des affaires sociales, de la santé et des droits des femmes modifiant l'arrêté du 15 avril 2010 relatif aux modalités d'inscription des avertissements à caractère sanitaire sur les unités de conditionnement des produits du tabac et insérant un pictogramme destiné aux femmes enceintes, en tant qu'il prévoit l'entrée en vigueur immédiate de l'obligation d'insertion sur les unités de conditionnement des produits du tabac du pictogramme destiné aux femmes enceintes et qu'il limite à six mois la période de mise en oeuvre des mesures transitoires destinées à éliminer les produits du tabac non conformes à la nouvelle réglementation ;  <br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 5 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Elle soutient que : <br/>
              - il existe un doute sérieux quant à la légalité de l'arrêté contesté ; <br/>
              - la ministre des affaires sociales, de la santé et des droits des femmes n'avait pas compétence pour prendre l'arrêté litigieux, dès lors que celui-ci restreint la liberté d'entreprendre des professionnels du tabac, en méconnaissance de l'article L. 3511-6 du code de la santé publique ; <br/>
              - l'arrêté litigieux méconnaît l'objectif constitutionnel d'accessibilité et d'intelligibilité de la norme juridique, le principe de sécurité juridique en tant qu'il ne prévoit pas de mesures transitoires suffisantes ainsi que le principe d'égalité ; <br/>
              - la condition d'urgence est remplie dès lors que l'arrêté litigieux porte une atteinte suffisamment grave et immédiate à sa situation industrielle, commerciale et financière. <br/>
<br/>
<br/>
              Vu l'arrêté dont la suspension de l'exécution est demandée ;<br/>
<br/>
              Vu la copie de la requête à fin d'annulation de cet arrêté ; <br/>
              Par deux mémoires en défense enregistrés les 2 et 7 avril 2015, la ministre des affaires sociales, de la santé et des droits des femmes conclut au rejet de la requête. Elle soutient que :<br/>
              - la condition d'urgence n'est pas remplie dès lors que l'arrêté répond à un impératif de protection de la santé publique et que les dispositions contestées ne préjudicient pas gravement et immédiatement à la société requérante ; <br/>
              - les moyens soulevés par la requérante ne sont pas fondés. <br/>
              Vu les autres pièces du dossier :<br/>
<br/>
              Vu : <br/>
<br/>
              - le code de la santé publique ;<br/>
<br/>
              - le code de justice administrative. <br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part la société nationale d'exploitation industrielle des tabacs et allumettes (SEITA), d'autre part, la ministre des affaires sociales, de la santé et des droits des femmes ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 10 avril 2015 à 10 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Briard, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la SEITA ;<br/>
<br/>
              - les représentants de la SEITA ; <br/>
<br/>
              - les représentants de la ministre des affaires sociales, de la santé et des droits des femmes ;<br/>
     et à l'issue de laquelle le juge des référés a clos l'instruction ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 3511-1 du code de la santé publique : " Sont considérés comme produits du tabac les produits destinés à être fumés, prisés, mâchés ou sucés, dès lors qu'ils sont, même partiellement, constitués de tabac, ainsi que les produits destinés à être fumés même s'ils ne contiennent pas de tabac, à la seule exclusion des produits qui sont destinés à un usage médicamenteux, au sens du troisième alinéa (2°) de l'article 564 decies du code général des impôts " ; qu'aux termes de l'article L. 3511-6 du même code : " Toutes les unités de conditionnement du tabac et des produits du tabac ainsi que du papier à rouler les cigarettes portent, dans les conditions fixées par un arrêté du ministre chargé de la santé, un message général et un message spécifique de caractère sanitaire " ;<br/>
<br/>
              3. Considérant que par arrêté du 24 février 2015, pris sur le fondement des dispositions précitées de l'article L. 3511-6, la ministre des affaires sociales, de la santé et des droits des femmes a modifié les dispositions de l'arrêté du 15 avril 2010 relatif aux modalités d'inscription des avertissements de caractère sanitaire sur les unités de conditionnement des produits du tabac, pour imposer, à l'article 1er de cet arrêté, en sus des avertissements déjà prévus par l'arrêté du 15 avril 2010, l'apposition sur les unités de conditionnement des produits du tabac d'un pictogramme destiné aux femmes enceintes, ainsi que pour agrandir, à l'article 2, la taille des avertissements prévus par ce précédent arrêté ; que, si les dispositions relatives à la taille des avertissements n'entreront en vigueur qu'au 20 mai 2016, en vertu de l'article 3 de l'arrêté, ce même article prévoit que la disposition relative à l'apposition du pictogramme destiné aux femmes enceintes entre en vigueur le lendemain de la date de publication de l'arrêté, tout en prévoyant que " les produits du tabac non conformes aux dispositions de l'article 1er peuvent être mis à la consommation dans un délai de six mois suivant son entrée en vigueur " ; que la société nationale d'exploitation industrielle des tabacs et allumettes (SEITA) demande que soit ordonnée la suspension de l'exécution des dispositions de l'article 3 de cet arrêté, en tant qu'elles concernent le pictogramme destiné aux femmes enceintes ;<br/>
<br/>
              4. Considérant, en premier lieu, que la ministre des affaires sociales, de la santé et des droits des femmes tenait des dispositions précitées de l'article L. 3511-6 du code de la santé publique compétence pour prendre l'arrêté contesté ; que le moyen tiré de ce qu'elle aurait excédé cette compétence en ne prévoyant pas, au titre des mesures transitoires de son article 3, un délai suffisant pour écouler les stocks de produits ne comportant pas le nouveau pictogramme n'est pas, en l'état de l'instruction, et en toute hypothèse, de nature à créer un doute sérieux quant à la légalité de ces mesures transitoires ; que, par ailleurs, la société requérante ne saurait utilement discuter la constitutionnalité de l'article L. 3511-6 faute d'avoir, dans la présente instance de référé, présenté une question prioritaire de constitutionnalité ;<br/>
<br/>
              5. Considérant, en deuxième lieu, que le moyen tiré de ce que les dispositions de l'arrêté méconnaissent l'objectif à valeur constitutionnelle d'accessibilité et d'intelligibilité de la norme juridique, au double motif que la notion de " produits du tabac " ne serait pas précisément définie et que l'article 3 de l'arrêté utilise cette expression cependant que l'article 1er utilise celle d' " unités de conditionnement des produits du tabac ", n'est pas, en l'état de l'instruction, de nature à créer un doute sérieux quant à la légalité des dispositions contestées ; qu'en effet, d'une part, la définition des " produits du tabac " est donnée par les dispositions précitées de l'article L. 3511-1 du code de la santé publique et, d'autre part, les dispositions transitoires de l'article 3, qui autorisent la mise à la consommation pendant six mois encore des " produits du tabac non conformes aux dispositions de l'article 1er " se réfèrent ainsi clairement aux " unités de conditionnement des produits du tabac " mentionnés à ce dernier article ; <br/>
<br/>
              6. Considérant, en troisième lieu, que la société requérante soutient que l'entrée en vigueur immédiate de l'arrêté, en tant qu'il concerne le pictogramme destiné aux femmes enceintes, à la seule réserve de la faculté de mettre à la consommation pendant une période transitoire de six mois les produits ne comportant pas ce pictogramme, méconnaît le principe de sécurité juridique ; qu'elle fait valoir que la trop grande brièveté de ce délai affectera le processus industriel et ses relations contractuelles avec ses fournisseurs et prestataires, risque de provoquer des ruptures d'approvisionnement et occasionnera des pertes de l'ordre de trois millions d'euros en raison de la destruction inévitable de stocks d'emballages et de produits finis qui n'auront pas pu être écoulés, ce d'autant plus que la publication de l'arrêté a été plus précoce que ce qui pouvait se déduire des informations fournies par l'administration et que, lors de l'édiction de l'arrêté du 15 avril 2010, un délai transitoire d'une année avait été prévu ; que cependant ce moyen n'est pas, en l'état de l'instruction, de nature à créer un doute sérieux quant à la légalité des dispositions contestées, dès lors que la société requérante n'apporte pas d'éléments suffisants au soutien de ses allégations sur l'impossibilité d'adapter le processus industriel et ses relations contractuelles dans le délai transitoire prévu par l'arrêté, que seuls les fabricants devront se conformer au terme de ce délai à la nouvelle réglementation, les distributeurs et les débitants de tabac pouvant écouler les stocks constitués sans être tenus par ce délai, que les pertes financières avancées ne tiennent pas compte de la possibilité pour les distributeurs d'écouler ces stocks, et que le montant de trois millions d'euros doit être mis en regard d'un résultat net annuel de la SEITA, pour le marché français, de 1,3 milliard d'euros, pour un chiffre d'affaires de 4,5 milliard d'euros ; que, par ailleurs, le moyen tiré de ce que le principe d'égalité aurait été méconnu du fait que l'arrêté prévoit une période transitoire plus longue pour l'agrandissement des avertissements à caractère sanitaire ne saurait davantage être de nature à créer un doute sérieux quant à la légalité des dispositions contestées ; <br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin de se prononcer sur la condition d'urgence, la requête de la SEITA doit être rejetée, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de la SEITA est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à la société nationale d'exploitation industrielle des tabacs et allumettes (SEITA) et à la ministre des affaires sociales, de la santé et des droits des femmes. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
