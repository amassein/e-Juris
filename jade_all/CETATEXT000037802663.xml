<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037802663</ID>
<ANCIEN_ID>JG_L_2018_12_000000421294</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/80/26/CETATEXT000037802663.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 12/12/2018, 421294</TITRE>
<DATE_DEC>2018-12-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421294</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Richard Senghor</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:421294.20181212</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Strasbourg d'annuler, pour excès de pouvoir, les décisions des 7 novembre, 5 décembre  et 16 décembre 2013 par lesquelles le directeur de la maison centrale d'Ensisheim a refusé de lui attribuer l'aide aux personnes détenues dépourvues de ressources suffisantes au titre des mois de novembre et décembre 2013. Par un jugement n° 1403949 du 6 octobre 2016, le tribunal administratif de Strasbourg a annulé la décision du 7 novembre 2013 et a rejeté le surplus de ses conclusions.<br/>
<br/>
              Par un arrêt n° 17NC00643 du 10 avril 2018, la cour administrative d'appel de Nancy  a, sur appel de M.A..., partiellement annulé le jugement du tribunal administratif de Strasbourg du 6 octobre 2016 en tant que celui-ci avait rejeté ses conclusions tendant à l'annulation de la décision du 5 décembre 2013, annulé cette dernière et rejeté le surplus des conclusions dont elle était saisie.<br/>
<br/>
              Par un pourvoi, enregistré le 7 juin 2018 au secrétariat du contentieux du Conseil d'Etat, la garde des sceaux, ministre de la justice demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il a annulé la décision du 5 décembre 2013 ;<br/>
<br/>
2°) réglant l'affaire au fond, de rejeter intégralement l'appel de M. A.... <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de procédure pénale ;<br/>
              -	la loi n° 2009-436 du 24 novembre 2009 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Richard Senghor, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. A...s'est vu opposer des décisions de refus à ses demandes tendant à l'attribution de l'aide financière destinée aux personnes détenues dépourvues de ressources suffisantes, le 7 novembre 2013 au titre du mois de novembre 2013, le 5 décembre 2013 au titre des mois de novembre et décembre 2013 et le 16 décembre 2013 au titre du mois de décembre 2013. Saisi  de conclusions dirigées contre ces trois décisions, le tribunal administratif de Strasbourg a annulé la décision du 7 novembre 2013 et rejeté le surplus de la demande de M.A..., par un jugement du 6 octobre 2016. Saisie en appel, la cour administrative d'appel de Nancy a annulé partiellement le dispositif de rejet de ce jugement ainsi que la décision du 5 décembre 2013 par un arrêt de du 10 avril 2018 contre lequel la garde des sceaux, ministre de la justice se pourvoit, dans cette mesure, en cassation.<br/>
<br/>
              2. L'article 31 de la loi du 24 novembre 2009 pénitentiaire dispose que : " Les personnes détenues dont les ressources sont inférieures à un montant fixé par voie réglementaire reçoivent de l'Etat une aide en nature destinée à améliorer leurs conditions matérielles d'existence. Cette aide peut aussi être versée en numéraire dans des conditions prévues par décret ". Aux termes de l'article D. 347-1 du code de procédure pénale : " Les personnes détenues sont considérées comme dépourvues de ressources suffisantes lorsque, cumulativement : / -la part disponible du compte nominatif pendant le mois précédant le mois courant est inférieure à 50 &#128; ; / -la part disponible du compte nominatif pendant le mois courant est inférieure à 50 &#128; ; / -et le montant des dépenses cumulées dans le mois courant est inférieur à 50 &#128;. (...) / L'aide que reçoivent les personnes détenues dépourvues de ressources suffisantes est attribuée par l'administration pénitentiaire. Il est tenu compte des aides attribuées à la personne détenue intéressée par toute personne physique ou morale de droit public ou privé autorisée à le faire par l'administration pénitentiaire. / L'aide est fournie prioritairement en nature, notamment par la remise de vêtements, par le renouvellement de la trousse de toilette dans les conditions prévues à l'article D. 357 et par la remise d'un nécessaire de correspondance. /Lorsque l'administration pénitentiaire ou la personne autorisée à attribuer l'aide n'est pas en mesure de la fournir en nature ou lorsque les besoins de la personne détenue le justifient, elle est versée en numéraire, en tout ou partie, sur la part disponible du compte nominatif ".<br/>
<br/>
              3. Aux termes de l'article D. 89 du code de procédure pénale : " Le parcours d'exécution de la peine est élaboré après avis de la commission pluridisciplinaire unique mentionnée à l'article D. 90 ".  Aux termes de l'article D. 90 du même code : " Il est institué auprès du chef de chaque établissement pénitentiaire, pour une durée de cinq ans, une commission pluridisciplinaire unique./ La commission pluridisciplinaire unique est présidée par le chef d'établissement ou son représentant (...) ". Aux termes de l'article D. 91 du même code : " La commission pluridisciplinaire unique se réunit au moins une fois par mois pour examiner les parcours d'exécution de la peine ". Il résulte de ces dispositions que la commission pluridisciplinaire revêt un caractère consultatif dont les avis ont pour objet d'éclairer le choix du chef d'établissement dans l'édiction des décisions qui relèvent de sa compétence.<br/>
<br/>
              4. Il s'ensuit qu'en déduisant de la référence, dans la décision du 5 décembre 2013, à l'avis émis par la commission pluridisciplinaire unique sur la demande de M. A...de bénéficier de l'aide aux personnes détenues dépourvues de ressources suffisantes au titre des mois de novembre et décembre 2013, que le directeur de la maison centrale d'Ensisheim s'était estimé lié par cet avis et avait ainsi méconnu l'étendue de sa compétence, la cour administrative d'appel de Nancy a entaché son arrêt d'erreur de droit.<br/>
<br/>
              5. Il résulte de ce qui précède que la garde des sceaux ministre de la justice est fondée à demander l'annulation de l'article 1er de l'arrêt qu'elle attaque.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond, dans cette mesure, en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              7. Il résulte des dispositions citées au point 2 que l'aide aux personnes détenues dépourvues de ressources suffisantes vise à garantir à ces dernières des conditions matérielles d'existence décentes, en particulier en matière d'hygiène et d'habillement. Le niveau de ressources d'une personne détenue est apprécié compte tenu, le cas échéant, des aides qu'elle a reçues, avec l'autorisation de l'administration pénitentiaire. L'article D. 320 du code de procédure pénale prévoit d'ailleurs que " Toutes les sommes qui échoient aux détenus sont considérées comme ayant un caractère alimentaire, dans la mesure où elles n'excèdent pas chaque mois 200 Euros (...) " et " sont dès lors entièrement versées à la part disponible jusqu'à concurrence de cette provision alimentaire ". <br/>
<br/>
              8. Il ressort des pièces du dossier que la décision attaquée est fondée sur la circonstance que la part disponible du compte nominatif de M.A..., laquelle est laissée à sa libre disposition en vertu de l'article 728-1 du code de procédure pénale, présentait un solde positif de 63,64 euros au 1er octobre 2013 et de 65, 14 &#128; au 1er novembre 2013, soit des montants supérieurs au seuil de 50 euros fixé par l'article D. 347-1 du code de procédure pénale précité. Il est constant que ce solde résulte du versement, à l'initiative de l'administration pénitentiaire, sur la part disponible du compte nominatif de l'intéressé, d'une somme de 45 euros correspondant au don versé au cours de l'été 2013 par l'association Caritas, en vue de permettre l'acquisition par l'intéressé d'une imprimante dont l'achat s'était révélé impossible dans l'immédiat, faute de disponibilité. Dans les circonstances particulières de l'espèce, le refus d'allouer à M.A..., au titre des mois de novembre et décembre 2013, l'aide aux personnes détenues dépourvues de ressources suffisantes alors qu'il se trouvait structurellement dans une situation d'indigence et que le solde de la part disponible de son compte nominatif n'excédait le seuil de 50 euros qu'à raison de la prise en compte d'un don dont il n'est pas contesté qu'il n'était pas destiné à améliorer les conditions matérielles d'existence de l'intéressé, est entaché d'erreur manifeste d'appréciation.<br/>
<br/>
              9. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner l'autre moyen de sa requête qui est suffisamment motivé, contrairement à ce que soutient la garde des sceaux, ministre de la justice, que M. A...est fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Strasbourg a rejeté sa demande tendant à l'annulation de la décision du 5 décembre 2013.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'article 1er de l'arrêt du 10 avril 2018 de la cour administrative d'appel de Nancy est annulé.<br/>
Article 2 : Le jugement du tribunal administratif de Strasbourg du 6 octobre 2016 est annulé en tant qu'il a rejeté les conclusions dirigées contre la décision du 5 décembre 2013 du directeur de la maison centrale d'Ensisheim.<br/>
Article 3 : La décision du 5 décembre 2013 du directeur de la maison centrale d'Ensisheim est annulée.<br/>
Article 4 : La présente décision sera notifiée à la garde des sceaux, ministre de la justice, et à M. B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-05-01-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - MOTIFS. POUVOIRS ET OBLIGATIONS DE L'ADMINISTRATION. COMPÉTENCE LIÉE. - RÉGIME PÉNITENTIAIRE - AVIS DE LA COMMISSION PLURIDISCIPLINAIRE UNIQUE (ART. D. 89 À D. 91 DU CPP) - 1) PRINCIPE - COMPÉTENCE LIÉE DU CHEF D'ÉTABLISSEMENT POUR LES DÉCISIONS RELEVANT DE SA COMPÉTENCE, PRISES APRÈS AVIS DE CETTE COMMISSION - ABSENCE - 2) ESPÈCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">37-05-02-01 JURIDICTIONS ADMINISTRATIVES ET JUDICIAIRES. EXÉCUTION DES JUGEMENTS. EXÉCUTION DES PEINES. SERVICE PUBLIC PÉNITENTIAIRE. - AVIS DE LA COMMISSION PLURIDISCIPLINAIRE UNIQUE (ART. D. 89 À D. 91 DU CPP) - 1) PRINCIPE - COMPÉTENCE LIÉE DU CHEF D'ÉTABLISSEMENT POUR LES DÉCISIONS RELEVANT DE SA COMPÉTENCE, PRISES APRÈS AVIS DE CETTE COMMISSION - ABSENCE - 2) ESPÈCE.
</SCT>
<ANA ID="9A"> 01-05-01-03 1) Il résulte des articles D. 89, D. 90 et D. 91 du code de procédure pénale (CPP) que la commission pluridisciplinaire revêt un caractère consultatif dont les avis ont pour objet d'éclairer le choix du chef d'établissement dans l'édiction des décisions qui relèvent de sa compétence.,,2) Il s'ensuit qu'en déduisant de la référence, dans la décision du chef d'établissement, à l'avis émis par la commission pluridisciplinaire unique sur la demande du requérant, que ce chef d'établissement s'était estimé lié par cet avis et avait ainsi méconnu l'étendue de sa compétence, une cour administrative d'appel entache son arrêt d'erreur de droit.</ANA>
<ANA ID="9B"> 37-05-02-01 1) Il résulte des articles D. 89, D. 90 et D. 91 du code de procédure pénale (CPP) que la commission pluridisciplinaire revêt un caractère consultatif dont les avis ont pour objet d'éclairer le choix du chef d'établissement dans l'édiction des décisions qui relèvent de sa compétence.,,2) Il s'ensuit qu'en déduisant de la référence, dans la décision du chef d'établissement, à l'avis émis par la commission pluridisciplinaire unique sur la demande du requérant, que ce chef d'établissement s'était estimé lié par cet avis et avait ainsi méconnu l'étendue de sa compétence, une cour administrative d'appel entache son arrêt d'erreur de droit.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
