<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044087023</ID>
<ANCIEN_ID>JG_L_2021_08_000000455665</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/08/70/CETATEXT000044087023.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 31/08/2021, 455665, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-08-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>455665</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:455665.20210831</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 16 août 2021 au secrétariat du contentieux du Conseil d'Etat, M. A... B... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution de la délibération du jury du concours externe d'officier de la police nationale du 17 juin 2021 arrêtant la liste des candidats admis à ce concours, telle que publiée le 22 juin 2021 sur le site internet " devenirpolicier.fr " ; <br/>
<br/>
              2°) d'ordonner la suspension des nominations à l'école nationale supérieure des officiers de police intervenues ou à intervenir.<br/>
<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - la condition d'urgence est satisfaite dès lors que, en premier lieu, la décision contestée a des effets immédiats sur sa situation, tant professionnelle que personnelle, en deuxième lieu, cette décision, une fois définitive, est créatrice de droits et empêche donc l'administration de revenir sur celle-ci et, en dernier lieu, les candidats admis par la décision attaquée feront leur rentrée à l'école nationale supérieure des officiers de police (ENSOP) en septembre 2021 ;<br/>
              - il existe un doute sérieux quant à la légalité de la décision contestée ; <br/>
              - la décision attaquée est entachée d'irrégularité dès lors que, d'une part, le prénom des " examinateurs qualifiés " assistant les membres du jury n° 2 ne sont pas inscrits sur la grille d'évaluation des épreuves orales d'admission, et leurs noms sont difficilement lisibles et, d'autre part, le jury n° 2 était irrégulièrement composé eu égard à l'arrêté du 30 octobre 2020 fixant la composition des jurys des concours d'officiers de la police nationale pour la session 2021 modifié, qui prévoit la présence au sein du jury d'au moins un membre de la police nationale, une personnalité extérieure et un psychologue ; <br/>
              - cette décision porte atteinte au principe d'égalité entre les candidats dès lors que, d'une part, le jury a pris en considération, pour son appréciation, des éléments extérieurs à la valeur de sa prestation et, d'autre part, les notes qui lui ont été attribuées ne sont pas en adéquation avec la grille d'évaluation des épreuves orales d'admission ; <br/>
              - elle méconnaît la loi n° 2008-496 du 27 mai 2008 portant diverses dispositions d'adaptation au droit communautaire dans le domaine de la lutte contre les discriminations dès lors que, d'une part, le jury s'est montré hostile à son égard sans qu'aucune raison ne le justifie et, d'autre part, il a attaché une importance décisive à son orientation professionnelle, ce qui constitue une discrimination se rattachant à l'âge.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu : <br/>
              - la Constitution, et notamment son préambule ; <br/>
              - la loi n° 2008-496 du 27 mai 2008 portant diverses dispositions d'adaptation au droit communautaire dans le domaine de la lutte contre les discriminations ;<br/>
              - l'arrêté du 30 octobre 2020 fixant la composition des jurys des concours d'officiers de la police nationale pour la session 2021 modifié ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. M. B..., candidat au concours externe d'officier de la police nationale aux épreuves duquel il a participé de janvier à juin 2021 demande, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de la délibération du jury de ce concours ayant arrêté la liste des candidats déclarés admis, ainsi que la suspension des nominations à l'école nationale supérieure des officiers de police.<br/>
<br/>
              3. M. B... estime que l'illégalité externe de ces actes résulte d'abord de l'absence d'inscription du prénom des examinateurs qualifiés ainsi que de l'illisibilité de leur nom sur la grille d'évaluation qu'ils ont signée. Il résulte cependant des écritures mêmes de M. B... qu'il a parfaitement identifié ces examinateurs. Il soutient en effet que la composition de ce groupe d'examinateurs aurait dû suivre les règles régissant celle du jury, mentionnant à ce titre leur nom et leur qualité, alors qu'il ne résulte d'aucun texte que les règles applicables à la composition du jury valaient également pour la constitution de groupes d'examinateurs qualifiés. Enfin, la participation d'un examinateur qualifié au jury de l'épreuve finale résulte de la désignation de ces examinateurs pour " être associés aux travaux du jury ", par l'article 2 de l'arrêté du 30 octobre 2020, et n'a donc pu avoir pour conséquence de vicier la composition de celui-ci.<br/>
<br/>
              4. Pour critiquer la légalité interne de la délibération attaquée, M. B... soutient que le jury aurait pris en compte des informations que l'un de ses membres aurait consultées à son sujet sur sa page personnelle d'un réseau social professionnel durant l'entretien. A supposer ces circonstances établies, il ne résulte pas de l'appréciation du jury qu'il aurait tenu compte des éléments ainsi recueillis. En se référant au doute qu'il éprouvait quant à l'orientation du candidat, le jury a, contrairement à ce qui est soutenu, et conformément à l'article 5 de l'arrêté du 27 janvier 2014 organisant ce concours, apprécié son aptitude et sa motivation à exercer l'emploi postulé. La circonstance que les grilles d'évaluation portent des notes chiffrées et non des croix situées dans les colonnes affectées de signes allant de " -- " à " ++ " ou que les croix soient situées d'une manière pouvant aboutir à des valeurs différentes de celles retenues, ne permet nullement d'estimer que l'appréciation opérée aurait été arbitraire. Enfin, la perception d'une hostilité du jury à son encontre par le candidat ne permet pas de regarder comme établi un manquement du jury à ses obligations ou aux règles régissant son office.<br/>
<br/>
              5. Faute qu'aucun des moyens énoncés par M. B... puisse être regardé comme faisant naître un doute sérieux sur la légalité des décisions attaquées, sa requête tendant à leur suspension ne peut, sans qu'il soit besoin d'examiner les autres conditions auxquelles l'intervention d'une décision du juge des référés est subordonnée, qu'être rejetée.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. B... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. A... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
