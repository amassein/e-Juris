<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027626016</ID>
<ANCIEN_ID>JG_L_2013_06_000000355096</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/62/60/CETATEXT000027626016.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  9ème sous-section jugeant seule, 28/06/2013, 355096, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-06-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>355096</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 9ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>M. Jean-Luc Matt</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:355096.20130628</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaires, enregistrés les 21 décembre 2011 et 20 mars 2012, au secrétariat du contentieux du Conseil d'Etat, présentés par le ministre de l'écologie, du développement durable, des transports et du logement ; le ministre demande au Conseil d'Etat d'annuler le jugement n° 0900414 du 29 septembre 2011 par lequel le tribunal administratif de Saint-Denis a déchargé M. A...B...des taxes d'urbanisme et des pénalités correspondantes auxquelles il a été assujetti au titre de l'année 2005 ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Matt, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, avocat de M. B...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. B... a obtenu, par un arrêté préfectoral du 11 octobre 2001, une autorisation de créer une plate-forme à l'usage d'aéronefs ultra légers motorisés (ULM) sur la commune de Saint-Paul dans la zone de Cambaie ; qu'à la suite, il y a édifié un hangar d'une surface hors oeuvre nette totale de 252,50 m² pour le stationnement de ses avions ultra légers motorisés ; qu'après qu'un procès-verbal d'infraction a été établi le 25 avril 2005, l'administration a mis à la charge de M. B..., auquel ont été adressés le 25 avril 2005 un avis d'imposition et le 8 février 2009 un commandement de payer, la taxe locale d'équipement, la taxe pour le financement des dépenses des conseils d'architecture, d'urbanisme et de l'environnement et la taxe des espaces naturels sensibles, ainsi que des pénalités correspondantes, au titre de cette construction ; que, par un jugement du 29 septembre 2011 contre lequel le ministre de l'écologie, du développement durable, des transports et du logement se pourvoit en cassation, le tribunal administratif de Saint-Denis a prononcé la décharge de ces impositions et pénalités ; <br/>
<br/>
              2. Considérant que la taxe locale d'équipement, établie sur la construction, la reconstruction et l'agrandissement des bâtiments de toute nature en vertu de l'article 1585 A du code général des impôts alors applicable, est due, selon l'article 1723 quater A du même code, soit par le bénéficiaire de l'autorisation de construire, soit par le constructeur en cas de construction sans autorisation ou en infraction aux obligations résultant de l'autorisation ; qu'il suit de là que seules les opérations prévues à l'article 1585 A et entrant dans le champ d'une autorisation de construire, expresse ou tacite, sont soumises à la taxe locale d'équipement, à  la taxe pour le financement des dépenses des conseils d'architecture, d'urbanisme et de l'environnement et à la taxe des espaces naturels sensibles ;<br/>
<br/>
              3. Considérant qu'aux termes du quatrième alinéa de l'article L. 421-1 du code de l'urbanisme, dans sa rédaction alors applicable, le permis de construire n'est pas exigé " pour les ouvrages qui, en raison de leur nature ou de leur très faible dimension, ne peuvent être qualifiés de constructions " ; que selon l' article R. 421-1 du même code, pris pour son application, " n'entrent pas dans le champ d'application du permis de construire, notamment, les travaux ou ouvrages suivants : / (...) 2. Les ouvrages d'infrastructure des voies de communication ferroviaires, fluviales, routières ou piétonnières, publiques ou privées, ainsi que les ouvrages d'infrastructure portuaire ou aéroportuaire " ; qu'au sens de ce dernier article, les ouvrages d'infrastructure correspondent à des travaux de génie civil et non à des ouvrages de bâtiment ;<br/>
<br/>
              4. Considérant qu'en jugeant qu'un hangar destiné au stationnement d'aéronefs ULM, qui est un ouvrage de bâtiment, constitue, au sens des dispositions précitées, une infrastructure aéroportuaire et n'est donc pas une construction entrant dans le champ d'application du permis de construire et soumise, de ce fait, à la taxe locale d'équipement, le tribunal a commis une erreur de droit ; que par suite, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, son jugement doit être annulé ;<br/>
<br/>
              5. Considérant que l'article L. 761-1 du code de justice administrative fait obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le jugement du 29 septembre 2011 du tribunal administratif de Saint-Denis est annulé.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Saint-Denis.<br/>
Article 3 : Les conclusions présentés par M. B...au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la ministre de l'écologie, du développement durable et de l'énergie et à M. A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
