<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032582792</ID>
<ANCIEN_ID>JG_L_2016_05_000000395860</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/58/27/CETATEXT000032582792.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 25/05/2016, 395860, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-05-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>395860</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Isabelle Lemesle</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:395860.20160525</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire complémentaire et un mémoire en réplique, enregistrés les 4 janvier, 8 février, 21 avril et le 28 avril 2016 au secrétariat du contentieux du Conseil d'Etat, le Mouvement des entreprises de France (MEDEF), l'association française des banques - fédération bancaire française, la société Brasserie de Tahiti, la société GAN outre-mer IARD, la société Total Tahitienne Entreposage et la société Total Polynésie demandent au Conseil d'Etat :<br/>
<br/>
              1°) de déclarer les articles LP 4 et LP 5 de la " loi du pays " n° 2015-18 LP/APF adoptée le 26 novembre 2015 instituant le principe exceptionnel d'un apurement des impayés de cotisations sociales dues par les employeurs, au titre des avantages en nature et en espèces, non conformes au bloc de légalité défini au III de l'article 176 de la loi organique du 27 février 2004 portant statut d'autonomie de la Polynésie française ;<br/>
<br/>
              2°) de mettre à la charge de la Polynésie française la somme de 6 000 euros à leur verser au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 74 ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, ainsi que le premier protocole additionnel à cette convention ;<br/>
              - la loi organique n° 2004-192 du 27 février 2004 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Isabelle Lemesle, conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat du Mouvement des entreprises de France (MEDEF) de la Polynésie Française, de l'association française des banques - fédération bancaire française, de la société Brasserie de Tahiti, de la société Gan Outre-mer IARD, de la société Total Tahitienne d'entreposage et de la société Total Polynésie et à la SCP Piwnica, Moliné, avocat de la société France Télévisions ;<br/>
<br/>
<br/>
              Vu la note en délibéré, enregistrée le 9 mai 2016, présentée par le MEDEF et autres ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du II de l'article 176 de la loi organique du 27 février 2004 portant statut d'autonomie de la Polynésie française : " A l'expiration de la période de huit jours suivant l'adoption d'un acte prévu à l'article 140 dénommé "loi du pays" ou au lendemain du vote intervenu à l'issue de la nouvelle lecture prévue à l'article 143, l'acte prévu à l'article 140 dénommé "loi du pays" est publié au Journal officiel de la Polynésie française à titre d'information pour permettre aux personnes physiques ou morales, dans le délai d'un mois à compter de cette publication, de déférer cet acte au Conseil d'Etat. / Le recours des personnes physiques ou morales est recevable si elles justifient d'un intérêt à agir (...) " ;<br/>
<br/>
              Sur les fins de non-recevoir opposées en défense :<br/>
<br/>
              2. Considérant que, eu égard aux moyens qu'ils soulèvent, les requérants doivent être regardés comme ne contestant que les articles LP 4 et LP 5 de la " loi du pays " n° 2015-18 LP/APF, adoptée le 26 novembre 2015 par l'assemblée de la Polynésie française, instituant le principe exceptionnel d'un apurement des impayés de cotisations sociales dues par les employeurs au titre des avantages en nature et en espèces ; <br/>
<br/>
              3. Considérant que les sociétés requérantes, qui ont leur siège social en Polynésie française, sont redevables, en application de l'article 19 de l'arrêté n° 1336 IT du 28 septembre 1956, des cotisations sociales mentionnées ci-dessus et que le mouvement des entreprises de France (MEDEF) ainsi que l'association française des banques-fédération bancaire française ont pour objet la défense des droits et des intérêts matériels et moraux, tant collectifs qu'individuels, des employeurs ; que les requérants justifient d'un intérêt direct et certain leur donnant qualité pour contester les dispositions de l'article LP 4 de la " loi du pays " attaquée qui donnent compétence à la caisse de prévoyance sociale (CPS) pour apprécier les " difficultés financières insurmontables de nature à conduire à la cessation des paiements " dont sont susceptibles de se prévaloir les employeurs de droit privé qui demandent, sur ce fondement, l'annulation des cotisations sociales, majorations de retard et pénalités correspondantes dues au titre des avantages en nature et en espèces après une procédure de redressement ou une déclaration de main-d'oeuvre ; <br/>
<br/>
              4. Considérant, en revanche, que les dispositions de l'article LP 5 de la " loi du pays " attaquée ont pour objet l'annulation de droit des cotisations, majorations et pénalités lorsqu'elles concernent les personnels enseignants et non enseignants des établissements d'enseignement privé sous contrat ; que, eu égard à l'objet de ces dispositions, les requérants ne peuvent être regardés, en leur qualité d'employeurs ou d'associations de défense de leurs intérêts, comme justifiant d'un intérêt direct et certain leur donnant qualité pour demander qu'elles soient déclarées illégales ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que la présente requête, qui a été régulièrement introduite par les représentants légaux des différents requérants, n'est recevable qu'en tant qu'elle tend à ce que l'article LP 4 de la " loi du pays " n° 2015-9 LP/APF soit déclaré illégal ;<br/>
<br/>
              Sur l'intervention de la société France Télévisions :<br/>
<br/>
              6. Considérant que la société France Télévisions qui se prévaut de sa qualité d'employeur en Polynésie française au titre de la chaîne généraliste de proximité " Polynésie 1ère ", justifie d'un intérêt suffisant à ce que soit déclaré illégal l'article LP 4 de la " loi du pays " attaquée ; qu'ainsi son intervention à l'appui de la requête est recevable dans cette mesure ; qu'il résulte en revanche de ce qui a été dit au point 5 que son intervention est, pour le surplus, irrecevable :<br/>
<br/>
              Sur les conclusions aux  fins d'annulation :<br/>
<br/>
              En ce qui concerne la procédure d'adoption de la " loi du pays " :<br/>
<br/>
              7. Considérant que les requérants soutiennent que les règles de quorum fixées au premier alinéa de l'article 122 de la loi organique n'auraient pas été respectées lors de la séance au cours de laquelle l'assemblée de la Polynésie française a adopté la " loi du pays " attaquée ; qu'il ressort, toutefois, des mentions portées au procès-verbal de cette séance, établi et publié au Journal officiel de la Polynésie française en application de l'article 12 du règlement intérieur de l'assemblée, que ce moyen manque en fait ;<br/>
<br/>
              8. Considérant qu'aux termes de l'article 8 du règlement intérieur de l'assemblée de la Polynésie française, dans sa rédaction issue de la délibération n° 2005-59 APF du 13 mai 2005 : " Trois jours au moins avant la date fixée pour une séance déterminée, le président de l'assemblée réunit la conférence des présidents de groupe pour préparer l'ordre du jour de ladite séance " ; que ce délai, qui n'est pas un délai franc, a été respecté en l'espèce dès lors qu'il ressort des pièces du dossier que la conférence des présidents s'est réunie le 20 novembre 2015 en vue de la préparation de l'ordre du jour de la séance du 26 novembre suivant ; que le moyen tiré de ce que la conférence des présidents se serait réunie tardivement ne peut, par suite, qu'être écarté ;<br/>
<br/>
              9. Considérant qu'il ressort des pièces du dossier que le rapport de présentation de la " loi du pays " attaquée a été distribué aux membres de l'assemblée de la Polynésie française le jour de son dépôt, soit le 4 novembre 2015 ; que le projet de " loi du pays " a été soumis au vote le 26 novembre suivant, soit après l'expiration du délai minimum de douze jours suivant la distribution du rapport, prévu à l'article 130 de la loi organique du 27 février 2004 ; qu'ainsi, le moyen tiré de ce que les dispositions de ce dernier article auraient été méconnues manque en fait ;<br/>
<br/>
              En ce qui concerne le moyen d'incompétence négative: <br/>
<br/>
              10. Considérant qu'en prévoyant l'annulation des créances détenues par la Caisse de prévoyance sociale (CPS) sur les employeurs au titre des cotisations mentionnées ci-dessus si ces employeurs justifient de " difficultés financières insurmontables de nature à conduire à la cessation des paiements ", l'article LP 4 de la loi du pays attaquée a suffisamment défini les conditions dans lesquelles le conseil d'administration de la CPS annule les dettes des employeurs de droit privé à son égard ; que, par suite, le moyen tiré de ce que la " loi du pays " attaquée serait entachée sur ce point d'incompétence négative doit être écarté ; <br/>
<br/>
              11. Considérant qu'il résulte de tout ce qui précède que la requête doit être rejetée;<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de la justice administrative :<br/>
<br/>
              12. Considérant que ces dispositions font obstacle à ce que la somme que les requérants demandent soit mise à la charge de la Polynésie française, qui n'est pas la partie perdante dans la présente instance ; que le président de la Polynésie française qui n'a pas eu recours au ministère d'avocat, ne justifie pas de frais spécifiques qu'il aurait exposés à l'occasion de cette instance ; que, par suite, sa demande présentée au titre des mêmes dispositions ne peut qu'être rejetée ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'intervention de la société France Télévisions est admise en tant qu'elle tend à ce que soit déclaré illégal l'article LP 4 de la " loi du pays " n° 2015-18LP/APF. <br/>
Article 2 : La requête du MEDEF et autres est rejetée.<br/>
Article 3 : Les conclusions présentées par le président de la Polynésie française au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au Mouvement des entreprises de France (MEDEF), premier requérant dénommé, au président de la Polynésie française, au président de l'assemblée de la Polynésie française, au haut-commissaire de la République en Polynésie française et à la ministre des outre-mer.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
