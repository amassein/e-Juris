<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000022106899</ID>
<ANCIEN_ID>JG_L_2010_04_000000309480</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/22/10/68/CETATEXT000022106899.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 09/04/2010, 309480, Publié au recueil Lebon</TITRE>
<DATE_DEC>2010-04-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>309480</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Arrighi de Casanova</PRESIDENT>
<AVOCATS>SPINOSI</AVOCATS>
<RAPPORTEUR>Mme Agnès  Fontana</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Boulouis Nicolas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2010:309480.20100409</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 17 septembre et 18 décembre 2007 au secrétariat du contentieux du Conseil d'Etat, présentés pour la COMMUNE DE LEVALLOIS-PERRET, représentée par son maire ; la commune demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 07PA00558 et 07PA00559 du 5 juillet 2007 par lequel la cour administrative d'appel de Paris a rejeté sa requête tendant à l'annulation du jugement du 29 décembre 2006 par lequel le tribunal administratif  de Paris a déclaré nul l'avenant n° 8 à la convention d'aménagement de la zone d'aménagement concerté " Front de Seine " approuvé par la délibération du conseil municipal du 15 décembre 2003 et rejeté ainsi ses demandes tendant à ce qu'il se borne à prononcer la résiliation de cet avenant, uniquement en ce qui concerne les missions confiées à la société anonyme d'économie mixte d'aménagement, de rénovation et d'équipement de Levallois-Perret (SEMALREP) pour les trois îlots jouxtant la place du 11 novembre 1918 et à ce que cette résiliation ne prenne effet qu'à compter de l'adoption de la délibération du conseil municipal qui approuvera le choix d'un nouvel aménageur ; <br/>
<br/>
              2°) réglant l'affaire au fond, de prononcer la résiliation de l'avenant n° 8 en en limitant et en en différant les effets ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Agnès Fontana, chargée des fonctions de Maître des Requêtes,  <br/>
<br/>
              - les observations de Me Spinosi, avocat de la COMMUNE DE LEVALLOIS-PERRET, <br/>
<br/>
              - les conclusions de M. Nicolas Boulouis, rapporteur public ;<br/>
<br/>
<br/>
              La parole ayant été à nouveau donnée à Me Spinosi, avocat de la COMMUNE DE LEVALLOIS-PERRET ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la COMMUNE DE LEVALLOIS-PERRET a conclu un traité de concession relatif à la zone d'aménagement concerté dite " Front de Seine " ; qu'au cours des opérations d'aménagement, la commune a souhaité étendre les missions de l'aménageur, la société d'économie mixte pour l'aménagement, la rénovation et l'équipement de Levallois-Perret, à trois îlots dits " Collange " initialement situés hors du périmètre de l'opération ; qu'elle a, à cet effet, conclu avec l'aménageur un avenant n° 8 à la convention publique d'aménagement, approuvé par une délibération du conseil municipal du 15 décembre 2003 ; que cette délibération a ensuite été annulée par un jugement du tribunal administratif de Paris du 31 mars 2006, qui a décidé que le vice affectant la délibération entachait de nullité l'avenant et a en conséquence enjoint à la commune, soit d'obtenir de son cocontractant la résolution de l'avenant, soit de saisir le juge du contrat pour qu'il en constate la nullité ; que, saisi en exécution de ce jugement, le tribunal administratif de Paris, par un jugement du 29 décembre 2006, a déclaré nul cet avenant ; que, par l'arrêt attaqué du 5 juillet 2007, la cour administrative d'appel de Paris a rejeté son appel contre ce second jugement ; <br/>
<br/>
              Considérant que la cour a rejeté l'appel de la COMMUNE DE LEVALLOIS-PERRET au motif qu'eu égard à la nature de l'acte annulé par le juge de l'excès de pouvoir, lequel était nécessaire pour que l'avenant ait pu se former, le tribunal administratif de Paris, saisi de conclusions à cette fin, ne pouvait que déclarer nul cet avenant ; que si, contrairement à ce que soutient la commune, elle a ainsi suffisamment motivé son arrêt, elle a, ce faisant, commis une erreur de droit, dès lors qu'il appartient en principe au juge du contrat d'apprécier, en fonction de la nature du vice ayant conduit à l'annulation de l'acte détachable du contrat et de son éventuelle régularisation, les conséquences de cette annulation sur la continuité ou la validité du contrat ;<br/>
<br/>
              Considérant toutefois que, saisi d'une demande en exécution de son jugement du 31 mars 2006, dont ni la commune, ni la société d'aménagement n'avaient fait appel, le tribunal administratif de Paris devait assurer l'exécution de la chose jugée, laquelle s'attache tant au dispositif qu'aux motifs qui en sont le support nécessaire, sans pouvoir remettre en cause les mesures décidées par son premier jugement, d'où il ressortait, ainsi qu'il a été dit, que le vice justifiant l'annulation de la délibération du conseil municipal de Levallois-Perret approuvant l'avenant n° 8, entachait de nullité cet avenant et qu'il était fait injonction à la commune, soit d'obtenir de son cocontractant la résolution de l'avenant, soit de saisir le juge du contrat pour qu'il en constate la nullité ; que ce motif, dont l'examen n'implique l'appréciation d'aucune circonstance de fait, doit être substitué au motif erroné en droit retenu par l'arrêt attaqué, dont il justifie le dispositif ; <br/>
<br/>
              Considérant qu'il résulte de ce qui précède que le pourvoi de la COMMUNE DE LEVALLOIS PERRET doit être rejeté ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la COMMUNE DE LEVALLOIS PERRET est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la COMMUNE DE LEVALLOIS PERRET et à la société d'économie mixte pour l'aménagement, la rénovation et l'équipement de Levallois-Perret.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-08-03-02 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. POUVOIRS ET OBLIGATIONS DU JUGE. POUVOIRS DU JUGE DU CONTRAT. - JUGE DU CONTRAT SAISI EN EXÉCUTION D'UNE DÉCISION D'ANNULATION D'UN ACTE DÉTACHABLE DU CONTRAT - CAS PARTICULIER OÙ EST DEVENUE DÉFINITIVE UNE DÉCISION D'ANNULATION ENJOIGNANT LA SAISINE DU JUGE DU CONTRAT POUR CONSTATER LA NULLITÉ DE CE DERNIER [RJ1].
</SCT>
<ANA ID="9A"> 39-08-03-02 Délibération autorisant la signature d'un avenant à un contrat annulée par le juge de l'excès le pouvoir. Le jugement prévoit que le vice affectant cette délibération entache de nullité l'avenant et enjoint en conséquence à la collectivité contractante, soit d'obtenir de son cocontractant la résolution de l'avenant, soit de saisir le juge du contrat pour qu'il en constate la nullité. Ce jugement n'est frappé d'appel par aucune des parties. Le juge du contrat, saisi en exécution de ce jugement, déclare l'avenant nul en considérant qu'eu égard à la nature de l'acte annulé par le juge de l'excès de pouvoir, lequel était nécessaire pour que l'avenant ait pu se former, il ne pouvait que déclarer nul cet avenant. Il commet, ce faisant, une erreur de droit puisqu'il appartient en principe au juge du contrat d'apprécier, en fonction de la nature du vice ayant conduit à l'annulation de l'acte détachable du contrat et de son éventuelle régularisation, les conséquences de cette annulation sur la continuité ou la validité du contrat. Cependant, dans la configuration très particulière de cette affaire, le juge du contrat, saisi d'une demande d'exécution de la décision non contestée du juge de l'excès de pouvoir, devait assurer l'exécution de la chose jugée s'attachant tant au dispositif qu'aux motifs qui en sont le support nécessaire, sans pouvoir remettre en cause les mesures décidées par le premier jugement qui avait considéré que le vice justifiant l'annulation de la délibération entachait de nullité l'avenant et avait fait injonction à la commune, soit d'obtenir de son cocontractant la résolution de l'avenant, soit de saisir le juge du contrat pour qu'il en constate la nullité.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., quant à l'étendue des pouvoirs du juge de l'exécution, 3 mai 2004, Magnat, n° 250730, T. pp. 838-841 ; 23 novembre 2005, Société Eiffage TP, n° 271329, p. 524. Cf. décision du même jour, Commune de Levallois-Perret, n° 309481, inédite au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
