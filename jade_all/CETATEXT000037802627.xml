<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037802627</ID>
<ANCIEN_ID>JG_L_2018_11_000000414539</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/80/26/CETATEXT000037802627.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 30/11/2018, 414539, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-11-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>414539</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CARBONNIER</AVOCATS>
<RAPPORTEUR>Mme Cécile Renault</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:414539.20181130</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. G...A...a demandé au tribunal administratif de Rouen d'annuler pour excès de pouvoir l'arrêté du 24 décembre 2015 par lequel le préfet de l'Eure a rejeté sa demande de titre de séjour, l'a obligé à quitter le territoire français dans un délai de trente jours et a fixé le pays de destination. Par un jugement n° 1602118 du 20 octobre 2016, le tribunal administratif de Rouen a rejeté cette demande. <br/>
<br/>
              Par un arrêt n° 16DA02534 du 27 avril 2017, la cour administrative d'appel de Douai a rejeté l'appel de M. A...contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 25 septembre et 22 décembre 2017 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 600 euros à verser à Me Carbonnier, son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la convention internationale relative aux droits de l'enfant ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code pénal ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Renault, auditeur,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Carbonnier, avocat de M.A....<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 24 décembre 2015, le préfet de l'Eure a rejeté la demande de titre de séjour de M.A..., de nationalité togolaise, lui a fait l'obligation de quitter le territoire français et fixé le pays de destination ; que M. A...se pourvoit en cassation contre l'arrêt du 27 avril 2017 par lequel la cour administrative d'appel de Douai a rejeté son appel dirigé contre le jugement du 20 octobre 2016 par lequel le tribunal administratif de Rouen a rejeté sa demande tendant à l'annulation pour excès de pouvoir de l'arrêté du 24 décembre 2015 ; <br/>
<br/>
              2. Considérant, en premier lieu, qu'aux termes de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " 1° Toute personne a droit au respect de  sa vie privée et familiale (...). Il ne peut y avoir ingérence d'une autorité publique dans l'exercice de ce droit que pour autant que cette ingérence est prévue par la loi et qu'elle constitue une mesure qui, dans une société démocratique, est nécessaire à la sécurité nationale, à la sûreté publique, au bien-être économique du pays, à la défense de l'ordre et à la prévention des infractions pénales, à la protection de la santé ou de la morale, ou à la protection des droits et libertés d'autrui " ; qu'aux termes de l'article 3, paragraphe 1, de la convention internationale relative aux droits de l'enfant : " Dans toutes les décisions qui concernent les enfants, qu'elles soient le fait (...) des tribunaux, des autorités administratives (...), l'intérêt supérieur des enfants doit être une considération primordiale " ; qu'aux termes de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile, dans sa rédaction applicable au litige : " Sauf si sa présence constitue une menace pour l'ordre public, la carte de séjour temporaire portant la mention " vie privée et familiale " est délivrée de plein droit : (...) / 7° A l'étranger (...) dont les liens personnels et familiaux en France, appréciés notamment au regard de leur intensité, de leur ancienneté et de leur stabilité, des conditions d'existence de l'intéressé, de son insertion dans la société française ainsi que de la nature de ses liens avec la famille restée dans le pays d'origine, sont tels que le refus d'autoriser son séjour porterait à son droit au respect de sa vie privée et familiale une atteinte disproportionnée au regard des motifs du refus (...) " ;<br/>
<br/>
              3. Considérant que, pour estimer que l'arrêté préfectoral attaqué n'avait pas méconnu les stipulations et dispositions citées au point précédent, la cour administrative d'appel de Douai a relevé que M.A..., entré irrégulièrement en France en 2012, soutenait avoir une liaison depuis 2014 avec Mme C...B..., ressortissante togolaise, titulaire d'une carte de résident, que de cette union était née en 2015 l'enfantD..., que le couple élevait également la fille de MmeB..., E...Mavoungoudh, née en France 2013 d'un père français ; que la cour a également relevé que, si Mme B...résidait en France depuis 1999, y disposait d'attaches familiales et y travaillait ponctuellement en intérim, d'une part, en admettant même que la communauté de vie entre M. A...et Mme B...ait débuté en mars 2014 comme ils le prétendaient, cette communauté de vie était récente, d'autre part, que rien ne s'opposait à ce que la cellule familiale se recompose au Togo dès lors qu'il n'était établi ni que le père de l'enfant E...participerait effectivement à son entretien et son éducation, ni que les soins requis par l'état de santé de MmeB..., atteinte d'un diabète, et la jeuneD..., porteuse du gène de la drépanocytose, ne pouvaient pas être assurés au Togo, ni enfin que M. A...n'y avait plus d'attaches familiales ; qu'en statuant ainsi, et alors même qu'un jugement du 30 janvier 2015 du juge aux affaires familiales du tribunal de grande instance d'Evreux, revêtu de l'autorité de chose jugée, reconnaît un droit de visite au père de la jeuneE..., la cour n'a ni commis d'erreur de droit, dès lors que ce jugement ne fait, par lui-même, pas obstacle à ce que Mme B...établisse avec sa fille E...sa résidence hors de France, ni inexactement qualifié les faits de l'espèce ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que M. A...n'est pas fondé à demander l'annulation de l'arrêt de la cour administrative de Douai qu'il attaque ; que, par suite, ses conclusions tendant à l'application des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ne peuvent qu'être rejetées ;  <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. F...A...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
