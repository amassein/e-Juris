<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025947502</ID>
<ANCIEN_ID>JG_L_2012_05_000000357694</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/94/75/CETATEXT000025947502.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 30/05/2012, 357694</TITRE>
<DATE_DEC>2012-05-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>357694</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Jacques Arrighi de Casanova</PRESIDENT>
<AVOCATS/>
<RAPPORTEUR>Mme Paquita Morellet-Steiner</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:357694.20120530</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu l'ordonnance n° 11MA01966 QPC du 13 mars 2012, enregistrée le 19 mars 2012 au secrétariat du contentieux du Conseil d'Etat, par laquelle le président de la 7ème chambre de la cour administrative d'appel de Marseille, avant qu'il soit statué sur l'appel de M. Denis A, tendant à l'annulation du jugement n° 1002942 du 18 février 2011 par lequel le tribunal administratif de Toulon l'a condamné, d'une part, à payer une amende de 3 000 euros pour occupation irrégulière du domaine public maritime et, d'autre part, à libérer et remettre en état les lieux dans un délai de 30 jours à compter de la notification du jugement et à peine de 50 euros d'astreinte par jour de retard, a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution des dispositions de l'article L. 2132-3 du code général de la propriété des personnes publiques ;<br/>
<br/>
              Vu le mémoire, enregistré le 16 novembre 2011 au greffe de la cour administrative d'appel de Marseille, présenté par M. Denis A, demeurant ..., en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel ; <br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule et son article 61-1 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
              Vu le code général de la propriété des personnes publiques, notamment  son article L. 2132-3 ; <br/>
<br/>
              Vu le code de justice administrative ;		<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Paquita Morellet-Steiner, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              Considérant que l'article L. 2132-3 du code général de la propriété des personnes publiques dispose : "  Nul ne peut bâtir sur le domaine public maritime ou y réaliser quelque aménagement ou quelque ouvrage que ce soit sous peine de leur démolition, de confiscation des matériaux et d'amende. / Nul ne peut en outre, sur ce domaine, procéder à des dépôts ou à des extractions, ni se livrer à des dégradations. " ;<br/>
<br/>
              Considérant, en premier lieu, que M. A soutient qu'en permettant, dans l'interprétation que leur a donnée la jurisprudence du Conseil d'Etat, de poursuivre aussi bien la personne qui a irrégulièrement édifié un ouvrage sur le domaine public maritime naturel que celle qui, l'ayant acquis auprès de tiers ou reçu par transmission, n'en est que l'occupante ou la gardienne, ces dispositions méconnaissent le principe de légalité des peines, la règle d'interprétation stricte de la loi pénale et le principe de personnalité des peines en vertu duquel nul n'est punissable que de son propre fait, qui découlent de l'article 8 de la Déclaration des droits de l'homme et du citoyen ;<br/>
<br/>
              Considérant que les dispositions de l'article L. 2132-3 du code général de la propriété des personnes publiques tendent à assurer, au moyen de l'action domaniale qu'elles instituent, la remise du domaine public maritime naturel dans un état conforme à son affectation publique en permettant aux autorités chargées de sa protection d'ordonner au propriétaire d'un bien irrégulièrement construit, qu'il l'ait ou non édifié lui-même, sa démolition, ou de confisquer des matériaux ; qu'elles n'ont, dans cette mesure, ni pour objet ni pour effet d'infliger une sanction ayant le caractère d'une punition au sens de l'article 8 de la Déclaration des droits de l'homme et du citoyen ; que, par suite, les moyens tirés de ce que, en tant qu'elles habilitent les autorités publiques à mettre en oeuvre l'action domaniale, ces dispositions contreviendraient à cet article sont inopérants ; <br/>
<br/>
              Considérant, toutefois, que ces dispositions habilitent également les autorités publiques à mettre en oeuvre une action répressive, consistant dans le prononcé par le tribunal administratif d'une amende sanctionnant l'atteinte portée au domaine public maritime naturel par des constructions irrégulières ; que cette amende constitue une sanction ayant le caractère d'une punition, au sens de l'article 8 de la Déclaration des droits de l'homme et du citoyen ;<br/>
<br/>
              Considérant, d'une part, qu'il résulte de l'article L. 2132-3 du code général de la propriété des personnes publiques, tel qu'interprété par la jurisprudence du Conseil d'Etat, que l'amende instituée par ces dispositions peut être infligée à toute personne ayant la garde d'un bien irrégulièrement construit sur le domaine public maritime, qu'elle l'ait ou non édifié elle-même ; qu'ainsi, tant les éléments constitutifs de l'infraction que la personne qui peut en être tenue, sur le plan répressif, responsable sont définis avec suffisamment de précision et de clarté ; que, par suite, le moyen tiré de ce que cet article méconnaîtrait le principe de légalité des délits et des peines qui résulte de l'article 8 de la Déclaration des droits de l'homme et du citoyen ne peut être regardé comme sérieux ;<br/>
<br/>
              Considérant, d'autre part, qu'il résulte des dispositions combinées des articles L. 2132-2, L. 2132-3 et L. 2132-27 du même code que l'atteinte portée au domaine public maritime naturel par une construction irrégulière, que l'amende vise à prévenir et à réprimer, constitue une infraction matérielle dont le caractère continu permet de condamner le propriétaire, qu'il ait ou non construit l'édifice irrégulièrement implanté sur le domaine public ; que, par suite, M. A n'est pas fondé à soutenir que les dispositions contestées méconnaissent le principe en vertu duquel nul n'est punissable du fait d'autrui ;<br/>
<br/>
              Considérant, en deuxième lieu, que si M. A soutient que les poursuites engagées à son encontre sur le fondement des dispositions de l'article L. 2132-3 du code général de la propriété des personnes publiques seraient dépourvues de base légale, faute pour les autorités préfectorales d'avoir délimité le domaine public maritime naturel, ce moyen, qui n'est pas tiré de ce qu'une disposition législative porterait atteinte à un droit ou à une liberté garantis par la Constitution, n'est, en tout état de cause, pas invocable à l'appui de la question prioritaire de constitutionnalité qu'il soulève ; <br/>
<br/>
              Considérant, en troisième lieu, que les dispositions critiquées, qui interdisent en principe l'édification ou le maintien d'aménagements ou de constructions non compatibles avec l'affectation publique du rivage de la mer et exposent celui qui y procède ou en a la garde à la démolition de ses installations, ne portent pas d'atteinte excessive au droit de propriété, auquel le législateur a, lorsqu'il s'exerce sur le domaine public maritime naturel, fixé des bornes justifiées au regard de l'exigence constitutionnelle, résidant dans les droits et libertés des personnes à l'usage desquelles il est affecté, qui s'attache à la protection de ce domaine et que met en oeuvre l'obligation faite aux autorités qui en sont chargées de poursuivre le propriétaire d'un ouvrage irrégulièrement édifié ou maintenu afin qu'il procède à sa démolition ; <br/>
<br/>
              Considérant, enfin, que M. A fait valoir que les dispositions contestées, en permettant de poursuivre aussi bien celui qui a édifié l'ouvrage irrégulier que celui qui, après en avoir acquis la propriété sans l'avoir construit, l'a maintenu, traiteraient de la même manière des personnes placées dans des situations différentes ; que, toutefois, au regard de l'atteinte portée au domaine public maritime naturel, qui constitue, ainsi qu'il a été dit, une infraction matérielle à caractère continu, celui qui a édifié l'ouvrage irrégulier et celui qui, après en avoir acquis la propriété, l'a maintenu illégalement sur le domaine, sont dans la même situation et peuvent ainsi faire l'objet des mêmes poursuites ; qu'au demeurant, le principe d'égalité n'implique pas que des personnes placées dans des situations différentes soient traitées de façon différente ; que le moyen tiré de la violation du principe d'égalité devant la loi ne peut donc être regardé comme présentant un caractère sérieux ;   <br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède qu'il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité transmise par le président de la 7ème chambre de la cour administrative d'appel de Marseille.<br/>
Article 2 : La présente décision sera notifiée à M. Denis A, au Premier ministre et à la ministre de l'écologie, du développement durable et de l'énergie.<br/>
Copie en sera adressée au Conseil constitutionnel et à la cour administrative d'appel de Marseille. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">24-01-03-01 DOMAINE. DOMAINE PUBLIC. PROTECTION DU DOMAINE. CONTRAVENTIONS DE GRANDE VOIRIE. - ARTICLE L. 2132-3 DU CG3P RELATIF À LA PROTECTION DU DOMAINE PUBLIC MARITIME - QPC [RJ1] - 1) MOYEN TIRÉ DE LA MÉCONNAISSANCE DU PRINCIPE DE LÉGALITÉ DES DÉLITS ET DES PEINES - ABSENCE DE CARACTÈRE SÉRIEUX, COMPTE TENU DE L'INTERPRÉTATION PAR LE CONSEIL D'ETAT DE CET ARTICLE - 2) MOYEN TIRÉ DE LA MÉCONNAISSANCE DU PRINCIPE DE PERSONNALITÉ DES PEINES - ABSENCE DE CARACTÈRE SÉRIEUX, EU ÉGARD AU CARACTÈRE CONTINU DE L'INFRACTION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-10-05-04-02 PROCÉDURE. - ARTICLE L. 2132-3 DU CG3P RELATIF À LA PROTECTION DU DOMAINE PUBLIC MARITIME - QPC [RJ1] - 1) MOYEN TIRÉ DE LA MÉCONNAISSANCE DU PRINCIPE DE LÉGALITÉ DES DÉLITS ET DES PEINES - ABSENCE DE CARACTÈRE SÉRIEUX, COMPTE TENU DE L'INTERPRÉTATION PAR LE CONSEIL D'ETAT DE CET ARTICLE - 2) MOYEN TIRÉ DE LA MÉCONNAISSANCE DU PRINCIPE DE PERSONNALITÉ DES PEINES - ABSENCE DE CARACTÈRE SÉRIEUX, EU ÉGARD AU CARACTÈRE CONTINU DE L'INFRACTION.
</SCT>
<ANA ID="9A"> 24-01-03-01 Contestation par une QPC des dispositions de l'article L. 2132-3 du code général de la propriété des personnes publiques (CG3P) relatif à la protection du domaine public maritime.,,1) Il résulte de cet article, tel qu'interprété par la jurisprudence du Conseil d'Etat, que l'amende instituée par ces dispositions peut être infligée à toute personne ayant la garde d'un bien irrégulièrement construit sur le domaine public maritime, qu'elle l'ait ou non édifié elle-même. Ainsi, tant les éléments constitutifs de l'infraction que la personne qui peut en être tenue, sur le plan répressif, responsable sont définis avec suffisamment de précision et de clarté. Par suite, le moyen tiré de ce que cet article méconnaîtrait le principe de légalité des délits et des peines ne peut être regardé comme sérieux.,,2) Il résulte des dispositions combinées des articles L. 2132-2, L. 2132-3 et L. 2132-27 du CG3P que l'atteinte portée au domaine public maritime naturel par une construction irrégulière, que l'amende vise à prévenir et à réprimer, constitue une infraction matérielle dont le caractère continu permet de condamner la personne responsable, qu'elle ait ou non construit l'édifice irrégulièrement implanté sur le domaine public. Par suite, n'est pas sérieux le moyen tiré de ce que les dispositions contestées méconnaîtraient le principe en vertu duquel nul n'est punissable du fait d'autrui.</ANA>
<ANA ID="9B"> 54-10-05-04-02 Contestation par une QPC des dispositions de l'article L. 2132-3 du code général de la propriété des personnes publiques (CG3P) relatif à la protection du domaine public maritime.,,1) Il résulte de cet article, tel qu'interprété par la jurisprudence du Conseil d'Etat, que l'amende instituée par ces dispositions peut être infligée à toute personne ayant la garde d'un bien irrégulièrement construit sur le domaine public maritime, qu'elle l'ait ou non édifié elle-même. Ainsi, tant les éléments constitutifs de l'infraction que la personne qui peut en être tenue, sur le plan répressif, responsable sont définis avec suffisamment de précision et de clarté. Par suite, le moyen tiré de ce que cet article méconnaîtrait le principe de légalité des délits et des peines ne peut être regardé comme sérieux.,,2) Il résulte des dispositions combinées des articles L. 2132-2, L. 2132-3 et L. 2132-27 du CG3P que l'atteinte portée au domaine public maritime naturel par une construction irrégulière, que l'amende vise à prévenir et à réprimer, constitue une infraction matérielle dont le caractère continu permet de condamner la personne responsable, qu'elle ait ou non construit l'édifice irrégulièrement implanté sur le domaine public. Par suite, n'est pas sérieux le moyen tiré de ce que les dispositions contestées méconnaîtraient le principe en vertu duquel nul n'est punissable du fait d'autrui.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., pour des QPC portant sur le même article au regard d'autres droits et libertés garantis par la Constitution, CE, 10 juin 2010, Muntoni, n° 341537, T. pp. 763, 952 ; CE, 7 mars 2012, Tomaselli, n° 355009, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
