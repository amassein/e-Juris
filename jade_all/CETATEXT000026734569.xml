<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026734569</ID>
<ANCIEN_ID>JG_L_2012_12_000000343421</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/73/45/CETATEXT000026734569.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 06/12/2012, 343421</TITRE>
<DATE_DEC>2012-12-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>343421</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>M. Didier Ribes</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne Von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:343421.20121206</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 21 septembre 2010 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre d'Etat, ministre de l'écologie, de l'énergie, du développement durable et de la mer, en charge des technologies vertes et des négociations sur le climat ; le ministre demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09PA01835 du 14 juin 2010 par lequel la cour administrative d'appel de Paris, à la demande de la société Fauna et Films, a, d'une part, annulé le jugement n° 0601489/2 du 26 décembre 2008 du tribunal administratif de Melun et la décision du 17 août 2004 par laquelle la direction régionale de l'environnement d'Ile-de-France a rejeté la demande de cette société tendant à la délivrance d'un certificat intracommunautaire de circulation d'un faucon gerfaut et, d'autre part, condamné l'État à payer à cette société la somme de 3 490 euros hors taxes au titre du préjudice subi du fait de la décision du 17 août 2004 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la société Fauna et Films ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le règlement (CE) n° 338/97 du Conseil du 9 décembre 1996 ;<br/>
<br/>
              Vu le règlement (CE) n° 1801/2001 de la Commission du 30 août 2001 ;<br/>
<br/>
              Vu le code de l'environnement ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier Ribes, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Gaschignard, avocat de la société Fauna et Films,<br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Gaschignard, avocat de la société Fauna et Films ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des énonciations de l'arrêt attaqué que, par une décision du 17 août 2004, la direction régionale de l'environnement d'Île-de-France a rejeté une demande de certificat de circulation dans la Communauté européenne portant sur un spécimen vivant de faucon gerfaut, espèce protégée inscrite à l'annexe A du règlement n° 338/97 du Conseil du 9 décembre 1996 relatif à la protection des espèces de faune et de flore sauvages par le contrôle de leur commerce, présentée par la société Fauna et Films, qui avait le projet de transporter cet animal au Royaume-Uni à des fins de location pour les besoins du tournage d'un film ; que, par un jugement du 26 décembre 2008, le tribunal administratif de Melun a rejeté la demande de cette société tendant à la réparation par l'Etat du préjudice que lui aurait causé le refus illégal de lui délivrer le certificat demandé ; que, par un arrêt du 14 juin 2010, contre lequel le ministre d'Etat, ministre de l'écologie, de l'énergie, du développement durable et de la mer, en charge des technologies vertes et des négociations sur le climat se pourvoit en cassation, la cour administrative d'appel de Paris a, d'une part, annulé ce jugement et la décision du 17 août 2004 et, d'autre part, condamné l'État à payer à la société Fauna et Films la somme de 3 490 euros hors taxes ;<br/>
<br/>
              Sur le pourvoi du ministre d'Etat, ministre de l'écologie, de l'énergie, du développement durable et de la mer, en charge des technologies vertes et des négociations sur le climat :<br/>
<br/>
              2. Considérant, en premier lieu, que la société Fauna et Films n'a présenté devant les juges du fond que des conclusions tendant à être indemnisée du préjudice causé par la décision du 17 août 2004 ; qu'en prononçant l'annulation de cette décision, la cour administrative d'appel de Paris a statué au-delà des conclusions dont elle était saisie ; que le ministre est, par suite, fondé à demander l'annulation de l'arrêt qu'il attaque en tant qu'il annule la décision du 17 août 2004 ; <br/>
<br/>
              3. Considérant, en second lieu, qu'aux termes du paragraphe 1 de l'article 4 du règlement du Conseil du 9 décembre 1996 mentionné ci-dessus : " L'introduction dans la Communauté de spécimens d'espèces inscrites à l'annexe A est subordonnée à la réalisation des vérifications nécessaires et à la présentation préalable, au bureau de douane frontalier d'introduction d'un permis d'importation délivré par un organe de gestion de l'État membre de destination. / Ce permis d'importation ne peut être qu'en accord avec les restrictions imposées au titre du paragraphe 6 et lorsque les conditions suivantes sont remplies : / a) l'autorité scientifique compétente, prenant en considération tout avis du groupe d'examen scientifique est d'avis que l'introduction dans la Communauté:/ i) ne nuirait pas à l'état de conservation de l'espèce ou à l'étendue du territoire occupé par la population de l'espèce concernée ; / ii) s'effectue : - dans l'un des objectifs visés à l'article 8 paragraphe 3 points e), f) et g)/ ou / - à d'autres fins ne nuisant pas à la survie de l'espèce concernée ; (...) d) l'organe de gestion s'est assuré que le spécimen ne sera pas utilisé à des fins principalement commerciales ; (...) " ;<br/>
<br/>
              4. Considérant que l'article 8 du même règlement, qui est relatif au contrôle des activités commerciales concernant certaines espèces protégées, dispose : " 1. Il est interdit d'acheter, d'acquérir à des fins commerciales, d'exposer à des fins commerciales, d'utiliser dans un but lucratif et de vendre, de détenir pour la vente, de mettre en vente ou de transporter pour la vente des spécimens d'espèces inscrites à l'annexe A. (...) / 3. Conformément aux exigences des autres actes législatifs communautaires relatifs à la conservation de la faune et de la flore sauvages, il peut être dérogé aux interdictions prévues au paragraphe 1 à condition d'obtenir de l'organe de gestion de l'Etat membre dans lequel les spécimens se trouvent un certificat à cet effet, délivré cas par cas, lorsque les spécimens : / (...) c) ont été introduits dans la Communauté conformément aux dispositions du présent règlement et sont destinés à être utilisés à des fins ne nuisant pas à la survie de l'espèce concernée (...) " ; <br/>
<br/>
              5. Considérant qu'il résulte de la combinaison des dispositions rappelées aux points 3 et 4 qu'une dérogation à l'interdiction de principe de commercialisation de certaines espèces protégées édictée par le paragraphe 1 de l'article 8 du règlement ne peut être légalement accordée sur le fondement du c) de son paragraphe 3 que si la décision initiale autorisant l'entrée du ou des spécimens dont s'agit sur le territoire de la Communauté européenne ou de l'Union européenne permet, compte tenu des termes dans lesquels elle est rédigée, une utilisation commerciale ultérieure et sous réserve qu'elle ne nuise pas à la survie de l'espèce concernée ; <br/>
<br/>
              6. Considérant qu'il résulte des énonciations de l'arrêt attaqué que la cour a jugé que l'administration, ainsi que cette dernière l'indiquait dans ses écritures en défense, s'était prononcée sur une demande d'autorisation dérogatoire de commercialisation d'un spécimen d'une espèce protégée sur le fondement de l'article 8 du règlement du 9 décembre 1996 ; que, pour faire application des dispositions du c) du paragraphe 3 de l'article 8, la cour a relevé qu'il n'était pas établi que le motif de la demande présentée par la société Fauna et Films pouvait nuire à la survie de l'espèce concernée ; qu'en statuant ainsi, alors qu'il lui appartenait, ainsi qu'il a été dit au point 5, de s'assurer également que la décision initiale d'importation permettait une exploitation commerciale ultérieure, la cour a entaché son arrêt d'erreur de droit ; que le ministre est fondé, par suite, à demander l'annulation de l'arrêt en tant qu'il statue sur les conclusions indemnitaires présentées par la société Fauna et Films ; <br/>
<br/>
              7. Considérant qu'il résulte de ce qui a été dit ci-dessus, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que le ministre d'Etat, ministre de l'écologie, de l'énergie, du développement durable et de la mer, en charge des technologies vertes et des négociations sur le climat est fondé à demander l'annulation de l'arrêt qu'il attaque ; <br/>
<br/>
              8. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Sur la requête d'appel de la société Fauna et Films :<br/>
<br/>
              9. Considérant qu'aux termes de l'article 9 du règlement du Conseil du 9 décembre 1996 : " 1. Toute circulation dans la Communauté d'un spécimen vivant d'une espèce inscrite à l'annexe A par rapport à l'emplacement indiqué dans le permis d'importation ou dans tout certificat délivré au titre du présent règlement est subordonné à l'autorisation préalable d'un organe de gestion de l'Etat membre dans lequel se trouve le spécimen (...) / 2. Cette autorisation : / a) ne peut être accordée que si l'autorité scientifique compétente de l'Etat membre ou, lorsque le déplacement s'effectue vers un autre Etat membre, l'autorité scientifique compétente de cet autre État, s'est assurée que le lieu d'hébergement prévu sur le lieu de destination d'un spécimen vivant est équipé de manière adéquate pour le conserver et le traiter avec soin ; / b) doit être confirmée par la délivrance d'un certificat/ et / c) est, le cas échéant, communiquée immédiatement à un organe de gestion de l'Etat membre dans lequel le spécimen doit être placé (...) " ; <br/>
<br/>
              10. Considérant qu'il résulte de l'instruction que la société Fauna et Films a présenté à l'administration une demande tendant à la délivrance non pas d'une autorisation dérogatoire de commercialisation d'un spécimen d'une espèce protégée sur le fondement de l'article 8 du règlement du 9 décembre 1996, mais d'un certificat de circulation intracommunautaire au titre de l'article 9 du même règlement ; qu'en relevant, pour rejeter la demande dont il était saisi, que le faucon avait été " importé initialement dans un but d'élevage  en captivité ou de reproduction artificielle " et que le thème du film au tournage duquel il devait participer n'était " pas axé sur l'intérêt pédagogique ou scientifique de l'espèce ", l'auteur de la décision attaquée s'est fondé sur des motifs étrangers à ceux limitativement énumérés à l'article 9 du règlement et seuls de nature à justifier légalement un tel refus et a ainsi commis une erreur de droit ; <br/>
<br/>
              11. Considérant, toutefois, que la seule délivrance d'un certificat de circulation intracommunautaire sur le fondement de l'article 9 du règlement n'aurait pas permis d'exploiter commercialement le spécimen dont s'agit ; qu'il ne résulte pas de l'instruction et qu'il n'est d'ailleurs pas soutenu que la société Fauna et Films aurait présenté une demande d'autorisation de commercialisation sur le fondement de l'article 8 ; qu'au demeurant, il ressort des mentions portées sur la décision initiale autorisant  l'importation de ce spécimen dans la Communauté européenne et notamment de celles de sa rubrique 14  (code objet " B ") qu'elle n'a été accordée qu'à des fins d'" élevage en captivité ou reproduction artificielle " ; que, par suite, le refus illégal de délivrance d'un certificat de circulation intracommunautaire opposé le 17 août 2004 est dépourvu de lien direct avec le préjudice invoqué par la société Fauna et Films et résultant de l'impossibilité d'exploitation commerciale de l'animal concerné ; <br/>
<br/>
              12. Considérant qu'il résulte de tout ce qui précède que la société Fauna et Films n'est pas fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Melun a rejeté sa demande ; <br/>
<br/>
              13. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de l'État qui n'est pas, dans la présente instance, la partie perdante,  la somme que demande la société Fauna et Films au titre des frais exposés par elle et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>
<br/>		D E C I D E :<br/>
              		--------------<br/>
<br/>
 Article 1er : L'arrêt de la cour administrative d'appel de Paris du 14 juin 2010 est annulé. <br/>
<br/>
 Article 2 : La requête présentée par la société Fauna et Films devant la cour administrative d'appel de Paris est rejetée.<br/>
<br/>
 Article 3 : Les conclusions présentées par la société Fauna et Films au titre de l'article L. 761-1   du code de justice administrative sont rejetées. <br/>
<br/>
 Article 4 : La présente décision sera notifiée à la ministre de l'écologie, du développement    durable et de l'énergie et à la société Fauna et Films.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">15-05-10 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. RÈGLES APPLICABLES. ENVIRONNEMENT. - RÈGLEMENT N° 338/97 DU CONSEIL DU 9 DÉCEMBRE 1996 RELATIF À LA PROTECTION DES ESPÈCES DE FAUNE ET DE FLORE SAUVAGES PAR LE CONTRÔLE DE LEUR COMMERCE - INTERDICTION DE COMMERCIALISATION DES ESPÈCES MENTIONNÉES À L'ANNEXE A (ART. 8, PARAGRAPHE 1) - DÉROGATIONS ACCORDÉES SUR LE FONDEMENT DU C DU PARAGRAPHE 3 - CONDITIONS D'OCTROI - DÉCISION INITIALE D'IMPORTATION DU SPÉCIMEN PERMETTANT UNE COMMERCIALISATION ULTÉRIEURE - ABSENCE D'ATTEINTE À LA SURVIE DE L'ESPÈCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">44-045-06-01 NATURE ET ENVIRONNEMENT. - RÈGLEMENT N° 338/97 DU CONSEIL DU 9 DÉCEMBRE 1996 RELATIF À LA PROTECTION DES ESPÈCES DE FAUNE ET DE FLORE SAUVAGES PAR LE CONTRÔLE DE LEUR COMMERCE - INTERDICTION DE COMMERCIALISATION DES ESPÈCES MENTIONNÉES À L'ANNEXE A (ART. 8, PARAGRAPHE 1) - DÉROGATIONS ACCORDÉES SUR LE FONDEMENT DU C DU PARAGRAPHE 3 - CONDITIONS D'OCTROI - DÉCISION INITIALE D'IMPORTATION DU SPÉCIMEN PERMETTANT UNE COMMERCIALISATION ULTÉRIEURE - ABSENCE D'ATTEINTE À LA SURVIE DE L'ESPÈCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">44-045-06-04 NATURE ET ENVIRONNEMENT. - RÈGLEMENT N° 338/97 DU CONSEIL DU 9 DÉCEMBRE 1996 RELATIF À LA PROTECTION DES ESPÈCES DE FAUNE ET DE FLORE SAUVAGES PAR LE CONTRÔLE DE LEUR COMMERCE - INTERDICTION DE COMMERCIALISATION DES ESPÈCES MENTIONNÉES À L'ANNEXE A (ART. 8, PARAGRAPHE 1) - DÉROGATIONS ACCORDÉES SUR LE FONDEMENT DU C DU PARAGRAPHE 3 - CONDITIONS D'OCTROI - DÉCISION INITIALE D'IMPORTATION DU SPÉCIMEN PERMETTANT UNE COMMERCIALISATION ULTÉRIEURE - ABSENCE D'ATTEINTE À LA SURVIE DE L'ESPÈCE.
</SCT>
<ANA ID="9A"> 15-05-10 Une dérogation à l'interdiction de principe de commercialisation de certaines espèces protégées, édictée par le paragraphe 1 de l'article 8 du règlement n° 338/97 du Conseil du 9 décembre 1996 relatif à la protection des espèces de faune et de flore sauvages par le contrôle de leur commerce, ne peut être légalement accordée sur le fondement du c de son paragraphe 3 que si la décision initiale autorisant l'entrée du ou des spécimens concernés sur le territoire de la Communauté européenne ou de l'Union européenne permet, compte tenu des termes dans lesquels elle est rédigée, une utilisation commerciale ultérieure et sous réserve qu'elle ne nuise pas à la survie de l'espèce concernée.</ANA>
<ANA ID="9B"> 44-045-06-01 Une dérogation à l'interdiction de principe de commercialisation de certaines espèces protégées, édictée par le paragraphe 1 de l'article 8 du règlement n° 338/97 du Conseil du 9 décembre 1996 relatif à la protection des espèces de faune et de flore sauvages par le contrôle de leur commerce, ne peut être légalement accordée sur le fondement du c de son paragraphe 3 que si la décision initiale autorisant l'entrée du ou des spécimens concernés sur le territoire de la Communauté européenne ou de l'Union européenne permet, compte tenu des termes dans lesquels elle est rédigée, une utilisation commerciale ultérieure et sous réserve qu'elle ne nuise pas à la survie de l'espèce concernée.</ANA>
<ANA ID="9C"> 44-045-06-04 Une dérogation à l'interdiction de principe de commercialisation de certaines espèces protégées, édictée par le paragraphe 1 de l'article 8 du règlement n° 338/97 du Conseil du 9 décembre 1996 relatif à la protection des espèces de faune et de flore sauvages par le contrôle de leur commerce, ne peut être légalement accordée sur le fondement du c de son paragraphe 3 que si la décision initiale autorisant l'entrée du ou des spécimens concernés sur le territoire de la Communauté européenne ou de l'Union européenne permet, compte tenu des termes dans lesquels elle est rédigée, une utilisation commerciale ultérieure et sous réserve qu'elle ne nuise pas à la survie de l'espèce concernée.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
