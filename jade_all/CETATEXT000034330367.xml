<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034330367</ID>
<ANCIEN_ID>JG_L_2017_03_000000397644</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/33/03/CETATEXT000034330367.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème chambres réunies, 31/03/2017, 397644, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-03-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>397644</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Dorothée Pradines</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean Lessi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:397644.20170331</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 4 mars et 29 juin 2016 au secrétariat du contentieux du Conseil d'Etat, la société Vitamins, la société Noria et la société Vit'All+ demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du ministre des affaires sociales, de la santé et des droits des femmes du 8 septembre 2015 modifiant l'arrêté du 22 février 1990 portant exonération à la réglementation des substances vénéneuses destinées à la médecine humaine ainsi que la décision implicite de rejet de leur recours gracieux formé le 6 novembre 2015 ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros à verser à chacune d'elles au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le traité sur le fonctionnement de l'Union européenne, notamment son article 34 ;<br/>
              - la directive n° 98/34/CE du Parlement européen et du Conseil du 22 juin 1998 ;<br/>
              - le code de la santé publique ;<br/>
              - l'arrêté du 23 septembre 2011 portant classement sur la liste des substances vénéneuses ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 17 mars 2017, présentée par la ministre des affaires sociales et de la santé ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Dorothée Pradines, auditeur, <br/>
<br/>
              - les conclusions de M. Jean Lessi, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 5132-1 du code de la santé publique : " Sont comprises comme substances vénéneuses : (...) / 4° Les substances inscrites sur la liste I et la liste II définies à l'article L. 5132-6 (...) " ; que, selon cet article : " Les listes I et II mentionnées au 4° de l'article L. 5132-1 comprennent : / (...) 2° Les médicaments susceptibles de présenter directement ou indirectement un danger pour la santé ; / (...) 5° Tout autre produit ou substance présentant pour la santé des risques directs ou indirects. (...) " ; qu'en vertu de l'article R. 5132-1 du même code, les dispositions réglementaires relatives aux médicaments relevant des listes I et II s'appliquent aux médicaments qui renferment une ou plusieurs substances ou préparations classées sur les listes I ou II précitées ; que toutefois, le 1° de l'article R. 5132-2 du même code permet d'en exonérer " les médicaments mentionnés à l'article R. 5132-1 qui sont destinés à la médecine humaine et renferment des substances classées à des doses ou concentrations très faibles ou sont utilisés pendant une durée de traitement très brève. Les formes ou voies d'administration de ces médicaments, leur composition, les doses ou concentrations maximales de substances qu'ils renferment, ainsi que, le cas échéant, la durée maximale du traitement, sont fixées, sur proposition du directeur général de l'Agence nationale de sécurité du médicament et des produits de santé, après avis de l'Académie nationale de pharmacie, par arrêté du ministre chargé de la santé " ; que, sur le fondement de ces dispositions, l'arrêté attaqué a exonéré les produits contenant jusqu'à 1 milligramme par unité de prise de mélatonine, inscrite par un arrêté du 23 septembre 2011 sur la liste II des substances vénéneuses, de la réglementation applicable aux médicaments relevant des listes I et II ; que les sociétés requérantes en demandent l'annulation en tant qu'il n'a pas fixé le seuil d'exonération à 2 milligrammes ;<br/>
<br/>
              2. Considérant que l'article 1er de la directive 98/34/CE du Parlement européen et du Conseil du 22 juin 1998 prévoyant une procédure d'information dans le domaine des normes et réglementations techniques et des règles relatives aux services de la société de l'information, alors en vigueur, définit, à son point 11, une " règle technique " comme " une spécification technique ou autre exigence (...), y compris les dispositions administratives qui s'y appliquent, dont l'observation est obligatoire de jure ou de facto, pour la commercialisation (...) ou l'utilisation dans un État membre ou dans une partie importante de cet État, de même que, sous réserve de celles visées à l'article 10, les dispositions législatives, réglementaires et administratives des États membres interdisant la fabrication, l'importation, la commercialisation ou l'utilisation d'un produit (...) " ; qu'aux termes du paragraphe 1 de l'article 8 de la même directive : " (...) les Etats membres communiquent immédiatement à la Commission tout projet de règle technique, sauf s'il s'agit d'une simple transposition intégrale d'une norme internationale ou européenne, auquel cas une simple information quant à la norme concernée suffit. Ils adressent également à la Commission une notification concernant les raisons pour lesquelles l'établissement d'une telle règle technique est nécessaire, à moins que ces raisons ne ressortent déjà du projet (...) " ;<br/>
<br/>
              3. Considérant que l'arrêté du 23 septembre 2011 qui a classé la mélatonine sur la liste II des substances vénéneuses n'a pas été notifié en tant que règle technique à la Commission européenne ; qu'en fixant à 1 milligramme le dosage en-deçà duquel la mélatonine est exonérée de la règlementation attachée à son inscription sur la liste II des substances vénéneuses, l'arrêté attaqué a pour effet de confirmer la soumission de la commercialisation des produits contenant plus d'1 milligramme de mélatonine à cette réglementation, comportant notamment l'obligation d'une délivrance en pharmacie sur prescription médicale ; que, dans cette mesure, cet arrêté doit être regardé comme constituant une " règle technique ", au sens de la directive 98/34/CE ; qu'il est constant qu'il n'a pas été notifié à la Commission européenne ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens de la requête, les sociétés requérantes sont fondées à en demander l'annulation en tant qu'il n'a pas fixé le seuil d'exonération à 2 milligrammes ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 1 000 euros à verser à chacune des sociétés Vitamins, Noria et Vit'All+, au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : L'arrêté du 8 septembre 2015 modifiant l'arrêté du 22 février 1990 portant exonération à la réglementation des substances vénéneuses destinées à la médecine humaine est annulé en tant qu'il n'a pas fixé à 2 milligrammes le seuil en deçà duquel les produits contenant de la mélatonine sont dispensés de l'application des dispositions des articles R. 5132-1 à R. 5132-26 du code de la santé publique.<br/>
Article 2 : L'Etat versera à chacune des sociétés Vitamins, Noria et Vit'All+ une somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : La présente décision sera notifiée, pour l'ensemble des sociétés requérantes, à la société Vitamins, première dénommée, et à la ministre des affaires sociales et de la santé.<br/>
Copie en sera adressée à l'Agence nationale de sécurité du médicament et des produits de santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
