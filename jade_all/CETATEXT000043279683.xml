<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043279683</ID>
<ANCIEN_ID>JG_L_2021_03_000000437181</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/27/96/CETATEXT000043279683.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 22/03/2021, 437181, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-03-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>437181</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Hervé Cassagnabère</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:437181.20210322</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société par actions simplifiée (SAS) Carrefour Hypermarchés a demandé au tribunal administratif de Bordeaux de prononcer la décharge des cotisations de taxe foncière sur les propriétés bâties auxquelles elle a été assujettie au titre des années 2015 et 2016 à raison d'un établissement situé à Bègles (Gironde). Par un jugement n° 1704253 du 31 octobre 2019, ce tribunal a rejeté cette demande.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 27 décembre 2019 et 27 mars 2020 au secrétariat du contentieux du Conseil d'Etat, la société Carrefour Hypermarchés demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
- le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Hervé Cassagnabère, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la Société Carrefour Hypermarchés ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. La société Carrefour Hypermarchés est propriétaire de locaux au sein du centre commercial " Les Rives d'Arcins " à Bègles (Gironde), à raison desquels elle a été assujettie à la taxe foncière sur les propriétés bâties au titre des années 2015 et 2016. Ces locaux constituent le local-type n°46 du procès-verbal d'évaluation foncière complémentaire n°4 de la commune de Bègles, créé le 5 mars 1996 pour représenter la catégorie des hypermarchés. La valeur locative de cet immeuble a elle-même été déterminée par référence au local-type n°43 figurant sur le procès-verbal complémentaire n° 2 du 18 décembre 1973 de la commune de Bègles, qui correspondait à un supermarché " Mammouth " construit en 1968. La société Carrefour Hypermarchés se pourvoit en cassation contre le jugement du 31 octobre 2019 par lequel le tribunal administratif de Bordeaux a rejeté sa demande tendant à la décharge des impositions auxquelles elle a été assujettie au titre de ces années.<br/>
<br/>
              2. Pour écarter le moyen tiré de l'irrégularité de l'évaluation de l'immeuble litigieux par application de la méthode par comparaison du b du 2° de l'article 1498 du code général des impôts, le tribunal administratif s'est fondé sur ce que le local-type n° 43 était donné en location au 1er janvier 1970. En retenant l'existence d'un bail au 1er janvier 1970 au seul motif qu'il résultait de l'instruction que l'occupant de ce local était une personne autre que son propriétaire et alors que le procès-verbal complémentaire de 1973 portait, s'agissant de cet immeuble, la mention " code-bien 32 ", qui correspond, ainsi que le précise le " bulletin de renseignements " produit par l'administration, à l'application à la surface pondérée d'une valeur unitaire du " type de référence ", c'est-à-dire à une évaluation par voie de comparaison, le tribunal administratif a dénaturé les faits et les pièces du dossier. <br/>
<br/>
              3. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, que la société Carrefour Hypermarchés est fondée à demander l'annulation du jugement qu'elle attaque.<br/>
<br/>
              4. Il y a lieu de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              5. Aux termes de l'article R. 199-1 du livre des procédures fiscales : " L'action doit être introduite devant le tribunal compétent dans le délai de deux mois à partir du jour de la réception de l'avis par lequel l'administration notifie au contribuable la décision prise sur la réclamation (...) ". L'administration fiscale ayant rejeté la réclamation relative à la taxe foncière sur les propriétés bâties due au titre des années 2015 et 2016 par une décision du 19 juillet 2017, notifiée le vendredi 21 juillet 2017, le délai pour présenter une requête devant le tribunal administratif expirait à minuit le vendredi 22 septembre 2017. Par suite, la requête introduite par voie de télécopie le lundi 25 septembre 2017, régularisée le 27 septembre 2017, était tardive. La demande de la société relative aux impositions dues au titre des années 2015 et 2016 doit, par suite, être rejetée.<br/>
<br/>
              6. Les dispositions de l'article L.761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente espèce, la partie perdante.<br/>
              .<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement n° 1704253 du 31 octobre 2019 du tribunal administratif de Bordeaux est annulé. <br/>
 Article 2 : La demande de la société Carrefour Hypermarchés est rejetée.<br/>
Article 3 : les conclusions de la société Carrefour Hypermarchés sur le fondement de l'article  <br/>
L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la société par actions simplifiée Carrefour Hypermarchés et au ministre de l'économie, des finances et de la relance. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
