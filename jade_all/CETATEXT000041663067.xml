<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041663067</ID>
<ANCIEN_ID>JG_L_2020_02_000000429646</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/66/30/CETATEXT000041663067.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 28/02/2020, 429646, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-02-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>429646</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>GOLDMAN ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. François Weil</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:429646.20200228</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1°/ Sous le n° 429646, par une requête, un nouveau mémoire et un mémoire en réplique, enregistrés les 11 avril, 20 septembre et 17 octobre 2019 au secrétariat du contentieux du Conseil d'Etat, Mme B... A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 9 avril 2019 par laquelle la présidente de l'Agence française de lutte contre le dopage l'a suspendue provisoirement, à titre conservatoire, de toutes les activités mentionnées à l'article L. 232-23-4 du code du sport.<br/>
<br/>
              2°) de mettre à la charge de l'Agence française de lutte contre le dopage la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2/° Sous le n° 431499, par une requête et deux mémoires en réplique, enregistrés les 7 juin, 16 septembre et 17 octobre 2019 au secrétariat du contentieux du Conseil d'Etat, Mme A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 25 avril 2019 par laquelle l'Agence française de lutte contre le dopage a prononcé à son encontre une mesure de suspension provisoire à titre conservatoire de toutes les activités mentionnées à l'article L. 232-23-4 du code du sport ;<br/>
<br/>
              2°) de mettre à la charge de l'Agence française de lutte contre le dopage la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le code du sport ;<br/>
              - l'ordonnance n° 2018-1178 du 19 décembre 2018 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Weil, conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Maître Goldman, avocat de Mme A..., et à la SCP Matuchansky, Poupot, Valdelièvre, avocat de l'Agence française de lutte contre le dopage ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1.	Les requêtes visées ci-dessus présentent à juger des questions semblables. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2.	Aux termes de l'article L. 232-9-2 du code du sport : " A l'occasion des opérations de contrôle (...), il est interdit : / 1° De se soustraire au prélèvement d'un échantillon ; / 2° De refuser sans justification valable, après s'être vu notifier le contrôle, de se soumettre au prélèvement d'un échantillon ; / 3° De ne pas se soumettre, intentionnellement ou par négligence, sans justification valable après s'être vu notifier le contrôle, au prélèvement d'un échantillon. " Selon l'article L. 232-23-4 du même code : " Lorsqu'un résultat d'analyse implique une substance interdite ou une méthode interdite, à l'exception d'une substance spécifiée au sens de l'annexe I à la convention internationale mentionnée à l'article L. 230-2, le président de l'Agence française de lutte contre le dopage ordonne à l'encontre du sportif, à titre conservatoire et dans l'attente de la décision de la commission des sanctions, une suspension provisoire : / 1° De la participation directe ou indirecte à l'organisation et au déroulement de toute manifestation sportive donnant lieu à une remise de prix en argent ou en nature, et à des manifestations sportives autorisées par une fédération délégataire ou organisées par une fédération agréée ou par une ligue sportive professionnelle ainsi qu'aux entraînements y préparant organisés par une fédération agréée ou une ligue professionnelle ou l'un des membres de celles-ci ; / 2° De l'exercice des fonctions définies à l'article L. 212-1 ; / 3° De l'exercice des fonctions de personnel d'encadrement ou de toute activité administrative au sein d'une fédération agréée ou d'une ligue professionnelle, ou de l'un des membres de celles-ci ; / 4° De la participation à toute autre activité organisée par une fédération sportive, une ligue professionnelle ou l'un de leurs membres, ou le comité national olympique et sportif français, ainsi qu'aux activités sportives impliquant des sportifs de niveau national ou international et financées par une personne publique, à moins que ces activités ne s'inscrivent dans des programmes ayant pour objet la prévention du dopage. / Lorsque le résultat d'analyse implique une substance spécifiée au sens de l'annexe I à la convention internationale mentionnée à l'article L. 230-2, ou lorsqu'une autre infraction aux dispositions du présent titre est en cause, d'une part, l'intéressé peut accepter la suspension provisoire décrite à l'alinéa précédent dans l'attente de la décision de la commission des sanctions, d'autre part, le président de l'Agence française de lutte contre le dopage peut, de sa propre initiative, ordonner une telle suspension provisoire à l'égard de l'intéressé. / La décision de suspension provisoire est motivée. L'intéressé est convoqué par le président de l'agence, dans les meilleurs délais, pour faire valoir ses observations sur cette mesure. / La durée de la suspension provisoire est déduite de la durée de l'interdiction de participer aux manifestations sportives que la commission des sanctions peut ultérieurement prononcer ".<br/>
<br/>
              3.	Il ressort des pièces du dossier que,  par une décision du 9 avril 2019, la présidente de l'Agence française de lutte contre le dopage (AFLD), se fondant sur la circonstance que Mme A..., à l'occasion d'un contrôle antidopage diligenté par ses services à Marrakech le 27 mars, s'était enfuie lorsque les personnes chargées du contrôle s'étaient présentées à elle et estimant que ce fait était susceptible de constituer une violation des dispositions du 1° de l'article L. 232-9-2 du code du sport, a prononcé à son encontre une mesure de suspension provisoire applicable à l'ensemble des activités mentionnées du 1° au 4° de l'article L. 232-23-4. Saisi par Mme A..., le juge des référés du Conseil d'Etat a suspendu l'exécution de cette décision. Après avoir abrogé sa première décision, la présidente de l'AFLD a prononcé, par une décision du 25 avril 2019, une nouvelle mesure de suspension provisoire ayant la même portée. Mme A... conteste ces deux décisions.<br/>
<br/>
              En ce qui concerne la requête n° 429646 :<br/>
<br/>
              4.	L'exécution de la mesure de suspension provisoire prononcée à l'encontre de Mme A... le 9 avril 2019 ayant été suspendue par le juge des référés du Conseil d'Etat dès le 12 avril 2019, l'intéressée a pu participer le 14 avril 2019 au marathon de Paris. Cette mesure a ensuite été formellement abrogée par une décision de la présidente de l'AFLD en date du 15 avril 2019. Si Mme A... soutient que cette mesure a produit des effets, elle n'apporte aucun élément de nature à en justifier. Par suite, les conclusions qu'elle présente contre cette première décision de suspension sont devenues sans objet. Il n'y a, dès lors, plus lieu d'y statuer.<br/>
<br/>
              5.	Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions que présentent l'Agence française de lutte contre le dopage et Mme A... au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              En ce qui concerne la requête n° 431499 :<br/>
<br/>
              Sur les moyens tirés, par la voie de l'exception, de l'illégalité des dispositions de l'article L. 232-23-24 du code du sport :<br/>
<br/>
              6.	Mme A... soulève, à l'appui de sa requête, des moyens mettant en cause la légalité des dispositions de l'article L. 232-23-4 du code du sport, issues de l'article 29 de l'ordonnance du 19 décembre 2018, dont le projet de loi de ratification a été déposé à l'Assemblée nationale le 6 mars 2019, mais qui, à la date de la présente décision, n'a pas été ratifiée.<br/>
<br/>
              7.	En premier lieu, la mesure de suspension provisoire, prononcée sur le fondement de l'article L. 232-23-4 du code du sport, est prise à titre conservatoire dans un objectif de protection de la santé des sportifs ainsi que de garantie de l'équité et de l'éthique des compétitions sportives. Compte tenu de l'objet et de la portée d'une telle mesure, qui ne constitue pas une sanction, le moyen tiré de ce que l'article L 232-23-4 du code du sport méconnaîtrait le principe constitutionnel des droits de la défense, faute de prévoir une procédure contradictoire préalable au prononcé de cette mesure, ne peut qu'être écarté. Au demeurant, il ressort des pièces du dossier que Mme A... et son conseil ont été reçus par la présidente de l'AFLD le 23 avril 2019 préalablement à l'intervention de la mesure contestée et ont pu ainsi présenter leurs observations sur la mesure envisagée. <br/>
<br/>
              8.	En second lieu, compte tenu de l'objet et de la portée d'une mesure de suspension provisoire, le moyen tiré de ce que l'article L. 232-23-4 du code du sport méconnaîtrait les principes de séparation des pouvoirs et de non confusion des fonctions de poursuite et de sanction doit en tout état de cause être écarté.<br/>
<br/>
              Sur les autres moyens :<br/>
<br/>
              9.	En premier lieu, il résulte des termes mêmes de la décision attaquée, qui est suffisamment motivée, que, après avoir entendu Mme A... et son conseil, la présidente de l'AFLD a procédé à l'examen de la situation de l'intéressée et a porté une appréciation sur les faits de l'espèce, sans estimer être dans un cas dans lequel le prononcé d'une mesure de suspension provisoire revêt un caractère obligatoire.<br/>
<br/>
              10.	En deuxième lieu, il ressort des pièces du dossier que le directeur du département des contrôles de l'AFLD a délivré à deux agents de l'Agence habilités à procéder à des contrôles antidopage des ordres de mission en vue de prélèvements sanguins et urinaires sur Mme A... au Maroc le 27 mars 2019. Un premier ordre de mission autorisait ces prélèvements en tout lieu entre 16 heures et 23 heures, un second entre 20 heures et 21 heures à l'adresse que Mme A... avait indiquée au préalable en sa qualité de membre du " groupe cible " des sportifs assujettis à l'obligation de transmettre leur localisation. Ces deux agents étaient accompagnés du directeur du département des contrôles de l'AFLD. Les rapports établis par ces agents assermentés indiquent qu'ils ont abordé dans la rue Mme A..., le 27 mars à 17 heures 55, ont décliné leur identité et la raison de leur présence, mais que celle-ci, après avoir dans un premier temps semblé disposée à se prêter au contrôle, a gagné une salle de sport où se trouvait son compagnon, puis a pris la fuite en courant avec le concours de celui-ci et n'a pas réapparu le soir à son domicile entre 20 heures et 21 heures. Si Mme A... conteste que les intéressés se soient prévalus de la qualité d'agents de l'AFLD et lui aient notifié leur intention d'effectuer des prélèvements et si elle indique avoir quitté les lieux pour conduire à la pharmacie son enfant qui aurait chuté du fait d'un de ces agents, les éléments qu'elle produit au soutien de ses allégations ne sont pas de nature à remettre en cause les constatations opérées par des agents assermentés, qui, contrairement à ce qui est soutenu, ne sont pas entachées d'incohérences ou de contradictions. Par suite, les moyens tirés de ce que la mesure de suspension prononcée à son encontre aurait été prise après une procédure de contrôle irrégulière et au vu de faits matériellement inexacts doivent être écartés. En outre, la présidente de l'AFLD a pu légalement regarder les faits relevés comme susceptibles de caractériser une soustraction au prélèvement d'un échantillon au sens du 1° de l'article L. 232-9-2 du code du sport.<br/>
<br/>
              11.	En troisième lieu, ainsi qu'il a été dit au point 8, une suspension provisoire, prononcée sur le fondement de l'article L. 232-23-4 du code du sport, est une mesure conservatoire, dans l'attente de la décision de la commission des sanctions de l'AFLD, et ne constitue pas par elle-même une sanction. La participation d'un sportif auquel il est reproché de s'être soustrait à un prélèvement, manquement particulièrement grave s'il est avéré, à des manifestations sportives ou aux autres activités mentionnées à l'article L. 232-23-4 est susceptible, en particulier si elle intervient dans un bref délai, de jeter un discrédit sur celles-ci ou, à tout le moins, de compromettre leur bonne tenue et est également susceptible de porter atteinte à l'équité et à l'éthique sportives. Si Mme A... fait valoir les effets de la mesure prise à son encore sur sa carrière sportive et ses contrats de sponsoring et si elle se prévaut du fait qu'elle n'a pas fait l'objet de contrôles positifs, la présidente de l'AFLD n'a pas, dans les circonstances de l'espèce, commis d'erreur d'appréciation en décidant, de sa propre initiative, de faire usage des pouvoirs qu'elle tient de l'article L. 232-23-4.<br/>
<br/>
              12.	En quatrième lieu, la circonstance alléguée que la publication dans certains médias d'articles consacrés aux procédures relatives à Mme A... révélerait une méconnaissance, par les services de l'AFLD, du secret professionnel est, en tout état de cause, sans incidence sur la légalité de la décision contestée.<br/>
<br/>
              13.	En cinquième et dernier lieu, la décision attaquée ne constituant qu'une mesure conservatoire sans caractère de sanction, Mme A... ne peut utilement invoquer les stipulations de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. En tout état de cause, il ne ressort pas des pièces du dossier que le traitement par l'AFLD du cas de l'intéressée révèlerait un manquement au principe d'impartialité ou un détournement de procédure. <br/>
<br/>
              14.	Il résulte de tout ce qui précède que Mme A... n'est pas fondée à demander l'annulation pour excès de pouvoir de la décision du 25 avril 2019 de la présidente de l'Agence française de lutte contre le dopage ordonnant des mesures conservatoires sur le fondement de l'article 232-23-4 du code du sport. <br/>
<br/>
              15.	Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Agence française de lutte contre le dopage qui n'est pas, dans la présente instance, la partie perdante. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme A... une somme de 3 000 euros à verser à l'Agence au même titre.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Il n'y a pas lieu à statuer sur les conclusions d'annulation de la requête n° 429646.<br/>
<br/>
Article 2 : Le surplus des conclusions de la requête n° 429646 et la requête n° 431499 sont rejetés.<br/>
<br/>
Article 3 : Mme A... versera une somme de 3 000 euros à l'Agence française de lutte contre le dopage au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à Mme B... A..., à l'Agence française de lutte contre le dopage et à la ministre des sports.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
