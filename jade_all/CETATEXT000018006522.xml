<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000018006522</ID>
<ANCIEN_ID>JG_L_2007_06_000000289336</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/18/00/65/CETATEXT000018006522.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 18/06/2007, 289336</TITRE>
<DATE_DEC>2007-06-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>289336</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Delarue</PRESIDENT>
<AVOCATS>SCP THOUIN-PALAT ; SCP BACHELLIER, POTIER DE LA VARDE</AVOCATS>
<RAPPORTEUR>M. Jean-Philippe  Thiellay</RAPPORTEUR>
<COMMISSAIRE_GVT>Mlle Verot</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 23 janvier et 24 mai 2006 au secrétariat du contentieux du Conseil d'Etat, présentés pour l'ASSOCIATION SYNDICALE LIBRE DES PROPRIETAIRES DU LOTISSEMENT TE MARU ATA, dont le siège est B.P. 4608 à Papeete - Tahiti (98713) ; l'ASSOCIATION SYNDICALE LIBRE DES PROPRIETAIRES DU LOTISSEMENT TE MARU ATA demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt en date du 27 septembre 2005 par lequel la cour administrative d'appel de Paris a, sur la demande de M. A, annulé le jugement du tribunal administratif de la Polynésie française du 5 juillet 2004 annulant l'arrêté du 17 décembre 2002 accordant à M. A une autorisation de travaux pour la modification et l'aménagement d'une maison située sur les terres Toia, Papauri, Papahiaroa et Farepapa à Panuaauia ; <br/>
<br/>
              2°) statuant au fond, de rejeter les requêtes de M. A ; <br/>
<br/>
              3°) de mettre à la charge de M. A une somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'aménagement de la Polynésie française ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Philippe Thiellay, Maître des Requêtes, <br/>
<br/>
              - les observations de la SCP Bachellier, Potier de la Varde, avocat de l'ASSOCIATION SYNDICALE LIBRE DES PROPRIETAIRES DU LOTISSEMENT TE MARU ATA et de la SCP Thouin-Palat, avocat de M. Patrick A, <br/>
              - les conclusions de Mlle Célia Verot, Commissaire du gouvernement ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que  M. Patrick A a obtenu, le 17 décembre 2002, une autorisation de travaux pour modifier et aménager une maison située sur une parcelle issue d'un partage de propriété réalisé en dernier lieu en 1992 ; que, saisi par l'ASSOCIATION SYNDICALE LIBRE DES PROPRIETAIRES DU LOTISSEMENT TE MARU ATA, le tribunal administratif de la Polynésie française a annulé cette autorisation au motif que le lotissement ainsi constitué n'avait jamais été autorisé et que, par suite, cette autorisation de réaliser des travaux sur une parcelle comprise dans le lotissement de fait était illégale ; que, par un arrêt du 27 septembre 2005 dont l'ASSOCIATION SYNDICALE LIBRE DES PROPRIETAIRES DU LOTISSEMENT TE MARU ATA demande la cassation, la cour administrative d'appel de Paris a annulé ce jugement ;<br/>
<br/>
              Sur la régularité de l'arrêt attaqué :<br/>
<br/>
              Considérant que la cour administrative d'appel de Paris, qui n'était saisie d'aucune conclusion dirigée contre le jugement de première instance, en tant qu'il s'était prononcé sur la recevabilité de l'intervention volontaire présentée devant lui par M. B, et qui a répondu à l'ensemble des moyens dont elle était saisie par l'effet dévolutif de l'appel, n'a commis aucune irrégularité en ne se prononçant pas à nouveau sur la recevabilité de cette intervention ; <br/>
<br/>
              Sur le fond :<br/>
<br/>
              Considérant qu'aux termes des dispositions de l'article D. 141-1 du code de l'aménagement de la Polynésie française, dans sa rédaction applicable aux faits de l'espèce : « Constituent un lotissement l'opération et le résultat de l'opération ayant pour objet ou ayant eu pour effet la division volontaire d'une ou plusieurs propriétés foncières par ventes ou locations simultanées ou successives consenties en vue de l'habitation » ; que l'article D. 114-12 du même code dispose que « toute partition de terrain en plus de trois parties est un lotissement » ; que, toutefois, une délibération du 26 septembre 2002 a modifié ces dispositions pour prévoir qu'un lotissement s'entendait de « toute partition de terrain en plus de cinq parties sur une période de moins de 10 ans » ; <br/>
<br/>
              Considérant qu'une autorisation de travaux ne peut être légalement délivrée pour une construction à édifier sur un terrain compris dans un lotissement non autorisé, à moins que ce lotissement n'ait fait l'objet d'une régularisation ultérieure, sous l'empire des dispositions législatives ou réglementaires intervenues postérieurement ; que, dans l'hypothèse où les textes postérieurs retiennent une définition plus restrictive du lotissement, celle-ci ne saurait rétroactivement régulariser les opérations de divisions ayant constitué un lotissement de fait non autorisé ; qu'en revanche, dès lors que le lotissement de fait n'entre plus, à la date à laquelle l'autorisation de travaux contestée a été délivrée, dans le champ d'application des dispositions relatives aux opérations de lotissement soumises à autorisation, des travaux de constructions sur une parcelle incluse dans le périmètre d'un tel lotissement peuvent légalement y être autorisés ;<br/>
<br/>
              Considérant que, pour rejeter l'argumentation présentée en défense par l'ASSOCIATION SYNDICALE LIBRE DES PROPRIETAIRES DU LOTISSEMENT TE MARU ATA, la cour a relevé, comme le tribunal, que le lotissement dans lequel la parcelle de M. A était incluse, consistant en plus de trois parcelles, n'avait jamais été autorisé, ce qui n'est au demeurant contesté par aucune des parties ; qu'elle a également jugé que, compte tenu de l'évolution sus-rappelée des dispositions du code de l'aménagement de la Polynésie française, ce lotissement, consistant en moins de cinq parcelles, n'entrait plus dans le champ d'application des règles applicables aux lotissements de fait et qu'il ne « constituait plus un lotissement non autorisé pour lequel une régularisation par obtention d'un permis de lotir serait demeurée nécessaire afin que pussent être régulièrement autorisés des travaux de construction sur la parcelle provenant de la division » ; qu'en jugeant ainsi, pour les motifs qui viennent d'être indiqués, elle n'a entaché son arrêt d'aucune erreur de droit ; <br/>
<br/>
              Considérant que la cour a ensuite jugé, compte tenu de la nature et de l'importance des travaux projetés ainsi que de l'existence d'un droit d'accès de ce terrain d'assiette à la voirie et aux autres réseaux présents sur le lotissement voisin, que la construction envisagée par M. A respectait les exigences réglementaires fixées par les articles A. 114-28 et A. 114-1 du code de l'aménagement de la Polynésie française ; que la cour a ainsi suffisamment répondu au moyen soulevé en défense devant elle par l'ASSOCIATION SYNDICALE LIBRE DES PROPRIETAIRES DU LOTISSEMENT TE MARU ATA tiré de ce que le terrain d'assiette des travaux projetés par M. A ne disposait plus d'aucun droit d'utilisation des réseaux environnants et que l'autorisation contestée méconnaissait les exigences réglementaires ; <br/>
<br/>
              Considérant enfin qu'il ressort des pièces du dossier soumis aux juges du fond que les travaux, objets de la demande d'autorisation contestée par l'ASSOCIATION SYNDICALE LIBRE DES PROPRIETAIRES DU LOTISSEMENT TE MARU ATA, portaient sur l'extension d'une maison existante, par l'adjonction de deux ailes de bâtiment ; qu'après avoir souverainement apprécié les faits, la cour a pu, sans les dénaturer, décider que les travaux envisagés pouvaient être légalement autorisés, le terrain d'assiette bénéficiant d'un accès à la voirie et aux réseaux répondant aux exigences réglementaires ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que l'ASSOCIATION SYNDICALE LIBRE DES PROPRIETAIRES DU LOTISSEMENT TE MARU ATA n'est pas fondée à demander l'annulation de l'arrêt de la cour administrative de Paris du 27 septembre 2005 ; <br/>
<br/>
              Sur les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de faire application des dispositions de l'article L. 761-1 du code de justice administrative et de mettre à la charge de l'ASSOCIATION SYNDICALE LIBRE DES PROPRIETAIRES DU LOTISSEMENT TE MARU ATA une somme de 3 000 euros au titre des frais exposés par M. A et non compris dans les dépens ; que ces dispositions font en revanche obstacle à ce que soient mises à la charge de M. A, qui n'est pas la partie perdante dans la présente instance, les sommes que l'ASSOCIATION SYNDICALE LIBRE DES PROPRIETAIRES DU LOTISSEMENT TE MARU ATA demande au titre des frais exposés par elle et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de l'ASSOCIATION SYNDICALE LIBRE DES PROPRIETAIRES DU LOTISSEMENT TE MARU ATA est rejetée.<br/>
Article 2 : L'ASSOCIATION SYNDICALE LIBRE DES PROPRIETAIRES DU LOTISSEMENT TE MARU ATA versera à M. A une somme de 3 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à l'ASSOCIATION SYNDICALE LIBRE DES PROPRIETAIRES DU LOTISSEMENT TE MARU ATA, à M. A et au ministre d'Etat, ministre de l'écologie, du développement et de l'aménagement durables.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-02-04-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PROCÉDURES D'INTERVENTION FONCIÈRE. LOTISSEMENTS. AUTORISATION DE LOTIR. - ABSENCE D'AUTORISATION - RÉGULARISATION AVANT LA DÉLIVRANCE D'UN PERMIS DE CONSTRUIRE - ABSENCE - CONDITION - RÉGLEMENTATION À LA DATE DE LA DÉLIVRANCE DU PERMIS PLUS RESTRICTIVE AYANT POUR CONSÉQUENCE D'EXCLURE LA QUALIFICATION DE LOTISSEMENT POUR LE TERRAIN CONSIDÉRÉ.
</SCT>
<ANA ID="9A"> 68-02-04-02 Une autorisation de travaux ne peut être légalement délivrée pour une construction à édifier sur un terrain compris dans un lotissement non autorisé, à moins que ce lotissement n'ait fait l'objet d'une régularisation ultérieure, sous l'empire des dispositions législatives ou réglementaires intervenues postérieurement. Dans l'hypothèse où les textes postérieurs retiennent une définition plus restrictive du lotissement, celle-ci ne saurait rétroactivement régulariser les opérations de divisions ayant constitué un lotissement de fait non autorisé. En revanche, dès lors que le lotissement de fait n'entre plus, à la date à laquelle l'autorisation de travaux contestée a été délivrée, dans le champ d'application des dispositions relatives aux opérations de lotissement soumises à autorisation, des travaux de constructions sur une parcelle incluse dans le périmètre d'un tel lotissement peuvent légalement y être autorisés.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
