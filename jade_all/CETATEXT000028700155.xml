<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028700155</ID>
<ANCIEN_ID>JG_L_2014_03_000000375093</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/70/01/CETATEXT000028700155.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 06/03/2014, 375093, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-03-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>375093</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET ; RICARD</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2014:375093.20140306</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 31 janvier 2014 au secrétariat du contentieux du Conseil d'Etat, présentée par la société Nordic Pharma, dont le siège social est situé 216, boulevard Saint-Germain à Paris (75007), et la société Exelgyn, dont le siège social est situé 216, boulevard Saint-Germain à Paris (75007) ; les sociétés requérantes demandent au juge des référés du Conseil d'Etat d'ordonner, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de l'arrêté interministériel du 20 janvier 2014 en tant qu'il inscrit sur la liste des spécialités pharmaceutiques agréées à l'usage des collectivités et divers services la spécialité  Miffee 200 mg  du laboratoires Linepharma ; <br/>
<br/>
<br/>
              elles soutiennent que : <br/>
              - la condition d'urgence est remplie dès lors que l'arrêté litigieux porte une atteinte grave et immédiate à la santé publique ainsi qu'à leurs intérêts commerciaux et financiers ; <br/>
              - il existe un doute sérieux quant à la légalité de l'arrêté contesté ;<br/>
              - il est fondé sur un avis de la commission de la transparence du 16 octobre 2013 modifié le 5 novembre 2013 qui est entaché d'une illégalité manifeste, en ce que, d'une part, il a été modifié en méconnaissance du principe du parallélisme des compétences et des procédures , que, d'autre part, il contredit les recommandations de bonnes pratiques de la Haute autorité de santé sur l'interruption volontaire de grossesse par voie médicamenteuse de décembre 2010, qu'enfin, il est entaché d'erreurs de fait et d'erreur manifeste d'appréciation au regard des critères d'évaluation du service médical rendu énumérés à l'article R. 163-3 du code de la sécurité sociale ;<br/>
<br/>
<br/>
              Vu l'arrêté dont la suspension de l'exécution est demandée ;<br/>
              Vu la copie de la requête à fin d'annulation de cet arrêté ;<br/>
              Vu les observations, enregistrées le 19 février 2014, présentées par la Haute autorité de santé ; <br/>
<br/>
              Vu le mémoire en défense, enregistré le 25 février 2014, présenté par la ministre des affaires sociales et de la santé, qui conclut au rejet de la requête ;<br/>
<br/>
              elle soutient que : <br/>
              - la condition d'urgence n'est pas remplie, dès lors que les sociétés requérantes n'ont nullement établi que l'arrêté litigieux porterait, à lui seul et par lui-même, une atteinte grave et immédiate à un intérêt public et à leur situation personnelle ; <br/>
              - aucun doute sérieux n'existe quant à la légalité de l'arrêté litigieux ;<br/>
              - les moyens de légalité soulevés par les sociétés requérantes sont inopérants dès lors qu'ils ne visent pas l'arrêté contesté mais uniquement l'avis de la commission de la transparence de la Haute autorité de santé du 16 octobre 2013 modifié le 5 novembre 2013, lequel ne lie pas les ministres chargés de la santé et de la sécurité sociale ;<br/>
              - en tout état de cause, l'avis de la commission de la transparence modifié le 5 novembre 2013 a été rendu par une autorité compétente et ne contrevient pas aux recommandations de bonnes pratiques de la Haute autorité de santé de décembre 2010, ni ne méconnait les critères d'évaluation du " service médical rendu " énumérés à l'article R. 163-3 du code de la sécurité sociale ;<br/>
<br/>
              Vu les mémoires en réplique, enregistrés le 28 février 2014, présentés pour la société Nordic Pharma et la société Exelgyn qui concluent aux mêmes fins par les mêmes moyens ; <br/>
<br/>
              Vu le mémoire en défense, enregistré le 28 février 2014, présenté pour la société Linepharma France, dont le siège social est situé 55, rue de Turbigo, à Paris (75003), qui conclut au rejet de la requête et à ce que soit mis à la charge des sociétés requérantes le versement de la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
              elle soutient que :<br/>
              - la condition d'urgence n'est pas remplie dès lors que l'arrêté contesté ne porte pas une atteinte grave et immédiate à l'intérêt général de protection de la santé publique, ni aux intérêts des sociétés requérantes ;<br/>
              - il n'existe pas de doute sérieux quant à la légalité de l'arrêté contesté ;<br/>
              - l'avis de la commission de la transparence du 16 octobre 2013 est un simple acte préparatoire qui ne fait pas grief ;<br/>
              - l'avis de la commission de la transparence du 16 octobre 2013 a été modifié le 5 novembre 2013 par une autorité compétente ;<br/>
              - l'arrêté contesté ne méconnaît pas les recommandations de la Haute autorité de santé ;<br/>
              - l'avis de la commission de la transparence ne contredit pas les recommandations de bonnes pratiques de la Haute autorité de santé de décembre 2010 ;<br/>
              - l'avis de la commission de la transparence du 16 octobre 2013 n'est pas fondé sur des faits erronés dès lors que la commission s'est prononcée sur le service médical rendu par la spécialité Miffee au regard d'études pertinentes qui lui ont été fournies ;<br/>
               - la commission de la transparence a rendu son avis du 16 octobre 2013 dans le respect des critères d'évaluation du service médical rendu énumérés à l'article R. 163-3 du code de la sécurité sociale ;<br/>
<br/>
              Vu le nouveau mémoire, enregistré le 3 mars 2014, présenté par la ministre des affaires sociales et de la santé, qui conclut au rejet de la requête ; elle soutient en outre que l'absence de production par la société Linepharma France de l'étude observationnelle exigée par le CHMP n'a aucune influence sur la légalité de l'arrêté contesté ; <br/>
<br/>
              Vu le mémoire en réplique, enregistré le 3 mars 2014, présenté pour la société Nordic Pharma et la société Exelgyn, qui concluent aux mêmes fins par les mêmes moyens ; <br/>
                          Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la sécurité sociale ;<br/>
<br/>
              Vu le code de la santé publique ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, la société Nordic Pharma et la société Exelgyn et, d'autre part, la ministre des affaires sociales et de la santé, la Haute autorité de santé et ainsi que la société Linepharma France ;<br/>
<br/>
              Vu le procès-verbal de l'audience publique du 3 mars 2014 à 11 heures au cours de laquelle ont été entendus : <br/>
<br/>
               - Me Ricard, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la société Nordic Pharma et de la société Exelgyn ;<br/>
<br/>
              - les représentants de la société Nordic Pharma et de la société Exelgyn ;<br/>
<br/>
              - les représentants de la ministre des affaires sociales et de la santé ; <br/>
<br/>
              - les représentantes de la Haute autorité de santé ; <br/>
<br/>
              - Me Stoclet, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la société Laboratoires Linepharma ;<br/>
              - les représentantes de la société Linepharma France ;<br/>
              et à l'issue de laquelle le juge des référés a clôturé l'instruction ;<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 5123-2 du code de la santé publique : " l'achat, la fourniture, la prise en charge et l'utilisation par les collectivités publiques des médicaments définis aux articles L. 5121-8, L. 5121-9-1, L. 5121-12, L. 5121-13 et L. 5121-14-1 (...)  sont limités, dans les conditions propres à ces médicaments fixées par le décret mentionné à l'article L. 162-17 du code de la sécurité sociale, aux produits agréés dont la liste est établie par arrêté des ministres chargés de la santé et de la sécurité sociale. (...) " ; qu'il résulte des dispositions combinées des articles L. 5123-3 du même code et L. 161-37 du code de la sécurité sociale que l'inscription sur la liste des spécialités pharmaceutiques agréées à l'usage des collectivités et de divers services publics est prononcée après avis de la commission de la transparence de la Haute autorité de santé ;<br/>
<br/>
              3. Considérant que, le 29 mars 2013, l'Agence nationale de sécurité du médicament et des produits de santé a octroyé à la société Linepharma une autorisation de mise sur le marché pour sa spécialité Miffee 200 mg ayant pour indication unique le traitement médicamenteux de l'interruption volontaire de grossesse ; que l'arrêté contesté du 20 janvier 2014 a inscrit cette spécialité sur la liste mentionnée ci-dessus après l'avis rendu par la commission de la transparence de la Haute autorité de santé le 16 octobre 2013, modifié le 5 novembre 2013 par le bureau de cette commission et attribuant à cette spécialité un service médical rendu important ;<br/>
<br/>
              4. Considérant que les sociétés requérantes soutiennent, en premier lieu, que la modification de l'avis de la commission de la transparence par son bureau, qui s'est borné à préciser les informations administratives et réglementaires relatives à la spécialité Mifee par une reprise du résumé des caractéristiques de ce produit, serait contraire au principe de parallélisme des compétences et des procédures ; qu'elles soutiennent, en deuxième lieu, que, dans la mesure de cette modification, cet avis serait en outre contraire aux recommandations de bonnes pratiques de la Haute autorité de santé de décembre 2010 en matière d'interruption volontaire de grossesse par voie médicamenteuse ; qu'elle soutiennent enfin que l'octroi d'un service médical rendu important à la spécialité Miffee serait entaché d'erreurs de fait tenant notamment à l'absence d'essais cliniques pertinents et d'une erreur manifeste d'appréciation, la mise en oeuvre des critères d'évaluation du service médical rendu énumérés à l'article R. 163-3 du code de la sécurité sociale devant conduire à l'attribution d'un service médical rendu insuffisant ; <br/>
<br/>
              5. Considérant qu'aucun de ces moyens n'est de nature, en l'état de l'instruction,  à faire sérieusement douter de la légalité de l'arrêté contesté ; qu'ainsi, l'une des conditions auxquelles est subordonné l'exercice, par le juge des référés, des pouvoirs qu'il tient de l'article L. 521-1 du code de justice administrative n'est pas remplie ; que, dès lors, et sans qu'il soit besoin de statuer sur la condition d'urgence, la requête des sociétés Nordic Pharma et Exelgyn doit être rejetée ; qu'il y a lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées pour la société Linepharma France au titre des dispositions de l'article L. 761-1 en mettant à la charge des sociétés requérantes la somme de 3 000 euros à ce titre ; <br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
      Article 1er : La requête des sociétés Nordic Pharma et Exelgyn est rejetée.<br/>
<br/>
Article 2 : Les sociétés Nordic Pharma et Exelgyn verseront à la société Linepharma France la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente ordonnance sera notifiée à la société Nordic Pharma, à la société Exelgyn, à la ministre des affaires sociales et de la santé, à la Haute autorité de santé et à la société Linepharma France.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
