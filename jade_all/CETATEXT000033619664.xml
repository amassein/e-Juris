<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033619664</ID>
<ANCIEN_ID>JG_L_2016_12_000000385796</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/61/96/CETATEXT000033619664.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 15/12/2016, 385796, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-12-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>385796</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, SOLTNER, TEXIDOR, PERIER ; SCP MONOD, COLIN, STOCLET ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>Mme Karin Ciavaldini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:385796.20161215</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La commune de Chessy et le syndicat d'agglomération nouvelle (SAN) du Val d'Europe ont demandé au tribunal administratif de Melun d'annuler la décision du directeur général de l'établissement public d'aménagement du secteur IV de Marne-la-Vallée (EPAFRANCE) de vendre différentes parcelles situées sur le territoire de la commune de Chessy formant l'esplanade dite " des Parcs " et de signer l'acte authentique, d'annuler l'acte de vente et d'enjoindre à l'EPAFRANCE et à la société Euro Disney Associés de résilier le contrat de vente dans un délai d'un mois et, à défaut, de saisir le juge du contrat afin qu'il en prononce la nullité.<br/>
<br/>
              Par un jugement n° 1203526 du 5 juillet 2013, le tribunal a annulé la décision attaquée, enjoint à l'EPAFRANCE de saisir le juge compétent afin que soient prises en compte les conséquences de l'annulation prononcée et rejeté le surplus des conclusions de la demande.  <br/>
<br/>
              Par un arrêt n°s 13PA03467, 13PA03492 du 18 septembre 2014, la cour administrative d'appel de Paris, statuant sur les appels de la société Euro Disney Associés et de l'EPAFRANCE, a :<br/>
              - annulé l'article 3 du jugement statuant sur les conclusions à fin d'injonction du SAN du Val d'Europe et de la commune de Chessy ;<br/>
              - enjoint à l'EPAFRANCE, à défaut d'avoir obtenu la résolution de l'acte de vente des parcelles formant l'esplanade " des Parcs ", de saisir le juge compétent afin que soient prises en compte les conséquences sur la validité du contrat de cession de l'annulation de la décision de son directeur général de vendre ces parcelles, dans un délai de six mois suivant la notification de l'arrêt ;<br/>
              - rejeté le surplus des requêtes ;<br/>
              - rejeté l'appel incident formé par le SAN du Val d'Europe et la commune de Chessy contre l'article 1er du jugement rejetant leurs conclusions tendant à l'annulation du contrat de vente.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 18 novembre 2014, 18 février 2015 et 4 février 2016 au secrétariat du contentieux du Conseil d'Etat, la société Euro Disney Associés demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler les articles 2, 3 et 4 de cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge du SAN du Val d'Europe et de la commune de Chessy la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général de la propriété des personnes publiques ;<br/>
              - le code de justice administrative ;		<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Karin Ciavaldini, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Soltner, Texidor, Perier, avocat de la SCA Euro Disney Associés et à la SCP Monod, Colin, Stoclet, avocat de la communauté d'agglomération du Val d'Europe Agglomération et de la commune de Chessy.<br/>
<br/>
<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que le directeur général de l'établissement public d'aménagement du secteur IV de Marne-la-Vallée (EPAFRANCE) a décidé de vendre à la société Euro Disney Associés différentes parcelles de l'esplanade François Truffaut, formant l'esplanade dite " des Parcs ", située entre le parc de loisirs Euro Disney et les gares RER et TGV de Marne-la-Vallée Chessy et a signé, le 21 décembre 2011, l'acte authentique de vente de ces parcelles. Le syndicat d'agglomération nouvelle (SAN) du Val d'Europe et la commune de Chessy ont saisi le tribunal administratif de Melun d'une demande tendant à l'annulation de cette décision. Par un jugement du 5 juillet 2013, le tribunal a fait droit à cette demande et a enjoint à l'EPAFRANCE de saisir le juge compétent afin que soient prises en compte les conséquences de cette annulation sur la validité du contrat de cession. La société Euro Disney et l'EPAFRANCE ont chacun relevé appel de ce jugement. La société se pourvoit en cassation contre les articles 2, 3 et 4 de l'arrêt par lequel la cour administrative d'appel de Paris, après avoir joint les deux requêtes, a rejeté leurs conclusions dirigées contre le jugement du tribunal administratif en tant qu'il avait annulé la décision de l'EPAFRANCE et a enjoint à l'EPAFRANCE, à défaut d'avoir obtenu la résolution de l'acte de vente des parcelles en cause dans le litige, de saisir le juge compétent afin que soient prises en compte les conséquences sur la validité du contrat de cession de l'annulation de la décision du directeur général de l'EPAFRANCE, dans un délai de six mois suivant la notification de l'arrêt.<br/>
<br/>
              2. L'EPAFRANCE justifie d'un intérêt suffisant pour intervenir au soutien du pourvoi en cassation présenté par la société Euro Disney Associés. Son intervention est, par suite, recevable.<br/>
<br/>
              3. En premier lieu, aux termes du dernier alinéa de l'article R. 741-2 du code de justice administrative : " La décision fait apparaître la date de l'audience et la date à laquelle elle a été prononcée ". Si l'arrêt attaqué porte des indications contradictoires quant à la date de l'audience, cette circonstance n'est pas de nature à l'entacher d'irrégularité dès lors qu'il ressort des pièces du dossier d'appel que l'audience publique a eu lieu le 4 septembre 2014 et que c'est par une simple erreur matérielle que l'arrêt mentionne également la date du 12 juin 2014, qui était celle à laquelle l'audience avait initialement été fixée avant d'être reportée. <br/>
<br/>
              4. En deuxième lieu, la voie du recours en cassation n'est ouverte, en vertu des règles générales de procédure, qu'aux personnes qui ont eu la qualité de partie dans l'instance ayant donné lieu à la décision attaquée. Si la société Euro Disney Associés, qui était défendeur en première instance, a été appelée par la cour à présenter des observations sur l'appel formé contre le jugement du tribunal administratif par l'EPAFRANCE, ce que la cour avait la faculté de faire sans y être tenue, elle n'aurait pas eu, si elle n'avait pas été ainsi appelée en la cause, qualité pour former tierce-opposition contre l'arrêt statuant sur cet appel, celui-ci ne préjudiciant pas à ses droits dès lors qu'elle avait la possibilité de relever elle-même appel du jugement rendu contre ses conclusions, ce qu'elle a d'ailleurs fait. La circonstance que la cour a joint l'appel formé par l'EPAFRANCE à celui formé par la société Euro Disney Associés pour y statuer par un seul arrêt n'a pas davantage, en vertu du principe de neutralité de la jonction des requêtes, eu pour effet de conférer à la société Euro Disney Associés la qualité de partie dans l'instance d'appel introduite par l'EPAFRANCE. La société Euro Disney Associés n'étant ainsi recevable à se pourvoir contre l'arrêt attaqué qu'en tant que celui-ci statue sur son propre appel, ses conclusions de cassation doivent être regardées comme limitées à cet objet. <br/>
<br/>
              5. Si l'EPAFRANCE a critiqué, dans sa requête d'appel, d'une part, les motifs par lesquels le tribunal a jugé que l'affichage de la décision du directeur général de l'EPAFRANCE n'avait pas, compte tenu de ses caractéristiques, fait courir le délai de recours contentieux à l'égard des tiers, d'autre part, l'omission de réponse du tribunal à son argumentation tirée de l'application en l'espèce de la théorie de la connaissance acquise, la société Euro Disney Associés n'a, en revanche, pas soulevé de tels moyens dans sa propre requête d'appel. Par suite, les moyens tirés de ce que la cour aurait insuffisamment motivé sur ces deux points l'arrêt attaqué en tant qu'il rejette son appel ne peuvent qu'être écartés.<br/>
<br/>
              6. La société Euro Disney Associés et l'EPAFRANCE, dans son intervention au soutien du pourvoi de cette dernière, soutiennent également que la cour aurait commis une erreur de droit et entaché son arrêt de dénaturation en jugeant recevable la demande de la commune de Chessy et du SAN du Val d'Europe, dès lors que, selon eux, cette demande était tardive, tant en raison de la date à laquelle la commune et le SAN avaient eu effectivement connaissance de la décision attaquée qu'en raison de ce que l'affichage de cette décision avait été suffisant pour faire courir le délai de recours. D'une part, ainsi qu'il a été dit, de tels moyens n'ont pas été soulevés devant la cour par la société Euro Disney Associés dans sa requête d'appel. D'autre part, si la question de la recevabilité d'une demande de première instance est d'ordre public, les pièces du dossier soumis aux juges du fond, qui peuvent seules être prises en compte à cet égard par le juge de cassation, ne font ressortir ni que l'affichage de la décision en litige au siège de l'EPAFRANCE aurait été suffisant pour que le délai de recours commence à courir à l'égard des tiers, ni que la commune de Chessy et le SAN du Val d'Europe auraient acquis, antérieurement à la réception, le 2 février 2012, du courrier du 31 janvier 2012 du directeur général de l'EPAFRANCE, une connaissance de la décision en litige suffisante pour faire courir ce même délai. Ces pièces ne permettent pas de retenir que la demande de première instance était tardive. Par suite, le moyen soulevé par la société Euro Disney Associés et l'EPAFRANCE doit être écarté.<br/>
<br/>
              7. En troisième lieu, il ressort des pièces du dossier soumis aux juges du fond que le cheminement piétonnier reliant le rond-point de l'avenue Paul Séramy aux gares RER et SNCF ainsi qu'au parc de loisirs est emprunté quotidiennement aussi bien par des clients de la société Euro Disney Associés que par des usagers des gares RER et SNCF, que le parking de la société Vinci qu'il relie aux gares n'a pas été conçu exclusivement pour accueillir les visiteurs des parcs de loisirs et qu'une partie de l'esplanade des Parcs, comprenant ce cheminement, était grevée d'une servitude de passage piéton, de secours et de sécurité publique, ainsi qu'il ressort notamment du plan annexé à l'acte de cession en litige et de l'acte notarié portant état descriptif de division en volumes de la parcelle en litige, daté du 30 septembre 1992. Ainsi, en relevant qu'une partie de l'esplanade " des Parcs " cédée par l'EPAFRANCE relevait de son domaine public, la cour, qui a suffisamment motivé son arrêt sur ce point, n'a pas inexactement qualifié les faits qui lui étaient soumis.<br/>
<br/>
              8. Il résulte de tout ce qui précède que le pourvoi de la société Euro Disney Associés doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de cette société le versement au même titre à la commune de Chessy et à la communauté d'agglomération Val d'Europe Agglomération de la somme totale de 3 000 euros.<br/>
<br/>
<br/>
<br/>              D E C I D E :<br/>
                             --------------<br/>
<br/>
Article 1er : L'intervention de l'établissement public d'aménagement du secteur IV de Marne-la-Vallée est admise. <br/>
<br/>
Article 2 : Le pourvoi de la société Euro Disney Associés est rejeté.<br/>
<br/>
Article 3 : La société Euro Disney Associés versera à la commune de Chessy et à la communauté d'agglomération Val d'Europe Agglomération la somme totale de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 4 : La présente décision sera notifiée à la SCA Euro Disney Associés, à la commune de Chessy et à la communauté d'agglomération Val d'Europe Agglomération.<br/>
Copie en sera adressée à l'établissement public d'aménagement du secteur IV de Marne-la-Vallée.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
