<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038829051</ID>
<ANCIEN_ID>JG_L_2019_07_000000432066</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/82/90/CETATEXT000038829051.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 12/07/2019, 432066, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-07-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>432066</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2019:432066.20190712</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 28 juin 2019 et<br/>
10 juillet 2019 au secrétariat du contentieux du Conseil d'Etat, Mme C...E...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
              1°) de suspendre, d'une part, la décision du 10 avril 2019 du jury du concours ouvert au titre de l'année 2019 pour le recrutement du professeur des universités-praticien hospitalier dans la spécialité " chirurgie plastique, reconstructrice et esthétique ; brûlologie ", concours de type 1 et, d'autre part, l'arrêté du 3 mai 2019 de la ministre de l'enseignement supérieur, de la recherche et de l'innovation et de la directrice générale du Centre national de gestion des praticiens hospitaliers, en tant qu'il assure la publicité de cette décision ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Il soutient que : <br/>
              - la condition d'urgence est remplie dès lors que l'exécution des deux actes litigieux porte une atteinte grave et immédiate, d'une part, aux intérêts de Mme E...en ce qu'elle est privée de la possibilité d'exercer sa profession et, d'autre part, à l'intérêt public de ne pas voir nommer dans un emploi public un candidat admis à l'issue d'une procédure irrégulière ;<br/>
              - il existe un doute sérieux quant à la légalité des décisions contestées ;<br/>
              - les décisions contestées ont été prises en violation du principe d'égalité de traitement des candidats dès lors que deux des membres du jury étaient absents lors de l'audition de Mme E... ;<br/>
              - ils ont été pris en violation du principe d'impartialité dès lors que, d'une part, Mme Brochant-Rodier, présidente du CNU, a fait parvenir le 12 mars 2019 à l'ensemble des membres un pré-programme ne mentionnant la candidature que de M. D...et, d'autre part, il existe des liens professionnels très étroits entre M. D...et trois des membres du jury ;<br/>
              - ils ont été pris en violation du principe d'égalité devant la loi et en violation de l'arrêté du 17 septembre 1987 dès lors que Mme E...n'a pas été interrogée lors de l'épreuve pédagogique sur un thème en rapport avec ses travaux personnels ;<br/>
              - ils sont entachés d'une erreur manifeste d'appréciation dès lors que le jury, en relevant l'absence d'affectation et l'isolement de MmeE..., s'est fondé sur un critère erroné pour rejeter sa candidature. <br/>
<br/>
<br/>
              Par un mémoire en défense, enregistré le 9 juillet 2019, la ministre de l'enseignement supérieur, de la recherche et de l'innovation conclut au rejet de la requête. Elle soutient, à titre principal, que la requête est irrecevable et, à titre subsidiaire, que la condition d'urgence n'est pas remplie et qu'aucun des moyens soulevés n'est propre à créer un doute sérieux quant à la légalité des décisions attaquées. <br/>
<br/>
              Par un mémoire en défense, enregistré le 10 juillet 2019, le Centre national de gestion des praticiens hospitaliers et des personnels de direction de la fonction publique hospitalière demande au Conseil d'Etat de rejeter la requête de MmeE....<br/>
<br/>
<br/>
	Vu les autres pièces des dossiers ;<br/>
	Vu :<br/>
              - le code de l'éducation ;<br/>
	- le code de la santé publique ;<br/>
	- le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, Mme E...et, d'autre part, la ministre de l'enseignement supérieur, de la recherche et de l'innovation, la ministre des solidarités et de la santé et la directrice générale du Centre national de gestion des praticiens hospitaliers ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 11 juillet 2019 à 11 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Rousseau, avocat au Conseil d'Etat et à la Cour de cassation, avocat de Mme E...;<br/>
<br/>
              - Mme E...;<br/>
<br/>
              - les représentants de la ministre de l'enseignement supérieur, de la recherche et de l'innovation et de la ministre des solidarités et de la santé ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". <br/>
<br/>
              2. Par une décision du 10 avril 2019, le Conseil national des universités a rejeté la candidature de Mme E...pour le recrutement de professeur des universités-praticien hospitalier dans la spécialité " Chirurgie plastique ; reconstructrice et esthétique ; brûlologie ", concours de type 1. La publicité de cette décision est assurée par un arrêté du 3 mai 2019 de la ministre de l'enseignement supérieur, de la recherche et de l'innovation et de la directrice générale du Centre national de gestion. Mme E...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de la décision du 10 avril 2019 et de l'arrêté du 3 mai 2019.<br/>
<br/>
              Sur la recevabilité :<br/>
<br/>
              3. Dans son mémoire en défense, la ministre de l'enseignement supérieur et de la recherche soutient que la demande de suspension est irrecevable, faute de comporter une copie de la décision attaquée. Toutefois, à l'audience, les représentants du ministère ont convenu que la décision du jury dont la suspension est demandée n'ayant pas été rendue publique, ne pouvait être détenue par les candidats évincés, et l'ont d'ailleurs produite à l'audience, renonçant à opposer cette fin de non-recevoir.<br/>
<br/>
              Sur la condition d'urgence : <br/>
<br/>
              4. La brûlologie est une spécialité rare, ce qu'illustrent la faible fréquence des concours et le fait que celui auquel a participé Mme E...n'a suscité que deux candidatures. Mme E...ne peut exercer à l'heure actuelle aucune activité clinique en rapport avec sa spécialité et sa compétence demeure inemployée. Il n'est pas contesté, sans préjuger des résultats d'un concours régulier, que Mme E...présente les qualifications requises pour être nommée au grade de professeur des universités-praticien hospitalier, et que sa carrière dans sa spécialité ne peut se développer qu'en passant ce concours. La suspension de la décision du jury ne donne certes aucune assurance à l'intéressée d'un succès au concours, mais permet au ministère d'organiser de nouvelles épreuves à bref délai. Dans la mesure où la personne retenue par le jury peut continuer à exercer ses fonctions cliniques et où la suspension n'a, ainsi que le ministère l'a confirmé à l'audience, d'effet que sur des enseignements qui ne commenceraient qu'à la prochaine rentrée universitaire, il n'est pas porté une atteinte excessive à l'intérêt public. Enfin, l'annulation dans plusieurs mois de la décision contestée, alors que son bénéficiaire aurait commencé une activité de PUPH, porterait atteinte de manière plus grave à ses intérêts. Dès lors, il y a lieu, au vu de l'ensemble des circonstances particulières à l'espèce, de regarder la lésion des intérêts dont se prévaut la requérante, au regard de la gravité des irrégularités dont est affectée la procédure suivie, comme constitutive d'une situation d'urgence au sens de l'article<br/>
L. 521-1 du code de justice administrative.<br/>
<br/>
              Sur la condition tenant à l'existence d'un moyen propre à faire naître un doute sérieux sur la légalité de l'acte litigieux : <br/>
<br/>
              5. La décision dont suspension est demandée a été prise en exécution de l'arrêté du 17 septembre 1987 fixant la procédure de recrutement des professeurs d'université-praticiens hospitaliers (PU-PH). Cet arrêté a été pris en exécution des dispositions du décret du 24 février 1984, dont l'article 66 dispose que " les candidatures sont examinées par des jurys formés, selon les cas, par les membres de la sous-section, de la section ou de l'intersection du conseil national des universités pour les disciplines médicales, odontologiques et pharmaceutiques dont l'emploi relève. "<br/>
<br/>
              6. En vue de l'examen de la candidature de MmeE..., la présidente du jury l'a invitée à communiquer les travaux et pièces exigés à l'ensemble des personnes figurant dans un courrier du 18 mars 2019, soit tous les membres, moins la requérante qui en fait partie, de la section compétente du CNU, parmi lesquelles figuraient Mme F...B...et M. A..., maîtres de conférences. Mme E...soutient, sans être sérieusement contredite, et en étayant ses allégations de copies de messages échangées avec deux membres du jury et constatées par huissier, que ces deux personnes étaient présentes lors de l'examen de la candidature qui a été ultérieurement retenue, et ont posé des questions au premier candidat, mais étaient absentes lors de son propre passage devant le jury. <br/>
<br/>
              7. La ministre soutient en défense que si ces deux personnes ont été présentes, elles n'ont pas participé aux débats et n'ont pas signé le procès-verbal des délibérations.<br/>
Il soutient, en outre, que les deux personnes en cause ne faisaient pas partie du jury, dès lors que l'article 13 du décret de 1987 dispose que " l'examen des questions individuelles relatives au recrutement relève des seuls représentants des enseignants et personnels assimilés occupant un emploi d'un rang au moins égal à celui de l'emploi postulé ". <br/>
<br/>
              8. Si les docteurs F...B...et A...appartenaient au jury, comme les conditions de convocation de la requérante le laissent comprendre, le moyen tiré de ce que leur absence lors du passage de Mme E...méconnaît le principe d'unicité du jury et a vicié la régularité de la décision qu'il a prise fait naître un doute sérieux sur la légalité de cette  décision. Dans le cas où, comme le soutient la ministre, le décret du 20 janvier 1987 serait regardé comme excluant les maîtres de conférences d'une section du CNU des délibérations d'un jury de recrutement d'un professeur, la participation de deux personnes qui n'étaient pas membres du jury au débats du jury avec le candidat qui a été retenu et la vraisemblance de leur association à sa délibération, que les éléments apportés par Mme E...permettent de regarder en l'état comme établie, constitue une irrégularité portant tout autant atteinte à la régularité de la procédure suivie et fondant le même doute sérieux sur la légalité de la décision contestée.<br/>
<br/>
              9. Il résulte de ce qui précède que Mme E...est fondée à demander la suspension de l'exécution de la décision contestée. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à Mme E...de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : L'exécution de la décision du 10 avril 2019 du jury du concours ouvert au titre de l'année 2019 pour le recrutement du professeur des universités-praticien hospitalier dans la spécialité " chirurgie plastique, reconstructrice et esthétique ; brûlologie ", concours de type 1, est suspendue.<br/>
Article 2 : L'Etat versera à Mme E...la somme de 3 000 au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente ordonnance sera notifiée à Mme C...E..., au Centre national de gestion des praticiens hospitaliers, à la ministre de l'enseignement supérieur, de la recherche et de l'innovation et à la ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
