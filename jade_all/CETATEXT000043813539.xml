<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043813539</ID>
<ANCIEN_ID>JG_L_2021_07_000000434268</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/81/35/CETATEXT000043813539.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 19/07/2021, 434268, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>434268</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>M. Olivier Guiard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:434268.20210719</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Genefinance, venant aux droits de la société Interga, a demandé au tribunal administratif de Montreuil de prononcer la décharge des retenues à la source auxquelles elle a été assujettie au titre des années 2008 et 2009. Par un jugement n° 1508174 du 1er décembre 2016, le tribunal administratif de Montreuil a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 17VE00308 du 9 juillet 2019, la cour administrative d'appel de Versailles a rejeté l'appel formé par la société Genefinance contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 5 septembre et 27 novembre 2019 au secrétariat du contentieux du Conseil d'Etat, la société Genefinance demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Guiard, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de la société Genefinance ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Interga exerçait une activité de garantie du risque de crédit au profit de certaines succursales et filiales étrangères du groupe de la Société Générale auquel elle appartenait. À l'issue d'une vérification de comptabilité, l'administration fiscale a estimé insuffisant, par rapport aux garanties accordées, le montant des primes versées par trois entités étrangères en 2008 et quatre en 2009 et considéré que l'avantage ainsi consenti caractérisait un transfert de bénéfices au sens de l'article 57 du code général des impôts. La société Genefinance, qui vient aux droits de la société Interga, demande l'annulation de l'arrêt du 9 juillet 2019 par lequel la cour administrative d'appel de Versailles a rejeté l'appel qu'elle a formé contre le jugement du 1er décembre 2016 du tribunal administratif de Montreuil qui a rejeté sa demande tendant à la décharge de la retenue à la source prévue par le 2 de l'article 119 bis du code général des impôts à laquelle elle a été assujettie au titre des années 2008 et 2009 à raison des sommes ainsi réputées distribuées aux entités étrangères. <br/>
<br/>
              2. Aux termes de l'article 57 du code général des impôts, applicable en matière d'impôt sur les sociétés en vertu de l'article 209 du même code : " Pour l'établissement de l'impôt sur le revenu dû par les entreprises qui sont sous la dépendance ou qui possèdent le contrôle d'entreprises situées hors de France, les bénéfices indirectement transférés à ces dernières, soit par voie de majoration ou de diminution des prix d'achat ou de vente, soit par tout autre moyen, sont incorporés aux résultats accusés par les comptabilités (...) ". Ces dispositions instituent, dès lors que l'administration fiscale établit l'existence d'un lien de dépendance et d'une pratique entrant dans les prévisions de l'article 57 du code général des impôts, une présomption de transfert indirect de bénéfices qui ne peut utilement être combattue par l'entreprise imposable en France que si celle-ci apporte la preuve que les avantages qu'elle a consentis ont été justifiés par l'obtention de contreparties. <br/>
<br/>
              3. Lorsqu'elle constate que les prix facturés par une entreprise établie en France à une entreprise étrangère qui lui est liée sont inférieurs à ceux pratiqués par des entreprises similaires exploitées normalement, c'est-à-dire dépourvues de liens de dépendance, l'administration doit être regardée comme établissant l'existence d'un avantage qu'elle est en droit de réintégrer dans les résultats de l'entreprise française, sauf pour celle-ci à justifier que cet avantage a eu pour elle des contreparties aux moins équivalentes. À défaut d'avoir procédé à une telle comparaison, l'administration n'est, en revanche, pas fondée à invoquer la présomption de transfert de bénéfices ainsi instituée mais doit, pour démontrer qu'une entreprise a consenti une libéralité en facturant des prestations à un prix insuffisant, établir l'existence d'un écart injustifié entre le prix convenu et la valeur vénale du bien cédé ou du service rendu. <br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond qu'à la suite d'une vérification de comptabilité, le vérificateur a constaté qu'en 2008 et 2009, la sté Interga, jusqu'alors bénéficiaire, avait enregistré des pertes importantes, le montant de primes perçues ne permettant pas de couvrir les charges résultant des appels en garantie. Il a constaté que le montant des garanties versées aux entités clientes correspondait à la différence entre le coût du risque pour chacune de ces entités et le double de leur résultat brut d'exploitation moyen respectif et qu'en contrepartie de ce service, la société percevait une prime correspondant à la somme d'un pourcentage des soldes des comptes de bilan et d'un pourcentage des soldes de comptes hors bilan couverts par la garantie de chacune des entités clientes. Il a constaté que ces deux pourcentages figurant dans la formule de calcul des primes avaient été fixés en 2004 sur la base de quatre paramètres calculés au niveau de l'ensemble des entités clientes, dont un seuil de déclenchement de 2,4 % de l'encours couvert.<br/>
<br/>
              5. Il ressort des énonciations de l'arrêt attaqué que, pour retenir l'existence d'un avantage consenti par la société Interga à certaines succursales étrangères du groupe, la cour a relevé qu'alors que pour ces succursales le seuil contractuel de déclenchement de la garantie du risque de crédit était fixé à 2,4 % de l'encours couvert, le seuil effectif était en réalité de 0 % et que, par suite, celles-ci étaient indemnisées dès le premier euro de sinistre sans verser de primes correspondantes. Elle a jugé que, faute pour la société de justifier de l'obtention de contreparties, cet avantage caractérisait un transfert de bénéfices au sens de l'article 57 du code général des impôts. En statuant ainsi, la cour a toutefois dénaturé les pièces du dossier desquelles il ne ressortait pas que le seuil contractuel de déclenchement de la garantie avait été fixé, pour les succursales concernées, à 2,4 % de l'encours couvert, un tel seuil n'étant prévu qu'au niveau de l'ensemble des entités clientes et uniquement pour contribuer au calcul des primes. Par suite, sans qu'il besoin de se prononcer sur les autres moyens du pourvoi, la société Genefinance est fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la société Genefinance au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt du 9 juillet 2019 de la cour administrative d'appel de Versailles est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Versailles.<br/>
Article 3 : L'Etat versera à la société Genefinance la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la société Genefinance et au ministre de l'économie, des finances et de la relance. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
