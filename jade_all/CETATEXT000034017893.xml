<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034017893</ID>
<ANCIEN_ID>JG_L_2017_02_000000391241</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/01/78/CETATEXT000034017893.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 08/02/2017, 391241, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-02-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>391241</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT, ROBILLOT</AVOCATS>
<RAPPORTEUR>M. Matias de Sainte Lorette</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:391241.20170208</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
<br/>
              Par une décision n° 391241 du 13 avril 2016, le Conseil d'Etat statuant au contentieux a prononcé l'admission des conclusions du pourvoi de M. B...C...dirigées contre l'arrêt n° 14MA00798 du 21 avril 2015 de la cour administrative d'appel de Marseille en tant qu'il s'est prononcé sur les conclusions tendant à la décharge des impositions en litige.<br/>
<br/>
              Par un mémoire en défense, enregistré le 15 juin 2016, le ministre des finances et des comptes publics s'en remet à la sagesse du Conseil d'Etat.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - la loi n° 99-1173 du 30 décembre 1999 ;<br/>
              - le décret n° 99-469 du 4 juin 1999 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Matias de Sainte Lorette, maître des requêtes en service extraordinaire, <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Potier de la Varde, Buk Lament, Robillot, avocat de M. C...;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que MM. A... C..., D...C...et B...C...étaient associés de la SCEA Saint-Jean, laquelle a fait l'objet d'une vérification de comptabilité au titre de l'année 2007. L'administration fiscale a remis en cause les dettes inscrites au passif du bilan, constituées de dettes fournisseurs pour un montant de 249 556 euros, d'intérêts d'emprunts auprès des établissements de crédits pour un montant de 248 197 euros et de provisions pour risques pour un montant de 75 618 euros. Chacun des associés a fait l'objet d'un redressement en matière d'impôt sur le revenu, à proportion de sa participation dans cette société, qui relevait du régime fiscal des sociétés de personnes. M. B...C...se pourvoit en cassation contre l'arrêt du 21 avril 2015 par lequel la cour administrative d'appel de Marseille a rejeté son appel formé contre le jugement du 19 décembre 2013 par lequel le tribunal administratif de Toulon a rejeté sa demande en décharge de l'imposition à laquelle il a été assujetti au titre de l'année 2007 et de l'obligation de payer procédant du commandement émis à son encontre le 26 août 2010. Par une décision du 13 avril 2016, le Conseil d'Etat a prononcé l'admission des conclusions du pourvoi de M. C...dirigées contre cet arrêt en tant seulement qu'il s'est prononcé sur les conclusions tendant à la décharge des impositions en litige.<br/>
<br/>
              2. Aux termes de l'article 1er du décret du 4 juin 1999 visé ci-dessus : " Il est institué un dispositif de désendettement au bénéfice des personnes mentionnées à l'article 2 qui, exerçant une profession non salariée ou ayant cessé leur activité professionnelle ou cédé leur entreprise, rencontrent de graves difficultés économiques et financières, les rendant incapables de faire face à leur passif. " Aux termes de l'article 8 du même décret : " Si la demande est déclarée éligible, le préfet assure le traitement du dossier ; avec le concours du trésorier-payeur général, il invite les créanciers et le débiteur à négocier un plan d'apurement global et définitif de l'ensemble de la dette de l'intéressé. Le plan établi comporte les abandons de créances librement acceptés et les modalités de paiement des sommes restant dues par le débiteur en fonction de ses capacités contributives et de la valeur de ses actifs. " Aux termes de l'article 9 du même décret : " Si les éléments du dossier le rendent indispensable, une aide de l'Etat peut être attribuée par le ministre chargé des rapatriés dans les limites de 0,5 MF et de 50 % du passif. " Aux termes de l'article 10 du même décret : " Lorsque le plan d'apurement signé par le débiteur et par ses créanciers comporte une demande d'aide de l'Etat, le préfet transmet le dossier à la commission. Celle-ci examine le plan d'apurement et statue sur la demande d'aide. Elle peut renvoyer le dossier au préfet pour qu'il procède à un examen complémentaire dans un délai de trois mois. ".<br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond que le GAEC Saint-Jean, devenu SCEA Saint-Jean, a sollicité, sur le fondement du décret du 4 juin 1999, son éligibilité au dispositif de désendettement des rapatriés réinstallés dans une profession non salariée. Son dossier a été déclaré éligible le 4 avril 2002. Une proposition de plan d'apurement sollicitant l'aide de l'Etat, non finalisée faute de l'obtention de la signature de l'ensemble des créanciers, a été adressée le 30 octobre 2004 par la SCEA Saint-Jean à la préfecture du Var et portait sur un total de créances bancaires et de créances fournisseurs de 746 598,72 euros. M. C...avait par ailleurs présenté à la cour administrative d'appel de Marseille un courrier du 30 juin 2010 du secrétaire général de la mission interministérielle aux rapatriés et un courrier du 20 avril 2011 du président de cette même mission dont il ressortait que cette mission avait adressé à la SCEA Saint-Jean, le 14 octobre 2008, une proposition de plan d'apurement de ses dettes comprenant une aide d'Etat de 217 098,68 euros. Il résulte de ce qui précède que la cour a dénaturé les pièces du dossier qui lui étaient soumis en jugeant que le requérant n'établissait pas le bien-fondé des écritures comptables de l'exercice 2007 constituées de dettes fournisseurs pour un montant de 249 556 euros, d'intérêts d'emprunts auprès des établissements de crédits pour un montant de 248 197 euros et de provisions pour risques pour un montant de 75 618 euros que l'administration avait remises en cause en les estimant éteintes du fait de la cessation de l'activité de la société au cours de l'année 2007. Par suite, et sans qu'il soit besoin de statuer sur les autres moyens du pourvoi, M. C... est fondé à demander l'annulation de l'arrêt qu'il attaque, en tant qu'il s'est prononcé sur ses conclusions tendant à la décharge des impositions en litige. <br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 2 000 euros à verser à M. C...sur le fondement de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 21 avril 2015 de la cour administrative de Marseille est annulé en tant qu'il s'est prononcé sur les conclusions tendant à la décharge des impositions en litige.<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, devant la cour administrative d'appel de Marseille.<br/>
Article 3 : L'Etat versera à M. B...C...une somme de 2 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M.  B...C...et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
