<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037220742</ID>
<ANCIEN_ID>JG_L_2018_07_000000418844</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/22/07/CETATEXT000037220742.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 1ère chambres réunies, 18/07/2018, 418844, Publié au recueil Lebon</TITRE>
<DATE_DEC>2018-07-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418844</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean-François de Montgolfier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:418844.20180718</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire en réplique et deux nouveaux mémoires, enregistrés les 8 mars, 19 mai, 17 juin et 26 juin 2018 au secrétariat du contentieux du Conseil d'Etat, M. C...B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 11 janvier 2018 par lequel la présidente de l'université Paris VIII Vincennes Saint-Denis a prononcé sa suspension à titre conservatoire ; <br/>
<br/>
              2°) d'enjoindre à l'Etat de prendre toutes mesures utiles de nature à lui permettre de reprendre ses fonctions et d'être rétabli dans ses droits pécuniaires. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution ; <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - le code de l'éducation, notamment son article L. 951-4 ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - le décret n° 84-431 du 6 juin 1984 ;<br/>
              - l'arrêté du ministre de l'enseignement supérieur et de la recherche du 10 février 2012 portant délégation de pouvoirs en matière de recrutement et de gestion de certains personnels enseignants des établissements publics d'enseignement supérieur et de recherche ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Vu la décision du Conseil d'Etat n°418844 du 30 mai 2018 décidant de ne pas renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M.  B... ; <br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-François de Montgolfier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 6 juillet 2018, présentée par M.B... ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 951-4 du code de l'éducation : " Le ministre chargé de l'enseignement supérieur peut prononcer la suspension d'un membre du personnel de l'enseignement supérieur pour un temps qui n'excède pas un an, sans suspension de traitement " ; que la suspension d'un professeur des universités, sur la base de ces dispositions, est une mesure à caractère conservatoire, prise dans le souci de préserver l'intérêt du service public universitaire ; qu'elle ne peut être prononcée que lorsque les faits imputés à l'intéressé présentent un caractère suffisant de vraisemblance et de gravité et que la poursuite des activités de l'intéressé au sein de l'établissement présente des inconvénients suffisamment sérieux pour le service ou pour le déroulement des procédures en cours ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier que, par un arrêté du 11 janvier 2018, la présidente de l'université Paris VIII Vincennes Saint-Denis, agissant par délégation du ministre chargé de l'enseignement supérieur, a, sur le fondement des dispositions de l'article L. 951-4 du code de l'éducation citées ci-dessus, suspendu M.B..., professeur des universités affecté à cette université, pour la durée de la procédure disciplinaire engagée à son encontre à la suite d'une plainte le visant, déposée par une personne travaillant dans le département d'études arabes qu'il dirige ; que M. B...demande l'annulation pour excès de pouvoir de cet arrêté ;<br/>
<br/>
              3. Considérant, en premier lieu, que le moyen tiré de ce que l'arrêté attaqué ne fait pas apparaître qu'il a été pris, au nom de l'Etat, en vertu de l'arrêté du 10 février 2012 portant délégation de pouvoirs en matière de recrutement et de gestion de certains personnels enseignants des établissements publics d'enseignement supérieur et de recherche, manque en fait ; <br/>
<br/>
              4. Considérant, en deuxième lieu, que M. B...n'est pas fondé à soutenir que l'arrêté le suspendant de ses fonctions dans le but exclusif de préserver, alors même qu'une procédure disciplinaire venait d'être engagée à son encontre pour des faits de " harcèlements sexuel et moral ", le bon fonctionnement du service public universitaire, revêt le caractère d'une sanction disciplinaire déguisée ; que, ayant ainsi pour objet de restaurer et préserver, dans l'intérêt de l'ensemble des étudiants et du corps enseignant, la sérénité nécessaire au déroulement des cours et aux activités de recherche universitaire, elle ne revêt pas davantage le caractère d'une mesure prise en considération de la personne au sens des dispositions de l'article L. 121-1 du code des relations entre le public et l'administration ; qu'il s'ensuit que si, pour apprécier le bien fondé de la mesure de suspension, la présidente de l'université Paris VIII Vincennes Saint-Denis aurait pu utilement entendre l'intéressé avant l'édiction de cette mesure, M. B...n'est pas fondé à soutenir que l'arrêté attaqué, faute d'avoir été précédé d'une procédure contradictoire, est entaché d'un vice de procédure ;  <br/>
<br/>
              5. Considérant, en troisième lieu, que M.B..., qui se borne à faire valoir que l'arrêté attaqué comprend deux mots pour lesquels il a été fait usage de l'écriture dite " inclusive ", ne saurait, en tout état de cause, sérieusement soutenir que les exigences posées par l'article 2 de la Constitution en vertu duquel " la langue de la République est le français " ont été méconnues ; <br/>
<br/>
              6. Considérant, en quatrième lieu, que M. B...soutient que les faits que l'administration lui impute ne revêtent pas le caractère de vraisemblance suffisant pour prendre légalement la mesure attaquée ; qu'eu égard à la nature de l'acte de suspension prévu par les dispositions de l'article L.951-4 du code de l'éducation et à la nécessité d'apprécier, à la date à laquelle cet acte a été pris, la condition de légalité tenant au caractère vraisemblable de certains faits, il appartient au juge de l'excès de pouvoir de statuer au vu des informations dont disposait effectivement l'autorité administrative au jour de sa décision ; que les éléments nouveaux qui seraient, le cas échéant, portés à la connaissance de l'administration postérieurement à sa décision, ne peuvent ainsi, alors même qu'ils seraient relatifs à la situation de fait prévalant à la date de l'acte litigieux, être utilement invoqués au soutien d'un recours en excès de pouvoir contre cet acte ; que l'administration est en revanche tenue d'abroger la décision en cause si de tels éléments font apparaître que la condition tenant à la vraisemblance des faits à l'origine de la mesure n'est plus satisfaite ;<br/>
<br/>
              7. Considérant qu'il ressort des pièces du dossier que, par un courrier daté du 20 décembre 2017, MmeA..., maître de conférence affectée au sein du département dirigé par M.B..., a dénoncé des faits de " harcèlement moral et sexuel " qui auraient été commis à son encontre par M. B...et demandé à la présidente de l'université de saisir la section disciplinaire de cette université  ; qu'avant ce courrier, l'intéressée avait fait part de ces agissements lors d'un entretien avec la présidente de l'université, de même qu'à des collègues enseignants-chercheurs, aux responsables du dispositif de prévention et de lutte contre les violences sexistes et sexuelles de Paris VIII et au médecin du travail de l'université, qui en avaient alerté l'administration ; que, par suite, et même si la matérialité de ces faits est contestée par M.B..., la présidente de l'Université Paris VIII Vincennes Saint-Denis a pu, en l'état de ces éléments portés alors à sa connaissance, estimer que les faits imputés à M. B...revêtaient un caractère suffisant de vraisemblance et de gravité ; qu'eu égard à ce caractère suffisant de vraisemblance et de gravité et compte tenu du retentissement de ces allégations au sein de l'université, elle n'a, par suite, pas fait une inexacte application des dispositions de l'article L.951-4 du code de l'éducation en prenant, le 11 janvier 2018, la mesure attaquée ; <br/>
<br/>
              8. Considérant, en cinquième lieu, que M. B...ne peut utilement soutenir que la mesure qu'il attaque, qui, ainsi qu'il a été dit, est une mesure conservatoire exclusivement prise en vue du bon fonctionnement du service public universitaire, aurait été prise en méconnaissance des stipulations de l'article 6, § 2, de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales en vertu desquelles " toute personne accusée d'une infraction est présumée innocente jusqu'à ce que sa culpabilité ait été légalement établie " ; <br/>
<br/>
              9. Considérant, en sixième lieu, qu'en prévoyant, à son article 2, que " durant la période de suspension, l'intéressé conservera l'intégralité de son traitement indiciaire ", sans mentionner qu'il en serait de même de ses primes, l'arrêté attaqué fait une exacte application des dispositions de l'article L. 951-4 du code de l'éducation citées au point 1; que, par suite, M. B... n'est pas fondé à soutenir que l'arrêté qu'il attaque est pour ce motif entaché d'erreur de droit, ni, en tout état de cause, qu'il a été édicté en méconnaissance d'un " principe général du droit relatif au maintien de la pleine rémunération " ; <br/>
<br/>
              10. Considérant, enfin, que le détournement de pouvoir allégué n'est pas établi ; <br/>
<br/>
              11. Considérant qu'il résulte de tout ce qui précède que la requête de M. B... doit être rejetée, y compris les conclusions à fin d'injonction qu'elle présente sur le fondement de l'article L. 911-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. B...est rejetée. <br/>
Article 2 : La présente décision sera notifiée à M. C...B...et à la ministre de l'enseignement supérieur, de la recherche et de l'innovation. <br/>
Copie en sera adressée à l'université de Paris VIII Vincennes-Saint-Denis.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-09-01 FONCTIONNAIRES ET AGENTS PUBLICS. DISCIPLINE. SUSPENSION. - SUSPENSION D'UN PROFESSEUR DES UNIVERSITÉS (ART. L. 951-4 DU CODE DE L'ÉDUCATION) - 1) NATURE DE LA MESURE - SANCTION DISCIPLINAIRE DÉGUISÉE - ABSENCE [RJ1] - MESURE PRISE EN CONSIDÉRATION DE LA PERSONNE - ABSENCE - CONSÉQUENCE - OBLIGATION DE MENER UNE PROCÉDURE CONTRADICTOIRE - ABSENCE - 2) LÉGALITÉ DE LA MESURE - CONDITION - FAITS PRÉSENTANT UN CARACTÈRE SUFFISANT DE VRAISEMBLANCE ET DE GRAVITÉ [RJ2] - ELÉMENTS NOUVEAUX PORTÉS À LA CONNAISSANCE DE L'ADMINISTRATION POSTÉRIEUREMENT À SA DÉCISION - ELÉMENTS SUSCEPTIBLES D'ÊTRE INVOQUÉS DEVANT LE JUGE DE L'EXCÈS DE POUVOIR - ABSENCE - ELÉMENTS OBLIGEANT L'ADMINISTRATION À ABROGER SA DÉCISION - EXISTENCE, DÈS LORS QU'ILS FONT APPARAÎTRE QUE LA CONDITION TENANT À LA VRAISEMBLANCE DES FAITS N'EST PLUS SATISFAITE [RJ3] - 3) ESPÈCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-09-03 FONCTIONNAIRES ET AGENTS PUBLICS. DISCIPLINE. MOTIFS. - SUSPENSION D'UN PROFESSEUR DES UNIVERSITÉS (ART. L. 951-4 DU CODE DE L'ÉDUCATION) - 1) NATURE DE LA MESURE - SANCTION DISCIPLINAIRE DÉGUISÉE - ABSENCE [RJ1] - MESURE PRISE EN CONSIDÉRATION DE LA PERSONNE - ABSENCE - CONSÉQUENCE - OBLIGATION DE MENER UNE PROCÉDURE CONTRADICTOIRE - ABSENCE - 2) LÉGALITÉ DE LA MESURE - CONDITION - FAITS PRÉSENTANT UN CARACTÈRE SUFFISANT DE VRAISEMBLANCE ET DE GRAVITÉ [RJ2] - ELÉMENTS NOUVEAUX PORTÉS À LA CONNAISSANCE DE L'ADMINISTRATION POSTÉRIEUREMENT À SA DÉCISION - ELÉMENTS SUSCEPTIBLES D'ÊTRE INVOQUÉS DEVANT LE JUGE DE L'EXCÈS DE POUVOIR - ABSENCE - ELÉMENTS OBLIGEANT L'ADMINISTRATION À ABROGER SA DÉCISION - EXISTENCE, DÈS LORS QU'ILS FONT APPARAÎTRE QUE LA CONDITION TENANT À LA VRAISEMBLANCE DES FAITS N'EST PLUS SATISFAITE [RJ3].
</SCT>
<ANA ID="9A"> 36-09-01 1) L'arrêté suspendant un professeur des universités de ses fonctions, pris sur le fondement de l'article L. 951-4 du code de l'éducation, dans le but exclusif de préserver, alors même qu'une procédure disciplinaire vient d'être engagée à son encontre pour des faits de harcèlements sexuel et moral, le bon fonctionnement du service public universitaire, ne revêt pas le caractère d'une sanction disciplinaire déguisée. Ayant ainsi pour objet de restaurer et préserver, dans l'intérêt de l'ensemble des étudiants et du corps enseignant, la sérénité nécessaire au déroulement des cours et aux activités de recherche universitaire, elle ne revêt pas davantage le caractère d'une mesure prise en considération de la personne au sens des dispositions de l'article L. 121-1 du code des relations entre le public et l'administration (CRPA). Il s'ensuit que si, pour apprécier le bien fondé de la mesure de suspension, la présidente de l'université aurait pu utilement entendre l'intéressé avant l'édiction de cette mesure, l'intéressé n'est pas fondé à soutenir que l'arrêté prononçant sa suspension, faute d'avoir été précédé d'une procédure contradictoire, est entaché d'un vice de procédure.,,,2) Eu égard à la nature de l'acte de suspension prévu par les dispositions de l'article L. 951-4 du code de l'éducation et à la nécessité d'apprécier, à la date à laquelle cet acte a été pris, la condition de légalité tenant au caractère vraisemblable de certains faits, il appartient au juge de l'excès de pouvoir de statuer au vu des informations dont disposait effectivement l'autorité administrative au jour de sa décision. Les éléments nouveaux qui seraient, le cas échéant, portés à la connaissance de l'administration postérieurement à sa décision, ne peuvent ainsi, alors même qu'ils seraient relatifs à la situation de fait prévalant à la date de l'acte litigieux, être utilement invoqués au soutien d'un recours en excès de pouvoir contre cet acte. L'administration est en revanche tenue d'abroger la décision en cause si de tels éléments font apparaître que la condition tenant à la vraisemblance des faits à l'origine de la mesure n'est plus satisfaite.</ANA>
<ANA ID="9B"> 36-09-03 1) L'arrêté suspendant un professeur des universités de ses fonctions, pris sur le fondement de l'article L. 951-4 du code de l'éducation, dans le but exclusif de préserver, alors même qu'une procédure disciplinaire vient d'être engagée à son encontre pour des faits de harcèlements sexuel et moral, le bon fonctionnement du service public universitaire, ne revêt pas le caractère d'une sanction disciplinaire déguisée. Ayant ainsi pour objet de restaurer et préserver, dans l'intérêt de l'ensemble des étudiants et du corps enseignant, la sérénité nécessaire au déroulement des cours et aux activités de recherche universitaire, elle ne revêt pas davantage le caractère d'une mesure prise en considération de la personne au sens des dispositions de l'article L. 121-1 du code des relations entre le public et l'administration (CRPA). Il s'ensuit que si, pour apprécier le bien fondé de la mesure de suspension, la présidente de l'université aurait pu utilement entendre l'intéressé avant l'édiction de cette mesure, l'intéressé n'est pas fondé à soutenir que l'arrêté prononçant sa suspension, faute d'avoir été précédé d'une procédure contradictoire, est entaché d'un vice de procédure.,,,2) Eu égard à la nature de l'acte de suspension prévu par les dispositions de l'article L. 951-4 du code de l'éducation et à la nécessité d'apprécier, à la date à laquelle cet acte a été pris, la condition de légalité tenant au caractère vraisemblable de certains faits, il appartient au juge de l'excès de pouvoir de statuer au vu des informations dont disposait effectivement l'autorité administrative au jour de sa décision. Les éléments nouveaux qui seraient, le cas échéant, portés à la connaissance de l'administration postérieurement à sa décision, ne peuvent ainsi, alors même qu'ils seraient relatifs à la situation de fait prévalant à la date de l'acte litigieux, être utilement invoqués au soutien d'un recours en excès de pouvoir contre cet acte. L'administration est en revanche tenue d'abroger la décision en cause si de tels éléments font apparaître que la condition tenant à la vraisemblance des faits à l'origine de la mesure n'est plus satisfaite.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 26 octobre 2005,,, n° 279189, p. 443 ; CE, 25 mars 2002,,n°s 224221 233719, T. pp. 587 ;, ,[RJ2] Cf. CE, 11 juin 1997,,, n° 142167, T. p. 905 ; CE, 10 décembre 2014,,n°s 363202 363373, T. pp. 694-719.,,[RJ3] Rappr. CE, 31 août 2009, Commune de Cregols, n° 296458, p. 343 ; CE, Ass., 6 juillet 2016,,l et autres, n° s 398234 399135, p. 320.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
