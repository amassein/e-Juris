<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032698981</ID>
<ANCIEN_ID>JG_L_2016_06_000000386837</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/69/89/CETATEXT000032698981.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème chambres réunies, 08/06/2016, 386837</TITRE>
<DATE_DEC>2016-06-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>386837</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Florence Marguerite</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:386837.20160608</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et trois mémoires, enregistrés les 31 décembre 2014, 22 avril, 7 octobre et 21 décembre 2015, au secrétariat du contentieux du Conseil d'Etat, M. A...B...demande au Conseil d'Etat d'annuler pour excès de pouvoir la décision implicite par laquelle le Premier ministre a rejeté sa demande reçue le 29 octobre 2014 tendant à l'abrogation totale ou partielle de l'article R. 723-45-2 du code de la sécurité sociale et la décision implicite par laquelle le ministre des affaires sociales, de la santé et des droits des femmes a rejeté sa demande tendant à l'abrogation de l'arrêté du 20 juin 2014 portant approbation des modifications apportées au règlement du régime complémentaire des avocats établi par la Caisse nationale des barreaux français et, en particulier, au deuxième alinéa de l'article 13, à l'article 8 et à l'article 9 de ce règlement.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code civil ;<br/>
              - le code de la sécurité sociale ; <br/>
              - la loi n° 71-1130 du 31 décembre 1971 ;<br/>
              - le décret n° 91-1197 du 27 novembre 1991 ;<br/>
              - la décision n° 386837 du 27 mars 2015 par laquelle le Conseil d'Etat, statuant au contentieux n'a pas renvoyé au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M. B...;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Florence Marguerite, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Sur les conclusions tendant à l'annulation du refus d'abroger l'article R. 723-45-2 du code de la sécurité sociale :<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 723-11-1 du code de la sécurité sociale, dans sa rédaction en vigueur à la date de la décision attaquée : " L'attribution de la pension de retraite est subordonnée à la cessation de l'activité d'avocat. / Par dérogation au précédent alinéa, et sous réserve que l'assuré ait liquidé ses pensions de vieillesse personnelles auprès de la totalité des régimes légaux ou rendus légalement obligatoires, de base et complémentaires, français et étrangers, ainsi que des régimes des organisations internationales dont il a relevé, une pension de vieillesse peut être entièrement cumulée avec une activité professionnelle : / a) A partir de l'âge prévu au 1° de l'article L. 351-8 ; / b) A partir de l'âge prévu au premier alinéa de l'article L. 351-1, lorsque l'assuré justifie d'une durée d'assurance et de périodes reconnues équivalentes mentionnée au deuxième alinéa du même article au moins égale à la limite mentionnée au même alinéa " ; qu'aux termes de l'article L. 723-5 du même code : " La caisse instituée par l'article L. 723-1 perçoit, outre le montant des droits de plaidoirie mentionnés à l'article L. 723-3, une cotisation annuelle obligatoire pour tous les avocats, à l'exception de ceux qui en sont exonérés. Elle peut être graduée suivant l'âge lors de la prestation de serment et l'ancienneté d'exercice depuis la prestation de serment. / La caisse perçoit également une cotisation assise sur les revenus définis en application des articles L. 131-6, L. 131-6-1 et L. 131-6-2 dans la limite d'un plafond fixé par décret " ; qu'enfin, l'article L. 723-5-1 prévoit une exonération partielle de cotisation en faveur des femmes ayant accouché durant l'année au titre de laquelle la cotisation est appelée ;<br/>
<br/>
              2. Considérant, d'une part, qu'il résulte des dispositions de l'article L. 723-11-1 du code de la sécurité sociale qu'un avocat ne peut cumuler sa pension de vieillesse avec une activité professionnelle qu'à la condition d'avoir liquidé ses pensions de vieillesse personnelles auprès de la totalité des régimes légaux ou rendus légalement obligatoires ; qu'ainsi, le législateur a nécessairement entendu exclure que la reprise d'activité par un avocat puisse lui permettre d'acquérir de nouveaux droits en matière de retraite ; que, d'autre part, les articles L. 723-5 et suivants du même code ne prévoient aucune exonération de cotisation en faveur des avocats qui cumulent leur pension de vieillesse avec un revenu d'activité ; que, par suite, l'article R. 723-45-2 du code de la sécurité sociale ne fait que tirer les conséquences de la loi en prévoyant que : " le versement des cotisations et contributions dues (...) pour des périodes postérieures à l'entrée en jouissance de la pension ne peut entraîner la révision de la pension déjà liquidée ni permettre l'acquisition de nouveaux droits " ; que M. B...n'est, en conséquence, pas fondé à soutenir que ces dispositions n'auraient pas de base légale ou méconnaîtraient la compétence confiée au législateur par l'article 34 de la Constitution ; qu'il ne peut utilement se prévaloir, pour contester leur légalité, de ce qu'elles méconnaîtraient le droit des contrats, notamment l'article 1134 du code civil, ou le principe d'égalité ; <br/>
<br/>
              3. Considérant qu'il résulte de ce qui précède que M. B...n'est pas fondé à soutenir que l'article R. 723-45-2 du code de la sécurité sociale est illégal et, par suite, à demander l'annulation de la décision implicite par laquelle le Premier ministre a rejeté sa demande tendant à son abrogation ;<br/>
<br/>
              Sur les conclusions tendant à l'annulation du refus d'abroger certaines dispositions du règlement du régime complémentaire des avocats : <br/>
<br/>
              4. Considérant qu'aux termes de l'article L. 723-14 du code de la sécurité sociale : " La caisse nationale des barreaux français peut décider l'institution pour les avocats d'un régime complémentaire obligatoire d'assurance vieillesse et survivants (...) " ; qu'aux termes de l'article L. 723-15 du même code : " Le régime complémentaire obligatoire est financé par les cotisations des assurés assises sur le revenu défini en application des articles L. 131-6, L. 131-6-1 et L. 131-6-2 ou sur les rémunérations brutes pour celles acquittées pour le compte des avocats visés au 19° de l'article L. 311-3, dans la limite d'un plafond (...) " ; que l'article L. 723-19 précise que : " Le régime complémentaire est régi par un règlement établi par la caisse nationale des barreaux français et approuvé par arrêté interministériel " ;  <br/>
<br/>
              5. Considérant que si les dernières modifications apportées au règlement du régime complémentaire des avocats par la caisse nationale des barreaux français, qui ont notamment conduit à une renumérotation de ses articles, ont été approuvées par arrêté du 20 juin 2014, la rédaction des trois dispositions contestées par M. B...ne procède pas de ces modifications ; que, par suite, ce dernier doit être regardé comme ayant entendu demander l'abrogation de l'approbation des articles 8, 9 et du deuxième alinéa de l'article 13 du règlement du régime complémentaire des avocats établi par la caisse nationale, selon la numérotation issue des modifications approuvées par l'arrêté du 20 juin 2014 ;<br/>
<br/>
              En ce qui concerne le deuxième alinéa de l'article 13 du règlement : <br/>
<br/>
              6. Considérant, en premier lieu, que, par l'article L. 723-19 du code de la sécurité sociale, le législateur a habilité la caisse nationale des barreaux français à établir et les ministres compétents à approuver l'ensemble des règles, autres que celles qui sont fixées par la loi, régissant le régime complémentaire obligatoire d'assurance vieillesse des avocats ; que, par suite, le requérant n'est pas fondé à soutenir que le deuxième alinéa de l'article 13 du règlement, qui prévoit que les cotisations versées au titre de périodes d'activité postérieures à la liquidation des droits ne peuvent entraîner la révision de celle-ci et n'ouvrent aucun droit supplémentaire, serait dépourvu de base légale ou méconnaîtrait la compétence confiée au législateur par l'article 34 de la Constitution ;<br/>
<br/>
              7. Considérant, en deuxième lieu, que les avocats qui reprennent ou poursuivent une activité professionnelle après avoir fait liquider leur pension de retraite ne se trouvent pas, au regard des règles relatives à la constitution du droit à pension, dans la même situation que les avocats qui exercent leur activité sans bénéficier d'une pension de retraite ; qu'en prévoyant, comme le fait d'ailleurs désormais l'article L. 161-22-1 A du code de la sécurité sociale issu de la loi du 20 janvier 2014 pour l'ensemble des régimes légaux ou rendus légalement obligatoires d'assurance vieillesse, que les droits à pension des avocats qui reprennent ou poursuivent une activité professionnelle après avoir fait liquider leur pension de retraite ne sont plus susceptibles d'être modifiés, sans pour autant les exonérer des cotisations dues par tout avocat à ce régime complémentaire obligatoire, la caisse nationale des barreaux français a instauré une différence de traitement qui est en rapport direct avec l'objectif de solidarité entre actifs et retraités poursuivi par ce régime et n'est pas manifestement disproportionnée au regard de la différence de situation qui la justifie ; qu'ainsi, le moyen tiré de la méconnaissance du principe d'égalité doit être écarté ; <br/>
<br/>
              8. Considérant, en dernier lieu, que M. B...ne peut utilement se prévaloir du droit des contrats, notamment de l'article 1134 du code civil selon lequel les conventions légalement formées tiennent lieu de loi à ceux qui les ont faites, pour contester la légalité de ces dispositions réglementaires prises en application des articles L. 723-14 et suivants du code de la sécurité sociale ;<br/>
<br/>
              En ce qui concerne l'article 8 du règlement : <br/>
<br/>
              9. Considérant que le moyen tiré de ce que l'article 8 du règlement du régime complémentaire des avocats prévoit des pénalités excessives n'est pas assorti de précisions suffisantes pour permettre d'en apprécier le bien-fondé ;<br/>
<br/>
              En ce qui concerne l'article 9 du règlement :<br/>
<br/>
              10. Considérant qu'aux termes de l'article 105 du décret du 27 novembre 1991 organisant la profession d'avocat, pris sur le fondement du 1° de l'article 53 de la loi du 31 décembre 1971 qui renvoie à un décret en Conseil d'Etat la fixation des conditions d'omission du tableau : " Peut être omis du tableau (...) : 2° L'avocat qui, sans motifs valables, n'acquitte pas dans les délais prescrits sa contribution aux charges de l'ordre ou sa cotisation à la Caisse nationale des barreaux français ou au Conseil national des barreaux, soit les sommes dues au titre des droits de plaidoirie ou appelées par la caisse au titre de la contribution équivalente (...) " ;<br/>
<br/>
              11. Considérant qu'en précisant que le recouvrement des cotisations par voie de rôle exécutoire s'exerce " sans préjudice de la sanction d'omission qui pourra être prononcée par le conseil de l'ordre ", l'article 9 du règlement du régime complémentaire des avocats se borne à rappeler la possibilité résultant des dispositions précitées de l'article 105 du décret du 27 novembre 1991 ; que, dès lors, M. B... n'est pas fondé à soutenir que cet article est entaché d'illégalité ;<br/>
<br/>
              12. Considérant qu'il résulte de ce qui précède que M. B...n'est pas fondé à demander l'annulation de la décision par laquelle le ministre des affaires sociales, de la santé et des droits des femmes, auquel le Premier ministre est réputé l'avoir transmise, a rejeté sa demande tendant à l'abrogation de l'arrêté d'approbation de l'article 8, de l'article 9, ainsi que du deuxième alinéa de l'article 13 du règlement du régime complémentaire des avocats ; <br/>
<br/>
              13. Considérant qu'il résulte de tout ce qui précède que la requête de M. B... doit être rejetée, sans qu'il soit besoin de se prononcer sur les fins de non-recevoir opposées par le ministre des affaires sociales, de la santé et des droits de femmes ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. B...est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. A...B..., au Premier ministre et à la ministre des affaires sociales et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-01-07-06-01-02-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉLAIS. RÉOUVERTURE DES DÉLAIS. ABSENCE. DÉCISION CONFIRMATIVE. ABSENCE. - SECOND REFUS D'ABROGER LE MÊME ACTE RÉGLEMENTAIRE (SOL.IMPL.) [RJ1].
</SCT>
<ANA ID="9A"> 54-01-07-06-01-02-01 Le refus d'abroger un acte réglementaire n'est pas purement confirmatif d'un refus antérieurement opposé à une demande tendant aux mêmes fins.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 27 avril 2011, Association pour une formation médicale indépendante, n° 334396, p. 168, aux Tables sur d'autres points ; CE, 25 juillet 2001, Fédération des syndicats généraux de l'éducation nationale et de la recherche publique, n° 210797, p. 389, sol. impl. ; CE, 14 octobre 2009, Commune de Saint Jean d'Aulps, n° 300608, T. p. 642, sol. impl..</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
