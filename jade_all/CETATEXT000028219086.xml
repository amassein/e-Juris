<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028219086</ID>
<ANCIEN_ID>JG_L_2013_11_000000367600</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/21/90/CETATEXT000028219086.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 20/11/2013, 367600</TITRE>
<DATE_DEC>2013-11-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>367600</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SPINOSI ; SCP BORE, SALVE DE BRUNETON</AVOCATS>
<RAPPORTEUR>Mme Sophie-Caroline de Margerie</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:367600.20131120</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance n° 13MA01156 du 6 avril 2013, enregistrée le 8 avril 2013 au secrétariat du contentieux du Conseil d'Etat, par laquelle le président de la cour administrative d'appel de Marseille a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête dont il a été saisi par Mme C...B...le 21 mars 2013 ; <br/>
<br/>
              Vu le pourvoi et le mémoire complémentaire, enregistrés les 11 avril et 2 juillet 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme C...B..., demeurant ...; MmeB...  demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 1200977 du 14 février 2013 par lequel le tribunal administratif de Bastia, à la demande de M. A...Leccia, a, d'une part, annulé la décision du 3 décembre 2012 par laquelle le préfet de la Corse-du-Sud a rejeté la demande de M. Leccia tendant à ce que MmeB..., conseillère municipale et maire de la commune de Cauro, soit déclarée démissionnaire d'office, d'autre part, déclaré Mme B...démissionnaire d'office de ses fonctions de conseillère municipale de la commune de Cauro, et, par voie de conséquence, de maire ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande tendant à ce qu'elle soit déclarée démissionnaire d'office ;<br/>
<br/>
              3°) de mettre à la charge de M. Leccia le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code électoral ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie-Caroline de Margerie, Conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Xavier Domino, Rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boré, Salve de Bruneton, avocat de MmeB..., et à Me Spinosi, avocat de M. Leccia ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 231 du code électoral : " (...) Ne peuvent être élus conseillers municipaux dans les communes situées dans le ressort où ils exercent ou ont exercé leurs fonctions depuis moins de six mois : (...) 8° (...) les directeurs généraux, directeurs, directeurs adjoints, chefs de service et chefs de bureau de la collectivité territoriale de Corse (...) " ; qu'aux termes de l'article L. 236 du même code : " Tout conseiller municipal qui, pour une cause survenue postérieurement à son élection, se trouve dans un des cas d'inéligibilité prévus par les articles L. 230, L. 231 et L. 232 est immédiatement déclaré démissionnaire d'office par le préfet, sauf réclamation au tribunal administratif dans les dix jours de la notification, et sauf recours au Conseil d'Etat, conformément aux articles L. 249 et L. 250. (...) " ; qu'il ressort de ces dispositions combinées que la procédure de démission d'office instituée par l'article L. 236 s'applique aux conseillers municipaux qui se trouvent, postérieurement à leur élection, dans une situation d'incompatibilité du fait de l'exercice de fonctions qui les auraient rendus inéligibles en application de l'article L. 231 ; que, pour autant, ne leur est pas applicable le délai d'inéligibilité de six mois suivant la cessation des fonctions qu'a institué l'article L. 231 pour les candidats à l'élection au conseil municipal ;  <br/>
<br/>
              2. Considérant que M. Leccia, conseiller municipal de la commune de Cauro, a demandé au préfet de la Corse-du-Sud de prononcer la démission d'office de MmeB..., conseillère municipale de la commune de Cauro depuis mars 2008 et élue maire le 28 novembre 2012, au motif que celle-ci occupait depuis le 21 juin 2012 le poste de chef de service de la gestion administrative à la collectivité territoriale de Corse, contrevenant ainsi aux dispositions du 8° de l'article L. 231 du code électoral ; qu'il avait toutefois été mis fin aux fonctions de Mme B... par arrêté du président du conseil exécutif de la Corse du 27 novembre 2012 ; qu'à cette date, la situation d'incompatibilité de Mme B...avait disparu ; que les conditions du prononcé de sa démission d'office sur le fondement de l'article L. 236 du même code n'étaient donc pas remplies ; qu'il résulte de ce qui précède que c'est à tort que le tribunal administratif s'est fondé sur le fait que Mme B...demeurait inéligible jusqu'au terme d'une période de six mois suivant la cessation de ses fonctions à la collectivité territoriale de Corse pour annuler la décision du 3 décembre 2012 du préfet de la Corse-du-Sud refusant de déclarer Mme B...démissionnaire d'office de son mandat de conseiller municipal et maire de la commune de Cauro et l'a déclarée démissionnaire d'office de ses fonctions ; que M. Leccia n'invoque pas d'autres griefs à l'appui de ses conclusions ; que par suite, Mme B...est fondée à soutenir que c'est à tort que, par le jugement attaqué du 14 février 2013, le tribunal administratif de Bastia a annulé la décision du 3 décembre 2012 du préfet de la Corse-du-Sud et l'a déclarée démissionnaire d'office de ses fonctions ;<br/>
<br/>
              3. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de MmeB..., qui n'est pas la partie perdante dans la présente instance, la somme demandée par M. Leccia au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de M. Leccia le versement à Mme B...de la somme de 3 000 euros qu'elle demande au même titre ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement en date du 14 février 2013 du tribunal administratif de Bastia est annulé.<br/>
<br/>
Article 2 : La demande présentée par M. Leccia devant le tribunal administratif de Bastia est rejetée.<br/>
<br/>
Article 3 : Les conclusions de Mme B...tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 4 : Les conclusions de M. Leccia tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à Mme C...B..., à M. A...Leccia et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">28-04-03-02 ÉLECTIONS ET RÉFÉRENDUM. ÉLECTIONS MUNICIPALES. INCOMPATIBILITÉS. FONCTIONS INCOMPATIBLES AVEC CELLES DE MAIRE OU D'ADJOINT. - DÉMISSION D'OFFICE POUR INCOMPATIBILITÉ (ART. L. 236 DU CODE ÉLECTORAL) - APPRÉCIATION DE LA SITUATION D'INCOMPATIBILITÉ - APPLICATION DU DÉLAI D'INÉLIGIBILITÉ (ART. L. 231 DU MÊME CODE) - ABSENCE.
</SCT>
<ANA ID="9A"> 28-04-03-02 La procédure de démission d'office instituée par l'article L. 236 du code électoral s'applique aux conseillers municipaux qui se trouvent, postérieurement à leur élection, dans une situation d'incompatibilité du fait de l'exercice de fonctions qui les auraient rendus inéligibles en application de l'article L. 231 du même code. Pour autant, la fin de l'incompatibilité permet de conserver le mandat, sans qu'il y ait à respecter le délai prévu pour que prenne fin une situation initiale d'inéligibilité, fixé par l'article L. 231.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
