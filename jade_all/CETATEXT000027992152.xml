<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027992152</ID>
<ANCIEN_ID>JG_L_2013_09_000000354677</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/99/21/CETATEXT000027992152.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 25/09/2013, 354677</TITRE>
<DATE_DEC>2013-09-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>354677</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GADIOU, CHEVALLIER</AVOCATS>
<RAPPORTEUR>M. Jean-Marc Vié</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:354677.20130925</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 6 décembre 2011 et 6 mars 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. A... C..., demeurant... ; il demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 11MA01671 du 3 octobre 2011 par lequel la cour administrative d'appel de Marseille a rejeté sa requête tendant à l'annulation du jugement n° 1000900 du 25 mars 2011 par lequel le tribunal administratif de Toulon, à la demande du préfet du Var, en premier lieu, lui a enjoint, sous astreinte définitive de 750 euros par jour de retard, de supprimer les installations édifiées sur le domaine public maritime ayant fait l'objet d'un procès-verbal de contravention de grande voirie le 30 juillet 2009, dans le délai d'un mois à compter de la notification du jugement, en deuxième lieu, l'a condamné au paiement d'une amende de 1 500 euros, et, enfin, a autorisé le préfet du Var à procéder à la suppression d'office de ces installations à ses risques et périls passé le délai d'un mois à compter de la notification du jugement ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général de la propriété des personnes publiques ;<br/>
<br/>
              Vu le décret n° 88-399 du 21 avril 1988 ;<br/>
<br/>
              Vu le décret n° 2004-309 du 29 mars 2004 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Vié, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gadiou, Chevallier, avocat de M. C... ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. C... a fait l'objet, le 30 juillet 2009, d'un procès-verbal de contravention de grande voirie pour avoir maintenu sans autorisation, sur le domaine public maritime de la plage de Pampelonne à Ramatuelle (Var), les installations de l'établissement " La Voile rouge " qu'il y exploitait ; que M. C...se pourvoit en cassation contre l'arrêt du 3 octobre 2011 par lequel la cour administrative d'appel de Marseille a rejeté sa requête tendant à l'annulation du jugement du 25 mars 2011 par lequel le tribunal administratif de Toulon, à la demande du préfet du Var, en premier lieu, lui a enjoint, sous astreinte de 750 euros par jour de retard, de supprimer les installations édifiées sur le domaine public maritime ayant fait l'objet de ce procès-verbal, dans un délai d'un mois à compter de la notification du jugement, en deuxième lieu, l'a condamné au paiement d'une amende de 1 500 euros, et, enfin, a autorisé le préfet du Var à procéder à la suppression d'office de ces installations à ses risques et périls après un délai d'un mois à compter de la notification du jugement ; <br/>
<br/>
              2. Considérant, en premier lieu, qu'aux termes de l'article L. 2132-2 du code général de la propriété des personnes publiques : " Les contraventions de grande voirie sont instituées par la loi ou par décret, selon le montant de l'amende encourue, en vue de la répression des manquements aux textes qui ont pour objet, pour les dépendances du domaine public n'appartenant pas à la voirie routière, la protection soit de l'intégrité ou de l'utilisation de ce domaine public, soit d'une servitude administrative mentionnée à l'article L. 2131-1. / Elles sont constatées, poursuivies et réprimées par voie administrative. " ; que lorsqu'il qualifie de contravention de grande voirie des faits d'occupation irrégulière d'une dépendance du domaine public, le juge administratif, saisi d'un procès-verbal de contravention de grande voirie accompagné ou non de conclusions de l'administration tendant à l'évacuation de cette dépendance, enjoint au contrevenant de libérer sans délai le domaine public et peut, s'il l'estime nécessaire, prononcer une astreinte en fixant lui-même, dans le cadre de son pouvoir d'appréciation, le point de départ de cette astreinte, sans être lié par la demande de l'administration ;<br/>
<br/>
              3. Considérant que la cour a relevé que le tribunal administratif de Toulon, alors qu'il était saisi de conclusions de l'administration tendant à ce que la condamnation de M. C... à libérer le domaine public maritime soit assortie d'une astreinte de 750 euros par jour de retard en cas d'inexécution du jugement dans un délai de trois mois, a fixé ce délai à un mois ; qu'elle n'a pas commis d'erreur de droit en jugeant qu'en modifiant ainsi le délai demandé par l'administration, les premiers juges n'ont pas excédé leur office ; <br/>
<br/>
              4. Considérant, en deuxième lieu, que pour constater que l'infraction, à caractère matériel, d'occupation irrégulière du domaine public, est constituée, le juge de la contravention de grande voirie doit déterminer, au vu des éléments de fait et de droit pertinents, si la dépendance concernée relève du domaine public ; que s'agissant du domaine public maritime, le juge doit appliquer les critères fixés par l'article L. 2111-4 du code général de la propriété des personnes publiques et n'est pas lié par les termes d'un arrêté, à caractère déclaratif, de délimitation du domaine public maritime, adopté sur le fondement du décret du 29 mars 2004 relatif à la procédure de délimitation du rivage de la mer, des lais et relais de la mer et des limites transversales de la mer à l'embouchure des fleuves et rivières ; que l'appartenance d'une dépendance au domaine public ne peut résulter de l'application d'un tel arrêté, dont les constatations ne représentent que l'un des éléments d'appréciation soumis au juge ; que le bien-fondé des poursuites pour contravention de grande voirie n'est donc pas subordonné à la légalité d'un tel acte ; que, par suite, en jugeant que M. C...ne pouvait, pour contester le bien-fondé des poursuites engagées contre lui, utilement exciper de l'illégalité de l'arrêté préfectoral du 11 juin 2008 portant délimitation du domaine public maritime de la plage de Pampelonne, la cour n'a pas commis d'erreur de droit ;<br/>
<br/>
              5. Considérant, en troisième lieu, qu'aux termes de l'article L. 2132-21 du code général de la propriété des personnes publiques : " Sous réserve de dispositions législatives spécifiques, les agents de l'Etat assermentés à cet effet devant le tribunal de grande instance et les officiers de police judiciaire sont compétents pour constater les contraventions de grande voirie. " ; que selon l'article 3 du décret du 21 avril 1988 relatif au statut particulier du corps des contrôleurs de travaux publics : " Les contrôleurs des trois grades sont chargés de la gestion et de l'exploitation des infrastructures de transport, de l'organisation et du contrôle des travaux neufs ou d'entretien réalisés par une entreprise ou en régie, du conseil et de l'assistance à la maîtrise d'ouvrage. / Ils participent à la mise en oeuvre des politiques de l'Etat et au contrôle du respect des réglementations relatives notamment à l'urbanisme, à la construction, à l'environnement et au domaine public. " ; que l'article 5 de ce décret dispose : " Les membres du corps des contrôleurs des travaux publics de l'Etat assurent la surveillance du domaine public. A cet effet, ils peuvent être assermentés pour constater les infractions " ; qu'il résulte de ces dispositions que, sous la seule réserve d'une assermentation, les contrôleurs de travaux publics de l'Etat sont compétents pour constater les contraventions de grande voirie commises, notamment, sur le domaine public maritime ;<br/>
<br/>
              6. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que, si la carte de commissionnement de M.B..., agent verbalisateur, ne mentionne pas expressément qu'il pouvait constater les infractions relatives au domaine public maritime, le procès-verbal de contravention de grande voirie dressé le 30 juillet 2009, dont les mentions font foi jusqu'à preuve contraire, précise que cet agent, exerçant les fonctions de surveillant du domaine public maritime, était assermenté conformément à la loi ; qu'ainsi, la cour n'a pas commis d'erreur de droit en écartant le moyen tiré de l'incompétence du signataire du procès-verbal ; <br/>
<br/>
              7. Considérant, en quatrième lieu, qu'en relevant que le procès-verbal de contravention de grande voirie établi à l'encontre de M.C..., d'une part, mentionnait que l'établissement de plage en cause et les tables, chaises et parasols, avaient été maintenus sans autorisation sur le domaine public maritime, d'autre part, précisait l'emprise totale des bâtiments, le lieu, l'époque et l'auteur de l'infraction et était accompagné de " photographies montrant  la présence de transats avec matelas sur la plage ainsi que d'un croquis faisant état de la présence de pergolas et de la superficie de l'emprise du bâti ", la cour n'a pas dénaturé les pièces du dossier ; qu'en déduisant de ces constatations que le procès-verbal était suffisamment précis pour permettre de désigner les biens meubles et immeubles à retirer du domaine public, la cour n'a pas commis d'erreur de droit ;<br/>
<br/>
              8. Considérant, enfin, que, pour écarter l'argumentation de M. C...selon laquelle il ne pouvait être regardé comme un occupant sans titre du domaine public, la cour a, d'une part, estimé, par une appréciation souveraine exempte de dénaturation, que l'intéressé ne disposait d'aucun titre l'autorisant à maintenir les installations de l'établissement " La Voile rouge " sur le domaine public maritime ; qu'elle a, d'autre part, relevé qu'elle avait en tout état de cause rejeté, par un arrêt du 7 décembre 2009 devenu définitif, la requête de M. C... tendant à l'annulation de la délibération du 28 juin 2001 du conseil municipal de la commune de Ramatuelle excluant le lot de plage n° 6, qu'il occupait, des dépendances du domaine public susceptibles d'être exploitées par un tiers, et que, par des décisions des 24 novembre 2010 et 27 janvier 2011, le Conseil d'Etat, statuant au contentieux avait rejeté les conclusions de l'intéressé tendant à l'annulation des décisions de la commission des délégations de service public de la commune de Ramatuelle rejetant sa candidature pour l'attribution d'un lot de plage ainsi que des délibérations du conseil municipal de cette commune décidant de déléguer l'exploitation des plages situées sur son territoire ; que la cour a en outre relevé que si M. C... avait contesté devant le tribunal administratif de Toulon la délibération du conseil municipal refusant d'abroger la délibération précitée du 28 juin 2001 ainsi que la décision de rejet de sa candidature du 29 janvier 2009 prise par la commission des délégations de service public de la commune, l'éventuelle annulation de ces décisions n'aurait, en tout état de cause, pas eu pour effet de conférer à M. C... une autorisation d'occupation du domaine public ; qu'en en déduisant que l'intéressé ne pouvait utilement se prévaloir ni de l'illégalité des actes mentionnés ci-dessus et ni des instances contentieuses pendantes devant la juridiction administrative pour soutenir qu'il n'était pas un occupant sans droit ni titre du domaine public maritime, la cour n'a pas commis d'erreur de droit ;<br/>
<br/>
              9. Considérant qu'il résulte de ce tout qui précède que le pourvoi de M. C... doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
 Article 1er : Le pourvoi de M. C...est rejeté.<br/>
<br/>
 Article 2 : La présente décision sera notifiée à M. A... C...et au ministre de l'écologie, du développement durable et de l'énergie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">24-01-03-01-04-015 DOMAINE. DOMAINE PUBLIC. PROTECTION DU DOMAINE. CONTRAVENTIONS DE GRANDE VOIRIE. POURSUITES. PROCÉDURE DEVANT LE JUGE ADMINISTRATIF. - POUVOIRS DU JUGE - OCCUPATION IRRÉGULIÈRE QUALIFIÉE DE CONTRAVENTION DE GRANDE VOIRIE - INJONCTION DE LIBÉRER SANS DÉLAI LE DOMAINE PUBLIC - FACULTÉ D'ASSORTIR CETTE INJONCTION D'UNE ASTREINTE - 1) POUVOIR DU JUGE DE FIXER LUI-MÊME LE POINT DE DÉPART DE CETTE ASTREINTE, SANS ÊTRE LIÉ PAR LA DEMANDE DE L'ADMINISTRATION -  EXISTENCE - 2) APPLICATION À L'ESPÈCE - DÉLAI FIXÉ À UN MOIS ALORS QUE L'ADMINISTRATION DEMANDAIT UN DÉLAI DE TROIS MOIS - MÉCONNAISSANCE PAR LE JUGE DE SON OFFICE - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">24-01-03-02 DOMAINE. DOMAINE PUBLIC. PROTECTION DU DOMAINE. PROTECTION CONTRE LES OCCUPATIONS IRRÉGULIÈRES. - OCCUPATION IRRÉGULIÈRE QUALIFIÉE DE CONTRAVENTION DE GRANDE VOIRIE - INJONCTION DE LIBÉRER SANS DÉLAI LE DOMAINE PUBLIC - FACULTÉ D'ASSORTIR CETTE INJONCTION D'UNE ASTREINTE - 1) POUVOIR DU JUGE DE FIXER LUI-MÊME LE POINT DE DÉPART DE CETTE ASTREINTE, SANS ÊTRE LIÉ PAR LA DEMANDE DE L'ADMINISTRATION -  EXISTENCE - 2) APPLICATION À L'ESPÈCE - DÉLAI FIXÉ À UN MOIS ALORS QUE L'ADMINISTRATION DEMANDAIT UN DÉLAI DE TROIS MOIS - MÉCONNAISSANCE PAR LE JUGE DE SON OFFICE - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-06-07-01 PROCÉDURE. JUGEMENTS. EXÉCUTION DES JUGEMENTS. ASTREINTE. - JUGE DE LA CONTRAVENTION DE GRANDE VOIRIE - INJONCTION DE LIBÉRER SANS DÉLAI LE DOMAINE PUBLIC IRRÉGULIÈREMENT OCCUPÉ - FACULTÉ D'ASSORTIR CETTE INJONCTION D'UNE ASTREINTE - 1) POUVOIR DU JUGE DE FIXER LUI-MÊME LE POINT DE DÉPART DE CETTE ASTREINTE, SANS ÊTRE LIÉ PAR LA DEMANDE DE L'ADMINISTRATION - EXISTENCE - 2) APPLICATION À L'ESPÈCE - DÉLAI FIXÉ À UN MOIS ALORS QUE L'ADMINISTRATION DEMANDAIT UN DÉLAI DE TROIS MOIS - MÉCONNAISSANCE PAR LE JUGE DE SON OFFICE - ABSENCE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">54-07-04 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. POUVOIRS DU JUGE RÉPRESSIF. - JUGE DE LA CONTRAVENTION DE GRANDE VOIRIE - INJONCTION DE LIBÉRER SANS DÉLAI LE DOMAINE PUBLIC IRRÉGULIÈREMENT OCCUPÉ - FACULTÉ D'ASSORTIR CETTE INJONCTION D'UNE ASTREINTE - 1) POUVOIR DU JUGE DE FIXER LUI-MÊME LE POINT DE DÉPART DE CETTE ASTREINTE, SANS ÊTRE LIÉ PAR LA DEMANDE DE L'ADMINISTRATION - EXISTENCE - 2) APPLICATION À L'ESPÈCE - DÉLAI FIXÉ À UN MOIS ALORS QUE L'ADMINISTRATION DEMANDAIT UN DÉLAI DE TROIS MOIS - MÉCONNAISSANCE PAR LE JUGE DE SON OFFICE - ABSENCE.
</SCT>
<ANA ID="9A"> 24-01-03-01-04-015 1) Lorsqu'il qualifie de contravention de grande voirie des faits d'occupation irrégulière d'une dépendance du domaine public, le juge administratif, saisi d'un procès-verbal de contravention de grande voirie accompagné ou non de conclusions de l'administration tendant à l'évacuation de cette dépendance, enjoint au contrevenant de libérer sans délai le domaine public et peut, s'il l'estime nécessaire, prononcer une astreinte en fixant lui-même, dans le cadre de son pouvoir d'appréciation, le point de départ de cette astreinte, sans être lié par la demande de l'administration.... ,,2) En l'espèce, le juge, saisi de conclusions de l'administration tendant à ce que la condamnation à libérer le domaine public maritime soit assortie d'une astreinte en cas d'inexécution du jugement dans un délai de trois mois, n'a pas excédé son office en modifiant le délai demandé par l'administration et en le fixant à un mois.</ANA>
<ANA ID="9B"> 24-01-03-02 1) Lorsqu'il qualifie de contravention de grande voirie des faits d'occupation irrégulière d'une dépendance du domaine public, le juge administratif, saisi d'un procès-verbal de contravention de grande voirie accompagné ou non de conclusions de l'administration tendant à l'évacuation de cette dépendance, enjoint au contrevenant de libérer sans délai le domaine public et peut, s'il l'estime nécessaire, prononcer une astreinte en fixant lui-même, dans le cadre de son pouvoir d'appréciation, le point de départ de cette astreinte, sans être lié par la demande de l'administration.... ,,2) En l'espèce, le juge, saisi de conclusions de l'administration tendant à ce que la condamnation à libérer le domaine public maritime soit assortie d'une astreinte en cas d'inexécution du jugement dans un délai de trois mois, n'a pas excédé son office en modifiant le délai demandé par l'administration et en le fixant à un mois.</ANA>
<ANA ID="9C"> 54-06-07-01 1) Lorsqu'il qualifie de contravention de grande voirie des faits d'occupation irrégulière d'une dépendance du domaine public, le juge administratif, saisi d'un procès-verbal de contravention de grande voirie accompagné ou non de conclusions de l'administration tendant à l'évacuation de cette dépendance, enjoint au contrevenant de libérer sans délai le domaine public et peut, s'il l'estime nécessaire, prononcer une astreinte en fixant lui-même, dans le cadre de son pouvoir d'appréciation, le point de départ de cette astreinte, sans être lié par la demande de l'administration.... ,,2) En l'espèce, le juge, saisi de conclusions de l'administration tendant à ce que la condamnation à libérer le domaine public maritime soit assortie d'une astreinte en cas d'inexécution du jugement dans un délai de trois mois, n'a pas excédé son office en modifiant le délai demandé par l'administration et en le fixant à un mois.</ANA>
<ANA ID="9D"> 54-07-04 1) Lorsqu'il qualifie de contravention de grande voirie des faits d'occupation irrégulière d'une dépendance du domaine public, le juge administratif, saisi d'un procès-verbal de contravention de grande voirie accompagné ou non de conclusions de l'administration tendant à l'évacuation de cette dépendance, enjoint au contrevenant de libérer sans délai le domaine public et peut, s'il l'estime nécessaire, prononcer une astreinte en fixant lui-même, dans le cadre de son pouvoir d'appréciation, le point de départ de cette astreinte, sans être lié par la demande de l'administration.... ,,2) En l'espèce, le juge, saisi de conclusions de l'administration tendant à ce que la condamnation à libérer le domaine public maritime soit assortie d'une astreinte en cas d'inexécution du jugement dans un délai de trois mois, n'a pas excédé son office en modifiant le délai demandé par l'administration et en le fixant à un mois.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
