<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037279269</ID>
<ANCIEN_ID>JG_L_2018_08_000000413113</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/27/92/CETATEXT000037279269.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 02/08/2018, 413113, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-08-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>413113</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>M. Alain Seban</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois De Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:413113.20180802</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif de Paris de condamner l'Etat à lui verser la somme totale de 42 000 euros en réparation des préjudices résultant de son absence de relogement. Par un jugement n° 1612751 du 9 juin 2017, le tribunal administratif a condamné l'Etat à lui verser une somme de 100 euros et rejeté le surplus de ses conclusions.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés au secrétariat du contentieux du Conseil d'Etat le 4 août et le 3 novembre 2017, Mme B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement en tant qu'il rejette le surplus des conclusions de sa demande ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit intégralement à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu :<br/>
              - le code de la construction et de l'habitation ;<br/>
	- le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alain Seban, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de MmeB....<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis au tribunal administratif de Paris que Mme B...a été reconnue comme prioritaire et devant être relogé en urgence par une décision de la commission de médiation de Paris du 5 septembre 2014, prise sur le fondement de l'article L. 441-2-3 du code de la construction et de l'habitation  ; que, par un jugement du 23 juin 2015, le tribunal administratif de Paris a enjoint au préfet de Paris de la reloger sous astreinte de 300 euros par mois de retard ; qu'en l'absence de relogement, l'intéressée a saisi le tribunal administratif de Paris d'une demande tendant à la condamnation de l'Etat à lui verser une somme de 42 000 euros en réparation des préjudices qu'elle estimait avoir subis ; qu'elle se pourvoit en cassation contre le jugement du 9 juin 2017 par lequel le tribunal administratif a limité à la somme de 100 euros l'indemnisation qu'il lui a accordée, en se fondant notamment, pour fixer cette somme, sur la circonstance que, dans sa demande de logement social en date du 21 janvier 2016, elle avait restreint ses voeux à la ville de Paris ;<br/>
<br/>
              2. Considérant qu'il résulte des dispositions du septième alinéa du II de l'article L. 441-2-3 du code de la construction et de l'habitation et des articles R. 441-16-1 et R. 441-16-3 du même code que, lorsqu'un demandeur a été reconnu comme prioritaire et devant être relogé en urgence par une commission de médiation, il incombe au représentant de l'Etat dans le département de définir le périmètre au sein duquel le logement à attribuer doit être situé, sans être tenu par les souhaits de localisation formulés par l'intéressé dans sa demande de logement social ; que le refus, sans motif sérieux, d'une proposition de logement adaptée est de nature à faire perdre à l'intéressé le bénéfice de la décision de la commission de médiation, pour autant qu'il ait été préalablement informé de cette éventualité conformément à l'article R. 441-16-3 du code de la construction et de l'habitation ; qu'en l'espèce, s'il ressort des énonciations du jugement attaqué que Mme B...a limité sa demande de logement social à la ville de Paris, le préfet de la région d'Ile-de-France, préfet de Paris, n'était pas tenu par ce souhait ; qu'il lui était loisible de proposer à l'intéressée un logement social dans le périmètre qu'il lui revenait de déterminer et qui pouvait inclure d'autres départements de la région Ile-de-France ; qu'ainsi, c'est au prix d'une erreur de droit que le tribunal administratif de Paris a jugé que la circonstance que l'intéressée avait limité sa demande de logement social à la ville de Paris était de nature à exonérer l'Etat d'une partie de sa responsabilité ; <br/>
<br/>
              3. Considérant qu'il résulte de ce qui précède, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, que Mme B...est fondée à demander l'annulation du jugement qu'elle attaque en tant qu'il limite à 100 euros l'indemnité mise à la charge de l'Etat ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat, au titre de l'article L. 761-1 du code de justice administrative, le versement à Mme B...de la somme de 3 000 euros ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 9 juin 2017 du tribunal administratif de Paris est annulé en tant qu'il limite à 100 euros l'indemnité mise à la charge de l'Etat.<br/>
Article 2 : L'affaire est renvoyée devant le tribunal administratif de Paris dans la mesure de l'annulation prononcée à l'article 1er.<br/>
<br/>
Article 3 : L'Etat versera à Mme B...une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à Mme A...B...et au ministre de la cohésion des territoires.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
