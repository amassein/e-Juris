<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028890887</ID>
<ANCIEN_ID>JG_L_2014_04_000000363166</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/89/08/CETATEXT000028890887.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème SSJS, 30/04/2014, 363166, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-04-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>363166</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Sophie Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:363166.20140430</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 1er octobre 2012 au secrétariat du contentieux du Conseil d'État, présentée par la société Yprema, dont le siège social est 7, rue Condorcet à Chennevières-sur-Marne (94437), représentée par son président en exercice ; la société Yprema demande au Conseil d'État :<br/>
<br/>
              1°) à titre principal, d'annuler pour excès de pouvoir la décision implicite de rejet résultant du silence gardé par le ministre de l'écologie, du développement durable et de l'énergie sur sa demande tendant à la modification de l'article 2 du décret n° 2012-602 du 30 avril 2012 relatif à la procédure de sortie du statut de déchet ;<br/>
<br/>
              2°) de substituer la terminologie d'" utilisateur " ou de " consommateur " à celle de " détenteur " figurant à l'article 2 du décret et de supprimer l'exigence, prévue par ce même article, d'attestation de conformité pour les activités répondant au système de gestion de la qualité ISO 9001 ;<br/>
<br/>
              3°) à titre subsidiaire, d'annuler pour excès de pouvoir l'article 2 du décret litigieux ;<br/>
<br/>
              4°) de mettre à la charge de l'État la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, et notamment son Préambule ; <br/>
<br/>
              Vu la directive 2008/98/CE du Parlement européen et du Conseil du 19 novembre 2008 ;<br/>
<br/>
              Vu le code de l'environnement ;<br/>
<br/>
              Vu l'ordonnance n° 2010-1579 du 17 décembre 2010 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que, par une lettre reçue le 6 juin 2012, la société Yprema a saisi le ministre de l'écologie, du développement durable et de l'énergie d'une demande tendant, à titre principal, à la modification des dispositions de l'article D. 541-13-12 du code de l'environnement, issues de l'article 2 du décret du 30 avril 2012 relatif à la procédure de sortie du statut de déchet et, subsidiairement, à l'abrogation de ces dispositions ; qu'elle demande au Conseil d'État, à titre principal, d'une part, d'annuler pour excès de pouvoir la décision implicite de rejet résultant du silence gardé par le Premier ministre sur cette demande, et d'autre part, de réformer l'article 2 du décret du 30 avril 2012 en substituant la terminologie d'" utilisateur " ou de " consommateur " à celle utilisée de " détenteur " à l'article D. 541-13-12 du code de l'environnement et en supprimant l'exigence, prévue par ce même article, d'attestation de conformité pour les activités répondant à un système de contrôle de qualité, et, à titre subsidiaire, d'annuler le même article 2 ;<br/>
<br/>
              Sur la fin de non-recevoir opposée par le ministre de l'écologie, du développement durable et de l'énergie :<br/>
<br/>
              2. Considérant que la société Yprema ne saurait, dans le cadre d'un recours pour excès de pouvoir, solliciter du Conseil d'Etat la modification des dispositions de l'article 2 du décret du 30 avril 2012 ; que ces conclusions sont irrecevables et ne peuvent, par suite, qu'être rejetées ;<br/>
<br/>
              Sur les conclusions tendant à l'annulation pour excès de pouvoir du décret attaqué :<br/>
<br/>
              En ce qui concerne la légalité externe du décret :<br/>
<br/>
              3. Considérant, d'une part, qu'aux termes de l'article 7 de la Charte de l'environnement : " Toute personne a le droit, dans les conditions et les limites définies par la loi (...) de participer à l'élaboration des décisions publiques ayant une incidence sur l'environnement " ; qu'aux termes de l'article L. 120-1 du code de l'environnement, dans sa rédaction applicable à la date du décret contesté : " Le présent article définit les conditions et limites dans lesquelles le principe de participation du public défini à l'article 7 de la Charte de l'environnement est applicable aux décisions réglementaires de l'État et de ses établissements publics. / I. - Sauf disposition particulière relative à la participation du public prévue par le présent code ou par la législation qui leur est applicable, les décisions réglementaires de l'État (...) sont soumises à participation du public lorsqu'elles ont une incidence directe et significative sur l'environnement (...) " ;<br/>
<br/>
              4. Considérant que les dispositions de l'article L. 120-1 du code de l'environnement ont été prises afin de préciser les conditions et les limites dans lesquelles le principe de participation du public défini à l'article 7 de la Charte de l'environnement est applicable aux décisions réglementaires de l'État et de ses établissements publics ; que, par suite, la requérante n'est pas fondée à se prévaloir, pour soutenir que le principe de participation aurait été méconnu lors de l'adoption du décret attaqué, d'un moyen fondé sur la méconnaissance des dispositions de l'article 7 de la Charte de l'environnement ; <br/>
<br/>
              5. Considérant, d'autre part, que la société requérante soutient que le décret qu'elle attaque n'a pas été adopté, en méconnaissance du 4° du II de l'article L. 110-1 du code de l'environnement, au terme d'une procédure de consultation du public et de concertation à laquelle elle aurait été associée, alors qu'elle est un " acteur majeur de la filière de recyclage des déchets inertes " ; que, toutefois, cette disposition législative se borne à énoncer des principes dont la portée a vocation à être définie dans le cadre d'autres lois ; que doit par suite, et en tout état de cause, être écarté le moyen tiré de ce que la procédure d'adoption du décret attaqué a méconnu le 4° du II de l'article L. 110-1 du code de l'environnement ;<br/>
<br/>
              En ce qui concerne la légalité interne du décret :<br/>
<br/>
              6. Considérant que l'article D. 541-12-13 du code de l'environnement, issu de l'article 2 du décret litigieux, dispose : " Les exploitants des installations mentionnées à l'article L. 214-1 ou à l'article L. 511-1 qui mettent en oeuvre la procédure de sortie du statut de déchets délivrent, pour chaque lot de substances ou objets qui ont cessé d'être des déchets, une attestation de conformité. / Ils transmettent cette attestation de conformité au détenteur suivant. Ils en conservent une copie pendant au moins cinq ans. Cette copie est tenue à disposition des autorités compétentes. / Le modèle et le contenu de cette attestation de conformité sont définis dans les arrêtés mentionnés aux articles D. 541-12-10 ou D. 541-12-15. " ;<br/>
<br/>
              7. Considérant, d'une part, qu'aux termes de l'article 11 de la directive 2008/98/CE du 19 novembre 2008 relative aux déchets : " (...) Afin de se conformer aux objectifs de la présente directive et de tendre vers une société européenne du recyclage, avec un niveau élevé de rendement des ressources, les États membres prennent les mesures nécessaires pour parvenir aux objectifs suivants : (...) b) d'ici 2020, la préparation en vue du réemploi, le recyclage et les autres formules de valorisation de matière, y compris les opérations de remblayage qui utilisent des déchets au lieu d'autres matériaux, des déchets non dangereux de construction et de démolition, à l'exclusion des matériaux géologiques naturels définis dans la catégorie 17 05 04 de la liste des déchets, passent à un minimum de 70 % en poids. (...) " ; que l'article L. 541-1 du code de l'environnement, issu de l'ordonnance du 17 décembre 2010 portant diverses dispositions d'adaptation au droit de l'Union européenne dans le domaine des déchets précise, au sujet du chapitre Ier du titre IV du livre V de ce code, intitulé " prévention et gestion des déchets ", que : " les dispositions du présent chapitre et de l'article L. 125-1 ont pour objet : (...) 2° De mettre en oeuvre une hiérarchie des modes de traitement des déchets consistant à privilégier, dans l'ordre : a) La préparation en vue de la réutilisation ; / b) Le recyclage ; / c) Toute autre valorisation, notamment la valorisation énergétique ; / d) L'élimination ; / 3° D'assurer que la gestion des déchets se fait sans mettre en danger la santé humaine et sans nuire à l'environnement, notamment sans créer de risque pour l'eau, l'air, le sol, la faune ou la flore, (...) " ;<br/>
<br/>
              8. Considérant, d'autre part, qu'aux termes de l'article L. 541-4-3 du code de l'environnement : " Un déchet cesse d'être un déchet après avoir été traité dans une installation visée à l'article L. 214-1 soumise à autorisation ou à déclaration ou dans une installation visée à l'article L. 511-1 soumise à autorisation, à enregistrement ou à déclaration et avoir subi une opération de valorisation, notamment de recyclage ou de préparation en vue de la réutilisation, s'il répond à des critères remplissant l'ensemble des conditions suivantes : / - la substance ou l'objet est couramment utilisé à des fins spécifiques ; / - il existe une demande pour une telle substance ou objet ou elle répond à un marché ; / - la substance ou l'objet remplit les exigences techniques aux fins spécifiques et respecte la législation et les normes applicables aux produits ; / - son utilisation n'aura pas d'effets globaux nocifs pour l'environnement ou la santé humaine. / Ces critères sont fixés par l'autorité administrative compétente. Ils comprennent le cas échéant des teneurs limites en substances polluantes et sont fixés en prenant en compte les effets nocifs des substances ou de l'objet sur l'environnement. / Les modalités d'application du présent article sont fixées par décret. " ;<br/>
<br/>
              9. Considérant, en premier lieu, que l'attestation de conformité prévue par l'article D. 541-12-13 du code de l'environnement, issu de l'article 2 du décret attaqué, qui porte sur des substances ou objets ayant cessé d'être des déchets conformément à l'article L. 541-4-3 du code de l'environnement, est destinée à contrôler le respect des critères posés par ces dispositions ; que, par suite, les dispositions de l'article D. 541-12-13 issues du décret attaqué ne  méconnaissent pas celles de l'article L. 541-4-3 du code de l'environnement, mais permettent au contraire d'en assurer la correcte application ; qu'elles ne sauraient par ailleurs méconnaître celles de l'arrêté du 29 février 2012 fixant le contenu des registres mentionnés aux articles R. 541-43 et R. 541-46 du code de l'environnement, dès lors que le respect du principe de légalité impose seulement qu'un texte soit conforme aux normes qui lui sont supérieures dans la hiérarchie des normes ; <br/>
<br/>
              10. Considérant, en deuxième lieu, que la délivrance de l'attestation prévue à l'article D. 541-12-13 vise à garantir, dans le cadre de la relation entre l'exploitant et son client que le produit vendu satisfait aux conditions définies à l'article L. 541-4-3 ; que, pour contraignante qu'elle soit, cette exigence répond, contrairement à ce qui est soutenu, à une finalité distincte de l'obligation, faite aux exploitants par l'article D. 541-12-14 du code issu du même article 2 du décret attaqué, d'appliquer à la procédure de sortie du statut de déchets un système de gestion de la qualité, dont l'objet est de garantir la réalisation de contrôles du processus de traitement des déchets ; qu'il ne ressort pas des pièces du dossier qu'en exigeant la délivrance d'une attestation de conformité, l'article D. 541-12-13, issu de l'article 2 du décret attaqué, ferait peser sur les exploitants une contrainte excessive de nature à méconnaître les objectifs rappelés au point 7, notamment celui de réemploi et de recyclage de 70 % des déchets non dangereux de construction et de démolition fixé par l'article 11 de la directive du 19 novembre 2008 et le principe de hiérarchie des modes de traitement des déchets posé par l'article L. 541-1 du code de l'environnement  ; <br/>
<br/>
              11. Considérant, en dernier lieu, que l'article L. 541-1 du code l'environnement définit, au sens des dispositions du chapitre 1er du titre IV du livre V de la partie législative du même code issues de l'ordonnance du 17 décembre 2010 et prises pour la transposition de la directive du 19 novembre 2008, le " détenteur de déchets " comme le producteur des déchets ou toute autre personne qui se trouve en possession des déchets ; que l'article D. 541-12-13 du code de l'environnement issu du décret attaqué, cité au point 6, vise les seuls détenteurs de substances ou d'objets qui n'ont plus le statut juridique de déchet ; que, dès lors, le moyen tiré de ce que l'emploi du terme " détenteur " par l'article D. 541-12-13 du code de l'environnement serait de nature à créer une confusion entre le régime juridique des déchets et celui qui est applicable aux substances ou objets ayant cessé de l'être ne peut qu'être écarté ;<br/>
<br/>
              En ce qui concerne la légalité de la décision du ministre de l'écologie, du développement durable et de l'énergie rejetant la demande d'abrogation du décret attaqué :<br/>
<br/>
              12. Considérant que l'autorité compétente, saisie d'une demande tendant à l'abrogation d'un règlement illégal, est tenue d'y déférer, soit que ce règlement ait été illégal dès sa signature, soit que l'illégalité résulte de circonstances de droit ou de fait postérieures à cette date ; que, toutefois, cette autorité ne saurait être tenue d'accueillir une telle demande dans le cas où l'illégalité du règlement a cessé, en raison d'un changement de circonstances, à la date à laquelle elle se prononce ;<br/>
<br/>
              13. Considérant que, contrairement à ce qui est soutenu et pour les motifs précédemment énoncés, la décision par laquelle le ministre de l'écologie, du développement durable et de l'énergie a rejeté la demande d'abrogation du décret attaqué n'est pas entachée d'illégalité ;<br/>
<br/>
              14. Considérant qu'il résulte de tout ce qui précède que la société Yprema n'est pas fondée à demander l'annulation pour excès de pouvoir du décret et de la décision du ministre de l'écologie, du développement durable et de l'énergie qu'elle attaque ; que ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
  Article 1er : La requête de la société Yprema est rejetée.<br/>
<br/>
  Article 2 : La présente décision sera notifiée à la société Yprema et à la ministre de l'écologie, du développement durable et de l'énergie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
