<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037022279</ID>
<ANCIEN_ID>JG_L_2018_06_000000405453</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/02/22/CETATEXT000037022279.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 1ère chambres réunies, 06/06/2018, 405453</TITRE>
<DATE_DEC>2018-06-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>405453</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 1ère chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY ; SCP WAQUET, FARGE, HAZAN ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>Mme Françoise Tomé</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:405453.20180606</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Orys a porté plainte contre M. C...A...devant la chambre disciplinaire de première instance du conseil régional du Centre de l'ordre des médecins. Le conseil départemental d'Indre-et-Loire de l'ordre des médecins s'est associé à la plainte. Par une décision n° 228 du 16 janvier 2014, la chambre disciplinaire de première instance a infligé à M. A... la sanction de l'avertissement.<br/>
<br/>
              Par une décision n° 12218 du 26 septembre 2016, la chambre disciplinaire nationale de l'ordre des médecins a rejeté l'appel formé par M. A...contre cette décision.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés le 28 novembre 2016 et les 28 février et 9 novembre 2017 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge du conseil départemental d'Indre-et-Loire de l'ordre des médecins la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de la santé publique ;<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Françoise Tomé, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat de M.A..., à la SCP Waquet, Farge, Hazan, avocat de la société Orys et à la SCP Matuchansky, Poupot, Valdelièvre, avocat du conseil départemental d'Indre-et-Loire de l'ordre des médecins ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M.A..., médecin du travail d'Electricité de France en fonction sur le site de Chinon, a rédigé, le 2 décembre 2011, un certificat médical en faveur de M.B..., salarié de la société Orys travaillant sur le même site, relatif à des faits qui s'étaient déroulés en avril 2011, à l'époque où M. B...travaillait, en qualité de salarié de la même société, sur un site de la société Areva situé au Tricastin ; que, ce certificat ayant été produit par M. B...devant le juge prud'homal dans le cadre d'une instance l'opposant à son employeur, la société Orys a porté plainte contre M. A...devant les instances disciplinaires de l'ordre des médecins, au motif qu'il avait, en établissant ce certificat, méconnu les obligations déontologiques fixées par les articles R. 4127-28 et R. 4127-76 du code de la santé publique ; que M. A...se pourvoit en cassation contre la décision du 26 septembre 2016 par laquelle la chambre disciplinaire nationale de l'ordre des médecins a rejeté son appel formé contre la décision du 16 janvier 2014 par laquelle la chambre disciplinaire de première instance lui a infligé la sanction de l'avertissement ;<br/>
<br/>
              Sur la recevabilité de la plainte de la société Orys :<br/>
<br/>
              2. Considérant, en premier lieu, qu'aux termes de l'article R. 4126-1 du code de la santé publique : " L'action disciplinaire contre un médecin (...) ne peut être introduite devant la chambre disciplinaire de première instance que par l'une des personnes ou autorités suivantes : / 1° Le conseil national ou le conseil départemental de l'ordre au tableau duquel le praticien poursuivi est inscrit à la date de la saisine de la juridiction, agissant de leur propre initiative ou à la suite de plaintes, formées notamment par les patients, les organismes locaux d'assurance maladie obligatoires, les médecins-conseils chefs ou responsables du service du contrôle médical placé auprès d'une caisse ou d'un organisme de sécurité sociale, les associations de défense des droits des patients, des usagers du système de santé ou des personnes en situation de précarité, qu'ils transmettent, le cas échéant en s'y associant, dans le cadre de la procédure prévue à l'article L. 4123-2 ; (...) " ;  que ces dispositions confèrent à toute personne, lésée de manière suffisamment directe et certaine par le manquement d'un médecin à ses obligations déontologiques, la faculté d'introduire, par une plainte portée devant le conseil départemental de l'ordre et transmise par celui-ci au juge disciplinaire, une action disciplinaire à l'encontre de ce médecin, en cas d'échec de la conciliation organisée conformément aux dispositions de l'article L. 4123-2 du même code ;<br/>
<br/>
              3. Considérant que si ces dispositions permettent ainsi à un employeur, dès lors qu'il est lésé de manière suffisamment directe et certaine par un certificat ou une attestation établie par un médecin du travail, d'introduire une plainte disciplinaire à l'encontre de ce médecin, cette faculté n'a pas pour effet d'imposer au médecin poursuivi de méconnaître le secret médical pour assurer sa défense ou de limiter son droit à se défendre ; que, par suite, la chambre disciplinaire nationale n'a pas commis d'erreur de droit en jugeant, pour statuer sur la recevabilité de la plainte de la société Orys, que les dispositions citées ci-dessus de l'article R. 4126-1 du code de la santé publique ne méconnaissent pas les dispositions de l'article L. 1110-4 du même code relatif à la protection du secret médical et ne sont pas incompatibles avec les stipulations de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, relatives au droit à un procès équitable ;<br/>
<br/>
              4. Considérant en deuxième lieu qu'aux termes de l'article L. 4124-2 du code de la santé publique : " les médecins (...) chargés d'un service public et inscrits au tableau de l'ordre ne peuvent être traduits devant la chambre disciplinaire de première instance, à l'occasion des actes de leur fonction publique, que par le ministre chargé de la santé, le représentant de l'Etat dans le département, le procureur de la République ou, lorsque lesdits actes ont été réalisés dans un établissement public de santé, le directeur de l'agence régionale de l'hospitalisation " ; que, contrairement à ce que soutient M.A..., la délivrance, par un médecin du travail, d'un certificat médical à un salarié de l'entreprise dans laquelle il exerce ses fonctions, ne revêt pas le caractère d'un acte de fonction publique accompli par un médecin chargé d'un service public, au sens de ces dispositions, alors même que l'entreprise au sein de laquelle il exerce ses fonctions serait, quant à elle, chargée de missions de service public ; que, par suite, en jugeant que ces mêmes dispositions ne faisaient pas obstacle à la recevabilité de la plainte de la société Orys, la chambre disciplinaire nationale n'a pas commis d'erreur de droit ;<br/>
<br/>
              5. Considérant, enfin, qu'en jugeant que la mention, dans un certificat médical produit par un salarié devant le juge prud'homal dans le cadre d'un litige l'opposant à son employeur, d'un " enchaînement délétère de pratiques maltraitantes " de la part de ce dernier, lésait cet employeur de manière suffisamment directe et certaine pour que sa plainte dirigée contre le médecin auteur de ce certificat soit recevable, la chambre disciplinaire nationale a exactement qualifié les faits qui lui était soumis et n'a pas commis d'erreur de droit ;<br/>
<br/>
              Sur la sanction infligée par la décision attaquée :<br/>
<br/>
              6. Considérant qu'aux termes de l'article R. 4127-28 du code la santé publique : " La délivrance d'un rapport tendancieux ou d'un certificat de complaisance est interdite " ; qu'aux termes de l'article R. 4127-76 du même code : " L'exercice de la médecine comporte normalement l'établissement par le médecin, conformément aux constatations médicales qu'il est en mesure de faire, des certificats, attestations et documents dont la production est prescrite par les textes législatifs et réglementaires " ; que ces obligations déontologiques s'imposent aux médecins du travail comme à tout médecin, y compris dans l'exercice des missions qui leur sont confiées par les dispositions du titre II du livre VI de la quatrième partie du code du travail ; <br/>
<br/>
              7. Considérant qu'il appartient toutefois au juge disciplinaire d'apprécier le respect des obligations déontologiques en tenant compte des conditions dans lesquelles le médecin exerce son art et, en particulier, s'agissant des médecins du travail, des missions et prérogatives qui sont les leurs ; qu'il résulte des dispositions de l'article L. 4622-3 du code du travail que le rôle du médecin du travail " consiste à éviter toute altération de la santé des travailleurs du fait de leur travail, notamment en surveillant leurs conditions d'hygiène au travail, les risques de contagion et leur état de santé (...) " et qu'à cette fin, l'article R. 4624-3 du même code lui confère le droit d'accéder librement aux lieux de travail et d'y réaliser toute visite à son initiative ; que, par suite, la circonstance qu'un certificat établi par un médecin du travail prenne parti sur un lien entre l'état de santé de ce salarié et ses conditions de vie et de travail dans l'entreprise, n'est pas, par elle-même, de nature à méconnaître les obligations déontologiques résultant des articles R. 4127-28 et R. 4127-76 du code du travail cités au point 6 ; que le médecin ne saurait, toutefois, établir un tel certificat qu'en considération de constats personnellement opérés par lui, tant sur la personne du salarié que sur son milieu de travail ;<br/>
<br/>
              8. Considérant que le juge du fond a souverainement relevé, sans dénaturer les termes du certificat médical litigieux, que M.A..., par ce certificat établi en sa qualité de médecin du travail d'Electricité de France en fonction sur le site de Chinon, d'une part, avait pris parti sur le bien-fondé d'un " droit de retrait " exercé plus de huit mois plus tôt sur un site de la société Areva situé au Tricastin qu'il ne connaissait pas, d'autre part, avait laissé entendre que la société Orys ne respectait pas ses obligations en terme de protection de la santé des salariés sans préciser les éléments qui le conduisaient à une telle suspicion et qu'il aurait été à même de constater, et enfin, reprochait notamment à cette société des " pratiques maltraitantes " sans là encore faire état de faits qu'il aurait pu lui-même constater ; qu'en jugeant que M.A..., en prenant ainsi en considération pour établir le certificat médical litigieux des faits qu'il n'avait pas personnellement constatés, avait méconnu les dispositions des articles R. 4127-28 et R. 4127-76 du code de la santé publique citées au point 7, la chambre disciplinaire nationale, dont la décision est suffisamment motivée, a exactement qualifié les faits qui lui étaient soumis et n'a pas commis d'erreur de droit ; <br/>
<br/>
              9. Considérant qu'il résulte de tout ce qui précède que M. A...n'est pas fondé à demander l'annulation de la décision qu'il attaque ; que les dispositions de l'article L. 761-1 du code de justice administrative font par suite obstacle à ce qu'une somme soit mise à ce titre à la charge du conseil départemental d'Indre-et-Loire de l'ordre des médecins qui n'est pas, dans la présente instance, la partie perdante ; qu'il n'y a pas lieu dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la société Orys sur le fondement des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A...est rejeté.<br/>
Article 2 : Les conclusions présentées par la société Orys au titre de l'article L. 761-1 du code de la justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. C...A..., à la société Orys, au conseil départemental d'Indre-et-Loire de l'ordre des médecins et au Conseil national de l'ordre des médecins.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">55-04-01-01 PROFESSIONS, CHARGES ET OFFICES. DISCIPLINE PROFESSIONNELLE. PROCÉDURE DEVANT LES JURIDICTIONS ORDINALES. INTRODUCTION DE L'INSTANCE. - ORDRE DES MÉDECINS - ACTIONS DISCIPLINAIRES INTRODUITES DEVANT LE CONSEIL DE L'ORDRE - 1) PERSONNES AUTORISÉES À INTRODUIRE UNE ACTION (1° DE L'ART. R. 4126-1 DU CSP) - PRINCIPE - PERSONNES LÉSÉES DE MANIÈRE SUFFISAMMENT DIRECTE ET CERTAINE PAR LE MANQUEMENT D'UN MÉDECIN À SES OBLIGATIONS DÉONTOLOGIQUES [RJ1] - 2) MÉDECIN DU TRAVAIL MIS EN CAUSE PAR UN EMPLOYEUR - A) MÉDECIN TENU DE MÉCONNAÎTRE LE SECRET MÉDICAL POUR ASSURER SA DÉFENSE - ABSENCE -  II) MENTION DANS UN CERTIFICAT MÉDICAL PRODUIT DEVANT LE JUGE PRUD'HOMAL DE PRATIQUES MALTRAITANTES D'UN EMPLOYEUR - INTÉRÊT LÉSÉ - EXISTENCE - C) ACTES SUSCEPTIBLES D'ÊTRE TRADUITS DEVANT LA CHAMBRE DISCIPLINAIRES - ACTES ACCOMPLIS PAR UN MÉDECIN CHARGÉ D'UN SERVICE PUBLIC À L'OCCASION DE SA FONCTION PUBLIQUE - DÉLIVRANCE PAR UN MÉDECIN DU TRAVAIL D'UN CERTIFICAT MÉDICAL - MISSION DE SERVICE PUBLIC - ABSENCE, Y COMPRIS SI LE MÉDECIN EXERCE DANS UNE ENTREPRISE CHARGÉE DE MISSIONS DE SERVICE PUBLIC.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">66-03-04-01-02 TRAVAIL ET EMPLOI. CONDITIONS DE TRAVAIL. MÉDECINE DU TRAVAIL. STATUT DES MÉDECINS DU TRAVAIL DANS L'ENTREPRISE. ATTRIBUTIONS. - 1) DÉLIVRANCE D'UN CERTIFICAT MÉDICAL - MISSION DE SERVICE PUBLIC - ABSENCE, Y COMPRIS SI LE MÉDECIN EXERCE DANS UNE ENTREPRISE CHARGÉE DE MISSIONS DE SERVICE PUBLIC [RJ2] - 2) A) OBLIGATIONS DÉONTOLOGIQUES - FACULTÉ D'ÉTABLIR UN CERTIFICAT PRENANT PARTI SUR UN LIEN ENTRE L'ÉTAT DE SANTÉ D'UN SALARIÉ ET SES CONDITIONS DE TRAVAIL DANS L'ENTREPRISE - EXISTENCE, DÈS LORS QUE LE CERTIFICAT EST FONDÉ SUR DES FAITS PERSONNELLEMENT CONSTATÉS PAR LE MÉDECIN - B) ESPÈCE.
</SCT>
<ANA ID="9A"> 55-04-01-01 1) a) L'article R. 4126-1 du code de la santé publique (CSP) confère à toute personne, lésée de manière suffisamment directe et certaine par le manquement d'un médecin à ses obligations déontologiques, la faculté d'introduire, par une plainte portée devant le conseil départemental de l'ordre et transmise par celui-ci au juge disciplinaire, une action disciplinaire à l'encontre de ce médecin, en cas d'échec de la conciliation organisée conformément aux dispositions de l'article L. 4123-2 du même code.... ,,2) a) Si cet article permet ainsi à un employeur, dès lors qu'il est lésé de manière suffisamment directe et certaine par un certificat ou une attestation établie par un médecin du travail, d'introduire une plainte disciplinaire à l'encontre de ce médecin, cette faculté n'a pas pour effet d'imposer au médecin poursuivi de méconnaître le secret médical pour assurer sa défense ou de limiter son droit à se défendre. L'article R. 4126-1 du CSP ne méconnaît par suite pas l'article L. 1110-4 du même code relatif à la protection du secret médical et n'est pas incompatible avec les stipulations de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales relatives au droit à un procès équitable.... ,,b) La mention, dans un certificat médical produit par un salarié devant le juge prud'homal dans le cadre d'un litige l'opposant à son employeur, d'un enchaînement délétère de pratiques maltraitantes de la part de ce dernier, lése cet employeur de manière suffisamment directe et certaine pour que sa plainte dirigée contre le médecin auteur de ce certificat soit recevable.,,,c) La délivrance, par un médecin du travail, d'un certificat médical à un salarié de l'entreprise dans laquelle il exerce ses fonctions, ne revêt pas le caractère d'un acte de fonction publique accompli par un médecin chargé d'un service public, au sens de l'article L. 4124-2 du CSP, alors même que l'entreprise au sein de laquelle il exerce ses fonctions serait, quant à elle, chargée de missions de service public.</ANA>
<ANA ID="9B"> 66-03-04-01-02 1) La délivrance, par un médecin du travail, d'un certificat médical à un salarié de l'entreprise dans laquelle il exerce ses fonctions, ne revêt pas le caractère d'un acte de fonction publique accompli par un médecin chargé d'un service public, au sens de l'article L. 4124-2 du code de la santé publique (CSP), alors même que l'entreprise au sein de laquelle il exerce ses fonctions serait, quant à elle, chargée de missions de service public.,,,2) a) La circonstance qu'un certificat établi par un médecin du travail prenne parti sur un lien entre l'état de santé de ce salarié et ses conditions de vie et de travail dans l'entreprise, n'est pas, par elle-même, de nature à méconnaître les obligations déontologiques résultant des articles R.4127-28 et R.4127-76 du code du travail. Le médecin ne saurait, toutefois, établir un tel certificat qu'en considération de constats personnellement opérés par lui, tant sur la personne du salarié que sur son milieu de travail.... ,,b) Certificat médical établi par un médecin du travail de la société Electricité de France en fonction sur le site de Chinon, ayant pris parti sur le droit de retrait d'un des salariés d'une entreprise intervenant sur le site de Chinon exercé sur le site du Tricastin que le médecin ne connaissait pas, ayant laissé entendre que cette entreprise ne respectait pas ses obligations en termes de protection de la santé des salariés sans préciser les éléments qui le conduisaient à une telle suspicion et qu'il aurait été à même de constater et reprochant notamment à cette entreprise des pratiques malveillantes sans faire état de faits qu'il aurait pu lui-même constater. En prenant en considérantion pour établir ce certificat médical des faits qu'il n'avait pas personnellement constatés, ce médecin a méconnu les articles R. 4127-28 et R. 4127-76 du code de la santé publique (CSP).</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 11 octobre 2017, Association santé et médecine du travail SMT et autres, n° 403576, à mentionner aux Tables., ,[RJ2] Rappr., pour un certificat d'inaptitude, CE, 10 février 2016,,, n° 384299, T. p. 972.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
