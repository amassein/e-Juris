<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027800641</ID>
<ANCIEN_ID>JG_L_2013_08_000000357852</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/80/06/CETATEXT000027800641.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème / 8ème SSR, 01/08/2013, 357852</TITRE>
<DATE_DEC>2013-08-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>357852</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème / 8ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>Mme Angélique Delorme</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:357852.20130801</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 23 mars et 22 juin 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B... A..., demeurant... ; M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10DA00400 du 17 janvier 2012 par lequel la cour administrative d'appel de Douai a rejeté sa requête tendant à l'annulation du jugement n° 0802742 du 4 février 2010 par lequel le tribunal administratif de Rouen a rejeté sa demande tendant, à titre principal, à l'annulation de la décision du 15 janvier 2008 par laquelle le président de la chambre de commerce et d'industrie de Dieppe a procédé à son licenciement, à ce qu'il soit enjoint à cette dernière de le réintégrer dans son ancienne situation et à la condamnation de la chambre de commerce et d'industrie à lui verser la somme de 15 000 euros et, à titre subsidiaire, à la condamnation de la chambre de commerce et d'industrie de Dieppe à lui verser  une indemnité de 472 338,56 euros en réparation du préjudice subi du fait de son licenciement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de la chambre de commerce et d'industrie de Dieppe la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 52-1311 du 10 décembre 1952 ;<br/>
<br/>
              Vu l'arrêté interministériel du 25 juillet 1997 relatif au statut du personnel de l'assemblée des chambres françaises de commerce et d'industrie, des chambres régionales de commerce et d'industrie et des groupements inter-consulaires ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Angélique Delorme, Auditeur,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de M. B...A...et à la SCP Lyon-Caen, Thiriez, avocat de la chambre de commerce et d'industrie de Dieppe  ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. A...a été recruté et titularisé à compter du 1er février 2007 par la chambre de commerce et d'industrie de Dieppe en qualité de directeur technique et des moyens généraux ; qu'à la suite de la suppression de son emploi décidée par une délibération de l'assemblée générale du 12 novembre 2007, il a été licencié par une décision du président de la chambre de commerce et d'industrie de Dieppe en date du 15 janvier 2008 ; que M. A...a demandé au tribunal administratif de Rouen, à titre principal, d'annuler la décision du 15 janvier 2008, de condamner la chambre de commerce et d'industrie de Dieppe à lui verser la somme de 15 000 euros, de lui enjoindre de procéder à sa réintégration et, à titre subsidiaire, de condamner la chambre de commerce et d'industrie de Dieppe à lui verser une indemnité de 472 338,56 euros en réparation du préjudice subi du fait de son licenciement ; que, par un jugement du 4 février 2010, le tribunal administratif de Rouen a rejeté sa requête ; que M. A...se pourvoit en cassation contre l'arrêt du 17 janvier 2012 par lequel la cour administrative d'appel de Douai a rejeté l'appel qu'il a interjeté de ce jugement ;<br/>
<br/>
              2. Considérant, en premier lieu, qu'aux termes de l'article 33 du statut du personnel administratif des chambres de commerce et d'industrie : " La cessation de fonctions de tout agent titulaire ne peut intervenir que dans les conditions suivantes : (...) / 5) Par suppression d'emploi, après avis de la commission paritaire compétente (...) " ; qu'aux termes de l'article 35-1 du même statut : " Lorsqu'une Compagnie Consulaire décide de prendre des mesures pouvant entraîner un ou plusieurs licenciements par suppression d'emploi, le Président, au vu de la délibération prise en Assemblée Générale, convoque la Commission Paritaire Locale aux fins de l'informer. Un dossier est communiqué, au plus tard quinze jours avant la date de la réunion, aux membres de la Commission Paritaire Locale et aux délégués syndicaux. Ce dossier comprend : / - une information sur les raisons économiques, financières et techniques qui sont à l'origine de la suppression d'un ou plusieurs postes de travail ; / - une information sur les moyens examinés par la Compagnie Consulaire pour éviter les suppressions d'emplois tels que notamment : / les possibilités de création d'activités nouvelles, d'augmentation de ressources ou de diminution de charges, d'aménagement du temps de travail et/ou de réduction du temps de travail, de reclassement des agents dont l'emploi pourrait être supprimé dans d'autres services de la Compagnie Consulaire, d'autres Compagnies Consulaires ou à l'extérieur de l'Institution Consulaire ainsi que toutes autres mesures alternatives au licenciement ; / - la liste des emplois susceptibles d'être supprimés et les critères retenus ; / - le coût et les modalités de mise en oeuvre des mesures annoncées ; / - les aides et mesures d'accompagnement apportées aux agents licenciés pour faciliter leur réemploi telles que bilan de compétences ou financement de formations. (...) / La Compagnie Consulaire ne peut effectuer de recrutement sur poste permanent correspondant à un ou plusieurs emplois supprimés pendant un délai de dix-huit mois à compter de la (des) notification(s) de licenciement pour suppression d'emploi. Les autres emplois mis en recrutement pendant cette période doivent être proposés en priorité aux agents licenciés " ; qu'il résulte de ces dispositions que, lorsqu'une décision peut entraîner un ou plusieurs licenciements par suppression d'emploi, le président de l'organisme consulaire a l'obligation de communiquer à la commission paritaire locale, quinze jours au moins avant qu'elle ne se réunisse, un dossier comprenant plusieurs informations relatives aux causes des suppressions d'emplois, aux solutions envisagées pour les éviter, aux emplois supprimés, au coût et aux modalités des licenciements et aux mesures d'accompagnement proposées aux personnes concernées ; <br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la chambre de commerce et d'industrie de Dieppe a remis aux membres de la commission paritaire locale, qui s'est réunie le 5 décembre 2007, une note de deux pages dans laquelle figuraient l'information sur les raisons économiques, financières et techniques conduisant à la suppression de l'emploi de directeur technique et des moyens généraux, une information sur les moyens examinés pour éviter cette suppression d'emploi, la désignation de l'emploi devant être supprimé et les critères retenus à cette fin, le coût et les modalités de la mesure de licenciement ainsi que les aides et les mesures d'accompagnement envisagées ; que, par suite, en estimant que le document remis aux membres de la commission paritaire locale du 5 décembre 2007 comportait l'ensemble des informations requises par l'article 35-1 précité du statut des agents des chambres de commerce et d'industrie et dont elle a reproduit la liste et était ainsi de nature à répondre aux exigences de cet article, la cour n'a ni entaché son arrêt de dénaturation, ni commis d'erreur de droit ; <br/>
<br/>
              4. Considérant, en deuxième lieu, qu'en vertu des dispositions précitées de l'article 35-1 du statut, lorsqu'une compagnie consulaire décide de prendre des mesures pouvant entraîner un ou plusieurs licenciements par suppression d'emploi, il doit être communiqué aux membres de la commission paritaire locale un dossier comprenant notamment une information sur les moyens examinés par la compagnie consulaire pour éviter les suppressions d'emploi tels que les possibilités de reclassement des agents dont l'emploi pourrait être supprimé dans d'autres services de la compagnie consulaire, d'autres compagnies consulaires ou à l'extérieur de l'institution consulaire ; qu'aux termes de l'article 35-3 du statut du personnel administratif des chambres de commerce et d'industrie : " L'agent qui, dans la même compagnie consulaire, aura été reclassé, avec son accord, dans une situation inférieure à celle qu'il occupait auparavant, aura droit au paiement d'une indemnité différentielle pendant une durée maximum de trois ans " ; qu'il résulte de la combinaison de ces dispositions qu'avant de prononcer le licenciement pour suppression d'emploi d'un agent soumis au statut du personnel des chambres de commerce et d'industrie, il appartient à la compagnie consulaire d'examiner les possibilités de reclassement de cet agent notamment en son sein, tant sur des emplois équivalents que sur des emplois de rang hiérarchique inférieur ; que, toutefois, ces dispositions ne sauraient être interprétées comme faisant obligation au président de la chambre de commerce et d'industrie, préalablement à tout licenciement pour suppression d'emploi, d'examiner les possibilités de reclassement de l'agent concerné sur des postes sans rapport avec sa qualification et son rang hiérarchique ; que, par suite, en jugeant que le président de la chambre de commerce et d'industrie avait pu légalement prononcer le licenciement de M. A...sans lui avoir proposé de reclassement sur des postes sans rapport tant avec son rang hiérarchique qu'avec sa qualification, la cour n'a pas commis d'erreur de droit ; <br/>
<br/>
              5. Considérant, en troisième lieu, que le moyen invoqué devant la cour tiré de ce que la compagnie consulaire aurait méconnu les dispositions du dernier alinéa de l'article 35-1 du statut du personnel administratif des chambres de commerce et d'industrie en omettant de proposer  à M.A..., dans les dix-huit mois suivant son licenciement, des postes ouverts au recrutement et distincts de celui qu'il occupait précédemment, était sans incidence sur la légalité de la décision de licenciement de M.A... ; que ce moyen était, par suite, inopérant à l'encontre de ladite décision; que, par suite et en tout état de cause, la circonstance que la cour a expressément écarté ce moyen  est sans influence sur la régularité de son arrêt ; <br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que le pourvoi de M. A... doit être rejeté, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions de la chambre de commerce et d'industrie de Dieppe au titre de ces dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A...est rejeté.<br/>
Article 2 : Les conclusions de la chambre de commerce et d'industrie de Dieppe présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. B... A...et à la chambre de commerce et d'industrie de Dieppe.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">14-06-01-03 COMMERCE, INDUSTRIE, INTERVENTION ÉCONOMIQUE DE LA PUISSANCE PUBLIQUE. ORGANISATION PROFESSIONNELLE DES ACTIVITÉS ÉCONOMIQUES. CHAMBRES DE COMMERCE ET D'INDUSTRIE. PERSONNEL. - OBLIGATION D'EXAMEN DES POSSIBILITÉS DE RECLASSEMENT, TANT SUR DES EMPLOIS ÉQUIVALENTS QUE SUR DES EMPLOIS DE RANG HIÉRARCHIQUE INFÉRIEUR, PRÉALABLEMENT AU LICENCIEMENT D'UN AGENT POUR SUPPRESSION DE POSTE - EXISTENCE - LIMITE - OBLIGATION D'EXAMINER LES POSSIBILITÉS DE RECLASSEMENT DE L'AGENT SUR DES POSTES SANS RAPPORT AVEC SA QUALIFICATION ET SON RANG HIÉRARCHIQUE - ABSENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-08-02-02-01-03 PROCÉDURE. VOIES DE RECOURS. CASSATION. CONTRÔLE DU JUGE DE CASSATION. BIEN-FONDÉ. APPRÉCIATION SOUVERAINE DES JUGES DU FOND. - CARACTÈRE SUFFISANT DE L'INFORMATION D'UNE CAP.
</SCT>
<ANA ID="9A"> 14-06-01-03 Il résulte de la combinaison des dispositions des articles 35-1 et 35-3 du statut du personnel administratif des chambres de commerce et d'industrie (CCI) qu'avant de prononcer le licenciement pour suppression d'emploi d'un agent soumis au statut du personnel des CCI, il appartient à la compagnie consulaire d'examiner les possibilités de reclassement de cet agent notamment en son sein, tant sur des emplois équivalents que sur des emplois de rang hiérarchique inférieur. Toutefois, ces dispositions ne sauraient être interprétées comme faisant obligation au président de la CCI, préalablement à tout licenciement pour suppression d'emploi, d'examiner les possibilités de reclassement de l'agent concerné sur des postes sans rapport avec sa qualification et son rang hiérarchique.</ANA>
<ANA ID="9B"> 54-08-02-02-01-03 Le caractère, suffisant ou non, de l'information d'une commission administrative paritaire (CAP) relève de l'appréciation souveraine des juges du fond.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. Cass. soc., 24 septembre 2008, n° 07-42.200, Bull. 2008, V, n° 178.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
