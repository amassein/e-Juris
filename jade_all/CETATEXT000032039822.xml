<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032039822</ID>
<ANCIEN_ID>JG_L_2016_01_000000395827</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/03/98/CETATEXT000032039822.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 12/01/2016, 395827, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-01-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>395827</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2016:395827.20160112</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête enregistrée le 4 janvier 2016 au secrétariat du contentieux du Conseil d'Etat, M. B...A...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, d'ordonner la suspension de l'exécution de l'arrêté du 2 novembre 2015 du ministre de l'économie, de l'industrie et du numérique relatif aux tarifs des courses de taxi.<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - la condition d'urgence est remplie, eu égard aux coûts qu'implique la modification de la table tarifaire des taximètres, dès lors que cette modification doit intervenir, aux termes de l'arrêté contesté, avant le 1er mars 2016 ;<br/>
              - il existe un doute sérieux sur la légalité de l'arrêté contesté ;<br/>
              - l'arrêté est entaché d'une erreur manifeste d'appréciation en ce qu'il fait obstacle à l'application de suppléments tarifaires pour la prise en charge des animaux et des bagages pour les taxis  parisiens, sans que cette différence de traitement nouvelle trouve sa justification dans une différence de situation ;<br/>
              - l'arrêté méconnaît les dispositions de l'article 2 du décret n° 2015-1252 du 7 octobre 2015 en ce qu'il prévoit que le supplément pour la prise en charge de passagers supplémentaires est applicable à partir du cinquième passager et non du quatrième comme le permet cet article et alors que seulement 0,5% des taxis parisiens comptent plus de cinq places et pourront facturer ce supplément ;<br/>
              - le ministre de l'économie, de l'industrie et du numérique a excédé sa compétence et a commis une erreur manifeste d'appréciation, d'une part, en arrêtant un montant forfaitaire pour le prix des courses entre les aéroports franciliens et Paris alors qu'ils n'appartiennent pas à la même zone de tarification que Paris, d'autre part, en retenant des tarifs différenciés selon que le lieu de prise en charge ou de destination est localisé au nord ou au sud de la Seine dans Paris alors qu'il s'agit d'une même zone de tarification.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le décret n° 2015-1252 du 7 octobre 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; qu'en vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée ;<br/>
<br/>
              2. Considérant que l'urgence justifie la suspension de l'exécution d'un acte administratif lorsque celle-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ; qu'il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte contesté sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue ; que l'urgence doit être appréciée objectivement et compte tenu de l'ensemble des circonstances de l'affaire ;<br/>
<br/>
              3. Considérant qu'aux termes de l'article 4 du décret du 7 octobre 2015 relatif aux tarifs des courses de taxi : " Le ministre chargé de l'économie arrête le tarif minimum, majorations et suppléments inclus, susceptible d'être perçu pour une course./ Il peut définir la période d'attente commandée par le client mentionnée à l'article 1er et déterminer les conditions d'application des majorations mentionnées à l'article 1er et des suppléments mentionnés à l'article 2. Il peut également fixer le montant de ces majorations et le prix de ces suppléments./ Par dérogation aux dispositions de l'article 1er, il peut instituer des tarifications forfaitaires pour la desserte de certains lieux ou sites faisant l'objet d'une fréquentation régulière ou élevée. Il détermine les conditions dans lesquelles la variation des forfaits peut s'écarter de celle du tarif de la course type mentionnée à l'article 3. " ; que M.A..., qui exerce la profession de chauffeur de taxi parisien " locataire ", demande la suspension de l'exécution de l'arrêté du 2 novembre 2015 relatif aux tarifs des courses de taxi, dans sa rédaction issue de sa modification par l'arrêté du 3 décembre 2015, pris sur le fondement de ces dispositions ;<br/>
<br/>
<br/>
              4. Considérant que, pour justifier l'urgence qui s'attacherait à la suspension de son exécution, le requérant invoque uniquement le coût de la modification de la table tarifaire du taximètre dont tout véhicule de taxi est équipé, rendue nécessaire afin de prendre en compte la modification des tarifs opérés par cet arrêté, et soutient qu'elle " entrainerait des frais redondants en période économique difficile exacerbée par les récents attentats de Paris " ; qu'en l'absence de toute précision, notamment sur le coût exact de cette opération de modification au regard du chiffre d'affaires de son activité et sur le délai requis pour y procéder, alors que cette obligation de modification avant le 1er mars 2016 s'impose pour tout taxi, la condition d'urgence requise par l'article L. 521-1 du code de justice administrative pour suspendre l'exécution de l'arrêté du 2 novembre 2015 n'est pas caractérisée ; que par suite, sans qu'il soit besoin de se prononcer sur l'existence d'un doute sérieux quant à la légalité des dispositions contestées, la requête de M. A...doit être rejetée ;<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. A...est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. B...A....<br/>
Copie en sera adressée au ministre de l'économie, de l'industrie et du numérique.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
