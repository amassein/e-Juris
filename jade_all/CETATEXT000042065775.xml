<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042065775</ID>
<ANCIEN_ID>JG_L_2020_06_000000428529</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/06/57/CETATEXT000042065775.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 29/06/2020, 428529, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-06-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428529</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Bertrand Mathieu</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:428529.20200629</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé à la Cour nationale du droit d'asile d'annuler la décision du 17 novembre 2016 par laquelle le directeur général de l'Office français de protection des réfugiés et apatrides (OFPRA) a rejeté sa demande d'asile et refusé de lui reconnaître la qualité de réfugié ou, à défaut, de lui accorder le bénéfice de la protection subsidiaire.<br/>
<br/>
              Par une décision n° 16039180 du 3 janvier 2019, la Cour nationale du droit d'asile a fait droit à sa demande et lui a reconnu la qualité de réfugié.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 1er mars et 3 juin 2019 au secrétariat du contentieux du Conseil d'Etat, l'OFPRA demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ; <br/>
<br/>
              2°) de renvoyer l'affaire devant la Cour nationale du droit d'asile.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention de Genève du 28 juillet 1951 et le protocole signé à New York le 31 janvier 1967 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bertrand Mathieu, conseiller d'Etat en service extraordinaire,<br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de l'Office français de protection des réfugiés et apatrides, et à la SCP Rocheteau, Uzan-Sarano, avocat de M. A... ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
<br/>
              1.	Selon les stipulations du 2 du A de l'article 1er de la convention de Genève du 28 juillet 1951 relative au statut des réfugiés et du protocole signé à New-York le 31 janvier 1967, doit être considérée comme réfugié toute personne qui, " craignant avec raison d'être persécutée du fait de sa race, de sa religion, de sa nationalité, de son appartenance à un certain groupe social ou de ses opinions politiques, se trouve hors du pays dont elle a la nationalité et qui ne peut, ou, du fait de cette crainte, ne veut se réclamer de la protection de ce pays (...) ". Aux termes du F de cet article : " Les dispositions de cette convention ne seront pas applicables aux personnes dont on aura des raisons sérieuses de penser : / a) qu'elles ont commis un crime contre la paix, un crime de guerre ou un crime contre l'humanité, au sens des instruments internationaux élaborés pour prévoir des dispositions relatives à ces crimes ; / b) qu'elles ont commis un crime grave de droit commun en dehors du pays d'accueil avant d'y être admises comme réfugiés ; / c) qu'elles se sont rendues coupables d'agissements contraires aux buts et aux principes des Nations Unies "  En vertu de l'article L. 711-3 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Le statut de réfugié n'est pas accordé à une personne qui relève de l'une des clauses d'exclusion prévues aux sections D, E ou F de l'article 1er de la convention de Genève du 28 juillet 1951 [...] / La même section F s'applique également aux personnes qui sont les instigatrices ou les complices des crimes ou des agissements mentionnés à ladite section ou qui y sont personnellement impliquées ". <br/>
<br/>
              2.	Par une décision en date du 3 janvier 2019, la Cour nationale du droit d'asile, d'une part, a annulé la décision du directeur général de l'Office français de protection des réfugiés et apatrides (OFPRA) en date du 17 novembre 2016 rejetant la demande d'asile de M. B... A... et refusant de lui accorder le bénéfice de la protection subsidiaire, d'autre part, a reconnu la qualité de réfugié à l'intéressé à raison des risques de persécution auxquels il serait exposé en cas de retour en Irak, notamment de la part de l'organisation dite " Etat islamique ". L'Office se pourvoit en cassation contre cette décision.<br/>
<br/>
              3.	Il ressort des pièces du dossier soumis au juge du fond que M. B... A..., de nationalité irakienne, a travaillé de 1997 à 2002 dans une commission d'enquête des services de renseignements irakiens en tant qu'officier adjoint, puis de 2002 à 2014 dans la force d'intervention spéciale arabo-kurde de l'armée irakienne, avec le grade de lieutenant, au sein de laquelle il a participé à des perquisitions dans le cadre d'opérations anti-terroristes visant notamment les membres d'Al Qaida, enfin, entre 2014 et 2015, à Kirkouk, dans une commission d'enquête pour les forces armées du Kurdistan irakien dans laquelle il était chargé d'interroger des prisonniers avant, le cas échéant, saisine du procureur. Selon les déclarations faites spontanément par l'intéressé lors d'un entretien qui s'est déroulé à l'OFPRA, des actes de torture étaient pratiqués lors de ces interrogatoires et des sévices étaient infligés à des prisonniers.  <br/>
<br/>
              4.	Pour écarter l'application de la clause d'exclusion prévue au F de l'article 1er de la convention de Genève, la Cour nationale du droit d'asile s'est bornée à relever que les explications apportées par l'intéressé lors des deux audiences devant la cour avaient permis de clarifier ses responsabilités au sein des commissions d'enquête et de lever les doutes sur le traitement réservé aux prisonniers, et sur son degré d'implication lors des séances d'interrogatoire. En statuant ainsi, sans expliciter la teneur des déclarations faites à l'audience qu'elle retenait pour porter son appréciation ni préciser les éléments de clarification apportés, la Cour n'a pas suffisamment motivé sa décision et n'a pas mis le juge de cassation à même d'exercer son contrôle. Par suite, et sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, l'Office français de protection des réfugiés et apatrides est fondé à demander l'annulation de la décision qu'il attaque.<br/>
<br/>
              5.	Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'OFPRA qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La décision de la Cour nationale du droit d'asile du 3 janvier 2019 est annulée.<br/>
Article 2 : L'affaire est renvoyée devant la Cour nationale du droit d'asile.<br/>
<br/>
Article 3 : Les conclusions de M. A... présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée à l'Office français de protection des réfugiés et apatrides et à M. B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
