<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037254038</ID>
<ANCIEN_ID>JG_L_2018_07_000000417663</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/25/40/CETATEXT000037254038.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 26/07/2018, 417663, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-07-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417663</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>Mme Dominique Chelle </RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:417663.20180726</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif de Marseille d'annuler pour excès de pouvoir la décision du 28 mars 2017 par laquelle la commission d'attribution de logements de la SA d'HLM Famille et Provence a refusé de lui attribuer un logement social. Par une ordonnance n° 1704120 du 14 juin 2017, le président de la 5ème chambre du tribunal administratif a rejeté sa demande comme portée devant un ordre de juridiction incompétent pour en connaître. <br/>
<br/>
              Par un pourvoi, enregistré au secrétariat du contentieux du Conseil d'Etat le 26 janvier 2018, Mme B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat ou, à défaut, de la SA d'HLM Famille et Provence, la  somme de 3 000 euros au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de la construction et de l'habitation ;<br/>
<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;  <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Dominique Chelle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de MmeB....<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que Mme B... est bénéficiaire d'une décision de la commission de médiation des Bouches-du-Rhône du 3 septembre 2015 la reconnaissant prioritaire pour l'attribution d'un logement social et d'un jugement du tribunal administratif de Marseille du 10 août 2016 enjoignant au préfet de ce département de la reloger ; qu'une proposition de logement lui a été faite à l'initiative du préfet le 9 janvier 2017 ; que Mme B... a soumis son dossier à la SA d'HLM Famille et Provence, qui a refusé de lui attribuer ce logement par une décision de la commission d'attribution du 28 mars 2017 ; que Mme B... a demandé l'annulation de cette décision au tribunal administratif de Marseille ; qu'elle se pourvoit en cassation contre l'ordonnance du 14 juin 2017 par laquelle le président de la 5ème chambre de ce  tribunal a rejeté sa demande comme portée devant un ordre de juridiction incompétent pour en connaître ; <br/>
<br/>
              2. Considérant que la circonstance que, le 18 mai 2018, postérieurement à l'introduction du pourvoi, Mme B...a accepté une nouvelle proposition de logement, dont il n'est d'ailleurs pas établi qu'elle aurait reçu une suite favorable du bailleur, ne prive pas d'objet le pourvoi dirigé contre l'ordonnance attaquée ;<br/>
<br/>
              3. Considérant que, si le contrat qui lie un bailleur social à un locataire est un contrat de droit privé, la décision de refus d'attribuer un logement ne porte pas sur l'exécution d'un tel contrat ; qu'elle est prise dans le cadre de l'exécution d'un service public, dans les conditions et selon des procédures qu'imposent au bailleur social les articles L. 441-1 et suivants du code de la construction et de l'habitation et les dispositions réglementaires prises pour leur application ; qu'ainsi, quel que soit le statut, public ou privé, du bailleur social, elle constitue une décision administrative, dont il incombe à la seule juridiction administrative d'apprécier la légalité ; qu'ainsi, c'est au prix d'une erreur de droit que, par l'ordonnance attaquée, le tribunal administratif a décliné la compétence du juge administratif pour connaître de la demande de Mme B...tendant à l'annulation pour excès de pouvoir de la décision du 28 mars 2017 par laquelle la commission d'attribution des logements de la SA d'HLM Famille et Provence a rejeté sa candidature ; que, par  suite, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, Mme B... est fondée à en demander l'annulation ;    <br/>
<br/>
              4. Considérant que Mme B...a obtenu le bénéfice de l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Foussard, Froger, avocat de MmeB..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à cette société ; <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 14 juin 2017 du président de la 5e chambre du tribunal administratif de Marseille est annulée.<br/>
<br/>
		Article 2 : L'affaire est renvoyée au tribunal administratif de Marseille.<br/>
<br/>
Article 3 : L'Etat versera à la SCP Foussard, Froger, avocat de MmeB..., une somme de 3 000 euros au titre des articles L. 761-1 du code de justice administrative et 37 et de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
<br/>
Article 4 : La présente décision sera notifiée à Mme A...B...et au ministre de la cohésion des territoires.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
