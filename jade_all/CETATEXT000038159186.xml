<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038159186</ID>
<ANCIEN_ID>JG_L_2019_02_000000413165</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/15/91/CETATEXT000038159186.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 22/02/2019, 413165, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-02-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>413165</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR>M. Jean-Yves Ollier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:413165.20190222</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Toulouse, d'une part, d'annuler les arrêtés du directeur de l'office public de l'habitat de Toulouse des 28 novembre et 26 décembre 2011 prononçant son placement, à titre provisoire, en temps partiel thérapeutique du 15 avril au 31 août 2009 puis en arrêt maladie ordinaire du 1er septembre 2009 au 31 août 2010 et, enfin, en disponibilité d'office à compter du 1er septembre 2010 et la déclarant, à titre provisoire et dans l'attente de l'avis du comité médical de Haute-Garonne, débitrice et redevable de la somme de 27 783,55 euros et, d'autre part, d'annuler les arrêtés du 7 septembre 2012 par lesquels le directeur de l'office a prononcé son placement, à titre provisoire, en temps partiel thérapeutique du 15 avril au 31 août 2009 puis en arrêt maladie ordinaire du 1er septembre 2009 au 31 août 2010 et, enfin, en disponibilité d'office à compter du 1er septembre 2010 et l'a déclarée redevable de cette même somme de 27 783,55 euros. Par un jugement n°s 1200880, 1205043 du 24 septembre 2015, le tribunal administratif de Toulouse, après avoir prononcé un non-lieu à statuer sur les conclusions aux fins d'annulation, d'une part, des deux arrêtés du 28 novembre 2011 et du 26 décembre 2011 et, d'autre part, de l'article 3 de l'arrêté du 7 septembre 2012, a rejeté le surplus de ses demandes.<br/>
<br/>
              Par un arrêt n°s 15BX03756, 15BX03764 du 6 juin 2017, la cour administrative d'appel de Bordeaux a rejeté l'appel formé par Mme A...contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 7 août et 7 novembre 2017 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au Conseil d'Etat :<br/>
<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de Toulouse Métropole Habitat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le décret n° 85-1054 du 30 septembre 1985 ;<br/>
              - le décret n° 86-68 du 13 janvier 1986 ;<br/>
              - le décret n° 86-442 du 14 mars 1986 ;<br/>
              - le décret n° 87-602 du 30 juillet 1987 ;<br/>
              - l'arrêté du 4 août 2004 relatif aux commissions de réforme des agents de la fonction publique territoriale et de la fonction publique hospitalière ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Yves Ollier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Sevaux, Mathonnet, avocat de MmeA..., et à la SCP Foussard, Froger, avocat de Toulouse Métropole Habitat ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme A..., attachée principale de l'office public de l'habitat de Toulouse, aux droits duquel vient Toulouse Métropole Habitat, a été victime, le 17 mars 2008, d'une chute alors qu'elle se rendait sur son lieu de travail, à l'origine d'une fracture du radius droit et du styloïde ulnaire droit. L'office a saisi, le 23 septembre 2008, la commission de réforme des agents des collectivités locales de la Haute-Garonne aux fins d'apprécier l'imputation au service de l'accident de MmeA.... La commission a ordonné, le 6 mars 2009, une expertise aux fins d'apprécier ses lésions directement imputables à l'accident, l'éventuelle consolidation, la durée de son incapacité totale de travail et son taux d'incapacité permanente partielle. Par une lettre du 8 avril 2009, Mme A...a demandé à son employeur de reprendre son activité dans le cadre d'un mi-temps thérapeutique. L'office n'a pas fait droit à cette demande mais a saisi la commission de réforme afin qu'elle se prononce sur cette question. La commission de réforme a, le 10 septembre 2010, rendu son avis sur l'ensemble des questions dont elle était saisie et émis un avis favorable à une imputation au service de l'accident et à ce que l'intéressée soit rétroactivement placée  dans une position de temps partiel thérapeutique à 60 % à compter du 15 avril 2009. Le médecin-expert missionné a conclu, dans son rapport du 3 mai 2011, à une consolidation de l'état de santé de Mme A...au 31 août 2009 et à l'attribution un taux d'incapacité permanente partielle de 8 %. Mme A...avait entre temps sollicité, par lettre du 28 avril 2011, son départ à la retraite et a été radiée des cadres à compter du 1er décembre 2011. Le 5 septembre 2011, l'office a saisi le comité médical départemental afin qu'il se prononce sur la situation administrative de Mme A...à compter du 31 août 2009. Par un avis rendu le 12 janvier 2012, le comité médical départemental s'est prononcé en faveur d'un placement en congé de maladie ordinaire pour la période du 1er septembre 2009 au 31 août 2010 puis d'une mise en disponibilité d'office du 1er septembre 2010 au 29 novembre 2011. Par un arrêté du 7 septembre 2012, le directeur de l'office a placé Mme A... en temps partiel thérapeutique du 15 avril au 31 août 2009, puis en arrêt maladie ordinaire du 1er septembre 2009 au 31 août 2010 et, enfin, en disponibilité d'office du 1er septembre 2010 au 30 novembre 2012, date ultérieurement corrigée au 30 novembre 2011 par un arrêté du 30 novembre 2012. Par un second arrêté du même jour, il a déclaré l'intéressée redevable de la somme de 27 783,55 euros, pour tenir compte du fait qu'elle avait été payée à plein traitement durant toute la période.<br/>
<br/>
              2. Par un jugement du 24 septembre 2015, le tribunal administratif de Toulouse a rejeté la demande de Mme A...tendant à l'annulation de ces arrêtés. Par l'arrêt attaqué du 6 juin 2017, la cour administrative d'appel de Bordeaux a rejeté l'appel formé par Mme A... contre ce jugement.<br/>
<br/>
              3. Aux termes de l'article 57 de la loi du 26 janvier 1984 portant dispositions relatives à la fonction publique territoriale : " Le fonctionnaire en activité a droit :  (...) 2° A des congés de maladie dont la durée totale peut atteindre un an pendant une période de douze mois consécutifs en cas de maladie dûment constatée mettant l'intéressé dans l'impossibilité d'exercer ses fonctions (...) / Toutefois, si la maladie provient de l'une des causes exceptionnelles prévues à l'article L. 27 du code des pensions civiles et militaires de retraite, à l'exception des blessures ou des maladies contractées ou aggravées en service, le fonctionnaire conserve l'intégralité de son traitement jusqu'à ce qu'il soit en état de reprendre son service ou jusqu'à la mise à la retraite. Il a droit, en outre, au remboursement des honoraires médicaux et des frais directement entraînés par la maladie ou l'accident, même après la date de radiation des cadres pour mise à la retraite. / (...) 4° bis. Après un congé de maladie, un congé de longue maladie ou un congé de longue durée, les fonctionnaires peuvent être autorisés à accomplir un service à temps partiel pour raison thérapeutique, accordé pour une période de trois mois renouvelable dans la limite d'un an pour une même affection. / Après un congé pour accident de service ou maladie contractée dans l'exercice des fonctions, le travail à temps partiel thérapeutique peut être accordé pour une période d'une durée maximale de six mois renouvelable une fois ". L'article 72 de la même loi dispose que : " La disponibilité est prononcée, soit à la demande de l'intéressé, soit d'office à l'expiration des congés prévus aux 2° (...) de l'article 57 (...) ". Aux termes de l'article 37 du décret du 30 juillet 1987 (...) portant dispositions statutaires relatives à la fonction publique territoriale et relatif à l'organisation des comités médicaux (...) et au régime des congés de maladie des fonctionnaires territoriaux alors applicable : " Le fonctionnaire ne pouvant, à l'expiration de la dernière période de congé de longue maladie ou de longue durée attribuable, reprendre son service est soit reclassé (...), soit mis en disponibilité, soit admis à la retraite après avis de la commission de réforme (...) ". L'article 38 du décret du 30 septembre 1985 relatif au reclassement des fonctionnaires territoriaux reconnus inaptes à l'exercice de leurs fonctions précise que : " La mise en disponibilité visée aux articles 17 (...) du présent décret est prononcée après avis du comité médical (...) sur l'inaptitude du fonctionnaire à reprendre ses fonctions (...) ". <br/>
<br/>
              4. Les décisions administratives ne peuvent légalement disposer que pour l'avenir. S'agissant des décisions relatives à la carrière des fonctionnaires ou des militaires, l'administration ne peut déroger à cette règle générale en leur conférant une portée rétroactive que dans la mesure nécessaire pour assurer la continuité de la carrière de l'agent intéressé ou procéder à la régularisation de sa situation.<br/>
<br/>
              5. En premier lieu, il ressort des pièces du dossier soumis au juge du fond que Mme A...n'a pas repris ses fonctions à la suite de l'accident dont elle a été victime le 17 mars 2008 avant de solliciter, le 28 avril 2011, le bénéfice d'un départ à la retraite. La durée d'inactivité de l'intéressée a ainsi dépassé la durée totale d'un an prévue par les dispositions précitées du 2° de l'article 57 de la loi du 26 janvier 1984 portant dispositions relatives à la fonction publique territoriale. Dans ces conditions, la cour administrative d'appel de Bordeaux n'a pas entaché son arrêt d'insuffisance de motivation ou d'erreur de qualification juridique en relevant, pour rejeter le moyen tiré de l'illégalité de la rétroactivité des décisions attaquées, que celles-ci avaient pour objet de placer l'intéressée dans une position régulière au terme de ses congés de maladie ordinaire et d'assurer la continuité de sa carrière jusqu'à la date de son départ à la retraite.<br/>
<br/>
              6. En deuxième lieu, il ressort des pièces du dossier soumis au juge du fond que Mme A...n'était plus en mesure de reprendre ses fonctions ou d'être affectée dans un autre emploi à compter du 1er septembre 2010, date à laquelle elle devait être regardée comme ayant épuisé ses droits à congés de maladie ordinaire. Il résulte des dispositions précitées de l'article 72 de la loi du 26 janvier 1984 portant dispositions relatives à la fonction publique territoriale que les négligences et retards reprochés à l'office public de l'habitat de Toulouse dans l'examen de la situation de Mme A...sont sans incidence sur la légalité des décisions par lesquelles le directeur de l'office a placé rétroactivement Mme A...en position de disponibilité d'office à compter du 1er septembre 2010 afin de régulariser sa situation. Il s'en suit que Mme A...n'est pas fondée à soutenir que la cour administrative d'appel de Bordeaux aurait commis une erreur de droit et insuffisamment motivé son arrêt en estimant qu'elle pouvait être placée en disponibilité d'office à compter du 1er septembre 2010 sans tirer les conséquences des négligences et retards imputables à l'office.<br/>
<br/>
              7. En troisième lieu, il résulte des dispositions du 4° bis de l'article 57 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale, citées au point 3 de la présente décision, qu'un fonctionnaire ne dispose d'aucun droit à accomplir un service à temps partiel pour raison thérapeutique pour la durée maximale de six mois renouvelable une fois après un congé pour accident de service. Il s'en suit que Mme A... n'est pas fondée à soutenir que la cour administrative d'appel de Bordeaux aurait commis une erreur de droit ou dénaturé les pièces du dossier en écartant le moyen tiré de ce que le choix de la date du 31 août 2009 pour fixer le terme de la période de mi-temps thérapeutique était erroné.<br/>
<br/>
              8. En dernier lieu, il ressort des énonciations de l'arrêt attaqué que la cour administrative d'appel de Bordeaux a relevé, pour écarter l'application des dispositions du deuxième alinéa du 2° de l'article 57 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale citées au point 3 de la présente décision, que Mme A...  ne contestait pas sérieusement avoir été en état de reprendre son service dès le 15 avril 2009. Par suite, Mme A...n'est pas fondée à soutenir que la cour aurait commis une erreur de droit en jugeant qu'elle ne pouvait bénéficier, à l'instar des fonctionnaires victimes d'un accident de service inaptes à reprendre leur service, du maintien de l'intégralité de son traitement jusqu'à la date de sa mise à la retraite, sur le fondement des dispositions du 2° de l'article 57 de la loi du 26 janvier 1984.<br/>
<br/>
              9. Il résulte de tout ce qui précède que Mme A...n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              10. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de Toulouse Métropole Habitat qui n'est pas, dans la présente instance, la partie perdante. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées au même titre par Toulouse Métropole Habitat.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de Mme A...est rejeté. <br/>
Article 2 : Les conclusions présentées par Toulouse Métropole Habitat sur le fondement de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à Mme B...A...et à Toulouse Métropole Habitat.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
