<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031938209</ID>
<ANCIEN_ID>JG_L_2016_01_000000372112</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/93/82/CETATEXT000031938209.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème SSR, 21/01/2016, 372112, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-01-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>372112</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>Mme Karin Ciavaldini</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:372112.20160121</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Le préfet de la Haute-Garonne a demandé au tribunal administratif de Toulouse d'annuler pour excès de pouvoir la délibération du 7 juin 2010 par laquelle la communauté d'agglomération du sud-est toulousain a approuvé les modalités de tarification de la participation pour raccordement à l'égout applicable sur son territoire et la décision implicite par laquelle la communauté d'agglomération a refusé de retirer cette délibération. Par un jugement n° 1005267 du 5 octobre 2012, le tribunal administratif a rejeté la demande. <br/>
<br/>
              Par un arrêt n° 12BX02986 du 9 juillet 2013, la cour administrative d'appel de Bordeaux a rejeté l'appel formé contre ce jugement par le préfet de la Haute-Garonne. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 11 septembre et 11 décembre 2013 au secrétariat du contentieux du Conseil d'Etat, la ministre de l'égalité des territoires et du logement demande au Conseil d'Etat d'annuler cet arrêt.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Karin Ciavaldini, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Odent, Poulet, avocat de la communauté d'agglomération du sud-est toulousain ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que, par une délibération du 7 juin 2010, la communauté d'agglomération du sud-est toulousain a approuvé les modalités de tarification de la participation pour raccordement à l'égout applicables sur le territoire de cet établissement public ; que la délibération prévoyait notamment que " la participation au raccordement à l'égout sera exigée sur les constructions édifiées dans les zones d'aménagement concerté sauf dispositions particulières relatives au financement par l'aménageur des équipements publics de collecte ou de traitement des eaux usées extérieurs à l'unité foncière de la zone " ; que la ministre de l'égalité des territoires et du logement se pourvoit en cassation contre l'arrêt du 9 juillet 2013 par lequel la cour administrative d'appel de Bordeaux a rejeté l'appel du préfet de la Haute-Garonne contre le jugement du tribunal administratif de Toulouse rejetant sa demande tendant à l'annulation de cette délibération ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 311-4 du code de l'urbanisme, applicable aux zones d'aménagement concerté : " Il ne peut être mis à la charge de l'aménageur de la zone que le coût des équipements publics à réaliser pour répondre aux besoins des futurs habitants ou usagers des constructions à édifier dans la zone (...) " ; qu'aux termes de l'article L. 332-6 du même code, dans sa rédaction applicable au litige : " Les bénéficiaires d'autorisations de construire ne peuvent être tenus que des obligations suivantes : / (...) 2° Le versement des contributions aux dépenses d'équipements publics mentionnées à l'article L. 332-6-1 (...) / 3° La réalisation des équipements propres mentionnés à l'article L. 332-15 (...) " ; qu'en vertu de l'article L. 332-6-1 du même code, dans cette même rédaction, les contributions aux dépenses d'équipements publics prévus au 2° de l'article L. 332-6 comprennent notamment la participation pour raccordement à l'égout prévue à l'article L. 1331-7 du code de la santé publique ; qu'aux termes de ce dernier article, dans sa rédaction applicable au litige : " Les propriétaires des immeubles édifiés postérieurement à la mise en service du réseau public de collecte auquel ces immeubles doivent être raccordés peuvent être astreints par la commune, pour tenir compte de l'économie par eux réalisée en évitant une installation d'évacuation ou d'épuration individuelle réglementaire, à verser une participation s'élevant au maximum à 80 % du coût de fourniture et de pose d'une telle installation (...) " ;<br/>
<br/>
              3. Considérant qu'eu égard à son objet et aux termes de l'article L. 1331-7 du code de la santé publique, la participation prévue par cet article ne saurait, sans double emploi, être imposée au propriétaire ou au constructeur de l'immeuble lorsque celui-ci a déjà contribué, en vertu d'obligations mises à sa charge par l'autorité publique, au financement d'installations collectives d'évacuation ou d'épuration pour un montant égal ou supérieur au maximum légal prévu par l'article L. 1331-7 ; qu'en revanche, la participation reste due lorsque le propriétaire ou le constructeur de l'immeuble a seulement contribué à l'exécution, même sous la voie publique, d'ouvrages qui, étant destinés à la conduite des eaux usées de l'immeuble vers l'égout public existant, lui évitent d'avoir à procéder à une installation individuelle ; que la circonstance que des équipements réalisés au sein d'une zone d'aménagement concerté puissent être qualifiés d'équipements publics, au sens de l'article L. 311-4 du code de l'urbanisme ne conduit pas, par elle-même, à les regarder comme des installations collectives d'évacuation ou d'épuration, pour l'application de la règle rappelée ci-dessus ; <br/>
<br/>
              4. Considérant que la cour a relevé que la délibération contestée excluait, dans les zones d'aménagement concerté, l'assujettissement des constructeurs à la participation pour le raccordement à l'égout lorsque l'aménageur avait dû prendre à sa charge, outre la construction du réseau de collecte et d'évacuation des eaux usées au sein de la zone, des travaux d'assainissement extérieurs au périmètre de celle-ci lorsqu'ils étaient nécessaires ; qu'il résulte de ce qui est dit au point 3 que la construction du réseau secondaire de collecte et d'évacuation des eaux usées au sein de la zone est sans influence sur l'exigibilité de la participation pour le raccordement à l'égout, dès lors que ce réseau est raccordé à des installations collectives d'évacuation et d'épuration au financement desquelles l'aménageur n'a pas contribué ; que, par suite, en jugeant que la délibération contestée avait instauré un régime de participation pour le raccordement à l'égout qui ne faisait pas double emploi avec les charges résultant, pour le propriétaire ou le constructeur, de l'aménagement de la zone, sans qu'importe la qualification d'ouvrage public que le réseau d'assainissement réalisé au sein de la zone était susceptible de recevoir, la cour n'a pas commis d'erreur de droit ; que le pourvoi de la ministre de l'égalité des territoires et du logement doit être rejeté ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à la communauté d'agglomération du sud-est toulousain de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>               D E C I D E :<br/>
                              --------------<br/>
<br/>
Article 1er : Le pourvoi de la ministre de l'égalité des territoires et du logement est rejeté.<br/>
<br/>
Article 2 : L'Etat versera à la communauté d'agglomération du sud-est toulousain la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la ministre du logement, de l'égalité des territoires et de la ruralité et à la communauté d'agglomération du sud-est toulousain.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
