<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000021468333</ID>
<ANCIEN_ID>JG_L_2009_12_000000305031</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/21/46/83/CETATEXT000021468333.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 09/12/2009, 305031, Publié au recueil Lebon</TITRE>
<DATE_DEC>2009-12-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>305031</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Martin</PRESIDENT>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Fabienne  Lambolez</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Boucher Julien</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête et le mémoire complémentaire, enregistrés les 24 avril et 30 novembre 2007 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. Paul A, demeurant ... ; M. A demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite, confirmée par la décision expresse du 8 novembre 2007, par laquelle la commission de recours contre les décisions de refus de visa d'entrée en France a rejeté son recours dirigé contre la décision du 23 août 2006 du consul général de France à Cotonou (Bénin) refusant un visa d'entrée et de long séjour en France à Mlle Bellissima B ;<br/>
<br/>
              2°) d'enjoindre au ministre des affaires étrangères et européennes de réexaminer la demande de visa de l'intéressée dans un délai d'un mois à compter de la notification de la décision à intervenir, sous astreinte de 200 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 1 000 euros en application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention relative aux droits de l'enfant du 26 janvier 1990 ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Fabienne Lambolez, Maître des Requêtes,<br/>
<br/>
              - les observations de la SCP Piwnica, Molinié, avocat de M. A,<br/>
<br/>
              - les conclusions de M. Julien Boucher, Rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Piwnica, Molinié, avocat de M. A.<br/>
<br/>
<br/>
<br/>
<br/>Sur les conclusions à fin d'annulation :<br/>
<br/>
              Sans qu'il soit besoin d'examiner les autres moyens de la requête ;<br/>
<br/>
              Considérant que M. C, de nationalité française, conteste le rejet du recours qu'il a formé devant la commission de recours contre les décisions de refus de visa d'entrée en France à l'encontre de la décision du 23 août 2006 des autorités consulaires françaises au Bénin refusant la délivrance d'un visa d'entrée et de long séjour en France à Mlle Bellissima B, de nationalité béninoise, née en 2000, dont la tutelle et la prise en charge lui avaient été confiées par une ordonnance du tribunal de première instance de Cotonou du 1er juillet 2005, rendue exécutoire par une ordonnance du tribunal de grande instance de Bordeaux du 10 août 2005 ; qu'il ressort des pièces du dossier que pour rejeter ce recours, la commission s'est fondée, sans que le préfet n'ait eu à se prononcer sur une demande de regroupement familial, d'une part, sur le motif tiré de l'intérêt supérieur de l'enfant et, d'autre part, sur le risque de détournement de l'objet du visa de séjour à des fins migratoires ;<br/>
<br/>
              Considérant que l'intérêt d'un enfant est en principe de vivre auprès de la personne qui, en vertu d'une décision de justice qui produit des effets juridiques en France, est titulaire à son égard de l'autorité parentale ; qu'ainsi, dans le cas où un visa d'entrée et de long séjour en France est sollicité en vue de permettre à un enfant de rejoindre un ressortissant français ou étranger qui a reçu délégation de l'autorité parentale dans les conditions qui viennent d'être indiquées, ce visa ne peut en règle générale, eu égard notamment aux stipulations du paragraphe 1 de l'article 3 de la convention du 26 janvier 1990 relative aux droits de l'enfant, être refusé pour un motif tiré de ce que l'intérêt de l'enfant serait au contraire de demeurer auprès de ses parents ou d'autres membres de sa famille ; qu'en revanche, et sous réserve de ne pas porter une atteinte disproportionnée au droit de l'intéressé au respect de sa vie privée et familiale, l'autorité chargée de la délivrance des visas peut se fonder, pour rejeter la demande dont elle est saisie, non seulement sur l'atteinte à l'ordre public qui pourrait résulter de l'accès de l'enfant au territoire national, mais aussi sur le motif tiré de ce que les conditions d'accueil de celui-ci en France seraient, compte tenu notamment des ressources et des conditions de logement du titulaire de l'autorité parentale, contraires à son intérêt ;<br/>
<br/>
              Considérant, en premier lieu, qu'il ressort des pièces du dossier que si l'enfant Bellissima B a toujours vécu au Bénin auprès de ses parents, M. C, qui justifie de ressources et de conditions d'accueil suffisantes, dispose d'une délégation d'autorité parentale, qui a d'ailleurs été rendue exécutoire en droit français, pour prendre toutes mesures de tutelle et de prise en charge de cette enfant ; qu'ainsi, en l'absence de circonstances particulières, en estimant que l'intérêt de l'enfant était de demeurer dans son pays d'origine auprès de ses parents, la commission de recours contre les décisions de refus de visa d'entrée en France a entaché sa décision d'erreur d'appréciation ;<br/>
<br/>
              Considérant, en deuxième lieu, que lorsque, comme en l'espèce, la délivrance d'un visa de séjour répond à l'intérêt supérieur de l'enfant, la circonstance que la délégation d'autorité parentale aurait pour motivation de permettre à la jeune Bellissima B de s'installer durablement en France ne saurait caractériser un détournement de l'objet de ce visa, qui répond au contraire à un projet de cette nature ; <br/>
<br/>
              Considérant qu'il résulte de ce qui précède que M. A est fondé à demander l'annulation de la décision attaquée ;<br/>
<br/>
              Sur les conclusions à fin d'injonction :<br/>
<br/>
              Considérant qu'aux termes de l'article L. 911-1 du code de justice administrative :  Lorsque sa décision implique nécessairement qu'une personne morale de droit public (...) prenne une mesure d'exécution dans un sens déterminé, la juridiction, saisie de conclusions en ce sens, prescrit, par la même décision, cette mesure assortie, le cas échéant, d'un délai d'exécution  ; que, eu égard aux motifs de la présente décision, il y a lieu de faire droit aux conclusions de M. C et de prescrire au ministre de l'immigration, de l'intégration, de l'identité nationale et du développement solidaire de réexaminer, dans le délai d'un mois à compter de la notification de la présente décision, la demande de visa d'entrée et de long séjour présenté par Mlle Bellissima B ; qu'il n'y a pas lieu d'assortir cette injonction d'une astreinte ;<br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de faire application de ces dispositions et de mettre à la charge de l'Etat le versement à M. A de la somme de 1 000 euros au titre des frais exposés par lui et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>				D E C I D E  :<br/>
              				--------------<br/>
<br/>
Article 1er : La décision implicite, confirmée par la décision du 8 novembre 2007, de la commission de recours contre les décisions de refus de visa d'entrée en France, refusant un visa à Mlle Bellissima B, est annulée.<br/>
<br/>
Article 2 : Il est enjoint au ministre de l'immigration, de l'intégration, de l'identité nationale et du développement solidaire de réexaminer, dans le délai d'un mois à compter de la notification de la présente décision, la demande de visa d'entrée et de long séjour de Mlle Bellissima B.<br/>
<br/>
Article 3 : L'Etat versera à M. A la somme de 1 000 euros au titre l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Le surplus des conclusions de la requête de M. C est rejeté.<br/>
<br/>
Article 5 : La présente décision sera notifiée à M. Paul A et au ministre de l'immigration, de l'intégration, de l'identité nationale et du développement solidaire.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">335-005-01 ÉTRANGERS. ENTRÉE EN FRANCE. VISAS. - VISA D'ENTRÉE ET DE LONG SÉJOUR SOLLICITÉ POUR UN ENFANT - 1) MOTIF DE REFUS - APPRÉCIATION DE L'INTÉRÊT DE L'ENFANT (ART. 3§1 DE LA CONVENTION INTERNATIONALE RELATIVE AUX DROITS DE L'ENFANT DU 26 JANVIER 1990) - CRITÈRES - 2) CONTRÔLE DU JUGE - NATURE - CONTRÔLE NORMAL - 3) CIRCONSTANCE QU'UNE DÉLÉGATION D'AUTORITÉ PARENTALE AURAIT POUR MOTIVATION DE PERMETTRE À L'ENFANT ÉTRANGER DE S'INSTALLER DURABLEMENT EN FRANCE - DÉTOURNEMENT DE L'OBJET DU VISA - ABSENCE EN PRINCIPE LORSQUE LA DÉLIVRANCE RÉPOND À L'INTÉRÊT SUPÉRIEUR DE L'ENFANT.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-02-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR. APPRÉCIATIONS SOUMISES À UN CONTRÔLE NORMAL. - VISA D'ENTRÉE ET DE LONG SÉJOUR SOLLICITÉ POUR UN ENFANT - APPRÉCIATION DE L'INTÉRÊT DE L'ENFANT (ART. 3§1 DE LA CONVENTION INTERNATIONALE RELATIVE AUX DROITS DE L'ENFANT DU 26 JANVIER 1990) PAR L'ADMINISTRATION.
</SCT>
<ANA ID="9A"> 335-005-01 1) L'intérêt d'un enfant est en principe de vivre auprès de la personne qui, en vertu d'une décision de justice qui produit des effets juridiques en France, est titulaire à son égard de l'autorité parentale. Ainsi, dans le cas où un visa d'entrée et de long séjour en France est sollicité en vue de permettre à un enfant de rejoindre un ressortissant français ou étranger qui a reçu délégation de l'autorité parentale dans les conditions qui viennent d'être indiquées, ce visa ne peut en règle générale, eu égard notamment aux stipulations du paragraphe 1 de l'article 3 de la convention internationale relative aux droits de l'enfant du 26 janvier 1990, être refusé pour un motif tiré de ce que l'intérêt de l'enfant serait au contraire de demeurer auprès de ses parents ou d'autres membres de sa famille. En revanche, l'autorité chargée de la délivrance des visas peut se fonder, pour rejeter la demande dont elle est saisie, non seulement sur l'atteinte à l'ordre public qui pourrait résulter de l'accès de l'enfant au territoire national, mais aussi sur le motif tiré de ce que les conditions d'accueil de celui-ci en France seraient, compte tenu notamment des ressources et des conditions de logement du titulaire de l'autorité parentale, contraires à son intérêt.... ...2) Le juge de l'excès de pouvoir exerce un contrôle normal sur l'appréciation par l'administration de l'intérêt de l'enfant.... ...3) Lorsque la délivrance d'un visa de séjour répond à l'intérêt supérieur de l'enfant, la circonstance que la délégation d'autorité parentale aurait pour motivation de permettre à l'enfant étranger de s'installer durablement en France ne saurait caractériser un détournement de l'objet de ce visa, qui répond au contraire à un projet de cette nature.</ANA>
<ANA ID="9B"> 54-07-02-03 Refus de visa sollicité pour un enfant. Le juge de l'excès de pouvoir exerce un contrôle normal sur l'appréciation par l'administration de l'intérêt de l'enfant au sens du paragraphe 1 de l'article 3 de la convention internationale relative aux droits de l'enfant du 26 janvier 1990.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
