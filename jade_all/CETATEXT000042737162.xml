<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042737162</ID>
<ANCIEN_ID>JG_L_2020_12_000000431858</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/73/71/CETATEXT000042737162.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 23/12/2020, 431858, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>431858</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Arnaud Skzryerbak</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:431858.20201223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés les 21 juin et 20 septembre 2019 au secrétariat du contentieux du Conseil d'Etat, la Fédération des établissements hospitaliers et d'aide à la personne, privés à but non lucratif, demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté de la ministre des solidarités et de la santé et du ministre de l'action et des comptes publics du 18 avril 2019 fixant pour l'année 2019 les éléments tarifaires mentionnés aux 1° à 3° du I de l'article L. 162-22-3 du code de la sécurité sociale des établissements de santé mentionnés au d de l'article L. 162-22-6 du code de la sécurité sociale ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts ;<br/>
              - le code de la santé publique ;<br/>
              - le code de la sécurité sociale ;<br/>
              - la loi n° 2015-1702 du 21 décembre 2015 ;<br/>
              - la loi n° 2016-1917 du 29 décembre 2016 ; <br/>
              - la loi n° 2018-1203 du 22 décembre 2018 ;<br/>
              - le décret n° 2000-685 du 21 juillet 2000 ;<br/>
              - le décret n° 2005-850 du 27 juillet 2005 ;<br/>
              - le décret n° 2017-1082 du 24 mai 2017 ;<br/>
              - le décret n° 2017-1076 du 24 mai 2017 ;<br/>
              - l'arrêté du 6 mars 2019 fixant pour l'année 2019 l'objectif quantifié national mentionné à l'article L. 162-22-2 du code de la sécurité sociale ; <br/>
              - l'arrêté du 16 avril 2019 fixant pour l'année 2019 les éléments tarifaires mentionnés aux 1° à 3° du I de l'article L. 162-23-4 du code de la sécurité sociale et au 2° du E du III de l'article 78 modifié de la loi n° 2015-1702 du 21 décembre 2015 de financement de la sécurité sociale pour 2016 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arnaud Skzryerbak, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de la Fédération des établissements hospitaliers, et d'aide à la personne, privés non lucratif ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il résulte des dispositions du 2° de l'article L. 162-22 du code de la sécurité sociale, dans sa rédaction antérieure à la loi du 21 décembre 2015 de financement de la sécurité sociale pour 2016, et des articles R. 162-29-1 et R. 162-29-2 du même code, alors applicables, que les activités de soins de suite et de réadaptation étaient, avant l'intervention de cette loi, financées selon des modalités différentes selon la catégorie dont relevait l'établissement. S'agissant des établissements de santé privés mentionnés aux d et e de l'article L. 162-22-6 de ce code, les prestations d'hospitalisation prises en charge par les régimes obligatoires de sécurité sociale l'étaient sur la base de tarifs journaliers, fixés pour chaque établissement par le directeur général de l'agence régionale de santé, en vertu des articles L. 162-22-1 et R. 162-29-1 du même code. La loi du 21 décembre 2015 de financement de la sécurité sociale pour 2016 a substitué à ces modalités un financement reposant principalement, en application du 1° de l'article L. 162 23-2 du code de la sécurité sociale et dans les conditions prévues par l'article L. 162-23-3 du même code, d'une part, sur une dotation calculée chaque année sur la base de l'activité antérieure et, d'autre part, pour chaque séjour, sur une fraction des tarifs nationaux des prestations d'hospitalisation mentionnés par l'article L. 162-23-4 du même code. A titre transitoire, le 2° du E du III de l'article 78 de la loi du 21 décembre 2015, dans sa rédaction en vigueur à la date de l'arrêté attaqué, a prévu que, du 1er mars 2017 au 31 décembre 2019, les prestations d'hospitalisation relevant des activités de soins de suite et de réadaptation sont financées par le cumul de deux montants correspondant, pour chaque établissement, à une fraction des recettes issues de l'application des modalités de financement antérieures à la loi et à une fraction des recettes issues de l'application des nouvelles modalités de financement prévues au 1° de l'article L. 162-23-2 du code de la sécurité sociale. Un arrêté du 16 avril 2019 a fixé à 90 %, pour 2019, la fraction correspondant aux recettes issues de l'application des modalités de financement antérieures à la loi du 21 décembre 2015.<br/>
<br/>
              2. Aux termes du I de l'article L. 162-22-3 du code de la sécurité sociale, applicable aux activités de psychiatrie et demeuré applicable aux activités de soins de suite et de réadaptation dans les conditions mentionnées ci-dessus pour les établissements de santé mentionnés aux d et e de l'article L. 162-22-6  du même code : " Chaque année, l'Etat détermine : / 1° L'évolution moyenne nationale et l'évolution moyenne dans chaque région des tarifs des prestations mentionnées au 1° de l'article L. 162-22-1, selon les modalités prévues à l'article L. 162-22-2 et au I de l'article L. 162-22-2-1. Ces évolutions peuvent être différentes pour certaines activités médicales ; / 2° Les variations maximales et minimales des taux d'évolution des tarifs des prestations qui peuvent être allouées aux établissements par les agences régionales ; / 3° Les tarifs des prestations d'hospitalisation nouvellement créées dans les conditions prévues au 1° de l'article L. 162-22-1 ".  <br/>
<br/>
              3. Sur le fondement de ces dispositions, par un arrêté du 18 avril 2019, la ministre des solidarités et de la santé et le ministre de l'action et des comptes publics ont fixé, pour les établissements mentionnés au d de l'article L. 162-22-6 du code de la sécurité sociale, à - 0,62 % et - 0,03 %, dont - 0,7 % au titre de la réserve prudentielle, l'évolution moyenne nationale des tarifs des prestations relatifs, respectivement, aux activités de soins de suite et de réadaptation mentionnées à l'article L. 162-23-1 du code de la sécurité sociale et aux activités de psychiatrie mentionnées à l'article L. 162-22-1 du même code. Par ce même arrêté, les ministres ont également fixé, pour ces établissements, l'évolution moyenne des tarifs des prestations de soins de suite et de réadaptation et de psychiatrie de chaque région. Il ressort des pièces du dossier que ces taux d'évolution ont été déterminés en tenant compte, à hauteur de 50 %, après en avoir tenu compte à hauteur de 30 %  l'année précédente, de l'incidence positive des allègements de charges que représente, pour les établissements privés à but non lucratif, le crédit d'impôt sur la taxe sur les salaires prévu par l'article 231 A du code général des impôts issu de l'article 88 de la loi du 29 décembre 2016 de finances pour 2017, applicable à raison des rémunérations versées jusqu'au 31 décembre 2018, et qu'ils sont de ce fait inférieurs, de 0,03 % pour les activités de soins de suite et de réadaptation et de 0,01 % pour les activités de psychiatrie, par rapport à ce qu'ils auraient été sans cette prise en compte.<br/>
<br/>
              Sur la légalité externe de l'arrêté attaqué : <br/>
<br/>
              4. En premier lieu, le délai de quinze jours suivant la promulgation de la loi de financement de la sécurité sociale, prévu par l'article R. 162-31-4 du code de la sécurité sociale pour arrêter le montant de l'objectif quantifié national relatif aux activités de psychiatrie exercées par les établissements de santé privés mentionnés aux d et e de l'article L. 162-22-6, ainsi que le délai de quinze jours suivant la publication de l'arrêté fixant ce montant, prévu par l'article R. 162-31-5 du même code pour arrêter les éléments tarifaires mentionnés au I de l'article L. 162-22-3 de ce code, ne sont pas prescrits à peine de nullité. Par suite, la circonstance que l'arrêté attaqué a été pris plus de quinze jours après la publication de l'arrêté du 6 mars 2019 fixant pour l'année 2019 l'objectif quantifié national mentionné à l'article L. 162-22-2 du code de la sécurité sociale, lui-même pris plus de 15 jours après la promulgation de la loi du 22 décembre 2018 de financement de la sécurité sociale pour 2019, n'affecte pas sa légalité.<br/>
<br/>
              5. En deuxième lieu, en vertu du II de l'article L. 162-23 du code de la sécurité sociale, dans sa rédaction issue de la loi du 21 décembre 2015 de financement de la sécurité sociale pour 2016, les tarifs nationaux applicables aux activités de soins de suite et de réadaptation, conformément aux nouvelles modalités de financement prévues au 1° de l'article L. 162-23-2 du code de la sécurité sociale, " peuvent être déterminés, en tout ou partie, à partir des données afférentes au coût relatif des prestations, issues notamment de l'étude nationale de coûts définie à l'article L. 6113-11 du code de la santé publique ". Aux termes de cet article : " Afin de disposer de données sur les coûts de prise en charge au sein des établissements de santé, des études nationales de coûts sont réalisées chaque année auprès d'établissements de santé relevant des catégories mentionnées aux a à d de l'article L. 162-22-6 du code de la sécurité sociale (...) ". Toutefois, ces dispositions ne subordonnent pas l'adoption de l'arrêté fixant les éléments tarifaires mentionnés aux 1° à 3° du I de l'article L. 162-22-3 du code de la sécurité sociale à la réalisation préalable de l'étude nationale de coûts qu'elles mentionnent. Le moyen tiré, pour ce motif, de leur méconnaissance ne peut, par suite, qu'être écarté.<br/>
<br/>
              6. En troisième lieu, l'article R. 161-31-5 du code de la sécurité sociale dispose que les éléments tarifaires mentionnés aux 1° à 3° du I de l'article L. 162-22-3 sont arrêtés par les ministres chargés de la santé et de la sécurité sociale. Aux termes de l'article 1er du décret du 27 juillet 2005 relatif aux délégations de signature des membres du Gouvernement : " A compter du jour suivant la publication au Journal officiel de la République française de l'acte les nommant dans leurs fonctions ou à compter du jour où cet acte prend effet, si ce jour est postérieur, peuvent signer, au nom du ministre ou du secrétaire d'Etat et par délégation, l'ensemble des actes, à l'exception des décrets, relatifs aux affaires des services placés sous leur autorité : / 1° (...) les directeurs d'administration centrale (...) ". D'une part, il résulte des articles 1er et 2 du décret du 24 mai 2017 relatif aux attributions du ministre des solidarités et de la santé que celui-ci prépare et met en oeuvre la politique du gouvernement dans les domaines de la santé publique et de l'organisation du système de santé et les règles relatives aux régimes et à la gestion des organismes de sécurité sociale en matière d'assurance maladie et, à cette fin, a autorité sur la direction générale de l'offre de soins ainsi que, conjointement avec le ministre de l'action et des comptes publics pour l'exercice par ce dernier de ses fonctions dans le domaine des finances sociales, sur la direction de la sécurité sociale. D'autre part, il résulte des articles 2 et 5 du décret du 24 mai 2017 relatif aux attributions du ministre de l'action et des comptes publics que ce ministre est chargé, conjointement avec le ministre des solidarités et de la santé, de la préparation de la loi de financement de la sécurité sociale et du suivi de son exécution et est responsable de l'équilibre général des comptes sociaux et que, pour l'exercice de ses attributions relatives aux finances sociales, il a autorité conjointe avec le ministre des solidarités et de la santé sur la direction de la sécurité sociale. Il suit de là que Mme A... C..., nommée directrice générale de l'offre de soins, et Mme B... D..., nommée directrice de la sécurité sociale, par des décrets du 14 juin 2017 publiés au Journal officiel de la République française du 15 juin suivant, avaient compétence, eu égard aux attributions de ces directions, fixées respectivement par l'article D. 1421-2 du code de la santé publique et par le décret du 21 juillet 2000 relatif à l'organisation de l'administration centrale du ministère de l'emploi et de la solidarité et aux attributions de certains de ses services, pour signer l'arrêté attaqué, la première au nom du ministre des solidarités et de la santé, en sa qualité de ministre chargé de la santé, et la seconde au nom de ce ministre et du ministre de l'action et des comptes publics, ministres chargés de la sécurité sociale au sens de l'article R. 161-31-5 du code de la sécurité sociale. <br/>
<br/>
              7. Enfin, si l'arrêté attaqué tient compte des charges fiscales et sociales des différentes catégories d'établissements entrant dans son champ d'application, il fixe l'évolution moyenne nationale et l'évolution moyenne dans chaque région des tarifs des prestations de soins de suite et de réadaptation et de psychiatrie pour les établissements privés mentionnés au d de l'article L. 162-22-6, sans distinguer les taux d'évolution applicables aux établissements à but lucratif de ceux applicables aux établissements à but non lucratif. Ainsi, la fédération requérante n'est pas fondée à soutenir qu'il serait entaché d'incompétence en ce qu'il fixerait des taux d'évolution différents selon le statut des établissements de santé.<br/>
<br/>
              Sur la légalité interne de l'arrêté attaqué : <br/>
<br/>
              8. En premier lieu, si l'arrêté attaqué prend en considération, dans la fixation des taux d'évolution des tarifs des prestations de soins de suite et de réadaptation et de psychiatrie, une partie de l'incidence du crédit d'impôt sur la taxe sur les salaires, prévu par l'article 231 A du code général des impôts issu de l'article 88 de la loi du 29 décembre 2016 de finances pour 2017 et applicable à raison des rémunérations versées jusqu'au 31 décembre 2018, sur le montant des charges des établissements de santé privés à but non lucratif, il n'en résulte pas pour autant qu'il modifierait les règles relatives à ce dispositif d'allègement de charges fiscales. Par suite, la fédération requérante n'est pas fondée à soutenir que les auteurs de l'arrêté auraient, pour ce motif, méconnu les dispositions législatives instituant l'allègement en cause.<br/>
<br/>
              9. En deuxième lieu, sur le fondement des dispositions des articles L. 162-22-2 et R. 162-31-4 du code de la sécurité sociale, les ministres chargés de la santé, de la sécurité sociale et du budget peuvent légalement tenir compte du niveau respectif des charges réellement exposées par les établissements mentionnés à l'article L. 162-22-6 du même code ainsi que des produits susceptibles de venir en atténuation des charges que les tarifs ont vocation à financer afin de déterminer l'objectif quantifié national relatif aux activités de psychiatrie. Il en va de même, sur le fondement des articles L. 162-23 et R. 162-34-4 du code de la sécurité sociale et du 6° du E du III de l'article 78 de la loi du 21 décembre 2015, pour la fixation de l'objectif de dépenses d'assurance maladie afférent aux activités de soins de suite et de réadaptation. Ces dispositions ne font pas obstacle, à ce titre, à ce qu'ils prennent en considération des charges de nature fiscale, ainsi que des atténuations de charge participant du régime fiscal auquel les établissements sont soumis, telles que celles qui résulte du crédit d'impôt sur la taxe sur les salaires prévu par l'article 231 A du code général des impôts, sur le niveau des charges de ces établissements. S'ils procèdent ainsi, les ministres chargés de la santé et de la sécurité sociale( doivent alors tirer les conséquences de l'objectif quantifié national et de l'objectif de dépenses de soins de suite et de réadaptation ainsi fixés et déterminer les éléments tarifaires mentionnés aux 1° à 3° du I de l'article L. 162-22-3 du code de la sécurité sociale en tenant compte, sur le fondement de l'article R. 162-31-5 du même code, des mêmes atténuations de charges. Par suite, en prenant en considération, pour édicter l'arrêté attaqué, l'incidence positive du crédit d'impôt sur la taxe sur les salaires sur le niveau des charges des établissements privés de santé à but non lucratif mentionnés au d de l'article L. 162 22-6, les ministres n'ont pas méconnu les dispositions de l'article R. 162-31-5 dont ils devaient faire application et n'ont pas commis d'erreur de droit.<br/>
<br/>
              10. En troisième lieu, si la fédération requérante soutient que l'incidence positive du crédit d'impôt sur la taxe sur les salaires a été absorbée par les hausses de salaires que les établissements de santé privés à but non lucratif ont consenties par un avenant signé le 15 mars 2017 à la convention collective du 31 octobre 1951 et agréé par un arrêté de la ministre des solidarités et de la santé du 4 juin 2017, la reprise du crédit d'impôt sur la taxe sur les salaires n'a été opérée qu'à hauteur de la moitié du bénéfice que les établissements en ont retiré à raison des rémunérations versées en 2018. Alors, au surplus, que les établissements privés à but non lucratif bénéficiaient également de la réduction générale de cotisations d'assurance maladie prévue à l'article L. 241-2-1 du code de la sécurité sociale dans sa rédaction issue de la loi du 22 décembre 2018 de financement de la sécurité sociale pour 2019, il ne ressort pas des pièces du dossier qu'en fixant l'évolution moyenne nationale des tarifs des prestations de soins de suite et de réadaptation et de psychiatrie aux niveaux prévus par l'arrêté attaqué, les ministres auraient commis, au regard notamment des charges de personnel des établissements de santé ayant une activité de soins de suite et de réadaptation ou de psychiatrie, une erreur manifeste dans l'application des dispositions qu'ils mettaient en oeuvre.<br/>
<br/>
              11. En quatrième lieu, l'arrêté attaqué se borne à fixer l'évolution moyenne nationale et l'évolution moyenne dans chaque région des tarifs des prestations des soins de suite et de réadaptation et de psychiatrie ainsi que les variations maximales et minimales des taux d'évolution des prestations qui peuvent être allouées aux établissements, ainsi que le prévoit l'article L. 162-22-3 du code de la sécurité sociale. Il ne fait pas obstacle à ce que les agences régionales de santé fixent les tarifs applicables à chaque établissement en tenant compte de sa situation particulière. Par suite, la fédération requérante n'est pas fondée à soutenir que, faute de prendre en considération l'activité propre à chaque établissement, l'arrêté serait entaché d'erreur manifeste d'appréciation.<br/>
<br/>
              12. Enfin, si la fédération requérante fait valoir que certains postes de charges des établissements de santé privés à but non lucratif, liés notamment aux condition d'emploi de leurs personnels, seraient supérieurs à ceux des établissements publics, il n'en résulte pas qu'en tenant compte pour partie de l'incidence positive des allègements de charges que représente, pour les établissements privés à but non lucratif, le crédit d'impôt sur la taxe sur les salaires, l'arrêté attaqué, qui fixe l'évolution moyenne nationale et l'évolution moyenne dans chaque région des tarifs des prestations de soins de suite et de réadaptation et de psychiatrie des établissements privés mentionnés au d de l'article L. 162-22-6 du code de la sécurité sociale, aurait méconnu le principe d'égalité. <br/>
<br/>
              13. Il résulte de tout ce qui précède que la Fédération des établissements hospitaliers et d'aide à la personne, privés à but non lucratif, n'est pas fondée à demander l'annulation pour excès de pouvoir de l'arrêté du 18 avril 2019 fixant pour l'année 2019 les éléments tarifaires mentionnés aux 1° à 3° du I de l'article L. 162-22-3 du code de la sécurité sociale des établissements de santé mentionnés au d de l'article L. 162-22-6 du code de la sécurité sociale.<br/>
<br/>
              Sur les frais liés au litige : <br/>
<br/>
              14. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1 : La requête de la Fédération des établissements hospitaliers, et d'aide à la personne, privés à but non lucratif est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la Fédération des établissements hospitaliers, et d'aide à la personne, privés à but non lucratif, au ministre des solidarités et de la santé et au ministre de l'économie, des finances et de la relance.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
