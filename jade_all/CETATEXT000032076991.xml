<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032076991</ID>
<ANCIEN_ID>JG_L_2016_02_000000392270</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/07/69/CETATEXT000032076991.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème SSJS, 17/02/2016, 392270, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-02-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>392270</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>Mme Charline Nicolas</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2016:392270.20160217</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés au secrétariat du contentieux du Conseil d'Etat les 3 août et 24 décembre 2015, M. et Mme A...C...demandent au Conseil d'Etat :<br/>
<br/>
              1°) de condamner l'Etat à leur verser les sommes, assorties des intérêts au taux légal, de :<br/>
              - 126 639 euros en réparation du préjudice matériel subi par M. C...; <br/>
              - 5 000 euros en réparation du préjudice moral subi par M. C...; <br/>
              - 5 000 euros en réparation du préjudice subi par Mme C...; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la défense ;<br/>
              - la loi n° 79-587 du 11 juillet 1979 ;<br/>
              - le décret n° 2008-939 du 12 septembre 2008 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Charline Nicolas, auditeur,<br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de M. et Mme C...;<br/>
<br/>
<br/>
<br/>1. Considérant que par décisions des 23 février et 5 mars 2015, le ministre de la défense a décidé de recruter M. C...en qualité d'officier sous contrat de la filière " spécialiste " ; que, par une décision du 31 mars 2015, il a retiré ces décisions ; que, par une décision n° 390487 du 19 octobre 2015, le Conseil d'Etat, statuant au contentieux, a annulé pour excès de pouvoir cette décision de retrait au motif que les décisions de recrutement avaient créé des droits au profit de l'intéressé et que leur retrait aurait dû être motivé en vertu des dispositions de l'article 1er de la loi du 11 juillet 1979 ; que M. et Mme C...demandent la condamnation de l'Etat à réparer les préjudices qu'ils ont subis du fait de cette décision illégale ;<br/>
<br/>
              Sur la fin de non recevoir opposée par le ministre de la défense :<br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 4125-1 du code de la défense : " I.- Tout recours contentieux formé par un militaire à l'encontre d'actes relatifs à sa situation personnelle est précédé d'un recours administratif préalable, à peine d'irrecevabilité du recours contentieux. / Ce recours administratif préalable est examiné par la commission des recours des militaires, placée auprès du ministre de la défense (...) ; II.- Les dispositions de la présente section ne sont pas applicables aux recours contentieux formés à l'encontre d'actes ou de décisions : 1° Concernant le recrutement du militaire (...) " ; qu'il résulte de ces dispositions que la recevabilité des recours contentieux tendant à l'indemnisation des dommages consécutifs à une décision de retrait d'une décision de recrutement n'est pas subordonnée à l'introduction d'un recours administratif préalable devant la commission des recours des militaires ; que, par suite, si le ministre soutient à bon droit que, faute pour l'intéressé d'avoir auparavant saisi aucun service du ministère à cette fin, la demande d'indemnisation adressée à cette commission le 14 avril 2015 par M.C..., en son nom propre et pour le compte de son épouse dont il mentionne le préjudice propre, doit être regardée comme une réclamation préalable auprès du ministre lui-même, il n'est pas fondé à soutenir que la requête serait irrecevable faute de saisine de cette commission ;<br/>
<br/>
              Sur les conclusions indemnitaires :<br/>
<br/>
              3. Considérant que l'illégalité de la décision du 31 mars 2015 est constitutive d'une faute de nature à engager la responsabilité de l'Etat ; que si le ministre de la défense fait valoir que sa décision de retrait du recrutement de M. C...aurait été justifiée sur le fond eu égard au comportement passé adopté par l'intéressé à l'occasion de ses précédentes fonctions d'officier sous contrat " spécialiste ", il ne soutient pas que les décisions des 23 février et 5 mars 2015, qui étaient créatrices de droit, auraient été illégales ; que la régularité du retrait de l'acte d'engagement d'un agent étant subordonnée notamment au caractère illégal de cet acte, le ministre n'est ainsi pas fondé à soutenir, par ce motif, que la faute qu'il a commise serait sans lien avec le préjudice invoqué par les requérants ; qu'en tout état de cause, les allégations de l'administration sur la manière de servir de M. C...ne sont assorties d'aucune pièce ni d'aucun commencement de preuve ;<br/>
<br/>
              4. Considérant que si le ministre de la défense fait valoir, de manière générale, qu'en vertu des dispositions de l'article 6 du décret du 12 septembre 2008 relatif aux officiers sous contrat, les officiers sous contrats sont soumis à une période probatoire de six mois durant laquelle l'administration peut dénoncer unilatéralement leurs contrats et que ces contrats ne deviennent définitifs qu'à l'issue de cette période probatoire, il résulte de l'instruction que du fait de la décision illégale de l'administration, M. C...n'a pas été mis à même de commencer à exercer ses fonctions d'officier sous contrat et a par ailleurs perdu une chance sérieuse que son contrat de cinq ans devienne définitif ; que, dans les circonstances de l'espèce, son préjudice matériel correspond à la différence entre la rémunération qu'il aurait dû obtenir et celle qu'il a effectivement perçue de la part de l'agence de reconversion de la défense qui l'a maintenu dans ses fonctions à compter de la date d'effet de la décision de retrait ; qu'il résulte de l'instruction et n'est d'ailleurs pas contesté sur ce point par l'administration que ce préjudice s'élève à la somme de 671,88 euros par mois à compter du 1er avril 2015 ; qu'à la date de lecture de la présente décision, le préjudice subi à ce titre par M. C...s'élevait à la somme de 7 099,53 euros ; qu'en revanche, M. C...n'est pas fondé à demander la réparation du préjudice qu'il serait susceptible de subir à l'avenir, ce dernier n'étant, en l'espèce, qu'éventuel, l'administration pouvant notamment lui demander d'exercer les fonctions d'officier sous contrat pour lesquelles il a été recruté par les décisions des 23 février et 5 mars 2015 ; <br/>
<br/>
              5. Considérant qu'il sera fait une juste appréciation du préjudice moral subi par M. C...du fait de la faute commise par l'administration en l'évaluant à la somme de 1 000 euros ; <br/>
<br/>
              6. Considérant, en revanche, que le lien de causalité direct entre la faute commise par l'administration et la perte de chance de promotion de l'épouse de M. C...n'étant pas établi, les conclusions indemnitaires présentées par Mme C...doivent être rejetées ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que l'Etat doit être condamné à verser à M. C...la somme de 8 099,53 euros, assortie des intérêts au taux légal à compter de la réception de sa demande préalable du 14 avril 2015 ;<br/>
<br/>
              8. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement d'une somme de 3 000 euros à M. et Mme C...au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'Etat est condamné à verser la somme de 8 099,53 euros à M.C.... Cette somme portera intérêt à compter de la réception par l'administration de la demande du 14 avril 2015.<br/>
Article 2 : L'Etat versera la somme de 3 000 euros à M. et Mme C...en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Le surplus des conclusions de la requête est rejeté.<br/>
Article 4 : La présente décision sera notifiée à M. A...C..., à Mme B...C...et au ministre de la défense.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
