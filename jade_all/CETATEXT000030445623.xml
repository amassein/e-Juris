<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030445623</ID>
<ANCIEN_ID>JG_L_2015_03_000000371852</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/44/56/CETATEXT000030445623.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème / 5ème SSR, 27/03/2015, 371852</TITRE>
<DATE_DEC>2015-03-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>371852</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème / 5ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON ; SCP BOULLEZ</AVOCATS>
<RAPPORTEUR>M. Philippe Orban</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:371852.20150327</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 3 septembre et 3 décembre 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société Den Hartogh, dont le siège est au Centre de commerce international, quai George V au Havre (76000) ; la société Den Hartogh demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 13DA00114 du 3 juillet 2013 par lequel la cour administrative d'appel de Douai a rejeté sa requête tendant à l'annulation du jugement n° 1000748 du 27 novembre 2012 par lequel le tribunal administratif d'Amiens a, à la demande de Mme A...B..., annulé pour excès de pouvoir la décision du 12 janvier 2010 de l'inspecteur du travail autorisant son licenciement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de lui allouer la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code du travail ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Philippe Orban, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boullez, avocat de la Société Den Hartogh et à la SCP Boré, Salve de Bruneton, avocat de Mme B... ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'en vertu des dispositions du code du travail, les salariés légalement investis de fonctions représentatives bénéficient, dans l'intérêt de l'ensemble des travailleurs qu'ils représentent, d'une protection exceptionnelle ; qu'en particulier, il résulte des dispositions des articles L. 425-1 et L. 436-1 du code du travail, alors en vigueur, désormais reprises à l'article L. 2421-3 du même code, que tout licenciement envisagé par l'employeur d'un salarié élu délégué du personnel ou membre du comité d'entreprise, en qualité de titulaire ou de suppléant, est obligatoirement soumis à l'avis du comité d'entreprise ; qu'il appartient à l'employeur de mettre le comité d'entreprise à même d'émettre son avis, en toute connaissance de cause, sur la procédure dont fait l'objet le salarié protégé ; qu'à cette fin, il doit lui transmettre, notamment à l'occasion de la communication qui est faite aux membres du comité de l'ordre du jour de la réunion en cause, des informations précises et écrites sur l'identité du salarié visé par la procédure, sur l'intégralité des mandats détenus par ce dernier ainsi que sur les motifs du licenciement envisagé ; qu'il appartient à l'administration saisie d'une demande d'autorisation de licenciement d'apprécier si l'avis du comité d'entreprise a été régulièrement émis, et notamment si le comité a disposé des informations lui permettant de se prononcer en toute connaissance de cause ; qu'à défaut, elle ne peut légalement accorder l'autorisation demandée ; <br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que MmeB..., salariée de la société Den Hartogh exerçant les mandats de déléguée du personnel suppléante ainsi que de membre de la délégation unique du personnel au comité d'entreprise, a fait l'objet d'une procédure de licenciement pour faute ; que l'inspecteur du travail a, par une décision du 12 janvier 2010, autorisé le licenciement de la salariée ;<br/>
<br/>
              3. Considérant que pour juger que l'avis émis par le comité d'entreprise sur le licenciement de Mme B...était irrégulier, la cour administrative d'appel de Douai s'est fondée sur ce que l'employeur, en n'indiquant pas le motif du licenciement envisagé dans la convocation adressée aux membres du comité d'entreprise, n'avait pas satisfait à l'obligation, résultant des dispositions des articles L. 2421-3 et L. 2323-4 du code du travail, de fournir au comité d'entreprise des informations précises et écrites et ne l'avait, ainsi, pas mis à même d'émettre son avis en toute connaissance de cause sur les motifs du licenciement ;<br/>
<br/>
              4. Considérant toutefois que l'absence de transmission par l'employeur lors de la convocation du comité d'entreprise des informations requises, pour la consultation prévue à l'article L. 2421-3 du code du travail, par l'article L. 2323-4 du même code, n'entache pas d'irrégularité cette consultation si le comité d'entreprise a tout de même disposé de ces informations dans des conditions lui permettant d'émettre son avis en toute connaissance de cause ; qu'ainsi, en se fondant sur la seule circonstance que le motif du licenciement envisagé ne figurait pas dans la convocation du comité d'entreprise pour en déduire que celui-ci n'avait pu se prononcer en toute connaissance de cause et que l'administration ne pouvait en conséquence légalement autoriser le licenciement demandé, la cour administrative d'appel de Douai a entaché son arrêt d'erreur de droit ; que, par suite, la société Den Hartogh est fondée à en demander l'annulation ;<br/>
<br/>
              5. Considérant que la société Den Hartogh ne précisant pas la personne à la charge de laquelle doivent être mises les sommes qu'elle demande sur le fondement  de l'article L. 761-1 du code de justice administrative, ces dispositions font obstacle à ce qu'il soit fait droit à ses conclusions ; <br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Douai du 3 juillet 2013 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Douai.<br/>
Article 3 : Les conclusions présentées par la société Den Hartogh au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la société Den Hartogh et à Mme A...B....<br/>
Copie en sera adressée pour information au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">66-07-01-02-02 TRAVAIL ET EMPLOI. LICENCIEMENTS. AUTORISATION ADMINISTRATIVE - SALARIÉS PROTÉGÉS. PROCÉDURE PRÉALABLE À L'AUTORISATION ADMINISTRATIVE. CONSULTATION DU COMITÉ D'ENTREPRISE. - DEVOIR DE L'EMPLOYEUR DE METTRE LE COMITÉ D'ENTREPRISE À MÊME D'ÉMETTRE SON AVIS EN TOUTE CONNAISSANCE DE CAUSE - EXISTENCE - INFORMATIONS DEVANT ÊTRE TRANSMISES AU COMITÉ À CE TITRE - DEVOIR DE L'ADMINISTRATION SAISIE D'UNE DEMANDE D'AUTORISATION DE LICENCIEMENT DE VÉRIFIER LA RÉGULARITÉ DE L'AVIS DU COMITÉ D'ENTREPRISE ET DE REFUSER L'AUTORISATION EN CAS D'IRRÉGULARITÉ - EXISTENCE.
</SCT>
<ANA ID="9A"> 66-07-01-02-02 Il résulte des dispositions des articles L. 425-1 et L. 436-1 du code du travail, désormais reprises à l'article L. 2421-3 du même code, que tout licenciement envisagé par l'employeur d'un salarié élu délégué du personnel ou membre du comité d'entreprise, en qualité de titulaire ou de suppléant, est obligatoirement soumis à l'avis du comité d'entreprise. Il appartient à l'employeur de mettre le comité d'entreprise à même d'émettre son avis, en toute connaissance de cause, sur la procédure dont fait l'objet le salarié protégé. A cette fin, il doit lui transmettre, notamment à l'occasion de la communication qui est faite aux membres du comité de l'ordre du jour de la réunion en cause, des informations précises et écrites sur l'identité du salarié visé par la procédure, sur l'intégralité des mandats détenus par ce dernier ainsi que sur les motifs du licenciement envisagé. Il appartient à l'administration saisie d'une demande d'autorisation de licenciement d'apprécier si l'avis du comité d'entreprise a été régulièrement émis, et notamment si le comité a disposé des informations lui permettant de se prononcer en toute connaissance de cause. A défaut, elle ne peut légalement accorder l'autorisation demandée.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
