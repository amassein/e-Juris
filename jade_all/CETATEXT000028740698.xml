<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028740698</ID>
<ANCIEN_ID>JG_L_2014_03_000000357657</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/74/06/CETATEXT000028740698.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 17/03/2014, 357657, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-03-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>357657</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>Mme Julia Beurton</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:357657.20140317</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête et le nouveau mémoire, enregistrés les 16 mars et 18 avril 2012 au secrétariat du contentieux du Conseil d'Etat, présentés par le syndicat des médecins d'Aix et région, dont le siège est 5, boulevard du Roy René à Aix-en-Provence (13100), représenté par son secrétaire général ; le syndicat requérant demande au Conseil d'Etat d'annuler pour excès de pouvoir l'avis réputant approuvé l'avenant n° 2 à la convention nationale organisant les rapports entre les médecins libéraux et l'assurance maladie signée le 26 juillet 2011 ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule ; <br/>
<br/>
              Vu le code de la sécurité sociale ;<br/>
<br/>
              Vu la loi n° 2011-940 du 10 août 2011 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Julia Beurton, Auditeur,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 161-35 du code de la sécurité sociale, dans sa rédaction issue du I de l'article 11 de la loi du 10 août 2011 : " I.-Les professionnels de santé (...) sont tenus d'assurer, pour les bénéficiaires de l'assurance maladie, la transmission électronique des documents visés à l'article L. 161-33 et servant à la prise en charge des soins, produits ou prestations remboursables par l'assurance maladie. / II.-Sans préjudice des dispositions de l'article L. 161-33, le non-respect de l'obligation de transmission électronique par les professionnels (...) donne lieu à l'application d'une sanction conventionnelle. / III.-Les conventions mentionnées aux articles L. 162-14-1, L. 162-16-1 et L. 162-32-1 déterminent le mode de mise en oeuvre de cette sanction conventionnelle (...) / IV.-A défaut de dispositions conventionnelles applicables au titre du présent article, le directeur général de l'Union nationale des caisses d'assurance maladie fixe les dispositions mentionnées au III " ; qu'aux termes du II du même article 11 : " Si les parties conventionnelles n'ont pas conclu avant le 30 septembre 2011 un accord pour la mise en oeuvre du présent article, le IV de l'article L. 161-35 du code de la sécurité sociale s'applique à compter de cette date " ;<br/>
<br/>
              2. Considérant que, pour l'application des dispositions de l'article L. 161-35 du code de la sécurité sociale, l'Union nationale des caisses d'assurance maladie (UNCAM), la confédération des syndicats médecins français, la fédération française des médecins généralistes et le syndicat des médecins libéraux ont conclu, le 24 novembre 2011, un avenant à la convention nationale organisant les rapports entre les médecins libéraux et l'assurance maladie signée le 26 juillet 2011 ; que le syndicat des médecins d'Aix et région doit être regardé comme demandant l'annulation pour excès de pouvoir de la décision implicite par laquelle, en vertu de l'article L. 162-15 du code de la sécurité sociale, les ministres chargés de la santé et de la sécurité sociale ont approuvé cet avenant ; <br/>
<br/>
              3. Considérant, en premier lieu, que les dispositions du II de l'article 11 de la loi du 10 août 2011 n'ont eu ni pour objet ni pour effet de dessaisir les partenaires conventionnels, à compter du 30 septembre 2011, de leur compétence pour fixer les modalités de mise en oeuvre de la sanction prévue au II de l'article L. 161-35 du code de la sécurité sociale, mais seulement de fixer la date à partir de laquelle le directeur général de l'UNCAM pourrait fixer ces dispositions en cas de carence de leur part ; qu'à la date du 24 novembre 2011, en l'absence d'intervention de celui-ci, les partenaires conventionnels étaient habilités à signer un avenant à la convention nationale organisant les rapports entre les médecins libéraux et l'assurance maladie, prévoyant la sanction applicable en cas de manquement aux dispositions du I de l'article L. 161-35 et adaptant la procédure conventionnelle de sanction à respecter ; que, par suite, le moyen tiré de ce que la décision attaquée approuve un avenant entaché d'incompétence doit être écarté ;<br/>
<br/>
              4. Considérant, en second lieu, que l'avenant litigieux se borne à définir la sanction applicable aux médecins ne respectant pas l'obligation de transmission électronique des documents de facturation des actes et prestations prévue par l'article L. 161-35 du code de la sécurité sociale et à adapter en conséquence la procédure de sanction prévue par la convention du 26 juillet 2011, en prévoyant des délais et des conditions d'exécution spécifiques ; que, contrairement à ce que soutient le syndicat requérant, il n'a ni pour objet ni pour effet de confier des missions de poursuite, d'instruction et de jugement aux caisses d'assurance maladie et à leurs directeurs ; que, par suite, le moyen tiré de ce que l'avenant litigieux porterait atteinte, pour ce motif, à la séparation des pouvoirs et méconnaîtrait les principes d'impartialité et d'indépendance découlant de l'article 16 de la Déclaration des droits de l'homme et du citoyen ne peut qu'être écarté ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que la requête du syndicat des médecins d'Aix et région doit être rejetée ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du syndicat requérant la somme de 2 000 euros à verser à l'Union nationale des caisses d'assurance maladie au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête du syndicat des médecins d'Aix et région est rejetée.<br/>
Article 2 : Le syndicat des médecins d'Aix et région versera à l'Union nationale des caisses d'assurance maladie la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée au syndicat des médecins d'Aix et région, à l'Union nationale des caisses d'assurance maladie et à la ministre des affaires sociales et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
