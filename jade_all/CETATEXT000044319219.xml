<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044319219</ID>
<ANCIEN_ID>JG_L_2021_11_000000445353</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/31/92/CETATEXT000044319219.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 10/11/2021, 445353, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-11-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>445353</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Frédéric Gueudar Delahaye</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Marc Pichon de Vendeuil</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:445353.20211110</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une ordonnance n° 2005995 du 13 octobre 2020, enregistrée le 14 octobre 2020 au secrétariat du contentieux du Conseil d'Etat, le président du tribunal administratif de Paris a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête présentée à ce tribunal par le syndicat Solidaires CCRF et SCL. Par cette requête, enregistrée au greffe du tribunal administratif de Paris le 31 mars 2020, le syndicat Solidaires CCRF et SCL demande au Conseil d'Etat d'annuler la note de service du 1er février 2020 de la directrice générale de la concurrence, de la consommation et de la répression des fraudes (DGCCRF) relative au temps de travail en administration centrale et dans les trois services à compétence nationale de la DGCCRF.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - le décret n° 82-453 du 28 mai 1982 ;<br/>
              - le décret n° 2000-815 du 25 août 2000 ;<br/>
              - le décret n° 2009-1630 du 23 décembre 2009 ;<br/>
              - le décret n° 2011-184 du 15 février 2011 ;<br/>
              - l'arrêté du 8 février 2002 fixant des dispositions spécifiques pour l'aménagement et la réduction du temps de travail de certains personnels du ministère de l'économie, des finances et de l'industrie;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Gueudar Delahaye, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Marc Pichon de Vendeuil, rapporteur public ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Le syndicat Solidaires CCRF et SCL demande l'annulation pour excès de pouvoir de la note de service du 1er février 2020 de la directrice générale de la concurrence, de la consommation et de la répression des fraudes (DGCCRF) relative au temps de travail en administration centrale et dans les trois services à compétence nationale de la DGCCRF. Eu égard aux moyens soulevés, la requête doit être regardée comme tendant à l'annulation des dispositions du deuxième alinéa du point 9 et du point 19 de cette note, ainsi que de celles relatives au recours aux astreintes, en tant qu'elles s'appliquent aux enquêteurs du service à compétence nationale " Service national des enquêtes " (SNE). <br/>
<br/>
              2. En premier lieu, aux termes de l'article 10 du décret du 25 août 2000 relatif à l'aménagement et à la réduction du temps de travail dans la fonction publique de l'Etat et dans la magistrature : " (...) le régime de travail de personnels chargés soit de fonctions d'encadrement, soit de fonctions de conception lorsqu'ils bénéficient d'une large autonomie dans l'organisation de leur travail ou sont soumis à de fréquents déplacements de longue durée peut, le cas échéant, faire l'objet de dispositions spécifiques adaptées à la nature et à l'organisation du service ainsi qu'au contenu des missions de ces personnels. Ces dispositions sont adoptées par arrêté du ministre intéressé, du ministre chargé de la fonction publique et du ministre chargé du budget, pris après avis du comité technique ministériel ". Aux termes de l'article 1er de l'arrêté du 8 février 2002 fixant des dispositions spécifiques pour l'aménagement et la réduction du temps de travail de certains personnels du ministère de l'économie, des finances et de l'industrie : " En application de l'article 10 du décret du 25 août 2000 susvisé, les personnels exerçant les fonctions dont la liste est fixée en annexe au présent arrêté peuvent être soumis à un régime forfaitaire du temps de travail. Les chefs de service désignent les personnels concernés. ". Aux termes de la liste annexée à cet arrêté, sont au nombre des " personnels pouvant être soumis à un régime forfaitaire de temps de travail " ceux qui exercent des " fonctions d'inspection et de contrôle " dans les services centraux. Aux termes de l'article 2 du décret du 23 décembre 2009 portant création d'un service à compétence nationale dénommé " service national des enquêtes " : " Le service national des enquêtes a compétence pour réaliser sur l'ensemble du territoire national des enquêtes nationales et européennes. / Ces enquêtes visent à la recherche et à la constatation des infractions et manquements au droit national et européen et à la collecte d'informations économiques (...) ".<br/>
<br/>
              3. Il résulte des dispositions citées au point précédent que la directrice générale de la DGCCRF était compétente pour soumettre, par la note de service attaquée, les enquêteurs du service national des enquêtes au régime forfaitaire de temps de travail et pour prévoir que les missions de contrôle éloignant ces agents du siège du service pourraient faire l'objet d'une comptabilisation forfaitaire. Elle n'a, ce faisant, méconnu ni les dispositions du décret du 25 août 2000, ni celles de l'arrêté précité du 8 février 2002.<br/>
<br/>
              4. En second lieu, d'une part, aux termes de l'article 34 du décret du 15 février 2011 relatif aux comités techniques dans les administrations et les établissements publics de l'Etat : " Les comités techniques sont consultés, dans les conditions et les limites précisées pour chaque catégorie de comité par les articles 35 et 36 sur les questions et projets de textes relatifs : / 1° A l'organisation et au fonctionnement des administrations, établissements ou services ; (...) 9° A l'hygiène, à la sécurité et aux conditions de travail lorsqu'aucun comité d'hygiène, de sécurité et de conditions de travail n'est placé auprès d'eux. Le comité technique bénéficie du concours du comité d'hygiène, de sécurité et de conditions de travail dans les matières relevant de sa compétence et peut le saisir de toute question. Il examine en outre toute question dont il est saisi par le comité d'hygiène, de sécurité et des conditions de travail créé auprès de lui. (...) ". D'autre part, l'article 47 du décret du 28 mai 1982 pris pour l'application de l'article 16 de la loi du 11 janvier 1984, précise, dans sa rédaction issue du décret du 28 juin 2011, que les comités d'hygiène, de sécurité et des conditions de travail exercent leurs missions " sous réserve des compétences des comités techniques ". Le 1° de l'article 57 du même décret prévoit que le comité d'hygiène, de sécurité et des conditions de travail est notamment consulté " sur les projets d'aménagement importants modifiant les conditions de santé et de sécurité ou les conditions de travail (...) ".<br/>
<br/>
              5. Il résulte de ces dispositions qu'une question ou un projet de disposition ne doit être soumis à la consultation du comité d'hygiène, de sécurité et des conditions de travail que si le comité technique ne doit pas lui-même être consulté sur la question ou le projet de disposition en cause. Le comité d'hygiène, de sécurité et des conditions de travail ne doit ainsi être saisi que d'une question ou projet de disposition concernant exclusivement la santé, la sécurité ou les conditions de travail. En revanche, lorsqu'une question ou un projet de disposition concerne ces matières et l'une des matières énumérées à l'article 34 du décret du 15 février 2011, seul le comité technique doit être obligatoirement consulté. Par suite, le moyen tiré de ce que le comité d'hygiène, de sécurité et des conditions de travail aurait dû être consulté sur une note de service relative au temps de travail question qui relève de la compétence du comité technique doit être écarté.<br/>
<br/>
              6. En troisième lieu, si le syndicat requérant soutient que la note de service litigieuse mettrait en œuvre un régime d'astreintes illégal pour les enquêteurs du SNE, il n'assortit cette allégation d'aucun élément permettant d'en apprécier le bien-fondé. <br/>
<br/>
              7. Il résulte de ce qui précède que le syndicat Solidaires CCRF et SCL n'est pas fondé à demander l'annulation de la note de service attaquée.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête du syndicat Solidaires CCRF et SCL est rejetée.<br/>
Article 2 : La présente décision sera notifiée au syndicat Solidaires CCRF et SCL et au ministre de l'économie, des finances et de la relance.<br/>
              Délibéré à l'issue de la séance du 21 octobre 2021 où siégeaient : M. Olivier Japiot, président de chambre, présidant ; M. Gilles Pellissier, conseiller d'Etat et M. Frédéric Gueudar Delahaye, conseiller d'Etat-rapporteur. <br/>
<br/>
              Rendu le 10 novembre 2021.<br/>
<br/>
<br/>
                 Le président : <br/>
                 Signé : M. Olivier Japiot<br/>
 		Le rapporteur : <br/>
      Signé : M. Frédéric Gueudar Delahaye<br/>
                 La secrétaire :<br/>
                 Signé : Mme B... A...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
