<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038166192</ID>
<ANCIEN_ID>JG_L_2019_02_000000421926</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/16/61/CETATEXT000038166192.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 25/02/2019, 421926, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-02-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421926</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Guillaume Leforestier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:421926.20190225</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Cergy-Pontoise d'annuler la décision du 1er mars 2017 de la commission de médiation du département des Hauts-de-Seine refusant de reconnaître comme prioritaire sa demande de logement. Par un jugement n° 1702657 du 2 mai 2018, le tribunal administratif a annulé cette décision et enjoint au préfet des Hauts-de-Seine de saisir la commission de médiation en vue du réexamen de la situation de Mme A...dans un délai d'un mois à compter de la notification de ce jugement.<br/>
<br/>
              Par un pourvoi enregistré le 3 juillet 2018 au secrétariat du contentieux du Conseil d'Etat, le ministre de la cohésion des territoires demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) statuant au fond, de rejeter la demande de MmeA....<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la construction et de l'habitation ;<br/>
              - le code de la sécurité sociale ;<br/>
              - le décret n° 2002-120 du 30 janvier 2002 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Leforestier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que Mme A...a saisi la commission de médiation des Hauts-de-Seine sur le fondement du II de l'article L. 441-2-3 du code de la construction et de l'habitation afin que soit reconnu le caractère prioritaire et urgent de sa demande de logement social. Par une décision du 1er mars 2017, la commission a rejeté sa demande au motif que le logement qu'elle occupait, dont l'état de non-décence n'était pas avéré en l'absence d'un rapport du service d'hygiène, ne pouvait être regardé comme suroccupé dès lors que sa surface était supérieure à la surface minimale de 43 m2 pour cinq personnes prévue par les dispositions de l'article D. 542-14 du code de la sécurité sociale. Le ministre de la cohésion des territoires se pourvoit en cassation contre le jugement du 2 mai 2018 par lequel le tribunal administratif de Cergy-Pontoise a annulé cette décision à la demande de l'intéressée. <br/>
<br/>
              2. Aux termes du deuxième aliéna du II de l'article L. 441-2-3 du code de la construction et de l'habitation, la commission de médiation peut être saisie par toute personne satisfaisant aux conditions règlementaires d'accès à un logement locatif social, sans condition de délai, " lorsque le demandeur est logé dans les locaux manifestement suroccupés ou ne présentant pas le caractère d'un logement décent, s'il a au moins un enfant mineur, s'il présente un handicap au sens de l'article L. 114 du code de l'action sociale et des familles ou s'il a au moins une personne à charge présentant un tel handicap. (...) ".  L'article R. 441-14-1 du même code dispose que  : " Peuvent être désignées par la commission comme prioritaires et devant être logées d'urgence en application du II de l'article L. 441-2-3 les personnes de bonne foi qui satisfont aux conditions règlementaires d'accès au logement social qui se trouvent dans l'une des situations prévues au même article et qui répondent aux caractéristiques suivantes : (...) - être handicapées, ou avoir à leur charge une personne en situation de handicap, ou avoir à leur charge au moins un enfant mineur, et occuper un logement (...) d'une surface habitable inférieure aux surfaces mentionnées au 2° de l'article D. 542-14 du code de la sécurité sociale, ou, pour une personne seule, d'une surface inférieure à celle mentionnée au  premier alinéa de l'article 4 du même décret ". Aux termes de l'article D. 542-14 du code de la sécurité sociale : " Le logement au titre duquel le droit à l'allocation de logement est ouvert doit être occupé à titre de résidence principale et répondre aux conditions suivantes : (...) / 2° Présenter une surface habitable globale au moins égale à seize mètres carrés pour un ménage sans enfant ou deux personnes, augmentée de neuf mètres carrés par personne en plus dans la limite de soixante-dix mètres carrés pour huit personnes et plus ".<br/>
<br/>
              3. Il résulte de ces dispositions qu'une personne handicapée ou ayant à sa charge au moins un enfant mineur ou une personne handicapée peut être désignée comme prioritaire et devant être logée en urgence si la surface habitable de son logement est inférieure au minimum fixé au 2° de l'article D. 542-14 du code de la sécurité sociale, soit 16 m² pour un ménage sans enfant ou deux personnes, augmenté de 9 m² par personne en plus dans la limite de 70 m². Il ressort des pièces du dossier soumis au juge du fond que Mme A...occupe avec quatre autres personnes, dont un enfant mineur, un appartement de 53 m2 dont la surface habitable est supérieure au seuil de 43 m2 applicable à un foyer de cinq personnes en vertu de ces dispositions. En se fondant, pour retenir néanmoins que l'appartement en cause était suroccupé, sur les dispositions de l'article 4 du décret du 30 janvier 2002 relatif aux caractéristiques du logement décent, alors que le seuil de surface habitable devait être apprécié au regard des critères fixés par le 2° de l'article D. 542-14 du code de la sécurité sociale, le tribunal administratif a entaché son jugement d'une erreur de droit. Le ministre de la cohésion des territoires est, par suite, fondé à en demander l'annulation.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 2 mai 2018 du tribunal administratif de Cergy-Pontoise est annulé.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Cergy-Pontoise.<br/>
Article 3 : La présente décision sera notifiée à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales.<br/>
Copie pour information en sera adressée à Mme B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
