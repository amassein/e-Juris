<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039335848</ID>
<ANCIEN_ID>JG_L_2019_11_000000418174</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/33/58/CETATEXT000039335848.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 06/11/2019, 418174, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-11-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418174</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>Mme Françoise Tomé</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Raphaël Chambon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:418174.20191106</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés les 14 février et 17 septembre 2018 au secrétariat du contentieux du Conseil d'Etat, Mme B... A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 14 décembre 2017 par laquelle le Conseil national de l'ordre des chirurgiens-dentistes, siégeant en formation restreinte, a rejeté sa demande d'inscription au tableau du Conseil départemental de Seine-Saint-Denis de l'ordre des chirurgiens-dentistes ;<br/>
<br/>
              2°) de mettre à la charge du Conseil national de l'ordre des chirurgiens-dentistes la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la directive 2005/36/CE du Parlement européen et du Conseil du 7 septembre 2005 relative à la reconnaissance des qualifications professionnelles ;<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Françoise Tomé, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de Mme A... et à la SCP Lyon-Caen, Thiriez, avocat du Conseil national de l'ordre des chirurgiens-dentistes ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier que Mme A... a demandé au conseil départemental de Seine-Saint-Denis de l'ordre des chirurgiens-dentistes son inscription au tableau des chirurgiens-dentistes, qui lui a été refusée. Le Conseil régional d'Ile-de-France de l'ordre des chirurgiens-dentistes a, par une décision du 19 septembre 2017, estimé que la demande d'inscription au tableau des chirurgiens-dentistes formée par Mme A... auprès du conseil départemental de Seine-Saint-Denis était irrecevable. Le Conseil national de l'ordre des chirurgiens-dentistes, statuant en formation restreinte, a, par la décision attaquée du 14 décembre 2017, annulé cette décision et refusé de faire droit à sa demande d'inscription au tableau des chirurgiens-dentistes, au motif qu'elle ne remplissait pas les conditions légales de diplômes.<br/>
<br/>
              2. Il ressort des pièces du dossier que, contrairement à ce que soutient Mme A..., les membres composant la formation restreinte du Conseil national de l'ordre des chirurgiens-dentistes ont été régulièrement convoqués à la séance au cours de laquelle son recours a été examiné. Par suite, le moyen tiré de l'irrégularité de leur convocation doit être écarté. <br/>
<br/>
              3. Aux termes de l'article L. 4111-1 du code de la santé publique : " Nul ne peut exercer la profession (...) de chirurgien-dentiste (...) s'il n'est : / 1° Titulaire d'un diplôme, certificat ou autre titre mentionné aux articles L. 4131-1, L. 4141-3 ou L. 4151-5 ; / 2° De nationalité française, de citoyenneté andorrane ou ressortissant d'un Etat membre de l'Union européenne ou partie à l'accord sur l'Espace économique européen, du Maroc ou de la Tunisie, sous réserve de l'application, le cas échéant, soit des règles fixées au présent chapitre, soit de celles qui découlent d'engagements internationaux autres que ceux mentionnés au présent chapitre ; / 3° Inscrit (...) à un tableau de l'ordre des chirurgiens-dentistes (...) , sous réserve des dispositions des articles L. 4112-6 et L. 4112-7. / Les (...) chirurgiens-dentistes (...) titulaires d'un diplôme, certificat ou autre titre mentionné au 1° de l'article L. 4131-1, aux 1° et 2° de l'article L. 4141-3 ou au 1° de l'article L. 4151-5 sont dispensés de la condition de nationalité prévue au 2 ". Aux termes de l'article L. 4141-3 du même code : " Les titres de formation exigés en application du 1° de l'article L. 4111-1 sont pour l'exercice de la profession de chirurgien-dentiste : / 1° Soit le diplôme français d'Etat de docteur en chirurgie dentaire ; / 2° Soit le diplôme français d'Etat de chirurgien-dentiste ; / 3° Soit si l'intéressé est ressortissant d'un Etat membre de l'Union européenne ou partie à l'accord sur l'Espace économique européen : (...../ g) Les titres de formation de praticien de l'art dentaire délivrés par un Etat, membre ou partie, sanctionnant une formation débutée avant le 18 janvier 2016 ".<br/>
<br/>
              4. Il ressort des pièces du dossier que Mme A..., de nationalité française, est titulaire d'un diplôme de chirurgie dentaire délivré en 1988 par la faculté d'Oran en Algérie et de plusieurs diplômes obtenus en France entre 1993 et 2000, sanctionnant diverses formations universitaires relevant de spécialités de l'art dentaire. Si, sur sa demande, le ministre en charge de l'enseignement supérieur lui a délivré le 25 novembre 2004, une attestation valant reconnaissance de la valeur scientifique de son diplôme algérien, tout en lui indiquant qu'une telle attestation, destinée à lui permettre de poursuivre ses études, ne lui permettrait pas de s'inscrire au tableau de l'ordre, il est constant  que Mme A... n'est titulaire d'aucun des diplômes français d'Etat de docteur en chirurgie dentaire ou de chirurgien-dentiste prévus par les 1° et 2° de l'article L. 4141-3 du code de la santé publique mentionnés au point précédent. D'autre part, les divers diplômes français sanctionnant des formations de praticien de l'art dentaire dont elle est titulaire ne sont pas au nombre des titres de formation, délivrés par un Etat membre de l'Union européenne ou partie à l'accord sur l'Espace économique européen, relevant du g) du 3° du même article, lequel doit être interprété comme ne visant que les titres mentionnés à l'article 5.3.2 de l'annexe V de la directive 2005/36/CE du Parlement européen et du Conseil du 7 septembre 2005 relative à la reconnaissance des qualifications professionnelles. Dès lors, le moyen tiré de ce que la décision attaquée procèderait d'une application erronée des articles L. 4111-1 et L. 4141-3 du code de la santé publique ne peut qu'être écarté en ses deux branches.<br/>
<br/>
              5. Il résulte de ce qui précède que Mme A... n'est pas fondée à demander l'annulation de la décision attaquée. <br/>
<br/>
              6. Les dispositions de l'article L. 761-1 du code de justice administrative font, par suite, obstacle à ce qu'une somme soit mise, à ce titre, à la charge du Conseil national de l'ordre des chirurgiens-dentistes, qui n'est pas, dans la présente instance, la partie perdante. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par le Conseil national de l'ordre des chirurgiens-dentistes sur le fondement des mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de Mme A... est rejetée.<br/>
Article 2 : Les conclusions présentées par le Conseil national de l'ordre des chirurgiens-dentistes au titre de l'article L. 761-1 du code de la justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à Mme B... A... et au Conseil national de l'ordre des chirurgiens-dentistes.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
