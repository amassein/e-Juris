<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029835084</ID>
<ANCIEN_ID>JG_L_2014_12_000000358365</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/83/50/CETATEXT000029835084.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère sous-section jugeant seule, 03/12/2014, 358365, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-12-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>358365</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Yannick Faure</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:358365.20141203</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 10 avril et 11 septembre 2012 au secrétariat du contentieux du Conseil d'Etat, le syndicat SUD travail affaires sociales demande au Conseil d'Etat d'annuler pour excès de pouvoir l'arrêté du ministre du travail, de l'emploi et de la santé, du ministre de l'éducation nationale, de la jeunesse et de la vie associative, du ministre des solidarités et de la cohésion sociale, du ministre de la ville et du ministre des sports du 14 février 2012 modifiant l'arrêté du 31 mars 2009 modifié relatif à l'entretien professionnel des personnels du ministère du travail, des relations sociales, de la famille, de la solidarité et de la ville et du ministère de la santé et des sports et l'arrêté du 7 mai 2009 modifié relatif à l'entretien professionnel des personnels du ministère du travail, des relations sociales, de la famille de la solidarité et de la ville.<br/>
<br/>
              Vu : <br/>
              - les autres pièces du dossier ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - le décret n° 82-453 du 28 mai 1982 ;<br/>
              - le décret n° 2007-1365 du 17 septembre 2007 ;<br/>
              - le décret n° 2011-184 du 15 février 2011 ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yannick Faure, auditeur,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article 55 bis de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat, dans sa rédaction issue de la loi du 3 août 2009 relative à la mobilité et aux parcours professionnels dans la fonction publique : " Au titre des années 2007, 2008, 2009, 2010 et 2011, les administrations de l'Etat peuvent être autorisées, à titre expérimental et par dérogation au premier alinéa des articles 17 du titre Ier du statut général et 55 de la présente loi, à se fonder sur un entretien professionnel pour apprécier la valeur professionnelle des fonctionnaires prise en compte pour l'application des articles 57 et 58 ". Sur ce fondement, le décret du 17 septembre 2007 a fixé les dispositions applicables à l'entretien professionnel, en prévoyant à son article 1er que ces dispositions seraient " rendues applicables aux corps de fonctionnaires de l'Etat soumis au titre II du décret du 29 avril 2002 par un arrêté des ministres dont ils relèvent, pour au moins une année de référence, au titre des années 2007, 2008, 2009, 2010 ou 2011 ". L'arrêté attaqué prolonge d'une année, jusqu'au 31 décembre 2012, l'expérimentation de l'entretien professionnel pour les personnels du ministère du travail, des relations sociales, de la famille, de la solidarité et de la ville et du ministère de la santé et des sports, en prévoyant que, pour l'exercice 2012, l'année 2011 sera l'année de référence pour l'appréciation de la valeur professionnelle des agents.<br/>
<br/>
              2. L'article 34 du décret du 15 février 2011 relatif aux comités techniques dans les administrations et les établissements publics de l'Etat dispose que : " Les comités techniques sont consultés, dans les conditions et les limites précisées pour chaque catégorie de comité (...) sur les questions et projets de textes relatifs: (...) / 3° Aux règles statutaires et aux règles relatives à l'échelonnement indiciaire (...) ". En vertu du même article, les comités techniques bénéficient du concours du comité d'hygiène, peuvent le saisir de toute question et examinent toute question que lui soumet le comité d'hygiène. En vertu du premier alinéa de l'article 47 du décret du 28 mai 1982 relatif à l'hygiène et à la sécurité du travail ainsi qu'à la prévention médicale dans la fonction publique, les comités d'hygiène, de sécurité et des conditions de travail exercent leurs missions " sous réserve des compétences des comités techniques ". Enfin, aux termes du 1° de l'article 57 du même décret, ces comités sont consultés : " Sur les projets d'aménagement importants modifiant les conditions de santé et de sécurité ou les conditions de travail et, notamment, avant toute transformation importante des postes de travail découlant de la modification de l'outillage, d'un changement de produit ou de l'organisation du travail, avant toute modification des cadences et des normes de productivité liées ou non à la rémunération du travail ".<br/>
<br/>
              3. Eu égard à son objet, l'arrêté attaqué ne peut être regardé comme entraînant un aménagement important modifiant les conditions de santé et de sécurité ou les conditions de travail des agents et ne soulève aucune autre question dont les comités d'hygiène, de sécurité et des conditions de travail ont obligatoirement à connaître. <br/>
<br/>
              4. Au surplus, il résulte des dispositions des articles 34 du décret du 15 février 2011 et 47 du décret du 28 mai 1982, citées ci-dessus, qu'une question ou un projet de disposition ne doit être soumis à la consultation du comité d'hygiène, de sécurité et des conditions de travail que si le comité technique ne doit pas lui-même être consulté sur la question ou le projet de disposition en cause. Or les dispositions de l'arrêté attaqué, qui portent sur les modalités d'appréciation de la valeur professionnelle des fonctionnaires prise en compte pour les avancements d'échelon et de grade, ont le caractère de règles statutaires et ont été soumises pour avis aux comités techniques ministériels compétents. <br/>
<br/>
              5. Il résulte de ce qui précède que l'arrêté attaqué n'avait pas à être soumis à la consultation des comités d'hygiène, de sécurité et des conditions de travail placés auprès des ministres concernés. Par suite, la requête du syndicat SUD travail affaires sociales doit être rejetée.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête du syndicat SUD travail affaires sociales est rejetée.<br/>
Article 2 : La présente décision sera notifiée au syndicat SUD travail affaires sociales et à la ministre des affaires sociales, de la santé et des droits des femmes.<br/>
Copie en sera adressée pour information au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social et au ministre de la ville, de la jeunesse et des sports.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
