<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039274914</ID>
<ANCIEN_ID>JG_L_2019_10_000000400758</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/27/49/CETATEXT000039274914.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 24/10/2019, 400758</TITRE>
<DATE_DEC>2019-10-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>400758</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ORTSCHEIDT ; SCP MEIER-BOURDEAU, LECUYER</AVOCATS>
<RAPPORTEUR>M. Sylvain Monteillet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:400758.20191024</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une décision du 26 juillet 2018, le Conseil d'Etat, statuant au contentieux sur le pourvoi de la société coopérative agricole (SCA) COPEBI, tendant à l'annulation de l'arrêt n° 15MA00981 du 18 avril 2016 par lequel la cour administrative d'appel de Marseille a rejeté l'appel formé par cette société contre le jugement n° 1301517 du 20 janvier 2015 du tribunal administratif de Nîmes rejetant sa demande tendant à l'annulation du titre de recettes du 29 mars 2013 émis à son encontre par l'établissement national des produits de l'agriculture et de la mer (FranceAgriMer), en vue du recouvrement d'une somme de 5 042 768,78 euros correspondant au remboursement d'aides publiques qui lui ont été versées entre 1998 et 2002 et des intérêts correspondants, a sursis à statuer jusqu'à ce que la Cour de justice de l'Union européenne se soit prononcée sur la question suivante :<br/>
<br/>
              - la décision de la Commission européenne 2009/402/CE du 28 janvier 2009 concernant les " plans de campagne " dans le secteur des fruits et légumes mis à exécution par la France [C 29/05 (ex NN 57/05)] doit-elle être interprétée en ce sens qu'elle couvre les aides versées par l'office national interprofessionnel des fruits, des légumes et de l'horticulture (ONIFLHOR) au comité économique agricole du bigarreau d'industrie (CEBI) et attribuées aux producteurs de bigarreaux d'industrie par les groupements de producteurs membres de ce comité, alors que le CEBI ne figure pas parmi les huit comités économiques agricoles mentionnés au point 15 de la décision et que les aides en cause, contrairement au mécanisme de financement décrit aux points 24 à 28 de cette décision, étaient financées seulement par des subventions de l'ONIFLHOR et non pas également par des contributions volontaires des producteurs, dites parts professionnelles '<br/>
<br/>
              Par un arrêt C-505/18 du 13 juin 2019, la Cour de justice de l'Union européenne s'est prononcée sur cette question.<br/>
<br/>
              Par un mémoire, enregistré le 22 juillet 2019, FranceAgriMer maintient ses conclusions. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier, y compris celles visées par la décision du Conseil d'Etat du 26 juillet 2018 ;<br/>
<br/>
              Vu :<br/>
              - le traité sur le fonctionnement de l'Union européenne ; <br/>
              - le règlement (CEE) n° 1035/72 du Conseil, du 18 mai 1972 ;<br/>
              - le règlement (CE) n° 2200/96 du Conseil, du 28 octobre 1996 ;<br/>
              - la décision 2009/402/CE de la Commission européenne, du 28 janvier 2009 ;<br/>
              - les arrêts du Tribunal de l'Union européenne du 27 septembre 2012 dans les affaires T-139/09, France/Commission, et T-243/09, Fédération de l'organisation économique fruits et légumes (Fedecom)/Commission ;<br/>
              - l'arrêt de la Cour de justice de l'Union européenne du 12 février 2015 dans l'affaire C-37/14, Commission/France ;<br/>
              - l'arrêt de la Cour de Justice de l'Union européenne du 13 juin 2019 dans l'affaire C-505/18, Copebi/FranceAgriMer ; <br/>
              - le code rural et de la pêche maritime ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Sylvain Monteillet, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Ortscheidt, avocat de la société coopérative agricole (SCA) COPEBI et à la SCP Meier-Bourdeau, Lecuyer, avocat de l'établissement national des produits de l'agriculture et de la mer (FranceAgrimer) ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que l'office national interprofessionnel des fruits, des légumes et de l'horticulture (ONIFLHOR), aux droits duquel vient FranceAgriMer, a mis en place, entre 1998 et 2002, une incitation conjoncturelle à la contractualisation des approvisionnements des industries de transformation des cerises dites bigarreaux à destination industrielle, sous la forme d'une aide financière à chaque campagne concernée. Cette aide était destinée aux groupements de producteurs ayant procédé au titre de la récolte en cause à des livraisons de bigarreaux aux industriels de la transformation dans le cadre de contrats pluriannuels conclus en application d'un accord interprofessionnel. L'aide versée par l'ONIFLHOR transitait par le Comité économique bigarreau industrie (CEBI), qui reversait les fonds à ses adhérents, dont la SCA COPEBI, laquelle a reçu une somme totale de 2 823 708,83 euros. Saisie d'une plainte, la Commission européenne a, par une décision 2009/402/CE du 28 janvier 2009, concernant les " plans de campagne " dans le secteur des fruits et légumes mis à exécution par la France, énoncé que les aides versées au secteur des fruits et légumes français avaient pour but de faciliter l'écoulement des produits français en manipulant le prix de vente ou les quantités offertes sur les marchés, que de telles interventions constituaient des aides d'Etat instituées en méconnaissance du droit de l'Union européenne et prescrit leur récupération. Cette décision a été confirmée par deux arrêts du Tribunal de l'Union européenne du 27 septembre 2012, France/Commission (T-139/09), et Fédération de l'organisation économique fruits et légumes (Fedecom) /Commission (T-243/09). A la suite de ces arrêts, l'administration française a entrepris de récupérer les aides illégalement versées aux producteurs de bigarreaux d'industrie, dont la SCA COPEBI, à l'encontre de laquelle FranceAgriMer a émis, le 29 mars 2013, un titre de recettes en vue du recouvrement d'une somme de 5 042 768,78 euros correspondant au remboursement d'aides publiques versées entre 1998 et 2002 et des intérêts ayant couru. Par un jugement du 20 janvier 2015, le tribunal administratif de Nîmes a rejeté la demande de la société tendant à l'annulation de ce titre de recettes. Par un arrêt du 18 avril 2016, dont la société demande l'annulation, la cour administrative d'appel de Marseille a rejeté l'appel formé par cette société contre ce jugement. Par un arrêt du 26 juillet 2018, le Conseil d'Etat, statuant au contentieux, après avoir répondu à deux des moyens du pourvoi, a sursis à statuer sur les autres moyens dans l'attente de la réponse à la question préjudicielle qu'elle a renvoyée à la Cour de Justice de l'Union européenne sur la portée de la décision 2009/402/CE de la Commission du 28 janvier 28 janvier 2009 concernant les " plans de campagne " dans le secteur des fruits et légumes mis à exécution par la France.<br/>
<br/>
              2. En réponse à cette question, la Cour de justice de l'Union européenne a dit pour droit que cette décision de la Commission " doit être interprétée en ce sens qu'elle couvre les aides versées par l'Office national interprofessionnel des fruits, des légumes et de l'horticulture (Oniflhor) au comité économique bigarreau industrie (CEBI), et attribuées aux producteurs de bigarreaux d'industrie par les groupements de producteurs membres de ce comité quand bien même, d'une part, ce comité ne figure pas parmi les huit comités économiques agricoles mentionnés dans cette décision et, d'autre part, ces aides, contrairement au mécanisme de financement décrit dans ladite décision, étaient financées uniquement par des subventions de l'Oniflhor et non pas, également, par des contributions volontaires des producteurs ".<br/>
<br/>
              3. Il résulte de l'interprétation ainsi donnée par la Cour de justice de l'Union européenne que la cour administrative d'appel de Marseille a pu, sans commettre d'erreur de droit, rejeter la requête de la SCA COPEBI en jugeant que les aides aux bigarreaux d'industrie entraient dans le champ d'application de la décision 2009/402/CE de la Commission du 28 janvier 2009 et constituaient des aides déclarées incompatibles avec le marché commun par cette décision. Si la SCA COPEBI soutient que la cour a commis une erreur de droit en écartant le moyen tiré de ce que la créance de FranceAgriMer était prescrite lorsqu'elle a été mise en recouvrement, c'est en se prévalant seulement de la circonstance que les aides qu'elle a perçues ne relèveraient pas du champ couvert par la décision de la Commission européenne du 28 janvier 2009. Par suite, compte tenu de ce qu'a jugé la Cour de justice de l'Union européenne, les aides dont a bénéficié la SCA COPEBI relèvent de cette décision et ce moyen doit être écarté. <br/>
<br/>
              4. Il résulte de ce qui précède que la SCA COPEBI n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              5. Dans les circonstances de l'espèce, il y a lieu de mettre à la charge de la SCA COPEBI la somme de 3 000 euros à verser à FranceAgriMer au titre de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de FranceAgriMer qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la SCA COPEBI est rejeté. <br/>
Article 2 : La SCA COPEBI versera à FranceAgriMer une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société coopérative agricole COPEBI, à FranceAgriMer et au ministre de l'agriculture et de l'alimentation. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">15-03-02-01 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. APPLICATION DU DROIT DE L'UNION EUROPÉENNE PAR LE JUGE ADMINISTRATIF FRANÇAIS. RENVOI PRÉJUDICIEL À LA COUR DE JUSTICE. - CHAMP D'APPLICATION - INTERPRÉTATION D'UNE DÉCISION DE LA COMMISSION EUROPÉENNE - INCLUSION [RJ1].
</SCT>
<ANA ID="9A"> 15-03-02-01 Le juge administratif peut renvoyer à la Cour de justice de l'Union européenne une question préjudicielle portant sur l'interprétation d'une décision de la Commission européenne.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 26 juillet 2018, Société Copebi, n° 400758, inédite. Rappr. CJUE, 13 juin 2019, COPEBI SCA contre Établissement national des produits de l'agriculture et de la mer (FranceAgriMer), aff. C-505/18.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
