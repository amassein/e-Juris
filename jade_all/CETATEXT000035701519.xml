<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035701519</ID>
<ANCIEN_ID>JG_L_2017_09_000000412027</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/70/15/CETATEXT000035701519.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 29/09/2017, 412027, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-09-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412027</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Emmanuelle Petitdemange</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:412027.20170929</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un mémoire et deux nouveaux mémoires, enregistrés les 30 juin, 10 août et 8 septembre 2017 au secrétariat du contentieux du Conseil d'Etat, M. et Mme B...demandent au Conseil d'État, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 et à l'appui de leur requête tendant à l'annulation pour excès de pouvoir des paragraphes nos 60, 70 et 210 des commentaires administratifs publiés le 8 mars 2017 au Bulletin officiel des finances publiques (Bofip) sous la référence BOI-PAT-ISF-30-20-30, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution de l'article 885 G ter et des alinéas 4 et 5 du III de l'article 990 J du code général des impôts.  	 <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
- la Constitution, notamment son Préambule et son article 61-1 ;<br/>
- l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
- le code général des impôts, notamment ses articles 885 G ter et 990 J ; <br/>
- la loi n° 2911-900  du 29 juillet 2011 ;  <br/>
- le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Emmanuelle Petitdemange, auditeur,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1.1.  Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé, y compris pour la première fois en cassation, à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              Sur la question prioritaire de constitutionnalité relative à l'article 885 G ter :<br/>
<br/>
              2. Aux termes du premier alinéa de l'article 885 G ter du code général des impôts, dans sa rédaction issue du V de l'article 14 de la loi du 29 juillet 2011 portant loi de finances rectificative pour 2011 : " Les biens ou droits placés dans un trust défini à l'article 792-0 bis ainsi que les produits qui y sont capitalisés sont compris, pour leur valeur vénale nette au 1er janvier de l'année d'imposition, selon le cas, dans le patrimoine du constituant ou dans celui du bénéficiaire qui est réputé être un constituant en application du II du même article 792-0 bis. (...) ". Il résulte de ces dispositions que le constituant d'un trust, ou le bénéficiaire réputé constituant, est, sous réserve que la valeur nette de l'ensemble de son patrimoine excède le seuil d'assujettissement à l'impôt de solidarité sur la fortune, redevable de cet impôt à raison des biens ou droits placés dans ce trust ainsi que des produits qui y sont capitalisés, lesquels sont, pour l'établissement de cet impôt, rattachés à son patrimoine en application de la loi, sans considération des caractéristiques particulières de l'acte en vertu duquel le trust est constitué et des droits que cet acte confère au redevable sur les actifs en cause. <br/>
<br/>
              3. L'article 885 G ter du code général des impôts est applicable au présent litige au sens et pour l'application de l'article 23-5 de l'ordonnance du 7 novembre 1958. Cette disposition n'a pas déjà été déclarée conforme à la Constitution par le Conseil constitutionnel. Soulève par ailleurs une question présentant un caractère sérieux le grief de M. et Mme B...tiré de ce qu'elle porte atteinte aux droits et libertés garantis par la Constitution, et notamment au principe d'égalité devant les charges publiques issu de l'article 13 de la Déclaration de 1789 dans la mesure où elle prévoit le rattachement systématique des biens et droits placés dans un trust au patrimoine du constituant, ou du bénéficiaire réputé constituant, alors que ces biens ou droits, ainsi que les revenus qu'ils procurent, sont susceptibles, selon les modalités de constitution retenues, de ne conférer aucune capacité contributive au redevable ainsi désigné. Toutefois, par sa décision n° 412031 du 25 septembre 2017, le Conseil d'Etat, statuant au contentieux, a renvoyé au Conseil constitutionnel la même question prioritaire de constitutionnalité, posée par M.A.... Par suite, en application de l'article R. 771-18 du code de justice administrative, il n'y a pas lieu de statuer sur la question prioritaire de constitutionnalité soulevée par M. et Mme B....<br/>
<br/>
              Sur la question prioritaire de constitutionnalité relative aux alinéas 4 et 5 du III de l'article 990 J : <br/>
<br/>
              4. Aux termes de l'article 990 J du code général des impôts, dans sa rédaction applicable aux commentaires administratifs contestés par le requérant : " I. - Les personnes physiques constituants ou bénéficiaires d'un trust défini à l'article 792-0 bis sont soumises à un prélèvement fixé au tarif le plus élevé mentionné au 1 de l'article 885 U. (...) III. - (...) Toutefois, le prélèvement n'est pas dû à raison des biens, droits et produits capitalisés lorsqu'ils ont été : a. Inclus dans le patrimoine, selon le cas, du constituant ou d'un bénéficiaire pour l'application de l'article 885 G ter et régulièrement déclarés à ce titre par ce contribuable ; (...) ". Il résulte de ces dispositions, éclairées par les travaux préparatoires de l'article 14 de la loi du 29 juillet 2011 portant loi de finances rectificative pour 2011 duquel elles sont issues, que, pour l'application du a. précité, le prélèvement se substitue à l'impôt de solidarité sur la fortune en l'absence de déclaration régulière à ce titre sans se cumuler avec lui. Il en résulte que dans le cas où un contribuable omet de déclarer régulièrement des biens, droits et produits placés dans un trust qui doivent être inclus dans son patrimoine, en vertu de l'article 885 G ter du code général des impôts, pour l'assujettissement à l'impôt de solidarité sur la fortune, ces biens, droits et produits capitalisés sont pris en compte pour déterminer si ce contribuable, eu égard à la valeur nette de son patrimoine, entre dans le champ de l'impôt de solidarité sur la fortune mais sont soumis, si tel est le cas, au seul prélèvement prévu au I de l'article 990 J précité du code général des impôts sans être pris en compte pour le calcul de la cotisation d'impôt de solidarité sur la fortune due par le contribuable. <br/>
<br/>
              5. M. et Mme B...soutiennent que les dispositions des alinéas 4 et 5 du III de l'article 990 J du code général des impôts, en tant qu'elles n'excluent pas, en cas d'absence de déclaration régulière au titre de l'impôt de solidarité sur la fortune de biens ou droits placés dans un trust,  la possibilité d'une application cumulée, à raison de ces biens ou droits, de l'impôt de solidarité sur la fortune et du prélèvement prévu à l'article 990 J du code général des impôts seraient contraires aux principes de proportionnalité des peines et de respect des facultés contributives, respectivement issus des articles 8 et 13 de la Déclaration de 1789. Il résulte toutefois de ce qui a été dit au point 4 que ces impositions sont alternatives et ne peuvent se cumuler. Par suite, la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux. Dès lors, il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de statuer sur la question prioritaire de constitutionnalité soulevée par M. et Mme B...relative à l'article 885 G ter du code général des impôts.<br/>
Article 2 : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M. et Mme B...relative aux alinéas 4 et 5 du III de l'article 990 J du code général des impôts.<br/>
Article 3 : La présente ordonnance sera notifiée à M. et Mme B...et au ministre de l'action et des comptes publics.<br/>
Copie en sera adressée au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
