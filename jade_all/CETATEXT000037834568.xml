<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037834568</ID>
<ANCIEN_ID>JG_L_2018_12_000000404059</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/83/45/CETATEXT000037834568.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 19/12/2018, 404059, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-12-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>404059</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Sophie Baron</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:404059.20181219</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une ordonnance n° 1505275 du 3 octobre 2016, enregistrée le 5 octobre 2016 au secrétariat du contentieux du Conseil d'Etat, le président du tribunal administratif de Marseille a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête présentée à ce tribunal par M. D...A....<br/>
<br/>
              Par cette requête, enregistrée au greffe du tribunal administratif de Marseille le 10 juillet 2015 et par un mémoire complémentaire, un mémoire en réplique et un nouveau mémoire, enregistrés au secrétariat du contentieux du Conseil d'Etat les 18 novembre 2016, 20 novembre 2017 et 30 juillet 2018, M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la délibération du 5 juin 2015 du conseil d'administration d'Aix-Marseille Université statuant sur la liste des candidats au poste n° 324 de professeur des universités dressée par le comité de sélection, la décision de rejet implicite du recours gracieux qu'il a formé le 2 avril 2015 auprès du président de l'université ainsi que le décret du 18 décembre 2015 en tant qu'il nomme M. G...B...professeur des universités à l'université d'Aix-Marseille (institut universitaire de technologie d'Aix-Marseille) ;  <br/>
<br/>
              2°) d'enjoindre au comité de sélection de statuer à nouveau sur les candidatures au poste de professeur des universités n° 324 dans un délai de trois mois à compter de la notification de la décision du Conseil d'Etat ;<br/>
<br/>
              3°) de mettre à la charge d'Aix-Marseille Université la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'éducation ;<br/>
              - le décret n° 84-430 du 6 juin 1984 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie Baron, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que M.A..., maître de conférences à Aix-Marseille Université, demande l'annulation pour excès de pouvoir de la délibération du 5 juin 2015 par laquelle le conseil d'administration de l'université Aix-Marseille Université a approuvé la liste de candidats proposée par le comité de sélection constitué dans le cadre des épreuves du concours ouvert en 2015 pour pourvoir le poste n° 324 de professeur dans cette université ; qu'il demande également l'annulation, par voie de conséquence, du refus implicite opposé par le président de l'université à son recours gracieux formé contre cette délibération ainsi que du décret du Président de la République du 18 décembre 2015 en tant qu'il nomme M. B...sur le poste n° 324 ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 952-6-1 du code de l'éducation : " (...) lorsqu'un emploi d'enseignant-chercheur est créé ou déclaré vacant, les candidatures des personnes dont la qualification est reconnue par l'instance nationale prévue à l'article L. 952-6 sont soumises à l'examen d'un comité de sélection créé par délibération du conseil académique ou, pour les établissements qui n'en disposent pas, du conseil d'administration, siégeant en formation restreinte aux représentants élus des enseignants-chercheurs, des chercheurs et des personnels assimilés. / Le comité est composé d'enseignants-chercheurs et de personnels assimilés, pour moitié au moins extérieurs à l'établissement, d'un rang au moins égal à celui postulé par l'intéressé. (...) / Au vu de son avis motivé, le conseil académique ou, pour les établissements qui n'en disposent pas, le conseil d'administration, siégeant en formation restreinte aux enseignants-chercheurs et personnels assimilés de rang au moins égal à celui postulé, transmet au ministre compétent le nom du candidat dont il propose la nomination ou une liste de candidats classés par ordre de préférence. (...) " ; qu'aux termes de l'article 9-2 du décret du 6 juin 1984 fixant les dispositions statutaires communes applicables aux enseignants-chercheurs et portant statut particulier du corps des professeurs des universités et du corps des maîtres de conférences : " Le comité de sélection examine les dossiers des maîtres de conférences ou professeurs postulant à la nomination dans l'emploi par mutation et des candidats à cette nomination par détachement et par recrutement au concours parmi les personnes inscrites sur la liste de qualification aux fonctions, selon le cas, de maître de conférences ou de professeur des universités. Au vu de rapports pour chaque candidat présentés par deux de ses membres, le comité établit la liste des candidats qu'il souhaite entendre. Les motifs pour lesquels leur candidature n'a pas été retenue sont communiqués aux candidats qui en font la demande. (...) / Après avoir procédé aux auditions, le comité de sélection délibère sur les candidatures et, par un avis motivé unique portant sur l'ensemble des candidats, arrête la liste, classée par ordre de préférence, de ceux qu'il retient. (...)/ Le comité de sélection émet un avis motivé unique portant sur l'ensemble des candidats ainsi qu'un avis motivé sur chaque candidature. Ces deux avis sont communiqués aux candidats sur leur demande. (...)/Au vu de l'avis motivé émis par le comité de sélection, le conseil académique ou l'organe compétent pour exercer les attributions mentionnées au IV de l'article L. 712-6-1, siégeant en formation restreinte aux enseignants-chercheurs et personnels assimilés de rang au moins égal à celui postulé, propose le nom du candidat sélectionné ou, le cas échéant, une liste de candidats classés par ordre de préférence. Il ne peut proposer que les candidats retenus par le comité de sélection. En aucun cas, il ne peut modifier l'ordre de la liste de classement. /Le conseil d'administration, siégeant en formation restreinte aux enseignants-chercheurs et personnels assimilés de rang au moins égal à celui postulé, prend connaissance du nom du candidat sélectionné ou, le cas échéant, de la liste des candidats proposée par le conseil académique ou l'organe compétent pour exercer les attributions mentionnées au IV de l'article L. 712-6-1. /Sauf dans le cas où le conseil d'administration émet un avis défavorable motivé, le président ou directeur de l'établissement communique au ministre chargé de l'enseignement supérieur le nom du candidat sélectionné ou, le cas échéant, une liste de candidats classés par ordre de préférence. En aucun cas, il ne peut modifier l'ordre de la liste de classement " ; <br/>
<br/>
              3. Considérant, en premier lieu, qu'il ressort des pièces du dossier que le moyen tiré de ce que le comité de sélection aurait statué sur la candidature de M. A...sans disposer des rapports établis par deux de ses membres manque en fait ; <br/>
<br/>
              4. Considérant, en deuxième lieu, que M. A...ne peut utilement invoquer, au soutien de ses conclusions, la circonstance que les écritures en défense de l'université devant le Conseil d'Etat ne respectent pas les exigences de motivation posées par les dispositions citées ci-dessus de l'article 9-2 du décret du 6 juin 1984 fixant les dispositions statutaires communes applicables aux enseignants-chercheurs et portant statut particulier du corps des professeurs des universités et du corps des maîtres de conférences ;<br/>
<br/>
              5. Considérant, en troisième lieu, que la seule circonstance qu'un membre du jury d'un concours connaisse un candidat ne suffit pas à justifier qu'il s'abstienne de participer aux délibérations de ce concours ; qu'en revanche, le respect du principe d'impartialité exige que, lorsqu'un membre du jury d'un concours a avec l'un des candidats des liens, tenant à la vie personnelle ou aux activités professionnelles, qui seraient de nature à influer sur son appréciation, ce membre doit s'abstenir de participer aux interrogations et aux délibérations concernant non seulement ce candidat mais encore l'ensemble des candidats au concours ;<br/>
<br/>
              6. Mais considérant que si M. A...indique qu'il s'est opposé à M. F..., membre du comité de sélection et directeur de l'institut dans lequel se trouve le laboratoire au sein duquel il exerce, notamment lors d'une réunion de cet institut organisée en 2013, cette circonstance n'est pas, par elle-même, de nature à entacher d'irrégularité la participation de M. F...au comité de sélection ; que si M. A...fait valoir que le candidat classé premier par le comité de sélection a co-signé plusieurs articles avec M. Pasquinelli, président du comité de sélection, et avec M.C..., membre du comité de sélection, il n'apporte aucun élément permettant de déterminer l'importance de ces publications au regard de l'ensemble de la production scientifique du candidat ; qu'il ne ressort pas des pièces du dossier qu'il aurait existé, entre certains candidats et des membres du comité de sélection, une collaboration scientifique dont l'étroitesse aurait fait obstacle à ce que ces membres participent régulièrement au comité de sélection ; que, par ailleurs, il ne ressort pas davantage des pièces du dossier que M.E..., membre du comité de sélection, serait placé dans une relation de subordination hiérarchique à l'égard du candidat classé premier ; que le moyen tiré de ce que le comité de sélection aurait été irrégulièrement composé doit, par suite, être écarté ;<br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que M. A...n'est pas fondé à demander l'annulation de la délibération qu'il attaque ; qu'il n'est, par suite, pas fondé à demander l'annulation, par voie de conséquence, des autres décisions qu'il attaque ; que, sans qu'il soit besoin d'examiner la fin de non-recevoir soulevée par la ministre de l'enseignement supérieur, de la recherche et de l'innovation, sa requête doit être rejetée, y compris ses conclusions à fins d'injonction et ses conclusions présentées au titre de l'article L.761-1 du code de justice administrative ; qu'enfin il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par l'université Aix-Marseille Université au titre  de  l'article  L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
Article 1er : La requête de M. A...est rejetée. <br/>
<br/>
Article 2 : Les conclusions de l'université Aix-Marseille Université présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. D...A..., à l'université Aix-Marseille Université, à M. G...B..., au Premier ministre et à la ministre de l'enseignement supérieur, de la recherche et de l'innovation.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
