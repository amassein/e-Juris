<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035277084</ID>
<ANCIEN_ID>JG_L_2017_07_000000411511</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/27/70/CETATEXT000035277084.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 21/07/2017, 411511, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-07-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411511</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2017:411511.20170721</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête enregistrée le 14 juin 2017 au secrétariat du contentieux du Conseil d'Etat, la Confédération générale du travail demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution de l'article 3 du décret n° 2016-1359 du 11 octobre 2016 ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
<br/>
              La Confédération générale du travail soutient que :<br/>
              - la condition d'urgence est remplie, dès lors que, d'une part, les suffrages d'un très grand nombre de personnels relevant de la section " encadrement " ne seront pas pris en compte pour la détermination de la composition de cette section, alors qu'ils en sont justiciables et, d'autre part, les effets du décret attaqué sont à la fois imminents et susceptibles de se manifester avant l'intervention de la décision du Conseil d'Etat statuant au fond, puisque l'arrêté portant attribution des sièges de conseillers prud'hommes a d'ores et déjà été adopté le 5 mai 2017 et prévoit une période de dépôt des candidatures jusqu'au 31 juillet 2017 ;<br/>
              - il existe un doute sérieux quant à la légalité du décret contesté ; <br/>
              - il est entaché d'une erreur de droit en ce qu'il méconnaît les objectifs de l'ordonnance du 31 mars 2016, dès lors que la corrélation entre, d'une part, l'appartenance syndicale exprimée lors des élections professionnelles par les salariés relevant, en cas de litige, de la section " encadrement " du conseil de prud'hommes et, d'autre part, la représentation des syndicats et organisations professionnelles au sein de la section " encadrement " n'est pas assurée ;<br/>
              - il est entaché d'une erreur manifeste d'appréciation en ce qu'il n'a pas pris en compte les résultats aux élections professionnelles des collèges comprenant exclusivement des ingénieurs, des cadres et des agents de maîtrise pour la répartition des sièges de conseillers de prud'hommes de la section " encadrement " ;<br/>
              - il méconnaît le principe d'égalité, dès lors qu'il institue une différence de traitement dans la prise en compte des suffrages exprimés, pour la détermination de la composition de la section " encadrement ", par les salariés dont le régime de retraite complémentaire relève de l'Association générale des institutions de retraite des cadres, en fonction du nombre de salariés de l'entreprise qui les emploie, sans qu'elle soit justifiée par une différence de situation en rapport avec l'objet de la norme ou par un motif d'intérêt général ;<br/>
              - il est entaché d'erreur de droit en ce qu'il fait application des dispositions de l'article L. 1423-1-2 du code du travail, qui n'entre en vigueur que le 1er janvier 2018 ;<br/>
              - le pouvoir réglementaire n'a pas épuisé sa compétence au regard des exigences de l'article L. 1441-4 du même code en ce que le décret se borne à indiquer que sont " pris en compte " les suffrages exprimés aux élections professionnelles dans les collèges mentionnés, sans en spécifier les modalités.<br/>
<br/>
              Par un mémoire en défense enregistré le 3 juillet 2017, la ministre du travail conclut au rejet de la requête. Elle soutient que la condition d'urgence n'est pas remplie et qu'aucun des moyens soulevés par la confédération requérante n'est de nature à créer un doute sérieux quant à la légalité du décret contesté.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - l'ordonnance n° 2016-388 du 31 mars 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, la Confédération générale du travail, d'autre part, le Premier ministre et la ministre du travail ;<br/>
<br/>
              Vu le procès-verbal de l'audience publique du lundi 10 juillet 2017 à 9 heures au cours de laquelle ont été entendus :<br/>
<br/>
              - Me Pinet, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la Confédération générale du travail ;<br/>
<br/>
              - les représentants de la Confédération générale du travail ; <br/>
<br/>
              - les représentants de la ministre du travail ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a différé la clôture de  l'instruction jusqu'au lundi 17 juillet à 9 heures ;<br/>
<br/>
              Vu le mémoire complémentaire enregistré le 13 juillet 2017 présenté par la Confédération générale du travail qui persiste dans ses écritures ;<br/>
<br/>
              Vu le mémoire complémentaire enregistré le 13 juillet 2017, présenté par la ministre du travail qui persiste dans ses écritures ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ;<br/>
<br/>
              2. Considérant que, par l'ordonnance du 31 mars 2016 relative à la désignation des conseillers prud'hommes, le gouvernement a mis en place de nouvelles modalités de désignation des conseillers prud'hommes  qui entreront en fonction après le 31 décembre 2017, en remplaçant le mode de désignation électif par une désignation des conseillers prud'hommes fondée sur les résultats de l'audience syndicale et patronale; qu'aux termes de l'article L. 1441-4 du code du travail dans sa rédaction issue de cette ordonnance : " Le garde des sceaux, ministre de la justice, et le ministre chargé du travail arrêtent le nombre de sièges attribués pour la durée du mandat aux organisations syndicales et professionnelles par conseil de prud'hommes, collège et section, en fonction du nombre de conseillers défini à l'article L. 1423-2 et, pour les organisations syndicales de salariés, des suffrages obtenus au niveau départemental par chaque organisation dans le cadre de la mesure de l'audience définie au 5° de l'article L. 2121-1. " ; que par la présente requête, la Confédération générale du travail demande au juge des référés du Conseil d'Etat de suspendre l'exécution de l'article 3 du décret du 11 octobre 2016 relatif à la désignation des conseillers prud'hommes en tant qu'il définit pour les salariés le collège électoral de la section " encadrement " ; <br/>
<br/>
              3. Considérant, en premier lieu, que la confédération requérante soutient que le décret contesté méconnaît les objectifs de l'ordonnance du 31 mars 2016 en ce qu'il ne permet pas d'assurer une corrélation exacte entre, d'une part, l'appartenance syndicale exprimée lors des élections professionnelles par les salariés relevant, en cas de litige, de la section " encadrement " du conseil des prud'hommes et, d'autre part, la représentation des syndicats et organisations professionnelles au sein de la section " encadrement " ; que, toutefois, l'article L. 1423-1-2 du code du travail issu de cette ordonnance définit les catégories de salariés qui relèvent de la section de l'encadrement ; que le dernier alinéa de l'article R. 1441-4 du même code, issu de l'article du décret dont la suspension est demandée, dispose que " Pour la section de l'encadrement, sont pris en compte les suffrages exprimés aux élections professionnelles mentionnées à l'article L. 2122-9 dans les collèges dans lesquels seuls des personnels relevant de la section de l'encadrement définie à l'article L. 1423-1-2 sont amenés à s'exprimer, ainsi que les suffrages exprimés dans le collège " cadres " mentionné à l'article L. 2122-10-4 " ; qu'en renvoyant à l'article L. 1423-1-2 du code du travail, le décret attaqué ne peut être regardé comme ayant adopté un mécanisme n'assurant pas, ainsi que le soutient la confédération requérante, que le suffrage exprimé par tout salarié relevant, en cas de contentieux, de la section encadrement du conseil de prud'hommes soit pris en compte pour la détermination de la composition de la dite section ; que l'effet dénoncé par la confédération requérante est imputable exclusivement à la composition des collèges électoraux définis dans le cadre de la mesure de l'audience syndicale, composition que le décret n'avait ni pour objet ni pour effet de modifier ; que, par suite, le moyen tiré de ce que le décret méconnaîtrait les objectifs de l'ordonnance du 31 mars 2016 n'est pas de nature à créer, en l'état de l'instruction, un doute sérieux sur sa légalité ;<br/>
<br/>
              4. Considérant, en deuxième lieu, que la confédération requérante soutient que le décret contesté est entaché d'une erreur manifeste d'appréciation en ce que la méthode retenue pour définir le corps électoral de la section " encadrement " ne permet pas de prendre en compte un nombre d'électeurs le plus proche possible du nombre de salariés relevant en cas de litige de la section de l'encadrement ; qu'elle fait valoir que le choix fait par le pouvoir réglementaire en ne prenant en compte, pour définir ce corps électoral, que les suffrages exprimés aux élections professionnelles d'une part dans les collèges dans lesquels seuls des personnels relevant de la section de l'encadrement sont amenés à s'exprimer et d'autre part dans le collège " cadres " mentionné à l'article L. 2122-10-4 du code du travail, a pour effet d'exclure un nombre substantiel de salariés relevant pourtant de la section " encadrement " et qu'il aurait été plus pertinent d'adopter le principe d'une répartition sur la base des résultats aux élections professionnelles obtenus également dans les collèges, définis par voie conventionnelle,  comprenant exclusivement des ingénieurs, des cadres et des agents de maîtrise ; que cependant, ainsi qu'il a été dit au point 3, la confédération requérante met ainsi en cause la composition des collèges électoraux définis pour la mesure de l'audience syndicale ; qu'ainsi également qu'il a été dit au point précédent, le décret n'avait ni pour objet ni pour effet - et n'aurait d'ailleurs pas pu légalement - de modifier la composition de ces collèges ; que par suite, le moyen tiré de ce que le décret serait entaché d'une erreur manifeste d'appréciation n'est pas non plus de nature à créer, en l'état de l'instruction, un doute sérieux sur la légalité du décret ;<br/>
<br/>
              5. Considérant, enfin, que les autres moyens, tirés de la méconnaissance du principe d'égalité, de l'erreur de droit tenant à l'application d'une législation qui n'était pas encore entrée en vigueur et de l'incompétence négative dont serait entaché le décret, ne sont pas davantage de nature, en l'état de l'instruction, à créer un doute sérieux sur la légalité du décret contesté ;<br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que, sans qu'il soit besoin de se prononcer sur la condition d'urgence, les conclusions tendant à ce que soit ordonnée en référé, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de l'article 3 du décret ne peuvent qu'être rejetées ;<br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mis à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance, au titre des frais exposés et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de la Confédération générale du travail est rejetée. <br/>
Article 2 : La présente ordonnance sera notifiée à la Confédération générale du travail, au Premier ministre et à la ministre du travail. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
