<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043926998</ID>
<ANCIEN_ID>JG_L_2021_08_000000444563</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/92/69/CETATEXT000043926998.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 06/08/2021, 444563, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-08-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>444563</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. François Charmont</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:444563.20210806</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Clermont-Ferrand d'annuler la décision référencée " 48 M " du 1er juin 2019 par laquelle le ministre de l'intérieur l'a informé d'un retrait de trois points de son permis de conduire à la suite d'une infraction commise le 30 juillet 2018, ainsi que le rejet de son recours gracieux du 3 juillet 2019. Par un jugement n° 1901820 du 9 juillet 2020, le tribunal administratif de Clermont-Ferrand a annulé ces décisions et enjoint au ministre de l'intérieur de restituer à M. B... trois points sur son permis de conduire.<br/>
<br/>
              Par un pourvoi, enregistré le 17 septembre 2020 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de M. B....<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
<br/>
              Vu : <br/>
              - le code de la route ;<br/>
              - le code de procédure pénale ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Charmont, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteure publique.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Le ministre de l'intérieur demande l'annulation du jugement du 9 juillet 2020 par lequel le tribunal administratif de Clermont-Ferrand a annulé sa décision référencée " 48 M " du 1er juin 2019 par laquelle il a informé M. B... d'un retrait de trois points de son permis de conduire à la suite d'une infraction commise le 30 juillet 2018, a annulé le rejet du recours gracieux de l'intéressé contre cette décision et lui a enjoint de restituer trois points sur le permis de conduire de M. B....<br/>
<br/>
              2. Les dispositions portant application des articles R. 49-1 et R. 49-10 du code de procédure pénale en vigueur à la date des infractions litigieuses, notamment celles des articles A. 37-15 à A. 37-18 de ce code issues de l'arrêté du 13 mai 2011 relatif aux formulaires utilisés pour la constatation et le paiement des contraventions soumises à la procédure de l'amende forfaitaire, prévoient que lorsqu'une contravention soumise à cette procédure est constatée par un procès-verbal dressé avec un appareil électronique sécurisé, sans que l'amende soit payée immédiatement entre les mains de l'agent verbalisateur, il est adressé au contrevenant un avis de contravention, qui comporte une information suffisante au regard des exigences des articles L. 223-3 et R. 223-3 du code de la route, une notice de paiement qui comprend une carte de paiement et un formulaire de requête en exonération. Dès lors, le titulaire d'un permis de conduire à l'encontre duquel une infraction au code de la route est relevée au moyen d'un appareil électronique sécurisé et dont il est établi, notamment par la mention qui en est faite au système national des permis de conduire, qu'il a payé, à une date postérieure à celle de l'infraction, l'amende forfaitaire correspondant à celle-ci, a nécessairement reçu l'avis de contravention. Eu égard aux mentions dont cet avis est réputé être revêtu, l'administration doit être regardée comme s'étant acquittée envers le titulaire du permis de son obligation de lui délivrer les informations requises préalablement au paiement de l'amende, à moins que l'intéressé, à qui il appartient à cette fin de produire l'avis qu'il a nécessairement reçu, ne démontre s'être vu remettre un avis inexact ou incomplet.<br/>
<br/>
              3. Il résulte de ce qui précède qu'en estimant que le ministre de l'intérieur n'apportait pas la preuve de la délivrance à M. B... des informations prévues par les articles L. 223-3 et R. 223-3 du code de la route, alors qu'il ressortait des pièces du dossier qui lui était soumis, notamment des mentions du relevé d'information intégral, que cette infraction avait donné lieu au paiement différé par l'intéressé de l'amende forfaitaire, le président du tribunal administratif de Clermont-Ferrand a dénaturé ces mêmes pièces.<br/>
<br/>
              4. Il résulte de ce qui précède que le ministre de l'intérieur est fondé à demander l'annulation du jugement qu'il attaque.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 9 juillet 2020 du tribunal administratif de Clermont-Ferrand est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Clermont-Ferrand.<br/>
<br/>
Article 3 : La présente décision sera notifiée au ministre de l'intérieur et à M. A... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
