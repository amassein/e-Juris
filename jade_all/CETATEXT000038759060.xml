<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038759060</ID>
<ANCIEN_ID>JG_L_2019_07_000000420085</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/75/90/CETATEXT000038759060.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 12/07/2019, 420085, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-07-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420085</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MARLANGE, DE LA BURGADE ; CABINET BRIARD</AVOCATS>
<RAPPORTEUR>Mme Laurence Franceschini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:420085.20190712</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'Association de préservation du site d'Octeville-l'Avenel et des communes environnantes (APSOLACE) a demandé au tribunal administratif de Caen d'annuler pour excès de pouvoir l'arrêté du 11 septembre 2014 par lequel la préfète de la Manche a accordé à la société Ferme éolienne d'Octeville-l'Avenel un permis de construire pour un parc de huit éoliennes et un poste de livraison, ainsi que la décision implicite de rejet de son recours gracieux. Par un jugement n° 1500567 du 15 décembre 2015, le tribunal administratif de Caen a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 16NT00514 du 5 mars 2018, la cour administrative d'appel de Nantes a rejeté l'appel formé par l'Association de préservation du site d'Octeville-l'Avenel et des communes environnantes contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 23 avril et 2 juillet 2018 au secrétariat du contentieux du Conseil d'Etat, l'Association de préservation du site d'Octeville-l'Avenel et des communes environnantes demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre solidairement à la charge de l'Etat et de la société Ferme éolienne d'Octeville-l'Avenel la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code de l'environnement ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Laurence Franceschini, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Marlange, de la Burgade, avocat de l'Association de préservation du site d'Octeville-l'Avenel et des communes environnantes, et au Cabinet Briard, avocat de la société Ferme éolienne d'Octeville-l'Avenel ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 24 juin 2019, présentée par l'Association pour la préservation du site d'Octeville-l'Avenel et des communes environnantes (APSOLACE) ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 11 septembre 2014, la préfète de la Manche a accordé à la société Ferme éolienne d'Octeville-l'Avenel un permis de construire pour un parc de 8 éoliennes et un poste de livraison au réseau public d'électricité. Par un jugement du 15 décembre 2015, le tribunal administratif de Caen a rejeté la demande de l'Association de préservation du site d'Octeville-l'Avenel et des communes environnantes (APSOLACE) tendant à l'annulation de cet arrêté. Par un arrêt du 5 mars 2018, contre lequel l'association requérante se pourvoit en cassation, la cour administrative de Nantes a rejeté l'appel formé contre ce jugement.<br/>
<br/>
              2. En premier lieu, aux termes de l'article R. 711-3 du code de justice administrative : " Si le jugement de l'affaire doit intervenir après le prononcé de conclusions du rapporteur public, les parties ou leurs mandataires sont mis en mesure de connaître, avant la tenue de l'audience, le sens de ces conclusions sur l'affaire qui les concerne ". La communication aux parties du sens des conclusions prévue par ces dispositions a pour objet de les mettre en mesure d'apprécier l'opportunité d'assister à l'audience publique, de préparer, le cas échéant, les observations orales qu'elles peuvent y présenter, après les conclusions du rapporteur public, à l'appui de leur argumentation écrite et d'envisager, si elles l'estiment utile, la production, après la séance publique, d'une note en délibéré. En conséquence, les parties ou leurs mandataires doivent être mis en mesure de connaître, dans un délai raisonnable avant l'audience, l'ensemble des éléments du dispositif de la décision que le rapporteur public compte proposer à la formation de jugement d'adopter, à l'exception de la réponse aux conclusions qui revêtent un caractère accessoire, notamment celles qui sont relatives à l'application de l'article L. 761-1 du code de justice administrative. Cette exigence s'impose à peine d'irrégularité de la décision rendue sur les conclusions du rapporteur public. En outre, le rapporteur public qui, après avoir communiqué le sens de ses conclusions, envisage de modifier sa position doit, à peine d'irrégularité de la décision, mettre les parties à même de connaître ce changement. Par ailleurs, il appartient au rapporteur public de préciser, en fonction de l'appréciation qu'il porte sur les caractéristiques de chaque dossier, les raisons qui déterminent la solution qu'appelle, selon lui, le litige, et notamment d'indiquer, lorsqu'il propose le rejet de la requête, s'il se fonde sur un motif de recevabilité ou sur une raison de fond, et de mentionner, lorsqu'il conclut à l'annulation d'une décision, les moyens qu'il propose d'accueillir, la communication de ces informations n'étant toutefois pas prescrite à peine d'irrégularité de la décision. <br/>
<br/>
              3. Pour écarter le moyen, soulevé devant elle par l'association requérante, mettant en cause la régularité du jugement rendu par le tribunal administratif à raison des conditions dans lesquelles avait été indiqué le sens des conclusions du rapporteur public avant l'audience du tribunal, la cour administrative d'appel, après avoir relevé qu'il ressortait du dossier de la procédure, notamment de la copie de la fiche de l'application informatique " Sagace " de suivi de l'instruction que, le 27 novembre 2015, soit cinq jours avant l'audience, figurait, dans cette application informatique, une discordance entre les contenus de la rubrique " sens synthétique des conclusions ", qui portait la mention " rejet au fond ", et la rubrique " sens des conclusions et moyens ou causes retenus ", qui mentionnait le rejet pour " défaut de qualité pour agir ", c'est-à-dire un rejet pour irrecevabilité, a retenu que les deux informations permettaient de savoir que le rapporteur public allait conclure au rejet de la requête et de connaître avec précision le motif justifiant la solution proposée. Elle a, en outre, retenu que l'avocat de l'association requérante, informé du sens des conclusions dès le 27 novembre 2015, n'avait pas pris l'attache du greffe pour relever cette discordance et demander des éclaircissements, que s'il avait fait état de cette discordance lors de l'audience publique, il n'avait pas été pour autant privé de la possibilité de faire valoir ses observations sur le motif d'irrecevabilité retenu par le rapporteur public et qu'il n'avait pas produit de note en délibéré. En écartant ainsi le moyen dont elle était saisie, la cour administrative d'appel n'a pas commis d'erreur de droit. <br/>
<br/>
              4. En deuxième lieu, aux termes de l'article R. 611-7 du code de justice administrative : " Lorsque la décision lui paraît susceptible d'être fondée sur un moyen relevé d'office, le président de la formation de jugement ou le président de la chambre chargée de l'instruction en informe les parties avant la séance de jugement et fixe le délai dans lequel elles peuvent, sans qu'y fasse obstacle la clôture éventuelle de l'instruction, présenter leurs observations sur le moyen communiqué ". Il ressort des pièces du dossier soumis aux juges du fond que la cour ne s'est pas méprise sur la portée des écritures de première instance des parties en retenant que l'irrecevabilité tirée du défaut de qualité de la personne déclarant agir au nom de l'association requérante avait été opposée en défense par la société Ferme éolienne d'Octeville-l'Avenel dans un mémoire enregistré au greffe du tribunal administratif le 21 avril 2015. C'est, dès lors, sans erreur de droit qu'elle a jugé que le tribunal n'avait pas soulevé d'office un moyen en rejetant comme irrecevable la demande de l'association.<br/>
<br/>
              5. En troisième lieu, la cour administrative d'appel a souverainement considéré que l'article 11 des statuts de l'association requérante, selon lequel " le conseil d'administration donne pouvoir au président d'agir au nom de l'association dans ses rapports avec la justice. Il dispose de la capacité d'ester en justice au nom de 1'association. Il est mandaté pour mettre en oeuvre tous les recours devant toutes les juridictions administratives (...) nécessaires à la poursuite des buts de l'association " ne donne au président de l'association qualité pour agir en justice au nom de l'association qu'après avoir été autorisé par le conseil d'administration. La cour n'a pas dénaturé les pièces du dossier en relevant que ni la délibération du conseil d'administration du 3 mai 2013 donnant mandat au président de l'association pour former en son nom un recours contre l'arrêté préfectoral du 28 septembre 2012 portant approbation et mise en révision du schéma régional éolien (SRE) de la région Basse-Normandie, ni aucune autre délibération du conseil d'administration n'avaient donné mandat au président pour former un recours contentieux contre le permis de construire contesté du 11 septembre 2014, avant que le tribunal n'ait statué. La cour n'a, par suite, pas commis d'erreur de qualification juridique en jugeant, sans insuffisance de motivation, que la demande de première instance était irrecevable.<br/>
<br/>
              6. Il résulte de tout ce qui précède que l'association requérante n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque. <br/>
<br/>
              7. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat et de la société Ferme éolienne d'Octeville-l'Avenel, qui ne sont pas, dans la présente instance, les parties perdantes. En revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Association de préservation du site d'Octeville-l'Avenel et des communes environnantes (APSOLACE) la somme de 2 000 euros à verser à la société Ferme éolienne d'Octeville-l'Avenel au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de l'Association de préservation du site d'Octeville-l'Avenel et des communes environnantes (APSOLACE) est rejeté.<br/>
Article 2 : L'Association de préservation du site d'Octeville-l'Avenel et des communes environnantes (APSOLACE) versera à la société Ferme éolienne d'Octeville-l'Avenel une somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : La présente décision sera notifiée à l'Association de préservation du site d'Octeville-l'Avenel et des communes environnantes (APSOLACE), au ministre de la cohésion des territoires et des relations avec les collectivités territoriales à la société ferme éolienne d'Octeville-l'Avenel. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
