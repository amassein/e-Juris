<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041575528</ID>
<ANCIEN_ID>JG_L_2020_02_000000421823</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/57/55/CETATEXT000041575528.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 13/02/2020, 421823, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-02-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421823</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; SCP GADIOU, CHEVALLIER</AVOCATS>
<RAPPORTEUR>M. Géraud Sajust de Bergues</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:421823.20200213</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 27 juin 2018 et 16 janvier 2019 au secrétariat du contentieux du Conseil d'Etat, le syndicat de défense et de promotion des charcuteries corses " Salameria Corsa " demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêté du 20 avril 2018 du ministre de l'agriculture et de l'alimentation et du ministre de l'économie et des finances relatif à l'homologation du cahier des charges concernant la dénomination " Bulagna de l'Ile de Beauté " en vue de la transmission à la Commission européenne d'une demande d'enregistrement en tant qu'indication géographique protégée ;<br/>
<br/>
              2°) d'enjoindre aux autorités françaises, en application des dispositions de l'article L. 911-1 du code de justice administrative, de retirer la décision par laquelle elles auraient transmis à la Commission européenne la demande d'enregistrement de l'indication géographique protégée telle qu'elle avait été délimitée dans le cahier des charges ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le règlement (CE) n° 1151/2012 du Parlement européen et du Conseil du 21 novembre 2012 ;<br/>
              - le code rural et de la pêche maritime ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Géraud Sajust de Bergues, conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gadiou, Chevallier, avocat du syndicat de défense et de promotion des charcuteries corses " Salameria Corsa " et à la SCP Didier, Pinet, avocat de l'Institut national de l'origine et de la qualité ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 10 février 2020, présentée par le syndicat de défense et de promotion des charcuteries corses " Salameria Corsa " ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Le syndicat de défense et de promotion des charcuteries corses " Salameria Corsa " demande l'annulation de l'arrêté du 20 avril 2018 par lequel le ministre de l'agriculture et de l'alimentation et le ministre de l'économie et des finances ont homologué le cahier des charges relatif à la dénomination " Bulagna de l'Ile de Beauté " en vue de la transmission de sa demande d'enregistrement en tant qu'indication géographique protégée par la Commission européenne et ont accordé une protection nationale transitoire aux produits répondant à ce cahier des charges jusqu'à la publication au Journal officiel de l'Union européenne de l'acte d'enregistrement ou de la décision de rejet de la Commission européenne.<br/>
<br/>
              2. En premier lieu, le syndicat requérant soutient qu'en homologuant le cahier des charges de l'indication géographique protégée " Bulagna de l'Ile de Beauté ", alors qu'il existe déjà, pour des produits comparables, les appellations d'origine protégée " Jambon sec de Corse - Prisuttu ", " Coppa de Corse / Coppa de Corse - Coppa di Corsica " et " Lonzo de Corse / Lonzo de Corse - Lonzu ", les auteurs de l'arrêté attaqué ont méconnu les dispositions de l'article 6, paragraphe 4, et de l'article 13 du règlement  du Parlement européen et du Conseil du 21 novembre 2012 relatif aux systèmes de qualité applicables aux produits agricoles et aux denrées alimentaires, ainsi que le Guide du demandeur d'une AOP ou d'une IGP édité par l'INAO en novembre 2017.<br/>
<br/>
              3. Aux termes de l'article 6 du règlement du 21 novembre 2012 : " 1. Les dénominations génériques ne peuvent être enregistrées en tant qu'appellations d'origine protégées ou indications géographiques protégées. / 2. Une dénomination ne peut être enregistrée en tant qu'appellation d'origine ou indication géographique lorsqu'elle entre en conflit avec le nom d'une variété végétale ou d'une race animale et qu'elle est susceptible d'induire le consommateur en erreur quant à la véritable origine du produit. / 3. Une dénomination proposée à l'enregistrement qui est partiellement ou totalement homonyme avec une dénomination déjà inscrite dans le registre établi conformément à l'article 11 ne peut être enregistrée à moins que les conditions d'usages locaux et traditionnels et la présentation de l'homonyme enregistré ultérieurement soient suffisamment distinctes en pratique de la dénomination déjà inscrite au registre, compte étant tenu de la nécessité d'assurer un traitement équitable des producteurs concernés et de ne pas induire le consommateur en erreur. / (...) / 4. Une dénomination proposée à l'enregistrement en tant qu'appellation d'origine ou d'indication géographique n'est pas enregistrée lorsque, compte tenu de la réputation d'une marque, de sa renommée et de la durée de son usage, cet enregistrement est de nature à induire le consommateur en erreur quant à la véritable identité du produit ". Aux termes de l'article 13 du même règlement : " 1. Les dénominations enregistrées sont protégées contre : / (...) / b) toute usurpation, imitation ou évocation, même si l'origine véritable des produits ou des services est indiquée ou si la dénomination protégée est traduite ou accompagnée d'une expression telle que 'genre', 'type', 'méthode', 'façon', 'imitation' ou d'une expression similaire, y compris quand ces produits sont utilisés en tant qu'ingrédients ; / (...) ".<br/>
<br/>
              4. D'une part, il ressort des dispositions précitées de l'article 6, paragraphe 4, du règlement du 21 novembre 2012 que ces dispositions visent l'hypothèse d'une dénomination proposée à l'enregistrement qui porterait atteinte à la réputation d'une marque, compte tenu de la renommée et de la durée de l'usage de celle-ci. Elles ne sauraient donc être utilement invoquées à l'encontre de l'arrêté attaqué, dès lors que les dénominations " Jambon de Corse / Jambon de Corse - Prisuttu ", " Coppa de Corse / Coppa de Corse - Coppa di Corsica " et " Lonzo de Corse / Lonzo de Corse - Lonzu " ont été enregistrées en tant qu'appellations d'origine protégée et non déposées comme marques.<br/>
<br/>
              5. D'autre part, si le syndicat requérant soutient que l'expression " Ile de Beauté " est le synonyme notoire et traditionnel du terme " Corse " et que le produit bénéficiant de l'indication géographique protégée " Bulagna de l'Ile de Beauté " est un produit comparable au jambon, à la coppa et au lonzo bénéficiant, respectivement, des appellations d'origine protégée " Jambon sec de Corse - Prisuttu ", " Coppa de Corse / Coppa de Corse - Coppa di Corsica " et " Lonzo de Corse / Lonzo de Corse - Lonzu ", la différence de produit, l'emploi de termes géographiques différents par l'indication géographique et ces appellations d'origine ainsi que la différence des protections conférées par une appellation d'origine, d'une part, et par une indication géographique, d'autre part, sont de nature à écarter le risque que des consommateurs normalement informés et raisonnablement attentifs et avisés aient, en présence de l'indication géographique contestée, directement à l'esprit, comme image de référence, les marchandises bénéficiant des appellations d'origine protégée déjà enregistrées. Par suite, les requérants ne sont pas fondés à soutenir que l'arrêté attaqué méconnaîtrait les dispositions précitées de l'article 13, paragraphe 1, sous b), du règlement du 21 novembre 2012.<br/>
<br/>
              6. Il résulte de ce qui a été dit au point 5 ci-dessus que le syndicat requérant n'est pas davantage fondé à soutenir, en tout état de cause, que l'arrêté attaqué méconnaîtrait les dispositions du Guide du demandeur d'une AOP ou d'une IGP édité par l'INAO aux termes desquelles " l'enregistrement d'une dénomination en tant qu'AOP ou IGP ne permet plus l'enregistrement d'une dénomination portant sur le même nom géographique dès lors qu'il s'agit de produits similaires ou comparables " (p. 14). Par ailleurs, et en tout état de cause, l'argumentation selon laquelle l'arrêté attaqué méconnaîtrait les dispositions de ce Guide aux termes desquelles " lorsque l'aire géographique proposée recouvre en tout ou partie l'aire géographique de production d'un produit comparable bénéficiant d'une AOP ou d'une IGP, il convient de s'assurer que ce chevauchement n'interfère pas avec les spécificités de chaque produit en lien avec les spécificités de l'aire " (p. 16) est inopérante à l'appui du moyen tiré de ce que la dénomination " Bulagna de l'Ile de Beauté " ne pouvait être retenue compte tenu de l'existence des appellations d'origine protégée " Jambon sec de Corse - Prisuttu ", " Coppa de Corse / Coppa de Corse - Coppa di Corsica " et " Lonzo de Corse / Lonzo de Corse - Lonzu ".<br/>
<br/>
              7. En deuxième lieu, aux termes de l'article 7, paragraphe 1, du règlement du 21 novembre 2012 : " Une appellation d'origine protégée ou une indication géographique protégée respecte un cahier des charges qui comporte au moins les éléments suivants : / a) la dénomination devant être protégée en tant qu'appellation d'origine ou indication géographique telle qu'elle est utilisée dans le commerce ou le langage commun, (...) / (...) ". D'une part, ces dispositions n'imposent pas de reprendre la dénomination sous laquelle le produit concerné était, le cas échéant, connu antérieurement, de sorte que la branche du moyen du syndicat requérant tirée de ce que l'usage de cette dénomination dans le commerce ne serait pas ancien doit être écartée. D'autre part, il ressort des pièces du dossier que la dénomination " Bulagna de l'Ile de Beauté " est employée dans le commerce, de sorte que la branche du moyen du syndicat requérant tirée de ce que l'usage de cette dénomination dans le commerce ne serait pas avéré doit être également écartée.<br/>
<br/>
              8. Il résulte de ce qui a été dit au point 7 ci-dessus, d'une part, que les dispositions du Guide du demandeur d'une AOP ou d'une IGP édité par l'INAO qui exigent un usage ancien ajoutent à la réglementation et que les requérants ne sauraient dès lors utilement s'en prévaloir, d'autre part, que les dispositions de ce Guide aux termes desquelles " Il faut veiller à ne pas recourir au nom d'une entité administrative ou à un nom géographique notoire avec lequel le produit n'a pas de lien spécifique (collectivité territoriale, mais aussi nom d'une AOP ou d'une IGP déjà enregistrée) dans le seul but d'utiliser sa notoriété ou sa réputation " (p. 15), n'ont, en tout état de cause, pas été méconnues par les auteurs de l'arrêté attaqué.<br/>
<br/>
              9. En troisième lieu, aux termes de l'article 5, paragraphe 2, du règlement  du 21 novembre 2012 : " Aux fins du présent règlement, on entend par 'indication géographique protégée' une dénomination qui identifie un produit : a) comme étant originaire d'un lieu déterminé, d'une région ou d'un pays ; / b) dont une qualité déterminée, la réputation ou une autre propriété peut être attribuée essentiellement à son origine géographique ; / c) dont au moins une des étapes de production a lieu dans l'aire géographique délimitée ". Aux termes de l'article 7, paragraphe 1, du même règlement : " Une appellation d'origine protégée ou une indication géographique respecte un cahier des charges qui comporte au moins les éléments suivants : / (...)  f) les éléments établissant : / (...) / ii) le cas échéant, le lien entre une qualité déterminée, la réputation ou une autre caractéristique du produit et l'origine géographique visée à l'article 5, paragraphe 2 ; / (...) ".<br/>
<br/>
              10. Il résulte de ces dispositions que l'homologation d'un cahier des charges d'une indication géographique protégée, qui n'est pas une simple indication de provenance géographique, ne peut légalement intervenir que si ce cahier précise les éléments qui permettent d'attribuer à une origine géographique déterminée une qualité, une réputation ou d'autres caractéristiques particulières du produit qui fait l'objet de l'indication et met en lumière de manière circonstanciée le lien géographique et l'interaction causale entre la zone géographique et la qualité, la réputation ou d'autres caractéristiques du produit. Il découle en outre nécessairement de ces mêmes dispositions qu'elles ne permettent de reconnaître un lien avec une origine géographique que pour une production existante, attestée dans la zone géographique à la date de l'homologation et depuis un temps suffisant pour établir ce lien.<br/>
<br/>
              11. Le cahier des charges homologué par l'arrêté attaqué, qui fait obligation, pour la fabrication de la spécialité de la bulagna portant la dénomination " Bulagna de l'Ile de Beauté ", de recourir à des méthodes telles que la salaison au sel sec, l'utilisation de poivre noir en quantité importante, le fumage au bois de feuillus locaux et le séchage avec apport d'air extérieur à l'intérieur du séchoir, établit dans sa partie 6.1 que ces méthodes reposent sur des savoir-faire traditionnels propres à la Corse favorisés par les conditions climatiques, la situation insulaire, le relief montagneux et la couverture forestière de celle-ci. Il établit, d'autre part, dans sa partie 6.3, le lien entre ces méthodes et les caractéristiques particulières de cette spécialité, telles que décrites dans sa partie 6.2, en particulier sa couleur, son odeur et son goût. Si le syndicat requérant soutient que le fumage était originellement une pratique cantonnée à quelques régions de Corse, il ne conteste pas que cette pratique s'est répandue à l'ensemble de l'île et n'apporte aucun élément de nature à établir qu'elle ne caractérise pas depuis un temps suffisant la production de cette spécialité. Enfin, s'il soutient que la production de la bulagna portant la dénomination " Bulagna de l'Ile de Beauté " revêtirait un caractère industriel et qu'il serait possible de reproduire ailleurs sa méthode de fabrication, ces circonstances ne sont pas déterminantes pour apprécier le lien entre la qualité ou les caractéristiques d'un produit et son origine géographique. Par suite, le moyen du syndicat requérant tiré de ce que l'arrêté attaqué serait entaché d'une erreur d'appréciation en ce que le cahier des charges de la dénomination " Bulagna de l'Ile de Beauté " proposée à l'enregistrement en tant qu'indication géographique protégée n'établirait pas l'existence du lien causal requis entre l'aire géographique et la qualité ou les caractéristiques du produit et qu'il méconnaîtrait les exigences en ce sens des articles 5 et 7 du règlement du 21 novembre 2012 doit être écarté. Il en va de même, en tout état de cause, du moyen tiré de ce que ce cahier des charges méconnaîtrait les exigences similaires énoncées par le Guide du demandeur d'une AOP ou d'une IGP édité par l'INAO.<br/>
<br/>
              12. Il résulte de tout ce qui précède que le syndicat de défense et de promotion des charcuteries corses " Salameria Corsa " n'est pas fondé à demander l'annulation de l'arrêté qu'il attaque. Ses conclusions aux fins d'injonction ainsi que celles qu'il présente au titre de l'article L. 761-1 du code de justice administrative doivent, par voie de conséquence, être rejetées.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête du syndicat de défense et de promotion des charcuteries corses " Salameria Corsa " est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée au syndicat de défense et de promotion des charcuteries corses " Salameria Corsa ", au ministre de l'agriculture et de l'alimentation, au ministre de l'économie et des finances, au cunsorziu di i salamaghji corsi - consortium des salaisonniers corses, ainsi qu'à l'Institut national de l'origine et de la qualité. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
