<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033551436</ID>
<ANCIEN_ID>JG_L_2016_12_000000386973</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/55/14/CETATEXT000033551436.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 07/12/2016, 386973, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-12-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>386973</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LESOURD</AVOCATS>
<RAPPORTEUR>M. Bastien Lignereux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:386973.20161207</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Securicor International Holding SAS a demandé au tribunal administratif de Versailles de prononcer la décharge des cotisations supplémentaires d'impôt sur les sociétés et de contribution additionnelle sur cet impôt auxquelles elle a été assujettie au titre de l'exercice clos en 2004. Par un jugement n° 0905657 du 27 juin 2013, le tribunal administratif de Versailles a partiellement fait droit à cette demande en prononçant la décharge des impositions résultant de la réintégration, dans le plafond de la quote-part pour frais et charges prévu à l'article 216 du code général des impôts, d'une provision pour dépréciation sur titres d'un montant de 29 652 183 euros et rejeté le surplus de sa demande.<br/>
<br/>
              Par un arrêt n° 13VE03185 du 14 octobre 2014, la cour administrative d'appel de Versailles a, sur le recours du ministre de l'économie et des finances, annulé ce jugement en tant qu'il avait prononcé cette décharge, remis ces impositions à la charge de la société Securicor International Holding SAS, devenue la société G4S International Holding SAS, et décidé qu'il n'y avait pas lieu de statuer sur les conclusions incidentes présentées par cette société. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 6 janvier, 7 avril et 8 septembre 2015 au secrétariat du contentieux du Conseil d'Etat, la société G4S International Holding SAS demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bastien Lignereux, auditeur, <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lesourd, avocat de la société G4S International Holding SAS ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article 216 du code général des impôts, dans sa rédaction applicable aux impositions en litige : " I. Les produits nets des participations, ouvrant droit à l'application du régime des sociétés mères et visées à l'article 145, touchés au cours d'un exercice par une société mère, peuvent être retranchés du bénéfice net total de celle-ci, défalcation faite d'une quote-part de frais et charges. / La quote-part de frais et charges visée au premier alinéa est fixée uniformément à 5 % du produit total des participations, crédit d'impôt compris. Cette quote-part ne peut toutefois excéder, pour chaque période d'imposition, le montant total des frais et charges de toute nature exposés par la société participante au cours de la même période. / (...) ".<br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que la société Securicor International Holding SAS, devenue la société G4S International Holding SAS, a perçu, au cours de son exercice clos en 2004, des dividendes de la société Securicor International Holding BV pour un montant de 110 millions d'euros. Ayant opté pour l'application du régime fiscal des sociétés mères prévu aux articles 145 et 216 du code général des impôts, la société a retranché ces dividendes de son résultat fiscal après défalcation d'une quote-part de frais et charges qu'elle a plafonnée au montant de ses charges d'exploitation, soit 90 145 euros. A l'issue d'une vérification de comptabilité, l'administration fiscale a estimé qu'en application des dispositions de l'article 216 de ce code citées au point 1 ci-dessus, le plafond de la quote-part devait être fixé à hauteur de l'ensemble des charges de toute nature comptabilisées par la société et y a notamment réintégré une provision pour dépréciation de titres d'un montant de 29 652 183 euros. Constatant que ce plafond, ainsi arrêté à 30 008 696 euros, excédait le montant forfaitaire de la quote-part fixé à 5 % du produit total des participations, soit 5,5 millions d'euros, l'administration a assujetti la société à des cotisations supplémentaires d'impôt sur les sociétés et de contribution additionnelle sur cet impôt correspondant à ce rehaussement du montant de la quote-part. La société se pourvoit en cassation contre l'arrêt de la cour administrative d'appel de Versailles du 14 octobre 2014 en tant que, par cet arrêt, la cour a fait droit au recours du ministre de l'économie et des finances tendant à l'annulation du jugement du 27 juin 2013 par lequel le tribunal administratif de Versailles avait prononcé la décharge de ces impositions supplémentaires et les a remises à sa charge.<br/>
<br/>
              3. Aux termes de l'article 39 du code général des impôts, dans sa rédaction applicable aux impositions en litige : " 1. Le bénéfice net est établi sous déduction de toutes charges, celles-ci comprenant, sous réserve des dispositions du 5, notamment : / (...) 5° Les provisions constituées en vue de faire face à des pertes ou charges nettement précisées et que des événements en cours rendent probables, à condition qu'elles aient été effectivement constatées dans les écritures de l'exercice. (...) / Par dérogation aux dispositions des premier et seizième alinéas, la provision pour dépréciation qui résulte éventuellement de l'estimation du portefeuille est soumise au régime fiscal des moins-values à long terme défini au 2 du I de l'article 39 quindecies ; si elle devient ultérieurement sans objet, elle est comprise dans les plus-values à long terme de l'exercice, visées au 1 du I de l'article 39 quindecies. (...) ". Aux termes de l'article 39 quindecies de ce code : " I. (...) / 2. L'excédent éventuel des moins-values à long terme ne peut être imputé que sur les plus-values à long terme réalisées au cours des dix exercices suivants. / (...) ".<br/>
<br/>
              4. Pour la détermination du plafond de la quote-part pour frais et charges prévu par les dispositions, citées au point 1 ci-dessus, de l'article 216 du code général des impôts dans sa rédaction applicable aux impositions en litige, il convient de prendre en compte l'ensemble des frais et charges supportés par la société mère qui concourent à la détermination du résultat imposable au taux de droit commun de l'impôt sur les sociétés. Or, il résulte des dispositions du 5° du 1 de l'article 39 de ce code, citées au point 3 ci-dessus, qu'une provision pour dépréciation de titres de portefeuille relève du régime des moins-values à long terme défini au 2 du I de l'article 39 quindecies de ce code et n'est donc pas déductible dans les conditions de droit commun. Il suit de là que la cour a commis une erreur de droit en jugeant que cette provision devait être prise en compte dans le calcul du plafond de la quote-part pour frais et charges.<br/>
<br/>
              5. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que la société G4S International Holding SAS est fondée à demander l'annulation de l'arrêt qu'elle attaque. Il y a lieu, dans les circonstances de l'espère, de mettre à la charge de l'Etat la somme de 3 500 euros à verser à la société G4S International Holding SAS.  <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Versailles du 14 octobre 2014 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Versailles.<br/>
Article 3 : L'Etat versera à la société G4S International Holding SAS une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la société G4S International Holding SAS et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
