<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028105162</ID>
<ANCIEN_ID>JG_L_2013_10_000000372677</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/10/51/CETATEXT000028105162.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 16/10/2013, 372677, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-10-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>372677</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CAPRON, CAPRON</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2013:372677.20131016</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le recours, enregistré le 7 octobre 2013 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre de l'intérieur, qui demande au juge des référés du Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1301638 du 20 septembre 2013 par laquelle le juge des référés du tribunal administratif de Châlons-en-Champagne, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, a enjoint au préfet de la Marne de délivrer à M. B...A...une autorisation provisoire de séjour ;<br/>
<br/>
              2°) de rejeter la demande de M. A...;<br/>
<br/>
<br/>
              il soutient que : <br/>
              - l'appréciation du juge des référés du tribunal administratif de Châlons-en-Champagne est entachée d'une erreur de fait dès lors que M. A...n'a pas séjourné dans le centre de Debrecen et que ses empreintes ont été relevées à Békéscsaba ; <br/>
              - l'ordonnance attaquée est entachée d'une erreur manifeste d'appréciation dès lors que M. A...n'a pas apporté la preuve de son passage au centre de Debrecen et que ses allégations, imprécises et stéréotypées sur les conditions de son séjour, ont servi de fondement à la caractérisation d'une atteinte grave et manifestement illégale au droit de solliciter le statut de réfugié ; <br/>
<br/>
<br/>
              Vu l'ordonnance attaquée ;<br/>
              Vu le mémoire en défense, enregistré le 10 octobre 2013, présenté par M.A..., qui conclut au rejet du recours ; il soutient que les moyens du ministre de l'intérieur ne sont pas fondés ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, le ministre de l'intérieur et, d'autre part, M. A...; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 11 octobre 2013 à 9 heures 30 au cours de laquelle ont été entendus : <br/>
<br/>
              - les représentantes du ministre de l'intérieur ;<br/>
<br/>
              - Me Capron, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. A..., qui invoque un moyen nouveau tiré de ce que la législation récemment adoptée par la Hongrie méconnaît les garanties afférentes au respect du droit d'asile ;<br/>
              et à l'issue de laquelle le juge des référés a prolongé l'instruction jusqu'au 15 octobre 2013 à 12 heures ;<br/>
<br/>
              Vu le nouveau mémoire, enregistré le 14 octobre 2013, présenté par le ministre de l'intérieur qui soutient que la législation hongroise, qui transpose la directive 2013/33/CE du 26 juin 2013, ne méconnaît pas les garanties afférentes au respect du droit d'asile ;<br/>
<br/>
                          Vu les autres pièces du dossier ;<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le règlement (CE) n° 343/2003 du Conseil du 18 février 2003 ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures " ;<br/>
<br/>
              2. Considérant que le droit constitutionnel d'asile, qui a pour corollaire le droit de solliciter la qualité de réfugié, constitue une liberté fondamentale au sens des dispositions de l'article L. 521-2 du code de justice administrative ; qu'il implique que l'étranger qui sollicite la qualité de réfugié soit en principe autorisé à demeurer sur le territoire jusqu'à ce qu'il ait été statué sur sa demande, dans les conditions définies par l'article L. 741-4 du code de l'entrée et du séjour des étrangers et du droit d'asile ; que le 1° de cet article permet de refuser l'admission au séjour en France d'un demandeur d'asile lorsque la demande de d'asile relève de la compétence d'un autre Etat membre de l'Union européenne en application des dispositions du règlement (CE) n° 343/2003 du Conseil du 18 février 2003 établissant les critères et mécanismes de détermination de l'Etat membre responsable de l'examen d'une demande d'asile présentée dans l'un des Etats membres par un ressortissant d'un pays tiers ; que, toutefois, le dernier alinéa du même article prévoit que " les dispositions du présent article ne font pas obstacle au droit souverain de l'Etat d'accorder l'asile à toute personne qui se trouverait néanmoins dans l'un des cas mentionnés aux 1° à 4° " ; que le paragraphe 2 de l'article 3 du règlement du 18 février 2003 prévoit que " chaque État membre peut examiner une demande d'asile qui lui est présentée par un ressortissant d'un pays tiers, même si cet examen ne lui incombe pas en vertu des critères fixés dans le présent règlement. Dans ce cas, cet État devient l'État membre responsable au sens du présent règlement et assume les obligations qui sont liées à cette responsabilité " ;<br/>
<br/>
              3. Considérant qu'il résulte de l'instruction que M. B...A..., de nationalité mauritanienne, qui avait quitté son pays d'origine le 15 février 2013, a été arrêté le 27 mars 2013 à la frontière serbo-hongroise par les services de la police hongroise et a été placé en détention ; que, l'intéressé ayant ensuite quitté la Hongrie et étant entré en France, il s'est présenté le 30 juillet 2013 à la préfecture de la Marne en vue d'être admis au séjour pour déposer une demande d'asile ; que les vérifications auxquelles il a alors été procédé ont révélé que les empreintes de M. A...avaient auparavant été relevées par les autorités hongroises, auprès desquelles il avait déjà déposé une demande d'asile ; que ces autorités ont, le 14 août 2013, accepté de reprendre en charge l'examen de cette demande ; que le préfet de la Marne a en conséquence refusé, par une décision du 22 août 2013, l'admission de M. A...au séjour au titre de l'asile et a décidé sa réadmission en Hongrie ; que, saisi par l'intéressé sur le fondement de l'article L. 521-2 du code de justice administrative, le juge des référés du tribunal administratif de Châlons-en-Champagne a, par l'ordonnance dont le ministre de l'intérieur relève appel, enjoint au préfet de lui délivrer une autorisation provisoire de séjour ;<br/>
<br/>
              4. Considérant que, pour faire droit à la demande de M.A..., le juge des référés a relevé que, depuis son arrestation lors de son arrivée en Hongrie, il avait été incarcéré dans un centre dont il a donné une description précise, tant dans le cadre de la procédure écrite qu'à l'audience, et qu'il a identifié comme étant le camp de Debrecen ; qu'après avoir rappelé que la Hongrie est un Etat membre de l'Union européenne et partie tant à la convention de Genève du 28 juillet 1951 sur le statut des réfugiés qu'à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales,  le premier juge a néanmoins estimé que, compte tenu des conditions dans lesquelles le requérant avait été traité au cours de son incarcération en Hongrie, un risque sérieux existait, en l'espèce, que sa demande d'asile ne soit pas examinée par les autorités hongroises dans des conditions conformes à l'ensemble des garanties exigées par le respect du droit d'asile et que, par suite, le refus d'admettre le requérant au séjour en France au titre de l'asile devait être regardé comme portant une atteinte grave et manifestement illégale à son droit de solliciter le statut de réfugié ;<br/>
<br/>
              5. Considérant que, pour demander au juge des référés du Conseil d'Etat d'annuler cette ordonnance et de rejeter la demande de M.A..., le ministre de l'intérieur soutient que le premier juge s'est fondé sur des allégations mensongères et que la réalité de la détention du requérant à Debrecen n'est pas établie ; que, selon le ministre, les empreintes de l'intéressé ont été relevées au centre de Békéscsaba, qui est plus proche de la frontière serbo-hongroise que Debrecen, et que, compte tenu du parcours de l'intéressé qui, après avoir quitté la Hongrie, est notamment passé par l'Autriche avant d'entrer en France, sa présence à Debrecen, situé au Nord-Est du trajet qu'il est réputé avoir parcouru entre Békéscsaba et la frontière autrichienne, est invraisemblable ; que toutefois, il ressort d'un rapport établi en avril 2012 par le Haut-commissariat des Nations unies pour les réfugiés, mentionné dans les pièces produites par le ministre, notamment des indications fournies par ce rapport sur la répartition des demandeurs d'asile par les autorités hongroises entre les différents centres destinés à traiter leurs demandes et, le cas échéant, à les maintenir en détention, que le transfert à Debrecen d'un demandeur dont les empreintes ont d'abord été relevées à Békéscsaba ne peut être regardé comme dépourvu de vraisemblance ; qu'en l'absence de tout autre élément soumis au juge des référés du Conseil d'Etat par le ministre, à qui il appartenait, le cas échéant, de demander des précisions aux autorités hongroises, l'argumentation qu'il présente en appel n'est pas de nature à remettre en cause l'appréciation portée sur les circonstances de l'espèce par le juge des référés du tribunal administratif de Châlons-en-Champagne ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin de statuer sur l'argumentation invoquée en défense devant le Conseil d'Etat à propos des mesures législatives récemment adoptées par la Hongrie en matière d'asile, ni par conséquent sur celle que l'administration a présentée à la suite du supplément d'instruction auquel il a été procédé à l'issue de l'audience d'appel, le ministre de l'intérieur n'est pas fondé à soutenir que c'est à tort que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Châlons-en-Champagne a fait droit à la demande de M.A... ;<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : Le recours du ministre de l'intérieur est rejeté.<br/>
Article 2 : La présente ordonnance sera notifiée au ministre de l'intérieur et à M. B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
