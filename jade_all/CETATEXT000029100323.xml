<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029100323</ID>
<ANCIEN_ID>JG_L_2014_06_000000354921</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/10/03/CETATEXT000029100323.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 17/06/2014, 354921, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-06-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>354921</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>Mme Marie Grosset</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:354921.20140617</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 16 et 28 décembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour le Collectif santé innovation Sud Réunion, dont le siège est 81 route de la Grande Corniche à Saint-Joseph (97480), M. A...C..., demeurant..., et M. B... D..., demeurant... ; le Collectif santé innovation Sud Réunion et autres demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2011-1598 du 21 novembre 2011 relatif à la création d'un centre hospitalier régional à La Réunion par fusion du centre hospitalier Félix Guyon et du groupe hospitalier Sud-Réunion ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la santé publique ; <br/>
<br/>
              Vu le code du travail ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Grosset, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat du Collectif santé innovation Sud Réunion et autres ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que le décret attaqué crée à compter du 1er janvier 2012, par fusion du centre hospitalier Félix Guyon (CHFG) et du groupe hospitalier Sud-Réunion (GHSR), un établissement public de ressort régional dénommé " centre hospitalier régional de la Réunion ", ayant son siège à Saint-Denis ; <br/>
<br/>
              Sur la régularité de la procédure :<br/>
<br/>
              2.Considérant qu'en vertu de l'article L. 6141-1 du code de la santé publique, le centres hospitaliers " sont créés par décret lorsque leur ressort est national, interrégional ou régional " et, en vertu du premier alinéa de l'article L. 6141-2 du même code : " Les centres hospitaliers qui ont une vocation régionale liée à leur haute spécialisation et qui figurent sur une liste établie par décret sont dénommés centres hospitaliers régionaux ; ils assurent en outre les soins courants à la population proche " ; qu'aux termes de l'article L. 6141-7-1 du même code : " La transformation d'un ou plusieurs établissements publics de santé résultant d'un changement de ressort ou d'une fusion intervient dans des conditions définies par le présent article. (...) En cas de fusion de plusieurs établissements, les décisions nécessaires à la mise en place de l'établissement qui en résultera sont prises conjointement par les directeurs des établissements concernés, après que les conseils de surveillance des ces établissements se soient prononcés en application du 4° de l'article L. 6143-1 (...) " ; qu'aux termes de l'article L. 6143-1 du même code : " Le conseil de surveillance se prononce sur la stratégie et exerce le contrôle permanent de la gestion de l'établissement. Il délibère sur : / (...) 4° (...) tout projet tendant à la fusion avec un ou plusieurs établissements publics de santé (...) " ; qu'enfin, aux termes de l'article R. 6141-14 du même code : " La modification de la liste des centres hospitaliers régionaux fixée par le décret mentionné au premier alinéa de l'article L. 6141-2 intervient après avis du conseil de surveillance, de la commission médicale et du comité technique de l'établissement concerné, de la commission spécialisée de la conférence régionale de la santé et de l'autonomie compétente pour le secteur sanitaire de la région où est situé le siège de cet établissement et du Comité national d'organisation sanitaire et sociale " ;<br/>
<br/>
              En ce qui concerne la consultation des comités techniques d'établissement et des commissions médicales d'établissement :<br/>
<br/>
              3. Considérant que l'organisme dont une disposition législative ou  réglementaire prévoit la consultation avant l'intervention d'une décision doit être mis à même d'exprimer son avis sur l'ensemble des questions soulevées par cette décision ; que, par suite, dans le cas où, après avoir  recueilli son avis, l'autorité compétente pour prendre la décision envisage d'apporter à son projet des modifications qui posent des questions nouvelles, elle doit le consulter à nouveau ;<br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier que les comités techniques et les commissions médicales des deux établissements ont été consultés sur le projet de fusion de ces établissements et de création d'un centre hospitalier régional ; que les garanties introduites postérieurement à ces consultations dans le projet de convention à passer entre le futur centre hospitalier régional et l'université de la Réunion en vue de la constitution d'un centre hospitalier universitaire, afin de préserver le caractère " bipolaire nord-sud " de l'organisation des soins, de l'enseignement et de la recherche, ne peuvent être regardées comme une modification du projet de fusion des établissements et de création d'un centre hospitalier régional soulevant une question nouvelle ; que, par suite, les requérants ne sont pas fondés à soutenir que les comités techniques et les commissions médicales des deux établissements auraient dû être de nouveau consultés ;<br/>
<br/>
              En ce qui concerne la consultation des conseils de surveillance :<br/>
<br/>
              5. Considérant, en premier lieu, que les délibérations des conseils de surveillance des deux établissements des 24 et 25 mai 2011 n'ayant pas fait l'objet d'une annulation contentieuse, les requérants ne sont pas fondés à demander l'annulation du décret attaqué par voie de conséquence de celle de ces délibérations ;<br/>
<br/>
              6. Considérant, en deuxième lieu, qu'en vertu de l'article R. 6143-3 du code de la santé publique, le conseil de surveillance d'un établissement public de santé de ressort communal ou intercommunal comprend notamment " le président du conseil général du département siège de l'établissement principal, ou le représentant qu'il désigne " ; qu'il ressort des pièces du dossier que, compte tenu des incertitudes sur la date de prise d'effet de la désignation d'un nouveau représentant par le président du conseil général de la Réunion, le président du conseil de surveillance du groupe hospitalier Sud-Réunion a invité tant l'ancien que le nouveau représentants du conseil général à exprimer son point de vue, sans prendre part au vote, et que les deux intéressés, après avoir manifesté leur accord avec le projet de fusion à l'occasion du débat sur le projet de convention constitutive d'un centre hospitalier universitaire de la Réunion, ont quitté la séance avant le début du débat sur la fusion des deux centres hospitaliers, qui s'est conclu par un vote favorable, acquis par 13 voix pour et 1 voix contre sur 14 votants ; que, dans ces conditions, si le refus d'admettre le représentant régulièrement désigné par le conseil général à participer au vote entache d'irrégularité la délibération du conseil de surveillance du groupe hospitalier Sud-Réunion, il ressort des pièces du dossier que ce vice n'a pas été susceptible d'exercer, en l'espèce, une influence sur le sens de la décision prise et n'a pas privé les intéressés d'une garantie ; <br/>
<br/>
              7. Considérant, en dernier lieu, que les requérants font valoir que les membres des conseils de surveillance des deux établissements n'ont reçu la version définitive du projet de convention à passer en vue de la constitution d'un centre hospitalier universitaire et du projet de délibération sur la fusion des deux établissements que le jour des délibérations, alors que le projet de fusion et de création du centre hospitalier régional était directement lié à celui de création d'un centre hospitalier universitaire ; que, toutefois, d'une part, le décret attaqué ne porte que sur la création d'un centre hospitalier régional ; que, d'autre part, si la délibération approuvant la création d'un tel centre a été complétée d'une mention relative au maintien d'une organisation bipolaire des soins, au demeurant discutée en séance, il n'en résulte aucunement que les membres des deux conseils de surveillance ne se seraient pas prononcés de façon éclairée sur le projet de fusion ; que, par suite, les requérants ne sont pas fondés à soutenir que les membres des conseils de surveillance des deux groupes hospitaliers n'ont pas été mis en mesure de délibérer valablement pour rendre un avis sur les questions posées par le projet de décret attaqué ;<br/>
<br/>
              En ce qui concerne l'absence de consultation des comités d'hygiène, de sécurité et des conditions de travail :<br/>
<br/>
              8. Considérant qu'aux termes de l'article L. 4111-1 du code du travail : " (...) les dispositions de la présente partie (...) sont également applicables : / (...) 3° aux établissements de santé, sociaux et médico-sociaux mentionnés à l'article 2 de la loi n° 86-33 du 9 janvier 1986 portant dispositions statutaires relatives à la fonction publique hospitalière " ; qu'aux termes de l'article L. 4612-8 du même code : " Le comité d'hygiène, de sécurité et des conditions de travail est consulté avant toute décision d'aménagement important modifiant les conditions de santé et de sécurité ou les conditions de travail et, notamment, avant toute transformation importante des postes de travail découlant de la modification de l'outillage, d'un changement de produit ou de l'organisation du travail, avant toute modification des cadences et des normes de productivité liées ou non à la rémunération du travail " ; que si une opération de fusion d'établissements de santé doit faire l'objet d'une consultation du comité d'hygiène, de sécurité et des conditions de travail lorsqu'elle modifie par elle-même les conditions de travail, notamment en cas de transformation importante des postes de travail, le décret attaqué, qui se borne à créer le nouveau centre hospitalier régional par fusion du centre hospitalier Félix Guyon et du groupe hospitalier Sud-Réunion, à fixer son siège et à transférer les droits et obligations des deux établissements au nouveau centre, ne modifie pas par lui-même les conditions de travail des personnels concernés ; que, par suite, les requérants ne sont pas fondés à soutenir que le décret attaqué devait être précédé de la consultation des comités d'hygiène, de sécurité et des conditions de travail des deux groupes hospitaliers ;<br/>
<br/>
              En ce qui concerne la consultation des communes concernées :<br/>
<br/>
              9. Considérant qu'aux termes du premier alinéa de l'article R. 6141-11 du code de la santé publique : " La transformation d'un ou de plusieurs établissements publics de santé, prévue à l'article L. 6141-7-1, est décidée par arrêté du directeur général de l'agence régionale de santé de la région où est situé le siège de l'établissement qui en est issu, après avis du conseil de surveillance du ou des établissements concernés et de la commune où est situé le siège de l'établissement. Toutefois, elle est décidée par décret lorsqu'elle concerne un établissement public de santé à ressort national, interrégional ou régional " ;<br/>
<br/>
              10. Considérant, en premier lieu, qu'il résulte de ces dispositions qu'en cas de fusion d'établissements publics de santé, seule la commune où est situé le siège de l'établissement issu de la transformation doit être consultée préalablement à la décision de création du nouvel établissement ; que, par suite, le moyen tiré de l'irrégularité du décret faute d'avoir été précédé de la consultation de la commune de Saint-Pierre où le groupe hospitalier Sud-Réunion avait son siège ne peut qu'être écarté ;<br/>
<br/>
              11. Considérant, en second lieu, qu'il ressort des pièces du dossier que le conseil municipal de Saint-Denis a approuvé l'implantation du siège du centre hospitalier régional à Saint-Denis au vu d'un rapport décrivant la constitution de ce centre par fusion du centre hospitalier Félix Guyon et du groupe hospitalier Sud-Réunion ; que, par suite, les requérants ne sont pas fondés à soutenir qu'il ne se serait pas prononcé sur le projet de création du centre hospitalier régional par fusion des deux établissements, mais seulement sur le siège du nouveau centre ;<br/>
<br/>
              En ce qui concerne la consultation du Comité national d'organisation sanitaire et sociale :<br/>
<br/>
              12. Considérant que les requérants font valoir que l'avis du Comité national d'organisation sanitaire et sociale fait référence à un avis de la commune de Saint-Denis, alors que celui-ci n'a été émis que postérieurement ; qu'il ressort, cependant, des pièces du dossier que le Comité national a, en réalité, entendu viser un simple courrier du maire par lequel ce dernier faisait part de son accord sur le projet, ultérieurement confirmé par une délibération du conseil municipal ; que cette erreur matérielle n'entache pas d'irrégularité l'avis émis par le Comité national d'organisation sanitaire et sociale ;<br/>
<br/>
              En ce qui concerne l'absence d'étude d'impact :<br/>
<br/>
              13. Considérant qu'aucune disposition législative ou réglementaire ne faisait obligation de procéder, avant l'adoption du décret attaqué, à une étude d'impact ; que, par suite, les requérants ne sont pas fondés à soutenir qu'il serait illégal faute d'avoir été précédé d'une telle étude ;<br/>
<br/>
              Sur la méconnaissance des exigences des articles L. 6141-7-1 et R. 6141-11 du code de la santé publique et l'existence d'une subdélégation illégale :<br/>
<br/>
              14. Considérant qu'aux termes des quatrième et cinquième alinéas de l'article L. 6141-7-1 du code de la santé publique : " (...) En cas de fusion de plusieurs établissements, les décisions nécessaires à la mise en place  de l'établissement qui en résultera sont prises conjointement par les directeurs des établissements concernés (...) / Le directeur général de l'agence régionale de santé fixe les conditions dans lesquelles les autorisations prévues aux articles L. 5126-7 et L. 6122-1, détenues par le ou les établissements transformés , ainsi que les biens meubles et immeubles de leur domaine public et privé sont transférés au nouvel établissement et atteste des transferts de propriété immobilière en vue de leur publication au fichier immobilier (...) / Le décret ou l'arrêté mentionnés à l'article L. 6141-1 déterminent la date de la transformation et en complètent, en tant que de besoin, les modalités " ; qu'aux termes de l'article R. 6141-11 du même code : " La transformation d'un ou de plusieurs établissements publics de santé, prévue à l'article L. 6141-7-1, est (...) décidée par décret lorsqu'elle concerne un établissement public de santé à ressort national, interrégional ou régional. / La décision définit les modalités de dévolution des éléments de l'actif et du passif et précise la nature des autorisations transférées au nouvel établissement en vertu des dispositions du dernier alinéa de l'article L. 6141-7-1. Elle désigne la collectivité territoriale ou l'établissement public destinataire des legs et donations. Sous réserve des dispositions de l'article L. 6145-10, les legs et donations sont reportés sur cette collectivité ou cet établissement avec la même affectation (...) " ; <br/>
<br/>
              15. Considérant, en premier lieu, qu'il résulte de ces dispositions combinées, et notamment du cinquième alinéa de l'article L. 6141-7-1 du code de la santé publique, que le décret pouvait légalement se borner à prévoir, à son article 1er, que " les droits et obligations du centre hospitalier Félix Guyon et du groupe hospitalier Sud-Réunion, ainsi que leurs biens meubles et immeubles de leur domaine public et privé, sont transférés au centre hospitalier régional de la Réunion " et renvoyer au directeur général de l'agence de santé de l'Océan Indien la fixation des modalités du transfert de ces biens, droits et obligations, parmi lesquels figurent les autorisations d'activités de soins et d'équipements matériels lourds ;<br/>
<br/>
              16. Considérant, en second lieu, qu'aucune disposition n'était nécessaire pour prévoir la dissolution des organes des deux établissements fusionnés, résultant de plein droit de l'entrée en vigueur du décret attaqué le 1er janvier 2012 ; qu'il résulte des dispositions de l'article L. 6141-7-1 du code de la santé publique citées ci-dessus qu'avant même la fusion, les directeurs des établissements concernés ont compétence pour prendre conjointement les décisions nécessaires à la mise en place de l'établissement qui résultera de la fusion ; que, dès lors, les requérants ne sont pas fondés à soutenir que le décret serait illégal, faute de comporter des dispositions afférentes à la dissolution des organes des établissements appelés à fusionner et de préciser l'organisation et le fonctionnement interne du nouvel établissement ;<br/>
<br/>
              Sur l'existence d'une erreur manifeste d'appréciation :<br/>
<br/>
              17. Considérant qu'il ressort des pièces du dossier que la création du centre hospitalier régional de la Réunion vise à améliorer l'accès et la qualité des soins, à permettre le développement sur place d'activités et de prises en charge spécialisées, ainsi que, par la constitution d'un centre hospitalier universitaire, à développer la formation, à attirer des personnels hautement qualifiés et à favoriser la recherche liée aux problèmes de santé spécifiques à la région, tout en  permettant le développement d'une coopération dans la zone de l'Océan Indien ; qu'elle a fait l'objet d'une concertation et s'est accompagnée de garanties relatives au maintien des implantations antérieurement existantes et, conformément aux dispositions de l'article L. 6141-2 du code de la santé publique, aux soins courants assurés à la population proche ; que la circonstance que le protocole d'accord relatif au volet social de la fusion prévoirait un fonctionnement dérogatoire illégal des instances paritaires, à titre transitoire, est sans incidence sur la légalité du décret attaqué ; que si les deux établissements sont distants de près de 85 kilomètres et ont des taux d'endettement et des besoins d'investissement très différents, il ne résulte pas de ces seules circonstances que le pouvoir réglementaire aurait commis une erreur manifeste d'appréciation en procédant à la fusion critiquée ;<br/>
<br/>
              18. Considérant qu'il résulte de tout ce qui précède que les requérants ne sont pas fondés à demander l'annulation du décret qu'ils attaquent ; que, par suite, les conclusions qu'ils présentent sur le fondement de l'article L. 761-1 du code de justice administrative ne peuvent qu'être également rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête du Collectif santé innovation Sud Réunion, de M. C...et de M. D...est rejetée.<br/>
Article 2 : La présente décision sera notifiée au Collectif santé innovation Sud Réunion, premier requérant dénommé, au Premier ministre et à la ministre des affaires sociales et de la santé.<br/>
Les autres requérants seront informés de la présente décision par la SCP Masse-Dessen, Thouvenin, Coudray, avocat au Conseil d'Etat et à la Cour de cassation, qui les représente devant le Conseil d'Etat.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
