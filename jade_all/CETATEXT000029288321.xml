<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029288321</ID>
<ANCIEN_ID>JG_L_2014_07_000000377918</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/28/83/CETATEXT000029288321.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  3ème sous-section jugeant seule, 23/07/2014, 377918, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-07-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>377918</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 3ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Fabrice Benkimoun</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:377918.20140723</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête enregistrée le 15 avril 2014 au secrétariat du contentieux du Conseil d'Etat, la commune de Bénévent-l'Abbaye demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2014-161 du 17 février 2014 portant délimitation des cantons dans le département de la Creuse ;<br/>
<br/>
              2°) d'enjoindre, dans un délai de deux mois à compter de la décision à intervenir sous astreinte de 300 euros par jour de retard, au gouvernement de procéder à une nouvelle délimitation des cantons du département de la Creuse attribuant à la commune de Bénévent-l'Abbaye la qualité de bureau centralisateur ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - le code électoral ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - le décret n° 2012-1479 du 27 décembre 2012 ;<br/>
              - le décret 2013-938 du n° 18 octobre 2013 ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Fabrice Benkimoun, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 191-1 du code électoral, résultant de la loi du 17 mai 2013 relative à l'élection des conseillers départementaux, des conseillers municipaux et des conseillers communautaires et modifiant le calendrier électoral : " Le nombre de cantons dans lesquels seront élus les conseillers départementaux est égal, pour chaque département, à la moitié du nombre de cantons existant au 1er janvier 2013, arrondi à l'unité impaire supérieure si ce nombre n'est pas entier impair. / Le nombre de cantons dans chaque département comptant plus de 500 000 habitants ne peut être inférieur à dix-sept. Il ne peut être inférieur à treize dans chaque département comptant entre 150 000 et 500 000 habitants ". Aux termes de l'article L. 3113-2 du code général des collectivités territoriales, dans sa rédaction issue de la même loi, applicable à la date du décret attaqué : " I. - Les modifications des limites territoriales des cantons, les créations et suppressions de cantons et le transfert du siège de leur chef-lieu sont décidés par décret en Conseil d'Etat après consultation du conseil général qui se prononce dans un délai de six semaines à compter de sa saisine. A l'expiration de ce délai, son avis est réputé rendu. / (...) III. - La modification des limites territoriales des cantons effectuée en application du I est conforme aux règles suivantes : / a) Le territoire de chaque canton est défini sur des bases essentiellement démographiques ; / b) Le territoire de chaque canton est continu ; / c) Est entièrement comprise dans le même canton toute commune de moins de 3 500 habitants ; / IV. - Il n'est apporté aux règles énoncées au III que des exceptions de portée limitée, spécialement justifiées, au cas par cas, par des considérations géographiques ou par d'autres impératifs d'intérêt général ".<br/>
<br/>
              2. Compte tenu de la réduction du nombre des cantons résultant de l'application de l'article L. 191-1 du code électoral, le décret attaqué a, sur le fondement de l'article L. 3113-2 du code général des collectivités territoriales, procédé à une nouvelle délimitation des cantons du département de la Creuse.<br/>
<br/>
              3. En premier lieu, aux termes du I de l'article L. 3113-2 du code général des collectivités territoriales, dans sa version actuellement en vigueur : " Les modifications des limites territoriales des cantons, les créations et suppressions de cantons et le transfert du siège de leur chef-lieu sont décidés par décret en Conseil d'Etat après consultation du conseil général qui se prononce dans un délai de six semaines à compter de sa saisine ", et aux termes de l'article L. 3121-19 de ce même code : " Douze jours au moins avant la réunion du conseil général, le président adresse aux conseillers généraux un rapport, sous quelque forme que ce soit, sur chacune des affaires qui doivent leur être soumises (...) ". Il ressort des pièces du dossier que le président du conseil général de la Creuse a été saisi le 4 décembre 2013 par le préfet de la Creuse pour rendre son avis sur le décret attaqué. Le conseil général bénéficiait d'un délai de six semaines à compter de cette date pour rendre son avis. Il a rendu son avis dans ce délai, le 17 décembre 2013. La consultation a été régulière.<br/>
<br/>
              4. En deuxième lieu, ni l'article L. 3113-2 du code général des collectivités territoriales, ni aucun autre texte non plus qu'aucun principe n'imposait au Premier ministre de prévoir que les limites des cantons, qui sont des circonscriptions électorales, coïncident avec les périmètres des établissements publics de coopération intercommunale ou avec les limites des " bassins de vie " définis par l'institut national de la statistique et des études économiques ou de retenir la proximité géographique des communes comme critère de délimitation des cantons. Par suite, la commune requérante ne saurait utilement soutenir que la délimitation des cantons du département ne correspondrait pas à celle des établissements publics de coopération intercommunale ou des " bassins de vie ".<br/>
<br/>
              5. En troisième lieu, aux termes de l'article 71 du décret du 18 octobre 2013, dans sa rédaction applicable à la date du décret attaqué et dont la légalité n'est pas contestée: "  (...) Pour la première délimitation générale des cantons opérée en application de l'article L. 3113-2  du code général des collectivités territoriales, dans sa rédaction résultant de l'article 46 de la loi n° 2013-403 du 17 mai 2013 relative à l'élection des conseillers départementaux, (...), le chiffre de la population municipale auquel il convient de se référer est celui authentifié par le décret n° 2012-1479 du 27 décembre 2012 authentifiant les chiffres des populations (...) ". Il est constant que les nouveaux cantons du département de la Creuse ont été délimités sur la base des données authentifiées par le décret du 27 novembre 2012. Par suite, le moyen tiré de ce que les données retenues pour le redécoupage des cantons de ce département ne correspondraient pas à la réalité démographique ne peut qu'être écarté. <br/>
<br/>
              6. En quatrième lieu, le décret attaqué, s'il modifie les délimitations des cantons et désigne pour chacun d'eux un bureau centralisateur, n'a ni pour objet, ni pour effet de procéder au transfert du siège des chefs-lieux de canton. Le décret attaqué désigne non pas la commune de Grand-Bourg comme chef-lieu du canton n° 11 mais comme bureau centralisateur de ce canton, cette qualité devant être, à compter de l'entrée en vigueur des nouvelles dispositions, dépourvue de tout lien avec celle de chef-lieu de canton. Il suit de là que le moyen de la commune requérante tiré de ce que la perte par la commune de Bénévent-l'Abbaye de la qualité de chef-lieu de canton, et par là même des dotations qui y sont actuellement attachées, entacherait d'erreur d'appréciation l'article 12 du décret attaqué, ne peut qu'être écarté. Enfin, en désignant la commune de Grand-Bourg, le bourg le plus peuplé du canton n° 11, comme bureau centralisateur, le Premier ministre n'a pas entaché le décret attaqué d'erreur manifeste d'appréciation. <br/>
<br/>
              7. Enfin, en dernier lieu, le détournement de pouvoir allégué n'est pas établi.<br/>
<br/>
              8. Il résulte de tout ce qui précède que la commune requérante n'est pas fondée à demander l'annulation pour excès de pouvoir du décret attaqué. Ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées.<br/>
<br/>
<br/>
<br/>DECIDE :<br/>
--------------<br/>
Article 1er : La requête de la commune de Bénévent-l'Abbaye est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la commune de Bénévent-l'Abbaye, au Premier ministre et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
