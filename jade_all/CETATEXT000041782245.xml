<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041782245</ID>
<ANCIEN_ID>JG_L_2020_03_000000420197</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/78/22/CETATEXT000041782245.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 16/03/2020, 420197, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-03-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420197</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>Mme Ophélie Champeaux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:420197.20200316</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société anonyme à responsabilité limitée (SARL) Pharmacie des Musées a demandé au tribunal administratif de Grenoble de prononcer la décharge des cotisations supplémentaires d'impôt sur les sociétés auxquelles elle a été assujettie au titre des exercices clos les 30 juin des années 2008, 2009, et 2010, des rappels de taxe sur la valeur ajoutée qui lui ont été réclamés au titre de la période du 1er juillet 2007 au 30 juin 2010 ainsi que des pénalités correspondantes. Par un jugement n° 1306256 du 12 novembre 2015, le tribunal a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 16LY00077 du 27 février 2018, la cour administrative d'appel de Nantes a accordé à la société une réduction des impositions supplémentaires et des pénalités mises à sa charge et rejeté le surplus des conclusions de sa requête d'appel.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et deux nouveaux mémoires, enregistrés les 27 avril et 27 juillet 2018 et les 15 avril et 11 septembre 2019 au secrétariat du contentieux du Conseil d'Etat, la société Pharmacie des Musées demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'article 4 de cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Ophélie Champeaux, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat de la société Pharmacie des Musées ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Pharmacie des Musées a fait l'objet d'une vérification de comptabilité au titre de la période du 1er juillet 2007 au 30 juin 2010, à l'issue de laquelle l'administration fiscale a regardé sa comptabilité comme irrégulière et non probante pour l'ensemble de la période vérifiée et procédé à la reconstitution de son chiffre d'affaires ainsi qu'à la détermination du bénéfice imposable et de la taxe sur la valeur ajoutée (TVA). La société a contesté les cotisations supplémentaires d'impôt sur les sociétés et les rappels de taxe sur la valeur ajoutée ainsi que les pénalités correspondantes mis à sa charge en conséquence de ces rectifications. Par un jugement du 12 novembre 2015, le tribunal administratif de Grenoble a rejeté sa demande tendant à la décharge de ces suppléments d'impositions. Par l'arrêt attaqué du 27 février 2018, la cour administrative d'appel de Lyon a prononcé la réduction des impositions et pénalités contestées et rejeté le surplus des conclusions d'appel de la société.<br/>
<br/>
              Sur les motifs de l'arrêt relatifs à la régularité de la procédure d'imposition :<br/>
<br/>
              2. Aux termes de l'article L. 57 du livre des procédures fiscales : " L'administration adresse au contribuable une proposition de rectification qui doit être motivée de manière à lui permettre de formuler ses observations ou de faire connaître son acceptation. (...) En cas d'application des dispositions de l'article L. 47 A, l'administration précise au contribuable la nature des traitements effectués ". Aux termes de l'article L. 47 A du même livre : "  I.- Lorsque la comptabilité est tenue au moyen de systèmes informatisés, le contribuable satisfait à l'obligation de représentation des documents comptables mentionnés au premier alinéa de l'article 54 du code général des impôts en remettant au début des opérations de contrôle, sous forme dématérialisée répondant à des normes fixées par arrêté du ministre chargé du budget, une copie des fichiers des écritures comptables définies aux articles 420-1 et suivants du plan comptable général. / (...) II.- En présence d'une comptabilité tenue au moyen de systèmes informatisés et lorsqu'ils envisagent des traitements informatiques, les agents de l'administration fiscale indiquent par écrit au contribuable la nature des investigations souhaitées. Le contribuable formalise par écrit son choix parmi l'une des options suivantes : / (...) c) Le contribuable peut également demander que le contrôle ne soit pas effectué sur le matériel de l'entreprise. Il met alors à la disposition de l'administration les copies des documents, données et traitements soumis à contrôle. Ces copies sont produites sur tous supports informatiques, répondant à des normes fixées par arrêté du ministre chargé du budget. L'administration restitue au contribuable avant la mise en recouvrement les copies des fichiers et n'en conserve pas de double. L'administration communique au contribuable, sous forme dématérialisée ou non au choix du contribuable, le résultat des traitements informatiques qui donnent lieu à des rehaussements au plus tard lors de l'envoi de la proposition de rectification mentionnée à l'article L. 57 ".<br/>
<br/>
              3. Il résulte de ces dispositions que, lorsqu'une société vérifiée choisit, en vertu du c du II de l'article L. 47 A du livre des procédures fiscales, de mettre à la disposition de l'administration les copies des documents, données et traitements soumis à contrôle, l'administration est tenue de préciser, dans sa proposition de rectification, les fichiers utilisés, la nature des traitements qu'elle a effectués sur ces fichiers et les modalités de détermination des éléments servant au calcul des rehaussements, mais n'a l'obligation de communiquer ni les algorithmes, logiciels ou matériels qu'elle a utilisés ou envisage de mettre en oeuvre pour effectuer ces traitements, ni les résultats de l'ensemble des traitements qu'elle a réalisés, que ce soit préalablement à la proposition de rectification ou dans le cadre de celle-ci. <br/>
<br/>
              4. Il résulte de ce qui a été dit au point 3 ci-dessus que c'est sans erreur de droit que la cour, après avoir relevé que l'administration avait exposé, dans la proposition de rectification adressée à la société requérante, la nature des traitements informatiques réalisés par ses soins et qu'elle avait annexé à cette proposition de rectification un CD-Rom comprenant les résultats de ces traitements, a jugé que si certains des fichiers joints ne comprenaient pas le champ relatif à la date des opérations, cette circonstance n'emportait pas pour autant une méconnaissance des dispositions du c du II de l'article L 47 A du livre des procédures fiscales. Par ailleurs, la cour, qui ne s'est pas méprise sur la portée des écritures de la société requérante, n'a pas davantage commis d'erreur de droit ni insuffisamment motivé son arrêt en jugeant que, compte tenu de la nature des traitements réalisés en l'espèce par le vérificateur sur les fichiers source de la société, l'administration n'était pas tenue de joindre les fichiers intermédiaires mentionnés dans la proposition de rectification.<br/>
<br/>
              Sur les motifs de l'arrêt relatifs au bien-fondé des impositions :<br/>
<br/>
              5. La cour a jugé, par une appréciation souveraine non arguée de dénaturation, que l'administration apportait la preuve des graves irrégularités dont la comptabilité de l'officine était entachée dès lors qu'elle démontrait l'existence de ruptures de séquentialité dans la numérotation des ventes et des règlements. Elle n'a pas commis d'erreur de droit en jugeant que la seule circonstance que le mode de présentation de ces résultats, sans champ relatif à la date et dans un ordre non chronologique, rendait plus difficile leur analyse, n'était pas de nature à remettre en cause leur exactitude, dès lors que de telles ruptures de séquentialité ont été mises en évidence au vu de la numérotation des ventes et des règlements et non de leur date.<br/>
<br/>
              6. Il résulte de tout ce qui précède que la société Pharmacie des Musées n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              7. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société Pharmacie des Musées est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la société Pharmacie des Musées et au ministre de l'action et des comptes publics.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
