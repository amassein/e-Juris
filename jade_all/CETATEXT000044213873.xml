<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044213873</ID>
<ANCIEN_ID>JG_L_2021_10_000000437333</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/21/38/CETATEXT000044213873.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 14/10/2021, 437333, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-10-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>437333</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP RICARD, BENDEL-VASSEUR, GHNASSIA ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Thomas Janicot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:437333.20211014</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Procédure contentieuse antérieure<br/>
<br/>
              M. B... C... a demandé au tribunal administratif de Mayotte, sous le n° 1200357, d'annuler la décision implicite par laquelle le président de la collectivité départementale de Mayotte a refusé de prononcer sa promotion au grade d'attaché principal du cadre d'emplois des attachés territoriaux et les arrêtés portant nomination au grade d'attaché principal en application du tableau d'avancement à ce grade pour 2011, et d'enjoindre au président de cette collectivité de prononcer sa promotion au grade d'attaché principal et de régulariser sa situation administrative et financière dans un délai de 10 jours à compter de la date d'épuisement des délais de recours, sous astreinte de 500 euros par jour de retard et, sous le n° 1300227, d'annuler l'arrêté du 27 décembre 2012 prononçant sa promotion au grade d'attaché principal à compter du 1er avril 2012 ainsi que la décision implicite de rejet de son recours gracieux tendant à sa nomination à ce grade au 1er octobre 2011 et d'enjoindre au président de cette collectivité de prononcer sa promotion au grade d'attaché principal à compter du 1er octobre 2011 et de régulariser sa situation administrative et financière dans un délai de dix jours à compter à compter de la date d'épuisement des délais de recours, sous astreinte de 500 euros par jour de retard.<br/>
<br/>
              Par un jugement nos 1200357, 1300227 du 10 juin 2014, le tribunal administratif de Mayotte, après avoir constaté qu'il n'y avait pas lieu de statuer sur la demande n°1200357, d'une part, a annulé l'arrêté du 27 décembre 2012 et la décision implicite de rejet du recours gracieux de l'intéressé tendant à sa nomination au grade d'attaché principal au 1er octobre 2011, en tant qu'ils fixent la date de promotion de M. C... au 1er avril 2012 et, d'autre part, a enjoint au président du conseil général de Mayotte de promouvoir l'intéressé au grade d'attaché principal à compter du 1er octobre 2011 et de procéder à la reconstitution de sa carrière à compter de cette date. <br/>
<br/>
              Par un arrêt n° 14BX02696 du 29 février 2016, la cour administrative d'appel de Bordeaux a rejeté l'appel du département de Mayotte tendant à l'annulation de ce jugement et, sur l'appel incident de M. C..., a enjoint au département de Mayotte, dans le délai de trois mois à compter de cet arrêt, de promouvoir l'intéressé au grade d'attaché principal à compter du 1er octobre 2011 et de procéder à la reconstitution de sa carrière à compter de cette date et a rejeté le surplus des conclusions de cet appel incident.<br/>
<br/>
              Par un arrêt n° 17BX00517 du 16 février 2018, la cour administrative d'appel de Bordeaux a accueilli la demande d'exécution de l'arrêt n° 14BX02696 du 29 février 2016 présentée par M. C... en prononçant une astreinte de 500 euros par jour de retard à l'encontre du département de Mayotte s'il ne justifie pas avoir exécuté, dans le délai d'un mois suivant la notification de cet arrêt, les mesures qu'impliquent l'arrêt n° 14BX02696 du 29 février 2016.<br/>
<br/>
              Par un arrêt n° 18BX04587 du 21 octobre 2019, la cour administrative d'appel de Bordeaux, sur la requête de M. C... tendant à la liquidation d'office de l'astreinte prononcée par l'arrêt n° 17BX00517 du 16 février 2018 pour la période comprise entre le 17 mars 2018 et la date de l'arrêt à intervenir, avec versement en sa faveur de l'intégralité de l'astreinte liquidée assortie des intérêts à compter de la date de l'arrêt à intervenir et majoration du taux de l'astreinte à 2 000 euros par jour de retard à compter de l'expiration du délai de dix jours à compter de la notification de l'arrêt à intervenir, a condamné le département de Mayotte à lui verser la somme de 19 550 euros au titre de la liquidation définitive de l'astreinte prononcée par son arrêt n°17BX00517 du 16 février 2018 et a rejeté le surplus des conclusions de la requête.<br/>
<br/>
              Procédure devant le Conseil d'État<br/>
<br/>
              Par un pourvoi et un mémoire en réplique, enregistrés les 3 janvier 2020 et 19 juillet 2021 au secrétariat du contentieux du Conseil d'Etat, M. C... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 18BX04587 du 21 octobre 2019 en tant qu'il a rejeté les conclusions de sa requête tendant à ce qu'il soit constaté que l'arrêté n° 1251/DRHFI/FPT/CD/2019 du 15 avril 2019 du président du conseil départemental de Mayotte n'a pas prononcé toutes les mesures nécessaires qu'implique l'arrêt n° 14BX02696 du 29 février 2016 ;<br/>
<br/>
              2°) réglant l'affaire au fond, d'enjoindre au département de Mayotte, sous une astreinte de 2 000 euros par jour de retard, de produire un relevé de carrière conforme à la chose jugée par l'arrêt n°14BX02696 du 29 février 2016 ;<br/>
<br/>
              3°) après la production de ce relevé de carrière, de procéder à la liquidation définitive de l'astreinte prononcée par l'arrêt n° 17BX00517 du 16 février 2018 et à la liquidation de l'astreinte fixée par la décision du Conseil d'Etat, le tout à son profit ; <br/>
<br/>
              4°) de mettre à la charge du département de Mayotte la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le décret n° 87-1099 du 30 décembre 1987 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thomas Janicot, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Ricard, Bendel-Vasseur, Ghnassia, avocat de M. B... C... et à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat du conseil départemental de Mayotte ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il résulte des pièces du dossier soumis aux juges du fond que la cour administrative d'appel de Bordeaux, par un arrêt du 29 février 2016, a enjoint au département de Mayotte de promouvoir M. C... au grade d'attaché territorial principal à compter du 1er octobre 2011 et de procéder à la reconstitution de sa carrière à compter de cette date, dans le délai de trois mois à compter de cet arrêt et, ensuite, par un arrêt du 16 février 2018, a prononcé à l'encontre de cette collectivité une astreinte au taux de 500 euros par jour de retard pris, à compter de la fin du délai d'un mois à partir de la notification de cet arrêt, dans l'exécution de l'injonction prononcée par son arrêt du 29 février 2016. M. C... se pourvoit en cassation contre l'arrêt du 21 octobre 2019 par lequel la cour administrative d'appel de Bordeaux a liquidé définitivement cette astreinte à la somme de 19 550 euros pour la période ayant couru entre le 20 mars 2018 et le 15 avril 2019, date de l'adoption de l'arrêté n°1251/DRHFI/FPT/CD/2019 du président du conseil départemental de Mayotte portant reconstitution de la carrière de l'intéressé au grade d'attaché territorial principal à la direction des affaires foncières. <br/>
<br/>
              2. En vertu de l'article 17 du décret du 30 décembre 1987 portant statut particulier du cadre d'emplois des attachés territoriaux, dans sa version applicable au litige, la durée maximale et la durée minimale du temps devant être passé dans le deuxième échelon comme dans le troisième échelon du grade d'attaché territorial principal sont, respectivement, de deux ans ou d'un an et six mois.<br/>
<br/>
              3. Il ressort des pièces du dossier soumis à la cour administrative d'appel que l'arrêté du 15 avril 2019 n'a reconstitué la carrière de M. C... qu'en prononçant, d'une part, son avancement au grade d'attaché territorial principal, au premier échelon, à compter du 1er octobre 2011, avec une ancienneté conservée de six mois et, d'autre part, son avancement d'échelon au deuxième échelon de ce grade à compter du 1er avril 2012 et sans conservation d'ancienneté mais que, pour la période ayant couru entre le 1er avril 2012 et le 31 mai 2016, date de la prise d'effet de la démission de l'intéressé, l'article 3 de cet arrêté se borne à prévoir que le déroulé de sa carrière " se poursuit conformément aux arrêtés établis ". Cependant, cet arrêté ne vise aucun autre acte normatif portant continuité de carrière avec avancements normaux d'échelon dans les délais prévus par les dispositions, rappelés au point précédent et il ressort, également, des autres pièces du dossier soumis à la cour que le département de Mayotte n'a produit devant elle aucun élément permettant d'établir que M. C... a effectivement bénéficié, dans ces mêmes délais, d'un avancement au troisième échelon, puis au quatrième échelon, du grade d'attaché territorial principal.<br/>
<br/>
              4. Par suite, la cour a dénaturé les pièces du dossier qui lui était soumis en jugeant que l'arrêté du 15 avril 2019 répondait intégralement à l'injonction prononcée par son arrêt du 16 février 2018, pour liquider définitivement l'astreinte prononcée contre le département de Mayotte. Dès lors, M. C... est fondé à demander l'annulation de l'arrêt qu'il attaque.<br/>
<br/>
              5. Dans les circonstances de l'espèce, il y a lieu de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              6. Les articles L. 911-1 et L. 911-2 du code de justice administrative reconnaissent à la juridiction saisie un pouvoir d'injonction. Aux termes de l'article L. 911-3 du même code : " La juridiction peut assortir, dans la même décision, l'injonction prescrite en application des articles L. 911-1 et L. 911-2 d'une astreinte qu'elle prononce dans les conditions prévues au présent livre et dont elle fixe la date d'effet ". Aux termes de l'article L. 911-7 du même code : " En cas d'inexécution totale ou partielle ou d'exécution tardive, la juridiction procède à la liquidation de l'astreinte qu'elle avait prononcée. / (...) / Elle peut modérer ou supprimer l'astreinte provisoire, même en cas d'inexécution constatée ". Aux termes de l'article L. 911-8 du même code : " La juridiction peut décider qu'une part de l'astreinte ne sera pas versée au requérant. / Cette part est affectée au budget de l'Etat ".<br/>
<br/>
              7. Il résulte de ces dispositions que l'astreinte a pour finalité de contraindre la personne qui s'y refuse à exécuter les obligations qui lui ont été assignées par une décision de justice et, ainsi, à respecter l'autorité de la chose jugée. Sa liquidation a pour objet de tirer les conséquences du refus ou du retard mis à exécuter ces obligations. En cas d'inexécution totale ou partielle ou d'exécution tardive de la décision, la juridiction procède, en vertu de l'article L. 911-7 du code de justice administrative, à la liquidation de l'astreinte. En vertu du premier alinéa de l'article L. 911-8 de ce code, la juridiction a la faculté de décider, afin d'éviter un enrichissement indu, qu'une fraction de l'astreinte liquidée ne sera pas versée au requérant, le second alinéa prévoyant que cette fraction est alors affectée au budget de l'État.<br/>
<br/>
              8. En premier lieu, il résulte de l'instruction que, par un arrêté n°3945/DRHFI/FPT/CD/2021 du 12 juillet 2021, le président du conseil départemental de Mayotte a prononcé la promotion de M. C... au quatrième échelon du grade d'attaché territorial principal à compter du 1er avril 2016, à partir d'une promotion au troisième échelon de ce grade acquise à compter du 1er avril 2014, à l'issue de la durée maximale de deux ans devant être passée, respectivement, dans le deuxième et le troisième échelons. De la sorte, et en l'absence, en tout état de cause, de contestation de M. C... sur cette durée, le président du conseil départemental de Mayotte doit être regardé comme ayant procédé à la reconstitution de toute la carrière de M. C... en exécutant intégralement l'injonction prononcée par l'article 2 du dispositif de l'arrêt n° 14BX02696 du 29 février 2016.<br/>
<br/>
              9. Toutefois, entre l'expiration du délai d'un mois suivant la notification au département de Mayotte de l'arrêt n° 17BX00517 du 16 février 2018, qui a fixé la date d'effet de l'astreinte provisoire ordonnée par l'article 1er de son dispositif, soit à compter du 21 mars 2018, d'une part et, d'autre part, le 12 juillet 2021, date de l'exécution intégrale de l'injonction de reconstitution de la carrière de M. C..., à savoir pendant 1 210 jours, le département de Mayotte s'est refusé à exécuter les obligations qui lui ont été assignées par une décision de justice. Dès lors, il y a lieu de procéder à la liquidation de l'astreinte définitive pour cette période en fixant, dans les circonstances de l'espèce et en considération des conséquences pécuniaires en faveur de M. C... que le département doit tirer de la reconstitution complète de sa carrière, son taux à 30 euros par jour de retard, soit à la liquidation d'une astreinte définitive pour la somme totale de 36 300 euros, dont une fraction de 40 %, soit 14 520 euros, ne sera pas versée au requérant mais sera affectée au budget de l'Etat et une fraction de 60 %, soit 21 780 euros, sera versée au requérant.<br/>
<br/>
              10. En second lieu et comme il est dit au point 8, postérieurement à l'introduction de la requête d'appel de M. C..., l'arrêté n° 3945/DRHFI/FPT/CD/2021 du 12 juillet 2021 du président du conseil départemental de Mayotte doit être regardé comme ayant exécuté intégralement l'injonction ordonnée par l'article 2 du dispositif de l'arrêt n° 14BX02696 du 29 février 2016, avec les effets sur la carrière de son ancien fonctionnaire qui sont repris dans le relevé de  carrière à jour que le département a produit dans la présente instance. Ainsi, les conclusions de M. C... tendant à ce qu'il soit enjoint au département de Mayotte, sous une astreinte de 2 000 euros par jour de retard, de produire un relevé de carrière conforme à la chose jugée par cet arrêt, sont devenues sans objet et il n'y a pas lieu d'y statuer.<br/>
<br/>
              11. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. C..., qui n'est pas la partie perdante dans la présente instance. En revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du département de Mayotte la somme de 3 000 euros à verser à M. C..., au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Bordeaux n°18BX04587 du 21 octobre 2019 est annulé.<br/>
Article 2 : L'astreinte définitive résultant de l'arrêt n° 17BX00517 du 16 février 2018 de la cour administrative de Bordeaux est liquidée à la somme de 36 300 euros. Le département de Mayotte est condamné à verser à M. C... 60 % de cette astreinte, soit 21 780 euros et à verser à l'Etat 40 % de la même astreinte, soit 14 520 euros.<br/>
Article 3 : Il n'y a pas lieu de statuer sur les conclusions de la requête de M. C... tendant à ce qu'il soit enjoint au département de Mayotte, sous une astreinte de 2 000 euros par jour de retard, de produire un relevé de carrière conforme à la chose jugée par l'arrêt n° 14BX02696 du 29 février 2016 de la cour administrative de Bordeaux.<br/>
Article 4 : Le département de Mayotte versera à M. C... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : Le surplus des conclusions des parties est rejeté.<br/>
Article 6 : La présente décision sera notifiée à M. B... C... et au département de Mayotte et au ministre de l'économie, des finances et de la relance.<br/>
Copie en sera adressée au ministère public près la Cour de discipline budgétaire et financière.<br/>
<br/>
              Délibéré à l'issue de la séance du 23 septembre 2021 où siégeaient : M. Guillaume Goulard, président de chambre, présidant ; M. Stéphane Verclytte, conseiller d'Etat et M. Thomas Janicot, maître des requêtes-rapporteur. <br/>
<br/>
              Rendu le 14 octobre 2021.<br/>
                 Le Président : <br/>
                 Signé : M. Guillaume Goulard<br/>
 		Le rapporteur : <br/>
      Signé : M. Thomas Janicot<br/>
                 La secrétaire :<br/>
                 Signé : Mme A... E...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
