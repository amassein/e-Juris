<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039184602</ID>
<ANCIEN_ID>JG_L_2019_10_000000421016</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/18/46/CETATEXT000039184602.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  7ème chambre jugeant seule, 04/10/2019, 421016, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-10-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421016</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 7ème chambre jugeant seule</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE ; HAAS</AVOCATS>
<RAPPORTEUR>Mme Cécile Renault</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:421016.20191004</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C... B... a demandé au tribunal administratif de la Guadeloupe d'annuler les décisions du 1er octobre 2009 et du 6 janvier 2010 par lesquelles le directeur des pensions de La Poste et de France Télécom a refusé de lui accorder une rente viagère d'invalidité sur le fondement de l'article L. 28 du code des pensions civiles et militaires de retraite, de condamner La Poste à lui verser la somme de 32 400 euros pour les sommes dues depuis l'année 2001 au titre de cette rente, et de l'indemniser de son préjudice à hauteur de 86 364 euros au titre de la perte de salaires et de 150 000 euros au titre du préjudice moral et des troubles dans les conditions d'existence. Par un jugement n° 1000470 du 21 décembre 2012, le tribunal administratif de la Guadeloupe a annulé les décisions litigieuses, enjoint au président directeur général de La Poste d'allouer à M. B... dans un délai de trois mois une rente viagère d'invalidité à compter du 27 mars 2006, condamné La Poste à lui verser la somme de 5 000 euros en réparation de son préjudice et rejeté le surplus de ses demandes.<br/>
<br/>
              Par un arrêt n° 13BX00640 du 10 mars 2016, la cour administrative d'appel de Bordeaux a, sur appel de La Poste, annulé l'article 2 du jugement, en ce qu'il fixe la date d'effet de la rente viagère d'invalidité au 27 mars 2006, et enjoint à La Poste de soumettre à la décision du ministre des finances et des comptes publics l'octroi d'une rente viagère d'invalidité à M. B... dans un délai de trois mois.<br/>
<br/>
              Par une décision n° 399473 du 19 juillet 2017, le Conseil d'Etat, statuant au contentieux, a, sur un pourvoi du ministre des finances et des comptes publics, annulé cet arrêt et a renvoyé l'affaire à la cour administrative d'appel de Bordeaux. <br/>
<br/>
              Par un arrêt n° 17BX02492 du 27 février 2018, la cour administrative d'appel de Bordeaux, en premier lieu, a annulé les articles 1er et 2 du jugement du 21 décembre 2012 par lequel le tribunal administratif de la Guadeloupe a annulé les décisions des 1er octobre 2009 et 6 janvier 2010 et enjoint au président directeur général de La Poste d'allouer à M. B... une rente viagère d'invalidité à compter du 27 mars 2006 dans un délai de trois mois, en deuxième lieu, a rejeté ses conclusions de première instance tendant à l'annulation des deux décisions précitées et à l'attribution d'une rente viagère d'invalidité ainsi que ses conclusions d'appel incident et, enfin, rejeté le surplus des conclusions de la Poste.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 28 mai et 28 août 2018 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ; <br/>
<br/>
              3°) de mettre à la charge de La Poste la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ;<br/>
              - le code des pensions civiles et militaires de retraite ;<br/>
              - le décret n° 60-1089 du 6 octobre 1960 ;<br/>
              - le décret n° 2009-1053 du 26 août 2009 ; <br/>
              - la décision du 23 novembre 2018 par laquelle le Conseil d'Etat, statuant au contentieux, n'a pas renvoyé au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M. B... ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme A... D..., auditrice,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de M. B... et à Me Haas, avocat de la La Poste ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              Sur le pourvoi :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. B... a été victime, le 24 juillet 1966, alors qu'il effectuait son service national, d'un accident lui occasionnant un traumatisme crânien, au titre duquel il perçoit une pension militaire d'invalidité. Après avoir été titularisé à La Poste, il a été victime d'un accident de service le 22 juin 1976. Il a été radié des cadres à compter du 1er janvier 2001 pour invalidité non imputable au service. Il a demandé en 2009 au service des pensions de La Poste et de France Télécom que lui soit allouée une rente viagère d'invalidité au titre de séquelles tardives de son accident de service de 1976. Par des décisions du 1er octobre 2009 et du 6 janvier 2010, le directeur des pensions de La Poste et de France Télécom a refusé de faire droit à sa demande. Par un jugement du 21 décembre 2012, le tribunal administratif de la Guadeloupe, saisi par M. B..., a annulé les décisions litigieuses, enjoint au président directeur général de La Poste de lui allouer dans un délai de trois mois une rente viagère d'invalidité à compter du 27 mars 2006, condamné La Poste à lui verser la somme de 5 000 euros en réparation de son préjudice et rejeté le surplus de ses demandes. Par un arrêt du 10 mars 2016, la cour administrative d'appel de Bordeaux a, sur appel de La Poste, d'une part, annulé ce jugement en tant qu'il enjoignait à La Poste d'allouer à M. B... une rente viagère d'invalidité à compter du 27 mars 2006 dans un délai de trois mois, et, d'autre part, enjoint à La Poste de soumettre à la décision du ministre des finances et des comptes publics l'octroi d'une rente viagère d'invalidité à l'intéressé dans un délai de trois mois. Par une décision du 19 juillet 2017, le Conseil d'Etat, statuant au contentieux a, sur un pourvoi du ministre des finances et des comptes publics, annulé cet arrêt et renvoyé l'affaire à la cour administrative d'appel de Bordeaux. Par un arrêt du 27 février 2018 contre lequel M. B... se pourvoit en cassation, cette cour a, en premier lieu, annulé les articles 1er et 2 du jugement du 21 décembre 2012 du tribunal administratif de la Guadeloupe, en deuxième lieu, rejeté ses conclusions de première instance tendant à l'annulation des décisions des 1er octobre 2009 et 6 janvier 2010 et à l'attribution d'une rente viagère d'invalidité ainsi que ses conclusions d'appel incident, et, en troisième lieu, rejeté le surplus des conclusions d'appel de La Poste.<br/>
<br/>
              En ce qui concerne la réparation des préjudices invoqués par M. B... :<br/>
<br/>
              2. Aux termes de l'article L. 27 du code des pensions civiles et militaires de retraite : " Le fonctionnaire civil qui se trouve dans l'incapacité permanente de continuer ses fonctions en raison d'infirmités résultant de blessures ou de maladie contractées ou aggravées soit en service (...) peut être radié des cadres par anticipation soit sur sa demande (...) ". Aux termes de l'article L. 28 du même code : " Le fonctionnaire civil radié des cadres dans les conditions prévues à l'article L. 27 a droit à une rente viagère d'invalidité cumulable (...) avec la pension rémunérant les services. / Le droit à cette rente est également ouvert au fonctionnaire retraité qui est atteint d'une maladie professionnelle dont l'imputabilité au service est reconnue par la commission de réforme postérieurement à la date de la radiation des cadres, dans les conditions définies à l'article L. 31. Dans ce cas, la jouissance de la rente prend effet à la date du dépôt de la demande de l'intéressé (...) ".<br/>
<br/>
              3. Ces dispositions déterminent forfaitairement la réparation à laquelle un fonctionnaire victime d'un accident de service ou atteint d'une maladie professionnelle peut prétendre, au titre de l'atteinte qu'il a subie dans son intégrité physique, dans le cadre de l'obligation qui incombe aux collectivités publiques de garantir leurs agents contre les risques qu'ils peuvent courir dans l'exercice de leurs fonctions. Elles ne font cependant obstacle ni à ce que le fonctionnaire qui a enduré, du fait de l'accident ou de la maladie, des souffrances physiques ou morales et des préjudices esthétiques ou d'agrément, obtienne de la collectivité qui l'emploie, même en l'absence de faute de celle-ci, une indemnité complémentaire réparant ces chefs de préjudice, distincts de l'atteinte à l'intégrité physique, ni à ce qu'une action de droit commun pouvant aboutir à la réparation intégrale de l'ensemble du dommage soit engagée contre la collectivité, dans le cas notamment où l'accident ou la maladie serait imputable à une faute de nature à engager la responsabilité de cette collectivité ou à l'état d'un ouvrage public dont l'entretien incombait à celle-ci.<br/>
<br/>
              4. Il ressort des énonciations de l'arrêt attaqué que, pour rejeter les conclusions d'appel incident de M. B... en ce qui concerne la réparation de ses préjudices, la cour administrative d'appel de Bordeaux a considéré, d'une part, que celui-ci ne justifiait d'aucune faute de La Poste de nature à fonder son droit à indemnisation, d'autre part, qu'il n'apportait pas de preuves ou de précisions, concernant les préjudices matériel, psychologique et moral qu'il aurait subis, qui permettraient d'établir qu'il pourrait prétendre à une somme supérieure à celle de 5 000 euros que lui a allouée le tribunal administratif. M. B... n'est dès lors pas fondé à soutenir que l'arrêt attaqué, qui est suffisamment motivé sur ce point, se serait fondé uniquement sur l'absence de faute de La Poste pour rejeter ses conclusions d'appel incident et serait ainsi entaché d'une erreur de droit. M. B... n'est, par suite, pas fondé à demander l'annulation de l'arrêt attaqué en tant qu'il rejette ces conclusions.<br/>
<br/>
              En ce qui concerne l'allocation d'une rente viagère d'invalidité :<br/>
<br/>
              5. Les dispositions du deuxième alinéa de l'article 28 du code des pensions civiles et militaires de retraite, citées au point 2, qui ne comportent aucune restriction quant à l'origine des maladies professionnelles qu'elles mentionnent, ne sauraient avoir pour effet d'exclure du bénéfice du droit à une rente viagère d'invalidité les agents atteints d'infirmités résultant des séquelles d'un accident de service apparues tardivement et reconnues comme imputables au service postérieurement à la date de radiation des cadres. Il suit de là que, la cour administrative d'appel de Bordeaux a commis une erreur de droit en jugeant que ces dispositions ne pouvaient pas être invoquées par les fonctionnaires qui se prévalent de séquelles tardives d'un accident de service. Par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, M. B... est fondé à demander l'annulation de l'arrêt attaqué en tant qu'il a statué sur les conclusions relatives à l'attribution d'une rente viagère d'invalidité.<br/>
<br/>
              6. Aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire ". Le Conseil d'Etat étant saisi, en l'espèce, d'un second pourvoi en cassation, il lui incombe de régler l'affaire au fond dans cette mesure.<br/>
<br/>
              Sur l'appel principal de La Poste :<br/>
<br/>
              7. En premier lieu, La Poste ne peut utilement se prévaloir des dispositions de l'article 6 du décret du 6 octobre 1960, qui sont applicables aux allocations temporaires d'invalidité susceptibles d'être allouées aux fonctionnaires en activité et non à la rente viagère d'invalidité en litige.<br/>
<br/>
              8. En deuxième lieu, M. B... a été radié des cadres à compter du 1er janvier 2001 en raison d'une invalidité non imputable au service, en application des articles L. 4 et L. 29 du code des pensions civiles et militaires de retraite. Il est donc constant que sa situation n'est pas au nombre de celles régies par l'article L. 27 du même code relatif à la radiation des cadres d'un fonctionnaire civil incapable de continuer ses fonctions en raison d'infirmités résultant de blessures ou de maladie contractées ou aggravées en service. Sa situation n'entre pas non plus, par voie de conséquence, dans le champ des dispositions du premier alinéa de l'article L. 28, qui concernent les fonctionnaires civils radiés des cadres dans les conditions prévues à l'article L. 27 du code des pensions civiles et militaires de retraite.<br/>
<br/>
              9. En revanche, il résulte de l'instruction que la pseudarthrose et l'arthrose radio carpienne constatées en 2006 par un médecin expert ont été qualifiées par ce dernier de rechute de l'accident du travail du 22 juin 2006, ce qui n'a pas été contesté par La Poste. La commission de réforme de la direction de la santé et du développement social de La Poste a retenu, dans un avis du 17 mars 2009, un taux d'incapacité permanente partielle de 15 % pour M. B.... Par suite, il résulte de ce qui a été dit au point 5 que M. B... peut bénéficier d'une rente viagère d'invalidité sur le fondement des dispositions du deuxième alinéa de l'article L. 28 du code des pensions civiles et militaires de retraite.<br/>
<br/>
              10. En dernier lieu, l'article L. 31 du code des pensions civiles et militaires de retraite, auquel renvoie l'article L. 28, dispose que : " La réalité des infirmités invoquées, la preuve de leur imputabilité au service, le taux d'invalidité qu'elles entraînent, l'incapacité permanente à l'exercice des fonctions sont appréciés par une commission de réforme selon des modalités qui sont fixées par un décret en Conseil d'Etat. / Le pouvoir de décision appartient, dans tous les cas, au ministre dont relève l'agent et au ministre des finances ".<br/>
<br/>
              11. Il résulte de ces dispositions que le pouvoir de décision en matière d'allocation de la rente viagère d'invalidité litigieuse est partagé entre le ministre dont relève l'agent et le ministre des finances, dont les services sont en particulier chargés de la liquidation de la rente. Par suite, il appartient seulement à La Poste de proposer au ministre des finances le versement d'une rente viagère d'invalidité à M. B.... <br/>
<br/>
              12. Il résulte de ce qui précède que La Poste est seulement fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif lui a enjoint d'allouer à M. B... une rente viagère d'invalidité à compter du 27 mars 2006 dans un délai de trois mois.<br/>
<br/>
              Sur l'appel incident de M. B... :<br/>
<br/>
              13. En premier lieu, M. B... ne justifie pas, par les pièces qu'il produit, que le montant de la rente viagère d'invalidité à laquelle il peut prétendre devrait être fixé à 3 240 euros comme il le soutient. <br/>
<br/>
              14. En deuxième lieu, il est constant que M. B... n'a demandé le bénéfice d'une telle rente qu'en 2009. Par suite, il ne peut prétendre au paiement de cette rente qu'à compter de la date de sa demande, en application des dispositions du deuxième alinéa de l'article L. 28 du même code, citées au point 2. <br/>
<br/>
              15. En dernier lieu, l'annulation des décisions du 1er octobre 2009 et du 6 janvier 2010 du service des pensions de La Poste et de France Télécom, par lesquelles ce service a considéré que M. B... ne pouvait pas prétendre au bénéfice d'une telle rente, n'implique pas que La Poste procède au versement de la rente mais seulement, ainsi qu'il a été dit au point 11, qu'elle propose au ministre de l'action et des comptes publics le versement d'une rente viagère d'invalidité à l'intéressé.<br/>
<br/>
              16. Il suit de là que M. B... n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de la Guadeloupe a rejeté ses conclusions tendant à ce que la rente annuelle qui lui est due soit fixée à 3 240 euros et à ce que la somme de 32 400 euros lui soit versée au titre d'arrérages sur une période de 10 ans. Il est en revanche fondé à demander qu'il soit enjoint à La Poste de proposer au ministre des finances et des comptes publics l'octroi d'une rente viagère d'invalidité à M. B..., dans un délai de trois mois à compter de la notification de la présente décision.<br/>
<br/>
              Sur les conclusions présentées sur le fondement de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              17. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de La Poste la somme de 4 500 euros à verser à M. B... sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative au titre de la procédure devant la cour administrative d'appel et le Conseil d'Etat. Ces mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. B... qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 27 février 2018 de la cour administrative d'appel de Bordeaux est annulé en tant qu'il statue sur les conclusions relatives à l'attribution d'une rente viagère d'invalidité.<br/>
Article 2 : L'article 2 du jugement du 21 décembre 2012 du tribunal administratif de la Guadeloupe est annulé.<br/>
Article 3 : Il est enjoint à La Poste de proposer au ministre de l'action et des comptes publics l'octroi d'une rente viagère d'invalidité à M. B..., dans un délai de trois mois à compter de la notification de la présente décision.<br/>
Article 4 : La Poste versera à M. B... la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : Le surplus des conclusions des parties présentées devant le Conseil d'Etat et la cour administrative d'appel de Bordeaux est rejeté.<br/>
Article 6 : La présente décision sera notifiée à M. C... B..., à La Poste et au ministre de l'action et des comptes publics.<br/>
Copie en sera adressée à la ministre des outre-mer.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
