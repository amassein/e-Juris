<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034026075</ID>
<ANCIEN_ID>JG_L_2017_02_000000392245</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/02/60/CETATEXT000034026075.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 10/02/2017, 392245, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-02-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>392245</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Marie-Anne Lévêque</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:392245.20170210</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 31 juillet et 30 décembre 2015 au secrétariat du contentieux du Conseil d'Etat, M. B...A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite de rejet née du silence gardé par le Premier ministre sur son recours gracieux du 19 mai 2015 tendant à l'abrogation des dispositions de l'article R. 4137-29 du code de la défense ; <br/>
<br/>
              2°) d'enjoindre au Premier ministre d'abroger ces dispositions ;  <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ; <br/>
              - le code de la défense ; <br/>
              - la loi n° 2016-483 du 20 avril 2016 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Anne Lévêque, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public.<br/>
<br/>
<br/>
<br/>1. Considérant que, par une lettre du 19 mai 2015, reçue le lendemain, M. A..., militaire sous contrat, a demandé au Premier ministre d'abroger les dispositions de l'article R. 4137-29 du code de la défense, qui fixent les règles applicables à la sanction des jours d'arrêts assortis d'une période d'isolement ; qu'il demande au Conseil d'Etat d'annuler la décision implicite par laquelle le Premier ministre a refusé de faire droit à sa demande et de lui enjoindre d'abroger les dispositions en litige ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 4137-2 du code de la défense dans sa rédaction en vigueur à la date de la décision en litige : " Les sanctions disciplinaires applicables aux militaires sont réparties en trois groupes : / 1° Les sanctions du premier groupe sont : / (...) / e) Les arrêts ; / (...) En cas de nécessité, les arrêts et les consignes sont prononcés avec effet immédiat. Les arrêts avec effet immédiat peuvent être assortis d'une période d'isolement. / Les conditions d'application du présent article font l'objet d'un décret en Conseil d'Etat " ; qu'aux termes de l'article R. 4137-29 du code de la défense, dont l'abrogation est demandée : " Lorsque une sanction d'arrêts est motivée par une faute ou un manquement qui traduit un comportement dangereux pour lui-même ou pour autrui, l'autorité militaire de premier niveau peut décider de prononcer des arrêts avec effet immédiat assortis d'une période d'isolement. Il doit y être mis fin dès que les conditions qui l'ont justifiée ne sont plus réunies. / Cette décision est notifiée oralement au militaire qui se voit communiquer sans délai les éléments au vu desquels la mesure d'isolement a été prise. / Au cours de cette période, le militaire en isolement cesse de participer au service de sa formation. Il est placé dans un local fermé et doit faire l'objet d'un suivi médical. Il est autorisé à s'entretenir avec un militaire de sa formation, à communiquer par écrit avec les personnes de son choix et à recevoir les courriers qui lui sont destinés. Lorsque des arrêts avec effet immédiat sont prononcés, la permission en cours est suspendue. / (...) " ; <br/>
<br/>
              3. Considérant que les dispositions de l'article L. 4137-2 du code de la défense, aux termes desquelles " Les arrêts avec effet immédiat peuvent être assortis d'une période d'isolement ", qui constituent la base légale des dispositions de l'article R. 4137-29 de ce code, ont été abrogées par l'article 38 de la loi du 20 avril 2016, soit après l'intervention de la décision contestée ; que si les dispositions de l'article R. 4137-29 ne peuvent plus, en l'absence de base légale, recevoir application depuis l'entrée en vigueur de la loi du 20 avril 2016 et si l'administration est tenue de les abroger, il y a lieu, toutefois, d'apprécier la légalité de la décision refusant de les abroger à la date à laquelle elle a été prise, soit le 20 juillet 2015 ; qu'à cette date, les dispositions, citées au point 2, de l'article L. 4137-2 du même code renvoyaient à un décret en Conseil d'Etat le soin de fixer les modalités d'application de cet article, lequel instituait la sanction disciplinaire d'arrêts assortis d'une période d'isolement ; que, contrairement à ce que soutient le requérant, les dispositions contestées de l'article R. 4137-29 n'ont pas excédé le champ de cette habilitation législative et n'ont donc pas méconnu les dispositions de l'article 34 de la Constitution ; qu'il suit de là que M. A...n'est pas fondé à demander l'annulation de la décision du Premier ministre refusant d'abroger ces dispositions ; que, par suite, ses conclusions à fin d'injonction et celles présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent qu'être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. A...est rejetée.  <br/>
Article 2 : La présente décision sera notifiée à M. B...A...et au ministre de la défense.<br/>
Copie en sera adressée au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
