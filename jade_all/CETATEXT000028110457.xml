<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028110457</ID>
<ANCIEN_ID>JG_L_2013_10_000000353333</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/11/04/CETATEXT000028110457.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 23/10/2013, 353333, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-10-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>353333</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Jean-Marc Vié</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:353333.20131023</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 13 octobre 2011 et 6 janvier 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme B...A..., demeurant... ; elle demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n°10BX02043 du 23 juin 2011 par lequel la cour administrative d'appel de Bordeaux, statuant sur la requête de la commune de Castres, a annulé l'article 2 du jugement n° 0702451 du 7 mai 2010 par lequel le tribunal administratif de Toulouse a condamné la commune à lui verser une indemnité de 15 000 euros en réparation du préjudice que lui a causé la décision de reporter les travaux d'aménagement de la place Soult à Castres ; <br/>
<br/>
              2°) réglant l'affaire au fond, de condamner la commune de Castres à lui verser la somme de 23 000 euros en réparation du préjudice que lui a causé la décision de reporter les travaux d'aménagement de la place Soult et, en tout état de cause, de rejeter l'appel de la commune ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Castres le versement à son avocat, la SCP Célice-Blancpain-Soltner, de la somme de 2 000 euros au titre des articles L. 761-1 du code de justice administrative  et 37 de la loi du 10 juillet 1991 ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Vié, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Blancpain, Soltner, avocat de Mme A...et à la SCP Lyon-Caen, Thiriez, avocat de la commune de Castres  ;<br/>
<br/>
<br/>
<br/>
              1. Considérant que, par l'arrêt du 23 juin 2011 contre lequel Mme A...se pourvoit en cassation, la cour administrative d'appel de Bordeaux, après avoir écarté la fin de non-recevoir opposée à l'appel de la commune de Castres, tirée du défaut de qualité pour agir du maire au nom de la commune, a fait droit à l'appel de cette dernière et annulé le jugement du 7 mai 2010 du tribunal administratif de Toulouse en tant qu'il l'avait condamnée à verser une indemnité à Mme A...; <br/>
<br/>
              2. Considérant, d'une part, que, lorsqu'une partie est une personne morale, il appartient à la juridiction administrative saisie, qui en a toujours la faculté, de s'assurer, le cas échéant, que le représentant de cette personne morale justifie de sa qualité pour agir au nom de cette partie ; que tel est le cas lorsque cette qualité est sérieusement contestée par l'autre partie ou qu'au premier examen, l'absence de qualité du représentant de la personne morale semble ressortir des pièces du dossier ; que, d'autre part, lorsqu'il est saisi, postérieurement à la clôture de l'instruction, d'un mémoire émanant d'une des parties à l'instance, le juge n'est tenu de soumettre au débat contradictoire les éléments contenus dans le mémoire que si celui-ci contient soit l'exposé d'une circonstance de fait dont la partie qui l'invoque n'était pas en mesure de faire état avant la clôture de l'instruction et que le juge ne pourrait ignorer sans fonder sa décision sur des faits matériellement inexacts, soit d'une circonstance de droit nouvelle ou que le juge devrait relever d'office ; que, par suite, dans l'hypothèse où le juge administratif, saisi d'un moyen tiré du défaut de qualité pour agir de la personne morale requérante, décide de tenir compte de la délibération habilitant son représentant à agir en justice lorsque celle-ci est produite postérieurement à la clôture de l'instruction, il lui incombe de soumettre cette pièce à un débat contradictoire en rouvrant l'instruction ; <br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis à la cour administrative d'appel de Bordeaux que Mme A... a soutenu, par un mémoire enregistré le 4 avril 2011 devant cette cour, que la requête de la commune de Castres était irrecevable, faute pour son maire d'avoir justifié d'une autorisation d'ester en justice ; que la commune a été mise en demeure, le 18 avril 2011, de produire la délibération du conseil municipal autorisant le maire à relever appel du jugement attaqué ; qu'elle a transmis, par télécopie du 23 mai 2011, soit postérieurement à la clôture d'instruction, intervenue trois jours francs avant l'audience du 26 mai 2011, un mémoire comportant la copie de la délibération du 29 avril 2011 par laquelle le conseil municipal autorisait le maire à faire appel du jugement du 7 mai 2010 ; que la cour a écarté la fin de non-recevoir en se fondant sur cette délibération ; qu'en statuant ainsi, sans avoir, au préalable, rouvert l'instruction, communiqué cet acte à Mme A...et laissé à celle-ci un délai suffisant pour présenter d'éventuelles observations, la cour a méconnu le principe du caractère contradictoire de la procédure ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, Mme A... est fondée à demander l'annulation de l'arrêt qu'elle attaque ;<br/>
<br/>
              4. Considérant que Mme A...a obtenu le bénéfice de l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Célice-Blancpain-Soltner, avocat de MmeA..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de la commune de Castres la somme de 2 000 euros à verser à la SCP Célice-Blancpain-Soltner ; que les mêmes dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de Mme A...qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>		D E C I D E :<br/>
              		--------------<br/>
<br/>
Article 1er : L'arrêt du 23 juin 2011 de la cour administrative d'appel de Bordeaux est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Bordeaux.<br/>
Article 3 : La commune de Castres versera à la SCP Célice-Blancpain-Soltner, avocat de Mme A..., une somme de 2 000 euros en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : Les conclusions de la commune de Castres présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
           Article 5 : La présente décision sera notifiée à Mme B...A...et à la commune de Castres.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
