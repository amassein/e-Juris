<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035991004</ID>
<ANCIEN_ID>JG_L_2017_11_000000402511</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/99/10/CETATEXT000035991004.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 08/11/2017, 402511</TITRE>
<DATE_DEC>2017-11-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>402511</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLEZ ; BALAT</AVOCATS>
<RAPPORTEUR>M. Clément Malverti</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:402511.20171108</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...et Mme C...B...ont demandé au tribunal administratif d'Orléans d'annuler pour excès de pouvoir la délibération du 20 mars 2014 par laquelle le conseil municipal de Dammarie (Eure-et-Loir) a approuvé le plan local d'urbanisme de cette commune. Par un jugement n° 1402490 du 31 mars 2015, le tribunal administratif d'Orléans a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 15NT01645 du 17 juin 2016, la cour administrative d'appel de Nantes a rejeté l'appel formé par M. et Mme B...contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 17 août et 17 novembre 2016 et le 17 juillet 2017 au secrétariat du contentieux du Conseil d'Etat, M. et Mme B...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de la commune de Dammarie la somme de 4 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Clément Malverti, auditeur, <br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boullez, avocat de M. et MmeB..., et à Me Balat, avocat de la commune de Dammarie ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une délibération du 20 mars 2014, le conseil municipal de Dammarie a approuvé le plan local d'urbanisme de la commune, lequel comprend notamment une orientation d'aménagement et de programmation relative au secteur " rue de Concrez / rue de Rigauderie " ; que cette orientation d'aménagement et de programmation prévoit notamment que l'aménagement du secteur " devra ménager la possibilité de réaliser une sortie sur la rue de Concrez " et comporte un plan sur lequel est indiquée une " liaison ultérieure possible avec la rue de Concrez " ; que M. et MmeB..., qui possèdent un terrain situé sur l'emprise de cette liaison, ont demandé au tribunal administratif d'Orléans d'annuler pour excès de pouvoir la délibération du 20 mars 2014  approuvant le plan local d'urbanisme, en particulier cette orientation d'aménagement et de programmation ; que leur demande a été rejetée par un jugement du 31 mars 2015 ; que M. et Mme B...se pourvoient en cassation contre l'arrêt du 17 juin 2016 par lequel la cour administrative d'appel de Nantes a rejeté leur appel contre ce jugement ; <br/>
<br/>
              2.	Considérant qu'en vertu de l'article L. 123-1 du code de l'urbanisme, dans sa rédaction applicable en l'espèce, le plan local d'urbanisme " comprend un rapport de présentation, un projet d'aménagement et de développement durables, des orientations d'aménagement et de programmation, un règlement et des annexes ", chacun de ces éléments pouvant comprendre un ou plusieurs documents graphiques ; que l'article L. 123-1-3 du même code, alors applicable, prévoit que le projet d'aménagement et de développement durables définit les orientations générales des politiques d'aménagement, d'équipement, d'urbanisme, de protection des espaces naturels, agricoles et forestiers, et de préservation ou de remise en bon état des continuités écologiques, qu'il arrête les orientations générales concernant l'habitat, les transports et les déplacements, le développement des communications numériques, l'équipement commercial, le développement économique et les loisirs et qu'il fixe des objectifs de modération de la consommation de l'espace et de lutte contre l'étalement urbain ; qu'aux termes de l'article L. 123-1-4 du code de l'urbanisme, dans sa rédaction applicable au litige : " Dans le respect des orientations définies par le projet d'aménagement et de développement durables, les orientations d'aménagement et de programmation comprennent des dispositions portant sur l'aménagement, l'habitat, les transports et les déplacements. / 1. En ce qui concerne l'aménagement, les orientations peuvent définir les actions et opérations nécessaires pour mettre en valeur l'environnement, les paysages, les entrées de villes et le patrimoine, lutter contre l'insalubrité, permettre le renouvellement urbain et assurer le développement de la commune. / Elles peuvent comporter un échéancier prévisionnel de l'ouverture à l'urbanisation des zones à urbaniser et de la réalisation des équipements correspondants. / Elles peuvent porter sur des quartiers ou des secteurs à mettre en valeur, réhabiliter, restructurer ou aménager. / Elles peuvent prendre la forme de schémas d'aménagement et préciser les principales caractéristiques des voies et espaces publics. / 2. En ce qui concerne l'habitat, elles définissent les objectifs et les principes d'une politique visant à répondre aux besoins en logements et en hébergements,  à favoriser le renouvellement urbain et la mixité sociale et à améliorer l'accessibilité du cadre bâti aux personnes handicapées en assurant entre les communes et entre les quartiers d'une même commune une répartition équilibrée et diversifiée de l'offre de logements (...) " ; qu'aux termes de l'article L. 123-1-5 du même code, dans sa rédaction applicable au litige : " Le règlement [du plan local d'urbanisme] fixe, en cohérence avec le projet d'aménagement et de développement durables, les règles générales et les servitudes d'utilisation des sols permettant d'atteindre les objectifs mentionnés à l'article L. 121-1, qui peuvent notamment comporter l'interdiction de construire, délimitent les zones urbaines ou à urbaniser et les zones naturelles ou agricoles et forestières à protéger et définissent, en fonction des circonstances locales, les règles concernant l'implantation des constructions. / A ce titre, le règlement peut : / (...) 8° Fixer les emplacements réservés aux voies et ouvrages publics, aux installations d'intérêt général ainsi qu'aux espaces verts (...) " ; <br/>
<br/>
              3.	Considérant que l'article L. 123-5 du code de l'urbanisme, dans sa rédaction applicable au litige, dispose que : " Le règlement et ses documents graphiques sont opposables à toute personne publique ou privée pour l'exécution de tous travaux, constructions, plantations, affouillements ou exhaussements des sols, pour la création de lotissements et l'ouverture des installations classées appartenant aux catégories déterminées dans le plan. / Ces travaux ou opérations doivent en outre être compatibles, lorsqu'elles existent, avec les orientations d'aménagement mentionnées à l'article L. 123-1-4 et avec leurs documents graphiques " ; <br/>
<br/>
              4.	Considérant qu'il résulte de ces dernières dispositions que les travaux ou opérations d'urbanisme doivent être compatibles avec les orientations d'aménagement et de programmation ; que si de telles orientations, dans cette mesure opposables aux demandes d'autorisations d'urbanisme, sont, en principe, susceptibles d'être contestées par la voie du recours pour excès de pouvoir à l'occasion d'un recours dirigé contre la délibération qui approuve le plan local d'urbanisme, il en va différemment dans le cas où les orientations adoptées, par leur teneur même, ne sauraient justifier légalement un refus d'autorisation d'urbanisme ; <br/>
<br/>
              5.	Considérant, en l'espèce, que pour juger irrecevables les conclusions de M. et Mme B...tendant à l'annulation de l'orientation d'aménagement et de programmation relative au secteur " rue de Concrez / rue de Rigauderie " en ce qu'elle prévoit une liaison empiétant sur leur propriété, la cour administrative d'appel a retenu que la délimitation de cette liaison ne pouvait être assimilée à la création d'un emplacement réservé, relevé qu'elle n'était pas reportée sur les documents graphiques du plan local d'urbanisme et estimé qu'elle était insusceptible de créer par elle-même des obligations pour les propriétaires des parcelles concernées, pour en déduire que cette orientation ne constituait qu'une prévision ne faisant pas grief aux requérants ; <br/>
<br/>
              6.	Considérant, d'une part, que la cour n'a pas inexactement interprété les documents constituant le plan local d'urbanisme attaqué et n'a pas commis d'erreur de droit en jugeant que l'orientation contestée ne pouvait être assimilée à la création d'un emplacement réservé, au sens du 8° de l'article L. 123-1-5 du code de l'urbanisme et en refusant de la tenir pour une servitude au sens de l'article L. 123-2 du même code ;<br/>
<br/>
              7.	Considérant, d'autre part, que la cour ne s'est pas livrée à une inexacte interprétation du plan local d'urbanisme et de la portée de l'orientation critiquée en relevant que cette dernière n'était pas reportée aux documents graphiques du plan local d'urbanisme et en retenant qu'elle ne constituait qu'une simple prévision insusceptible de faire par elle-même grief ; qu'en jugeant en conséquence irrecevables les conclusions tendant à l'annulation de cette orientation, la cour n'a pas commis d'erreur de droit ; <br/>
<br/>
              8.	Considérant qu'il résulte de ce qui précède que M. et Mme B...ne sont pas fondés à demander l'annulation de l'arrêt qu'ils attaquent, lequel est suffisamment motivé ; que leurs conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, en conséquence, qu'être rejetées ; qu'en revanche il y a lieu, dans les circonstances de l'espèce, de mettre à leur charge la somme de 3 000 à verser à la commune de Dammarie au titre de ces dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi de M. et Mme B...est rejeté.<br/>
<br/>
Article 2 : M. et Mme B...verseront à la commune de Dammarie une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. A...B..., à Mme C...B...et à la commune de Dammarie. Copie en sera adressée au ministre d'Etat, ministre de la transition écologique et solidaire.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-01-01-02-019 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PLANS D'AMÉNAGEMENT ET D'URBANISME. PLANS D`OCCUPATION DES SOLS (POS) ET PLANS LOCAUX D'URBANISME (PLU). APPLICATION DES RÈGLES FIXÉES PAR LES POS OU LES PLU. PORTÉE DES DIFFÉRENTS ÉLÉMENTS DU PLAN. - ORIENTATIONS D'AMÉNAGEMENT ET DE PROGRAMMATION - POSSIBILITÉ DE CONTESTER LES ORIENTATIONS D'AMÉNAGEMENT ET D'ORIENTATION À L'OCCASION D'UN RECOURS CONTRE LA DÉLIBÉRATION APPROUVANT LE PLU - EXISTENCE, SI ELLES SONT OPPOSABLES AUX DEMANDES D'AUTORISATION D'URBANISME - ABSENCE DANS LE CAS OÙ ELLES NE SAURAIENT JUSTIFIER LÉGALEMENT UN REFUS D'AUTORISATION D'URBANISME.
</SCT>
<ANA ID="9A"> 68-01-01-02-019 Il résulte des dispositions de l'article L. 123-5 du code de l'urbanisme que les travaux ou opérations d'urbanisme doivent être compatibles avec les orientations d'aménagement et de programmation. Si de telles orientations, dans cette mesure opposables aux demandes d'autorisations d'urbanisme, sont, en principe, susceptibles d'être contestées par la voie du recours pour excès de pouvoir à l'occasion d'un recours dirigé contre la délibération qui approuve le plan local d'urbanisme, il en va différemment dans le cas où les orientations adoptées, par leur teneur même, ne sauraient justifier légalement un refus d'autorisation d'urbanisme.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
