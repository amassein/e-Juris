<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044806231</ID>
<ANCIEN_ID>JG_L_2021_12_000000450193</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/80/62/CETATEXT000044806231.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 30/12/2021, 450193, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>450193</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Cécile Chaduteau-Monplaisir</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:450193.20211230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire en réplique et un nouveau mémoire, enregistrés les 26 février, 9 juillet et 3 septembre 2021 au secrétariat du contentieux du Conseil d'Etat, le syndicat Les Entreprises du médicament (LEEM) demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision de rejet implicite de sa demande d'abrogation des dispositions du a) du 11° de l'article 1er du décret n° 2020-1090 du 25 août 2020 portant diverses mesures relatives à la prise en charge des produits de santé en tant qu'elles modifient le I de l'article R. 163-5 du code de la sécurité sociale pour introduire un renvoi " à l'article L. 5123-2 du code de la santé publique " ; <br/>
<br/>
              2°) d'enjoindre au Premier ministre d'abroger les dispositions litigieuses, dans un délai de 15 jours à compter de la notification de la décision à venir, sous astreinte de 1 500 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la directive 89/105/CEE du Conseil du 21 décembre 1988 ;  <br/>
              - le code de la santé publique ;<br/>
              - le code de la sécurité sociale ;<br/>
              - la loi n° 2020-1525 du 7 décembre 2020 ; <br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Chaduteau-Monplaisir, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Le a) du 11° de l'article 1er du décret du 25 août 2020 portant diverses mesures relatives à la prise en charge des produits de santé a modifié les dispositions du I de l'article R. 163-5 du code de la sécurité sociale pour remplacer, au premier et au dernier alinéa de ce I, les mots : " la liste prévue au premier alinéa de l'article L. 162-17 " par les mots : " les listes ou l'une des listes prévues aux premier et deuxième alinéas de l'article L. 162-17 du présent code et à l'article L. 5123-2 du code de la santé publique ". La liste prévue au premier alinéa de l'article L. 162-17 est celle des médicaments délivrés en officine remboursables aux assurés sociaux, celle prévue au deuxième alinéa de cet article est celle des médicaments pris en charge ou remboursés par l'assurance maladie lorsqu'ils sont délivrés par une pharmacie à usage intérieur d'un établissement de santé, enfin, celle prévue à l'article L. 5123-2 du code de la santé publique est celle des médicaments agréés à l'usage des collectivités publiques. Le I de l'article R. 163-5 du code de la sécurité sociale dispose désormais que : " ne peuvent être inscrits sur les listes ou l'une des listes prévues aux premier et deuxième alinéas de l'article L. 162-17 du présent code et à l'article L. 5123-2 du code de la santé publique, le cas échéant pour certaines de leurs indications seulement : / 1° Les médicaments dont les éléments de conditionnement, l'étiquetage ou la notice, définis à l'article R. 5000 du code de la santé publique, ou la publicité auprès des professionnels de santé font mention d'une utilisation non thérapeutique ou sans visée diagnostique ; / 2° Les médicaments qui n'apportent ni amélioration du service médical rendu appréciée par la commission mentionnée à l'article R. 163-15 [c'est-à-dire la commission de la transparence] ni économie dans le coût du traitement médicamenteux ; / 3° Les médicaments susceptibles d'entraîner des hausses de consommation ou des dépenses injustifiées ; / 4° Les médicaments dont le prix proposé par l'entreprise ne serait pas justifié eu égard aux critères prévus au I et au II de l'article L. 162-16-4 ; / 5° Les médicaments dont les forme, dosage ou présentation ne sont pas justifiés par l'utilisation thérapeutique ou diagnostique. / (...) ". <br/>
<br/>
              2. Il ressort des pièces du dossier que le pouvoir réglementaire a entendu, par la modification introduite par le décret du 25 août 2020, prévoir que les motifs de refus énoncés à l'article R. 163-5 du code de la sécurité sociale pour l'inscription des spécialités sur la liste établie, en vertu de l'article L. 162-17 du code de la sécurité sociale, en vue de leur prise en charge ou leur remboursement par les caisses d'assurance maladie lorsqu'elles sont dispensées en officine, peuvent s'appliquer, notamment, à l'inscription sur la liste établie par le ministre chargé de la santé et le ministre chargé de la sécurité sociale des produits agréés, sur le fondement de l'article L. 5123-2 du code de la santé publique, pour l'achat, la fourniture, la prise en charge et l'utilisation des spécialités pharmaceutiques par les collectivités publiques. Le syndicat Les Entreprises du médicament (LEEM) demande au Conseil d'Etat d'annuler pour excès de pouvoir le refus du Premier ministre d'abroger le I de l'article R. 163-5 du code la sécurité sociale en tant qu'il renvoie " à l'article L. 5123-2 du code de la santé publique ".<br/>
<br/>
              3. En premier lieu, en vertu de l'article L. 5123-2 du code de la santé publique, l'achat, la fourniture, la prise en charge et l'utilisation des spécialités pharmaceutiques par les collectivités publiques sont limités à des produits agréés " dans les conditions propres à ces médicaments fixées par le décret mentionné à l'article L. 162-17 du code de la sécurité sociale ", c'est-à-dire le décret en Conseil d'Etat qui fixe les conditions selon lesquelles les médicaments sont inscrits sur la liste mentionnée à l'article L. 162-17 du code de la sécurité sociale. Par suite le syndicat requérant n'est pas fondé à soutenir que les dispositions litigieuses sont dénuées de base légale.<br/>
<br/>
              4. En second lieu, à la différence des dispositions du 1° de l'article R. 163-5 du code de la sécurité sociale, qui se rapportent à l'hypothèse où les éléments de conditionnement, l'étiquetage ou la notice ou encore la publicité auprès des professionnels de santé font mention d'une utilisation non thérapeutique ou sans visée diagnostique, ou de celles du 5° de cet article, qui se rapportent à l'hypothèse dans laquelle les forme, dosage ou présentation des médicaments ne sont pas justifiés par l'utilisation thérapeutique ou diagnostique, celles du 2°, 3° et 4° prévoient chacune un motif de refus d'inscription fondé sur l'objectif de maîtrise des dépenses de santé, se rapportant, pour chacun de ces alinéas, à des situations distinctes. En vertu du 2°, un refus doit être opposé à la demande d'inscription d'un médicament qui n'apporte aucune amélioration du service médical rendu, telle qu'elle est appréciée par la commission de la transparence, lorsqu'il ne permet pas à l'assurance maladie, eu égard au prix fixé pour cette spécialité, de réaliser une économie dans le coût du traitement médicamenteux par rapport à celui d'une spécialité déjà inscrite sur la liste. En vertu du 3°, un refus doit de même être opposé à la demande d'inscription d'une spécialité qui est susceptible d'entraîner des hausses de consommation ou des dépenses injustifiées pour l'assurance maladie au regard de son utilité pour la santé publique, eu égard par exemple aux charges d'exploitation afférentes, à ses conditions de forme, de dosage ou de présentation ou aux conditions dans lesquelles elle est prescrite. Enfin, en vertu du 4°, un refus doit être opposé si le prix proposé par l'entreprise qui exploite le médicament en vue de la conclusion avec le Comité économique des produits de santé de la convention fixant ce prix prévue à l'article L. 162-16-4 du code de la sécurité sociale n'est pas justifié au regard des critères fixés à cet article.<br/>
<br/>
              5. Les dispositions du 2° et du 4° de l'article R. 163-5 du code de la sécurité sociale impliquent, ainsi, que soient connus, soit le prix fixé pour la spécialité pharmaceutique concernée, soit le prix proposé par l'entreprise qui exploite le médicament en vue de la conclusion de la convention avec le Comité économique des produits de santé. Dans le cas où est sollicitée son inscription sur la liste mentionnée à l'article L. 5123-2 du code de la santé publique en vue de son achat, sa fourniture, sa prise en charge ou son utilisation par les collectivités publiques, une spécialité pharmaceutique ne fait cependant l'objet ni d'un prix, ni d'une proposition de prix, celui-ci résultant, postérieurement à son inscription sur la liste, d'une procédure d'achat public menée par les établissements de santé ou leurs groupements. En outre, le prix fixé ou proposé dans le cadre d'une demande d'inscription sur la liste mentionnée à l'article L. 162-17 du code de la sécurité sociale, dont l'objet diffère de celui de la liste mentionnée à l'article L. 5123-2 du code de la santé publique, ne pouvant utilement être pris en considération, le syndicat requérant est fondé à soutenir que les dispositions du 2° de l'article R. 163-5 ne peuvent, pas davantage au demeurant que celles du 4° de cet article, trouver à s'appliquer à une demande d'inscription sur la liste mentionnée à l'article L. 5123-2 du code de la santé publique, que cette inscription soit seule demandée ou qu'elle le soit simultanément avec celle sur la liste mentionnée à l'article L. 162-17 du code de la sécurité sociale. <br/>
<br/>
              6. Toutefois, la seule circonstance que l'article R. 163-5 du code de la sécurité sociale énonce, depuis sa modification par le a) du 11° de l'article 1er du décret du 25 août 2020, sous forme d'une liste unique, l'ensemble des motifs susceptibles de fonder un refus d'inscription sur les trois listes auxquelles il s'applique ne porte atteinte, par elle-même, du fait que certains de ces motifs ne seraient pas dans certains cas susceptibles d'être opposés pour refuser l'inscription sur l'une de ces listes, ni à la distinction entre la liste mentionnée au premier alinéa de l'article L. 162-17 du code de la sécurité sociale et la liste mentionnée à l'article L. 5123-2 du code de la santé publique, ni, en tout état de cause, à la libre détermination des prix par les collectivités publiques lors de l'achat des spécialités inscrites sur la liste mentionnée à l'article L. 5123-2 du code de la santé publique ou à la liberté du commerce et de l'industrie.<br/>
<br/>
              7. Les dispositions du 2° de l'article R. 163-5 du code de la sécurité sociale n'étant, comme il a été dit, pas susceptibles de s'appliquer à une demande d'inscription sur la liste mentionnée à l'article L. 5123-2 du code de la santé publique, qu'elle soit seule demandée ou qu'elle le soit simultanément avec celle sur la liste mentionnée au premier alinéa de l'article L. 162-17 du code de la sécurité sociale, le syndicat requérant ne peut utilement soutenir que l'application de ce motif de refus dans ces hypothèses méconnaîtrait, du fait de l'absence d'un prix fixé ou proposé pour la spécialité en cause, le 2) de l'article 6 de la directive 89/105/CEE du Conseil du 21 décembre 1988 concernant la transparence des mesures régissant la fixation des prix des médicaments à usage humain et leur inclusion dans le champ d'application des systèmes d'assurance-maladie, qui impose que toute décision refusant d'inscrire un médicament sur la liste des produits couverts par le système d'assurance maladie comporte un exposé des motifs fondé sur des critères objectifs et vérifiables. <br/>
<br/>
              8. Il résulte de tout ce qui précède que, sans qu'il soit besoin de se prononcer sur la fin de non-recevoir opposée par le ministre des solidarités et de la santé, les conclusions du syndicat LEEM, y compris celles présentées au titre de l'article L. 761-1 du code de justice administrative, doivent être rejetées.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête du syndicat Les Entreprises du médicament est rejetée.<br/>
Article 2 : La présente décision sera notifiée au syndicat Les Entreprises du médicament et au ministre des solidarités et de la santé.<br/>
Copie en sera adressée au Premier ministre. <br/>
              Délibéré à l'issue de la séance du 29 novembre 2021 où siégeaient : Mme Maud Vialettes, présidente de chambre, présidant ; Mme Gaëlle Dumortier, présidente de chambre ; Mme A... C..., Mme E... G..., M. F... D..., M. Damien Botteghi, conseillers d'Etat et Mme Cécile Chaduteau-Monplaisir, maître des requêtes-rapporteure. <br/>
<br/>
<br/>
Rendu le 30 décembre 2021.<br/>
<br/>
<br/>
                 La présidente : <br/>
                 Signé : Mme Maud Vialettes<br/>
<br/>
 		La rapporteure : <br/>
      Signé : Mme Cécile Chaduteau-Monplaisir<br/>
<br/>
<br/>
                 La secrétaire :<br/>
                 Signé : Mme H... B...<br/>
<br/>
<br/>
<br/>
La République mande et ordonne au ministre des solidarités et de la santé en ce qui le concerne ou à tous huissiers de justice à ce requis en ce qui concerne les voies de droit commun contre les parties privées, de pourvoir à l'exécution de la présente décision.<br/>
			Pour expédition conforme,<br/>
			Pour la secrétaire du contentieux, par délégation :<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
