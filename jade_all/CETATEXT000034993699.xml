<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034993699</ID>
<ANCIEN_ID>JG_L_2017_06_000000399704</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/99/36/CETATEXT000034993699.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 21/06/2017, 399704, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-06-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>399704</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ZRIBI, TEXIER</AVOCATS>
<RAPPORTEUR>M. Stéphane Hoynck</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:399704.20170621</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé à la Cour nationale du droit d'asile d'annuler la décision du 13 juillet 2015 par laquelle le directeur général de l'Office français de protection des réfugiés et apatrides (OFPRA) a rejeté sa demande d'asile. Par une décision n° 15022667 du 21 décembre 2015, la Cour nationale du droit d'asile a rejeté la demande que lui avait présentée M.A....<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 11 mai et 11 août 2016 au secrétariat du contentieux du Conseil d'Etat, M.A..., représenté par la SCP Zribi et Texier, demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision n° 15022667 du 21 décembre 2015 de la Cour nationale du droit d'asile ;<br/>
<br/>
              2°) de mettre à la charge de l'OFPRA la somme de 3 000 euros au titre de l'article 37 de la loi du 10 juillet 1991 et de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Convention de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la Convention de Genève du 28 juillet 1951 relative au statut des réfugiés ;<br/>
              - la loi du 10 juillet 1991 relative à l'aide juridique et le décret du 19 décembre 1991;<br/>
              - le code d'entrée et de séjour des étrangers et des demandeurs d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Stéphane Hoynck, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Zribi et Texier, avocat de M. B...A...;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis à la Cour nationale du droit d'asile que M.A..., de nationalité sri-lankaise, s'est vu refuser la qualité de réfugié par une décision du 13 juillet 2015 du directeur général de l'Office français de protection des réfugiés et apatrides. Il se pourvoit en cassation contre la décision du 21 décembre 2015 par laquelle la Cour nationale du droit d'asile rejeté sa demande d'annulation de cette décision.<br/>
<br/>
              2. Aux termes des dispositions du paragraphe A, 2° de l'article 1er de la convention de Genève du 28 juillet 1951 et du protocole signé à New York le 31 janvier 1967, doit être considérée comme réfugiée toute personne qui " craignant avec raison d'être persécutée du fait de sa race, de sa religion, de sa nationalité, de son appartenance à un certain groupe social ou de ses opinions politiques, se trouve hors du pays dont elle a la nationalité et qui ne peut, ou, du fait de cette crainte, ne veut se réclamer de la protection de ce pays ". Aux termes de 1'article L. 712-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, " sous réserve des dispositions de l'article L. 712-2, le bénéfice de la protection subsidiaire est accordé à toute personne qui ne remplit pas les conditions d'octroi du statut de réfugié énoncées à l'alinéa précédent et qui établit qu'elle est exposée dans son pays à l'une des menaces graves suivantes : a) la peine de mort ; b) la torture ou des peines ou traitements inhumains ou dégradants (...;) ".<br/>
<br/>
              3. Il appartient à la Cour nationale du droit d'asile, qui statue comme juge de plein contentieux sur le recours d'un demandeur d'asile dont la demande a été rejetée par l'OFPRA, de se prononcer elle-même sur le droit de l'intéressé à la qualité de réfugié ou, à défaut, de la protection subsidiaire, au vu de l'ensemble des circonstances de fait dont elle a connaissance au moment où elle statue. A ce titre il lui revient, pour apprécier la réalité des risques invoqués par le demandeur, de prendre en compte l'ensemble des pièces que celui-ci produit à l'appui de ses prétentions. En particulier, lorsque le demandeur produit devant elle des pièces qui comportent des éléments circonstanciés en rapport avec les risques allégués, il lui incombe, après avoir apprécié si elle doit leur accorder crédit et les avoir confrontées aux faits rapportés par le demandeur, d'évaluer les risques qu'elles sont susceptibles de révéler et, le cas échéant, de préciser les éléments qui la conduisent à ne pas regarder ceux-ci comme sérieux.<br/>
<br/>
              4. Pour rejeter la demande de M.A..., qui soutenait être exposé à des risques en raison des soupçons qu'ont nourris les militaires sri-lankais quant à son appartenance au mouvement des Tigres de libération de l'Eelam tamoul, la Cour a estimé que la tentative d'assassinat dont il aurait été victime est restée inexpliquée et que les craintes alléguées ne sauraient être tenues pour établies. En statuant de la sorte, alors que M. A...avait produit un certificat médical fort circonstancié délivré le 21 novembre 2015, et qui figure au dossier, selon lequel M. A...présente de nombreuses cicatrices compatibles avec son récit, la Cour nationale du droit d'asile, qui n'a pas cherché à évaluer les risques que cette pièce était susceptible de révéler ni précisé les éléments qui la conduisaient à ne pas les regarder comme sérieux, a commis une erreur de droit.<br/>
<br/>
              5. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, que M. A...est fondé à demander l'annulation de la décision qu'il attaque.<br/>
<br/>
              6. M. A...a obtenu le bénéfice de l'aide juridictionnelle. Par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a donc lieu, dans les circonstances de l'espèce, et sous réserve que SCP Zribi et Texier renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Office français de protection des réfugiés et apatrides le versement à cette SCP de la somme de 3 000 euros.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La décision de la Cour nationale du droit d'asile du 21 décembre 2015 est annulée. <br/>
Article 2 : L'affaire est renvoyée à la Cour nationale du droit d'asile. <br/>
Article 3 : L'Office français de protection des réfugiés et apatrides versera à l'avocat de M. A..., la SCP Zribi et Texier, sous réserve qu'elle renonce à l'indemnité due au titre de l'aide juridictionnelle totale, la somme de 3 000 euros au titre du 2ème alinéa de l'article 37 de la loi du 10 juillet 1991. <br/>
Article 4 : La présente décision sera notifiée à M. A...et au directeur général de l'Office français de protection des réfugiés et apatrides.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
