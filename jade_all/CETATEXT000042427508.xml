<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042427508</ID>
<ANCIEN_ID>JG_L_2020_10_000000426241</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/42/75/CETATEXT000042427508.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 14/10/2020, 426241, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-10-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>426241</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR>Mme Catherine Moreau</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHR:2020:426241.20201014</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° Sous le n° 426241, par une requête sommaire et un mémoire complémentaire, enregistrés les 13 décembre 2018 et 13 mars 2019 au secrétariat du contentieux du Conseil d'Etat, l'Association Agir Espèces, la World pheasant association France, l'Association Aviornis France international, l'Association nationale des chasseurs de gibiers d'eaux, le Club des éleveurs d'oiseaux exotiques, la Fédération française d'aquariophilie, la Fédération française d'ornithologie, la Fédération nationale des chasseurs, la Fédération nationale des métiers de la jardinerie, le syndicat professionnel des métiers et services de l'animal familial, l'Union internationale pour l'étude et la préservation des psittacidae sud-américains et l'Union ornithologique de France demandent au Conseil d'Etat d'annuler pour excès de pouvoir l'arrêté du 8 octobre 2018 du ministre de la transition écologique et solidaire et du ministre de l'agriculture et de l'alimentation fixant les règles générales de détention d'animaux d'espèces non domestiques. <br/>
<br/>
<br/>
              2° Sous le n° 426253, par une requête et un nouveau mémoire, enregistrés le 13 décembre 2018 et 29 janvier 2020 au secrétariat du contentieux du Conseil d'Etat, les associations One Voice, Sea Shepherd France, Le Biome, Centre Athenas et Wildlife Angel demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 8 octobre 2018 fixant les règles générales de détention d'animaux d'espèces non domestiques ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - la convention sur le commerce international des espèces de faune et de flore sauvages menacées d'extinction signée à Washington le 3 mars 1973 ;<br/>
              - le traité sur le fonctionnement de l'Union européenne ; <br/>
              - le règlement (CE) n° 338/97 du 9 décembre 1996 relatif à la protection des espèces de faune et de flore sauvages par le contrôle de leur commerce ; <br/>
              - le règlement (CE) n° 865/2006 du 4 mai 2006 portant modalités d'application du règlement (CE) n° 338/97 ;<br/>
              - la directive n° 2009/147/CE du Parlement européen et du Conseil du 30 novembre 2009 concernant la conservation des oiseaux sauvages ;<br/>
              - le code de l'environnement ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le code rural et de la pêche maritime ; <br/>
              - l'arrêt C-100 /08 du 10 septembre 2009 de la Cour de justice des Communautés européennes ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme B... A..., conseillère d'Etat en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Stéphane Hoynck, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de l'association Agir Espèce et autres et à la SCP Rousseau, Tapie avocat de la Société d'actions et de promotion vétérinaires ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Les requêtes de l'association Agir espèces et autres et de l'association One Voice et autres demandent l'annulation pour excès de pouvoir du même arrêté du 8 octobre 2018 fixant les règles générales de détention d'animaux d'espèces non domestiques. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2. L'association Wildlife Angel a déclaré se désister de son action. Ce désistement est pur et simple et rien ne s'oppose à ce qu'il en soit donné acte. <br/>
<br/>
              3. La Société d'actions et de promotion vétérinaires justifiant d'un intérêt suffisant au maintien de l'arrêté du 8 octobre 2018 fixant les règles générales de détention d'animaux d'espèces non domestiques, son intervention est recevable.<br/>
<br/>
              4. Aux termes de l'article L. 412-1 du code de l'environnement, dans sa version issue de la loi du 8 août 2016 pour la reconquête de la biodiversité, de la nature et des paysages : " La production, le ramassage, la récolte, la capture, la détention, la cession à titre gratuit ou onéreux, à travers tout support, y compris numérique, l'utilisation, le transport, l'introduction quelle qu'en soit l'origine, l'importation sous tous régimes douaniers, l'exportation, la réexportation de tout ou partie d'animaux d'espèces non domestiques et de leurs produits ainsi que de tout ou partie de végétaux d'espèces non cultivées et de leurs produits, dont la liste est fixée par arrêtés conjoints du ministre chargé de l'environnement et, en tant que de besoin, du ou des ministres compétents, s'ils en font la demande, sont soumis, suivant la gravité de leurs effets sur l'état de conservation des espèces concernées et des risques qu'ils présentent pour la santé, la sécurité et la salubrité publiques, à déclaration ou à autorisation de l'autorité administrative délivrée dans les conditions et selon les modalités fixées par un décret en Conseil d'Etat (...) ". <br/>
<br/>
              5. Pour l'application de ces dispositions, l'article R. 412-1 du code de l'environnement prévoit que : " Les arrêtés prévus à l'article L. 412-1, pris par le ministre chargé de l'environnement, précisent les espèces ou les catégories de spécimens d'animaux non domestiques et de végétaux non cultivés concernés, les activités soumises à autorisation ou à déclaration (...) ". Aux termes de l'article R. 412-1-1 du même code : " Le ministre chargé de l'environnement fixe par arrêté : / 1° la forme des déclarations et des demandes d'autorisation ; / 2° les caractéristiques auxquelles doivent répondre les installations de production et de détention ainsi que les règles générales dans le respect desquelles doivent s'exercer les activités mentionnées à l'article L. 412-1 (...) ". Les articles R. 412-5 et R. 412-6 du même code prévoient que la déclaration doit être adressée à l'autorité compétente dans le territoire concerné, qu'elle peut être transmise par télé service et qu'elle donne lieu, selon les cas, soit, lorsqu'elle est incomplète, à un simple accusé de réception indiquant les pièces manquantes, soit, lorsqu'elle est complète, à un récépissé de déclaration qui indique la date à laquelle, en l'absence d'opposition, l'opération projetée pourra être entreprise ou bien l'absence d'opposition qui permet d'entreprendre cette opération sans délai. Il est en outre prévu que l'absence de réponse de l'administration pendant un mois vaut délivrance du récépissé. Enfin, l'article R. 413-43 du même code prévoit que des arrêtés conjoints des ministres chargés de la protection de la nature et de l'agriculture fixent les règles de détention des animaux dans les établissements soumis aux dispositions de ce chapitre du code de l'environnement.<br/>
<br/>
              6. C'est pour l'application de ces dispositions qu'a été pris l'arrêté attaqué, qui détermine les règles générales de détention d'animaux d'espèces non domestiques. Il fixe notamment, par son chapitre premier, des règles communes à la détention de ces animaux, en particulier des règles d'identification par marquage et enregistrement dans un fichier national. Il définit, par son chapitre 2, des règles applicables aux procédures préalables à la détention d'animaux d'espèces non domestiques, prévoyant notamment que, lorsque la détention d'animaux est faite dans un but lucratif ou de négoce, elle est soumise à un régime d'autorisation mais que, lorsque tel n'est pas le cas, la procédure applicable dépend de l'espèce et du nombre de spécimens détenus, par référence à un tableau figurant à son annexe 2.<br/>
<br/>
              Sur la légalité externe de l'arrêté attaqué :<br/>
<br/>
              7. En premier lieu, l'arrêté attaqué a été signé conjointement par le ministre de la transition écologique et solidaire et par le ministre de l'agriculture et de l'alimentation, conformément à ce que prévoient les articles L. 412-1, R. 413-9 et R. 413-43 du code de l'environnement. Si les requérantes font grief à la procédure d'adoption de l'arrêté de n'avoir pas été conduite conjointement par les ministres chargés de l'environnement et de l'agriculture, aucune disposition n'édicte de prescription particulière à cet égard. Le moyen ne peut, dès lors, qu'être écarté.<br/>
<br/>
              8. En second lieu, préalablement à l'édiction de l'arrêté attaqué, les ministres de la transition écologique et solidaire et de l'agriculture et de l'alimentation ont organisé une consultation du public en application des dispositions de l'article L. 132-1 du code des relations entre le public et l'administration, aux termes desquelles : " Lorsque l'administration est tenue de procéder à la consultation d'une commission consultative préalablement à l'édiction d'un acte réglementaire, elle peut décider d'organiser une consultation ouverte permettant de recueillir, sur un site internet, les observations des personnes concernées. / Cette consultation ouverte se substitue à la consultation obligatoire en application d'une disposition législative ou réglementaire. Les commissions consultatives dont l'avis doit être recueilli en application d'une disposition législative ou réglementaire peuvent faire part de leurs observations dans le cadre de la consultation prévue au présent article ". Cette consultation s'est ainsi substituée aux consultations auxquelles les auteurs de cet arrêté étaient tenus de procéder, notamment celle du Conseil national de la chasse et de la faune sauvage. La circonstance que, contrairement à ce que prévoient les dispositions de l'article L. 132-2 du même code, la synthèse des observations et propositions du public n'aurait pas été rendue publique avant la signature de l'arrêté attaqué est, par elle-même, sans incidence sur la légalité de cet arrêté.<br/>
<br/>
              Sur la légalité interne de l'arrêté attaqué : <br/>
<br/>
              En ce qui concerne l'article 1er de l'arrêté attaqué : <br/>
<br/>
              9. En imposant à toute personne, physique ou morale, qui détient en captivité des animaux d'espèces non domestiques de disposer d'un lieu d'hébergement, d'installations et d'équipements conçus " pour garantir le bien-être des animaux hébergés, c'est-à-dire satisfaire à leurs besoins physiologiques et comportementaux ", sans définir en quoi consiste le bien-être des animaux hébergés, les dispositions du II de l'article 1er de l'arrêté litigieux n'ont pas méconnu le principe de clarté et d'intelligibilité de la norme.<br/>
<br/>
              En ce qui concerne les obligations de marquage et de déclaration de marquage des animaux d'espèces non domestiques résultant des articles 3, 4 et 7 de l'arrêté attaqué : <br/>
<br/>
              10. L'article 3 de l'arrêté attaqué soumet les mammifères, oiseaux, reptiles et amphibiens des espèces ou groupes d'espèces inscrits sur les listes établies en application des articles L. 411-1 et L. 411-2 du code de l'environnement ou sur les listes des annexes A à D du règlement n° 338/97 du 9 décembre 1996 relatif à la protection des espèces de faune et de flore sauvages par le contrôle de leur commerce, à une obligation de marquage individuel et permanent, effectué, selon les procédés et les modalités techniques définis à l'annexe 1 de l'arrêté, sous la responsabilité du propriétaire, dans le délai d'un mois suivant leur naissance. Le IV de l'article 4 de l'arrêté attaqué prévoit toutefois que l'obligation de marquage des animaux provenant d'un pays autre que la France dans les huit jours suivant leur arrivée au lieu de détention ne s'applique pas aux animaux provenant d'un Etat membre de l'Union européenne et déjà marqués par un procédé de marquage approuvé par les autorités de cet Etat conformément aux dispositions de l'article 66 du règlement (CE) n° 865/2006 du 4 mai 2006 portant modalités d'application du règlement (CE) n° 338/97. Enfin, ce même article 4 prévoit des méthodes d'identification pour certains animaux en cas d'impossibilité biologique de procéder à leur marquage.<br/>
<br/>
              11. En premier lieu, contrairement à ce que soutiennent l'association Agir espèces et autres, le règlement (CE) n° 338/97 ne comporte pas de disposition relative au marquage des animaux. Et, pour sa part, l'article 64 du règlement (CE) n° 865/2006, qui prévoit que, pour qu'un permis d'importation dans l'Union européenne puisse être délivré, le demandeur doit démontrer à l'organe de gestion compétent que les spécimens ont été marqués individuellement conformément à son article 66, ne limite pas cette obligation aux spécimens d'espèces figurant dans l'annexe A du règlement (CE) n° 338/97. <br/>
<br/>
              12. En deuxième lieu, ainsi que l'a jugé la Cour de justice des Communautés européennes dans son arrêt Commission contre Belgique (C-100 /08) du 10 septembre 2009, les mesures de protection prévues par le règlement (CE) n° 338/97 ne font pas obstacle au maintien ou à l'établissement, par chaque Etat membre, de mesures de protection renforcées, qui doivent être compatibles avec l'article 28 du traité instituant la Communauté européenne, devenu l'article 34 du traité sur le fonctionnement de l'Union européenne. Or, l'obligation de marquage posée par l'article 3 de l'arrêté attaqué est destinée à satisfaire à l'une des raisons d'intérêt général mentionnées à l'article 36 du traité, à savoir la protection des animaux et plus particulièrement la lutte contre le trafic et les prélèvements illicites dans la nature. L'obligation de marquage et d'enregistrement imposée par l'arrêté attaqué est proportionnée aux objectifs recherchés de protection des animaux et, dès lors qu'elle s'applique indistinctement aux animaux nés sur le territoire français et à ceux nés à l'étranger, ne constitue pas une entrave prohibée aux échanges entre les Etats membres par l'article 34 du traité sur le fonctionnement de l'Union européenne.<br/>
<br/>
              13. En troisième lieu, les dispositions de l'article 3 de l'arrêté attaqué, applicables à toutes les formes de détention d'animaux non domestiques, s'appliquent sans préjudice de celles de l'article R. 413-30 du code de l'environnement qui prévoient le marquage des animaux détenus dans un établissement d'élevage, de vente et de transit des espèces de gibier dont la chasse est autorisée. <br/>
<br/>
              14. En quatrième lieu, les auteurs de l'arrêté attaqué ont pu légalement prévoir, au II de son article 4, eu égard aux motifs pris en considération et sans commettre d'erreur d'appréciation au regard de l'article L. 411-2 du code de l'environnement, que le marquage d'un animal puisse être différé dans le cas de détention en semi-liberté ou en groupe, ou lorsque la capture présente un risque pour l'animal ou la sécurité des intervenants, un tel marquage devant en tout état de cause intervenir avant la sortie de l'animal pour une nouvelle destination. Ils n'ont pas davantage commis d'erreur d'appréciation en dispensant, au III de l'article 3, de l'obligation de marquage les spécimens qu'il est prévu de relâcher dans le milieu naturel.<br/>
<br/>
              15. En cinquième lieu, le I de l'article 7 de l'arrêté attaqué prévoit que les personnes ayant procédé au marquage des animaux établissent une déclaration de marquage. Le II du même article prévoit que lorsque les animaux sont déjà marqués, le propriétaire procède à l'inscription de l'animal dans le fichier national d'identification des animaux d'espèces non domestiques ou adresse au gestionnaire de ce fichier une copie de la déclaration de marquage. Il résulte de ces dispositions, combinées avec celles du IV de l'article 4 de l'arrêté qui prévoit, ainsi qu'il a été dit, que, pour les animaux provenant d'un pays autre que la France, le marquage doit être effectué dans les huit jours suivant leur arrivée au lieu de détention, cette dernière obligation n'étant pas applicable aux animaux déjà marqués provenant d'un Etat membre de l'Union européenne, que la déclaration de marquage n'est pas exigée lors de l'importation de spécimens déjà marqués en provenance d'autres pays de l'Union. Le moyen tiré de ce qu'une déclaration de marquage exigée lors de l'importation constituerait une restriction aux échanges entre les Etats membres doit donc être écarté. <br/>
<br/>
              16. En dernier lieu, l'association Agir espèces et autres ne peuvent utilement soutenir que les dispositions de l'article 7 de l'arrêté attaqué, en imposant l'enregistrement des spécimens détenus dans un fichier national d'identification et leur identification par transpondeur à radiofréquences ou par des bagues fermées, aurait pour effet de faire perdre toute rentabilité à la vente de nombreuses espèces sur le territoire français.<br/>
<br/>
              En ce qui concerne les procédures préalables à la détention d'animaux d'espèces non domestiques prévues par les articles 12, 13 et 14 et l'annexe 2 de l'arrêté attaqué : <br/>
<br/>
              17. L'article 12 de l'arrêté attaqué prévoit que la détention en captivité d'animaux d'espèces non domestiques n'est soumise ni à déclaration ni à autorisation lorsque ne sont détenus que des animaux des espèces ou groupes d'espèces dont la liste figure dans l'annexe 2 dans la limite des effectifs fixés dans la colonne (a) de cette annexe et que la détention de ces animaux n'a pas de but lucratif ou de négoce, leur reproduction ne pouvant en particulier pas avoir pour objectif la production habituelle de spécimens destinés à la vente. Aux termes du dernier alinéa de cet article 12 : " Les effectifs des animaux appartenant à une espèce ou à un groupe d'espèces qui relève, quel que soit l'effectif détenu, de la colonne (a) de l'annexe 2 ne sont pas pris en compte dans l'appréciation des seuils mentionnés aux (ii) et (iii) de l'article 14. ".  <br/>
<br/>
              18. L'article 13 de l'arrêté prévoit que la détention en captivité d'animaux d'espèces non domestiques est soumise à déclaration lorsque ne sont détenus que des animaux des espèces ou groupes d'espèces dont la liste figure dans l'annexe 2 dans la limite des effectifs fixés dans la colonne (b) de cette annexe et que la détention de ces animaux n'a pas de but lucratif ou de négoce, leur reproduction ne pouvant en particulier pas avoir pour objectif la production habituelle de spécimens destinés à la vente. <br/>
<br/>
              19. L'article 14 de l'arrêté prévoit que la détention en captivité d'animaux d'espèces non domestiques est soumise à autorisation lorsque l'une au moins des conditions suivantes est satisfaite : " (i) l'élevage porte sur des animaux d'espèces ou groupes d'espèces inscrites à la colonne (c) de l'annexe 2 et les effectifs détenus sont égaux ou supérieurs à la valeur mentionnée dans cette même colonne ; / (ii) le nombre d'animaux adultes hébergés excède 40 pour les mammifères, 100 pour les oiseaux, 40 pour les reptiles et 40 pour les amphibiens ; / (iii) le nombre total d'animaux adultes hébergés excède 40 lorsqu'ils appartiennent à plusieurs des classes zoologiques mentionnées au (ii) ;/ (iv) l'élevage est pratiqué dans un but lucratif (...) ". <br/>
<br/>
              20. L'annexe 2 de l'arrêté attaqué, à laquelle renvoient ses articles 12, 13 et 14, comporte trois colonnes correspondant chacune aux modalités régissant la détention des animaux des espèces qui y sont mentionnées en fonction des effectifs d'animaux adultes détenus que sont l'absence de formalité préalable, la déclaration préalable et l'autorisation. Il en résulte que la détention d'animaux d'une même espèce est possible sans formalité préalable lorsque l'effectif des spécimens est inférieur ou égal au nombre indiqué dans la colonne a) alors qu'au-dessus de cet effectif, leur détention peut être soumise soit à un régime de déclaration, correspondant à la colonne b), soit à autorisation, correspondant à la colonne c).<br/>
<br/>
              21. En premier lieu, les dispositions de l'article L. 412-1 du code de l'environnement renvoient au pouvoir règlementaire le soin de définir les régimes applicables à la capture, la détention, la cession, l'importation et l'exportation des animaux d'espèces non domestiques en fonction de la gravité de leurs effets sur l'état de conservation des espèces concernées et des risques qu'ils présentent pour la santé, la sécurité et la salubrité publiques. Par suite, le moyen tiré de ce que l'arrêté attaqué méconnaîtrait les dispositions législatives et réglementaires en application desquelles elles ont été prises au motif qu'il ne soumettrait pas toute détention d'animaux non domestiques à un régime d'autorisation préalable ne peut qu'être écarté.<br/>
<br/>
              22. En deuxième lieu, si les associations One Voice et autres soutiennent qu'en permettant que la détention d'animaux qui, en application des arrêtés qu'abroge l'arrêté attaqué, devait être autorisée, ne fasse plus l'objet que d'une simple déclaration, voire d'aucune formalité préalable, cet arrêté méconnaîtrait le principe de non régression de la protection de l'environnement, il résulte toutefois des dispositions de l'article L. 412-1, telles qu'elles ont été modifiées par la loi du 8 août 2016 pour la reconquête de la biodiversité, de la nature et des paysages, que le législateur a entendu établir un dispositif faisant dépendre les régimes juridiques applicables à la capture, la détention, la cession, l'importation et l'exportation des animaux d'espèces non domestiques de la gravité de leurs effets sur l'état de conservation des espèces concernées et des risques qu'ils présentent pour la santé, la sécurité et la salubrité publiques et qu'il a confié au pouvoir règlementaire le soin de définir ces régimes. Par suite, la méconnaissance des dispositions générales du 9° de l'article L. 110-1 du code de l'environnement posant le principe de non-régression de la protection de l'environnement ne peut être utilement invoquée à l'encontre de l'arrêté fixant, pour l'application des dispositions de l'article L. 412-1 du code de l'environnement, les règles générales de détention d'animaux d'espèces non domestiques.<br/>
<br/>
              23. En troisième lieu, si la directive 2009/147/CE du 30 novembre 2009 proscrit la détention de certaines espèces d'oiseaux sauvages, à l'exclusion des oiseaux nés et élevés en captivité, son article 9 permet cependant aux Etats membres de prévoir des dérogations à cette interdiction dans les conditions qu'il fixe. Dès lors que l'arrêté attaqué ne permet pas de se dispenser de l'obtention, dans les conditions prévues par l'article L. 411-2 du code de l'environnement, d'une dérogation pour la détention d'espèces d'oiseaux protégées par la directive, les associations One Voice et autres ne sont pas fondées à soutenir que cet arrêté méconnaîtrait les objectifs des articles 5, 6 et 11 de cette directive.<br/>
<br/>
              24. En quatrième lieu, le règlement (CE) n° 338/97 du Conseil du 9 décembre 1996 comporte l'inscription, à son annexe A, des animaux des espèces menacées d'extinction qui sont ou pourraient être affectées par le commerce et qu'il est interdit d'acheter, d'acquérir à des fins commerciales, d'exposer à des fins commerciales, d'utiliser dans un but lucratif et de vendre, de détenir pour la vente, de mettre en vente ou de transporter pour la vente. Son article 8 prévoit cependant qu'il peut être dérogé à ces interdictions notamment lorsque les spécimens sont destinés à l'élevage ou à la reproduction et contribueront de ce fait à la conservation des espèces concernées. En outre, ce règlement n'impose pas aux Etats membres d'obligations procédurales particulières pour la détention de ces animaux. <br/>
<br/>
              25. D'une part, si l'arrêté attaqué prévoit que la détention d'animaux de certaines espèces figurant à l'annexe A du règlement (CE) n° 338/97 en deçà d'un nombre déterminé de spécimens fait simplement l'objet d'une déclaration ou ne fait l'objet d'aucune formalité, les associations One Voice et autres ne sont pas fondées à soutenir qu'il méconnaîtrait de ce fait les dispositions du règlement (CE) n° 338/97 ou des dispositions législatives ou réglementaires d'un niveau supérieur, le changement de régime applicable à la détention d'animaux d'espèces non domestiques ne portant pas, par lui-même, atteinte à l'objectif de protection de la nature et des animaux.<br/>
<br/>
              26. Mais, d'autre part, le tableau annexé à l'arrêté qui détermine le régime de détention applicable aux différentes espèces en fonction des effectifs d'animaux concernés se réfère exclusivement aux animaux adultes. Aucune disposition n'étant applicable à la détention de spécimens n'ayant pas encore atteint l'âge adulte, il résulte de l'arrêté qu'elle est dispensée de toute formalité préalable. Dans ces conditions, eu égard à la nécessité de lutter contre le trafic d'animaux y compris de spécimens n'ayant pas atteints l'âge adulte, les associations One Voice et autres sont fondées à soutenir que les auteurs de l'arrêté litigieux ont, ce faisant, méconnu les exigences découlant des dispositions de l'article L. 412-1 du code de l'environnement.<br/>
<br/>
              27. En cinquième lieu, aux termes du III de l'article L. 424-8 du code de l'environnement : " Le transport, la vente, la mise en vente, la détention pour la vente et l'achat des animaux vivants ou morts d'espèces dont la chasse est autorisée et qui sont nés et élevés en captivité sont libres toute l'année ". Aux termes de l'article L. 214-2 du code rural et de la pêche maritime : " Tout homme a le droit de détenir des animaux dans les conditions définies à l'article L. 214-1 et de les utiliser dans les conditions prévues à l'article L. 214-3, sous réserve des droits des tiers et des exigences de la sécurité et de l'hygiène publique et des dispositions de la loi n° 76-629 du 10 juillet 1976 relative à la protection de la nature ", les dispositions de la loi du 10 juillet 1976 étant désormais codifiées aux articles L. 412-1 et suivants du code de l'environnement.<br/>
<br/>
              28. Il résulte des dispositions précitées de l'article L. 214-2 du code rural et de la pêche maritime que le législateur autorise la libre détention des animaux sous réserve de la possibilité que prévoit l'article L. 412-1 du code de l'environnement de soumettre à un régime d'autorisation la détention d'animaux d'espèces non domestiques pour tenir compte de la gravité des effets de cette détention sur l'état de conservation des espèces concernées et des risques qu'ils présentent pour la santé, la sécurité et la salubrité publiques. L'association Agir espèces et autres ne sont donc pas fondées à soutenir que les articles 12, 13 et 14 de l'arrêté attaqué, pris pour l'application de l'article L. 412-1 du code de l'environnement, méconnaîtraient les dispositions du III de l'article L. 424-8 du code de l'environnement. <br/>
<br/>
              29. En sixième lieu, le dernier alinéa de l'article 14 de l'arrêté attaqué interdit d'exposer en vue de la vente des animaux appartenant à une espèce ou à un groupe d'espèces qui relève, dès le premier spécimen détenu, de la colonne c) de l'annexe 2. L'article 7 du règlement (CE) n° 338/97 prévoit que " a) à l'exception de l'application de l'article 8, les spécimens d'espèces inscrites à l'annexe A qui sont nés et élevés en captivité sont traités conformément aux dispositions applicables aux spécimens des espèces inscrites à l'annexe B ". L'article 8 du même règlement interdit notamment d'exposer à des fins commerciales des spécimens d'espèces inscrites à l'annexe A. Il résulte de ces dispositions que l'interdiction d'exposer en vue de la vente des spécimens d'espèces inscrites dans l'annexe A du règlement s'applique à tous les spécimens de ces espèces sans qu'il y ait lieu de les distinguer selon qu'ils ont été prélevés dans la nature ou qu'ils sont nés et ont été élevés en captivité. L'association Agir espèces et autres, qui ne soutiennent pas que des espèces inscrites, dès le premier spécimen détenu, dans le c) de l'annexe 2 de l'arrêté attaqué ne figureraient pas également dans l'annexe A du règlement n° 338/97, ne sont pas fondés à soutenir qu'en n'excluant pas les spécimens nés et élevés en captivité de l'interdiction qu'il pose, le dernier alinéa de l'article 14 de l'arrêté attaqué constituerait une entrave injustifiée à l'importation et à la commercialisation de ces espèces, contraire au droit de l'Union. Eu égard à l'objet de ses dispositions destinées à protéger les espèces animales menacées d'extinction, le dernier alinéa de l'article 14 de l'arrêté attaqué ne porte pas non plus, par lui-même, d'atteinte illégale à la liberté du commerce et de l'industrie. <br/>
<br/>
              30. Enfin, contrairement à ce que soutiennent l'association Agir espèces et autres, les articles 12, 13 et 14 de l'arrêté attaqué et son annexe 2 n'ont ni pour objet ni pour effet d'imposer des quotas de détention pour certaines espèces. <br/>
<br/>
              En ce qui concerne les autres dispositions de l'arrêté attaqué : <br/>
<br/>
              31. En premier lieu, l'article 10 de l'arrêté attaqué soumet la cession de tout animal vivant appartenant à une espèce protégée en application des articles L. 411-1 du code de l'environnement ou figurant à l'annexe A du règlement n° 338/97 à l'établissement d'une attestation de cession sur laquelle doivent figurer différentes informations dont " le mode et le numéro de marquage de l'animal cédé, le cas échéant ". Pour les animaux des autres espèces, le II prévoit une attestation de cession " sur laquelle figurent a minima les informations suivantes : - les noms scientifique et vernaculaire de l'espèce à laquelle appartient l'animal cédé ; / - le nom ou la raison sociale et les coordonnées complètes du cédant ; / - le nom ou la raison sociale et les coordonnées complètes du cessionnaire ; / - la date, le lieu et les conditions financières de la cession " et précise que cette attestation de cession peut prendre la forme d'un ticket de caisse ou d'une facture. Ces dispositions ne méconnaissent pas celles de l'article L. 413-7 du code de l'environnement qui prévoient que toute cession, à titre gratuit ou onéreux, d'un animal vivant d'une espèce non domestique doit s'accompagner, au moment de la livraison à l'acquéreur de la délivrance d'une attestation de cession.<br/>
<br/>
              32. En second lieu, si les associations requérantes critiquent la suppression de certaines des formalités que prévoyaient des arrêtés abrogés par l'arrêté attaqué, ils n'assortissent pas ces moyens de précisions permettant d'en apprécier le bien-fondé.<br/>
<br/>
              Sur l'étendue et les conséquences de l'annulation prononcée : <br/>
<br/>
              33. Il résulte de tout ce qui précède que les requérantes ne sont fondées à demander l'annulation de l'arrêté attaqué qu'en tant que son annexe 2, ainsi qu'il a été dit au point 26, ne prévoit pas de formalité obligatoire préalable à la détention des animaux appartenant à des espèces protégées ou dangereuses n'ayant pas atteint l'âge adulte. <br/>
<br/>
              34. Aux termes de l'article. L. 911-1 du code de justice administrative : " Lorsque sa décision implique nécessairement qu'une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public prenne une mesure d'exécution dans un sens déterminé, la juridiction, saisie de conclusions en ce sens, prescrit, par la même décision, cette mesure assortie, le cas échéant, d'un délai d'exécution. / La juridiction peut également prescrire d'office cette mesure. "<br/>
<br/>
              35. Il y a lieu, dans les circonstances de l'espèce, d'enjoindre à la ministre de la transition écologique et au ministre de l'agriculture et de l'alimentation de modifier l'annexe II de l'arrêté du 8 octobre 2018 afin d'y prendre en compte les animaux n'ayant pas atteint l'âge adulte, dans un délai de trois mois à compter de la notification de la présente décision. <br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              36. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative à verser solidairement aux associations One Voice et autres. En revanche, la Société d'actions et de promotion vétérinaires, en sa qualité d'intervenant en défense, n'ayant pas la qualité de partie au litige, les conclusions qu'elle a présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent qu'être rejetées.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il est donné acte du désistement de l'association Wildlife Angel.<br/>
Article 2 : L'intervention de la Société d'actions et de promotion vétérinaires est admise.<br/>
Article 3 : L'annexe 2 de l'arrêté du 8 octobre 2018 est annulée en tant qu'elle ne prévoit aucune formalité préalable pour la détention des animaux non domestiques n'ayant pas atteint l'âge adulte.<br/>
Article 4 : Il est enjoint aux ministre de la transition écologique et de l'agriculture et de l'alimentation de modifier l'annexe II de l'arrêté du 8 octobre 2018 afin d'y prendre en compte les animaux n'ayant pas atteint l'âge adulte, dans un délai de trois mois à compter de la notification de la présente décision.<br/>
Article 5 : L'Etat versera à l'association One Voice et autres la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 6 : Le surplus des conclusions de la requête de l'association One Voice et autres et la requête de l'association Agir espèces et autres sont rejetés.<br/>
Article 7 : Les conclusions de la Société d'actions et de promotion vétérinaires présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 8 : La présente décision sera notifiée à l'association Agir espèces, première dénommée pour l'ensemble des associations requérantes sous le n° 426241, à l'association One Voice, première dénommée pour l'ensemble des associations requérantes sous le n° 426253, à la ministre de la transition écologique, au ministre de l'agriculture et de l'alimentation et à la Société d'actions et de promotion vétérinaires.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
