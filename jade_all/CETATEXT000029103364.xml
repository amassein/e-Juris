<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029103364</ID>
<ANCIEN_ID>JG_L_2014_06_000000368512</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/10/33/CETATEXT000029103364.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  7ème sous-section jugeant seule, 18/06/2014, 368512, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-06-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>368512</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 7ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>M. François Lelièvre</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:368512.20140618</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 14 mai et 13 août 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour la chambre des métiers et de l'artisanat de l'Hérault, dont le siège est 44 avenue Saint-Lazare à Montpellier (34000) ; la chambre des métiers et de l'artisanat de l'Hérault demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10MA04386 du 5 mars 2013 par lequel la cour administrative d'appel de Marseille a, d'une part, annulé le jugement du tribunal administratif de Montpellier du 21 septembre 2010 rejetant la demande de M. C...B...A...tendant à la condamnation de la chambre des métiers et de l'artisanat de l'Hérault à lui verser la somme de 15 000 euros en réparation des préjudices qu'il aurait subis en raison du harcèlement moral dont il aurait été victime, et, d'autre part, condamné la chambre des métiers et de l'artisanat de l'Hérault à verser 15 000 euros à M. B...A...; <br/>
<br/>
              2°) de mettre à la charge de M. B...A...la somme de 7 000 euros et 35 euros au titre des articles L. 761-1 et R. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 52-1311 du 10 décembre 1952 ; <br/>
<br/>
              Vu la loi n° 83-634 du 13 juillet 1983 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Lelièvre, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de la chambre des métiers et de l'artisanat de l'Hérault, et à la SCP Didier, Pinet, avocat de M. B...A...;<br/>
<br/>
<br/>
<br/>1. Considérant que, pour faire droit aux conclusions d'indemnisation présentées par M. B...A..., chef de service à la chambre des métiers et de l'artisanat de l'Hérault, la cour administrative d'appel de Marseille a estimé, à la différence du tribunal administratif de Montpellier, que M. B...A...avait été victime de faits constitutifs de harcèlement moral de la part des autorités de cette chambre à compter de l'année 2005 et jusqu'en 2007 ; que celle-ci se pourvoit en cassation contre cet arrêt ;<br/>
<br/>
              2. Considérant qu'aux termes du premier alinéa de l'article 6 quinquies de la loi du 13 juillet 1983 : " Aucun fonctionnaire ne doit subir les agissements répétés de harcèlement moral qui ont pour objet ou pour effet une dégradation des conditions de travail susceptible de porter atteinte à ses droits et à sa dignité, d'altérer sa santé physique ou mentale ou de compromettre son avenir professionnel. " ; que, pour être qualifiés de harcèlement moral, de tels faits répétés doivent excéder les limites de l'exercice normal du pouvoir hiérarchique ; que dès lors qu'elle n'excède pas ces limites, une simple diminution des attributions justifiée par l'intérêt du service, en raison d'une manière de servir inadéquate ou de difficultés relationnelles, n'est pas constitutive de harcèlement moral ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que si M. B...A...a vu en 2005 et 2006 certains éléments de son évaluation professionnelle se dégrader ou ne pas progresser par rapport aux années précédentes, au titre desquelles au demeurant des efforts supplémentaires lui avaient été demandés sur certains points et ses attributions modifiées du fait de la réorganisation de l'ensemble des services engagée à la suite de l'élection consulaire de 2005, ces faits ne caractérisent pas, dans les circonstances où ils sont intervenus, des mesures excédant le pouvoir hiérarchique et le pouvoir d'organisation du service ; que la circonstance qu'un audit diligenté par la mission d'inspection des chambres de commerce et d'industrie et des métiers et de l'artisanat à compter de la fin de l'année 2009 ait critiqué l'organisation mise en place par le nouveau président de la chambre, comme celle que certains des agents de la chambre aient estimé que l'intéressé avait " subi des pressions " ne permettaient pas non plus de caractériser, en l'espèce, des faits constitutifs de harcèlement moral ; que par suite la chambre des métiers et de l'artisanat de l'Hérault est fondée à soutenir que la cour administrative d'appel de Marseille a inexactement qualifié les faits qui lui étaient soumis en les regardant comme constitutifs de harcèlement moral et à demander, par suite, l'annulation de l'arrêt attaqué ; <br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              5. Considérant que M. B...A...fait valoir que l'absence d'augmentation de sa rémunération en 2007 à la différence des autres chefs de service, le refus de lui attribuer un téléphone portable, la diminution de ses responsabilités, les propos émanant de sa hiérarchie à son égard comme les mauvaises évaluations professionnelles dont il a fait l'objet en 2005 et 2006, établissent l'existence d'un harcèlement moral lui ayant causé les préjudices dont il demande réparation ; <br/>
<br/>
              6. Considérant, d'une part, qu'ainsi qu'il a été dit au point 3 et que l'a relevé le tribunal administratif le contenu des notations pour les années 2005 et 2006 ainsi que la modification des attributions du requérant ne caractérisent pas, dans les circonstances de l'espèce, un harcèlement moral ; que, d'autre part, eu égard notamment aux différences de fonction et de situation des autres chefs de service, l'absence d'augmentation indiciaire du requérant ne résulte d'aucune discrimination illégale ; qu'il ne ressort pas des pièces du dossier que la circonstance qu'il ne soit pas doté d'un téléphone portable professionnel traduirait l'existence d'un comportement vexatoire de la part des autorités de la chambre ; que n'établit pas plus, ainsi que l'a relevé le tribunal administratif, l'existence de tels comportement les propos tenus par le secrétaire général de la chambre à la suite d'une réunion amicale ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que M. B...A...n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif a rejeté ses conclusions à fin d'indemnisation ainsi que, par voie de conséquence, ses conclusions à fin d'injonction ;<br/>
<br/>
              8. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de M. B...A...les sommes que demande la chambre des métiers et de l'artisanat de l'Hérault au titre des dispositions des articles L. 761-1 et R. 761-1 du code de justice administrative ; que les dispositions de l'article L. 761-1 font obstacle à ce que soit mise à la charge de la chambre des métiers et de l'artisanat de l'Hérault, qui n'est pas, dans la présente instance, la partie perdante, la somme demandée par M. B...A...au titre des frais exposés par lui et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 5 mars 2013 est annulé.<br/>
Article 2 : la requête de M. B...A...devant la cour administrative d'appel de Marseille est rejetée.<br/>
Article 3 : Le surplus des conclusions du pourvoi de la chambre des métiers et de l'artisanat de l'Hérault et les conclusions présentées pour M. B...A...sur le fondement des articles L. 761-1 du code de justice administrative sont rejetés. <br/>
Article 4 : La présente décision sera notifiée à la chambre des métiers et de l'artisanat de l'Hérault et à M. C...B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
