<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044500337</ID>
<ANCIEN_ID>JG_L_2021_12_000000449243</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/50/03/CETATEXT000044500337.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 14/12/2021, 449243, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>449243</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; CABINET ROUSSEAU ET TAPIE</AVOCATS>
<RAPPORTEUR>M. Laurent Cabrera</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:449243.20211214</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B... A... a demandé au tribunal administratif de Nantes d'annuler pour excès de pouvoir la décision du 28 juillet 2017 par laquelle l'inspecteur du travail a autorisé la société Elior Restauration à procéder à son licenciement ainsi que la décision implicite de la ministre du travail rejetant son recours hiérarchique présenté le 26 septembre 2017. Par un jugement n° 1802121 du 15 février 2019, le tribunal administratif a fait droit à sa demande. <br/>
<br/>
              Par un arrêt n° 19NT01459 du 1er décembre 2020, la cour administrative d'appel de Nantes a, sur appel de la société Elres, qui vient aux droits de la société Elior Restauration, annulé ce jugement et rejeté la demande de Mme A.... <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 1er février et 3 mai 2021 au secrétariat du contentieux du Conseil d'Etat, Mme A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de rejeter l'appel de la société Elres ;<br/>
<br/>
              3°) de mettre à la charge de la société Elres la somme de 4 000 euros sur le fondement de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du travail ;<br/>
              - le code de justice administrative ; <br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Cabrera, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de Mme A... et au cabinet Rousseau et Tapie, avocat de la société Elres ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que par une décision du 28 juillet 2017, l'inspecteur du travail de l'unité de contrôle n° 2 au sein de l'unité départementale de Loire-Atlantique à la direction régionale des entreprises, de la concurrence, de la consommation, du travail et de l'emploi (DIRECCTE) des Pays-de-la-Loire a autorisé la société Elior Restauration à procéder au licenciement pour faute Mme B... A..., salariée protégée. Une décision implicite de rejet est née du silence gardé par la ministre du travail sur le recours hiérarchique formée par Mme A... contre cette autorisation. Par un jugement du 15 février 2019, le tribunal administratif de Nantes, sur demande de Mme A..., a annulé ces deux décisions. Mme A... se pourvoit en cassation contre l'arrêt du 1er décembre 2020 par lequel la cour administrative d'appel de Nantes a, sur la requête de la société Elres, qui vient aux droits de la société Elior Restauration, annulé ce jugement en ce qu'il a annulé les décisions de l'inspecteur du travail et de la ministre du travail autorisant son licenciement et rejeté les conclusions de sa demande.<br/>
<br/>
              2. Aux termes de l'article L. 1232-2 du code du travail : " L'employeur qui envisage de licencier un salarié le convoque, avant toute décision, à un entretien préalable ". Aux termes de l'article L. 1232-4 du même code : " Lors de son audition, le salarié peut se faire assister par une personne de son choix appartenant au personnel de l'entreprise. / Lorsqu'il n'y a pas d'institutions représentatives du personnel dans l'entreprise, le salarié peut se faire assister soit par une personne de son choix appartenant au personnel de l'entreprise, soit par un conseiller du salarié choisi sur une liste dressée par l'autorité administrative. / La lettre de convocation à l'entretien préalable adressée au salarié mentionne la possibilité de recourir à un conseiller du salarié et précise l'adresse des services dans lesquels la liste de ces conseillers est tenue à sa disposition ". Aux termes de l'article R. 1232-1 de ce code : " La lettre de convocation prévue à l'article L. 1232-2 indique l'objet de l'entretien entre le salarié et l'employeur. / (...) Elle rappelle que le salarié peut se faire assister pour cet entretien par une personne de son choix appartenant au personnel de l'entreprise ou, en l'absence d'institutions représentatives dans l'entreprise, par un conseiller du salarié ". Il résulte de ces dispositions que la lettre de convocation à l'entretien préalable au licenciement doit mentionner les modalités d'assistance du salarié applicables en fonction de la situation de l'entreprise. A ce titre, lorsque l'entreprise appartient à une unité économique et sociale (UES) dotée d'institutions représentatives du personnel, elle doit mentionner la possibilité pour le salarié convoqué de se faire assister par une personne de son choix appartenant au personnel de l'entreprise ou d'une autre entreprise appartenant à l'UES. Toutefois, la procédure n'est pas entachée d'irrégularité s'il est établi que le salarié a été pleinement informé, en temps utile, des modalités d'assistance auxquelles il avait droit, en fonction de la situation de l'entreprise, pour son entretien préalable.<br/>
<br/>
              3. Il résulte des termes mêmes de l'arrêt attaqué qu'après avoir relevé que l'employeur de Mme A... appartenait à une unité économique et sociale (UES) dotée d'institutions représentatives du personnel, la cour administrative d'appel de Nantes a jugé que Mme A... n'était pas fondée à soutenir que la procédure suivie par son employeur était irrégulière, bien que la convocation qui lui avait été adressée en vue de l'entretien préalable à son licenciement se bornait à mentionner sa faculté d'être assistée par une personne de son choix de l'entreprise, et non de l'UES, dès lors que les entités de l'UES constituent des entreprises distinctes et que Mme A... avait été, en l'espèce, assistée lors de son entretien par une personne de son entreprise. En statuant ainsi, alors qu'il lui appartenait de rechercher seulement s'il était établi que Mme A... avait été pleinement informée, en temps utile, des modalités d'assistance auxquelles elle avait droit, en fonction de la situation de son entreprise, pour son entretien préalable, la cour a entaché son arrêt d'erreur de droit. <br/>
<br/>
              4. Il résulte de ce qui précède que, sans qu'il soit besoin de se prononcer sur les autres moyens de son pourvoi, Mme A... est fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Elres une somme de 2 000 euros à verser à Mme A... au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces dispositions font, en revanche, obstacle à ce qu'une somme soit mise à ce titre à la charge de la requérante qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nantes du 1er décembre 2020 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nantes.<br/>
Article 3 : La société Elres versera à Mme A... une somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de la société Elres présentées au titre de l'article L.  761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à Mme B... A... et à la société Elres.<br/>
Copie en sera adressée à la ministre du travail, de l'emploi et de l'insertion.<br/>
<br/>
              Délibéré à l'issue de la séance du 9 novembre 2021 où siégeaient : Mme Maud Vialettes, présidente de chambre, présidant ; Mme Fabienne Lambolez, conseillère d'Etat et M. Laurent Cabrera, conseiller d'Etat-rapporteur. <br/>
<br/>
              Rendu le 14 décembre 2021.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
