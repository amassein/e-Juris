<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026480632</ID>
<ANCIEN_ID>JG_L_2012_10_000000359283</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/48/06/CETATEXT000026480632.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  3ème sous-section jugeant seule, 11/10/2012, 359283, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-10-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>359283</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 3ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Jean Courtial</PRESIDENT>
<AVOCATS>FOUSSARD ; SCP MASSE-DESSEN, THOUVENIN</AVOCATS>
<RAPPORTEUR>Mme Christine Allais</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:359283.20121011</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 10 mai et 25 mai 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. Roger A, demeurant ... ; M. A demande au Conseil d'Etat :  <br/>
<br/>
              1°) d'annuler l'ordonnance n° 1204962/9 du 27 mars 2012 par laquelle le juge des référés du tribunal administratif de Paris a rejeté sa demande tendant à ce que soit ordonnée la suspension de la décision du 9 février 2012 du maire de Paris l'admettant à faire valoir ses droits à la retraite d'office pour limite d'âge à compter du 12 février 2012 jusqu'à ce qu'il soit statué au fond sur la légalité de cette décision ; <br/>
<br/>
              2°) statuant en référé, de faire droit à ses conclusions de première instance ; <br/>
<br/>
              3°) de mettre à la charge de la ville de Paris le versement d'une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 84-834 du 13 septembre 1984 ;<br/>
<br/>
              Vu la loi n° 2010-1330 du 9 novembre 2010 ;<br/>
<br/>
              Vu le décret n° 2011-2103 du 30 décembre 2011 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Christine Allais, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de la SCP Masse-Dessen, Thouvenin, avocat de M. A et de Me Foussard, avocat de la ville de Paris,<br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Masse-Dessen, Thouvenin, avocat de M. A et à Me Foussard, avocat de la ville de Paris ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que M. A, fonctionnaire de la Ville de Paris, a, sur sa demande, obtenu une prolongation d'activité au-delà de l'âge limite de la retraite, qu'il a atteint le 12 août 2009, jusqu'au 12 février 2012 ; que l'intéressé ayant sollicité une nouvelle prolongation d'activité, le maire de Paris n'a pas fait droit à cette demande et l'a admis à faire valoir ses droits à la retraite, d'office, pour limite d'âge, à compter de cette dernière date ; que le 22 mars 2012, M. A a saisi le tribunal administratif de Paris d'une demande tendant à l'annulation de cet arrêté ; que le même jour, il a saisi le juge des référés du tribunal administratif de Paris d'une demande de suspension de l'exécution de cette même décision sur le fondement de l'article L. 521-1 du code de justice administrative ; que par une ordonnance n° 1204962 du 27 mars 2012, contre laquelle M. A se pourvoit en cassation, le juge des référés de ce tribunal a rejeté cette demande ; <br/>
<br/>
              Sans qu'il soit besoin d'examiner les autres moyens du pourvoi ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 1-1 de la loi n° 84-834 du 13 septembre 1984, dans sa rédaction issue de la loi n° 2003-775 du 21 août 2003 :  " Sous réserve des droits au recul des limites d'âge reconnus au titre des dispositions de la loi du 18 août 1936 concernant les mises à la retraite par ancienneté, les fonctionnaires dont la durée des services liquidables est inférieure à celle définie à l'article L. 13 du code des pensions civiles et militaires de retraite peuvent, lorsqu'ils atteignent les limites d'âge applicables aux corps auxquels ils appartiennent, sur leur demande, sous réserve de l'intérêt du service et de leur aptitude physique, être maintenus en activité. / La prolongation d'activité prévue à l'alinéa précédent ne peut avoir pour effet de maintenir le fonctionnaire concerné en activité au-delà de la durée des services liquidables prévue à l'article L. 13 du même code ni au-delà d'une durée de dix trimestres. / Cette prolongation d'activité est prise en compte au titre de la constitution et de la liquidation du droit à pension. " ;  <br/>
<br/>
              3. Considérant que M. A avait soutenu devant le juge des référés du tribunal administratif de Paris qu'en repoussant sa limite d'âge à 67 ans, la loi du 9 novembre 2010 portant réforme des retraites avait eu pour effet de lui rouvrir la possibilité, en application des dispositions précitées de l'article 1-1 du 13 septembre 1984, de bénéficier d'une nouvelle prolongation d'activité de dix trimestres à compter de la date à laquelle il avait atteint l'âge de 67 ans ; qu'en jugeant que l'intéressé, qui avait déjà bénéficié d'une prorogation de dix trimestres, avait atteint l'âge de 67 ans fixé comme date limite par la loi du 9 novembre 2010 et ne justifiait pas entrer dans les exceptions à cette limite au motif notamment qu'il occuperait un emploi fonctionnel, sans rechercher si les conditions posées par l'article 1-1 de la loi du 13 septembre 1984 pour bénéficier d'une prolongation d'activité en dépit de cette limite d'âge étaient remplies, le juge des référés du tribunal administratif de Paris a commis une erreur de droit ; que, par suite, M. A est fondé à demander l'annulation de l'ordonnance qu'il attaque ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de statuer sur la demande de suspension en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
               5. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision. " ;<br/>
<br/>
              6. Considérant que si M. A soutient que sa limite d'âge aurait été portée à 67 ans par la loi susvisée du 9 novembre 2010, il résulte des dispositions de l'article 28 de cette loi que le relèvement de la limite d'âge à 67 ans ne s'applique qu'aux fonctionnaires nés à compter du 1er janvier 1956 ; que l'intéressé, né le 12 août 1944, ne peut pas non plus bénéficier des mesures transitoires prévues par l'article 8 du décret du 30 décembre 2011 pris en application de l'article 28 de cette loi du 9 novembre 2010 ; qu'il ressort des pièces du dossier que M. A, dont la limite d'âge est restée fixée à 65 ans, a déjà bénéficié d'une prolongation d'activité de dix trimestres au-delà de cette limite d'âge en application des dispositions précitées de l'article 1-1 de la loi du 13 septembre 1984 ; <br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que <br/>
M. A ne fait état d'aucun moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision du 9 février 2012 du maire de Paris l'admettant à faire valoir ses droits à la retraite d'office à compter du 12 février 2012 ; que, dès lors, sa demande de suspension de cette décision doit être rejetée ; <br/>
<br/>
              8. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'il soit fait droit aux conclusions présentées à ce titre par M. A ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées à ce titre par la Ville de Paris ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 27 mars 2012 du juge des référés du tribunal administratif de Paris est annulée. <br/>
Article 2 : La demande de suspension présentée par M. A devant le juge des référés du tribunal administratif de Paris est rejetée.<br/>
Article 3 : Les conclusions présentées par M. A et par la Ville de Paris au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à M. Roger A et à la Ville de Paris.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
