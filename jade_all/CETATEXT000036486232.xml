<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036486232</ID>
<ANCIEN_ID>JG_L_2018_01_000000406847</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/48/62/CETATEXT000036486232.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 12/01/2018, 406847</TITRE>
<DATE_DEC>2018-01-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>406847</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>Mme Déborah Coricon</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:406847.20180112</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 13 janvier et 30 octobre 2017 au secrétariat du contentieux du Conseil d'Etat, le Syndicat des vins de Bugey demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du ministre de l'économie et des finances et du ministre de l'agriculture, de l'agroalimentaire et de la forêt, porte-parole du Gouvernement en date du 16 novembre 2016 homologuant le cahier des charges relatif à l'appellation d'origine contrôlée " Clairette de Die ", en tant qu'il homologue celles des dispositions du cahier des charges de cette appellation d'origine contrôlée relatives aux " vins mousseux rosés " ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le règlement (CE) n° 1234/ 2007 du Conseil du 22 octobre 2007 ;<br/>
<br/>
              Vu le règlement n° 1308/2013 du Parlement européen et du Conseil du 17 décembre 2013 ; <br/>
<br/>
              Vu la loi n° 27-1286 du 20 décembre 1957 ;<br/>
<br/>
              Vu le code de la consommation ; <br/>
<br/>
              Vu le code rural et de la pêche maritime ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Deborah Coricon, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat du Syndicat des vins de Bugey et à la SCP Didier, Pinet, avocat de l'institut national de l'origine et de la qualité ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 22 décembre 2017, présentée par l'Institut national de l'origine et de la qualité ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 641-5 du code rural et de la pêche maritime : " Peuvent bénéficier d'une appellation d'origine contrôlée les produits agricoles, forestiers ou alimentaires et les produits de la mer, bruts ou transformés, qui remplissent les conditions fixées par les dispositions de l'article L. 115-1 du code de la consommation, possèdent une notoriété dûment établie et dont la production est soumise à des procédures comportant une habilitation des opérateurs, un contrôle des conditions de production et un contrôle des produits ". Aux termes de l'article L. 115-1 du code de la consommation, dans la rédaction à laquelle renvoie l'article L. 641-5 précité : " Constitue une appellation d'origine la dénomination d'un pays, d'une région ou d'une localité servant à désigner un produit qui en est originaire et dont la qualité ou les caractères sont dus au milieu géographique, comprenant des facteurs naturels ou des facteurs humains ". Il résulte par ailleurs du règlement n° 1308/2013 du Parlement européen et du Conseil du 17 décembre 2013 portant organisation commune des  marchés des produits agricoles, reprenant la substance du a) du paragraphe 1 de l'article 118 ter du règlement n° 1234 /2007 du Conseil du 22 octobre 2007 et du 2 de l'article 118 quater du même règlement, que le cahier des charges d'une appellation d'origine protégée doit comporter les éléments permettant de corroborer le lien entre, d'une part, la qualité et les caractéristiques de l'appellation et, d'autre part, le milieu géographique particulier ainsi que les facteurs naturels et humains qui lui sont inhérents.<br/>
<br/>
              2. L'arrêté du 16 novembre 2016, publié au Journal officiel de la République française du 26 novembre 2016, a homologué le cahier des charges de l'appellation d'origine contrôlée (AOC) " Clairette de Die " et abrogé le précédent cahier des charges de cette appellation. Le Syndicat des vins de Bugey, qui a notamment pour objet la défense et la valorisation de l'AOC " Bugey Cerdon ", demande l'annulation pour excès de pouvoir de cet arrêté. Il ressort des termes de sa requête que celle-ci doit être regardée comme demandant l'annulation de l'arrêté en tant seulement qu'il étend aux vins mousseux de couleur rosé la possibilité de se prévaloir de l'AOC " Clairette de Die ", auparavant réservée aux seuls vins mousseux blancs.<br/>
<br/>
              3. Il ressort des pièces du dossier que la nouvelle version du cahier des charges de l'AOC " Clairette de Die " a pour objet principal d'élargir à la production de vin mousseux rosés l'appellation " Clairette de Die " complétée de la mention " méthode ancestrale ". Il résulte toutefois de la comparaison des versions antérieures et actuelle du cahier des charges que les modifications apportées se contentent d'indiquer, d'une part, qu'en marge de la culture largement majoritaire des cépages blancs depuis l'antiquité subsiste une présence de cépages rouges et, d'autre part, qu'une dénomination présentée comme antérieure de l'appellation actuelle, le vin de Claret du XVIème siècle, aurait probablement été un vin blanc et " un vin rouge très clair ou rosé ", sans qu'aucun élément historique ou factuel ne vienne étayer ces affirmations et justifier qu'elles aient été écartées des précédentes versions des cahiers des charges depuis la reconnaissance de l'appellation par le décret du 30 décembre 1942, alors surtout que cette production est interdite sur l'aire géographique de l'AOC depuis la loi du 20 décembre 1957, dont l'article 1er ne réserve la possibilité d'élaborer des vins mousseux sur cette aire qu'à ceux bénéficiant de la reconnaissance de l'appellation à la date d'intervention de cette loi. Par suite, le ministre de l'économie et des finances et le ministre de l'agriculture, de l'agroalimentaire et de la forêt, porte-parole du Gouvernement ont commis une erreur d'appréciation en estimant que la condition d'antériorité était remplie et en approuvant ce nouveau cahier des charges.<br/>
<br/>
              4. Il suit de là que, sans qu'il soit besoin d'examiner les autres moyens de la requête, le Syndicat des vins de Bugey est fondé à demander l'annulation de l'arrêté qu'il attaque en tant qu'il homologue les dispositions du cahier des charges litigieux autorisant les vins mousseux de couleur rosé issus des zones qu'il définit à se prévaloir de cette appellation d'origine contrôlée. <br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser au Syndicat des vins de Bugey au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêté du ministre de l'économie et des finances et du ministre de l'agriculture, de l'agroalimentaire et de la forêt, porte-parole du Gouvernement en date du 16 novembre 2016 relatif à l'appellation d'origine contrôlée " Clairette de Die " est annulé en tant qu'il homologue celles des dispositions du cahier des charges de cette appellation d'origine contrôlée relatives aux " vins mousseux rosés ".<br/>
Article 2 : L'Etat versera une somme de 3 000 euros au Syndicat des vins de Bugey au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3: La présente décision sera notifiée au Syndicat des vins de Bugey, au ministre de l'économie et des finances, au ministre de l'agriculture et de l'alimentation et à l'Institut national de l'origine et de la qualité. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">03-05-06-02 AGRICULTURE ET FORÊTS. PRODUITS AGRICOLES. VINS. CONTENTIEUX DES APPELLATIONS. - CONDITION D'ANTÉRIORITÉ DU PRODUIT À LAQUELLE EST SUBORDONNÉ LE BÉNÉFICE D'UNE AOC - CONTRÔLE DU JUGE - CONTRÔLE NORMAL.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-02-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR. APPRÉCIATIONS SOUMISES À UN CONTRÔLE NORMAL. - CONDITION D'ANTÉRIORITÉ DU PRODUIT À LAQUELLE EST SUBORDONNÉ LE BÉNÉFICE D'UNE AOC.
</SCT>
<ANA ID="9A"> 03-05-06-02 Le juge administratif exerce un contrôle normal sur le respect de la condition d'antériorité du produit à laquelle est subordonné le bénéfice d'une appellation d'origine contrôlée (AOC).</ANA>
<ANA ID="9B"> 54-07-02-03 Le juge administratif exerce un contrôle normal sur le respect de la condition d'antériorité du produit à laquelle est subordonné le bénéfice d'une appellation d'origine contrôlée (AOC).</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
