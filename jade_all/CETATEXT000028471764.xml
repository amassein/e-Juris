<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028471764</ID>
<ANCIEN_ID>JG_L_2014_01_000000368906</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/47/17/CETATEXT000028471764.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 15/01/2014, 368906, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-01-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>368906</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Camille Pascal</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:368906.20140115</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 28 mai 2013 au secrétariat du contentieux du Conseil d'Etat, du ministre de l'économie et des finances ; le ministre de l'économie et des finances demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler le jugement n° 1105122/2 du 28 mars 2013 par lequel le tribunal administratif de Melun a, à la demande de M. B...A..., d'une part, annulé la décision du directeur général des finances publiques du 27 avril 2011 refusant son reclassement, d'autre part, fait injonction au ministre de l'économie et des finances de procéder au reclassement de M. A...dans l'échelon correspondant à l'indice qu'il avait atteint dans son corps d'origine au moment de sa titularisation dans le corps des contrôleurs des finances publiques, ou à défaut à l'indice immédiatement supérieur, dans le délai de 3 mois ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande présentée par M. A...devant le tribunal administratif de Melun ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 84-16 du 11 janvier 1984 ;<br/>
<br/>
              Vu le décret n° 85-986 du 16 septembre 1985 ;<br/>
<br/>
              Vu le décret n° 94-1016 du 18 novembre 1994 ;<br/>
<br/>
              Vu le décret n° 95-379 du 10 avril 1995 ;<br/>
<br/>
              Vu le décret n° 2006-1441 du 24 novembre 2006 ;<br/>
<br/>
              Vu le décret n° 2010-982 du 26 août 2010 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Camille Pascal, Conseiller d'Etat,<br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, Rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'à la suite de sa réussite au concours de contrôleur des impôts, M.A..., surveillant de l'administration pénitentiaire, a été détaché de son corps d'origine et classé, à la date de sa nomination comme contrôleur stagiaire et pour la période d'accomplissement du stage qu'il a effectué du 1er septembre 2009 au 1er septembre 2010, au 9ème échelon de son grade à l'indice nouveau majoré 384 ; qu'il a été, à l'issue de sa scolarité à l'école nationale des impôts, titularisé au même grade, puis reclassé au 8ème échelon avec le même indice par arrêté du 7 octobre 2010, pris en application du décret du 26 août 2010 portant statut particulier du corps des contrôleurs des finances publiques ; que, par une décision du 27 avril 2011, le directeur général des finances publiques a refusé qu'il soit tenu compte dans la situation administrative de M. A...de l'avancement d'échelon obtenu dans son administration d'origine le 28 avril 2010, postérieurement à sa nomination en qualité de contrôleur stagiaire mais avant sa titularisation ; que le ministre de l'économie et des finances se pourvoit en cassation contre le jugement du 28 mars 2013 par lequel le tribunal administratif de Melun a, sur la requête de M.A..., annulé cette décision ; <br/>
<br/>
              2.	Considérant qu'aux termes de l'article 45 de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat, dans sa rédaction alors en vigueur : " A l'expiration de son détachement, le fonctionnaire est, sauf intégration dans le corps ou cadre d'emplois de détachement, réintégré dans son corps d'origine... Lorsque le fonctionnaire est intégré dans le corps ou cadre d'emplois de détachement, il est tenu compte du grade et de l'échelon qu'il a atteints dans le corps d'origine, sous réserve qu'ils lui soient plus favorables " ; que le IV de l'article 3 du décret du 18 novembre 1994, dans sa version issue du décret n° 2006-1441 du 24 novembre 2006 alors applicable, fixant les dispositions communes applicables à divers corps de fonctionnaires de la catégorie B et applicable au corps des contrôleurs des impôts en vertu de l'article 1er du décret du 10 avril 1995 relatif au statut des contrôleurs de ce corps, dans sa rédaction alors applicable, dispose que : " Les fonctionnaires nommés dans l'un des corps régis par le présent décret sont classés, lors de leur nomination, au 1er échelon du grade de début, sous réserve des dispositions ci-après et de celles des articles 4 à 7 : (...) IV. -  Les fonctionnaires autres que ceux mentionnés au I, au II et au III sont classés à l'échelon du grade de début qui comporte un traitement égal ou, à défaut, immédiatement supérieur au traitement perçu en dernier lieu dans leur corps d'origine (...) " ; qu'il résulte de ces dispositions que la situation du fonctionnaire intégré dans un corps dans lequel il est entré par la voie du détachement s'apprécie au regard de sa situation dans son corps d'origine, à la date de sa titularisation dans le corps d'intégration ; qu'il en va différemment de la situation du fonctionnaire qui, entré dans un nouveau corps à l'issue de sa réussite à un concours d'entrée, y est nommé comme stagiaire, placé durant son stage et avant d'être titularisé, en position de détachement de son corps d'origine, en application du 10° de l'article 14 du décret du 16 septembre 1985 relatif aux positions des fonctionnaires, et dont la situation administrative lors de son intégration s'apprécie au regard de sa situation dans son corps d'origine, à la date de sa nomination dans le corps d'intégration et non à celle de sa titularisation ; <br/>
<br/>
              3.	Considérant qu'il résulte de ce qui précède qu'en jugeant que M. A...était en position de détachement pendant l'accomplissement de son stage et que, par suite, il était fondé à soutenir que l'échelon qui lui avait été attribué dans son corps d'origine, le 28 avril 2010, devait être pris en compte lors de sa titularisation, alors que M. A...avait été nommé dans le corps des contrôleurs des impôts en qualité de stagiaire à compter du 1er septembre 2009 et qu'il avait été mis fin à son détachement le 31 août 2010 pour procéder à sa titularisation le 1er septembre 2010, le tribunal administratif de Melun a commis une erreur de droit ; que, par suite, le ministre de l'économie et des finances est fondé à demander l'annulation du jugement attaqué ;<br/>
<br/>
              4.	Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              5.	Considérant qu'il résulte de ce qui précède que la situation administrative de M. A...lors de son intégration dans le corps des contrôleurs des impôts devait être appréciée à la date de sa nomination comme stagiaire dans ce corps, le 1er septembre 2009 ; qu'il n'est, dès lors, pas fondé à demander l'annulation de la décision du 27 avril 2011, par laquelle le directeur général des finances publiques a refusé de procéder au reclassement dans l'échelon qu'il avait acquis le 28 avril 2010, dans son corps d'origine ;  <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Melun du 28 mars 2013 est annulé.<br/>
<br/>
Article 2 : La demande portée par M. A...devant le tribunal administratif de Melun est rejetée.<br/>
<br/>
Article 3 : La présente décision sera notifiée au ministre de l'économie et des finances et à M. B... A....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
