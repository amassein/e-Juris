<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044167184</ID>
<ANCIEN_ID>JG_L_2021_10_000000443133</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/16/71/CETATEXT000044167184.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 04/10/2021, 443133, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-10-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>443133</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>M. François-René Burnod</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:443133.20211004</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société par actions simplifiée (SAS) SKF Holding France a demandé au tribunal administratif de Montreuil de prononcer la décharge des cotisations supplémentaires d'impôt sur les sociétés auxquelles elle a été assujettie au titre des exercices 2009 et 2010, ainsi que des pénalités correspondantes. Par un jugement n° 1608787 du 23 avril 2018, le tribunal a fait droit à cette demande.  <br/>
<br/>
              Par un arrêt n° 18VE02849 du 22 juin 2020, la cour administrative d'appel de Versailles, sur appel du ministre de l'action et des comptes publics, a annulé ce jugement et remis à la charge de la société les impositions dont le tribunal administratif avait prononcé la décharge.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 21 août 2020 et 23 novembre 2020 au secrétariat du contentieux du Conseil d'Etat, la société SKF Holding demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel du ministre ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François-René Burnod, auditeur,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Célice, Texidor, Perier, avocat de la société SKF Holding France ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société RKS, dont l'activité consiste en la fabrication de roulements sur mesure de très grandes dimensions à destination de l'industrie civile et militaire, est contrôlée par le groupe suédois SKF à travers la société par actions simplifiées (SAS) SKF Holding France, qui est son unique associé. La société RKS a fait l'objet d'une vérification de comptabilité au titre des exercices clos en 2009 et 2010, au terme de laquelle l'administration, par propositions de rectification des 20 décembre 2012 et 31 juillet 2013, a remis en cause les prix auxquels elle avait facturé ses produits aux sociétés distributrices du groupe SKF à l'étranger. Elle a assujetti en conséquence la société SKF Holding France, tête du groupe fiscal intégré auquel appartient la société RKS, à des cotisations supplémentaires d'impôt sur les sociétés et de contribution sociale sur l'impôt sur les sociétés au titre des exercices 2009 et 2010, d'un montant de 5 385 325 euros, pénalités incluses. La société SKF Holding France se pourvoit en cassation contre l'arrêt du 22 juin 2020 par lequel la cour administrative de Versailles, statuant sur appel du ministre de l'action et des comptes publics, a annulé le jugement du 23 avril 2018 du tribunal administratif de Montreuil prononçant la décharge de ces impositions et les a remises à sa charge. <br/>
<br/>
              2. Aux termes de l'article 57 du code général des impôts : " Pour l'établissement de l'impôt sur le revenu dû par les entreprises qui sont sous la dépendance ou qui possèdent le contrôle d'entreprises situées hors de France, les bénéfices indirectement transférés à ces dernières, soit par voie de majoration ou de diminution des prix d'achat ou de vente, soit par tout autre moyen, sont incorporés aux résultats accusés par les comptabilités. Il est procédé de même à l'égard des entreprises qui sont sous la dépendance d'une entreprise ou d'un groupe possédant également le contrôle d'entreprises situées hors de France. / (...) A défaut d'éléments précis pour opérer les rectifications prévues aux premier, deuxième et troisième alinéas, les produits imposables sont déterminés par comparaison avec ceux des entreprises similaires exploitées normalement ". Il résulte de ces dispositions que, lorsqu'elle constate que les prix facturés par une entreprise établie en France à une entreprise étrangère qui lui est liée - ou ceux qui lui sont facturés par cette entreprise étrangère -, sont inférieurs - ou supérieurs - à ceux pratiqués par des entreprises similaires exploitées normalement, c'est-à-dire dépourvues de liens de dépendance, l'administration doit être regardée comme établissant l'existence d'un avantage qu'elle est en droit de réintégrer dans les résultats de l'entreprise française, sauf pour celle-ci à justifier que cet avantage a eu pour elle des contreparties au moins équivalentes. A défaut d'avoir procédé à une telle comparaison, le service n'est, en revanche, pas fondé à invoquer la présomption de transferts de bénéfices ainsi instituée mais doit, pour démontrer qu'une entreprise a consenti une libéralité en facturant des prestations à un prix insuffisant - ou en les payant à un prix excessif -, établir l'existence d'un écart injustifié entre le prix convenu et la valeur vénale du bien cédé ou du service rendu.<br/>
<br/>
              3. D'une part, l'administration peut, pour établir l'existence d'une majoration des prix d'achat ou d'une minoration des prix de vente facturés entre une entreprise établie en France et une entreprise étrangère qui lui est liée, ainsi d'ailleurs que le préconisent les Principes de l'OCDE applicables aux prix de transfert à l'intention des entreprises multinationales et des administrations publiques, se fonder sur la comparaison d'un ratio financier pertinent de l'une ou l'autre entreprise, tel que le taux de marge sur ces transactions, avec celui d'entreprises similaires exploitées normalement, c'est-à-dire dépourvues de lien de dépendance. D'autre part, une différence ainsi constatée par l'administration entre les prix pratiqués par une entreprise française avec les entreprises qui lui sont liées et les prix pratiqués entre des entreprises similaires exploitées normalement peut être regardée comme ne constituant pas un avantage dépourvu de contrepartie susceptible d'être réintégré dans les résultats de cette entreprise si elle est justifiée par les risques que celle-ci a vocation à assumer et qui affectent sa rentabilité. Dans ce dernier cas, il lui incombe de justifier à la fois qu'elle avait, du fait des fonctions qu'elle exerçait au sein du groupe, vocation à assumer ces risques, et que l'écart entre les ratios financiers constatés et ceux d'entreprises similaires exploitées normalement s'explique par la réalisation de ces risques. <br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond que l'administration a fait application, lors du contrôle de la société RKS, d'une " méthode transactionnelle de marges nettes " (MTMN), consistant à comparer le ratio de marge nette sur le chiffre d'affaires de cette société à raison des opérations en cause avec celui de huit entreprises dépourvues de lien de dépendance et exerçant dans des domaines d'activité voisins. Ce faisant, elle a constaté que le ratio de marge nette de la société était de -10,46% en 2009 et de -21,87% en 2010, là où il s'élevait à 2,33% en 2009 et 2,62% en 2010 pour la moyenne des sociétés comparées. Dès lors, la cour administrative d'appel a pu sans erreur de droit juger que le service, à l'issue de cette comparaison dont elle a relevé qu'aucune critique ne lui était adressée, avait établi une présomption de transfert de bénéfices pour les transactions en cause, à hauteur de la différence entre le montant constaté des recettes et celui qui aurait résulté de l'application du taux de marge nette moyen du panel d'entreprises comparables. <br/>
<br/>
              5. Toutefois, la société SKF Holding France soutenait devant la cour, pour justifier cet écart, que la société RKS exerçait un rôle fonctionnel plus important que celui d'une simple unité de production au sein du groupe SKF, ce qui lui donnait vocation à assumer un risque de développement et un risque commercial et que ce risque avait affecté, pour les années en litige, son bénéfice d'exploitation. <br/>
<br/>
              6. En premier lieu, ainsi d'ailleurs que le préconisent les Principes de l'OCDE applicables aux prix de transfert, pour qu'il puisse être regardé comme établi qu'une société membre d'un groupe a effectivement vocation à assumer un risque économique que la politique de prix de transfert du groupe la conduit à supporter, et que cette politique est par suite conforme au principe de pleine concurrence, il faut que cette société dispose de fonctions de contrôle et d'atténuation effectives de ce risque ainsi que de la capacité financière de l'assumer. En se fondant, pour juger que la société RKS n'avait pas vocation à assumer des pertes économiques liées à l'exploitation de son activité, sur le seul motif que cette société n'avait pas le statut " d'entrepreneur principal " au sein du groupe SKF, sans rechercher si sa position fonctionnelle au sein du groupe lui donnait vocation à porter les risques spécifiques qu'elle invoquait, à savoir, d'une part, des risques stratégiques liés au choix de développer de nouveaux produits, et, d'autre part, des risques opérationnels liés à l'efficacité des processus de production, la cour a entaché son arrêt d'erreur de droit. <br/>
<br/>
              7. En second lieu, pour juger que le taux de marge négatif de la société RKS ne résultait pas de la réalisation d'un risque que celle-ci avait vocation à assumer, la cour administrative d'appel a relevé que le résultat consolidé du groupe SKF, toutes activités confondues, se situait dans le même temps entre 6 et 14%, que les achats de matières premières de la société avaient été stables et que ses ventes n'avaient pas subi de baisse en volume sauf en ce qui concerne les éoliennes. Ce faisant, elle n'a pas répondu à l'argumentation que la société SKF Holding France soulevait pour justifier de la baisse de marge de la société RKS sur les deux exercices en cause, selon laquelle cette société avait subi les conséquences d'un risque stratégique lié à son choix de réorienter son unique activité vers le secteur de l'éolien. Elle a, dès lors, entaché son arrêt d'une insuffisance de motivation. <br/>
<br/>
              8. Il résulte de ce qui précède que la société SKF Holding France est fondée à demander l'annulation de l'arrêt qu'elle attaque. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3 000 euros à verser à la société SKF Holding France au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 22 juin 2020 de la cour administrative d'appel de Versailles est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Versailles. <br/>
Article 3 : L'Etat versera à la société SKF Holding France une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la société par actions simplifiée SKF Holding France et au ministre de l'économie, des finances et de la relance.  <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
