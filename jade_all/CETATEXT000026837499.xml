<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026837499</ID>
<ANCIEN_ID>JG_L_2012_12_000000347458</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/83/74/CETATEXT000026837499.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  6ème sous-section jugeant seule, 26/12/2012, 347458, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-12-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>347458</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 6ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL ; SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>M. Marc Pichon de Vendeuil</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier De Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:347458.20121226</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 14 mars et 14 juin 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune de Montrouge, représentée par son maire ; la commune de Montrouge demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 09VE02602 du 30 décembre 2010 par lequel la cour administrative d'appel de Versailles a rejeté sa requête tendant à l'annulation du jugement n° 0709529-0801228 du 20 mai 2009 par lequel le tribunal administratif de Versailles a annulé, à la demande de l'association Mon Montrouge, de Mme Mnouchkine et de M. et Mme Murez, l'arrêté municipal du 25 juillet 2007 par lequel le maire de Montrouge a accordé à la SCI du 1 rue Amaury Duval un permis de construire un immeuble sur un terrain sis 22, rue Louis Rolland et 1, rue Amaury Duval, ainsi que l'arrêté municipal du 13 décembre 2007 portant permis de construire modificatif ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'association Mon Montrouge, de M. et Mme Murez et de Mme Mnouchkine la somme de 4.000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Pichon de Vendeuil, Maître des Requêtes en service extraordinaire, <br/>
<br/>
              - les observations de la SCP Baraduc, Duhamel, avocat de la commune de Montrouge et de la SCP Gaschignard, avocat de Mme Ariane Mnouchkine, de M. et Mme Murez et de l'association Mon Montrouge,<br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Baraduc, Duhamel, avocat de la commune de Montrouge  et à la SCP Gaschignard, avocat de Mme Ariane Mnouchkine, de M. et Mme Murez et de l'association Mon Montrouge ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 25 juillet 2007, le maire de Montrouge a accordé à la SCI du 1 rue Amaury Duval un permis de construire un immeuble collectif comportant treize logements et un local d'activités, sur un terrain situé en zone pavillonnaire 22, rue Louis Rolland et 1, rue Amaury Duval ; qu'un permis de construire modificatif a été délivré le 13 décembre 2007 ; que, par un jugement du 20 mai 2009, à la demande de l'association Mon Montrouge et autres, le tribunal administratif de Versailles a annulé ces décisions ; que, par un arrêt du 30 décembre 2010, contre lequel la commune de Montrouge se pourvoit en cassation, la cour administrative d'appel de Versailles a confirmé ce jugement ;<br/>
<br/>
              2. Considérant, en premier lieu, qu'aux termes de l'article UEb 6.5 du règlement de plan d'occupation des sols de la commune de Montrouge, dans sa rédaction en vigueur à la date de délivrance des arrêtés litigieux : " A l'exception des plantations et des garages (...), aucune construction du sol ni du sous-sol n'est autorisée dans les marges de reculement de type A. " ;<br/>
<br/>
              3. Considérant qu'aucun texte ni principe ne fait par lui-même obstacle à ce que les règlements des plans d'urbanisme prescrivent, pour des motifs d'hygiène, d'urbanisme et de protection du voisinage, des interdictions de construire, y compris en sous-sol, dans les marges de reculement qu'ils définissent ; qu'il résulte des dispositions rappelées au point 2 que toutes les constructions en sous-sol sont interdites dans les marges de reculement de type A, sans qu'il soit opéré de distinction entre celles entièrement enterrées et celles qui comportent des parties aériennes ; que, par suite, en jugeant que les dispositions de l'article UEb 6.5 précitées s'opposaient à la présence d'un local technique de ventilation en sous-sol entièrement enterré dans la marge de reculement de type A de la construction projetée, la cour administrative d'appel de Versailles n'a pas entaché son arrêt d'erreur de droit ou d'une erreur de qualification juridique ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui a été dit au point 3 qu'aucune exception n'est prévue à la règle d'inconstructibilité dans les marges de reculement de type A instituée par le règlement du plan d'occupation des sols ; que, dès lors, en relevant que la terrasse dallée, dont la construction était également projetée dans la même marge de reculement, était indissociable de l'immeuble faisant l'objet de la demande de permis de construire, puis en jugeant que les dispositions de l'article UEb 6.5 s'opposaient également à la réalisation de cet ouvrage, la cour administrative d'appel de Versailles n'a pas commis d'erreur de droit ou d'erreur de qualification juridique ;<br/>
<br/>
              5. Considérant, en second lieu, qu'aux termes du second alinéa de l'article       L. 123-6 du code de l'urbanisme, dans sa rédaction en vigueur à la date des permis attaqués : " A compter de la publication de la délibération prescrivant l'élaboration d'un plan local d'urbanisme, l'autorité compétente peut décider de surseoir à statuer, dans les conditions et délai prévus à l'article L. 111-8, sur les demandes d'autorisation concernant des constructions, installations ou opérations qui seraient de nature à compromettre ou à rendre plus onéreuse l'exécution du futur plan. " ;<br/>
<br/>
              6. Considérant que, pour juger que le maire de Montrouge avait commis une erreur manifeste d'appréciation en n'opposant pas un sursis à statuer sur le fondement des dispositions citées au point 5, la cour administrative d'appel de Versailles a d'abord indiqué qu'à la date du 25 juillet 2007 à laquelle le permis de construire a été délivré, le projet de règlement du plan local d'urbanisme en cours de révision était connu depuis le mois de mars précédent et était donc suffisamment avancé pour que le maire soit à même d'apprécier si, eu égard à ses caractéristiques, la construction projetée était de nature à compromettre l'exécution du futur plan  ; qu'elle a, ensuite, relevé les contradictions flagrantes existant entre les prescriptions futures du plan qui, dans le secteur pavillonnaire concerné, tendaient à limiter l'emprise au sol des bâtiments à 100 m² et leur hauteur à 10 mètres au faîtage, alors que l'emprise au sol du projet de construction litigieux était de 450 m² et sa hauteur de 12 mètres ; qu'en statuant ainsi, la cour administrative d'appel de Versailles n'a pas commis d'erreur de droit dans l'application des dispositions de l'article L. 123-6 précité et a porté une appréciation souveraine sur les pièces du dossier, qui est exempte de dénaturation ;<br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que le pourvoi de la commune de Montrouge doit être rejeté ;<br/>
<br/>
              8. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à ce titre à la charge de l'association Mon Montrouge et autres, qui ne sont pas, dans la présente instance, la partie perdante la somme que demande la commune de Montrouge au titre des frais exposés par elle et non compris dans les dépens ; qu'en revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Montrouge la somme globale de 3.500 euros qui sera versée à l'association Mon Montrouge, à Mme Mnouchkine et à M. et Mme Murez, au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
 Article 1er : Le pourvoi de la commune de Montrouge est rejeté.<br/>
<br/>
 Article 2 : La commune de Montrouge versera à l'association Mon Montrouge, à Mme Mnouchkine et à M. et Mme Murez une somme globale de 3.500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
 Article 3 : La présente décision sera notifiée à la commune de Montrouge, à l'association Mon Montrouge, à Mme Ariane Mnouchkine et à M. et Mme Stefen Murez.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
