<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031587392</ID>
<ANCIEN_ID>JG_L_2015_12_000000386980</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/58/73/CETATEXT000031587392.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème / 2ème SSR, 07/12/2015, 386980, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-12-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>386980</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème / 2ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>Mme Charline Nicolas</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:386980.20151207</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a saisi le tribunal administratif de Montreuil d'une demande tendant à l'annulation de la décision du 16 mars 2012 du ministre de la défense rejetant sa demande tendant au bénéfice de la loi n° 2010-2 du 5 janvier 2010 relative à la reconnaissance et à l'indemnisation des victimes des essais nucléaires français. Par un jugement n° 1203979 du 11 avril 2013, le tribunal a fait droit à sa demande. <br/>
<br/>
              Par un arrêt n° 13VE01910 du 21 octobre 2014, la cour administrative d'appel de Versailles a rejeté l'appel formé par le ministre de la défense contre ce jugement. <br/>
<br/>
              Par un pourvoi, enregistré le 7 janvier 2015 au secrétariat du contentieux du Conseil d'Etat, le ministre de la défense demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de première instance de M. B....<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 2010-2 du 5 janvier 2010 ; <br/>
              - le décret n° 2010-653 du 11 juin 2010 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Charline Nicolas, auditeur,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de M. B...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article 1er de la loi du 5 janvier 2010 relative à la reconnaissance et à l'indemnisation des victimes des essais nucléaires français : " Toute personne souffrant d'une maladie radio-induite résultant d'une exposition à des rayonnements ionisants dus aux essais nucléaires français et inscrite sur une liste fixée par décret en Conseil d'Etat conformément aux travaux reconnus par la communauté scientifique internationale peut obtenir réparation intégrale de son préjudice dans les conditions prévues par la présente loi. Si la personne est décédée, la demande de réparation peut être présentée par ses ayants droit " ; que l'article 2 de cette même loi définit les conditions de temps et de lieu de séjour ou de résidence que le demandeur doit remplir ; qu'aux termes du I de l'article 4 de cette même loi, dans sa version applicable au litige : " Les demandes d'indemnisation sont soumises à un  comité d'indemnisation (...) " et qu'aux termes du II de ce même article : " Ce comité examine si les conditions de l'indemnisation sont réunies. Lorsqu'elles le sont, l'intéressé bénéficie d'une présomption de causalité à moins qu'au regard de la nature de la maladie et des conditions de son exposition le risque attribuable aux essais nucléaires puisse être considéré comme négligeable. Le comité le justifie auprès de l'intéressé (...) " ; qu'aux termes de l'article 7 du décret du 11 juin 2010, pris pour l'application de la loi du 5 janvier 2010, dans sa rédaction alors applicable : " (...) Le comité d'indemnisation détermine la méthode qu'il retient pour formuler sa recommandation au ministre en s'appuyant sur les méthodologies recommandées par l'Agence internationale de l'énergie atomique. (...) " ;<br/>
<br/>
              2. Considérant qu'il résulte de ces dispositions que le législateur a entendu faire bénéficier toute personne souffrant d'une maladie radio-induite ayant résidé ou séjourné, durant des périodes déterminées, dans des zones géographiques situées en Polynésie française et en Algérie, d'une présomption de causalité aux fins d'indemnisation du préjudice subi en raison de l'exposition aux rayonnements ionisants due aux essais nucléaires ; que, toutefois, cette présomption peut être renversée lorsqu'il est établi que le risque attribuable aux essais nucléaires, apprécié tant au regard de la nature de la maladie que des conditions particulières d'exposition du demandeur, est négligeable ; qu'à ce titre, l'appréciation du risque peut notamment prendre en compte le délai de latence de la maladie, le sexe du demandeur, son âge à la date du diagnostic, sa localisation géographique au moment des tirs, les fonctions qu'il exerçait effectivement, ses conditions d'affectation, ainsi que, le cas échéant, les missions de son unité au moment des tirs ; <br/>
<br/>
              3. Considérant que le calcul de la dose reçue de rayonnements ionisants constitue l'un des éléments sur lequel l'autorité chargée d'examiner la demande peut se fonder afin d'évaluer le risque attribuable aux essais nucléaires ; que si, pour ce calcul, l'autorité peut utiliser les résultats des mesures de surveillance de la contamination tant interne qu'externe des personnes exposées, qu'il s'agisse de mesures individuelles ou collectives en ce qui concerne la contamination externe, il lui appartient de vérifier, avant d'utiliser ces résultats, que les mesures de surveillance de la contamination interne et externe ont, chacune, été suffisantes au regard des conditions concrètes d'exposition de l'intéressé, et sont ainsi de nature à établir si le risque attribuable aux essais nucléaires était négligeable ; qu'en l'absence de mesures de surveillance de la contamination interne ou externe et en l'absence de données relatives au cas des personnes se trouvant dans une situation comparable à celle du demandeur du point de vue du lieu et de la date de séjour, il appartient à cette même autorité de vérifier si, au regard des conditions concrètes d'exposition de l'intéressé précisées ci-dessus, de telles mesures auraient été nécessaires ; que si tel est le cas, l'administration ne peut être regardée comme rapportant la preuve de ce que le risque attribuable aux essais nucléaires doit être regardé comme négligeable et la présomption de causalité ne peut être renversée ; <br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. B...a été affecté à bord du croiseur anti-aérien de Grasse en Polynésie française du 2 mars 1971 au 1er février 1972 ; qu'il a développé un cancer, diagnostiqué en 2005 ; que pour confirmer le droit de M. B...au bénéfice de l'indemnisation organisée par les dispositions citées ci-dessus, la cour administrative d'appel a notamment relevé une erreur dans le calcul opéré par le comité d'indemnisation de la dose de rayonnement à la suite du tir du 14 août 1971 et l'absence de mesure de dosimétrie d'ambiance individuelle ou collective pour le mois de juin 1971 alors que deux tirs avaient eu lieu pendant cette période ; qu'elle en a déduit qu'il ne pouvait être exclu que M. B...avait fait l'objet d'une contamination interne ; qu'en procédant ainsi, sans rechercher si les mesures de surveillance de la contamination externe étaient suffisantes au regard des conditions d'exposition de l'intéressé et si des mesures de surveillance de la contamination interne auraient été nécessaires au regard de ces mêmes conditions, la cour administrative d'appel de Versailles a entaché son arrêt d'erreur de droit ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt doit être annulé ;<br/>
<br/>
              5. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Versailles du 21 octobre 2014 est annulé. <br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Versailles.<br/>
Article 3 : La présente décision sera notifiée au ministre de la défense et à M. A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
