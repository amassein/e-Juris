<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035506191</ID>
<ANCIEN_ID>JG_L_2017_09_000000413842</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/50/61/CETATEXT000035506191.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 01/09/2017, 413842, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-09-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>413842</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2017:413842.20170901</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au juge des référés du tribunal administratif de Nantes, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre à la préfète de la Loire-Atlantique de lui délivrer dans le délai de 24 heures à compter de la notification de l'ordonnance à intervenir, une convocation dans les 72 heures en vue de se voir délivrer une attestation de demandeur d'asile, d'enregistrer lors de cette convocation sa demande d'asile et de lui remettre le dossier à adresser à l'Office français de protection des réfugiés et apatrides, sous astreinte de 100 euros par jour de retard. Par une ordonnance n° 1707441 du 23 août 2017, le juge des référés du tribunal administratif de Nantes a rejeté sa demande. <br/>
<br/>
              Par une requête, enregistrée le 30 août 2017 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - la condition d'urgence est satisfaite dès lors que la décision de transfert est exécutoire à tout moment, qu'il ne peut avoir accès aux conditions matérielles des demandeurs d'asile et qu'il peut à tout moment être placé en centre de détention ;<br/>
              - l'urgence résulte au surplus du caractère manifestement illégal de la pratique préfectorale consistant à empêcher l'accès au droit d'asile ;<br/>
              - le droit d'asile constitue une liberté fondamentale au sens de l'article L. 521-2 du code de justice administrative ;<br/>
              - il est porté atteinte de manière grave et manifestement illégale au droit d'asile dès lors que l'absence à deux convocations ne caractérise pas, dans les circonstances de l'espèce, une " fuite " au sens de l'article 29 du règlement Dublin III, notion que la jurisprudence définit comme une soustraction systématique et intentionnelle à la mesure de transfert. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - Vu le règlement (UE) n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". En vertu de l'article L. 522-3 du même code, le juge des référés peut rejeter une requête par une ordonnance motivée, sans instruction contradictoire ni audience publique, lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée. A cet égard, il appartient au juge d'appel de prendre en considération les éléments recueillis par le juge de première instance dans le cadre de la procédure écrite et orale qu'il a diligentée.<br/>
<br/>
              2. Le droit constitutionnel d'asile, qui a le caractère d'une liberté fondamentale, a pour corollaire le droit de solliciter le statut de réfugié. S'il implique que l'étranger qui sollicite la reconnaissance de la qualité de réfugié soit en principe autorisé à demeurer sur le territoire jusqu'à ce qu'il ait été statué sur sa demande, ce droit s'exerce dans les conditions définies par les articles L. 741-1 et suivants du code de l'entrée et du séjour des étrangers et du droit d'asile. En vertu de l'article L. 742-3 de ce code, l'étranger dont l'examen de la demande d'asile relève de la responsabilité d'un autre Etat peut faire l'objet d'un transfert vers l'Etat qui est responsable de cet examen en application des dispositions du règlement du Parlement européen et du Conseil du 26 juin 2013 établissant les critères et mécanismes de détermination de l'Etat membre responsable de l'examen d'une demande de protection internationale introduite dans l'un des Etats membres par un ressortissant de pays tiers ou un apatride. L'article 29 de ce règlement prévoit que le transfert du demandeur d'asile vers le pays de réadmission doit se faire dans les six mois à compter de l'acceptation de la demande de prise en charge et que ce délai peut être porté à dix-huit mois si l'intéressé " prend la fuite ". La notion de fuite, au sens de ces dernières dispositions, doit s'entendre comme visant notamment le cas où un ressortissant étranger non admis au séjour se serait soustrait de façon intentionnelle et systématique au contrôle de l'autorité administrative en vue de faire obstacle à une mesure d'éloignement le concernant. <br/>
<br/>
              3. Il résulte de l'instruction diligentée en première instance par le juge des référés du tribunal administratif de Nantes que M.B..., ressortissant soudanais, déclare être entré en France le 28 octobre 2016. Il a sollicité le 21 décembre suivant la reconnaissance de la qualité de réfugié. Le préfet de la Loire-Atlantique lui a délivré une attestation de demande d'asile, renouvelée jusqu'au 10 mai 2017. Il est apparu que ses empreintes digitales avaient déjà été relevées en Italie, où il avait sollicité l'asile auprès des autorités le 25 août précédent. Le préfet a alors saisi les autorités italiennes d'une demande de reprise en charge du requérant, implicitement acceptée le 6 janvier 2017. Par un arrêté du 13 janvier 2017, le préfet de la Loire-Atlantique a décidé la remise de M. B...aux autorités italiennes. Par un jugement n° 1700824 du 10 février 2017, le tribunal administratif de Nantes a rejeté la requête de M. B...contre cette décision. L'intéressé s'est vu remettre le 28 février 2017 un billet pour un vol à destination de Rome le 21 mars suivant, à l'embarquement duquel il ne s'est pas présenté. Il a été à nouveau convoqué le 21 mars suivant à se présenter le 4 avril 2017 et informé qu'à défaut, il serait considéré comme étant en fuite et que le délai de transfert serait alors allongé à dix-huit mois. Il ne s'est pas davantage rendu à cette convocation, alors que toutes les informations nécessaires et traduites lui ont été communiquées. Les autorités italiennes ont été avisées le 16 juin 2017 de la prolongation du délai de transfert. Par une ordonnance du 23 août 2017, dont M. B... demande l'annulation, le tribunal administratif de Nantes a rejeté sa requête tendant à ce qu'il soit enjoint à la préfète de la Loire-Atlantique de lui délivrer, dans un délai de 24 heures à compter de la notification de l'ordonnance à intervenir, une convocation dans les 72 heures en vue de se voir délivrer une attestation de demandeur d'asile, d'enregistrer lors de cette convocation sa demande d'asile et de lui remettre le dossier à adresser à l'Office français de protection des réfugiés et apatrides, sous astreinte de 100 euros par jour de retard. <br/>
<br/>
              4. Le requérant n'apporte en appel aucun élément de nature à infirmer l'appréciation portée par le juge des référés de première instance sur le fait qu'il s'est volontairement soustrait, dans le délai de six mois prescrit par l'article 29 du règlement du 26  juin 2013, à deux convocations en vue de son transfert, la première remise le 28 février 2017, la seconde présentée le 4 avril 2017, et qu'il devait ainsi être regardé comme ayant pris la fuite, au sens du même article. Dès lors, en portant à dix-huit mois le délai de réadmission de M. B... vers l'Italie, la préfète de la Loire-Atlantique n'a pas méconnu de manière grave et manifeste les obligations qu'impose le respect du droit d'asile, ainsi que l'a jugé à bon droit le juge des référés du tribunal administratif de Nantes.<br/>
<br/>
              5. Il résulte de ce qui précède qu'il est manifeste que l'appel de M. B... est mal fondé. Par suite, sa requête, y compris les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative, doit être rejetée selon la procédure prévue par l'article L. 522-3 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. B...est rejetée. <br/>
Article 2 : La présente ordonnance sera notifiée à M. A...B...et au ministre d'Etat, ministre de l'intérieur. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
