<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030853932</ID>
<ANCIEN_ID>JG_L_2015_07_000000365200</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/85/39/CETATEXT000030853932.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème SSJS, 01/07/2015, 365200, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-07-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>365200</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>M. Philippe Orban</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:365200.20150701</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 15 janvier et 12 avril 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme B...A..., demeurant ...; Mme A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 12LY00775 du 15 novembre 2012 par lequel la cour administrative d'appel de Lyon a rejeté sa requête tendant à l'annulation du jugement n° 0902214 du 20 décembre 2011 par lequel le tribunal administratif de Dijon a rejeté sa demande tendant à l'annulation pour excès de pouvoir de la décision du 23 juillet 2009 du ministre du travail, des relations sociales, de la famille, de la solidarité et de la ville autorisant la Société internationale de lingerie à la licencier ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre solidairement à la charge de l'Etat et de la SCP Bécheret, Thierry, Sénéchal, Gorrias, en sa qualité liquidateur judiciaire de la Société internationale de lingerie, le versement d'une somme de 3 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code du travail ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Philippe Orban, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de Mme A...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, dans le cadre de la liquidation judiciaire de la Société nationale de lingerie, le liquidateur de la société a sollicité l'autorisation de licencier MmeA..., salariée protégée, pour motif économique ; que cette autorisation lui a été accordée le 23 juillet 2009 par le ministre du travail, des relations sociales, de la famille, de la solidarité et de la ville ; que Mme A...se pourvoit en cassation contre l'arrêt du 15 novembre 2012 par lequel la cour administrative d'appel de Lyon a rejeté sa requête tendant à l'annulation du jugement du tribunal administratif de Dijon du 20 décembre 2011 rejetant sa demande d'annulation pour excès de pouvoir de cette décision ;<br/>
<br/>
              2. Considérant que, lorsque le licenciement d'un salarié protégé est envisagé, il ne doit pas être en rapport avec les fonctions qu'il exerce, ni avec son appartenance syndicale ; que, dans le cas où la demande de licenciement est fondée sur un motif à caractère économique, il appartient à l'inspecteur du travail et, le cas échéant, au ministre saisi par la voie d'un recours hiérarchique, de rechercher, sous le contrôle du juge de l'excès de pouvoir, si la situation de l'entreprise justifie le licenciement du salarié, en tenant compte notamment de la nécessité des réductions envisagées d'effectifs et de la possibilité d'assurer le reclassement du salarié dans l'entreprise ou au sein du groupe auquel appartient cette dernière ; qu'en outre, pour refuser l'autorisation sollicitée, l'autorité administrative a la faculté de retenir des motifs d'intérêt général relevant de son pouvoir d'appréciation de l'opportunité, sous réserve qu'une atteinte excessive ne soit pas portée à l'un ou l'autre des intérêts en présence ;<br/>
<br/>
              3. Considérant, en premier lieu, que si la requérante soutient avoir soulevé devant la cour administrative d'appel de Lyon un moyen auquel l'arrêt n'a pas répondu, tiré de ce que les offres de reclassement qui lui avaient été adressées n'étaient ni précises, ni individualisées, il ressort des pièces du dossier soumis à la cour administrative d'appel que ce moyen ne portait que sur l'effort de reclassement externe à l'entreprise et au groupe, dont il ne revient pas à l'autorité administrative de contrôler le respect lorsqu'elle est saisie d'une demande d'autorisation de licenciement ; que ce moyen étant, par suite, inopérant, la cour n'a pas entaché son arrêt d'irrégularité en omettant d'y répondre ;<br/>
<br/>
              4. Considérant, en second lieu, qu'en jugeant que l'existence de personnels de direction communs à la société Eminence et à la Société internationale de lingerie n'était pas de nature, à elle seule, à créer à l'égard de la société Eminence une obligation de procéder à des recherches en vue de reclasser MmeA..., la cour administrative d'appel de Lyon n'a, en tout état de cause, pas commis d'erreur de droit ni inexactement qualifié les faits qui lui étaient soumis ;<br/>
<br/>
              5. Considérant qu'il résulte de tout ce qui précède que Mme A...n'est pas fondée à demander l'annulation de l'arrêt attaqué ; que ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Le pourvoi de Mme A...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à Mme B...A..., à la SCP Becheret, Thierry, Senechal, Gorrias en sa qualité de liquidateur judiciaire de la Société internationale de lingerie et au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
