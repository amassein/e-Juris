<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032408986</ID>
<ANCIEN_ID>JG_L_2016_04_000000384685</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/40/89/CETATEXT000032408986.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème / 8ème SSR, 15/04/2016, 384685</TITRE>
<DATE_DEC>2016-04-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384685</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème / 8ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, VEXLIARD, POUPOT ; SCP CELICE, BLANCPAIN, SOLTNER, TEXIDOR</AVOCATS>
<RAPPORTEUR>M. Christian Fournier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:384685.20160415</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Procédure contentieuse antérieure<br/>
<br/>
              M. B... A...a demandé au tribunal administratif de Rennes, par deux requêtes enregistrées les 15 décembre 2011 et 1er mars 2012, d'une part, d'annuler la rupture conventionnelle de son contrat de travail intervenue dans le cadre d'un accord signé le 26 janvier 2011 ainsi que la décision d'homologation de cette rupture et, d'autre part, d'enjoindre à l'office public de l'habitat (OPH) " Blavet Habitat ", puis à l'office public de l'habitat (OPH) " Cap l'Orient Agglomération Habitat qui s'est substitué à l'OPH " Blavet Habitat ", de le réintégrer dans un délai de deux mois dans ses fonctions de directeur général avec régularisation de sa situation administrative.<br/>
<br/>
              Par un jugement unique n° 1104833-1201018 du 14 juin 2012, le tribunal administratif de Rennes a annulé la rupture conventionnelle du contrat de travail de M. A... ainsi que la décision d'homologation de cette rupture et a enjoint à l'OPH " Cap l'Orient Agglomération Habitat ", d'une part, de réintégrer M. A... dans des fonctions identiques ou équivalentes à celles qu'il occupait à l'OPH " Blavet Habitat " et, d'autre part, de procéder à la régularisation de sa situation administrative dans un délai de deux mois à compter de la notification du jugement.<br/>
<br/>
              Par un arrêt n° 12NT02357 du 25 janvier 2013, la cour administrative d'appel de Nantes a prononcé, à la demande de l'OPH " Cap l'Orient Agglomération Habitat ", le sursis à exécution du jugement rendu par le tribunal administratif de Rennes le 14 juin 2012 jusqu'à ce qu'il soit statué sur la requête d'appel au fond.<br/>
<br/>
              Par une décision n° 366009 du 28 mai 2014, le Conseil d'Etat, statuant au contentieux, a annulé l'arrêt rendu par la cour administrative d'appel de Nantes le 25 janvier 2013 et rejeté la demande de sursis à exécution de l'OPH " Cap l'Orient Agglomération Habitat ".<br/>
<br/>
              Par un arrêt n° 12NT02356 du 18 juillet 2014, la cour administrative d'appel de Nantes, statuant au fond, a rejeté la requête d'appel de l'OPH " Cap L'Orient Agglomération Habitat ", devenu depuis le 1er janvier 2014, l'OPH " Lorient Habitat ", contre le jugement du tribunal administratif de Rennes du 14 juin 2012 ainsi que les conclusions de M. A... dirigé contre ce même jugement.<br/>
<br/>
              Procédure devant le Conseil d'Etat<br/>
<br/>
              1° Sous le n° 384685, par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 23 septembre et 29 octobre 2014 et le 26 juin 2015 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 12NT02356 du 18 juillet 2014 de la cour administrative d'appel de Nantes en tant qu'il rejette ses conclusions tendant, en premier lieu, à ce qu'il soit ordonné à l'OPH " Cap l'Orient Agglomération habitat " de le réintégrer et de régulariser sa situation administrative conformément à l'article 4 du jugement du tribunal administratif de Rennes, en deuxième lieu à ce que la décision du 31 octobre 2012 par laquelle cet office a procédé à sa réintégration juridique et non effective et a refusé de lui verser son salaire du 25 août au 31 octobre 2012 ainsi que la part variable de son salaire à compter du 1er novembre 2012 soit annulée et, en dernier lieu, à ce qu'il soit enjoint à l'office de lui verser la somme de 9 385,57 euros nets au titre du salaire dû pour la période du 25 août au 31 octobre 2012 ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à l'ensemble de ses demandes ;<br/>
<br/>
              3°) de mettre à la charge de l'OPH " Cap l'Orient Agglomération Habitat " la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Sous le n° 384690, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 23 septembre et 23 décembre 2014 au secrétariat du contentieux du Conseil d'Etat, l'OPH " Lorient Habitat ", demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 12NT02356 du 18 juillet 2014 de la cour administrative d'appel de Nantes en tant qu'il rejette ses conclusions subsidiaires tendant à l'annulation de l'article 4 du jugement du tribunal administratif de Rennes du 14 juin 2012 en ce qu'il lui a enjoint de réintégrer M. A... dans des fonctions identiques ou équivalentes à celles qu'il occupait à l'OPH " Blavet Habitat " et de procéder à la régularisation de sa situation administrative ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel présentées à titre subsidiaire ;<br/>
<br/>
              3°) de mettre à la charge de M. A... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la construction de l'habitation ;<br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christian Fournier, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Blancpain, Soltner, Texidor, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'il a été mis fin au contrat à durée indéterminée, en vertu duquel l'Office Public de l'Habitat (OPH) " Blavet Habitat " employait M. B... A...en qualité de directeur général, par un accord de rupture conventionnelle conclu le 26 janvier 2011 ; que, par un jugement du 14 juin 2012, le tribunal administratif de Rennes a annulé l'acte de rupture conventionnelle du contrat de M. A... ainsi que son homologation par l'inspecteur du travail ; que l'article 4 de ce jugement enjoint à l'OPH " Cap l'Orient Agglomération habitat ", issu de la fusion de l'OPH " Blavet Habitat " avec l'OPH de Lorient, prononcée par arrêté du préfet du Morbihan avec effet au 1er janvier 2012, de réintégrer M. A... dans des fonctions identiques ou équivalentes à celles qu'il occupait à l'OPH " Blavet Habitat " avant la fusion des OPH et de procéder à la régularisation de sa situation administrative ; que, par un premier arrêt du 25 janvier 2013, la cour administrative d'appel de Nantes a prononcé le sursis à exécution de ce jugement ; que, par une décision du 28 mai 2014, le Conseil d'Etat, statuant au contentieux a annulé cet arrêt et rejeté la demande de sursis à exécution ; que, par un second arrêt du 18 juillet 2014, la cour administrative d'appel de Nantes a rejeté la requête de l'OPH " Cap l'Orient Agglomération Habitat ", devenu OPH " Lorient Habitat ", tendant, à titre principal, à l'annulation du jugement du 14 juin 2012 et, à titre subsidiaire, à sa réformation en tant qu'il lui enjoint de réintégrer M. A... dans des fonctions identiques ou équivalentes à celles qu'il occupait à l'OPH " Blavet Habitat " et de procéder à la régularisation de sa situation administrative ; que la cour administrative d'appel a également rejeté des " conclusions additionnelles " présentées devant elle par M. A... et tendant, d'une part, à ce qu'il soit enjoint, en application de l'article L. 911-4 du code de justice administrative, à l'OPH " Cap l'Orient Agglomération Habitat " de le réintégrer et de régulariser sa situation administrative conformément à l'article 4 du jugement du 14 juin 2012 et, d'autre part, à l'annulation d'une décision du 31 octobre 2012 de l'office refusant de lui verser sa rémunération pour la période du 25 août au 31 octobre 2012 ainsi que la part variable de cette rémunération à compter du 1er novembre 2012 et de procéder à sa réintégration effective ; que, sous le n° 384685, M. A... se pourvoit en cassation contre cet arrêt en tant qu'il rejette ses " conclusions additionnelles " ; que, sous le n° 384690, l'OPH " Lorient Habitat " se pourvoit en cassation contre cet arrêt en tant qu'il rejette ses conclusions subsidiaires ; <br/>
<br/>
              2. Considérant que les pourvois de M. A... et de l'OPH " Lorient habitat " sont dirigés contre le même arrêt ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              Sur le pourvoi de l'OPH " Lorient Habitat " :<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 10 octobre 2011, le préfet du Morbihan a procédé, en application des dispositions de l'article L. 421-7 du code de la construction et de l'habitation, à la fusion de l'OPH " Blavet Habitat " avec l'OPH de Lorient, avec effet au 1er janvier 2012 et dissolution de l'OPH " Blavet Habitat " sans liquidation et transmission de son patrimoine à l'OPH de Lorient ; que l'OPH de Lorient est devenu l'OPH " Cap l'Orient Agglomération habitat " après transfert à la communauté d'agglomération du Pays de Lorient ; que, après avoir relevé que la fusion des OPH de Lorient et de " Blavet Habitat " à compter du 1er janvier 2012 avait provoqué la suppression, le 31 décembre 2011, de l'emploi occupé par M. A...avant son départ et jugé que ce dernier ne pouvait prétendre qu'à une réintégration juridique au sein de l'OPH " Cap l'Orient Agglomération habitat " pour la période du 1er juillet au 31 décembre 2011, la cour administrative d'appel de Nantes a rejeté la requête de cet office, y compris ses conclusions subsidiaires tendant à l'annulation de l'article 4 du jugement du 14 juin 2012 en ce qu'il lui enjoignait de procéder à la réintégration effective de M. A... dans des fonctions identiques ou équivalentes à celles qu'il occupait à l'OPH " Blavet Habitat " ; qu'en statuant ainsi, sans d'ailleurs se prononcer expressément dans ses motifs sur les conclusions subsidiaires de l'office, la cour a entaché son arrêt d'une contradiction entre ses motifs et son dispositif ; que, dès lors, l'OPH " Lorient habitat " est fondé, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, à demander l'annulation, sur ce point, de l'arrêt qu'il attaque ;<br/>
<br/>
              Sur le pourvoi de M. A... :<br/>
<br/>
              4. Considérant, d'une part, qu'il ressort des pièces du dossier soumis à la cour administrative d'appel de Nantes que M. A... a demandé, dans ses " conclusions additionnelles ", l'annulation de la décision du 31 octobre 2012 de l'OPH " Cap l'Orient Agglomération Habitat ", postérieure au jugement du tribunal administratif de Rennes contre lequel il formait appel, en ce qu'elle lui refuse le versement de sa rémunération pour la période du 25 août au 31 octobre 2012 ainsi que la part variable de cette rémunération à compter du 1er novembre 2012 et rejette sa demande de réintégration effective ; que de telles conclusions ne pouvaient pas être portées devant la cour dans le cadre du litige d'appel dont elle était saisie ; que ce motif, qui est d'ordre public et n'appelle l'appréciation d'aucune circonstance de fait, doit être substitué au motif retenu par l'arrêt attaqué, dont il justifie, sur ce point, le dispositif ;<br/>
<br/>
              5. Considérant, d'autre part, qu'il ressort également des pièces du dossier soumis à la cour que, par ses " conclusions additionnelles ", M. A... a aussi demandé à la cour qu'il soit enjoint à l'OPH " Cap l'Orient Agglomération Habitat " de le réintégrer et de régulariser sa situation administrative, conformément à l'article 4 du jugement du 14 juin 2012 ; qu'il a ainsi demandé à la juridiction d'appel d'assurer, en application des dispositions de l'article L. 911-4 du code de justice administrative et selon la procédure prévue au livre IX de ce code, l'exécution de l'article 4 du jugement du tribunal administratif de Rennes du 14 juin 2012 ; qu'en regardant de telles conclusions comme des conclusions d'appel incident tendant à l'annulation ou à la réformation du dispositif du jugement du tribunal administratif, la cour administrative d'appel a méconnu son office ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que M. A... est fondé, sans qu'il soit besoin d'examiner les moyens de son pourvoi, à demander l'annulation de l'arrêt attaqué en tant qu'il a statué sur ses " conclusions additionnelles " tendant à obtenir l'exécution de l'article 4 du jugement du tribunal administratif de Rennes du 14 juin 2012 ;<br/>
<br/>
              7. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond, dans la mesure de la cassation prononcée ci-dessus, par application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Sur les conclusions d'appel subsidiaires de l'OPH " Cap L'Orient Agglomération Habitat "<br/>
<br/>
              8. Considérant, d'une part, qu'aux termes de l'article L. 911-1 du code de justice administrative : " Lorsque sa décision implique nécessairement qu'une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public prenne une mesure d'exécution dans un sens déterminé, la juridiction, saisie de conclusions en ce sens, prescrit, par la même décision, cette mesure assortie, le cas échéant, d'un délai d'exécution. " ; qu'aux termes de l'article L. 911-2 du même code : " Lorsque sa décision implique nécessairement qu'une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public prenne à nouveau une décision après une nouvelle instruction, la juridiction, saisie de conclusions en ce sens, prescrit, par la même décision juridictionnelle, que cette nouvelle décision doit intervenir dans un délai déterminé. " ;<br/>
<br/>
              9. Considérant, d'autre part, qu'aux termes de l'article L. 421-7 du code de la construction et de l'habitation : " (...) Un ou plusieurs offices publics de l'habitat peuvent, par voie de fusion, transmettre leur patrimoine à un office public de l'habitat existant. La fusion entraîne la dissolution sans liquidation des offices qui disparaissent et la transmission universelle de leur patrimoine à l'office public de l'habitat bénéficiaire, dans l'état où il se trouve à la date de réalisation définitive de l'opération. / Le changement de collectivité territoriale ou d'établissement public de rattachement d'un office, le changement de son appellation, ainsi que la fusion de plusieurs offices sont prononcés par le préfet sur demande des organes délibérants des collectivités territoriales et des établissements publics intéressés, dans des conditions définies par décret en Conseil d'Etat. " ; qu'aux termes du V de l'article 120 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale : " En cas de fusion entre offices publics de l'habitat, les fonctionnaires territoriaux et les fonctionnaires mentionnés à l'article 118 de la présente loi relevant des offices concernés et qui sont placés dans l'une des positions prévues à l'article 55 de la présente loi, ainsi que les agents non titulaires employés par ces offices sont réputés relever de l'office issu du regroupement dans les conditions de statut et d'emploi qui sont les leurs. " ; qu'il résulte de ces dispositions qu'en cas de fusion entre offices publics de l'habitat sur le fondement des dispositions précitées de l'article L. 421-7 du code de la construction et de l'habitation, le contrat de droit public conclu entre l'office public de l'habitat qui a fait l'objet d'une dissolution et son directeur général est repris de plein droit par l'office public de l'habitat issu de la fusion ;<br/>
<br/>
              	10. Considérant qu'il résulte des dispositions de l'article 120 de la loi du 26 janvier 1984 qu'à la suite de l'annulation de l'acte de rupture conventionnelle du 26 janvier 2011 mettant fin au contrat à durée indéterminée de M. A..., il incombait à l'OPH " Cap L'Orient Agglomération Habitat", d'une part, de régulariser la situation administrative de M. A..., d'autre part, de rechercher s'il était possible de réintégrer M. A... dans un de ses emplois de direction, en vertu d'un contrat de droit public à durée indéterminée alors même que ces emplois sont en principe soumis aux règles du code du travail, ou, à défaut d'un tel emploi et si l'intéressé le demandait, dans tout autre emploi de l'office, le licenciement de M. A...ne pouvant être envisagé, sous réserve du respect des règles relatives au préavis et aux droits à indemnités qui lui sont applicables, que si une telle réintégration s'avérait impossible faute d'emploi vacant ou du fait du refus par l'intéressé de la proposition qui lui serait faite ; que l'annulation de l'acte de rupture conventionnelle du 26 janvier 2011 n'impliquait donc pas nécessairement la réintégration effective de l'intéressé dans les fonctions de directeur général ; que, par suite, l'OPH requérant est fondé à soutenir que c'est à tort que le tribunal administratif lui a enjoint, sur le fondement des dispositions de l'article L. 911-1 du code de justice administrative, par l'article 4 du jugement attaqué, de réintégrer M. A... dans des fonctions identiques ou équivalentes à celles qu'il occupait à l'OPH " Blavet habitat " avant la fusion des deux établissements publics dans le délai de deux mois à compter de la notification de son jugement ; qu'il y a seulement lieu d'enjoindre à l'OPH " Lorient Habitat ", en application des dispositions de l'article L. 911-2 du code de justice administrative, de régulariser la situation administrative de M. A...et de procéder au réexamen défini ci-dessus, dans le délai de deux mois à compter de la notification de la présente décision, et d'annuler l'article 4 du jugement du tribunal administratif de Rennes ;<br/>
<br/>
              Sur les conclusions " additionnelles " de M. A... :<br/>
<br/>
              11. Considérant qu'en raison de l'annulation de l'article 4 du jugement du tribunal administratif de Rennes du 14 juin 2012, les " conclusions additionnelles " de M. A...tendant à l'exécution de cet article, en application des dispositions de l'article L. 911-4 du code de justice administrative, ne peuvent qu'être rejetées ;<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              12. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'OPH " Lorient Habitat " qui n'est pas, dans la présente instance, la partie perdante ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de M. A... la somme que demande l'OPH " Lorient Habitat " au titre de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nantes du 18 juillet 2014 est annulé en tant, d'une part, qu'il rejette les conclusions de l'OPH " Cap l'Orient Agglomération Habitat " tendant à l'annulation de l'article 4 du jugement tribunal administratif de Rennes du 14 juin 2012 et, d'autre part, qu'il statue sur les " conclusions additionnelles " présentées par M. A... tendant à obtenir l'exécution de cet article 4.<br/>
<br/>
Article 2 : Il est enjoint à l'OPH " Lorient Habitat " de procéder, sur le fondement de l'article L. 911-2 du code de justice administrative, à la régularisation de la situation administrative de M. A... et au réexamen défini au point 10 de la présente décision dans le délai de deux mois à compter de la notification de celle-ci.<br/>
<br/>
Article 3 : L'article 4 du jugement du tribunal administratif de Rennes du 14 juin 2012 est annulé.<br/>
<br/>
Article 4 : Les " conclusions additionnelles " présentées par M. A... devant la cour administrative d'appel de Nantes sont rejetées.<br/>
<br/>
Article 5 : Le surplus du pourvoi de M. A...est rejeté. <br/>
<br/>
Article 6: Les conclusions présentées par l'OPH " Lorient Habitat " et par M. A... au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 7 : La présente décision sera notifiée à M. B... A...et à l'OPH " Lorient Habitat ".<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-10-06-02 Fonctionnaires et agents publics. Cessation de fonctions. Licenciement. Auxiliaires, agents contractuels et temporaires.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-12-03 Fonctionnaires et agents publics. Agents contractuels et temporaires. Fin du contrat.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">38-04-01-005 Logement. Habitations à loyer modéré. Organismes d'habitation à loyer modéré.
</SCT>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
