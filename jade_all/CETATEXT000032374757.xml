<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032374757</ID>
<ANCIEN_ID>JG_L_2016_04_000000374489</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/37/47/CETATEXT000032374757.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème / 5ème SSR, 06/04/2016, 374489</TITRE>
<DATE_DEC>2016-04-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>374489</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème / 5ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>Mme Pauline Pannier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:374489.20160406</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 8 janvier et 11 juin 2014 au secrétariat du contentieux du Conseil d'Etat, la société Stud'Arts demande au Conseil d'Etat :<br/>
<br/>
              1°) de condamner l'Etat à lui verser une indemnité de 504 120,40 euros, en réparation du préjudice qu'elle estime avoir subi en raison de la durée excessive des différentes procédures engagées devant la juridiction administrative ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code général des impôts ; <br/>
              - le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Pannier, auditeur,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Odent, Poulet, avocat de la société Stud'Arts ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il résulte de l'instruction que la société Stud'Arts a fait l'objet, le 11 août 1998, d'un avis de recouvrement portant sur la récupération du remboursement d'un crédit de TVA, au motif qu'elle n'était pas assujettie à cet impôt ; qu'elle a introduit une réclamation auprès de l'administration fiscale le 8 octobre 1998, en demandant à bénéficier de la suspension de l'exigibilité de sa créance en application des dispositions de l'article L. 277 du livre des procédures fiscales ; qu'elle a contesté le rejet de sa réclamation par une demande introduite le 13 février 2002 devant le tribunal administratif de Montpellier, qui l'a rejetée pour irrecevabilité le 30 novembre 2006 ; que, sur appel de la société Stud'Arts, la cour administrative d'appel de Marseille a confirmé, par un arrêt du 22 juin 2010, le jugement du tribunal administratif ; que par une décision du 23 décembre 2011, le Conseil d'Etat statuant au contentieux a refusé d'admettre le pourvoi en cassation formé par la société contre cet arrêt ; que la société Stud'Arts recherche la responsabilité de l'Etat en réparation du préjudice qu'elle estime avoir subi du fait de la durée excessive de cette procédure ;<br/>
<br/>
              Sur le caractère raisonnable du délai de jugement :<br/>
<br/>
              2. Considérant qu'il résulte des principes généraux qui gouvernent le fonctionnement des juridictions administratives que les justiciables ont droit à ce que leurs requêtes soient jugées dans un délai raisonnable ; que si la méconnaissance de cette obligation est sans incidence sur la validité de la décision juridictionnelle prise à l'issue de la procédure, les justiciables doivent néanmoins pouvoir en faire assurer le respect ; qu'ainsi, lorsque la méconnaissance du droit à un délai raisonnable de jugement leur a causé un préjudice, ils peuvent obtenir la réparation du dommage ainsi causé par le fonctionnement défectueux du service public de la justice ; que le caractère raisonnable du délai de jugement d'une affaire doit s'apprécier de manière à la fois globale, compte tenu notamment, de l'exercice des voies de recours, particulière à chaque instance et concrète, en prenant en compte sa complexité, les conditions de déroulement de la procédure et, en particulier, le comportement des parties tout au long de celle-ci, mais aussi, dans la mesure où la juridiction saisie a connaissance de tels éléments, l'intérêt qu'il peut y avoir, pour l'une ou l'autre, compte tenu de sa situation particulière, des circonstances propres au litige et, le cas échéant, de sa nature même, à ce qu'il soit tranché rapidement ;<br/>
<br/>
              3. Considérant qu'aux termes de l'article R* 198-10 du livre des procédures fiscales : " La direction générale des finances publiques ou la direction générale des douanes et droits indirects, selon le cas, statue sur les réclamations dans le délai de six mois suivant la date de leur présentation " ; que la société Stud'Arts soutient que l'administration fiscale n'a expressément rejeté sa réclamation du 8 octobre 1998 que par une décision notifiée le 3 janvier 2002 qu'elle n'a pu contester devant le tribunal administratif de Montpellier que le 13 février 2002 et qu'il doit, par suite, être tenu compte de la durée excessive de ce recours administratif préalable obligatoire dans l'appréciation du caractère raisonnable de la durée globale de la procédure ;<br/>
<br/>
              4. Considérant toutefois qu'aux termes de l'article R* 199-1 du même livre : " L'action doit être introduite devant le tribunal compétent dans le délai de deux mois à partir du jour de la réception de l'avis par lequel l'administration notifie au contribuable la décision prise sur la réclamation, que cette notification soit faite avant ou après l'expiration du délai de six mois prévu à l'article R. 198-10. / Toutefois, le contribuable qui n'a pas reçu la décision de l'administration dans un délai de six mois mentionné au premier alinéa peut saisir le tribunal dès l'expiration de ce délai " ; que la société Stud'Arts pouvait ainsi, à l'expiration du délai de six mois suivant la réception de sa réclamation du 8 octobre 1998, saisir le tribunal administratif ; que le caractère excessif de la durée de la procédure précontentieuse lui est ainsi entièrement imputable ;<br/>
<br/>
              5. Considérant, en revanche, qu'il résulte de ce qui a été dit au point 1 ci-dessus que la durée totale de la procédure juridictionnelle devant les trois degrés de juridiction pour se prononcer sur la demande de la société Stud'Arts a été de neuf ans et dix mois ; que si, devant le tribunal administratif de Montpellier, la société Stud'Arts a elle-même tardé à répondre aux mémoires en défense de l'administration et n'a ainsi pas fait preuve d'une diligence raisonnable, ce qui a partiellement concouru à l'allongement de la durée de la procédure, elle est cependant fondée à soutenir que la durée totale de la procédure est excessive, dès lors que le litige ne présentait pas de complexité particulière ; <br/>
<br/>
              6. Considérant que son droit à un délai raisonnable de jugement ayant été méconnu, la société requérante est fondée à obtenir, pour ce motif, la réparation des préjudices qu'elle a subis ;<br/>
<br/>
<br/>
              Sur les préjudices :<br/>
<br/>
              En ce qui concerne le préjudice moral :<br/>
<br/>
              7. Considérant qu'il résulte de l'instruction que la durée excessive de la procédure contentieuse a occasionné pour la société Stud'Arts un préjudice moral consistant en des désagréments qui vont au-delà des préoccupations habituellement causés par un procès ; que, dans les circonstances de l'espèce, il sera fait une juste appréciation de ce préjudice en condamnant l'Etat à lui verser une indemnité de 3 000 euros ; <br/>
<br/>
              En ce qui concerne le préjudice né du paiement d'intérêts de retard :<br/>
<br/>
              8. Considérant qu'aux termes du I de l'article 1727 du code général des impôts : " Toute créance de nature fiscale, dont l'établissement ou le recouvrement incombe aux administrations fiscales, qui n'a pas été acquittée dans le délai légal donne lieu au versement d'un intérêt de retard. (...) " ; qu'aux termes du IV du même article : " 1. L'intérêt de retard est calculé à compter du premier jour du mois suivant celui au cours duquel l'impôt devait être acquitté jusqu'au dernier jour du mois du paiement (...) " ; que par ailleurs, aux termes de l'article L. 277 du livre des procédures fiscales dans sa rédaction alors applicable : " Le contribuable qui conteste le bien-fondé ou le montant des impositions mises à sa charge peut, s'il en a expressément formulé la demande dans sa réclamation et précisé le montant ou les bases du dégrèvement auquel il estime avoir droit, être autorisé à différer le paiement de la partie contestée de ces impositions et des pénalités y afférentes (...) " ; qu'en application de ces dispositions, si le contribuable qui a demandé la suspension de l'exigibilité de sa créance en application de l'article L. 277 cité ci-dessus est dispensé de la payer jusqu'à ce qu'il ait été statué sur son recours, il devient redevable, à la date du jugement du tribunal administratif rejetant sa demande, des intérêts de retard ayant couru depuis l'établissement de la créance, même si ce jugement est frappé d'appel ; que la durée éventuellement excessive de la procédure après l'intervention du jugement du tribunal administratif ne peut lui avoir causé un préjudice au titre des intérêts de retard dès lors qu'il lui appartenait de payer sa créance et les intérêts de retard déjà constitués dès l'intervention de ce jugement ; que, toutefois, lorsque le délai mis par le tribunal administratif à statuer sur ce recours est excessif, le contribuable est fondé à obtenir réparation pour la fraction des intérêts de retard qui sont imputables à la durée qui excède le délai raisonnable de jugement devant ce tribunal ;<br/>
<br/>
              9. Considérant que la société Stud'Arts ayant demandé le bénéfice des dispositions de l'article L. 277, l'exigibilité de sa créance a été suspendue jusqu'à ce que le tribunal administratif de Montpellier rejette sa demande, date à laquelle la société devenait alors redevable du paiement tant du principal que des intérêts de sa créance ; qu'elle est ainsi fondée à soutenir que le caractère excessif de la durée de la procédure devant le tribunal administratif de Montpellier lui a causé un préjudice correspondant aux intérêts de retard imputables à la fraction de cette durée qui a excédé le délai raisonnable de jugement devant ce tribunal ; <br/>
<br/>
              10. Considérant que, eu égard notamment aux délais mis par la requérante pour répondre aux mémoires en défense de l'administration devant le tribunal administratif de Montpellier ainsi qu'il a été dit au point 5, la période ayant excédé le délai raisonnable de jugement devant ce tribunal est de deux ans ; qu'il résulte de l'instruction, et notamment des pièces versées par l'administration à la suite de la mesure d'instruction diligentée par la quatrième sous-section de la section du contentieux du Conseil d'Etat, que les intérêts de retard réclamés à la société Stud'Arts au titre de cette période s'élèvent à 78 612 euros ;<br/>
<br/>
              11. Considérant qu'il résulte toutefois de l'instruction que, sur la totalité des 305 395 euros d'intérêts de retard réclamés à la société requérante et dont elle demande réparation, le ministre chargé des finances lui a accordé une remise gracieuse de 82 060 euros ; que, dans ces conditions, le préjudice né de la durée excessive de la procédure devant le tribunal administratif de Montpellier au titre du paiement des intérêts de retard doit être, dans la même proportion, ramené à la somme de 57 488 euros ;<br/>
              En ce qui concerne le préjudice né du versement de la TVA :<br/>
<br/>
              12. Considérant que si la société requérante, qui soutenait être assujettie à la TVA, a décidé de poursuivre, pendant la procédure contentieuse, le versement de la TVA dont elle s'estimait redevable vis-à-vis de l'administration fiscale, le préjudice né du caractère indu de ce versement est sans lien direct avec la durée excessive de la procédure devant les juridictions administratives ; que, dès lors, ses conclusions indemnitaires présentées au titre de ce chef de préjudice doivent être rejetées ;<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              13. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la société requérante au titre de ces dispositions ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : L'Etat est condamné à verser à la société Stud'Arts la somme de 60 488 euros.<br/>
<br/>
Article 2 : L'Etat versera à la société Stud'Arts la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : Le surplus des conclusions de la requête de la société Stud'Arts est rejeté.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la société Stud'Arts et au garde des sceaux, ministre de la justice.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">37-06 JURIDICTIONS ADMINISTRATIVES ET JUDICIAIRES. RESPONSABILITÉ DU FAIT DE L'ACTIVITÉ DES JURIDICTIONS. - RESPONSABILITÉ DU FAIT DU DÉLAI EXCESSIF DE JUGEMENT - 1) PRISE EN COMPTE DE LA DURÉE EXCESSIVE DU RAPO - EXISTENCE - CAS D'ESPÈCE - 2) INDEMNISATION DES INTÉRÊTS DE RETARD DUS À LA DATE DU JUGEMENT   STATUANT SUR UN LITIGE FISCAL - A) CAS GÉNÉRAL - B) ESPÈCE - PRISE EN COMPTE D'UNE REMISE GRACIEUSE ACCORDÉE PAR L'ADMINISTRATION FISCALE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-01-03-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. FAITS SUSCEPTIBLES OU NON D'OUVRIR UNE ACTION EN RESPONSABILITÉ. AGISSEMENTS ADMINISTRATIFS SUSCEPTIBLES D'ENGAGER LA RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RETARDS. - DURÉE EXCESSIVE DE JUGEMENT - 1) PRISE EN COMPTE DE LA DURÉE EXCESSIVE DU RAPO - EXISTENCE - CAS D'ESPÈCE - 2) INDEMNISATION DES INTÉRÊTS DE RETARD DUS À LA DATE DU JUGEMENT   STATUANT SUR UN LITIGE FISCAL - A) CAS GÉNÉRAL - B) ESPÈCE - PRISE EN COMPTE D'UNE REMISE GRACIEUSE ACCORDÉE PAR L'ADMINISTRATION FISCALE.
</SCT>
<ANA ID="9A"> 37-06 1) Dans l'appréciation du caractère raisonnable de la durée globale d'une procédure juridictionnelle, il doit être tenu compte, le cas échéant, de la durée excessive d'un recours administratif préalable obligatoire (RAPO) à l'introduction du recours.... ,,En l'espèce, la réclamation fiscale du 8 octobre 1998 n'a été expressément rejetée que par une décision notifiée le 3 janvier 2002. Cependant, en vertu de l'article R* 199-1 du livre des procédures fiscales, l'action pouvait être introduite dès l'expiration d'un délai de six mois sans réponse de l'administration fiscale sur la réclamation. Dès lors, le caractère excessif de la durée de la procédure précontentieuse est entièrement imputable au requérant.... ,,2) a) En application des articles 1727 du code général des impôts et L. 277 du livre des procédures fiscales, si le contribuable qui a demandé la suspension de l'exigibilité de sa créance fiscale est dispensé de la payer jusqu'à ce qu'il ait été statué sur son recours, il devient redevable, à la date du jugement du tribunal administratif rejetant sa demande, des intérêts de retard ayant couru depuis l'établissement de la créance, même si ce jugement est frappé d'appel. La durée éventuellement excessive de la procédure après l'intervention du jugement du tribunal administratif ne peut lui avoir causé un préjudice au titre des intérêts de retard dès lors qu'il lui appartenait de payer sa créance et les intérêts de retard déjà constitués dès l'intervention de ce jugement. Toutefois, lorsque le délai mis par le tribunal administratif à statuer sur ce recours est excessif, le contribuable est fondé à obtenir réparation pour la fraction des intérêts de retard qui sont imputables à la durée qui excède le délai raisonnable de jugement devant ce tribunal.... ,,b) En l'espèce, il résulte de ces principes que les intérêts de retard réclamés au requérant au titre de la durée excessive de jugement du tribunal administratif s'élèvent à 78 612 euros. Il résulte toutefois de l'instruction que, sur la totalité des 305 395 euros d'intérêts de retard réclamés à la société requérante, le ministre chargé des finances lui a accordé une remise gracieuse de 82 060 euros. Dans ces conditions, le préjudice né de la durée excessive de la procédure devant le tribunal administratif de Montpellier au titre du paiement des intérêts de retard doit être, dans la même proportion, ramené à la somme de 57 488 euros.</ANA>
<ANA ID="9B"> 60-01-03-01 1) Dans l'appréciation du caractère raisonnable de la durée globale d'une procédure juridictionnelle, il doit être tenu compte, le cas échéant, de la durée excessive d'un recours administratif préalable obligatoire (RAPO) à l'introduction du recours.... ,,En l'espèce, la réclamation fiscale du 8 octobre 1998 n'a été expressément rejetée que par une décision notifiée le 3 janvier 2002. Cependant, en vertu de l'article R* 199-1 du livre des procédures fiscales, l'action pouvait être introduite dès l'expiration d'un délai de six mois sans réponse de l'administration fiscale sur la réclamation. Dès lors, le caractère excessif de la durée de la procédure précontentieuse est entièrement imputable au requérant.... ,,2) a) En application des articles 1727 du code général des impôts et L. 277 du livre des procédures fiscales, si le contribuable qui a demandé la suspension de l'exigibilité de sa créance fiscale est dispensé de la payer jusqu'à ce qu'il ait été statué sur son recours, il devient redevable, à la date du jugement du tribunal administratif rejetant sa demande, des intérêts de retard ayant couru depuis l'établissement de la créance, même si ce jugement est frappé d'appel. La durée éventuellement excessive de la procédure après l'intervention du jugement du tribunal administratif ne peut lui avoir causé un préjudice au titre des intérêts de retard dès lors qu'il lui appartenait de payer sa créance et les intérêts de retard déjà constitués dès l'intervention de ce jugement. Toutefois, lorsque le délai mis par le tribunal administratif à statuer sur ce recours est excessif, le contribuable est fondé à obtenir réparation pour la fraction des intérêts de retard qui sont imputables à la durée qui excède le délai raisonnable de jugement devant ce tribunal.... ,,b) En l'espèce, il résulte de ces principes que les intérêts de retard réclamés au requérant au titre de la durée excessive de jugement du tribunal administratif s'élèvent à 78 612 euros. Il résulte toutefois de l'instruction que, sur la totalité des 305 395 euros d'intérêts de retard réclamés à la société requérante, le ministre chargé des finances lui a accordé une remise gracieuse de 82 060 euros. Dans ces conditions, le préjudice né de la durée excessive de la procédure devant le tribunal administratif de Montpellier au titre du paiement des intérêts de retard doit être, dans la même proportion, ramené à la somme de 57 488 euros.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
