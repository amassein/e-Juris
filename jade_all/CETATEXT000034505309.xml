<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034505309</ID>
<ANCIEN_ID>JG_L_2017_04_000000398102</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/50/53/CETATEXT000034505309.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 25/04/2017, 398102, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-04-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>398102</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>Mme Mireille Le Corre</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:398102.20170425</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              	M. A...E...et M. C...E...ont demandé au tribunal administratif de Montreuil d'annuler pour excès de pouvoir l'arrêté du 27 septembre 2012 par lequel le maire de La Courneuve a délivré un permis de construire à M. B...D...pour un immeuble sur un terrain situé 10, rue Albert Duludet à La Courneuve. Par un jugement n° 1500404 du 21 janvier 2016, le tribunal administratif de Montreuil a rejeté leur demande.<br/>
<br/>
              	Par un pourvoi sommaire, un autre mémoire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 21 mars, 24 mars et 8 juin 2016 et 31 janvier 2017 au secrétariat du contentieux du Conseil d'Etat, MM. E...demandent au Conseil d'Etat :<br/>
<br/>
              	1°) d'annuler ce jugement ;<br/>
<br/>
              	2°) réglant l'affaire au fond, de faire droit à leur demande devant le tribunal administratif de Montreuil ;<br/>
<br/>
              	3°) de mettre à la charge de la commune de La Courneuve et de M.D..., la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Mireille Le Corre, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de M. E...et autre et à la SCP Waquet, Farge, Hazan, avocat de la commune de La Courneuve.<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté en date du 27 septembre 2012, le maire de La Courneuve a délivré à M. D... un permis de construire un bâtiment d'habitation, sur un terrain situé 10, rue Albert Duludet ; que par un jugement du 21 janvier 2016 contre lequel MM. E...se pourvoient en cassation, le tribunal administratif de Montreuil a rejeté leur demande tendant à l'annulation de cet arrêté ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article UA-3 du règlement du plan local d'urbanisme de la commune La Courneuve : " 3.1 Accès / Toute propriété pour être constructible doit comporter un accès d'une largeur minimum de 3,50 mètres sur une voie publique, une voie privée ouverte à la circulation générale ou sur un passage privé. / Les accès doivent être adaptés à 1'opération et aménagés de façon à apporter la moindre gêne à la circulation publique. Ils doivent présenter des caractéristiques permettant de satisfaire aux exigences de la sécurité, de la défense contre l'incendie et de la protection civile " ; qu'en prenant en considération les conditions de desserte du terrain d'assiette du projet et notamment les exigences des services de lutte contre l'incendie, et en en déduisant que le projet ne méconnaissait pas les dispositions de l'article UA-3 du règlement du plan local d'urbanisme, alors qu'il comportait une entrée charretière d'une largeur de 2,55 mètres seulement, située directement à l'alignement de la rue, le tribunal administratif a commis une erreur de droit ;<br/>
<br/>
              3. Considérant qu'il suit de là, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, que le jugement du tribunal administratif doit être annulé ;<br/>
<br/>
              4. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de MM. E...qui ne sont pas, dans la présente instance, la partie perdante ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de La Courneuve la somme de 3 500 euros à verser à MM. E...au titre des mêmes dispositions ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Montreuil du 21 janvier 2016 est annulé.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Montreuil.<br/>
Article 3 : La commune de La Courneuve versera à MM. E...la somme totale de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : La présente décision sera notifiée à M. A...E..., représentant désigné, pour l'ensemble des requérants, à la commune de La Courneuve et à M. B...D....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
