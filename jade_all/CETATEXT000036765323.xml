<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036765323</ID>
<ANCIEN_ID>JG_L_2018_03_000000408052</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/76/53/CETATEXT000036765323.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 6ème chambres réunies, 30/03/2018, 408052</TITRE>
<DATE_DEC>2018-03-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>408052</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO ; SCP SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR>M. Guillaume Leforestier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:408052.20180330</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) a demandé au tribunal administratif de Strasbourg de condamner les Hôpitaux universitaires de Strasbourg à lui verser la somme de 127 522,70 euros, correspondant à l'indemnité transactionnelle versée à M. A...B...à raison des préjudices subis à la suite d'une infection nosocomiale contractée lors de son hospitalisation. Par un jugement n° 0802888 du 18 septembre 2012, le tribunal administratif a condamné les Hôpitaux universitaires de Strasbourg, d'une part, à verser à l'ONIAM une somme de 40 000 euros au titre de l'indemnité transactionnelle versée à M. B...et une somme de 6 000 euros au titre de la pénalité prévue par l'article L. 1142-15 du code de la santé publique et, d'autre part, à verser à la caisse primaire d'assurance maladie (CPAM) du Bas-Rhin une somme de 94 971,81 euros en remboursement de ses débours.<br/>
<br/>
              Par un arrêt n° 12NC01877, 12NC01878 du 5 décembre 2013, la cour administrative d'appel de Nancy a, sur appels de l'ONIAM et des Hôpitaux universitaires de Strasbourg, porté ces sommes respectivement à 57 416,62 euros, 8 762,54 euros et 102 656,32 euros. <br/>
<br/>
              Par une décision n° 375286 du 23 décembre 2015, le Conseil d'Etat, statuant au contentieux, a annulé l'arrêt du 5 décembre 2013, en tant qu'il fixait les sommes mises à la charge des Hôpitaux universitaires de Strasbourg au titre des préjudices professionnels de M. B.en litige, des pertes de revenus professionnels et une incidence professionnelle et, dans l'affirmative, d'évaluer ces postes de préjudice sans tenir compte, à ce stade, du fait qu'ils ont donné lieu au versement d'une pension d'invalidité<br/>
<br/>
              Par un arrêt n° 15NC02549, 15NC02573 du 15 décembre 2016, la cour administrative d'appel de Nancy a mis à la charge des Hôpitaux universitaires de Strasbourg une somme de 17 413,62 euros à verser à l'ONIAM au titre des préjudices professionnels de M. B..., avec intérêts de droit et capitalisation des intérêts.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 15 février et 15 mai 2017 au secrétariat du contentieux du Conseil d'Etat, l'ONIAM demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge des Hôpitaux universitaires de Strasbourg la somme de 3 000 euros au titre de l'article L.761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de la santé publique ;<br/>
<br/>
- le code de justice administrative ;<br/>
.<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Leforestier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Sevaux, Mathonnet, avocat de l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales et à Me Le Prado, avocat du centre hospitalier universitaire de Strasbourg.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. B...a été pris en charge par les Hôpitaux universitaires de Strasbourg pour traiter les suites douloureuses d'une chute, dans un service de neurochirurgie du 7 mars au 6 avril 2002 puis dans un service de rhumatologie du 6 au 30 avril 2002 ; qu'il a présenté au cours de cette hospitalisation une infection par staphylocoque doré diagnostiquée le 9 avril 2002 ; que les suites de cette infection ont rendu nécessaire le remplacement par une prothèse de sa hanche droite, le 3 mai 2002, puis de sa hanche gauche, le 15 novembre 2002 ; que, par un avis du 4 mai 2005, la commission régionale de conciliation et d'indemnisation des accidents médicaux d'Alsace a retenu le caractère nosocomial de cette infection ; que l'assureur des Hôpitaux universitaires de Strasbourg ayant refusé de faire une offre d'indemnisation à M. B..., l'ONIAM s'est substitué à cet assureur, en application des dispositions de l'article L. 1142-15 du code de la santé publique, pour proposer à la victime une indemnisation transactionnelle de 127 522, 70 euros, dont 74 719,64 euros au titre du préjudice professionnel subi du 1er janvier 2004 au 1er janvier 2014 ; que M. B...a accepté cette indemnisation le 25 avril 2007 ; que l'ONIAM s'est trouvé, de ce fait, subrogé à hauteur des sommes versées dans les droits de M. B... contre l'auteur du dommage ; que l'office a formé devant le tribunal administratif de Strasbourg un recours subrogatoire dirigé contre les Hôpitaux universitaires de Strasbourg auquel le tribunal, après avoir appelé en la cause la caisse primaire d'assurance maladie (CPAM) du Bas-Rhin, a partiellement fait droit par un jugement du 18 septembre 2012 en condamnant notamment les Hôpitaux universitaires de Strasbourg à verser à l'ONIAM une somme de 40 000 euros, ainsi qu'une somme de 6 000 euros au titre de la pénalité prévue par l'article L. 1142-15 du code de la santé publique, et à la CPAM du Bas-Rhin la somme de 94 971, 81 euros ; que la cour administrative d'appel de Nancy, statuant après les avoir joints sur les appels de l'ONIAM et des Hôpitaux universitaires de Strasbourg, a, par un arrêt du 5 décembre 2013, porté ces sommes, respectivement, à 58 416, 62 euros, 8 762, 54 euros et 102 656, 32 euros ; que, par une décision du 23 décembre 2015, le Conseil d'Etat, statuant au contentieux sur le pourvoi principal des Hôpitaux universitaires de Strasbourg et sur le pourvoi incident de l'ONIAM, a annulé l'arrêt du 5 décembre 2013 en tant qu'il fixait les sommes mises à la charge des Hôpitaux universitaires de Strasbourg au titre des préjudices professionnels de M. B... ; que par un arrêt du 15 décembre 2016, la cour administrative d'appel de Nancy, statuant à nouveau sur le litige dans la limite de la cassation ainsi prononcée, a condamné les Hôpitaux universitaires de Strasbourg à verser, au titre des préjudices professionnels de la victime,  une somme de 17 413,62 euros à l'ONIAM et une somme de 40 872,24 euros à la CPAM du Bas-Rhin ; que l'ONIAM se pourvoit en cassation contre cet arrêt ;<br/>
<br/>
              2. Considérant que l'arrêt attaqué constate que M. B...s'est trouvé dans l'incapacité, du fait de l'infection nosocomiale et de la mise en place de prothèses des hanches, de poursuivre son activité professionnelle de tuyauteur soudeur et qu'il a perdu son emploi en novembre 2004 ; qu'il regarde comme une conséquence directe de l'infection l'intégralité des pertes de revenus subies jusqu'au 31 juillet 2006 ; qu'en revanche, pour la période ultérieure, il juge que l'intéressé n'a subi aucun préjudice professionnel dès lors qu'il " n'est pas inapte à toute activité professionnelle, sous réserve qu'elle soit compatible avec son handicap " ; qu'en excluant ainsi tout préjudice professionnel postérieurement au 31 juillet 2006, alors qu'elle avait constaté l'impossibilité de poursuivre l'activité exercée antérieurement et l'existence d'un handicap limitant les possibilités de reconversion professionnelle, la cour administrative d'appel de Nancy n'a pas tiré les conséquences de ses propres constatations et a commis une erreur de droit ; que son arrêt doit, par suite, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, être annulé en tant qu'il statue sur les préjudices professionnels subis par M. B...à compter du 1er août 2006 et refuse toute indemnité à ce titre à l'ONIAM et à la CPAM du Bas-Rhin ;<br/>
<br/>
              3. Considérant qu'aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire " ; que, le Conseil d'Etat étant saisi, en l'espèce, d'un second pourvoi en cassation, il lui incombe de régler l'affaire au fond ;<br/>
<br/>
              4. Considérant, d'une part, qu'en application des dispositions de l'article L. 376-1 du code de la sécurité sociale, dans sa rédaction résultant de la loi du 21 décembre 2006 relative au financement de la sécurité sociale, le juge saisi d'un recours de la victime d'un dommage corporel ou d'un recours de l'ONIAM, subrogé dans les droits de la victime en application de l'article L. 1142-15 du code de la santé publique, et du recours subrogatoire d'un organisme de sécurité sociale doit, pour chacun des postes de préjudices, déterminer le montant du préjudice en précisant la part qui a été réparée par des prestations de sécurité sociale et celle qui est demeurée à la charge de la victime ; qu'il lui appartient ensuite de fixer l'indemnité mise à la charge de l'auteur du dommage au titre du poste de préjudice en tenant compte, s'il a été décidé, du partage de responsabilité avec la victime ; que le juge doit allouer cette indemnité à la victime ou à l'ONIAM, subrogé dans ses droits, dans la limite de la part du poste de préjudice qui n'a pas été réparée par des prestations, le solde, s'il existe, étant alloué à l'organisme de sécurité sociale ; que lorsqu'il procède à l'évaluation des préjudices subis afin de fixer le montant des indemnités dues à l'ONIAM subrogé dans des droits de la victime en application des dispositions de l'article L. 1142-15, le juge n'est pas lié par le contenu de la transaction intervenue entre l'ONIAM et la victime ; que l'ONIAM ne saurait, toutefois, obtenir un montant supérieur à celui qu'il a versé à la victime ; <br/>
<br/>
              5. Considérant, d'autre part, qu'eu égard à la finalité de réparation d'une incapacité permanente de travail qui lui est assignée par ces dispositions de l'article L. 341-1 du code de la sécurité sociale et à son mode de calcul, en fonction du salaire, fixé par l'article R. 341-4 du même code, la pension d'invalidité doit être regardée comme ayant pour objet exclusif de réparer, sur une base forfaitaire, les préjudices subis par la victime dans sa vie professionnelle en conséquence de l'accident, c'est-à-dire ses pertes de revenus professionnels et l'incidence professionnelle de son incapacité ; que, dès lors, le recours exercé par une caisse de sécurité sociale au titre d'une pension d'invalidité ne saurait s'exercer que sur ces deux postes de préjudice ; <br/>
<br/>
              6. Considérant qu'il convient, en conséquence, de déterminer si l'incapacité permanente conservée par M. B...en raison de l'infection contractée lors de son hospitalisation a entraîné, pendant la période postérieure au 31 juillet 2006 demeurant en litige, des pertes de revenus professionnels et une incidence professionnelle et, dans l'affirmative, d'évaluer ces postes de préjudice sans tenir compte, à ce stade, du fait qu'ils ont donné lieu au versement d'une pension d'invalidité; que, pour déterminer ensuite dans quelle mesure ces préjudices ont été réparés par la pension, il y a lieu de regarder cette prestation comme réparant prioritairement les pertes de revenus professionnels et, par suite, comme ne réparant tout ou partie de l'incidence professionnelle que si la victime ne subissait pas de pertes de revenus ou si le montant de ces pertes était inférieur à celui perçu au titre de la pension ;<br/>
<br/>
              7. Considérant que, dans les circonstances de l'espèce, eu égard à la circonstance que M. B...était âgé de 52 ans à la date du 31 juillet 2006 et au fait que son handicap, qui lui a fait perdre son emploi de tuyauteur soudeur dont il tirait des revenus stables, rendait impossible la reprise tant de cette activité que d'une activité comparable, l'infection nosocomiale qu'il a contractée doit être regardée comme la cause directe de la perte de tout revenu professionnel jusqu'à l'âge de la retraite, qu'il a atteint le 15 décembre 2013 ; que, compte tenu des revenus de l'intéressé en 2001, les revenus qu'il aurait dû percevoir entre le 1er août 2006 et le 15 décembre 2013 doivent être évalués à la somme de 154 362,76 euros ; qu'il ne résulte pas de l'instruction que M. B...aurait justifié d'une chance sérieuse d'augmenter ses revenus professionnels au cours de cette période, dont la privation serait constitutive d'une incidence professionnelle ;<br/>
<br/>
              8. Considérant qu'il résulte de l'instruction que, pendant la même période, la CPAM du Bas-Rhin a versé à M. B...une pension d'invalidité pour un montant total de 30 002,70 euros ; que cette prestation doit être regardée comme ayant eu pour objet de réparer ses pertes de revenus ; que la part de ces pertes restée à sa charge s'est en conséquence élevée à 124 360,06 euros ;<br/>
<br/>
              9. Considérant que l'ONIAM est subrogé dans les droits de M. B...à concurrence de la somme de 74 719,64 euros qu'il lui a versée au titre de son préjudice professionnel dans le cadre de la transaction qu'il a conclue avec lui ; que l'arrêt du 15 décembre 2016 de la cour administrative d'appel de Nancy lui ayant accordé une indemnité de 17 413,62 euros au titre du préjudice subi avant le 1er août 2006, il ne peut prétendre, au titre du préjudice subi à compter de cette date, à une somme supérieure à 57 306,02 euros ; que ce montant étant inférieur à celui des pertes de revenus non compensées par la pension d'invalidité subies par M. B...au cours de cette période, il y a lieu d'accorder à l'office une indemnité de ce montant ; que la CPAM du Bas-Rhin peut, quant à elle, prétendre au remboursement de la somme de 30 002,70 euros versée à l'intéressé au titre de la pension d'invalidité au cours de cette même période ; <br/>
<br/>
              10. Considérant qu'il résulte de ce qui précède qu'il y a lieu de mettre à la charge des Hôpitaux universitaires de Strasbourg une somme de 57 306, 02 euros à verser à l'ONIAM et une somme de 30 002,70 à verser à la CPAM du Bas-Rhin, ces sommes s'ajoutant aux montants qu'ils ont été condamnés à leur verser par l'arrêt du 15 décembre 2016 de la cour administrative d'appel de Nancy ; <br/>
<br/>
              Sur les intérêts et leur capitalisation :<br/>
<br/>
              11. Considérant que l'ONIAM a droit aux intérêts au taux légal sur la somme de 57 306, 02 euros à compter du 18 juin 2008, date de réception de sa demande préalable par les Hôpitaux universitaires de Strasbourg ; que la capitalisation des intérêts peut être demandée à tout moment devant le juge du fond ; que si, à la date où elle est demandée, les intérêts sont dus depuis moins d'une année, cette demande ne prend toutefois effet qu'à la date à laquelle, pour la première fois, les intérêts sont dus pour une année entière ; que la capitalisation des intérêts a été demandée par l'ONIAM le 27 juin 2008 au tribunal administratif de Strasbourg ; qu'il y a ainsi lieu de capitaliser les intérêts au 18 juin 2009, date à laquelle une année d'intérêts a été due, et à chaque échéance annuelle ultérieure ; <br/>
<br/>
              12. Considérant que la CPAM du Bas-Rhin a droit aux intérêts au taux légal sur la somme de 30 002,70 euros à compter du 7 juin 2012, date à laquelle elle en a fait la demande devant le tribunal administratif de Strasbourg ;<br/>
<br/>
              Sur l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              13. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge des Hôpitaux universitaires de Strasbourg une somme de 3 000 euros à verser à l'ONIAM au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nancy du 15 décembre 2016 est annulé en tant qu'il statue sur les préjudices professionnels subis par M. B...à compter du 1er août 2006 et refuse toute indemnité à ce titre à l'ONIAM et à la CPAM du Bas-Rhin.<br/>
<br/>
Article 2 : Les Hôpitaux universitaires de Strasbourg sont condamnés à verser à ce titre à l'ONIAM une somme de 57 306, 02 euros, assortie des intérêts au taux légal à compter du 18 juin 2008. Les intérêts échus le 18 juin 2009 seront capitalisés à cette date puis à chaque échéance annuelle ultérieure à compter de cette date pour produire eux mêmes intérêts.<br/>
<br/>
Article 3 : Les Hôpitaux universitaires de Strasbourg sont condamnés à verser à ce titre à la CPAM du Bas-Rhin une somme de 30 002,70 euros, assortie des intérêts au taux légal à compter du 7 juin 2012. <br/>
<br/>
Article 4 : Le jugement du tribunal administratif de Strasbourg du 18 septembre 2012 est réformé en ce qu'il a de contraire à la présente décision.<br/>
<br/>
Article 5 : Les Hôpitaux universitaires de Strasbourg verseront à l'ONIAM une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 6 : La présente décision sera notifiée à l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales, aux Hôpitaux universitaires de Strasbourg, à M. A...B...et à la caisse primaire d'assurance maladie du Bas-Rhin.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-04-01-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RÉPARATION. PRÉJUDICE. ABSENCE OU EXISTENCE DU PRÉJUDICE. - EVALUATION DES PRÉJUDICES SUBIS PAR LA VICTIME EN CONSÉQUENCE D'UN ACCIDENT - APPRÉCIATION DISTINCTE DES PERTES DE REVENUS PROFESSIONNELS DE L'INCIDENCE PROFESSIONNELLE [RJ1] - RÉPARATION DES PERTES DE REVENUS PROFESSIONNELS EN PRIORITÉ - EXISTENCE - RÉPARATION DE TOUT OU PARTIE DE L'INCIDENCE PROFESSIONNELLE - EXISTENCE, SI LA VICTIME NE SUBIT PAS DE PERTE DE REVENUS OU SI LE MONTANT DE CES PERTES EST INFÉRIEUR À CELUI PERÇU AU TITRE DE LA PENSION D'INVALIDITÉ PERÇUE AU TITRE DE L'ARTICLE L. 341-1 DU CSS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-05-04-01-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RECOURS OUVERTS AUX DÉBITEURS DE L'INDEMNITÉ, AUX ASSUREURS DE LA VICTIME ET AUX CAISSES DE SÉCURITÉ SOCIALE. DROITS DES CAISSES DE SÉCURITÉ SOCIALE. IMPUTATION DES DROITS À REMBOURSEMENT DE LA CAISSE. ARTICLE L. 376-1 (ANCIEN ART. L. 397) DU CODE DE LA SÉCURITÉ SOCIALE. - EVALUATION DES PRÉJUDICES SUBIS PAR LA VICTIME EN CONSÉQUENCE D'UN ACCIDENT - APPRÉCIATION DISTINCTE DES PERTES DE REVENUS PROFESSIONNELS DE L'INCIDENCE PROFESSIONNELLE [RJ1] - RÉPARATION DES PERTES DE REVENUS PROFESSIONNELS EN PRIORITÉ - EXISTENCE - RÉPARATION DE TOUT OU PARTIE DE L'INCIDENCE PROFESSIONNELLE - EXISTENCE, SI LA VICTIME NE SUBIT PAS DE PERTE DE REVENUS OU SI LE MONTANT DE CES PERTES EST INFÉRIEUR À CELUI PERÇU AU TITRE DE LA PENSION D'INVALIDITÉ PERÇUE AU TITRE DE L'ARTICLE L. 341-1 DU CSS.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">62-04-03 SÉCURITÉ SOCIALE. PRESTATIONS. PRESTATIONS D'ASSURANCE INVALIDITÉ. - EVALUATION DES PRÉJUDICES SUBIS PAR LA VICTIME EN CONSÉQUENCE D'UN ACCIDENT - APPRÉCIATION DISTINCTE DES PERTES DE REVENUS PROFESSIONNELS DE L'INCIDENCE PROFESSIONNELLE [RJ1] - RÉPARATION DES PERTES DE REVENUS PROFESSIONNELS EN PRIORITÉ - EXISTENCE - RÉPARATION DE TOUT OU PARTIE DE L'INCIDENCE PROFESSIONNELLE - EXISTENCE, SI LA VICTIME NE SUBIT PAS DE PERTE DE REVENUS OU SI LE MONTANT DE CES PERTES EST INFÉRIEUR À CELUI PERÇU AU TITRE DE LA PENSION D'INVALIDITÉ PERÇUE AU TITRE DE L'ARTICLE L. 341-1 DU CSS.
</SCT>
<ANA ID="9A"> 60-04-01-01 Recours de l'ONIAM, subrogé dans les droits d'une victime ayant interrompu son activité professionnelle compte-tenu de son handicap résultant d'un accident.,,,Pour déterminer dans quelle mesure les préjudices ont été réparés par la pension d'invalidité, il y a lieu de regarder cette prestation comme réparant prioritairement les pertes de revenus professionnels et, par suite, comme ne réparant tout ou partie de l'incidence professionnelle que si la victime ne subit pas de pertes de revenus ou si le montant de ces pertes est inférieur à celui perçu au titre de la pension.</ANA>
<ANA ID="9B"> 60-05-04-01-01 Recours de l'ONIAM, subrogé dans les droits d'une victime ayant interrompu son activité professionnelle compte-tenu de son handicap résultant d'un accident.,,,Pour déterminer dans quelle mesure les préjudices ont été réparés par la pension d'invalidité, il y a lieu de regarder cette prestation comme réparant prioritairement les pertes de revenus professionnels et, par suite, comme ne réparant tout ou partie de l'incidence professionnelle que si la victime ne subit pas de pertes de revenus ou si le montant de ces pertes est inférieur à celui perçu au titre de la pension.</ANA>
<ANA ID="9C"> 62-04-03 Recours de l'ONIAM, subrogé dans les droits d'une victime ayant interrompu son activité professionnelle compte-tenu de son handicap résultant d'un accident.,,,Pour déterminer dans quelle mesure les préjudices ont été réparés par la pension d'invalidité, il y a lieu de regarder cette prestation comme réparant prioritairement les pertes de revenus professionnels et, par suite, comme ne réparant tout ou partie de l'incidence professionnelle que si la victime ne subit pas de pertes de revenus ou si le montant de ces pertes est inférieur à celui perçu au titre de la pension.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur la distinction entre pertes de revenus professionnels et incidence professionnelle, CE, Section, avis, 4 juin 2007, Lagier et Consorts Guignon, n°s 303422 304214, p. 228 ; CE, 17 avril 2013, Centre hospitalier d'Elbeuf, n° 346334, T. pp. 842-852. Rappr., pour la rente d'accident du travail, CE, Section, avis, 8 mars 2013,,, n° 361273, p. 38.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
