<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036739765</ID>
<ANCIEN_ID>JG_L_2018_03_000000401562</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/73/97/CETATEXT000036739765.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 23/03/2018, 401562, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-03-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>401562</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP RICHARD</AVOCATS>
<RAPPORTEUR>M. Etienne de Lageneste</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:401562.20180323</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société à responsabilité limitée (SARL) Maysam France a demandé au tribunal administratif de Paris de prononcer la décharge des rappels de taxe sur la valeur ajoutée mis à sa charge pour la période du 1er janvier 2006 au 31 décembre 2008 et des cotisations supplémentaires d'impôt sur les sociétés auxquelles elle a été assujettie au titre des exercices clos en 2006 et 2007. Par un jugement n° 1311009 du 29 avril 2014, ce tribunal a prononcé un non-lieu à statuer à concurrence de dégrèvements accordés en cours d'instance et rejeté le surplus de cette demande.<br/>
<br/>
              Par un arrêt n° 14PA02593 du 13 mai 2016, la cour administrative d'appel de Paris a rejeté l'appel formé par la société Maysam France contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 18 juillet et 18 octobre 2016 au secrétariat du contentieux du Conseil d'Etat, la société Maysam France demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Etienne de Lageneste, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Richard, avocat de la SARL Maysam France ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Maysam France a acquis un immeuble, en qualité de marchand de biens, qu'elle a inscrit en stock et donné en location. A la suite d'une vérification de comptabilité portant sur les années 2006 et 2007, étendue en matière de taxe sur la valeur ajoutée jusqu'au 31 décembre 2008, des rappels de taxe sur la valeur ajoutée et des cotisations supplémentaires d'impôt sur les sociétés ont été notifiés à la société. Celle-ci a demandé au tribunal administratif de Paris de prononcer la décharge des rappels de taxe sur la valeur ajoutée mis à sa charge pour la période du 1er janvier 2006 au 31 décembre 2008 et des cotisations supplémentaires d'impôt sur les sociétés auxquelles elle a été assujettie au titre des exercices clos en 2006 et 2007. Par un jugement du 29 avril 2014, le tribunal a prononcé un non-lieu à statuer à concurrence de dégrèvements accordés en cours d'instance et rejeté le surplus de la demande. La société se pourvoit en cassation contre l'arrêt du 13 mai 2016 par lequel la cour administrative d'appel de Paris a rejeté son appel dirigé contre ce jugement.<br/>
<br/>
              Sur le bien-fondé de l'arrêt en ce qui concerne la procédure d'imposition :<br/>
<br/>
              2. En premier lieu, en cas de retour à l'expéditeur d'un pli recommandé, le contribuable ne peut être regardé comme l'ayant reçu que s'il est établi qu'il a été avisé, par la délivrance d'un avis de passage, de ce que le pli était à sa disposition au bureau de poste dont il relève et n'a été retourné à l'expéditeur qu'après l'expiration du délai de mise en instance prévu par la réglementation en vigueur. Cette preuve peut résulter soit des mentions précises, claires et concordantes portées sur l'enveloppe, soit, à défaut, d'une attestation de l'administration postale ou d'autres éléments de preuve. Doit être regardé comme portant des mentions précises, claires et concordantes suffisant à constituer la preuve d'une notification régulière le pli recommandé retourné à l'administration auquel est rattaché un volet " avis de réception " sur lequel a été apposée par voie de duplication la date de vaine présentation du courrier, et qui porte, sur l'enveloppe ou sur l'avis de réception, l'indication du motif pour lequel il n'a pu être remis.<br/>
<br/>
              3. En jugeant, après avoir relevé, par une appréciation souveraine non arguée de dénaturation, que les avis de réception postaux des plis contenant les avis de mise en recouvrement des impositions litigieuses portaient, d'une part, une date de présentation au 12 octobre 2009 et, d'autre part, une mention " pas de réponse à 10h20 - avisé ", que la société avait été avisée, à la date de présentation des courriers, de la mise en instance de ceux-ci et que l'administration apportait ainsi la preuve de la notification régulière des avis de mise en recouvrement, la cour n'a pas commis d'erreur de droit.<br/>
<br/>
              4. En second lieu, lorsque le mandat donné à un conseil ou à tout autre mandataire par un contribuable pour l'assister dans ses relations avec l'administration ne contient aucune mention expresse habilitant le mandataire à recevoir l'ensemble des actes de la procédure d'imposition, ce mandat n'emporte pas élection de domicile auprès de ce mandataire. Dans ce cas, l'administration n'entache pas la procédure d'imposition d'irrégularité en notifiant l'ensemble des actes de la procédure au contribuable, alors même que le mandat confie au mandataire le soin de répondre à toute notification de redressements et d'accepter ou de refuser tout redressement.<br/>
<br/>
              5. La cour a relevé, par une appréciation souveraine non arguée de dénaturation, que la société requérante n'établissait ni avoir donné à son conseil mandat pour recevoir l'ensemble des actes de procédure, ni avoir informé le service qu'elle entendait élire domicile auprès de son conseil, lequel n'avait pas davantage informé le service que la société Maysam France avait élu domicile à son cabinet. C'est dès lors sans erreur de droit qu'elle en a déduit que la notification au contribuable, et non à son mandataire, des avis de mise en recouvrement n'était pas irrégulière.<br/>
<br/>
              Sur le bien-fondé de l'arrêt en ce qui concerne le bien-fondé des impositions :<br/>
<br/>
              S'agissant de la taxe sur la valeur ajoutée :<br/>
<br/>
              6. Pour juger que la société Maysam France n'était pas fondée à soutenir qu'elle ne pouvait être assujettie à la taxe sur la valeur ajoutée, la cour s'est appuyée sur la déclaration d'existence que cette société avait déposée au centre de formalités des entreprises lors de sa constitution, dont elle a relevé qu'elle avait été produite en défense. Toutefois, il ne ressort pas des pièces du dossier de la procédure d'appel ou du dossier de première instance que cette pièce aurait été produite, ce que reconnaît d'ailleurs le ministre de l'action et des comptes publics en défense. Ainsi, la cour a dénaturé les pièces du dossier qui lui était soumis. Par suite, sans qu'il soit besoin de statuer sur l'autre moyen relatif à ce chef d'imposition, la société Maysam France est fondée à demander l'annulation de l'arrêt qu'elle attaque en tant qu'il a statué sur les conclusions relatives à la taxe sur la valeur ajoutée.<br/>
<br/>
              S'agissant de l'impôt sur les sociétés :<br/>
<br/>
              7. Devant la cour, la société requérante soutenait qu'elle pouvait imputer un déficit constaté au titre des exercices clos en 2002 et 2003 sur l'impôt sur les sociétés dû au titre des exercices clos en 2006 et 2007. C'est sans erreur de droit et sans méconnaître les règles de dévolution de la charge de la preuve que la cour a jugé qu'il appartenait à la société Maysam France d'établir, d'une part, que le déficit dont elle se prévalait était reportable sur les exercices clos en 2004 et 2005 et, d'autre part, qu'à la clôture de ce dernier exercice, un déficit imputable sur les exercices en litige subsistait. Par suite, la société Maysam France n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque en tant qu'il a statué sur les conclusions relatives à l'impôt sur les sociétés.<br/>
<br/>
              8. Il résulte de tout ce qui précède que la société Maysam France est fondée à demander l'annulation de l'arrêt qu'elle attaque en tant seulement qu'il a statué sur les conclusions relatives à la taxe sur la valeur ajoutée.<br/>
<br/>
              9. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 1 000 euros à verser à la société Maysam France au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
                              D E C I D E :<br/>
                              --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 13 mai 2016 est annulé en tant qu'il statue sur les conclusions relatives à la taxe sur la valeur ajoutée.<br/>
<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Paris.<br/>
<br/>
Article 3 : L'Etat versera à la société Maysam France une somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 4 : Le surplus des conclusions du pourvoi est rejeté.<br/>
<br/>
Article 5 : La présente décision sera notifiée à la société à responsabilité limitée Maysam France et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
