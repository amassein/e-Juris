<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034808248</ID>
<ANCIEN_ID>JG_L_2017_05_000000397946</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/80/82/CETATEXT000034808248.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème chambres réunies, 24/05/2017, 397946</TITRE>
<DATE_DEC>2017-05-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>397946</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>Mme Marie Sirinelli</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean Lessi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:397946.20170524</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Nantes d'annuler la décision du 31 juillet 2013 par laquelle le président du conseil général de Maine-et-Loire a confirmé le bien-fondé de l'indu de revenu de solidarité active d'un montant de 8 370 euros qui lui a été réclamé par une décision de la caisse d'allocations familiales du 5 juin 2013, au titre de la période de juin 2011 à mai 2013, et de lui accorder la remise gracieuse de sa dette.<br/>
<br/>
              Par un jugement n° 1307858 du 15 janvier 2016, le tribunal administratif de Nantes a annulé la décision du président du conseil général de Maine-et-Loire du 31 juillet 2013, a enjoint à celui-ci de procéder au calcul et au versement du montant du revenu de solidarité active auquel M. A...avait droit au titre de la période en litige, conformément aux motifs de son jugement, et a rejeté le surplus des conclusions de M.A....<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 15 mars et 14 juin 2016 au secrétariat du contentieux du Conseil d'Etat, le département de Maine-et-Loire demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement du tribunal administratif de Nantes du 15 janvier 2016 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de M.A.... <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code civil ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Sirinelli, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Jean Lessi, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat du département de Maine-et-Loire et à la SCP Didier, Pinet, avocat de M. A....<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 31 juillet 2013, le président du conseil général de Maine-et-Loire a confirmé la décision de la caisse d'allocations familiales de ce département du 5 juin 2013 de récupérer un indu de revenu de solidarité active d'un montant de 8 370 euros à l'encontre de M.A..., au titre de la période de juin 2011 à mai 2013, en raison de la réintégration, dans les ressources à prendre en compte pour le calcul de ce revenu, du montant des pensions en nature que sa mère avait déclaré, auprès de l'administration fiscale, lui avoir versées. Par un jugement du 15 janvier 2016, le tribunal administratif de Nantes, saisi par M.A..., a estimé que les pensions en nature dont le requérant avait bénéficié représentaient le coût de son hébergement au domicile de sa mère et qu'en conséquence, leur montant devait être réintégré à ses ressources sur la base de l'évaluation forfaitaire prévue par l'article R. 262-9 du code de l'action sociale et des familles et non sur la base de leur montant réel. Le tribunal administratif a, par suite, annulé la décision du président du conseil général de Maine-et-Loire du 31 juillet 2013, enjoint à celui-ci de procéder au calcul et au versement du montant du revenu de solidarité active auquel M. A...avait droit au titre de la période en litige, conformément à ces motifs, et rejeté le surplus des conclusions de M.A.... Le département de Maine-et-Loire demande l'annulation de ce jugement en tant qu'il réduit l'indu mis à la charge de M.A..., tandis que, par la voie du pourvoi incident, ce dernier demande son annulation en tant, d'une part, qu'il laisse à sa charge un indu et, d'autre part, qu'il rejette ses conclusions tendant à la remise gracieuse de sa dette.<br/>
<br/>
              Sur le pourvoi du département de Maine-et-Loire :<br/>
<br/>
              2. Aux termes de l'article L. 262-3 du code de l'action sociale et des familles : " (...) L'ensemble des ressources du foyer, y compris celles qui sont mentionnées à l'article L. 132-1, est pris en compte pour le calcul du revenu de solidarité active, dans des conditions fixées par un décret en Conseil d'Etat qui détermine notamment : (...) 2° Les modalités d'évaluation des ressources, y compris les avantages en nature. L'avantage en nature lié à la disposition d'un logement à titre gratuit est déterminé de manière forfaitaire (...) ". Le premier alinéa de l'article R. 262-6 du même code précise que : " Les ressources prises en compte pour la détermination du montant du revenu de solidarité active comprennent, sous les réserves et selon les modalités figurant au présent chapitre, l'ensemble des ressources, de quelque nature qu'elles soient, de toutes les personnes composant le foyer, et notamment les avantages en nature ainsi que les revenus procurés par des biens mobiliers et immobiliers et par des capitaux ". Enfin, l'article R. 262-9 du même code, dans sa rédaction applicable au litige, prévoit que : " Les avantages en nature procurés par un logement occupé soit par son propriétaire ne bénéficiant pas d'aide personnelle au logement, soit, à titre gratuit, par les membres du foyer, sont évalués mensuellement et de manière forfaitaire : / 1° A 12 % du montant forfaitaire mentionné au 2° de l'article L. 262-2 applicable à un foyer composé d'une seule personne ; (...) / Les avantages en nature procurés par un jardin exploité à usage privatif ne sont pas pris en compte ".<br/>
<br/>
              3. Il résulte de ces dispositions que les avantages en nature que reçoivent les bénéficiaires du revenu de solidarité active doivent être intégrés dans les ressources prises en compte pour la détermination du montant de l'allocation à laquelle ils peuvent prétendre, à l'exclusion de l'usage privatif d'un jardin. Si la fourniture d'un logement à titre gratuit doit être évaluée sur la base forfaitaire prévue par l'article R. 262-9 du code de l'action sociale et des familles, les autres avantages en nature doivent, en l'absence de dispositions réglementaires prévoyant un mode d'évaluation forfaitaire, être évalués sur la base de leur valeur réelle. A défaut d'éléments plus précis apportés par le bénéficiaire qui reçoit d'un obligé alimentaire une pension en nature correspondant tant à l'hébergement qu'à d'autres dépenses telles que la nourriture, la valeur retenue pour le calcul des ressources du bénéficiaire doit être celle de la pension déclarée par le débiteur d'aliment auprès de l'administration fiscale, laquelle doit être réputée comprendre la part forfaitaire prévue à l'article R. 262-9 pour la fourniture d'un logement à titre gratuit et, pour le surplus, la valeur réelle des autres avantages en nature.<br/>
<br/>
              4. Par suite, le département de Maine-et-Loire est fondé à soutenir que le tribunal administratif a commis une erreur de droit en évaluant l'intégralité des pensions en nature perçues par M. A...selon la base forfaitaire prévue à l'article R. 262-9 du code de l'action sociale et des familles, au motif que celui-ci était hébergé par sa mère, qui lui fournissait un logement à titre gratuit au sens de ces dispositions, sans rechercher si une part de ces pensions correspondait à des avantages distincts de la seule fourniture d'un logement. <br/>
<br/>
              Sur les conclusions incidentes de M.A... :<br/>
<br/>
              5. Aux termes de l'article L. 262-46 du code de l'action sociale et des familles, dans sa rédaction applicable au litige : " Tout paiement indu de revenu de solidarité active est récupéré par l'organisme chargé du service de celui-ci ainsi que, dans les conditions définies au présent article, par les collectivités débitrices du revenu de solidarité active. / (...) La créance peut être remise ou réduite par le président du conseil général ou l'organisme chargé du service du revenu de solidarité active pour le compte de l'Etat, en cas de bonne foi ou de précarité de la situation du débiteur, sauf si cette créance résulte d'une manoeuvre frauduleuse ou d'une fausse déclaration (...) ". L'article L. 262-47 du même code, dans sa rédaction applicable, prévoit en outre que : " Toute réclamation dirigée contre une décision relative au revenu de solidarité active fait l'objet, préalablement à l'exercice d'un recours contentieux, d'un recours administratif auprès du président du conseil général (...) ".<br/>
<br/>
              6. Le tribunal a rejeté comme irrecevable la demande présentée devant lui par M.A..., qui tendait à la remise gracieuse de sa dette, au motif qu'il ne justifiait pas avoir sollicité auprès du président du conseil général de Maine-et-Loire une telle remise. Il ressort, toutefois, des termes mêmes de la décision du 31 juillet 2013, prise sur sa réclamation du 13 juin précédent, que le président du conseil général de Maine-et-Loire lui a indiqué qu'il n'entendait pas lui " accorder de remise gracieuse, en l'absence, dans sa requête, d'éléments probants justifiant (sa) précarité ".  Par suite, M. A...est fondé à soutenir que, pour juger irrecevables, au motif qu'elles étaient portées directement devant le juge, ses conclusions tendant à la remise gracieuse de l'indu qui lui était réclamé, le tribunal s'est mépris sur la portée de la décision du président du conseil général.<br/>
<br/>
              7. Il résulte de tout ce qui précède, sans qu'il soit besoin d'examiner les autres moyens soulevés par le département de Maine-et-Loire et par M.A..., que le jugement du tribunal administratif de Nantes du 15 janvier 2016 doit être annulé. <br/>
<br/>
              8. Il n'y a lieu, dans les circonstances de l'espèce, de faire droit ni aux conclusions du département de Maine-et-Loire présentées au titre de l'article L. 761-1 du code de justice administrative ni aux conclusions de M. A...présentées au titre des dispositions de cet article et de l'article 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Nantes du 15 janvier 2016 est annulé.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Nantes.<br/>
Article 3 : Les conclusions du département de Maine-et-Loire présentées au titre de l'article L. 761-1 du code de justice administrative et celles de M. A...présentées au titre des dispositions de cet article et de l'article 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au département de Maine-et-Loire et à M. B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-02-06 AIDE SOCIALE. DIFFÉRENTES FORMES D'AIDE SOCIALE. REVENU MINIMUM D'INSERTION (RMI). - RSA - RESSOURCES PRISES EN COMPTE POUR LE CALCUL DE L'ALLOCATION - 1) AVANTAGES EN NATURE - MODALITÉS [RJ1] - 2) CAS D'UN OBLIGÉ ALIMENTAIRE RECEVANT UNE PENSION CORRESPONDANT TANT À L'HÉBERGEMENT QU'À D'AUTRES DÉPENSES.
</SCT>
<ANA ID="9A"> 04-02-06 1) Il résulte des termes mêmes des articles L. 262-3, R. 262-6 et R. 262-9 du code de l'action sociale et des familles que les avantages en nature que reçoivent les bénéficiaires du revenu de solidarité active (RSA) doivent être intégrés dans les ressources prises en compte pour la détermination du montant de l'allocation à laquelle ils peuvent prétendre, à l'exclusion de l'usage privatif d'un jardin. Si la fourniture d'un logement à titre gratuit doit être évaluée sur la base forfaitaire prévue par l'article R. 262-9 du code, les autres avantages en nature, telle une pension alimentaire en nature, doivent, en l'absence de dispositions réglementaires prévoyant un mode d'évaluation forfaitaire, être en principe évalués sur la base de leur valeur réelle.,,,2) A défaut d'éléments plus précis apportés par le bénéficiaire qui reçoit d'un obligé alimentaire une pension en nature correspondant tant à l'hébergement qu'à d'autres dépenses telles que la nourriture, la valeur retenue pour le calcul des ressources du bénéficiaire doit être celle de la pension déclarée par le débiteur d'aliment auprès de l'administration fiscale, laquelle doit être réputée comprendre la part forfaitaire prévue à l'article R. 262-9 pour la fourniture d'un logement à titre gratuit et, pour le surplus, la valeur réelle des autres avantages en nature.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 3 octobre 2016, M. Roudier, n° 391211, T. p. 639.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
