<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043799695</ID>
<ANCIEN_ID>JG_L_2021_07_000000427551</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/79/96/CETATEXT000043799695.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 09/07/2021, 427551, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>427551</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>Mme Ophélie Champeaux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:427551.20210709</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une décision du 22 mars 2021, le Conseil d'Etat, statuant au contentieux a prononcé l'admission des conclusions du pourvoi de la société anonyme (SA) VOA Verrerie d'Albi dirigées contre l'article 3 de l'arrêt nos 16BX01944, 18BX01607 du 30 novembre 2018 de la cour administrative d'appel de Bordeaux, en tant qu'il s'est prononcé sur l'inclusion dans les bases d'imposition de cotisation foncière des entreprises de la valeur locative des cuves de stockage, du réseau informatique, du système de sécurité-incendie, du système de traitement de l'air et des murs anti-bruit.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Ophélie Champeaux, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Célice, Texidor, Perier, avocat de la société VOA Verrerie d'Albi ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société VOA Verrerie d'Albi a fait l'objet d'une vérification de comptabilité à l'issue de laquelle l'administration fiscale l'a assujettie à des cotisations supplémentaires de cotisation foncière des entreprises au titre des années 2010 à 2015 dans les rôles de la commune d'Albi (Tarn). Par deux jugements nos 1303353, 1303472 du 12 mai 2016 et nos 1505658, 1603034 du 5 mars 2018, le tribunal administratif de Toulouse lui a accordé une décharge partielle, à concurrence de l'exclusion des bases d'imposition de la valeur locative de certaines immobilisations, et a rejeté le surplus des conclusions de sa demande en décharge de ces impositions. Par un arrêt nos 16BX01944, 18BX01607 du 30 novembre 2018, la cour administrative d'appel, après avoir prononcé une nouvelle décharge partielle des impositions en litige et réformé en conséquence le jugement, a rejeté le surplus des conclusions de la requête d'appel formée par la société. Par une décision du 22 mars 2021, le Conseil d'Etat, statuant au contentieux a admis le pourvoi formé par la société contre l'article 3 de cet arrêt, en tant qu'il s'est prononcé sur l'inclusion dans les bases d'imposition de cotisation foncière des entreprises de la valeur locative des cuves de stockage, du réseau informatique, du système de sécurité-incendie, du système de traitement de l'air et des murs anti-bruit.<br/>
<br/>
              2. En premier lieu, la cour administrative d'appel, après avoir jugé que la société n'était pas fondée à soutenir que les cuves de stockage dont elle dispose pour son activité devaient être exonérées de cotisation foncière des entreprises sur le fondement de la loi fiscale, a écarté les prétentions de la société tendant au bénéfice de l'interprétation donnée de cette loi par la doctrine administrative 6 C-112, paragraphe 6, du 15 décembre 1998 reprise depuis le 12 septembre 2012 au paragraphe 30 du BOI-IF-TFB-10-10-20 qui admettent, " à titre de règle pratique ", que " sont imposables toutes les installations de stockage, à l'exception : - des installations d'une capacité inférieure à 100 m3 ; / - des installations d'une capacité égale ou supérieure à 100 m3 lorsqu'elles peuvent être déplacées par des moyens normaux ". En jugeant que la société n'était pas fondée à se prévaloir de cette interprétation au seul motif que les cuves en litige n'étaient pas facilement déplaçables, sans répondre à l'argumentation soulevée devant elle, tirée de ce que leur capacité de stockage n'excédait pas le volume de 100 mètres cubes, la cour a insuffisamment motivé son arrêt.<br/>
<br/>
              3. En second lieu, l'article 1380 du code général des impôts dispose : " La taxe foncière est établie annuellement sur les propriétés bâties sises en France à l'exception de celles qui en sont expressément exonérées par les dispositions du présent code ". Selon l'article 1381 du même code : " Sont également soumis à la taxe foncière sur les propriétés bâties : / 1° Les installations destinées à abriter des personnes ou des biens ou à stocker des produits ainsi que les ouvrages en maçonnerie présentant le caractère de véritables constructions tels que, notamment, les cheminées d'usine, les réfrigérants atmosphériques, les formes de radoub, les ouvrages servant de support aux moyens matériels d'exploitation ; / 2° Les ouvrages d'art et les voies de communication (...) ". Selon l'article 1382 du code général des impôts, dans sa rédaction applicable aux impositions en litige : " Sont exonérés de la taxe foncière sur les propriétés bâties : / (...) / 11° Les outillages et autres installations et moyens matériels d'exploitation des établissements industriels à l'exclusion de ceux visés à l'article 1381 1° et 2° ". Aux termes du premier alinéa de l'article 1495 de ce code : " Chaque propriété ou fraction de propriété est appréciée d'après sa consistance, son affectation, sa situation et son état, à la date de l'évaluation ". Aux termes du II de l'article 324 B de l'annexe III au même code : " Pour l'appréciation de la consistance il est tenu compte de tous les travaux équipements ou éléments d'équipement existant au jour de l'évaluation ".<br/>
<br/>
              4. Pour apprécier, en application de l'article 1495 du code général des impôts et de l'article 324 B de son annexe III, la consistance des propriétés qui entrent, en vertu de ses articles 1380 et 1381, dans le champ de la taxe foncière sur les propriétés bâties, il est tenu compte, non seulement de tous les éléments d'assiette mentionnés par ces deux derniers articles mais également des biens faisant corps avec eux. Sont toutefois exonérés de cette taxe, en application du 11° de l'article 1382 du même code, ceux de ces biens qui font partie des outillages, autres installations et moyens matériels d'exploitation d'un établissement industriel, c'est-à-dire ceux de ces biens qui relèvent d'un établissement qualifié d'industriel au sens de l'article 1499, qui sont spécifiquement adaptés aux activités susceptibles d'être exercées dans un tel établissement et qui ne sont pas au nombre des éléments mentionnés aux 1° et 2° de l'article 1381.<br/>
<br/>
              5. Aux termes de l'article 1467 du code général des impôts, dans sa rédaction applicable aux impositions en litige : " La cotisation foncière des entreprises a pour base la valeur locative des biens passibles d'une taxe foncière situés en France, à l'exclusion des biens exonérés de taxe foncière sur les propriétés bâties en vertu des 11° et 12° de l'article 1382, dont le redevable a disposé pour les besoins de son activité professionnelle pendant la période de référence définie aux articles 1467 A et 1478, à l'exception de ceux qui ont été détruits ou cédés au cours de la même période. (...) ".<br/>
<br/>
              6. Il résulte de ce qui précède qu'en jugeant que le réseau informatique, le système de sécurité-incendie, le système de traitement de l'air et les murs anti-bruit ne pouvaient être exonérés de taxe foncière sur les propriétés bâties et que leur valeur locative devait par suite être incluse dans l'assiette de la cotisation foncière des entreprises due par la société requérante, au seul motif que ces immobilisations étaient indissociables des bâtiments pour lesquels elles étaient conçues et auxquels elles s'incorporaient, la cour a commis une erreur de droit. <br/>
<br/>
              7. Dès lors, la société VOA Verrerie d'Albi est fondée, sans qu'il soit besoin de se prononcer sur les autres moyens de son pourvoi, à demander l'annulation de l'article 3 de l'arrêt qu'elle attaque, en tant qu'il s'est prononcé sur l'inclusion dans les bases d'imposition de cotisation foncière des entreprises de la valeur locative des cuves de stockage, du réseau informatique, du système de sécurité-incendie, du système de traitement de l'air et des murs anti-bruit.<br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3 000 euros à verser à la société VOA Verrerie d'Albi au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 3 de l'arrêt de la cour administrative d'appel de Bordeaux du 30 novembre 2018 est annulé en tant qu'il s'est prononcé sur l'inclusion dans les bases d'imposition de cotisation foncière des entreprises de la valeur locative des cuves de stockage, du réseau informatique, du système de sécurité-incendie, du système de traitement de l'air et des murs anti-bruit.<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Bordeaux. <br/>
Article 3 : L'Etat versera à la société VOA Verrerie d'Albi une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la société anonyme VOA Verrerie d'Albi et au ministre de l'économie, des finances et de la relance.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
