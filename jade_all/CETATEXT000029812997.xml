<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029812997</ID>
<ANCIEN_ID>JG_L_2014_11_000000382601</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/81/29/CETATEXT000029812997.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  3ème sous-section jugeant seule, 28/11/2014, 382601, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-11-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>382601</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 3ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>Mme Angélique Delorme</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:382601.20141128</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Par une requête sommaire et un mémoire complémentaire, enregistrés les 15 juillet et 7 août 2014 au secrétariat du contentieux du Conseil d'Etat, la communauté de communes Porte de Maurienne demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2014-272 du 27 février 2014 portant délimitation des cantons dans le département de la Savoie ainsi que la décision du 15 mai 2014 par laquelle le ministre de l'intérieur a rejeté son recours gracieux dirigé contre ce décret ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - le code électoral ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - le décret n° 2013-938 du 18 octobre 2013 ; <br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Angélique Delorme, auditeur,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 191-1 du code électoral, résultant de la loi du 17 mai 2013 relative à l'élection des conseillers départementaux, des conseillers municipaux et des conseillers communautaires et modifiant le calendrier électoral : " Le nombre de cantons dans lesquels sont élus les conseillers départementaux est égal, pour chaque département, à la moitié du nombre de cantons existant au 1er janvier 2013, arrondi à l'unité impaire supérieure si ce nombre n'est pas entier impair. / Le nombre de cantons dans chaque département comptant plus de 500 000 habitants ne peut être inférieur à dix-sept. Il ne peut être inférieur à treize dans chaque département comptant entre 150 000 et 500 000 habitants ". Aux termes de l'article L. 3113-2 du code général des collectivités territoriales, dans sa rédaction issue de la même loi, applicable à la date du décret attaqué : " I. - Les modifications des limites territoriales des cantons, les créations et suppressions de cantons et le transfert du siège de leur chef-lieu sont décidés par décret en Conseil d'Etat après consultation du conseil général qui se prononce dans un délai de six semaines à compter de sa saisine. A l'expiration de ce délai, son avis est réputé rendu. (...)/ III. - La modification des limites territoriales des cantons effectuée en application du I est conforme aux règles suivantes : / a) Le territoire de chaque canton est défini sur des bases essentiellement démographiques ; / b) Le territoire de chaque canton est continu ; c) Est entièrement comprise dans le même canton toute commune de moins de 3500 habitants ; / IV. - Il n'est apporté aux règles énoncées au III que des exceptions de portée limitée spécialement justifiées, au cas par cas, par des considérations géographiques (...) ou par d'autres impératifs d'intérêt général ". <br/>
<br/>
              2. Compte tenu de la réduction du nombre des cantons résultant de l'application de l'article L. 191-1 du code électoral, le décret attaqué a, sur le fondement de l'article L. 3113-2 du code général des collectivités territoriales, procédé à une nouvelle délimitation du département de la Savoie. <br/>
<br/>
<br/>
              3. En premier lieu, il ressort des pièces du dossier que, contrairement aux allégations de la requérante, le décret attaqué a été pris le Conseil d'Etat entendu. Il ressort de la copie de la minute de la délibération de la section de l'intérieur du Conseil d'Etat, produite le 10 octobre 2014 par le ministre de l'intérieur, que le texte publié ne contient pas de disposition qui diffèrerait à la fois du texte soumis au Conseil d'Etat et du texte adopté par lui. Le moyen tiré de ce que le décret attaqué aurait été pris sans consultation régulière du Conseil d'Etat ne peut, par suite, qu'être écarté.<br/>
<br/>
              4. En deuxième lieu, la communauté de communes requérante fait valoir que les nouveaux cantons de Modane et d'Ugine délimités par le décret attaqué ont une population sensiblement inférieure à la moyenne départementale et que la délimitation de ces cantons s'écarterait ainsi de la règle posée au III de l'article L. 3113-2 du code général des collectivités territoriales cité au point 1 selon laquelle le territoire de chaque canton est défini sur des bases essentiellement démographiques. Toutefois le IV du même article autorise des exceptions de portée limitée à cette règle dès lors qu'elles sont spécialement justifiées par des considérations géographiques ou d'autres impératifs d'intérêt général. <br/>
<br/>
              5. D'une part, il ressort des pièces du dossier que la population du canton d'Ugine (17 651 habitants) est inférieure de 19,18% à la moyenne départementale (21 840 habitants). Eu égard à la situation de ce territoire, entièrement en zone de montagne, et à sa faible densité démographique, sa délimitation ne peut être regardée comme procédant d'une erreur manifeste d'appréciation. <br/>
<br/>
              6. D'autre part, s'agissant du canton de Modane, dont il ressort des pièces du dossier que la population (15 024 habitants) est inférieure de 31,21% à la moyenne départementale, le Gouvernement fait valoir qu'il regroupe, l'ensemble des communes de la Haute Maurienne, soit trois anciens cantons faiblement peuplés, sur un territoire très étendu et totalement enclavé sauf vers l'aval de la vallée où il jouxte le canton et la commune de Saint-Jean-de-Maurienne. Le Gouvernement ajoute que le rattachement de cette commune au canton de Modane aurait eu pour effet de reporter sur le canton aval le déséquilibre démographique qui affecte le canton amont. Ces considérations géographiques relatives à l'enclavement d'une vallée de haute montagne à faible densité démographique peuvent être regardées comme des justifications suffisantes à l'exception limitée apportée à la règle selon laquelle le territoire de chaque canton est défini sur des bases essentiellement démographiques. Le moyen tiré de ce que cette délimitation méconnaît les dispositions de l'article L. 3113-2 du code général des collectivités territoriales et le principe d'égalité des citoyens devant le suffrage qu'elles mettent en oeuvre doit donc être écarté.<br/>
<br/>
              7. En troisième lieu, ni les dispositions de ce même article L. 3113-2 du code général des collectivités territoriales, ni aucun autre texte non plus qu'aucun principe n'imposent au Premier ministre de prévoir que les limites des cantons, qui sont des circonscriptions électorales, coïncident avec les périmètres des arrondissements et des établissements publics de coopération intercommunale. Si la communauté de communes requérante fait valoir qu'en délimitant le canton de Saint-Pierre-d'Albigny et en supprimant l'ancien canton d'Aiguebelle, l'auteur du décret n'aurait pas suffisamment tenu compte de l'organisation administrative et des services publics dans la vallée de la Maurienne ni des intérêts économiques et sociaux, ces circonstances ne sont pas de nature à établir que le décret attaqué aurait été pris en méconnaissance de la règle posée par article L. 3113-2 mentionné ci-dessus, selon laquelle le territoire de chaque canton est défini sur des bases essentiellement démographiques ni que ce décret serait entaché d'erreur manifeste d'appréciation. <br/>
<br/>
              8. Enfin, la modification des limites territoriales des cantons doit être réalisée selon les règles prévues aux III et IV de l'article L. 3113-2 du code général des collectivités territoriales. Par suite, la communauté de communes Porte de Maurienne ne peut utilement invoquer la circonstance que les zones rurales seraient, du fait de la mise en oeuvre de ces règles, qui n'est pas contestée, moins bien représentées que les zones urbaines au sein de l'assemblée départementale. <br/>
<br/>
              9. Il résulte de tout ce qui précède que la communauté de communes Porte de Maurienne n'est pas fondée à demander l'annulation pour excès de pouvoir des décisions qu'elle attaque. <br/>
<br/>
              10. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>
<br/>DECIDE:<br/>
Article 1er : La requête de la communauté de communes Porte de Maurienne est rejetée. <br/>
Article 2 : La présente décision sera notifiée à la communauté de communes Porte de Maurienne, au ministre de l'intérieur et au secrétariat général du Gouvernement.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
