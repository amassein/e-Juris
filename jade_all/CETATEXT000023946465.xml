<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000023946465</ID>
<ANCIEN_ID>JG_L_2011_04_000000342329</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/23/94/64/CETATEXT000023946465.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 27/04/2011, 342329</TITRE>
<DATE_DEC>2011-04-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>342329</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Alain  Boulanger</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Landais Claire</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2011:342329.20110427</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 9 et 24 août 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SARL LB PRESTATIONS DE SERVICES, dont le siège est 127, boulevard de Ménilmontant à Paris (75011), représentée par son gérant en exercice et pour la SARL FLASH BACK, dont le siège est 78, avenue Gabriel Péri à Gennevilliers (92230), représentée par son gérant en exercice ; la SARL LB PRESTATIONS DE SERVICES et la SARL FLASH BACK demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'ordonnance n° 1005499 du 20 juillet 2010 par laquelle le juge des référés du tribunal administratif de Cergy-Pontoise, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, a rejeté leur demande tendant à la suspension de l'exécution de la décision de préemption du 2 juillet 2010 prise par le maire de la commune de Gennevilliers ; <br/>
<br/>
              2°) statuant en référé, de faire droit à leur demande de suspension ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Gennevilliers le versement de la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu le code de l'urbanisme, notamment son article L. 214-1 ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alain Boulanger, chargé des fonctions de Maître des requêtes,  <br/>
<br/>
              - les observations de la SCP Didier, Pinet, avocat de la SARL LB PRESTATIONS DE SERVICES et de la SARL FLASH BACK, et de la SCP Lyon-Caen, Thiriez, avocat de la commune de Gennevilliers ; <br/>
<br/>
              - les conclusions de Mme Claire Landais, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Didier, Pinet, avocat de la SARL LB PRESTATIONS DE SERVICES et de la SARL FLASH BACK et à la SCP Lyon-Caen, Thiriez, avocat de la commune de Gennevilliers ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant, d'une part, qu'il ne ressort pas des pièces du dossier que la SARL FLASH BACK a reçu notification de l'ordonnance attaquée, d'autre part, qu'il ressort des pièces du dossier que la SARL LB PRESTATIONS DE SERVICES a reçu notification de l'ordonnance attaquée, rendue en application de l'article L. 521-1 du code de justice administrative, le 26 juillet 2010 ; que leur pourvoi contre cette ordonnance a été présenté par télécopie enregistrée au secrétariat du contentieux du Conseil d'Etat le 9 août 2010, soit dans le délai de quinze jours prévu par les dispositions de l'article R. 523-1 du code de justice administrative, et régularisé par la production d'une requête signée ; qu'il suit de là que la fin de non recevoir opposée par la commune de Gennevilliers ne peut qu'être écartée ;<br/>
<br/>
              Considérant qu'aux termes de l'article L. 214-1 du code de l'urbanisme : " Le conseil municipal peut, par délibération motivée, délimiter un périmètre de sauvegarde du commerce et de l'artisanat de proximité, à l'intérieur duquel sont soumises au droit de préemption institué par le présent chapitre les cessions de fonds artisanaux, de fonds de commerce ou de baux commerciaux. / (...) Chaque cession est subordonnée, à peine de nullité, à une déclaration préalable faite par le cédant à la commune. Cette déclaration précise le prix et les conditions de la cession. / (...) Le silence de la commune pendant le délai de deux mois à compter de la réception de cette déclaration vaut renonciation à l'exercice du droit de préemption. Le cédant peut alors réaliser la vente aux prix et conditions figurant dans sa déclaration " ; <br/>
<br/>
              Considérant qu'il ressort des énonciations de l'ordonnance attaquée que la SARL FLASH BACK, qui avait informé la commune de Gennevilliers de son intention de céder à la SARL LB PRESTATIONS DE SERVICES le droit au bail commercial dont elle était titulaire, et qui s'était vu notifier le 1er juin 2010 une décision par laquelle la commune renonçait à exercer le droit de préemption qu'elle détenait sur ce bail commercial en vertu des dispositions citées ci-dessus de l'article L. 214-1 du code de l'urbanisme, a alors cédé ce bail à la SARL LB PRESTATIONS DE SERVICES avant que, par une seconde décision du 2 juillet 2010 prise dans ce même délai de deux mois, la commune de Gennevilliers ne décide d'exercer son droit de préemption sur le bail commercial en cause ; qu'en estimant que la SARL LB PRESTATIONS DE SERVICES et la SARL FLASH BACK ne pouvaient utilement se prévaloir de la passation de l'acte de cession pour justifier l'urgence de leur demande de suspension de la seconde décision du 2 juillet 2010, au motif que cet acte de cession était intervenu " de leur seul fait " avant l'expiration du délai de préemption de deux mois, le juge des référés du tribunal administratif de Cergy-Pontoise a commis une erreur de droit ; que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son ordonnance doit être annulée ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au titre de la procédure de référé engagée, en application de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ;<br/>
<br/>
              Considérant, qu'eu égard à l'objet d'une décision de préemption, et à ses effets vis-à-vis de l'acquéreur évincé, la condition d'urgence doit en principe être constatée lorsque celui-ci en demande la suspension ; que cette présomption n'a en revanche plus lieu de s'appliquer lorsque l'acquéreur est entré en possession du bien ou du droit objet de la décision de préemption litigieuse ;<br/>
<br/>
              Considérant que pour caractériser l'urgence dont elles se prévalent, les sociétés requérantes invoquent l'impossibilité pour la SARL LB PRESTATIONS DE SERVICES, désormais titulaire du droit au bail, d'exercer son commerce, en raison du risque d'expulsion à tout moment des locaux objet de la décision de préemption en litige ; que, toutefois, la cession ayant eu lieu, cette décision de préemption ne porte par elle-même aucune atteinte immédiate au droit de jouissance du bail commercial ; qu'au  demeurant, le juge judiciaire est seul compétent pour prononcer la nullité de l'acte de cession du droit au bail ; que, par ailleurs, les sociétés requérantes ne produisent pas d'élément de nature à établir que la décision de préemption aurait par elle-même des effets graves et immédiats sur la situation économique de l'une ou l'autre d'entre elles ; que, par suite, la condition d'urgence n'est pas satisfaite ;<br/>
<br/>
              Considérant que l'une des conditions posées par l'article L. 521-1 du code de justice administrative n'étant pas remplie, la demande présentée par la SARL LB PRESTATIONS DE SERVICES et la SARL FLASH BACK devant le juge des référés du tribunal administratif de Cergy-Pontoise tendant à ce que soit ordonnée la suspension de l'exécution de la décision de préemption du 2 juillet 2010 prise par le maire de la commune de Gennevilliers doit être rejetée ;<br/>
<br/>
              Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la commune en application des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font obstacle à ce que soit mis à la charge de la commune qui n'est pas, dans la présente instance, la partie perdante, le versement d'une somme au titre des frais exposés par la SARL LB PRESTATIONS DE SERVICES et la SARL FLASH BACK et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Cergy-Pontoise du 20 juillet 2010 est annulée.<br/>
Article 2 : La demande présentée par la SARL LB PRESTATIONS DE SERVICES et la SARL FLASH BACK devant le juge des référés du tribunal administratif de Cergy-Pontoise ainsi que le surplus des conclusions de leur pourvoi sont rejetés.<br/>
Article 3 : Les conclusions de la commune de Gennevilliers tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la SARL LB PRESTATIONS DE SERVICES, à la SARL FLASH BACK et à la commune de Gennevilliers.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-03-01-04-01 PROCÉDURE. PROCÉDURES D'URGENCE. RÉFÉRÉ TENDANT AU PRONONCÉ D'UNE MESURE URGENTE. CONDITIONS. URGENCE. - PRÉEMPTION PAR LA COMMUNE D'UN BAIL COMMERCIAL ALORS QUE LA CESSION DU BAIL A EU LIEU - ACQUÉREUR DEMANDANT LA SUSPENSION DE L'EXÉCUTION DE LA DÉCISION DE PRÉEMPTION - PRÉSOMPTION D'URGENCE - ABSENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-02-01-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PROCÉDURES D'INTERVENTION FONCIÈRE. PRÉEMPTION ET RÉSERVES FONCIÈRES. DROITS DE PRÉEMPTION. - EXERCICE DU DROIT DE PRÉEMPTION LORSQUE LA CESSION DU BIEN EN CAUSE A EU LIEU - RÉFÉRÉ-SUSPENSION (ART. L. 521-2 DU CJA) - PRÉSOMPTION D'URGENCE - ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 54-03-01-04-01 Si, eu égard à l'objet d'une décision de préemption et à ses effets vis-à-vis de l'acquéreur évincé, la condition d'urgence doit en principe être constatée lorsque celui-ci en demande la suspension, cette présomption n'a  plus lieu de s'appliquer lorsque l'acquéreur est entré en possession du bien ou du droit objet de la décision de préemption litigieuse.</ANA>
<ANA ID="9B"> 68-02-01-01 Si, eu égard à l'objet d'une décision de préemption et à ses effets vis-à-vis de l'acquéreur évincé, la condition d'urgence posée à l'article L. 521-2 du code de justice administrative (CJA) doit en principe être constatée lorsque celui-ci en demande la suspension, cette présomption n'a plus lieu de s'appliquer lorsque l'acquéreur est entré en possession du bien ou du droit objet de la décision de préemption litigieuse.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp., lorsque la préemption intervient avant la cession du bail, CE, 13 novembre 2002, Hourdin, n° 248851, p. 396.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
