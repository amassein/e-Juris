<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027656236</ID>
<ANCIEN_ID>JG_L_2013_07_000000342291</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/65/62/CETATEXT000027656236.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 5ème sous-sections réunies, 03/07/2013, 342291, Publié au recueil Lebon</TITRE>
<DATE_DEC>2013-07-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>342291</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 5ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL ; SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>M. Régis Fraisse</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:342291.20130703</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 9 août  et 9 novembre 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B... A..., demeurant... ; M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09VE01362 du 20 mai 2010 par lequel la cour administrative d'appel de Versailles a rejeté sa requête tendant à l'annulation, d'une part, du jugement n° 0301846 du 6 juin 2005 par lequel le tribunal administratif de Versailles a rejeté sa demande tendant à l'annulation de la décision du 26 mars 2003 de l'inspectrice du travail de la direction départementale du travail, de l'emploi et de la formation professionnelle de l'Essonne autorisant son licenciement pour motif économique et, d'autre part, de cette décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat et de la société Exel Textile la somme de 4 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code du travail ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Régis Fraisse, Conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de M. A...et à la SCP Baraduc, Duhamel, avocat de la société Excel France ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'en vertu des dispositions des articles L. 412-18 et L. 436-1 du code du travail alors en vigueur, devenus les articles L. 2411-3 et L. 2411-8, le licenciement des salariés légalement investis des fonctions de délégué syndical et de représentant syndical au comité d'entreprise, qui bénéficient d'une protection exceptionnelle dans l'intérêt de l'ensemble des travailleurs qu'ils représentent, ne peut intervenir que sur autorisation de l'inspecteur du travail ; que, lorsque le licenciement d'un de ces salariés est envisagé, ce licenciement ne doit pas être en rapport avec les fonctions représentatives normalement exercées ou l'appartenance syndicale de l'intéressé ; que, dans le cas où la demande de licenciement est fondée sur un motif de caractère économique, il appartient à l'inspecteur du travail et, le cas échéant, au ministre, de rechercher, sous le contrôle du juge de l'excès de pouvoir, si la situation de l'entreprise justifie le licenciement du salarié, en tenant compte notamment de la nécessité des réductions envisagées d'effectifs et de la possibilité d'assurer le reclassement du salarié dans l'entreprise ou au sein du groupe auquel appartient cette dernière conformément aux dispositions de l'article L. 321-1 du code du travail alors en vigueur, devenu l'article L. 1233-4 ; qu'en outre, pour refuser l'autorisation sollicitée, l'autorité administrative a la faculté de retenir des motifs d'intérêt général relevant de son pouvoir d'appréciation de l'opportunité, sous réserve qu'une atteinte excessive ne soit pas portée à l'un ou l'autre des intérêts en présence ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 26 mars 2003, l'inspectrice du travail de la deuxième section de la direction départementale du travail, de l'emploi et de la formation professionnelle de l'Essonne, saisie d'une demande en ce sens par la société Exel Textile en raison d'une cessation totale d'activité, a autorisé le licenciement pour motif économique de M.A..., qui exerçait les fonctions de préparateur de commandes, en précisant qu'il détenait des mandats de délégué syndical et de représentant syndical au comité d'entreprise ; que, par jugement du 6 juin 2005, le tribunal administratif de Versailles a rejeté la demande de M. A...tendant à l'annulation de la décision de l'inspectrice du travail ; que, par un arrêt du 13 juillet 2007, la cour administrative d'appel de Versailles a confirmé ce jugement ; que, par décision n° 309195 du 20 mars 2009, le Conseil d'Etat statuant au contentieux a annulé cet arrêt pour erreur de droit et renvoyé le jugement de l'affaire à la même cour ; que, par l'arrêt attaqué du 20 mai 2010, cette dernière a de nouveau rejeté l'appel de M.A... ;<br/>
<br/>
              3. Considérant, en premier lieu, que les juges d'appel ont relevé que, dans sa décision du 26 mars 2003, l'inspectrice du travail mentionnait que la société avait satisfait à son obligation de reclassement en proposant à l'intéressé des postes de préparateur de commandes, de cariste, d'agent de manutention dans le groupe Exel ; qu'ils ont pu en déduire, par une appréciation souveraine, que cette décision était suffisamment motivée au sens des dispositions de l'article R. 436-4 du code du travail, alors même qu'elle ne précisait pas le lieu d'exercice et les conditions financières de ces propositions ; qu'en statuant ainsi, ils n'ont pas commis d'erreur de droit ;<br/>
<br/>
              4. Considérant, en deuxième lieu, qu'après avoir constaté que le mandat de délégué du personnel détenu par M. A...ayant expiré le 15 septembre 2002 avait été " prorogé ", après son terme, par l'accord conclu le 31 décembre 2002 entre la société Exel Textile, la CFDT et la CFE-CGC, la cour en a déduit que cet accord était resté sans effet sur la compétence de l'inspectrice du travail et qu'à la date à laquelle celle-ci s'est prononcée, plus de six mois après l'expiration du mandat de délégué du personnel précédemment détenu par M. A..., celui-ci ne relevait plus, au titre de ce mandat, de la protection exceptionnelle instituée par l'article L. 425-1 du code du travail alors en vigueur ; qu'en statuant ainsi, la cour, dont l'arrêt est suffisamment motivé, n'a pas commis d'erreur de droit ;<br/>
<br/>
              5. Considérant, en dernier lieu, qu'en relevant que la société Exel Textile se trouvait en cessation totale d'activité et en jugeant qu'elle n'était pas tenue, dans le cadre de son obligation de rechercher des possibilités de reclassement du salarié, de proposer à ce dernier des offres lui permettant de poursuivre ses fonctions représentatives, la cour administrative d'appel n'a pas non plus commis d'erreur de droit ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que M. A...n'est pas fondé à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société Exel France, venant aux droits de la société Exel Textile, qui n'est pas, dans la présente instance, la partie perdante ; qu'il n'y a pas lieu non plus, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la société Exel France au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
Article 1er : Le pourvoi de M. A...est rejeté.<br/>
Article 2 : Les conclusions de la société Exel France présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. B... A..., au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social et à la société Exel France.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-03 COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE LES DEUX ORDRES DE JURIDICTION. - ACCORD COLLECTIF PROROGEANT APRÈS SON TERME UN MANDAT DE DÉLÉGUÉ DU PERSONNEL - INCIDENCE SUR LA PROTECTION DU SALARIÉ AU TITRE DE L'ARTICLE L. 425-1 DU CODE DU TRAVAIL - 1) ABSENCE - CONSÉQUENCE - INCOMPÉTENCE DE L'INSPECTEUR DU TRAVAIL POUR CONNAÎTRE D'UNE AUTORISATION DE LICENCIEMENT - 2) NATURE DE LA QUESTION AINSI TRANCHÉE - QUESTION DE VALIDITÉ DE L'ACCORD - ABSENCE - CONSÉQUENCE - COMPÉTENCE DU JUGE ADMINISTRATIF POUR EN JUGER (SOL. IMPL.) .
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">66-01-01-02 TRAVAIL ET EMPLOI. INSTITUTIONS DU TRAVAIL. ADMINISTRATION DU TRAVAIL. INSPECTION DU TRAVAIL. - ACCORD COLLECTIF PROROGEANT APRÈS SON TERME UN MANDAT DE DÉLÉGUÉ DU PERSONNEL - INCIDENCE SUR LA PROTECTION DU SALARIÉ AU TITRE DE L'ARTICLE L. 425-1 DU CODE DU TRAVAIL - 1) ABSENCE - CONSÉQUENCE - INCOMPÉTENCE DE L'INSPECTEUR DU TRAVAIL POUR CONNAÎTRE D'UNE AUTORISATION DE LICENCIEMENT - 2) NATURE DE LA QUESTION AINSI TRANCHÉE - QUESTION DE VALIDITÉ DE L'ACCORD - ABSENCE - CONSÉQUENCE - COMPÉTENCE DU JUGE ADMINISTRATIF (SOL. IMPL.).
</SCT>
<ANA ID="9A"> 17-03 1) Un accord collectif prorogeant après son terme un mandat de délégué du personnel est sans incidence sur la circonstance que ce salarié, dont le mandat a expiré, ne relève plus, au titre de ce mandat, de la protection exceptionnelle instituée par l'article L. 425-1 du code du travail.... ,,2) L'absence d'effet de l'accord collectif sur la compétence de l'inspecteur du travail n'est pas une question de validité de l'accord. Par suite, compétence du juge administratif pour en juger.</ANA>
<ANA ID="9B"> 66-01-01-02 1) Un accord collectif prorogeant après son terme un mandat de délégué du personnel est sans incidence sur la circonstance que ce salarié, dont le mandat a expiré, ne relève plus, au titre de ce mandat, de la protection exceptionnelle instituée par l'article L. 425-1 du code du travail.... ,,2) L'absence d'effet de l'accord collectif sur la compétence de l'inspecteur du travail n'est pas une question de validité de l'accord. Par suite, compétence du juge administratif pour en juger.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
