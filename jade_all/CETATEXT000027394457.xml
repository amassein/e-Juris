<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027394457</ID>
<ANCIEN_ID>JG_L_2013_05_000000367569</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/39/44/CETATEXT000027394457.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 03/05/2013, 367569, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-05-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>367569</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUIN-PALAT, BOUCARD</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2013:367569.20130503</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête, enregistrée le 10 avril 2013 au secrétariat du contentieux du Conseil d'Etat, présentée pour Mme A...B..., demeurant... ; Mme B...demande au juge des référés du Conseil d'Etat :<br/>
<br/>
              1°) d'ordonner, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution du décret du 27 mars 2013, publié au Journal officiel de la République française du 29 mars 2013, par lequel le Président de la République l'a déchargée des fonctions de l'instruction ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              elle soutient que :<br/>
              - la condition d'urgence est remplie dès lors que l'exécution du décret contesté préjudicie de manière grave et immédiate à l'intérêt d'une bonne administration de la justice et qu'elle préjudicie de manière suffisamment grave au principe de l'indépendance de l'autorité judiciaire et de l'inamovibilité des magistrats du siège ; <br/>
              - il existe un doute sérieux quant à la légalité du décret litigieux, qui n'est pas motivé, qui a le caractère d'une sanction déguisée et qui méconnaît les dispositions de l'article 28-3 de l'ordonnance n° 58-1270 du 22 décembre 1958 portant loi organique relative au statut de la magistrature ;<br/>
<br/>
<br/>
              Vu le décret dont la suspension de l'exécution est demandée ;<br/>
<br/>
              Vu la copie de la requête à fin d'annulation de ce décret ; <br/>
<br/>
              Vu le mémoire en défense, enregistré le 25 avril 2013, présenté par la garde des sceaux, ministre de la justice ; elle soutient que :<br/>
              - la condition d'urgence n'est pas remplie ;<br/>
              - l'exécution du décret ne préjudicie pas de manière grave et immédiate à l'intérêt d'une bonne administration de la justice ; <br/>
              - le décret contesté ne méconnaît pas les dispositions de l'article 28-3 de l'ordonnance n° 58-1270 du 22 décembre 1958 portant loi organique relative au statut de la magistrature ;<br/>
              - le décret contesté n'est entaché ni d'erreur de fait, ni d'erreur de droit, ni d'erreur manifeste d'appréciation ;<br/>
              - le décret contesté ne constitue pas une sanction disciplinaire déguisée ;<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, Mme A...B...et, d'autre part, la garde des sceaux, ministre de la justice ainsi que le Premier ministre ;<br/>
<br/>
              Vu le procès-verbal de l'audience publique du 29 avril 2013 à 16 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Thouin Palat, avocat au Conseil d'Etat et à la Cour de cassation, avocat de Mme B...;<br/>
<br/>
              - MmeB... ;<br/>
<br/>
              - les représentantes de la garde des sceaux, ministre de la justice ;<br/>
<br/>
              et à l'issue de laquelle l'instruction a été prolongée jusqu'au jeudi 2 mai à 17 heures ;<br/>
<br/>
              Vu les pièces, enregistrées le 29 avril 2013, produites par la garde des sceaux, ministre de la justice ;<br/>
<br/>
              Vu les observations, enregistrées le 2 mai 2013, présentées pour Mme B...;<br/>
<br/>
              Vu les observations, enregistrées le 2 mai 2013, présentées par la garde des sceaux, ministre de la justice ;<br/>
<br/>
              Vu la Constitution ;<br/>
<br/>
              Vu l'ordonnance n° 58-1270 du 22 décembre 1958 modifiée notamment par la loi organique n° 2001-539 du 25 juin 2001, ensemble la décision du Conseil constitutionnel n° 2001-445 DC du 19 juin 2001 ; <br/>
<br/>
              Vu la loi n° 79-587 du 11 juillet 1979 ;<br/>
<br/>
              Vu le décret n° 93-21 du 7 janvier 1993 modifié notamment par le décret n° 2001-1380 du 31 décembre 2001 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ;<br/>
<br/>
              2. Considérant que Mme B...a été chargée des fonctions de juge d'instruction au tribunal de grande instance de Paris par décret du 13 novembre 1980 ; qu'un décret du 6 novembre 1990 l'a nommée premier juge d'instruction à ce tribunal ; qu'elle a été nommée à ce même tribunal vice-président chargée des fonctions de l'instruction par décret du 26 février 2003 ; que le décret contesté du 27 mars 2013 l'a déchargée de ces dernières fonctions ;<br/>
<br/>
              3. Considérant, d'une part, que l'article 23-8 de l'ordonnance du 22 décembre 1958 portant loi organique relative au statut de la magistrature, tel qu'il résulte de la loi organique du 25 juin 2001, limite à dix ans la durée d'exercice de la fonction de juge d'instruction dans un même tribunal de grande instance ; qu'il ajoute qu'à l'expiration de cette période de dix ans, le magistrat est, s'il n'a pas reçu une nouvelle affectation, déchargé de ses fonctions par décret du Président de la République et qu'il exerce alors au sein du tribunal les fonctions de magistrat du siège auxquelles il a été initialement nommé ; que l'article 13 de la loi organique du 25 juin 2001 précise que la règle limitant à dix ans les fonctions de juge chargé de l'instruction dans un même tribunal ne s'applique qu'aux magistrats nommés après le 1er janvier 2002 ; que, par sa décision du 19 juin 2001, le Conseil constitutionnel a jugé que, compte tenu notamment de cette dernière garantie, la limitation à dix ans des fonctions de juge chargé de l'instruction dans un même tribunal ne portait pas atteinte au principe de l'inamovibilité des magistrats du siège ;<br/>
<br/>
              4. Considérant, d'autre part, que le décret du 31 décembre 2001 a modifié le décret du 7 janvier 1993 et supprimé pour l'avenir les fonctions de premier juge d'instruction, qu'il a remplacées par des fonctions de vice-président chargé de l'instruction, tout en prévoyant que les premiers juges d'instruction en fonctions à la date de son entrée en vigueur avaient le choix entre conserver leurs fonctions, sans faire en conséquence l'objet d'une nouvelle nomination dans celles-ci, ou demander à être nommés en qualité de vice-président chargé de l'instruction ; que la nomination en cette dernière qualité, qui supposait un avis conforme du Conseil supérieur de la magistrature, et assurait, afin de répondre aux exigences résultant de la loi organique du 25 juin 2001, la nomination dans un emploi de magistrat du siège indépendamment de l'affectation aux fonctions d'instruction, avait, pour l'application de l'article 23-8 de l'ordonnance du 22 décembre 1958, le caractère d'une nouvelle nomination, même si elle ne s'accompagnait pas d'un changement d'attributions ; <br/>
<br/>
              5. Considérant qu'il résulte de l'instruction qu'après qu'elle avait fait le choix de demander à être nommée vice-président chargée de l'instruction, Mme B...a été nommée en cette qualité par décret du 26 février 2003 ; qu'il résulte de ce qui a été dit au point 4 que le délai de dix ans prévu par l'article 23-8 de l'ordonnance du 22 décembre 1958 s'appliquait à cette nomination, postérieure au 1er janvier 2002, alors même que les fonctions exercées par l'intéressée demeuraient en fait inchangées ; qu'il en résulte que le moyen tiré de ce que le décret contesté, déchargeant au terme de ce délai Mme B...de ses fonctions à l'instruction, méconnaîtrait les règles posées par l'ordonnance du 22 décembre 1958 et porterait atteinte aux garanties relatives à l'inamovibilité des magistrats du siège ne paraît pas de nature, en l'état de l'instruction, à faire naître un doute sérieux sur la légalité de ce décret ;<br/>
<br/>
              6. Considérant que le décret litigieux, qui se borne à faire application des dispositions issues de la loi organique du 25 juin 2001, n'a pas le caractère d'une décision individuelle défavorable au sens de la loi du 11 juillet 1979 ; que le moyen tiré de ce que ce décret aurait dû être motivé n'est donc pas de nature à créer, en l'état de l'instruction, un doute sérieux sur sa légalité ; que n'est pas non plus de nature à faire naître un tel doute le moyen tiré de ce qu'il aurait le caractère d'une sanction disciplinaire déguisée ; <br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que la requête de Mme B...tendant à la suspension de l'exécution du décret du 27 mars 2013 ne peut, en l'absence, en l'état de l'instruction, de moyen de nature à créer un doute sérieux sur la légalité de ce décret, qu'être rejetée ; que les conclusions de Mme B...tendant à l'application de l'article L. 761-1 du code de justice administrative ne peuvent, par voie de conséquence, qu'être également rejetées ; <br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de Mme B...est rejetée. <br/>
Article 2 : La présente ordonnance sera notifiée à Mme A...B..., au Premier ministre et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
