<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041509289</ID>
<ANCIEN_ID>JG_L_2020_01_000000425812</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/50/92/CETATEXT000041509289.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 30/01/2020, 425812, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-01-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>425812</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>Mme Isabelle Lemesle</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:425812.20200130</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              1° sous le n° 425812, par une requête, un mémoire complémentaire et un nouveau mémoire, enregistrés le 28 novembre 2018, 28 février 2019 et 2 avril 2019, au secrétariat du contentieux du Conseil d'Etat, le syndicat départemental Confédération française des travailleurs chrétiens Santé-Sociaux de La Réunion et de Mayotte, le groupement départemental des syndicats des personnels des services publics et des services de santé Force ouvrière du département de La Réunion et l'union départementale UNSA santé et sociaux public et privé de La Réunion demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2018-814 du 27 septembre 2018 relatif à l'indemnité allouée à certains fonctionnaires exerçant dans l'un des établissements mentionnés à l'article 2 de la loi n° 86-33 du 9 janvier 1986 situés à La Réunion ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              2° sous le n° 428615, par une requête enregistrée le 5 mars 2019 au secrétariat du contentieux du Conseil d'Etat, le syndicat départemental Confédération française des travailleurs chrétiens Santé-Sociaux de La Réunion et de Mayotte demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite par laquelle le Premier ministre a rejeté sa demande tendant à l'abrogation du décret n° 2018-814 du 27 septembre 2018 relatif à l'indemnité allouée à certains fonctionnaires exerçant dans l'un des établissements mentionnés à l'article 2 de la loi n° 86-33 du 9 janvier 1986 situés à La Réunion ; <br/>
<br/>
              2°) d'annuler ce décret ou d'enjoindre au Premier ministre de l'abroger ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, et notamment son article 73 ; <br/>
              - la loi n° 50-407 du 3 avril 1950 ; <br/>
              - la loi n° 86-33 du 9 janvier 1986 ; <br/>
              - le décret n° 49-55 du 11 janvier 1949 ; <br/>
              - le décret n° 60-406 du 26 avril 1960 ; <br/>
              - le décret n° 2012-739 du 9 mai 2012 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Isabelle Lemesle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat du syndicat -départemental CFTC Santé-Sociaux Réunion-Mayotte et autres<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Les requêtes enregistrées sous les n°s 425812 et 428615 présentent à juger les mêmes questions. Il y a lieu de les joindre pour statuer par une même décision. <br/>
<br/>
              2. Le décret du 11 janvier 1949 complétant le régime de rémunération des fonctionnaires de l'Etat en service dans les départements de la Guadeloupe, de la Guyane française, de la Martinique et de La Réunion, applicable aux fonctionnaires hospitaliers en vertu de l'article 77 de la loi du 9 janvier 1986 portant dispositions statutaires relatives à la fonction publique hospitalière, institue un index de correction applicable au traitement indiciaire, à l'indemnité de résidence, au supplément familial de traitement et à la majoration de traitement prévue par la loi du 3 avril 1950 concernant les conditions de rémunération et les avantages divers accordés aux fonctionnaires en service dans les départements de la Martinique, de la Guadeloupe, de la Guyane et de La Réunion. Des irrégularités ont été constatées dans l'application par les établissements hospitaliers de La Réunion de cet index de correction, avec la majoration d'éléments de rémunération non mentionnés par les textes applicables. Le décret attaqué du 27 septembre 2018 institue une indemnité ayant pour objet de compenser, à titre transitoire, les conséquences sur la rémunération des agents concernés de la fin des modalités illégales d'application de l'index de correction. Les syndicats requérants demandent l'annulation pour excès de pouvoir de ce décret ainsi que du refus implicite du Premier ministre de l'abroger. <br/>
<br/>
              Sur les moyens de légalité externe :  <br/>
<br/>
              3. Si, dans le cadre d'un recours pour excès de pouvoir dirigé contre la décision refusant d'abroger un acte réglementaire, la légalité des règles fixées par celui-ci, la compétence de son auteur et l'existence d'un détournement de pouvoir peuvent être utilement critiquées, il n'en va pas de même des conditions d'édiction de cet acte, les vices de forme et de procédure dont il serait entaché ne pouvant être utilement invoqués que dans le cadre du recours pour excès de pouvoir dirigé contre l'acte réglementaire lui-même.<br/>
<br/>
              4. Il résulte de ce qui précède que les syndicats requérants ne peuvent utilement invoquer les moyens tirés du défaut de consultation du Conseil supérieur de la fonction publique hospitalière et du conseil départemental de La Réunion qu'à l'appui de leurs conclusions tendant à l'annulation pour excès de pouvoir du décret du 27 septembre 2018. <br/>
<br/>
              5. L'article 12 de la loi du 9 janvier 1986 portant dispositions statutaires relatives à la fonction publique hospitalière dispose que : " Le Conseil supérieur de la fonction publique hospitalière est saisi pour avis des projets de loi, des projets de décret de portée générale relatifs à la situation des personnels des établissements mentionnés à l'article 2 et des projets de statuts particuliers des corps et emplois ". Aux termes de l'article 6 du décret du 9 mai 2012 relatif au Conseil supérieur de la fonction publique hospitalière et à l'Observatoire national des emplois et des métiers de la fonction publique hospitalière : " Le conseil supérieur est saisi pour avis : / (...) 4° Des projets de décret relatifs à la situation des agents publics de la fonction publique hospitalière ; / 5° Des projets de décret relatifs aux statuts particuliers des corps et emplois de la fonction publique hospitalière ; / 6° Des projets de décret qui modifient ou abrogent, de manière coordonnée par des dispositions ayant le même objet, un ou plusieurs statuts particuliers de corps ". Par ailleurs, aux termes de l'article 1er du décret du 26 avril 1960 relatif à l'adaptation du régime législatif et de l'organisation administrative des départements de la Guadeloupe, de la Guyane, de la Martinique et de La Réunion : " Tous les projets de loi et de décrets tendant à adapter la législation ou l'organisation administrative des départements d'outre-mer à leur situation particulière seront préalablement soumis, pour avis, aux conseils généraux de ces départements (...) ".  <br/>
<br/>
              6. Le décret attaqué du 27 septembre 2018, qui institue une indemnité au bénéfice des agents de la fonction publique hospitalière affectés à La Réunion à la date de sa publication, ne revêt pas de caractère statutaire au sens et pour l'application de l'article 12 précité de la loi du 9 janvier 1986. Il n'a pas non plus pour objet d'adapter la législation ou l'organisation administrative des départements d'outre-mer à leur situation particulière au sens et pour l'application de l'article 1er du décret du 26 avril 1960 également précité. Dès lors, les moyens tirés respectivement du défaut de consultation du Conseil supérieur de la fonction publique hospitalière et du conseil départemental de La Réunion ne peuvent qu'être écartés. <br/>
<br/>
              Sur les moyens de légalité interne : <br/>
<br/>
              7. En premier lieu, le principe d'égalité ne s'oppose pas à ce que l'autorité investie du pouvoir réglementaire règle de façon différente des situations différentes, ni à ce qu'elle déroge à l'égalité pour des raisons d'intérêt général pourvu que, dans l'un comme dans l'autre cas, la différence de traitement qui en résulte soit en rapport direct avec l'objet de la norme qui l'établit et ne soit pas manifestement disproportionnée au regard des motifs susceptibles de la justifier. <br/>
<br/>
              8. D'une part, les syndicats requérants ne sauraient utilement invoquer la situation des fonctionnaires affectés en métropole pour contester la légalité du décret attaqué qui s'inscrit dans la mise en oeuvre du régime prévu par le décret précité du 11 janvier 1949 pour les seuls fonctionnaires affectés en Guadeloupe, Guyane française, Martinique et à La Réunion afin de leur permettre de bénéficier d'un index de correction applicable à leurs rémunérations. D'autre part, le décret attaqué a pour objet de compenser temporairement et à court terme les effets de la fin des pratiques illégales des établissements publics hospitaliers de La Réunion dans l'application de l'index de correction pour tenir compte des difficultés auxquelles pourraient être confrontés les agents du fait de la baisse de leurs rémunérations consécutive à la suppression de ces pratiques. A cet égard, les agents hospitaliers qui n'étaient pas en fonction à La Réunion à la date d'entrée en vigueur du décret sont placés dans une situation différente et la différence de traitement qui résulte de son application est en rapport direct avec l'objet poursuivi et n'est pas manifestement disproportionnée.<br/>
<br/>
              9. En deuxième lieu, le décret attaqué n'a ni pour objet, ni pour effet, de diminuer la rémunération des agents affectés dans les services hospitaliers de La Réunion, mais vise au contraire à permettre le maintien, à titre transitoire, de leur rémunération en dépit de la fin des modalités illégales d'application de l'index de correction. Par suite, nonobstant l'absence de pérennité et la dégressivité du montant de l'indemnité instituée, qui se trouve réduit à due concurrence de l'augmentation du traitement indiciaire de l'agent, les syndicats requérants ne sont pas fondés à soutenir que le décret attaqué serait entaché d'une erreur manifeste d'appréciation. <br/>
<br/>
              10. Il résulte de tout ce qui précède que les requérants ne sont pas fondés à demander l'annulation pour excès de pouvoir du décret attaqué ni, en l'absence de circonstances nouvelles, du refus du Premier ministre d'abroger ce décret. Leurs requêtes doivent être rejetées, sans qu'il soit besoin de se prononcer sur la fin de non-recevoir opposée par le ministre des solidarités et de la santé en défense. L'article L. 761-1 du code de justice administrative fait obstacle à ce que la somme demandée par les requérants soit mise à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les requêtes du syndicat départemental Confédération française des travailleurs chrétiens Santé-Sociaux de La Réunion et de Mayotte et autres sont rejetées.<br/>
Article 2 : La présente décision sera notifiée au syndicat départemental Confédération française des travailleurs chrétiens Santé-Sociaux de La Réunion et de Mayotte, au groupement départemental des syndicats des personnels des services publics et des services de santé Force ouvrière du département de La Réunion, à l'union départementale UNSA santé et sociaux public et privé de La Réunion, au Premier ministre, au ministre des solidarités et de la santé, au ministre de l'action et des comptes publics et à la ministre des outre-mer. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
