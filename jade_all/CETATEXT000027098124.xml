<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027098124</ID>
<ANCIEN_ID>JG_L_2013_02_000000337987</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/09/81/CETATEXT000027098124.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 22/02/2013, 337987</TITRE>
<DATE_DEC>2013-02-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>337987</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BLONDEL</AVOCATS>
<RAPPORTEUR>M. Thierry Carriol</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:337987.20130222</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu, 1° sous le n° 337987, le pourvoi sommaire et le mémoire complémentaire, enregistrés les 26 mars et 28 juin 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour la Fédération chrétienne des témoins de Jéhovah de France, dont le siège est 11, rue de Seine à Boulogne-Billancourt (92100) ; la Fédération chrétienne des témoins de Jéhovah de France demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 0706726 du 28 janvier 2010 par lequel le tribunal administratif de Paris a rejeté sa demande tendant à l'annulation de la décision implicite du Premier ministre lui refusant la communication de documents relatifs aux Témoins de Jéhovah détenus par la mission interministérielle de vigilance et de lutte contre les dérives sectaires ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande et d'enjoindre au Premier ministre de communiquer ces documents dans un délai d'un mois à compter de la notification de l'arrêt ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu, 2° sous le n° 337988, le pourvoi sommaire et le mémoire complémentaire, enregistrés les 26 mars et 28 juin 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour la Fédération chrétienne des témoins de Jéhovah de France, dont le siège est 11, rue de Seine à Boulogne-Billancourt (92100) ; la Fédération chrétienne des témoins de Jéhovah de France demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 0712489 du 28 janvier 2010 par lequel le tribunal administratif de Paris a rejeté sa demande tendant à l'annulation de la décision implicite du ministre de la santé et des solidarités lui refusant la communication de la note du 30 janvier 2001 de la mission interministérielle de vigilance et de lutte contre les dérives sectaires détenue par ce ministre ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande et d'enjoindre au ministre de communiquer ces documents dans un délai d'un mois à compter de la notification de l'arrêt ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu la loi n° 78-753 du 17 juillet 1978 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thierry Carriol, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de Me Blondel, avocat de la Fédération chrétienne des témoins de Jéhovah de France,<br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à Me Blondel, avocat de la Fédération chrétienne des témoins de Jéhovah de France ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que les pourvois visés ci-dessus présentent à juger les mêmes questions ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 2 de la loi du 17 juillet 1978 portant diverses mesures d'amélioration des relations entre l'administration et le public et diverses mesures d'ordre administratif, social et fiscal : " Sous réserve des dispositions de l'article 6, les autorités mentionnées à l'article 1er sont tenues de communiquer les documents administratifs qu'elles détiennent aux personnes qui en font la demande (...) " ; qu'aux termes de l'article 6 de la même loi : " I. Ne sont pas communicables les documents administratifs dont la consultation ou la communication porterait atteinte : (...) à la sûreté de l'Etat, à la sécurité publique ou à la sécurité des personnes (...) " ;<br/>
<br/>
              3. Considérant qu'en jugeant, pour rejeter les demandes de la Fédération chrétienne des témoins de Jéhovah de France tendant à l'annulation des décisions par lesquelles le Premier ministre et le ministre de la santé ont refusé, le premier, de lui communiquer des documents relatifs aux Témoins de Jéhovah de la mission interministérielle de vigilance et de lutte contre les dérives sectaires, le second une note de cette mission qu'il avait utilisée, que, compte tenu de la mission de cette autorité administrative " d'information du public sur les risques, et le cas échéant les dangers, auxquels les dérives sectaires l'exposent, et de l'intérêt qui s'attache à la protection de la santé et de la sécurité des personnes ", la communication de tels documents méconnaîtrait les dispositions précitées du I de l'article 6 de la loi du 17 juillet 1978 , sans rechercher si, en raison des informations qu'ils contiendraient, la divulgation de ces documents risquait de porter atteinte à la sûreté de l'Etat, à la sécurité publique ou à la sécurité des personnes ni si une communication partielle ou après occultation de certaines informations était le cas échéant possible, le tribunal administratif de Paris a commis une erreur de droit ; que la Fédération chrétienne des témoins de Jéhovah de France est fondée à demander, pour ce motif et sans qu'il soit besoin d'examiner les autres moyens des pourvois, l'annulation des jugements attaqués ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à la Fédération chrétienne des témoins de Jéhovah de France d'une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>				D E C I D E  :<br/>
              				--------------<br/>
<br/>
Article 1er : Les jugements du 28 janvier 2010 du tribunal administratif de Paris sont annulés.<br/>
<br/>
Article 2 : Les affaires sont renvoyées au tribunal administratif de Paris.<br/>
Article 3 : L'Etat versera à la Fédération chrétienne des témoins de Jéhovah de France une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la Fédération chrétienne des témoins de Jéhovah de France, au secrétaire général du Gouvernement et à la ministre des affaires sociales et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-06-01-02-02 DROITS CIVILS ET INDIVIDUELS. ACCÈS AUX DOCUMENTS ADMINISTRATIFS. ACCÈS AUX DOCUMENTS ADMINISTRATIFS AU TITRE DE LA LOI DU 17 JUILLET 1978. DROIT À LA COMMUNICATION. DOCUMENTS ADMINISTRATIFS COMMUNICABLES. - DOCUMENTS DE LA MIVILUDES - DEMANDE D'ANNULATION DU REFUS DE COMMUNIQUER CES DOCUMENTS - FACULTÉ POUR LE JUGE DE REJETER CETTE DEMANDE AU MOTIF QUE, COMPTE TENU DE LA MISSION DE CETTE AUTORITÉ, CETTE COMMUNICATION MÉCONNAÎTRAIT LE I DE L'ARTICLE 6 DE LA LOI DU 17 JUILLET 1978 SANS RECHERCHER SI, EU ÉGARD AU CONTENU DES DOCUMENTS DEMANDÉS, LEUR DIVULGATION RISQUAIT EFFECTIVEMENT DE PORTER ATTEINTE À LA SÛRETÉ DE L'ETAT, À LA SÉCURITÉ PUBLIQUE OU À LA SÉCURITÉ DES PERSONNES NI SI UNE COMMUNICATION PARTIELLE OU APRÈS OCCULTATION DE CERTAINES INFORMATIONS ÉTAIT POSSIBLE - ABSENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">26-06-01-02-03 DROITS CIVILS ET INDIVIDUELS. ACCÈS AUX DOCUMENTS ADMINISTRATIFS. ACCÈS AUX DOCUMENTS ADMINISTRATIFS AU TITRE DE LA LOI DU 17 JUILLET 1978. DROIT À LA COMMUNICATION. DOCUMENTS ADMINISTRATIFS NON COMMUNICABLES. - DOCUMENTS DE LA MIVILUDES - DEMANDE D'ANNULATION DU REFUS DE COMMUNIQUER CES DOCUMENTS - FACULTÉ POUR LE JUGE DE REJETER CETTE DEMANDE AU MOTIF QUE, COMPTE TENU DE LA MISSION DE CETTE AUTORITÉ, CETTE COMMUNICATION MÉCONNAÎTRAIT LE I DE L'ARTICLE 6 DE LA LOI DU 17 JUILLET 1978 SANS RECHERCHER SI, EU ÉGARD AU CONTENU DES DOCUMENTS DEMANDÉS, LEUR DIVULGATION RISQUAIT EFFECTIVEMENT DE PORTER ATTEINTE À LA SÛRETÉ DE L'ETAT, À LA SÉCURITÉ PUBLIQUE OU À LA SÉCURITÉ DES PERSONNES NI SI UNE COMMUNICATION PARTIELLE OU APRÈS OCCULTATION DE CERTAINES INFORMATIONS ÉTAIT POSSIBLE - ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 26-06-01-02-02 Demande tendant à l'annulation du refus de communication de documents administratifs de la mission interministérielle de vigilance et de lutte contre les dérives sectaires (Miviludes).... ...Le juge ne peut rejeter cette demande au motif que, compte tenu de la mission de cette autorité administrative d'information du public sur les risques, et le cas échéant les dangers, auxquels les dérives sectaires l'exposent, et de l'intérêt qui s'attache à la protection de la santé et de la sécurité des personnes, la communication de tels documents méconnaîtrait les dispositions du I de l'article 6 de la loi n° 78-753 du 17 juillet 1978, sans rechercher si, en raison des informations qu'ils contiendraient, la divulgation de ces documents risque de porter atteinte à la sûreté de l'Etat, à la sécurité publique ou à la sécurité des personnes ni si une communication partielle ou après occultation de certaines informations est, le cas échéant, possible.</ANA>
<ANA ID="9B"> 26-06-01-02-03 Demande tendant à l'annulation du refus de communication de documents administratifs de la mission interministérielle de vigilance et de lutte contre les dérives sectaires (Miviludes).... ...Le juge ne peut rejeter cette demande au motif que, compte tenu de la mission de cette autorité administrative d'information du public sur les risques, et le cas échéant les dangers, auxquels les dérives sectaires l'exposent, et de l'intérêt qui s'attache à la protection de la santé et de la sécurité des personnes, la communication de tels documents méconnaîtrait les dispositions du I de l'article 6 de la loi n° 78-753 du 17 juillet 1978, sans rechercher si, en raison des informations qu'ils contiendraient, la divulgation de ces documents risque de porter atteinte à la sûreté de l'Etat, à la sécurité publique ou à la sécurité des personnes ni si une communication partielle ou après occultation de certaines informations est, le cas échéant, possible.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. CE, 14 mars 2011, ministre de l'intérieur, de l'outre-mer et des collectivités territoriales c/ comité français des scientologues contre la discrimination, n° 309313, inédite.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
