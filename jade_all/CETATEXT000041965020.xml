<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041965020</ID>
<ANCIEN_ID>JG_L_2020_06_000000424036</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/96/50/CETATEXT000041965020.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 05/06/2020, 424036, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-06-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>424036</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT, ROBILLOT</AVOCATS>
<RAPPORTEUR>Mme Cécile Nissen</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:424036.20200605</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Alcyom a demandé au tribunal administratif de Paris de condamner l'Etat à lui verser la somme de 180 199 euros, assortie des intérêts au taux légal et des intérêts moratoires, en réparation du préjudice qu'elle estimait avoir subi à raison du rejet de la demande d'agrément qu'elle avait déposée le 19 décembre 2011, pour le compte de la société Sofaplast 2012, au titre du financement d'investissements productifs neufs en Nouvelle-Calédonie au bénéfice de la société Sofaplast.<br/>
<br/>
              Par une ordonnance du 16 février 2017, le président du tribunal administratif de Paris a transmis sa demande au tribunal administratif de Nouvelle-Calédonie. <br/>
<br/>
              Par un jugement n° 1700070 du 14 septembre 2017, le tribunal administratif de Nouvelle-Calédonie a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 17PA03544 du 6 juillet 2018, la cour administrative d'appel de Paris a rejeté l'appel formé par la société Alcyom contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 10 septembre et 11 décembre 2018 au secrétariat du contentieux du Conseil d'Etat, la société Alcyom demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Nissen, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Potier de la Varde, Buk Lament, Robillot, avocat de la société Alcyom ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Sofaplast, entreprise industrielle ayant des activités en Nouvelle-Calédonie, a donné mandat le 15 décembre 2011 à la société Alcyom pour assurer le financement d'investissements destinés à l'un de ses sites de production dans le cadre d'une société de portage et au moyen de l'avantage fiscal pour les investissements productifs neufs en Nouvelle-Calédonie prévu à l'article 199 undecies B du code général des impôts. En application de cet article, la société Alcyom a sollicité le 19 décembre 2011 auprès du ministre chargé du budget, pour le compte de la société de portage Sofaplast 2012 qu'elle avait constituée, l'agrément préalable des investissements envisagés. Cet agrément a été refusé par une décision du 29 janvier 2013 qui a été annulée par un jugement du 18 juin 2014 du tribunal administratif de Paris devenu définitif. La société Alcyom demande l'annulation de l'arrêt du 6 juillet 2018 par lequel la cour administrative d'appel de Paris a rejeté l'appel qu'elle avait formé contre le jugement du 14 septembre 2017 du tribunal administratif de Nouvelle-Calédonie rejetant sa demande tendant à la condamnation de l'Etat à réparer le préjudice qu'elle estimait avoir subi du fait du refus illégal d'agrément. <br/>
<br/>
              2. Aux termes de l'article 199 undecies B du code général des impôts dans sa rédaction applicable au litige : " I. Les contribuables domiciliés en France au sens de l'article 4 B peuvent bénéficier d'une réduction d'impôt sur le revenu à raison des investissements productifs neufs qu'ils réalisent dans les départements d'outre-mer, à Saint-Pierre-et-Miquelon, à Mayotte, en Nouvelle-Calédonie, en Polynésie française, à Saint-Martin, à Saint-Barthélemy, dans les îles Wallis-et-Futuna et les Terres australes et antarctiques françaises, dans le cadre d'une entreprise exerçant une activité agricole ou une activité industrielle, commerciale ou artisanale relevant de l'article 34. / (...) La réduction d'impôt prévue au présent I s'applique, dans les conditions prévues au vingt-sixième alinéa, aux investissements réalisés par une société soumise de plein droit à l'impôt sur les sociétés dont les actions sont détenues intégralement et directement par des contribuables, personnes physiques, domiciliés en France au sens de l'article 4 B. (...) L'application de cette disposition est subordonnée au respect des conditions suivantes : / 1° Les investissements ont reçu un agrément préalable du ministre chargé du budget dans les conditions prévues au III de l'article 217 undecies ; / (...) ". <br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond qu'en vertu du mandat signé le 15 décembre 2011 avec la société Sofaplast, la société Alcyom a créé la société de portage Sofaplast 2012, sollicité pour le compte de celle-ci l'agrément prévu par l'article 199 undecies B du code général des impôts et procédé à la recherche des investisseurs intéressés par l'obtention de l'avantage fiscal. En vertu des stipulations du 2 de l'article 6 de la convention de mandat, les frais d'ingénierie et de placement engagés par la société Alcyom pour la mise en oeuvre de l'opération devaient intégralement être pris en charge directement par les investisseurs fiscaux ou indirectement par la société de portage. Ces frais ont été inscrits en charges dans les comptes prévisionnels de la société Sofaplast 2012 figurant au dossier, sous l'intitulé " frais d'étude et de placement ". Toutefois, compte tenu du refus d'agrément qui a mis un terme à l'opération de défiscalisation, alors au stade de la souscription des parts de la société de portage par les investisseurs, les frais engagés par la société Alcyom sont restés à sa charge. <br/>
<br/>
              4. En jugeant que le préjudice financier subi par la société Alcyom du fait du refus illégal d'agrément résultait uniquement de ses relations avec la société Sofaplast, alors que le défaut de remboursement des frais d'ingénierie et de placement engagés par la société Alcyom, qui n'a pas été indemnisé au titre du préjudice subi par la société Sofaplast, résultait directement du refus d'agrément qui privait de son intérêt fiscal l'opération proposée aux investisseurs, la cour a inexactement qualifié les faits qui lui étaient soumis.  <br/>
<br/>
              5. Il résulte de ce qui précède que la société Alcyom est fondée, sans qu'il soit besoin de se prononcer sur l'autre moyen du pourvoi, à demander l'annulation de l'arrêt qu'elle attaque. <br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la société Alcyom au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 6 juillet 2018 de la cour administrative d'appel de Paris est annulé. <br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br/>
<br/>
Article 3 : L'Etat versera à la société Alcyom la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la société Alcyom et au ministre de l'action et des comptes publics.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
