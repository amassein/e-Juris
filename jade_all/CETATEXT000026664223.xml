<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026664223</ID>
<ANCIEN_ID>JG_L_2012_11_000000340087</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/66/42/CETATEXT000026664223.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  9ème sous-section jugeant seule, 21/11/2012, 340087, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-11-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>340087</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 9ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Jean-Pierre Jouguelet</PRESIDENT>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>M. Matthieu Schlesinger</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Claire Legras</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:340087.20121121</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 31 mai et 30 août 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. Jean-Paul B, demeurant ... ; M. B demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 08LY01605 du 9 mars 2010 par lequel la cour administrative d'appel de Lyon, sur appel du ministre du budget, des comptes publics, de la fonction publique et de la réforme de l'Etat, après avoir annulé l'article 1er du jugement du 9 avril 2008 du tribunal administratif de Clermont-Ferrand l'ayant déchargé des cotisations supplémentaires d'impôt sur le revenu et des pénalités correspondantes auxquelles il a été assujetti au titre des années 2002 et 2003 à raison de la remise en cause des déductions forfaitaires pour frais professionnels de 14 % et de 5 %, l'a rétabli aux rôles des cotisations supplémentaires d'impôt sur le revenu et des pénalités correspondantes auxquelles il a été assujetti au titre des années 2002 et 2003 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel du ministre du budget, des comptes publics de la fonction publique et de la réforme de l'Etat ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Matthieu Schlesinger, Auditeur,  <br/>
<br/>
              - les observations de la SCP Masse-Dessen, Thouvenin, avocat de M. B,<br/>
<br/>
              - les conclusions de Mme Claire Legras, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Masse-Dessen, Thouvenin, avocat de M. B ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. B, qui exerçait une activité de professeur de musique au sein de l'école de musique de Vichy, a déclaré avoir perçu en 2002 et 2003, dans la catégorie des traitements et salaires, d'une part, en sa qualité d'enseignant, des salaires pour des montants de 36 533 euros et 37 692 euros et, d'autre part, à raison de la rémunération de représentations artistiques, les sommes de respectivement 477,30 euros, 450 et 250 euros ; que le service a substitué aux frais réels que M. B avait déduits au titre de ces deux années la déduction forfaitaire de 10 % ; que par une réclamation en date du 18 décembre 2006, M. B a demandé à bénéficier des deux déductions forfaitaires, de 14 % et 5 %, sur l'ensemble des revenus imposables dans la catégorie des traitements et salaires en se fondant sur l'instruction du 30 décembre 1998 publiée au Bulletin officiel des impôts (BOI 5 F-1-99) ; que sa réclamation ayant été rejetée, M. B a saisi le tribunal administratif de Clermont-Ferrand qui a fait droit à sa demande par un jugement du 9 avril 2008 que la cour administrative d'appel de Lyon a annulé par un arrêt en date du 9 mars 2010 et qui a remis à la charge des époux B les cotisations supplémentaires d'impôt sur le revenu dont le tribunal administratif de Clermont-Ferrand avait prononcé la décharge ; que M. B se pourvoit en cassation contre cet arrêt ;<br/>
<br/>
              2. Considérant que l'instruction 5 F-1-99 du 30 décembre 1998 précise au point 90 du A de sa section 4, portant sur les artistes musiciens, que " la déduction accordée au titre de l'amortissement des instruments de musique et des frais accessoires, ainsi que des matériels techniques à usage professionnel, est fixée à 14 % du montant total de la rémunération déclarée ès qualités à l'impôt sur le revenu, y compris, le cas échéant, les rémunérations perçues au titre d'une activité d'enseignement artistique, exercée notamment dans les conservatoires ou écoles de musique [...] " ; qu'au point 92, elle admet que " les dépenses suivantes : frais vestimentaires et de coiffure, de représentation, de communications téléphoniques à caractère professionnel, de fournitures diverses (partition, métronome, pupitre...), ainsi que les frais de formation et les frais médicaux spécifiques (...) soient prises en compte dans le cadre d'une déduction égale à 5 % de la même rémunération nette annuelle (...) " ;<br/>
<br/>
              3. Considérant que cette instruction ne définit pas la qualité d'artiste musicien et ne subordonne pas le bénéfice des déductions qu'elle prévoit à la prédominance de cette activité par rapport à celle d'enseignant ; que ses termes mêmes n'excluent en rien que les revenus d'enseignement susceptibles d'être inclus dans la base à laquelle s'appliquent les déductions dont le contribuable a demandé le bénéfice, soient prépondérants ;<br/>
<br/>
              4. Considérant que la cour a relevé que M. B n'avait donné que quelques concerts par an au cours des années en litige en tant qu'artiste musicien pour une rémunération représentant moins de 10 % de celle perçue en tant qu'enseignant dans des écoles de musique ; qu'en en déduisant que M. B ne pouvait être regardé comme ayant exercé ses fonctions dans des conditions analogues à celles d'un musicien professionnel et que, par suite, ses revenus de professeur de musique n'entraient pas dans les prévisions de l'instruction 5-F-1-99 alors que celle-ci ne prévoit aucun critère, quantitatif ou autre, d'exercice de l'activité artistique, la cour a commis une erreur de droit ; que, dès lors et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, M. B est fondé à demander l'annulation de l'arrêt qu'il attaque ;<br/>
<br/>
              5. Considérant que l'Etat étant la partie perdante dans la présente instance, il y a lieu de mettre à sa charge la somme de 3 000 euros que M. B demande au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon du 9 mars 2010 est annulé.<br/>
Article 2 : Le jugement de l'affaire est renvoyé à la cour administrative d'appel de Lyon. <br/>
Article 3 : L'Etat versera à M. B la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : La présente décision sera notifiée à M. Jean-Paul B et au ministre de l'économie et des finances.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
