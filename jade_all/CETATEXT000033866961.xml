<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033866961</ID>
<ANCIEN_ID>JG_L_2017_01_000000389112</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/86/69/CETATEXT000033866961.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 12/01/2017, 389112, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-01-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389112</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>M. Jean-Luc Matt</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Astrid Nicolazo de Barmon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:389112.20170112</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La SAS LMBO Finance a demandé au tribunal administratif de Paris de prononcer la décharge de la cotisation minimale de taxe professionnelle à laquelle elle a été assujettie au titre de l'année 2006. Par un jugement n° 1013651 du 11 avril 2012, le tribunal administratif de Paris a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 12PA03042 du 29 janvier 2015, la cour administrative d'appel de Paris a rejeté l'appel formé par la SAS LMBO Finance contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 30 mars et 30 juin 2015 au secrétariat du contentieux du Conseil d'Etat, la SAS LMBO Finance demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'État la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - la loi n° 85-695 du 11 juillet 1985 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Matt, maître des requêtes, <br/>
<br/>
              - les conclusions de Mme Marie-Astrid Nicolazo de Barmon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Soltner, Texidor, Perier, avocat de la société LMBO Finance ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. D'une part, aux termes de l'article 1-1 de la loi du 11 juillet 1985 portant diverses dispositions d'ordre économique et financier, dans sa rédaction alors applicable, les sociétés de capital-risque sont des sociétés par actions ayant pour objet social " la gestion d'un portefeuille de valeurs mobilières ". En vertu de l'article 1er de cette loi, elles doivent procéder à des investissements dans des sociétés non cotées pour pouvoir bénéficier d'un régime de faveur au regard de l'imposition des sociétés. Il en résulte que ces sociétés doivent être regardées comme exerçant à titre habituel une activité professionnelle au sens des dispositions, alors en vigueur, du I de l'article 1447 du code général des impôts relatives à l'assujettissement à la taxe professionnelle.<br/>
<br/>
              2. D'autre part, aux termes du I de l'article 1647 E du code général des impôts, dans sa rédaction applicable aux années d'imposition en litige : " La cotisation de taxe professionnelle des entreprises dont le chiffre d'affaires est supérieur à 7 600 000 euros est au moins égale à 1,5 % de la valeur ajoutée produite par l'entreprise, telle que définie au II de l'article 1647 B sexies (...) ". Selon le II de l'article 1647 B sexies du même code, dans sa rédaction alors en vigueur : " 1. La valeur ajoutée (...) est égale à l'excédent hors taxe de la production sur les consommations de biens et services en provenance de tiers (...). / 2. Pour la généralité des entreprises, la production de l'exercice est égale à la différence entre : / d'une part, les ventes, les travaux, les prestations de services ou les recettes, les produits accessoires ; les subventions d'exploitation ; les ristournes, rabais et remises obtenus ; les travaux faits par l'entreprise pour elle-même ; les stocks à la fin de l'exercice ; / et, d'autre part, les achats de matières et marchandises, droits de douane compris ; les réductions sur ventes ; les stocks au début de l'exercice. (...) / 3. La production des établissements de crédit, des entreprises ayant pour activité exclusive la gestion de valeurs mobilières est égale à la différence entre : / d'une part, les produits d'exploitation bancaires et produits accessoires ; / et, d'autre part, les charges d'exploitation bancaires. (...) ". Eu égard à l'objet de ces dispositions, qui est de tenir compte de la capacité contributive des entreprises en fonction de leur activité, les entreprises ayant pour activité exclusive la gestion de valeurs mobilières ne s'entendent, pour leur application, que des seules entreprises qui exercent cette activité pour leur propre compte. Les sociétés de capital-risque, qui gèrent pour leur propre compte des participations financières, sont, dès lors, soumises aux modalités de calcul de la valeur ajoutée prévues au 3 du II de l'article 1647 B sexies du code général des impôts pour les entreprises ayant pour activité exclusive la gestion de valeurs mobilières.<br/>
<br/>
              3. Les dispositions de l'article 1647 B sexies du code général des impôts fixent la liste limitative des catégories d'éléments comptables qui doivent être pris en compte dans le calcul de la valeur ajoutée servant de base à la cotisation minimale de taxe professionnelle. Pour l'application de ces dispositions, la production d'une entreprise ayant pour activité exclusive la gestion de valeurs mobilières doit être calculée, comme celle des établissements de crédit, en fonction des règles comptables fixés par le règlement du comité de la réglementation bancaire n° 91-01 du 16 janvier 1991 relatif à l'établissement et à la publication des comptes individuels annuels des établissements de crédit. L'annexe à ce règlement contient un modèle de compte de résultat et des commentaires de chacun des postes de ce compte. En vertu de ces dispositions, les produits d'exploitation bancaires et produits accessoires incluent les " gains et pertes sur opérations des portefeuilles de négociation " et les " gains et pertes sur opérations des portefeuilles de placement et assimilés ", mais non les " gains et pertes sur actifs immobilisés ", ce dernier poste étant défini comme " le solde en bénéfice ou perte des opérations sur titres de participation, sur autres titres détenus à long terme et sur parts dans les entreprises liées ".<br/>
<br/>
              4. Pour l'application de l'article 1647 E du code général des impôts à une société de capital-risque, le seuil d'assujettissement à la cotisation minimale de taxe professionnelle doit aussi être calculé en ne tenant compte que des produits d'exploitation bancaires et des produits accessoires, lesquels ne comprennent pas, selon par le règlement du comité de la réglementation bancaire n° 91-01 du 16 janvier 1991, les plus-values sur titres de participation et autres titres détenus à long terme. Il en va de même pour l'application du II de l'article 1647 B sexies du même code relatif à la détermination de la valeur ajoutée.<br/>
<br/>
              5. L'article 9 bis du règlement du comité de la réglementation bancaire n° 90-01 du 23 février 1990 relatif à la comptabilisation des opérations sur titres des établissements de crédit définit les titres de l'activité de portefeuille comme ceux qui ont été acquis dans le cadre d'investissements " réalisés de façon régulière avec pour seul objectif d'en retirer un gain en capital à moyen terme sans intention d'investir durablement dans le développement du fonds de commerce de l'entreprise émettrice, ni de participer activement à sa gestion opérationnelle ", en ajoutant que " des titres ne peuvent être affectés à ce portefeuille que si cette activité, exercée de manière significative et permanente dans un cadre structuré, procure à l'établissement une rentabilité récurrente, provenant principalement des plus-values de cession réalisées " et en citant comme exemple ceux " les titres détenus dans le cadre d'une activité de capital-risque ". Le même article dispose que relèvent de la catégorie des autres titres détenus à long terme " les investissements réalisés sous forme de titres dans l'intention de favoriser le développement de relations professionnelles durables en créant un lien privilégié avec l'entreprise émettrice, mais sans influence dans la gestion des entreprises dont les titres sont détenus en raison du faible pourcentage des droits de vote qu'ils représentent ". Enfin, selon les dispositions du même article, les titres de participation sont ceux " dont la possession durable est estimée utile à l'activité de l'entreprise, notamment parce qu'elle permet d'exercer une influence sur la société émettrice des titres, ou d'en assurer le contrôle ". Une telle utilité peut notamment être caractérisée si les conditions d'achat des titres en cause révèlent l'intention de l'acquéreur d'exercer une influence sur la société émettrice et lui donnent les moyens d'exercer une telle influence.<br/>
<br/>
              6. Les dispositions citées au point 5, élaborées pour la comptabilisation des titres détenus en propre par les établissements de crédit, sociétés de financement et organismes assimilés, doivent être appliquées, pour l'application des articles 1647 E et 1647 B sexies du code général des impôts, à la répartition, entre les catégories qu'elles définissent, des titres détenus par les sociétés de capital-risque. Compte tenu de l'objet spécifique des sociétés de capital-risque, défini à l'article 1-1 de la loi du 11 juillet 1985 portant diverses dispositions d'ordre économique et financier, l'exemple des " titres détenus dans le cadre d'une activité de capital-risque " donné à la fin de l'alinéa définissant les titres de portefeuille, ne doit pas être compris, contrairement à ce que soutient le ministre en défense, comme excluant que les titres détenus par de telles sociétés puissent être, s'ils en remplissent les conditions, placés dans les catégories des titres de participation ou des autres titres détenus à long terme. <br/>
<br/>
              7. Il résulte de ce qui précède que la cour administrative d'appel de Versailles, en jugeant qu'eu égard à l'objet légal d'une société de capital-risque, l'ensemble des produits, y compris ceux résultant de la cession des titres composant son portefeuille de valeurs mobilières comptabilisés comme titres de participation et autres titres détenus à long terme, doit être pris en compte pour l'appréciation du chiffre d'affaires servant de seuil d'assujettissement à la cotisation minimale de taxe professionnelle et pour le calcul de la valeur ajoutée au sens du 3 du II de l'article 1647 B sexies du code général des impôts, a commis une erreur de droit et inexactement qualifié les faits qui lui étaient soumis. Par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la SAS LMBO Finance est fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'État la somme de 3 500 euros à verser à la SAS LMBO Finance au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 29 janvier 2015 de la cour administrative d'appel de Paris est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br/>
Article 3 : L'État versera à la SAS LMBO Finance une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la SAS LMBO Finance et au ministre de l'économie et des finances.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
