<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039442398</ID>
<ANCIEN_ID>JG_L_2019_12_000000418026</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/44/23/CETATEXT000039442398.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 02/12/2019, 418026</TITRE>
<DATE_DEC>2019-12-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418026</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELVOLVE ET TRICHET ; SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>M. Laurent-Xavier Simonel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:418026.20191202</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              En premier lieu, la commune de Montauban a demandé au tribunal administratif de Toulouse d'annuler les deux délibérations n° 2 et n° 3 du 14 décembre 2015 par lesquelles le conseil d'administration du service départemental d'incendie et de secours (SDIS) de Tarn-et-Garonne a fixé, d'une part, le montant global des contributions des communes et des établissements publics de coopération intercommunale à son budget pour l'année 2016 à la somme de 6 750 009,55 euros et, d'autre part, la répartition entre eux de ce montant global. Par un jugement n° 1600680, 1600681 du 30 juin 2016, le tribunal administratif de Toulouse a rejeté sa demande.<br/>
<br/>
              En deuxième lieu, la commune de Montauban a demandé au tribunal administratif de Toulouse d'annuler les quatre titres exécutoires émis à son encontre par le SDIS de Tarn-et-Garonne, les 15 septembre, 12 octobre, 29 octobre et 24 novembre 2015, pour le recouvrement de sa contribution au titre du transfert de ses personnels et de ses biens et de sa contribution au budget de cet établissement public, fixées à la somme de 54 684,09 euros pour chacun des mois de septembre à décembre 2015. Par un jugement n° 1505302, 1505303, 1505867, 1505868 du 30 juin 2016, le tribunal administratif de Toulouse a rejeté sa demande.<br/>
<br/>
              En troisième lieu, la commune de Montauban a demandé au tribunal administratif de Toulouse d'annuler les trois titres exécutoires émis à son encontre par le SDIS de Tarn-et-Garonne, les 19 septembre, 24 octobre et 24 novembre 2016, pour le recouvrement des contributions de mêmes natures, fixées à la somme de 55 169 euros et 50 ou 49 centimes, pour chacun des mois d'octobre à décembre 2016. Par un jugement n° 1605161, 1605614, 1700324 du 5 juillet 2017, le tribunal administratif de Toulouse a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 16BX02983, 16BX02984, 17BX03007 du 11 décembre 2017, la cour administrative d'appel de Bordeaux a rejeté l'appel formé par la commune de Montauban contre ces trois jugements.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 9 février et 9 mai 2018 au secrétariat du contentieux du Conseil d'Etat, la commune de Montauban demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel en annulant les jugements du 30 juin 2016 et le jugement du 5 juillet 2017 ;<br/>
<br/>
              3°) de mettre à la charge du SDIS de Tarn-et-Garonne la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des collectivités territoriales ;<br/>
              - la loi n° 96-369 du 3 mai 1996 ;<br/>
              - la loi n° 2002-276 du 27 février 2002 ;<br/>
              - la loi n° 2004-811 du 13 août 2004 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent-Xavier Simonel, conseiller d'Etat en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delvolvé et Trichet, avocat de la commune de Montauban et à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat du service départemental d'incendie et de secours du Tarn et Garonne (SDIS) ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la commune de Montauban et le service départemental d'incendie et de secours (SDIS) de Tarn-et-Garonne ont conclu, le 26 janvier 2001, avec effet au 1er janvier 2001, une convention ayant pour objet le transfert des personnels et des biens affectés par la commune au fonctionnement des services d'incendie et de secours et la fixation de la " dotation annuelle de transfert " que la commune devait verser au titre de la charge nette qu'elle aurait supportée si le transfert n'était pas intervenu, s'ajoutant, au sein de la contribution due par la commune pour le financement de ce SDIS, à la somme qu'elle versait antérieurement pour le fonctionnement de ce dernier, dite " contingent d'incendie et de secours ". Cette convention a fixé le montant de cette dotation de transfert à 16 000 000 francs pour l'année 2001 et, après réduction progressive à raison de l'amortissement linéaire annuel entre 2002 et 2007 de la charge spécifique liée au nouveau régime indemnitaire des sapeurs-pompiers professionnels, à 2 256 245,46 euros, soit la contrevaleur de 14 800 000 francs, à partir de l'année 2007. <br/>
<br/>
              2. La commune de Montauban a demandé au tribunal administratif de Toulouse l'annulation des titres exécutoires émis à son encontre par le SDIS de Tarn-et-Garonne les 15 septembre, 12 octobre, 29 octobre et 24 novembre 2015, pour le recouvrement de sa contribution au financement de ce SDIS pour les mois de septembre à décembre 2015, des délibérations n° 2 et n° 3 du 14 décembre 2015 par lesquelles le conseil d'administration de ce SDIS a arrêté à 2 918 279,45 euros le montant global de la contribution qui lui était demandée pour 2016 ainsi que des titres exécutoires émis à son encontre les 19 septembre, 24 octobre et 24 novembre 2016, pour le recouvrement de sa contribution au financement de ce SDIS pour les mois d'octobre à décembre 2016, au motif que les sommes ainsi mises à sa charge intégraient une revalorisation du montant de sa " dotation de transfert " par application de l'indice des prix à la consommation. Le tribunal administratif de Toulouse a rejeté ces demandes par deux jugements du 30 juin 2016 et un jugement du 5 juillet 2017. La commune de Montauban se pourvoit en cassation contre l'arrêt du 11 décembre 2017 par lequel la cour administrative de Bordeaux a rejeté l'appel qu'elle a formé contre ces jugements.<br/>
<br/>
              3. D'une part, en vertu des articles L. 1424-13 et L. 1424-14 du code général des collectivités territoriales, les sapeurs-pompiers professionnels relevant d'un corps communal ou intercommunal et les sapeurs-pompiers volontaires relevant d'un corps communal ou intercommunal et desservant un centre de secours principal ou un centre de secours sont transférés au corps départemental, géré par le SDIS, dans un délai de cinq ans à compter de la promulgation de la loi du 3 mai 1996 relative aux services d'incendie et de secours, dans les conditions fixées par convention entre le SDIS et les communes ou établissements publics de coopération intercommunale (EPCI) considérés. Au-delà de ces transferts obligatoires, l'article L. 1424-15 du même code prévoit que, lorsqu'une commune ou un EPCI a demandé le rattachement au corps départemental d'un corps communal ou intercommunal, le SDIS procède au rattachement des sapeurs-pompiers volontaires dans les conditions prévues par son conseil d'administration. En vertu de l'article L. 1424-17 de ce code, les biens affectés, à la date de promulgation de la même loi du 3 mai 1996, notamment par les communes et les EPCI, au fonctionnement des SDIS et nécessaires au fonctionnement du service départemental d'incendie et de secours sont mis à disposition de ce dernier, à titre gratuit, à une date à fixer par convention dans le même délai de cinq ans, cette convention fixant également les conditions de prise en charge des emprunts contractés au titre de ces biens. L'article L. 1424-19 du même code prévoit qu'à toute époque, le transfert des biens au SDIS peut avoir lieu en plein propriété, selon des modalités fixées par convention.<br/>
<br/>
              4. D'autre part, aux termes de l'article L. 1424-29 du même code : " Le conseil d'administration règle par ses délibérations les affaires relatives à l'administration du service départemental d'incendie et de secours ". Aux termes de l'article L. 1424-35 du même code dans sa rédaction applicable au litige issue, notamment pour son huitième alinéa, de la loi du 13 août 2004 de modernisation de la sécurité civile : " (...) / Les modalités de calcul et de répartition des contributions des communes et des établissements publics de coopération intercommunale compétents pour la gestion des services d'incendie et de secours au financement du service départemental d'incendie et de secours sont fixées par le conseil d'administration de celui-ci. (...) (3ème alinéa). / Les contributions des communes, des établissements publics de coopération intercommunale et du département au budget du service départemental d'incendie et de secours constituent des dépenses obligatoires (4ème alinéa). / (...) Avant le 1er janvier de l'année en cause, le montant prévisionnel des contributions mentionnées aux quatrième et cinquième alinéas, arrêté par le conseil d'administration du service départemental d'incendie et de secours, est notifié aux maires et aux présidents des établissements publics de coopération intercommunale (7ème alinéa). / Pour les exercices suivant la promulgation de la loi n° 2002-276 du 27 février 2002 relative à la démocratie de proximité, le montant global des contributions des communes et des établissements publics de coopération intercommunale ne pourra excéder le montant global des contributions des communes et des établissements publics de coopération intercommunale de l'exercice précédent, augmenté de l'indice des prix à la consommation et, le cas échéant, du montant des contributions de transfert à verser par les communes et les établissements publics de coopération intercommunale sollicitant le rattachement de leurs centres de secours et d'incendie au service départemental (8ème alinéa). / (...) ".<br/>
<br/>
              5. En premier lieu, il résulte des dispositions citées ci-dessus de l'article L. 1424-35 du code général des collectivités territoriales que les modalités de calcul et de répartition des contributions que les communes et les EPCI compétents pour la gestion des services d'incendie et de secours versent au budget du SDIS sont arrêtées chaque année par délibération du conseil d'administration de cet établissement public, avant d'être notifiées aux différents contributeurs. Un SDIS ne peut renoncer à exercer la compétence qu'il tient de ces dispositions en concluant, avec une collectivité territoriale ou un EPCI contribuant à son financement, un contrat dont l'objet est de définir le montant des contributions qui doivent lui être versées, y compris pour la part de ces contributions résultant des transferts ou des mises à disposition réalisés, par les communes ou les EPCI, obligatoirement en exécution des articles L. 1424-13, L. 1424-14 et L. 1424-17 du code général des collectivités territoriales ou, le cas échéant, volontairement sur le fondement des articles L. 1424-15 et L. 1424-19 du même code. Par suite, il appartient au juge d'écarter d'office, le cas échéant, les stipulations qui auraient un tel objet figurant dans une convention conclue pour déterminer les modalités de ces transferts et mises à disposition entre le SDIS, d'une part et une commune ou un EPCI, d'autre part.<br/>
<br/>
              6. En second lieu, il résulte des dispositions du huitième alinéa de ce même article L. 1424-35, éclairées par les travaux préparatoires de la loi du 13 août 2004, que le plafonnement de l'évolution annuelle du montant global des contributions des communes et des EPCI au budget du SDIS, sous réserve de l'application de l'indice des prix à la consommation, ne fait pas obstacle, lorsqu'une commune ou un EPCI sollicite, au-delà des transferts et mises à disposition dont la réalisation était imposée par la loi, le rattachement au SDIS d'un centre d'incendie et de secours communal ou intercommunal, à ce que le SDIS, pour l'année de ce rattachement, intègre dans les contributions qu'il demande, au-delà du montant ainsi plafonné et au titre des charges transférées par ce rattachement volontaire, une majoration de la contribution mise à la charge de la commune ou de l'EPCI concerné, l'évolution du total des contributions ainsi majorées étant ensuite soumise, pour les années ultérieures, au plafonnement déterminé par application de l'indice des prix à la consommation.<br/>
<br/>
              7. Il résulte de ce qui précède que c'est sans erreur de droit que la cour administrative d'appel a jugé, d'une part, que les dispositions de l'article L. 1424-35 du code général des collectivités territoriales ne faisaient pas obstacle à ce que le SDIS de Tarn-et-Garonne revalorise la contribution globale demandée à la commune de Montauban à hauteur de l'évolution de l'indice des prix à la consommation, y compris pour la part de cette contribution intitulée " dotation annuelle de transfert " par la convention du 21 janvier 2001, que les transferts en cause résultent des obligations posées par les articles L. 1424-13, L. 1424-14 et L. 1424-17 de ce code ou, le cas échéant, de transferts volontaires réalisés sur le fondement de ses articles L. 1424-15 et L. 1424-19 et, d'autre part, que la commune de Montauban ne pouvait se prévaloir, pour contester cette revalorisation, des termes de cette convention prévoyant un montant fixe pour cette " dotation de transfert " à compter de l'année 2007.<br/>
<br/>
              8. Il résulte de ce qui précède que les conclusions du pourvoi de la commune de Montauban doivent être rejetées.<br/>
<br/>
              9. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge du SDIS de Tarn-et-Garonne, qui n'est pas la partie perdante dans la présente instance, la somme que demande à ce titre la commune de Montauban. Au titre de ces mêmes dispositions, il y a lieu en revanche, dans les circonstances de l'espèce, de mettre à la charge de la commune de Montauban la somme de 3 000 euros à verser au SDIS de Tarn-et-Garonne. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la commune de Montauban est rejeté, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 2 : La commune de Montauban versera au service départemental d'incendie et de secours de Tarn-et-Garonne la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : La présente décision sera notifiée à la commune de Montauban et au service départemental d'incendie et de secours de Tarn-et-Garonne.<br/>
Copie en sera adressée au département de Tarn-et-Garonne et au préfet de Tarn-et-Garonne.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-01-04-02-03 COLLECTIVITÉS TERRITORIALES. DISPOSITIONS GÉNÉRALES. SERVICES PUBLICS LOCAUX. DISPOSITIONS PARTICULIÈRES. SERVICES D'INCENDIE ET SECOURS. - FINANCEMENT DES SDIS - PLAFONNEMENT DE L'ÉVOLUTION DES CONTRIBUTIONS DES COMMUNES ET EPCI AU BUDGET DU SDIS AU TITRE DES TRANSFERTS DE PERSONNEL IMPOSÉS PAR LA LOI - POSSIBILITÉ, MALGRÉ CE PLAFONNEMENT, DE MAJORER LA CONTRIBUTION MISE À LA CHARGE D'UNE COMMUNE OU D'UN EPCI EN CONSÉQUENCE D'UN TRANSFERT VOLONTAIRE - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">135-02-04-02 COLLECTIVITÉS TERRITORIALES. COMMUNE. FINANCES COMMUNALES. DÉPENSES. - FINANCEMENT DES SDIS - PLAFONNEMENT DE L'ÉVOLUTION DES CONTRIBUTIONS DES COMMUNES ET EPCI AU BUDGET DU SDIS AU TITRE DES TRANSFERTS DE PERSONNEL IMPOSÉS PAR LA LOI - POSSIBILITÉ, MALGRÉ CE PLAFONNEMENT, DE MAJORER LA CONTRIBUTION MISE À LA CHARGE D'UNE COMMUNE OU D'UN EPCI EN CONSÉQUENCE D'UN TRANSFERT VOLONTAIRE - EXISTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">135-05-01-01 COLLECTIVITÉS TERRITORIALES. COOPÉRATION. ÉTABLISSEMENTS PUBLICS DE COOPÉRATION INTERCOMMUNALE - QUESTIONS GÉNÉRALES. DISPOSITIONS GÉNÉRALES ET QUESTIONS COMMUNES. - FINANCEMENT DES SDIS - PLAFONNEMENT DE L'ÉVOLUTION DES CONTRIBUTIONS DES COMMUNES ET EPCI AU BUDGET DU SDIS AU TITRE DES TRANSFERTS DE PERSONNEL IMPOSÉS PAR LA LOI - POSSIBILITÉ, MALGRÉ CE PLAFONNEMENT, DE MAJORER LA CONTRIBUTION MISE À LA CHARGE D'UNE COMMUNE OU D'UN EPCI EN CONSÉQUENCE D'UN TRANSFERT VOLONTAIRE - EXISTENCE.
</SCT>
<ANA ID="9A"> 135-01-04-02-03 Il résulte du huitième alinéa de l'article L. 1424-35 du code général des collectivités territoriales (CGCT), éclairé par les travaux préparatoires de la loi n° 2004-811 du 13 août 2004, que le plafonnement de l'évolution annuelle du montant global des contributions des communes et des établissements publics de coopération intercommunale (EPCI) au budget du service départemental d'incendie et de secours (SDIS), sous réserve de l'application de l'indice des prix à la consommation, ne fait pas obstacle, lorsqu'une commune ou un EPCI sollicite, au-delà des transferts et mises à disposition dont la réalisation était imposée par la loi, le rattachement au SDIS d'un centre d'incendie et de secours communal ou intercommunal, à ce que le SDIS, pour l'année de ce rattachement, intègre dans les contributions qu'il demande, au-delà du montant ainsi plafonné et au titre des charges transférées par ce rattachement volontaire, une majoration de la contribution mise à la charge de la commune ou de l'EPCI concerné, l'évolution du total des contributions ainsi majorées étant ensuite soumise, pour les années ultérieures, au plafonnement déterminé par application de l'indice des prix à la consommation.</ANA>
<ANA ID="9B"> 135-02-04-02 Il résulte du huitième alinéa de l'article L. 1424-35 du code général des collectivités territoriales (CGCT), éclairé par les travaux préparatoires de la loi n° 2004-811 du 13 août 2004, que le plafonnement de l'évolution annuelle du montant global des contributions des communes et des établissements publics de coopération intercommunale (EPCI) au budget du service départemental d'incendie et de secours (SDIS), sous réserve de l'application de l'indice des prix à la consommation, ne fait pas obstacle, lorsqu'une commune ou un EPCI sollicite, au-delà des transferts et mises à disposition dont la réalisation était imposée par la loi, le rattachement au SDIS d'un centre d'incendie et de secours communal ou intercommunal, à ce que le SDIS, pour l'année de ce rattachement, intègre dans les contributions qu'il demande, au-delà du montant ainsi plafonné et au titre des charges transférées par ce rattachement volontaire, une majoration de la contribution mise à la charge de la commune ou de l'EPCI concerné, l'évolution du total des contributions ainsi majorées étant ensuite soumise, pour les années ultérieures, au plafonnement déterminé par application de l'indice des prix à la consommation.</ANA>
<ANA ID="9C"> 135-05-01-01 Il résulte du huitième alinéa de l'article L. 1424-35 du code général des collectivités territoriales (CGCT), éclairé par les travaux préparatoires de la loi n° 2004-811 du 13 août 2004, que le plafonnement de l'évolution annuelle du montant global des contributions des communes et des établissements publics de coopération intercommunale (EPCI) au budget du service départemental d'incendie et de secours (SDIS), sous réserve de l'application de l'indice des prix à la consommation, ne fait pas obstacle, lorsqu'une commune ou un EPCI sollicite, au-delà des transferts et mises à disposition dont la réalisation était imposée par la loi, le rattachement au SDIS d'un centre d'incendie et de secours communal ou intercommunal, à ce que le SDIS, pour l'année de ce rattachement, intègre dans les contributions qu'il demande, au-delà du montant ainsi plafonné et au titre des charges transférées par ce rattachement volontaire, une majoration de la contribution mise à la charge de la commune ou de l'EPCI concerné, l'évolution du total des contributions ainsi majorées étant ensuite soumise, pour les années ultérieures, au plafonnement déterminé par application de l'indice des prix à la consommation.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
