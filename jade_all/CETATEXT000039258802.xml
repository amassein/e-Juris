<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039258802</ID>
<ANCIEN_ID>JG_L_2019_09_000000421064</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/25/88/CETATEXT000039258802.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 20/09/2019, 421064, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-09-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421064</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Olivier Yeznikian</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:421064.20190920</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La Société moderne d'assainissement et de nettoyage (SMA) a demandé au tribunal administratif de Toulon d'annuler le titre n° 2014-120 émis à son encontre et rendu exécutoire le 10 février 2014 par le Syndicat mixte du développement durable de l'Est-Var pour le traitement et la valorisation des déchets ménagers (SMIDDEV) pour un montant de 638 007,62 euros et de la décharger de l'obligation de payer cette somme. Par un jugement n° 1402073 du 22 avril 2016, le tribunal administratif de Toulon a fait droit à ces demandes.<br/>
<br/>
              Par un arrêt n° 16MA02521 du 30 mars 2018, la cour administrative d'appel de Marseille, sur appel du SMIDDEV, a réformé ce jugement en annulant le titre exécutoire en litige en tant seulement qu'il avait mis à la charge de la société SMA une somme supérieure à 569 575,42 euros et en déchargeant la société Valéor, venant aux droits de la société SMA, de l'obligation de payer la somme de 68 432,10 euros, puis a rejeté le surplus des conclusions de la demande de cette société.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 30 mai et 30 août 2018 au secrétariat du contentieux du Conseil d'Etat, la société Valéor demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel du SMIDDEV ;<br/>
<br/>
              3°) de mettre à la charge du SMIDDEV la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code des marchés publics ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Yeznikian, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la société Valéor et à la SCP Rocheteau, Uzan-Sarano, avocat de la société SMIDDEV ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que l'exploitation du site n° 3 du centre d'enfouissement des déchets non dangereux, dit "des Lauriers", situé sur le territoire de la commune de Bagnols-en-Forêt, a été confiée par le syndicat intercommunal pour le traitement des ordures ménagères (SITOM) de l'aire de Fréjus-Saint-Raphaël, devenu le syndicat mixte du développement durable de l'Est-Var pour le traitement et la valorisation des déchets ménagers (SMIDDEV), à la société moderne d'assainissement et de nettoyage (SMA), aux droits de laquelle vient désormais la société Valéor, par une convention de délégation de service public conclue le 31 décembre 2002 pour une durée initiale de 6 ans jusqu'au 31 décembre 2008 et prolongée par avenants. Au terme de la délégation et à la suite d'un audit financier, le SMIDDEV a réclamé au délégataire le remboursement de diverses sommes au titre de trop-perçus. La société a contesté le titre de recettes n° 2014-120 émis à son encontre et rendu exécutoire le 10 février 2014 d'un montant de 638 007,62 euros correspondant à un trop-perçu au titre de l'exercice 2005. Par un jugement du 22 avril 2016, le tribunal administratif de Toulon a annulé le titre en litige et a déchargé la société des sommes à payer. Par un arrêt du 30 mars 2018, la cour administrative d'appel de Marseille a, sur appel du SMIDDEV, réformé ce jugement, annulé ce titre en tant qu'il avait mis à la charge du délégataire une somme supérieure à 569 575,42 euros, déchargé la société Valéor de l'obligation de payer la somme de 68 432,10 euros et rejeté le surplus des conclusions de cette société. Son pourvoi doit être regardé comme tendant à l'annulation de l'arrêt en tant qu'il a rejeté ces dernières conclusions.<br/>
<br/>
<br/>
<br/>
<br/>
              Sur les conclusions du pourvoi relatives à la décharge d'une somme de 20 000 euros :<br/>
<br/>
              2. Par un mémoire, enregistré le 26 juin 2019, le SMIDDEV a déclaré renoncer au bénéfice de l'arrêt attaqué en tant qu'il a laissé à tort une somme de 20 000 euros à la charge de la société Valéor. Dès lors, il n'y a plus lieu de statuer sur les conclusions du pourvoi dirigées contre l'arrêt attaqué en tant qu'il a omis de décharger la société d'une somme supplémentaire de 20 000 euros et d'annuler le titre exécutoire en litige dans cette mesure.<br/>
<br/>
              Sur les autres conclusions du pourvoi :<br/>
<br/>
              En ce qui concerne le moyen relatif aux stipulations de l'article 12 de la convention de délégation de service public :<br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond que le I de l'article 12 de la convention de délégation de service public en litige, relatif aux " conditions financières et modalités de paiement ", prévoit, d'une part, que le délégataire est rémunéré par le syndicat pour les déchets apportés par les communes membres et par ses clients et, d'autre part, que les prix unitaires pratiqués sont déterminés sur la base d'un compte prévisionnel d'exploitation annexé à la convention. Le II de cet article stipule que : " Le délégataire est astreint à remettre annuellement au Syndicat dans son compte-rendu annuel un compte-rendu financier qui devra être établi sur la base et selon le modèle du compte prévisionnel d'exploitation annexé à la présente [convention]. / Si les contrôles exercés sur les postes de ce compte d'exploitation font ressortir qu'en réalité les coûts figurant dans le compte prévisionnel d'exploitation sont supérieurs à ceux réellement exposés dans une proportion d'au moins 10%, les tarifs mentionnés au paragraphe I du présent article seront automatiquement revus à la baisse dans les mêmes proportions avec effet pour l'année suivant celle pour laquelle le contrôle du compte d'exploitation sera intervenu. [...] ". <br/>
<br/>
              4. En premier lieu, la cour a jugé que si le délégataire était tenu de produire annuellement un compte-rendu financier détaillant notamment les charges d'exploitation effectivement exposées qui devait être soumis à l'assemblée délibérante du SMIDDEV, l'absence d'observations à l'occasion de l'examen annuel par l'assemblée délibérante ayant suivi la remise du compte-rendu financier ne pouvait être regardée comme une renonciation du délégant à tout contrôle ultérieur du compte-rendu de l'exercice en cause et à la mise en oeuvre de la révision tarifaire selon les modalités prévues par les stipulations du II de l'article 12 de la convention. En statuant ainsi, la cour n'a pas dénaturé les pièces du dossier qui lui était soumis et, en tout état de cause, n'a pas méconnu les principes de bonne foi et de loyauté contractuelle.<br/>
<br/>
              5. En deuxième lieu, en jugeant qu'il ne résultait d'aucune stipulation de la convention en litige que les frais financiers liés aux investissements réalisés par la société Valéor en application de cette convention devaient faire l'objet d'une répartition sur la totalité de la durée de l'exploitation, la cour s'est livrée à une interprétation de ces stipulations exempte de dénaturation.<br/>
<br/>
              6. En dernier lieu, en retenant que le taux applicable à la révision tarifaire pour l'année suivant celle au titre de laquelle le contrôle du compte-rendu financier produit par le délégataire fait ressortir que ses coûts d'exploitation sont inférieurs de plus de 10 % aux coûts figurant dans le compte prévisionnel annexé à la convention est égal à celui de la disproportion constatée à l'occasion de ce contrôle et non, comme le soutient la société Valéor, à la fraction de ce taux excédant 10 % du coût prévisionnel d'exploitation, la cour n'a pas dénaturé les stipulations de l'article 12 de la convention qui lui étaient soumises.<br/>
<br/>
              En ce qui concerne le moyen relatif à l'article 15 de la convention de délégation de service public : <br/>
<br/>
              7. D'une part, lorsque les parties soumettent au juge un litige relatif à l'exécution du contrat qui les lie, il incombe en principe à celui-ci, eu égard à l'exigence de loyauté des relations contractuelles, de faire application du contrat. Toutefois, dans le cas seulement où il constate une irrégularité invoquée par une partie ou relevée d'office par lui, tenant au caractère illicite du contenu du contrat ou à un vice d'une particulière gravité relatif notamment aux conditions dans lesquelles les parties ont donné leur consentement, il doit écarter le contrat et ne peut régler le litige sur le terrain contractuel.<br/>
<br/>
              8. D'autre part, une collectivité publique est irrecevable à demander au juge administratif de prononcer une mesure qu'elle a le pouvoir de prendre. En particulier, les collectivités territoriales, qui peuvent émettre des titres exécutoires à l'encontre de leurs débiteurs, ne peuvent saisir directement le juge administratif d'une demande tendant au recouvrement de leur créance. Toutefois, lorsque la créance trouve son origine dans un contrat, la faculté d'émettre un titre exécutoire dont dispose une personne publique ne fait pas obstacle à ce qu'elle saisisse le juge administratif d'une demande tendant à son recouvrement, notamment dans le cadre d'un référé-provision engagé sur le fondement de l'article R. 541-1 du code de justice administrative. <br/>
<br/>
              9. Si une personne publique peut s'engager, par une convention, à ce que son pouvoir d'émettre un titre exécutoire à l'encontre de son cocontractant débiteur ne soit le cas échéant exercé qu'après qu'aura été mise en oeuvre une procédure de conciliation, elle ne peut renoncer contractuellement ni à ce pouvoir ni à sa faculté de saisir le juge administratif dans les conditions rappelées au point précédent.<br/>
<br/>
              10. La cour a estimé, par une interprétation souveraine non arguée de dénaturation, qu'en application de l'article 15 de la convention de délégation de service public en litige, relatif au règlement amiable des litiges, les parties devaient soumettre leurs différends à une commission constituée par voie amiable et étaient ensuite tenues, en cas d'échec de cette conciliation, de porter le litige devant le tribunal administratif compétent. La cour en a déduit que le pouvoir adjudicateur devait être regardé comme ayant renoncé à l'exercice du pouvoir d'émettre un titre exécutoire pour le recouvrement de ses créances en cas d'échec de la procédure de règlement amiable des litiges. En écartant comme illicites ces stipulations, compte tenu de l'interprétation qu'elle a cru pouvoir en donner, la cour n'a pas commis d'erreur de droit.<br/>
<br/>
              11. Il résulte de tout ce qui précède que le surplus des conclusions du pourvoi de la société Valéor doit être rejeté.<br/>
<br/>
<br/>
<br/>
              Sur les frais du litige :<br/>
<br/>
              12. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Valéor une somme de 3 000 euros à verser au SMIDDEV au titre de l'article L. 761-1 du code de justice administrative. En revanche, ces mêmes dispositions font obstacle à ce qu'une somme soit mise à la charge de ce syndicat, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de statuer sur les conclusions du pourvoi dirigées contre l'arrêt de la cour administrative d'appel de Marseille en tant qu'il a omis de décharger la société d'une somme supplémentaire de 20 000 euros et d'annuler le titre exécutoire en litige dans cette mesure.<br/>
Article 2 : Le surplus des conclusions du pourvoi de la société Valéor est rejeté.<br/>
Article 3 : La société Valéor versera au SMIDDEV la somme de 3 000 euros au titre l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la société Valéor et au Syndicat mixte du développement durable de l'Est-Var pour le traitement et la valorisation des déchets ménagers.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
