<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035831276</ID>
<ANCIEN_ID>JG_L_2017_10_000000404065</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/83/12/CETATEXT000035831276.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 18/10/2017, 404065, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-10-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>404065</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO ; SCP OHL, VEXLIARD</AVOCATS>
<RAPPORTEUR>M. Florian Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:404065.20171018</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Procédure contentieuse antérieure<br/>
<br/>
              M. D...B...a demandé au tribunal administratif de Strasbourg de condamner les Hôpitaux universitaires de Strasbourg à l'indemniser des conséquences dommageables d'un retard dans le diagnostic d'une entorse scapho-lunaire consécutive à un accident du travail survenu le 29 juin 2005. Par un jugement n° 0800060 du 26 juillet 2012, le tribunal a condamné les Hôpitaux universitaires de Strasbourg à verser à M. B...la somme de 28 619,90 euros et à la caisse d'assurance-accidents agricole du Bas-Rhin la somme de 33 226,73 euros ainsi que 67 % du montant des arrérages à échoir de la rente d'accident du travail à compter du 30 juin 2011. <br/>
<br/>
              Par un arrêt n° 12NC01630 du 14 novembre 2013, la cour administrative d'appel de Nancy a, sur appel des Hôpitaux universitaires de Strasbourg et sur appel incident de M. B... et de la caisse d'assurance-accidents agricole du Bas-Rhin, fixé ces sommes respectivement à 24 702,83 euros et 40 502,76 euros et condamné l'établissement à rembourser à la caisse 67 % des arrérages de la rente d'accident du travail à compter du 1er avril 2013.<br/>
<br/>
              Par une décision n° 374628 du 23 décembre 2015, le Conseil d'Etat a annulé l'arrêt du 14 novembre 2013 de la cour administrative d'appel de Nancy en tant qu'il statuait sur la réparation de la perte de chance d'éviter les préjudices professionnels et renvoyé l'affaire à la cour dans la limite de la cassation prononcée.<br/>
<br/>
              Par un arrêt n° 15NC02572 du 5 août 2016, la cour administrative d'appel de Nancy a condamné les Hôpitaux universitaires de Strasbourg à verser, au titre des préjudices professionnels, à M. B...la somme de  12 757,20 euros et à la caisse d'assurance-accidents agricole du Bas-Rhin à la somme de 44 892,80 euros et réformé dans cette mesure le jugement du tribunal administratif de Strasbourg.<br/>
<br/>
              Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 5 octobre 2016 et 5 janvier 2017 au secrétariat du contentieux du Conseil d'Etat, les Hôpitaux universitaires de Strasbourg demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de ne les condamner à verser aucune somme à la succession de M.B..., décédé, et de ramener à 27 808,69 euros la somme due à la caisse d'assurance-accidents agricole du Bas-Rhin au titre du préjudice professionnel.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le code de la sécurité sociale ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Le Prado, avocat des Hôpitaux universitaires de Strasbourg et à la SCP Ohl, Vexliard, avocat de la caisse d'assurance accidents-agricole du Bas-Rhin, de M. C... B...et de Mme E...A...Veuve B...en leur qualité d'héritiers de M. D...B....<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. D... B...a été victime le 29 juin 2005 d'un accident de travail et a bénéficié à ce titre de prestations versées par la caisse d'assurance-accidents agricole du Bas-Rhin ; qu'il a recherché la responsabilité des Hôpitaux universitaires de Strasbourg au titre du retard de diagnostic et de prise en charge d'une entorse scapho-lunaire provoquée par cet accident ; que, par un arrêt du 14 novembre 2013, la cour administrative d'appel de Nancy, statuant sur l'appel des Hôpitaux universitaires de Strasbourg et sur l'appel incident de M. B...et de la caisse d'assurance-accidents agricole du Bas-Rhin contre un jugement du 26 juillet 2012 du tribunal administratif de Strasbourg, a retenu l'existence d'un retard fautif de diagnostic ayant entraîné pour l'intéressé une perte de chance évaluée à 67 % d'échapper aux séquelles qu'il avait conservées et condamné les Hôpitaux universitaires de Strasbourg à verser à M. B... la somme de 24 702,83 euros, dont 7 952,83 euros au titre des préjudices professionnels, et à la caisse d'assurance-accidents agricole du Bas-Rhin la somme de 40 502,76 euros, dont 39 916,33 euros au titre des préjudices professionnels, ainsi que 67 % des arrérages à échoir de la rente d'accident du travail de l'intéressé, au titre de ces mêmes préjudices ; que par une décision rendue le 23 décembre 2015 sur le pourvoi des Hôpitaux universitaires de Strasbourg, le Conseil d'Etat statuant au contentieux a annulé cet arrêt en tant qu'il statuait sur la réparation des préjudices professionnels de M. B...et renvoyé l'affaire dans cette mesure à la cour administrative d'appel de Nancy ; que, par un arrêt du 5 août 2016 contre lequel les Hôpitaux universitaires de Strasbourg se pourvoient en cassation, la cour a fixé à 12 757,20 euros et 44 892,80 euros les sommes dues par ces hôpitaux respectivement à M. B... et à la caisse d'assurance-accidents agricole du Bas-Rhin au titre des préjudices professionnels ;<br/>
<br/>
              2. Considérant, d'une part, qu'en application des dispositions de l'article L. 376-1 du code de la sécurité sociale, dans sa rédaction résultant de la loi du 21 décembre 2006 relative au financement de la sécurité sociale, le juge saisi d'un recours de la victime d'un dommage corporel et du recours subrogatoire d'un organisme de sécurité sociale doit, pour chacun des postes de préjudice, déterminer le montant du préjudice en précisant la part qui a été réparée par des prestations de sécurité sociale et celle qui est demeurée à la charge de la victime ; qu'il lui appartient ensuite de fixer l'indemnité mise à la charge de l'auteur du dommage au titre du poste de préjudice en tenant compte, s'il a été décidé, du partage de responsabilité avec la victime ou du fait que celle-ci n'a subi que la perte d'une chance d'éviter le dommage corporel ; que le juge doit allouer cette indemnité à la victime dans la limite de la part du poste de préjudice qui n'a pas été réparée par des prestations, le solde, s'il existe, étant alloué à l'organisme de sécurité sociale ;<br/>
<br/>
              3. Considérant, d'autre part, que la rente d'accident du travail prévue par l'article L. 431-1 du code de la sécurité sociale, rendu applicable aux salariés des professions agricoles par l'article L. 751-8 du code rural et de la pêche maritime, et la rente d'accident du travail servie aux non-salariés agricoles sur le fondement de l'article L. 752-6 de ce code doivent, en raison de la finalité de réparation d'une incapacité permanente de travail qui leur est assignée par les dispositions qui les instituent et de leur mode de calcul, appliquant le taux d'incapacité permanente au salaire de référence défini par l'article L. 434-2 du code de la sécurité sociale ou au gain forfaitaire annuel mentionné à l'article L. 752-5 du code rural et de la pêche maritime, être regardées comme ayant pour objet exclusif de réparer, sur une base forfaitaire, les préjudices subis par la victime dans sa vie professionnelle en conséquence de l'accident, c'est-à-dire ses pertes de revenus professionnels et l'incidence professionnelle de son incapacité ; que, dès lors, le recours exercé par une caisse de sécurité sociale au titre d'une telle rente ne saurait s'exercer que sur ces deux postes de préjudice ;<br/>
<br/>
              4. Considérant que, pour se conformer aux règles rappelées ci-dessus, la cour devait, pour la période d'incapacité temporaire, évaluer les pertes de gains professionnels avant compensation par les indemnités journalières, mettre 67 % du montant de ces pertes à la charge des Hôpitaux universitaires de Strasbourg, accorder à M. B... une indemnité couvrant les pertes non compensées par les indemnités journalières et accorder le solde à la caisse au titre de ces indemnités ; que, pour la période postérieure à la consolidation, la cour devait évaluer les pertes de gains professionnels et l'incidence professionnelle avant compensation par la rente d'accident du travail, mettre 67 % du montant de ces postes de préjudice à la charge des Hôpitaux universitaires de Strasbourg, accorder à M. B...des indemnités couvrant la part des postes non couverte par des prestations et accorder le solde à la caisse au titre de la rente d'accident du travail ;<br/>
<br/>
              5. Considérant que, pour évaluer la perte de revenus professionnels subie par M. B... au titre de son activité de paysagiste durant sa période d'incapacité temporaire, la cour a jugé qu'il résultait de l'instruction que la caisse d'assurance-accidents agricole du Bas-Rhin avait versé des indemnités journalières pour un montant total de 38 329,84 euros qui avaient remplacé son salaire, l'intéressé ne subissant aucune perte nette de rémunération ; qu'il ressort des pièces du dossier soumis aux juges du fond que les Hôpitaux universitaires de Strasbourg indiquaient eux-mêmes, dans le dernier état de leurs écritures d'appel, que M. B... avait subi une perte brute de revenus professionnels de 38 329,84 euros ; qu'ainsi, en évaluant la perte de revenus professionnels subie par M. B...avant compensation par des prestations sociales à un montant coïncidant avec celui de ses indemnités journalières, la cour s'est livrée à une appréciation souveraine des faits et pièces du dossier exempte de dénaturation et n'a pas commis d'erreur de droit ;<br/>
<br/>
              6. Considérant que, pour la période postérieure à la consolidation de l'état de M. B..., l'arrêt attaqué évalue le montant total du préjudice de perte de revenus imputables aux Hôpitaux universitaires de Strasbourg à la somme des vacations non perçues de sapeur pompier volontaire et de la rente d'accident du travail que la caisse d'assurance-accidents agricole du Bas-Rhin lui a versée du 1er novembre 2008 à la fin du mois de février 2016 ; qu'en présumant ainsi l'existence d'un préjudice professionnel de la victime dont le montant aurait coïncidé avec celui de la rente d'invalidité, alors qu'il lui appartenait de vérifier l'existence d'un tel préjudice et d'en déterminer le montant en fonction des revenus professionnels non perçus du fait de l'accident et de l'incidence professionnelle de celui-ci, la cour n'a pas mis en oeuvre les principes rappelés aux points 2 à 4 ci-dessus et a entaché son arrêt d'erreur de droit ;<br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que l'arrêt attaqué doit être annulé en tant seulement qu'il statue sur l'indemnisation des pertes de revenus professionnels postérieures au 31 octobre 2008 et met à ce titre à la charge des Hôpitaux universitaire de Strasbourg le versement à M. B...de la somme de 7 953 euros et le versement à la caisse d'assurance-accidents agricole du Bas-Rhin de la somme de 19 212 euros ;<br/>
<br/>
              8. Considérant qu'aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire " ; que, le Conseil d'Etat étant saisi, en l'espèce, d'un second pourvoi en cassation, il lui incombe de régler l'affaire au fond ;<br/>
<br/>
              9. Considérant qu'il résulte de l'instruction que M. B...a repris son activité de paysagiste à temps complet à compter du 31 octobre 2008 et n'a pas subi de perte de salaire à ce titre ; que l'existence d'un manque à gagner de 1,67 euros par mois au titre de droits à pension n'est pas établie ; que M. B...exerçait régulièrement une activité de sapeur pompier volontaire pour laquelle il percevait des vacations pour un montant annuel moyen de 1 695,70 euros ; qu'il comptait poursuivre cette activité jusqu'à l'âge de 55 ans, qu'il aurait atteint le 29 octobre 2013 ; que la perte de gains professionnels subie par M. B...postérieurement au 31 octobre 2008 s'élève donc à 8 478,50 euros ; que l'existence d'une incidence professionnelle ne résulte pas de l'instruction ; qu'eu égard à l'ampleur de la chance perdue du fait des fautes commises par les Hôpitaux universitaires de Strasbourg, il y a lieu de mettre à la charge de cet établissement 67 % de la somme de 8 478,50 euros, soit 5 680,60 euros ; que le préjudice de M. B... ayant été intégralement réparé par la rente d'invalidité, cette somme, augmentée des intérêts au taux légal à compter du 14 novembre 2013, devra être versée à la caisse d'assurance-accidents agricole du Bas-Rhin ;<br/>
<br/>
              10. Considérant qu'il résulte de ce qui précède que la demande présentée par la succession de M. B...au titre des pertes de revenus professionnels de M. B...postérieures au 31 octobre 2008 doit être rejetée et que les Hôpitaux universitaires de Strasbourg doivent être condamnés à verser à la caisse d'assurance accidents agricoles du Bas-Rhin, au titre des prestations destinées à compenser les pertes de revenus professionnels de M. B...postérieures au 31 octobre 2008, la somme de 5 680,60 euros, augmentée des intérêts légal à compter du 14 novembre 2013, sous déduction des sommes déjà versées à ce titre ;<br/>
<br/>
              11. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge des Hôpitaux universitaires de Strasbourg qui ne sont pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nancy du 5 août 2016 est annulé en tant qu'il statue sur l'indemnisation des pertes de revenus professionnels de M. B...postérieures au 31 octobre 2008 et met à ce titre à la charge des Hôpitaux universitaire de Strasbourg le versement à M. B...de la somme de 7 953 euros et le versement à la caisse d'assurance-accidents agricole du Bas-Rhin de la somme de 19 212 euros.<br/>
<br/>
Article 2 : La demande présentée par la succession de M. B...au titre des pertes de revenus professionnels de M. B...postérieures au 31 octobre 2008 est rejetée.<br/>
<br/>
Article 3 : Les Hôpitaux universitaires de Strasbourg sont condamnés à verser à la caisse d'assurance accidents agricoles du Bas-Rhin, au titre des prestations destinées à compenser les pertes de revenus professionnels de M. B...postérieures au 31 octobre 2008, la somme de 5 680,60 euros, augmentée des intérêts légal à compter du 14 novembre 2013, sous déduction des sommes déjà versées à ce titre.<br/>
<br/>
Article 4 : Le surplus des conclusions des Hôpitaux universitaires de Strasbourg est rejeté.<br/>
<br/>
Article 5 : La présente décision sera notifiée aux Hôpitaux universitaires de Strasbourg, à la caisse d'assurance accidents agricoles du Bas-Rhin, à Mme E...A..., veuve B...et à M. C...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
