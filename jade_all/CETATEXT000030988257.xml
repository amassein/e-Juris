<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030988257</ID>
<ANCIEN_ID>JG_L_2015_06_000000368671</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/98/82/CETATEXT000030988257.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème / 3ème SSR, 24/06/2015, 368671, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-06-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>368671</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème / 3ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LESOURD</AVOCATS>
<RAPPORTEUR>M. Jean-Marc Anton</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:368671.20150624</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société par actions simplifiée Holcim Investments France a demandé au tribunal administratif de Montreuil de la décharger des cotisations supplémentaires d'impôt sur les sociétés et de contributions additionnelles à cet impôt auxquelles elle a été assujettie au titre de ses exercices clos en 1997, 1998, 2002, 2003 et 2004, ainsi que des intérêts de retard correspondants. Par un jugement nos 0903975,1100614 du 30 juin 2011, ce tribunal a rejeté cette demande.<br/>
<br/>
              Par un arrêt n°11VE03316 du 29 janvier 2013, la cour administrative d'appel de Versailles a rejeté l'appel formé contre ce jugement du tribunal administratif de Montreuil par la société Holcim Investments France.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 21 mai et 20 août 2013 et le 12 décembre 2014 au secrétariat du contentieux du Conseil d'Etat, la société Holcim Investments France demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Anton, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lesourd, avocat de la SAS Holcim Investments France ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société Groupe Origny, aux droits de laquelle vient la société Holcim Investments France, a émis en 1992 des titres subordonnés à durée indéterminée rémunérés à taux variable ; qu'elle a versé, à la date à laquelle les titres ont été souscrits, un montant égal à 23 % du produit de l'émission à un trust domicilié..., l'intégralité des titres subordonnés ainsi émis et de les lui revendre pour un montant négligeable tout en renonçant aux droits et aux intérêts attachés à ces titres ; que, conformément au rescrit délivré à la société émettrice le 6 février 1992 et confirmé en 1993, la déductibilité des intérêts semestriels versés par elle aux porteurs de ces titres n'a été admise que pour les quinze premières années et pour une part égale à la part de l'émission demeurée à sa disposition après le versement mentionné plus haut, soit 77 % ; que, concomitamment à la signature des contrats et engagements nécessaires à l'émission des titres subordonnées à taux variable dont il s'agit ainsi qu'à l'organisation de leur rachat quinze ans plus tard, la société Groupe Origny a conclu avec un établissement financier appartenant au même groupe que celui qui a monté l'opération d'émission et en a assuré la prise ferme pour la totalité, un contrat d'échange de taux d'intérêt sur une durée de quinze ans, la plaçant dans la même situation que celle où elle aurait dû acquitter, pour une fraction chaque année croissante du montant de l'émission, un coupon à taux fixe en lieu et place de celui à taux variable perçu par les porteurs des titres ; que le taux fixe ainsi prévu au contrat d'échange de taux s'est révélé supérieur au taux variable correspondant à la rémunération des titres subordonnés ; qu'à la suite de vérifications de comptabilité portant sur les exercices clos pendant les années 1997, 1998 et 2002 à 2004, l'administration fiscale n'a accepté de regarder comme déductibles les charges financières issues de ce contrat d'échange de taux d'intérêt qu'à hauteur de la même proportion de 77 % que celle mentionnée plus haut ; <br/>
<br/>
              2. Considérant, en premier lieu, qu'il ressort des pièces du dossier soumis aux juges du fond que, contrairement à ce que soutient la société requérante et ainsi que l'a jugé la cour administrative d'appel de Versailles, si l'administration fiscale a adopté diverses formulations pour justifier sa position au cours de la procédure contentieuse, empruntant tantôt à la motivation des décisions de rescrit délivrées à la société émettrice tantôt aux conclusions qu'elle en tirait, elle n'a pas demandé, même implicitement, de substitution de motifs pour fonder les redressements litigieux ; que, par suite, le moyen tiré de ce que cette cour aurait dénaturé les pièces du dossier et commis une erreur de droit au regard des règles régissant la substitution de motifs en matière de contentieux fiscal ne peut qu'être écarté ;<br/>
<br/>
              3. Considérant, en deuxième lieu, que la cour a relevé que les redressements en litige étaient fondés non sur les termes mêmes des décisions de rescrit de 1992 et de 1993 mais sur " [le] même raisonnement et [les] mêmes règles que celui et celles rappelées dans [la] réponse [de l'administration] du 6 février 1992 relative au projet d'émission ", déduisant des caractéristiques propres du contrat d'échange de taux d'intérêt ainsi que du caractère indissociable des intérêts versés aux porteurs de titres et des écarts d'intérêts issus de ce contrat d'échange que ces intérêts et ces écarts, correspondant en l'espèce à des charges tout au long de la période de quinze ans en cause, relevaient des mêmes règles fiscales ; que la cour n'a ainsi nullement jugé que ces charges étaient régies par les règles fixées par ce rescrit ; que, dès lors, elle n'a pas commis d'erreur de droit au regard de l'article L. 80 B du livre des procédures fiscales, ni entaché son arrêt d'une contradiction de motifs en jugeant que l'administration n'avait pas illégalement fondé les redressements sur les énonciations de ces rescrits ;<br/>
<br/>
              4. Considérant, en troisième lieu, qu'aux termes du 1 de l'article 39 du code général des impôts : " Le bénéfice net est établi sous déduction de toutes charges (...) " ; que la fraction d'une émission qui n'a pas été inscrite au bilan de la société et dont la société émettrice décide de transférer le produit à une entité tierce pour lui en laisser la disposition ne saurait être regardée comme directement utile au financement de la société émettrice ; que, par suite, les intérêts payés par la société aux détenteurs des titres émis dans ces conditions ne sauraient, à hauteur de la fraction de son émission dont la société a abandonné la disposition, être regardés comme des charges exposées dans l'intérêt de l'entreprise déductibles du bénéfice net des exercices au cours desquels ils ont été acquittés ;<br/>
<br/>
              5. Considérant, d'une part, que la cour a jugé que, dès lors qu'une soulte avait été versée à un trust domicilié... ; qu'en jugeant en outre que cette soulte ne pouvait être elle-même regardée comme un intérêt déductible du résultat imposable, alors même qu'elle avait été qualifiée de " différentiel d'intérêts futurs " par la société et par l'administration fiscale lors de l'examen de ce montage, en raison de ce qu'elle était égale à la valeur actualisée en 1992 du flux d'intérêts auxquels le trust acheteur domicilié dans les Iles Caïman avec, en contrepartie de ce versement, l'engagement de ce trust de racheter, à l'issue d'une période de quinze ans et après avoir investi cette soulte en obligations à coupon zéro garanties par le royaume du Danemark;<br/>
<br/>
              6.  Considérant, d'autre part, que la cour a relevé que la justification donnée au contrat d'échange de taux en litige était de substituer au versement des intérêts à taux variable dus aux porteurs de titres subordonnés un versement à taux fixe et de se prémunir ainsi contre le risque de hausse des taux sur une fraction croissante du produit total de l'émission et, à compter de 2005, sur des montants légèrement supérieurs à ce produit ; qu'elle a également relevé par des motifs non contestés qu'une fraction de l'emprunt n'avait pas été inscrite au passif du bilan de la société ; qu'elle a exactement qualifié les faits qui lui étaient soumis en déduisant de ces constatations souveraines exemptes de dénaturation que les charges financières issues de ce contrat d'échange de taux étaient indissociablement liées aux intérêts d'emprunt payés par la société requérante aux porteurs des titres subordonnés émis en 1992, et que, dès lors, l'administration fiscale était fondée à n'admettre leur déductibilité que selon le même régime que celui applicable aux intérêts, soit en proportion de la fraction de l'émission demeurée à la disposition de la société émettrice après le versement de la soulte ; qu'elle n'a, par suite, pas commis d'erreur de droit en déduisant de cette qualification que l'administration fiscale était fondée à refuser la déduction  des intérêts relatifs à cette émission en proportion du montant de la soulte rapporté à celui du montant total de l'émission ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que le pourvoi de la société Holcim Investments France doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
                              D E C I D E :<br/>
                              --------------<br/>
<br/>
Article 1er : Le pourvoi de la société Holcim Investments France est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la société par actions simplifiée Holcim Investments France et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
