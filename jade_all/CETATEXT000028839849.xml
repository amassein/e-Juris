<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028839849</ID>
<ANCIEN_ID>JG_L_2014_04_000000366483</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/83/98/CETATEXT000028839849.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème SSR, 09/04/2014, 366483, Publié au recueil Lebon</TITRE>
<DATE_DEC>2014-04-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>366483</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; FOUSSARD</AVOCATS>
<RAPPORTEUR>M. Jean-Marc Vié</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2014:366483.20140409</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi et le mémoire complémentaire, enregistrés les 28 février et 29 avril 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour l'établissement public du domaine national de Chambord, dont le siège est Maison des réfractaires à Chambord (41250) ; il demande au Conseil d'Etat : <br/>
<br/>
               1°) d'annuler l'arrêt n° 12NT00752 du 28 décembre 2012 par lequel la cour administrative d'appel de Nantes, statuant sur la requête de la commune de Chambord, a annulé les articles 2 et 3 du jugement n° 1102178-1103342 du 6 mars 2012 par lequel le tribunal administratif d'Orléans, faisant droit aux demandes de l'établissement public et du préfet du Loir-et-Cher, a annulé l'arrêté du 23 mai 2011 par lequel le maire de la commune de Chambord a délivré un permis de stationnement autorisant M. A...B...à installer une terrasse au droit de son commerce sur la place Saint-Louis à Chambord ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la commune de Chambord ; <br/>
<br/>
              3°) de mettre à la charge de la commune de Chambord la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative ainsi que la contribution pour l'aide juridique prévue à l'article R. 761-1 du même code ; <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 26 mars 2014, présentée pour l'établissement public du domaine national de Chambord ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 27 mars 2014, présentée pour la commune de Chambord ;<br/>
<br/>
              Vu le code général des collectivités territoriales ; <br/>
<br/>
              Vu la loi n° 2005-157 du 23 février 2005, notamment son article 230 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Vié, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Foussard, avocat du Domaine national de Chambord et à la SCP Waquet, Farge, Hazan, avocat de la commune de Chambord ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant, en premier lieu, que la minute de l'arrêt attaqué a été signée par le président de la formation de jugement, le rapporteur et le greffier d'audience, conformément aux prescriptions de l'article R. 741-7 du code de justice administrative ; que, par suite, le moyen tiré de la méconnaissance de ces prescriptions manque en fait ;<br/>
<br/>
              2. Considérant, en deuxième lieu et d'une part, qu'aux termes du III de l'article 230 de la loi du 23 février 2005 relative au développement des territoires ruraux : " Les biens constitutifs du domaine national de Chambord sont remis en dotation à l'établissement public. Les affectations et les attributions à titre de dotation sont effectuées à titre gratuit. / Les voies du domaine national de Chambord ouvertes à la circulation publique à la date d'entrée en vigueur du décret prévu au VIII du présent article sont également remises en dotation à l'établissement public à titre gratuit. Le directeur général de l'établissement public exerce les pouvoirs de police afférents à leur gestion, notamment en ce qui concerne la circulation sur ces voies, sous réserve des pouvoirs dévolus au maire de la commune de Chambord sur les voies de communication situées à l'intérieur de l'agglomération en application de l'article L. 2213-1 du code général des collectivités territoriales (...) " ; <br/>
<br/>
              3. Considérant, d'autre part, qu'en vertu de l'article L. 2213-1 du code général des collectivités territoriales : " Le maire exerce la police de la circulation sur les routes nationales, les routes départementales et les voies de communication à l'intérieur des agglomérations, sous réserve des pouvoirs dévolus au représentant de l'Etat dans le département sur les routes à grande circulation (...) " ; que selon l'article L. 2213-6 du même code : " Le maire peut, moyennant le paiement de droits fixés par un tarif dûment établi, donner des permis de stationnement ou de dépôt temporaire sur la voie publique et autres lieux publics, sous réserve que cette autorisation n'entraîne aucune gêne pour la circulation et la liberté du commerce. " ; <br/>
<br/>
              4. Considérant que l'occupation d'une dépendance du domaine public fait l'objet, lorsqu'elle donne lieu à emprise, d'une permission de voirie délivrée par l'autorité responsable de la gestion du domaine et, dans les autres cas, d'un permis de stationnement ; que si la délivrance incombe en principe à ce même gestionnaire, c'est sous réserve de dispositions contraires ; qu'il résulte des dispositions des articles L. 2213-1 et L. 2213-6 du code général des collectivités territoriales qu'en sa qualité d'autorité compétente en matière de police de la circulation sur les voies de communication situées à l'intérieur des agglomérations, le maire est seul compétent pour délivrer des permis de stationnement sur ces mêmes voies et sur les autres lieux publics visés à l'article L. 2213-6 ;<br/>
<br/>
              5. Considérant que les dispositions de l'article 230 de la loi du 23 février 2005 ont pour objet de coordonner, sur les voies du  domaine  national de Chambord ouvertes à la circulation publique, les pouvoirs de police respectifs du maire de la commune et du directeur général de l'établissement public ; que, si elles confèrent à ce dernier le pouvoir de police afférent à la gestion de ces voies en y incluant celui de la circulation, elles réservent au maire la police de la circulation sur les voies de communication situées à l'intérieur de l'agglomération, dans les conditions de droit commun de l'article L. 2213-1 du code général des collectivités territoriales, auquel elles renvoient expressément et qui impliquent sa compétence pour délivrer, sur ces voies ainsi que sur les autres lieux publics qui en sont l'accessoire, des permis de stationnement en application de l'article L. 2213-6 du même code ; que, dès lors, la cour n'a pas commis d'erreur de droit en jugeant qu'en sa qualité d'autorité chargée de la police de la circulation, le maire est compétent pour y délivrer des permis de stationnement, alors même que ces voies font partie du domaine public de l'Etat, qu'elles ont été remises en dotation à l'établissement public et que celui-ci exerce les pouvoirs de police afférents à leur gestion ; que la cour ayant ainsi correctement interprété les dispositions de l'article 230 de la loi du 23 février 2005, le moyen tiré de ce qu'elle se serait à tort référée aux travaux préparatoires de cette loi en présence d'un texte clair est sans incidence sur le bien-fondé de son arrêt et ne peut qu'être écarté ; <br/>
<br/>
              6. Considérant, en troisième lieu, qu'une place piétonne ouverte à la circulation du public et située au sein d'une agglomération est au nombre des dépendances domaniales visées à l'article L. 2213-6 du code général des collectivités territoriales ; que la cour a relevé, par une appréciation des faits non arguée de dénaturation, que la place Saint-Louis est située dans l'agglomération de Chambord et qu'elle constitue une place piétonne ; qu'il ressort des pièces du dossier qui lui était soumis que le permis de stationnement contesté porte, selon les termes de l'arrêté du 23 mai 2011, sur le terre-plein de cette place, que cette dernière est ouverte à la circulation du public et qu'elle est empruntée par tous les visiteurs pour se rendre au château depuis les parcs de stationnement de véhicules ; que, par suite, la cour n'a pas commis d'erreur de droit en jugeant que le maire de Chambord était compétent pour délivrer à M. B...un permis de stationnement l'autorisant à installer une terrasse au droit de son commerce sur la place Saint-Louis ; <br/>
<br/>
              7. Considérant, enfin, que la cour a suffisamment motivé son arrêt en énonçant, après avoir jugé que le maire de Chambord était compétent pour délivrer le permis de stationnement en litige, que le détournement de pouvoir allégué n'était pas établi et que l'arrêté contesté n'était pas constitutif d'une voie de fait ;<br/>
<br/>
               8. Considérant qu'il résulte de ce qui précède que le pourvoi de l'établissement public du domaine national de Chambord doit être rejeté, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de l'établissement public la somme de 1 000 euros à verser à la commune de Chambord au titre de ces mêmes dispositions ; qu'enfin, il y a lieu de laisser à sa charge la contribution pour l'aide juridique prévue à l'article R. 761-1 du même code ;<br/>
<br/>
<br/>
<br/>                       D E C I D E :<br/>
                                       --------------<br/>
<br/>
 Article 1er : Le pourvoi de l'établissement public du domaine national de Chambord est rejeté.<br/>
<br/>
 Article 2 : L'établissement public du domaine national de Chambord versera à la commune de Chambord une somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
 Article 3 : La présente décision sera notifiée à l'établissement public du domaine national de Chambord et à la commune de Chambord. <br/>
 Copie en sera adressée, pour information, à la ministre de la culture et de la communication et à M. A...B.... <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-02-01-02-02-03 COLLECTIVITÉS TERRITORIALES. COMMUNE. ORGANISATION DE LA COMMUNE. ORGANES DE LA COMMUNE. MAIRE ET ADJOINTS. POUVOIRS DU MAIRE. - STATIONNEMENT - 1) AUTORITÉ COMPÉTENTE POUR DÉLIVRER UN PERMIS DE STATIONNEMENT [RJ1] - PRINCIPE - GESTIONNAIRE DU DOMAINE - EXCEPTION - DISPOSITIONS CONTRAIRES - INCLUSION - DISPOSITIONS DES ARTICLES L. 2213-1 ET L. 2213-6 DU CGCT - CONSÉQUENCES - COMPÉTENCE DU MAIRE POUR DÉLIVRER DES PERMIS DE STATIONNEMENT SUR LES VOIES DE COMMUNICATION SITUÉES À L'INTÉRIEUR DE L'AGGLOMÉRATION ET SUR LES AUTRES LIEUX PUBLICS VISÉS À L'ARTICLE L. 2213-6, DONT FONT PARTIE LES PLACES PIÉTONNES OUVERTES À LA CIRCULATION DU PUBLIC ET SITUÉES AU SEIN DE L'AGGLOMÉRATION - 2) ESPÈCE - DOMAINE NATIONAL DE CHAMBORD - COMPÉTENCE DU MAIRE POUR DÉLIVRER DES PERMIS DE STATIONNEMENT SUR LES VOIES DE CE DOMAINE OUVERTES À LA CIRCULATION PUBLIQUE AINSI QUE SUR LES AUTRES LIEUX PUBLICS QUI EN SONT L'ACCESSOIRE - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">135-02-03-02-04-02-04 COLLECTIVITÉS TERRITORIALES. COMMUNE. ATTRIBUTIONS. POLICE. POLICE DE LA CIRCULATION ET DU STATIONNEMENT. RÉGLEMENTATION DU STATIONNEMENT. PERMIS DE STATIONNEMENT. - 1) AUTORITÉ COMPÉTENTE POUR DÉLIVRER UN PERMIS DE STATIONNEMENT [RJ1] - PRINCIPE - GESTIONNAIRE DU DOMAINE - EXCEPTION - DISPOSITIONS CONTRAIRES - INCLUSION - DISPOSITIONS DES ARTICLES L. 2213-1 ET L. 2213-6 DU CGCT - CONSÉQUENCES - COMPÉTENCE DU MAIRE POUR DÉLIVRER DES PERMIS DE STATIONNEMENT SUR LES VOIES DE COMMUNICATION SITUÉES À L'INTÉRIEUR DE L'AGGLOMÉRATION ET SUR LES AUTRES LIEUX PUBLICS VISÉS À L'ARTICLE L. 2213-6, DONT FONT PARTIE LES PLACES PIÉTONNES OUVERTES À LA CIRCULATION DU PUBLIC ET SITUÉES AU SEIN DE L'AGGLOMÉRATION - 2) ESPÈCE - DOMAINE NATIONAL DE CHAMBORD - COMPÉTENCE DU MAIRE POUR DÉLIVRER DES PERMIS DE STATIONNEMENT SUR LES VOIES DE CE DOMAINE OUVERTES À LA CIRCULATION PUBLIQUE AINSI QUE SUR LES AUTRES LIEUX PUBLICS QUI EN SONT L'ACCESSOIRE - EXISTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">24-01-02-01-01 DOMAINE. DOMAINE PUBLIC. RÉGIME. OCCUPATION. UTILISATIONS PRIVATIVES DU DOMAINE. - 1) DISTINCTION ENTRE PERMISSION DE VOIRIE DÉLIVRÉE PAR L'AUTORITÉ GESTIONNAIRE DU DOMAINE ET PERMIS DE STATIONNEMENT - 2) AUTORITÉ COMPÉTENTE POUR DÉLIVRER UN PERMIS DE STATIONNEMENT [RJ1] - PRINCIPE - GESTIONNAIRE DU DOMAINE - EXCEPTION - DISPOSITIONS CONTRAIRES - INCLUSION - DISPOSITIONS DES ARTICLES L. 2213-1 ET L. 2213-6 DU CGCT - CONSÉQUENCES - COMPÉTENCE DU MAIRE POUR DÉLIVRER DES PERMIS DE STATIONNEMENT SUR LES VOIES DE COMMUNICATION SITUÉES À L'INTÉRIEUR DE L'AGGLOMÉRATION ET SUR LES AUTRES LIEUX PUBLICS VISÉS À L'ARTICLE L. 2213-6, DONT FONT PARTIE LES PLACES PIÉTONNES OUVERTES À LA CIRCULATION DU PUBLIC ET SITUÉES AU SEIN DE L'AGGLOMÉRATION - 3) ESPÈCE - DOMAINE NATIONAL DE CHAMBORD - COMPÉTENCE DU MAIRE POUR DÉLIVRER DES PERMIS DE STATIONNEMENT SUR LES VOIES DE CE DOMAINE OUVERTES À LA CIRCULATION PUBLIQUE AINSI QUE SUR LES AUTRES LIEUX PUBLICS QUI EN SONT L'ACCESSOIRE - EXISTENCE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">71-02-03 VOIRIE. RÉGIME JURIDIQUE DE LA VOIRIE. OCCUPATIONS PRIVATIVES DE LA VOIE PUBLIQUE. - 1) DISTINCTION ENTRE PERMISSION DE VOIRIE DÉLIVRÉE PAR L'AUTORITÉ GESTIONNAIRE DU DOMAINE ET PERMIS DE STATIONNEMENT - 2) AUTORITÉ COMPÉTENTE POUR DÉLIVRER UN PERMIS DE STATIONNEMENT [RJ1] - PRINCIPE - GESTIONNAIRE DU DOMAINE - EXCEPTION - DISPOSITIONS CONTRAIRES - INCLUSION - DISPOSITIONS DES ARTICLES L. 2213-1 ET L. 2213-6 DU CGCT - CONSÉQUENCES - COMPÉTENCE DU MAIRE POUR DÉLIVRER DES PERMIS DE STATIONNEMENT SUR LES VOIES DE COMMUNICATION SITUÉES À L'INTÉRIEUR DE L'AGGLOMÉRATION ET SUR LES AUTRES LIEUX PUBLICS VISÉS À L'ARTICLE L. 2213-6, DONT FONT PARTIE LES PLACES PIÉTONNES OUVERTES À LA CIRCULATION DU PUBLIC ET SITUÉES AU SEIN DE L'AGGLOMÉRATION - 3) ESPÈCE - DOMAINE NATIONAL DE CHAMBORD - COMPÉTENCE DU MAIRE POUR DÉLIVRER DES PERMIS DE STATIONNEMENT SUR LES VOIES DE CE DOMAINE OUVERTES À LA CIRCULATION PUBLIQUE AINSI QUE SUR LES AUTRES LIEUX PUBLICS QUI EN SONT L'ACCESSOIRE - EXISTENCE.
</SCT>
<ANA ID="9A"> 135-02-01-02-02-03 1) Si la délivrance d'un permis de stationnement incombe en principe au gestionnaire du domaine, c'est sous réserve de dispositions contraires. Il résulte des dispositions des articles L. 2213-1 et L. 2213-6 du code général des collectivités territoriales (CGCT) qu'en sa qualité d'autorité compétente en matière de police de la circulation sur les voies de communication situées à l'intérieur des agglomérations, le maire est seul compétent pour délivrer des permis de stationnement sur ces mêmes voies et sur les autres lieux publics visés à l'article L. 2213-6. Une place piétonne ouverte à la circulation du public et située au sein d'une agglomération est au nombre des dépendances domaniales visées à l'article L. 2213-6 du CGCT.... ,,2) Les dispositions de l'article 230 de la loi n° 2005-157 du 23 février 2005 ont pour objet de coordonner, sur les voies du domaine national de Chambord ouvertes à la circulation publique, les pouvoirs de police respectifs du maire de la commune et du directeur général de l'établissement public. Si elles confèrent à ce dernier le pouvoir de police afférent à la gestion de ces voies en y incluant celui de la circulation, elles réservent au maire la police de la circulation sur les voies de communication situées à l'intérieur de l'agglomération, dans les conditions de droit commun de l'article L. 2213-1 du CGCT, auquel elles renvoient expressément et qui impliquent sa compétence pour délivrer, sur ces voies ainsi que sur les autres lieux publics qui en sont l'accessoire, des permis de stationnement en application de l'article L. 2213-6 du même code. Dès lors, en sa qualité d'autorité chargée de la police de la circulation, le maire est compétent pour y délivrer des permis de stationnement, alors même que ces voies font partie du domaine public de l'Etat, qu'elles ont été remises en dotation à l'établissement public et que celui-ci exerce les pouvoirs de police afférents à leur gestion.</ANA>
<ANA ID="9B"> 135-02-03-02-04-02-04 1) Si la délivrance d'un permis de stationnement incombe en principe au gestionnaire du domaine, c'est sous réserve de dispositions contraires. Il résulte des dispositions des articles L. 2213-1 et L. 2213-6 du code général des collectivités territoriales (CGCT) qu'en sa qualité d'autorité compétente en matière de police de la circulation sur les voies de communication situées à l'intérieur des agglomérations, le maire est seul compétent pour délivrer des permis de stationnement sur ces mêmes voies et sur les autres lieux publics visés à l'article L. 2213-6. Une place piétonne ouverte à la circulation du public et située au sein d'une agglomération est au nombre des dépendances domaniales visées à l'article L. 2213-6 du CGCT.... ,,2) Les dispositions de l'article 230 de la loi n° 2005-157 du 23 février 2005 ont pour objet de coordonner, sur les voies du domaine national de Chambord ouvertes à la circulation publique, les pouvoirs de police respectifs du maire de la commune et du directeur général de l'établissement public. Si elles confèrent à ce dernier le pouvoir de police afférent à la gestion de ces voies en y incluant celui de la circulation, elles réservent au maire la police de la circulation sur les voies de communication situées à l'intérieur de l'agglomération, dans les conditions de droit commun de l'article L. 2213-1 du CGCT, auquel elles renvoient expressément et qui impliquent sa compétence pour délivrer, sur ces voies ainsi que sur les autres lieux publics qui en sont l'accessoire, des permis de stationnement en application de l'article L. 2213-6 du même code. Dès lors, en sa qualité d'autorité chargée de la police de la circulation, le maire est compétent pour y délivrer des permis de stationnement, alors même que ces voies font partie du domaine public de l'Etat, qu'elles ont été remises en dotation à l'établissement public et que celui-ci exerce les pouvoirs de police afférents à leur gestion.</ANA>
<ANA ID="9C"> 24-01-02-01-01 1) L'occupation d'une dépendance du domaine public fait l'objet, lorsqu'elle donne lieu à emprise, d'une permission de voirie délivrée par l'autorité responsable de la gestion du domaine et, dans les autres cas, d'un permis de stationnement.... ,,2) Si la délivrance d'un permis de stationnement incombe en principe à ce même gestionnaire, c'est sous réserve de dispositions contraires. Il résulte des dispositions des articles L. 2213-1 et L. 2213-6 du code général des collectivités territoriales (CGCT) qu'en sa qualité d'autorité compétente en matière de police de la circulation sur les voies de communication situées à l'intérieur des agglomérations, le maire est seul compétent pour délivrer des permis de stationnement sur ces mêmes voies et sur les autres lieux publics visés à l'article L. 2213-6. Une place piétonne ouverte à la circulation du public et située au sein d'une agglomération est au nombre des dépendances domaniales visées à l'article L. 2213-6 du CGCT.... ,,3) Les dispositions de l'article 230 de la loi n° 2005-157 du 23 février 2005 ont pour objet de coordonner, sur les voies du domaine national de Chambord ouvertes à la circulation publique, les pouvoirs de police respectifs du maire de la commune et du directeur général de l'établissement public. Si elles confèrent à ce dernier le pouvoir de police afférent à la gestion de ces voies en y incluant celui de la circulation, elles réservent au maire la police de la circulation sur les voies de communication situées à l'intérieur de l'agglomération, dans les conditions de droit commun de l'article L. 2213-1 du CGCT, auquel elles renvoient expressément et qui impliquent sa compétence pour délivrer, sur ces voies ainsi que sur les autres lieux publics qui en sont l'accessoire, des permis de stationnement en application de l'article L. 2213-6 du même code. Dès lors, en sa qualité d'autorité chargée de la police de la circulation, le maire est compétent pour y délivrer des permis de stationnement, alors même que ces voies font partie du domaine public de l'Etat, qu'elles ont été remises en dotation à l'établissement public et que celui-ci exerce les pouvoirs de police afférents à leur gestion.</ANA>
<ANA ID="9D"> 71-02-03 1) L'occupation d'une dépendance du domaine public fait l'objet, lorsqu'elle donne lieu à emprise, d'une permission de voirie délivrée par l'autorité responsable de la gestion du domaine et, dans les autres cas, d'un permis de stationnement.... ,,2) Si la délivrance d'un permis de stationnement incombe en principe à ce même gestionnaire, c'est sous réserve de dispositions contraires. Il résulte des dispositions des articles L. 2213-1 et L. 2213-6 du code général des collectivités territoriales (CGCT) qu'en sa qualité d'autorité compétente en matière de police de la circulation sur les voies de communication situées à l'intérieur des agglomérations, le maire est seul compétent pour délivrer des permis de stationnement sur ces mêmes voies et sur les autres lieux publics visés à l'article L. 2213-6. Une place piétonne ouverte à la circulation du public et située au sein d'une agglomération est au nombre des dépendances domaniales visées à l'article L. 2213-6 du CGCT.... ,,3) Les dispositions de l'article 230 de la loi n° 2005-157 du 23 février 2005 ont pour objet de coordonner, sur les voies du domaine national de Chambord ouvertes à la circulation publique, les pouvoirs de police respectifs du maire de la commune et du directeur général de l'établissement public. Si elles confèrent à ce dernier le pouvoir de police afférent à la gestion de ces voies en y incluant celui de la circulation, elles réservent au maire la police de la circulation sur les voies de communication situées à l'intérieur de l'agglomération, dans les conditions de droit commun de l'article L. 2213-1 du CGCT, auquel elles renvoient expressément et qui impliquent sa compétence pour délivrer, sur ces voies ainsi que sur les autres lieux publics qui en sont l'accessoire, des permis de stationnement en application de l'article L. 2213-6 du même code. Dès lors, en sa qualité d'autorité chargée de la police de la circulation, le maire est compétent pour y délivrer des permis de stationnement, alors même que ces voies font partie du domaine public de l'Etat, qu'elles ont été remises en dotation à l'établissement public et que celui-ci exerce les pouvoirs de police afférents à leur gestion.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 26 avril 1966, Société d'affichage Giraudy, n° 60127, p. 293 ; CE, 14 juin 1972, Sieur,, n° 83682, p. 436 ; pour la compétence, sauf dispositions contraires, du gestionnaire non propriétaire du domaine pour délivrer les autorisations d'occupation du domaine, CE, 1er février 2012, SA RTE EDF Transport, n° 338665, T. pp. 745-779.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
