<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027476378</ID>
<ANCIEN_ID>JG_L_2013_05_000000368390</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/47/63/CETATEXT000027476378.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 15/05/2013, 368390, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-05-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>368390</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2013:368390.20130515</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête, enregistrée le 10 mai 2013 au secrétariat du contentieux du Conseil d'Etat, présentée par M. A... Jazahay, élisant domicile ...; M. Jazahaydemande au juge des référés du Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1305898/9 du 2 mai 2013 par laquelle le juge des référés du tribunal administratif de Paris, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, a rejeté sa demande tendant à ce qu'il soit enjoint au préfet de police de lui délivrer une autorisation provisoire de séjour en vue du dépôt d'une demande d'asile, dans le délai de 72 heures à compter de la notification de l'ordonnance à intervenir ;<br/>
<br/>
              2°) de faire droit à sa demande de première instance ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement d'une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              il soutient que :<br/>
              - la condition d'urgence est remplie dès lors que le refus du préfet d'enregistrer sa demande d'admission au séjour le prive de manière grave et immédiate des droits afférents au statut de demandeur d'asile ;<br/>
              - le refus de l'administration d'assumer, à compter du 19 mars 2013, la responsabilité de l'examen de sa demande d'asile et la prorogation du délai de son transfert vers les autorités espagnoles jusqu'au 19 mars 2014 constituent une atteinte grave et manifestement illégale au droit d'asile dès lors qu'il ne peut être considéré comme étant en fuite au sens de l'article 19 du règlement (CE) n° 343/2003 du 18 février 2003 ;<br/>
<br/>
<br/>
              Vu l'ordonnance attaquée ;<br/>
<br/>
              Vu le mémoire en défense, enregistré le 13 mai 2013, présenté par le ministre de l'intérieur, qui conclut au rejet de la requête ; il soutient que :<br/>
              - M. Jazahayest à l'origine de l'urgence qu'il invoque ;<br/>
              - en prorogeant le délai de transfert de M. Jazahayjusqu'au 19 mars 2014, le préfet de police n'a porté aucune atteinte grave et manifestement illégale au droit d'asile dès lors que la soustraction intentionnelle et systématique du requérant au contrôle de l'autorité administrative était caractérisée ;<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu le règlement (CE) n° 343/2003 du Conseil du 18 février 2003 ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. Jazahayet, d'autre part, le ministre de l'intérieur ;<br/>
<br/>
              Vu le procès-verbal de l'audience publique du 14 mai 2013 à 11 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Waquet, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. Jazahay ;<br/>
              - le représentant de M. Jazahay ;<br/>
<br/>
              - les représentants du ministre de l'intérieur ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a clôturé l'instruction ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures. " ;<br/>
<br/>
              2. Considérant que le droit constitutionnel d'asile, qui a le caractère d'une liberté fondamentale, a pour corollaire le droit de solliciter le statut de réfugié ; que, s'il implique que l'étranger qui sollicite la reconnaissance de la qualité de réfugié soit en principe autorisé à demeurer sur le territoire jusqu'à ce qu'il ait été statué sur sa demande, ce droit s'exerce dans les conditions définies par l'article L. 741-4 du code de l'entrée et du séjour des étrangers et du droit d'asile ; que le 1° de cet article permet de refuser l'admission en France d'un demandeur d'asile, lorsque l'examen de la demande d'asile relève de la compétence d'un autre Etat en application des dispositions du règlement (CE) n° 343/2003 du Conseil du 18 février 2003 ; que l'article 19 de ce règlement prévoit que le transfert du demandeur d'asile vers le pays de réadmission doit se faire dans les six mois à compter de l'acceptation de la demande de prise en charge et que ce délai peut être porté à dix-huit mois si l'intéressé " prend la fuite " ; <br/>
<br/>
              3. Considérant qu'il résulte de l'instruction que M.Jazahay, ressortissant nigérian entré en France en avril 2012, a sollicité l'asile auprès de la préfecture de police ; que l'enregistrement de sa demande ayant fait apparaître qu'il était auparavant passé par l'Espagne, cet Etat a été saisi d'une demande de prise en charge de la demande d'asile, en application des dispositions du règlement du 18 février 2003 mentionnées ci-dessus ; qu'à la suite de l'acceptation de cette prise en charge par les autorités espagnoles, le 19 septembre 2012, le préfet de police a, par décision du 4 octobre 2012, refusé d'admettre M. Jazahayau séjour au titre de l'asile et décidé sa remise aux autorités espagnoles en lui laissant un délai d'un mois pour quitter volontairement le territoire français ; que l'intéressé s'est à nouveau présenté dans les services de la préfecture de police, le 11 avril 2013, en vue d'être admis au séjour au titre de l'asile, en faisant valoir que la France était désormais compétente pour examiner sa demande ; que l'administration a toutefois estimé que, le délai de réadmission étant prolongé jusqu'au 19 mars, l'examen de sa demande d'asile relevait toujours des autorités espagnoles ;<br/>
<br/>
              4. Considérant, en premier lieu, que pour estimer que M. Jazahayest en fuite, au sens des dispositions de l'article 19 du règlement du 18 février 2003 qui permettent de porter à dix-huit mois le délai de réadmission, l'administration se prévaut de ce que l'intéressé, qui avait été informé, lors de la notification de la décision du 4 octobre 2012, qu'il disposait d'un délai d'un mois pour quitter volontairement le territoire français et qu'à défaut, cette décision pourrait faire l'objet d'une exécution forcée, n'a ensuite pas donné suite à une lettre du 9 novembre 2012 le convoquant le 29 novembre en vue de l'exécution de la mesure de réadmission ;<br/>
<br/>
              5. Considérant, toutefois, que la notion de fuite au sens du règlement du 18 février 2003 doit s'entendre comme visant le cas où un ressortissant étranger non admis au séjour se serait soustrait de façon intentionnelle et systématique au contrôle de l'autorité administrative en vue de faire obstacle à une mesure d'éloignement le concernant ; que, d'une part, la circonstance que le requérant n'a pas spontanément donné suite à l'invitation qui lui était faite, par la décision du 4 octobre 2012, de quitter la France dans le délai d'un mois ne saurait caractériser un comportement de fuite ; que, d'autre part, à supposer même que, contrairement à ce qu'il soutient, M. Jazahayait effectivement reçu la lettre du 9 novembre 2012 - ce que l'administration n'établit pas - le fait de s'abstenir de donner suite à cette unique convocation ne pourrait, en l'absence de toute autre initiative de l'administration vis-à-vis de l'intéressé, permettre de le regarder comme s'étant intentionnellement et systématiquement soustrait à l'exécution de la mesure de réadmission dont il faisait l'objet ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que, contrairement à ce qu'a estimé le premier juge, les faits invoqués par l'administration pour estimer qu'en dépit de l'expiration du délai de six mois, l'Espagne demeure responsable de l'examen de la demande d'asile, ne révèlent, dans les circonstances de l'espèce, aucun comportement de fuite au sens de l'article 19 du règlement du 18 février 2003 ; qu'il apparaît ainsi qu'en refusant de faire droit à la nouvelle demande du requérant tendant à être admis au séjour au titre de l'asile, le préfet a porté une atteinte grave et manifestement illégale à son droit d'asile ; <br/>
<br/>
              7. Considérant, en second lieu, qu'eu égard à la situation dans laquelle se trouve M.Jazahay, convoqué à la préfecture de police le 16 mai prochain en vue de l'exécution de la mesure de réadmission, il est satisfait à la condition particulière d'urgence posée par l'article L. 521-2 du code de justice administrative ;<br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que M. Jazahayest fondé à soutenir que c'est à tort que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Paris a rejeté sa demande ; qu'en l'état de l'instruction, il ne résulte ni des termes du mémoire en défense du ministre, ni des indications fournies par ses représentants à l'audience que l'intéressé entrerait dans l'un des autres cas prévus à l'article L. 741-4 du code de l'entrée et du séjour des étrangers et du droit d'asile ; qu'en particulier, il ne résulte pas des éléments mentionnés au point 5 que le comportement du requérant au cours des six mois qui ont suivi l'acceptation de la demande de prise en charge par l'Espagne pourrait être regardé comme constituant une fraude délibérée ni un recours abusif aux procédures d'asile ; que, dans ces conditions, il y a lieu d'enjoindre au préfet de police de lui délivrer, dans un délai de huit jours à compter de la notification de la présente ordonnance, une autorisation provisoire de séjour lui permettant de présenter sa demande d'asile à l'Office français de protection des réfugiés et apatrides ;<br/>
<br/>
              9. Considérant, enfin, qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à M. Jazahayde la somme de 1 500 euros qu'il demande sur le fondement de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Paris du 2 mai 2013 est annulée.<br/>
Article 2 : Il est enjoint au préfet de police de délivrer à M.Jazahay, dans un délai de huit jours à compter de la notification de la présente ordonnance, une autorisation provisoire de séjour lui permettant de présenter sa demande d'asile.<br/>
Article 3 : L'Etat versera à M. Jazahayune somme de 1 500 euros sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente ordonnance sera notifiée à M. A...Jazahayet au ministre de l'intérieur.<br/>
Copie en sera adressée au préfet de police.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
