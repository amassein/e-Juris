<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030983383</ID>
<ANCIEN_ID>JG_L_2015_07_000000376761</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/98/33/CETATEXT000030983383.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème SSJS, 31/07/2015, 376761, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-07-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>376761</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Julien Anfruns</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:376761.20150731</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              La SA Les Complices a demandé au tribunal administratif de Cergy-Pontoise de prononcer la réduction, par application des dispositions relatives au plafonnement en fonction de la valeur ajoutée, de la cotisation de taxe professionnelle à laquelle elle a été assujettie au titre de l'année 2000 pour son établissement situé 20 rue Rabelais à Montreuil (Seine-Saint-Denis). Par un jugement n° 0503726 du 8 juillet 2008, le tribunal administratif de Cergy-Pontoise a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 08VE03480 du 23 mars 2010, la cour administrative de Versailles a rejeté l'appel formé par la société contre ce jugement. <br/>
<br/>
              Par une décision n° 339977 du 28 décembre 2012, le Conseil d'Etat statuant au contentieux a annulé cet arrêt et renvoyé l'affaire devant la cour administrative d'appel de Versailles.<br/>
<br/>
              Par un second arrêt n° 13VE00174 du 4 février 2014, la cour administrative d'appel de Versailles a annulé le jugement n° 0503726 du 8 juillet 2008 du tribunal administratif de Cergy-Pontoise en tant qu'il a rejeté comme irrecevable la demande en réduction de l'imposition primitive de l'établissement principal situé à Montreuil de la SA Les Complices à concurrence d'un montant de 119 187 euros, prononcé la décharge correspondante et rejeté le surplus des conclusions de la requête de la société. <br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi et un nouveau mémoire, enregistrés le 27 mars 2014 et le 29 avril 2015 au secrétariat du contentieux du Conseil d'Etat, le ministre délégué chargé du budget demande au Conseil d'Etat d'annuler les articles 1er, 2 et 4 de ce second arrêt.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le livre des procédures fiscales et le code général des impôts ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Julien Anfruns, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de la SA Les Complices ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. D'une part, si, aux termes de l'article R. 196-2 du livre des procédures fiscales dans sa rédaction applicable à la procédure d'imposition en litige : " Pour être recevables, les réclamations relatives aux impôts directs locaux (...) doivent être présentées à l'administration des impôts au plus tard le 31 décembre de l'année suivant, selon le cas :/ a) L'année de la mise en recouvrement du rôle (...) ", l'article R. 196-3 du même livre dans sa rédaction applicable dispose que : " Dans le cas où un contribuable fait l'objet d'une procédure de reprise ou de redressement de la part de l'administration des impôts, il dispose d'un délai égal à celui de l'administration pour présenter ses propres réclamations ". D'autre part, aux termes du premier alinéa de l'article L 174 du même livre dans sa rédaction applicable : " Les omissions ou les erreurs concernant la taxe professionnelle peuvent être réparées par l'administration jusqu'à l'expiration de la troisième année suivant celle au titre de laquelle l'imposition est due ". <br/>
<br/>
              2. Le contribuable à l'égard duquel l'administration met en oeuvre le pouvoir de réparation des erreurs ou omissions que lui confère l'article L. 174 précité du livre des procédures fiscales en matière de taxe professionnelle doit être regardé comme faisant l'objet d'une procédure de reprise au sens de l'article R 196-3 du même livre, en application duquel il dispose, dès lors, pour présenter ses propres réclamations, d'un délai dont l'expiration coïncide avec celle du délai de répétition restant ouvert à l'administration elle-même, soit, en l'absence de tout acte interruptif de prescription, le 31 décembre de la troisième année suivant celle au titre de laquelle la taxe professionnelle est due. Dans le cadre de ce délai spécial, un redevable de la taxe professionnelle peut présenter une réclamation relative non seulement aux cotisations supplémentaires mises à sa charge mais également à l'ensemble des cotisations primitives dues au titre de la même année dans les rôles de la même commune. Il en est ainsi, notamment, en cas de réclamation tendant à la réduction, par application des dispositions relatives au plafonnement en fonction de la valeur ajoutée, de la cotisation de taxe professionnelle à laquelle le contribuable avait été primitivement assujetti. <br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond que la SA Les Complices, qui exerce une activité de fabrication et de négoce d'articles de confection, exploite un établissement principal et deux établissements secondaires situés à Montreuil. Usant du pouvoir de réparation des erreurs ou omissions que lui confère l'article L. 174 précité du livre des procédures fiscales, l'administration a mis en recouvrement le 31 décembre 2003 des cotisations supplémentaires de taxe professionnelle au titre de l'année 2000 pour ses deux établissements secondaires. La société Les Complices a demandé, par une réclamation du 11 février 2004, la réduction des cotisations de taxe professionnelle mises à sa charge pour l'année 2000 dans les rôles de la commune de Montreuil au titre du plafonnement en fonction de la valeur ajoutée. L'administration a fait droit à cette demande de dégrèvement dans la limite de la fraction éligible au plafonnement des cotisations supplémentaires relatives aux établissements secondaires et l'a rejetée pour le surplus, estimant qu'elle était tardive pour l'imposition relative à l'établissement principal. Le 29 septembre 2004, la société a réitéré sa réclamation, à laquelle l'administration n'a pas répondu. <br/>
<br/>
              4. Pour annuler le jugement du 8 juillet 2008 du tribunal administratif de Cergy-Pontoise en tant qu'il a rejeté comme irrecevable la demande en réduction de l'imposition primitive de l'établissement principal situé à Montreuil de la SA Les Complices à concurrence d'un montant de 119 187 euros, la cour administrative d'appel de Versailles, statuant sur renvoi du Conseil d'Etat, a jugé que la demande contenue dans ces deux réclamations, relative à l'imposition primitive de l'année 2000 due au titre de l'établissement principal qu'elle exploite dans la même commune, n'était pas tardive en application de l'article R. 196-3 du livre des procédures fiscales. Il résulte toutefois de ce qui a été dit au point 3 que si la société pouvait se prévaloir du délai spécial de réclamation prévu par ces dispositions y compris pour obtenir la réduction de l'imposition primitive due au titre de la même année dans les rôles de la même commune pour son établissement principal, ce délai de réclamation expirait, en l'absence de tout acte interruptif de prescription, le 31 décembre 2003. En jugeant que les réclamations de la société présentées en 2004 n'étaient, au titre de cette imposition, pas tardives, la cour a, par suite, commis une erreur de droit. Pour ce motif, les articles 1er, 2 et 4 de son arrêt doivent dès lors être annulés. <br/>
<br/>
              5. Aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire ". Il incombe, par suite, au Conseil d'Etat, de régler, dans la mesure indiquée ci-dessus, l'affaire au fond.<br/>
<br/>
              6. Ainsi qu'il vient d'être dit, les réclamations présentées par la SA Les Complices les 11 février et 29 septembre 2004, s'agissant de l'imposition primitive due au titre de l'année 2000 pour l'établissement principal que la SA Les Complices exploite à Montreuil, étaient tardives. Par suite, la SA Les Complices n'est pas fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Cergy-Pontoise a rejeté sa demande sur ce point. <br/>
<br/>
              7. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'État qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Les articles 1er, 2 et 4 de l'arrêt du 4 février 2014 de la cour administrative d'appel de Versailles sont annulés. <br/>
Article 2 : La requête présentée par la SA Les Complices devant la cour administrative d'appel de Versailles en tant qu'elle porte sur la réduction de l'imposition primitive due au titre de l'année 2000 pour son établissement principal exploité à Montreuil et ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 3 : La présente décision sera notifiée au ministre des finances et des comptes publics et à la SA Les Complices.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
