<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000018006999</ID>
<ANCIEN_ID>JG_L_2007_07_000000305595</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/18/00/69/CETATEXT000018006999.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 11/07/2007, 305595, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2007-07-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>305595</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Martin Laprade</PRESIDENT>
<AVOCATS>SPINOSI</AVOCATS>
<RAPPORTEUR>M. Bruno  Martin Laprade</RAPPORTEUR>
<COMMISSAIRE_GVT/>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 14 mai 2007 au secrétariat du contentieux du Conseil d'Etat, présentée pour la SECTION FRANÇAISE DE L'OBSERVATOIRE INTERNATIONAL DES PRISONS (OIP-SF), dont le siège est 31 rue des Lilas à Paris (75019) ; l'OIP-SF demande au juge des référés du Conseil d'Etat :<br/>
<br/>
              1°) de suspendre, sur le fondement de l'article L. 521-1 du code de justice administrative, l'exécution de la décision implicite par laquelle le ministre de la justice a rejeté la demande qu'elle lui a adressée le 10 janvier 2007 de doter immédiatement l'ensemble des matelas équipant les établissements pénitentiaires d'une housse ignifugée inamovible, de faire procéder à des études techniques en vue d'éliminer les risques liés à la combustion des matelas et autres matériels de literie et de fixer, sur la base des résultats de ces études, une réglementation de ces matériels assurant une protection efficace des détenus ;<br/>
<br/>
              2°) d'enjoindre au ministre de réexaminer sa demande dans les meilleurs délais ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros sur le fondement de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              la requérante soutient que l'urgence ressort du très grand nombre de feux qui sont déclenchés dans les cellules de prison, volontairement ou non, et de la très haute et immédiate toxicité des gaz dégagés par la combustion des matelas ; que la décision attaquée méconnaît l'obligation incombant à l'administration pénitentiaire de prendre les mesures nécessaires pour protéger la vie des détenus, ainsi qu'il résulte de l'article 2 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, tel qu'interprété par la Cour de Strasbourg, en particulier dans ses arrêts du 17 janvier 2002, Calvelli et Ciglio c/ Italie, à propos de l'obligation pour l'Etat de mettre en place une réglementation propre à assurer la vie des malades hospitalisés, et du 27 juillet 2004, Slimani c/ France du 27 juillet 2004, soulignant la dimension particulière de cette obligation vis à vis des détenus, qui se trouvent entièrement sous le contrôle des autorités et sont particulièrement vulnérables ; qu'en effet, le risque mortel présenté par la combustion des matelas est reconnu depuis 1995 par l'administration, laquelle n'a équipé d'une housse ignifugée inamovible que les matelas des seuls quartiers d'isolement ou de discipline ; que le refus d'étendre cette mesure aux matelas équipant l'ensemble des services pénitentiaires peut engager la responsabilité pour faute lourde de l'administration, en cas de sinistre, comme l'a jugé la cour administrative d'appel de Versailles le 2 février 2006 ; que les normes françaises de résistance des matelas à la combustion sont anormalement laxistes ;<br/>
<br/>
<br/>
              Vu la demande adressée le 10 janvier 2007 au ministre de la justice par l'OIP-SF ;<br/>
<br/>
              Vu, enregistré comme ci dessus le 27 juin 2007, le mémoire en défense présenté par le ministre de la justice, qui tend au rejet de la requête par les motifs que celle-ci est irrecevable car les mesures demandées n'ont pas le caractère provisoire de celles que peut ordonner le juge du référé-suspension ; que la situation dénoncée par la requérante n'est pas nouvelle, en sorte qu'il n'y a pas d'urgence à la faire cesser avant que ne statue le juge du fond ; que les feux dans les cellules sont généralement allumés volontairement par les détenus ; qu'aucune faute ne peut être reprochée à l'administration ; que les matelas équipant les prisons respectent la recommandation D1bis-89 du GPEM/CP et relèvent de la « classe A » retenue par la recommandation D1-90 du GPEM/CP (brochure n°5590-1991 « réaction au feu des matelas utilisés dans les lieux à hauts risques») ; que l'absence de housse inamovible et ignifugée ne constitue pas une faute, car les détenus enlèvent eux mêmes la housse dont sont dotés les matelas ; que l'administration a pris de nombreuses mesures pour prévenir et limiter les dangers des incendies, en particulier un arrêté du 18 juillet 2006 et une circulaire du 4 mai 2007 ; qu'elle a conclu le 21 novembre 2006 un marché avec la société Celso pour doter les quartiers d'isolement et de discipline de matelas haute sécurité ; qu'un autre marché a été conclu en 2003 avec la société Cawe pour les matelas des autres quartiers ; <br/>
<br/>
              Vu, enregistré comme ci-dessus le 3 juillet 2007, le mémoire en réplique produit pour l'OIP-SF, qui maintient les conclusions et les moyens de sa requête, et précise en outre que les mesures réglementaires de 2006 ne règlent pas le problème du comportement au feu des matelas ; que l'administration est consciente que seuls des motifs budgétaires expliquent que l'ensemble des matelas n'aient pas été dotés d'une double housse (l'une amovible et l'autre inamovible), comme dans les quartiers de sécurité ; <br/>
<br/>
              Vu, enregistré comme ci-dessus le 5 juillet 2007, le mémoire par lequel le ministre précise que l'incendie qui s'est produit à Blois le 25 janvier 2007 a été volontairement provoqué par un détenu, à partir d'un matelas Cawe, livré en septembre 2004 ; que les recherches n'ont pas permis d'obtenir un comparatif probant entre les différents types de matelas utilisés dans les quartiers de détention normale ; <br/>
<br/>
              Vu, enregistré comme ci-dessus le 6 juillet 2007, le mémoire en réplique par lequel l'OIP-SF précise que, contrairement à ce qui a été avancé oralement à l'audience du 4 juillet, les matelas de « classe A » attribués aux détenus dans les quartiers ordinaires n'ont qu'une seule housse, qui est une sorte d'alèse amovible, mais n'ont aucune housse inamovible ; que les matelas Cawe commandés depuis 2003 ne diffèrent pas des précédents et que leur combustion reste très toxique ; <br/>
<br/>
              Vu la requête au fond présentée pour l'OIP-SF le 14 mai 2007 sous le n° 305594 ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, la SECTION FRANÇAISE DE L'OBSERVATOIRE INTERNATIONAL DES PRISONS et, d'autre part, le Garde des sceaux, ministre de la justice ;<br/>
<br/>
              Vu les procès-verbaux des audiences publiques du 4 juillet 2007 à 11 heures et du 9 juillet 2007 à 9 heures au cours desquelles ont été entendus :<br/>
<br/>
              - Me Spinosi, avocat au Conseil d'Etat et à la Cour de cassation, avocat de l'OIP-SF ;<br/>
<br/>
              - le représentant de l'OIP-SF ;<br/>
<br/>
              - les représentants du Garde des sceaux, ministre de la justice ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant que l'association requérante fait valoir que de très nombreux incendies sont allumés volontairement par des détenus, non seulement dans les quartiers d'isolement et de discipline, mais aussi dans les quartiers ordinaires, d'où sont partis 30% des 195 débuts d'incendie recensés pendant les quatre premiers mois de 2007 ; que l'un de leurs moyens « privilégiés » d'action consiste à mettre le feu, grâce au « chauffe-à-l'huile » mis à leur disposition pour réchauffer des plats, à un ou plusieurs de leurs matelas, dont la composition en mousse de polyuréthane implique, en cas de combustion, l'émanation de gaz hautement toxiques (acide cyanhydrique et monoxyde de carbone) susceptibles d'entraîner la mort des occupants de la cellule en moins d'un quart d'heure ; que si ces matelas répondent aux normes du classement de résistance au feu les plus élevées préconisées par le GPEM/CP (groupe permanent d'étude des marchés - produits divers des industries chimiques et para-chimiques), notamment dans sa recommandation D1-90 du GPEM/CP (brochure n° 5590-1991 « réaction au feu des matelas utilisés dans les lieux à hauts risques»), ces documents précisent que les comportements étudiés portent toujours sur le matelas complet, c'est à dire l'ensemble comprenant l'intérieur du matelas et la housse qui le revêt ; <br/>
<br/>
              Considérant qu'ainsi que l'a jugé la cour européenne de sauvegarde des droits de l'homme et des libertés fondamentales, il résulte de l'article 2 de la convention qu'elle est chargée d'appliquer qu'eu égard à la vulnérabilité des détenus et à leur situation d'entière dépendance vis à vis de l'administration, il appartient tout particulièrement à celle-ci de prendre les mesures propres à protéger leur vie ; <br/>
<br/>
              Considérant qu'il ressort du dossier que le marché, passé par la direction de l'administration pénitentiaire le 25 novembre 2003 avec la société Cawe, a prévu la fourniture de deux catégories de matelas : d'une part, des matelas « houssés », pour une quantité annuelle comprise entre 16000 et 24000, au prix unitaire de 33,91 euros TTC (ces prix s'entendant hors remises supplémentaires pour quantité), couverts d'une sorte d'alèse ignifugée aisément amovible (pour en faciliter le nettoyage), et, d'autre part, des matelas avec « double housse », pour une quantité annuelle comprise entre 1100 et 1700 matelas, au prix unitaire de 50,87 euros TTC, lesquels sont destinés aux seuls quartiers de discipline et d'isolement et comportent, sous la même alèse, une seconde housse ignifugée, plus épaisse et prévue comme inamovible ; que la seule mesure que l'association a demandée au ministre de prendre immédiatement, et dont elle demande de suspendre le refus au motif qu'elle traduirait la méconnaissance de l'obligation qui vient d'être rappelée, consiste à faire doter l'ensemble des matelas attribués aux détenus d'une housse ignifugée « inamovible », semblable à celles prévues pour la seconde catégorie de matelas faisant l'objet de ce marché ;<br/>
<br/>
              Considérant que, des explications qui ont été données au cours des deux audiences tenues,  il ressort que cette mesure n'apparaît pas, en l'état de l'instruction, comme de nature à réduire le danger de combustion des matelas d'une manière telle que le moyen tiré de l'illégalité du refus de la prendre paraisse de nature à créer un doute sérieux ; qu'en effet, les pièces produites au cours de la deuxième audience font apparaître que la housse fixe dont étaient dotés les matelas Cawe dans les quartiers de sécurité n'était en réalité pas réellement inamovible, compte tenu de la légèreté de sa texture, qui permet encore trop aisément de la déchirer ou de la découdre ; que pour cette raison, l'administration a conclu le 21 novembre 2006 avec la société Celso un marché pour la fourniture de plus de 4000 matelas, au prix unitaire de 269 euros, qui auront remplacé, à la fin de l'année 2007, l'ensemble des matelas Cawe double houssés sus décrits et dont la composition interne et l'enveloppe protectrice sont beaucoup mieux sécurisés ; que l'association requérante a convenu qu'elle ne demandait pas que ces matelas soient diffusés dans l'ensemble des quartiers, eu égard à la disproportion entre le surcroît de sécurité qu'ils procurent et l'inconfort qu'ils imposent aux intéressés ; que dans ces conditions, eu égard à l'office du juge des référés, la requête doit être rejetée, ainsi, par voie de conséquence, que les conclusions présentées sur le fondement de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de la SECTION FRANÇAISE DE L'OBSERVATOIRE INTERNATIONAL DES PRISONS est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à la SECTION FRANÇAISE DE L'OBSERVATOIRE INTERNATIONAL DES PRISONS et au Garde des sceaux, ministre de la justice.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
