<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036791205</ID>
<ANCIEN_ID>JG_L_2018_04_000000408380</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/79/12/CETATEXT000036791205.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 11/04/2018, 408380, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-04-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>408380</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT, ROBILLOT</AVOCATS>
<RAPPORTEUR>Mme Dominique Chelle </RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:408380.20180411</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
               Mme B...A...a demandé au tribunal administratif de Paris de condamner l'Etat à lui verser une indemnité de 27 000 euros, augmentée des intérêts au taux légal, en réparation des troubles dans ses conditions d'existence et du préjudice moral résultant de son absence de relogement. Par un jugement n° 1517480/3-2 du 16 novembre 2016, le tribunal administratif a rejeté cette demande.<br/>
<br/>
               Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 27 février et 29 mai 2017 au secrétariat du contentieux du Conseil d'Etat, Mme A... demande au Conseil d'Etat :<br/>
<br/>
               1°) d'annuler ce jugement ;<br/>
<br/>
               2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 000 euros à verser à son avocat, la SCP Potier de La Varde, Buk Lament, Robillot, au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de la construction et de l'habitation ;<br/>
<br/>
- la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Dominique Chelle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Potier de la Varde, Buk Lament, Robillot, avocat de MmeA....<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que Mme A...a été reconnue prioritaire et devant être relogée en urgence, sur le fondement de l'article L. 411-2-3 du code de la construction et de l'habitation, par une décision du 8 avril 2011 de la commission de médiation de Paris au motif qu'elle était dépourvue de logement et hébergée par un tiers ; que, par un jugement du 4 avril 2012, le tribunal administratif de Paris, saisi par Mme A...sur le fondement du I de l'article L. 441-2-3-1 du même code a enjoint au préfet de la région Ile-de-France, préfet de Paris, d'assurer le relogement de MmeA..., en constatant que si elle était désormais logée à titre temporaire en résidence sociale, elle se trouvait toujours dans une situation conférant à sa demande de logement social un caractère prioritaire et urgent ; que, le 25 octobre 2015, Mme A...a demandé au tribunal administratif de condamner l'Etat à réparer les préjudices qu'entraînait pour elle l'absence de relogement ; qu'elle se pourvoit en cassation contre le jugement du 16 novembre 2016 par lequel le tribunal administratif a rejeté sa demande au motif qu'elle n'établissait pas le caractère anormal de ses conditions de logement en résidence sociale ;<br/>
<br/>
              2. Considérant que, lorsqu'une personne a été reconnue comme prioritaire et comme devant être logée ou relogée d'urgence par une commission de médiation, en application des dispositions de l'article L. 441-2-3 du code de la construction et de l'habitation, la carence fautive de l'Etat à exécuter cette décision dans le délai imparti engage sa responsabilité à l'égard du seul demandeur, au titre des troubles dans les conditions d'existence résultant du maintien de la situation qui a motivé la décision de la commission, que l'intéressé ait ou non fait usage du recours en injonction contre l'Etat prévu par l'article L. 441-2-3-1 du code de la construction et de l'habitation ; que ces troubles doivent être appréciés en fonction des conditions de logement qui ont perduré du fait de la carence de l'Etat, de la durée de cette carence et du nombre de personnes composant le foyer du demandeur pendant la période de responsabilité de l'Etat, qui court à compter de l'expiration du délai de trois ou six mois à compter de la décision de la commission de médiation que les dispositions de l'article R. 441-16-1 du code de la construction et de l'habitation impartissent au préfet pour provoquer une offre de logement ; <br/>
<br/>
              3. Considérant qu'il suit de là qu'ayant constaté que le préfet n'avait pas proposé un relogement à Mme A...dans le délai qui lui était imparti, le tribunal administratif de Paris ne pouvait, sans commettre une erreur de droit, juger que cette carence, constitutive d'une faute de nature à engager la responsabilité de l'Etat, ne causait à l'intéressée aucun préjudice indemnisable, au motif que le logement dont elle disposait à titre temporaire dans une résidence sociale n'était ni insalubre, ni affecté de désordres et n'était pas occupé dans des conditions anormales, alors qu'il était constant que Mme A...demeurait logée dans des conditions conférant à sa demande de logement social un caractère prioritaire et urgent, et qu'elle subissait de ce fait des troubles dans ses conditions d'existence lui ouvrant droit à réparation dans les conditions rappelées ci-dessus ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, la requérante est fondée à demander l'annulation du jugement qu'elle attaque ;<br/>
<br/>
              4. Considérant que Mme A...a obtenu le bénéfice de l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Potier de La Varde, Buk Lament, Robillot, avocat de Mme A..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat le versement à cette société de la somme de 2 000 euros ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 16 novembre 2016 du tribunal administratif de Paris est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Paris.<br/>
Article 3 : L'Etat versera à la SCP Potier de La Varde, Buk Lament, Robillot  une somme de 2 000 euros au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : La présente décision sera notifiée à Mme B...A...et au ministre de la cohésion des territoires.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
