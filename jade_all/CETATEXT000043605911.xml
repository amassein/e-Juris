<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043605911</ID>
<ANCIEN_ID>JG_L_2021_05_000000438058</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/60/59/CETATEXT000043605911.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 28/05/2021, 438058, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-05-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>438058</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOUZIDI, BOUHANNA</AVOCATS>
<RAPPORTEUR>M. Clément Tonon</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:438058.20210528</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1°/ Sous le n° 438058, par une requête sommaire, enregistrée le 28 janvier 2020 au secrétariat du contentieux du Conseil d'Etat, M. B... A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du Premier ministre du 22 novembre 2019 lui refusant l'acquisition de la nationalité française ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              2°/ Sous le n° 438102, par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 28 janvier, 22 mai et 28 août 2020 au secrétariat du contentieux du Conseil d'Etat, M. B... A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du Premier ministre du 22 novembre 2019 lui refusant l'acquisition de la nationalité française ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le code civil ;<br/>
              - le code de procédure pénale ;<br/>
              - le décret n° 93-1362 du 30 décembre 1993 ;<br/>
              - le code justice administrative et le décret n° 2020-1806 du 18 novembre 2020 ; <br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Clément Tonon, auditeur ;<br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Bouzidi, Bouhanna, avocat de M. A... ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Les requêtes visées ci-dessus tendent à l'annulation du même décret. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2. Aux termes de l'article 21-2 du code civil : " L'étranger (...) qui contracte mariage avec un conjoint de nationalité française peut, après un délai de quatre ans à compter du mariage, acquérir la nationalité française par déclaration à condition qu'à la date de cette déclaration la communauté de vie tant affective que matérielle n'ait pas cessé entre les époux depuis le mariage et que le conjoint français ait conservé sa nationalité (...) ". L'article 21-4 du même code prévoit toutefois que : " Le Gouvernement peut s'opposer par décret en Conseil d'Etat, pour indignité ou défaut d'assimilation, autre que linguistique, à l'acquisition de la nationalité française par le conjoint étranger dans un délai de deux ans à compter de la date du récépissé prévu au deuxième alinéa de l'article 26 ou, si l'enregistrement a été refusé, à compter du jour où la décision judiciaire admettant la régularité de la déclaration est passée en force de chose jugée. (...) ".<br/>
<br/>
              3. M. A..., ressortissant marocain, a souscrit le 12 mars 2018 une déclaration en vue d'acquérir la nationalité française à raison de son mariage avec une ressortissante française. Par le décret attaqué, le Premier ministre s'est opposé à l'acquisition de la nationalité française par M. A... au motif qu'il ne pouvait être regardé comme étant digne de l'acquérir. <br/>
<br/>
              4. Il ressort des pièces des dossiers que le Premier ministre s'est fondé sur les circonstances que M. A... a été reconnu coupable d'avoir commis en 1995 de façon réitérée des faits de falsification de documents d'identité, permis de séjour et permis de conduire et d'avoir pénétré irrégulièrement sur le territoire français en 1988, faits pour lesquels il a été condamné à 18 mois d'emprisonnement et à une interdiction du territoire de trois ans. Il a estimé que ces faits avaient été commis en relation avec une entreprise terroriste, certains des documents falsifiés ayant été utilisés par des personnes poursuivies de ce chef, et étaient d'une gravité telle qu'ils rendaient M. A... indigne d'acquérir la nationalité française.<br/>
<br/>
              5. Il ressort des pièces des dossiers, notamment du jugement du tribunal de grande instance de Paris du 15 septembre 1999, confirmé par l'arrêt de la cour d'appel de Paris du 25 mai 2000, que M. A... a fait l'objet d'une décision de non-lieu pour le chef d'association de malfaiteurs ayant pour objet de préparer un acte de terrorisme et ne connaissait pas les utilisateurs des documents falsifiés. Sa demande d'être relaxé de la circonstance aggravante de ce que les faits qui lui étaient reprochés auraient été commis en relation avec une entreprise à caractère terroriste a été rejetée non du fait de l'existence d'une telle circonstance, mais au motif que si ces faits présentaient un lien de connexité avec des actes de terrorisme, cette connexité, prévue par les dispositions de l'article 706-6 du code de procédure pénale, n'était ni une infraction nouvelle, ni une circonstance aggravante, mais le fondement d'une règle de procédure justifiant en particulier la compétence de la juridiction saisie pour juger des infractions de droit commun connexes avec les actes de terrorisme. Par suite, le Premier ministre, qui n'apporte aucun autre élément au soutien de sa décision, a fait une inexacte application des dispositions de l'article 21-4 du code civil en estimant que ces faits, anciens, rendaient l'intéressé indigne d'acquérir la nationalité française à la suite de son mariage.<br/>
<br/>
              6. Il résulte de ce qui précède que M. A... est fondé à demander l'annulation pour excès de pouvoir du décret du 22 novembre 2019 lui refusant l'acquisition de la nationalité française. Il y a lieu dans les circonstances de l'espèce de mettre à la charge de l'Etat la somme de 3 000 euros à verser M. A... sur le fondement de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le décret du 22 novembre 2019 refusant l'acquisition de la nationalité française à M. A... est annulé.<br/>
<br/>
Article 2 : L'Etat versera à M. A... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. B... A... et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
