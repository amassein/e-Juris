<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042854750</ID>
<ANCIEN_ID>JG_L_2020_12_000000439436</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/85/47/CETATEXT000042854750.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 31/12/2020, 439436</TITRE>
<DATE_DEC>2020-12-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>439436</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET BRIARD</AVOCATS>
<RAPPORTEUR>Mme Sophie-Caroline de Margerie</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:439436.20201231</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par un mémoire, enregistré le 28 octobre 2020 au secrétariat du contentieux du Conseil d'Etat, M. A...-C... B... demande au Conseil d'Etat, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 et à l'appui de sa requête tendant à l'annulation pour excès de pouvoir du décret du 21 février 2020 accordant son extradition aux autorités burkinabè, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du second alinéa de l'article 696-18 du code de procédure pénale.<br/>
<br/>
<br/>
              	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ;<br/>
              - l'accord de coopération en matière de justice signé par la République française et la République de Haute-Volta le 24 avril 1961 ;<br/>
              - le code de procédure pénale ;<br/>
              - le code de justice administrative, l'ordonnance n° 2020-1402 et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie-Caroline de Margerie, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, au Cabinet Briard, avocat de M. A...-françois B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. Le second alinéa de l'article 696-18 du code de procédure pénale dispose que le recours pour excès de pouvoir contre un décret autorisant une extradition " doit, à peine de forclusion, être formé dans le délai d'un mois (...) ". <br/>
<br/>
              3. A l'appui de sa requête tendant à l'annulation pour excès de pouvoir du décret du 21 février 2020 accordant son extradition aux autorités du Burkina Faso, M. B... soutient que le législateur a méconnu sa compétence dans des conditions affectant le droit à un recours effectif et la liberté d'aller et venir, la liberté personnelle, le droit de ne pas être soumis à la torture et le droit à un procès équitable, en ce que le second alinéa de l'article 696-18 du code de procédure pénale donne un délai d'un mois seulement pour former un recours contre un décret d'extradition, sans indiquer que l'absence de mention de ce délai dans la notification de cette décision le rend inopposable et en ce qu'il ne prévoit ni de caractère suspensif pour ce recours, ni de délai dans lequel la juridiction saisie doit statuer.<br/>
<br/>
              4. En premier lieu, la limitation à un mois du délai dans lequel la personne réclamée peut former un recours pour excès de pourvoir contre le décret accordant son extradition contribue à assurer la célérité et l'efficacité de la procédure d'extradition, laquelle peut comporter, le cas échéant, une période de détention à titre extraditionnel et les articles 696-10 à 696-18 du code de procédure pénale organisent une procédure préalable contradictoire donnant lieu à un avis motivé de la chambre d'instruction. Dans ces conditions, le délai de recours prévu au second alinéa de l'article 696-18 du même code ne méconnaît pas le droit à un recours effectif garanti par l'article 16 de la Déclaration des droits de l'homme et du citoyen. <br/>
<br/>
              5. Par ailleurs, si le requérant fait grief à cette disposition de ne pas faire obligation à la notification du décret autorisant l'extradition de mentionner les voies et délais de recours et de ne pas prévoir l'inopposabilité du délai de recours en cas de non-respect de cette obligation, la définition d'une telle exigence, qui, au demeurant, n'affecte pas la substance du droit au recours, relève du pouvoir réglementaire. Contrairement à ce que soutient le requérant, en énonçant que le recours doit être exercé dans le délai d'un mois " à peine de forclusion ", le législateur n'a pas entendu faire obstacle à l'application des dispositions de l'article R. 421-5 du code de justice administrative, aux termes desquelles " les délais de recours contre une décision administrative ne sont opposables qu'à la condition d'avoir été mentionnés, ainsi que les voies de recours, dans la notification de la décision ".  <br/>
<br/>
              6. En deuxième lieu, il résulte des dispositions du second alinéa de l'article 696-18 du code de procédure pénale, telles qu'interprétées au regard de l'article 16 de la Déclaration des droits de l'homme et du citoyen et à la lumière d'un usage constant, qu'un décret d'extradition ne saurait être mis à exécution tant que le délai de recours n'est pas expiré et, le cas échéant, tant que le Conseil d'Etat, saisi d'un recours dans ce délai, n'a pas statué. Il ne peut dès lors être fait grief aux dispositions contestées de ne pas avoir prévu d'effet suspensif à l'exercice d'un tel recours.<br/>
<br/>
              7. En troisième lieu, l'absence de fixation d'un délai dans lequel la juridiction serait tenue de statuer sur un recours contentieux formé contre le décret d'extradition n'affecte pas par elle-même la liberté individuelle et la liberté d'aller et venir de la personne réclamée. Si cette personne est susceptible d'être incarcérée sous écrou extraditionnel durant l'examen de son recours, sa situation est régie à ce titre par d'autres dispositions du code de procédure pénale et est entourée de garanties, dont la faculté de solliciter à tout instant de la procédure, qu'elle soit juridictionnelle ou administrative, sa mise en liberté devant la chambre de l'instruction et l'obligation pour l'autorité judiciaire de faire droit à la demande de mise en liberté lorsque la durée totale de la détention, dans le cadre de la procédure d'extradition, excède un délai raisonnable.<br/>
<br/>
              8. Il suit de là que l'exercice du recours contentieux contre un décret d'extradition est, dans son ensemble, assorti de garanties suffisantes. Par suite, M. B... n'est pas fondé à soutenir que les dispositions du second alinéa de l'article 696-18 du code de procédure pénale seraient entachées d'une incompétence négative privant de garanties légales les exigences constitutionnelles qu'il invoque. <br/>
<br/>
              9. Il résulte de tout ce qui précède que la question de la conformité aux droits et libertés garantis par la Constitution de la disposition contestée ne présente pas un caractère sérieux. Il n'y a, dès lors, pas lieu de la renvoyer au Conseil constitutionnel.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M. B....  <br/>
Article 2 : La présente décision sera notifiée à M. A...-C... B... et au garde des sceaux, ministre de la justice.<br/>
Copie en sera adressée au Conseil constitutionnel et au Premier ministre. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-015-03-01-01-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. - DROIT À UN RECOURS EFFECTIF (ART. 16) - INTERPRÉTATION CONFORME DU SECOND ALINÉA DE L'ARTICLE 696-18 DU CPP - CONSÉQUENCE - CARACTÈRE EXÉCUTOIRE D'UN DÉCRET D'EXTRADITION NON DÉFINITIF - ABSENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">26-045 DROITS CIVILS ET INDIVIDUELS. EXTRADITION (VOIR : ÉTRANGERS). - DÉCRET D'EXTRADITION NON DÉFINITIF - CARACTÈRE EXÉCUTOIRE - ABSENCE [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">335-04 ÉTRANGERS. EXTRADITION. - DÉCRET D'EXTRADITION NON DÉFINITIF - CARACTÈRE EXÉCUTOIRE - ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 01-015-03-01-01-01 Il résulte du second alinéa de l'article 696-18 du code de procédure pénale (CPP), tel qu'interprété au regard de l'article 16 de la Déclaration des droits de l'homme et du citoyen et à la lumière d'un usage constant, qu'un décret d'extradition ne saurait être mis à exécution tant que le délai de recours n'est pas expiré et, le cas échéant, tant que le Conseil d'Etat, saisi d'un recours dans ce délai, n'a pas statué. Il ne peut dès lors être fait grief au second alinéa de l'article 696-18 du CPP de ne pas avoir prévu d'effet suspensif à l'exercice d'un tel recours.</ANA>
<ANA ID="9B"> 26-045 Il résulte du second alinéa de l'article 696-18 du code de procédure pénale (CPP), tel qu'interprété au regard de l'article 16 de la Déclaration des droits de l'homme et du citoyen et à la lumière d'un usage constant, qu'un décret d'extradition ne saurait être mis à exécution tant que le délai de recours n'est pas expiré et, le cas échéant, tant que le Conseil d'Etat, saisi d'un recours dans ce délai, n'a pas statué. Il ne peut dès lors être fait grief au second alinéa de l'article 696-18 du CPP de ne pas avoir prévu d'effet suspensif à l'exercice d'un tel recours.</ANA>
<ANA ID="9C"> 335-04 Il résulte du second alinéa de l'article 696-18 du code de procédure pénale (CPP), tel qu'interprété au regard de l'article 16 de la Déclaration des droits de l'homme et du citoyen et à la lumière d'un usage constant, qu'un décret d'extradition ne saurait être mis à exécution tant que le délai de recours n'est pas expiré et, le cas échéant, tant que le Conseil d'Etat, saisi d'un recours dans ce délai, n'a pas statué. Il ne peut dès lors être fait grief au second alinéa de l'article 696-18 du CPP de ne pas avoir prévu d'effet suspensif à l'exercice d'un tel recours.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Ab. jur., dans cette mesure, CE, juge des référés, 29 juillet 2003, M.,, n° 258900, p. 344.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
