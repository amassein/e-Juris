<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033285498</ID>
<ANCIEN_ID>JG_L_2016_10_000000400688</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/28/54/CETATEXT000033285498.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Formation spécialisée, 19/10/2016, 400688, Publié au recueil Lebon</TITRE>
<DATE_DEC>2016-10-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>400688</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Formation spécialisée</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Emmanuelle Prada Bordenave</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2016:400688.20161019</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 13 juin et 19 août 2016 au secrétariat du contentieux du Conseil d'Etat, M. B...A...demande au Conseil d'Etat d'annuler pour excès de pouvoir la décision, révélée par le courrier de la présidente de la Commission nationale de l'informatique et des libertés (CNIL) du 2 juin 2016, par laquelle le ministre de la défense lui a refusé l'accès aux données susceptibles de le concerner figurant dans les traitements automatisés de données de la direction générale de la sécurité extérieure (DGSE).<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de la sécurité intérieure ;<br/>
              - la loi n° 78-17 du 6 janvier 1978 ; <br/>
              - la loi n° 2000-516 du 15 juin 2000 ;<br/>
              - le décret n° 2005-1309 du 20 octobre 2005 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une séance à huis-clos, d'une part, M.A..., et d'autre part, le ministre de la défense et la Commission nationale de l'informatique et des libertés, qui ont été mis à même de prendre la parole avant les conclusions ;<br/>
<br/>
              et après avoir entendu en séance :<br/>
              - le rapport de Mme Catherine de Salins, conseiller d'Etat,<br/>
              - et, hors la présence des parties, les conclusions de Mme Béatrice <br/>
Bourgeois-Machureau, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Aux termes de l'article 41 de la loi du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés : " Par dérogation aux articles 39 et 40, lorsqu'un traitement intéresse la sûreté de l'Etat, la défense ou la sécurité publique, le droit d'accès s'exerce dans les conditions prévues par le présent article pour l'ensemble des informations qu'il contient. / La demande est adressée à la commission qui désigne l'un de ses membres appartenant ou ayant appartenu au Conseil d'Etat, à la Cour de cassation ou à la Cour des comptes pour mener les investigations utiles et faire procéder aux modifications nécessaires. Celui-ci peut se faire assister d'un agent de la commission. Il est notifié au requérant qu'il a été procédé aux vérifications. / Lorsque la commission constate, en accord avec le responsable du traitement, que la communication des données qui y sont contenues ne met pas en cause ses finalités, la sûreté de l'Etat, la défense ou la sécurité publique, ces données peuvent être communiquées au requérant. / Lorsque le traitement est susceptible de comprendre des informations dont la communication ne mettrait pas en cause les fins qui lui sont assignées, l'acte réglementaire portant création du fichier peut prévoir que ces informations peuvent être communiquées au requérant par le gestionnaire du fichier directement saisi ". Aux termes de l'article 88 du décret du 20 octobre 2005 pris pour l'application de cette loi : " Aux termes de ses investigations, la commission constate, en accord avec le responsable du traitement, celles des informations susceptibles d'être communiquées au demandeur dès lors que leur communication ne met pas en cause les finalités du traitement, la sûreté de l'Etat, la défense ou la sécurité publique. Elle transmet au demandeur ces informations (...). Lorsque le responsable du traitement s'oppose à la communication au demandeur de tout ou partie des informations le concernant, la commission l'informe qu'il a été procédé aux vérifications nécessaires. / La commission peut constater en accord avec le responsable du traitement, que les informations concernant le demandeur doivent être rectifiées ou supprimées et qu'il y a lieu de l'en informer. En cas d'opposition du responsable du traitement, la commission se borne à informer le demandeur qu'il a été procédé aux vérifications nécessaires. Lorsque le traitement ne contient aucune information concernant le demandeur, la commission informe celui-ci, avec l'accord du responsable du traitement. / En cas d'opposition du responsable du traitement, la commission se borne à informer le demandeur qu'il a été procédé aux vérifications nécessaires. / La réponse de la commission mentionne les voies et délais de recours ouverts au demandeur ".<br/>
<br/>
              2. L'article 26 de la loi du 6 janvier 1978 dispose : " I. Sont autorisés par arrêté du ou des ministres compétents, pris après avis motivé et publié de la Commission nationale de l'informatique et des libertés, les traitements de données à caractère personnel mis en oeuvre pour le compte de l'Etat et : / 1° Qui intéressent la sûreté de l'Etat, la défense ou la sécurité publique ; (...) / L'avis de la commission est publié avec l'arrêté autorisant le traitement. / II. Ceux de ces traitements qui portent sur des données mentionnées au I de l'article 8 sont autorisés par décret en Conseil d'Etat pris après avis motivé et publié de la commission ; cet avis est publié avec le décret autorisant le traitement. / III. Certains traitements mentionnés au I et au II peuvent être dispensés, par décret en Conseil d'Etat, de la publication de l'acte réglementaire qui les autorise ; pour ces traitements, est publié, en même temps que le décret autorisant la dispense de publication de l'acte, le sens de l'avis émis par la commission (...) ".<br/>
<br/>
              3. L'article L. 841-2 du code de la sécurité intérieure, issu de la loi du 24 juillet 2015 relative au renseignement, dispose que : " Le Conseil d'Etat est compétent pour connaître, dans les conditions prévues au chapitre III bis du titre VII du livre VII du code de justice administrative, des requêtes concernant la mise en oeuvre de l'article 41 de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, pour les traitements ou parties de traitements intéressant la sûreté de l'Etat dont la liste est fixée par décret en Conseil d'Etat ". L'article R. 841-2 de ce code prévoit que : " Relèvent des dispositions de l'article L. 841-2 du présent code les traitements ou parties de traitements automatisés de données à caractère personnel intéressant la sûreté de l'Etat autorisés par les actes réglementaires ou dispositions suivants : (...)/2° Décret portant application des dispositions de l'article 31 de la loi n° 78-17 du 6 janvier 1978 aux fichiers d'informations nominatives mis en oeuvre par la direction générale de la sécurité extérieure  (...) ".<br/>
<br/>
              4. L'article L. 773-8 du code de justice administrative, issu de la loi du 24 juillet 2015, dispose que : " Lorsqu'elle traite des requêtes relatives à la mise en oeuvre de l'article 41 de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, la formation de jugement se fonde sur les éléments contenus, le cas échéant, dans le traitement sans les révéler ni révéler si le requérant figure ou non dans le traitement. Toutefois, lorsqu'elle constate que le traitement ou la partie de traitement faisant l'objet du litige comporte des données à caractère personnel le concernant qui sont inexactes, incomplètes, équivoques ou périmées, ou dont la collecte, l'utilisation, la communication ou la conservation est interdite, elle en informe le requérant, sans faire état d'aucun élément protégé par le secret de la défense nationale. Elle peut ordonner que ces données soient, selon les cas, rectifiées, mises à jour ou effacées. Saisie de conclusions en ce sens, elle peut indemniser le requérant. " . L'article <br/>
R. 773-20 du même code précise que : " Le défendeur indique au Conseil d'Etat, au moment du dépôt de ses mémoires et pièces, les passages de ses productions et, le cas échéant, de celles de la Commission nationale de contrôle des techniques de renseignement, qui sont protégés par le secret de la défense nationale. /Les mémoires et les pièces jointes produits par le défendeur et, le cas échéant, par la Commission nationale de contrôle des techniques de renseignement sont communiqués au requérant, à l'exception des passages des mémoires et des pièces qui, soit comportent des informations protégées par le secret de la défense nationale, soit confirment ou infirment la mise en oeuvre d'une technique de renseignement à l'égard du requérant, soit divulguent des éléments contenus dans le traitement de données, soit révèlent que le requérant figure ou ne figure pas dans le traitement. /Lorsqu'une intervention est formée, le président de la formation spécialisée ordonne, s'il y a lieu, que le mémoire soit communiqué aux parties, et à la Commission nationale de contrôle des techniques de renseignement, dans les mêmes conditions et sous les mêmes réserves que celles mentionnées à l'alinéa précédent ".<br/>
<br/>
              5. Il ressort des pièces du dossier que M. A...a saisi, le <br/>
11 janvier 2016, la Commission nationale de l'informatique et des libertés (CNIL) afin de pouvoir accéder aux données le concernant contenues dans le fichier de la direction générale de la sécurité extérieure (DGSE). La CNIL a désigné, en application de l'article 41 de la loi du 6 janvier 1978, un membre pour mener toutes investigations utiles et faire procéder, le cas échéant, aux modifications nécessaires. Par lettre du 2 juin 2016, la présidente de la CNIL a informé M. A...qu'il avait été procédé à l'ensemble des vérifications demandées et que la procédure était terminée, sans apporter à l'intéressé d'autres informations. M. A...demande l'annulation pour excès de pouvoir du refus, révélé par ce courrier, de lui donner accès aux mentions susceptibles de le concerner et figurant dans le fichier litigieux.<br/>
<br/>
              6. Le ministre de la défense et la CNIL ont communiqué au Conseil d'Etat, dans les conditions prévues à l'article R. 773-20 du code de justice administrative, les éléments relatifs à la situation de l'intéressé. Par ce même mémoire, le ministre de la défense a en outre communiqué l'acte réglementaire autorisant la création du fichier litigieux.<br/>
<br/>
              7. Il appartient à la formation spécialisée, créée par l'article L. 773-2 du code de justice administrative, saisie de conclusions dirigées contre le refus de communiquer les données relatives à une personne qui allègue être mentionnée dans un fichier figurant à l'article R. 841-2 du code de la sécurité intérieure, de vérifier, au vu des éléments qui lui ont été communiqués hors la procédure contradictoire, si le requérant figure ou non dans le fichier litigieux. Dans l'affirmative, il lui appartient d'apprécier si les données y figurant sont pertinentes au regard des finalités poursuivies par ce fichier, adéquates et proportionnées. Lorsqu'il apparaît soit que le requérant n'est pas mentionné dans le fichier litigieux, soit que les données à caractère personnel le concernant qui y figurent ne sont entachées d'aucune illégalité, la formation de jugement rejette les conclusions du requérant sans autre précision. Dans le cas où des informations relatives au requérant figurent dans le fichier litigieux et apparaissent entachées d'illégalité, soit que les données à caractère personnel le concernant sont inexactes, incomplètes, équivoques ou périmées, soit que leur collecte ou leur utilisation, leur communication ou leur conservation est interdite, elle en informe le requérant, sans faire état d'aucun élément protégé par le secret de la défense nationale. Cette circonstance, le cas échéant relevée d'office par le juge dans les conditions prévues à l'article R. 773-21 du code de justice administrative, implique nécessairement que l'autorité gestionnaire du fichier rétablisse la légalité en effaçant ou en rectifiant, dans la mesure du nécessaire, les données litigieuses. En pareil cas, doit être annulée la décision implicite refusant de procéder à un tel effacement ou une telle rectification.<br/>
<br/>
              8. Il résulte de l'examen par la formation de jugement de l'acte règlementaire autorisant la création du fichier litigieux ainsi que des éléments fournis par le ministre de la défense et la CNIL, qui s'est déroulé selon les modalités décrites au point précédent, qui sont, contrairement à ce qui est soutenu, de nature à garantir de manière effective la présomption d'innocence, le droit au respect de la vie privée et familiale posé par l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, le droit à un procès équitable rappelé à l'article 6 de la même convention, les droits de la défense ainsi que le droit à un recours juridictionnel effectif et qui ne méconnaissent pas, en tout état de cause, l'article 5 de cette convention, que les conclusions de M. A...doivent être rejetées.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. A...est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. B...A...et au ministre de la défense. <br/>
Copie en sera adressée à la Commission nationale de l'informatique et des libertés.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-07-06 DROITS CIVILS ET INDIVIDUELS. - TRAITEMENTS AUTOMATISÉS INTÉRESSANT LA SÛRETÉ DE L'ETAT - LOI N° 2015-912 DU 24 JUILLET 2015 - CONTRÔLE JURIDICTIONNEL - OFFICE DE LA FORMATION SPÉCIALISÉE [RJ1] - 1) MODALITÉS D'INSTRUCTION SPÉCIFIQUES - 2) DÉCISION - A) LORSQUE LE REQUÉRANT N'EST PAS MENTIONNÉ DANS LE TRAITEMENT OU EN L'ABSENCE D'ILLÉGALITÉ - B) EN PRÉSENCE D'UNE ILLÉGALITÉ.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">49-03 POLICE. ÉTENDUE DES POUVOIRS DE POLICE. - TECHNIQUES DE RENSEIGNEMENT (LOI N° 2015-912 DU 24 JUILLET 2015) - CONTRÔLE JURIDICTIONNEL - TRAITEMENTS AUTOMATISÉS INTÉRESSANT LA SÛRETÉ DE L'ETAT - OFFICE DE LA FORMATION SPÉCIALISÉE [RJ1] - 1) MODALITÉS D'INSTRUCTION SPÉCIFIQUES - 2) DÉCISION - A) LORSQUE LE REQUÉRANT N'EST PAS MENTIONNÉ DANS LE TRAITEMENT OU EN L'ABSENCE D'ILLÉGALITÉ - B) EN PRÉSENCE D'UNE ILLÉGALITÉ.
</SCT>
<ANA ID="9A"> 26-07-06 1) Il appartient à la formation spécialisée, créée par l'article L. 773-2 du code de justice administrative, saisie de conclusions dirigées contre le refus de communiquer les données relatives à une personne qui allègue être mentionnée dans un fichier figurant à l'article R. 841-2 du code de la sécurité intérieure, de vérifier, au vu des éléments qui lui ont été communiqués hors la procédure contradictoire, si le requérant figure ou non dans le fichier litigieux. Dans l'affirmative, il lui appartient d'apprécier si les données y figurant sont pertinentes au regard des finalités poursuivies par ce fichier, adéquates et proportionnées.,,,2) a) Lorsqu'il apparaît soit que le requérant n'est pas mentionné dans le fichier litigieux, soit que les données à caractère personnel le concernant qui y figurent ne sont entachées d'aucune illégalité, la formation de jugement rejette les conclusions du requérant sans autre précision.... ,,b) Dans le cas où des informations relatives au requérant figurent dans le fichier litigieux et apparaissent entachées d'illégalité, soit que les données à caractère personnel le concernant sont inexactes, incomplètes, équivoques ou périmées, soit que leur collecte ou leur utilisation, leur communication ou leur conservation est interdite, elle en informe le requérant, sans faire état d'aucun élément protégé par le secret de la défense nationale. Cette circonstance, le cas échéant relevée d'office par le juge dans les conditions prévues à l'article R. 773-21 du code de justice administrative, implique nécessairement que l'autorité gestionnaire du fichier rétablisse la légalité en effaçant ou en rectifiant, dans la mesure du nécessaire, les données litigieuses. En pareil cas, doit être annulée la décision implicite refusant de procéder à un tel effacement ou une telle rectification.</ANA>
<ANA ID="9B"> 49-03 1) Il appartient à la formation spécialisée, créée par l'article L. 773-2 du code de justice administrative, saisie de conclusions dirigées contre le refus de communiquer les données relatives à une personne qui allègue être mentionnée dans un fichier figurant à l'article R. 841-2 du code de la sécurité intérieure, de vérifier, au vu des éléments qui lui ont été communiqués hors la procédure contradictoire, si le requérant figure ou non dans le fichier litigieux. Dans l'affirmative, il lui appartient d'apprécier si les données y figurant sont pertinentes au regard des finalités poursuivies par ce fichier, adéquates et proportionnées.,,,2) a) Lorsqu'il apparaît soit que le requérant n'est pas mentionné dans le fichier litigieux, soit que les données à caractère personnel le concernant qui y figurent ne sont entachées d'aucune illégalité, la formation de jugement rejette les conclusions du requérant sans autre précision.... ,,b) Dans le cas où des informations relatives au requérant figurent dans le fichier litigieux et apparaissent entachées d'illégalité, soit que les données à caractère personnel le concernant sont inexactes, incomplètes, équivoques ou périmées, soit que leur collecte ou leur utilisation, leur communication ou leur conservation est interdite, elle en informe le requérant, sans faire état d'aucun élément protégé par le secret de la défense nationale. Cette circonstance, le cas échéant relevée d'office par le juge dans les conditions prévues à l'article R. 773-21 du code de justice administrative, implique nécessairement que l'autorité gestionnaire du fichier rétablisse la légalité en effaçant ou en rectifiant, dans la mesure du nécessaire, les données litigieuses. En pareil cas, doit être annulée la décision implicite refusant de procéder à un tel effacement ou une telle rectification.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., avant l'entrée en vigueur de la loi n° 2015-912 du 24 juillet 2015 relative au renseignement, CE, 11 juillet 2016, Ministre de l'intérieur et ministre de la défense c/ M.,, n°s 375977 376457, à publier au Recueil. Comp., en matière de mise en oeuvre des techniques de renseignement, décision du même jour, M.,, n° 396958, à publier au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
