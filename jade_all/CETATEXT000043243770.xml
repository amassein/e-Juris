<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043243770</ID>
<ANCIEN_ID>JG_L_2021_03_000000426352</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/24/37/CETATEXT000043243770.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 10/03/2021, 426352, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-03-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>426352</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON, MEGRET</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:426352.20210310</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au tribunal administratif de Toulon de condamner l'Etat à lui verser une somme de 24 000 euros en réparation du préjudice qu'il estime avoir subi en raison de l'absence d'indemnisation de services qu'il a effectués en qualité de citoyen volontaire de la police nationale du 16 mars 2009 au 26 mars 2013. Par un jugement n° 1401656 du 5 février 2016, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 16MA01304 du 15 octobre 2018, la cour administrative d'appel de Marseille a rejeté l'appel formé par M. A... contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 17 décembre 2018, 18 mars 2019 et 5 septembre 2019 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 2003-239 du 18 mars 2003 ;<br/>
              - la loi n° 2007-297 du 5 mars 2007 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Boré, Salve de Bruneton, Mégret, avocat de M. A....<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article 7, alors en vigueur, de la loi du 18 mars 2003 pour la sécurité intérieure : " Les périodes d'emploi des réservistes et des volontaires du service volontaire citoyen de la police nationale sont indemnisées ". Il ressort de ces dispositions, éclairées par les travaux préparatoires de la loi du 5 mars 2007 relative à la prévention de la délinquance dont elles sont issues, que l'indemnisation des volontaires du service volontaire citoyen de la police nationale leur est due à raison du service qu'ils effectuent lors de leurs périodes d'emploi. <br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que M. A..., engagé en qualité de volontaire du service volontaire citoyen de la police nationale par un contrat du 16 mars 2009, tacitement reconduit jusqu'à sa radiation par décision du 26 mars 2013, a demandé la condamnation de l'Etat à lui verser les sommes qu'il estimait lui être dues au titre de l'indemnisation des services effectués par lui durant cette période. Il se pourvoit en cassation contre l'arrêt du 15 octobre 2018 par lequel la cour administrative d'appel de Marseille a rejeté son appel formé contre le jugement du 5 février 2016 par lequel le tribunal administratif de Toulon a rejeté sa demande.<br/>
<br/>
              3. Il résulte des termes de l'arrêt attaqué que, pour écarter tout droit à indemnisation de M. A..., la cour administrative d'appel s'est fondée sur ce que celui-ci n'établissait ni la nature ni le montant des frais qu'il avait exposés lors de ses périodes de service. Il résulte de ce qui a été dit au point 1 qu'en statuant ainsi, alors qu'il était constant que l'intéressé avait effectué plusieurs périodes d'emploi qui n'avaient pas été indemnisées, elle a entaché son arrêt d'une erreur de droit. <br/>
<br/>
              4. M. A... est, par suite, sans qu'il soit besoin de se prononcer sur les autres moyens de son pourvoi, fondé à demander l'annulation de l'arrêt qu'il attaque.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3 000 euros à verser à M. A... au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 15 octobre 2018 de la cour administrative d'appel de Marseille est annulé.<br/>
<br/>
		Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
<br/>
Article 3 : L'Etat versera la somme de 3 000 euros à M. A... au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. B... A... et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
