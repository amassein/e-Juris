<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036733450</ID>
<ANCIEN_ID>JG_L_2018_03_000000418019</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/73/34/CETATEXT000036733450.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 16/03/2018, 418019, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-03-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418019</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP KRIVINE, VIAUD</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2018:418019.20180316</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au juge des référés du tribunal administratif de Pau, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'une part, de suspendre l'exécution de la décision du 16 janvier 2018 par laquelle le préfet des Pyrénées-Atlantiques a refusé d'enregistrer sa demande d'asile et, d'autre part, d'enjoindre au préfet d'enregistrer cette demande, sous astreinte de 120 euros par jour de retard. Par une ordonnance n° 1800190 du 30 janvier 2018, le juge des référés du tribunal administratif de Pau a, d'une part, suspendu l'exécution de la décision du 30 juin 2017, par laquelle le préfet des Pyrénées-Atlantiques a décidé du transfert de M. A...vers la Norvège et, d'autre part, enjoint à l'Etat de procéder à l'enregistrement de la demande d'asile de M. A... et de lui délivrer une attestation de demande d'asile.<br/>
<br/>
              Par une requête enregistrée le 9 février 2018 au secrétariat du contentieux du Conseil d'Etat, le ministre d'Etat, ministre de l'intérieur demande au juge des référés du Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de rejeter les conclusions de première instance de M. A....<br/>
<br/>
<br/>
              Il soutient :<br/>
              - que l'ordonnance contestée est entachée d'irrégularité dès lors que le juge des référés du tribunal administratif de Pau a statué ultra petita en suspendant l'exécution des  décisions des 30 juin 2017 et 16 janvier 2018 ;<br/>
              - qu'elle est entachée d'une erreur dans l'appréciation de l'urgence dès lors que, d'une part, M. A... n'a jamais demandé l'annulation de la décision du 30 juin 2017 de transfert aux autorités norvégiennes et n'a saisi le juge des référés que le 28 janvier 2018 et que, d'autre part, la circonstance que son attestation de demandeur d'asile soit arrivée à expiration le 15 octobre 2017 et n'ait pas été renouvelée est imputable à son seul comportement et qu'enfin, il n'allègue pas être exposé en cas de transfert en Norvège à un risque de traitements inhumains ou dégradants ou à des défaillances systémiques de la procédure d'asile ;<br/>
              - que le juge des référés du tribunal administratif de Pau a entaché son ordonnance d'une erreur de droit en ce qu'il a considéré que l'attestation de demandeur d'asile de M. A...ne lui conférait aucun droit au maintien sur le territoire français ;<br/>
              - que l'ordonnance contestée est entachée d'une erreur d'appréciation en ce qu'elle juge qu'il est porté une atteinte grave et manifestement illégale au droit d'asile de M. A..., alors que le comportement de l'intéressé caractérise un refus réitéré d'exécuter volontairement une décision de transfert et que, par suite, le délai de six mois à compter de l'acceptation de la demande de prise en charge à l'issue duquel l'Etat français serait devenu responsable de la demande d'asile de M. A... a été porté à dix-huit mois ;<br/>
              - qu'il n'était pas nécessaire que l'Etat prenne en charge le pré-acheminement de M. A...de son lieu de résidence au lieu d'embarquement.<br/>
<br/>
              Par un mémoire en défense, enregistré le 25 février 2018, M. A...conclut, à titre principal, au rejet de la requête et, à titre subsidiaire, à ce qu'il soit fait droit à ses conclusions de première instance. Il demande en outre que la somme de 2 000 euros soit mise à la charge de l'Etat sur le fondement de l'article L. 761-1 du code de justice administrative. Il soutient que les moyens de la requête ne sont pas fondés.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le règlement (UE) n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, le ministre d'Etat, ministre de l'intérieur, d'autre part, M.A...; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du mardi 6 mars 2018 à 14 heures au cours de laquelle ont été entendus :<br/>
<br/>
              - les représentants du ministre d'Etat, ministre de l'intérieur ;<br/>
<br/>
              - Me Krivine, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. A... ;<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 7 mars 2018 après la clôture de l'instruction, présentées par le ministre d'Etat, ministre de l'intérieur ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". <br/>
<br/>
              2. Le droit constitutionnel d'asile, qui a le caractère d'une liberté fondamentale, a pour corollaire le droit de solliciter le statut de réfugié. S'il implique que l'étranger qui sollicite la reconnaissance de la qualité de réfugié soit, en principe, autorisé à demeurer sur le territoire jusqu'à ce qu'il ait été statué sur sa demande, ce droit s'exerce dans les conditions définies par le code de l'entrée et du séjour des étrangers et du droit d'asile. L'article L. 742-3 de ce code prévoit que l'étranger dont l'examen de la demande d'asile relève de la responsabilité d'un autre Etat peut faire l'objet d'un transfert vers l'Etat qui est responsable de cet examen en application des dispositions du règlement du Parlement européen et du Conseil du 26 juin 2013. Ce transfert peut avoir lieu pendant une période de six mois à compter de l'acceptation de la demande de prise en charge, susceptible d'être portée à dix-huit mois dans les conditions prévues à l'article 29 de ce règlement si l'intéressé " prend la fuite ", cette notion devant s'entendre comme visant le cas où un ressortissant étranger se serait soustrait de façon intentionnelle et systématique au contrôle de l'autorité administrative en vue de faire obstacle à une mesure d'éloignement le concernant.<br/>
<br/>
              3. Il résulte de l'instruction que M. A..., de nationalité afghane, est entré sur le territoire français le 20 mars 2017 et a présenté une demande d'asile en France. La consultation du fichier " Eurodac " ayant permis d'établir que ses empreintes avaient été relevées en Norvège, une demande de reprise en charge a été adressée aux autorités norvégiennes. Elle a été acceptée le 10 avril 2017, date à laquelle a commencé à courir le délai de six mois dont disposaient les autorités françaises, en application de l'article 29 du règlement du Parlement européen et du Conseil du 26 juin 2013, pour procéder au transfert de M. A...vers la Norvège. Par une décision du 30 juin 2017, le préfet des Pyrénées-Atlantiques a décidé le transfert de M. A... vers la Norvège. Les autorités françaises ont déclaré le 28 septembre 2017 M. A...en fuite et ont informé les autorités norvégiennes de ce que le délai prévu à l'article 29 du règlement du 26 juin 2013 était porté à dix-huit mois. Elles ont refusé en conséquence, le 16 janvier 2018, d'enregistrer sa nouvelle demande d'asile en France. M. A...a demandé au juge des référés du tribunal administratif de Pau, sur le fondement de l'article L. 521-2 du code de justice administrative, d'une part, de suspendre l'exécution de la décision du 16 janvier 2018 et, d'autre part, d'enjoindre au préfet des Pyrénées-Atlantiques d'enregistrer sa demande d'asile, sans délai. Par une ordonnance du 30 janvier 2018, le juge des référés du tribunal administratif de Pau a, d'une part, suspendu l'exécution de la décision du 30 juin 2017 par laquelle le préfet des Pyrénées-Atlantiques a décidé le transfert de M. A...vers la Norvège et, d'autre part, enjoint à l'Etat de procéder à l'enregistrement de la demande d'asile de M. A... et de lui délivrer une attestation de demande d'asile. Le ministre de l'Etat, ministre de l'intérieur, relève appel de cette ordonnance. <br/>
<br/>
<br/>
<br/>
              Sur les conclusions dirigées contre l'ordonnance attaquée en tant qu'elle suspend l'exécution de la décision de transfert du 30 juin 2017 :<br/>
<br/>
              4. Il ressort des écritures de M. A...devant le juge des référés du tribunal administratif que celui-ci ne demandait pas la suspension de l'exécution de cette décision. Il y a lieu, par suite, d'annuler l'ordonnance attaquée en tant qu'elle procède à cette suspension. <br/>
<br/>
              Sur les conclusions dirigées contre l'ordonnance attaquée en tant qu'elle enjoint à l'Etat d'enregistrer la demande d'asile de M.A... : <br/>
<br/>
              5. Il résulte de l'instruction qu'à la suite de la décision prononçant son transfert vers la Norvège, M. A...a été convoqué à se présenter le 28 août 2017 à l'aéroport de Pau-Uzein en vue de sa reconduite vers ce pays. M. A...ne s'étant pas présenté à l'aéroport, les services préfectoraux l'ont convoqué à nouveau, en vue de procéder à son transfert, à se présenter le 26 septembre 2017 au même aéroport, ce que l'intéressé s'est abstenu de faire. M. A... n'a ainsi pas réagi aux diligences accomplies par l'administration pour procéder à son transfert vers la Norvège et a attendu l'expiration du délai de six mois pour formuler une nouvelle demande d'asile. Il n'a fait état d'aucune impossibilité matérielle l'ayant empêché à deux reprises de se rendre à la convocation qui lui avait adressée, et la circonstance que l'hébergement où il se trouvait était à une dizaine de kilomètres de l'aéroport ne saurait être regardée comme constituant une telle impossibilité. L'intéressé ne peut, en outre utilement invoquer le fait que l'administration n'aurait pas mis en oeuvre les moyens matériels et humains permettant de le conduire à l'aéroport. La circonstance qu'il ait respecté les obligations de pointage liées à son assignation à résidence ayant pris fin le 30 octobre 2017 est, enfin, sans incidence sur le fait qu'en refusant de déférer à ces convocations, il a fait obstacle de manière intentionnelle et systématique à la décision de transfert le concernant. Dès lors, le préfet a pu légalement estimer que M. A...était en fuite au sens de l'article 29 du règlement du 26 juin 2013 et porter pour ce motif à dix-huit mois le délai d'exécution de la décision de transfert. Par suite, le préfet n'a porté aucune atteinte grave et manifestement illégale au droit d'asile de l'intéressé en refusant d'enregistrer sa demande présentée le 16 janvier 2018. Il en résulte que le ministre est fondé à soutenir que c'est à tort que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Pau a prononcé la suspension de cette décision et enjoint à l'Etat de procéder à la demande d'enregistrement présentée par M.A.... Par les mêmes motifs, les conclusions présentées par M. A...sur le fondement de l'article L. 521-2 du code de justice administrative doivent être rejetées.<br/>
<br/>
              6. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à sa charge de l'Etat, qui n'est pas la partie perdante dans la présente instance, la somme que M. A...demande sur ce fondement.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : Les articles 2, 3 et 4 de l'ordonnance du 30 janvier 2018 du juge des référés du tribunal administratif de Pau sont annulés. <br/>
Article 2 : Les conclusions présentées par M. A...devant le juge des référés du tribunal administratif de Pau sur le fondement des articles L. 521-2 et L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 3 : La présente ordonnance sera notifiée au ministre d'Etat, ministre de l'intérieur et à M. B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
