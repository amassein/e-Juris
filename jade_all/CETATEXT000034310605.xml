<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034310605</ID>
<ANCIEN_ID>JG_L_2017_03_000000392940</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/31/06/CETATEXT000034310605.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 1ère chambres réunies, 29/03/2017, 392940</TITRE>
<DATE_DEC>2017-03-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>392940</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR>M. Stéphane Decubber</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:392940.20170329</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société civile immobilière (SCI) Maryse a demandé au tribunal administratif de Rennes d'annuler pour excès de pouvoir l'arrêté du 12 juin 2012 par lequel le maire de la commune de Belz a retiré le permis de construire qui lui avait été tacitement accordé le 21 avril 2012 pour la pose de six lucarnes et l'agrandissement d'une véranda sur un ensemble immobilier situé sur l'île de Saint-Cado. Par un jugement n° 1202911 du 16 mai 2014, le tribunal administratif de Rennes a rejeté sa requête.<br/>
<br/>
              Par un arrêt n° 14NT01920 du 26 juin 2015, la cour administrative d'appel de Nantes a rejeté l'appel formé par la SCI Maryse.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés au secrétariat du contentieux du Conseil d'Etat les 25 août et 20 novembre 2015, la SCI Maryse demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt de la cour administrative d'appel de Nantes ;<br/>
<br/>
              2°) de renvoyer l'affaire devant une cour administrative d'appel, sur le fondement de l'article L. 821-2 du code de justice administrative, pour qu'elle soit jugée au fond ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Belz la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'urbanisme ;<br/>
              - le code du patrimoine ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Stéphane Decubber, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de la SCI Maryse et à la SCP Didier, Pinet, avocat de la commune de Belz ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces soumises aux juges du fond que la SCI Maryse, qui est propriétaire d'un ensemble immobilier situé place de Saint-Cado à Belz (Morbihan), a, le 21 octobre 2011, sollicité un permis de construire afin de créer six lucarnes en façade et d'agrandir une véranda en créant une surface hors oeuvre nette de 50 m² ; que le terrain d'assiette du projet étant situé dans le périmètre de protection de la chapelle Saint-Cado, immeuble inscrit à l'inventaire des monuments historiques au titre des dispositions des articles L. 621-1 et suivants du code du patrimoine, le maire de la commune de Belz a, par un courrier en date du 25 octobre 2011, informé la société requérante de l'allongement du délai d'instruction de la demande à six mois au motif de la situation de l'immeuble dans ce périmètre de protection ; que l'architecte des bâtiments de France a rendu, le 20 janvier 2012, un avis favorable au projet assorti de prescriptions, qu'il a régulièrement notifié à la commune ; qu'estimant que le silence qu'il avait gardé pendant six mois avait fait naître une décision tacite d'acceptation de la demande de permis de construire le 21 avril 2012, le maire de la commune de Belz a entendu retirer ce permis de construire par un arrêté du 12 juin 2012, dont la SCI Maryse a demandé l'annulation pour excès de pouvoir devant le tribunal administratif de Rennes ; qu'après avoir estimé qu'aucune décision tacite d'acceptation n'était née le 21 avril 2012 mais qu'au contraire avait été prise à cette même date une décision implicite de rejet, que l'arrêté du 12 juin 2012 n'avait fait que confirmer expressément, le tribunal administratif de Rennes a requalifié les conclusions de la SCI Maryse contre cette décision comme étant dirigées contre le refus implicite de délivrer le permis de construire sollicité et les a rejetées par un jugement du 16 mai 2014 ; que la cour administrative d'appel de Nantes a rejeté l'appel de la SCI Maryse contre ce jugement par un arrêt du 26 juin 2015 contre lequel la société se pourvoit en cassation ; <br/>
<br/>
              2. Considérant, en premier lieu, d'une part, que le délai d'instruction des demandes de permis de construire est, en vertu de l'article R. 423-28 du code de l'urbanisme dans sa rédaction alors en vigueur : " porté à six mois : / (...) b) Lorsqu'un permis de construire ou d'aménager porte sur un projet situé dans le périmètre de protection des immeubles classés ou inscrits au titre des monuments historiques ou dans un secteur sauvegardé dont le plan de sauvegarde et de mise en valeur n'est pas approuvé ; (...) " ; qu'aux termes de l'article R. 423-42 du même code : " Lorsque le délai d'instruction de droit commun est modifié en application des articles R. 423-24 à R. 423-33, l'autorité compétente indique au demandeur ou à l'auteur de la déclaration, dans le délai d'un mois à compter de la réception ou du dépôt du dossier à la mairie : / a) Le nouveau délai et, le cas échéant, son nouveau point de départ ; / b) Les motifs de la modification de délai ; " ; qu'il résulte de ces dispositions que le demandeur d'un permis de construire dont le délai d'instruction est modifié doit être informé que sa demande fait l'objet d'un délai d'instruction modifié ainsi que des motifs de cette modification du délai d'instruction ;<br/>
<br/>
              3. Considérant, d'autre part, qu'aux termes de l'article L. 424-2 du même code : " Le permis est tacitement accordé si aucune décision n'est notifiée au demandeur à l'issue du délai d'instruction. / Un décret en Conseil d'Etat précise les cas dans lesquels un permis tacite ne peut être acquis " ; qu'aux termes de l'article R. 424-3 du même code, dans sa rédaction alors en vigueur : " Par exception au b de l'article R. 424-1, le défaut de notification d'une décision expresse dans le délai d'instruction vaut décision implicite de rejet lorsque la décision est soumise à l'accord de l'architecte des Bâtiments de France et que celui-ci a notifié, dans le délai mentionné à l'article R. 423-67, un avis défavorable ou un avis favorable assorti de prescriptions " ; qu'aux termes de l'article R. 424-4 du même code, dans sa rédaction alors en vigueur : " Dans le cas prévu à l'article précédent, l'architecte des Bâtiments de France adresse copie de son avis au demandeur et lui fait savoir qu'en conséquence de cet avis il ne pourra pas se prévaloir d'un permis tacite " ; qu'il résulte de ces dispositions que s'il incombe à l'architecte des Bâtiments de France d'adresser au demandeur d'un permis de construire dont la délivrance est soumise à son accord copie de son avis lorsque celui-ci est défavorable ou favorable mais assorti de prescriptions et d'informer alors le demandeur qu'il ne pourra pas se prévaloir d'un permis tacite, la non-exécution de cette formalité, dont le seul objet est l'information du demandeur, ne peut avoir pour effet l'acquisition d'un permis tacite ; qu'au demeurant, lorsqu'il n'a pas reçu copie de l'avis de l'architecte des Bâtiments de France, le demandeur, qui a été informé de ce que le délai d'instruction était allongé en raison de la nécessité de recueillir l'avis favorable de cette autorité, a la faculté de se renseigner, auprès du service instructeur, sur le sens de l'avis rendu ; que, dès lors, c'est sans erreur de droit que la cour administrative d'appel de Nantes a jugé que la circonstance que l'architecte des Bâtiments de France avait omis d'adresser à la requérante copie de son avis favorable assorti de prescriptions n'avait pu avoir pour effet de faire naître un permis tacite ;<br/>
<br/>
              4. Considérant, en second lieu, qu'aux termes du premier alinéa du III de l'article L. 146-4 du code de l'urbanisme alors en vigueur : " En dehors des espaces urbanisés, les constructions ou installations sont interdites sur une bande littorale de cent mètres à compter de la limite haute du rivage ou des plus hautes eaux pour les plans d'eau intérieurs désignés à l'article 2 de la loi n° 86-2 du 3 janvier 1986 précitée. " ; qu'en jugeant que l'ensemble des espaces entourant le terrain d'assiette de la construction envisagée et proches de celui-ci ne constituait pas un espace caractérisé par une densité significative des constructions et que, dès lors, cette construction ne se trouvait pas dans un espace urbanisé au sens de ces dispositions, la cour s'est livrée à une appréciation souveraine des faits exempte de dénaturation ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que le pourvoi de la SCI Maryse doit être rejeté ;<br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la SCI Maryse la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative qu'elle versera à la commune de Belz ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
      Article 1er : Le pourvoi de la SCI Maryse est rejeté.<br/>
<br/>
Article 2 : La SCI Maryse versera à la commune de Belz la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la SCI Maryse et à la commune de Belz.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">41-01-05-03 MONUMENTS ET SITES. MONUMENTS HISTORIQUES. MESURES APPLICABLES AUX IMMEUBLES SITUÉS DANS LE CHAMP DE VISIBILITÉ D'UN ÉDIFICE CLASSÉ OU INSCRIT. PERMIS DE CONSTRUIRE. - PERMIS DE CONSTRUIRE DONT LA DÉLIVRANCE EST SOUMISE À L'ACCORD DE L'ABF - SILENCE VALANT DÉCISION IMPLICITE DE REJET LORSQUE L'ABF ÉMET UN AVIS DÉFAVORABLE OU UN AVIS FAVORABLE ASSORTI DE PRESCRIPTIONS - CAS OÙ L'ABF OMET D'ADRESSER UNE COPIE DE SON AVIS AU DEMANDEUR - NAISSANCE D'UN PERMIS TACITE - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-03-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. PROCÉDURE D'ATTRIBUTION. - PERMIS DE CONSTRUIRE DONT LA DÉLIVRANCE EST SOUMISE À L'ACCORD DE L'ABF - SILENCE VALANT DÉCISION IMPLICITE DE REJET LORSQUE L'ABF ÉMET UN AVIS DÉFAVORABLE OU UN AVIS FAVORABLE ASSORTI DE PRESCRIPTIONS - CAS OÙ L'ABF OMET D'ADRESSER UNE COPIE DE SON AVIS AU DEMANDEUR - NAISSANCE D'UN PERMIS TACITE - ABSENCE.
</SCT>
<ANA ID="9A"> 41-01-05-03 Il résulte des articles L. 424-2, R. 424-3 et R. 424-4 du code de l'urbanisme que, s'il incombe à l'architecte des Bâtiments de France (ABF) d'adresser au demandeur d'un permis de construire dont la délivrance est soumise à son accord copie de son avis lorsque celui-ci est défavorable ou favorable mais assorti de prescriptions et d'informer alors le demandeur qu'il ne pourra pas se prévaloir d'un permis tacite, la non-exécution de cette formalité, dont le seul objet est l'information du demandeur, ne peut avoir pour effet l'acquisition d'un permis tacite.</ANA>
<ANA ID="9B"> 68-03-02 Il résulte des articles L. 424-2, R. 424-3 et R. 424-4 du code de l'urbanisme que, s'il incombe à l'architecte des Bâtiments de France (ABF) d'adresser au demandeur d'un permis de construire dont la délivrance est soumise à son accord copie de son avis lorsque celui-ci est défavorable ou favorable mais assorti de prescriptions et d'informer alors le demandeur qu'il ne pourra pas se prévaloir d'un permis tacite, la non-exécution de cette formalité, dont le seul objet est l'information du demandeur, ne peut avoir pour effet l'acquisition d'un permis tacite.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
