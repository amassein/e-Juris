<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035317215</ID>
<ANCIEN_ID>JG_L_2017_07_000000394811</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/31/72/CETATEXT000035317215.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 28/07/2017, 394811, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-07-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>394811</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Sabine Monchambert</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:394811.20170728</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une requête, enregistrée le 23 novembre 2015 au secrétariat du contentieux du Conseil d'Etat, l'association Entraide universitaire, la Fédération des établissements hospitaliers et d'aide à la personne (FEHAP), l'Association des paralysés de France (APF), l'association &#140;uvre Falret, la Fédération des associations pour adultes et jeunes handicapés (APAJH), la Fédération nationale des associations de parents et amis employeurs et gestionnaires d'établissements et services pour personnes handicapées mentales (FEGAPEI), l'Union nationale des associations de parents de personnes handicapées mentales et de leurs amis (UNAPEI) et l'Union nationale interfédérale des oeuvres et organismes privés non lucratifs sanitaires et sociaux (UNIOPSS) demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du ministre des finances et des comptes publics et du ministre des affaires sociales, de la santé et des droits des femmes du 18 mai 2015 fixant les tarifs plafonds prévus au deuxième alinéa de l'article L. 314-4 du code de l'action sociale et des familles applicables aux établissements et services mentionnés au a) du 5° du I de l'article L. 312-1 du même code ainsi que la décision de rejet de leur recours gracieux formé contre cet arrêté ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'action sociale et des familles ;<br/>
              - l'arrêté du ministre des finances et des comptes publics et du ministre des affaires sociales et de la santé du 30 avril 2014 fixant les tarifs plafonds prévus au deuxième alinéa de l'article L. 314-4 du code de l'action sociale et des familles applicables aux établissements et services mentionnés au a du 5° du I de l'article L. 312-1 du même code ;<br/>
              - la décision du Conseil d'Etat statuant au contentieux n° 382652 du 7 avril 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sabine Monchambert, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il résulte du second alinéa de l'article L. 314-4 du code de l'action sociale et des familles que le montant total annuel des dépenses prises en compte pour le calcul des dotations globales de fonctionnement des établissements et services d'aide par le travail mentionnés au a) du 5° du I de l'article L. 312-1 du même code " est constitué en dotations régionales limitatives. Le montant de ces dotations régionales est fixé (...) en fonction des besoins de la population, des priorités définies au niveau national en matière de politique médico-sociale, en tenant compte de l'activité et des coûts moyens des établissements et services et d'un objectif de réduction progressive des inégalités dans l'allocation des ressources entre régions. A cet effet, un arrêté interministériel fixe, annuellement, les tarifs plafonds ou les règles de calcul desdits tarifs plafonds (...) ainsi que les règles permettant de ramener les tarifs pratiqués au niveau des tarifs plafonds ". Pour l'application de ces dispositions, un arrêté du ministre des finances et des comptes publics et du ministre des affaires sociales, de la santé et des droits des femmes du 18 mai 2015 a fixé les tarifs plafonds applicables aux établissements et services d'aide par le travail pour l'année 2015. Eu égard aux moyens qu'elles soulèvent, les associations requérantes doivent être regardées comme demandant l'annulation pour excès de pouvoir de l'article 1er de cet arrêté en tant qu'il prévoit l'opposabilité des tarifs plafonds aux établissements et services ayant conclu un contrat pluriannuel d'objectifs et de moyens à compter du 21 mai 2014, ainsi que de l'article 2 de cet arrêté, qui fixe le niveau des tarifs plafonds pour 2015. <br/>
<br/>
              Sur les conclusions à fin d'annulation pour excès de pouvoir :<br/>
<br/>
              En ce qui concerne l'article 1er de l'arrêté attaqué :<br/>
<br/>
              2. En raison des effets qui s'y attachent, l'annulation pour excès de pouvoir d'un acte administratif, qu'il soit ou non réglementaire, emporte, lorsque le juge est saisi de conclusions recevables, l'annulation par voie de conséquence des décisions administratives consécutives qui n'auraient pu légalement être prises en l'absence de l'acte annulé ou qui sont, en l'espèce, intervenues en raison de l'acte annulé.<br/>
<br/>
              3. Aux termes de l'article L. 313-11 du code de l'action sociale et des familles, dans sa rédaction applicable au litige : " (...) des contrats pluriannuels peuvent être conclus entre les personnes physiques et morales gestionnaires d'établissements et services et la ou les autorités chargées de l'autorisation et, le cas échéant, les organismes de protection sociale, afin notamment de permettre la réalisation des objectifs retenus par le schéma d'organisation sociale et médico-sociale dont ils relèvent, la mise en oeuvre du projet d'établissement ou de service ou de la coopération des actions sociales et médico-sociales. / Ces contrats fixent les obligations respectives des parties signataires et prévoient les moyens nécessaires à la réalisation des objectifs poursuivis, sur une durée maximale de cinq ans notamment dans le cadre de la tarification. Dans ce cas, les tarifs annuels ne sont pas soumis à la procédure budgétaire annuelle prévue aux II et III de l'article L. 314-7. (...) ". Aux termes de l'article R. 314-40 du même code, dans sa rédaction en vigueur à la date de l'arrêté attaqué : " Les éléments pluriannuels du budget sont fixés dans le cadre, soit du contrat pluriannuel prévu par l'article L. 313-11, soit de la convention pluriannuelle mentionnée au I de l'article L. 313-12. / Le contrat ou la convention comportent alors un volet financier qui fixe, par groupes fonctionnels ou par section tarifaire selon la catégorie d'établissement ou de service, et pour la durée de la convention, les modalités de fixation annuelle de la tarification. / Ces modalités peuvent consister : / 1° Soit en l'application directe à l'établissement ou au service du taux d'évolution des dotations régionales limitatives mentionnées aux articles L. 314-3 et L. 314-4 ; / 2° Soit en l'application d'une formule fixe d'actualisation ou de revalorisation ; / 3° Soit en la conclusion d'avenants annuels d'actualisation ou de revalorisation ".<br/>
<br/>
              4. Par une décision du 7 avril 2016, le Conseil d'Etat statuant au contentieux a annulé, au motif qu'elles méconnaissaient l'article R. 314-40 du code de l'action sociale et des familles, les dispositions du troisième alinéa de l'article 1er de l'arrêté du ministre des finances et des comptes publics et du ministre des affaires sociales et de la santé du 30 avril 2014 fixant les tarifs plafonds applicables aux établissements et services d'aide par le travail pour l'année 2014, publié au Journal officiel de la République française du 20 mai 2014, qui prévoyaient que : " Les contrats pluriannuels d'objectifs et de moyens signés à compter de la date de la publication du présent arrêté comportent un volet financier prévoyant, par groupe fonctionnel et pour la durée du contrat, les modalités de fixation annuelle de la tarification conformes aux règles permettant de ramener les tarifs pratiqués au niveau des tarifs plafonds ".  <br/>
<br/>
              5. Le second alinéa de l'article 1er de l'arrêté attaqué dispose que les tarifs plafonds sont opposables aux établissements et services d'aide par le travail à l'exception de ceux ayant conclu un contrat tel que le contrat pluriannuel d'objectifs et de moyens, mentionné à l'article L. 313-11 du code de l'action sociale et des familles, " avant le 21 mai 2014 et en cours de validité pour l'année 2015 ". Les auteurs de l'arrêté ont ainsi tiré les conséquences de l'obligation faite, par l'arrêté du 30 avril 2014, aux contrats pluriannuels d'objectifs et de moyens nouvellement signés de comporter un volet financier prévoyant les modalités de fixation annuelle de la tarification conformes aux règles permettant de ramener les tarifs pratiqués au niveau des tarifs plafonds. <br/>
<br/>
              6. A la date de l'arrêté attaqué, il est constant que le pouvoir réglementaire n'avait pas modifié les dispositions de l'article R. 314-40 du code de l'action sociale et des familles relatives aux modalités de fixation annuelle de la tarification applicable aux établissements et services ayant conclu un contrat pluriannuel d'objectifs et de moyens. Il y a donc lieu d'annuler, par voie de conséquence de l'annulation du 3e alinéa de l'article 1er de l'arrêté du 30 avril 2014, les mots " avant le 21 mai 2014 et " figurant à l'article 1er de l'arrêté attaqué, qui sont divisibles des autres dispositions du même article, ainsi que, dans la même mesure, la décision implicite rejetant le recours gracieux des associations requérantes. Le moyen retenu suffisant à entraîner cette annulation, il n'est pas nécessaire d'examiner les autres moyens de la requête dirigés contre les mêmes dispositions.<br/>
<br/>
              En ce qui concerne l'article 2 de l'arrêté attaqué :<br/>
<br/>
              7. Il résulte des dispositions citées au point 1 de l'article L. 314-4 du code de l'action sociale et des familles que la fixation des tarifs plafonds applicables aux établissements et services d'aide par le travail doit, comme la détermination des dotations régionales limitatives mentionnées au même article, être arrêtée en fonction des besoins de la population et des priorités définies au niveau national en matière de politique médico-sociale, en tenant compte de l'activité et des coûts moyens de ces structures ainsi que d'un objectif de réduction progressive des inégalités dans l'allocation des ressources entre régions.<br/>
<br/>
              8. Il ressort des pièces du dossier que les ministres ont fixé le tarif plafond de référence et les tarifs plafonds majorés pour 2015, comme ceux qu'ils avaient fixés pour 2014, au vu des résultats d'une étude de coûts réalisée sur la base de données issues de la quasi-totalité des comptes administratifs approuvés relatifs à l'exercice 2008, leur permettant d'appréhender les coûts moyens des établissements et services d'aide par le travail, y compris les charges liées à leur implantation immobilière, et d'identifier certains des facteurs expliquant les écarts à la moyenne. D'une part, les associations requérantes ne font pas valoir de circonstance propre à modifier significativement la dispersion des coûts entre établissements et services ou les facteurs expliquant les écarts au coût moyen de fonctionnement par rapport à l'année précédente. D'autre part, pour réduire progressivement les inégalités dans l'allocation des ressources entre régions, l'arrêté se borne à procéder au gel, à un niveau correspondant au montant des charges nettes autorisé au titre de l'année 2014, de la dotation globale de financement pour 2015 des établissements et services dont le tarif au 31 décembre 2014 est supérieur aux tarifs plafonds, lesquels ont été fixés à un niveau supérieur au coût net moyen à la place, tel qu'il peut être calculé à partir des données de l'exercice 2013, tant au niveau national que dans les régions marquées par le coût élevé de l'immobilier, telles les régions Ile-de-France et Provence-Alpes-Côte d'Azur. Dans ces conditions, les associations requérantes ne sont pas fondées à soutenir que les ministres ne pouvaient, dans l'attente des résultats des travaux engagés pour mieux connaître les coûts des établissements et services et réformer leur tarification, se fonder sur les données utilisées pour la fixation des tarifs plafonds précédents et que l'arrêté attaqué serait entaché d'erreur manifeste d'appréciation en ce qu'il fixe les tarifs plafonds pour 2015 au même niveau que ceux de 2014.<br/>
<br/>
              9. Par suite, les associations requérantes ne sont pas fondées à demander l'annulation de l'article 2 de l'arrêté qu'elles attaquent, non plus que, dans la même mesure, la décision implicite rejetant leur recours gracieux.<br/>
<br/>
              Sur les effets de l'annulation prononcée :<br/>
<br/>
              10. Il ne ressort pas des pièces du dossier que l'annulation de l'arrêté attaqué en tant qu'il rend les tarifs plafonds qu'il fixe opposables aux établissements et services d'aide par le travail ayant conclu un contrat pluriannuel à compter du 21 mai 2014 soit de nature à emporter des conséquences manifestement excessives en raison tant des effets que cet acte a produits que des situations qui ont pu se constituer lorsqu'il était en vigueur. Ainsi, il n'y a pas lieu, dans les circonstances de l'espèce, de limiter les effets de l'annulation de cette disposition.<br/>
<br/>
              Sur les frais exposés par les parties à l'occasion du litige :<br/>
<br/>
              11. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 300 euros à verser à chacune des associations requérantes au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                         --------------<br/>
<br/>
Article 1er : Les mots " avant le 21 mai 2014 et " figurant à l'article 1er de l'arrêté du ministre des finances et des comptes publics et du ministre des affaires sociales, de la santé et des droits des femmes du 18 mai 2015, ainsi que, dans cette mesure, la décision implicite rejetant le recours gracieux des associations requérantes, sont annulés.<br/>
Article 2 : L'Etat versera une somme de 300 euros à chacune des associations requérantes au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Le surplus des conclusions de la requête de l'association Entraide universitaire et des autres associations requérantes est rejeté.<br/>
Article 4 : La présente décision sera notifiée, pour l'ensemble des associations requérantes, à l'association Entraide universitaire, première dénommée, au ministre de l'action et des comptes publics et à la ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
