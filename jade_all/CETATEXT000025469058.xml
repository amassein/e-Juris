<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025469058</ID>
<ANCIEN_ID>JG_L_2012_03_000000339851</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/46/90/CETATEXT000025469058.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 09/03/2012, 339851</TITRE>
<DATE_DEC>2012-03-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>339851</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Christian Vigouroux</PRESIDENT>
<AVOCATS>SCP PIWNICA, MOLINIE ; DE NERVO</AVOCATS>
<RAPPORTEUR>Mme Christine Allais</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Geffray</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:339851.20120309</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et les mémoires complémentaires, enregistrés les 21 mai, 23 août et 7 décembre 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour le DEPARTEMENT DE LA MOSELLE, représenté par le président du conseil général ; le DEPARTEMENT DE LA MOSELLE demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09NC00432 du 18 mars 2010 par lequel la cour administrative d'appel de Nancy a rejeté l'appel qu'il a interjeté du jugement n° 0605831-0605832 du 21 janvier 2009 par lequel le tribunal administratif de Strasbourg a annulé les décisions des 2 juin et 2 octobre 2006 par lesquelles le président du conseil général a retiré à Mme A son agrément d'assistante maternelle puis rejeté le recours gracieux formé par l'intéressée contre ce retrait ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de Mme A la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'action sociale et des familles ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Christine Allais, chargée des fonctions de Maître des Requêtes, <br/>
<br/>
              - les observations de la SCP Piwnica, Molinié, avocat du DEPARTEMENT DE LA MOSELLE et de Me de Nervo, avocat de Mme A, <br/>
<br/>
              - les conclusions de M. Edouard Geffray, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Piwnica, Molinié, avocat du DEPARTEMENT DE LA MOSELLE et à Me de Nervo, avocat de Mme A ;<br/>
<br/>
<br/>
<br/>Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'après avoir suspendu, par une décision du 6 mars 2006, l'agrément dont bénéficiait Mme Catherine A pour exercer la profession d'assistante maternelle, le président du conseil général de la Moselle a procédé au retrait de cet agrément par une décision du 2 juin 2006, confirmée par une décision du 2 octobre 2006 à la suite d'un recours gracieux de l'intéressée, au motif qu'une suspicion d'agression sexuelle d'un membre de son entourage sur un enfant confié à sa garde ne permettait pas de garantir la santé, la sécurité et l'épanouissement des mineurs accueillis ; que le DEPARTEMENT DE LA MOSELLE se pourvoit en cassation contre l'arrêt du 18 mars 2010 par lequel la cour administrative d'appel de Nancy, confirmant un jugement du 21 janvier 2009 du tribunal administratif de Strasbourg, a annulé ces deux décisions à la demande de Mme A ;<br/>
<br/>
              Considérant que, contrairement à ce que soutient le département, la cour administrative d'appel de Nancy a visé et analysé son mémoire en réplique du 8 octobre 2009, en même temps que sa requête d'appel ; que, par suite, le moyen tiré de ce que l'arrêt attaqué aurait été rendu en méconnaissance des dispositions de l'article R. 741-2 du code de justice administrative manque en fait ;<br/>
<br/>
              Considérant qu'aux termes de l'article L. 421-3 du code de l'action sociale et des familles, dans sa rédaction en vigueur à la date des décisions litigieuses : " L'agrément nécessaire pour exercer la profession d'assistant maternel ou d'assistant familial est délivré par le président du conseil général du département où le demandeur réside. (...) / L'agrément est accordé à ces deux professions si les conditions d'accueil garantissent la sécurité, la santé et l'épanouissement des mineurs et majeurs de moins de vingt et un ans accueillis, en tenant compte des aptitudes éducatives de la personne (...) " ; qu'aux termes de l'article L. 421-6 du même code, dans sa rédaction applicable au litige : "  (...) Si les conditions de l'agrément cessent d'être remplies, le président du conseil général peut, après avis d'une commission consultative paritaire départementale, modifier le contenu de l'agrément ou procéder à son retrait. En cas d'urgence, le président du conseil général peut suspendre l'agrément. Tant que l'agrément reste suspendu, aucun enfant ne peut être confié (...) " ;<br/>
<br/>
              Considérant qu'il résulte de ces dispositions qu'il incombe au président du conseil général de s'assurer que les conditions d'accueil garantissent la sécurité, la santé et l'épanouissement des enfants accueillis et de procéder au retrait de l'agrément si ces conditions ne sont plus remplies ; qu'à cette fin, dans l'hypothèse où il est informé de suspicions de comportements susceptibles de compromettre la santé, la sécurité ou l'épanouissement d'un enfant,  notamment de suspicions d'agression sexuelle, de la part du bénéficiaire de l'agrément ou de son entourage, il lui appartient de tenir compte de tous les éléments portés à la connaissance des services compétents du département ou recueillis par eux et de déterminer si ces éléments sont suffisamment établis pour lui permettre raisonnablement de penser que l'enfant est victime des comportements en cause ou risque de l'être ; <br/>
<br/>
              Considérant, par ailleurs, que si la légalité d'une décision doit être appréciée à la date à laquelle elle a été prise, il incombe cependant au juge de l'excès de pouvoir de tenir compte, le cas échéant, d'éléments objectifs antérieurs à cette date mais révélés postérieurement ; <br/>
<br/>
              Considérant qu'il résulte de ce qui a été dit ci-dessus, d'une part, qu'après avoir relevé, par une appréciation souveraine exempte de dénaturation, que les seuls faits invoqués pour justifier le retrait d'agrément contesté devant elle étaient ceux à partir desquels avait été diligentée une enquête préliminaire confiée à la gendarmerie à la suite d'un signalement et d'une plainte d'un tiers ultérieurement classée sans suite et que la suspicion d'agression sexuelle par un membre de l'entourage de Mme A était née des seuls propos de l'enfant, dont aucun autre élément tiré de l'enquête administrative ne venait conforter la crédibilité, la cour administrative d'appel de Nancy a pu, sans commettre d'erreur de droit, juger que le président du conseil général n'avait pu procéder légalement, sur la base des éléments de fait qu'il avait retenus, au retrait d'agrément litigieux ; que, d'autre part, pour apprécier la légalité de cette décision, la cour administrative d'appel a pu, sans commettre d'erreur de droit, tenir compte, par un motif d'ailleurs surabondant, de la circonstance que la plainte déposée contre un membre de l'entourage de Mme A a été classée sans suite par le parquet du tribunal de grande instance de Metz à l'issue d'une enquête préliminaire, postérieurement à la décision contestée devant elle ;<br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que le DEPARTEMENT DE LA MOSELLE n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du DEPARTEMENT DE LA MOSELLE le versement à Mme A d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ; que les dispositions de cet article font en revanche obstacle à ce que soit mis à la charge de Mme A, qui n'est pas, dans la présente instance, la partie perdante, le versement d'une somme au DEPARTEMENT DE LA MOSELLE au titre des frais exposés par lui et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du DEPARTEMENT DE LA MOSELLE est rejeté.<br/>
Article 2 : Le DEPARTEMENT DE LA MOSELLE versera une somme de 3 000 euros à Mme A au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée au DEPARTEMENT DE LA MOSELLE et à Mme Catherine A.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-02-02-02-01 AIDE SOCIALE. DIFFÉRENTES FORMES D'AIDE SOCIALE. AIDE SOCIALE À L'ENFANCE. PLACEMENT DES MINEURS. PLACEMENT FAMILIAL. - ASSISTANTES MATERNELLES (ART. L. 421-3 DU CASF) - OBLIGATION POUR LE PRÉSIDENT DU CONSEIL GÉNÉRAL DE RETRAIT DE L'AGRÉMENT SI LES CONDITIONS MISES À SON OCTROI NE SONT PLUS REMPLIES.
</SCT>
<ANA ID="9A"> 04-02-02-02-01 Il résulte des dispositions des articles L. 421-3 et L. 421-6 du code de l'action sociale et des familles (CASF), qu'il incombe au président du conseil général de s'assurer que les conditions d'accueil garantissent la sécurité, la santé et l'épanouissement des enfants accueillis et de procéder au retrait de l'agrément si ces conditions ne sont plus remplies. A cette fin, dans l'hypothèse où il est informé de suspicions de comportements susceptibles de compromettre la santé, la sécurité ou l'épanouissement d'un enfant, notamment de suspicions d'agression sexuelle, de la part du bénéficiaire de l'agrément ou de son entourage, il lui appartient de tenir compte de tous les éléments portés à la connaissance des services compétents du département ou recueillis par eux et de déterminer si ces éléments sont suffisamment établis pour lui permettre raisonnablement de penser que l'enfant est victime des comportements en cause ou risque de l'être.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
