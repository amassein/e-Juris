<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027377291</ID>
<ANCIEN_ID>JG_L_2013_04_000000364240</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/37/72/CETATEXT000027377291.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 29/04/2013, 364240</TITRE>
<DATE_DEC>2013-04-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>364240</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CAPRON, CAPRON</AVOCATS>
<RAPPORTEUR>Mme Esther de Moustier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:364240.20130429</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le mémoire, enregistré le 1er mars 2013, présenté pour Mme B...A..., en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 ; elle demande au Conseil d'Etat, à l'appui de son pourvoi tendant à l'annulation du jugement n° 1000587 du 28 juin 2012 du tribunal administratif de Pau, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du II de l'article 1691 bis du code général des impôts ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule et ses articles 1er et 61-1 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
              Vu le code général des impôts, notamment son article 1691 bis ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Esther de Moustier, Auditeur, <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Capron, Capron, avocat de Mme A... ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé, y compris pour la première fois en cassation, à l'occasion d'une instance devant le Conseil d'Etat (...) " ; qu'il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et qu'elle soit nouvelle ou présente un caractère sérieux ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article 1691 bis du code général des impôts : " I. - Les époux et les partenaires liés par un pacte civil de solidarité sont tenus solidairement au paiement : / 1° De l'impôt sur le revenu lorsqu'ils font l'objet d'une imposition commune ; / 2° De la taxe d'habitation lorsqu'ils vivent sous le même toit. / II. - 1. Les personnes divorcées ou séparées peuvent demander à être déchargées des obligations de paiement prévues au I ainsi qu'à l'article 1723 ter-00 B lorsque, à la date de la demande : / a) Le jugement de divorce ou de séparation de corps a été prononcé ; / b) La déclaration conjointe de dissolution du pacte civil de solidarité établie par les partenaires ou la signification de la décision unilatérale de dissolution du pacte civil de solidarité de l'un des partenaires a été enregistrée au greffe du tribunal d'instance ; / c) Les intéressés ont été autorisés à avoir des résidences séparées ;/ d) L'un ou l'autre des époux ou des partenaires liés par un pacte civil de solidarité a abandonné le domicile conjugal ou la résidence commune. / 2. La décharge de l'obligation de paiement est accordée en cas de disproportion marquée entre le montant de la dette fiscale et, à la date de la demande, la situation financière et patrimoniale, nette de charges, du demandeur. (...) / 3. Le bénéfice de la décharge de l'obligation de paiement est subordonné au respect des obligations déclaratives du demandeur prévues par les articles 170 et 885 W à compter de la date de la fin de la période d'imposition commune.(...) " ; <br/>
<br/>
              3. Considérant que le II de l'article 1691 bis du code général des impôts est applicable au présent litige au sens et pour l'application de l'article 23-5 de l'ordonnance du 7 novembre 1958 ; que cette disposition n'a pas déjà été déclarée conforme à la Constitution par le Conseil constitutionnel ; que le moyen tiré de ce qu'elle porte atteinte aux droits et libertés garantis par la Constitution, notamment au principe d'égalité devant la loi garanti par l'article 6 de la Déclaration des droits de l'homme et du citoyen, soulève une question présentant un caractère sérieux ; qu'ainsi, il y a lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : La question de la conformité à la Constitution des dispositions du II de l'article 1691 bis du code général des impôts est renvoyée au Conseil constitutionnel.<br/>
<br/>
Article 2 : Il est sursis à statuer sur le pourvoi de Mme A... jusqu'à ce que le Conseil constitutionnel ait tranché la question de constitutionnalité ainsi soulevée. <br/>
<br/>
		Article 3 : La présente décision sera notifiée à Mme B...A.... <br/>
Copie en sera adressée au Premier ministre et au ministre de l'économie et des finances. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-05 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. - CONSEIL D'ETAT SAISI D'UN LITIGE RELEVANT D'UNE AUTRE JURIDICTION ADMINISTRATIVE - QPC SOULEVÉE DEVANT LUI - RENVOI DANS LE DÉLAI DE TROIS MOIS DU DOSSIER AVEC LA QPC À LA JURIDICTION COMPÉTENTE OU EXAMEN PAR LE CONSEIL D'ETAT DE LA QPC ET RENVOI ULTÉRIEUR DE L'AFFAIRE (SOL. IMPL.).
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-10 PROCÉDURE. - CONSEIL D'ETAT SAISI D'UN LITIGE RELEVANT D'UNE AUTRE JURIDICTION ADMINISTRATIVE - QPC SOULEVÉE DEVANT LUI - RENVOI DANS LE DÉLAI DE TROIS MOIS DU DOSSIER AVEC LA QPC À LA JURIDICTION COMPÉTENTE OU EXAMEN PAR LE CONSEIL D'ETAT DE LA QPC ET RENVOI ULTÉRIEUR DE L'AFFAIRE (SOL. IMPL.).
</SCT>
<ANA ID="9A"> 17-05 Saisi d'une question prioritaire de constitutionnalité (QPC) soulevée à l'occasion d'un litige qui relève de la compétence d'une autre juridiction administrative, le Conseil d'Etat peut, dans le délai de trois mois, renvoyer le dossier avec la QPC à la juridiction compétente. Il peut aussi se prononcer lui-même sur la QPC et renvoyer ultérieurement l'affaire à la juridiction compétente.</ANA>
<ANA ID="9B"> 54-10 Saisi d'une question prioritaire de constitutionnalité (QPC) soulevée à l'occasion d'un litige qui relève de la compétence d'une autre juridiction administrative, le Conseil d'Etat peut, dans le délai de trois mois, renvoyer le dossier avec la QPC à la juridiction compétente. Il peut aussi se prononcer lui-même sur la QPC et renvoyer ultérieurement l'affaire à la juridiction compétente.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
