<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034166799</ID>
<ANCIEN_ID>JG_L_2017_03_000000403916</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/16/67/CETATEXT000034166799.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 10/03/2017, 403916, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-03-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>403916</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Vincent Villette</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:403916.20170310</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un mémoire et un mémoire en réplique , enregistrés le 29 décembre 2016 et le 6 février 2017 au secrétariat du contentieux du Conseil d'Etat, présentés en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958, l'association Regards citoyens demande au Conseil d'Etat, à l'appui de sa requête tendant à l'annulation du décret n° 2016-1036 du 28 juillet 2016 relatif au principe et aux modalités de fixation des redevances de réutilisation des informations du secteur public, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des articles L. 324-1, L. 324-2, L. 324-4 et L. 324-5 du code des relations entre le public et l'administration.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule et  son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - la directive n° 2013/37/UE du Parlement européen et du Conseil du 26 juin 2013 modifiant la directive 2003/98/CE concernant la réutilisation des informations du secteur public ;<br/>
              - le code des relations entre le public et l'administration ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Villette, auditeur, <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. Aux termes de l'article 88-1 de la Constitution : " La République participe à l'Union européenne constituée d'États qui ont choisi librement d'exercer en commun certaines de leurs compétences en vertu du traité sur l'Union européenne et du traité sur le fonctionnement de l'Union européenne, tels qu'ils résultent du traité signé à Lisbonne le 13 décembre 2007 ". En l'absence de mise en cause d'une règle ou d'un principe inhérent à l'identité constitutionnelle de la France, le Conseil constitutionnel juge qu'il n'est pas compétent pour contrôler la conformité aux droits et libertés que la Constitution garantit de dispositions législatives qui se bornent à tirer les conséquences nécessaires de dispositions inconditionnelles et précises d'une directive de l'Union européenne et qu'en ce cas, il n'appartient qu'au juge de l'Union européenne, saisi le cas échéant à titre préjudiciel, de contrôler le respect par cette directive des droits fondamentaux garantis par l'article 6 du Traité sur l'Union européenne.<br/>
<br/>
              3. En l'absence de mise en cause, à l'occasion d'une question prioritaire de constitutionnalité soulevée sur des dispositions législatives se bornant à tirer les conséquences nécessaires de dispositions précises et inconditionnelles d'une directive de l'Union européenne, d'une règle ou d'un principe inhérent à l'identité constitutionnelle de la France, une telle question n'est pas au nombre de celles qu'il appartient au Conseil d'Etat de transmettre au Conseil constitutionnel sur le fondement de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel.<br/>
<br/>
              4. En l'espèce, les dispositions issues de l'article 15 de la loi du 28 décembre 2015 relative à la gratuité et aux modalités de réutilisation des informations du secteur public du code de la propriété intellectuelle, aujourd'hui reprises au code des relations entre le public et l'administration, dont la conformité aux droits et libertés que la Constitution garantit est contestée, se bornent à tirer les conséquences nécessaires des dispositions précises et inconditionnelles des articles 6 et 7 de la directive 2003/98/CE concernant la réutilisation des informations du secteur public dans leur rédaction résultant de la directive du 26 juin 2013 du Parlement européen et du Conseil concernant la protection juridique des bases de données, sans mettre en cause une règle ou un principe inhérent à l'identité constitutionnelle de la France. Il n'y a, dès lors, pas lieu de transmettre au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée, sans qu'il soit besoin de statuer sur les fins de non recevoir opposées par le Premier ministre.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par l'association Regards citoyens.<br/>
Article 2 : La présente décision sera notifiée à l'association Regards citoyens, au ministre de l'économie et des finances et au Premier ministre. <br/>
Copie en sera adressée au Conseil constitutionnel.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
