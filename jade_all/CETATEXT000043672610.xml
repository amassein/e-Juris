<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043672610</ID>
<ANCIEN_ID>JG_L_2021_06_000000437366</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/67/26/CETATEXT000043672610.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 16/06/2021, 437366</TITRE>
<DATE_DEC>2021-06-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>437366</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; SCP THOUIN-PALAT, BOUCARD ; SCP HEMERY, THOMAS-RAQUIN, LE GUERER</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:437366.20210616</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Mme B... A... a porté plainte contre Mme C... D... devant la chambre disciplinaire de première instance du conseil régional de l'ordre des masseurs-kinésithérapeutes d'Occitanie. Le conseil départemental des Pyrénées-Orientales de l'ordre des masseurs-kinésithérapeutes s'est associé à la plainte. Par une décision du 19 avril 2018, la chambre disciplinaire a infligé à Mme D... la sanction de l'interdiction temporaire d'exercer la profession de masseur-kinésithérapeute pour une durée de trois mois, assortie du sursis.<br/>
<br/>
              Par une décision du 4 novembre 2019, la chambre disciplinaire nationale de l'ordre des masseurs-kinésithérapeutes a rejeté l'appel formé par Mme D... contre cette décision. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 6 janvier, 5 juin et 7 octobre 2020 au secrétariat du contentieux du Conseil d'Etat, Mme D... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge du Conseil national de l'ordre des masseurs-kinésithérapeutes et de Mme A... la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ; <br/>
              - la loi n° 2005-882 du 2 août 2005 ;<br/>
              - le code de justice administrative<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteure publique.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Thouin-Palat, Boucard, avocat de Mme D..., à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de Mme A... et à la SCP Hémery, Thomas-Raquin, Le Guerer, avocat du Conseil national de l'ordre des masseurs-kinésithérapeutes.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un contrat signé le 31 octobre 2011, Mme D..., masseur-kinésithérapeute, s'est engagée en qualité d'assistant collaborateur auprès de Mme A..., qui exerce également la profession de masseur-kinésithérapeute. A la suite de la résiliation unilatérale de ce contrat par Mme D... le 6 février 2016, Mme A... a porté plainte contre elle devant les instances de leur ordre, au motif, d'une part, qu'elle aurait interrompu dès le début de l'année 2015 le reversement de la quote-part d'honoraires due au titre d'actes effectués dans un établissement d'hébergement pour personnes âgées dépendantes (EHPAD), d'autre part, qu'elle aurait, après la résiliation du contrat, violé la clause de non-concurrence qui y figurait en s'installant dans un cabinet situé dans la même commune que le sien et en poursuivant son activité dans l'EHPAD en question. Par une décision du 19 avril 2018, la chambre disciplinaire de première instance a infligé à Mme D... la sanction de l'interdiction temporaire d'exercer la profession de masseur-kinésithérapeute pour une durée de trois mois, assortie du sursis. Mme D... se pourvoit en cassation contre la décision du 4 novembre 2019 par laquelle la chambre disciplinaire nationale a rejeté son appel formé contre cette décision.  <br/>
<br/>
              2. Il ressort des termes de la décision attaquée que, pour rejeter l'appel de Mme D..., la chambre disciplinaire nationale a d'abord jugé que cette dernière n'avait pas respecté deux clauses du contrat de collaboration du 31 octobre 2011 la liant à Mme A..., relatives, pour la première, à l'obligation de reversement d'une partie de ses honoraires et, pour la seconde, à l'obligation d'exercer, à l'issue de leur collaboration, dans des lieux situés à une certaine distance du lieu d'exercice de Mme A.... La chambre disciplinaire a ensuite jugé que la violation par Mme D... de la première des deux clauses du contrat constituait une méconnaissance de la règle fixée à l'article R. 4321-99 du code de la santé publique, aux termes duquel : " Les masseurs-kinésithérapeutes entretiennent entre eux des rapports de bonne confraternité (...) " et que la violation de la seconde clause constituait une méconnaissance de la règle fixée à l'article R. 4321-100 du même code, aux termes duquel : " Le détournement ou la tentative de détournement de clientèle sont interdits ".<br/>
<br/>
              Sur l'application, par la chambre disciplinaire nationale, des clauses du contrat de collaboration du 31 octobre 2011 :<br/>
<br/>
              3. Il appartient au juge disciplinaire, lorsqu'il est saisi d'un grief tiré de ce qu'un masseur-kinésithérapeute aurait méconnu ses obligations déontologiques en ne respectant pas une clause d'un contrat de droit privé, notamment un contrat de collaboration le liant à un confrère, d'apprécier le respect de cette clause, dès lors qu'elle n'est, à la date du manquement, ni résiliée, ni annulée par une décision de justice, ni entachée d'une illégalité faisant obstacle à son application et susceptible d'être relevée d'office, ainsi que le serait par exemple une clause ayant par elle-même pour effet d'entraîner une violation des obligations déontologiques qui s'imposent à la profession. <br/>
<br/>
              En ce qui concerne l'applicabilité du contrat de collaboration du 31 octobre 2011 :<br/>
<br/>
              4. En premier lieu, les dispositions du III l'article 18 de la loi du 2 août 2005 en faveur des petites et moyennes entreprises, applicables au contrat en question, prévoient que : " Le contrat de collaboration libérale doit être conclu dans le respect des règles régissant la profession. / Ce contrat doit, à peine de nullité, être établi par écrit et préciser : / 1° Sa durée, indéterminée ou déterminée, en mentionnant dans ce cas son terme et, le cas échéant, les conditions de son renouvellement ; / (...) ". L'article R. 4321-131 du code de la santé publique, dans sa rédaction applicable à ce contrat, prévoit cependant que le contrat de collaboration libérale ne peut être qu'un contrat à durée déterminée, dans les termes suivants : " La durée de la collaboration libérale ne peut excéder quatre années. Passé ce délai, les modalités de la collaboration sont renégociées ".<br/>
<br/>
              5. Il ressort des pièces du dossier soumis aux juges du fond que l'article 9 du contrat de collaboration libérale du 31 octobre 2011 conclu entre Mme D... et Mme A... stipule que ce contrat, passé pour une durée initiale de deux ans, est tacitement reconductible. Si Mme D... soutient qu'une telle clause de reconduction tacite peut entraîner, en ce qu'elle est susceptible de prolonger le contrat initial au-delà d'une durée totale de quatre ans, une méconnaissance des dispositions de l'article R.4321-131 du code de la santé publique, cette circonstance n'est, en tout état de cause, pas de nature à entacher le contrat d'une illégalité faisant obstacle à son application. Par suite, dès lors qu'il est constant que le contrat n'avait été, avant la date à laquelle il a été résilié par Mme A..., ni résilié ni annulé par une décision de justice, la chambre disciplinaire nationale de l'ordre des masseurs-kinésithérapeutes a pu, sans erreur de droit, apprécier son respect pour statuer sur la poursuite disciplinaire dont elle était saisie.<br/>
<br/>
              En ce qui concerne la clause de non-concurrence de ce contrat de collaboration :<br/>
<br/>
              6. Il ressort des pièces du dossier soumis aux juges du fond que l'article 10 du contrat de collaboration libérale du 31 octobre 2011 stipule que si, au moment de sa résiliation, il s'est écoulé plus de trois mois depuis sa signature, Mme D... s'interdit d'exercer sa profession pendant une durée d'un an dans un rayon de cinq kilomètres autour du cabinet de Mme A.... En jugeant qu'une telle clause n'était pas entachée d'une illégalité faisant obstacle à son application, qui seule aurait été de nature à justifier que Mme D... puisse ne pas en respecter les termes sans méconnaître ses obligations déontologiques, la chambre disciplinaire nationale n'a pas commis d'erreur de droit.<br/>
<br/>
              Sur les autres moyens du pourvoi :<br/>
<br/>
              7. En estimant qu'il résultait clairement des stipulations de l'article 7 du contrat de collaboration libérale du 31 octobre 2011 qu'elles faisaient obligation à Mme D... de reverser à Mme A... 30% des honoraires tirés de l'intégralité des actes effectués par elle, y compris ceux pratiqués hors du cabinet de Mme A..., la chambre disciplinaire nationale a porté sur ces stipulations une appréciation souveraine, exempte de dénaturation. Elle n'a, par suite, pas commis d'erreur de droit en en déduisant que le refus de Mme D... de verser à Mme A..., à compter du début de l'année 2015, 30% des honoraires perçus dans l'EHPAD " Les tuiles vertes " constituait un manquement à ses obligations contractuelles, lui-même constitutif d'une méconnaissance du devoir de confraternité mentionné à l'article R. 4321-99 du code de la santé publique.<br/>
<br/>
              8. Enfin, le principe du libre choix du praticien qui résulte de l'article L. 1110-8 du code de la santé publique n'ayant pas pour effet de permettre à un professionnel de santé de méconnaître une interdiction d'exercice qui s'imposerait à lui, la requérante n'est pas fondée à soutenir qu'en jugeant que la poursuite de son exercice au sein de l'EHPAD " Les tuiles vertes " constituait, en raison de ce qu'elle violait une clause de non-concurrence, un manquement à une règle déontologique, la chambre disciplinaire nationale aurait méconnu le droit des résidents de cet établissement au libre choix de leur masseur-kinésithérapeute.<br/>
<br/>
              9. Il résulte de tout ce qui précède que le pourvoi de Mme D... doit être rejeté, y compris, par voie de conséquence, ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme D... la somme de 3 000 euros à verser à Mme A... au titre des mêmes dispositions. Enfin, le Conseil national de l'ordre des masseurs-kinésithérapeutes n'ayant pas la qualité de partie à la présente instance, ces mêmes dispositions font obstacle à ce que soit mise à la charge de Mme D... la somme qu'il demande à ce titre. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
		Article 1er : Le pourvoi de Mme D... est rejeté.<br/>
<br/>
Article 2 : Mme D... versera à Mme A... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : Les conclusions présentées par le Conseil national de l'ordre des masseurs-kinésithérapeutes au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 4 : La présente décision sera notifiée à Mme C... D..., à Mme B... A..., au Conseil national de l'ordre des masseurs-kinésithérapeutes et au conseil départemental des Pyrénées-Orientales de l'ordre des masseurs-kinésithérapeutes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-04-02-02 COMPÉTENCE. COMPÉTENCES CONCURRENTES DES DEUX ORDRES DE JURIDICTION. CONTENTIEUX DE L'APPRÉCIATION DE LA LÉGALITÉ. CAS OÙ UNE QUESTION PRÉJUDICIELLE NE S'IMPOSE PAS. - QUESTION PRÉJUDICIELLE DU JUGE ADMINISTRATIF AU JUGE JUDICIAIRE - VALIDITÉ DE LA CLAUSE D'UN CONTRAT DE DROIT PRIVÉ, NOTAMMENT DE COLLABORATION, INVOQUÉE PAR UN MASSEUR-KINÉSITHÉRAPEUTE POURSUIVI DEVANT SON ORDRE - 1) CONDITIONS [RJ1] - 2) ILLUSTRATION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">55-04-01-03 PROFESSIONS, CHARGES ET OFFICES. DISCIPLINE PROFESSIONNELLE. PROCÉDURE DEVANT LES JURIDICTIONS ORDINALES. POUVOIRS DU JUGE DISCIPLINAIRE. - SANCTION DE LA MÉCONNAISSANCE D'UNE CLAUSE D'UN CONTRAT DE DROIT PRIVÉ DONT LA VALIDITÉ EST CONTESTÉE - 1) CONDITIONS [RJ1] - 2) ILLUSTRATION.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">55-04-02-01-035 PROFESSIONS, CHARGES ET OFFICES. DISCIPLINE PROFESSIONNELLE. SANCTIONS. FAITS DE NATURE À JUSTIFIER UNE SANCTION. - INEXÉCUTION D'UNE CLAUSE D'UN CONTRAT DE DROIT PRIVÉ DONT LA VALIDITÉ EST CONTESTÉE - 1) CONDITIONS [RJ1] - 2) ILLUSTRATION.
</SCT>
<ANA ID="9A"> 17-04-02-02 1) Il appartient au juge disciplinaire, lorsqu'il est saisi d'un grief tiré de ce qu'un masseur-kinésithérapeute aurait méconnu ses obligations déontologiques en ne respectant pas une clause d'un contrat de droit privé, notamment un contrat de collaboration le liant à un confrère, d'apprécier le respect de cette clause, dès lors qu'elle n'est, à la date du manquement, ni résiliée, ni annulée par une décision de justice, ni entachée d'une illégalité faisant obstacle à son application et susceptible d'être relevée d'office, ainsi que le serait par exemple une clause ayant par elle-même pour effet d'entraîner une violation des obligations déontologiques qui s'imposent à la profession.... ,,2) Contrat de collaboration libérale conclu entre le praticien poursuivi et un autre praticien stipulant que ce contrat, passé pour une durée initiale de deux ans, est tacitement reconductible.... ,,Praticien poursuivi soutenant qu'une telle clause de reconduction tacite peut entraîner, en ce qu'elle est susceptible de prolonger le contrat initial au-delà d'une durée totale de quatre ans, une méconnaissance de l'article R. 4321-131 du code de la santé publique (CSP).,,,Cette circonstance n'est, en tout état de cause, pas de nature à entacher le contrat d'une illégalité faisant obstacle à son application. Par suite, dès lors qu'il est constant que le contrat n'avait été ni résilié ni annulé par une décision de justice, la chambre disciplinaire nationale de l'ordre des masseurs-kinésithérapeutes a pu, sans erreur de droit, apprécier son respect pour statuer sur la poursuite disciplinaire dont elle était saisie.</ANA>
<ANA ID="9B"> 55-04-01-03 1) Il appartient au juge disciplinaire, lorsqu'il est saisi d'un grief tiré de ce qu'un masseur-kinésithérapeute aurait méconnu ses obligations déontologiques en ne respectant pas une clause d'un contrat de droit privé, notamment un contrat de collaboration le liant à un confrère, d'apprécier le respect de cette clause, dès lors qu'elle n'est, à la date du manquement, ni résiliée, ni annulée par une décision de justice, ni entachée d'une illégalité faisant obstacle à son application et susceptible d'être relevée d'office, ainsi que le serait par exemple une clause ayant par elle-même pour effet d'entraîner une violation des obligations déontologiques qui s'imposent à la profession.... ,,2) Contrat de collaboration libérale conclu entre le praticien poursuivi et un autre praticien stipulant que ce contrat, passé pour une durée initiale de deux ans, est tacitement reconductible.... ,,Praticien poursuivi soutenant qu'une telle clause de reconduction tacite peut entraîner, en ce qu'elle est susceptible de prolonger le contrat initial au-delà d'une durée totale de quatre ans, une méconnaissance de l'article R. 4321-131 du code de la santé publique (CSP).,,,Cette circonstance n'est, en tout état de cause, pas de nature à entacher le contrat d'une illégalité faisant obstacle à son application. Par suite, dès lors qu'il est constant que le contrat n'avait été ni résilié ni annulé par une décision de justice, la chambre disciplinaire nationale de l'ordre des masseurs-kinésithérapeutes a pu, sans erreur de droit, apprécier son respect pour statuer sur la poursuite disciplinaire dont elle était saisie.</ANA>
<ANA ID="9C"> 55-04-02-01-035 1) Il appartient au juge disciplinaire, lorsqu'il est saisi d'un grief tiré de ce qu'un masseur-kinésithérapeute aurait méconnu ses obligations déontologiques en ne respectant pas une clause d'un contrat de droit privé, notamment un contrat de collaboration le liant à un confrère, d'apprécier le respect de cette clause, dès lors qu'elle n'est, à la date du manquement, ni résiliée, ni annulée par une décision de justice, ni entachée d'une illégalité faisant obstacle à son application et susceptible d'être relevée d'office, ainsi que le serait par exemple une clause ayant par elle-même pour effet d'entraîner une violation des obligations déontologiques qui s'imposent à la profession.... ,,2) Contrat de collaboration libérale conclu entre le praticien poursuivi et un autre praticien stipulant que ce contrat, passé pour une durée initiale de deux ans, est tacitement reconductible.... ,,Praticien poursuivi soutenant qu'une telle clause de reconduction tacite peut entraîner, en ce qu'elle est susceptible de prolonger le contrat initial au-delà d'une durée totale de quatre ans, une méconnaissance de l'article R. 4321-131 du code de la santé publique (CSP).,,,Cette circonstance n'est, en tout état de cause, pas de nature à entacher le contrat d'une illégalité faisant obstacle à son application. Par suite, dès lors qu'il est constant que le contrat n'avait été ni résilié ni annulé par une décision de justice, la chambre disciplinaire nationale de l'ordre des masseurs-kinésithérapeutes a pu, sans erreur de droit, apprécier son respect pour statuer sur la poursuite disciplinaire dont elle était saisie.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 8 février 1985,,, n° 50591, p. 31.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
