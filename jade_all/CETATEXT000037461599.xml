<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037461599</ID>
<ANCIEN_ID>JG_L_2018_10_000000418700</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/46/15/CETATEXT000037461599.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 6ème chambres réunies, 03/10/2018, 418700</TITRE>
<DATE_DEC>2018-10-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418700</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>M. Alain Seban</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:418700.20181003</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La commune de Neuilly-sur-Seine a demandé au juge des référés du tribunal administratif de Cergy-Pontoise d'ordonner la suspension, sur le fondement de l'article L. 521-1 du code de justice administrative, de l'exécution de l'arrêté du 8 décembre 2017 par lequel le préfet des Hauts-de-Seine a, d'une part, constaté sa carence dans le respect de son objectif de réalisation de logements sociaux sur la période triennale 2014-2016 et, d'autre part, fixé à 2,7 le taux de majoration du prélèvement au titre des logements manquants. Par une ordonnance n° 1801277 du 14 février 2018, le juge des référés a rejeté cette demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés au secrétariat du contentieux du Conseil d'Etat les 1er et 16 mars et 17 septembre 2018, la commune de Neuilly-sur-Seine demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire au titre de l'article L. 521-1 du code de justice administrative, de faire droit à sa demande de suspension ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la construction et de l'habitation ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alain Seban, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la commune de Neuilly-sur-Seine.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision (...) " ; <br/>
<br/>
              2. Considérant qu'aux termes des deux premiers alinéas de l'article L. 302-9-1 du code de la construction et de l'habitation : " Lorsque, dans les communes soumises aux obligations définies aux I et II de l'article L. 302-5, au terme de la période triennale échue, le nombre de logements locatifs sociaux à réaliser en application du I de l'article L. 302-8 n'a pas été atteint ou lorsque la typologie de financement définie au III du même article L. 302-8 n'a pas été respectée, le représentant de l'Etat dans le département informe le maire de la commune de son intention d'engager la procédure de constat de carence. Il lui précise les faits qui motivent l'engagement de la procédure et l'invite à présenter ses observations dans un délai au plus de deux mois. / En tenant compte de l'importance de l'écart entre les objectifs et les réalisations constatées au cours de la période triennale échue, des difficultés rencontrées le cas échéant par la commune et des projets de logements sociaux en cours de réalisation, le représentant de l'Etat dans le département peut, par un arrêté motivé pris après avis du comité régional de l'habitat et de l'hébergement et, le cas échéant, après avis de la commission mentionnée aux II et III de l'article L. 302-9-1-1, prononcer la carence de la commune. Cet arrêté prévoit, pendant toute sa durée d'application, le transfert à l'Etat des droits de réservation mentionnés à l'article L. 441-1, dont dispose la commune sur des logements sociaux existants ou à livrer, et la suspension ou modification des conventions de réservation passées par elle avec les bailleurs gestionnaires, ainsi que l'obligation pour la commune de communiquer au représentant de l'Etat dans le département la liste des bailleurs et des logements concernés. Cet arrêté peut aussi prévoir les secteurs dans lesquels le représentant de l'Etat dans le département est compétent pour délivrer les autorisations d'utilisation et d'occupation du sol pour des catégories de constructions ou d'aménagements à usage de logements listées dans l'arrêté. Par le même arrêté et en fonction des mêmes critères, il fixe, pour une durée maximale de trois ans à compter du 1er janvier de l'année suivant sa signature, la majoration du prélèvement défini à l'article L. 302-7. (...) " ; qu'aux termes du cinquième alinéa du même article : " L'arrêté du représentant de l'Etat dans le département peut faire l'objet d'un recours de pleine juridiction " ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que, par un arrêté du 8 décembre 2017 pris sur le fondement des dispositions citées ci-dessus de l'article L. 302-9-1 du code de la construction et de l'habitation, le préfet des Hauts-de-Seine a constaté la carence de la commune de Neuilly-sur-Seine à atteindre ses objectifs en matière d'offre de logements locatifs sociaux pour la période 2014-2016 et fixé à 2,7, en application de l'article L. 302-7 du même code, le taux de majoration du prélèvement par logement social manquant applicable à cette commune ; que la commune a demandé l'annulation de cet arrêté au tribunal administratif de Cergy-Pontoise et demandé en outre, en référé, la suspension de son exécution en application de l'article L. 521-1 du code de justice administrative ; qu'elle se pourvoit en cassation contre l'ordonnance du 4 février 2018 par laquelle le juge des référés du tribunal administratif a rejeté sa demande pour défaut d'urgence en application des dispositions de l'article L. 522-3 du même code ;<br/>
<br/>
              4. Considérant que, pour rejeter la demande de suspension de l'arrêté préfectoral du 8 décembre 2017, l'ordonnance attaquée relève que la commune " n'apporte (...) pas de justification de nature à établir la gravité et l'immédiateté de l'atteinte portée à un intérêt public local et à sa situation financière par l'arrêté litigieux " et qu'ainsi elle " n'établit pas l'existence d'une situation d'urgence au sens des dispositions précitées de l'article L. 521-1 du code de justice administrative " ; qu'en se prononçant ainsi sur l'urgence au vu des éléments invoqués par la commune et en s'abstenant de la présumer en raison de la nature des mesures prononcées par l'arrêté attaqué, le juge des référés n'a pas commis d'erreur de droit ; qu'en estimant que la demande de suspension n'était pas justifiée par l'urgence, il a porté sur les circonstances de l'espèce une appréciation souveraine exempte de dénaturation ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que la commune de Neuilly-sur-Seine n'est pas fondée à demander l'annulation de l'ordonnance qu'elle attaque ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
		Article 1er : Le pourvoi de la commune de Neuilly-sur-Seine est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la commune de Neuilly-sur-Seine et au ministre de la cohésion des territoires.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-035-02-03-02 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ SUSPENSION (ART. L. 521-1 DU CODE DE JUSTICE ADMINISTRATIVE). CONDITIONS D'OCTROI DE LA SUSPENSION DEMANDÉE. URGENCE. - DEMANDE DE SUSPENSION D'UN ARRÊTÉ PRÉFECTORAL CONSTATANT LA CARENCE D'UNE COMMUNE À RESPECTER SON OBJECTIF DE CONSTRUCTION DE LOGEMENTS SOCIAUX (L. 309-9-1 DU CCH) - PRÉSOMPTION D'URGENCE - ABSENCE (SOL. IMPL.).
</SCT>
<ANA ID="9A"> 54-035-02-03-02 Un arrêté préfectoral, pris sur le fondement de l'article L. 309-9-1 du code de la construction et de l'habitation, ayant pour objet de constater la carence d'une commune à respecter son objectif de construction de logements sociaux sur une période triennale ne crée pas, par lui-même, une situation d'urgence à l'égard de cette commune.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
