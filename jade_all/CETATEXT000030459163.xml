<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030459163</ID>
<ANCIEN_ID>JG_L_2015_04_000000370242</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/45/91/CETATEXT000030459163.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème / 8ème SSR, 02/04/2015, 370242</TITRE>
<DATE_DEC>2015-04-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>370242</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème / 8ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP BOULLEZ</AVOCATS>
<RAPPORTEUR>Mme Angélique Delorme</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:370242.20150402</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Madame A...B...a demandé au tribunal administratif de Melun d'annuler les arrêtés n° 2005/28 et 2005/29 du 22 octobre 2005 par lesquels le maire de la commune de Villecerf l'a licenciée pour fautes et lui a retiré la régie de recettes de la cantine municipale. Par un jugement n° 00600364 du 16 novembre 2010, le tribunal administratif de Melun a rejeté sa demande.<br/>
<br/>
              Par un arrêt  n° 11PA00535 du 30 avril 2013, sur l'appel formé par MmeB..., la cour administrative d'appel de Paris a, d'une part, annulé le jugement du tribunal administratif de Melun du 16 novembre 2010 et les décisions du 22 octobre 2005 par lesquelles le maire de la commune de Villecerf a licencié Mme B...à titre disciplinaire et lui a retiré la régie de recettes de la cantine municipale et, d'autre part, enjoint au maire de Villecerf de réintégrer Mme B...dans les effectifs de la commune dans le délai de deux mois.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 16 juillet 2013, 16 octobre 2013 et 6 janvier 2015 au secrétariat du contentieux du Conseil d'Etat, la commune de Villecerf demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de Mme B...;<br/>
<br/>
              3°) de mettre à la charge de Mme B...la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :	<br/>
              - le code général des collectivités territoriales ;<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le décret n° 88-145 du 15 février 1988 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Angélique Delorme, auditeur,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de la commune de Villecerf et à la SCP Boullez, avocat de Mme B...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du second alinéa de l'article 37 du décret du 15 février 1988, relatif aux agents non titulaires de la fonction publique territoriale : " L'agent non titulaire à l'encontre duquel une procédure disciplinaire est engagée a droit à la communication de l'intégralité de son dossier individuel et de tous les documents annexes et à l'assistance de défenseurs de son choix. L'autorité territoriale doit informer l'intéressé de son droit à communication du dossier. " ; que si le droit à la communication du dossier comporte pour l'agent intéressé celui d'en prendre copie, à moins que sa demande ne présente un caractère abusif, ces dispositions n'imposent pas à l'administration d'informer l'agent de son droit à prendre copie de son dossier ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que MmeB..., recrutée en qualité d'agent contractuel par la commune de Villecerf en 1993, a été, au cours de cette même année, nommée régisseur de recettes de la cantine de la commune et parallèlement chargée de la surveillance et de la gestion de la cantine les jours de cantine scolaire ; que, par deux arrêtés du maire de Villecerf du 22 octobre 2005, Mme B...a été licenciée pour fautes et s'est vu retirer la régie de recettes ; que, par l'arrêt attaqué du 30 avril 2013, la cour administrative d'appel de Paris, infirmant le jugement du tribunal administratif de Melun du 16 novembre 2010, a annulé les décisions litigieuses ;<br/>
<br/>
              3. Considérant que, pour juger que la décision de licenciement était intervenue au terme d'une procédure irrégulière, la cour a relevé que l'affirmation de Mme B...selon laquelle le maire de Villecerf ne lui avait pas permis de prendre copie de son dossier, mais seulement de le consulter, n'était pas contredite par la commune ; que, toutefois, il ressort des pièces du dossier soumis aux juges du fond que la commune avait produit, l'appui de son mémoire en défense, des pièces, notamment les pièces n° 24 et 25, mentionnant que le maire avait informé l'agent de son droit à la communication de son dossier ; que l'obligation d'information prévue à l'article 37 du décret du 15 février 1988 précité n'impliquait pas, ainsi qu'il a été dit au point 1, que l'agent fût expressément informé de la possibilité de prendre une copie de son dossier ; qu'ainsi, en estimant que la commune ne contestait pas que le maire n'avait pas permis à Mme B...de prendre copie de son dossier, la cour a inexactement interprété les écritures de la commune ; que, par suite, son arrêt doit, pour ce motif et sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, être annulé ;<br/>
<br/>
              4. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme B...la somme que demande la commune de Villecerf au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces mêmes dispositions font obstacle à ce qu'une somme soit mise à la charge de la commune qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 30 avril 2013 de la cour administrative d'appel de Paris est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br/>
Article 3 : Les conclusions de la commune de Villercerf et de Mme B...présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 4 : La présente décision sera notifiée à la commune de Villecerf et à Mme A...B....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-07-07-03 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. COMMUNICATION DU DOSSIER. MODALITÉS DE LA COMMUNICATION. - 1) PORTÉE DU DROIT À COMMUNICATION - DROIT DE PRENDRE COPIE DU DOSSIER - EXISTENCE - 2) PROCÉDURE DISCIPLINAIRE - OBLIGATION POUR L'ADMINISTRATION D'INFORMER L'AGENT DE SON DROIT À COMMUNICATION - EXISTENCE - OBLIGATION POUR L'ADMINISTRATION D'INFORMER L'AGENT DE SON DROIT À PRENDRE COPIE DE SON DOSSIER - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-09-05 FONCTIONNAIRES ET AGENTS PUBLICS. DISCIPLINE. PROCÉDURE. - DROIT À LA COMMUNICATION DU DOSSIER - 1) PORTÉE DU DROIT - DROIT DE PRENDRE COPIE DU DOSSIER - EXISTENCE - 2) OBLIGATION POUR L'ADMINISTRATION D'INFORMER L'AGENT DE SON DROIT À COMMUNICATION - EXISTENCE - OBLIGATION POUR L'ADMINISTRATION D'INFORMER L'AGENT DE SON DROIT À PRENDRE COPIE DE SON DOSSIER - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">36-12-02 FONCTIONNAIRES ET AGENTS PUBLICS. AGENTS CONTRACTUELS ET TEMPORAIRES. EXÉCUTION DU CONTRAT. - 1) PORTÉE DU DROIT À COMMUNICATION - DROIT DE PRENDRE COPIE DU DOSSIER - EXISTENCE - 2) PROCÉDURE DISCIPLINAIRE - OBLIGATION POUR L'ADMINISTRATION D'INFORMER L'AGENT DE SON DROIT À COMMUNICATION - EXISTENCE - OBLIGATION POUR L'ADMINISTRATION D'INFORMER L'AGENT DE SON DROIT À PRENDRE COPIE DE SON DOSSIER - ABSENCE.
</SCT>
<ANA ID="9A"> 36-07-07-03 1) Le droit à la communication du dossier comporte pour l'agent intéressé celui d'en prendre copie, à moins que sa demande ne présente un caractère abusif.,,,2) Toutefois, les dispositions prévoyant l'obligation pour l'administration, dans le cadre d'une procédure disciplinaire, d'informer l'intéressé de son droit à communication du dossier n'imposent pas à l'administration d'informer l'agent de son droit à prendre copie de son dossier.</ANA>
<ANA ID="9B"> 36-09-05 1) Le droit à la communication du dossier comporte pour l'agent intéressé celui d'en prendre copie, à moins que sa demande ne présente un caractère abusif.,,,2) Toutefois, les dispositions prévoyant l'obligation pour l'administration, dans le cadre d'une procédure disciplinaire, d'informer l'intéressé de son droit à communication du dossier n'imposent pas à l'administration d'informer l'agent de son droit à prendre copie de son dossier.</ANA>
<ANA ID="9C"> 36-12-02 1) Le droit à la communication du dossier comporte pour l'agent intéressé celui d'en prendre copie, à moins que sa demande ne présente un caractère abusif.,,,2) Toutefois, les dispositions prévoyant l'obligation pour l'administration, dans le cadre d'une procédure disciplinaire, d'informer l'intéressé de son droit à communication du dossier n'imposent pas à l'administration d'informer l'agent de son droit à prendre copie de son dossier.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
