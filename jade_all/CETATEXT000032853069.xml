<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032853069</ID>
<ANCIEN_ID>JG_L_2016_07_000000397529</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/85/30/CETATEXT000032853069.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 06/07/2016, 397529, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-07-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>397529</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Isabelle Lemesle</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:397529.20160706</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              En application de l'article L. 52-15 du code électoral, la Commission nationale des comptes de campagne et de financement des partis politiques (CNCCFP) a saisi le tribunal administratif de Besançon de la décision du 12 octobre 2015 par laquelle elle a rejeté le compte de campagne de Mme B...C...et de M. A...D..., candidats élus lors des opérations électorales qui se sont déroulées les 22 et 29 mars 2015 dans le canton de Scey-sur-Saône-et-Saint-Albin (Haute-Saône) en vue de la désignation des membres du conseil départemental.<br/>
<br/>
              Par un jugement n° 1501693 du 2 février 2016, le tribunal administratif de Besançon a rejeté la saisine de la CNCCFP et fixé le montant du remboursement forfaitaire de l'Etat à 4 000 euros.<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés le 1er mars 2016 et le 25 mai 2016 au secrétariat du contentieux du Conseil d'Etat, la CNCCFP demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de déclarer que le compte de campagne de Mme C...et de M. D...a été rejeté à bon droit, de déclarer que ces derniers n'ont pas droit au remboursement dû par l'Etat et de statuer sur leur inéligibilité.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Isabelle Lemesle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que l'article L. 52-4 du code électoral dispose que le mandataire perçoit les recettes et règles les dépenses jusqu'à la date de dépôt du compte de campagne ; qu'aux termes de l'article L. 52-12 du code électoral : " Chaque candidat (...) soumis au plafonnement prévu à l'article L. 52-11 et qui a obtenu au moins 1 % des suffrages exprimés est tenu d'établir un compte de campagne retraçant, selon leur origine, l'ensemble des recettes perçues et, selon leur nature, l'ensemble des dépenses engagées ou effectuées en vue de l'élection, hors celles de la campagne officielle par lui-même ou pour son compte, au cours de la période mentionnée à l'article L. 52-4. (...) Le candidat estime et inclut, en recettes et en dépenses, les avantages directs ou indirects, les prestations de services et dons en nature dont il a bénéficié. Le compte de campagne doit être en équilibre ou excédentaire et ne peut présenter un déficit./ Au plus tard avant 18 heures le dixième vendredi suivant le premier tour de scrutin, chaque candidat (...) présent au premier tour dépose à la Commission nationale des comptes de campagne et des financements politiques son compte de campagne et ses annexes accompagné des justificatifs de ses recettes ainsi que des factures, devis et autres documents de nature à établir le montant des dépenses payées ou engagées par le candidat ou pour son compte. (...)/ Pour l'application du présent article, en cas de scrutin binominal, le candidat s'entend du binôme de candidats " ; qu'aux termes de l'article L. 52-15 du même code : " La Commission nationale des comptes de campagne et des financements politiques approuve et, après procédure contradictoire, rejette ou réforme les comptes de campagne. Elle arrête le montant du remboursement forfaitaire prévu à l'article L. 52-11-1. / (...) Lorsque la commission a constaté que le compte de campagne n'a pas été déposé dans le délai prescrit, si le compte a été rejeté ou si, le cas échéant après réformation, il fait apparaître un dépassement du plafond des dépenses électorales, la commission saisit le juge de l'élection. (...) Le remboursement total ou partiel des dépenses retracées dans le compte de campagne, quand la loi le prévoit, n'est possible qu'après l'approbation du compte de campagne par la commission " ; que l'article L. 118-3 de ce code dispose que, saisi par la Commission nationale des comptes de campagne et des financements politiques (CNCCFP), le juge de l'élection : " prononce (...) l'inéligibilité du candidat ou des membres du binôme de candidats dont le compte de campagne a été rejeté à bon droit en cas de volonté de fraude ou de manquement d'une particulière gravité aux règles relatives au financement des campagnes électorales. / L'inéligibilité (...) est prononcée pour une durée maximale de trois ans et s'applique à toutes les élections. Toutefois, elle n'a pas d'effet sur les mandats acquis antérieurement à la date de la décision. / Si le juge de l'élection a prononcé l'inéligibilité d'un candidat ou des membres d'un binôme proclamé élu, il annule son élection ou, si l'élection n'a pas été contestée, déclare le candidat ou les membres du binôme démissionnaires d'office " ;<br/>
<br/>
              2. Considérant que, par une décision du 12 octobre 2015, la CNCCFP a rejeté le compte de campagne de Mme C...et de M.D..., candidats élus aux élections départementales des 22 et 29 mars 2015 dans le canton de Scey-sur-Saône-et-Saint-Albin (Haute-Saône) pour omission de comptabilisation de dépenses en méconnaissance de l'article L. 52-12 du code électoral ; que la CNCCFP a saisi le tribunal administratif de Besançon, juge de l'élection, en application des dispositions de l'article L. 52-15 du code électoral ; que, par un jugement du 2 février 2016, le tribunal administratif a rejeté la saisine de la CNCCFP et fixé à 4 000 euros la somme due par l'Etat au candidat en application de l'article L. 52-11-1 du code électoral ; que la CNCCFP fait appel de ce jugement devant le Conseil d'Etat ; <br/>
<br/>
              3. Considérant qu'il résulte de l'instruction que le compte de campagne transmis le 27 mai 2015 à la CNCCFP, dans le délai imparti par l'article L. 52-12 du code électoral qui expirait le 29 mai 2015, par Mme C...et M. D...fait apparaître un montant total de dépenses de 4 862 euros et de recettes de 4 865 euros ; que les candidats ont informé la CNCCFP dans le cadre de la procédure contradictoire prévue par l'article L. 52-15 du même code, du règlement le 2 juillet 2015 par leurs soins de cinq factures, éditées le 31 mars 2015, correspondant pour un montant total de 2 460 euros à des frais d'impression de documents de campagne commandés entre le 19 février et le 23 mars 2015, et sollicité la prise en compte à hauteur de 756 euros de frais de déplacement des candidats revêtant un caractère électoral ;<br/>
<br/>
              Sur le rejet du compte de campagne :<br/>
<br/>
              4. Considérant, d'une part, que les dépenses d'impression dont l'engagement n'a pas été comptabilisé et qui ont été directement acquittées par les candidats, ajoutées aux frais de déplacement, représentent 66 % des dépenses déclarées dans le délai prescrit par l'article L. 52-12 du code électoral et 33 % du plafond des dépenses fixé à 9 730 euros pour le canton en application de l'article L. 52-11 du même code ; que, d'autre part, ni la circonstance que les candidats aient spontanément procédé à la déclaration rectificative des dépenses ainsi omises, ni la nature de ces dépenses, dont la nature électorale n'est pas contestée, ni la circonstance que leur réintégration n'emporte ni dépassement du plafond de dépenses électorales autorisées dans le canton, ni déficit du compte à condition que soit comptabilisé également en régularisation leur financement par les candidats, n'étaient de nature à faire obstacle au rejet du compte de campagne par la CNCCFP, dès lors que le compte déposé n'était pas sincère, faute d'intégrer l'ensemble des dépenses engagées au titre de la campagne en méconnaissance des articles L. 52-4 et L. 52-12 du code électoral, dont les dispositions sont dépourvues d'ambiguïté ; que, par suite, Mme C...et M. D...n'ont pas droit au remboursement forfaitaire de l'Etat en application de l'article L. 52-11-1 du code électoral ;<br/>
<br/>
              Sur l'inéligibilité :<br/>
<br/>
              5. Considérant qu'il résulte des dispositions de l'article L. 118-3 du code électoral qu'en dehors des cas de fraude, le juge de l'élection prononce l'inéligibilité d'un candidat s'il constate un manquement d'une particulière gravité aux règles de financement des campagnes électorales ; que, pour déterminer si un manquement est d'une particulière gravité au sens de ces dispositions, il incombe au juge de l'élection d'apprécier, d'une part, s'il s'agit d'un manquement caractérisé à une règle substantielle relative au financement des campagnes électorales, d'autre part, s'il présente un caractère délibéré ; <br/>
<br/>
              6. Considérant qu'il résulte de l'instruction, d'une part, que si Mme C...et M. D...ne pouvaient ignorer la portée des dispositions des articles L. 52-4 et L. 52-12 du code électoral, il est constant que cette méconnaissance ne résulte d'aucune volonté de fraude, qu'elle n'a pas été de nature à porter atteinte de manière sensible à l'égalité entre les candidats et que leur compte de campagne ne fait pas apparaître d'autres irrégularités de nature à justifier une déclaration d'inéligibilité ; que, d'autre part, la réintégration des dépenses illégalement omises dans leur compte de campagne, qu'ils ont volontairement déclarées et personnellement réglées après le dépôt de ce dernier, n'emporte, comme il a été dit au point 4, ni dépassement du plafond de dépenses électorales autorisées dans le canton, ni déficit du compte à condition que soit comptabilisé également en régularisation leur financement par les candidats ; qu'ainsi, pour blâmable qu'elle soit, leur légèreté ne peut être qualifiée, dans les circonstances de l'espèce, de manquement d'une particulière gravité aux règles de financement des campagnes électorales au sens de l'article L. 118-3 du code électoral ;<br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que la CNCCFP est fondée à demander l'annulation du jugement attaqué ; que, toutefois, il n'y a pas lieu, dans les circonstances de l'espèce, de déclarer Mme C...et M. D...inéligibles ; <br/>
<br/>
              Sur les autres conclusions de Mme C...et M.D... :<br/>
<br/>
              8. Considérant que les conclusions de Mme C...et M. D...tendant à ce que la somme due par l'Etat en application de l'article L. 52-11-1 du code électoral soit portée à 4 621, 55 euros ne peuvent, en tout état de cause, qu'être rejetées compte tenu de ce qui a été dit au point 4 ; que les dispositions de l'article L. 761-1 du code de justice administrative font, en conséquence, obstacle à ce qu'une somme soit mise la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le jugement du 2 février 2016 du tribunal administratif de Besançon est annulé.<br/>
<br/>
Article 2 : Le compte de campagne de Mme C...et M. D...a été rejeté à bon droit par la décision du 12 octobre 2015 de la Commission nationale des comptes de campagne et des financements politiques.<br/>
<br/>
Article 3 : Mme C...et M. D...n'ont pas droit au remboursement forfaitaire de l'Etat en application de l'article L. 52-11-1 du code électoral.<br/>
<br/>
Article 4 : Il n'y a pas lieu de déclarer Mme C...et M. D...inéligibles en application de l'article L. 118-3 du code électoral.<br/>
<br/>
Article 5 : Les conclusions de Mme C...et M. D...présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 6 : La présente décision sera notifiée à la Commission nationale des comptes de campagne et des financements politiques, à Mme B...C...et à M. A...D....<br/>
Copie en sera adressée au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
