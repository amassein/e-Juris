<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026410507</ID>
<ANCIEN_ID>JG_L_2012_09_000000336223</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/41/05/CETATEXT000026410507.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 24/09/2012, 336223</TITRE>
<DATE_DEC>2012-09-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>336223</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO ; SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>Mme Domitille Duval-Arnould</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:336223.20120924</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 3 février et 3 mai 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. Cyril B, demeurant ... ; M. B demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 06MA03172 du 14 mai 2009 par lequel la cour administrative d'appel de Marseille a rejeté sa requête tendant à la réformation du jugement n° 0204354 du 22 septembre 2006 du tribunal administratif de Nice relatif à la réparation des conséquences d'interventions chirurgicales pratiquées au centre hospitalier universitaire de Nice les 15 novembre 1998 et 15 juillet 1999 et fait partiellement droit à l'appel incident du centre hospitalier universitaire de Nice en réduisant les indemnités mises à la charge de cet établissement ;<br/>
<br/>
              2°) de mettre à la charge du centre hospitalier universitaire de Nice la somme de 4 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Domitille Duval-Arnould, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de la SCP Gatineau, Fattaccini avocat de M. B et de Me Le Prado avocat du centre hospitalier universitaire de Nice ;<br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Gatineau, Fattaccini avocat de M. B et à Me Le Prado avocat du centre hospitalier universitaire de Nice ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. B, atteint d'obésité, a subi au centre hospitalier universitaire de Nice, d'une part, le 25 novembre 1998, une gastroplastie verticale selon la technique dite de " Mason " qui a nécessité le 3 mars 2000 une reprise chirurgicale à l'hôpital Henri Mondor et entraîné un reflux gastro-oesophagien, d'autre part, le 15 juillet 1999, une intervention en vue de réduire une gynécomastie bilatérale, qui a nécessité une reprise le 28 juillet 1999 ; que M. B a recherché la responsabilité du centre hospitalier universitaire de Nice au titre d'un défaut de consentement aux interventions des 25 novembre 1998 et 15 juillet 1999 et de fautes médicales commises lors de leur réalisation ; que, par un jugement du 22 septembre 2006, le tribunal administratif de Nice, retenant que M. B n'avait pas donné son consentement à la réalisation d'une gastroplastie selon la technique de Mason, a condamné le centre hospitalier universitaire de Nice à verser à l'intéressé la somme de 10 000 euros au titre des conséquences dommageables de l'intervention du 25 novembre 1998 et rejeté le surplus de ses conclusions ; que M. B se pourvoit en cassation contre l'arrêt du 14 mars 2009 par lequel la cour administrative d'appel de Marseille a ramené à 5 000 euros l'indemnité mise à la charge de l'établissement au titre de la première intervention et confirmé la décision des premiers juges en ce qui concerne la seconde ; <br/>
<br/>
              Sur la gastroplastie pratiquée le 25 novembre 1998 :<br/>
<br/>
              2. Considérant, qu'hors les cas d'urgence ou d'impossibilité de consentir, la réalisation d'une intervention à laquelle le patient n'a pas consenti oblige l'établissement responsable à réparer tant le préjudice moral subi de ce fait par l'intéressé que, le cas échéant, toute autre conséquence dommageable de l'intervention ;<br/>
<br/>
              3. Considérant que l'arrêt attaqué relève, au vu d'un document signé par l'intéressé le 7 juillet 1998 et du rapport d'expertise, que M. B a donné son consentement à la pose d'un anneau péri-gastrique modulable, permettant un ajustement de la restriction alimentaire, et que le centre hospitalier universitaire de Nice a pratiqué une intervention de gastroplastie verticale consistant à scinder l'estomac en deux compartiments, technique qui ne permet aucun ajustement ultérieur, qui impose le respect de contraintes hygiéno-diététiques particulières et qui, sans être totalement irréversible, rend difficile la réfection de l'estomac ; que la cour n'a pas tiré les conséquences nécessaires de ces constatations, dont il ressortait que le patient n'avait pas donné son consentement à l'intervention réalisée par le chirurgien mais à une intervention substantiellement différente, en limitant le droit à réparation de M. B aux contraintes spécifiques liées à la technique utilisée et en ne lui reconnaissant pas le droit d'être indemnisé des complications survenues ; que le requérant est dès lors fondé à soutenir que l'arrêt attaqué est, sur ce point, entaché d'erreur de droit ; <br/>
<br/>
              Sur la gynécomastie pratiquée le 15 juillet 1999 : <br/>
<br/>
              4. Considérant qu'en relevant, en ce qui concerne l'intervention pratiquée le 15 juillet 1999, que M. B ne précisait pas les risques dont il n'aurait pas été informé et qui se seraient réalisés et en écartant en conséquence le moyen tiré d'un défaut de consentement éclairé, la cour a suffisamment motivé son arrêt ; <br/>
<br/>
              5. Considérant qu'il résulte de tout ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi relatifs à la gastroplastie, M. B est seulement fondé à demander l'annulation de l'arrêt du 14 mars 2009 de la cour administrative d'appel de Marseille en tant qu'il a statué sur son droit à réparation des conséquences de l'intervention pratiquée le 25 novembre 1998 ; <br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du centre hospitalier universitaire de Nice la somme de 3 000 euros à verser à M. B au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 14 mars 2009 est annulé en tant qu'il statue sur les conclusions de M. B relatives à la réparation des conséquences dommageables de l'intervention réalisée le 25 novembre 1998.<br/>
<br/>
Article 2 : L'affaire est renvoyée dans cette mesure devant la cour administrative d'appel de Marseille.<br/>
<br/>
Article 3 : Le centre hospitalier universitaire de Nice versera à M. B la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
		Article 4 : Le surplus des conclusions de M. B est rejeté.<br/>
<br/>
Article 5 : La présente décision sera notifiée à M. Cyril B, au centre hospitalier universitaire de Nice et à la caisse primaire d'assurance maladie des Alpes Maritimes. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-02-01-01-01-01-04 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICE PUBLIC DE SANTÉ. ÉTABLISSEMENTS PUBLICS D'HOSPITALISATION. RESPONSABILITÉ POUR FAUTE SIMPLE : ORGANISATION ET FONCTIONNEMENT DU SERVICE HOSPITALIER. EXISTENCE D'UNE FAUTE. MANQUEMENTS À UNE OBLIGATION D'INFORMATION ET DÉFAUTS DE CONSENTEMENT. - DÉFAUT DE CONSENTEMENT - RÉPARATION - PRÉJUDICE MORAL - INCLUSION [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-04-01-04 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RÉPARATION. PRÉJUDICE. CARACTÈRE INDEMNISABLE DU PRÉJUDICE - AUTRES CONDITIONS. - EXISTENCE - INTERVENTION MÉDICALE - DÉFAUT DE CONSENTEMENT DU PATIENT - RÉPARATION - PRÉJUDICE MORAL - INCLUSION [RJ1].
</SCT>
<ANA ID="9A"> 60-02-01-01-01-01-04 Hors les cas d'urgence ou d'impossibilité de consentir, la réalisation d'une intervention à laquelle le patient n'a pas consenti oblige l'établissement responsable à réparer tant le préjudice moral subi de ce fait par l'intéressé que, le cas échéant, toute autre conséquence dommageable de l'intervention.</ANA>
<ANA ID="9B"> 60-04-01-04 Hors les cas d'urgence ou d'impossibilité de consentir, la réalisation d'une intervention à laquelle le patient n'a pas consenti oblige l'établissement responsable à réparer tant le préjudice moral subi de ce fait par l'intéressé que, le cas échéant, toute autre conséquence dommageable de l'intervention.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant de la réparation des troubles subis du fait d'un défaut d'information du patient, CE, 10 octobre 2012, Beaupère et Lemaître, n° 350426, à publier au Recueil. Comp., dans la même mesure, Cass. civ. 1ère, 3 juin 2010, n° 09-13.591, Bull. 2010, I n° 128 ; Cass. civ. 1ère, 12 juin 2012, n° 11-18.327.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
