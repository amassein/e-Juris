<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029225112</ID>
<ANCIEN_ID>JG_L_2014_07_000000377999</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/22/51/CETATEXT000029225112.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 11/07/2014, 377999, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-07-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>377999</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Séverine Larere</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:377999.20140711</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance n° 1312732 du 15 avril 2014, enregistrée le 18 avril 2014 au secrétariat de la section du contentieux du Conseil d'Etat, par laquelle le président de la 1ère section du tribunal administratif de Paris, avant de statuer sur la demande de la société Linklaters LLP tendant à l'octroi, au titre de l'année 2011, du bénéfice du dégrèvement transitoire de la contribution économique territoriale prévu par les dispositions de l'article 1647 C quinquies B du code général des impôts, a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel, de transmettre au Conseil d'Etat la question prioritaire de constitutionnalité relative à la conformité aux droits et libertés garantis par la Constitution des dispositions combinées des articles 1647 C quinquies B du code général des impôts, issu de la loi du 30 décembre 2009 de finances pour 2010, et 1476 du même code, dans sa rédaction issue du L du I de l'article 108 de la loi du 29 décembre 2010 de finances pour 2011 ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule et son article 61-1 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
              Vu le code général des impôts, notamment ses articles 1647 C quinquies B et 1476 ;<br/>
<br/>
              Vu la loi n° 2010-1657 du 29 décembre 2010 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Séverine Larere, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2. Considérant, d'une part, que le 1er alinéa de l'article 1647 C quinquies B du code général des impôts, dont les dispositions sont issues de l'article 2 de la loi du 30 décembre 2009 de finances pour 2010, dispose que : " Sur demande du contribuable effectuée dans le délai légal de réclamation prévu pour la cotisation foncière des entreprises, la somme de la contribution économique territoriale, des taxes pour frais de chambres de commerce et d'industrie et pour frais de chambres de métiers et de l'artisanat et de l'imposition forfaitaire sur les entreprises de réseaux due par l'entreprise au titre des années 2010 à 2013 fait l'objet d'un dégrèvement lorsque cette somme, due au titre de l'année 2010, est supérieure de 500 euros et de 10 % à la somme des cotisations de taxe professionnelle et des taxes pour frais de chambres de commerce et d'industrie et pour frais de chambres de métiers et de l'artisanat qui auraient été dues au titre de 2010 en application du présent code en vigueur au 31 décembre 2009, à l'exception des coefficients forfaitaires déterminés en application de l'article 1518 bis qui sont, dans tous les cas, ceux fixés au titre de 2010. (...) " ; qu'en vertu des alinéas suivants du même article, le dégrèvement transitoire qu'il instaure est égal à un pourcentage de la différence entre la première des sommes précitées et la dernière majorée de 10 %, qui est fixé à 100 % puis 75 %, 50 % et 25 % pour les impositions établies en 2010, 2011, 2012 et 2013;<br/>
<br/>
              3. Considérant, d'autre part, qu'aux termes du premier alinéa de l'article 1476 du code général des impôts : " La cotisation foncière des entreprises est établie au nom des personnes qui exercent l'activité imposable, dans les conditions prévues en matière de contributions directes, sous les mêmes sanctions ou recours " ; que, dans sa rédaction applicable au titre de l'année 2010, le deuxième alinéa de cet article prévoyait que : " Pour les sociétés civiles professionnelles, les sociétés civiles de moyens et les groupements réunissant des membres de professions libérales, l'imposition est établie au nom de chacun des membres. (...) " ; que les dispositions du L du I de l'article 108 de la loi du 29 décembre 2010 de finances pour 2011 ont supprimé les deuxième à dernier alinéas de cet article et les ont remplacés par des dispositions prévoyant que, par exception aux dispositions du premier alinéa précité, la cotisation foncière des entreprises est établie au nom du ou des gérants lorsque l'activité est exercée par des sociétés non dotées de la personnalité morale ou au nom du fiduciaire lorsque l'activité est exercée en vertu d'un contrat de fiducie ; qu'il en résulte qu'à compter du 1er janvier 2011, l'imposition correspondant à l'activité exercée au sein des sociétés civiles professionnelles, des sociétés civiles de moyens et des groupements réunissant des professions libérales est établie à leur nom ou à celui de leur gérant si elles ne sont pas dotées de la personnalité morale ;<br/>
<br/>
              4. Considérant que la société Linklaters LLP soutient que les dispositions combinées du L du I de l'article 108 de la loi de finances pour 2011 et de l'article 1647 C quinquies B du code général des impôts citées au point 2 ont pour effet de priver les sociétés civiles professionnelles, les sociétés civiles de moyens et les groupements réunissant des membres de professions libérales, du bénéfice du dégrèvement transitoire de contribution économique territoriale prévu en faveur des entreprises soumises à cette contribution au titre de l'année 2010 et que, dès lors, elles méconnaissent les principes constitutionnels d'égalité devant la loi et d'égalité devant les charges publiques respectivement garantis par les articles 6 et 13 de la Déclaration des droits de l'homme et du citoyen ; <br/>
<br/>
              5. Considérant, toutefois, qu'en vertu des articles 1447 et 1586 ter du code général des impôts, la cotisation foncière des entreprises et la cotisation sur la valeur ajoutée des entreprises, qui constituent les deux composantes de la contribution économique territoriale, sont dues non seulement par les personnes morales qui exercent à titre habituel une activité professionnelle non salariée, mais aussi par les personnes physiques ainsi que les sociétés non dotées de la personnalité morale qui exercent une telle activité ; qu'il en résulte, à la lumière des travaux préparatoires de l'article 2 de la loi de finances pour 2010, que les membres des sociétés et groupements précités, qui étaient personnellement imposés, en 2010, à la contribution économique territoriale au titre de l'activité exercée au sein de ces structures, pouvaient, à cette date, bénéficier, dans les conditions visées au point 2, du dégrèvement prévu par l'article 1647 C quinquies B du code général des impôts ; qu'à la lumière des travaux préparatoires dont elles sont issues, les dispositions de l'article 108 de la loi de finances pour 2011 prévoyant que ces sociétés et groupements sont redevables de cette contribution, à compter de 2011, n'ont pas eu pour objet et ne sauraient être regardées comme ayant eu légalement pour effet de mettre un terme aux dégrèvements auxquels leurs membres avaient, le cas échéant, droit et ne font pas obstacle à ce que les nouveaux contribuables en demandent le bénéfice, en leur lieu et place, au titre des années 2011 à 2013 ; qu'il suit de là que le moyen invoqué ne soulève pas une question présentant un caractère sérieux ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède qu'il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité transmise par le tribunal administratif de Paris.<br/>
Article 2 : La présente décision sera notifiée à la société Linklaters LLP et au ministre des finances et des comptes publics. <br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre et au tribunal administratif de Paris.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
