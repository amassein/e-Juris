<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044393403</ID>
<ANCIEN_ID>JG_L_2021_11_000000448443</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/39/34/CETATEXT000044393403.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 19/11/2021, 448443</TITRE>
<DATE_DEC>2021-11-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>448443</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO ; SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>M. Damien Pons</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:448443.20211119</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme R... H... épouse J... a demandé au tribunal administratif de Marseille :<br/>
<br/>
              - sous le n° 1701202, d'annuler la décision du 28 juillet 2016 par laquelle la caisse d'allocations familiales des Bouches-du-Rhône lui a refusé le droit au revenu de solidarité active à compter du 1er juillet 2016, d'annuler la décision de la caisse d'allocations familiales des Bouches-du-Rhône du 11 septembre 2016, de la rétablir dans ses droits au revenu de solidarité active à compter du 1er juillet 2016, d'enjoindre au département des Bouches-du-Rhône de procéder au calcul et au versement de la somme due à ce titre, d'annuler la mise en demeure du 4 janvier 2017 par laquelle la caisse d'allocations familiales des Bouches-du-Rhône lui a demandé de rembourser 228,67 euros d'indu d'aide exceptionnelle de fin d'année et de la décharger de l'obligation de payer cette somme ;<br/>
<br/>
              - sous le n° 1708477, d'annuler le titre exécutoire émis à son encontre le 7 juin 2017 par le département des Bouches-du-Rhône en vue de recouvrer un indu de revenu de solidarité active d'un montant de 3 866,67 euros pour la période du 1er septembre 2015 au 30 juin 2016 ainsi que la décision du 14 septembre 2017 par laquelle la présidente du conseil départemental des Bouches-du-Rhône a rejeté le recours administratif formé contre ce titre.<br/>
<br/>
              Par un jugement n°s 1701202 et 1705477 du 1er juillet 2020, le tribunal administratif de Marseille a rejeté ces demandes.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 6 janvier, 6 avril et 1er septembre 2021 au secrétariat du contentieux du Conseil d'Etat, Mme H... épouse J... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses demandes ;<br/>
<br/>
              3°) de mettre à la charge du département des Bouches-du-Rhône la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - la directive 2004/38/CE du Parlement européen et du Conseil ;<br/>
              - le code de l'action sociale et des familles ; <br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Damien Pons, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat de Mme H... épouse J... et à Me Le Prado, avocat du département des Bouches-du-Rhône ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que Mme H... et son époux, M. J..., de nationalité bulgare, vivent en France, chez leur fille, de nationalité française, respectivement depuis septembre 2010 et décembre 2011. Le 28 juillet 2016, la caisse d'allocations familiales a mis fin, à compter de juillet 2016, aux droits au revenu de solidarité active de Mme H..., qui lui avaient été ouverts en juin 2016 avec effet rétroactif à compter du 1er septembre 2015, et lui a demandé le remboursement de la somme de 228,67 euros correspondant à l'aide exceptionnelle de fin d'année versée en décembre 2015 ainsi que de la somme de 3 866,38 euros correspondant au revenu de solidarité active perçu du 1er septembre 2015 au 30 juin 2016. Le 17 février 2017, la présidente du conseil départemental des Bouches-du-Rhône a confirmé le refus d'attribution du revenu de solidarité active et la récupération de l'indu. Mme H... se pourvoit en cassation contre le jugement du 1er juillet 2020 par lequel le tribunal administratif de Marseille a rejeté ses demandes contestant le refus d'attribution du revenu de solidarité active et les indus réclamés et tendant à l'annulation du titre exécutoire émis à son encontre le 7 juin 2017 par le département des Bouches-du-Rhône en vue de recouvrer la somme due au titre du revenu de solidarité active. <br/>
<br/>
              2. D'une part, les dispositions de l'article L. 262-6 du code de l'action sociale et des familles, par exception aux dispositions du 2° de l'article L. 262-4 de ce code qui réservent le bénéfice du revenu de solidarité active aux Français ou aux étrangers titulaires, depuis au moins cinq ans, d'un titre de séjour autorisant à travailler, ouvrent le droit à cette allocation aux ressortissants d'un Etat membre de l'Union européenne, d'un autre Etat partie à l'accord sur l'Espace économique européen ou de la Confédération suisse qui remplissent les conditions exigées pour bénéficier d'un droit de séjour et ont résidé en France durant les trois mois précédant la demande, en précisant que la condition de durée de résidence ne s'applique pas dans certaines hypothèses.<br/>
<br/>
              3. D'autre part, l'article L. 121-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, pris pour la transposition des dispositions de l'article 7 de la directive 2004/38/CE du Parlement européen et du Conseil du 29 avril 2004 relative au droit des citoyens de l'Union et des membres de leurs familles de circuler et de séjourner librement sur le territoire des Etats membres, désormais repris en substance à l'article L. 233-1 de ce code, dispose que : " Sauf si sa présence constitue une menace pour l'ordre public, tout citoyen de l'Union européenne, tout ressortissant d'un autre Etat partie à l'accord sur l'Espace économique européen ou de la Confédération suisse a le droit de séjourner en France pour une durée supérieure à trois mois s'il satisfait à l'une des conditions suivantes : (...) / 1° S'il exerce une activité professionnelle en France ; / 2° S'il dispose, pour lui et pour les membres de sa famille tels que visés au 4° de ressources suffisantes afin de ne pas devenir une charge pour le système d'assistance sociale, ainsi que d'une assurance maladie ; / (...) 4° S'il est un descendant direct âgé de moins de vingt et un ans ou à charge, ascendant direct à charge, conjoint, ascendant ou descendant direct à charge du conjoint, accompagnant ou rejoignant un ressortissant qui satisfait aux conditions énoncées aux 1° et 2° ; / (...)". En vertu de l'article L. 122-1 du même code, désormais repris en substance à l'article L. 234-1, sauf si leur présence constitue une menace pour l'ordre public, ceux de ces ressortissants qui ont résidé de manière légale et ininterrompue en France pendant les cinq années précédentes acquièrent un droit au séjour permanent sur l'ensemble du territoire français.  <br/>
<br/>
              4. Il ressort des pièces du dossier soumis au juge du fond que, pour soutenir qu'elle bénéficiait du droit au séjour requis pour présenter une demande de revenu de solidarité active en qualité de ressortissante de l'Union européenne, Mme H... faisait valoir qu'étant à la charge de sa fille de nationalité française, chez qui elle résidait en France depuis septembre 2010, elle séjournait en France depuis cette date de façon régulière au titre, d'une part, du 4° de l'article L. 121-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, en qualité d'ascendant à charge d'un citoyen de l'Union européenne et, d'autre part, du 2° du même article.<br/>
<br/>
              5. Il résulte toutefois, en premier lieu, des dispositions citées au point 3 que le droit au séjour ouvert à l'ascendant d'un citoyen de l'Union européenne au titre du 4° de l'article L. 121-1 du code de l'entrée et du séjour des étrangers et du droit d'asile est subordonné à la condition que le citoyen de l'Union européenne accompagné ou rejoint par l'intéressé se prévalant de sa qualité d'ascendant satisfasse lui-même aux conditions énoncées aux 1° ou 2° du même article, c'est-à-dire que ce citoyen de l'Union européenne accompagné ou rejoint séjourne lui-même en France en exerçant le droit au séjour résultant de ces dispositions. Or un ressortissant français, lorsqu'il réside en France, n'exerce pas un droit qui lui serait ouvert en qualité de citoyen de l'Union européenne au sens et pour l'application de la directive 2004/38/CE transposée par l'article L. 121-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, cette directive ne s'appliquant, ainsi que l'indique son article 3, qu'aux citoyens de l'Union qui, faisant usage de leur droit de libre circulation, se rendent ou séjournent dans un Etat membre autre que celui dont ils ont la nationalité, ainsi qu'aux membres de leur famille qui les accompagnent ou les rejoignent. Le 4° de l'article L. 121-1 du code de l'entrée et du séjour des étrangers et du droit d'asile n'ouvre ainsi pas un droit au séjour à l'ascendant qui rejoint ou accompagne un ressortissant français en France. Par suite, Mme H... ne pouvant, à l'appui de sa demande de revenu de solidarité active, se prévaloir comme elle le soutient, d'un droit au séjour au titre du 4° de l'article L. 121-1, le tribunal administratif n'a pas commis d'erreur de droit en recherchant si elle pouvait se prévaloir d'un droit au séjour au titre du 2° de cet article. <br/>
<br/>
              6. Ayant, en deuxième lieu, constaté sans dénaturer les pièces du dossier qui lui était soumis, que Mme H... n'avait produit aucun élément permettant d'établir que le montant de ses ressources, notamment la pension de retraite qui lui est versée ainsi qu'à son époux, était suffisant pour lui permettre de justifier d'un droit au séjour au titre du 2° de l'article L. 121-1 du même code, le tribunal administratif n'a pas non plus commis d'erreur de droit en en déduisant que Mme H..., qui ne se prévalait pas d'un droit au séjour à un autre titre, ne remplissait pas les conditions pour bénéficier du revenu de solidarité active sur la période considérée.<br/>
<br/>
              7. Par suite, Mme H... n'est pas fondée à demander l'annulation du jugement qu'elle attaque. Ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent par suite qu'être également rejetées.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi de Mme H... est rejeté.<br/>
Article 2 : La présente décision sera notifiée à Mme R... H..., épouse J... et au département des Bouches-du-Rhône.<br/>
              Délibéré à l'issue de la séance du 20 octobre 2021 où siégeaient : Mme Christine Maugüé, présidente adjointe de la Section du Contentieux, présidant ; Mme A... P..., Mme D... N..., présidentes de chambre ; M. B... M..., Mme C... F..., Mme I... L..., M. K... G..., M. Damien Botteghi, conseillers d'Etat et M. Damien Pons, maître des requêtes en service extraordinaire-rapporteur. <br/>
<br/>
Rendu le 19 novembre 2021.<br/>
<br/>
                 La président : <br/>
                 Signé : Mme Christine Maugüé<br/>
 		Le rapporteur : <br/>
      Signé : M. Damien Pons<br/>
                 La secrétaire :<br/>
                 Signé : Mme Q... E...<br/>
<br/>
<br/>
La République mande et ordonne au ministre des solidarités et de la santé en ce qui le concerne ou à tous huissiers de justice à ce requis en ce qui concerne les voies de droit commun contre les parties privées, de pourvoir à l'exécution de la présente décision.<br/>
			Pour expédition conforme,<br/>
			Pour le secrétaire du contentieux, par délégation :<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-02-06 AIDE SOCIALE. - DIFFÉRENTES FORMES D'AIDE SOCIALE. - REVENU MINIMUM D'INSERTION (RMI). - RSA - BÉNÉFICE SUBORDONNÉ AU DROIT AU SÉJOUR, POUR LES RESSORTISSANTS EUROPÉENS (ART. L. 262-6 DU CASF) - DROIT AU SÉJOUR EN TANT QU'ACCOMPAGNANT D'UN CITOYEN DE L'UNION BÉNÉFICIANT D'UN DROIT AU SÉJOUR (4° DE L'ART. L. 121-1 DU CESEDA) - ACCOMPAGNANT D'UN CITOYEN FRANÇAIS - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">15-05-01-01-01 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. - RÈGLES APPLICABLES. - LIBERTÉS DE CIRCULATION. - LIBRE CIRCULATION DES PERSONNES. - DROIT AU SÉJOUR DES RESSORTISSANTS EUROPÉENS (DIRECTIVE 2004/38/CE) - EN TANT QU'ACCOMPAGNANT D'UN CITOYEN DE L'UNION BÉNÉFICIANT D'UN DROIT AU SÉJOUR (4° DE L'ART. L. 121-1 DU CESEDA) - CHAMP D'APPLICATION - EXCLUSION - ACCOMPAGNANT D'UN CITOYEN FRANÇAIS.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">335-01 ÉTRANGERS. - SÉJOUR DES ÉTRANGERS. - DROIT AU SÉJOUR DES RESSORTISSANTS DES ETATS DE L'UE, DES AUTRES ETATS DE L'EEE OU DE LA CONFÉDÉRATION SUISSE (DIRECTIVE 2004/38/CE) - EN TANT QU'ACCOMPAGNANT D'UN CITOYEN DE L'UNION BÉNÉFICIANT D'UN DROIT AU SÉJOUR (4° DE L'ART. L. 121-1 DU CESEDA) - CHAMP D'APPLICATION - EXCLUSION - ACCOMPAGNANT D'UN CITOYEN FRANÇAIS.
</SCT>
<ANA ID="9A"> 04-02-06 Il résulte des articles L. 121-1 et L. 122-1 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), désormais repris en substance aux articles L. 233-1 et L. 234-1 de ce code, que le droit au séjour ouvert à l'ascendant d'un citoyen de l'Union européenne (UE) au titre du 4° de l'article L. 121-1 est subordonné à la condition que le citoyen de l'UE accompagné ou rejoint par l'intéressé se prévalant de sa qualité d'ascendant satisfasse lui-même aux conditions énoncées aux 1° ou 2° du même article, c'est-à-dire que ce citoyen de l'UE accompagné ou rejoint séjourne lui-même en France en exerçant le droit au séjour résultant de ces dispositions. ......Or un ressortissant français, lorsqu'il réside en France, n'exerce pas un droit qui lui serait ouvert en qualité de citoyen de l'Union européenne au sens et pour l'application de la directive 2004/38/CE transposée par l'article L. 121-1 du CESEDA, cette directive ne s'appliquant, ainsi que l'indique son article 3, qu'aux citoyens de l'Union qui, faisant usage de leur droit de libre circulation, se rendent ou séjournent dans un Etat membre autre que celui dont ils ont la nationalité, ainsi qu'aux membres de leur famille qui les accompagnent ou les rejoignent. ......Le 4° de l'article L. 121-1 du CESEDA n'ouvre ainsi pas un droit au séjour à l'ascendant qui rejoint ou accompagne un ressortissant français en France.</ANA>
<ANA ID="9B"> 15-05-01-01-01 Il résulte des articles L. 121-1 et L. 122-1 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), désormais repris en substance aux articles L. 233-1 et L. 234-1 de ce code, que le droit au séjour ouvert à l'ascendant d'un citoyen de l'Union européenne (UE) au titre du 4° de l'article L. 121-1 est subordonné à la condition que le citoyen de l'UE accompagné ou rejoint par l'intéressé se prévalant de sa qualité d'ascendant satisfasse lui-même aux conditions énoncées aux 1° ou 2° du même article, c'est-à-dire que ce citoyen de l'UE accompagné ou rejoint séjourne lui-même en France en exerçant le droit au séjour résultant de ces dispositions. ......Or un ressortissant français, lorsqu'il réside en France, n'exerce pas un droit qui lui serait ouvert en qualité de citoyen de l'Union européenne au sens et pour l'application de la directive 2004/38/CE transposée par l'article L. 121-1 du CESEDA, cette directive ne s'appliquant, ainsi que l'indique son article 3, qu'aux citoyens de l'Union qui, faisant usage de leur droit de libre circulation, se rendent ou séjournent dans un Etat membre autre que celui dont ils ont la nationalité, ainsi qu'aux membres de leur famille qui les accompagnent ou les rejoignent. ......Le 4° de l'article L. 121-1 du CESEDA n'ouvre ainsi pas un droit au séjour à l'ascendant qui rejoint ou accompagne un ressortissant français en France.</ANA>
<ANA ID="9C"> 335-01 Il résulte des articles L. 121-1 et L. 122-1 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), désormais repris en substance aux articles L. 233-1 et L. 234-1 de ce code, que le droit au séjour ouvert à l'ascendant d'un citoyen de l'Union européenne (UE) au titre du 4° de l'article L. 121-1 est subordonné à la condition que le citoyen de l'UE accompagné ou rejoint par l'intéressé se prévalant de sa qualité d'ascendant satisfasse lui-même aux conditions énoncées aux 1° ou 2° du même article, c'est-à-dire que ce citoyen de l'UE accompagné ou rejoint séjourne lui-même en France en exerçant le droit au séjour résultant de ces dispositions. ......Or un ressortissant français, lorsqu'il réside en France, n'exerce pas un droit qui lui serait ouvert en qualité de citoyen de l'Union européenne au sens et pour l'application de la directive 2004/38/CE transposée par l'article L. 121-1 du CESEDA, cette directive ne s'appliquant, ainsi que l'indique son article 3, qu'aux citoyens de l'Union qui, faisant usage de leur droit de libre circulation, se rendent ou séjournent dans un Etat membre autre que celui dont ils ont la nationalité, ainsi qu'aux membres de leur famille qui les accompagnent ou les rejoignent. ......Le 4° de l'article L. 121-1 du CESEDA n'ouvre ainsi pas un droit au séjour à l'ascendant qui rejoint ou accompagne un ressortissant français en France.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
