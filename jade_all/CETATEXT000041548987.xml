<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041548987</ID>
<ANCIEN_ID>JG_L_2020_02_000000422631</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/54/89/CETATEXT000041548987.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 05/02/2020, 422631, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-02-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>422631</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Catherine Calothy</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2020:422631.20200205</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire et un mémoire en réplique, enregistrés les 26 juillet 2018, 5 juin 2019 et 8 janvier 2020 au secrétariat du contentieux du Conseil d'Etat, l'Association pour la protection des animaux sauvages et du patrimoine naturel (ASPAS) et l'Association pour la sauvegarde et la réhabilitation de la faune des Antilles (ASFA) demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite de rejet née du silence gardé sur leur demande de modification des arrêtés du 17 janvier 2018 du ministre d'Etat, ministre de la transition écologique et solidaire et du ministre de l'agriculture et de l'alimentation fixant la liste des mammifères terrestres représentés respectivement dans le département de la Guadeloupe, le département de la Martinique et la collectivité d'outre-mer de Saint-Martin, protégés sur l'ensemble du territoire national et les modalités de leur protection ; <br/>
<br/>
              2°) d'enjoindre aux ministres de modifier ces arrêtés, en interdisant la perturbation intentionnelle des espèces citées aux articles 2 de ces arrêtés, quelle que soit la période concernée, en interdisant toute perturbation intentionnelle du molosse commun en milieu naturel et en encadrant strictement les perturbations en milieu anthropique, en inscrivant le tadaride du Brésil sur la liste figurant aux articles 2 des arrêtés contestés et, à titre subsidiaire, en interdisant toute perturbation intentionnelle de cette dernière espèce en milieu naturel et en encadrant strictement leur perturbation en milieu anthropique. <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention pour la protection et la mise en valeur du milieu marin de la région des Caraïbes signée à Carthagène le 24 mai 1983, ainsi que le protocole relatif aux zones et à la vie sauvage spécialement protégées adopté à Kingston le 18 janvier 1990 ;<br/>
              - le code de l'environnement ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Catherine Calothy, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article 10.3 du protocole relatif aux zones et à la vie sauvage spécialement protégées adopté à Kingston le 18 janvier 1990, signé dans le cadre de la convention pour la protection et la mise en valeur du milieu marin de la région des Caraïbes, adoptée à Carthagène le 24 mars 1983, et publié au Journal officiel de la République française du 11 juillet 2002 par l'effet du décret du 4 juillet 2002 : " En ce qui concerne les espèces animales protégées, les Parties contrôlent et, si nécessaire, interdisent : / (...) / b) Dans la mesure du possible, toute perturbation de la faune sauvage, en particulier pendant les périodes de reproduction, d'incubation, d'hibernation ou de migration ainsi que pendant toute autre période biologique critique ". Aux termes de l'article 11 du même protocole : " 1. Les Parties adoptent des mesures concertées pour assurer la protection et la restauration des espèces végétales et animales menacées ou en voie d'extinction qui sont énumérées dans les annexes I, II et III du présent Protocole : / (...) / b) Chaque Partie assure la protection totale et la restauration des espèces animales énumérées à l'annexe II en interdisant : / (...) / ii) dans la mesure du possible, de perturber ces espèces, en particulier pendant les périodes de reproduction, d'incubation, d'hibernation, de migration ou pendant toute autre période biologique critique ". Sont notamment inscrits à l'annexe II du protocole les espèces de chiroptères dénommées Tadarida brasiliensis, Pteronotus davyi, Ardops nicollsi, Brachyphylla cavernarum, Chiroderma improvisum et Eptesicus guadeloupensis.<br/>
<br/>
              2. L'article L. 411-1 du code de l'environnement prévoit, lorsque les nécessités de la préservation du patrimoine naturel justifient la conservation d'espèces animales non domestiques, l'interdiction de " 1° La destruction ou l'enlèvement des oeufs ou des nids, la mutilation, la destruction, la capture ou l'enlèvement, la perturbation intentionnelle, la naturalisation d'animaux de ces espèces ou, qu'ils soient vivants ou morts, leur transport, leur colportage, leur utilisation, leur détention, leur mise en vente, leur vente ou leur achat ". Le I. de l'article L. 411-2 du même code renvoie à un décret en Conseil d'Etat la détermination des conditions dans lesquelles sont fixées, notamment, la liste limitative des espèces animales non domestiques ainsi protégés, la durée et les modalités de mise en oeuvre des interdictions ainsi que la partie du territoire sur laquelle elles s'appliquent. En vertu des articles R. 411-1 et R. 411-3 du même code, un arrêté conjoint du ministre chargé de la protection de la nature et soit du ministre chargé de l'agriculture, soit, lorsqu'il s'agit d'espèces marines, du ministre chargé des pêches maritimes, fixe les listes des espèces animales non domestiques ainsi protégées, ainsi que la nature des interdictions applicables, leur durée et les parties du territoire et les périodes de l'année où elles s'appliquent.<br/>
<br/>
              3. En application de ces dispositions, les ministres chargés de la protection de la nature et de l'agriculture ont, par trois arrêtés du 17 janvier 2018, fixé la liste des mammifères terrestres protégés représentés respectivement dans la collectivité d'outre-mer de Saint-Martin, le département de la Martinique et le département de la Guadeloupe ainsi que les modalités de leur protection. Les associations ASPAS et ASFA demandent l'annulation pour excès de pouvoir de la décision implicite de rejet née du silence gardé sur leur demande tendant à la modification de ces arrêtés<br/>
<br/>
              Sur les articles 2 des arrêtés contestés :<br/>
<br/>
              4. Les articles 2 des arrêtés contestés dressent la liste des mammifères terrestres bénéficiant d'une protection sur le territoire des département de la Guadeloupe et de la Martinique et de la collectivité d'outre-mer de Saint-Martin, comprenant notamment, en vertu de leur 1°, une interdiction portant, d'une part, sur " la destruction, la mutilation, la capture ou l'enlèvement des animaux " et, d'autre part, sur " la perturbation intentionnelle des animaux notamment pendant la période de reproduction et de dépendance, pour autant que la perturbation remette en cause le bon accomplissement des cycles biologiques de l'espèce considérée. " Les mêmes articles énumèrent, au nombre des espèces protégées, celles qui figurent à l'annexe II du protocole relatif aux zones et à la vie sauvage spécialement protégées citées au point 1, à l'exception du tadaride du Brésil (Tadarida brasiliensis) pour les arrêtés relatifs au département de la Guadeloupe et à la collectivité d'outre-mer de Saint-Martin. <br/>
<br/>
              5. En premier lieu, la protection assurée par ces dispositions n'est pas moindre que celle garantie par les stipulations de l'article 11 b) du protocole adopté à Kingston, dès lors que la perturbation intentionnelle que prohibent ces stipulations doit être comprise comme celle qui remet en cause le bon accomplissement des cycles biologiques des espèces considérées, pendant les périodes expressément citées par le protocole ou toute autre période biologique critique. Le moyen selon lequel les articles 2 des arrêtés contestés méconnaîtraient les stipulations du protocole adopté à Kingston ne peut, par suite, qu'être écarté.<br/>
<br/>
              6. En second lieu, il ne ressort pas des pièces du dossier que les ministres auraient commis une erreur manifeste d'appréciation au regard de l'objectif de préservation du patrimoine naturel résultant des dispositions citées au point 2 en faisant relever de ce niveau de protection les espèces énumérées aux articles 2 des arrêtés contestés. <br/>
<br/>
              Sur les articles 3 des arrêtés contestés :<br/>
<br/>
              7. Les articles 3 des arrêtés contestés dressent la liste des mammifères terrestres bénéficiant d'une protection sur les territoires des trois collectivités concernées et en tous temps, comprenant uniquement, en vertu de leur 1°, une interdiction portant sur " la destruction, la mutilation, la capture ou l'enlèvement des animaux ". Les mêmes articles énumèrent, au nombre des espèces ainsi protégées, le molosse commun et, pour les arrêtés relatifs au département de la Guadeloupe et la collectivité d'outre-mer de Saint-Martin uniquement, le tadaride du Brésil. <br/>
<br/>
              8. D'une part, le tadaride du Brésil figurant à l'annexe II du protocole adopté à Kingston, il doit, en vertu des stipulations de l'article 11 b) de ce protocole, bénéficier, dans la mesure du possible, d'une protection contre les perturbations intentionnelles, en particulier pendant les périodes de reproduction, d'incubation, d'hibernation, de migration ou pendant toute autre période biologique critique. Il s'ensuit que les associations requérantes sont fondées à soutenir que les arrêtés contestés méconnaissent ces stipulations en ce qu'ils ont inscrit cette espèce non à leur article 2 mais à leur article 3, qui ne comporte aucune interdiction ni mesure permettant de protéger l'espèce contre les perturbations intentionnelles. <br/>
<br/>
              9. D'autre part, s'agissant du molosse commun, le ministre chargé de la protection de la nature justifie l'absence d'interdiction de toute perturbation intentionnelle, ou de mesures de protection à cet égard, par le caractère anthropophile de cette espèce, afin de ne pas rendre impossible toute action visant à empêcher ces chauves-souris de s'installer dans les habitations. Cependant, il ressort des pièces du dossier que si l'espèce vit souvent en milieu anthropisé, notamment dans des habitations, elle s'établit aussi en zone naturelle. Par suite, les associations requérantes sont fondées à soutenir que les arrêtés contestés, qui ne prévoient, à l'égard de cette espèce protégée, aucune mesure permettant de la protéger contre les perturbations intentionnelles, quelles qu'elles soient et en particulier dans le milieu naturel, sont dans cette mesure entachés d'illégalité. <br/>
<br/>
              10. Il résulte de ce qui précède que les associations requérantes sont fondées à demander l'annulation pour excès de pouvoir du refus qui a été opposé à leur demande tendant à la modification des arrêtés contestés, en tant qu'ils organisent la protection du molosse commun et, pour ceux relatifs au département de la Guadeloupe et à la collectivité d'outre-mer de Saint-Martin uniquement, celle du tadaride du Brésil. L'annulation, dans cette mesure, de cette décision implique nécessairement la modification des arrêtés contestés afin qu'ils permettent une protection de ces espèces conforme aux stipulations de l'article 11 b) du protocole adopté à Kingston le 18 janvier 1990 et aux dispositions du code de l'environnement. Par suite, il y a lieu pour le Conseil d'Etat d'ordonner cette édiction dans un délai de six mois.<br/>
<br/>
              11. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par les requérantes au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La décision implicite de rejet née du silence gardé par le ministre d'Etat, ministre de la transition écologique et solidaire et le ministre de l'agriculture et de l'alimentation sur la demande des associations requérantes tendant à la modification des arrêtés litigieux du 17 janvier 2018 est annulée, en tant qu'elle concerne la protection du molosse commun et, pour les arrêtés relatifs au département de la Guadeloupe et à la collectivité d'outre-mer de Saint-Martin, le tadaride du Brésil. <br/>
Article 2 : Il est enjoint aux ministres de modifier les arrêtés litigieux conformément aux motifs énoncés dans la présente décision, dans la mesure précisée au point 10, dans un délai de six mois à compter de la notification de la présente décision.<br/>
Article 3 : Le surplus des conclusions de la requête est rejeté.<br/>
Article 4 : La présente décision sera notifiée à l'Association pour la protection des animaux sauvages et du patrimoine naturel, à l'Association pour la sauvegarde et la réhabilitation de la faune des Antilles et à la ministre de la transition écologique et solidaire.<br/>
Copie en sera adressée à la section du rapport et des études.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
