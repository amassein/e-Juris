<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028389224</ID>
<ANCIEN_ID>JG_L_2013_12_000000350367</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/38/92/CETATEXT000028389224.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème sous-section jugeant seule, 26/12/2013, 350367, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-12-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>350367</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARTHELEMY, MATUCHANSKY, VEXLIARD</AVOCATS>
<RAPPORTEUR>M. Thierry Carriol</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Delphine Hedary</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:350367.20131226</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 24 juin 2011 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre du budget, des comptes publics, de la fonction publique et de la réforme de l'Etat, porte-parole du Gouvernement ; le ministre du budget, des comptes publics, de la fonction publique et de la réforme de l'Etat, porte-parole du Gouvernement demande au Conseil d'Etat d'annuler l'arrêt n° 09MA00160 du 12 mai 2011 par lequel la cour administrative d'appel de Marseille a rejeté son recours tendant, d'une part, à annuler le jugement du tribunal administratif de Marseille du 15 septembre 2008, d'autre part, à rétablir la SARL AM Energie au rôle de l'impôt sur les sociétés et de la contribution additionnelle assise sur cet impôt de l'année 2002 à concurrence des dégrèvements prononcés en première instance à hauteur de 10 456 euros d'impôt sur les sociétés et de 314 euros de contribution additionnelle ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thierry Carriol, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Delphine Hedary, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Barthélemy, Matuchansky, Vexliard, avocat de la SARL AM Energie ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la SARL AM Energie a conclu un contrat de crédit-bail afin de financer la réalisation d'une immobilisation ; qu'elle a bénéficié pour la réalisation de cet investissement d'une subvention de 52 507 euros de l'Office national interprofessionnel des fruits, des légumes, des vins et de l'horticulture qui lui a été versée directement en 2002 ; qu'elle a entendu faire application pour l'imposition de cette subvention du mécanisme d'étalement prévu par les dispositions de l'article 42 septies du code général des impôts ; qu'à l'issue d'une vérification de comptabilité, l'administration lui a notifié des redressements en matière d'impôt sur les sociétés et de contribution additionnelle à cet impôt au titre de l'exercice clos en 2002, par suite de la réintégration dans le résultat de cet exercice de la subvention d'équipement versée directement par l'Office national interprofessionnel des fruits, des légumes, des vins et de l'horticulture ; que, par un jugement du 15 septembre 2008, le tribunal administratif de Marseille a déchargé la SARL AM Energie des impositions en litige en jugeant que la subvention d'équipement pouvait faire l'objet de l'étalement prévu par l'article 42 septies du code général des impôts ; que le ministre du budget, des comptes publics, de la fonction publique et de la réforme de l'Etat, porte-parole du Gouvernement demande l'annulation de l'arrêt du 12 mai 2011 par lequel la cour administrative d'appel de Marseille a rejeté son recours tendant à l'annulation du jugement du tribunal administratif de Marseille ;<br/>
<br/>
              2. Considérant qu'en vertu de l'article 42 septies du code général des impôts dans sa rédaction alors applicable, les subventions d'équipement accordées à une entreprise par l'Etat, les collectivités publiques ou tout autre organisme public à raison de la création ou de l'acquisition d'immobilisations déterminées peuvent être rapportées aux bénéfices imposables en même temps et au même rythme que celui auquel l'immobilisation en cause est amortie et qu'aux termes du dernier alinéa de cet article : " La subvention attribuée par l'intermédiaire d'une entreprise de crédit-bail est répartie, par parts égales, sur les exercices clos au cours de la période couverte par le contrat de crédit-bail, à la condition que la décision accordant cette subvention prévoie son reversement immédiat au crédit-preneur. (...) " ; qu'il résulte de ces dispositions que lorsque les subventions accordées à l'occasion de la construction d'un immeuble financée par un contrat de crédit-bail qui ont été versées à la société de crédit-bail, demeurée propriétaire de l'immeuble durant la durée du contrat, ont fait l'objet d'une décision de rétrocession immédiate à l'entreprise locataire ayant la qualité de crédit preneur, cette dernière peut prétendre au bénéfice de l'étalement de l'imposition des subventions ; qu'il en va de même s'agissant des exploitants bénéficiant directement d'une subvention d'équipement afférente à un bien acquis en crédit-bail ; que, par suite, en jugeant que les dispositions précitées de l'article 42 septies du code général des impôts n'ont eu ni pour objet, ni pour effet d'exclure du bénéfice de l'étalement les exploitants bénéficiant directement d'une subvention d'équipement afférente à un bien acquis en crédit-bail, pour en déduire que la subvention versée directement à la SARL AM Energie pouvait être étalée au rythme de l'amortissement financier du bien acquis sur la durée du contrat de crédit-bail, la cour n'a commis ni erreur de droit, ni erreur de qualification juridique ; qu'il résulte de ce qui précède que le pourvoi du ministre du budget, des comptes publics, de la fonction publique et de la réforme de l'Etat, porte-parole du Gouvernement doit être rejeté ;<br/>
<br/>
<br/>
<br/>				D E C I D E  :<br/>
              				--------------<br/>
<br/>
Article 1er : Le pourvoi du ministre du budget, des comptes publics, de la fonction publique et de la reforme de l'Etat, porte-parole du Gouvernement est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée au ministre de l'économie et des finances et à la SARL AM Energie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
