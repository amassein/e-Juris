<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038784576</ID>
<ANCIEN_ID>JG_L_2019_07_000000416606</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/78/45/CETATEXT000038784576.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 18/07/2019, 416606, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-07-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>416606</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX ; SCP PIWNICA, MOLINIE ; SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>Mme Cécile Chaduteau-Monplaisir</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:416606.20190718</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Grenoble :<br/>
              - d'annuler la décision du 25 mai 2016 par laquelle la caisse d'allocations familiales de l'Isère lui a notifié un indu d'aide exceptionnelle de fin d'année ;<br/>
              - d'annuler la décision du 31 août 2016 par laquelle le président du conseil départemental de l'Isère a confirmé le bien fondé d'une créance de revenu de solidarité active de 12 287,58 euros et a refusé de lui accorder une remise gracieuse de sa dette ;<br/>
              - d'annuler les décisions implicites par lesquelles la caisse d'allocations familiales de l'Isère a confirmé le bien-fondé des indus d'aide exceptionnelle de fin d'année et de prime d'activité et a refusé de lui accorder une remise gracieuse de dette ;<br/>
              - de prononcer la décharge du paiement du solde des sommes réclamées au titre du revenu de solidarité active, de la prime d'activité et de l'aide exceptionnelle de fin d'année ;<br/>
              - de prononcer le remboursement des sommes retenues par la caisse d'allocations familiales pour la récupération de ces indus ;<br/>
              - d'annuler les décisions implicites par lesquelles le président du conseil départemental de l'Isère a rejeté sa demande tendant à suspendre les retenues sur prestations effectuées ;<br/>
              - d'enjoindre au département de l'Isère et à la caisse d'allocations familiales de l'Isère de lui reverser les sommes de 990,20 euros et de 2 073,71 euros.<br/>
<br/>
              Par un jugement nos 1606410, 1606411, 1607055, 1607058, 1607059, 1607060, 1607061 du 22 novembre 2017, le tribunal administratif de Grenoble a rejeté comme portées devant un ordre de juridiction incompétent pour en connaître ses conclusions relatives à l'aide au logement familial, au complément de libre choix de mode de garde, à l'allocation de base et à l'allocation de soutien familial et rejeté le surplus des conclusions de sa demande.<br/>
<br/>
              Par un pourvoi et un nouveau mémoire, enregistrés au secrétariat du contentieux du Conseil d'Etat les 12 décembre 2017 et 22 mai 2018, Mme A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat et du département de l'Isère la somme de 3 000 euros à verser à la SCP Baraduc, Duhamel, Rameix, son avocat, en application des dispositions des articles 37 de la loi du 10 juillet 1991 et L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le code de la sécurité sociale ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Chaduteau-Monplaisir, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de MmeA..., à la SCP Gatineau, Fattaccini, avocat de la caisse d'allocations familiales de l'Isère et à la SCP Piwnica, Molinié, avocat du département de l'Isère ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond qu'après un contrôle de sa situation par la caisse d'allocations familiales de l'Isère, Mme A...s'est vu réclamer, par une décision du 25 mai 2016, le remboursement d'une somme totale de 31 423,90 euros correspondant à un trop-perçu d'aide exceptionnelle de fin d'année, de prime d'activité, d'allocation de logement familiale, de complément de libre choix du mode de garde, d'allocation de base, de revenu de solidarité active et d'allocation de soutien familial. Mme A...a contesté devant le tribunal administratif de Grenoble la décision du président du conseil départemental de l'Isère du 31 août 2016 rejetant son recours administratif et sa demande de remise de dette concernant l'indu de revenu de solidarité active, les décisions implicites de rejet de ses recours concernant les indus d'aide exceptionnelle de fin d'année et de prime d'activité et les décisions implicites de rejet de ses demandes de suspension des retenues opérées sur prestations. Par un jugement du 22 novembre 2017, ce tribunal a rejeté comme portées devant un ordre de juridiction incompétent pour en connaître les conclusions de Mme A...relatives à l'allocation de logement familiale, au complément de libre choix du mode de garde, à l'allocation de base et à l'allocation de soutien familial et a rejeté comme non fondé le surplus des conclusions de sa demande. Eu égard aux moyens qu'elle soulève, Mme A...doit être regardée comme demandant l'annulation de ce jugement en tant qu'il rejette ses conclusions relatives à la récupération des indus de revenu de solidarité active, de prime d'activité et d'aide exceptionnelle de fin d'année et aux retenues sur prestations opérées pour la récupération de la prime d'activité. <br/>
<br/>
              2. D'une part, aux termes de l'article L. 262-16 du code de l'action sociale et des familles : " Le service du revenu de solidarité active est assuré, dans chaque département, par les caisses d'allocations familiales et, pour leurs ressortissants, par les caisses de mutualité sociale agricole ". Le huitième alinéa de l'article L. 262-40 de ce code prévoit que les organismes chargé du versement du revenu de solidarité active " réalisent les contrôles relatifs au revenu de solidarité active selon les règles, procédures et moyens d'investigation applicables aux prestations de sécurité sociale ". De même, l'article L. 845-1 du code de la sécurité sociale prévoit que les directeurs des caisses d'allocations familiales et des caisses de mutualité sociale agricole " procèdent aux contrôles et aux enquêtes concernant la prime d'activité et prononcent, le cas échéant, des sanctions selon les règles, procédures et moyens d'investigation prévus aux articles L. 114-9 à L. 114-17, L. 114-19 à L. 114-22, L. 161-1-4 et L. 161-1-5 ".<br/>
<br/>
              3. D'autre part, aux termes de l'article L. 114-19 du code de la sécurité sociale, dans sa rédaction applicable au litige : " Le droit de communication permet d'obtenir, sans que s'y oppose le secret professionnel, les documents et informations nécessaires : / 1° Aux agents des organismes de sécurité sociale pour contrôler la sincérité et l'exactitude des déclarations souscrites ou l'authenticité des pièces produites en vue de l'attribution et du paiement des prestations servies par lesdits organismes (...) ". Aux termes de l'article L. 114-21 du même code : " L'organisme ayant usé du droit de communication en application de l'article L. 114-19 est tenu d'informer la personne physique ou morale à l'encontre de laquelle est prise la décision de supprimer le service d'une prestation ou de mettre des sommes en recouvrement, de la teneur et de l'origine des informations et documents obtenus auprès de tiers sur lesquels il s'est fondé pour prendre cette décision. Il communique, avant la mise en recouvrement ou la suppression du service de la prestation, une copie des documents susmentionnés à la personne qui en fait la demande ". <br/>
<br/>
              4. Il résulte de ces dispositions que les caisses d'allocations familiales et les caisses de mutualité sociale agricole, chargées du service du revenu de solidarité active et de la prime d'activité, peuvent faire usage, pour contrôler la sincérité et l'exactitude des déclarations souscrites ou l'authenticité des pièces produites en vue de l'attribution et du paiement de ces prestations, du droit de communication instauré par l'article L. 114-19 du code de la sécurité sociale, en respectant les garanties procédurales qui s'attachent, en vertu de l'article L. 114-21 du même code, à l'exercice de ce droit par un organisme de sécurité sociale. Il incombe ainsi à l'organisme ayant usé du droit de communication, avant la suppression du service de la prestation ou la mise en recouvrement, d'informer l'allocataire à l'encontre duquel est prise la décision de supprimer le droit au revenu de solidarité active ou à la prime d'activité ou de récupérer un indu de ces prestations, tant de la teneur que de l'origine des renseignements qu'il a obtenus de tiers par l'exercice de son droit de communication et sur lesquels il s'est fondé pour prendre sa décision. Cette obligation a pour objet de permettre à l'allocataire, notamment, de discuter utilement leur provenance ou de demander que les documents qui, le cas échéant, contiennent ces renseignements soient mis à sa disposition avant la récupération de l'indu ou la suppression du service de la prestation, afin qu'il puisse vérifier l'authenticité de ces documents et en discuter la teneur ou la portée.<br/>
<br/>
              5. Pour écarter le moyen tiré de ce que Mme A...n'avait pas été informée de la mise en oeuvre du droit de communication, non plus que de la teneur et de l'origine des informations recueillies, le tribunal administratif de Grenoble, qui a jugé que la caisse d'allocations familiales de l'Isère avait usé du droit de communication auprès de l'établissement bancaire détenant le compte de l'intéressée, s'est borné à relever qu'il ne résultait pas de l'instruction qu'elle aurait vainement sollicité une copie des documents obtenus dans le cadre de ce droit de communication. Il résulte de ce qui a été dit ci-dessus qu'en statuant ainsi, il a commis une erreur de droit.<br/>
<br/>
              6. Ce moyen suffit à entraîner l'annulation du jugement attaqué en tant qu'il rejette les conclusions de Mme A...relatives à la récupération des indus de revenu de solidarité active, de prime d'activité et d'aide exceptionnelle de fin d'année et, par voie de conséquence, en tant qu'il rejette ses conclusions relatives aux retenues sur prestations opérées pour la récupération de la prime d'activité. Par suite, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, Mme A...est fondée à demander cette annulation. <br/>
<br/>
              7. Mme A...a obtenu le bénéfice de l'aide juridictionnelle. Par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Baraduc, Duhamel, Rameix, son avocat, renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge du département de l'Isère une somme de 1 500 euros à verser à cette société. Les dispositions de l'article L. 761-1 du code de justice administrative font en revanche obstacle à ce qu'il soit fait droit aux conclusions de la caisse d'allocations familiales de l'Isère présentées à ce titre.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                           --------------<br/>
<br/>
Article 1er : L'article 2 du jugement du tribunal administratif de Grenoble du 22 novembre 2017 est annulé en tant qu'il statue sur les conclusions des demandes de Mme A...relatives à la récupération des indus de revenu de solidarité active, de prime d'activité et d'aide exceptionnelle de fin d'année et aux retenues sur prestations opérées pour la récupération de la prime d'activité. <br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, au tribunal administratif de Grenoble.<br/>
Article 3 : Le département de l'Isère versera à la SCP Baraduc, Duhamel, Rameix, avocat de MmeA..., une somme de 1 500 euros en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
<br/>
Article 4 : Les conclusions de la caisse d'allocations familiales de l'Isère présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à Mme B...A..., à la ministre des solidarités et de la santé et au département de l'Isère.<br/>
Copie en sera adressée à la caisse d'allocations familiales de l'Isère. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
