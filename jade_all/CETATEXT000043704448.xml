<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043704448</ID>
<ANCIEN_ID>JG_L_2021_06_000000447672</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/70/44/CETATEXT000043704448.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 25/06/2021, 447672, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-06-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>447672</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Dorothée Pradines</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Raphaël Chambon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:447672.20210625</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Melun d'annuler les opérations électorales qui se sont déroulées le 15 mars 2020 pour la désignation des conseillers municipaux et communautaires de la commune de L'Haÿ-les-Roses (Val-de-Marne). Par un jugement n° 2002763 du 19 novembre 2020, le tribunal administratif a rejeté sa protestation.<br/>
<br/>
              Par une requête, enregistrée le 15 décembre 2020 au secrétariat du contentieux du Conseil d'État, M. B... demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de faire droit à sa protestation.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Dorothée Pradines, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. À l'issue du premier tour des élections municipales et communautaires qui se sont déroulées à L'Haÿ-les-Roses (Val-de-Marne) le 15 mars 2020, les trente-neuf sièges de conseillers municipaux et le siège de conseiller communautaire ont été pourvus. Trente des sièges de conseillers municipaux et le siège de conseiller communautaire ont été attribués à des candidats de la liste " Plus belle l'Haÿ avec Vincent H... " conduite par M. G... H..., qui a obtenu 3 948 voix, soit 54,26 % des suffrages exprimés, tandis que six sièges de conseillers municipaux ont été attribués à des candidats de la liste " 2020 L'Haÿ en commun " conduite par M. F... D..., qui a obtenu 2 223 voix, soit 30,55 % des suffrages exprimés et les trois sièges de conseillers municipaux restants ont été attribués à des candidats de la liste  " Réveillons L'Haÿ " conduite par M. E... C... qui a obtenu 1 104 voix, soit 15,17 % des suffrages exprimés. M. B..., candidat sur la liste de M. C..., demande l'annulation du jugement du 19 novembre 2020 par lequel le tribunal administratif de Melun a rejeté la protestation qu'il a formée contre les opérations électorales qui se sont déroulées le 15 mars 2020.<br/>
<br/>
              2. Aux termes de l'article L. 52-1 du code électoral : " Pendant les six mois précédant le premier jour du mois d'une élection et jusqu'à la date du tour de scrutin où celle-ci est acquise, l'utilisation à des fins de propagande électorale de tout procédé de publicité commerciale par la voie de la presse ou par tout moyen de communication audiovisuelle est interdite. A compter du premier jour du sixième mois précédant le mois au cours duquel il doit être procédé à des élections générales, aucune campagne de promotion publicitaire des réalisations ou de la gestion d'une collectivité ne peut être organisée sur le territoire des collectivités intéressées par le scrutin. Sans préjudice des dispositions du présent chapitre, cette interdiction ne s'applique pas à la présentation, par un candidat ou pour son compte, dans le cadre de l'organisation de sa campagne, du bilan de la gestion des mandats qu'il détient ou qu'il a détenus. Les dépenses afférentes sont soumises aux dispositions relatives au financement et au plafonnement des dépenses électorales contenues au chapitre V bis du présent titre ". <br/>
<br/>
              3. En premier lieu, si M. B... soutient que la commune de L'Haÿ-les-Roses a organisé quarante événements pendant les six mois précédant l'élection au cours desquels le maire ou un de ses adjoints a prononcé un discours et que ces événements doivent être regardés comme ayant constitué, au profit de la liste conduite par le maire sortant, une campagne de promotion publicitaire prohibée par les dispositions de l'article L. 52-1 du code électoral, citées ci-dessus, il ne résulte pas de l'instruction que ces événements - au nombre desquels figurent les conseils de quartier, des réunions de quartier à l'occasion de Noël et de la galette des rois et des réunions dédiées aux seniors, dont la fête des seniors du 3 mars 2020 -, lesquels relèvent de l'activité habituelle de cette commune, aient donné lieu à l'exposé d'un programme électoral ou à l'expression d'éléments de polémique électorale. Par suite, M. B... n'est pas fondé à soutenir que ces événements ont été organisés en méconnaissance des dispositions précitées. <br/>
<br/>
              4. En deuxième lieu, M. B... soutient que la campagne de voeux du maire sortant a méconnu les dispositions de l'article L. 52-1 du code électoral. Toutefois, il résulte de l'instruction que cette campagne de voeux a consisté, d'une part, en l'affichage sur des panneaux publicitaires de la ville d'une affiche portant, outre des photos de la ville, la mention " Vincent H... et le conseil municipal vous souhaitent une très belle année 2020 " et, d'autre part, en l'envoi aux habitants de la ville d'une carte de voeux reprenant le visuel de l'affiche et signée du maire. La campagne de voeux en cause, qui s'inscrit dans la continuité des campagnes de voeux des années précédentes, qui ne fait pas référence aux futures élections municipales et qui ne contient pas d'élément de polémique électorale, ne dépasse pas le cadre de la communication institutionnelle. Par suite, le grief tiré de ce que cette campagne de voeux devait être regardée comme ayant constitué une campagne de promotion publicitaire au sens de l'article L. 52-1 du code électoral au profit de la liste conduite par le maire sortant et prohibée par ces dispositions doit être écarté.<br/>
<br/>
              5. En troisième lieu, si M. B... fait valoir que l'affichage relatif au projet dit " Coeur de ville " sur de grands panneaux du centre-ville, en ce qu'il présenterait des visuels des projets immobiliers du maire-candidat, un slogan et un argumentaire, assurerait la promotion électorale du maire sortant, il ne résulte pas de l'instruction que cet affichage puisse être considéré comme un élément de propagande électorale et aurait, par suite, été réalisé en méconnaissance de l'article L. 52-1 du code électoral.<br/>
<br/>
              6. En dernier lieu, M. B... n'est en tout état de cause pas davantage fondé à invoquer à propos des faits qui viennent d'être exposés la méconnaissance de l'article L. 51 du code électoral, relatif aux emplacements spéciaux réservés pour les affiches électorales. <br/>
<br/>
              7. Il résulte de ce qui précède que M. B... n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Melun a rejeté sa protestation. <br/>
<br/>
              8. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par les défendeurs au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. B... est rejetée.<br/>
Article 2 : Les conclusions présentées pour M. H... et autres au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. A... B... et à M. G... H..., premier dénommé, pour l'ensemble des défendeurs.<br/>
Copie en sera adressée au ministre de l'intérieur, à la Commission nationale des comptes de campagne et des financements politiques et au préfet du Val-de-Marne.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
