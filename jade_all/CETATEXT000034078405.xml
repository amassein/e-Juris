<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034078405</ID>
<ANCIEN_ID>JG_L_2017_02_000000407645</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/07/84/CETATEXT000034078405.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 10/02/2017, 407645, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-02-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>407645</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2017:407645.20170210</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au juge des référés du tribunal administratif de Nantes, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre à l'Office français de l'immigration et de l'intégration (OFII) de mettre à sa disposition, sans délai, les conditions matérielles d'accueil liées à sa qualité de demandeur d'asile, et, à titre subsidiaire, d'enjoindre au préfet de la Loire-Atlantique de lui indiquer, sans délai, un lieu adapté susceptible de l'accueillir, sous astreinte de 100 euros par jour de retard. Par une ordonnance n° 1700988 du 2 février 2017, le juge des référés du tribunal administratif de Nantes a rejeté sa demande. <br/>
<br/>
              Par une requête, enregistrée le 2 février 2017 au secrétariat du contentieux du Conseil d'Etat, M.B...	 demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) d'enjoindre à l'Office français de l'immigration et de l'intégration (OFII) de mettre à sa disposition, sans délai, les conditions matérielles d'accueil liées à sa qualité de demandeur d'asile, sous astreinte de 200 euros par jour de retard ;<br/>
<br/>
              3°) à titre, subsidiaire, d'enjoindre au préfet de la Loire-Atlantique de lui indiquer, sans délai, un lieu adapté susceptible de l'accueillir, sous astreinte de 200 euros par jour de retard ;<br/>
<br/>
              4°) à défaut, et en tout état de cause, de renvoyer à la Cour de justice de l'Union européenne une question préjudicielle relative à la portée de la directive 2003/9/CE du 27 janvier 2003 relative à des normes minimales pour l'accueil des demandeurs d'asile dans les Etats membres ;<br/>
<br/>
              5°) de mettre à la charge de l'Office français de l'immigration et de l'intégration la somme de 1 500 euros et de l'Etat la somme de 1 700 euros, au titre des dispositions de l'article 37 de la loi du 10 juillet 1991 et de l'article L. 761-1 du code de justice administrative, sous réserve du renoncement au bénéfice de l'aide juridictionnelle.<br/>
<br/>
<br/>
              Il soutient que : <br/>
              - la condition d'urgence est remplie ; <br/>
              - il est porté une atteinte grave et manifestement illégale au droit d'asile, et, à titre subsidiaire, au principe de dignité humaine et au droit à un hébergement d'urgence, dès lors que le requérant ne s'est pas vu offrir une orientation d'hébergement par l'OFII, et qu'un examen de sa vulnérabilité n'est pas intervenu aux termes de la directive 2013/33/UE du 26 juin 2013 et du code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              -  l'ordonnance contestée est entachée d'une erreur de droit dès lors que, d'une part, le juge des référés, n'a pas considéré que la privation des conditions d'accueil constituait une méconnaissance manifeste des exigences qui découlent du droit d'asile, alors qu'une atteinte au droit d'accueil, dont il est le corollaire, a été établie et, d'autre part, a subordonné l'illégalité de l'atteinte au droit d'accueil aux conditions tenant aux " moyens dont dispose l'autorité administrative compétente " ou à la " situation personnelle " du demandeur d'asile, en méconnaissance des dispositions de la directive 2013/33/UE du 26 juin 2013 et de la directive 2003/9/CE du 27 janvier 2003 ; à défaut, une question préjudicielle devrait être posée à la Cour de justice de l'Union européenne pour qu'elle interprète les dispositions de la directive 2003/9/CE du 27 janvier 2003 ;<br/>
              - l'ordonnance contestée est entachée d'une erreur de fait en ne tenant pas compte de la vulnérabilité particulière du demandeur d'asile au regard du certificat médical établi le 18 janvier 2017.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la charte des droits fondamentaux de l'Union européenne ;<br/>
              - la directive 2003/9/CE du Conseil du 27 janvier 2003 relative à des normes minimales pour l'accueil des demandeurs d'asile dans les États membres ;<br/>
              - la directive 2013/33/UE du Parlement Européen et du Conseil du 26 juin 2013 établissant des normes pour l'accueil des personnes demandant la protection internationale ; <br/>
              -  le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              -  le code de l'action sociale et des familles ;<br/>
              -  la loi n° 91-647 du 10 juillet 1991 relative à l'aide juridique ;<br/>
              -  le code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". En vertu de l'article L. 522-3 du même code, le juge des référés peut rejeter une requête par une ordonnance motivée, sans instruction contradictoire ni audience publique, lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée. À cet égard, il appartient au juge d'appel de prendre en considération les éléments recueillis par le juge du premier degré dans le cadre de la procédure écrite et orale qu'il a diligentée.<br/>
<br/>
              2. Il résulte de l'instruction diligentée par le juge des référés du tribunal administratif de Nantes en première instance que M.B..., ressortissant guinéen, dispose d'une attestation préfectorale du 5 décembre 2016 qui lui reconnaît la qualité de demandeur d'asile. Il a perçu l'allocation de demandeur d'asile depuis l'acceptation de sa première offre de prise en charge, à l'exception du mois de décembre 2016. L'intéressé, qui n'a pas encore perçu le versement de l'allocation, est toutefois dépourvu de logement. Il a saisi, le 1er février 2017, le juge des référés du tribunal administratif de Nantes, sur le fondement de l'article L. 521-2 du code de justice administrative, d'une demande tendant notamment à ce qu'il soit enjoint au directeur de l'OFII de mettre à sa disposition les conditions matérielles d'accueil, sous astreinte de cent euros par jour de retard et, à titre subsidiaire, à ce qu'il soit enjoint au préfet de la Loire-Atlantique de lui indiquer un lieu susceptible de l'accueillir, sous astreinte de cent euros par jour de retard. Par une ordonnance n° 1700988 du 2 février 2017, le juge des référés a rejeté sa demande. M. B... relève appel de cette ordonnance. <br/>
              3. Si, d'une part, la privation du bénéfice des mesures prévues par la loi afin de garantir aux demandeurs d'asile des conditions matérielles d'accueil décentes, jusqu'à ce qu'il ait été statué sur leur demande, est susceptible de constituer une atteinte grave et manifestement illégale à la liberté fondamentale que constitue le droit d'asile, le caractère grave et manifestement illégal d'une telle atteinte s'apprécie en tenant compte des moyens dont dispose l'autorité administrative compétente et de la situation du demandeur. Ainsi, le juge des référés, qui apprécie si les conditions prévues par l'article L. 521-2 du code de justice administrative sont remplies à la date à laquelle il se prononce, ne peut faire usage des pouvoirs qu'il tient de cet article en adressant une injonction à l'administration que dans le cas où, d'une part, le comportement de celle-ci fait apparaître une méconnaissance manifeste des exigences qui découlent du droit d'asile et où, d'autre part, il résulte de ce comportement des conséquences graves pour le demandeur d'asile, compte tenu notamment de son âge, de son état de santé ou de sa situation de famille.<br/>
<br/>
              4. Il appartient, d'autre part, aux autorités de l'Etat de mettre en oeuvre le droit à l'hébergement d'urgence reconnu par la loi à toute personne sans abri qui se trouve en situation de détresse médicale, psychique et sociale. Une carence caractérisée dans l'accomplissement de cette tâche peut faire apparaître, pour l'application de l'article L. 521-2 du code de justice administrative, une atteinte grave et manifestement illégale à une liberté fondamentale lorsqu'elle entraîne des conséquences graves pour la personne intéressée. Il incombe au juge des référés d'apprécier dans chaque cas les diligences accomplies par l'administration en tenant compte des moyens dont elle dispose ainsi que de l'âge, de l'état de la santé et de la situation de famille de la personne intéressée.<br/>
<br/>
              5. Le requérant, qui est âgé de dix-neuf ans et célibataire, n'apporte en appel aucun élément de nature à infirmer l'appréciation portée, au regard des critères mentionnés aux points 3 et 4 et compte tenu des indications données devant lui par l'Office français de l'immigration et de l'intégration, par le juge des référés de première instance. Ainsi que l'a constaté à bon droit le juge des référés du tribunal administratif de Nantes et pour les motifs qu'il a retenus, aucune méconnaissance grave et manifeste des obligations qui s'imposent en la matière à l'administration ne peut être donc retenue en l'espèce. En outre, l'annulation, par le Conseil d'Etat statuant au contentieux, du montant de l'allocation versée aux demandeurs d'asile en l'absence d'hébergement ne conduit pas à faire apparaître par elle-même une illégalité manifeste dans la manière dont l'Office français de l'immigration et de l'intégration a traité le cas du requérant.<br/>
              6. Il résulte de ce qui précède, et sans qu'il y ait lieu de saisir la Cour de justice de l'Union européenne d'une question préjudicielle, qu'il est manifeste que l'appel de M. B... ne peut être accueilli. La requête, y compris les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991, doit dès lors être rejetée selon la procédure prévue par l'article L. 522-3 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. B...est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. A...B....<br/>
Copie en sera adressée au préfet de la Loire-Atlantique.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
