<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000018503463</ID>
<ANCIEN_ID/>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/18/50/34/CETATEXT000018503463.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 18/03/2008, 312301, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2008-03-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>312301</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Dandelot</PRESIDENT>
<AVOCATS>SCP BOULLEZ ; SCP VIER, BARTHELEMY, MATUCHANSKY</AVOCATS>
<RAPPORTEUR>M. Marc  Dandelot</RAPPORTEUR>
<COMMISSAIRE_GVT/>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 15 janvier 2008 au secrétariat du contentieux du Conseil d'Etat, présentée pour la société ACTIS, dont le siège est avenue de Catalogne à Limoux (11300) ; la société ACTIS demande au juge des référés du Conseil d'Etat :
              
              1°) de suspendre, sur le fondement de l'article L. 521-1 du code de justice administrative, le communiqué de presse du 7 novembre 2007 par lequel la direction générale de l'urbanisme, de l'habitat et de la construction a apporté les précisions sur les règles à appliquer pour respecter la réglementation concernant l'utilisation des isolants dans les bâtiments pour l'application de l'arrêté du 3 mai 2007 relatif aux caractéristiques thermiques et à la performance énergétique des bâtiments existants et ses annexes ;
              
              2°) de mettre à la charge de l'Etat la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative ;
              
     
              
              elle soutient que la condition d'urgence est remplie dès lors que la décision contestée porte une atteinte irréversible à la structure concurrentielle du marché des isolants et qu'elle entraîne une baisse importante de son chiffre d'affaire ; qu'il existe un doute sérieux quant à la légalité de la décision attaquée ; qu'elle est recevable à contester la légalité du communiqué dès lors qu'en imposant de faire obligatoirement application des valeurs définies dans les règles ThBat, il comporte des dispositions impératives à caractère général faisant grief ; que la décision contestée est entachée de défaut de signature et de nom de l'auteur ; qu'en prescrivant une application impérative de la norme ThBat, il donne une interprétation de l'arrêté du 3 mai 2007 qui en méconnaît le sens et la portée ; que la décision attaquée a été prise selon une procédure irrégulière, méconnaissant les exigences de la directive (CE) 98/34 du 22 juin 1998, dès lors que les règles ThBat sont des règles techniques obligatoires qui doivent être communiquées préalablement à la Commission européenne ; qu'il est de l'office du juge des référés de suspendre l'application d'une règle technique qui n'aurait pas été notifiée ; que ce défaut de notification méconnait également l'obligation de coopérer à l'application du droit communautaire faite à chacun des Etats ; qu'en reconnaissant une valeur impérative à une règle technique qui n'était qu'indicative jusque lors, sans période de transition, la décision attaquée méconnait les principes de confiance légitime et de sécurité juridique ;
              
     
	
              Vu la décision dont la suspension est demandée ;
              
              Vu la requête à fin d'annulation de la même décision ; 
              
              Vu, enregistré le 3 mars 2008, le mémoire en défense présenté par le ministre du logement et de la ville qui conclut au rejet de la requête et à ce que soit mise à la charge de la société requérante la somme de 3 500 euros en application des dispositions de l'article L. 761-1 du code de justice administrative ; le ministre soutient que la requête de la société ACTIS est irrecevable dès lors que le communiqué de presse litigieux est conforme aux dispositions de l'arrêté du 3 mai 2007 ; qu'en effet, celui-ci ne prévoit la possibilité d'utiliser soit des valeurs par défaut soit d'autres valeurs présentes dans les règles ThBat que dans le cas de parois déjà existantes ; qu'il renvoie aux règles ThBat pour de nouveaux produits installés ; que celles-ci indiquent, concernant les produits réfléchissants, que la résistance thermique est déterminée soit par un avis technique soit par l'utilisation des valeurs par défaut figurant dans les règles du Th&#143;U ; que le communiqué contesté est intervenu afin de prévenir tout vide juridique dans les cas où il n'y aurait pas d'avis technique, en précisant qu'alors les valeurs par défaut s'appliquent ; qu'ainsi le communiqué de presse ne crée aucune règle de droit nouvelle ni ne modifie le sens et la portée desdites dispositions et ne fait donc pas grief ; qu'il ne peut donc faire l'objet d'un recours ; que la condition d'urgence n'est pas remplie dès lors que la société ACTIS ne produit pas des documents comptables mais une simple attestation établie, non par un expert comptable indépendant, mais par son responsable administratif et financier ; que cette attestation fait état d'une baisse du chiffre d'affaire de la société ACTIS entre le 1er juillet 2007 et le 31 octobre 2007, avec une aggravation en novembre 2007, soit pour une période majoritairement antérieure à la date de diffusion du communiqué contesté ; qu'aucun élément ne permet d'établir que les contraintes qui pèseraient sur la société ACTIS du fait de ce communiqué seraient disproportionnées par rapport à ses avantages notamment en termes de réduction de la consommation d'énergie et de l'émission de gaz à effet de serre ; qu'il n'y a pas de doute sérieux quant à la légalité de la décision attaquée ; qu'en effet, la société ACTIS n'établit pas que le communiqué contesté n'aurait pas d'auteur identifié ; que le moyen tiré du défaut de notification de l'acte à la Commission européenne est inopérant devant le juge des référés ; que l'arrêté du 3 mai 2007 a été notifié ; que le moyen tiré de la méconnaissance de l'obligation de notification préalable des valeurs ThBat est mal fondé dès lors qu'elles figurent dans un document élaboré par les professionnels du bâtiment, conçu comme un code de bonne conduite et n'édictent pas de règles techniques au sens de la directive (CE) n° 98/34 ; qu'il n'existe pas de spécifications techniques spécifiques aux isolants minces multicouches réflecteurs ; que la réglementation thermique est en conformité avec le travail des organismes de normalisation européenne en se fondant sur les agréments techniques européens et sur des avis techniques délivrés sur la base des normes d'essai européennes applicables à tous les types d'isolants ; que les autorités européennes retiennent l'utilisation des méthodes d'essai normalisées reconnues ;
              
              Vu le mémoire en réplique enregistré le 10 mars 2008, présenté pour la société ACTIS ; il tend aux mêmes fins que la requête par les mêmes moyens ;
	
     
     
              Après avoir convoqué à une audience publique, d'une part, la société ACTIS et, d'autre part, le ministre du logement et de la ville ;
              
              Vu le procès-verbal de l'audience publique du 10 mars 2008 à 12 heures au cours de laquelle ont été entendus :
              
              - Me Boullez, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la société ACTIS ;
              
              - les représentants de la société ACTIS ;
              
              - Me Barthélémy, avocat au Conseil d'Etat et à la Cour de cassation, avocat du ministre du logement et de la ville ;
              
              - les représentants du ministre du logement et de la ville ;
              
              et à l'issue de laquelle l'instruction a été prolongée ;
              
              Vu les observations complémentaires, enregistrées les 11 et 13 mars 2008, présentées pour la société ACTIS, qui tendent aux même fins que la requête, et les diverses pièces propres à établir l'urgence ;
              
              Vu les observations complémentaires, enregistrées le 14 mars 2008, présentées pour le ministre du logement et de la ville ;
              
              
              Après avoir convoqué à une deuxième audience publique, d'une part, la société ACTIS et, d'autre part, le ministre du logement et de la ville ;
              
              Vu le procès-verbal de l'audience publique du 14 mars 2008 à 14 heures 30  au cours de laquelle ont été entendus :
              
              - Me Boullez, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la société ACTIS ;
              
              - les représentants de la société ACTIS ;
              
              - Me Barthélémy, avocat au Conseil d'Etat et à la Cour de cassation, avocat du ministre du logement et de la ville ;
              
              - les représentants du ministre du logement et de la ville ;
              
              Vu les autres pièces du dossier ;
              Vu la directive (CE) n° 98/34 du 22 juin 1998 prévoyant une procédure d'information dans le domaine des normes et des réglementations techniques ;
              
              Vu la directive (CE) n° 2002/91 du 16 décembre 2002 relative à la performance énergétique des bâtiments ;
              
              Vu le code de la construction et de l'habitation ;
              
              Vu la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations ;
              
              Vu la loi n° 2004-1343 du 9 décembre 2004 de simplification du droit ;
              
              Vu le décret n° 2000-1153 du 29 novembre 2000 relatif aux caractéristiques thermiques des constructions ;
              
              Vu le décret n° 2007-363 du 19 mars 2007 relatif aux études de faisabilité des approvisionnements en énergie, aux caractéristiques thermiques et à la performance énergétique des bâtiments existants et à l'affichage du diagnostic de performance énergétique ;
              
              Vu l'arrêté du 29 novembre 2000 relatif aux caractéristiques thermiques des bâtiments nouveaux et des parties nouvelles de bâtiments modifié ;
              
              Vu l'arrêté du 1er décembre 2000 portant approbation des méthodes de calcul modifié ;
              
              Vu l'arrêté du 3 mai 2007 relatif aux caractéristiques thermiques et à la performance énergétique des bâtiments existants ;
              
	Vu le code de justice administrative ;
		
     
     <br/>
              Considérant qu'aux termes de l'article L. 521-1 du code de justice administrative : « Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision » ;
              
              
              Sur la nature de la décision dont la suspension est demandée :
              
              Considérant que si l'interprétation que, par voie de circulaires ou d'instructions, l'autorité administrative donne des lois et règlements qu'elle a pour mission de mettre en oeuvre, n'est pas susceptible d'être déférée au juge de l'excès de pouvoir, il en va autrement lorsqu'une telle instruction contient des dispositions impératives ; que tel est le cas de l'instruction rendue publique par le communiqué de presse de la direction générale de l'urbanisme, de l'habitat et de la construction du 7 novembre 2007, faisant savoir aux opérateurs que seule la résistance thermique R répondant aux conditions qu'elle indique doit être utilisée pour l'application de l'arrêté du 3 mai 2007 relatif aux caractéristiques thermiques et à la performance énergétique des bâtiments existants, et que « pour les autres produits qui ne sont évalués ni selon une norme EN, ni selon un agrément technique européen ou un avis technique, pour respecter la réglementation, il est fait obligatoirement application des valeurs, fonction de l'épaisseur d'isolant et de sa nature, définies dans les règles ThBat auxquelles les textes réglementaires et en particulier l'arrêté du 3 mai 2007 fait référence » ;
              
              
              Sur l'urgence :
              
              Considérant que la société ACTIS, qui fabrique et distribue des produits isolants minces multicouches réflecteurs (IMMR), qui sont pour elle stratégiques, établit que son potentiel commercial est directement, et de façon significative, impacté par l'application de l'instruction attaquée ; qu'elle justifie donc de l'urgence à en demander la suspension ;
              
              
              Sur les moyens :
              		
              Considérant que la seule circonstance que le communiqué de presse ne mentionne pas le signataire de l'instruction publiée ne suffit à établir qu'elle ait été prise par une autorité incompétente ; que l'instruction attaquée n'a pas modifié l'arrêté du 3 mai 2007, dont elle se borne à demander une application impérative ; qu'il ressort du dossier que l'arrêté a fait l'objet, en tant que norme technique, d'une notification à la Commission européenne au titre de la directive 98/34/CE du Parlement européen et du Conseil du 22 juin 1998 ; que ni la Commission, ni aucun Etat membre, n'a fait à cette occasion d'observation ; que si l'un des objets de l'arrêté est de rendre obligatoires, à titre supplétif, pour certains produits, les règles Thbat auquel il se réfère, il ressort du dossier que ces règles Thbat sont des règles professionnelles objectives inspirées de règles établies au plan européen ; qu'ainsi le moyen tiré de ce que la notification de l'arrêté du 3 mai 2007 aurait été irrégulière, faute d'être accompagnée d'une notification des règles Thbat, ne paraît pas, en l'état de l'instruction, créer un doute sérieux sur la légalité de la décision attaquée ; que, par suite, la demande de suspension ne peut qu'être rejetée ; 
              
              
              Sur les conclusions au titre de l'article L. 761-1 du code de justice administrative :
              
              Considérant que ces dispositions font obstacle à ce qu'il soit fait droit aux conclusions de la société ACTIS ; dans les circonstances de l'espèce, il n'y a pas lieu de faire droit aux conclusions du ministre du logement et de la ville ;
     
     <br/>

O R D O N N E :
------------------

Article 1er : La requête de la société ACTIS est rejetée.
Article 2 : Les conclusions présentées par le ministre du logement et de la ville au titre de l'article L. 761-1 du code de justice administrative sont rejetées.
Article 3 : La présente ordonnance sera notifiée à la société ACTIS et au ministre du logement et de la ville.

<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
