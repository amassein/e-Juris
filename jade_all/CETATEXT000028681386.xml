<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028681386</ID>
<ANCIEN_ID>JG_L_2014_03_000000366874</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/68/13/CETATEXT000028681386.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  9ème sous-section jugeant seule, 03/03/2014, 366874, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-03-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>366874</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 9ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER</AVOCATS>
<RAPPORTEUR>M. Jean-Luc Matt</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Claire Legras</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:366874.20140303</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>VU LA PROCEDURE SUIVANTE :<br/>
<br/>
 Procédure contentieuse antérieure<br/>
<br/>
              La SAS Monoprix exploitation a demandé au tribunal administratif de Montreuil la décharge des cotisations supplémentaires de taxe professionnelle auxquelles elle a été assujettie au titre des années 2005 et 2006 pour son établissement situé 1, boulevard de Chanzy à Montreuil (Seine-Saint-Denis). Par un jugement n° 0900775 du 17 mai 2011, le tribunal administratif de Montreuil a fait droit à sa demande.<br/>
<br/>
              Par un arrêt n° 11VE02637 du 28 décembre 2012, la cour administrative d'appel de Versailles a rejeté le recours formé par le ministre de l'économie et des finances contre ce jugement. <br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi et un nouveau mémoire, enregistrés respectivement les 15 mars 2013 et 31 janvier 2014 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'économie et des finances demande au Conseil d'Etat d'annuler l'arrêt du 28 décembre 2012 de la cour administrative d'appel de Versailles.<br/>
<br/>
              Un mémoire en défense, enregistré le 23 décembre 2013, a été présenté pour la SAS Monoprix exploitation. Elle conclut au rejet du pourvoi et à ce que soit mise à la charge de l'Etat une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu : <br/>
              - les autres pièces du dossier ;<br/>
<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Matt, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Claire Legras, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Blancpain, Soltner, avocat de la SAS Monoprix exploitation ;<br/>
<br/>
<br/>
<br/>CONSIDERANT CE QUI SUIT :<br/>
<br/>
              1. Aux termes de l'article 1467 du code général des impôts, dans sa rédaction applicable aux impositions en litige : " La taxe professionnelle a pour base : / 1° Dans le cas des contribuables autres que ceux visés au 2° : / a. la valeur locative, telle qu'elle est définie aux articles 1469, 1518 A et 1518 B, des immobilisations corporelles dont le redevable a disposé pour les besoins de son activité professionnelle pendant la période de référence définie aux articles 1467 A et 1478, à l'exception de celles qui ont été détruites ou cédées au cours de la même période (...) ". Aux termes de l'article 1469 du même code, alors applicable : " La valeur locative est déterminée comme suit : / (...)  3° quater. Le prix de revient d'un bien cédé n'est pas modifié lorsque ce bien est rattaché au même établissement avant et après la cession et lorsque, directement ou indirectement : / a. l'entreprise cessionnaire contrôle l'entreprise cédante ou est contrôlée par elle ; / b. ou ces deux entreprises sont contrôlées par la même entreprise ; (...) ".<br/>
<br/>
              2. Il résulte des termes mêmes des dispositions précitées du 3° quater de l'article 1469 du code général des impôts qu'ainsi que l'a relevé l'arrêt attaqué, les cessions de biens qu'elles visent s'entendent des seuls transferts de propriété consentis entre un cédant et un cessionnaire. Ces dispositions, dont les termes renvoient à une opération définie et régie par le droit civil, ne sauraient s'entendre comme incluant toutes autres opérations qui, sans constituer des " cessions " proprement dites, ont pour conséquence une mutation patrimoniale.<br/>
<br/>
              3. Cependant, la notion de cession au sens du droit civil recouvre tous les transferts de propriété consentis entre un cédant et un cessionnaire, effectués à titre gratuit ou à titre onéreux. Par suite, en jugeant que l'opération par laquelle une société apporte une partie de ses éléments d'actif à une autre société et reçoit en contrepartie des droits sociaux de la société bénéficiaire de l'apport ne peut, eu égard à la nature de cette contrepartie, laquelle associe l'apporteur aux aléas de la société bénéficiaire et ne constitue dès lors pas un prix, être regardée comme une cession au sens du droit civil, pour en déduire que les apports partiels d'actifs n'entrent pas dans les prévisions du 3° quater de l'article 1469 du code général des impôts, la cour administrative d'appel de Versailles a entaché son arrêt d'une erreur de droit. Le ministre de l'économie et des finances est, par suite, fondé, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, à demander l'annulation de l'arrêt attaqué.<br/>
<br/>
              4. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat qui n'est pas, dans la présente instance la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt du 28 décembre 2012 de la cour administrative d'appel de Versailles est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Versailles.<br/>
Article 3 : Les conclusions présentées par la SAS Monoprix exploitation au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au ministre de l'économie et des finances et à la SAS Monoprix exploitation.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
