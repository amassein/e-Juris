<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043310076</ID>
<ANCIEN_ID>JG_L_2021_03_000000443948</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/31/00/CETATEXT000043310076.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 30/03/2021, 443948, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-03-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>443948</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP JEAN-PHILIPPE CASTON</AVOCATS>
<RAPPORTEUR>M. François Weil</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:443948.20210330</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au juge des référés du tribunal administratif C..., statuant sur le fondement des dispositions de l'article L. 521-2 du code de justice administrative, d'enjoindre au département de la Gironde, et à titre subsidiaire au préfet de la Gironde, d'assurer son hébergement et de prendre en charge ses besoins alimentaires, sanitaires et médicaux, jusqu'à ce que l'autorité judiciaire ait définitivement statué sur son recours fondé sur les articles 375 et suivants du code civil.<br/>
<br/>
              Par une ordonnance n° 2003807 du 28 août 2020, le juge des référés du tribunal administratif C... a rejeté sa demande.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 10 septembre et 1er octobre 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande de première instance ; <br/>
<br/>
              3°) de mettre à la charge du département de la Gironde ou de l'Etat une somme de 2 000 euros à verser à la SCP Caston, son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - la convention internationale relative aux droits de l'enfant ;<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code civil ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Weil, conseiller d'Etat, <br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteure publique,<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Jean-Philippe Caston, avocat de M. A... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1.	Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2.	Il ressort des pièces du dossier soumis au juge des référés que M. A..., qui indique être de nationalité malienne et né le 19 mai 2004, a fait l'objet, le 18 août 2020, d'une décision de refus de prise en charge au titre de l'aide sociale à l'enfance par le département de la Gironde, au motif que sa minorité n'était pas caractérisée. M. A..., qui a saisi le juge pour enfants C... le 27 août 2020, a demandé au juge des référés du tribunal administratif C..., sur le fondement des dispositions de l'article L. 521-2 du code de justice administrative, d'enjoindre au département de la Gironde, et à titre subsidiaire au préfet de la Gironde, de prendre en charge son hébergement jusqu'à ce que l'autorité judiciaire ait définitivement statué sur son recours fondé sur les articles 375 et suivants du code civil. Il se pourvoit en cassation contre l'ordonnance du 28 août 2020 par laquelle le juge des référés a rejeté sa demande.<br/>
<br/>
              3.	Il ressort du dossier de procédure que, contrairement à ce que mentionne l'ordonnance attaquée, la requête de M. A... n'a pas été communiquée au département de la Gironde et que, pour rejeter celle-ci, le juge des référés du tribunal administratif C... a entendu faire application des dispositions de l'article L. 522-3 du code de justice administrative. Toutefois, il n'a ni visé ni cité ces dispositions ni mentionné d'élément permettant de vérifier que les conditions prévues par cet article étaient remplies. Par suite et sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, M. A... est fondé à soutenir que l'ordonnance attaquée est entachée d'irrégularité et à en demander l'annulation.<br/>
<br/>
              4.	M. A... a obtenu le bénéfice de l'aide juridictionnelle. Par suite son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du département de la Gironde le versement d'une somme de 1 000 euros à la SCP Caston, avocat de M. A..., sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif C... du 28 août 2020 est annulée.<br/>
Article 2 : L'affaire est renvoyée au juge des référés du tribunal administratif C....<br/>
<br/>
Article 3 : Le département de la Gironde versera à la SCP Caston, avocat de M. A..., la somme de 1 000 euros au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : La présente décision sera notifiée à M. B... A..., au département de la Gironde et au ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
