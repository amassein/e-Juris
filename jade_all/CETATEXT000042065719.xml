<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042065719</ID>
<ANCIEN_ID>JG_L_2020_06_000000420612</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/06/57/CETATEXT000042065719.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 29/06/2020, 420612, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-06-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420612</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET BRIARD ; SCP GADIOU, CHEVALLIER</AVOCATS>
<RAPPORTEUR>Mme Céline Roux</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHS:2020:420612.20200629</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le président de l'université d'Aix-Marseille a saisi la section disciplinaire du conseil d'administration de cette université de poursuites disciplinaires visant M. B... A..., maître de conférences. Par une décision du 15 décembre 2015, la section disciplinaire a infligé à M. A... la sanction de l'interdiction d'exercer les fonctions d'enseignement dans l'établissement pour une durée de trois mois, assortie de la privation de la moitié du traitement. <br/>
<br/>
              Par une décision du 6 février 2018, le Conseil national de l'enseignement supérieur et de la recherche (CNESER), statuant en formation disciplinaire, a, sur appel de M. A..., annulé la décision de la section disciplinaire et prononcé sa relaxe.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés au secrétariat du contentieux du Conseil d'Etat les 14 mai,13 août 2018 et 27 avril 2020 l'université d'Aix-Marseille demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) de mettre à la charge de M. A... la somme de 3 000 euros, au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu :<br/>
              - le code de l'éducation ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 modifiée ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Céline Roux, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, au Cabinet Briard, avocat de l'université Aix-Marseille et à la SCP Gadiou, Chevallier, avocat de M. A... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 15 décembre 2015, la section disciplinaire du conseil d'administration de l'université d'Aix-Marseille a infligé à M. A..., maître de conférences, la sanction de l'interdiction d'exercer les fonctions d'enseignement dans l'établissement pour une durée de trois mois, assortie de la privation de la moitié de son traitement. Par une décision du 6 février 2018, contre laquelle l'université d'Aix-Marseille se pourvoit en cassation, le Conseil national de l'enseignement supérieur et de la recherche (CNESER) statuant en formation disciplinaire a annulé la décision de la section disciplinaire et prononcé la relaxe de M. A....<br/>
<br/>
              2. Aux termes de l'article R. 712-30 du code de l'éducation : " La section disciplinaire est saisie par une lettre adressée à son président. Ce document mentionne le nom, l'adresse et la qualité des personnes faisant l'objet des poursuites ainsi que les faits qui leur sont reprochés. Il est accompagné de toutes pièces justificatives. ". Aux termes de l'article R. 712-41 du même code : " La décision (...) est notifiée par le président de la section disciplinaire à la personne contre laquelle les poursuites ont été intentées, au président de l'université et au recteur de région académique. / (...) La notification doit mentionner les voies de recours et les délais selon lesquels la décision peut être contestée. La notification à l'intéressé a lieu par lettre recommandée avec demande d'avis de réception. (...) ". En cas de retour au greffe de la juridiction du pli contenant la notification du jugement, la preuve que le requérant a reçu notification régulière de ce jugement peut résulter soit des mentions précises, claires et concordantes portées sur l'enveloppe, soit, à défaut, d'une attestation de l'administration postale ou d'autres éléments de preuve établissant la délivrance par le préposé du service postal, conformément à la réglementation postale en vigueur, d'un avis d'instance prévenant le destinataire de ce que le pli était à sa disposition au bureau de poste.<br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond que si M. A... avait à plusieurs reprises au cours de l'année 2015, préalablement à l'engagement des poursuites disciplinaires, adressé à l'université d'Aix-Marseille des courriers mentionnant une autre adresse que celle figurant dans son dossier administratif, il n'a pas indiqué de manière explicite à l'université que son adresse aurait changé. En outre, les plis qui ont été adressés à M. A... par lettre recommandée avec avis de réception, postérieurement à l'engagement des poursuites disciplinaires, au 9 rue Berard à Marseille (5ème arrondissement), ont tous été renvoyés à l'université revêtus de la mention "avisé, pli non réclamé", et non de la mention "inconnu à l'adresse indiquée ". En jugeant dès lors que la décision de la section disciplinaire du conseil d'administration de l'université d'Aix-Marseille avait été notifiée à une adresse erronée, le CNESER a commis une erreur de droit et dénaturé les pièces du dossier. L'université d'Aix-Marseille est par suite fondée à en demander l'annulation pour ce motif, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi.<br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative. <br/>
<br/>
              5. Aux termes de l'article R. 712-43 du code de l'éducation : " L'appel et l'appel incident peuvent être formés devant le Conseil national de l'enseignement supérieur et de la recherche, statuant en matière disciplinaire contre les décisions des sections disciplinaires des universités, par les personnes à l'encontre desquelles ces décisions ont été rendues (...). / L'appel est formé dans le délai de deux mois à compter de la notification de la décision ".<br/>
<br/>
              6. Il ressort des pièces du dossier que la lettre notifiant à M. A... la décision du 15 décembre 2015 de la section disciplinaire du conseil d'administration de l'université d'Aix -Marseille a été retournée à l'université d'Aix-Marseille, accompagnée d'un avis de réception comportant la mention " présenté/avisé le 21/12/20015 ", et que la case " pli avisé et non réclamé " a été cochée. Dès lors, la notification de la décision du 15 décembre 2015 est réputée avoir été régulièrement accomplie à la date du 21 décembre 2015. Par suite, la requête d'appel de M. A..., formée le 10 mars 2016, après l'expiration du délai mentionné au point 5, est irrecevable et ne peut qu'être rejetée.<br/>
<br/>
              7. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit, à ce titre, mise à la charge de l'université d'Aix-Marseille qui n'est pas, dans la présente instance, la partie perdante. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par l'université d'Aix-Marseille au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
      -------------<br/>
Article 1er : La décision du 6 février 2018 du Conseil national de l'enseignement supérieur et de la recherche est annulée.<br/>
Article 2 : La requête présentée par M. A... devant le Conseil national de l'enseignement supérieur et de la recherche statuant en formation disciplinaire est rejetée. <br/>
Article 3 : Les conclusions de l'université d'Aix-Marseille présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à l'Université d'Aix-Marseille et à M. B... A.... <br/>
Copie en sera adressée à la ministre de l'enseignement supérieur, de la recherche et de l'innovation. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
