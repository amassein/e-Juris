<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043540356</ID>
<ANCIEN_ID>JG_L_2021_05_000000434065</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/54/03/CETATEXT000043540356.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 26/05/2021, 434065, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-05-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>434065</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE CHAISEMARTIN, DOUMIC-SEILLER</AVOCATS>
<RAPPORTEUR>M. Jean-Luc Prévoteau</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:434065.20210526</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Monsieur C... A...-B... a demandé au tribunal administratif de Paris de prononcer la décharge des rappels de taxe sur la valeur ajoutée au titre de la période allant du 1er janvier 2005 au 31 décembre 2006 et des cotisations supplémentaires d'impôt sur les sociétés au titre des exercices clos en 2005 et 2006 mis à la charge de la société Cambronne Autos, qui lui ont été réclamés par une mise en demeure de payer du 8 février 2016 en sa qualité de redevable solidaire de cette société. Par un jugement n° 1620003 du 5 décembre 2017, le tribunal administratif de Paris a prononcé un non-lieu à statuer à concurrence de la somme de 127 878 euros et rejeté le surplus des conclusions de sa demande.<br/>
<br/>
              Par un arrêt n° 18PA00337 du 27 juin 2019, la cour administrative d'appel de Paris a rejeté l'appel formé par M. A...-B... contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 29 août et 29 novembre 2019 et le 16 octobre 2020 au secrétariat du contentieux du Conseil d'Etat, M. A...-B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ; <br/>
              - le code de justice administrative et le décret n°2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Prévoteau, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP de Chaisemartin, Doumic-Seiller, avocat de M. A...-B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un jugement devenu définitif du 17 juin 2011, le tribunal de grande instance de Paris a déclaré M. A...-B..., gérant de la société Cambronne Autos, solidairement responsable du paiement des impôts éludés par cette société. Par un jugement du 5 décembre 2017, le tribunal administratif de Paris a, après avoir prononcé un non-lieu intervenu en cours d'instance, rejeté le surplus des conclusions de la demande de M. A...-B... tendant à la décharge des rappels de taxe sur la valeur ajoutée au titre de la période du 1er janvier 2005 au 31 décembre 2006 et des cotisations supplémentaires d'impôt sur les sociétés au titre des exercices clos en 2005 et 2006 mis à la charge de la société Cambronne Autos et qui lui ont été réclamés, en sa qualité de redevable solidaire, par une mise en demeure de payer du 8 février 2016. M. A...-B... demande l'annulation de l'arrêt du 27 juin 2019 par lequel la cour administrative d'appel de Paris a rejeté l'appel qu'il a formé contre ce jugement.  <br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond et notamment de la proposition de rectification du 30 octobre 2008 notifiée à la société Cambronne Autos à la suite de la vérification de comptabilité de ses exercices clos en 2005 et 2006, que l'administration a procédé à la reconstitution du résultat imposable de la société. A ce titre, elle a admis la déduction de charges à hauteur de 75% du chiffre d'affaires reconstitué, soit, selon elle, le taux moyen de charges constaté pour un échantillon d'entreprises comparables, charges d'impôts et taxes, charges sociales et dotations d'exploitation sur immobilisations et amortissements non comprises dès lors que la société Cambronne Autos n'aurait pas été en mesure d'établir la réalisation effective de telles charges.<br/>
<br/>
              3. En premier lieu, en écartant la demande de M. A...-B... tendant à ce que le montant des charges admises en déduction du chiffre d'affaires reconstitué soit majoré du montant de la taxe professionnelle, de la contribution sociale de solidarité et de la contribution additionnelle acquitté par la société Cambronne Autos au motif que le requérant ne démontrait pas que ces charges d'impôts n'avaient pas déjà été prises en compte lors de la détermination du taux de 75% mentionné au point 2 alors qu'elle avait précédemment relevé que ce taux correspondait à celui constaté pour les entreprises comparables, charges d'impôts exclues, la cour a entaché son arrêt d'une contradiction de motifs.<br/>
<br/>
              4. En second lieu, pour écarter la demande de M. A...-B... tendant à ce que le montant des charges admises en déduction du chiffre d'affaires reconstitué soit majoré du montant des charges sociales acquittées par la société Cambronne Autos, la cour a jugé qu'il résultait de l'instruction que les copies de chèques produites étaient illisibles, que le montant ainsi que le nom du bénéficiaire avaient été réécrits et qu'aucun extrait des relevés bancaires de la société n'avait été produit. En statuant ainsi, alors qu'avaient été produits à l'appui de la requête les relevés bancaires de la société pour les deux exercices vérifiés ainsi que des relevés de décompte de cotisations sociales avec la copie lisible des chèques correspondants, la cour a dénaturé les pièces du dossier qui lui était soumis.<br/>
<br/>
              5. Il résulte de ce qui précède que M. A...-B... est fondé à demander l'annulation de l'arrêt qu'il attaque, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à M. A...-B... au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt du 27 juin 2019 de la cour administrative d'appel de Paris est annulé.<br/>
Article 2 : L'affaire est renvoyée devant la cour administrative d'appel de Paris.<br/>
Article 3 : L'Etat versera à M. A...-B... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. C... A...-B... et au ministre de l'économie, des finances et de la relance.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
