<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043378345</ID>
<ANCIEN_ID>JG_L_2021_03_000000432683</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/37/83/CETATEXT000043378345.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 30/03/2021, 432683, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-03-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>432683</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD ; HAAS ; SCP BAUER-VIOLAS, FESCHOTTE-DESBOIS, SEBAGH</AVOCATS>
<RAPPORTEUR>M. François Weil</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:432683.20210330</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. N... L..., M. N... AP..., M. W... M..., M. C... AT..., M. AV... AY..., M. AE... H..., M. BE...-N... F..., M. U... AQ..., M. D... X..., Mme AH... X..., Mme AF... AM..., M. Q... O..., M. Z... BD..., M. S... AL..., M. P... AL..., M. AI... AG..., Mme AO... AG..., M. T... BG..., M. BE...-BH... B..., M. D... J..., M. V... E..., M. AC... AU..., M. AN... AS..., Mme AW... BB..., M. AK... BF..., M. BA... AZ..., M. G... AD..., M. I... K..., M. AB... Y..., M. AE... AA..., Mme AR... R..., Mme AJ... A... et Mme BC... AX... ont demandé au tribunal administratif de Marseille d'annuler pour excès de pouvoir la décision du 22 janvier 2019 par laquelle le maire de Port-de-Bouc (Bouches-du-Rhône) ne s'est pas opposé à la déclaration préalable déposée par la société Cellnex France tendant à l'implantation d'un pylône porteur d'antennes panneaux Bouygues Télécom sur le territoire de la commune.<br/>
<br/>
              Par une ordonnance n° 1902600 du 14 mai 2019, le président de la 4ème chambre du tribunal administratif de Marseille a rejeté leur demande.<br/>
<br/>
              Par un pourvoi et un mémoire complémentaire, enregistrés les 16 juillet et 17 octobre 2019 au secrétariat du contentieux du Conseil d'Etat, M. L..., M. AP..., M. M..., M. AT..., M. AY..., M. F..., M. O..., M. AG..., Mme AG..., M. B..., M. J..., M. E..., M. AU..., M. BF..., M. AZ..., M. K..., M. Y... et M. AA... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de mettre à la charge de la société Cellnex France la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;		<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. AE... Weil, conseiller d'Etat,<br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteure publique,<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Gaschignard, avocat de M. L... et autres, à la SCP Bauer-Violas, Feschotte-Desbois, Sebagh, avocat de la société Cellnex-France et de la société Bouygues Télécom, et à Me Haas, avocat de la commune de Port de Bouc ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. D'une part, aux termes de l'article R. 222-1 du code de justice administrative dans sa rédaction applicable au litige : " (...) les présidents de formation de jugement des tribunaux et des cours (...) peuvent, par ordonnance : / (...) 4° Rejeter les requêtes manifestement irrecevables, lorsque la juridiction n'est pas tenue d'inviter leur auteur à les régulariser ou qu'elles n'ont pas été régularisées à l'expiration du délai imparti par une demande en ce sens ". Aux termes de l'article R. 612-1 du même code : " Lorsque des conclusions sont entachées d'une irrecevabilité susceptible d'être couverte après l'expiration du délai de recours, la juridiction ne peut les rejeter en relevant d'office cette irrecevabilité qu'après avoir invité leur auteur à les régulariser. / (...) La demande de régularisation mentionne que, à défaut de régularisation, les conclusions pourront être rejetées comme irrecevables dès l'expiration du délai imparti qui, sauf urgence, ne peut être inférieur à quinze jours. La demande de régularisation tient lieu de l'information prévue à l'article R. 611-7 ".<br/>
<br/>
              2. D'autre part, aux termes de l'article R. 600-4 du code de l'urbanisme, dans sa version applicable à l'espèce : " Les requêtes dirigées contre une décision relative à l'occupation ou l'utilisation du sol régie par le présent code doivent, à peine d'irrecevabilité, être accompagnées du titre de propriété, de la promesse de vente, du bail, du contrat préliminaire mentionné à l'article L. 261-15 du code de la construction et de l'habitation, du contrat de bail, ou de tout autre acte de nature à établir le caractère régulier de l'occupation ou de la détention de son bien par le requérant (...) ". Prises dans l'intérêt d'une bonne administration de la justice, ces dispositions imposent d'accompagner les requêtes dirigées contre une décision relative à l'occupation ou à l'utilisation du sol des pièces justificatives nécessaires pour apprécier notamment si les conditions de recevabilité fixées par l'article L. 600-1-2 du même code sont remplies.<br/>
<br/>
              3. Pour rejeter comme irrecevable la requête de M. L... et autres tendant à l'annulation pour excès de pouvoir de la décision du 22 janvier 2019 du maire de Port-de-Bouc de non-opposition à la déclaration préalable déposée par la société Cellnex France, le président de la 4ème chambre du tribunal administratif de Marseille s'est fondé sur ce que, bien qu'invités par une demande de régularisation du greffe du tribunal à produire les pièces exigées par l'article R. 600-4 du code de l'urbanisme, les intéressés n'avaient pas fourni les justificatifs requis. Il ressort toutefois des pièces du dossier soumis au juge du fond qu'étaient joints à la requête introductive d'instance des attestations de vente notariées et des extraits d'acte de vente comportant les mentions de nature à établir la détention régulière de leurs biens par les requérants. Ainsi, l'ordonnance attaquée est entachée de dénaturation. Par suite, M. L... et autres sont fondés à en demander l'annulation.<br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Cellnex France la somme de 1 500 euros à verser à M. L... et autres au titre de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font obstacle à ce que soit mise à la charge de ceux-ci, qui ne sont pas, dans la présente instance, la partie perdante, les sommes demandées par la commune de Port-de-Bouc et par les sociétés Bouygues Télécom et Cellnex France au même titre.<br/>
<br/>
<br/>
<br/>
<br/>
              			D E C I D E :<br/>
              			 --------------<br/>
<br/>
Article 1er : L'ordonnance du président de la 4ème chambre du tribunal administratif de Marseille du 14 mai 2019 est annulée.<br/>
Article 2 : L'affaire est renvoyée devant le tribunal administratif de Marseille. <br/>
Article 3 : La société Cellnex France versera à M. L... et autres la somme totale de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : Les conclusions présentées par la commune de Port-de-Bouc et par les sociétés Bouygues Télécom et Cellnex France au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à M. N... L..., premier requérant dénommé, à la société Cellnex France, à la société Bouygues Télécom et à la commune de Port-de-Bouc.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
