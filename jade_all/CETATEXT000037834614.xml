<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037834614</ID>
<ANCIEN_ID>JG_L_2018_12_000000418945</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/83/46/CETATEXT000037834614.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 19/12/2018, 418945, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-12-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418945</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>M. Olivier Gariazzo</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:418945.20181219</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société par actions simplifiée (SAS) Brico Dépôt a demandé au tribunal administratif de Châlons-en-Champagne de prononcer la décharge des rappels de taxe sur les surfaces commerciales auxquels elle a été assujettie au titre des années 2010 et 2011 à raison de son établissement situé à Prix les Mézières (Ardennes). Par un jugement n° 1701488 du 11 janvier 2018, ce tribunal a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 12 mars, 12 juin et 17 septembre 2018 au secrétariat du contentieux du Conseil d'État, la société Brico Dépôt demande au Conseil d'Etat :<br/>
<br/>
              1°) de renvoyer à la cour administrative d'appel compétente ses conclusions dirigées contre le jugement, en tant qu'il concerne les impositions établies au titre de l'année 2010 ; <br/>
<br/>
              2°) d'annuler ce jugement en tant qu'il rejette ses conclusions relatives aux impositions établies au titre de l'année 2011 ;<br/>
<br/>
              3°) réglant l'affaire au fond dans cette mesure, de faire droit à sa demande ;<br/>
<br/>
              4°) de mettre à la charge de l'État la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ; <br/>
              - la loi n° 72-657 du 13 juillet 1972 ;<br/>
              - le décret n° 95-85 du 26 janvier 1995 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Gariazzo, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de la société Brico Dépôt.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              En ce qui concerne la taxe sur les surfaces commerciales due au titre de l'année 2010 :<br/>
<br/>
              1. Il résulte de l'article R. 811-1 du code de justice administrative que les litiges concernant la taxe sur les surfaces commerciales due au titre des années d'imposition antérieures à 2011 sont susceptibles d'un appel devant la cour administrative d'appel. La requête de la société Brico Dépôt tend à l'annulation du jugement du 11 janvier 2018 par lequel le tribunal administratif de Châlons-en-Champagne a rejeté sa requête tendant à la décharge des rappels de taxe sur les surfaces commerciales auxquels elle a été assujettie au titre des années 2010 et 2011. Par suite, cette requête a, en ce qui concerne l'année 2010, le caractère d'un appel qui ne ressortit pas à la compétence du Conseil d'État, juge de cassation mais à celle de la cour administrative d'appel de Nancy. Il y a lieu, dès lors, d'en attribuer le jugement à cette cour. <br/>
<br/>
              En ce qui concerne la taxe sur les surfaces commerciales due au titre de l'année 2011 :<br/>
<br/>
              2. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'État fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ". <br/>
<br/>
              3. Pour demander l'annulation du jugement qu'elle attaque, la société Brico Dépôt soutient que le tribunal administratif de Châlons-en-Champagne a : <br/>
              - méconnu les dispositions de l'article 3 de la loi du 13 juillet 1972 et du A de l'article 3 du décret du 26 janvier 1995 en jugeant qu'elle ne pouvait prétendre au bénéfice de la réduction de taux de 30 % instituée par ces dispositions au profit des professions dont l'exercice requiert des superficies de vente anormalement élevées ; <br/>
              - méconnu les principes d'égalité devant l'impôt et d'égalité devant les charges publiques, garantis par les articles 6 et 13 de la Déclaration des droits de l'homme et du citoyen de 1789, en n'écartant pas, dans le cas d'un contribuable vendant à titre principal des meubles meublants et des matériaux de construction, l'application de l'article 3 du décret du 26 janvier 1995 en tant que celui-ci subordonne à une condition d'exclusivité le bénéfice de la réduction de taux de 30 % qu'il institue ;<br/>
              - méconnu l'article 3 du décret du 26 janvier 1995 et le principe d'égalité en jugeant qu'il n'était pas établi que la part des ventes de meubles meublants dans son activité était suffisamment significative pour que celle-ci requière une surface de vente anormalement élevée ; <br/>
              - méconnu l'article L. 47 du livre des procédures fiscales en jugeant que la vérification de comptabilité était régulière en ce qui concerne l'année 2011, au seul motif qu'à la date d'engagement des opérations de contrôle, le délai de déclaration de la taxe sur les surfaces commerciales relative à cette année était expiré et que la taxe était exigible.<br/>
<br/>
              4. Aucun de ces moyens n'est de nature à permettre l'admission du pourvoi. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                                         --------------<br/>
<br/>
Article 1er : Le jugement des conclusions de la requête de la société Brico Dépôt relatives à la taxe sur les surfaces commerciales due au titre de l'année 2010 est attribué à la cour administrative d'appel de Nancy. <br/>
<br/>
Article 2 : Le pourvoi de la société Brico Dépôt n'est pas admis.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la société par actions simplifiée Brico Dépôt et au ministre de l'action et des comptes publics.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
