<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033364648</ID>
<ANCIEN_ID>JG_L_2016_11_000000397573</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/36/46/CETATEXT000033364648.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 09/11/2016, 397573, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-11-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>397573</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Yves Doutriaux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:397573.20161109</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...D...a demandé au juge des référés du tribunal administratif de Bordeaux, sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de l'arrêté du 30 novembre 2015 par lequel le maire de La Teste-de-Buch a délivré à M. C...B...un permis de construire en vue de l'édification d'un bâtiment à usage d'habitation sur un terrain située 9 bis, rue de l'Anjou sur le territoire de la commune. Par une ordonnance n° 1600132 du 16 février 2016, le juge des référés a suspendu l'exécution de cet arrêté.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 2 mars, 17 mars et 7 octobre 2016 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de suspension présentée par Mme D... ;<br/>
<br/>
              3°) de mettre à la charge de Mme D...la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'urbanisme ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yves Doutriaux, conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de M. B...et à la SCP Lyon-Caen, Thiriez, avocat de MmeD... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'il ressort des pièces du dossier soumis au juge des référés que, par un arrêté du 30 novembre 2015, le maire de La Teste-de-Buch a délivré à M. B...un permis de construire pour la réalisation d'une maison individuelle d'habitation sur une parcelle située rue de l'Anjou ; que, par une ordonnance du 16 février 2016, le juge des référés du tribunal administratif de Bordeaux a prononcé la suspension de l'exécution de cet arrêté au motif que le moyen tiré de la méconnaissance des dispositions de l'article 3-5 de la section III du chapitre 3 du règlement du plan local d'urbanisme de la commune était de nature à faire naître un doute sérieux quant à la légalité de l'arrêté ;<br/>
<br/>
              2.	Considérant qu'aux termes de l'article 3 de la section III du chapitre 3 du règlement du plan local d'urbanisme de la commune de La Teste-de-Buch : " 1. Accès / Pour être constructible, une unité foncière doit avoir un accès à une voie publique ou privée telle que décrite ci-après, soit directement, soit par l'intermédiaire d'un passage défini ci-dessous aménagé sur fond voisin ou éventuellement par application de l'article 682 du code civil (...) / 2. Voirie / Toute opération de construction nouvelle située en 2ème rideau par rapport aux voies existantes pourra être desservie par une bande d'accès de 3,50 mètres, si cet accès dessert moins de 2 lots ou logements existants ou a créer situés en 2ème rideau (...) / 4. Dans tous les cas les voies doivent avoir des caractéristiques adaptées à l'approche d'enlèvement des ordures ménagères (...) 5. Les voies en lacune dites impasses, devront comporter dans la partie terminale de leur chaussée, une palette de retournement telle que définie en annexe au présent règlement, sauf dans le cas d'opérations successives ou d'une opération par tranche dans la mesure ou la voie doit se poursuivre ultérieurement. Toutefois il pourra, en fonction de la longueur de la voie et de son trafic, être exigé une aire de retournement provisoire " ;<br/>
<br/>
              3.	Considérant que les dispositions des 4 et 5 de l'article précité sont relatives à l'aménagement des voies nouvelles et n'ont pas pour objet, à la différence de celles qui figurent au 1 et 2, de définir les conditions de constructibilité des terrains situés dans les zones concernées ; qu'ainsi, elles ne font pas obstacle à la délivrance d'un permis de construire en vue de l'édification d'une maison desservie par des voies construites avant leur adoption ; que, par suite, en jugeant qu'était, en l'état de l'instruction, propre à créer un doute sérieux quant à la légalité de la décision contestée, le moyen tiré de ce que le permis de construire accordé à M. B... ne respectait pas les prescriptions de ces dispositions, alors que le projet envisagé ne comportait pas l'aménagement d'une voie nouvelle, le juge des référés a commis une erreur de droit ; que, par suite, M. B...est fondé à demander l'annulation de l'ordonnance attaquée ;<br/>
<br/>
              4.	Considérant qu'il y a lieu, dans les circonstances de l'espèce, de statuer sur la demande en référé en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              5.	Considérant que, compte tenu de ce qui figure au point 3, le seul moyen invoqué par Mme D...à l'appui de sa demande de suspension et tiré de ce que le permis de construire accordé à M. B...ne respecterait pas les dispositions des 4 et 5 de l'article 3 de la section III du chapitre 3 du règlement du plan local d'urbanisme de la commune de La Teste-de-Buch ne paraît pas, en l'état de l'instruction, propre à créer un doute sérieux sur la légalité de la décision contestée ; <br/>
<br/>
              6.	Considérant que l'une des conditions posées par l'article L. 521-1 du code de justice administrative n'étant pas remplie, la demande présentée par Mme D...devant le juge des référés du tribunal administratif de Bordeaux tendant à ce que soit ordonnée la suspension de l'arrêté du 30 novembre 2015 par lequel le maire de La Teste-de-Buch a délivré à M. C... B...un permis de construire doit être rejetée ;<br/>
<br/>
              7.	Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme D...la somme de 1 000 euros à verser à M. B...au titre de l'article L. 761-1 du code de justice administrative ; qu'en revanche, ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de M.B..., qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'ordonnance du 16 février 2016 du juge des référés du tribunal administratif de Bordeaux est annulée.<br/>
<br/>
Article 2 : La demande présentée par Mme D...devant le juge des référés du tribunal administratif de Bordeaux est rejetée.<br/>
<br/>
Article 3 : Les conclusions de Mme D...présentées au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 sont rejetées. <br/>
<br/>
Article 4 : Mme D...versera à M. B...une somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 5 : La présente décision sera notifiée à M. C...B..., à Mme A...D...et à la commune de La Teste-de-Buch.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
