<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037022300</ID>
<ANCIEN_ID>JG_L_2018_06_000000411053</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/02/23/CETATEXT000037022300.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 06/06/2018, 411053</TITRE>
<DATE_DEC>2018-06-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411053</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE NERVO, POUPET ; SCP MARLANGE, DE LA BURGADE</AVOCATS>
<RAPPORTEUR>M. Laurent Domingo</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:411053.20180606</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Orange a demandé au tribunal administratif de Rennes, d'une part, de constater la nullité de la décision du 28 novembre 2013 par laquelle le président de la communauté d'agglomération Lorient Agglomération a dénoncé la convention d'occupation du château d'eau de Lanveur conclue le 15 juin 2002 et d'ordonner la reprise des relations contractuelles avec la communauté d'agglomération Lorient Agglomération et, d'autre part, de constater la nullité de la mise en demeure du 23 juin 2014 du président de la communauté d'agglomération Lorient Agglomération de procéder au démontage, avant le 15 juillet 2014, des équipements techniques de radiocommunication installés sur le château d'eau de Lanveur. Par un jugement nos 1400647, 1403785 du 6 novembre 2015, le tribunal administratif a rejeté ces demandes.<br/>
<br/>
              Par un arrêt nos 16NT00045, 16NT00058 du 3 avril 2017, la cour administrative d'appel de Nantes a rejeté les appels formés par la société Orange contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 31 mai 2017, 7 août 2017 et 1er mars 2018 au secrétariat du contentieux du Conseil d'Etat, la société Orange demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses appels ;<br/>
<br/>
              3°) de mettre à la charge de la communauté d'agglomération Lorient Agglomération la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Domingo, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Marlange, de la Burgade, avocat de la société Orange et à la SCP de Nervo, Poupet, avocat de la communauté d'agglomération Lorient Agglomération.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la commune de Languidic et la société Orange ont signé, le 15 juin 2002, une convention autorisant, pour une durée de douze ans, l'installation, sur le château d'eau de Lanveur et sur une partie du terrain d'assiette de cet ouvrage, d'équipements techniques de radiotéléphonie mobile. En vertu de son article 13, cette convention était reconductible de plein droit par périodes successives de deux ans, sauf dénonciation par l'une des parties, par lettre recommandée avec accusé de réception, six mois avant la date d'expiration de la période en cours. Par un courrier du 28 novembre 2013, le président de la communauté d'agglomération Lorient Agglomération, substituée à la commune de Languidic à compter du 1er janvier 2012, a informé la société Orange que, faisant usage de la faculté qui lui était ouverte par son article 13, il s'opposait à la reconduction de la convention à son terme initial, soit à compter du 15 juin 2014. Par un courrier du 23 juin 2014, il a rappelé à la société Orange la teneur de sa correspondance du 28 novembre 2013, lui a demandé de lui indiquer si elle souhaitait conclure une autre convention en vue de l'occupation du château d'eau de Lanveur et lui a précisé que, si tel n'était pas le cas, elle devait procéder, conformément à l'article 8 de la convention, au retrait de ses équipements, au plus tard le 15 juillet 2014. La société Orange a saisi le tribunal administratif de Rennes de demandes par lesquelles, d'une part, elle contestait la validité de la décision du 28 novembre 2013 et demandait que soit ordonnée la reprise des relations contractuelles avec la communauté d'agglomération Lorient Agglomération et, d'autre part, elle contestait la validité de la mise en demeure du 23 juin 2014 de procéder au démontage des équipements de radiotéléphonie. Par un jugement du 6 novembre 2015, le tribunal administratif de Rennes a rejeté ces demandes. La société Orange se pourvoit en cassation contre l'arrêt du 3 avril 2017 par lequel la cour administrative d'appel de Nantes a rejeté ses appels formés contre ce jugement.<br/>
<br/>
              Sur l'arrêt attaqué en tant qu'il statue sur les conclusions relatives à la décision du 28 novembre 2013 :<br/>
<br/>
              2. Le juge du contrat, saisi par une partie d'un litige relatif à une mesure d'exécution d'un contrat, peut seulement, en principe, rechercher si cette mesure est intervenue dans des conditions de nature à ouvrir droit à indemnité. Toutefois, une partie à un contrat administratif peut, eu égard à la portée d'une telle mesure d'exécution, former devant le juge du contrat un recours de plein contentieux contestant la validité de la résiliation de ce contrat et tendant à la reprise des relations contractuelles. Il incombe au juge du contrat, saisi par une partie d'un recours de plein contentieux contestant la validité d'une mesure de résiliation et tendant à la reprise des relations contractuelles, lorsqu'il constate que cette mesure est entachée de vices relatifs à sa régularité ou à son bien-fondé, de déterminer s'il y a lieu de faire droit, dans la mesure où elle n'est pas sans objet, à la demande de reprise des relations contractuelles, à compter d'une date qu'il fixe, ou de rejeter le recours, en jugeant que les vices constatés sont seulement susceptibles d'ouvrir, au profit du requérant, un droit à indemnité.<br/>
<br/>
              3. La cour a relevé que la décision du 28 novembre 2013 ne constituait pas une mesure de résiliation de la convention d'occupation du domaine public, mais une décision de ne pas la reconduire lorsqu'elle serait parvenue à son terme initial, prise en vertu des stipulations de son article 13. Eu égard à la portée d'une telle décision, qui n'a ni pour objet, ni pour effet de mettre unilatéralement un terme à une convention en cours, le juge du contrat peut seulement rechercher si elle est intervenue dans des conditions de nature à ouvrir droit à une indemnité. Dès lors, en jugeant que la société Orange ne pouvait pas saisir le juge d'un recours en reprise des relations contractuelles et que les conclusions relatives à la décision du 28 novembre 2013 qu'elle avait présentées devant le tribunal administratif de Rennes étaient par suite irrecevables, la cour n'a pas commis d'erreur de droit. Elle n'a pas davantage méconnu le droit à un recours juridictionnel.<br/>
<br/>
              Sur l'arrêt attaqué en tant qu'il statue les conclusions dirigées contre le courrier du 23 juin 2014 :<br/>
<br/>
              4. Pour rejeter les conclusions présentées par la société Orange contre le courrier du 23 juin 2014 du président de la communauté d'agglomération Lorient Agglomération, la cour a relevé que cette correspondance se bornait, d'une part, à rappeler les termes de la décision du 28 novembre 2013 et, d'autre part, à indiquer à la société Orange qu'elle pouvait soit envisager de conclure une autre convention selon de nouvelles modalités, soit procéder, en exécution de l'article 8 de la convention du 15 juin 2002, à la dépose des équipements techniques avant le 15 juillet 2014. Elle en a déduit que cette correspondance ne mettait pas à la charge de la société Orange d'autres obligations que celles qui résultaient directement de l'application des stipulations de la convention et qui lui avaient été précédemment rappelées, de sorte qu'elle n'était pas recevable à en contester la validité. En se bornant à soutenir, à l'appui de son moyen tiré de ce que l'arrêt attaqué serait entaché sur ce point d'une erreur de droit et d'une erreur de qualification juridique des faits, que l'intérêt qu'elle a au maintien des relations contractuelles lui donne qualité pour demander l'annulation de cette correspondance, la société Orange ne critique pas utilement la solution retenue par la cour, dès lors que celle-ci est seulement fondée sur la portée de la décision critiquée. Ces moyens ne peuvent, par suite, qu'être écartés, de même que le moyen tiré de ce que la cour aurait méconnu son droit au recours juridictionnel.<br/>
<br/>
              5. Par ailleurs, la cour n'a pas entaché son arrêt d'insuffisance de motivation en ne se prononçant pas sur le moyen soulevé par la société Orange relatif à la compétence de l'auteur du courrier du 23 juin 2014, dès lors qu'elle avait écarté les conclusions de la société dirigées contre ce courrier comme irrecevables.<br/>
<br/>
              6. Il résulte de tout ce qui précède que la société Orange n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              7. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de la communauté d'agglomération Lorient Agglomération, qui n'est pas la partie perdante dans la présente instance, la somme que la société Orange demande au titre des frais exposés par elle et non compris dans les dépens. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Orange une somme de 3 000 euros à verser à la communauté d'agglomération Lorient Agglomération au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>                       D E C I D E :<br/>
                                    --------------<br/>
<br/>
Article 1er : Le pourvoi de la société Orange est rejeté.<br/>
<br/>
Article 2 : La société Orange versera à la communauté d'agglomération Lorient Agglomération une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la société Orange et à la communauté d'agglomération Lorient Agglomération.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-08-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. RECEVABILITÉ. - SOCIÉTÉ CONTESTANT LA DÉCISION D'UNE COMMUNE, PRISE DANS LE RESPECT DU DÉLAI DE PRÉAVIS, DE NE PAS RECONDUIRE UNE CONVENTION PARVENUE À SON TERME INITIAL - RECEVABILITÉ DU RECOURS EN REPRISE DES RELATIONS CONTRACTUELLES [RJ1] - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-08-03-02 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. POUVOIRS ET OBLIGATIONS DU JUGE. POUVOIRS DU JUGE DU CONTRAT. - SOCIÉTÉ CONTESTANT LA DÉCISION D'UNE COMMUNE, PRISE DANS LE RESPECT DU DÉLAI DE PRÉAVIS, DE NE PAS RECONDUIRE UNE CONVENTION PARVENUE À SON TERME INITIAL - POUVOIRS DU JUGE DU CONTRAT - OBLIGATION DE RECHERCHER SI CETTE DÉCISION EST INTERVENUE DANS DES CONDITIONS DE NATURE À OUVRIR DROIT À UNE INDEMNITÉ - EXISTENCE - FACULTÉ D'ORDONNER LA REPRISE DES RELATIONS CONTRACTUELLES [RJ1] - ABSENCE - CONSÉQUENCE - IRRECEVABILITÉ DES CONCLUSIONS TENDANT À LA REPRISE DES RELATIONS CONTRACTUELLES.
</SCT>
<ANA ID="9A"> 39-08-01 Société contestant la validité de la décision par laquelle une commune avec laquelle elle avait conclu une convention d'occupation du domaine public reconductible tacitement autorisant l'installation sur son territoire d'équipements techniques de radiophonie mobile, a fait usage de la faculté que lui offrait cette convention de s'opposer, six mois avant le terme prévu, à la reconduction de la convention, et demandant également que soit ordonnée la reprise des relations contractuelles.... ,,La décision de la commune ne constituait pas une mesure de résiliation de la convention d'occupation du domaine public, mais une décision de ne pas la reconduire lorsqu'elle serait parvenue à son terme initial. Eu égard à la portée d'une telle décision, qui n'a ni pour objet, ni pour effet de mettre unilatéralement un terme à une convention en cours, le juge du contrat peut seulement rechercher si elle est intervenue dans des conditions de nature à ouvrir droit à une indemnité. Dès lors, la société ne pouvait pas saisir le juge d'un recours en reprise des relations contractuelles et les conclusions qu'elle avait formulées en ce sens à l'encontre de la décision prise par la commune en première instance étaient par suite irrecevables.</ANA>
<ANA ID="9B"> 39-08-03-02 Société contestant la validité de la décision par laquelle une commune avec laquelle elle avait conclu une convention d'occupation du domaine public reconductible tacitement autorisant l'installation sur son territoire d'équipements techniques de radiophonie mobile, a fait usage de la faculté que lui offrait cette convention de s'opposer, six mois avant le terme prévu, à la reconduction de la convention, et demandant également que soit ordonnée la reprise des relations contractuelles.... ,,La décision de la commune ne constituait pas une mesure de résiliation de la convention d'occupation du domaine public, mais une décision de ne pas la reconduire lorsqu'elle serait parvenue à son terme initial. Eu égard à la portée d'une telle décision, qui n'a ni pour objet, ni pour effet de mettre unilatéralement un terme à une convention en cours, le juge du contrat peut seulement rechercher si elle est intervenue dans des conditions de nature à ouvrir droit à une indemnité. Dès lors, la société ne pouvait pas saisir le juge d'un recours en reprise des relations contractuelles et les conclusions qu'elle avait formulées en ce sens à l'encontre de la décision prise par la commune en première instance étaient par suite irrecevables.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 21 mars 2011, Commune de Béziers, n° 304806, p. 117.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
