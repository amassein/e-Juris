<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033243464</ID>
<ANCIEN_ID>JG_L_2016_10_000000381574</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/24/34/CETATEXT000033243464.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 13/10/2016, 381574</TITRE>
<DATE_DEC>2016-10-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>381574</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP HEMERY, THOMAS-RAQUIN ; SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>Mme Emmanuelle Petitdemange</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:381574.20161013</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. et Mme B...A...et Mme F...E...ont demandé au tribunal administratif de Nice d'annuler la délibération du 14 avril 2008 du conseil municipal de la commune de La Colle-sur-Loup en tant qu'elle autorise le maire à solliciter du préfet du département le transfert dans le domaine public du chemin privé des Bas Campons et de l'impasse du Roure, l'arrêté du 18 septembre 2008 du préfet des Alpes-Maritimes en tant qu'il porte transfert d'office du chemin des Bas Campons, ainsi que la délibération du 17 octobre 2008 clôturant cette procédure de transfert. Par un jugement n° 0804017 du 10 avril 2012, le tribunal administratif de Nice a annulé l'arrêté du 18 septembre 2008 du préfet des Alpes-Maritimes et rejeté le surplus de leurs conclusions. <br/>
<br/>
              Par un arrêt n° 12MA02254 du 22 avril 2014, la cour administrative d'appel de Marseille a, sur appel de la commune de La Colle-sur-Loup, annulé l'article 1er du jugement en ce qu'il  annule l'arrêté du préfet des Alpes-Maritimes en ce qu'il porte transfert dans le domaine public communal de la partie demeurée privée de l'impasse du Roure et rejeté le surplus de ses conclusions. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 20 juin et 22 septembre 2014 au secrétariat du contentieux du Conseil d'Etat, la commune de La Colle-sur-Loup demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il rejette le surplus de ses conclusions ; <br/>
<br/>
              2°) de mettre à la charge de M. et Mme A...et de Mme E...la somme de 3 600 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;	<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Emmanuelle Petitdemange, auditeur,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Hémery, Thomas-Raquin, avocat de la commune de La Colle-sur-Loup et à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de M.A..., de Mme C...et de Mme D....<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une délibération du 31 mai 2007, la commune de La Colle-sur-Loup a décidé d'engager la procédure de transfert d'office dans le domaine public communal, telle que prévue par l'article L. 318-3 du code de l'urbanisme, de sept voies privées, dont le chemin des Bas Campons, qu'elle regardait comme ouvertes à la circulation publique ; qu'au terme de l'enquête publique, au cours de laquelle M. et Mme A...et MmeE..., propriétaires d'une portion de ce chemin, ont manifesté leur opposition au transfert de cette voie, la commune a, par une délibération du 14 avril 2008, autorisé le maire à demander au préfet de procéder à ce transfert ; que M. et Mme A... et Mme E...ont alors saisi le tribunal administratif de Nice d'une demande tendant à l'annulation, dans cette mesure, de cette délibération ; que, le 18 septembre 2008, le préfet des Alpes-Maritimes a pris un arrêté portant transfert d'office du chemin des Bas Campons ; que les requérants ont complété leurs conclusions initiales d'une demande tendant à l'annulation de l'arrêté préfectoral du 18 septembre 2008 en tant qu'il concerne le chemin des Bas Campons et de la délibération du 17 octobre 2008 de la commune de La Colle-sur-Loup qui a clos la procédure de transfert ; que, par un jugement du 10 avril 2012, le tribunal administratif de Nice a annulé l'arrêté du 18 septembre 2008 et rejeté le surplus de leurs conclusions; que la commune de La Colle-sur-Loup se pourvoit en cassation contre l'arrêt de la cour administrative d'appel de Marseille du 22 avril 2014, en tant qu'il rejette ses conclusions tendant à l'annulation de ce jugement en tant qu'il statue  sur le chemin des Bas Campons ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 318-3 du code de l'urbanisme, dans sa rédaction applicable aux faits de l'espèce : " La propriété des voies privées ouvertes à la circulation publique dans des ensembles d'habitations peut, après enquête publique, être transférée d'office sans indemnité dans le domaine public de la commune sur le territoire de laquelle ces voies sont situées. / La décision de l'autorité administrative portant transfert vaut classement dans le domaine public et éteint, par elle-même et à sa date, tous droits réels et personnels existant sur les biens transférés. / Cette décision est prise par délibération du conseil municipal. Si un propriétaire intéressé a fait connaître son opposition, cette décision est prise par arrêté du représentant de l'Etat dans le département, à la demande de la commune. (...) " ;<br/>
<br/>
              Sur le bien-fondé de l'arrêt en ce qui concerne la recevabilité de la demande : <br/>
<br/>
              3. Considérant que le délai de recours contentieux contre une décision de transfert prise sur le fondement des dispositions précitées ne peut courir, pour les propriétaires intéressés, qu'à compter de la date à laquelle celle-ci leur a été notifiée, peu important que cette décision ait été par ailleurs publiée ou affichée ; qu'ainsi, c'est sans erreur de droit que la cour administrative d'appel a jugé que, bien que l'arrêté du préfet des Alpes-Maritimes du 18 septembre 2008 ait été publié au recueil des actes administratifs de la préfecture des Alpes-Maritimes du 19 septembre 2008 et affiché en mairie pendant trois mois, du 18 septembre au 17 décembre 2008 inclus, le délai de recours contentieux ouvert contre cet arrêté ne pouvait, en l'absence de notification, être opposé à M. et Mme A...et à Mme E...; <br/>
<br/>
              4. Considérant qu'aux termes de l'article R. 421-5 du code de justice administrative : " Les délais de recours contre une décision administrative ne sont opposables qu'à la condition d'avoir été mentionnés, ainsi que les voies de recours, dans la notification de la décision " ;<br/>
<br/>
              5. Considérant que si la mention et la production, par le défendeur à une instance, d'une décision à l'appui d'un mémoire contentieux communiqué au requérant établissent que ce dernier a eu connaissance de cette décision au plus tard à la date à laquelle le mémoire lui a été communiqué, une telle circonstance est, par elle-même, sans incidence sur l'application des dispositions précitées de l'article R. 421-5 du code de justice administrative ; qu'il ressort des pièces du dossier soumis aux juges du fond que si la commune de La Colle-sur-Loup a fait mention de l'arrêté du 18 septembre 2008, dans son mémoire enregistré au greffe du tribunal administratif de Nice le 10 décembre 2009, communiqué aux époux A...et à Mme E... le 15 décembre 2009, et en a joint une copie, ces productions ne mentionnaient pas les voies et délais de recours contre cet arrêté ; que c'est donc sans erreur de droit que la cour a jugé qu'une telle circonstance n'était pas de nature à faire courir le délai de recours contentieux et que les conclusions enregistrées le 13 décembre 2010 tendant à l'annulation de l'arrêté du 18 septembre 2008 n'étaient pas tardives ;  <br/>
<br/>
              Sur le bien-fondé de l'arrêt en ce qui concerne la légalité de la décision du 18 septembre 2008 : <br/>
<br/>
              6. Considérant que le transfert des voies privées dans le domaine public communal prévu par les dispositions précitées de l'article L. 318-3 du code de l'urbanisme est subordonné à l'ouverture de ces voies à la circulation publique, laquelle traduit la volonté de leurs propriétaires d'accepter l'usage public de leur bien et de renoncer à son usage purement privé ; <br/>
<br/>
              7. Considérant que la cour a jugé, par une appréciation souveraine des faits  exempte de dénaturation, que par un courrier du 13 septembre 2001, de nombreux riverains propriétaires de portions du chemin des Bas Campons, dont M. et Mme A...et Mme E..., avaient adressé au maire de la commune de La Colle-sur-Loup une pétition manifestant leur opposition à ce que leur chemin soit affecté à la circulation publique, que M. et Mme A...et Mme E... avaient saisi les juridictions judiciaires d'une demande tendant à interdire aux habitants du groupement d'habitations La Pinatello non titulaires d'un droit de passage d'emprunter le chemin avec leurs véhicules, que le tribunal de grande instance de Grasse avait fait droit à leur demande par un jugement du 5 mars 2002, confirmé par un arrêt de la cour d'appel d'Aix-en-Provence du 14 janvier 2008, et qu'il résultait de ces éléments que les propriétaires devaient être regardés, au 18 septembre 2008, comme n'ayant pas consenti, même tacitement, à l'ouverture à la circulation du chemin des Bas Campons, les circonstances que cette opposition n'avait été formalisée que par certains propriétaires, qu'elle n'avait pas été matérialisée par une barrière ou que le chemin avait bénéficié du raccordement à plusieurs réseaux de service public ou d'une desserte de la part des services postaux étant à cet égard indifférentes ; que c'est sans erreur de droit que  la cour administrative d'appel a déduit de ce que le chemin litigieux n'était pas ouvert à la circulation publique que le préfet des Alpes-Maritimes n'avait pu légalement décider son transfert d'office dans le domaine public de la commune ;<br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que le pourvoi de la commune de La Colle-sur-Loup doit être rejeté, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              9. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de La Colle-sur-Loup la somme globale de 3 000 euros à verser à parts égales à M. et Mme A...et à MmeE..., au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              		  D E C I D E :<br/>
                                    --------------<br/>
<br/>
Article 1er : Le pourvoi de la commune de La Colle-sur-Loup est rejeté.<br/>
<br/>
Article 2 : La commune de La Colle-sur-Loup versera à M. et Mme A...et à Mme E... une somme globale de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la commune de La Colle-sur-Loup, à M. et Mme B... A...et à Mme F...E.... <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-02-02-05 COLLECTIVITÉS TERRITORIALES. COMMUNE. BIENS DE LA COMMUNE. VOIRIE COMMUNALE. - TRANSFERT D'UNE VOIE PRIVÉE OUVERTE AU PUBLIC DANS LE DOMAINE PUBLIC (ART. L. 318-3 DU CODE DE L'URBANISME) - DÉPART DU DÉLAI DE RECOURS POUR LES PROPRIÉTAIRES DE LA VOIE - NOTIFICATION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">24-01-01-01-01-02 DOMAINE. DOMAINE PUBLIC. CONSISTANCE ET DÉLIMITATION. DOMAINE PUBLIC ARTIFICIEL. BIENS FAISANT PARTIE DU DOMAINE PUBLIC ARTIFICIEL. VOIES PUBLIQUES ET LEURS DÉPENDANCES. - TRANSFERT D'UNE VOIE PRIVÉE OUVERTE AU PUBLIC DANS LE DOMAINE PUBLIC (ART. L. 318-3 DU CODE DE L'URBANISME) - DÉPART DU DÉLAI DE RECOURS POUR LES PROPRIÉTAIRES DE LA VOIE - NOTIFICATION.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-01-07-02-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉLAIS. POINT DE DÉPART DES DÉLAIS. NOTIFICATION. - RECOURS DU PROPRIÉTAIRE D'UNE VOIE CONTRE LA DÉCISION LA TRANSFÉRANT DANS LE DOMAINE PUBLIC COMMUNAL (ART. L. 318-3 DU CODE DE L'URBANISME).
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">68-06-01-03-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. INTRODUCTION DE L'INSTANCE. DÉLAIS DE RECOURS. POINT DE DÉPART DU DÉLAI. - RECOURS DU PROPRIÉTAIRE D'UNE VOIE CONTRE LA DÉCISION LA TRANSFÉRANT DANS LE DOMAINE PUBLIC COMMUNAL (ART. L. 318-3 DU CODE DE L'URBANISME) - NOTIFICATION AU PROPRIÉTAIRE.
</SCT>
<ANA ID="9A"> 135-02-02-05 Le délai de recours contentieux contre une décision de transfert dans le domaine public communal d'une voie privée ouverte à la circulation publique, prise sur le fondement de l'article L. 318-3 du code de l'urbanisme, ne peut courir, pour les propriétaires intéressés, qu'à compter de la date à laquelle celle-ci leur a été notifiée, peu important que cette décision ait été par ailleurs publiée ou affichée.</ANA>
<ANA ID="9B"> 24-01-01-01-01-02 Le délai de recours contentieux contre une décision de transfert dans le domaine public communal d'une voie privée ouverte à la circulation publique, prise sur le fondement de l'article L. 318-3 du code de l'urbanisme, ne peut courir, pour les propriétaires intéressés, qu'à compter de la date à laquelle celle-ci leur a été notifiée, peu important que cette décision ait été par ailleurs publiée ou affichée.</ANA>
<ANA ID="9C"> 54-01-07-02-01 Le délai de recours contentieux contre une décision de transfert dans le domaine public communal d'une voie privée ouverte à la circulation publique, prise sur le fondement de l'article L. 318-3 du code de l'urbanisme, ne peut courir, pour les propriétaires intéressés, qu'à compter de la date à laquelle celle-ci leur a été notifiée, peu important que cette décision ait été par ailleurs publiée ou affichée.</ANA>
<ANA ID="9D"> 68-06-01-03-01 Le délai de recours contentieux contre une décision de transfert dans le domaine public communal d'une voie privée ouverte à la circulation publique, prise sur le fondement de l'article L. 318-3 du code de l'urbanisme, ne peut courir, pour les propriétaires intéressés, qu'à compter de la date à laquelle celle-ci leur a été notifiée, peu important que cette décision ait été par ailleurs publiée ou affichée.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
