<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041647215</ID>
<ANCIEN_ID>JG_L_2020_02_000000438166</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/64/72/CETATEXT000041647215.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 12/02/2020, 438166, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-02-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>438166</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:438166.20200212</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
              Par une requête, enregistrée le 2 février 2020 au secrétariat du contentieux du Conseil d'Etat, l'Union défense active des forains (UDAF) et France Liberté Voyage, ainsi que Mme A... intervenante, demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative : <br/>
              1°) de suspendre l'exécution de la décision implicite par laquelle le Premier Ministre a refusé d'abroger à l'article 1er du décret n° 99-778 du 10 septembre 1999 les mots " du fait des législations antisémites " ;<br/>
<br/>
              2°) d'enjoindre au Premier ministre de réexaminer le décret n° 99-778 et d'abroger la disposition litigieuse ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement d'une somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Elles soutiennent que :<br/>
              - la condition d'urgence est remplie du fait de l'âge avancé des membres de la communauté des gens du voyage victimes de persécutions en France sous l'Occupation, exclus actuellement de la possibilité de saisir la commission crée par le décret n° 99-778, du fait que son accès est réservé aux personnes spoliées par les législations antisémites ;<br/>
              - il existe un doute sérieux quant à la légalité du décret contesté ; en effet :<br/>
              - il crée une rupture d'égalité entre les victimes de spoliations sous l'Occupation en méconnaissance de l'article 1 de la Déclaration des droits de l'homme et du citoyen de 1789 ;<br/>
<br/>
              - il porte atteinte au droit de propriété garanti par les articles 2 et 17 de la Déclaration des droits de l'homme et du citoyen de 1789 ;<br/>
              - il méconnaît le principe de fraternité tel que consacré par le Conseil constitutionnel dans la décision n° 2018-717/718 QPC.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, et notamment son préambule ;<br/>
              - le décret 99-778 du 10 septembre 1999 instituant une commission pour l'indemnisation des victimes de spoliation intervenues du fait des législations antisémites en vigueur pendant l'occupation ; <br/>
              - le code de justice administrative ;		<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". L'article L. 522-3 de ce code prévoit que le juge des référés peut rejeter une requête par une ordonnance motivée, sans instruction contradictoire ni audience publique, lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. Il résulte des dispositions de l'article L. 521-1 du code de justice administrative que le prononcé de la suspension d'un acte administratif est subordonné notamment à une condition d'urgence. L'urgence justifie la suspension de l'exécution d'un acte administratif lorsque celui-ci porte atteinte de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre. Il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte contesté sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue. L'urgence doit être appréciée objectivement et compte tenu de l'ensemble des circonstances de l'affaire.<br/>
<br/>
              3. Les associations requérantes demandent la suspension du refus du Premier ministre de supprimer de l'article 1er du décret n° 99-778 du 10 septembre 1999 les mots " du fait des législations antisémites " et qu'il soit ordonné au Premier ministre de modifier ce décret en conséquence. Elles soutiennent que cette disposition est illégale en ce qu'elle exclut du champ d'application du dispositif institué pour indemniser les victimes de spoliations intervenues pendant l'Occupation, les tsiganes, forains et membres de la communauté des gens du voyage également victimes de spoliations sous l'Occupation. Pour caractériser l'urgence qu'il y aurait à suspendre l'exécution de cette disposition, elles affirment que l'inclusion immédiate des victimes tsiganes, foraines et membres de la communauté des gens du voyage dans ce dispositif est impérative du fait de leur âge avancé. Toutefois, en se bornant à instaurer un mécanisme d'examen des demandes de réparation présentées par les victimes de spoliations du fait des lois antisémites ou par leurs ayants droit, l'article 1er du décret du 10 septembre 1999 n'est pas susceptible de porter à la situation des personnes représentées une atteinte suffisamment immédiate pour caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, une mesure de suspension soit prononcée. Il y a lieu, au surplus, de constater que les requérantes n'ont pas demandé directement l'annulation du décret litigieux et n'ont saisi le Premier ministre d'une demande d'abrogation que près de vingt ans après l'entrée en vigueur de ce décret. <br/>
<br/>
              4. Il résulte de ce qui précède que la condition d'urgence ne peut pas être regardée comme remplie. Par suite, il y a lieu de rejeter la requête, y compris les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative, selon la procédure prévue par l'article L. 522-3 du même code.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de l'UDAF et de France Liberté Voyage est rejetée.<br/>
<br/>
Article 2 : La présente ordonnance sera notifiée à l'Union défense active des forains (UDAF) et à France Liberté Voyage.<br/>
Copie en sera adressée au Premier ministre. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
