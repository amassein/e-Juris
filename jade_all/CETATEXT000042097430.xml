<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042097430</ID>
<ANCIEN_ID>JG_L_2020_07_000000441256</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/09/74/CETATEXT000042097430.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 03/07/2020, 441256, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-07-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>441256</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON, MEGRET</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:441256.20200703</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 17 juin 2020 au secrétariat du contentieux du Conseil d'Etat, le syndicat unité magistrat SNM FO demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution de la circulaire CRIM2020-15/E3 du 20 mai 2020 de la garde des sceaux, ministre de la justice, ayant pour objet la mise en oeuvre des dispositions relatives aux peines de la loi n° 2019-222 du 23 mars 2019 de réforme pour la justice ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - sa requête est recevable dès lors qu'il est représenté par sa secrétaire générale qui a qualité pour agir au nom du syndicat, qu'il justifie d'un intérêt à agir et que sa requête est dirigée contre une circulaire qui présente un caractère impératif et qui, en tout état de cause, est susceptible d'avoir des effets notables sur les droits ou la situation d'autres personnes que les agents chargés de la mettre en oeuvre ;<br/>
              - la condition d'urgence est remplie eu égard, d'une part, à l'intérêt public qui justifie, pour l'ensemble des justiciables, qu'il soit rapidement statué sur la légalité d'une circulaire qui emporte des effets sur les peines et aménagements de peine susceptibles d'être prononcés à leur encontre et qui édicte de nouvelles règles et, d'autre part, à l'intérêt propre qu'a le syndicat à ce que soit suspendue l'exécution d'une circulaire qui limite le pouvoir d'appréciation des magistrats du parquet et porte atteinte à leur indépendance ;<br/>
              - il existe un doute sérieux quant à la légalité de la circulaire attaquée ;<br/>
              - la circulaire attaquée est entachée d'incompétence en ce que la garde des sceaux, ministre de la justice, y fixe de nouvelles règles relatives au choix de la peine et à ses modalités d'exécution qui relèvent exclusivement du domaine de la loi en application de l'article 34 de la Constitution, notamment en érigeant la capacité hôtelière des centres pénitentiaires comme nouveau critère de détermination et d'exécution de la peine, en affirmant que les peines inférieures ou égales à six mois d'emprisonnement ferme dont l'aménagement n'a pu avoir lieu devront faire l'objet d'un nouvel examen en application de l'article 723-15 du code de procédure pénale, en prévoyant que les écrous inférieurs ou égaux à un mois ne seront pas mis à exécution, en prévoyant que les propositions de constat de fin de mesure pour les peines de travail d'intérêt général qui étaient en fin d'exécution avant l'entrée en vigueur de l'état d'urgence sanitaire seront favorablement accueillies par le ministère public dès lors que le travail d'intérêt général a été correctement commencé et que le reliquat du nombre d'heures à effectuer est résiduel et en énonçant que les peines d'emprisonnement ou les reliquats de peines qui sont à la fois anciens et de faible quantum pourront, en fonction de la personnalité de la personne condamnée et des faits reprochés, ne pas être exécutés ;<br/>
              - elle est entachée d'incompétence en ce qu'elle contraint les magistrats du parquet à requérir et statuer dans un sens prédéfini, en méconnaissance des exigences d'indépendance et d'impartialité de l'autorité judiciaire ;<br/>
              - elle méconnaît le sens et la portée de la loi du 23 mars 2019 en imposant au ministère public de tenir compte de la surpopulation carcérale dans le choix de la peine et de ses modalités d'exécution ;<br/>
              - en demandant aux magistrats du parquet de faire dépendre leurs décisions du taux d'occupation des centres pénitentiaires, elle crée une rupture d'égalité entre les justiciables selon qu'ils sont jugés dans un ressort dans lequel les capacités d'accueil sont, ou non, saturées ;<br/>
              - elle méconnaît l'autorité de la chose jugée et le principe posé à l'article 707 du code de procédure pénale, selon lequel les peines prononcées doivent être exécutées de manière effective dans les meilleurs délais, en ce qu'elle impose le réexamen des demandes d'aménagement de peine d'emprisonnement égales ou inférieures à six mois déjà refusées et la non-exécution systématique des peines courtes, anciennes ou résiduelles.<br/>
              Par un mémoire en défense, enregistré le 26 juin 2020, la garde des sceaux, ministre de la justice, conclut au rejet de la requête. Elle soutient, à titre principal, que le recours est irrecevable dès lors que, premièrement, la circulaire attaquée se borne à préconiser des méthodes de travail et de nouveaux outils de pilotage, à mettre en oeuvre le principe d'individualisation des peines et à adresser des recommandations de politique pénale à l'égard des parquets, sans s'écarter des objectifs ni des dispositions prévues par la loi n° 2019-222 du 23 mars 2019 et sans comporter d'injonction à l'égard des magistrats du siège ni de consignes à l'égard des parquets, dans des dossiers préalablement identifiés, que, deuxièmement, la requête est en réalité dirigée contre les dispositions de la loi elle-même et que, troisièmement, elle est dépourvue, en tant que telle, de tout effet sur les droits ou la situation d'autres personnes. A titre subsidiaire, elle soutient que la condition d'urgence n'est pas remplie et qu'aucun des moyens invoqués n'est propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la circulaire attaquée.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ;<br/>
              - le code de procédure pénale ;<br/>
              - le code pénal ;<br/>
              - le code de la santé publique ;<br/>
              - la loi n° 2019-222 du 23 mars 2019 ;<br/>
              - la loi n° 2020-290 du 23 mars 2020 ;<br/>
              - la loi n° 2020-546 du 11 mai 2020 ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, le syndicat unité magistrat SNM FO et, d'autre part, la garde des sceaux, ministre de la justice ;<br/>
<br/>
              Ont été entendus lors de l'audience publique du 30 juin 2020, à 14 heures 30 : <br/>
<br/>
              - Me Boré, avocat au Conseil d'Etat et à la Cour de Cassation, avocat du syndicat unité magistrat SNM FO ;<br/>
<br/>
              - les représentantes du syndicat unité magistrat SNM FO ;<br/>
<br/>
              - les représentants de la garde des sceaux, ministre de la justice ;<br/>
              et à l'issue de laquelle l'instruction a été close.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
<br/>
              2. L'urgence justifie la suspension de l'exécution d'un acte administratif lorsque celui-ci porte atteinte de manière suffisamment grave et immédiate à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre. Il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte contesté sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue. L'urgence doit, enfin, être appréciée objectivement et compte tenu de l'ensemble des circonstances de l'affaire, à la date à laquelle le juge des référés statue.<br/>
<br/>
              3. Aux termes de l'article 30 du code de procédure pénale : " Le ministre de la justice conduit la politique pénale déterminée par le Gouvernement. Il veille à la cohérence de son application sur le territoire de la République. / A cette fin, il adresse aux magistrats du ministère public des instructions générales (...) ". <br/>
<br/>
              4. Sur le fondement des dispositions citées au point 3, la garde des sceaux, ministre de la justice, dans le contexte de la crise sanitaire et de la sortie progressive du confinement, a adressé aux procureurs généraux près les cours d'appel, au procureur de la République près le tribunal supérieur d'appel, aux procureurs de la République près les tribunaux judiciaires et aux directeurs interrégionaux des services pénitentiaires une circulaire en date du 20 mai 2020 concernant la mise en oeuvre des dispositions relatives aux peines de la loi n° 2019-222 du 23 mars 2019 de réforme pour la justice, dont le second volet relatif aux peines est entré en vigueur le 24 mars 2020. Le syndicat unité magistrat SNM FO demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, d'ordonner la suspension de l'exécution de cette circulaire.<br/>
<br/>
              5. Pour justifier de l'urgence à ordonner la suspension demandée, le syndicat requérant fait valoir que, d'une part, il a un intérêt propre à ce que soit suspendue l'exécution d'une circulaire qui, selon lui, limite le pouvoir d'appréciation des magistrats du parquet et porte atteinte à leur indépendance et que, d'autre part, l'intérêt public justifie que, pour l'ensemble des justiciables, il soit rapidement statué sur la légalité d'une circulaire qui emporte des effets sur les peines et aménagements de peine susceptibles d'être prononcés à leur encontre. Il soutient expressément que les moyens qu'il invoque, lesquels sont, selon lui, propres à créer un doute sérieux sur la légalité de la circulaire, caractérisent suffisamment, en eux-mêmes, l'urgence à en suspendre l'exécution. Toutefois, la condition d'urgence posée par l'article L. 521-1 du code de justice administrative est distincte du point de savoir si les moyens invoqués sont propres à faire naître, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision contestée. Pour le reste, le syndicat requérant n'apporte aucun élément circonstancié et concret de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue.<br/>
<br/>
              6. Il résulte de ce qui précède que la condition d'urgence ne peut, en l'espèce, être regardée comme remplie. Dès lors, et sans qu'il soit besoin de statuer sur la recevabilité de la requête, ni de se prononcer sur l'existence d'un moyen propre à créer un doute sérieux quant à la légalité de la circulaire contestée, la requête présentée par le syndicat unité magistrat SNM FO ne peut qu'être rejetée.<br/>
<br/>
              7. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête du syndicat unité magistrat SNM FO est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée au syndicat unité magistrat SNM FO et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
