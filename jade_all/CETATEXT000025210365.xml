<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025210365</ID>
<ANCIEN_ID>JG_L_2012_01_000000348861</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/21/03/CETATEXT000025210365.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 23/01/2012, 348861</TITRE>
<DATE_DEC>2012-01-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>348861</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Bernard Stirn</PRESIDENT>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. David Gaudillère</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Damien Botteghi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:348861.20120123</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 29 avril 2011 au secrétariat du contentieux du Conseil d'Etat, présenté pour M. Saman Nilantha Fernando A B, domicilié ... ; M. A B demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1103013 du 15 avril 2011 par laquelle le juge des référés du tribunal administratif de Cergy-Pontoise, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, a rejeté sa demande, tendant, d'une part, à la suspension de l'exécution de la décision implicite par laquelle le préfet des Hauts-de-Seine a refusé de renouveler son titre de séjour " salarié ", et, d'autre part, à ce qu'il soit enjoint au préfet de renouveler son titre de séjour " salarié " dans un délai de quinze jours à compter de la notification de l'ordonnance à intervenir sous astreinte de 100 euros par jour de retard ; <br/>
<br/>
              2°) statuant en référé, d'ordonner la suspension de la décision contestée et d'enjoindre au préfet des Hauts-de-Seine de renouveler son titre de séjour " salarié ", ou, subsidiairement, de réexaminer sa demande de renouvellement  dans un délai de quinze jours à compter de la notification de la décision à intervenir sous astreinte de 100 euros par jour de retard ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              Vu le code du travail ;  <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. David Gaudillère, Auditeur,<br/>
<br/>
              - les observations de la SCP Rocheteau, Uzan-Sarano, avocat de M. A B,<br/>
<br/>
              - les conclusions de M. Damien Botteghi, rapporteur public,<br/>
<br/>
              La parole ayant à nouveau été donnée à la SCP Rocheteau, Uzan-Sarano, avocat de M. A B ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'en vertu de l'article L. 313-10 du code de l'entrée et du séjour des étrangers et du droit d'asile, la carte de séjour temporaire autorisant l'exercice d'une activité professionnelle est délivrée à l'étranger titulaire d'un contrat de travail visé conformément aux dispositions de l'article L. 5221-2 du code du travail ; qu'aux termes de l'article R. 311-3 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Sont dispensés de souscrire une demande de carte de séjour : (...) 7° Les étrangers mentionnés au 1° de l'article L. 313-10 séjournant en France pour l'exercice d'une activité d'une durée supérieure ou égale à douze mois sous couvert d'un visa pour un séjour d'une durée supérieure à trois mois et au plus égale à un an et portant la mention " salarié ", pendant la durée de validité de ce visa " ; qu'aux termes de l'article R. 5221-3 du code du travail : " L'autorisation de travail peut être constituée par l'un des documents suivants (...) 6° La carte de séjour temporaire portant la mention " salarié " (...) ou le visa pour un séjour d'une durée supérieure à trois mois mentionné au 7° de l'article R. 311-3 du code de l'entrée et du séjour des étrangers et du droit d'asile, accompagné du contrat de travail visé " ; que, selon les dispositions de l'article R. 5221-33 du code du travail, dans leur rédaction en vigueur à la date de la décision attaquée : "  Par dérogation à l'article R. 5221-32, la validité d'une autorisation de travail constituée d'un des documents mentionnés au 6° de l'article R. 5221-3 est prorogée d'un an lorsque l'étranger se trouve involontairement privé d'emploi à la date de la première demande de renouvellement " ; qu'aux termes de l'article R. 5221-36 du même code : " Le premier renouvellement peut également être refusé lorsque le contrat de travail a été rompu dans les douze mois suivant l'embauche sauf en cas de privation involontaire d'emploi " ;<br/>
<br/>
              Considérant qu'il résulte des dispositions de l'article R. 5221-33 du code du travail que la validité de l'autorisation de travail est prorogée d'un an lorsque l'étranger est involontairement privé d'emploi à la date de première demande de renouvellement ; que ces dispositions s'appliquent dans le cas où l'étranger est titulaire d'un visa délivré pour un séjour d'une durée supérieure à trois mois et portant la mention " salarié " ; <br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis au juge des référés qu'à la date à laquelle M. A B a sollicité le premier renouvellement de son autorisation de travail, il était involontairement privé d'emploi à la suite de la rupture de son contrat de travail, intervenue du fait de l'employeur à la fin de la période d'essai ; qu'il se trouvait ainsi dans la  situation visée au premier alinéa de l'article R. 5221-33 du code du travail ; qu'il suit de là qu'en jugeant qu'aucun des moyens soulevés par le requérant, au nombre desquels figurait la méconnaissance des dispositions précitées de l'article R. 5221-33 du code du travail, n'était de nature à faire naître un doute sérieux quant à la légalité de la décision implicite par laquelle le préfet des Hauts-de-Seine a refusé de renouveler son titre de séjour " salarié ", le juge des référés du tribunal administratif de Cergy-Pontoise a commis une erreur de droit ; que, dès lors, M. A B est fondé, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, à demander l'annulation de l'ordonnance attaquée ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de faire application des dispositions de l'article L. 821-2 du code de justice administrative et de régler l'affaire au titre de la procédure de référé engagée ; <br/>
<br/>
              Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ;<br/>
<br/>
              Considérant, d'une part, que l'urgence à suspendre une décision de refus de renouvellement d'un titre de séjour doit, en principe, être reconnue ; que, d'autre part, il résulte de ce qui a été dit ci-dessus que, en l'état de l'instruction, le moyen tiré de la prolongation d'un an de la validité du titre de séjour par application de l'article R. 5221-33 du code du travail est de nature à créer un doute sérieux quant à la légalité de la décision contestée ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède qu'il y a lieu de faire droit à la demande de M. A B et de suspendre l'exécution de la décision implicite par laquelle le préfet des Hauts-de-Seine a refusé de renouveler son titre de séjour " salarié " ; qu'en l'espèce, il y a lieu d'enjoindre au préfet des Hauts-de-Seine de réexaminer la demande de M. A B et de lui délivrer, dans l'attente, une autorisation provisoire de séjour, sans qu'il y ait lieu d'assortir cette injonction d'une astreinte ; <br/>
<br/>
              Considérant, enfin, qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'État, en application des dispositions de l'article L. 761-1 du code de justice administrative, le versement à M. A B de la somme de 3 000 euros au titre des frais qu'il a exposés tant en première instance qu'en cassation ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'ordonnance du 15 avril 2011 du juge des référés du tribunal administratif de Cergy-Pontoise est annulée.<br/>
<br/>
Article 2 : L'exécution de la décision par laquelle le préfet des Hauts-de-Seine a refusé à M. A B le renouvellement de son titre de séjour " salarié " est suspendue jusqu'à ce qu'il ait été statué sur la demande présentée par l'intéressé devant le tribunal administratif de Cergy-Pontoise.<br/>
<br/>
Article 3 : Il est enjoint au préfet des Hauts-de-Seine, à compter de la notification qui lui sera faite de la présente décision, de réexaminer la demande de M. A B et de lui délivrer, dans l'attente, une autorisation provisoire de séjour.<br/>
<br/>
Article 4 : L'Etat versera à M. A B la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 5 : La présente décision sera notifiée à M. Saman Nilantha Fernando A B, au préfet des Hauts-de-Seine et au ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">335-06-02-01 ÉTRANGERS. EMPLOI DES ÉTRANGERS. MESURES INDIVIDUELLES. TITRE DE TRAVAIL. - AUTORISATION DE TRAVAIL CONSTITUÉE D'UN DOCUMENT VISÉ AU 6° DE L'ARTICLE R. 5221-3 DU CODE DU TRAVAIL - RENOUVELLEMENT - PROROGATION D'UN AN LORSQUE L'ÉTRANGER SE TROUVE INVOLONTAIREMENT PRIVÉ D'EMPLOI À LA DATE DE PREMIÈRE DEMANDE DE RENOUVELLEMENT (ART. R. 5221-33 DU MÊME CODE) - NOTION - RUPTURE DU CONTRAT DE TRAVAIL PAR L'EMPLOYEUR À LA FIN DE LA PÉRIODE D'ESSAI - INCLUSION.
</SCT>
<ANA ID="9A"> 335-06-02-01 L'article R. 5221-33 du code du travail prévoit que la validité de l'autorisation de travail constituée de certains documents, dont la carte de séjour temporaire ou le visa de plus de trois mois portant la mention salarié, est prorogée d'un an lorsque l'étranger est involontairement privé d'emploi à la date de première demande de renouvellement. Est involontairement privé d'emploi au sens de cet article l'étranger dont le contrat de travail est rompu, du fait de l'employeur, à la fin de la période d'essai.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
