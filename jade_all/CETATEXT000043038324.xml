<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043038324</ID>
<ANCIEN_ID>JG_L_2021_01_000000429956</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/03/83/CETATEXT000043038324.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 21/01/2021, 429956, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-01-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>429956</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Réda Wadjinny-Green</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:429956.20210121</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés les 18 avril et 11 juillet 2019 au secrétariat du contentieux du Conseil d'Etat, l'association " Ouvre-boîte " demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite par laquelle le Premier ministre a rejeté sa demande tendant à ce que soient édictées les mesures réglementaires permettant d'assurer la mise en oeuvre de l'obligation de mise à disposition du public des décisions de justice prévue par les articles L. 10 du code de justice administrative et L. 111-13 du code de l'organisation judiciaire ;<br/>
              2°) d'enjoindre au Premier ministre d'édicter ces mesures dans un délai de deux mois et sous astreinte de 500 euros par jour de retard ; <br/>
              3°) de mettre à la charge de l'Etat une somme de 3 000 euros au titre de l'article L. 761 1 du code de justice administrative.  <br/>
<br/>
              L'association " Ouvre-boîte " soutient que la décision du Premier ministre est entachée d'illégalité dès lors que le délai raisonnable dont il disposait pour édicter ces mesures est désormais expiré. <br/>
<br/>
              En application des dispositions de l'article R. 611-7 du code de justice administrative, les parties ont été informées que la décision du Conseil d'Etat était susceptible d'être fondée sur le moyen, relevé d'office, tiré de ce que la requête ne présente plus d'objet dès lors que, postérieurement à son introduction, le décret n° 2020-797 du 29 juin 2020 relatif à la mise à la disposition du public des décisions des juridictions judiciaires et administratives a été publié et applique les dispositions des articles L. 10 du code de justice administrative et L. 111-13 du code de l'organisation judiciaire.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'organisation judiciaire ;<br/>
              - la loi n° 2016-1321 du 7 octobre 2016 ;<br/>
              - la loi n° 2019-22 du 23 mars 2019 ;<br/>
              - le décret n° 2020-797 du 29 juin 2020 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Réda Wadjinny-Green, auditeur, <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de l'association Ouvre-boite ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1.	Il ressort des pièces du dossier que, par un courrier du 18 décembre 2018, l'association " Ouvre-boîte " a demandé au Premier ministre de procéder à la publication des décrets d'application des articles L. 10 du code de justice administrative et L. 111-13 du code de l'organisation judiciaire. Du silence du Premier ministre est née une décision implicite de rejet de cette demande dont l'association " Ouvre-boîte " demande l'annulation pour excès de pouvoir. <br/>
<br/>
              Sur les conclusions du ministre de la justice aux fins de non-lieu :<br/>
<br/>
              2.	Les articles 20 et 21 de la loi du 7 octobre 2016 pour une République numérique ont modifié l'article L. 10 du code de justice administrative et inséré un article L. 111-13 dans le code de l'organisation judiciaire pour poser le principe d'une mise à disposition du public, à titre gratuit et dans le respect de la vie privée des personnes concernées, des décisions rendues par les juridictions administratives et judiciaires. Ces dispositions ont été modifiées par l'article 33 de la loi du 23 mars 2019 de programmation 2018-2022 et de réforme pour la justice, qui comporte plusieurs dispositions relatives à l'occultation des noms et prénoms des personnes physiques lorsqu'elles sont parties ou tiers, à l'occultation, lorsque sa divulgation est de nature à porter atteinte à la sécurité ou au respect de la vie privée de ces personnes ou de leur entourage, de tout élément permettant d'identifier les parties, les tiers, les magistrats et les membres du greffe et enfin à l'interdiction de réutiliser les données d'identité des magistrats et des membres du greffe pour évaluer, analyser, comparer ou prédire leurs pratiques professionnelles réelles ou supposées. Le 29 juin 2020, un décret a été pris pour l'application des articles L. 10 du code de justice administrative et L. 111-13 du code de l'organisation judiciaire dans leur nouvelle rédaction. L'article 9 de ce décret renvoie toutefois à un arrêté du garde des sceaux, ministre de la justice, le soin de fixer " pour chacun des ordres judiciaire et administratif et le cas échéant par niveau d'instance et par type de contentieux, la date à compter de laquelle les décisions de justice sont mises à la disposition du public ". <br/>
<br/>
              3.	Les conclusions de l'association requérante doivent être regardées comme dirigées contre le refus des autorités compétentes de prendre les mesures réglementaires nécessaires à la mise à disposition effective du public des décisions de justice prévue par les articles L. 10 du code de justice administrative et L. 111-13 du code de l'organisation judiciaire, dans leur rédaction issue de la loi du 23 mars 2019, ainsi que l'admet d'ailleurs le ministre de la justice en se bornant à conclure au non-lieu à statuer du fait de la publication du décret du 29 juin 2020 précité. <br/>
<br/>
              4.	Ce décret, publié postérieurement à l'introduction de la requête de l'association requérante, renvoie à l'intervention d'un arrêté ultérieur le soin de fixer la date à partir de laquelle entrera en vigueur le régime de mise à disposition du public des décisions de justice qu'il organise. Il ne saurait dès lors, à lui seul, assurer l'application des dispositions des articles L. 10 du code de justice administrative et L. 111-13 du code de l'organisation judiciaire. Il s'ensuit que, si le ministre de la justice est fondé à soutenir que la requête de l'association " Ouvre-boîte " a perdu son objet en tant qu'elle est dirigée contre le refus du Premier ministre de prendre un décret d'application des dispositions législatives en cause, cette requête conserve un objet en tant qu'elle est dirigée contre le refus du garde des sceaux de fixer par arrêté le calendrier d'entrée en vigueur de ces dispositions. <br/>
<br/>
              5.	Les conclusions aux fins de non-lieu présentées par le garde des sceaux, ministre de la justice ne peuvent donc être accueillies qu'en tant qu'elles portent sur le refus de prendre le décret d'application des articles L. 10 du code de justice administrative et L. 111-13 du code de l'organisation judiciaire.<br/>
<br/>
              Sur les conclusions de l'association dirigées contre le refus du garde des sceaux de prendre l'arrêté prévu par l'article 9 du décret du 29 juin 2020 :<br/>
<br/>
              6.	L'exercice du pouvoir réglementaire comporte non seulement le droit mais aussi l'obligation de prendre dans un délai raisonnable les mesures qu'implique nécessairement l'application de la loi, hors le cas où le respect d'engagements internationaux de la France y ferait obstacle. Lorsqu'un décret pris pour l'application d'une loi renvoie lui-même à un arrêté la détermination de certaines mesures nécessaires à cette application, cet arrêté doit également intervenir dans un délai raisonnable. <br/>
<br/>
              7.	L'effet utile de l'annulation pour excès de pouvoir du refus du pouvoir réglementaire de prendre les mesures qu'implique nécessairement l'application de la loi réside dans l'obligation, que le juge peut prescrire d'office en vertu des dispositions de l'article L. 911-1 du code de justice administrative, pour le pouvoir réglementaire, de prendre ces mesures. Il s'ensuit que lorsqu'il est saisi de conclusions aux fins d'annulation du refus d'une autorité administrative d'édicter les mesures nécessaires à l'application d'une disposition législative, le juge de l'excès de pouvoir est conduit à apprécier la légalité d'un tel refus au regard des règles applicables et des circonstances prévalant à la date de sa décision. <br/>
<br/>
              8.	Ainsi qu'il a été rappelé au point 2 ci-dessus, si le décret d'application des articles L. 10 du code de justice administrative et L. 111-13 du code de l'organisation judiciaire a été pris le 29 juin 2020, l'entrée en vigueur de ces articles demeure subordonnée à l'intervention d'un arrêté du garde des sceaux, ministre de la justices. Aux termes de l'article 9 du décret : " Jusqu'à cette date, la diffusion des décisions est poursuivie dans les conditions prévues par l'article 1er du décret n° 2002-1064 du 7 août 2002 ainsi que par les dispositions applicables aux sites internet du Conseil d'Etat et de la Cour de cassation ". Il résulte de ce qui a été dit au point 6 qu'il appartient au garde des sceaux, ministre de la justice de prendre, dans un délai raisonnable, l'arrêté mentionné à l'article 9 du décret du 29 juin 2020. <br/>
<br/>
              9.	Il n'est pas contesté que la mise à disposition du public des décisions de justice constitue une opération d'une grande complexité pouvant nécessiter, à compter de l'intervention du décret en organisant la mise en oeuvre, des dispositions transitoires. Toutefois, le garde des sceaux, ministre de la justice, ne pouvait, sans méconnaître ses obligations rappelées au point 6, s'abstenir de prendre l'arrêté prévu à l'article 9 du décret du 29 juin 2020 et de fixer le calendrier d'entrée en vigueur des dispositions de ce décret dans un délai raisonnable, plus de 20 mois après la loi du 23 mars 2019 et plus de six mois après la publication du décret du 29 juin 2020 à la date de la présente décision, pour l'application des dispositions législatives relatives à la mise à disposition du public des décisions de justice, laquelle, au demeurant, a été prévue par le législateur dès 2016. Il s'ensuit que l'association " Ouvre-boîte " est fondée à soutenir que le garde des sceaux, ministre de la justice, ne pouvait légalement refuser de prendre cet arrêté. <br/>
<br/>
              Sur les conclusions aux fins d'injonction et d'astreinte :<br/>
<br/>
              10.	Aux termes de l'article L. 911-1 du code de justice administrative, " lorsque sa décision implique nécessairement qu'une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public prenne une mesure d'exécution dans un sens déterminé, la juridiction, saisie de conclusions en ce sens, prescrit, par la même décision, cette mesure assortie, le cas échéant, d'un délai d'exécution ". <br/>
<br/>
              11.	L'annulation de la décision attaquée refusant de prendre les mesures nécessaires à l'application des articles L. 10 du code de justice administrative et L. 111-13 du code de l'organisation judiciaire implique nécessairement l'édiction de ces mesures. Il y a donc lieu pour le Conseil d'Etat d'ordonner au garde des sceaux, ministre de la justice, de prendre l'arrêté prévu par l'article 9 du décret du 29 juin 2020 dans un délai de trois mois à compter de la notification de la présente décision. Il n'y a pas lieu, dans les circonstances de l'espèce, d'assortir cette injonction d'une astreinte.<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              12.	Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à l'association " Ouvre-boîte " au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de statuer sur les conclusions de la requête en tant qu'elles portent sur le refus de prendre le décret d'application des articles L. 10 du code de justice administrative et L. 111-13 du code de l'organisation judiciaire. <br/>
Article 2 : La décision du garde des sceaux, ministre de la justice refusant de prendre l'arrêté prévu par l'article 9 du décret du 29 juin 2020 est annulée. <br/>
Article 3 : Il est enjoint au garde des sceaux, ministre de la justice, de prendre, dans un délai de trois mois à compter de la notification de la présente décision, l'arrêté prévu à l'article 9 du décret du 29 juin 2020.<br/>
Article 4 : L'Etat versera à l'association " Ouvre-boîte " une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : Le surplus des conclusions de la requête est rejeté. <br/>
Article 6 : La présente décision sera notifiée à l'association " Ouvre-boîte " et au garde des sceaux, ministre de la justice. <br/>
Copie en sera adressée au Premier ministre et à la Commission nationale de l'informatique et des libertés.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
