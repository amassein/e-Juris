<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044254895</ID>
<ANCIEN_ID>JG_L_2021_09_000000456319</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/25/48/CETATEXT000044254895.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 15/09/2021, 456319, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-09-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>456319</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:456319.20210915</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 5 septembre 2021 au secrétariat du contentieux du Conseil d'Etat, M. A... B... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative d'invalider plusieurs dispositions du décret n° 2021-1059 du 7 août 2021 et de la loi n° 2021-1040 du 5 août 2021 relative à la gestion de la crise sanitaire.<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - ce décret et cette loi portent atteinte de façon manifestement illégale et substantielle à plusieurs droits et libertés fondamentales, à savoir au principe de l'égalité entre citoyens, à la liberté d'aller et venir, à la liberté d'association, à la liberté de réunion, à la liberté d'entreprendre, au droit à la santé, au droit au respect de la vie privée et de la vie familiale ;<br/>
              - ces atteintes ont des impacts immédiats et concrets pour chacun dans la vie quotidienne ;<br/>
              - les articles 1ers de la loi du 5 août 2021 et du décret du 7 août 2021 qui prévoient la mise en place d'un passe sanitaire limitant l'accès aux lieux et activités compromettent les libertés et droits fondamentaux en ce que ces entraves ne sont pas justifiées et créent des inégalités entre les citoyens en ce qui concerne les modalités d'accès aux vaccins et aux tests de dépistage en fonction des territoires et aux revenus lorsqu'à compter du 15 octobre, ils cesseront d'être gratuits ;<br/>
              - l'utilité et l'efficacité du passe sanitaire dans la lutte contre l'épidémie de covid-19 ne sont pas établies alors qu'il porte une atteinte disproportionnée aux libertés et droits fondamentaux ;<br/>
              - l'application du passe sanitaire à un certain nombre de lieux est excessive au regard de l'objectif poursuivi, ne repose pas sur des fondements scientifiques crédibles et méconnaît la jurisprudence du Conseil constitutionnel, qu'il en va ainsi en ce qui concerne l'obligation de présenter un passe sanitaire dans les transports publics interrégionaux, pour l'exercice des activités de loisirs ou pour rendre visite aux personnes résidant en établissements d'hébergement des personnes âgées dépendantes.<br/>
<br/>
<br/>
              Vu les pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 2021-1040 du 5 août 2021 ;<br/>
              - le décret 2021-699 du 1er juin 2021; <br/>
              - le décret 2021-1059 du 7 août 2021;<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures. " En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. M. B... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'invalider diverses dispositions contenues dans la loi du 5 août 2021 relative à la gestion de la crise sanitaire et le décret du 7 août 2021 modifiant le décret n° 2021-699 du 1er juin 2021 prescrivant les mesures générales nécessaires à la gestion de la sortie de crise sanitaire. <br/>
<br/>
              3. La condition d'urgence posée par l'article L. 521-2 du code de justice administrative s'apprécie objectivement et compte tenu de l'ensemble des circonstances de chaque espèce. En particulier, le requérant qui saisit le juge des référés sur le fondement des dispositions de l'article L. 521-2 du code de justice administrative doit justifier des circonstances particulières caractérisant la nécessité pour lui de bénéficier à très bref délai d'une mesure de la nature de celles qui peuvent être ordonnées sur le fondement de cet article. La circonstance qu'une atteinte à une liberté fondamentale, portée par une mesure administrative, serait avérée n'est pas de nature à caractériser, à elle seule, l'existence d'une situation d'urgence au sens de cet article.<br/>
<br/>
              4. Il résulte des termes mêmes de sa requête que M. B... ne se prévaut explicitement d'aucune urgence. S'il entend cependant faire valoir à ce titre que les atteintes apportées par les dispositions en cause aux différents droits et libertés qu'il énumère ont " des impacts immédiats et concrets " sur la vie quotidienne de chacun, ces considérations générales ne sont pas, eu égard à ce qui a été rappelé au point précédent, de nature à caractériser la nécessité pour lui de bénéficier à très bref délai d'une mesure de la nature de celle que le juge des référés peut prononcer en vertu de l'article L. 521-2 du code de justice administrative.<br/>
<br/>
              5. En outre, il n'entre pas dans les pouvoirs confiés au juge des référés en application de l'article L. 521-2 du code de justice administrative, de prononcer " l'invalidation " des dispositions réglementaires contestées, ni, en tout état de cause, de contrôler la constitutionnalité de la loi.<br/>
<br/>
              6. Il résulte de tout ce qui précède, et sans qu'il soit besoin de se prononcer, en tout état de cause, sur les atteintes alléguées aux libertés ou droits fondamentaux, que la requête doit être rejetée selon la procédure prévue par l'article L. 522-3 du code de justice administrative. <br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de M. B... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. A... B.... <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
