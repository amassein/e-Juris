<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031447483</ID>
<ANCIEN_ID>JG_L_2015_10_000000393265</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/44/74/CETATEXT000031447483.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 09/10/2015, 393265, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-10-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>393265</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP COUTARD, MUNIER-APAIRE</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2015:393265.20151009</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 7 septembre et 1er octobre 2015 au secrétariat du contentieux du Conseil d'Etat, l'Institut de formation supérieure en ostéopathie de Vichy demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution de la décision du 8 juillet 2015 de la ministre des affaires sociales, de la santé et des droits des femmes lui refusant l'agrément permettant de délivrer la formation spécifique à l'ostéopathie ; <br/>
<br/>
              2°) d'ordonner la suspension de l'exécution de la décision rejetant le recours gracieux qu'il avait formé contre cette décision ;<br/>
<br/>
              3°) d'enjoindre à la ministre des affaires sociales, de la santé et des droits des femmes de lui délivrer un agrément provisoire dans un délai de sept jours à compter de la notification de l'ordonnance, sous une astreinte de 5 000 euros par jour de retard ;<br/>
<br/>
               4°) de mettre à la charge de l'Etat le versement de la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - le Conseil d'Etat est compétent pour connaître en premier et dernier ressort de la requête en annulation qu'il a présentée à l'encontre des décisions litigieuses ; <br/>
              - la condition d'urgence est remplie, dès lors qu'il tire l'intégralité de ses revenus des droits d'inscription versés par les étudiants qui ne pourront plus se voir délivrer la formation en ostéopathie, à la suite de la perte de l'agrément, et que celle-ci perturbe considérablement la formation des étudiants et l'organisation de la prochaine rentrée ; <br/>
              - il existe un doute sérieux quant à la légalité des décisions contestées ; <br/>
              - le ministère de la santé avait obligation de prendre en considération, dans l'examen de son recours gracieux, les éléments qu'il n'avait pas versés à son dossier initial de demande d'agrément ;<br/>
              - le directeur général de l'offre de soins n'était pas compétent pour refuser l'agrément sollicité ;<br/>
              - la sous-directrice par intérim de la direction générale de l'offre de soins ne pouvait rejeter son recours gracieux, dès lors qu'elle avait participé en tant que membre actif aux délibérations de la commission nationale d'agrément ayant émis sur sa demande d'agrément un avis défavorable, sur la base d'éléments que le ministère ne peut légalement refuser de lui communiquer ;<br/>
              - les décisions litigieuses sont entachées d'erreurs de fait, dès lors qu'il dispose de locaux permanents exclusivement dédiés à la formation en ostéopathie, dont la surface est mentionnée dans les plans annexés aux conventions de mise à disposition, et d'une clinique interne conforme à la règlementation en vigueur ;<br/>
              - aucune disposition réglementaire n'impose que la surface des locaux soit mentionnée dans la convention ou le bail ;<br/>
              - la convention de mise à disposition des locaux relative au site " Callou " est renouvelable sans formalité particulière ;<br/>
              - l'autorité administrative n'a aucun pouvoir d'appréciation pour s'assurer que les conditions posées par la réglementation sont remplies.<br/>
<br/>
<br/>
<br/>
              Par deux mémoires en défense, enregistrés les 29 septembre et 2 octobre 2015, la ministre des affaires sociales, de la santé et des droits des femmes conclut au rejet de la requête. Elle soutient à titre principal qu'elle est irrecevable et, à titre subsidiaire, que la condition d'urgence n'est pas remplie et que les moyens soulevés par l'institut requérant ne sont de nature à faire naître un doute sur la légalité des décisions contestées.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ; <br/>
              - la loi n° 2002-303 du 4 mars 2002, notamment son article 75 ;<br/>
              - le décret n° 2005-850 du 27 juillet 2005 ;<br/>
              - le décret n° 2014-1043 du 12 septembre 2014 ;<br/>
              - l'arrêté du 29 septembre 2014 relatif à l'agrément des établissements de formation en ostéopathie ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, l'Institut de formation supérieure en ostéopathie de Vichy, d'autre part, la ministre des affaires sociales, de la santé et des droits des femmes ; <br/>
<br/>
              Vu le procès-verbal de l'audience du 1er octobre 2015 à 10 heures à la suite de laquelle l'audience a été reportée au 5 octobre 2015 à 10 heures ;<br/>
<br/>
              Vu le procès-verbal de l'audience publique du 5 octobre 2015 à 10 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Coutard, avocat au Conseil d'Etat et à la Cour de cassation, avocat de l'Institut de formation supérieure en ostéopathie de Vichy ;<br/>
<br/>
              - les représentants de l'Institut de formation supérieure en ostéopathie de Vichy ;<br/>
<br/>
              - les représentantes de la ministre des affaires sociales, de la santé et des droits des femmes ; <br/>
<br/>
              et à l'issue de laquelle l'instruction a été close ; <br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; <br/>
<br/>
              2. Considérant qu'en vertu de l'article 75 de la loi du 4 mars 2002 relative aux droits des malades et à la qualité du système de santé, " l'usage professionnel du titre d'ostéopathe ou de chiropracteur est réservé aux personnes titulaires d'un diplôme sanctionnant une formation spécifique à l'ostéopathie ou à la chiropraxie délivrée par un établissement de formation agréé par le ministre chargé de la santé dans des conditions fixées par décret. Le programme et la durée des études préparatoires et des épreuves après lesquelles peut être délivré ce diplôme sont fixés par voie réglementaire " ; que les conditions de l'agrément des établissements de formation en ostéopathie ont été modifiées par le décret du 12 septembre 2014, dont l'article 29 a prévu que les agréments antérieurement délivrés prendraient fin le 31 août 2015 et que les établissements agréés à la date de publication du décret devraient adresser une nouvelle demande d'agrément, conforme aux exigences du décret, entre le 1er janvier et le 28 février 2015 ; <br/>
<br/>
              3. Considérant que l'Institut de formation supérieure en ostéopathie de Vichy, créé en 1992, a reçu, sur le fondement du décret du 25 mars 2007 pris pour l'application de la loi du 4 mars 2002, un agrément pour cette activité de formation, lequel a été renouvelé en 2013 ; qu'en application du décret du 12 septembre 2014, il a présenté le 27 février 2015 une nouvelle demande d'agrément ; que cette demande a été rejetée par une décision du 8 juillet 2015 ; que l'institut demande, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de cette décision et du rejet du recours gracieux qu'il avait présenté ; que cette demande n'échappe pas manifestement à la compétence du juge des référés du Conseil d'Etat ;<br/>
<br/>
              Sur les conclusions à fin de suspension du rejet du recours gracieux :<br/>
<br/>
              4. Considérant que par une lettre du 31 août 2015, l'Institut de formation supérieure en ostéopathie de Vichy a formé un recours gracieux contre la décision de refus d'agrément du 8 juillet 2015 ; qu'en l'absence de décision expresse de rejet de ce recours gracieux, dont il a été indiqué à l'audience qu'il était toujours en cours d'instruction, et faute que soit écoulé le délai de deux mois nécessaire à la naissance d'une décision implicite de rejet, il n'a été justifié, ni à la date de l'introduction de la requête aux fins de suspension ni à la date à laquelle le juge des référés a statué, d'aucune décision administrative dont la suspension serait susceptible d'être ordonnée par le juge des référés du Conseil d'Etat sur le fondement de l'article L. 521-1 du code de justice administrative ; que les conclusions formées en ce sens par l'institut requérant doivent, par suite, être rejetées ;<br/>
<br/>
              Sur les conclusions à fin de suspension de la décision de refus d'agrément :<br/>
<br/>
              5. Considérant qu'en vertu des dispositions de l'article 1er du décret du 27 juillet 2005 relatif aux délégations de signature des membres du Gouvernement, les directeurs d'administration centrale ont le pouvoir de signer, au nom du ministre et par délégation, l'ensemble des actes, à l'exception des décrets, relatifs aux affaires des services placés sous leur autorité ; que, par suite, le moyen tiré de ce que M.A..., directeur général de l'offre de soins, n'était pas compétent pour prendre la décision de refus d'agrément litigieuse n'est pas de nature à créer un doute sérieux quant à la légalité de celle-ci ;<br/>
<br/>
              6. Considérant qu'aux termes de l'article 2 du décret du 12 septembre 2014 : " L'agrément permettant de délivrer la formation spécifique à l'ostéopathie mentionnée à l'article 75 de la loi du 4 mars 2002 susvisée est accordé aux établissements répondant aux conditions suivantes : 1° Justifier des déclarations préalables prévues par le code de l'éducation pour l'ouverture d'un établissement d'enseignement supérieur privé ; / 2° Proposer une formation permettant l'acquisition des connaissances et des compétences professionnelles, conformément aux dispositions réglementaires relatives à la formation des ostéopathes ; / 3° Présenter un dossier pédagogique répondant aux conditions fixées à l'article 17 ; / 4° Présenter une organisation interne conforme aux articles 10 à 14 ; / 5° Disposer de locaux et d'une capacité financière suffisante dans les conditions prévues aux articles 22 et 23 ; / 6° Bénéficier d'une équipe pédagogique justifiant d'une qualification et répondant aux conditions précisées aux articles 15, 16, 20 et 21 ; / 7° Justifier d'une organisation de la formation clinique répondant aux conditions prévues à l'article 18 ; / 8° Présenter l'engagement mentionné à l'article 5 " ; <br/>
<br/>
              7. Considérant que la demande d'agrément présentée par l'Institut de formation supérieure en ostéopathie de Vichy a été refusée en raison, d'une part, de l'absence de locaux permanents exclusivement dédiés à la formation en ostéopathie et, d'autre part, de l'absence de disposition d'une clinique interne conforme aux exigences de l'article 18 du décret du 12 septembre 2014 ; <br/>
<br/>
              8. Considérant, en premier lieu, que l'article 22 du décret du 12 septembre 2014 dispose que : " Les locaux de l'établissement sont exclusivement dédiés à la formation. Ces locaux sont permanents et conformes à la réglementation applicable en matière de sécurité et d'accessibilité. / La superficie des locaux permanents dédiés à la formation est adaptée à l'effectif maximal des étudiants des différentes années de formation présents en même temps au sein de l'établissement. Cette superficie inclut uniquement les locaux accessibles aux étudiants, les salles de cours théoriques, les salles de travaux pratiques, les locaux dédiés à la formation pratique clinique, y compris les bureaux de l'équipe pédagogique et administrative. Sont exclus les locaux d'archives, les parkings et les locaux dédiés à l'encadrement et aux personnels administratifs non accessibles aux étudiants. Le rapport entre le nombre de mètres carrés et l'effectif maximal d'étudiants présents en même temps au sein de l'établissement est d'au moins cinq mètres carrés par étudiant. /  " ;  qu'il ressort des pièces produites au soutien de la demande d'agrément que l'institut requérant a conclu trois conventions de mise à disposition de locaux pour assurer la formation en ostéopathie litigieuse ; que la convention conclue le 1er décembre 2014 avec la SCM Kiné Ostéo Zénith pour la mise à disposition de locaux situés à Cournon d'Auvergne est d'une durée d'un an renouvelable par tacite reconduction, ce qui suffit à faire regarder les locaux en cause comme permanents au sens de l'article 22 du décret du 12 septembre 2014 ; qu'en revanche, tel n'est pas le cas des locaux situés sur le site de l'annexe Gallieni du pôle universitaire et technologique " Lardy ", à Vichy, dont la mise à disposition est prévue par la convention passée avec la communauté d'agglomération Vichy Val d'Allier, conclue le 23 janvier 2013 pour une durée d'un an et qui ne prévoyait son renouvellement par tacite reconduction, pour une durée équivalente, que pendant deux années ; que tel n'est pas non plus le cas des locaux situés au sein des Thermes Callou, à Vichy, dont la mise à disposition fait l'objet de la convention conclue le 17 juillet 2014 avec la Compagnie de Vichy pour la période allant du 1er janvier au 31 décembre 2014, qui, prévoyait sa résiliation de plein droit, sans formalités, à l'issue de son terme et laissait " à la convenance des parties " son renouvellement éventuel pour une durée d'un an ; que si l'institut requérant a produit dans le cadre de l'instance en référé, d'une part, un avenant à la convention passée avec la communauté d'agglomération Vichy Val d'Allier, conclu en juin 2015 et qui prévoit la mise à disposition des locaux pour une durée de six ans à compter du 1er janvier 2015, renouvelable une fois par reconduction expresse, et, d'autre part, la convention conclue le 15 janvier 2015 avec la Compagnie de Vichy pour la période allant du 1er janvier au 31 décembre 2015 ainsi qu'un courrier en date du 12 juin 2015 par lequel le sous-préfet de Vichy explique qu'en raison de leur caractère domanial, les locaux situés au sein des Thermes Callou ne peuvent faire l'objet d'une mise à disposition d'une durée supérieure à un an, mais qu'" aucun obstacle n'existe à ce jour pour remettre en cause un partenariat existant depuis plusieurs années entre l'Etat, la Compagnie de Vichy et l'Institut de formation supérieure en ostéopathie afin de l'accueillir dans des locaux adaptés ", ces documents, qui sont de nature à faire regarder les locaux qu'ils concernent comme permanents au sens de l'article 22 du décret du 12 septembre 2014 dans le cadre de l'instruction du recours gracieux formé par l'institut, ne sont en revanche pas de nature à remettre en cause l'appréciation à laquelle s'est livrée l'administration en estimant que celui-ci ne pouvait être regardé comme disposant de façon permanente des locaux en cause ; que, contrairement à ce que soutient l'institut requérant, le refus d'agrément litigieux n'impose pas que la surface des locaux soit mentionnée dans les conventions de mise à disposition, mais constate, ainsi que cela a été expliqué au cours de l'audience, qu'en l'absence d'une telle mention dans la convention de mise à disposition des locaux situés sur le site de l'annexe Gallieni du pôle universitaire et technologique " Lardy ", la simple production d'un planning d'occupation des salles pour une année universitaire n'a pas permis à l'administration de s'assurer que la superficie des locaux permanents dédiés à la formation resterait adaptée à l'effectif maximal des étudiants présents en même temps au sein de l'établissement, ainsi que l'impose l'article 22 du décret du 12 septembre 2014 ; que si l'avenant à la convention passée avec la communauté d'agglomération Vichy Val d'Allier précité, qui désigne avec précision les locaux faisant l'objet de la mise à disposition, permet à l'administration d'opérer cette vérification dans le cadre de l'instruction du recours gracieux formé par l'institut requérant, sa production dans le cadre de l'instance en référé n'est pas de nature à remettre en cause l'appréciation portée par l'administration pour refuser l'agrément sollicité ;  qu'il résulte de ce qui précède que le moyen tiré de ce que l'administration aurait commis une erreur de fait en estimant que l'institut ne disposait pas de locaux permanents exclusivement dédiés à la formation en ostéopathie n'est pas de nature à créer un doute sérieux sur la légalité du refus d'agrément contesté ;<br/>
<br/>
              9. Considérant, en second lieu, que l'article 18 du décret du 12 septembre 2014 exige que la formation pratique clinique se déroule au moins pour les deux tiers " au sein de la clinique de l'établissement de formation dédiée à l'accueil des patients, en présence et sous la responsabilité d'un enseignant ostéopathe de l'établissement " ; que si ces dispositions ne s'opposent pas à ce que la formation en clinique interne se déroule dans les locaux d'un établissement extérieur, il appartient dans ce cas au demandeur d'un agrément, compte tenu de l'incertitude découlant d'une telle configuration, de justifier qu'il a pris toutes les dispositions, notamment par voie conventionnelle, pour que la formation pratique clinique soit organisée sous la direction autonome et la responsabilité juridique de l'établissement de formation ; qu'en l'espèce, la clinique interne de l'établissement est organisée dans des locaux situés au sein de l'annexe Gallieni du pôle universitaire et technologique " Lardy ", à Vichy, à l'intérieur du centre d'affaires du Zénith, à Cournon d'Auvergne, et au sein des Thermes Callou, à Vichy ; qu'en l'absence de précisions suffisantes dans le dossier d'agrément sur les conditions et modalités d'intervention des tuteurs extérieurs qui supervisent au sein de ces locaux la formation clinique des étudiants, le moyen tiré de ce que l'administration aurait commis une erreur de fait en estimant que l'institut ne disposait pas d'une clinique interne conforme aux exigences de l'article 18 du décret du 12 septembre 2014 n'est pas de nature à créer un doute sérieux quant à la légalité du refus d'agrément contesté ;<br/>
<br/>
              10. Considérant qu'il résulte de tout ce qui précède qu'aucun des moyens soulevés ne paraît, en l'état de l'instruction, de nature à créer un doute sérieux sur la légalité de la décision de refus d'agrément litigieuse ; que par suite, et sans qu'il soit besoin de se prononcer sur la condition tenant à l'existence d'une situation d'urgence, les conclusions de la requête tendant à la suspension de l'exécution de cette décision doivent être rejetées, de même que, par voie de conséquence, les conclusions à fin d'injonction ;<br/>
<br/>
              11. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante, la somme que l'institut requérant demande au titre des frais exposés par lui et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de l'Institut de formation supérieure en ostéopathie de Vichy est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à l'Institut de formation supérieure en ostéopathie de Vichy et à la ministre des affaires sociales, de la santé et des droits des femmes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
