<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034026065</ID>
<ANCIEN_ID>JG_L_2017_02_000000391088</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/02/60/CETATEXT000034026065.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 10/02/2017, 391088</TITRE>
<DATE_DEC>2017-02-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>391088</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>Mme Catherine Bobo</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:391088.20170210</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés les 16 juin et 16 septembre 2015 au secrétariat du contentieux du Conseil d'Etat, la société Lagardère Active Broadcast demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision n° 2015-41 du 11 février 2015 par laquelle le Conseil supérieur de l'audiovisuel l'a mise en demeure de respecter, à l'avenir, sur le service Europe 1, les dispositions de l'article 1er de la loi du 30 septembre 1986 en ne diffusant plus de séquences portant atteinte à la sauvegarde de l'ordre public, ainsi que la décision du 8 avril 2005 par laquelle le Conseil supérieur de l'audiovisuel a rejeté son recours gracieux dirigé contre cette décision ; <br/>
<br/>
              2°) de mettre à la charge du Conseil supérieur de l'audiovisuel le versement d'une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              - la loi n° 79-587 du 11 juillet 1979 ;<br/>
<br/>
              - la loi n° 86-1067 du 30 septembre 1986 ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Catherine Bobo, auditeur,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de la société Lagardère Active Broadcast.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du premier alinéa de l'article 42 de la loi du 30 septembre 1986 relative à la liberté de communication : " Les éditeurs et distributeurs de services de communication audiovisuelle et les opérateurs de réseaux satellitaires peuvent être mis en demeure de respecter les obligations qui leur sont imposées par les textes législatifs et réglementaires et par les principes définis aux articles 1er et 3-1 " ; qu'aux termes de l'article 1er de la même loi : " la communication au public par voie électronique est libre./ L'exercice de cette liberté ne peut être limité que dans la mesure requise (...) par la sauvegarde de l'ordre public " ; <br/>
<br/>
              2. Considérant que, par une décision du 11 février 2015, le Conseil supérieur de l'audiovisuel, estimant que la diffusion en direct le 9 janvier 2015 sur l'antenne du service radiophonique Europe 1, alors que des actions terroristes simultanées étaient en cours, d'informations relatives à l'assaut des forces de l'ordre contre des terroristes retranchés à Dammartin-en-Goële, avant que le terroriste présent dans le magasin Hyper Cacher de Vincennes, qui avait menacé d'exécuter les otages qu'il retenait si les autres terroristes n'étaient pas libérés, ne soit mis hors d'état de nuire, avait mis en péril la vie des personnes retenues en otage à Vincennes, a mis la société Lagardère Active Broadcast, éditeur du service, en demeure de respecter à l'avenir, conformément aux dispositions rappelées ci-dessus de l'article 1er de la loi du 30 septembre 1986, " les règles élémentaires de prudence permettant d'assurer le maintien de la sécurité publique et la sauvegarde de l'ordre public " ; que la société demande l'annulation de cette mise en demeure ainsi que de la décision du 8 avril 2015 par laquelle le CSA a rejeté son recours gracieux ; <br/>
<br/>
              Sur la légalité externe des décisions attaquées :  <br/>
<br/>
              3. Considérant, en premier lieu, que les dispositions citées ci-dessus de l'article 42 de la loi du 30 septembre 1986 qui confèrent au  CSA le pouvoir de procéder à une mise en demeure impliquent, alors même qu'elle n'entre dans aucune des catégories de décisions administratives qui doivent être motivées en application de la loi du 11 juillet 1979 visée ci-dessus, qu'une telle décision mentionne les faits constatés par le CSA ainsi que les obligations dont il estime qu'elles ont été méconnues et auxquelles il invite l'éditeur, le distributeur ou l'opérateur à se conformer à l'avenir ; que, contrairement à ce que soutient la société requérante, la décision attaquée du 11 février 2015 comporte l'ensemble de ces éléments ; <br/>
<br/>
              4. Considérant, en second lieu, qu'en vertu de l'avant-dernier alinéa de l'article 4 de la loi du 30 septembre 1986, le CSA ne peut délibérer que si quatre au moins de ses membres sont présents ; qu'il ressort des mentions des procès-verbaux des séances des 11 février et 8 avril 2015, au cours desquelles ont été adoptées la décision de mise en demeure et la décision rejetant le recours gracieux de la société requérante, que huit membres du CSA étaient présents ; qu'ainsi, le moyen tiré de ce que la règle de quorum prévue par la loi aurait été méconnue manque en fait ;<br/>
<br/>
              Sur la légalité interne : <br/>
<br/>
              5. Considérant, en premier lieu, qu'il ressort de l'écoute de la séquence diffusée le vendredi 9 janvier 2015 entre 16h56 et 17h15 que les intervenants à l'antenne ont, dès 16h58, mentionné un assaut en cours à Dammartin-en-Goële puis déclaré, à 17h00 : " tout porte à croire que l'assaut a été donné et que l'assaut est terminé " et à 17h03 : " Véritablement, tout laisse penser que l'assaut a été donné, que l'assaut est terminé et que les terroristes ont été neutralisés " ; que ces indications ont été réitérées à plusieurs reprises au cours des minutes suivantes, alors que le terroriste retranché dans le magasin Hyper Cacher de Vincennes n'avait pas encore été mis hors d'état de nuire ; que, dès lors, contrairement à ce que soutient la société requérante, la mise en demeure litigieuse ne repose par sur des faits matériellement inexacts ; <br/>
<br/>
              6.  Considérant, en deuxième lieu, que le CSA n'a pas commis  d'erreur de droit en regardant la diffusion d'informations de nature à mettre en péril la vie de personnes retenues en otage comme contraire aux dispositions de l'article 1er de la loi du 30 septembre 1986 selon lesquelles l'exercice de la liberté de communication est limitée par la sauvegarde de l'ordre public, dont la sécurité des personnes constitue un élément ; qu'il n'a pas commis d'erreur d'appréciation en estimant que la diffusion des informations mentionnées au point 5 avait présenté, dans les circonstances de l'espèce, le caractère d'un manquement de l'éditeur du service Europe 1 à l'obligation qui résultait pour lui de ces dispositions ; que, contrairement à ce qui est soutenu, la mise en demeure litigieuse ne méconnaît pas les prérogatives de l'autorité judiciaire ;<br/>
<br/>
              7. Considérant, enfin, qu'aux termes de l'article 10 de la convention européenne des droits de l'homme et des libertés fondamentales : " 1.Toute personne a droit à la liberté d'expression. Ce droit comprend la liberté d'opinion et la liberté de recevoir ou de communiquer des informations ou des idées sans qu'il puisse y avoir ingérence d'autorités publiques et sans considération de frontière. Le présent article n'empêche pas les Etats de soumettre les entreprises de radiodiffusion, de cinéma ou de télévision à un régime d'autorisations./ 2. L'exercice de ces libertés comportant des devoirs et des responsabilités peut être soumis à certaines formalités, conditions, restrictions ou sanctions prévues par la loi, qui constituent des mesures nécessaires, dans une société démocratique, à la sécurité nationale, à l'intégrité territoriale ou à la sûreté publique, à la défense de l'ordre et à la prévention du crime, à la protection de la santé ou de la morale, à la protection de la réputation ou des droits d'autrui, pour empêcher la divulgation d'informations confidentielles ou pour garantir l'autorité et l'impartialité du pouvoir judiciaire " ; qu'en mettant en demeure la société requérante de ne pas procéder à l'avenir à la diffusion d'informations dans des conditions du type de celles qui ont été analysées aux points 2 et 5 ci-dessus, par une décision qui, au demeurant, ne présente pas le caractère d'une sanction mais constitue une mesure destinée à éclairer son destinataire sur l'étendue de ses obligations, le CSA n'a pas donné aux obligations qui résultent des dispositions de l'article 1er de la loi du 30 septembre 1986 une portée incompatible avec les stipulations précitées de l'article 10 de la convention européenne des droits de l'homme et des libertés fondamentales ; <br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que la société Lagardère Active Broadcast n'est pas fondée à demander l'annulation de la décision attaquée ; que ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
		Article 1er : La requête de la société Lagardère Active Broadcast est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la société Lagardère Active Broadcast et au Conseil supérieur de l'audiovisuel.<br/>
		Copie en sera adressée à la ministre de la culture et de la communication.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">56-04 RADIO ET TÉLÉVISION. SERVICES PRIVÉS DE RADIO ET DE TÉLÉVISION. - POUVOIR DE MISE EN DEMEURE DU CSA - ELÉMENTS DEVANT OBLIGATOIREMENT FIGURER DANS LA MISE EN DEMEURE - FAITS CONSTATÉS PAR LE CSA ET OBLIGATIONS QUI ONT SELON LUI ÉTÉ MÉCONNUES.
</SCT>
<ANA ID="9A"> 56-04 Les dispositions de l'article 42 de la loi n° 86-1067 du 30 septembre 1986 qui confèrent au Conseil supérieure de l'audiovisuel (CSA) le pouvoir de procéder à une mise en demeure impliquent, alors même qu'elle n'entre dans aucune des catégories de décisions administratives qui doivent être motivées en application de la loi n° 79-587 du 11 juillet 1979, qu'une telle décision mentionne les faits constatés par le CSA ainsi que les obligations dont il estime qu'elles ont été méconnues et auxquelles il invite l'éditeur, le distributeur ou l'opérateur à se conformer à l'avenir.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
