<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039217422</ID>
<ANCIEN_ID>JG_L_2019_10_000000417886</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/21/74/CETATEXT000039217422.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 14/10/2019, 417886</TITRE>
<DATE_DEC>2019-10-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417886</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>Mme Pauline Berne</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:417886.20191014</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Marseille d'annuler la décision du préfet des Hautes-Alpes du 17 mai 2013 portant réduction du montant de l'aide que lui ont accordé l'Etat et le Fonds européen agricole pour le développement rural au titre du plan de modernisation des bâtiments d'élevage (FEADER-PMBE). Par un jugement n° 1306598 du 3 décembre 2015, le tribunal administratif de Marseille a annulé la décision préfectorale du 17 mai 2013.<br/>
<br/>
              Par un arrêt n° 16MA00695 du 4 décembre 2017, la cour administrative d'appel de Marseille a rejeté l'appel formé par le ministre de l'agriculture, de l'agroalimentaire et de la forêt contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 5 février et 7 mai 2018 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'agriculture et de l'alimentation demande au Conseil d'Etat d'annuler cet arrêt.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le règlement (CE) n° 1290/2005 du Conseil du 21 juin 2005 ;<br/>
              - le règlement (CE) n° 1698/2005 du Conseil du 20 septembre 2005 ;<br/>
              - le règlement (CE) n° 65/2011 de la Commission du 27 janvier 2011 ;<br/>
              - la loi n° 79-587 du 11 juillet 1979 modifiée ;<br/>
              - le décret n° 99-1060 du 16 décembre 1999 ;<br/>
              - le décret n° 2009-1452 du 24 novembre 2009 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Berne, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté préfectoral du 19 octobre 2011, M. B... a bénéficié d'une subvention de 15 285,60 euros pour l'achat d'un tracteur d'un prix hors taxes de 50 952 euros, au titre de l'aide à la modernisation-mécanisation en zone de montagne prévue par le Programme de développement rural 2007-2013. A la suite d'un contrôle sur place réalisé le 28 mars 2013, le préfet des Hautes-Alpes a, par lettre du 17 mai 2013, prononcé la déchéance partielle de cette aide et exigé la restitution de 9 097,38 euros, au motif,  d'une part, que M. B... ne s'était pas personnellement acquitté d'une partie du prix du tracteur, à hauteur de 25 000 euros, réglée directement au fournisseur par ses oncles et, d'autre part, que le fournisseur avait réduit les versements demandés du montant de la reprise d'une presse agricole, soit 5 325 euros. A la suite du rejet implicite de son recours hiérarchique, M. B... a saisi le tribunal administratif de Marseille qui, par un jugement du 3 décembre 2015, a annulé la décision du préfet. Le ministre de l'agriculture et de l'alimentation se pourvoit en cassation contre l'arrêt du 4 décembre 2017 par lequel la cour administrative d'appel de Marseille a rejeté l'appel qu'il a formé contre ce jugement. <br/>
<br/>
              2. En premier lieu, aux termes de l'article 26 du règlement (CE) n°1698/2005 du Conseil du 20 septembre 2005 concernant le soutien au développement rural par le Fonds européen agricole pour le développement rural (FEADER), alors en vigueur : " 1. L'aide prévue à l'article 20, point b) i), est accordée pour les investissements matériels et/ou immatériels qui : / a) améliorent le niveau global des résultats de l'exploitation, et / b) respectent les normes communautaires applicables à l'investissement concerné. (...) / 2. L'aide est limitée au taux maximal fixé en annexe ". Aux termes de l'article 71 du même règlement " (...) 3. Les règles d'éligibilité des dépenses sont fixées au niveau national, sous réserve des conditions particulières établies au titre du présent règlement pour certaines mesures de développement rural. [...]. ". Aux termes de l'article 72 de ce règlement : " 1. Sans préjudice des règles relatives à la liberté d'établissement et à la libre prestation de services au sens des articles 43 à 49 du traité, l'État membre veille à ce que la participation du Feader ne reste acquise à une opération d'investissement cofinancée que si cette opération ne connaît pas, dans un délai de cinq ans à compter de la décision de financement par l'autorité de gestion, de modification importante: / a) affectant sa nature ou ses conditions de mise en oeuvre ou procurant un avantage indu à une entreprise ou à une collectivité publique; / b) résultant soit d'un changement dans la nature de la propriété d'une infrastructure, soit de l'arrêt ou d'une délocalisation d'une activité productive. " / 2. Les sommes indûment versées sont recouvrées conformément à l'article 33 du règlement (CE) n° 1290/2005 ".<br/>
<br/>
              3. Aux termes de l'article 5 du règlement (UE) n° 65/2011 de la Commission du 27 juillet 2011 portant modalités d'application du règlement (CE) n° 1698/2005 du Conseil, alors en vigueur : " 1.  En cas de paiement indu, le bénéficiaire concerné a l'obligation de rembourser les montants en cause majorés d'intérêts calculés conformément au paragraphe 2. (...) ". Aux termes de l'article 18 du même règlement : " L'État membre recouvre le montant de l'aide et/ou refuse cette dernière ou détermine le montant de la réduction de l'aide, en particulier en fonction de la gravité, de l'étendue et du caractère persistant du manquement constaté ". Aux termes de l'article 24 de ce règlement : " Les paiements effectués par les bénéficiaires sont attestés par des factures et des preuves de paiement. Lorsque cela n'est pas possible, les paiements sont accompagnés de pièces de valeur probante équivalente ". Aux termes du 1° de l'article 26 de ce règlement : " En effectuant les contrôles sur place, les États membres s'attachent à vérifier : a) que les demandes de paiement introduites par le bénéficiaire sont justifiées par des pièces comptables ou d'autres documents, y compris, le cas échéant, une vérification de l'exactitude des données de la demande de paiement sur la base de données ou de documents commerciaux détenus par des tiers ; [...] c) que la destination effective ou prévue de l'opération correspond aux objectifs décrits dans la demande d'aide ". <br/>
<br/>
              4. Enfin, aux termes de l'article 5 du décret du 24 novembre 2009 fixant les règles d'éligibilité des dépenses des programmes de développement rural : " I. - Sont regardés comme des dépenses réelles justifiées par les bénéficiaires les paiements justifiés soit par des factures acquittées, soit par des factures auxquelles sont jointes des pièces permettant de vérifier le paiement effectif aux créanciers, soit par des pièces comptables de valeur probante équivalente (...) ". Aux termes de l'article 7 du même décret : " (...) Les dépenses éligibles ne dépassent pas la valeur de l'investissement ou du projet, déduction faite des recettes ".<br/>
<br/>
              5. Il résulte de ces dispositions que les contrôles effectués par l'administration visent notamment à vérifier l'exactitude des données de la demande de paiement et à s'assurer que l'opération est réalisée conformément à la demande initiale de subvention, au regard de laquelle l'aide attribuée a été calculée. La cour a relevé, par une appréciation souveraine des faits non arguée de dénaturation, que M. B..., qui avait produit la facture du tracteur établie à son nom et attestant du règlement intégral du prix prévu, était seul propriétaire de cet engin agricole et qu'il n'était pas contesté par l'administration qu'il en était également l'unique exploitant. Dans ces conditions, c'est sans erreur de droit que la cour a jugé que le préfet ne pouvait légalement, au seul motif que le prix avait été en partie supporté par les oncles de M. B..., que ce soit au titre d'un don ou d'un prêt à ce dernier, réduire l'aide allouée au prorata des sommes versées par ces derniers.<br/>
<br/>
              6. En second lieu, il résulte des dispositions précitées de l'article 26 du règlement du 20 septembre 2005 que l'aide accordée par le FEADER est proportionnelle au montant de l'investissement subventionné. Par ailleurs, aux termes de l'article 10 du décret du 16 décembre 1999 relatif aux subventions de l'Etat pour des projets d'investissement, dans sa rédaction applicable au présent litige : " Pour chaque décision attributive, le montant maximum prévisionnel de la subvention est déterminé par l'application à la dépense subventionnable prévisionnelle d'un taux arrêté par l'autorité compétente. La dépense subventionnable prévisionnelle est calculée à partir du coût du projet d'investissement présenté " et aux termes de l'article 13 du même décret : " Sauf dans le cas prévu au deuxième alinéa de l'article 10 où le montant de la subvention est calculé conformément à un barème, la liquidation de la subvention s'effectue par application au montant de la dépense réelle, plafonné au montant prévisionnel de la dépense subventionnable, du taux de subvention mentionné au premier alinéa de l'article 10. Ce taux, ainsi que la nature de la dépense subventionnable, ne peuvent être modifiés par rapport à la décision attributive ".<br/>
<br/>
              7. Il résulte de ces dispositions que la subvention versée, tant par le FEADER que par l'Etat, est proportionnelle à la dépense d'investissement initialement présentée, le taux de subvention applicable au montant des dépenses éligibles étant déterminé par l'autorité compétente. Il résulte également de ces dispositions, ainsi que des dispositions précitées de l'article 72 du règlement du 20 septembre 2005 et de l'article 5 du règlement du 27 juillet 2011, que l'autorité compétente est tenue, si les dépenses réelles s'avèrent inférieures aux dépenses prévisionnelles, de réclamer le reversement de l'aide au prorata des dépenses non réalisées, sans préjudice, le cas échéant, de la réduction éventuellement plus importante susceptible d'être décidée en cas de manquement, en application de l'article 18 du règlement (UE) n° 65/2011, à raison de la gravité, de l'étendue et du caractère persistant de ce manquement. Lorsque le reversement est exigé au prorata des dépenses non réalisées, sa légalité s'apprécie, s'il y a lieu, au regard de chacune des causes de réduction du montant des dépenses réelles par rapport aux dépenses prévisionnelles. <br/>
<br/>
              8. Il ressort des pièces du dossier soumis aux juges du fond que la décision de déchéance partielle du préfet était fondée sur le constat que la dépense réelle de M. B... était inférieure à la dépense initialement présentée, à raison d'une part du versement des sommes acquittées par les oncles de M. B... et d'autre part de la reprise, par le fournisseur, d'une presse agricole et que le montant de la restitution exigée correspondait à la réduction de l'aide accordée, qui était de 30% de l'investissement subventionné, au prorata des dépenses ainsi regardées comme non réalisées. Le ministre de l'agriculture et de l'alimentation est fondé à soutenir que la cour a commis une erreur de droit en jugeant, pour annuler cette décision dans son ensemble, y compris en tant qu'elle procédait de la réduction des dépenses réelles exposées par M. B... à raison de la reprise par le fournisseur d'une presse agricole, que l'aide n'était pas proportionnelle à la dépense et qu'il ne résultait pas de l'instruction que le préfet des Hautes-Alpes aurait pris la même décision s'il avait  uniquement tenu compte de cette reprise.<br/>
<br/>
              9. Il résulte de tout ce qui précède que le ministre de l'agriculture et de l'alimentation est fondé à demander l'annulation de l'arrêt qu'il attaque en tant seulement qu'il annule la décision du 17 mai 2013 du préfet des Hautes-Alpes à hauteur de la fraction de restitution correspondant à la reprise de la presse agricole. <br/>
<br/>
              10. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille est annulé en tant qu'il annule la décision du 17 mai 2013 du préfet des Hautes-Alpes à hauteur de la fraction de restitution correspondant à la reprise de la presse agricole.<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Marseille.<br/>
Article 3 : Les conclusions présentées par M. B... au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4: La présente décision sera notifiée au ministre de l'agriculture et de l'alimentation et à M. A... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">03-03-06 AGRICULTURE ET FORÊTS. EXPLOITATIONS AGRICOLES. AIDES DE L'UNION EUROPÉENNE. - AIDES À L'INVESTISSEMENT (ART. 26 DU RÈGLEMENT (CE) N°1698/2005) - DÉPENSES ÉLIGIBLES - 1) CONTRÔLE DE L'ADMINISTRATION - PORTÉE - 2) APPLICATION - POSSIBILITÉ, POUR LE PRÉFET, DE RÉDUIRE L'AIDE AU MOTIF QUE LE PRIX DU TRACTEUR OBJET DE L'AIDE AVAIT ÉTÉ EN PARTIE SUPPORTÉ PAR LES ONCLES DU BÉNÉFICIAIRE - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">15-05-14 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. RÈGLES APPLICABLES. POLITIQUE AGRICOLE COMMUNE. - AIDES À L'INVESTISSEMENT (ART. 26 DU RÈGLEMENT (CE) N°1698/2005) - DÉPENSES ÉLIGIBLES - CONTRÔLE DE L'ADMINISTRATION - PORTÉE.
</SCT>
<ANA ID="9A"> 03-03-06 1) Il résulte des articles 26, 71 et 72 du règlement (CE) n° 1698/2005 du Conseil du 20 septembre 2005, des articles 5, 18, 24 et 26 du règlement (UE) n° 65/2011 de la Commission du 27 juillet 2011 et des articles 5 et 7 du décret n° 2009-1452 du 24 novembre 2009 que les contrôles effectués par l'administration visent notamment à vérifier l'exactitude des données de la demande de paiement et à s'assurer que l'opération est réalisée conformément à la demande initiale de subvention, au regard de laquelle l'aide attribuée a été calculée.... ,,2) La cour a relevé que le bénéficiaire de la subvention, qui avait produit la facture du tracteur établie à son nom et attestant du règlement intégral du prix prévu, était seul propriétaire de cet engin agricole et qu'il n'était pas contesté par l'administration qu'il en était également l'unique exploitant. Dans ces conditions, c'est sans erreur de droit que la cour a jugé que le préfet ne pouvait légalement, au seul motif que le prix avait été en partie supporté par les oncles de l'intéressé, que ce soit au titre d'un don ou d'un prêt à ce dernier, réduire l'aide allouée au prorata des sommes versées par ces derniers.</ANA>
<ANA ID="9B"> 15-05-14 Il résulte des articles 26, 71 et 72 du règlement (CE) n° 1698/2005 du Conseil du 20 septembre 2005, des articles 5, 18, 24 et 26 du règlement (UE) n° 65/2011 de la Commission du 27 juillet 2011 et des articles 5 et 7 du décret n° 2009-1452 du 24 novembre 2009 que les contrôles effectués par l'administration visent notamment à vérifier l'exactitude des données de la demande de paiement et à s'assurer que l'opération est réalisée conformément à la demande initiale de subvention, au regard de laquelle l'aide attribuée a été calculée.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
