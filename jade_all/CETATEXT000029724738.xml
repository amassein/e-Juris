<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029724738</ID>
<ANCIEN_ID>JG_L_2014_11_000000369632</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/72/47/CETATEXT000029724738.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  4ème sous-section jugeant seule, 07/11/2014, 369632, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-11-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>369632</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 4ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Pauline Pannier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:369632.20141107</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 24 juin 2013 au secrétariat du contentieux du Conseil d'Etat, présentée par la SAS Sebadis, dont le siège est Pôle commercial Comtal Sud à Onet-le-Château (12850) ; la SAS Sebadis demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision n° 1694 T du 3 avril 2013 par laquelle la commission nationale d'aménagement commercial a accordé à la SCCV Foncière Chabrières l'autorisation préalable requise en vue de procéder à la création d'un supermarché, à l'enseigne " Intermarché ", d'une surface de vente de 2 200 m² à Rodez (Aveyron) ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat et de la SCCV Foncière Chabrières le versement de la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de commerce ;<br/>
<br/>
              Vu la loi n° 73-1193 du 27 décembre 1973 ;<br/>
<br/>
              Vu la loi n° 2008-776 du 4 août 2008 ;<br/>
<br/>
              Vu le décret n° 2008-1212 du 24 novembre 2008 ;<br/>
<br/>
              Vu le décret n° 2011-921 du 1er août 2011 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Pannier, auditeur,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Sur la légalité de la décision attaquée :<br/>
<br/>
              En ce qui concerne la procédure devant la commission nationale :<br/>
<br/>
              1. Considérant que, contrairement à ce que soutient la société requérante, il ressort des pièces du dossier que la commission nationale était régulièrement composée et que les avis des ministres intéressés étaient signés par des personnes habilitées à le faire ;<br/>
<br/>
              En ce qui concerne la composition du dossier de demande :<br/>
<br/>
              2. Considérant que si la société requérante soutient que le projet dont la commission nationale était saisi était un projet d'extension d'un supermarché existant et qu'ainsi le dossier de demande était incomplet faute de comporter l'attestation exigée par l'article R. 752 10 du code de commerce, il ressort des pièces du dossier que le projet ne consiste pas en une extension d'un bâtiment existant au sens de cet article, mais en une création nouvelle ; qu'ainsi, le dossier de demande n'avait pas à comporter l'attestation visée à l'article R. 752-10 du code de commerce ;<br/>
<br/>
              3. Considérant que, contrairement à ce que soutient la requérante, il ressort des pièces du dossier que les éléments fournis par le pétitionnaire et complétés par les services instructeurs ont permis à la commission nationale d'apprécier l'impact du projet en termes d'aménagement du territoire et de développement durable conformément à l'article L. 752-6 du code de commerce ; <br/>
<br/>
              En ce qui concerne l'appréciation de la commission nationale :<br/>
<br/>
              4. Considérant qu'il appartient aux commissions d'aménagement commercial, lorsqu'elles se prononcent sur un projet d'exploitation commerciale soumis à autorisation en application de l'article L. 752-1 du code de commerce, d'apprécier la conformité de ce projet aux objectifs prévus à l'article 1er de la loi du 27 décembre 1973 et à l'article L. 750-1 du code de commerce, au vu des critères d'évaluation mentionnés à l'article L. 752-6 du même code ; que l'autorisation ne peut être refusée que si, eu égard à ses effets, le projet compromet la réalisation de ces objectifs ;<br/>
<br/>
              5. Considérant, en premier lieu, que, si la société requérante soutient que la décision attaquée compromet l'objectif d'aménagement du territoire, il ressort des pièces du dossier que le projet contesté permettra de développer une offre commerciale de proximité à l'est de l'agglomération de Rodez et que son impact sur les flux de transport restera limité au regard des capacités des infrastructures routières existantes ; <br/>
<br/>
              6. Considérant, en deuxième lieu, que si la société requérante soutient que la décision attaquée compromet l'objectif fixé par le législateur en matière de développement durable, il ressort des pièces du dossier que le projet autorisé prévoit la mise en place de dispositifs permettant la réduction des consommations d'énergie et le recyclage des déchets et que son site d'implantation est desservi par les transports collectifs et accessible par les modes de transport doux ;<br/>
<br/>
              7. Considérant, en troisième lieu, que, si la société requérante soutient que la décision attaquée méconnaît l'objectif fixé par le législateur en matière de protection des consommateurs, il ne ressort pas des pièces du dossier que le projet compromettrait cet objectif ;<br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que la société Sebadis n'est pas fondée à demander l'annulation de la décision attaquée ;<br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              9. Considérant que ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat et de la SCCV Foncière Chabrières, qui ne sont pas, dans la présente instance, les parties perdantes ; qu'il y a lieu, en revanche, de mettre à la charge de la requérante le versement à la SCCV Foncière Chabrières de la somme de 5 000 euros au titre des frais exposés par elle et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				---------------<br/>
<br/>
       Article 1er : La requête présentée par la SAS Sebadis est rejetée.<br/>
<br/>
Article 2 : La SAS Sebadis versera à la SCCV Foncière Chabrières la somme de 5 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la SAS Sebadis, à la SCCV Foncière Chabrières et à la Commission nationale d'aménagement commercial.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
