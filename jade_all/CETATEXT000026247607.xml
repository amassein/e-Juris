<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026247607</ID>
<ANCIEN_ID>JG_L_2012_08_000000348113</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/24/76/CETATEXT000026247607.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère sous-section jugeant seule, 01/08/2012, 348113, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-08-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>348113</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Denis Piveteau</PRESIDENT>
<AVOCATS>GEORGES ; SCP POTIER DE LA VARDE, BUK LAMENT</AVOCATS>
<RAPPORTEUR>Mme Chrystelle Naudan-Carastro</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Claire Landais</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:348113.20120801</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 4 avril et 1er juillet 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune de Mandelieu-la-Napoule, représentée par son maire ; la commune de Mandelieu-la-Napoule demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 08MA04105 du 27 janvier 2011 par laquelle la cour administrative d'appel de Marseille a, à la demande de M. A, annulé le jugement n° 0306019-0600928-0603467 du 19 juin 2008 du tribunal administratif de Nice et les arrêtés du 7 novembre 2003 et du 26 décembre 2005 du maire de Mandelieu-la-Napoule lui délivrant un certificat d'urbanisme négatif et lui refusant un permis de construire, enjoint au maire de Mandelieu-la-Napoule de réexaminer la demande de permis de construire de M. A en se prononçant dans un délai de quatre mois et mis à la charge de la commune de Mandelieu-la-Napoule le versement de la somme de 1 500 euros à M. A, sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M. A ;<br/>
<br/>
              3°) de mettre à la charge de M. A le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu le code de l'environnement ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Chrystelle Naudan-Carastro, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de Me Georges, avocat de la commune de Mandelieu-la-Napoule et de la SCP Potier de la Varde, Buk Lament, avocat de M. A,<br/>
<br/>
              - les conclusions de Mme Claire Landais, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à Me Georges, avocat de la commune de Mandelieu-la-Napoule et à la SCP Potier de la Varde, Buk Lament, avocat de M. A ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article R. 111-4 du code de l'urbanisme, dans sa rédaction applicable au litige : " Le permis de construire peut être refusé sur des terrains qui ne seraient pas desservis par des voies publiques ou privées dans des conditions répondant à l'importance ou à la destination de l'immeuble ou de l'ensemble d'immeubles envisagé, et notamment si les caractéristiques de ces voies rendent difficile la circulation ou l'utilisation des engins de lutte contre l'incendie. " ; que si l'arrêt attaqué de la cour administrative d'appel de Marseille énonce que le terrain d'assiette du projet litigieux n'était pas desservi par une voie totalement ouverte à la circulation automobile, il constate également que l'accès était possible par les voies de la zone d'aménagement concerté voisine ; que par ce second constat, la cour n'a, contrairement à ce que soutient la commune requérante, pas entaché son arrêt d'une contradiction de motifs ; qu'elle n'a pas non plus dénaturé les faits de l'espèce en estimant que cet itinéraire de desserte répondait à l'importance et à la destination des quatre villas autorisées par le permis de construire litigieux ;<br/>
<br/>
              2. Considérant que l'article 2.3.2.2. du règlement du plan de prévention des risques naturels prévisibles d'incendie de forêts de la commune de Mandelieu-la-Napoule, applicable au secteur où est situé le projet litigieux, prévoit notamment dans le cas d'un permis de construire groupé, une " densité minimale de quatre bâtiments à l'hectare sur le territoire concerné par le projet " ; que, compte tenu de l'objet de ce plan de prévention des risques naturels prévisibles d'incendie de forêt et de l'ensemble des dispositions de ce même article 2.3.2.2., qui visent à prévenir les difficultés qui résulteraient, pour la mise en oeuvre des secours, de la construction d'habitations isolées, les dispositions citées ci-dessus doivent être regardées comme visant à garantir une certaine densité de construction, non par rapport à la surface des parcelles d'assiette, mais par rapport à la surface occupée par l'ensemble des bâtiments faisant l'objet du permis de construire groupé ;<br/>
<br/>
              3. Considérant qu'il résulte de ce qui précède que, en se fondant sur la circonstance que les quatre villas autorisées par le permis attaqué occupaient une superficie de moins de un hectare pour en déduire que les dispositions de l'article 2.3.2.2. du plan de prévention des risques naturels n'avaient pas été méconnues, la cour n'a pas entaché son arrêt d'erreur de droit ;<br/>
<br/>
              4. Considérant qu'il résulte également de ce qui précède que la circonstance que la partie constructible du terrain d'assiette du projet serait d'une superficie de 1,8 hectare, et non d'environ 1,1 hectare ainsi que l'a affirmé la cour, est sans incidence sur le " territoire concerné par le projet " au sens des dispositions citées ci-dessus de l'article 2.3.2.2. du règlement du plan de prévention des risques naturels prévisibles d'incendie de forêts de la commune de Mandelieu-la-Napoule ; que cette inexactitude matérielle est par suite, à la supposer vérifiée, sans incidence sur le bien-fondé de l'arrêt ;<br/>
<br/>
              5. Considérant, enfin, que la cour n'a pas dénaturé les pièces du dossier en estimant que le projet litigieux ne portait pas atteinte au site du Vallon de la Vernède ;<br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que les conclusions de la commune de Mandelieu-la-Napoule doivent être rejetées, y compris celles présentées au titre de l'article L. 761-1 du code de justice administrative ; qu'il y a lieu, en revanche, de mettre à la charge de la commune de Mandelieu-la-Napoule le versement à M. A de la somme de 3 000 euros sur le fondement de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la commune de Mandelieu-la-Napoule est rejeté.<br/>
Article 2 : La commune de Mandelieu-la-Napoule versera à M. A une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la commune de Mandelieu-la-Napoule et à M. José Luis A.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
