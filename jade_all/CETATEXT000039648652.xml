<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039648652</ID>
<ANCIEN_ID>JG_L_2019_12_000000426547</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/64/86/CETATEXT000039648652.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 19/12/2019, 426547</TITRE>
<DATE_DEC>2019-12-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>426547</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>Mme Louise Cadin</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2019:426547.20191219</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              1° Sous le numéro 426547, par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés le 24 décembre 2018 et les 25 mars et 11 septembre 2019 au secrétariat du contentieux du Conseil d'Etat, la société RMC Découverte demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision du 3 octobre 2018 par laquelle le Conseil supérieur de l'audiovisuel (CSA) a prononcé à son encontre une sanction de 10 000 euros ;<br/>
<br/>
              2°) de mettre à la charge du Conseil supérieur de l'audiovisuel la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Sous le numéro 427412, par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 28 janvier, 19 avril et 22 novembre 2019 au secrétariat du contentieux du Conseil d'Etat, la société Télévision France 1 (TF 1) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la même décision du CSA en tant qu'elle limite le montant de la sanction prononcée à l'encontre de la société RMC Découverte à 10 000 euros ;<br/>
<br/>
              2°) d'enjoindre au CSA de statuer à nouveau sur le montant de la sanction ;<br/>
<br/>
              3°) de mettre à la charge du CSA la somme de 4 000 au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - la loi n ° 26-1067 du 30 septembre 1986 ;<br/>
              - le décret n° 90-66 du 17 janvier 1990 ;<br/>
              - la décision n° 88-248 DC du 17 janvier 1989 du Conseil constitutionnel ;<br/>
              - la décision n° 2000-433 DC du 27 juillet 2000 du Conseil constitutionnel ; <br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme B... A..., auditrice,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de la société RMC Découverte, à la SCP Baraduc, Duhamel, Rameix, avocat du Conseil supérieur de l'audiovisuel, au Cabinet Briard, avocat de la société Télévision France 1.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article 27 de la loi du 30 septembre 1986 relative à liberté de communication, applicable aux services de communication audiovisuelle utilisant la voie hertzienne : " (...) des décrets en Conseil d'Etat fixent les principes généraux définissant les obligations concernant : / (...) 2° La diffusion, en particulier aux heures de grande écoute, de proportions au moins égales à 60% d'oeuvres cinématographiques et audiovisuelles européennes et de proportions au moins égales à 40% d'oeuvres cinématographiques et audiovisuelles d'expression originale française ; (...) ". A ce titre, l'article 14 du décret du 17 janvier 1990, pris pour l'application de la loi n° 86-1067 du 30 septembre 1986 et fixant les principes généraux concernant la diffusion des oeuvres cinématographiques et audiovisuelles par les éditeurs de services de télévision dispose que, pour les éditeurs de services de télévision qui ne sont pas éditeurs de services de cinéma : " (...) sont considérées comme heures de grande écoute les heures comprises entre 18 heures et 23 heures ainsi que, le mercredi, les heures comprises entre 14 heures et 18 heures. Toutefois, pour les éditeurs de services diffusés par voie hertzienne terrestre en mode numérique, distribués par un réseau n'utilisant pas de fréquences assignées par le Conseil supérieur de l'audiovisuel ainsi que pour les programmes rediffusés des services de cinéma à programmation multiple, les conventions et cahiers des charges déterminent les heures de grande écoute en fonction de la nature de la programmation du service. (...) ". En application de ces dispositions, la convention conclue le 3 juillet 2012 entre le Conseil supérieur de l'audiovisuel (CSA) et la société RMC Découverte, éditrice du service de télévision par voie hertzienne du même nom, stipule, en son article 3-2-1, que : " L'éditeur réserve, dans le total du temps annuellement consacré à la diffusion d'oeuvres audiovisuelles, au moins 60 % à la diffusion d'oeuvres européennes et 40 % à la diffusion d'oeuvres d'expression originale française, au sens des articles 4, 5 et 6 du décret n° 90-66 du 17 janvier 1990 modifié relatif à la diffusion des oeuvres cinématographiques et audiovisuelles à la télévision. Toutefois, pour la diffusion d'oeuvres européennes, ces proportions sont fixées à au moins : - pour 2013, 50 % ; - pour 2014, 55 % ; - à partir de 2015, 60 %. Pour la diffusion d'oeuvres d'expression originale française, ces proportions sont fixées à au moins : - pour 2013, 33 % ; - pour 2014, 35 % ; - à partir de 2015, 40 %. Ces proportions doivent également être respectées aux heures de grande écoute. Ces heures sont celles qui sont comprises entre 15h00 et 23h00 tous les jours ".<br/>
<br/>
              2. Il résulte de l'instruction que le CSA a estimé que, pour l'année 2014, la société RMC Découverte n'avait pas respecté les obligations résultant des stipulations citées au point précédent et l'a, en conséquence, par une décision du 24 juin 2015, mise en demeure de s'y conformer pour l'avenir. Ayant estimé que cette société n'avait pas à nouveau, en 2016, respecté ses obligations de diffusion d'oeuvres audiovisuelles françaises et européennes aux heures de grande écoute, le CSA a, par une décision du 3 octobre 2018, prononcé à son encontre une sanction de 10 000 euros. La société RMC Découverte demande l'annulation de cette décision et, par une requête qu'il y a lieu de joindre pour statuer par une seule décision, la société Télévision France 1 (TF 1) en demande l'annulation en tant que son montant se limite à 10 000 euros.<br/>
<br/>
              3. Aux termes de l'article 42 de la loi du 30 septembre 1986 : " Les éditeurs et distributeurs de services de communication audiovisuelle et les opérateurs de réseaux satellitaires peuvent être mis en demeure de respecter les obligations qui leur sont imposées par les textes législatifs et réglementaires et par les principes définis aux articles 1er et 3-1. / Le Conseil supérieur de l'audiovisuel rend publiques ces mises en demeure. / Les organisations professionnelles et syndicales représentatives du secteur de la communication audiovisuelle, les organisations de défense de la liberté de l'information reconnues d'utilité publique en France, les offices publics des langues régionales et les associations concourant à la promotion des langues et cultures régionales, les associations familiales et les associations de défense des droits des femmes ainsi que les associations ayant dans leur objet social la défense des intérêts des téléspectateurs peuvent demander au Conseil supérieur de l'audiovisuel d'engager la procédure de mise en demeure prévue au premier alinéa du présent article ". Aux termes de son article 42-1 : " Si la personne faisant l'objet de la mise en demeure ne se conforme pas à celle-ci, le Conseil supérieur de l'audiovisuel peut prononcer à son encontre, compte tenu de la gravité du manquement, et à la condition que celui-ci repose sur des faits distincts ou couvre une période distincte de ceux ayant déjà fait l'objet d'une mise en demeure, une des sanctions suivantes : (...) 3° Une sanction pécuniaire assortie éventuellement d'une suspension de l'édition ou de la distribution du ou des services ou d'une partie du programme ; (...) ".<br/>
<br/>
              Sur la requête de la société TF 1 :<br/>
<br/>
              4. La société requérante soutient que la méconnaissance, par la société RMC Découverte, de ses obligations de diffusion d'oeuvres audiovisuelles françaises et européennes aux heures de grande écoute, porte atteinte à ses intérêts.  Toutefois cette circonstance ne lui confère pas un intérêt lui donnant qualité pour demander l'annulation d'une sanction infligée sur le fondement de l'article 42-1 de la même loi, en tant que cette sanction serait insuffisante.  Le CSA et la société RMC Découverte sont, par suite, fondés à soutenir que la requête de la société TF1 n'est pas recevable et doit être rejetée.<br/>
<br/>
              Sur la requête de la société RMC Découverte :<br/>
<br/>
              En ce qui concerne la légalité externe :<br/>
<br/>
              5. Contrairement à ce que soutient la société RMC Découverte, le CSA n'est pas tenu, lorsqu'il fait usage des pouvoirs qui lui sont conférés par l'article 42-1 de la loi du 30 septembre 1986 cité au point 3, de répondre, dans sa décision, aux arguments développés au cours de la procédure contradictoire par la personne qui fait l'objet de la sanction. La décision attaquée, qui énonce les motifs pour lesquels le CSA retient l'existence d'un manquement ainsi que la sanction qu'il inflige, est, par suite, suffisamment motivée.<br/>
<br/>
              En ce qui concerne la légalité interne :<br/>
<br/>
              6. Il résulte de l'instruction et n'est d'ailleurs pas contesté qu'en 2016 la part consacrée par le service RMC Découverte, aux heures de grande écoute fixées par la convention du 3 juillet 2012 " entre 15h00 et 23h00 tous les jours ", à la diffusion d'oeuvres audiovisuelles européennes, s'est élevée à 27,7% au lieu des 60% fixées par la même convention et que la part des oeuvres audiovisuelles d'expression originale française s'est élevé à 9,1% au lieu des 40% fixées par la convention.<br/>
<br/>
              7. En premier lieu, si la société requérante excipe de l'illégalité des stipulations fixant les heures de grande écoute entre 15h00 et 23h00 tous les jours, il résulte de l'instruction, sans qu'il soit besoin de statuer sur la recevabilité de ce moyen, que cette plage horaire n'est, au regard des courbes d'audience du service RMC Découverte et de la nature de sa programmation, qui doit, en vertu de l'article 3-1-1 de la même convention du 3 juillet 2012, être composée à 75% de documentaires, pas entachée d'erreur manifeste d'appréciation.<br/>
<br/>
              8. En deuxième lieu, la société RMC Découverte ne peut utilement exciper de l'illégalité du refus de modification des heures de grande écoute qui lui a été opposé par le CSA, ce refus ne constituant pas la base légale de la sanction litigieuse, laquelle n'a pas non plus été prise pour son application.<br/>
<br/>
              9. En troisième lieu, s'il résulte de l'instruction que, par un avenant du 5 décembre 2018, les heures de grande écoute du service RMC Découverte ont été fixées, à compter de l'année 2019, de 8h30 à midi et de 18h à minuit, cette circonstance est par elle-même sans incidence sur l'appréciation qu'il incombait au CSA de porter, pour 2016, sur le respect par la société RMC Découverte des obligations qui lui étaient faites, pour cette même année, au vu de ce qu'était alors la définition conventionnelle des heures de grande écoute. La société RMC Découverte n'est, pour la même raison, sans qu'il soit besoin de statuer sur la recevabilité du moyen, pas fondée à soutenir que la sanction attaquée, au demeurant antérieure à cet avenant, aurait, en se fondant sur les heures de grande écoute applicables en 2016, méconnu le principe d'application immédiate de la loi répressive moins sévère.<br/>
<br/>
              10. En quatrième lieu, contrairement à ce que soutient la société requérante, le CSA n'était pas tenu, avant de prendre la décision litigieuse, de statuer sur ses demandes tendant à la modification, dans la convention du 3 juillet 2012, des tranches horaires correspondant aux heures de grande écoute.<br/>
<br/>
              11. Enfin, eu égard à l'ampleur des manquements constatés, la société requérante n'est pas fondée à soutenir que le montant de la sanction, qui ne revêt pas de caractère automatique, n'est pas proportionné à leur gravité.<br/>
<br/>
              12. Il résulte de tout ce qui précède que la société RMC Découverte n'est pas fondée à demander l'annulation de la décision qu'elle attaque. Sa requête doit, par suite, être rejetée y compris, par voie de conséquence, ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              13. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société RMC Découverte une somme de 3 000 euros à verser au CSA au titre de l'article L.761-1 du code de justice administrative. Il y a également lieu, au titre des mêmes dispositions, de mettre à la charge de la société TF 1 une somme de 3000 euros à verser au CSA et une somme de 3 000 euros à verser à la société RMC Découverte.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Les requêtes des sociétés RMC Découverte et Télévision France 1 sont rejetées.<br/>
<br/>
Article 2 : La société RMC Découverte versera au Conseil supérieur de l'audiovisuel la somme de 3 000 euros au titre de l'article L.761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La société Télévision France 1 versera au Conseil supérieur de l'audiovisuel et à la société RMC Découverte la somme de 3 000 euros chacun au titre de l'article L.761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la société RMC Découverte, à la société Télévision France 1 et au Conseil supérieur de l'audiovisuel.<br/>
		Copie en sera adressée au ministre de la culture.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-03-01-02-02-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. QUESTIONS GÉNÉRALES. MOTIVATION. MOTIVATION SUFFISANTE. EXISTENCE. - DÉCISION DE SANCTION ÉNONÇANT LES MOTIFS POUR LESQUELS LE CSA RETIENT L'EXISTENCE D'UN MANQUEMENT AINSI QUE LA SANCTION QU'IL INFLIGE, ALORS MÊME QU'ELLE NE RÉPOND PAS AUX ARGUMENTS DÉVELOPPÉS AU COURS DE LA PROCÉDURE CONTRADICTOIRE PAR LA PERSONNE QUI FAIT L'OBJET DE LA SANCTION [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">52-045 POUVOIRS PUBLICS ET AUTORITÉS INDÉPENDANTES. AUTORITÉS ADMINISTRATIVES INDÉPENDANTES. - CSA - CONTESTATION PAR UNE SOCIÉTÉ ÉDITITRICE DE SERVICES DE TÉLÉVISION D'UNE SANCTION INFLIGÉE À UNE AUTRE SOCIÉTÉ EN TANT QUE CETTE SANCTION SERAIT INSUFFISANTE - INTÉRÊT POUR AGIR - ABSENCE [RJ2].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-01-04-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. INTÉRÊT POUR AGIR. ABSENCE D'INTÉRÊT. - CONTESTATION PAR UNE SOCIÉTÉ ÉDITITRICE DE SERVICES DE TÉLÉVISION D'UNE SANCTION INFLIGÉE PAR LE CSA À UNE AUTRE SOCIÉTÉ EN TANT QUE CETTE SANCTION SERAIT INSUFFISANTE [RJ2].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">56-01 RADIO ET TÉLÉVISION. CONSEIL SUPÉRIEUR DE L'AUDIOVISUEL. - CONTESTATION PAR UNE SOCIÉTÉ ÉDITITRICE DE SERVICES DE TÉLÉVISION D'UNE SANCTION INFLIGÉE PAR LE CSA À UNE AUTRE SOCIÉTÉ EN TANT QUE CETTE SANCTION SERAIT INSUFFISANTE - INTÉRÊT POUR AGIR - ABSENCE [RJ2].
</SCT>
<ANA ID="9A"> 01-03-01-02-02-02 Le Conseil supérieur de l'audiovisuel (CSA) n'est pas tenu, lorsqu'il fait usage des pouvoirs qui lui sont conférés par l'article 42-1 de la loi n° 86-1067 du 30 septembre 1986, de répondre, dans sa décision, aux arguments développés au cours de la procédure contradictoire par la personne qui fait l'objet de la sanction. La décision attaquée, qui énonce les motifs pour lesquels le CSA retient l'existence d'un manquement ainsi que la sanction qu'il inflige, est, par suite, suffisamment motivée.</ANA>
<ANA ID="9B"> 52-045 Société éditrice de services de télévision soutenant que la méconnaissance, par une autre société, de ses obligations de diffusion d'oeuvres audiovisuelles françaises et européennes aux heures de grande écoute, porte atteinte à ses intérêts.,,,Cette circonstance ne lui confère pas un intérêt lui donnant qualité pour demander l'annulation d'une sanction infligée par le Conseil supérieur de l'audiovisuel (CSA) sur le fondement de l'article 42-1 de la loi n° 86-1067 du 30 septembre 1986, en tant que cette sanction serait insuffisante.</ANA>
<ANA ID="9C"> 54-01-04-01 Société éditrice de services de télévision soutenant que la méconnaissance, par une autre société, de ses obligations de diffusion d'oeuvres audiovisuelles françaises et européennes aux heures de grande écoute, porte atteinte à ses intérêts.,,,Cette circonstance ne lui confère pas un intérêt lui donnant qualité pour demander l'annulation d'une sanction infligée par le Conseil supérieur de l'audiovisuel (CSA) sur le fondement de l'article 42-1 de la loi n° 86-1067 du 30 septembre 1986, en tant que cette sanction serait insuffisante.</ANA>
<ANA ID="9D"> 56-01 Société éditrice de services de télévision soutenant que la méconnaissance, par une autre société, de ses obligations de diffusion d'oeuvres audiovisuelles françaises et européennes aux heures de grande écoute, porte atteinte à ses intérêts.,,,Cette circonstance ne lui confère pas un intérêt lui donnant qualité pour demander l'annulation d'une sanction infligée par le Conseil supérieur de l'audiovisuel (CSA) sur le fondement de l'article 42-1 de la loi n° 86-1067 du 30 septembre 1986, en tant que cette sanction serait insuffisante.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., s'agissant d'une décision de la commission des sanctions de l'AMF, CE, 11 février 2011, Société Générale, n° 316508, p. 39.,,[RJ2] Cf., s'agissant de la décision d'une AAI prise à l'issue de l'engagement de la procédure de sanction, CE, 21 juin 2018, M.,, n° 416505, T. p. 695-819. Comp., s'agissant de l'intérêt à agir contre un refus du CSA de mettre en demeure, CE, 7 février 2017, M.,, aux Tables sur un autre point ; s'agissant du refus d'une AAI d'engager une procédure disciplinaire, CE, Section, 30 novembre 2007,,et autres, n° 293952, p. 459 ; CE, 4 juillet 2012, Association française des opérateurs de réseaux et services de télécommunications, n°s 334062 347163, T. p. 887 ; s'agissant du refus d'une AAI de mettre en oeuvre ses pouvoirs de contrôle et de police pour assurer la sécurité d'un marché, CE, 9 octobre 2013, Selafa MJA, n° 359161, T. pp. 471-741-746.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
