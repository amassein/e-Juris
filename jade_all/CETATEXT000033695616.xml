<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033695616</ID>
<ANCIEN_ID>JG_L_2016_12_000000403197</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/69/56/CETATEXT000033695616.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 23/12/2016, 403197, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-12-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>403197</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR>M. Marc Firoud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:403197.20161223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C...A...a demandé au tribunal administratif de Paris d'ordonner une expertise pour déterminer notamment la nature et la cause des désordres affectant le logement qui lui a été attribué par le ministre de la défense sur le site de Balard. Par une ordonnance n° 153409 du 18 février 2016, le juge des référés du tribunal administratif de Paris a fait droit à sa demande. <br/>
<br/>
              Par un arrêt n°s 16PA00944, 16PA00966 du 4 juillet 2016, la cour administrative d'appel de Paris a, sur requête du ministre de la défense, annulé l'ordonnance du juge des référés du tribunal administratif de Paris et rejeté la demande de M.A....<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 5 septembre, 20 septembre et 2 décembre 2016 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu : <br/>
              - le code général de la propriété des personnes publiques ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Firoud, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public. <br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de M. A...;<br/>
<br/>
<br/>
<br/>1  Considérant qu'aux termes de l'article L. 511-1 du code de justice administrative: " Le juge des référés statue par des mesures qui présentent un caractère provisoire. Il n'est pas saisi du principal et se prononce dans les meilleurs délais " ; qu'aux termes de l'article R. 532-1 du même code : " Le juge des référés peut, sur simple requête et même en l'absence de décision administrative préalable, prescrire toute mesure utile d'expertise ou d'instruction " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M.A..., qui bénéficie d'un logement situé dans un immeuble relevant du ministre de la défense, a saisi le juge des référés du tribunal administratif de Paris d'une demande tendant à ce que soit prescrite une expertise en vue de déterminer la nature et les causes des désordres acoustiques qui affectent selon lui ce logement, ainsi que les moyens d'y remédier ; qu'il se pourvoit en cassation contre l'arrêt par lequel la cour administrative d'appel de Paris a annulé l'ordonnance du juge des référés qui avait fait droit à sa demande et rejeté celle-ci ;<br/>
<br/>
              3. Considérant que si le juge des référés, statuant en application de l'article R. 532-1 du code de justice administrative, ne peut faire droit à une demande d'expertise lorsqu'elle est formulée à l'appui de prétentions qui ne relèvent manifestement pas de la compétence de la juridiction administrative, qui sont irrecevables ou qui se heurtent à la prescription, il ne lui appartient pas, en revanche, d'apprécier leur chance de succès ; qu'ainsi, en rejetant la demande d'expertise présentée par M. A...au motif qu'aucun recours contentieux ultérieur, notamment indemnitaire, ne pourrait être engagé avec succès, la cour a entaché son arrêt d'erreur de droit ;<br/>
<br/>
              4. Considérant, en outre, que la seule circonstance que le ministre de la défense n'ait pas été tenu d'attribuer un logement à M. A...ne faisait pas obstacle à ce que la responsabilité de l'Etat pût être engagée en raison des désordres l'affectant ; qu'ainsi, en jugeant que l'expertise demandée par M. A...n'était pas utile au motif qu'il ne justifiait d'aucun droit à se voir attribuer un logement par le ministre de la défense, que son maintien dans ce logement résultait de son choix et qu'il n'y était contraint par aucune obligation de service, la cour administrative d'appel de Paris a également entaché son arrêt d'une erreur de droit ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que M. A...est fondé, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, à demander l'annulation de l'arrêt attaqué ; <br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à M. A...d'une somme de 3 000 euros au titre des frais exposés par lui et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 4 juillet 2016 est annulé.<br/>
Article 2 : L'affaire est renvoyée devant la cour administrative d'appel de Paris.<br/>
Article 3 : L'Etat versera à M. A...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. C...A...et au ministre de la défense.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
