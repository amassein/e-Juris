<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044237293</ID>
<ANCIEN_ID>JG_L_2021_10_000000448043</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/23/72/CETATEXT000044237293.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 20/10/2021, 448043, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-10-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>448043</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ZRIBI, TEXIER</AVOCATS>
<RAPPORTEUR>M. Bertrand Mathieu</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Philippe Ranquet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:448043.20211020</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés les 21 décembre 2020 et 22 mars 2021 au secrétariat du contentieux du Conseil d'Etat, M. C... A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du 19 octobre 2020 rapportant le décret du 1er août 2016 lui ayant accordé la nationalité française ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - le code civil ;<br/>
              - le décret n° 93-1362 du 30 décembre 1993 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bertrand Mathieu, conseiller d'Etat en service extraordinaire,<br/>
<br/>
              - les conclusions de M. Philippe Ranquet, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Zribi, Texier, avocat de M. A... ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1.	Aux termes de l'article 27-2 du code civil : " Les décrets portant acquisition, naturalisation ou réintégration peuvent être rapportés sur avis conforme du Conseil d'Etat dans le délai de deux ans à compter de leur publication au Journal officiel si le requérant ne satisfait pas aux conditions légales ; si la décision a été obtenue par mensonge ou fraude, ces décrets peuvent être rapportés dans le délai de deux ans à partir de la découverte de la fraude ".<br/>
<br/>
              2.	Il ressort des pièces du dossier que M. A..., ressortissant sénégalais, a déposé une demande de naturalisation le 16 octobre 2015, par laquelle il a indiqué être célibataire et sans enfant. Au vu de ses déclarations, il a été naturalisé par décret du 1er août 2016, publié au Journal officiel de la République française du 3 août 2016. Toutefois, le consulat général de France à Dakar a informé le ministre chargé des naturalisations que M. A... était le père de six enfants mineurs, tous nés au Sénégal, dont cinq avant sa naturalisation, les 30 novembre 2003, 20 août 2006, 20 septembre 2009, 28 janvier 2012 et 4 novembre 2014, tous résidant habituellement au Sénégal avec leur mère, Mme B.... Par décret du 19 octobre 2020, le Premier ministre a rapporté le décret du 1er août 2016 prononçant la naturalisation de M. A... au motif qu'il avait été pris au vu d'informations mensongères délivrées par l'intéressé quant à sa situation familiale. M. A... demande l'annulation pour excès de pouvoir de ce décret. <br/>
<br/>
              3.	En premier lieu, il ressort des visas du décret attaqué que les observations en défense de M. A... produites le 10 avril 2020 ont bien été portées à la connaissance de la section de l'intérieur du Conseil d'Etat avant que celle-ci rende son avis le 29 septembre 2020. Dès lors, le moyen selon lequel la procédure suivie aurait été irrégulière doit être écarté.<br/>
<br/>
              4.	En deuxième lieu, le délai de deux ans prévu à l'article 27-2 du code civil pour rapporter le décret de M. A... a commencé à courir à la date à laquelle la réalité de la situation familiale de l'intéressé a été portée à la connaissance du ministre chargé des naturalisations. A cet égard, il ressort des pièces du dossier que les services du ministre chargé des naturalisations n'ont été informés de la réalité de la situation familiale du requérant que le 19 octobre 2018, date à laquelle ils ont reçu les documents relatifs à l'existence des enfants mineurs de l'intéressé nés avant le dépôt de sa demande de naturalisation, transmis par bordereau du consulat général de France à Dakar, ainsi que l'atteste le tampon apposé sur le bordereau reçu par le service des naturalisations. Dans ces conditions, le décret attaqué, signé le 19 octobre 2020, a été pris avant l'expiration du délai de deux ans prévu par les dispositions de l'article 27-2 du code civil.<br/>
<br/>
              5.	En troisième lieu, l'article 21-6 du code civil dispose que : " Nul ne peut être naturalisé s'il n'a en France sa résidence au moment de la signature du décret de naturalisation ". Il résulte de ces dispositions que la demande de naturalisation n'est pas recevable lorsque l'intéressé n'a pas fixé en France de manière durable le centre de ses intérêts. Pour apprécier si cette dernière condition se trouve remplie, l'autorité administrative peut notamment prendre en compte, sous le contrôle du juge de l'excès de pouvoir, la situation personnelle et familiale en France de l'intéressé à la date du décret lui accordant la nationalité française. Par suite, alors même qu'il remplirait les autres conditions requises à l'obtention de la nationalité française, la circonstance que l'intéressé soit le père de cinq enfants nés avant sa naturalisation et résidant habituellement à l'étranger, était de nature à modifier l'appréciation qui a été portée par l'autorité administrative sur la fixation du centre de ses intérêts.<br/>
<br/>
              6.	Il ressort des pièces du dossier que M. A..., qui ne conteste pas être le père de cinq enfants mineurs, nés antérieurement au dépôt de sa demande et résidant au Sénégal avec leur mère, a déclaré sur l'honneur être célibataire et sans enfant dans la demande qu'il a déposée le 16 octobre 2015 et a indiqué, dans l'entretien d'assimilation du jour même, que les seuls liens l'attachant au Sénégal étaient ses oncles, tantes, cousins et cousines. Si M. A... soutient que le lien de filiation avec ses enfants n'était pas juridiquement établi au moment du dépôt de sa demande de naturalisation, il ne conteste ni être l'auteur de la déclaration de naissance de trois d'entre eux, nés en 2003, 2006 et 2009, ni qu'en vertu des dispositions du code de la famille D..., en l'absence de déclaration de naissance des deux autres par lui-même, ces enfants sont nés du mariage célébré entre M. A... et Mme B..., ni enfin que si la reconnaissance était postérieure à l'acquisition de nationalité, les actes de naissance porteraient mention d'un jugement conformément aux dispositions du même code. L'intéressé, qui maîtrise la langue française ainsi qu'il ressort du procès-verbal d'assimilation du 16 octobre 2015, ne pouvait se méprendre ni sur la teneur des indications devant être portées à la connaissance de l'administration chargée d'instruire sa demande, ni sur la portée de la déclaration sur l'honneur qu'il a signée. Dans ces conditions, M. A... doit être regardé comme ayant sciemment dissimulé sa situation familiale. Par suite, le Premier ministre n'a pas fait une inexacte application des dispositions de l'article 27-2 du code civil en rapportant sa naturalisation, dans le délai de deux ans à compter de la découverte de la fraude.<br/>
<br/>
              7.	En quatrième lieu, un décret qui rapporte un décret ayant conféré la nationalité française est, par lui-même, dépourvu d'effet sur la présence sur le territoire français de celui qu'il vise, comme sur ses liens avec les membres de sa famille, et n'affecte pas, dès lors, le droit au respect de sa vie familiale. En revanche, un tel décret affecte un élément constitutif de l'identité de la personne concernée et est ainsi susceptible de porter atteinte au droit au respect de sa vie privée. En l'espèce, toutefois, eu égard à la date à laquelle il est intervenu et aux motifs qui le fondent, le décret attaqué ne peut être regardé comme ayant porté une atteinte disproportionnée au droit au respect de la vie privée de M. A... garanti par l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales.<br/>
<br/>
              8.	En dernier lieu, la définition des conditions d'acquisition et de perte de la nationalité relève de la compétence de chaque État membre de l'Union européenne. Toutefois, dans la mesure où la perte de la nationalité d'un Etat membre a pour conséquence la perte du statut de citoyen de l'Union, la perte de la nationalité d'un Etat membre doit, pour être conforme au droit de l'Union, répondre à des motifs d'intérêt général et être proportionnée à la gravité des faits qui la fondent, au délai écoulé depuis l'acquisition de la nationalité et à la possibilité pour l'intéressé de recouvrer une autre nationalité. L'article 27-2 du code civil permet de rapporter, dans un délai de deux ans à compter de la découverte de la fraude, un décret qui a conféré la nationalité française au motif que l'intéressé a obtenu la nationalité française par mensonge ou fraude. Ces dispositions, qui ne sont pas incompatibles avec les exigences résultant du droit de l'Union, permettaient en l'espèce au Premier ministre, eu égard à la date à laquelle il est intervenu et aux motifs qui le fondent, de rapporter légalement le décret accordant à M. A... la nationalité française, dont il n'est ni soutenu, ni a fortiori établi qu'il aurait perdu la nationalité sénégalaise. <br/>
<br/>
              9.	Il résulte de ce qui précède que M. A... n'est pas fondé à demander l'annulation pour excès de pouvoir du décret du 19 octobre 2020 lequel le Premier ministre a rapporté le décret du 1er août 2016. Ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, en conséquence, qu'être rejetées. <br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de M. A... est rejetée. <br/>
Article 2 : La présente décision sera notifiée à M. C... A... et au ministre de l'intérieur. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
