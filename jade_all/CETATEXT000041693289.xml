<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041693289</ID>
<ANCIEN_ID>JG_L_2020_03_000000428695</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/69/32/CETATEXT000041693289.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 05/03/2020, 428695</TITRE>
<DATE_DEC>2020-03-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428695</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Nicolas Agnoux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:428695.20200305</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société civile immobilière (SCI) F Banny a demandé au tribunal administratif de Clermont-Ferrand de prononcer la décharge de la cotisation supplémentaire de taxe foncière sur les propriétés bâties à laquelle elle a été assujettie au titre de l'année 2015 dans les rôles de la commune de Clermont-Ferrand (Puy-de-Dôme) à raison de locaux situés 27, rue des Ronzières.<br/>
<br/>
              Par un jugement n° 1701337 du 31 décembre 2018, le tribunal administratif de Clermont-Ferrand a rejeté cette demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 8 mars et 9 mai 2019 au secrétariat du contentieux du Conseil d'Etat, la SCI F Banny demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution du 4 octobre 1958, notamment son Préambule et son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Nicolas Agnoux, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat de la Société F Banny ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que la société civile immobilière (SCI) F Banny est propriétaire d'un ensemble immobilier situé 27, rue des Ronzières sur le territoire de la commune de Clermont-Ferrand, donné en location à la société Colorteam qui exerce une activité d'imprimerie. La société requérante a fait l'objet d'une vérification de comptabilité sur la période du 1er juillet 2012 au 30 juin 2015, à l'issue de laquelle l'administration a procédé à une nouvelle évaluation de la valeur locative de ces locaux selon la méthode comptable prévue à l'article 1499 du code général des impôts et a notifié en conséquence à la société requérante une cotisation supplémentaire de taxe foncière sur les propriétés bâties au titre de l'année 2015. La SCI F Banny a demandé au tribunal administratif de Clermont-Ferrand de prononcer la décharge de cette imposition. Elle a soulevé devant le tribunal, par un mémoire distinct, la question de la conformité à la Constitution de l'article 1499 du code général des impôts. Par une ordonnance du 11 décembre 2018, la présidente de la 1ère chambre du tribunal a refusé de transmettre la question prioritaire de constitutionnalité. Puis, par un jugement du 31 décembre 2018, le tribunal a rejeté la demande en décharge présentée par la SCI F Banny. Cette dernière se pourvoit en cassation contre ce jugement et conteste, par un mémoire distinct, le refus de transmission de la question prioritaire de constitutionnalité.<br/>
<br/>
              2. Aux termes de l'article 1388 du code général des impôts : " La taxe foncière sur les propriétés bâties est établie d'après la valeur locative cadastrale de ces propriétés déterminée conformément aux principes définis par les articles 1494 à 1508 et 1516 à 1518 B et sous déduction de 50 % de son montant en considération des frais de gestion, d'assurances, d'amortissement, d'entretien et de réparation ". L'article 1495 du même code, dans sa rédaction applicable à la date du fait générateur de l'imposition contestée, précise que " Chaque propriété ou fraction de propriété est appréciée d'après sa consistance, son affectation, sa situation et son état, à la date de l'évaluation ". Aux termes de l'article 1498 du même code dans sa rédaction alors applicable : " La valeur locative de tous les biens autres que les locaux visés au I de l'article 1496 et que les établissements industriels visés à l'article 1499 est déterminée au moyen de l'une des méthodes indiquées ci-après : / 1° Pour les biens donnés en location à des conditions de prix normales, la valeur locative est celle qui ressort de cette location ; / 2° a. Pour les biens loués à des conditions de prix anormales ou occupés par leur propriétaire (...), la valeur locative est déterminée par comparaison. (...) / 3° A défaut de ces bases, la valeur locative est déterminée par voie d'appréciation directe ". Aux termes de son article 1499 : " La valeur locative des immobilisations industrielles passibles de la taxe foncière sur les propriétés bâties est déterminée en appliquant au prix de revient de leurs différents éléments, revalorisé à l'aide des coefficients qui avaient été prévus pour la révision des bilans, des taux d'intérêt fixés par décret en Conseil d'Etat (...) ". Enfin, aux termes de l'article 1500 du même code dans sa rédaction alors applicable : " Les bâtiments et terrains industriels sont évalués : / 1° selon les règles fixées à l'article 1499 lorsqu'ils figurent à l'actif du bilan de leur propriétaire ou de leur exploitant, et que celui-ci est soumis aux obligations définies à l'article 53 A ; / 2° selon les règles fixées à l'article 1498 lorsque les conditions prévues au 1° ne sont pas satisfaites ". Revêtent un caractère industriel, au sens de ces articles, les établissements dont l'activité nécessite d'importants moyens techniques, non seulement lorsque cette activité consiste dans la fabrication ou la transformation de biens corporels mobiliers, mais aussi lorsque le rôle des installations techniques, matériels et outillages mis en oeuvre, fût-ce pour les besoins d'une autre activité, est prépondérant.<br/>
<br/>
              Sur le refus de transmission de la question prioritaire de constitutionnalité soulevée devant le tribunal administratif :<br/>
<br/>
              3. Les dispositions de l'article 23-2 de l'ordonnance portant loi organique du 7 novembre 1958 prévoient que, lorsqu'une juridiction relevant du Conseil d'Etat est saisie de moyens contestant la conformité d'une disposition législative aux droits et libertés garantis par la Constitution, elle transmet au Conseil d'Etat la question de constitutionnalité ainsi posée à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et qu'elle ne soit pas dépourvue de caractère sérieux.<br/>
<br/>
              4. Devant le tribunal administratif de Clermont-Ferrand, la SCI F Banny soutenait que les dispositions de l'article 1499 du code général des impôts telles qu'interprétées par la jurisprudence méconnaissent le principe d'égalité devant la loi et le principe d'égalité devant les charges publiques, garantis par les articles 6 et 13 de la Déclaration des droits de l'homme et du citoyen, en tant qu'elles tiennent compte, pour déterminer la méthode à retenir pour évaluer la valeur locative de biens mis en location, de l'importance des moyens techniques que nécessite l'activité du locataire ou du caractère prépondérant des installations techniques, matériels et outillages mis en oeuvre par ce dernier.<br/>
<br/>
              5. Il ressort des termes de l'ordonnance du 11 décembre 2018 que, pour écarter ces deux griefs, la présidente de la 1ère chambre du tribunal a relevé que les dispositions contestées, éclairées par la jurisprudence, prenaient en compte la différence de situation des biens à évaluer au regard de l'objet de la loi, sans énoncer cet objet ni répondre au grief tiré de ce que ces critères ne seraient pas en rapport avec les facultés contributives du redevable. En statuant ainsi, la présidente de la première chambre du tribunal a insuffisamment motivé son ordonnance. Il s'ensuit que la SCI F Banny est fondée, sans qu'il soit besoin d'examiner l'autre moyen dirigé contre le refus de transmission de la question prioritaire de constitutionnalité, à en demander l'annulation.<br/>
<br/>
              6. Il y a lieu, en application des dispositions de l'article L. 821-2 du code de justice administrative, d'examiner la question prioritaire de constitutionnalité soulevée devant le tribunal administratif.<br/>
<br/>
              7. D'une part, le principe d'égalité ne s'oppose ni à ce que le législateur règle de façon différente des situations différentes, ni à ce qu'il déroge à l'égalité pour des raisons d'intérêt général, pourvu que dans l'un et l'autre cas, la différence de traitement qui en résulte soit en rapport direct avec l'objet de la loi qui l'établit. D'autre part, en vertu de l'article 34 de la Constitution, il appartient au législateur de déterminer, dans le respect des principes constitutionnels et compte tenu des caractéristiques de chaque impôt, les règles selon lesquelles doivent être appréciées les facultés contributives. En particulier, pour assurer le respect du principe d'égalité, il doit fonder son appréciation sur des critères objectifs et rationnels en fonction des buts qu'il se propose. Cette appréciation ne doit cependant pas entraîner de rupture caractérisée de l'égalité devant les charges publiques garantie par l'article 13 de la Déclaration du 26 août 1789.<br/>
<br/>
              8. Les dispositions contestées telles qu'interprétées par la jurisprudence ont pour objet de déterminer la valeur locative des locaux selon une méthode spécifique lorsqu'ils présentent un caractère industriel, apprécié au regard, d'une part, de l'importance des moyens techniques et, d'autre part, de la nature de l'activité ou du rôle des installations techniques, matériels et outillages mis en oeuvre au cours de l'année considérée. La différence de traitement qui en résulte entre des propriétaires mettant en location leur bien, selon la nature et les conditions dans lesquelles s'exerce l'activité du locataire au regard des éléments précités, repose sur des critères objectifs et rationnels en fonction des buts poursuivis par le législateur. En outre, il résulte de ces dispositions que la valeur locative des locaux est déterminée à partir du prix de revient des seules immobilisations dont le redevable est propriétaire, de sorte que la requérante n'est pas fondée à soutenir qu'il ne serait pas tenu compte des facultés contributives du redevable.<br/>
<br/>
              9. Il résulte de ce qui précède que la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux. Ainsi, il n'y a pas lieu de la renvoyer au Conseil constitutionnel.<br/>
<br/>
              Sur le pourvoi :<br/>
<br/>
              10. Aux termes de l'article R. 771-10 du code de justice administrative : " Le refus de transmission dessaisit la juridiction du moyen d'inconstitutionnalité. La décision qui règle le litige vise le refus de transmission. / La formation de jugement peut, toutefois, déclarer non avenu le refus de transmission et procéder à la transmission, lorsque ce refus a été exclusivement motivé par la constatation que la condition prévue par le 1° de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel n'était pas remplie, si elle entend fonder sa décision sur la disposition législative qui avait fait l'objet de la question qui n'a pas été transmise ".<br/>
<br/>
              11. L'ordonnance du 11 décembre 2018 par laquelle la présidente de la 1ère chambre du tribunal a refusé de transmettre la question prioritaire de constitutionnalité soulevée à l'appui de sa demande ne figure pas, contrairement aux prescriptions du premier alinéa de l'article R. 771-10 du code de justice administrative précité, dans les visas du jugement attaqué et n'est pas davantage mentionnée dans les motifs. Par suite, le jugement est irrégulier. Sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, la société F Banny est fondée à en demander l'annulation.<br/>
<br/>
              12. Il y a lieu, dans les circonstances de l'espèce, de faire application des dispositions de l'article L. 821-2 du code de justice administrative et de régler l'affaire au fond.<br/>
<br/>
              En ce qui concerne la loi fiscale :<br/>
<br/>
              13. Il résulte de l'instruction que la SCI F Banny est propriétaire de locaux d'une superficie de 2 000 mètres carrés, pris en location par la SARL Colorteam qui y exerce une activité d'imprimerie. Il résulte de ce qui a été dit au point 2 que, compte tenu du rôle prépondérant des installations techniques, matériels et outillages mis en oeuvre pour les besoins de cette activité, ces immobilisations revêtaient un caractère industriel au sens de l'article 1499 du code général des impôts et leur valeur locative devait être déterminée selon la méthode comptable définie à cet article.<br/>
<br/>
              En ce qui concerne l'interprétation administrative de la loi fiscale :<br/>
<br/>
              14. La SCI F Banny se prévaut, sur le fondement de l'article L. 80 A du livre des procédures fiscales, de l'instruction administrative figurant au bulletin officiel des finances publiques - impôts sous la référence BOI-IF-TFB-20-10-10-30 qui prévoit, aux termes des paragraphes 490 et 500 dans leur rédaction applicable au litige, que la catégorie de biens mentionnée à l'article 1498 du code général des impôts, constituée des locaux commerciaux et biens divers passibles de la taxe foncière sur les propriétés bâties, autres que les locaux d'habitation ou à usage professionnel ordinaires et les établissements industriels relevant de la méthode d'évaluation comptable, " comprend, d'une manière générale, toutes les propriétés ou fractions de propriété passibles de la taxe foncière sur les propriétés bâties qui ne sont ni des locaux d'habitation ou servant à l'exercice soit d'une activité salariée à domicile, soit d'une activité professionnelle non commerciale au sens du 1 de l'article 92 du CGI, ni des établissements industriels (CGI art. 1498). / Parmi ces biens doivent être compris notamment : / - les locaux appartenant à des sociétés civiles immobilières, lesquelles ne peuvent pas être considérées comme exerçant une véritable profession (...) / ".<br/>
<br/>
              15. Cette instruction administrative comporte, dans sa rédaction applicable au litige, une interprétation formelle du texte fiscal dont la société requérante peut se prévaloir sur le fondement de l'article L. 80 A du livre des procédures fiscales pour soutenir qu'en raison de sa qualité de société civile immobilière, les immeubles lui appartenant se rattachaient à la catégorie, définie à l'article 1498 du code général des impôts, des locaux commerciaux et biens divers passibles de la taxe foncière sur les propriétés bâties autres que les locaux d'habitation ou à usage professionnel ordinaires et les établissements industriels et ne pouvaient, par suite, être évalués au moyen de la méthode comptable prévue à l'article 1499 du même code.<br/>
<br/>
              16. Il résulte de tout ce qui précède, sans qu'il soit besoin d'examiner l'autre moyen soulevé par la société F Banny, que celle-ci est fondée à demander la décharge de la cotisation supplémentaire de taxe foncière sur les propriétés bâties à laquelle elle a été assujettie au titre de l'année 2015.<br/>
<br/>
              17. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la société F Banny au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er: L'ordonnance de la présidente de la 1ère chambre du tribunal administratif de Clermont-Ferrand du 11 décembre 2018 est annulée.<br/>
<br/>
Article 2 : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée devant le tribunal administratif de Clermont-Ferrand par la SCI F Banny.<br/>
<br/>
Article 3 : Le jugement du tribunal administratif de Clermont-Ferrand du 31 décembre 2018 est annulé.<br/>
<br/>
Article 4 : La société F Banny est déchargée de la cotisation supplémentaire de taxe foncière sur les propriétés bâties à laquelle elle a été assujettie au titre de l'année 2015.<br/>
<br/>
Article 5 : L'Etat versera à la société F Banny la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
Article 6 : La présente décision sera notifiée à la SCI F Banny et au ministre de l'action et des comptes publics.<br/>
Copie en sera adressée au Conseil constitutionnel et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-06-04-01 PROCÉDURE. JUGEMENTS. RÉDACTION DES JUGEMENTS. VISAS. - OBLIGATION DE VISER LE REFUS DE TRANSMISSION D'UNE QPC (ART. R. 771-10 DU CJA) - EXISTENCE À PEINE D'IRRÉGULARITÉ DU JUGEMENT.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-10 PROCÉDURE. - OBLIGATION DE VISER LE REFUS DE TRANSMISSION D'UNE QPC (ART. R. 771-10 DU CJA) - EXISTENCE À PEINE D'IRRÉGULARITÉ DU JUGEMENT.
</SCT>
<ANA ID="9A"> 54-06-04-01 L'article R. 771-10 du code de justice administrative (CJA) impose au juge du fond de viser le refus de transmission d'une question prioritaire de constitutionnalité (QPC) dans la décision qui règle le litige. Par suite, est irrégulier le jugement qui ne vise pas l'ordonnance refusant la transmission d'une QPC et qui ne la mentionne pas dans ses motifs.</ANA>
<ANA ID="9B"> 54-10 L'article R. 771-10 du code de justice administrative (CJA) impose au juge du fond de viser le refus de transmission d'une question prioritaire de constitutionnalité (QPC) dans la décision qui règle le litige. Par suite, est irrégulier le jugement qui ne vise pas l'ordonnance refusant la transmission d'une QPC et qui ne la mentionne pas dans ses motifs.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
