<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030189627</ID>
<ANCIEN_ID>JG_L_2015_01_000000382902</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/18/96/CETATEXT000030189627.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère / 6ème SSR, 21/01/2015, 382902, Publié au recueil Lebon</TITRE>
<DATE_DEC>2015-01-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>382902</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère / 6ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>M. Philippe Combettes</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:382902.20150121</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'EURL 2B a demandé au tribunal administratif de Versailles d'annuler pour excès de pouvoir la décision du 22 juillet 2009 par laquelle le maire d'Aigremont a refusé de lui accorder un permis de construire ainsi que la décision du 2 octobre 2009 rejetant son recours gracieux contre ce refus. Par un jugement n° 0911170 du 16 janvier 2012, le tribunal administratif de Versailles a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 12VE00971 du 29 avril 2014, la cour administrative d'appel de Versailles a rejeté l'appel formé par l'EURL 2B contre le jugement du tribunal administratif de Versailles.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 21 juillet et 22 octobre 2014 au secrétariat du contentieux du Conseil d'Etat, l'EURL 2B demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) de mettre à la charge de la commune d'Aigremont la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - la loi n° 2009-526 du 12 mai 2009, notamment son article 9 ;<br/>
              - l'article L. 111- 3 du code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Philippe Combettes, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de la Société EURL 2B et à la SCP Piwnica, Molinié, avocat de la commune d'Aigremont ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, le 11 février 2009, l'EURL 2B a déposé une demande de permis de construire tendant à la reconstruction à l'identique d'un bâtiment, situé sur le territoire de la commune d'Aigremont, détruit en 1996 par une tempête puis entièrement en 1998 par un incendie ; que, par un arrêté du 22 juillet 2009, le maire de cette commune a rejeté cette demande ; que, par l'arrêt attaqué du 29 avril 2014, la cour administrative d'appel de Versailles a rejeté l'appel formé par l'EURL 2B contre le jugement du 16 janvier 2012 par lequel le tribunal administratif de Versailles a rejeté sa demande tendant à l'annulation de la décision du maire ; qu'à l'appui de son pourvoi, elle soulève une question prioritaire de constitutionnalité ;<br/>
<br/>
              Sur la question prioritaire de constitutionnalité :<br/>
<br/>
              2. Considérant qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé, y compris pour la première fois en cassation, à l'occasion d'une instance devant le Conseil d'Etat (...) " ; qu'il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              3. Considérant que l'article L. 111-3 du code de l'urbanisme, dans sa rédaction issue de la loi du 13 décembre 2000 relative à la solidarité et au renouvellement urbains, disposait que : " La reconstruction à l'identique d'un bâtiment détruit par un sinistre est autorisée nonobstant toute disposition d'urbanisme contraire, sauf si la carte communale ou le plan local d'urbanisme en dispose autrement, dès lors qu'il a été régulièrement édifié " ; que la loi du 12 mai 2009 de simplification et de clarification du droit et d'allègement des procédures a modifié ces dispositions pour prévoir que : " La reconstruction à l'identique d'un bâtiment détruit ou démoli depuis moins de dix ans est autorisée nonobstant toute disposition d'urbanisme contraire, sauf si la carte communale ou le plan local d'urbanisme en dispose autrement, dès lors qu'il a été régulièrement édifié " ;<br/>
<br/>
              4. Considérant que, pour demander au Conseil d'Etat de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution de ces dispositions, l'EURL 2B soutient que si elles devaient être interprétées, ainsi que l'ont fait les juges du fond, comme limitant à dix ans la possibilité de reconstruction d'un bâtiment détruit, quelle que soit la date du sinistre à l'origine de cette destruction, elles méconnaîtraient alors le principe de sécurité juridique garanti par l'article 16 de la Déclaration des droits de l'homme et du citoyen et le principe d'égalité devant la loi garanti par l'article 6 de cette Déclaration ;<br/>
<br/>
              5. Considérant que lorsqu'une loi nouvelle institue, sans comporter de disposition spécifique relative à son entrée en vigueur, un délai de prescription d'un droit précédemment ouvert sans condition de délai, ce délai est immédiatement applicable mais ne peut, à peine de rétroactivité, courir qu'à compter de l'entrée en vigueur de la loi nouvelle ; que si, en adoptant les dispositions de la loi du 13 décembre 2000 insérées à l'article L. 111-3 du code de l'urbanisme, le législateur n'a pas entendu permettre aux propriétaires d'un bâtiment détruit de le reconstruire au-delà d'un délai raisonnable afin d'échapper à l'application des règles d'urbanisme devenues contraignantes, les modifications apportées à cet article par la loi du 12 mai 2009 ont notamment eu pour objet de créer expressément un délai ayant pour effet d'instituer une prescription extinctive du droit, initialement conféré par la loi du 13 décembre 2000 aux propriétaires d'un bâtiment détruit par un sinistre, de le reconstruire à l'identique ; qu'il résulte de ce qui précède que le délai qu'elle instaure n'a commencé à courir, dans tous les autres cas de destruction d'un bâtiment par un sinistre, qu'à compter de la date d'entrée en vigueur de la loi du 12 mai 2009 ; que, dès lors, la société requérante n'est pas fondée à soutenir que les dispositions de l'article L. 111-3 du code de l'urbanisme issues de cette loi, qui n'ont pas d'effet rétroactif, méconnaîtraient le principe de sécurité juridique ainsi que celui d'égalité devant la loi garantis par la Déclaration des droits de l'homme et du citoyen ;<br/>
<br/>
              6. Considérant que, par suite, la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux ; qu'ainsi, sans qu'il soit besoin de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée, le moyen tiré de ce que l'article L. 111-3 du code de l'urbanisme porte atteinte aux droits et libertés garantis par la Constitution doit être écarté ;<br/>
<br/>
              Sur les autres moyens du pourvoi :<br/>
<br/>
              7. Considérant qu'en jugeant que l'entrée en vigueur de la loi du 12 mai 2009 mentionnée ci-dessus, en tant qu'elle a modifié l'article L. 111-3 du code de l'urbanisme, a eu pour effet, dès la date de son entrée en vigueur, de limiter à dix ans la possibilité qu'elle autorise de reconstruction d'un bâtiment détruit " et ce quelle qu'ait été la date de destruction ", alors qu'il résulte de ce qui a été dit au point 5 que, pour les bâtiments dont les propriétaires auraient pu se prévaloir des dispositions de la loi du 13 décembre 2000, la prescription du droit à la reconstruction d'un bâtiment détruit par un sinistre antérieurement à la date d'entrée en vigueur de la loi nouvelle ne commence à courir qu'à compter de cette dernière date, la cour a entaché son arrêt d'une erreur de droit ; <br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que l'EURL 2B est fondée, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, à demander l'annulation de l'arrêt qu'elle attaque ;<br/>
<br/>
              9. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune d'Aigremont le versement d'une somme de 3 000 euros à l'EURL 2B au titre de l'article L. 761-1 du code de justice administrative ; que les dispositions de cet article font en revanche obstacle à ce qu'il soit fait droit aux conclusions de la commune présentées au même titre ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par l'EURL 2B.<br/>
Article 2 : L'arrêt de la cour administrative d'appel de Versailles du 29 avril 2014 est annulé.<br/>
Article 3 : L'affaire est renvoyée à la cour administrative d'appel de Versailles.<br/>
Article 4 : La commune d'Aigremont versera une somme de 3 000 euros à l'EURL 2B.<br/>
Article 5 : Les conclusions de la commune d'Aigremont présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 6 : La présente décision sera notifiée à l'EURL 2B, à la commune d'Aigremont et à la ministre du logement, de l'égalité des territoires et de la ruralité.<br/>
Copie en sera adressée au Conseil constitutionnel et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-08-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. APPLICATION DANS LE TEMPS. ENTRÉE EN VIGUEUR. - LOI CRÉANT UN DÉLAI DE PRESCRIPTION D'UN DROIT PRÉCÉDEMMENT OUVERT SANS CONDITION DE DÉLAI - DÉLAI NE POUVANT COURIR QU'À COMPTER DE L'ENTRÉE EN VIGUEUR DE LA LOI NOUVELLE - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">01-08-02-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. APPLICATION DANS LE TEMPS. RÉTROACTIVITÉ. ABSENCE DE RÉTROACTIVITÉ. - DISPOSITIONS DE LA LOI DU 12 MAI 2009 LIMITANT À DIX ANS À COMPTER DE LA DESTRUCTION D'UN IMMEUBLE LE DROIT DE LE RECONSTRUIRE À L'IDENTIQUE OUVERT AUX PROPRIÉTAIRES PAR LA LOI DU 13 DÉCEMBRE 2000 (ART. L. 111-3 DU CODE DE L'URBANISME) - APPLICATION IMMÉDIATE - EXISTENCE - LIMITE - DÉLAI NOUVEAU NE POUVANT COURIR QU'À COMPTER DE L'ENTRÉE EN VIGUEUR DE LA LOI, Y COMPRIS POUR LES BIENS DÉTRUITS ANTÉRIEUREMENT.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">68-001-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES GÉNÉRALES D'UTILISATION DU SOL. RÈGLES GÉNÉRALES DE L'URBANISME. - DROIT À LA RECONSTRUCTION À L'IDENTIQUE D'UN BÂTIMENT DÉTRUIT PAR UN SINISTRE (ART. L. 111-3 DU CODE DE L'URBANISME) - LIMITE - A) DANS L'ÉTAT DU DROIT ANTÉRIEUR À L'ENTRÉE EN VIGUEUR DE LA LOI N° 2009-526 DU 12 MAI 2009 - DROIT NE POUVANT ÊTRE EXERCÉ AU-DELÀ D'UN DÉLAI RAISONNABLE [RJ1] - B) APRÈS CETTE LOI - DROIT NE POUVANT ÊTRE EXERCÉ QUE DANS UN DÉLAI DE DIX ANS - APPLICATION DANS LE TEMPS - DÉLAI NOUVEAU NE POUVANT COURIR QU'À COMPTER DE L'ENTRÉE EN VIGUEUR DE LA LOI, Y COMPRIS POUR LES BIENS DÉTRUITS ANTÉRIEUREMENT - EXISTENCE.
</SCT>
<ANA ID="9A"> 01-08-01 Lorsqu'une loi nouvelle institue, sans comporter de disposition spécifique relative à son entrée en vigueur, un délai de prescription d'un droit précédemment ouvert sans condition de délai, ce délai est immédiatement applicable mais ne peut, à peine de rétroactivité, courir qu'à compter de l'entrée en vigueur de la loi nouvelle.</ANA>
<ANA ID="9B"> 01-08-02-03 Dispositions de l'article L. 111-3 du code de l'urbanisme issues de la loi n° 2000-1208 du 13 décembre 2000 donnant aux propriétaires d'un bâtiment détruit par un sinistre le droit de le reconstruire à l'identique, sans prévoir expressément de condition de délai. La loi n° 2009-526 du 12 mai 2009 a ajouté que cette disposition ne valait que pour les bâtiments détruits ou démolis  depuis moins de dix ans .... ,,Lorsqu'une loi nouvelle institue, sans comporter de disposition spécifique relative à son entrée en vigueur, un délai de prescription d'un droit précédemment ouvert sans condition de délai, ce délai est immédiatement applicable mais ne peut, à peine de rétroactivité, courir qu'à compter de l'entrée en vigueur de la loi nouvelle.... ,,Si, en adoptant les dispositions de la loi du 13 décembre 2000 insérées à l'article L. 111-3 du code de l'urbanisme, le législateur n'a pas entendu permettre aux propriétaires d'un bâtiment détruit de le reconstruire au-delà d'un délai raisonnable afin d'échapper à l'application des règles d'urbanisme devenues contraignantes, les modifications apportées à cet article par la loi du 12 mai 2009 ont notamment eu pour objet de créer expressément un délai ayant pour effet d'instituer une prescription extinctive du droit, initialement conféré par la loi du 13 décembre 2000 aux propriétaires d'un bâtiment détruit par un sinistre, de le reconstruire à l'identique. Ce délai n'a donc commencé à courir, dans tous les autres cas de destruction d'un bâtiment par un sinistre, qu'à compter de la date d'entrée en vigueur de la loi du 12 mai 2009. Les dispositions de l'article L. 111-3 du code de l'urbanisme issues de cette loi n'ont ainsi pas d'effet rétroactif.</ANA>
<ANA ID="9C"> 68-001-01 Dispositions de l'article L. 111-3 du code de l'urbanisme issues de la loi n° 2000-1208 du 13 décembre 2000 donnant aux propriétaires d'un bâtiment détruit par un sinistre le droit de le reconstruire à l'identique, sans prévoir expressément de condition de délai. La loi n° 2009-526 du 12 mai 2009 a ajouté que cette disposition ne valait que pour les bâtiments détruits ou démolis  depuis moins de dix ans .... ,,a) En adoptant les dispositions de la loi du 13 décembre 2000 insérées à l'article L. 111-3 du code de l'urbanisme, le législateur n'a pas entendu permettre aux propriétaires d'un bâtiment détruit de le reconstruire au-delà d'un délai raisonnable afin d'échapper à l'application des règles d'urbanisme devenues contraignantes.... ,,b) Lorsqu'une loi nouvelle institue, sans comporter de disposition spécifique relative à son entrée en vigueur, un délai de prescription d'un droit précédemment ouvert sans condition de délai, ce délai est immédiatement applicable mais ne peut, à peine de rétroactivité, courir qu'à compter de l'entrée en vigueur de la loi nouvelle. Les modifications apportées à cet article par la loi du 12 mai 2009 ont notamment eu pour objet de créer expressément un délai ayant pour effet d'instituer une prescription extinctive du droit, initialement conféré par la loi du 13 décembre 2000 aux propriétaires d'un bâtiment détruit par un sinistre, de le reconstruire à l'identique. Ce délai n'a donc commencé à courir, dans tous les autres cas de destruction d'un bâtiment par un sinistre, qu'à compter de la date d'entrée en vigueur de la loi du 12 mai 2009. Les dispositions de l'article L. 111-3 du code de l'urbanisme issues de cette loi n'ont ainsi pas d'effet rétroactif.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 9 mai 2012, Commune de Tomino et Commune de Meria, n° 341259, T. p. 1016.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
