<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028665162</ID>
<ANCIEN_ID>JG_L_2014_02_000000370850</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/66/51/CETATEXT000028665162.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème / 9ème SSR, 19/02/2014, 370850</TITRE>
<DATE_DEC>2014-02-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>370850</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème / 9ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Véronique Rigal</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:370850.20140219</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu 1°, sous le n° 370850, la requête, enregistrée le 3 août 2013 au secrétariat du contentieux du Conseil d'Etat, présentée par le haut-commissaire de la République en Polynésie française, 43, avenue Bruat - BP 115 à Papeete (98713) ; le haut-commissaire de la République en Polynésie française demande au Conseil d'Etat de déclarer l'ensemble des dispositions de la " loi du pays "  n° 2013-17 LP/APF relative au Haut conseil de la Polynésie française du 11 juillet 2013 contraires aux dispositions de l'article 74 de la Constitution et à celles de la loi organique n° 2004-192 du 27 février 2004 ;<br/>
<br/>
<br/>
              Vu 2°, sous le n° 371540, la requête, enregistrée le 22 août 2013 au secrétariat du contentieux du Conseil d'Etat, présentée pour M. B...D..., demeurant.PK 4800 côté Montagne Quartier Tavararo à Faaa (98704) ; M. D...demande au Conseil d'Etat de déclarer illégale la " loi du pays " n° 2013-17 LP/APF relative au Haut conseil de la Polynésie Française du 11 juillet 2013 ;<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 29 janvier 2004, présentée par le Président de la Polynésie française ;<br/>
<br/>
              Vu la Constitution, notamment son article 74 ; <br/>
<br/>
              Vu la loi organique n° 2004-192 du 27 février 2004 ;<br/>
<br/>
              Vu la décision n° 2004-490 DC du 12 février 2004 du Conseil constitutionnel ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Véronique Rigal, Maître des Requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de M. D...;<br/>
<br/>
<br/>
<br/>1. Considérant que les requêtes visées ci-dessus sont dirigées contre la même " loi du pays " ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 176 de la loi organique du 27 février 2004 portant statut d'autonomie de la Polynésie française : " I.- A l'expiration de la période de huit jours suivant l'adoption d'un acte prévu à l'article 140 dénommé "loi du pays" ou au lendemain du vote intervenu à l'issue de la nouvelle lecture prévue à l'article 143, le haut-commissaire, le président de la Polynésie française, le président de l'assemblée de la Polynésie française ou six représentants à l'assemblée de la Polynésie française peuvent déférer cet acte au Conseil d'Etat. Ils disposent à cet effet d'un délai de quinze jours (...). II.- A l'expiration de la période de huit jours suivant l'adoption d'un acte prévu à l'article 140 dénommé "loi du pays" ou au lendemain du vote intervenu à l'issue de la nouvelle lecture prévue à l'article 143, l'acte prévu à l'article 140 dénommé "loi du pays" est publié au Journal officiel de la Polynésie française à titre d'information pour permettre aux personnes physiques ou morales, dans le délai d'un mois à compter de cette publication, de déférer cet acte au Conseil d'Etat. Le recours des personnes physiques ou morales est recevable si elles justifient d'un intérêt à agir (...) " ; qu'aux termes de l'article 177 de la même loi : " (...) Si le Conseil d'Etat constate qu'un acte prévu à l'article 140 dénommé "loi du pays" contient une disposition contraire à la Constitution, aux lois organiques, ou aux engagements internationaux ou aux principes généraux du droit, et inséparable de l'ensemble de l'acte, celle-ci ne peut être promulguée. / Si le Conseil d'Etat décide qu'un acte prévu à l'article 140 dénommé "loi du pays" contient une disposition contraire à la Constitution, aux lois organiques ou aux engagements internationaux, ou aux principes généraux du droit, sans constater en même temps que cette disposition est inséparable de l'acte, seule cette dernière disposition ne peut être promulguée (...) " ;<br/>
<br/>
              Sur l'intervention de M. C...:<br/>
<br/>
              3. Considérant que l'article 176 de la loi organique a institué deux voies de recours distinctes pour la contestation des " lois du pays ", l'une réservée aux autorités et personnes mentionnées à son I, l'autre ouverte aux personnes justifiant d'un intérêt pour agir ; que la première de ces voies obéit à des règles particulières de procédure ; que, notamment, la requête est communiquée, avec les moyens de droit et de fait qu'elle comporte, aux autorités titulaires du droit de saisine, qui disposent d'un délai de dix jours pour présenter leurs observations ; que ces règles particulières excluent la possibilité d'intervenir à l'instance dans le cadre d'un recours formé par les autorités ou personnes mentionnées au I de l'article 176 ; que dès lors, l'intervention de M. C...dans la présente instance n'est pas recevable ;<br/>
<br/>
              Sur la fin de non-recevoir opposée à la requête du haut-commissaire de la République en Polynésie Française :<br/>
<br/>
              4. Considérant que la saisine opérée au titre du I de l'article 176 de la loi  contient, au terme de cet article, " un exposé des moyens de droit et de fait qui la motivent " ; que contrairement à ce que soutient le Président de la Polynésie française, la requête présentée par le haut-commissaire de la République en Polynésie française est conforme à ces prescriptions ; que, par suite, la fin de non recevoir opposée par le Président de la Polynésie française, tirée de ce que le haut-commissaire de la République en Polynésie française n'a pas sérieusement motivé sa requête doit être rejetée ; <br/>
<br/>
              Sur la légalité de la " loi du pays " :<br/>
<br/>
              Sans qu'il soit besoin d'examiner les autres moyens ; <br/>
<br/>
              5. Considérant qu'en vertu du cinquième alinéa de l'article 74 de la Constitution, le statut de chaque collectivité d'outre-mer régie par cet article fixe " les règles d'organisation et de fonctionnement des institutions de la collectivité et le régime électoral de son assemblée délibérante " ; qu'aux termes de l'article 13 de la loi organique du 27 février 2004 portant statut d'autonomie de la Polynésie française, " les autorités de la Polynésie française sont compétentes dans toutes les matières qui ne sont pas dévolues à l'Etat par l'article 14 et celles qui ne sont pas dévolues aux communes en vertu des lois et règlements applicables en Polynésie française " ; qu'en application du 1°) de l'article 90 de la loi organique du 27 février 2004, le conseil des ministres du gouvernement de Polynésie fixe les règles applicables à la " création et organisation des services, des établissements publics et des groupement d'intérêt public de la Polynésie française " ; <br/>
<br/>
              6. Considérant que la loi du pays attaquée crée une " autorité consultative indépendante " dénommée " Haut conseil de la Polynésie française " ; que le Haut conseil "  participe à la confection des lois de pays. Il est saisi par le Président de la Polynésie française des projets établis par le gouvernement. / il donne son avis sur les projets d'acte règlementaire, individuel ou particuliers pour lesquels les lois du pays, les délibérations ou les arrêtés pris en conseil des ministres ont prévu cette consultation, ou qui lui sont soumis par le gouvernement " ; qu'en outre,  " le Haut conseil émet un avis sur les propositions de loi du pays ou de délibération, déposées sur le bureau de l'assemblée de la Polynésie française et non encore examinées en commission, dont il est saisi par le président de l'assemblée (...) " ; <br/>
<br/>
              7. Considérant que, eu égard à son champ d'intervention, qui couvre l'ensemble des domaines de compétence de la Polynésie française et toutes les catégories d'actes normatifs, et aux conditions dans lesquelles elle doit ou peut être saisie, aussi bien par le président de l'assemblée  que par le Président de la Polynésie française ou par le gouvernement, " l'autorité " ainsi créée doit être regardée, même si elle ne dispose d'aucun pouvoir de décision, comme concourant à l'équilibre institutionnel de la Polynésie française ; que, s'il est loisible à l'autorité compétente de la Polynésie française de créer des organes administratifs de conseil et d'expertise dans les différents domaines de compétence de la Polynésie française, la création du Haut conseil constitue ainsi un acte relatif au fonctionnement des institutions de la Polynésie française, qui relève de la compétence du seul législateur organique ; <br/>
<br/>
              8. Considérant que si la loi du pays attaquée confère au Haut conseil de la Polynésie française d'autres missions que celles qui ont été analysées ci-dessus, ses dispositions constituent un ensemble indivisible ;  que, par suite, sans qu'il soit besoin de statuer sur la fin de non-recevoir opposée à la requête de M.D..., il y a lieu de faire droit aux conclusions tendant à ce que le Conseil d'Etat déclare que la " loi du pays " n° 2013-17 LP/APF est illégale et ne peut être promulguée ;<br/>
<br/>
              Sur les conclusions tendant à la suppression de passages injurieux et diffamatoires dans les écritures du Président de la Polynésie française :<br/>
<br/>
              9. Considérant que, contrairement à ce que soutient le haut-commissaire de la République en Polynésie française, les termes du mémoire présenté pour le Président de la Polynésie Française, pour regrettables que soient certains d'entre eux, n'excèdent pas les limites de la controverse entre parties dans le cadre d'une procédure contentieuse ; que, dès lors, il n'y a pas lieu d'en prononcer la suppression par application des dispositions de l'article 41 de la loi du 29 juillet 1881, reproduites à l'article L. 741-2 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : L'intervention de M. A...C...au soutien de la requête n° 370850 n'est pas admise.<br/>
Article 2 : La " loi du pays " n° 2013-17 LP/APF du 11 juillet 2013 est illégale et ne peut être promulguée.<br/>
Article 3 : Le surplus des conclusions du haut-commissaire de la République en Polynésie française est rejeté.<br/>
Article 4 : La présente décision sera notifiée au haut commissaire de la République en Polynésie française, au Président de la Polynésie française, au Président de l'Assemblée de la Polynésie française, au ministre des outre-mer et à M. B...D.PK 4 <br/>
 Copie en sera adressée pour information à M. A...C.PK 4<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">46-01-01-005 OUTRE-MER. DROIT APPLICABLE. GÉNÉRALITÉS. - RÉPARTITION DES COMPÉTENCES ENTRE L'ETAT ET LA POLYNÉSIE FRANÇAISE - CRÉATION D'UN HAUT CONSEIL DE LA POLYNÉSIE FRANÇAISE PAR LA  LOI DU PAYS  DU 11 JUILLET 2013 -  AUTORITÉ  CONCOURANT À L'ÉQUILIBRE INSTITUTIONNEL DE LA POLYNÉSIE FRANÇAISE - EXISTENCE EU ÉGARD À SON CHAMP D'INTERVENTION ET AUX CONDITIONS DANS LESQUELLES ELLE DOIT OU PEUT ÊTRE SAISIE, MÊME SI ELLE NE DISPOSE D'AUCUN POUVOIR DE DÉCISION - CONSÉQUENCE - COMPÉTENCE DE LA POLYNÉSIE FRANÇAISE - ABSENCE - COMPÉTENCE DU SEUL LÉGISLATEUR ORGANIQUE - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">46-01-02-02 OUTRE-MER. DROIT APPLICABLE. STATUTS. POLYNÉSIE FRANÇAISE. - HAUT CONSEIL DE LA POLYNÉSIE FRANÇAISE CRÉÉ PAR LA  LOI DU PAYS  DU 11 JUILLET 2013 -  AUTORITÉ  CONCOURANT À L'ÉQUILIBRE INSTITUTIONNEL DE LA POLYNÉSIE FRANÇAISE - EXISTENCE EU ÉGARD À SON CHAMP D'INTERVENTION ET AUX CONDITIONS DANS LESQUELLES ELLE DOIT OU PEUT ÊTRE SAISIE, MÊME SI ELLE NE DISPOSE D'AUCUN POUVOIR DE DÉCISION - CONSÉQUENCE - COMPÉTENCE DE LA POLYNÉSIE FRANÇAISE - ABSENCE - COMPÉTENCE DU SEUL LÉGISLATEUR ORGANIQUE - EXISTENCE.
</SCT>
<ANA ID="9A"> 46-01-01-005 Eu égard à son champ d'intervention, qui couvre l'ensemble des domaines de compétence de la Polynésie française et toutes les catégories d'actes normatifs, et aux conditions dans lesquelles elle doit ou peut être saisie, aussi bien par le président de l'assemblée que par le président de la Polynésie française ou par le gouvernement,  l'autorité  créée par la  loi du pays   n° 2013-17 LP/APF relative au Haut conseil de la Polynésie française du 11 juillet 2013 doit être regardée, même si elle ne dispose d'aucun pouvoir de décision, comme concourant à l'équilibre institutionnel de la Polynésie française. S'il est loisible à l'autorité compétente de la Polynésie française de créer des organes administratifs de conseil et d'expertise dans les différents domaines de compétence de la Polynésie française, la création du Haut conseil constitue ainsi un acte relatif au fonctionnement des institutions de la Polynésie française, qui relève de la compétence du seul législateur organique.</ANA>
<ANA ID="9B"> 46-01-02-02 Eu égard à son champ d'intervention, qui couvre l'ensemble des domaines de compétence de la Polynésie française et toutes les catégories d'actes normatifs, et aux conditions dans lesquelles elle doit ou peut être saisie, aussi bien par le président de l'assemblée que par le président de la Polynésie française ou par le gouvernement,  l'autorité  créée par la  loi du pays   n° 2013-17 LP/APF relative au Haut conseil de la Polynésie française du 11 juillet 2013 doit être regardée, même si elle ne dispose d'aucun pouvoir de décision, comme concourant à l'équilibre institutionnel de la Polynésie française. S'il est loisible à l'autorité compétente de la Polynésie française de créer des organes administratifs de conseil et d'expertise dans les différents domaines de compétence de la Polynésie française, la création du Haut conseil constitue ainsi un acte relatif au fonctionnement des institutions de la Polynésie française, qui relève de la compétence du seul législateur organique.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
