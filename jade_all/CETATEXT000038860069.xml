<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038860069</ID>
<ANCIEN_ID>JG_L_2019_07_000000421013</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/86/00/CETATEXT000038860069.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Formation spécialisée, 31/07/2019, 421013, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-07-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421013</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Formation spécialisée</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Emmanuelle Prada Bordenave</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CEFSP:2019:421013.20190731</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée au secrétariat de la section du contentieux le 28 mai 2018, M. B...A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision, révélée par le courrier de la présidente de la Commission nationale de l'informatique et des libertés (CNIL) du 27 mars 2018, par laquelle la ministre des armées lui a refusé l'accès aux données susceptibles de le concerner figurant dans le fichier de la direction générale de la sécurité extérieure (DGSE) ;<br/>
<br/>
              2°) d'ordonner la communication des données personnelles le concernant et figurant éventuellement dans ce traitement et, le cas échéant, la rectification ou la suppression de ces données ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule ; <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et son protocole n° 4 ; <br/>
              - le code de la sécurité intérieure ; <br/>
              - la loi n° 78-17 du 6 janvier 1978 ;<br/>
              - le décret n° 2005-1309 du 20 octobre 2005 ;<br/>
              -le décret n° 2019-536 du 29 mai 2019 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une séance à huis-clos, d'une part, M. A...et, d'autre part, la ministre des armées et la Commission nationale de l'informatique et des libertés, qui ont été mis à même de prendre la parole avant les conclusions ;<br/>
<br/>
              Et après avoir entendu en séance :<br/>
<br/>
              - le rapport de Mme Prada Bordenave, conseiller d'Etat,<br/>
              - et, hors la présence des parties, les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. En vertu des dispositions de l'article 41 de la loi du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, applicables à la procédure en cause et désormais reprises à l'article 118 de la même loi, et des dispositions de l'article 88 du décret susvisé du 20 octobre 2005, applicables à la procédure en cause et reprises désormais à l'article 143 du décret susvisé du 29 mai 2019, lorsqu'un traitement intéresse la sûreté de l'Etat ou la défense, les demandes tendant à l'exercice du droit d'accès, de rectification ou d'effacement sont adressées à la Commission nationale de l'informatique et des libertés (CNIL). Celle-ci désigne l'un de ses membres appartenant ou ayant appartenu au Conseil d'Etat, à la Cour de cassation ou à la Cour des comptes pour mener les investigations utiles et faire procéder aux modifications nécessaires. Lorsque la Commission constate, en accord avec le responsable du traitement, que la communication de données qui y sont contenues, ou l'information selon laquelle le traitement ne contient aucune donnée concernant le demandeur, ne met pas en cause ses finalités, la sûreté de l'Etat, la défense ou la sécurité publique, ces données ou cette information peuvent être communiquées au requérant. Dans le cas contraire, la Commission se borne à notifier au requérant qu'il a été procédé aux vérifications nécessaires, sans autre précision, avec la mention des voies et délais de recours.<br/>
<br/>
              2. En vertu de l'article 31 de la loi du 6 janvier 1978, les traitements de données à caractère personnel mis en oeuvre pour le compte de l'Etat et intéressant la sûreté de l'Etat, la défense ou la sécurité publique sont autorisés par arrêté du ou des ministres compétents, pris après avis motivé de la CNIL, publié avec l'arrêté autorisant le traitement. Ceux de ces traitements qui portent sur des données mentionnées au I de l'article 6 de la même loi doivent être autorisés par décret en Conseil d'Etat pris après avis motivé la Commission, publié avec ce décret. Un décret en Conseil d'Etat peut dispenser de publication l'acte réglementaire autorisant la mise en oeuvre de ces traitements ; le sens de l'avis émis par la CNIL est alors publié avec ce décret.<br/>
<br/>
              3. L'article L. 841-2 du code de la sécurité intérieure prévoit que le Conseil d'Etat est compétent pour connaître, dans les conditions prévues au chapitre III bis du titre VII du livre VII du code de justice administrative, des requêtes concernant la mise en oeuvre de l'article 41 devenu l'article 118 de la loi du 6 janvier 1978, pour les traitements ou parties de traitements intéressant la sûreté de l'Etat dont la liste est fixée par décret en Conseil d'Etat. En vertu de l'article R. 841-2 du même code, figurent notamment au nombre de ces traitements le fichier de la direction générale de la sécurité extérieure (DGSE). <br/>
<br/>
              4. L'article L. 773-8 du code de justice administrative dispose que, lorsqu'elle traite des requêtes relatives à la mise en oeuvre de l'article 41 devenu l'article 118 de la loi du 6 janvier 1978 : " la formation de jugement se fonde sur les éléments contenus, le cas échéant, dans le traitement sans les révéler ni révéler si le requérant figure ou non dans le traitement. Toutefois, lorsqu'elle constate que le traitement ou la partie de traitement faisant l'objet du litige comporte des données à caractère personnel le concernant qui sont inexactes, incomplètes, équivoques ou périmées, ou dont la collecte, l'utilisation, la communication ou la conservation est interdite, elle en informe le requérant, sans faire état d'aucun élément protégé par le secret de la défense nationale. Elle peut ordonner que ces données soient, selon les cas, rectifiées, mises à jour ou effacées. Saisie de conclusions en ce sens, elle peut indemniser le requérant ". L'article R. 773-20 du même code précise que : " Le défendeur indique au Conseil d'Etat, au moment du dépôt de ses mémoires et pièces, les passages de ses productions et, le cas échéant, de celles de la Commission nationale de contrôle des techniques de renseignement, qui sont protégés par le secret de la défense nationale. /Les mémoires et les pièces jointes produits par le défendeur et, le cas échéant, par la Commission nationale de contrôle des techniques de renseignement sont communiqués au requérant, à l'exception des passages des mémoires et des pièces qui, soit comportent des informations protégées par le secret de la défense nationale, soit confirment ou infirment la mise en oeuvre d'une technique de renseignement à l'égard du requérant, soit divulguent des éléments contenus dans le traitement de données, soit révèlent que le requérant figure ou ne figure pas dans le traitement. /Lorsqu'une intervention est formée, le président de la formation spécialisée ordonne, s'il y a lieu, que le mémoire soit communiqué aux parties, et à la Commission nationale de contrôle des techniques de renseignement, dans les mêmes conditions et sous les mêmes réserves que celles mentionnées à l'alinéa précédent ".<br/>
<br/>
              5. Il ressort des pièces du dossier que M. A...a saisi la CNIL afin de pouvoir accéder aux données le concernant qui seraient contenues dans le fichier de la DGSE. La CNIL a désigné, à la suite de cette demande, en application de l'article 41 de la loi du 6 janvier 1978 précité, alors applicable, un de ses membres pour mener toutes investigations utiles et faire procéder, le cas échéant, aux modifications nécessaires. Par lettre en date du 27 mars 2018, la présidente de la CNIL a informé M. A...de ce qu'il avait été procédé à l'ensemble des vérifications demandées s'agissant de ce fichier et que la procédure était terminée, sans apporter à l'intéressé d'autre information. M. A... demande au Conseil d'Etat d'annuler le refus, révélé par ce courrier, de la ministre des armées de lui donner accès aux mentions susceptibles de le concerner et figurant dans le fichier litigieux et d'ordonner la rectification ou, le cas échéant, l'effacement des données le concernant qui seraient contenues dans ce fichier.<br/>
<br/>
              6. La ministre des armées et la CNIL ont communiqué au Conseil d'Etat, dans les conditions prévues à l'article R. 773-20 du code de justice administrative, les éléments relatifs à la situation de l'intéressé. La ministre des armées a, en outre, communiqué au Conseil d'Etat l'acte réglementaire, non publié, autorisant la création du fichier litigieux.<br/>
<br/>
              7. Il appartient à la formation spécialisée, créée par l'article L. 773-2 du code de justice administrative précité, saisie de conclusions dirigées contre le refus de communiquer les données relatives à une personne qui allègue être mentionnée dans un fichier figurant à l'article R. 841-2 du code de la sécurité intérieure, de vérifier, au vu des éléments qui lui ont été communiqués hors la procédure contradictoire, si le requérant figure ou non dans le fichier litigieux. Dans l'affirmative, il lui appartient d'apprécier si les données y figurant sont pertinentes au regard des finalités poursuivies par ce fichier, adéquates et proportionnées. Pour ce faire, elle peut relever d'office tout moyen ainsi que le prévoit l'article L. 773-5 du code de justice administrative. Lorsqu'il apparaît soit que le requérant n'est pas mentionné dans le fichier litigieux soit que les données à caractère personnel le concernant qui y figurent ne sont entachées d'aucune illégalité, la formation de jugement rejette les conclusions du requérant sans autre précision. Dans le cas où des informations relatives au requérant figurent dans le fichier litigieux et apparaissent entachées d'illégalité soit que les données à caractère personnel le concernant sont inexactes, incomplètes, équivoques ou périmées soit que leur collecte, leur utilisation, leur communication ou leur consultation est interdite, elle en informe le requérant sans faire état d'aucun élément protégé par le secret de la défense nationale. Cette circonstance, le cas échéant relevée d'office par le juge dans les conditions prévues à l'article R. 773-21 du code de justice administrative, implique nécessairement que l'autorité gestionnaire du fichier rétablisse la légalité en effaçant ou en rectifiant, dans la mesure du nécessaire, les données illégales. Dans pareil cas, doit être annulée la décision implicite refusant de procéder à un tel effacement ou à une telle rectification.<br/>
<br/>
              8. La formation spécialisée a procédé à l'examen de l'acte réglementaire autorisant la création du fichier litigieux ainsi que des éléments fournis par la ministre des armées et la CNIL, laquelle a effectué les diligences qui lui incombent dans le respect des règles de compétence et de procédure applicables. Cet examen s'est déroulé selon les modalités décrites au point précédent et n'a révélé aucune illégalité, en particulier aucune méconnaissance des articles 41 de la loi du 6 janvier 1978 et 88 du décret du 20 octobre 2005, pas plus, en tout état de cause, que de l'article 15 de la Déclaration des droits de l'homme et du citoyen, qui dispose que " La Société a le droit de demander compte à tout Agent public de son administration ", ni aucune atteinte au principe de transparence ou à la liberté d'aller et venir. Il en résulte, que les conclusions de M.A..., qui ne peut utilement se prévaloir de ce que la décision qu'il attaque serait entachée de défaut de motivation, doivent être rejetées, y compris ses conclusions à fin d'injonction et celles présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de M. A...est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. B...A...et à la ministre des armées.<br/>
Copie en sera adressée à la Commission nationale de l'informatique et des libertés.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
