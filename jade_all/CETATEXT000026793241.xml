<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026793241</ID>
<ANCIEN_ID>JG_L_2012_12_000000362532</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/79/32/CETATEXT000026793241.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 18/12/2012, 362532</TITRE>
<DATE_DEC>2012-12-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>362532</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>HAAS ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Fabrice Aubert</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:362532.20121218</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 6 et 24 septembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour le département de la Guadeloupe, représenté par le président du conseil général ; le département demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1200751-1200752-1200753-1200754-1200755-1200756-1200757-1200758-1200759-1200760-1200761-1200762-1200763-1200764-1200765-1200766-1200767-1200768-1200769-1200770 du 20 août 2012 par laquelle le juge des référés du tribunal administratif de Basse-Terre, statuant en application de l'article L. 551-1 du code de justice administrative, a fait droit à la demande de la société Transport du Centre en annulant dans son intégralité la procédure de passation des lots nos 6, 15, 2, 96, 10, 99, 47, 98, 1, 7, 8, 12, 82, 5, 11, 3, 86, 97, 45 et 95 du marché public de transport scolaire lancé par le département de la Guadeloupe ;<br/>
<br/>
              2°) statuant en référé, de rejeter les demandes de la société Transport du Centre ;<br/>
<br/>
              3°) de mettre à la charge de la société Transport du Centre la somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des marchés publics ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Fabrice Aubert, Auditeur,  <br/>
<br/>
              - les observations de Me Haas, avocat du département de la Guadeloupe et de la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la société Transport du Centre,<br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à Me Haas, avocat du département de la Guadeloupe et à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la société Transport du Centre ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 551-1 du code de justice administrative : " Le président du tribunal administratif, ou le magistrat qu'il délègue, peut être saisi en cas de manquement aux obligations de publicité et de mise en concurrence auxquelles est soumise la passation par les pouvoirs adjudicateurs de contrats administratifs ayant pour objet l'exécution de travaux, la livraison de fournitures ou la prestation de services, avec une contrepartie économique constituée par un prix ou un droit d'exploitation, ou la délégation d'un service public (...) " ; qu'aux termes de l'article L. 551-2 de ce code : " I. Le juge peut ordonner à l'auteur du manquement de se conformer à ses obligations et suspendre l'exécution de toute décision qui se rapporte à la passation du contrat, sauf s'il estime, en considération de l'ensemble des intérêts susceptibles d'être lésés et notamment de l'intérêt public, que les conséquences négatives de ces mesures pourraient l'emporter sur leurs avantages. (...) " ; qu'aux termes de l'article L. 551-10 du même code : " Les personnes habilitées à engager les recours prévus aux articles L. 551-1 et L. 551-5 sont celles qui ont un intérêt à conclure le contrat et qui sont susceptibles d'être lésées par le manquement invoqué (...) " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que, par avis d'appel public à la concurrence publié le 16 mars 2012, le département de la Guadeloupe a lancé la procédure de passation d'un marché de transport scolaire ; qu'aux termes du règlement de la consultation, les offres devaient être notées, d'une part, sur un critère de prix en fonction de l'écart de prix entre l'offre évaluée et le prix moyen proposé par l'ensemble des candidats pour le même lot et, d'autre part, sur un critère portant sur l'âge des véhicules, aucun véhicule de plus de quinze ans ne pouvant être proposé ; que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Basse-Terre, saisi sur le fondement de l'article L. 551-1 du code de justice administrative par la société Transport du Centre, candidat évincé, a annulé la procédure dans son intégralité pour les lots nos 6, 15, 2, 96, 10, 99, 47, 98, 1, 7, 8, 12, 82, 5, 11, 3, 86, 97, 45 et 95 du marché ;<br/>
<br/>
              3. Considérant que, pour faire droit à la demande de la société Transport du Centre, le juge des référés du tribunal administratif de Basse-Terre a jugé que le pouvoir adjudicateur avait manqué à ses obligations de publicité et de mise en concurrence en ne communiquant pas aux candidats la note qui correspondrait, pour le critère du prix, au prix moyen des offres présentées pour chaque lot ; que, toutefois, la note attribuée au prix moyen des offres est un élément relatif à la méthode de notation des offres, que le pouvoir adjudicateur n'est pas tenu de communiquer aux candidats ; que, par suite, le juge des référés a commis une erreur de droit ; que dès lors, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son ordonnance doit être annulée ; <br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de statuer sur la demande en référé en application des dispositions de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              5. Considérant, en premier lieu, que lorsque le pouvoir adjudicateur souhaite, en cours de procédure et avant la remise des offres, modifier un élément de la consultation, il n'est tenu de publier un avis d'appel public à la concurrence rectificatif que si cette modification est substantielle ; qu'en l'espèce, la suppression d'un unique lot sur les 124 lots du marché, dès la diffusion du dossier de la consultation aux entreprises, ne saurait être regardée comme une modification substantielle et n'avait pas, contrairement à ce que soutient la société requérante, à faire l'objet d'un avis d'appel public à la concurrence rectificatif ; <br/>
<br/>
              6. Considérant, en deuxième lieu, que s'il résulte des dispositions du III de l'article 6 du code des marchés publics que les spécifications techniques imposées par le pouvoir adjudicateur ne peuvent " avoir pour effet de créer des obstacles injustifiés à l'ouverture des marchés publics à la concurrence ", la limite d'âge de quinze ans pour les véhicules imposée par le département de la Guadeloupe, qui est en rapport avec l'objet du marché, ne peut être regardée comme restreignant la concurrence dès lors que tous les candidats étaient également susceptibles d'y satisfaire ; <br/>
<br/>
              7. Considérant, en troisième lieu, que l'évaluation des offres reposant sur des critères objectifs que sont le prix et l'âge des véhicules, le pouvoir adjudicateur pouvait, sans méconnaître son obligation de motiver le rejet des offres résultant de l'article 80 du code des marchés publics, se borner à communiquer aux candidats évincés un tableau comparant, pour les deux critères et pour chacun des lots, leurs notes à celles du candidat retenu ;<br/>
<br/>
              8. Considérant, en quatrième lieu, qu'ainsi qu'il a été dit, le pouvoir adjudicateur n'était pas tenu de communiquer aux candidats les éléments relatifs à la méthode de notation des offres, tels que la note correspondant au prix moyen des offres présentées ; <br/>
<br/>
              9. Considérant, en cinquième lieu, que les pouvoirs adjudicateurs ne peuvent, lorsqu'ils choisissent d'évaluer les offres par plusieurs critères pondérés, recourir à des méthodes de notation conduisant à l'attribution, pour un ou plusieurs critères, de notes négatives ; qu'en effet une telle note, en se soustrayant aux notes obtenues sur les autres critères dans le calcul de la note globale, serait susceptible de fausser la pondération relative des critères initialement définie et communiquée aux candidats ; qu'en l'espèce, il n'est pas contesté que le département de la Guadeloupe a adopté, pour la notation sur le critère du prix, une méthode le conduisant à attribuer des notes négatives à certains candidats ; que, ce faisant, il a manqué à ses obligations de publicité et de mise en concurrence ; <br/>
<br/>
              10. Considérant toutefois que, pour l'ensemble des lots à l'attribution desquels la société Transport du Centre s'est portée candidate, elle a, d'une part, obtenu une note égale ou inférieure à celle de l'attributaire sur le critère de la valeur technique et a, d'autre part, proposé un prix plus élevé que l'attributaire ; que cette société n'a ainsi pu être lésée par le manquement relevé dès lors qu'elle n'était, quelle que soit la méthode de notation retenue, pas susceptible de se voir attribuer l'un des lots litigieux ; qu'il résulte de ce qui précède que la société Transport du Centre n'est pas fondée à demander l'annulation de la procédure de passation engagée pour ces lots ; <br/>
<br/>
              11. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Transport du Centre la somme de 1 000 euros à verser au département de la Guadeloupe au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'en revanche ces dispositions font obstacle à ce que soit mis à la charge du département, qui n'est pas, dans la présente instance, la partie perdante, le versement de la somme demandée au même titre par la société Transport du Centre ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 20 août 2012 du juge des référés du tribunal administratif de Basse-Terre est annulée.<br/>
Article 2 : Les demandes de la société Transport du Centre devant le juge des référés du tribunal administratif de Basse-Terre sont rejetées.<br/>
Article 3 : La société Transport du Centre versera au département de la Guadeloupe une somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : Les conclusions présentées par la société Transport du Centre au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée au département de la Guadeloupe et à la société Transport du Centre. <br/>
Copie en sera adressée à la société Entreprise de transports Transka, à la société de transport Les 6F, à la société de transports Ramassamy Michel, à la SARL Transport Mausse, à la société Entreprise Alizé transport, à la société de transports Translom, à la société de transports Trans'vet, à la SARL Voyageurs, à la société Entreprise de transports Pajamandy Thomas et à la société de transports Virapin Claudy.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-02-005 MARCHÉS ET CONTRATS ADMINISTRATIFS. FORMATION DES CONTRATS ET MARCHÉS. FORMALITÉS DE PUBLICITÉ ET DE MISE EN CONCURRENCE. - EVALUATION DES OFFRES - MÉTHODE - PONDÉRATION DE PLUSIEURS CRITÈRES - POSSIBILITÉ DE RECOURIR À DES NOTES NÉGATIVES - ABSENCE.
</SCT>
<ANA ID="9A"> 39-02-005 Les pouvoirs adjudicateurs ne peuvent, lorsqu'ils choisissent d'évaluer les offres par plusieurs critères pondérés, recourir à des méthodes de notation conduisant à l'attribution, pour un ou plusieurs critères, de notes négatives.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
