<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026589665</ID>
<ANCIEN_ID>JG_L_2012_11_000000353624</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/58/96/CETATEXT000026589665.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 07/11/2012, 353624</TITRE>
<DATE_DEC>2012-11-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>353624</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Stéphanie Gargoullaud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Damien Botteghi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:353624.20121107</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 25 octobre 2011 au secrétariat du contentieux du Conseil d'Etat, présenté par le Groupe d'information et de soutien des immigrés (GISTI), dont le siège est 3, villa Marcès à Paris (75011) ; le GISTI demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir les paragraphes 1.1.1, 1.2.1, 1.2.2, 1.2.3 et 1.3 de la circulaire du 31 mai 2011 du ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration et du ministre du travail, de l'emploi et de la santé relative à la maîtrise de l'immigration professionnelle ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers en France et du droit d'asile ;<br/>
<br/>
              Vu le code du travail ;<br/>
<br/>
              Vu le code pénal ;<br/>
<br/>
              Vu la loi n° 79-587 du 11 juillet 1979 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Stéphanie Gargoullaud, Maître des requêtes en service extraordinaire, <br/>
<br/>
              - les conclusions de M. Damien Botteghi, Rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 5221-2 du code du travail : " Pour entrer en France en vue d'y exercer une profession salariée, l'étranger présente : (...) 2° Un contrat de travail visé par l'autorité administrative ou une autorisation de travail " ; qu'aux termes de l'article L. 5221-5 du même code : " Un étranger autorisé à séjourner en France ne peut exercer une activité professionnelle salariée en France sans avoir obtenu au préalable l'autorisation de travail mentionnée au 2° de l'article L. 5221-2 (...) " ; qu'aux termes de l'article L. 5221-11 du même code  : " Un décret en Conseil d'Etat détermine les modalités d'application des articles L. 5221-3 et L. 5221-5 à L. 5221-8 " ;  qu'aux termes de l'article R. 5221-20 du même code : " Pour accorder ou refuser l'une des autorisations de travail mentionnées à l'article R. 5221-11, le préfet prend en compte les éléments d'appréciation suivants : / 1° La situation de l'emploi dans la profession et dans la zone géographique pour lesquelles la demande est formulée, compte tenu des spécificités requises pour le poste de travail considéré, et les recherches déjà accomplies par l'employeur auprès des organismes de placement concourant au service public du placement pour recruter un candidat déjà présent sur le marché du travail ; / 2° L'adéquation entre la qualification, l'expérience, les diplômes ou titres de l'étranger et les caractéristiques de l'emploi auquel il postule ; / 3° le respect par l'employeur (...) de la législation relative au travail et à la protection sociale ; / 4° Le cas échéant, le respect par l'employeur (...) ou le salarié des conditions réglementaires d'exercice de l'activité considérée ; / 5° Les conditions d'emploi et de rémunération offertes à l'étranger (...) ; / 6° Le salaire proposé à l'étranger (...) ; / 7° Le cas échéant (...)  les dispositions prises par l'employeur pour assurer (...) le logement de l'étranger (...) " ;<br/>
<br/>
              2. Considérant que le ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration et le ministre du travail, de l'emploi et de la santé ont adressé, le 31 mai 2011, aux préfets une circulaire relative aux modalités de délivrance des autorisations de travail aux travailleurs étrangers ; que le GISTI demande l'annulation pour excès de pouvoir de certains des termes de cette circulaire ; que le syndicat SUD travail affaires sociales intervient au soutien de la requête ; <br/>
<br/>
              Sur les fins de non-recevoir opposées par le ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration :<br/>
<br/>
              3. Considérant d'une part que les ministres auteurs de la circulaire attaquée ont énoncé, au moyen de dispositions impératives, les directives à suivre en matière d'immigration professionnelle afin d'assurer l'insertion des demandeurs d'emploi aujourd'hui présents sur le marché du travail français dans un contexte de crise économique, en précisant à cet effet les contrôles qui doivent être réalisés avant de délivrer une autorisation de travail ; que, par suite, la fin de non-recevoir tirée de ce que cette circulaire ne serait pas susceptible d'être déférée au juge de l'excès de pouvoir doit être écartée ; <br/>
<br/>
              4. Considérant, d'autre part, que le GISTI justifie, eu égard à son objet statutaire, d'un intérêt lui donnant qualité pour former un recours pour excès de pouvoir contre cette circulaire ;<br/>
<br/>
              Sur l'intervention du syndicat SUD travail affaires sociales : <br/>
<br/>
              5. Considérant que le syndicat SUD travail affaires sociales ne justifie pas d'un intérêt le rendant recevable à intervenir à l'appui d'un recours tendant à l'annulation de la circulaire attaquée, qui définit des règles que les fonctionnaires qu'il représente sont appelés à mettre en oeuvre, sans que leurs conditions d'emploi et de travail soient affectées, ni qu'une atteinte soit portée à leurs prérogatives ; que, par suite, son intervention n'est pas recevable ;<br/>
<br/>
              Sur la légalité de la circulaire contestée :<br/>
<br/>
              En ce qui concerne le paragraphe  1.1.1 :<br/>
<br/>
              6. Considérant que ce paragraphe invite les préfets à procéder à des vérifications concernant l'employeur, afin notamment de s'assurer de l'existence réelle de l'entreprise et de pouvoir écarter les demandes sollicitées par une entreprise inscrite au registre du commerce mais qui n'aurait pas d'existence réelle ; qu'en adressant ces instructions aux préfets, les ministres signataires de la circulaire n'ont pas excédé leur compétence ; qu'en appelant leur attention sur la prévention d'éventuelles fraudes dans le cadre des demandes d'autorisations de travail au profit d'un étranger, les dispositions contestées n'ont méconnu ni le principe de la liberté du commerce et de l'industrie, qui n'est nullement en cause, ni les articles 111-3 et 121-3 du code pénal relatifs aux conditions de constatation des infractions pénales ; <br/>
<br/>
              En ce qui concerne le paragraphe 1.2.1 :<br/>
<br/>
              7. Considérant que ce paragraphe donne instruction aux préfets, sous réserve d'un examen individuel des dossiers et de la prise en compte de situations exceptionnelles, d'écarter " les demandes d'autorisation de travail pour des emplois pour lesquels la situation de l'emploi ne justifie pas l'introduction d'un travailleur étranger sur le marché du travail, soit en raison d'un taux de tension insuffisamment élevé, soit en raison de la possibilité de former, dans des délais très brefs, des demandeurs d'emploi résidant régulièrement en France pour répondre à l'offre de travail présentée " ; que ces instructions n'ont pas pour effet de soumettre l'examen de la demande d'autorisation à un critère distinct de l'appréciation de la situation de l'emploi dans la profession et dans la zone géographique dans les conditions prévues par le 1° de l'article R. 5221-20 du code du travail ; que, par suite, les ministres signataires de la circulaire ont pu les énoncer sans excéder leur compétence ; que ces termes de la circulaire n'ont ni pour objet ni pour effet d'autoriser une dérogation aux dispositions de la loi du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public ; <br/>
<br/>
              En ce qui concerne le paragraphe  1.2.2 :<br/>
<br/>
              8. Considérant que ce paragraphe indique aux préfets les conditions dans lesquelles sera appréciée la notion, mentionnée au 1° de l'article R. 5221-20, de " recherches déjà accomplies par l'employeur auprès des organismes de placement concourant au service public du placement pour recruter un candidat déjà présent sur le marché du travail " ; que la circulaire énonce que la recherche effective sera considérée comme ayant été réalisée si l'offre d'emploi a été diffusée par Pôle emploi et si elle n'a pas été satisfaite après deux mois de diffusion ; que la circulaire prévoit, à défaut d'une diffusion par Pôle emploi, qu'il y aura lieu de vérifier que " l'offre d'emploi a été correctement diffusée pendant un délai raisonnable qui peut être de deux ou trois mois avant la date de présentation de la demande d'autorisation de travail " ; qu'en définissant ces modalités d'examen des dossiers, les ministres signataires de la circulaire n'ont pas excédé leur compétence ; que les modalités d'examen des demandes d'autorisation ainsi prévues par la circulaire ne sont pas constitutives d'une rupture d'égalité entre les entreprises selon qu'elles ont recours à différents organismes concourant au service du placement ;  <br/>
<br/>
              En ce qui concerne le paragraphe 1.2.3 :<br/>
<br/>
              9. Considérant qu'aux termes du 2° de l'article R. 5221-20 du code du travail, le préfet prend notamment en compte " L'adéquation entre la qualification, l'expérience, les diplômes ou titres de l'étranger et les caractéristiques de l'emploi auquel il postule " ; que le paragraphe 1.2.3 de la circulaire donne instruction aux préfets d'examiner " l'adéquation entre l'offre d'emploi et le profil du candidat et les termes du contrat de travail proposé ", en précisant que l'étranger doit pouvoir justifier par ses diplômes ou son expérience professionnelle, de sa capacité à occuper l'emploi proposé, et en énonçant que, si les diplômes ou l'expérience professionnelle sont inférieurs aux exigences nécessaires pour occuper le poste, il ne sera pas donné une suite favorable à la demande d'autorisation de travail ; qu'en énonçant que " Si le candidat présente au contraire un profil manifestement surqualifié par rapport à l'emploi proposé, vous demanderez à l'employeur de retirer sa demande et de modifier son offre d'emploi en rapport avec le profil réellement recherché et d'entreprendre une nouvelle procédure pour vous permettre de vous prononcer sur la nécessité d'introduire un travailleur étranger ", la circulaire ne s'est pas bornée à tirer les conséquences des dispositions réglementaires citées, mais a ajouté à celles-ci, en fixant une nouvelle règle d'examen de la demande d'autorisation, qui impose à l'employeur de déposer une nouvelle demande ; que, par suite, l'association requérante est fondée à demander l'annulation de ces dispositions, qui sont divisibles du reste de la circulaire ; <br/>
<br/>
              En ce qui concerne le paragraphe 1.3 :<br/>
<br/>
              10. Considérant que l'article L. 5221-3 du code du travail dispose que " L'étranger qui souhaite entrer en France en vue d'y exercer une profession salariée et qui manifeste la volonté de s'y installer durablement atteste d'une connaissance suffisante de la langue française sanctionnée par une validation des acquis de l'expérience ou s'engage à l'acquérir après son installation en France " ; que, contrairement à ce que soutient l'association requérante, aucune disposition réglementaire n'apparaît nécessaire pour l'application de ces dispositions législatives ; qu'en invitant les préfets, dans son paragraphe 1.3, à les appliquer en vérifiant la connaissance suffisante de la langue française par l'étranger ou son engagement à l'acquérir après son installation en France, la circulaire attaquée s'est bornée, sans y ajouter, à tirer les conséquences des dispositions de l'article L. 5221-3 du code du travail ;<br/>
<br/>
              11. Considérant qu'il résulte de tout ce qui précède que le GISTI est seulement fondé à demander l'annulation des dispositions du quatrième alinéa du paragraphe 1.2.3 de la circulaire attaquée ; <br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              12. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par l'association requérante au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'intervention du syndicat SUD travail affaires sociales n'est pas admise.<br/>
<br/>
Article 2 : Le quatrième alinéa du paragraphe 1.2.3 de la circulaire du 31 mai 2011 du ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration et du ministre du travail, de l'emploi et de la santé est annulé.<br/>
<br/>
Article 3 : Le surplus des conclusions de la requête du Groupe d'information et de soutien des immigrés est rejeté.<br/>
<br/>
Article 4 : La présente décision sera notifiée au Groupe d'information et de soutien des immigrés, au Syndicat SUD travail affaires sociales, au ministre de l'intérieur et au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36 AFFICHAGE ET PUBLICITÉ. AFFICHAGE. RÉGIME DE LA LOI DU 29 DÉCEMBRE 1979. DISPOSITIONS APPLICABLES À LA PUBLICITÉ. PUBLICITÉ À L'INTÉRIEUR DES AGGLOMÉRATIONS. - SYNDICATS DE FONCTIONNAIRES - RECOURS TENDANT À L'ANNULATION DE RÈGLES QUE LES FONCTIONNAIRES SONT APPELÉS À METTRE EN OEUVRE - INTÉRÊT POUR INTERVENIR AU SOUTIEN DE CE RECOURS - ABSENCE, SAUF SI CES RÈGLES AFFECTENT LES CONDITIONS D'EMPLOI ET DE TRAVAIL DES FONCTIONNAIRES OU PORTENT UNE ATTEINTE À LEURS PRÉROGATIVES [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-05-03-01 PROCÉDURE. INCIDENTS. INTERVENTION. RECEVABILITÉ. - ABSENCE - ABSENCE D'INTÉRÊT POUR INTERVENIR - SYNDICAT DE FONCTIONNAIRES - RECOURS TENDANT À L'ANNULATION DE RÈGLES QUE LES FONCTIONNAIRES SONT APPELÉS À METTRE EN OEUVRE SANS QUE LEURS CONDITIONS D'EMPLOI ET DE TRAVAIL SOIENT AFFECTÉES, NI QU'UNE ATTEINTE SOIT PORTÉE À LEURS PRÉROGATIVES [RJ1].
</SCT>
<ANA ID="9A"> 36 Un syndicat de fonctionnaires n'a pas intérêt pour intervenir à l'appui d'un recours tendant à l'annulation de règles que les fonctionnaires qu'il représente sont appelés à mettre en oeuvre, sans que leurs conditions d'emploi et de travail soient affectées, ni qu'une atteinte soit portée à leurs prérogatives.</ANA>
<ANA ID="9B"> 54-05-03-01 Un syndicat de fonctionnaires n'a pas intérêt pour intervenir à l'appui d'un recours tendant à l'annulation de règles que les fonctionnaires qu'il représente sont appelés à mettre en oeuvre, sans que leurs conditions d'emploi et de travail soient affectées, ni qu'une atteinte soit portée à leurs prérogatives.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., pour l'assimilation, s'agissant d'un syndicat de fonctionnaires, entre intérêt pour agir et intérêt pour intervenir, CE, Assemblée, 2 juillet 1982, Huglo et autres, n°s 25288 25323, p. 257.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
