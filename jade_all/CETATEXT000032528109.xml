<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032528109</ID>
<ANCIEN_ID>JG_L_2016_05_000000398909</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/52/81/CETATEXT000032528109.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 04/05/2016, 398909, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-05-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>398909</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2016:398909.20160504</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au juge des référés du tribunal administratif de Cayenne, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'enjoindre au recteur de la Guyane de mettre en oeuvre dans un délai de quinze jours tous les moyens humains et matériels lui permettant d'exercer ses fonctions de chef de travaux conformément à la fiche de poste correspondant à son cadre d'emploi et de bénéficier d'un examen médical périodique du médecin de prévention ;<br/>
<br/>
              2°) d'enjoindre au recteur de Guyane de mettre en oeuvre dans un délai de quinze jours tous les moyens afin de faire cesser les agissements de harcèlement moral à l'encontre de la requérante. <br/>
<br/>
              Par une ordonnance n° 1600163 du 1er avril 2016, le juge des référés du tribunal administratif de Cayenne a enjoint au recteur de la Guyane de prendre toutes les mesures nécessaires pour que l'intéressée soit en mesure d'exercer son activité de chef de travaux sans faire l'objet de pratiques de harcèlement et de permettre à la requérante d'être examinée par le médecin compétent une fois par trimestre.<br/>
<br/>
              Par un recours, enregistré le 20 avril 2016 au secrétariat du contentieux du Conseil d'Etat, la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de rejeter les conclusions présentées par Mme B...devant le juge des référés du tribunal administratif.<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - le juge des référés a jugé à tort que les faits invoqués par Mme B... étaient constitutifs de harcèlement moral ;<br/>
              - Mme B...ne peut se prévaloir du caractère excessif des missions confiées dès lors qu'elle a bénéficié de plusieurs assistants et que certaines de ces missions ne lui ont été confiées que temporairement ;<br/>
              - le retrait de certaines tâches dont elle avait la charge ne caractérise pas une situation de harcèlement mais la mise en oeuvre normale du pouvoir hiérarchique et du pouvoir d'organisation du service ;<br/>
              - les entretiens non disciplinaires auxquels Mme B...a été conviée ne peuvent être regardés comme participant d'une stratégie de harcèlement dès lors qu'ils visaient à apaiser la situation au sein de l'établissement ;<br/>
              - le dénigrement public récurrent allégué n'est pas établi ; <br/>
              - Mme B...entretient des relations difficiles avec l'équipe de l'établissement ;<br/>
              - l'administration a mis en oeuvre les mesures nécessaires pour tenter de remédier aux dysfonctionnements du service.<br/>
<br/>
<br/>
<br/>
              Par un mémoire en défense, enregistré le 27 avril 2016, Mme B... conclut au rejet de la requête et à ce qu'une somme de 1 500 euros soit mise à la charge de l'Etat au titre de l'article L. 761-1 du code de justice administrative. Elle soutient que la condition d'urgence est remplie et que les moyens soulevés par la ministre ne sont pas fondés. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'éducation ;<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n°84-16 du 11 janvier 1984 ;<br/>
              - le code de justice administrative ; <br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche, d'autre part, Mme B... ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 2 mai 2016 à 10 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - les représentants de la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche ;<br/>
<br/>
              - Me Nicolaÿ, avocat au Conseil d'Etat et à la Cour de cassation, avocat de Mme B...;<br/>
              et à l'issue de laquelle l'instruction a été close ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures " ;<br/>
<br/>
              2. Considérant qu'il résulte de l'instruction que MmeB..., professeur de lycée professionnel, a été affectée le 1er septembre 2014 au lycée professionnel Max Joséphine de Cayenne afin d'y exercer, pour la première fois dans sa carrière, des fonctions de chef de travaux ; que sa prise de fonctions et ses premiers mois d'activité ont été perturbés par des difficultés dans l'organisation du travail, tenant à la définition et à l'étendue des missions qui lui étaient confiées ainsi qu'au faible concours qu'elle a été susceptible de recevoir de la part notamment de son assistant, qui a quitté l'établissement en octobre 2014, ou d'autres membres de l'équipe de direction de l'établissement ; que les relations entre Mme B...et le chef d'établissement ont pris rapidement un tour conflictuel et ont été depuis lors régulièrement émaillées d'incidents ; que si les éléments versés au dossier, en particulier les nombreux messages électroniques adressées par Mme B...aux membres de l'équipe de direction ou au personnel de l'établissement montrent que le comportement professionnel de l'intéressée n'a pas toujours été conforme à ce que devrait être la manière de servir d'un agent exerçant des fonctions comme les siennes, il résulte également des éléments recueillis dans le cadre de l'instruction en référé que les réactions du chef d'établissement à cette situation ont été excessives, inadéquates et inappropriées ; que, pour la rentrée 2015, le chef d'établissement a entendu provoquer le départ de l'établissement de MmeB..., dont le nom a été effacé de l'organigramme établi le 28 août 2015 alors que l'affectation de l'intéressée dans l'établissement n'avait pourtant pas été remise en cause par l'administration ; que le poste de chef de travaux a été indiqué comme vacant en novembre 2015 sur la plateforme en ligne du rectorat alors que tel n'était pas le cas, avant que cette indication, présentée comme une erreur, ne soit supprimée ; qu'à raison de la situation conflictuelle constatée dans l'établissement, une mission d'audit interne de l'établissement a été ordonnée par le recteur et confiée à deux inspecteurs, qui se sont rendus sur place le 26 novembre 2015 ; que le compte-rendu qu'ils ont établi témoigne de la situation de conflit existant entre le chef d'établissement et le chef de travaux depuis janvier 2015 et a préconisé de clarifier et de réattribuer les missions des membres de l'équipe de direction et de revenir " à des règles de communication professionnelles " au sein de l'établissement ; que la situation conflictuelle a toutefois perduré, ainsi que le montrent les éléments versés au dossier, notamment un compte-rendu d'audience accordée par le recteur de l'académie au début de l'année 2016 à des représentants syndicaux de l'établissement ; qu'à cette occasion, les représentants syndicaux ont fait valoir que cette situation conflictuelle opposant le chef d'établissement au chef de travaux n'était pas la première constatée au sein du lycée dans la période récente et que d'autres agents affectés dans l'établissement avaient fait l'objet de " formes de harcèlement de la part du chef d'établissement " ; que, récemment encore, MmeB..., a été mise dans l'impossibilité de se servir de son poste informatique le 11 avril 2016, en raison de la suppression de son mot de passe, expliquée par l'administration comme étant la conséquence d'une manoeuvre involontaire et fortuite ; <br/>
<br/>
              3. Considérant qu'aux termes de l'article 6 quinquiès de la loi du 13 juillet 1983 portant droits et obligations des fonctionnaires : " Aucun fonctionnaire ne doit subir les agissements répétés de harcèlement moral qui ont pour objet ou pour effet une dégradation des conditions de travail susceptible de porter atteinte à ses droits et à sa dignité, d'altérer sa santé physique ou mentale ou de compromettre son avenir professionnel " ; que le droit de ne pas être soumis à un harcèlement moral constitue pour un fonctionnaire une liberté fondamentale au sens des dispositions de l'article L. 521-2 du code de justice administrative ; qu'il appartient à l'autorité administrative de prendre toute mesure pour faire cesser, alors qu'elle en a connaissance, des pratiques de harcèlement moral, et de veiller à ce que les agents placés sous sa responsabilité ne soient pas exposés à de telles pratiques ;<br/>
<br/>
              4. Considérant que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Cayenne a estimé, au vu des éléments discutés devant lui et en l'état de l'instruction, que Mme B...avait été victime d'agissements de harcèlement moral de la part du chef d'établissement ; que les éléments présentés en appel ne conduisent pas à infirmer, en l'état de l'instruction, cette appréciation des circonstances particulières de l'affaire ; qu'alors qu'il appartient en toute hypothèse, ainsi qu'il vient d'être dit, au recteur de l'académie de veiller à ce que les agents placés sous sa responsabilité ne soient pas exposés à des pratiques de harcèlement moral, le ministre n'est pas fondé à soutenir que c'est à tort que le juge des référés a enjoint au recteur de prendre toute mesure en vue de prévenir la réitération d'agissements de harcèlement à l'encontre de MmeB..., alors même que le recteur s'est déjà préoccupé de la situation de l'établissement et a pris une première série de mesures ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que le recours de la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche doit être rejeté ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 1 500 euros à verser à Mme B...au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : Le recours de la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche est rejeté.<br/>
Article 2 : L'Etat versera la somme de 1 500 euros à Mme B...au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente ordonnance sera notifiée à la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche et à Mme A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
