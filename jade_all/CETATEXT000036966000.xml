<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036966000</ID>
<ANCIEN_ID>JG_L_2018_05_000000411785</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/96/60/CETATEXT000036966000.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 4ème chambres réunies, 30/05/2018, 411785, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-05-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411785</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>Mme Marie Sirinelli</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:411785.20180530</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et deux mémoires en réplique, enregistrés les 22 juin 2017, 13 décembre 2017 et 23 janvier 2018 au secrétariat du contentieux du Conseil d'Etat, Mme B... A...demande au Conseil d'Etat d'annuler pour excès de pouvoir la décision implicite par laquelle les ministres chargés de la santé et de la sécurité sociale ont approuvé l'avenant n° 3 à la convention nationale organisant les rapports entre les médecins libéraux et l'assurance maladie, signé le 8 février 2017, en tant qu'il restreint le bénéfice de la prestation qu'il prévoit à certaines catégories de médecins, excluant notamment les médecins remplaçants.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - le code de la sécurité sociale ;<br/>
              - la loi n° 2005-882 du 2 août 2005 ;<br/>
              - la loi n° 2016-1827 du 23 décembre 2016, ainsi que la décision du Conseil constitutionnel n° 2016-742 DC du 22 décembre 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Sirinelli, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de l'Union nationale des caisses d'assurance maladie.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 162-5 du code de la sécurité sociale : " Les rapports entre les organismes d'assurance maladie et les médecins sont définis par des conventions nationales conclues séparément pour les médecins généralistes et les médecins spécialistes, par l'Union nationale des caisses d'assurance maladie et une ou plusieurs organisations syndicales les plus représentatives pour l'ensemble du territoire de médecins généralistes ou de médecins spécialistes ou par une convention nationale conclue par l'Union nationale des caisses d'assurance maladie et au moins une organisation syndicale représentative pour l'ensemble du territoire de médecins généralistes et une organisation syndicale représentative pour l'ensemble du territoire de médecins spécialistes. / La ou les conventions déterminent notamment : / 1° Les obligations respectives des caisses primaires d'assurance maladie et des médecins d'exercice libéral (...) ". L'article 72 de la loi du 23 décembre 2016 de financement de la sécurité sociale pour 2017 a complété cet article L. 162-5 pour prévoir que la convention détermine également : " 25° Le cas échéant, les modalités de versement d'une aide financière complémentaire aux médecins interrompant leur activité médicale pour cause de maternité ou de paternité ". Les conventions et avenants conclus sur le fondement de ces dispositions sont, en vertu du deuxième alinéa de l'article L. 162-15 du même code, approuvés par les ministres chargés de la santé et de la sécurité sociale. A ce titre, le cas échéant, " ils sont réputés approuvés si les ministres n'ont pas fait connaître aux signataires, dans le délai de vingt-et-un jours à compter de la réception du texte, qu'ils s'opposent à leur approbation du fait de leur non-conformité aux lois et règlements en vigueur ou pour des motifs de santé publique ou de sécurité sanitaire ou lorsqu'il est porté atteinte au principe d'un égal accès aux soins ".<br/>
<br/>
              2. Sur le fondement des dispositions ajoutées à l'article L. 162-5 du code de la sécurité sociale par la loi de financement de la sécurité sociale pour 2017, un troisième avenant à la convention nationale organisant les rapports entre les médecins libéraux et l'assurance maladie, signée le 25 août 2016 et approuvée par arrêté du 20 octobre 2016, a été conclu le 8 février 2017 entre l'Union nationale des caisses d'assurance maladie et trois syndicats représentatifs. Cet avenant, qui insère un nouvel article 70.2 au sein de la convention, prévoit une aide financière complémentaire à destination des médecins libéraux conventionnés interrompant leur activité médicale libérale pour cause de maternité, de paternité ou d'adoption. Dans le dernier état de ses écritures, Mme A...demande l'annulation pour excès de pouvoir de la décision par laquelle les ministres chargés de la santé et de la sécurité sociale ont implicitement approuvé cet avenant, sur le fondement de l'article L. 162-15 du code de la sécurité sociale, en tant que cette décision approuve des clauses ayant pour objet ou pour effet de limiter le bénéfice de l'aide financière complémentaire à certaines catégories de médecins, à l'exclusion notamment des médecins remplaçants.<br/>
<br/>
              3. L'article 70.2 inséré dans la convention nationale du 25 août 2016 par l'article 1er de son avenant n° 3 reconnaît le bénéfice de l'aide financière complémentaire au " médecin libéral conventionné " dont l'activité libérale est d'au moins quatre demi-journées par semaine et qui interrompt cette activité pour cause de maternité, de paternité ou d'adoption. Il se réfère ainsi aux dispositions de l'article 2 de la convention nationale, qui prévoit que celle-ci s'applique aux catégories suivantes de médecins, regardés à ce titre comme " médecins libéraux conventionnés ", au sens de la convention : les " médecins exerçant à titre libéral, inscrits au tableau de l'Ordre national des médecins et qui ont fait le choix d'exercer sous le régime conventionnel, pour les soins dispensés sur leur lieu d'exercice professionnel ou au domicile du patient ", les " praticiens statutaires temps plein hospitalier exerçant dans les établissements publics de santé qui font également le choix d'exercer sous le régime de la présente convention, pour la partie de leur activité effectuée en libéral qu'ils sont autorisés à exercer dans les conditions définies dans le code de la santé publique " et, enfin, les " médecins autorisés par dérogation à effectuer en France une libre prestation de services au sens du code de la santé publique habilités par l'Ordre des médecins " qui font le choix d'exercer sous le régime conventionnel. En revanche, il résulte du même article que, bien que leur exercice professionnel soit pour partie régi par la convention, ne peuvent être regardés comme " médecins libéraux conventionnés ", au sens de la convention, les " médecins qui exercent avec ou à la place d'un médecin conventionné dans le cadre d'un remplacement ou d'une collaboration salariée ".<br/>
<br/>
              4. Dans ce cadre, d'une part, eu égard à la condition de durée hebdomadaire d'activité libérale posée par l'avenant critiqué, supérieure à celle que les praticiens hospitaliers sont autorisés à exercer, en vertu de l'article L. 6154-2 du code de la santé publique, l'aide financière complémentaire qu'il crée n'est susceptible de bénéficier qu'à la première des catégories de médecins énumérées par l'article 2 de la convention cité ci-dessus. D'autre part, il résulte des dispositions de ce même article que l'aide financière complémentaire est susceptible de bénéficier, en leur qualité de médecins exerçant à titre libéral, aux médecins collaborateurs libéraux, qui sont mentionnés à l'article R. 4127-87 du code de la santé publique et qui, en vertu de l'article 18 de la loi du 2 août 2005 en faveur des petites et moyennes entreprises, relèvent " du statut social et fiscal du professionnel libéral qui exerce en qualité de professionnel indépendant ". <br/>
<br/>
              5. En premier lieu, eu égard aux allocations et indemnités auxquels ont droit l'ensemble des médecins libéraux, exerçant dans le cadre conventionnel, à l'occasion de la maternité, de la paternité ou de l'adoption, en application des dispositions du code de la sécurité sociale, Mme A...ne démontre pas que l'avenant critiqué aurait méconnu les dispositions du onzième alinéa du Préambule de la Constitution du 27 octobre 1946, relatives à la protection de la santé en ce qu'il n'étend pas le bénéfice de l'aide financière complémentaire qu'il crée aux médecins remplaçants de médecins ayant adhéré à la convention nationale organisant les rapports entre les médecins libéraux et l'assurance maladie.<br/>
<br/>
              6. En deuxième lieu, l'avenant litigieux crée cette aide financière complémentaire dans le but, ainsi qu'il le précise, d'aider les médecins libéraux conventionnés " à faire face aux charges inhérentes à la gestion du cabinet médical " pendant la période de congé de maternité, de paternité ou d'adoption. En réservant le bénéfice de cette aide aux médecins ayant, au moins partiellement, la charge de la gestion d'un cabinet médical, les partenaires conventionnels n'ont pas méconnu l'objet des dispositions du 25° de l'article L. 162-5 du code de la sécurité sociale citées au point 1, par lesquelles, comme le révèlent les travaux préparatoires de la loi du 23 décembre 2016 dont elles sont issues, le législateur a notamment entendu encourager l'installation des jeunes médecins exerçant à titre libéral, en particulier en secteur à honoraires opposables, afin d'améliorer l'accessibilité des soins médicaux, ni commis sur ce point une erreur manifeste d'appréciation.<br/>
<br/>
              7. En troisième lieu, le principe d'égalité ne s'oppose pas à ce que l'autorité investie du pouvoir réglementaire règle de façon différente des situations différentes ni à ce qu'elle déroge à l'égalité pour des raisons d'intérêt général, pourvu que, dans l'un comme l'autre cas, la différence de traitement qui en résulte soit en rapport direct avec l'objet de la norme qui l'établit et ne soit pas manifestement disproportionnée au regard des motifs susceptibles de la justifier. <br/>
<br/>
              8. Les médecins libéraux installés dans un cabinet dont ils assument, au moins partiellement, la charge et les médecins, collaborateurs salariés ou remplaçants, qui peuvent être amenés à exercer dans ce cabinet, sont placés dans une situation différente au regard de l'objet de l'aide financière complémentaire instituée par l'avenant en litige, qui vise à encourager l'installation de jeunes médecins et revêt, en outre, un caractère d'intérêt général. Cette aide n'ayant qu'un caractère complémentaire aux allocations et indemnités mentionnées au point 5, la différence de traitement ainsi instituée entre les médecins en charge d'un cabinet et les autres praticiens qui, en rétrocédant le cas échéant une partie des honoraires perçus, participent aux coûts inhérents à cette structure sans toutefois assumer la responsabilité de la prise en charge continue d'une patientèle et le risque inhérent à la gestion d'un cabinet, ne revêt pas un caractère disproportionné au regard des motifs qui la justifient. Par suite, le moyen tiré de la méconnaissance du principe d'égalité doit être écarté.<br/>
<br/>
              9. En dernier lieu, l'avenant en litige détermine de façon suffisamment claire le champ d'application de l'aide financière complémentaire qu'il crée ainsi que les modalités du calcul de son montant en fonction du nombre de demi-journées hebdomadaires d'exercice libéral du médecin à la date à laquelle il est amené à interrompre son activité pour cause de maternité, de paternité ou d'adoption. Il en résulte que le moyen tiré de la méconnaissance de l'objectif de valeur constitutionnelle d'accessibilité et d'intelligibilité de la norme doit être écarté.<br/>
<br/>
              10. Il résulte de tout ce qui précède que Mme A...n'est pas fondée à soutenir que les ministres auraient dû s'opposer à l'approbation de l'avenant critiqué, en raison de son illégalité, et, dès lors, à demander l'annulation de la décision implicite d'approbation qu'elle attaque. <br/>
<br/>
              11. Par suite, ses conclusions présentées sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative doivent être rejetées. Enfin, il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par l'Union nationale des caisses d'assurance maladie sur le fondement des mêmes dispositions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                            --------------<br/>
<br/>
Article 1er : La requête de Mme A...est rejetée. <br/>
Article 2 : Les conclusions de l'Union nationale des caisses d'assurance maladie présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à Mme B...A..., à la ministre des solidarités et de la santé et à l'Union nationale des caisses d'assurance maladie.<br/>
Copie en sera adressée au ministre de l'action et des comptes publics, à la Fédération des médecins de France, à la Fédération française des médecins généralistes et au syndicat Le Bloc.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
