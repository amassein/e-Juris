<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042427507</ID>
<ANCIEN_ID>JG_L_2020_10_000000426168</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/42/75/CETATEXT000042427507.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 14/10/2020, 426168, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-10-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>426168</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR>Mme Catherine Moreau</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Stéphane Hoynck</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:426168.20201014</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° Sous le n° 426168, par une requête sommaire, un mémoire complémentaire et trois autres mémoires, enregistrés les 10 décembre 2018, 11 mars, 26 avril, 12 juin et 19 décembre 2019 au secrétariat du contentieux du Conseil d'Etat, l'association Agir Espèces demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler, pour excès de pouvoir, la décision implicite du ministre de la transition écologique et solidaire rejetant sa demande tendant à ce que le ministre retire sa décision habilitant la Société d'actions et de promotion vétérinaires (SAPV) à gérer le fichier national d'identification des animaux d'espèces non domestiques et à ce qu'il résilie et retire la convention qu'il a signée le 10 avril 2018 avec cette société pour la désigner comme gestionnaire de ce fichier. <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Sous le n° 427360, par une requête sommaire et un mémoire complémentaire, enregistrés les 25 janvier et 25 avril 2019 au secrétariat du contentieux du Conseil d'Etat, l'association Agir Espèces demande au Conseil d'Etat d'annuler pour excès de pouvoir l'arrêté du 15 novembre 2018 du ministre de la transition écologique et solidaire et du ministre de l'agriculture et de l'alimentation portant agrément du gestionnaire du fichier national d'identification des animaux d'espèces non domestiques et précisant les modalités d'établissement, de contrôle et d'exploitation des données traitées.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code de l'environnement ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme B... A..., conseillère d'Etat en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Stéphane Hoynck, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de l'Association Agir Espèces et à la SCP Rousseau, Tapie, avocat de la Société d'action et de promotion vétérinaires ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Les requêtes n° 426168 et 427360 de l'Association Agir Espèces sont dirigées contre des actes ayant le même objet. Il y a lieu de les joindre pour statuer par une seule décision. <br/>
<br/>
              2. Aux termes de l'article L. 413-6 du code de l'environnement : " I. - Les mammifères, oiseaux, reptiles et amphibiens d'espèces non domestiques figurant sur les listes établies en application des articles L. 411-1, L. 411-2 et L. 412-1 détenus en captivité doivent être identifiés individuellement dans les conditions précisées par arrêté conjoint des ministres chargés de l'environnement et de l'agriculture. / II. - Pour assurer le suivi statistique et administratif des animaux dont l'identification est obligatoire en application du I du présent article et pour permettre d'identifier leurs propriétaires, les données relatives à l'identification de ces animaux, le nom et l'adresse de leurs propriétaires successifs et la mention de l'exécution des obligations administratives auxquelles ces derniers sont astreints peuvent être enregistrés dans un fichier national et faire l'objet d'un traitement automatisé dans les conditions fixées par la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés. / Un décret en Conseil d'Etat, pris après avis de la Commission nationale de l'informatique et des libertés, détermine les modalités d'application du présent article. Il précise les conditions dans lesquelles la collecte des données et leur traitement peuvent être confiés à des personnes agréées par les ministres chargés de l'environnement et de l'agriculture, la durée de conservation et les conditions de mise à jour des données enregistrées et les catégories de destinataires de ces données ". Pour l'application de ces dispositions, l'article R. 413-23-6 dispose que : " Les ministres chargés de l'environnement et de l'agriculture fixent par arrêtés conjoints les conditions dans lesquelles la personne agréée assure l'inscription de tous les animaux d'espèces non domestiques identifiés dans le fichier national, l'édition des documents liés à leur identification et le traitement des données propres à chaque espèce ou groupe d'espèces. Ces arrêtés précisent les modalités d'établissement, de contrôle et d'exploitation des données traitées par la personne agréée ".<br/>
<br/>
              Sur les conclusions dirigées contre la convention du 10 avril 2018 :<br/>
<br/>
              3. Par une convention du 10 avril 2018, le ministre de la transition écologique et solidaire a désigné la Société d'actions et de promotion vétérinaires (SAPV) comme gestionnaire du fichier national d'identification des animaux d'espèces non domestiques pour une durée de cinq ans et fixé les conditions dans lesquelles doit être assurée l'inscription des animaux identifiés dans ce fichier national, l'édition des documents liés à leur identification et le traitement des données ainsi recueillies. L'association Agir Espèces a demandé au ministre, le 10 août 2018, de rapporter cette convention et de procéder à sa résiliation et a saisi le Conseil d'Etat après le rejet implicite de sa demande. <br/>
<br/>
              4. Il résulte des termes de la convention que, si elle accorde à la SAPV l'agrément prévu par l'article L. 413-6 du code de l'environnement, elle fixe également le montant des droits d'enregistrement mis à la charge des usagers du fichier, précise le contenu des missions confiées au gestionnaire du fichier et détermine les modalités d'exercice de ces missions. Ces clauses de la convention, qui définissent les règles d'organisation et de fonctionnement du fichier national d'identification des animaux d'espèces non domestiques et précisent les modalités d'établissement, de contrôle et d'exploitation des données traitées par la personne qu'elle agrée, en application de l'article R. 413-23-6 du code de l'environnement, présentent un caractère réglementaire. La requête de l'association Agir Espèces, qui tend à l'annulation pour excès de pouvoir de ces clauses réglementaires, ressortit à la compétence de premier ressort du Conseil d'Etat en vertu de l'article R. 311-1 du code de justice administrative. L'association requérante est, par ailleurs, recevable, eu égard aux intérêts qu'elle entend défendre, à en demander l'annulation.<br/>
<br/>
              5. Il résulte des dispositions, citées au point 2, des articles L. 413-6 et R. 413-23-6 du code de l'environnement que l'agrément du gestionnaire du fichier national d'identification des animaux d'espèces non domestiques doit être accordé conjointement par les ministres chargés de l'environnement et de l'agriculture et qu'un arrêté conjoint de ces deux ministres doit fixer les modalités d'établissement, de contrôle et d'exploitation des données traitées par la personne agréée. Dès lors que la convention du 10 avril 2018, qui accorde à la SAPV l'agrément prévu par l'article L. 413-6 du code de l'environnement et fixe les conditions dans lesquelles la personne agréée exerce les missions qui lui sont confiées, n'a pas été signée par le ministre chargé de l'agriculture, l'association Agir Espèces est fondée à soutenir qu'elle a été prise par une autorité incompétente et qu'elle est, pour ce motif et dans son ensemble, eu égard à son caractère indivisible, entachée d'illégalité.<br/>
<br/>
              Sur les conclusions dirigées contre l'arrêté du 18 novembre 2018 :<br/>
<br/>
              6. L'association Agir Espèces demande au Conseil d'Etat l'annulation de l'arrêté du l'arrêté du 18 novembre 2018 portant agrément du gestionnaire du fichier national d'identification des animaux d'espèces non domestiques et précisant les modalités d'établissement, de contrôle et d'exploitation des données traitées. L'article 1er de cet arrêté agrée la SAPV pour une durée de cinq ans et son article 2 prévoit que cette société respecte dans l'exécution des missions qui lui sont confiées les stipulations de la convention du 10 avril 2018. Les autres dispositions de l'arrêté reprennent l'essentiel des dispositions figurant dans la convention, à l'exception cependant de celles de son article 5 fixant les montants des droits d'enregistrement mis à la charge des usagers du fichier.<br/>
<br/>
              7. En premier lieu, l'arrêté attaqué a été signé conjointement par le ministre de la transition écologique et solidaire et le ministre de l'agriculture et de l'alimentation, conformément à ce que prévoient le dernier alinéa de l'article L. 413-6 et l'article R. 413-23-6 du code de l'environnement. Si les requérantes font grief à la procédure d'adoption de l'arrêté de n'avoir pas été conduite conjointement par les ministres chargés de l'environnement et de l'agriculture, aucune disposition n'édicte de prescription particulière à cet égard. Le moyen ne peut, dès lors, qu'être écarté.<br/>
<br/>
              8. En second lieu, préalablement à l'édiction de l'arrêté attaqué, les ministres de la transition écologique et solidaire et de l'agriculture et de l'alimentation ont organisé une consultation du public en application des dispositions de l'article L. 132-1 du code des relatons entre le public et l'administration, aux termes duquel : " Lorsque l'administration est tenue de procéder à la consultation d'une commission consultative préalablement à l'édiction d'un acte réglementaire, elle peut décider d'organiser une consultation ouverte permettant de recueillir, sur un site internet, les observations des personnes concernées. / Cette consultation ouverte se substitue à la consultation obligatoire en application d'une disposition législative ou réglementaire. Les commissions consultatives dont l'avis doit être recueilli en application d'une disposition législative ou réglementaire peuvent faire part de leurs observations dans le cadre de la consultation prévue au présent article ". Cette consultation s'est ainsi légalement substituée à la consultation de la commission nationale consultative pour la faune sauvage captive à laquelle les auteurs de cet arrêté étaient tenus de procéder en application de l'article R. 441-23-5 du code de l'environnement. La circonstance que, contrairement à ce que prévoient les dispositions de l'article L. 132-2 du code des relations entre le public et l'administration, les ministres signataires de l'arrêté attaqué n'auraient pas, à la date de sa signature, rendu publique la synthèse des observations recueillies au cours de la consultation, est, par elle-même, sans incidence sur la légalité de cet arrêté. <br/>
<br/>
              9. Il résulte de tout ce qui précède que l'association Agir Espèces n'est pas fondée à demander l'annulation pour excès de pouvoir de l'arrêté du 18 novembre 2018. <br/>
<br/>
              Sur les conséquences de l'illégalité de la convention :<br/>
<br/>
              10. L'annulation d'un acte administratif implique en principe que cet acte est réputé n'être jamais intervenu. Toutefois, s'il apparaît que cet effet rétroactif de l'annulation est de nature à emporter des conséquences manifestement excessives en raison tant des effets que cet acte a produits et des situations qui ont pu se constituer lorsqu'il était en vigueur, que de l'intérêt général pouvant s'attacher à un maintien temporaire de ses effets, il appartient au juge administratif - après avoir recueilli sur ce point les observations des parties et examiné l'ensemble des moyens, d'ordre public ou invoqués devant lui, pouvant affecter la légalité de l'acte en cause - de prendre en considération, d'une part, les conséquences de la rétroactivité de l'annulation pour les divers intérêts publics ou privés en présence et, d'autre part, les inconvénients que présenterait, au regard du principe de légalité et du droit des justiciables à un recours effectif, une limitation dans le temps des effets de l'annulation. Il lui revient d'apprécier, en rapprochant ces éléments, s'ils peuvent justifier qu'il soit dérogé au principe de l'effet rétroactif des annulations contentieuses et, dans l'affirmative, de prévoir dans sa décision d'annulation que, sous réserve des actions contentieuses engagées à la date de sa décision prononçant l'annulation contre les actes pris sur le fondement de l'acte en cause, tout ou partie des effets de cet acte antérieurs à son annulation devront être regardés comme définitifs ou même, le cas échéant, que l'annulation ne prendra effet qu'à une date ultérieure qu'il détermine. <br/>
<br/>
              11. Eu égard aux conséquences manifestement excessives qui résulteraient de l'annulation rétroactive de la convention du 10 avril 2018 au regard de l'intérêt général qui s'attache à la protection des animaux et à la lutte contre le trafic et les prélèvements illicites dans la nature, lequel justifie que les enregistrements dans le fichier national d'identification des animaux d'espèces non domestiques qui ont été effectués depuis sa mise en service le 15 juin 2018 en application des clauses de la convention ne soient pas remis en cause et que les données figurant dans ce fichier puissent être conservées et continuer d'être utilisées, il y a lieu, sous réserve des droits des personnes qui auraient engagé une action contentieuse à la date de la présente décision, de différer au 1er janvier 2021 l'annulation de la convention du 10 avril 2018 et de regarder comme définitifs les effets produits jusqu'à cette date.<br/>
<br/>
              12. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à l'association Agir Espèces, au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces dispositions font obstacle à ce qu'une somme soit mise à la charge de cette association qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La convention du 10 avril 2018 conclue par le ministre de la transition écologique et solidaire avec la Société d'actions et de promotion vétérinaires est annulée à compter du 1er janvier 2021. Les effets produits par cette convention jusqu'à cette date sont définitifs, sous réserve des actions engagées à la date de la présente décision.<br/>
Article 2 : L'Etat versera à l'association Agir Espèces une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Les conclusions de la Société d'actions et de promotion vétérinaires au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : Le surplus des conclusions de l'association Agir Espèces est rejeté.<br/>
Article 5 : La présente décision sera notifiée à l'Association Agir Espèces, à la ministre de la transition écologique, au ministre de l'agriculture et de l'alimentation et à la Société d'actions et de promotion vétérinaires.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
