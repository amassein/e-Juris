<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042434189</ID>
<ANCIEN_ID>JG_L_2020_10_000000423954</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/43/41/CETATEXT000042434189.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 16/10/2020, 423954, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-10-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>423954</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>Mme Airelle Niepce</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier  Fuchs</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:423954.20201016</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et deux mémoires en réplique, enregistrés les 7 septembre et 7 décembre 2018 et les 27 février et 25 juin 2020 au secrétariat du contentieux du Conseil d'État, le Syndicat des avocats de France demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir les décisions implicites par lesquelles le Président de la République, le Premier ministre et la garde des sceaux, ministre de la justice, ont rejeté ses demandes, reçues le 7 mai 2018, tendant, d'une part, à l'adoption des dispositions permettant de se conformer à la directive (UE) 2016/343 du Parlement européen et du Conseil du 9 mars 2016 portant renforcement de certains aspects de la présomption d'innocence et du droit d'assister à son procès dans le cadre des procédures, notamment pour la mise en oeuvre de son article 5 et, d'autre part, à ce que soit organisées les voies de recours permettant la contestation préalable des mesures de contraintes physiques utilisées à l'encontre des suspects et des personnes poursuivies, ainsi que la décision expresse de rejet de cette demande contenue dans le courrier du 18 juillet 2018 du chef de cabinet du Président de la République ;<br/>
<br/>
              2°) d'enjoindre au Président de la République, au Premier ministre et à la garde des sceaux, ministre de la justice, de prendre les mesures de transposition des articles 5 et 10 de la directive du 9 mars 2016 dans un délai d'un mois à compter de la notification de la décision à intervenir, sous astreinte de 1 000 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'État la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la directive UE 2016/343 du Parlement européen et du Conseil du 9 mars 2016 ;<br/>
              - le code de procédure pénale ;<br/>
              - l'arrêté du 18 août 2016 portant approbation de la politique ministérielle de défense et de sécurité au sein du ministère de la justice ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme B... A..., maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Olivier Fuchs, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat du Syndicat des avocats de France ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Par lettres reçues le 7 mai 2018, le Syndicat des avocats de France a demandé au Président de la République, au Premier ministre et à la garde des sceaux, ministre de la justice, d'une part, que soient prises les mesures permettant d'assurer la transposition de l'article 5 de la directive du 9 mars 2016 portant renforcement de certains aspects de la présomption d'innocence et du droit d'assister à son procès dans le cadre des procédures pénales et, d'autre part, que soient organisées les voies de recours permettant la contestation préalable des mesures de contraintes physiques utilisées à l'encontre des suspects et des personnes poursuivies, conformément à l'article 10 de cette même directive. Il demande, d'une part, l'annulation pour excès de pouvoir de la décision de rejet de cette demande par le Président de la République, contenue dans le courrier du 18 juillet 2018 de son chef de cabinet, et des décisions implicites de rejet nées du silence gardé par le Premier ministre et la garde des sceaux, ministre de la justice, sur sa demande et, d'autre part, qu'il soit enjoint à ces autorités de prendre les mesures de transposition des articles 5 et 10 de la directive du 9 mars 2016, dans un délai d'un mois à compter de la notification de la décision à intervenir, sous astreinte de 1 000 euros par jour de retard.<br/>
<br/>
              2. En premier lieu, aux termes de l'article 5 de la directive du 9 mars 2016 portant renforcement de certains aspects de la présomption d'innocence et du droit d'assister à son procès dans le cadre des procédures pénales : " 1. Les États membres prennent les mesures appropriées pour veiller à ce que les suspects et les personnes poursuivies ne soient pas présentés, à l'audience ou en public, comme étant coupables par le recours à des mesures de contrainte physique. / 2. Le paragraphe 1 n'empêche pas les États membres d'appliquer les mesures de contrainte physique qui s'avèrent nécessaires pour des raisons liées au cas d'espèce relatives à la sécurité ou à la nécessité d'empêcher les suspects ou les personnes poursuivies de prendre la fuite ou d'entrer en contact avec des tiers ". <br/>
<br/>
              3. Aux termes du quatrième alinéa du III de l'article préliminaire du code de procédure pénale : " Les mesures de contraintes dont la personne suspectée ou poursuivie peut faire l'objet sont prises sur décision ou sous le contrôle effectif de l'autorité judiciaire. Elles doivent être strictement limitées aux nécessités de la procédure, proportionnées à la gravité de l'infraction reprochée et ne pas porter atteinte à la dignité de la personne ". Les articles 309 et 318 du même code, relatifs à l'audience devant la cour d'assises, disposent que " le président a la police de l'audience et la direction des débats " et que " l'accusé comparaît libre et seulement accompagné de gardes pour l'empêcher de s'évader ". L'article 304 du même code inclut expressément le rappel du principe de la présomption d'innocence dans le serment que chaque juré est appelé à prêter, dès le début de l'audience.<br/>
<br/>
              4. Il résulte de ces dispositions qu'il appartient au président de la formation de jugement, dans le cadre de son pouvoir de police de l'audience, à son initiative ou sur la demande du ministère public, d'une partie ou de son avocat, et sous le contrôle de la Cour de cassation, de veiller, au cas par cas, à l'équilibre entre, d'une part, la sécurité des différents participants au procès ainsi que la nécessité d'empêcher l'accusé de fuir ou de communiquer avec des tiers et, d'autre part, le respect des droits de la défense, qui impliquent notamment que les modalités pratiques de comparution du prévenu ou de l'accusé devant la juridiction lui permettent de participer de manière digne et effective aux débats et de s'entretenir confidentiellement avec son avocat. Ces dispositions définissent ainsi les conditions dans lesquelles peut être décidé le placement d'un prévenu ou d'un accusé dans un box sécurisé. Par suite, le Syndicat des avocats de France n'est, en tout état de cause, pas fondé à soutenir que les décisions qu'il attaque seraient entachées d'une méconnaissance de l'article 5 de la directive du 9 mars 2016 qui, contrairement à ce qu'il soutient, ne proscrit pas l'usage de " menottes, boxes vitrés, cages et entraves de métal ", et indique, aux termes de son paragraphe 2 cité au point 2 que les autorités compétentes peuvent recourir à de telles mesures de contrainte physique " lorsqu'elles s'avèrent nécessaires pour des raisons liées au cas d'espèce ". <br/>
<br/>
              5. En second lieu, aux termes de l'article 10 de la même directive : " 1.   Les États membres veillent à ce que les suspects et les personnes poursuivies disposent d'une voie de recours effective en cas de violation des droits prévus au titre de la présente directive. (...) ".<br/>
<br/>
              6. Il résulte des dispositions du code de procédure pénale que la défense du prévenu ou de l'accusé peut présenter des conclusions tendant, au début de l'audience, à ce qu'il ne comparaisse pas dans un box sécurisé ou, ensuite, à ce qu'il soit extrait de ce box. Une méconnaissance de la présomption d'innocence résultant des conditions dans lesquelles la personne a comparu à l'audience peut également être soulevée à l'occasion du recours formé contre la décision pénale la condamnant. Par suite, le moyen tiré de ce que le refus de prévoir des dispositions permettant de contester le placement dans un box sécurisé serait entaché d'une méconnaissance de l'article 10 de la directive du 9 mars 2016 et d'une erreur de droit doit, en tout état de cause, être écarté.<br/>
<br/>
              7. Il résulte de tout ce qui précède que le Syndicat des avocats de France n'est pas fondé à demander l'annulation des décisions qu'il attaque. Sa requête, y compris les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ainsi que ses conclusions à fin d'injonction ne peuvent, par suite, qu'être rejetées.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête du Syndicat des avocats de France est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée au Syndicat des avocats de France, au Premier ministre et au garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
