<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027535314</ID>
<ANCIEN_ID>JG_L_2013_06_000000367146</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/53/53/CETATEXT000027535314.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 11/06/2013, 367146, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-06-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>367146</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Anne Egerszegi</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:367146.20130611</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu l'ordonnance n° 1007450 du 22 mars 2013, enregistrée le 25 mars 2013 au secrétariat du contentieux du Conseil d'Etat, par laquelle le président de la septième chambre du tribunal administratif de Versailles, avant qu'il ne soit statué sur la demande de Mme B... A...tendant à la décharge des droits de taxe sur la valeur ajoutée et des pénalités correspondantes, qui lui ont été réclamés au titre de la période du 1er février 2003 au 31 décembre 2004 à hauteur de la somme de 23 179 euros, a décidé, par application des dispositions de l'article 23-2 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du 3° de l'article L. 66 ainsi que des articles L. 67 et L. 68 du livre des procédures fiscales ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule et son article 61-1 ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ; <br/>
<br/>
              Vu la loi de finances pour 1987 (n°86-1317 du 30 décembre 1986), notamment son article 81 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport d'Anne Egerszegi, Maître des Requêtes ;<br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 66 du livre des procédures fiscales, dans sa rédaction applicable à la procédure d'imposition en cause : " Sont taxés d'office :/1° à l'impôt sur le revenu, les contribuables qui n'ont pas déposé dans le délai légal la déclaration d'ensemble de leurs revenus ou qui n'ont pas déclaré, en application des articles 150-0 E et 150 VG du code général des impôts, les gains nets et les plus-values imposables qu'ils ont réalisés, sous réserve de la procédure de régularisation prévue à l'article L. 67 ; / 2° à l'impôt sur les sociétés, les personnes morales passibles de cet impôt qui n'ont pas déposé dans le délai légal leur déclaration, sous réserve de la procédure de régularisation prévue à l'article L. 68 ; /  3° aux taxes sur le chiffre d'affaires et à la taxe différentielle sur les véhicules à moteur, les personnes qui n'ont pas déposé dans le délai légal les déclarations qu'elles sont tenues de souscrire en leur qualité de redevables des taxes ; / 4° aux droits d'enregistrement et aux taxes assimilées, les personnes qui n'ont pas déposé une déclaration ou qui n'ont pas présenté un acte à la formalité de l'enregistrement dans le délai légal, sous réserve de la procédure de régularisation prévue à l'article L. 67 ;  /5° aux taxes assises sur les salaires ou les rémunérations les personnes assujetties à ces taxes qui n'ont pas déposé dans le délai légal les déclarations qu'elles sont tenues de souscrire, sous réserve de la procédure de régularisation prévue l'article L. 68. " ; que les articles L. 67 et L. 68 du livre des procédures fiscales imposent la notification d'une mise en demeure au contribuable n'ayant pas satisfait à ses obligations déclaratives, avant d'engager la procédure de taxation d'office ou d'évaluation d'office prévue aux  1°, 2°, 4° et 5° de l'article L. 66 ; qu'il résulte de ces dispositions combinées qu'en matière de taxes sur le chiffre d'affaires, la loi n'impose pas à l'administration fiscale de mettre le contribuable défaillant en demeure de souscrire ses déclarations avant d'engager la procédure de taxation d'office prévue au 3° de l'article L. 66 du livre précité ; <br/>
<br/>
              3. Considérant, en premier lieu, que les dispositions contestées n'introduisent aucune différence entre les redevables de la taxe sur la valeur ajoutée et ne méconnaissent pas le principe d'égalité devant la loi fiscale ; <br/>
<br/>
              4. Considérant, en second lieu, que les redevables de cette taxe, qui la collectent pour le compte de l'Etat mais n'en supportent pas la charge finale, sont placés dans une situation différente de celle des contribuables assujettis aux autres impôts ; qu'au surplus, il ressort des travaux préparatoires relatifs au II de l'article 81 de la loi de finances pour 1987, qui a généralisé la procédure de la mise en demeure préalable à toute imposition d'office pour absence ou tardiveté de la déclaration, que cette garantie a été exclue dans l'hypothèse du défaut de déclaration en matière de taxes sur le chiffre d'affaires en raison du caractère mensuel de la plupart des déclarations à souscrire ; qu'ainsi l'absence d'obligation, pour l'administration fiscale, de notifier une mise en demeure avant la mise en oeuvre de la procédure de taxation d'office pour défaut de déclaration de la taxe sur la valeur ajoutée dans les délais légaux  repose sur des critères objectifs et rationnels en rapport avec l'objet de la loi et ne porte pas atteinte à l'égalité  des contribuables devant les charges publiques ; que, par suite, la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède qu'il n'y a pas lieu de renvoyer au Conseil constitutionnel  la question prioritaire de constitutionnalité invoquée ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil Constitutionnel la question de la conformité à la Constitution du 3° de l'article L. 66 ainsi que des articles L. 67 et L. 68 du livre des procédures fiscales.<br/>
Article 2 : La présente décision sera notifiée à Mme B... A...et au ministre délégué, chargé du budget.<br/>
Copie en sera adressée pour information au Premier ministre, au Conseil constitutionnel et au tribunal administratif de Versailles.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
