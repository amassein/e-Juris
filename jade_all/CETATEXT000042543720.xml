<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042543720</ID>
<ANCIEN_ID>JG_L_2020_11_000000437506</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/54/37/CETATEXT000042543720.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 19/11/2020, 437506, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-11-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>437506</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU ; SCP OHL, VEXLIARD</AVOCATS>
<RAPPORTEUR>M. Bruno Bachini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Stéphane Hoynck</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:437506.20201119</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 8 janvier 2020 au secrétariat du contentieux du Conseil d'Etat, M. C... B..., M. F... A... et M. D... E... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler les décisions par lesquelles la présidente, le rapporteur général et le président de la formation restreinte du haut conseil du commissariat aux comptes ont refusé de faire droit aux demandes qu'ils ont présentées les 25 et 30 septembre 2019 ainsi que les 2, 11 et 25 octobre 2019, tendant à ce qu'il soit sursis à statuer sur leur instance disciplinaire dans l'attente du jugement pénal relatif aux mêmes faits ; <br/>
<br/>
              2°) d'enjoindre au haut conseil de surseoir à statuer sur leur instance disciplinaire dans l'attente de l'issue définitive de la procédure pénale relative aux mêmes faits, sans délai et sous astreinte ;<br/>
<br/>
              3°) de mettre à la charge du haut conseil la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
- la Convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
- le code de commerce ;<br/>
- le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Bachini, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Stéphane Hoynck, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de M. B... et autres et à la SCP Ohl, Vexliard, avocat du haut conseil du commissariat aux comptes ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. A la suite de diverses malversations apparues en 2016 dans les comptes de la société Agripole et de ses principales filiales, MM. B..., A... et E..., commissaires aux comptes de ces sociétés, ont fait l'objet de l'ouverture d'une enquête disciplinaire du haut conseil du commissariat aux comptes. Arguant de l'existence d'une procédure pénale à leur encontre, les intéressés ont demandé par lettres des 25 et 30 septembre 2019 ainsi que des 2, 11 et 25 octobre 2019 à la présidente du haut conseil, à son rapporteur général et au président de sa formation restreinte de suspendre la procédure disciplinaire dans l'attente du jugement pénal à venir. <br/>
<br/>
              2. L'article L. 824-4 du code de commerce dispose que : " Le rapporteur général est saisi de tout fait susceptible de justifier l'engagement d'une procédure de sanction (...). / Le rapporteur général peut également se saisir des signalements dont il est destinataire (...) ". Aux termes de l'article L. 824-8 du même code : " A l'issue de l'enquête et après avoir entendu la personne intéressée, le rapporteur général établit un rapport d'enquête qu'il adresse au Haut conseil. Lorsque les faits justifient l'engagement d'une procédure de sanction, le Haut conseil délibérant hors la présence des membres de la formation restreinte arrête les griefs qui sont notifiés par le rapporteur général à la personne intéressée. / (....) Le rapporteur général établit un rapport final qu'il adresse à la formation restreinte avec les observations de la personne intéressée. ". L'article L. 824-11 du même code précise enfin que : " La formation restreinte convoque la personne poursuivie à une audience qui se tient deux mois au moins après la notification des griefs (...) ". <br/>
<br/>
              3. La requête de MM. B..., A... et E... est dirigée contre les décisions implicites qui résulteraient du silence gardé, d'une part, par la présidente et le rapporteur général du haut conseil et, d'autre part, par le président de la formation restreinte pour le compte de cette formation sur leur demande de suspension des poursuites et de la procédure disciplinaire les concernant. Ces actes, qui ne sont pas détachables de la décision qui sera prise à l'issue de la procédure disciplinaire et ne pourront être contestés par les intéressés qu'au soutien de recours contre les sanctions susceptibles d'être prononcées au terme de celle-ci, ne constituent pas, par eux-mêmes, des décisions susceptibles de faire l'objet d'un recours au cours de la procédure disciplinaire.<br/>
<br/>
              4. Il résulte de ce qui précède que la requête doit être rejetée comme irrecevable.<br/>
<br/>
              5. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge du haut conseil du commissariat aux comptes qui n'est pas, dans la présente instance, la partie perdante. Il y a lieu en revanche, dans les circonstances de l'espèce, de mettre à la charge de MM. B..., A... et E..., une somme de 1 000 euros chacun, à verser au haut conseil au titre de ces mêmes dispositions. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de MM. B..., A... et E... est rejetée.<br/>
Article 2 : MM. B..., A... et E... verseront au haut conseil du commissariat aux comptes une somme de 1 000 euros chacun au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : la présente décision sera notifiée à M. C... B..., à M. F... A..., à M. D... E... et à la présidente du haut conseil du commissariat aux comptes. <br/>
Copie sera adressée au garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
