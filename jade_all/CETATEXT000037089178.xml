<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037089178</ID>
<ANCIEN_ID>JG_L_2018_06_000000409322</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/08/91/CETATEXT000037089178.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  7ème chambre jugeant seule, 20/06/2018, 409322, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-06-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>409322</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 7ème chambre jugeant seule</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ODENT, POULET ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Thomas Pez-Lavergne</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:409322.20180620</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...C...épouse A...a demandé au tribunal administratif de Toulouse, en premier lieu, d'annuler la décision du 30 octobre 2008 par laquelle la Caisse nationale de retraite des agents des collectivités locales (CNRACL) a refusé de faire droit à sa demande de validation de trimestres, et, en second lieu, à ce qu'il soit enjoint à la CNRACL de valider les services tels que formulés dans sa demande du 12 octobre 2008. Par un jugement n° 1201268 du 10 septembre 2015, le tribunal administratif de Toulouse a rejeté sa demande.<br/>
<br/>
              Par une ordonnance n° 15BX03534 du 17 mars 2017, enregistrée le 27 mars 2017 au secrétariat du contentieux du Conseil d'Etat, le président de la cour administrative d'appel de Bordeaux a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi, enregistré le 29 octobre 2015 au greffe de cette cour, présenté par MmeA.... Par ce pourvoi et par un nouveau mémoire, enregistré le 13 octobre 2017 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement du tribunal administratif de Toulouse du 10 septembre 2015 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de la Caisse des dépôts et consignations la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des pensions civiles et militaires de retraite ;<br/>
              - le code de la sécurité sociale ;<br/>
              - le décret n° 2003-1306 du 26 décembre 2003 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thomas Pez-Lavergne, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de Mme A...et à la SCP Odent, Poulet, avocat de la Caisse des dépôts et consignations.<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des énonciations du jugement attaqué que Mme A..., infirmière, a été recrutée sous le statut d'agent contractuel du 1er septembre 1999 au 1er mars 2004 par le centre communal d'action sociale (CCAS) de Luzech ; qu'ayant réussi le concours externe sur titres d'infirmiers territoriaux, elle a été nommée dans le cadre d'emplois des infirmiers de classe normale en qualité de stagiaire pour une année à compter du 1er mars 2004, et titularisée le 1er mai 2005 en qualité d'infirmière diplômée d'Etat de classe normale au 8ème échelon ; que le 12 octobre 2008, Mme A...a présenté une demande de rachat de trimestres auprès de la CNRACL par l'intermédiaire de son employeur, le CCAS de Luzech ; que par un courrier du 30 octobre 2008, dont Mme A...allègue ne pas avoir eu communication, la CNRACL a refusé de faire droit à sa demande de validation au motif que le délai de deux ans suivant la titularisation de l'intéressée, prévu par le I de l'article 50 du décret du 26 décembre 2003, était expiré ; que le recours gracieux de Mme A...contre cette décision a été rejeté par la CNRACL en octobre 2009 ; que, par un jugement du 10 septembre 2015, le tribunal administratif de Toulouse a rejeté la demande de Mme A...tendant à l'annulation de la décision du 30 octobre 2008 ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 8 du décret du 26 décembre 2003 relatif au régime de retraite des fonctionnaires affiliés à la Caisse nationale de retraites des agents des collectivités locales : " Les services pris en compte dans la constitution du droit à pension sont : (...) 2° Les périodes de services dûment validées. Est admise à validation toute période de services, quelle qu'en soit la durée, effectués en qualité d'agent non titulaire de l'une des collectivités mentionnées aux 1°, 2° et 3° de l'article L. 86-1 du code des pensions civiles et militaires de retraite (...) " ; qu'aux termes du I de l'article 50 du même décret : " La validation des services visés à l'article 8 doit être demandée dans les deux années qui suivent la date de la notification de la titularisation. Chaque nouvelle titularisation dans un grade ouvre un délai de deux années pour demander la validation de l'ensemble des services. Pour les fonctionnaires titulaires occupant un emploi à temps non complet, le délai de deux ans court à compter de l'affiliation au régime de la Caisse nationale de retraites des agents des collectivités locales (...) " ;<br/>
<br/>
              3. Considérant que l'administration n'est pas tenue de donner aux agents une information particulière sur les droits spécifiques qu'ils pourraient éventuellement revendiquer en application des textes législatifs et réglementaires relatifs aux pensions civiles et militaires de retraite ; que les dispositions du décret du 26 décembre 2003 dont le bénéfice est invoqué ont fait, au moment de leur adoption, l'objet d'une publication régulière par insertion au Journal officiel ; qu'aucune autre mesure de publicité n'incombait à l'administration ; que Mme A...n'est, dès lors, pas fondée à soutenir que, faute pour l'administration de l'avoir informée de l'existence de ces dispositions, le délai de deux ans fixé par l'article 50 du décret du 26 décembre 2003 précité ne lui serait pas opposable ; que, par suite, le tribunal administratif de Toulouse n'a pas commis d'erreur de droit en jugeant que ce délai lui était applicable alors même que l'administration ne l'avait pas informée de l'existence des dispositions précitées ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que le pourvoi de Mme A...doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de Mme A...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à Mme B...A...et à la Caisse des dépôts et consignations.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
