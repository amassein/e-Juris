<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031640785</ID>
<ANCIEN_ID>JG_L_2015_12_000000390335</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/64/07/CETATEXT000031640785.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème / 2ème SSR, 16/12/2015, 390335, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-12-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>390335</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème / 2ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ODENT, POULET ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. François Lelièvre</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:390335.20151216</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Paris, le 19 décembre 2012, d'annuler la décision du 30 novembre 2012 du président du Sénat rejetant sa demande tendant à l'organisation d'élections professionnelles dans des conditions permettant d'assurer la représentativité syndicale au Sénat. Par un jugement n° 1221782/5-1 du 3 avril 2014, le tribunal administratif de Paris a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 14PA02429 du 23 mars 2015, la cour administrative d'appel de Paris a rejeté la requête du 3 juin 2014 par laquelle M. B...lui a demandé d'annuler ce jugement ainsi que la décision du 30 novembre 2012 du président du Sénat.<br/>
<br/>
              Par un pourvoi et un mémoire en réplique, enregistrés les 21 mai et 5 octobre 2015 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - l'ordonnance n° 58-1100 du 17 novembre 1958 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Lelièvre, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Odent, Poulet, avocat de M. A...B...et à la SCP Lyon-Caen, Thiriez, avocat du président du Sénat ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article 8 de l'ordonnance du 17 novembre 1958 relative au fonctionnement des assemblées parlementaires : " (...) Les agents titulaires des services des assemblées parlementaires sont des fonctionnaires de l'Etat dont le statut et le régime de retraite sont déterminés par le bureau de l'assemblée intéressée, après avis des organisations syndicales représentatives du personnel. Ils sont recrutés par concours selon des modalités déterminées par les organes compétents des assemblées. La juridiction administrative est appelée à connaître de tous litiges d'ordre individuel concernant ces agents, et se prononce au regard des principes généraux du droit et des garanties fondamentales reconnues à l'ensemble des fonctionnaires civils et militaires de l'Etat visées à l'article 34 de la Constitution. La juridiction administrative est également compétente pour se prononcer sur les litiges individuels en matière de marchés publics. / Dans les instances ci-dessus visées, qui sont les seules susceptibles d'être engagées contre une assemblée parlementaire, l'Etat est représenté par le président de l'assemblée intéressée qui peut déléguer cette compétence aux questeurs (...) " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M.B..., fonctionnaire du Sénat, a demandé au président de cette assemblée que les élections professionnelles y soient organisées dans des conditions permettant d'assurer la représentation des organisations syndicales selon les mêmes modalités que celles qui sont applicables aux fonctionnaires de l'Etat régis par le statut général de la fonction publique ; que, par une décision du 30 novembre 2012, le président du Sénat a rejeté cette demande ; que la demande de M. B... ne pouvait être regardée que comme tendant à la modification des dispositions du règlement du Sénat prévoyant l'élection des représentants du personnel dans les organismes consultatifs ; qu'ainsi, en jugeant que le litige dont M. B...avait saisi la juridiction administrative  n'était pas un litige d'ordre individuel au sens des dispositions de l'article 8 de l'ordonnance du 17 novembre 1958 et que sa demande devant le tribunal administratif de Paris était, par suite, irrecevable, la cour administrative d'appel de Paris n'a pas entaché son arrêt d'erreur de qualification juridique ou d'erreur de droit ;<br/>
<br/>
              3. Considérant qu'il résulte de ce qui précède que M. B...n'est pas fondé à demander l'annulation de l'arrêt attaqué, qui est suffisamment motivé ;<br/>
<br/>
              4. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante, le versement des sommes que demande, à ce titre, M. B...; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées au titre des mêmes dispositions par le président du Sénat ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. B...est rejeté.<br/>
<br/>
Article 2 : Les conclusions présentées par le président du Sénat au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. A...B...et au président du Sénat.<br/>
Copie en sera adressée au secrétaire d'Etat aux relations avec le Parlement.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
