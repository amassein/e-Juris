<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027689945</ID>
<ANCIEN_ID>JG_L_2013_07_000000360397</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/68/99/CETATEXT000027689945.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 10/07/2013, 360397</TITRE>
<DATE_DEC>2013-07-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>360397</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR>M. David Gaudillère</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Damien Botteghi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:360397.20130710</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu 1°, sous le n° 360397, la requête sommaire et le mémoire complémentaire, enregistrés les 21 juin et 21 septembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour ATetT Global Network Services France SAS, dont le siège est Tour Egée, 9-11 allée de l'Arche à Courbevoie (92400), représentée par son président en exercice, et pour ATetT Global Network Services LLC, dont le siège est One ATetT Way à Bedminster NJ 07921 (Etats-Unis) ; les sociétés requérantes demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision n° 2012-0366 du 29 mars 2012 de l'Autorité de régulation des communications électroniques et des postes (ARCEP) relative à la mise en place d'une collecte d'informations sur les conditions techniques et tarifaires de l'interconnexion et de l'acheminement de données ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement à chacune de la somme de 6 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu 2°, sous le n° 360398, la requête sommaire et le mémoire complémentaire, enregistrés les 21 juin et 21 septembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour Verizon France, dont le siège est Tour Franklin, La Défense 8 100-101 terrasse Boieldieu à Puteaux (92800), représentée par son président en exercice, et pour MCI Communications Services, dont le siège est One Verizon Way, Basking Ridge NJ 07920 (Etats-Unis) ; les sociétés requérantes demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision n° 2012-0366 du 29 mars 2012 de l'Autorité de régulation des communications électroniques et des postes (ARCEP) relative à la mise en place d'une collecte d'informations sur les conditions techniques et tarifaires de l'interconnexion et de l'acheminement de données ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement à chacune de la somme de 6 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 25 juin 2013, présentée pour ATetT Global Network Services France SAS et pour ATetT Global Network Services LLC ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 25 juin 2013, présentée pour Verizon France et pour MCI Communications Services ;<br/>
<br/>
              Vu la directive 2002/20/CE du Parlement européen et du Conseil du 7 mars 2002 ;<br/>
<br/>
              Vu la directive 2002/21/CE du Parlement européen et du Conseil du 7 mars 2002 ;<br/>
<br/>
              Vu la directive 2009/140/CE du Parlement européen et du Conseil du 25 novembre 2009 ;<br/>
<br/>
              Vu le code des postes et des communications électroniques ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. David Gaudillère, Auditeur,<br/>
<br/>
              - les conclusions de M. Damien Botteghi, Rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delaporte, Briard, Trichet, avocat des sociétés ATetT Global Network Services France SAS, ATetT Global Network Services France LLC, Verizon France et MCI Communications services ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant que les requêtes des sociétés ATetT Global Network Services France SAS, ATetT Global Network Services LLC, Verizon France et MCI Communications Services sont dirigées contre la même décision n° 2012-0366 de l'Autorité de régulation des communications électroniques et des postes (ARCEP) du 29 mars 2012 relative à la mise en place d'une collecte d'informations sur les conditions techniques et tarifaires de l'interconnexion et de l'acheminement de données ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              Sur la compétence de l'ARCEP :<br/>
<br/>
              2.	Considérant qu'aux termes de l'article L. 36-13 du code des postes et des communications électroniques : " L'Autorité de régulation des communications électroniques et des postes recueille les informations et procède aux enquêtes nécessaires à l'exercice de ses missions, dans les limites et conditions fixées par l'article L. 32-4 " ; qu'aux termes de l'article L. 32-4 de ce code, dans sa rédaction applicable à la date de la décision attaquée : " Le ministre chargé des communications électroniques et l'Autorité de régulation des communications électroniques et des postes peuvent, de manière proportionnée aux besoins liés à l'accomplissement de leurs missions, et sur la base d'une décision motivée : / 1° Recueillir auprès des personnes physiques ou morales exploitant des réseaux de communications électroniques ou fournissant des services de communications électroniques les informations ou documents nécessaires pour s'assurer du respect par ces personnes des principes définis aux articles L. 32-1 et L. 32-3, ainsi que des obligations qui leur sont imposées par le présent code ou par les textes pris pour son application ; / 2° Recueillir auprès de personnes fournissant des services de communication au public en ligne les informations ou documents concernant les conditions techniques et tarifaires d'acheminement du trafic appliquées à leurs services (...) " ; <br/>
<br/>
              3.	Considérant que, par l'article 1er de la décision attaquée, l'ARCEP a institué un mécanisme de collecte semestrielle de certaines informations relatives aux conditions techniques et tarifaires d'interconnexion et d'acheminement de données auprès des opérateurs de communications électroniques soumis à l'obligation de se déclarer auprès de l'Autorité au titre de l'article L. 33-1 du code des postes et des communications électroniques ; que, par l'article 2 de la décision attaquée, l'ARCEP a prévu qu'elle pourrait, lorsqu'elle l'estimera nécessaire pour vérifier ou compléter les informations recueillies au titre de l'article 1er, recueillir des informations relatives aux conditions techniques et tarifaires d'interconnexion et d'acheminement de données auprès d'opérateurs de communications électroniques non visés à l'article 1er ou des personnes fournissant un service de communication au public en ligne qui ont engagé une démarche active afin que leurs services ou contenus soient utilisés ou consultés par des utilisateurs finals situés en France, lorsque ces opérateurs ou personnes détiennent au moins un système autonome interconnecté avec au moins deux autres systèmes autonomes et disposent d'une relation d'interconnexion ou d'acheminement de données avec au moins un opérateur de communications électroniques visé à l'article 1er de la décision ;<br/>
<br/>
              4.	Considérant qu'il résulte des dispositions des articles L. 36-13 et L. 32-4 du code des postes et des communications électroniques que l'ARCEP peut décider, sur la base d'une décision motivée, de recueillir auprès des exploitants de réseaux de communications électroniques ou des personnes fournissant des services de communications électroniques les informations nécessaires pour s'assurer du respect des principes mentionnés aux articles L. 32-1 et L. 32-3 et de recueillir auprès des personnes fournissant des services de communication au public en ligne les informations concernant les conditions techniques et tarifaires d'acheminement du trafic appliquées à leurs services ; que le pouvoir ainsi conféré à l'ARCEP implique que l'Autorité puisse organiser auprès des personnes visées une collecte périodique d'informations si ces dernières sont nécessaires à l'accomplissement de ses missions ; que les sociétés requérantes ne sauraient utilement se prévaloir des dispositions de l'article L. 36-6 du code des postes et des communications électroniques, qui définissent le pouvoir réglementaire dont dispose l'ARCEP dans les matières énumérées à cet article, ou de celles du point a) du 2 de l'article D. 98-11 du même code, qui sont relatives au recueil d'informations par l'Autorité aux fins de vérifier le respect des règles auxquelles sont soumis, en application de l'article L. 33-1 de ce code, au stade de la déclaration préalable et pour le suivi de l'activité déclarée, les exploitants de réseaux ouverts au public et les fournisseurs de services de communications électroniques ; <br/>
<br/>
              5.	Considérant qu'au nombre des principes mentionnés au II de l'article L. 32-1 figurent notamment, au 4°, la définition de conditions d'accès aux réseaux ouverts au public et d'interconnexion de ces réseaux qui garantissent la possibilité pour tous les utilisateurs de communiquer librement et l'égalité des conditions de concurrence, au 4° bis, l'absence de discrimination dans les relations entre opérateurs et fournisseurs de services de communications au public en ligne pour l'acheminement du trafic et l'accès à ces services et, au 15°, la capacité des utilisateurs finals à accéder à l'information et à en diffuser ainsi qu'à accéder aux applications et services de leur choix ; qu'il s'ensuit que l'ARCEP tenait des articles L. 36-13 et L. 32-4 compétence pour fixer les règles d'une collecte semestrielle de certaines informations relatives aux conditions techniques et tarifaires d'interconnexion et d'acheminement de données auprès d'opérateurs de communications électroniques soumis à l'obligation de se déclarer auprès d'elle au titre de l'article L. 33-1 ; que l'Autorité pouvait, de même, prévoir de recueillir sur demande, aux fins de vérifier ou de compléter les informations recueillies par le biais de la collecte périodique, des informations de même nature auprès des opérateurs non soumis à l'obligation de collecte ou des fournisseurs de services de communication au public en ligne ayant engagé une démarche active afin que leurs services et contenus soient utilisés ou consultés en France, dès lors que ces opérateurs ou fournisseurs disposent d'une relation d'interconnexion ou d'acheminement de données avec un opérateur soumis à la collecte périodique d'informations ;<br/>
<br/>
              6.	Considérant que si, comme le font valoir les sociétés requérantes, certains des opérateurs de communications électroniques et des personnes fournissant un service de communication au public en ligne auprès desquels l'ARCEP envisage de recueillir des informations complémentaires en vertu de l'article 2 de la décision attaquée peuvent ne pas être établis en France, cette circonstance est dénuée d'incidence sur la compétence que tient l'ARCEP des dispositions des articles L. 36-13 et L. 32-4 pour recueillir des informations nécessaires à l'exercice de ses missions ; que l'article 2 de la décision attaquée ne vise que des opérateurs ou fournisseurs disposant d'une relation d'interconnexion ou d'acheminement de données avec au moins un opérateur de communications électroniques soumis à l'obligation de se déclarer auprès de l'Autorité au titre de l'article L. 33-1 du même code ; que les informations en cause se rapportent ainsi à l'exercice par l'ARCEP des missions qui lui ont été assignées par la loi ; <br/>
<br/>
              7.	Considérant qu'il résulte de ce qui précède que le moyen tiré de l'incompétence de l'ARCEP pour adopter la décision attaquée ne peut qu'être écarté ;<br/>
<br/>
              8.	Considérant, en outre, que la décision attaquée, qui se borne à organiser une collecte d'informations relatives aux conditions techniques et tarifaires d'interconnexion et d'acheminement de données, n'a ni pour objet ni pour effet, contrairement à ce que soutiennent les sociétés requérantes, de mettre en place une régulation ex ante des marchés d'interconnexion et d'acheminement de données ; que si, au surplus, les sociétés requérantes font valoir que le marché des échanges de trafic sur Internet ne ferait pas partie des marchés susceptibles de faire l'objet d'une régulation ex ante en vertu de la recommandation de la Commission européenne du 17 décembre 2007 concernant les marchés pertinents de produits et de services dans le secteur des communications électroniques susceptibles d'être soumis à une réglementation ex ante conformément à la directive 2002/21/CE du Parlement européen et du Conseil relative à un cadre réglementaire commun pour les réseaux et services de communications électroniques, les termes du point 2) de cette recommandation prévoient, en tout état de cause, la possibilité pour les autorités réglementaires nationales de recenser des marchés pertinents autres que ceux énumérés en son annexe ; que, par suite, le moyen tiré de ce que l'ARCEP ne disposerait d'aucune base légale pour mettre en place, s'agissant des prestations considérées, une régulation ex ante, ne peut qu'être écarté ;<br/>
<br/>
              9.	Considérant qu'il en va de même pour le moyen tiré de la méconnaissance de " l'objectif de dérégulation " qui résulterait des termes du f) du paragraphe 5 de l'article 8 de la directive 2002/21/CE du Parlement européen et du Conseil du 7 mars 2002, dans sa rédaction issue de la directive modificative 2009/140/CE du Parlement européen et du Conseil du 25 novembre 2009 ; <br/>
<br/>
              Sur la proportionnalité de la mesure :<br/>
<br/>
              10.	Considérant qu'il résulte de l'article L. 32-4 du code des postes et des communications électroniques que l'ARCEP ne peut procéder au recueil d'informations sur le fondement de ces dispositions que de manière proportionnée aux besoins liés à l'accomplissement de ses missions ;<br/>
<br/>
              11.	Considérant qu'il ressort des termes de la décision attaquée et du questionnaire qui lui est annexé que les informations devant être transmises à l'ARCEP dans le cadre de la collecte périodique portent sur l'identification des systèmes autonomes interconnectés, sur l'identité des personnes qui les détiennent et le type de relations nouées avec elles, sur les points ou sites d'interconnexion et leurs capacités, sur les conditions financières et tarifs de l'interconnexion et les flux échangés au cours du semestre ; que ces informations sont en nombre limité, simples à renseigner pour les personnes concernées, et exigées selon une fréquence semestrielle ; qu'elles sont ainsi proportionnées ; qu'au demeurant, l'ARCEP, après la consultation publique qui s'est déroulée du 23 décembre 2011 au 17 février 2012, a pris en compte les objections formulées par les opérateurs et adopté, dans la décision attaquée, des dispositions moins contraignantes, en termes de fréquence et de périmètre de la collecte, que celles qu'elle avait initialement soumises à la consultation ; <br/>
<br/>
              12.	Considérant qu'il résulte de ce qui précède que le moyen tiré de ce que la décision attaquée méconnaîtrait l'exigence de proportionnalité résultant de l'article L. 32-4 du code des postes et des communications électroniques, qui a procédé à la transposition des directives 2002/21/CE et 2002/20/CE, ne peut qu'être écarté ; qu'il ressort d'ailleurs des pièces du dossier que l'ARCEP relève, sans être contredite, que près de cinquante opérateurs, dont la société ATetT Global Network Services France SAS, ont transmis à l'Autorité les informations requises dans le questionnaire joint à la décision attaquée au titre du second semestre de l'année 2012, sans signaler aucune difficulté particulière ;<br/>
<br/>
              Sur les autres moyens :<br/>
<br/>
              13.	Considérant, en premier lieu, que les dispositions de l'article 2 de la décision attaquée mentionnant les personnes physiques ou morales qui " détiennent " un système autonome désignent sans ambiguïté les personnes qui contrôlent de fait ce système ; que, dès lors, contrairement à ce que soutiennent les sociétés requérantes, les termes utilisés par la décision attaquée ne sauraient être regardés comme méconnaissant l'objectif de valeur constitutionnelle de clarté et d'intelligibilité de la loi ;<br/>
<br/>
              14.	Considérant, en second lieu, que les requérantes soutiennent que la décision attaquée aurait été prise en méconnaissance du principe de sécurité juridique, en ce qu'elle aurait méconnu le principe de confiance légitime et n'aurait pas été accompagnée de mesures transitoires ; que, toutefois, eu égard à l'objet et à la portée de la décision attaquée, les sociétés requérantes ne sauraient soutenir que le principe de confiance légitime était de nature à faire obstacle à son intervention, non plus que cette intervention aurait dû être accompagnée de mesures transitoires ; <br/>
<br/>
              15.	Considérant qu'il résulte de tout ce qui précède que, sans qu'il soit besoin de statuer sur les fins de non-recevoir opposées par l'ARCEP, les sociétés requérantes ne sont pas fondées à demander l'annulation pour excès de pouvoir de la décision qu'elles attaquent ; que les dispositions de l'article L. 761-1 du code de justice administrative font, par suite, obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
  Article 1er : Les requêtes n°s 360397 et 360398 sont rejetées.<br/>
<br/>
Article 2 : La présente décision sera notifiée à ATetT Global Network Services France SAS, à ATetT Global Network Services LLC, à MCI Communications Services, à Verizon France et à l'Autorité de régulation des communications électroniques et des postes. Copie en sera adressée, pour information, au ministre du redressement productif.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">51-005 POSTES ET COMMUNICATIONS ÉLECTRONIQUES. - 1) POSSIBILITÉ POUR L'ARCEP DE RECUEILLIR AUPRÈS DES EXPLOITANTS DE RÉSEAUX LES INFORMATIONS NÉCESSAIRES POUR S'ASSURER DU RESPECT DES PRINCIPES MENTIONNÉS AUX ARTICLES L. 32-1 ET L. 32-3 DU CPCE ET, AUPRÈS DES FOURNISSEURS DE SERVICES DE COMMUNICATION AU PUBLIC EN LIGNE, LES INFORMATIONS CONCERNANT LES CONDITIONS TECHNIQUES ET TARIFAIRES D'ACHEMINEMENT DU TRAFIC APPLIQUÉES À LEURS SERVICES (NEUTRALITÉ DE RÉSEAU) - EXISTENCE - 2) POSSIBILITÉ POUR L'ARCEP D'ORGANISER, PAR DÉCISION RÉGLEMENTAIRE, LA COLLECTE PÉRIODIQUE D'INFORMATIONS NÉCESSAIRES À L'EXERCICE DE SES MISSIONS - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">51-02-03 POSTES ET COMMUNICATIONS ÉLECTRONIQUES. COMMUNICATIONS ÉLECTRONIQUES. INTERNET. - 1) POSSIBILITÉ POUR L'ARCEP DE RECUEILLIR AUPRÈS DES EXPLOITANTS DE RÉSEAUX LES INFORMATIONS NÉCESSAIRES POUR S'ASSURER DU RESPECT DES PRINCIPES MENTIONNÉS AUX ARTICLES L. 32-1 ET L. 32-3 DU CPCE ET, AUPRÈS DES FOURNISSEURS DE SERVICES DE COMMUNICATION AU PUBLIC EN LIGNE, LES INFORMATIONS CONCERNANT LES CONDITIONS TECHNIQUES ET TARIFAIRES D'ACHEMINEMENT DU TRAFIC APPLIQUÉES À LEURS SERVICES (NEUTRALITÉ DE RÉSEAU) - EXISTENCE - 2) POSSIBILITÉ POUR L'ARCEP D'ORGANISER, PAR DÉCISION RÉGLEMENTAIRE, LA COLLECTE PÉRIODIQUE D'INFORMATIONS NÉCESSAIRES À L'EXERCICE DE SES MISSIONS - EXISTENCE.
</SCT>
<ANA ID="9A"> 51-005 1) Il résulte des dispositions des articles L. 36-13 et L. 32-4 du code des postes et des communications électroniques (CPCE) que l'ARCEP peut décider, sur la base d'une décision motivée, de recueillir auprès des exploitants de réseaux de communications électroniques ou des personnes fournissant des services de communications électroniques les informations nécessaires pour s'assurer du respect des principes mentionnés aux articles L. 32-1 et L. 32-3 de ce code et de recueillir auprès des personnes fournissant des services de communication au public en ligne les informations concernant les conditions techniques et tarifaires d'acheminement du trafic appliquées à leurs services.,,,2) Le pouvoir ainsi conféré à l'ARCEP implique que l'Autorité puisse, par décision réglementaire, organiser, auprès des personnes visées, une collecte périodique d'informations si ces dernières sont nécessaires à l'accomplissement de ses missions.</ANA>
<ANA ID="9B"> 51-02-03 1) Il résulte des dispositions des articles L. 36-13 et L. 32-4 du code des postes et des communications électroniques (CPCE) que l'Autorité de régulation des communications électroniques et des postes (ARCEP) peut décider, sur la base d'une décision motivée, de recueillir auprès des exploitants de réseaux de communications électroniques ou des personnes fournissant des services de communications électroniques les informations nécessaires pour s'assurer du respect des principes mentionnés aux articles L. 32-1 et L. 32-3 de ce code et de recueillir auprès des personnes fournissant des services de communication au public en ligne les informations concernant les conditions techniques et tarifaires d'acheminement du trafic appliquées à leurs services.,,,2) Le pouvoir ainsi conféré à l'ARCEP implique que l'Autorité puisse, par décision réglementaire, organiser, auprès des personnes visées, une collecte périodique d'informations si ces dernières sont nécessaires à l'accomplissement de ses missions.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
