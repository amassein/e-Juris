<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035709766</ID>
<ANCIEN_ID>J1_L_2017_09_000001602305</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/70/97/CETATEXT000035709766.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>CAA de PARIS, 9ème chambre, 28/09/2017, 16PA02305, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-09-28</DATE_DEC>
<JURIDICTION>CAA de PARIS</JURIDICTION>
<NUMERO>16PA02305</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. JARDIN</PRESIDENT>
<AVOCATS>SCP KLEIN</AVOCATS>
<RAPPORTEUR>M. David  DALLE</RAPPORTEUR>
<COMMISSAIRE_GVT>M. PLATILLERO</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
       Procédure contentieuse antérieure :<br/>
<br/>
       La société FICI a demandé au Tribunal administratif de Nice, par deux demandes transmises au Tribunal administratif de Paris par deux ordonnances du président de la 3ème chambre du Tribunal administratif de Nice prises sur le fondement de l'article R. 351-3 du code de justice administrative, la décharge de la cotisation supplémentaire d'impôt sur les sociétés à laquelle elle a été assujettie au titre de l'année 2009 et des pénalités correspondantes. <br/>
<br/>
       Par un jugement nos 1517178 et 1518865/1-1 du 1er juin 2016, le Tribunal administratif de Paris a fixé à 345 000 euros au lieu de 1 476 151 euros la minoration de recettes relevée par le service à l'encontre de la société FICI à l'occasion de la cession par celle-ci, le 5 juin 2009, de la villa " Château Latour ", accordé en conséquence une décharge partielle à la société FICI et rejeté le surplus des conclusions de sa demande. <br/>
<br/>
       Procédure devant la Cour :<br/>
<br/>
       Par une requête, enregistrée le 18 juillet 2016, la société FICI, représentée par Me Ciussi, demande à la Cour :<br/>
<br/>
       1°) d'annuler le jugement nos 1517178 et 1518865/1-1 du 1er juin 2016 du Tribunal administratif de Paris, en tant qu'il n'a pas entièrement fait droit à ses conclusions en décharge ; <br/>
<br/>
       2°) de lui accorder la décharge des impositions et pénalités restant en litige ;<br/>
<br/>
       3°) de mettre une somme de 5 500 euros à la charge de l'Etat au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
       Elle soutient que : <br/>
<br/>
       - la provision qu'elle avait comptabilisée au titre de l'année 2009 remplissait toutes les conditions de déductibilité prévues par les articles 39 et 209 du code général des impôts ; <br/>
       - s'agissant du redressement relatif à la vente du " Château Latour " :<br/>
       - les huit termes de comparaison retenus par le service ne sont pas pertinents ;<br/>
       - la dépréciation de l'immeuble liée à la privation de vue s'établit à 40 % de la valeur vénale de l'immeuble, ainsi qu'il ressort du rapport de l'expert Zervudacki ; en tenant compte de cette dépréciation et de la détérioration environnementale, qui s'élève à 80 000 euros, la valeur du bien en 2009 était de 880 000 euros, soit un prix comparable à celui auquel il a été vendu ;<br/>
       - l'immeuble doit être évalué sur la base de 165 m² et non de 320 m² ou 339 m² ;<br/>
       - l'administration fiscale elle-même a admis en 2005, dans une procédure de rectification relative à la réintégration d'une provision, que la valeur du bien était de 650 000 euros ;<br/>
       - il ressort des différents rapports d'expertise établis dans le cadre des instances qu'elle a engagées auprès du juge judiciaire et du juge administratif que le prix de 900 000 euros n'était pas sous-évalué ;<br/>
       - dans son arrêt du 12 janvier 2010, la Cour d'appel d'Aix-en-Provence a nécessairement évalué le bien à 1 000 000 d'euros ; <br/>
       - l'acheteur du bien en 2009 l'a revendu 1 656 000 euros le 24 janvier 2014, après avoir pourtant effectué des travaux d'un montant de 758 130 euros ;<br/>
       - en admettant que l'administration puisse être regardée comme établissant l'existence d'un écart significatif entre le prix convenu et la valeur vénale du bien, elle n'établit pas en tout état de cause l'existence d'une intention libérale de la part de la société ; en effet, elle n'avait pas de lien avec l'acquéreur du bien et des raisons objectives justifiaient qu'elle consente une baisse de prix ;<br/>
       - la décision d'appliquer la majoration pour manquement délibéré n'est pas motivée ; elle est injustifiée dès lors que l'administration n'établit pas sa mauvaise foi. <br/>
<br/>
       Par un mémoire, enregistré le 5 avril 2017, le ministre de l'économie et des finances conclut au rejet de la requête et, par voie d'appel incident, à l'annulation des articles 1er, 2 et 3 du jugement attaqué et au rétablissement des impositions déchargées par le Tribunal administratif de Paris.  <br/>
<br/>
       Il soutient que :<br/>
<br/>
       - la société requérante ne justifie pas de la déductibilité de la provision qu'elle a comptabilisée au titre de l'année 2009 ;<br/>
       - la vente d'un bien à un prix minoré peut relever d'une gestion anormale, même en l'absence d'intention libérale, dès lors que le vendeur ne justifie pas de l'existence d'une contrepartie au prix minoré ; <br/>
       - en l'espèce, la société ne justifie pas de l'existence d'une contrepartie à l'écart de prix constaté ;<br/>
       - les termes de comparaison retenus par le service sont pertinents ;<br/>
       - les expertises dont se prévaut la requérante sont soit très antérieures à la date de cession, soit postérieures et entrent dans le cadre strict de l'évaluation du préjudice subi par la société en 1992 du fait de l'extension de la villa voisine ;<br/>
       - ce préjudice a été évalué à 355 000 euros par la Cour administrative d'appel de Marseille ; <br/>
       - l'application de la majoration pour manquement délibéré est justifiée dès lors que le prix de cession était largement inférieur au prix du marché, ce que la société ne pouvait ignorer eu égard à sa qualité de professionnel de l'immobilier ;<br/>
       - le Tribunal administratif s'est fondé sur un rapport d'expertise qui n'avait pas pour objet de déterminer la valeur du bien immobilier Château Latour mais d'évaluer le préjudice subi par la société du fait des constructions réalisées sur la propriété voisine ;<br/>
       - l'expert Galinelli a délibérément choisi la valeur au mètre carré la plus basse ; <br/>
       - le service, quant à lui, a retenu le prix de 8 047 euros cité par l'expert pour un bien de grand standing, dès lors que ce prix se rapprochait le plus de celui de 8 861 euros, résultant de la méthode par comparaison utilisée initialement. <br/>
<br/>
       Vu les autres pièces du dossier.<br/>
<br/>
       Vu :<br/>
       - le code général des impôts et le livre des procédures fiscales ;<br/>
       - le code de justice administrative.<br/>
<br/>
       Les parties ont été régulièrement averties du jour de l'audience.<br/>
<br/>
       Ont été entendus au cours de l'audience publique :<br/>
       - le rapport de M. Dalle,<br/>
       - les conclusions de M. Platillero, rapporteur public,<br/>
       - et les observations de Me Ciussi, avocate de la société Fici.<br/>
<br/>
       1. Considérant que la société FICI, qui exerçait une activité de marchand de biens et de prestations immobilières, a fait l'objet en 2011 d'une vérification de comptabilité portant sur les années 2008 et 2009, à l'issue de laquelle l'administration a mis à sa charge un complément d'impôt sur les sociétés au titre de l'année 2009, en conséquence de la réintégration dans le résultat de l'exercice clos le 31 décembre 2009, d'une part, d'une provision d'un montant de 55 327 euros comptabilisée par la société en vue de faire face au risque d'être appelée en paiement solidaire de charges sociales dues à l'URSSAF par l'un de ses anciens agents commerciaux, d'autre part, d'une minoration de recettes, s'élevant à 1 476 151 euros, correspondant à la différence entre le prix estimé du marché et le prix auquel la société a vendu, le 5 juin 2009, un immeuble lui appartenant, situé à Vallauris (Alpes-maritimes) ; que, par un jugement du 1er juin 2016, le Tribunal administratif de Paris a estimé que la minoration de recettes relevée à l'encontre de la société FICI devait être limitée à la somme de 345 000 euros ; qu'il a en conséquence réduit l'imposition supplémentaire assignée à la société et rejeté le surplus de ses conclusions en décharge ; que la société FICI relève appel de ce jugement en tant qu'il lui est défavorable ; que, par voie d'appel incident, le ministre de l'économie et des finances demande à la Cour de rétablir l'imposition dont la décharge a, selon lui, été prononcée à tort par les premiers juges ;<br/>
<br/>
<br/>
       Sur la provision de 55 327 euros :<br/>
<br/>
       2. Considérant que la société FICI reprend en appel les moyens qu'elle avait invoqués en première instance et tirés de ce que le risque de défaillance de l'agent commercial était établi dès lors qu'il avait quitté le territoire français en s'abstenant de régler ses charges sociales et de ce que le montant de la provision résultait d'un calcul de son expert-comptable ; que ces moyens doivent être écartés par adoption des motifs retenus à bon droit par les premiers juges, en l'absence de tout élément nouveau de droit ou de fait produit en appel ;<br/>
<br/>
       Sur la minoration de recettes : <br/>
<br/>
       3. Considérant que la société FICI a cédé le 5 juin 2009 la villa " Château Latour " à Vallauris, au prix de 900 000 euros ; que l'administration fiscale a estimé que la valeur vénale de ce bien était en réalité de 2 376 151 euros et qu'en le cédant à un prix nettement inférieur, la société FICI avait accompli un acte qui n'était pas compatible avec une gestion normale de cette entreprise ; qu'elle a, en conséquence, réintégré la différence d'un montant de 1 476 151 euros au bénéfice imposable de l'exercice 2009 ;<br/>
<br/>
       4. Considérant qu'en vertu des dispositions combinées des articles 38 et 209 du code général des impôts, le bénéfice imposable à l'impôt sur les sociétés est celui qui provient des opérations de toute nature faites par l'entreprise, à l'exception de celles qui, en raison de leur objet ou de leurs modalités, sont étrangères à une gestion normale ; que le fait, pour une entreprise, de renoncer à des recettes en cédant un bien à un prix notablement inférieur à sa valeur réelle ne relève pas, en règle générale, d'une gestion normale, sauf s'il apparaît qu'en consentant un tel avantage, l'entreprise a agi dans son propre intérêt ; que s'il appartient à l'administration d'apporter la preuve des faits sur lesquels elle se fonde pour estimer que l'avantage consenti sans contrepartie à l'occasion de cette cession constitue un acte anormal de gestion, à concurrence de l'insuffisance du prix stipulé, elle est réputée apporter cette preuve dès lors que cette entreprise n'est pas en mesure de justifier qu'elle a bénéficié en retour de contreparties ;<br/>
<br/>
       5. Considérant qu'il résulte de l'instruction que le service vérificateur a initialement déterminé la valeur vénale de l'immeuble en utilisant la méthode comparative, en appliquant à la superficie connue de la villa, soit 320 m², la moyenne du prix au m² de huit termes de comparaison, soit 8 861 euros, pour obtenir un prix de 2 835 520 euros ; qu'à l'issue du recours hiérarchique, il a modifié son calcul pour tenir compte d'éléments figurant dans un rapport d'expertise en date du 13 mai 2011, établi à la demande de la Cour administrative d'appel de Marseille en vue de déterminer la dépréciation de l'immeuble consécutive à l'édification de constructions illégales sur un terrain proche, ayant partiellement occulté la vue sur mer de la villa ; que le service a en conséquence appliqué un prix moyen de 8 047 euros le m² mentionné par l'expert pour un bien de grand standing à une surface corrigée de 339 m², également mentionnée par l'expert, et a obtenu, après déduction d'une dépréciation fixée par l'expert à 355 000 euros pour privation partielle de vue et détérioration environnementale, une valeur vénale de 2 376 151 euros ; que, toutefois, l'administration ne précisant pas la raison pour laquelle elle avait retenu le prix de 8 047 euros, qui était le prix le plus élevé de ceux mentionnés par l'expert pour des biens similaires, le Tribunal administratif a retenu, pour l'immeuble, la valeur de 1 600 000 euros indiquée par l'expert, à laquelle il a appliqué l'abattement pour dépréciation de 355 000 euros ; que le Tribunal a ainsi obtenu une valeur vénale de 1 245 000 euros ; <br/>
<br/>
       6. Considérant que, devant la Cour, le ministre conteste cette évaluation, en faisant valoir que le prix de 8 047 euros était réaliste dès lors qu'il était le plus proche de celui de 8 861 euros retenu jusque là et ressortant des termes de comparaison ; que, cependant, les termes de comparaison retenus par le service sont contestables dès lors qu'ils concernent ou bien des appartements non comparables à la villa litigieuse, ou bien des villas situées dans des secteurs éloignés de Vallauris, dotées de jardins sensiblement plus grands et ne comportant pas les mêmes nuisances, liées à la circulation automobile et ferroviaire ; que, par ailleurs, le prix de 1 245 000 euros n'apparaît pas insuffisant pour un bien qui, certes, n'est distant de la mer que de 100 mètres mais est séparé de celle-ci par une route nationale et une voie ferrée à grande circulation et, depuis les années 1990, par un mur anti bruit destiné à une habitation contigüe au rivage, qui occulte une partie de la vue sur mer de la villa " Château Latour ", est régulièrement recouvert de graffitis et renvoie vers la villa les bruits de la circulation ; que, par ailleurs, le terrain d'assiette de la villa n'est que de 820 m² ; que, par ailleurs, il est constant que la villa, vendue par la société FICI en 2009 a été revendue par le nouvel acquéreur en 2014 au prix de 1 656 000 euros, soit un prix très inférieur à celui de 2 376 151 euros retenu par l'administration ; que si ce prix est néanmoins supérieur à celui de 1 245 000 euros pris en compte par le Tribunal, alors que les prix de l'immobilier ont peu varié entre 2009 et 2014, il résulte de l'instruction que des travaux d'un montant de 758 130 euros ont été réalisés sur l'immeuble entre 2009 et 2014 ; qu'une partie au moins de ces travaux avait le caractère de travaux d'amélioration, nécessités par l'état de l'immeuble et ayant eu pour effet d'accroître sa valeur ; que dans ces conditions, la fixation par le tribunal de la valeur vénale de l'immeuble à 1 245 000 euros en juin 2009 n'apparaît pas insuffisante ; <br/>
<br/>
       7. Considérant que la société requérante soutient de son côté que cette évaluation est excessive ; que cependant les éléments dont elle fait état ne sont pas de nature à établir que la valeur de l'immeuble en juin 2009 n'était que de 900 000 euros ; que le rapport d'expertise en date du 6 octobre 2009 qu'elle invoque évalue le bien non pas à 900 000 euros mais à 1 128 600 euros, après application d'une décote de 40 % pour tenir compte de la perte de vue sur mer ; qu'il ne résulte d'aucune des pièces du dossier que la surface de la villa serait de 165 m² et non de 339 m² ; qu'à cet égard, l'arrêt de la Cour administrative d'appel de Marseille du 27 février 2012 ne mentionne pas que la surface de la villa aurait été de 165 m² ; que si, dans un rapport du 15 avril 2015, réalisé à sa demande, un expert a estimé que pour tenir compte de problèmes d'humidité et de l'aménagement en grenier du deuxième étage, la superficie de l'immeuble devait être pondérée et réduite à 263 m², ces pondérations ne sont justifiées ni dans leur principe ni dans leur montant ; que, par ailleurs, la société FICI, qui a réalisé sur l'immeuble entre 1990 et 2009 des travaux ayant nécessairement accru sa valeur, ne saurait invoquer des évaluations réalisées par des experts en 2002 et 2005, ni une procédure de rectification relative à la réintégration d'une provision dans le résultat de l'exercice 2001 ou de l'exercice 2002, au cours de laquelle l'administration aurait retenu comme valeur du bien la somme de 650 000 euros ; que la proposition de rectification du 3 août 2005 qu'elle verse au dossier ne concerne d'ailleurs pas la réintégration d'une provision mais les droits d'enregistrement dus par elle du fait qu'elle n'a pas respecté son engagement de revendre l'immeuble dans le délai de cinq ans à compter de son acquisition ; que si, à la demande de la société FICI, un expert a évalué la propriété à 925 000 euros le 5 mai 2008, le même expert, dans son rapport du 6 octobre 2009, a estimé que le bien valait 1 128 600 euros, après application d'une décote de 40 % pour perte de vue sur la mer ; que, dans son arrêt du 12 janvier 2010, la Cour d'appel d'Aix-en-Provence  s'est bornée à évaluer le préjudice subi par la société FICI du fait des constructions illégales voisines et n'a pas, contrairement à ce que soutient la société requérante, fixé la valeur du bien  à 1 000 000 euros en juin 2009 ; qu'enfin, les documents que produit la société FICI, à savoir un tableau récapitulatif et un constat d'huissier en date du 27 septembre 2013, ne permettent pas de distinguer précisément, parmi les travaux réalisés par le propriétaire du bien entre 2009 et 2014, ceux ayant valorisé l'immeuble de ceux non indispensables, ou de pure convenance ; qu'il n'est dès lors pas établi que, comme le soutient la requérante, la valeur de la villa en 2009 était d'environ 900 000 euros, compte tenu des travaux d'un montant de 758 130 euros réalisés par le propriétaire de la villa entre 2009 et 2014 et du prix de 1 656 000 euros auquel il l'a cédée en 2014 ;  <br/>
<br/>
       8. Considérant qu'il résulte de ce qui précède que le tribunal administratif doit être regardé comme ayant exactement évalué la valeur de l'immeuble en juin 2009, en la fixant à 1 245 000 euros ; que l'existence d'un écart significatif entre cette valeur et le prix de vente étant établie, la société FICI doit démontrer qu'elle a bénéficié en retour de contreparties ; qu'elle soutient que le prix minoré lui permettait de vendre plus facilement ce bien, qui ne lui avait apporté que des déboires et générait des coûts d'entretien et des frais financiers importants ; qu'elle invoque également des difficultés financières ; que, cependant, hormis une tentative en 1992, elle ne justifie d'aucune démarche en vue de vendre cet immeuble ; qu'il est inhérent à l'activité de marchand de biens de supporter des frais d'entretien pour les immeubles destinés à être commercialisés ; que les difficultés financières alléguées ne sont pas établies, alors que l'administration soutient sans être contredite que la société était bénéficiaire au cours des années en litige, à hauteur de 169 208 euros en 2008 et de 305 917 euros en 2009 ; que, dans ces conditions, la société FICI n'apporte pas la preuve qui lui incombe ; <br/>
<br/>
       Sur la majoration pour manquement délibéré :<br/>
<br/>
       9. Considérant que l'article 1729 du code général des impôts dispose : " Les inexactitudes ou les omissions relevées dans une déclaration ou un acte comportant l'indication d'éléments à retenir pour l'assiette ou la liquidation de l'impôt ainsi que la restitution d'une créance de nature fiscale dont le versement a été indûment obtenu de l'Etat entraînent l'application d'une majoration de : / a. 40 % en cas de manquement délibéré " ; que la société FICI conteste la majoration appliquée par l'administration sur le fondement de ces dispositions à la seule rectification résultant de la minoration de recettes ;<br/>
<br/>
       10. Considérant que l'administration a relevé que la société requérante ne pouvait, en tant que professionnel de l'immobilier, ignorer que le bien avait été cédé à un prix nettement inférieur à celui du marché et ne pouvait avancer aucun motif justifiant de cette insuffisance de prix ; que, cependant, dans les circonstances de l'espèce, compte tenu du contexte particulier dans lequel l'immeuble litigieux a été vendu et de la limitation, résultant du présent arrêt, à 345 000 euros de la minoration de recettes initialement évaluée par le service à 1 476 151 euros, l'administration ne peut être regardée comme apportant la preuve que ladite minoration de recettes procède d'une intention délibérée d'éluder l'impôt ; qu'il y a lieu en conséquence d'accorder à la société FICI la décharge de la majoration de 40 % dont les droits litigieux ont été assortis ; <br/>
<br/>
       11. Considérant qu'il résulte de tout ce qui précède, d'une part, que la société FICI est seulement fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif n'a pas fait droit à ses conclusions en décharge de la majoration de 40 % pour manquement délibéré à laquelle elle a été assujettie, d'autre part, que les conclusions de l'appel incident du ministre de l'économie et des finances doivent être rejetées ; que, dans les circonstances de l'espèce, il n'y a pas lieu de faire droit aux conclusions présentées par la société FICI sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative ;   <br/>
DECIDE<br/>
Article 1er : Il est accordé à la société FICI la décharge de la majoration de 40 % pour manquement délibéré à laquelle elle a été assujettie au titre de l'année 2009.<br/>
Article 2 : Le jugement n° 1517178 et 1518865/1-1 du 1er juin 2016 du Tribunal administratif de Paris est réformé en ce qu'il a de contraire au présent arrêt. <br/>
Article 3 : Le surplus des conclusions de la requête de la société FICI et les conclusions d'appel incident du ministre de l'économie et des finances sont rejetés.<br/>
Article 4 : Le présent arrêt sera notifié à la société FICI et au ministre de l'action et des comptes publics.<br/>
Copie en sera adressée à la direction nationale des vérifications de situations fiscales.<br/>
Délibéré après l'audience du 14 septembre 2017 à laquelle siégeaient :<br/>
M. Jardin, président de chambre,<br/>
M. Dalle, président assesseur,<br/>
Mme Notarianni, premier conseiller,<br/>
Lu en audience publique, le 28 septembre 2017.<br/>
Le rapporteur,	Le président,<br/>
 D. DALLE	C. JARDIN<br/>
Le greffier,<br/>
C. BUOT<br/>
       La République mande et ordonne au ministre l'action et des comptes publics en ce qui le concerne et à tous huissiers de justice à ce requis en ce qui concerne les voies de droit commun, contre les parties privées, de pourvoir à l'exécution de la présente décision.<br/>
2<br/>
N° 16PA02305<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">19-04-02-01-04-082 Contributions et taxes. Impôts sur les revenus et bénéfices. Revenus et bénéfices imposables - règles particulières. Bénéfices industriels et commerciaux. Détermination du bénéfice net. Acte anormal de gestion.
</SCT>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
