<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029490956</ID>
<ANCIEN_ID>JG_L_2014_09_000000366854</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/49/09/CETATEXT000029490956.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 22/09/2014, 366854, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-09-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>366854</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP CELICE, BLANCPAIN, SOLTNER</AVOCATS>
<RAPPORTEUR>M. Julien Anfruns</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Astrid Nicolazo de Barmon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:366854.20140922</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 15 mars et 5 juin 2013 au secrétariat du contentieux du Conseil d'État, présentés pour la commune de Pont-Saint-Esprit, représentée par son maire ; la commune de Pont-Saint-Esprit demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09MA03536 du 15 janvier 2013 par lequel la cour administrative d'appel de Marseille a rejeté sa requête tendant à l'annulation du jugement n° 0802224 du 3 juillet 2009 du tribunal administratif de Nîmes déchargeant l'association syndicale libre " Maison du Roy " de la somme de 79 274 euros mise à sa charge par l'article 6 du permis de construire qui lui a été délivré le 3 octobre 2006 ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de l'association syndicale libre " Maison du Roy ", d'une part, la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative et, d'autre part, au titre de l'article R. 761-1 du même code, une somme représentant la contribution à l'aide juridique qu'elle a acquittée ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Julien Anfruns, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Marie-Astrid Nicolazo de Barmon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de la commune de Pont-Saint-Esprit ;<br/>
<br/>
<br/>
<br/>1. Considérant que le maire de la commune de Pont-Saint-Esprit a délivré le 3 octobre 2006 à l'association syndicale libre " Maison du Roy " un permis de construire assorti d'une participation de 79 274 euros au titre de la non-réalisation d'aires de stationnement ; que cette association a demandé l'annulation de la décision implicite par laquelle le maire de Pont-Saint-Esprit a rejeté sa demande en décharge de cette participation ; que par un jugement du 3 juillet 2009, le tribunal administratif de Nîmes a annulé cette décision ; que, par l'arrêt attaqué du 15 janvier 2013, la cour administrative d'appel de Marseille, saisie par la commune de Pont-Saint-Esprit, a rejeté la requête de cette dernière et a déchargé l'association syndicale libre " Maison du Roy " de la somme de 79 274 euros mise à sa charge par l'article 6 du permis de construire du 3 octobre 2006 ;<br/>
<br/>
              2. Considérant que la commune soutient que la cour aurait, à tort, écarté le moyen tiré de ce que le jugement du tribunal administratif était irrégulier, faute de visa de l'un des moyens soulevés devant ce tribunal et de réponse à ce moyen ; qu'il ressort des énonciations de l'arrêt attaqué que la cour a regardé le moyen allégué  comme un argument venant au soutien d'un autre moyen soulevé devant le tribunal administratif, que ce dernier avait visé et auquel il avait répondu dans son jugement ; que, les juges du fond n'étant pas tenus de se prononcer sur le détail de l'argumentation des requérants, la cour, qui n'a pas fait une inexacte interprétation des écritures de première instance de la commune requérante, n'a ni commis d'erreur de droit ni insuffisamment motivé son arrêt sur ce point ;<br/>
<br/>
              3. Considérant, d'une part, qu'aux termes de l'article L. 421-3 du code de l'urbanisme, dans sa rédaction applicable au présent litige : " (...) Lorsque le pétitionnaire ne peut satisfaire lui-même aux obligations imposées par un document d'urbanisme en matière de réalisation d'aires de stationnement, il peut être tenu quitte de ces obligations en justifiant, pour les places qu'il ne peut réaliser lui-même sur le terrain d'assiette ou dans son environnement immédiat, soit de l'obtention d'une concession à long terme dans un parc public de stationnement existant ou en cours de réalisation, soit de l'acquisition de places dans un parc privé de stationnement existant ou en cours de réalisation (...) / A défaut de pouvoir réaliser l'obligation prévue au quatrième alinéa, le pétitionnaire peut être tenu de verser à la commune une participation fixée par le conseil municipal, en vue de la réalisation de parcs publics de stationnement (...) " ; que, d'autre part, la circonstance qu'une construction existante n'est pas conforme à une ou plusieurs dispositions d'un document d'urbanisme régulièrement approuvé ne s'oppose pas, en l'absence de dispositions de celui-ci spécialement applicables à la modification des immeubles existants, à la délivrance ultérieure d'un permis de construire s'il s'agit de travaux qui, ou bien doivent rendre l'immeuble plus conforme aux dispositions réglementaires méconnues, ou bien sont étrangers à ces dispositions ;<br/>
<br/>
              4. Considérant qu'aux termes de l'article UA 12 du règlement du plan d'occupation des sols de la commune de Pont-Saint-Esprit : " Le stationnement des véhicules correspondant aux besoins des constructions et installations doit être assuré en dehors des voies publiques ou privées sur des emplacements prévus à cet effet. Pour les constructions à usage d'habitation : une place par 100 m² de surface de plancher hors oeuvre nette (...) " ; que cet article, faute de contenir une disposition expresse relative aux travaux emportant la modification, sans extension de surface, des immeubles édifiés avant son édiction, doit être regardé comme s'appliquant aux seuls travaux accroissant la surface de plancher hors oeuvre nette de ces constructions ; que, par suite, la cour n'a pas commis d'erreur de droit  en jugeant qu'une participation relative à la non-réalisation d'aires de stationnement ne pouvait, en l'espèce, être exigée à l'occasion d'une opération de rénovation qui, tout en rendant habitable une partie substantielle du bâtiment en litige et, par suite, en accroissant son nombre d'occupants potentiels, n'impliquait, par elle-même, aucune augmentation de sa surface de plancher hors oeuvre nette ;<br/>
<br/>
              5. Considérant qu'il résulte de tout ce qui précède que la commune de Pont-Saint-Esprit n'est pas fondée à demander l'annulation de l'arrêt de la cour administrative d'appel de Marseille qu'elle attaque ; que, dans les circonstances de l'espèce, il y a lieu de mettre à sa charge une somme de 3 000 euros à verser à l'association syndicale libre " Maison du Roy ",  au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi de la commune de Pont-Saint-Esprit est rejeté.<br/>
Article 2 : La commune de Pont-Saint-Esprit versera à l'association syndicale libre " Maison du Roy " une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la commune de Pont-Saint-Esprit et à l'association syndicale libre " Maison du Roy ".<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
