<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043672587</ID>
<ANCIEN_ID>JG_L_2021_06_000000422535</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/67/25/CETATEXT000043672587.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 16/06/2021, 422535</TITRE>
<DATE_DEC>2021-06-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>422535</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SARL DIDIER-PINET ; SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>M. Alain Seban</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:422535.20210616</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Média Bonheur a demandé à la cour administrative d'appel de Paris de condamner l'Etat à lui verser la somme globale de 1 632 867 euros ou la somme de 1 716 euros par jour en réparation du préjudice qu'elle estime avoir subi du fait des décisions du Conseil supérieur de l'audiovisuel (CSA) des 5 avril 2011 et 16 octobre 2013 refusant de lui attribuer une fréquence dans la zone de Laval. Par un arrêt n° 15PA03418 du 24 mai 2018, la cour administrative d'appel a condamné l'Etat à lui verser une somme de 25 000 euros et rejeté le surplus des conclusions de sa requête.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire, un mémoire en réplique et deux nouveaux mémoires, enregistrés au secrétariat du contentieux du Conseil d'Etat les 24 juillet et 24 octobre 2018 et les 21 janvier, 10 juillet et 5 août 2020, la société Média Bonheur demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il rejette le surplus des conclusions de sa requête ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat et du CSA une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 86-1067 du 30 septembre 1986 ;<br/>
	- le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alain Seban, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteure publique.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SARL Didier-Pinet, avocat de la société Média Bonheur et à la SCP Baraduc, Duhamel, Rameix, avocat du Conseil supérieur de l'audiovisuel.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 21 mai 2021, présentée par le CSA.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 27 mai 2021, présentée par la société Média Bonheur.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 23 septembre 2013, le Conseil d'Etat statuant au contentieux a annulé la décision du 5 avril 2011 par laquelle le Conseil supérieur de l'audiovisuel (CSA) avait rejeté la candidature de la société Media Bonheur en vue de l'exploitation du service de radiodiffusion sonore par voie hertzienne " Radio Bonheur " dans la zone de Laval et a enjoint au CSA de réexaminer cette candidature. Par une décision du 16 octobre 2013, le CSA a, après l'avoir réexaminée, à nouveau rejeté la candidature de la société Média Bonheur au motif qu'il n'y avait pas de fréquence disponible dans la zone de Laval et a estimé qu'il n'y avait pas lieu de lancer un nouvel appel à candidatures pour un service de radiodiffusion sonore par voie hertzienne dans cette zone de Laval. Par une décision du 27 juillet 2015, le Conseil d'Etat statuant au contentieux a annulé cette décision en tant qu'elle refusait de lancer un appel à candidatures. Dès le 15 octobre 2014, le CSA avait toutefois lancé un appel à candidatures auquel la société Média Bonheur a été admise comme candidate de plein droit.<br/>
<br/>
              2. Par un arrêt du 24 mai 2018, la cour administrative d'appel de Paris, saisie par la société Média Bonheur, a condamné le CSA à lui verser une indemnité de 25 000 euros en réparation du préjudice causé par ses décisions du 5 avril 2011 et du 16 octobre 2013. Elle se pourvoit en cassation contre cet arrêt en tant qu'il rejette le surplus de ses conclusions indemnitaires, qu'elle avait chiffrées à la somme de 1 632 867 euros.<br/>
<br/>
              3. Par cet arrêt, la cour administrative d'appel de Paris juge, par des dispositions non contestées en cassation, que la société requérante doit être regardée comme ayant disposé d'une chance sérieuse d'obtenir une autorisation d'émettre dans la zone de Laval et que, par suite, la faute résultant de l'illégalité des décisions des 5 avril 2011 et 16 octobre 2013 engage la responsabilité du CSA pour le manque à gagner subi par la société du fait de son impossibilité d'émettre dans cette zone.<br/>
<br/>
              Sur l'étendue de la période d'indemnisation :<br/>
<br/>
              4. Aux termes de l'article 29 de la loi du 30 septembre 1986 relative à la liberté de communication visée ci-dessus : " Sous réserve des dispositions de l'article 26 de la présente loi, l'usage des fréquences pour la diffusion de services de radio par voie hertzienne terrestre est autorisé par le Conseil supérieur de l'audiovisuel dans les conditions prévues au présent article. / Pour les zones géographiques et les catégories de services qu'il a préalablement déterminées, le conseil publie une liste de fréquences disponibles ainsi qu'un appel à candidatures. Il fixe le délai dans lequel les candidatures doivent être déposées. / (...) Le conseil accorde les autorisations en appréciant l'intérêt de chaque projet pour le public, au regard des impératifs prioritaires que sont la sauvegarde du pluralisme des courants d'expression socio-culturels, la diversification des opérateurs, et la nécessité d'éviter les abus de position dominante ainsi que les pratiques entravant le libre exercice de la concurrence. (...) " Aux termes de l'article 28-1 de la même loi : " I - La durée des autorisations délivrées en application des articles 29, 29-1, 30, 30-1 et 30-2 ne peut excéder dix ans. Toutefois, pour les services de radio en mode analogique, elle ne peut excéder cinq ans. (...) / Les autorisations délivrées en application des articles 29, 29-1, 30 et 30-1 sont reconduites par le Conseil supérieur de l'audiovisuel, hors appel aux candidatures, dans la limite de deux fois en sus de l'autorisation initiale, et chaque fois pour cinq ans, sauf : / 1° Si l'Etat modifie la destination de la ou des fréquences considérées en application de l'article 21 ; / 2° Si une sanction, une astreinte liquidée ou une condamnation dont le titulaire de l'autorisation a fait l'objet sur le fondement de la présente loi, ou une condamnation prononcée à son encontre, sur le fondement des articles 23, 24 et 24bis de la loi du 29 juillet 1881 sur la liberté de la presse ou des articles 227-23 ou 227-24 du code pénal est de nature à justifier que cette autorisation ne soit pas reconduite hors appel aux candidatures ; / 3° Si la reconduction de l'autorisation hors appel aux candidatures est de nature à porter atteinte à l'impératif de pluralisme sur le plan national ou sur le plan régional et local ; / 4° Si la situation financière du titulaire ne lui permet pas de poursuivre l'exploitation dans des conditions satisfaisantes ; / 5° Pour les services de radio, si le service ne remplit plus les critères propres à la catégorie pour laquelle il est autorisé. / (...)  II. - Un an avant l'expiration de l'autorisation délivrée en application des articles 29 ou 30, le Conseil supérieur de l'audiovisuel publie sa décision motivée de recourir ou non à la procédure de reconduction hors appel aux candidatures. Ce délai est de dix-huit mois pour l'autorisation délivrée en application des articles 29-1 et 30-1. / Dans l'hypothèse où le Conseil supérieur de l'audiovisuel décide de recourir à la reconduction hors appel aux candidatures, sa décision mentionne, pour les services de communication audiovisuelle autres que radiophoniques, les points principaux de la convention en vigueur qu'il souhaite voir réviser, ainsi que ceux dont le titulaire demande la modification. / (...)  A défaut d'accord six mois au moins avant la date d'expiration de l'autorisation délivrée en application des articles 29 ou 30, ou neuf mois avant la date d'expiration de l'autorisation délivrée en application des articles 29-1 et 30-1, celle-ci n'est pas reconduite hors appel aux candidatures. Une nouvelle autorisation d'usage de fréquences ne peut être alors délivrée par le Conseil supérieur de l'audiovisuel que dans les conditions prévues aux articles 29, 29-1, 30 et 30-1. "<br/>
<br/>
              5. Lorsqu'un candidat à l'attribution d'une autorisation d'usage d'une fréquence hertzienne pour la diffusion d'un service radiophonique par voie hertzienne terrestre en mode analogique demande la réparation du préjudice né de son éviction irrégulière d'un appel à candidatures organisé par le CSA en application des dispositions citées ci-dessus et qu'il existe un lien direct de causalité entre la faute résultant de l'irrégularité et les préjudices subis par le requérant à cause de son éviction, il appartient au juge de vérifier si le candidat était ou non dépourvu de toute chance de remporter l'appel à candidatures. En l'absence de toute chance, il n'a droit à aucune indemnité. Dans le cas contraire, il a droit en principe au remboursement des frais qu'il a engagés pour présenter son offre et il convient de rechercher si ce candidat irrégulièrement évincé avait des chances sérieuses d'obtenir l'autorisation attribuée à un autre candidat. Si tel est le cas, il a droit à être indemnisé de son manque à gagner, incluant alors, puisqu'ils ont été intégrés dans ses charges, les frais de présentation de son offre.<br/>
<br/>
              6. Lorsqu'un candidat irrégulièrement évincé a droit, en application des principes qui viennent d'être indiqués, à l'indemnisation de son manque à gagner, celui-ci ne revêt un caractère certain qu'en tant qu'il porte sur la période d'exécution initiale de l'autorisation d'usage de la fréquence hertzienne et non sur les périodes ultérieures, lesquelles ne peuvent résulter que d'éventuelles reconductions. Il en va ainsi y compris lorsque le candidat irrégulièrement évincé soutient qu'il aurait, au terme de la période d'autorisation sur laquelle porte l'éviction irrégulière, rempli les conditions pour bénéficier, en application de l'article 28-1 de la loi du 30 septembre 1986 cité ci-dessus, d'une reconduction hors appel à candidatures.<br/>
<br/>
              7. Il résulte de ce qui précède que la cour administrative d'appel a pu, sans erreur de droit, juger que la société Média Bonheur ne pouvait prétendre à l'indemnisation de son préjudice que pour une période de cinq ans, durée de l'autorisation hors reconduction éventuelle, allant du 10 juillet 2011, point de départ des autorisations délivrées aux allocataires des fréquences pour lesquelles elle avait candidaté, jusqu'au 10 juillet 2016.<br/>
<br/>
              Sur le montant du préjudice indemnisable :<br/>
<br/>
              8. L'indemnité due, au titre du manque à gagner, à une entreprise irrégulièrement évincée d'un appel à candidatures qu'elle avait des chances sérieuses d'emporter ne constitue pas la contrepartie de la perte d'un élément d'actif mais est destinée à compenser une perte de recettes commerciales. Elle doit être regardée comme un profit de l'exercice au cours duquel elle a été allouée et soumise, à ce titre, à l'impôt sur les sociétés. Par suite, le manque à gagner du candidat évincé doit, lorsqu'il est calculé par référence au résultat d'exploitation de la société dans les zones pour lesquelles elle a été autorisée à émettre, être évalué avant déduction de l'impôt sur les sociétés.<br/>
<br/>
              9. Or il ressort des pièces du dossier soumis aux juges du fond que, pour évaluer le montant de l'indemnité qui devait être allouée à la société Média Bonheur, la cour administrative d'appel de Paris a pris pour base le bénéfice net annuel moyen de la société au cours des exercices 2011 à 2013, après imputation de l'impôt sur les sociétés. La société requérante est par suite fondée à soutenir qu'elle a commis, sur ce point, une erreur de droit et à demander, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, l'annulation de l'arrêt qu'elle attaque en tant qu'il fixe le montant du préjudice indemnisable.<br/>
<br/>
              10. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du CSA la somme de 3000 euros à verser à la société Média Bonheur au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces dispositions font obstacle à ce soit mise à la charge de la société Média Bonheur, qui n'est pas, dans la présente instance, la partie perdante, la somme que demande le CSA au même titre.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 24 mai 2018 est annulé en tant qu'il fixe le montant du préjudice indemnisable.<br/>
<br/>
Article 2 : L'affaire est renvoyée devant la cour administrative d'appel de Paris dans la mesure de l'annulation prononcée à l'article 1er.<br/>
<br/>
Article 3 : Le CSA versera à la société Média Bonheur une somme de 3000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Le surplus des conclusions du pourvoi et les conclusions du CSA présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetés.<br/>
<br/>
Article 5 : La présente décision sera notifiée à la société Média Bonheur et au Conseil supérieur de l'audiovisuel.<br/>
		Copie en sera adressée à la ministre de la culture.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">56-04-01-01 RADIO ET TÉLÉVISION. SERVICES PRIVÉS DE RADIO ET DE TÉLÉVISION. SERVICES DE RADIO. OCTROI DES AUTORISATIONS. - INDEMNISATION DE L'ENTREPRISE CANDIDATE IRRÉGULIÈREMENT ÉVINCÉE D'UNE PROCÉDURE D'AUTORISATION D'ÉMETTRE - 1) CONDITIONS ET CHEFS DE PRÉJUDICE RÉPARABLES [RJ1] - A) CANDIDAT DÉPOURVU DE TOUTE CHANCE - B) CANDIDAT NON DÉPOURVU DE TOUTE CHANCE - C) CANDIDAT AYANT DES CHANCES SÉRIEUSES D'OBTENIR L'AUTORISATION - 2) EVALUATION DU MANQUE À GAGNER - A) PÉRIODE D'INDEMNISATION - PÉRIODE D'EXÉCUTION INITIALE DU CONTRAT [RJ2] - B) MONTANT - EVALUATION PAR RÉFÉRENCE AU RÉSULTAT D'EXPLOITATION SUR LES ZONES CONCERNÉES - RÉSULTAT AVANT IS [RJ3].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-04-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RÉPARATION. PRÉJUDICE. - PRÉJUDICE RÉPARABLE DE L'ENTREPRISE CANDIDATE IRRÉGULIÈREMENT ÉVINCÉE D'UNE PROCÉDURE D'AUTORISATION D'ÉMETTRE - 1) CONDITIONS ET CHEFS DE PRÉJUDICE RÉPARABLES [RJ1] - A) CANDIDAT DÉPOURVU DE TOUTE CHANCE - B) CANDIDAT NON DÉPOURVU DE TOUTE CHANCE - C) CANDIDAT AYANT DES CHANCES SÉRIEUSES D'OBTENIR L'AUTORISATION - 2) EVALUATION DU MANQUE À GAGNER - A) PÉRIODE D'INDEMNISATION - PÉRIODE D'EXÉCUTION INITIALE DU CONTRAT [RJ2] - B) MONTANT - EVALUATION PAR RÉFÉRENCE AU RÉSULTAT D'EXPLOITATION SUR LES ZONES CONCERNÉES - RÉSULTAT AVANT IS [RJ3].
</SCT>
<ANA ID="9A"> 56-04-01-01 1) Lorsqu'un candidat à l'attribution d'une autorisation d'usage d'une fréquence hertzienne pour la diffusion d'un service radiophonique par voie hertzienne terrestre en mode analogique demande la réparation du préjudice né de son éviction irrégulière d'un appel à candidatures organisé par le Conseil supérieur de l'audiovisuel (CSA) en application des articles 28-1 et 29 de la loi n° 86-1067 du 30 septembre 1986 et qu'il existe un lien direct de causalité entre la faute résultant de l'irrégularité et les préjudices subis par le requérant à cause de son éviction, il appartient au juge de vérifier si le candidat était ou non dépourvu de toute chance de remporter l'appel à candidatures.... ,,a) En l'absence de toute chance, il n'a droit à aucune indemnité.... ,,b) Dans le cas contraire, il a droit en principe au remboursement des frais qu'il a engagés pour présenter son offre il convient de rechercher si ce candidat irrégulièrement évincé avait des chances sérieuses d'obtenir l'autorisation attribuée à un autre candidat.... ,,c) Si tel est le cas, il a droit à être indemnisé de son manque à gagner, incluant alors, puisqu'ils ont été intégrés dans ses charges, les frais de présentation de son offre.,,,2) a) Lorsqu'un candidat irrégulièrement évincé a droit, en application des principes qui viennent d'être indiqués, à l'indemnisation de son manque à gagner, celui-ci ne revêt un caractère certain qu'en tant qu'il porte sur la période d'exécution initiale de l'autorisation d'usage de la fréquence hertzienne et non sur les périodes ultérieures, lesquelles ne peuvent résulter que d'éventuelles reconductions.,,,Il en va ainsi y compris lorsque le candidat irrégulièrement évincé soutient qu'il aurait, au terme de la période d'autorisation sur laquelle porte l'éviction irrégulière, rempli les conditions pour bénéficier, en application de l'article 28-1 de la loi du 30 septembre 1986, d'une reconduction hors appel à candidatures.,,,b) L'indemnité due, au titre du manque à gagner, à une entreprise irrégulièrement évincée d'un appel à candidatures qu'elle avait des chances sérieuses d'emporter ne constitue pas la contrepartie de la perte d'un élément d'actif mais est destinée à compenser une perte de recettes commerciales. Elle doit être regardée comme un profit de l'exercice au cours duquel elle a été allouée et soumise, à ce titre, à l'impôt sur les sociétés.,,,Par suite, le manque à gagner du candidat évincé doit, lorsqu'il est calculé par référence au résultat d'exploitation de la société dans les zones pour lesquelles elle a été autorisée à émettre, être évalué avant déduction de l'impôt sur les sociétés (IS).</ANA>
<ANA ID="9B"> 60-04-01 1) Lorsqu'un candidat à l'attribution d'une autorisation d'usage d'une fréquence hertzienne pour la diffusion d'un service radiophonique par voie hertzienne terrestre en mode analogique demande la réparation du préjudice né de son éviction irrégulière d'un appel à candidatures organisé par le Conseil supérieur de l'audiovisuel (CSA) en application des articles 28-1 et 29 de la loi n° 86-1067 du 30 septembre 1986 et qu'il existe un lien direct de causalité entre la faute résultant de l'irrégularité et les préjudices subis par le requérant à cause de son éviction, il appartient au juge de vérifier si le candidat était ou non dépourvu de toute chance de remporter l'appel à candidatures.... ,,a) En l'absence de toute chance, il n'a droit à aucune indemnité.... ,,b) Dans le cas contraire, il a droit en principe au remboursement des frais qu'il a engagés pour présenter son offre il convient de rechercher si ce candidat irrégulièrement évincé avait des chances sérieuses d'obtenir l'autorisation attribuée à un autre candidat.... ,,c) Si tel est le cas, il a droit à être indemnisé de son manque à gagner, incluant alors, puisqu'ils ont été intégrés dans ses charges, les frais de présentation de son offre.,,,2) a) Lorsqu'un candidat irrégulièrement évincé a droit, en application des principes qui viennent d'être indiqués, à l'indemnisation de son manque à gagner, celui-ci ne revêt un caractère certain qu'en tant qu'il porte sur la période d'exécution initiale de l'autorisation d'usage de la fréquence hertzienne et non sur les périodes ultérieures, lesquelles ne peuvent résulter que d'éventuelles reconductions.,,,Il en va ainsi y compris lorsque le candidat irrégulièrement évincé soutient qu'il aurait, au terme de la période d'autorisation sur laquelle porte l'éviction irrégulière, rempli les conditions pour bénéficier, en application de l'article 28-1 de la loi du 30 septembre 1986, d'une reconduction hors appel à candidatures.,,,b) L'indemnité due, au titre du manque à gagner, à une entreprise irrégulièrement évincée d'un appel à candidatures qu'elle avait des chances sérieuses d'emporter ne constitue pas la contrepartie de la perte d'un élément d'actif mais est destinée à compenser une perte de recettes commerciales. Elle doit être regardée comme un profit de l'exercice au cours duquel elle a été allouée et soumise, à ce titre, à l'impôt sur les sociétés.,,,Par suite, le manque à gagner du candidat évincé doit, lorsqu'il est calculé par référence au résultat d'exploitation de la société dans les zones pour lesquelles elle a été autorisée à émettre, être évalué avant déduction de l'impôt sur les sociétés (IS).</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., en précisant, CE, 24 janvier 2014, Conseil supérieur de l'audiovisuel, n° 351274, T. pp. 831-845-851-862. Rappr., en matière de marchés publics, CE, 18 juin 2003, Groupement d'entreprises solidaires ETPO Guadeloupe, Société Biwater et Société Aqua TP, n° 249630, T. pp. 865-909 ; CE, 10 février 2017, Société Bancel, n° 393720, T. pp. 686-802 ; CE, 28 février 2020, Société Régal des Iles, n° 426162, T. pp. 844-956-991-994.,,[RJ2] Rappr., en matière de marchés publics, CE, 2 décembre 2019, Groupement de coopération sanitaire du Nord-Ouest Touraine, n° 423936, T. pp. 838-1018.,,[RJ3] Cf. CE, 19 mai 2015, Société Spie Est, n° 384653, pp. 760-872.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
