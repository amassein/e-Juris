<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027479453</ID>
<ANCIEN_ID>JG_L_2013_05_000000347811</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/47/94/CETATEXT000027479453.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère sous-section jugeant seule, 24/05/2013, 347811, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-05-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>347811</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>FOUSSARD ; SCP GASCHIGNARD ; SCP PEIGNOT, GARREAU, BAUER-VIOLAS</AVOCATS>
<RAPPORTEUR>M. Jean Lessi</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:347811.20130524</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête, enregistrée le 24 mars 2011 au secrétariat du contentieux du Conseil d'Etat, présentée pour M. D...C..., demeurant ... ; M. C...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision n° 1004420 du 23 février 2011 du tribunal administratif d'Orléans rejetant sa demande tendant à être autorisé à déposer, au nom du département du Loiret, une plainte avec constitution de partie civile à l'encontre de M. B...A..., de la société M.D.M. et de la société A...;<br/>
<br/>
              2°) de l'autoriser à engager cette action au nom du département du Loiret ;<br/>
<br/>
              3°) de mettre à la charge du département du Loiret la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu le code général des collectivités territoriales ;<br/>
	Vu le code pénal ; <br/>
	Vu le code de procédure pénale ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean Lessi, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Foussard, avocat de M.C..., à la SCP Peignot, Garreau, Bauer-Violas, avocat du département du Loiret et de la société SIVCOM et à la SCP Gaschignard, avocat de la société A...;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 3133-1 du code général des collectivités territoriales : " Tout contribuable inscrit au rôle du département a le droit d'exercer, tant en demande qu'en défense, à ses frais et risques, avec l'autorisation du tribunal administratif, les actions qu'il croit appartenir au département et que celui-ci, préalablement appelé à en délibérer, a refusé ou négligé d'exercer (...) " ; qu'il appartient au tribunal administratif statuant comme autorité administrative, et au Conseil d'Etat, saisi d'un recours de pleine juridiction dirigé contre la décision du tribunal administratif, lorsqu'ils examinent une demande présentée par un contribuable sur le fondement de ces dispositions, de vérifier, sans se substituer au juge de l'action, et au vu des éléments qui leur sont fournis, que l'action envisagée présente un intérêt matériel suffisant pour le département et qu'elle a une chance de succès ;<br/>
<br/>
              2. Considérant que le syndicat intercommunal à vocation multiple (SIVOM) pour l'aménagement et l'équipement de la région de Meung-sur-Loire / Beaugency, le département du Loiret et la société A...ont signé le 28 novembre 2000 un protocole portant sur la vente à cette entreprise de terrains situés sur un parc d'activités appartenant au SIVOM, pour un coût partiellement compensé par celui-ci, avec la participation financière du département du Loiret ; qu'en contrepartie, la société A...s'engageait à proposer un schéma global d'implantation d'activités nouvelles sur le site, à y construire cinq bâtiments représentant une superficie totale de 120 000 mètres carrés et un investissement global de 200 millions de francs et à y créer trois cents emplois stables ; qu'elle s'engageait également à ce que tout transfert ultérieur de ces terrains et bâtiments à une tierce personne s'effectue à un prix excluant toute plus-value pour l'entreprise sur le prix des terrains, contribue effectivement à la réalisation du projet et soit subordonné à l'accord préalable des deux collectivités publiques cosignataires ; que M.C..., après avoir adressé au président du conseil général du Loiret une demande tendant à ce que le département agisse en ce sens, a demandé au tribunal administratif d'Orléans l'autorisation de déposer, pour le compte du département, une plainte avec constitution de partie civile pour déclarations mensongères en vue d'obtenir d'une administration publique un avantage indu, faits réprimés par l'article 441-6 du code pénal, à l'encontre de M.A..., des sociétés A...et M.D.M. et de tous auteurs, co-auteurs, recéleurs et complices de ce délit ; que, par la décision attaquée, le tribunal lui a refusé cette autorisation ;<br/>
<br/>
              3. Considérant que M. C...soutient que M. A...et la société A...ont obtenu l'accord du département pour substituer à cette dernière la société M.D.M., puis pour céder trois bâtiments à des sociétés tierces, sur la base de déclarations mensongères quant à la propriété du capital de la société M.D.M. et quant au prix de cession des terrains ; qu'il en déduit que la subvention versée par le département du Loiret, d'un montant de 12,5 millions de francs, n'était pas due et que le département aurait ainsi subi un préjudice financier au moins égal à cette somme ; qu'il résulte toutefois de l'instruction que les engagements prévus par le protocole du 28 novembre 2000 en termes de constructions et de création d'emplois ont été globalement tenus ; que le protocole a concouru au développement d'un nombre important d'activités industrielles et commerciales, génératrices de ressources fiscales pour les collectivités territoriales concernées et qu'il n'est pas soutenu que la substitution de la société M.D.M. ou les conditions de revente des terrains auraient menacé la pérennité de ces activités ; qu'ainsi, à supposer même que M.A..., la société A...et la société M.D.M. se soient rendus coupables des faits qui leur sont reprochés, il n'en résulterait pas pour autant un préjudice direct et certain pour le département ; que, par suite, l'action envisagée par M. C...ne saurait être regardée comme présentant pour le département du Loiret un intérêt matériel suffisant ; qu'il suit de là que l'autorisation demandée devait lui être refusée ; que le requérant n'est, dès lors, pas fondé à demander l'annulation de la décision attaquée ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que la requête de M. C...doit être rejetée, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées au même titre par le département du Loiret, le SIVOM pour l'aménagement et l'équipement de la région de Meung-sur-Loire / Beaugency et la société A...;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. C...est rejetée.<br/>
Article 2 : Les conclusions présentées par le département du Loiret, le SIVOM pour l'aménagement et l'équipement de la région de Meung-sur-Loire / Beaugency et la société A...au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. D...C..., au département du Loiret, au SIVOM pour l'aménagement et l'équipement de la région de Meung-sur-Loire / Beaugency et à la sociétéA....<br/>
Copie en sera adressée pour information au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
