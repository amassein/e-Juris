<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041806965</ID>
<ANCIEN_ID>JG_L_2020_03_000000432714</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/80/69/CETATEXT000041806965.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 25/03/2020, 432714, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-03-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>432714</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP RICHARD</AVOCATS>
<RAPPORTEUR>M. Laurent Domingo</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:432714.20200325</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société d'exploitation de l'aéroport de Rouen a demandé au tribunal administratif de Rouen de prononcer l'expulsion de la société Jetstream Aero du hangar de l'aéroport de Rouen qu'elle occupait irrégulièrement depuis la résiliation, le 5 juin 2013, de la convention d'occupation, conclue le 1er septembre 2007, dont elle bénéficiait. Par un jugement n° 1303415 du 25 juin 2015, ce tribunal a, d'une part, enjoint à la société Jetstream Aero de libérer le hangar dans un délai de trois mois à compter de la notification de son jugement sous astreinte de cent euros par jour de retard et, d'autre part, condamné cette société à payer à la société d'exploitation de l'aéroport de Rouen une somme de 35 691,35 euros augmentée des intérêts et sous déduction de la provision déjà mise à sa charge par la cour administrative d'appel de Douai par ordonnance du 9 octobre 2014.<br/>
<br/>
              Par un arrêt n° 15DA01502 du 9 mai 2019, la cour administrative d'appel de Douai a rejeté l'appel formé par Me B... A..., agissant en qualité de mandataire liquidateur de la société Jetstream Aero, contre ce jugement, condamné la société à verser à la société Edeis concessions, venant aux droits de la société d'exploitation de l'aéroport de Rouen, la somme de 64 737, 46 euros augmentée des intérêts et sous déduction de la provision mentionnée ci-dessus, réformé le jugement du tribunal administratif de Rouen en ce qu'il avait de contraire et condamné Me A... à payer une amende de 1 500 euros sur le fondement de l'article R. 741-12 du code de justice administrative. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 17 juillet et 17 octobre 2019 au secrétariat du contentieux du Conseil d'Etat, Me A..., agissant en qualité de mandataire liquidateur de la société Jetstream Aero, demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge solidaire de la société d'exploitation de l'aéroport de Rouen, de la société Edeis concessions et du syndicat mixte de gestion de l'aéroport de Rouen-Vallée de la Seine la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général de la propriété des personnes publiques ;<br/>
              - le code de commerce ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Domingo, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Richard, avocat de Me A... ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
<br/>
              2. Pour demander l'annulation de l'arrêt qu'elle attaque, Me A..., agissant en qualité de mandataire liquidateur de la société Jetstream Aero, soutient que la cour administrative d'appel de Douai : <br/>
              - l'a insuffisamment motivé et a commis une erreur de droit en jugeant qu'elle ne pouvait utilement se prévaloir de ce que la société d'exploitation de l'aéroport de Rouen n'avait pas qualité pour demander l'expulsion de la société du domaine public ; <br/>
              - a commis une erreur de droit en jugeant que les dispositions de l'article L. 622-21 du code de commerce ne s'appliquaient qu'aux sociétés faisant l'objet d'une procédure de sauvegarde et non aux sociétés placées en liquidation judiciaire ;<br/>
              - a omis de répondre à son moyen tiré de ce que les trois commandements de payer qui n'auraient pas été suivis d'un paiement de la part de la société ne lui avaient pas été régulièrement notifiés préalablement à la résiliation de la convention d'occupation du domaine public ;<br/>
              - a commis une erreur de droit en écartant comme inopérant dans un litige relatif à l'expulsion du domaine public le moyen tiré du détournement de pouvoir commis par le gestionnaire de ce domaine ;<br/>
              - a dénaturé les faits de l'espèce et les pièces du dossier en ne retenant pas que le gestionnaire du domaine avait commis un détournement de pouvoir ; <br/>
              - a inexactement qualifié les faits de l'espèce en jugeant que son appel présentait un caractère abusif au sens de l'article R. 741-12 du code de justice administrative.<br/>
<br/>
              3. Eu égard aux moyens soulevés, il y a lieu d'admettre les conclusions du pourvoi qui sont dirigées contre l'arrêt attaqué en tant qu'il a condamné Me A... à payer une amende de 1 500 euros sur le fondement de l'article R. 741-12 du code de justice administrative. En revanche, aucun des autres moyens soulevés n'est de nature à permettre l'admission du surplus des conclusions du pourvoi.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Sont admises les conclusions du pourvoi de Me A... dirigées contre l'arrêt n° 15DA01502 du 9 mai 2019 de la cour administrative d'appel de Douai en tant qu'il l'a condamnée à payer une amende de 1 500 euros sur le fondement de l'article R. 741-12 du code de justice administrative.<br/>
Article 2 : Le surplus des conclusions du pourvoi de Me A... n'est pas admis.<br/>
Article 3 : La présente décision sera notifiée à Me B... A..., agissant en qualité de mandataire liquidateur de la société Jetstream Aero.<br/>
Copie en sera adressée à la société Edeis concessions et au Syndicat mixte de gestion de l'aéroport Rouen-Vallée de la Seine.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
