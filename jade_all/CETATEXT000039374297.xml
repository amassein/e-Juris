<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039374297</ID>
<ANCIEN_ID>JG_L_2019_11_000000420671</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/37/42/CETATEXT000039374297.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 13/11/2019, 420671</TITRE>
<DATE_DEC>2019-11-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420671</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOUZIDI, BOUHANNA</AVOCATS>
<RAPPORTEUR>Mme Airelle Niepce</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2019:420671.20191113</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A..., alors incarcéré à la maison d'arrêt d'Arles, a demandé au tribunal administratif de Marseille de condamner l'Etat à lui verser une indemnité de 4 803,05 euros au titre du préjudice qu'il estime avoir subi du fait du calcul erroné de sa rémunération pour son activité d'auxiliaire de bibliothèque au sein de cet établissement ainsi qu'une indemnité de 1 000 euros au titre de son préjudice moral. <br/>
<br/>
              Par une ordonnance n° 1308210 du 16 septembre 2014, le juge de référés du tribunal administratif a condamné l'Etat à lui verser une somme de 1 038,45 euros à titre de provision. <br/>
<br/>
              Par un jugement n° 1305303 du 29 septembre 2015, le tribunal administratif de Marseille a condamné l'Etat à verser à M. A... une somme de 1 038,45 euros au titre du préjudice résultant du calcul erroné de sa rémunération et rejeté le surplus de sa demande.<br/>
<br/>
              Par un arrêt n° 16MA00705 du 14 mai 2018, la cour administrative d'appel de Marseille a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi, enregistré le 19 février 2016, présenté par M. A....<br/>
<br/>
              Par ce pourvoi et un nouveau mémoire enregistré le 9 juillet 2018 secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler le jugement du tribunal administratif de Marseille du 29 septembre 2015 en tant qu'il rejette le surplus de sa demande ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 1 500 euros à verser à la SCP Bouzidi, Bouhanna, son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. <br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de procédure pénale ;<br/>
              - le code de la sécurité sociale ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - l'ordonnance n° 96-50 du 24 janvier 1996 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Airelle Niepce, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Bouzidi, Bouhanna, avocat de M. A... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. A..., alors incarcéré à la maison d'arrêt d'Arles où il a exercé des activités en qualité d'auxiliaire de bibliothèque a présenté une réclamation préalable auprès du directeur interrégional des services pénitentiaires de Marseille, reçue le 3 mai 2013, aux fins d'être indemnisé du préjudice résultant, selon lui, du mode de calcul erroné de sa rémunération pour ces activités professionnelles, entre janvier 2011 et décembre 2013. Une décision implicite de rejet est née le 3 juillet 2013 du silence gardé par l'administration sur cette demande. M. A... a alors saisi le juge des référés du tribunal administratif de Marseille d'une demande tendant au versement, à titre de provision, de la somme de 4 803,05 euros au titre du préjudice résultant du calcul erroné de sa rémunération et de la somme de 1 000 euros au titre du préjudice moral. Par une ordonnance du 16 septembre 2014, le juge des référés du tribunal administratif de Marseille a condamné l'Etat à verser à M. A... une provision de 1 038,45 euros. Par un jugement du 29 septembre 2015, le tribunal administratif de Marseille, statuant sur les demandes indemnitaires formulées au fond par ce dernier, a condamné l'Etat à lui verser une indemnité de 1 038,45 euros et rejeté le surplus de ses conclusions. M. A... se pourvoit en cassation contre ce jugement en tant qu'il a rejeté le surplus de ses conclusions.<br/>
<br/>
              2. En premier lieu, d'une part, aux termes de l'article 717-3 du code de procédure pénale : " (...) Les relations de travail des personnes incarcérées ne font pas l'objet d'un contrat de travail. (...) / Les règles relatives à la répartition des produits du travail des détenus sont fixées par décret. Le produit du travail des détenus ne peut faire l'objet d'aucun prélèvement pour frais d'entretien en établissement pénitentiaire. / La rémunération du travail des personnes détenues ne peut être inférieure à un taux horaire fixé par décret et indexé sur le salaire minimum de croissance défini à l'article L. 3231-2 du code du travail. Ce taux peut varier en fonction du régime sous lequel les personnes détenues sont employées. " Aux termes de l'article D. 432-1 du même code : " Hors les cas visés à la seconde phrase du troisième alinéa de l'article 717-3, la rémunération du travail effectué au sein des établissements pénitentiaires par les personnes détenues ne peut être inférieure au taux horaire suivant : / (...) / 33 % du salaire minimum interprofessionnel de croissance pour le service général, classe I ; / (...) / Un arrêté du garde des sceaux, ministre de la justice, détermine la répartition des emplois entre les différentes classes en fonction du niveau de qualification qu'exige leur exécution. " Il ressort des pièces du dossier soumis aux juges du fond que M. A..., en sa qualité d'auxiliaire de bibliothèque au sein de la maison d'arrêt d'Arles, a exercé entre janvier 2011 et décembre 2013 un travail relevant du service général, classe I, dans la classification prévue par l'article D. 432-1 du code de procédure pénale précité.<br/>
<br/>
              3. D'autre part, aux termes de l'article D. 433-4 du code de procédure pénale : " Les rémunérations pour tout travail effectué par une personne détenue sont versées, sous réserve des dispositions de l'article D. 121, à l'administration qui opère le reversement des cotisations sociales aux organismes de recouvrement et procède ensuite à l'inscription et à la répartition de la rémunération nette sur le compte nominatif des personnes détenues, conformément aux dispositions de l'article D. 434. / Ces rémunérations sont soumises à cotisations patronales et ouvrières selon les modalités fixées, pour les assurances maladie, maternité et vieillesse, par les articles R. 381-97 à R. 381-109 du code de la sécurité sociale. / (...) ". S'agissant de l'assurance maladie et maternité, l'article R. 381-99 du code de la sécurité sociale fixe ainsi le taux de la cotisation à 4,20 % du montant brut des rémunérations versées aux détenus et prévoit que cette cotisation est à la charge de l'employeur. S'agissant de l'assurance vieillesse, l'article R. 381-104 du code de la sécurité sociale prévoit que les cotisations, salariale et patronale, sont fixées au taux de droit commun du régime général et assises sur le total des rémunérations brutes des détenus et l'article R. 381-105 dispose que " Lorsque le travail est effectué pour le compte de l'administration et rémunéré sur les crédits affectés au fonctionnement des services généraux, les cotisations, salariale et patronale, sont intégralement prises en charge par l'administration.(...) ". <br/>
<br/>
              4. Enfin, en vertu de l'article L. 136-1 du code de la sécurité sociale, il est institué une contribution sociale sur les revenus d'activité et sur les revenus de remplacement, dite contribution sociale généralisée, à laquelle sont notamment assujetties " 1° Les personnes physiques qui sont à la fois considérées comme domiciliées en France pour l'établissement de l'impôt sur le revenu et à la charge, à quelque titre que ce soit, d'un régime obligatoire français d'assurance maladie ; / (...) ". Le I de l'article L. 136-2 du même code dispose que " La contribution est assise sur le montant brut des traitements, indemnités, émoluments, salaires (...). / Pour l'application du présent article, les traitements, salaires et toutes sommes versées en contrepartie ou à l'occasion du travail sont évalués selon les règles fixées à l'article L. 242-1. (...) ". L'article L. 242-1 du même code prévoit que, pour le calcul des cotisations de sécurité sociale dues pour les périodes au titre desquelles les revenus d'activité sont attribués, " sont considérées comme rémunérations toutes les sommes versées aux travailleurs en contrepartie ou à l'occasion du travail, notamment les salaires ou gains, les indemnités de congés payés, le montant des retenues pour cotisations ouvrières, les indemnités, primes, gratifications et tous autres avantages en argent, les avantages en nature, ainsi que les sommes perçues directement ou par l'entremise d'un tiers à titre de pourboire ". Le I de l'article 14 de l'ordonnance du 24 janvier 1996 relative au remboursement de la dette sociale institue " une contribution sur les revenus d'activité et de remplacement mentionnés aux articles L. 136-2 à L. 136-4 du code de la sécurité sociale ", dite contribution au remboursement de la dette sociale, et prévoit que " Cette contribution est assise sur les revenus visés et dans les conditions prévues aux articles L. 136-2 à L. 136-4 et au III de l'article L. 136 8 du code de la sécurité sociale. ".<br/>
<br/>
              5. Il résulte de l'ensemble de ces dispositions que la cotisation d'assurance maladie et maternité et la cotisation patronale pour l'assurance vieillesse auxquelles sont soumises les rémunérations versées pour tout travail effectué par une personne détenue sont prises en charge par l'employeur, tandis que la cotisation salariale pour l'assurance vieillesse reste en principe à la charge de la personne détenue sauf dans le cas où celle-ci effectue un travail pour le compte des services généraux de l'administration pénitentiaire. Par ailleurs, quelle que soit la nature de leur activité, toutes les personnes détenues sont assujetties à la contribution sociale généralisée et la rémunération qu'elles perçoivent en contrepartie du travail qu'elles effectuent dans les conditions prévues à l'article 717-3 du code de procédure pénale entre dans l'assiette de la contribution sociale généralisée ainsi que dans celle de la contribution pour le remboursement de la dette sociale.<br/>
<br/>
              6. Il ressort des énonciations du jugement attaqué que pour arrêter à 1 038,43 euros le montant du préjudice de M. A... résultant du calcul erroné de sa rémunération pour ses activités au sein de la maison d'arrêt d'Arles entre janvier 2011 et décembre 2013, qui relevaient d'un service général de classe I ainsi qu'il a été dit au point 2, le tribunal administratif de Marseille a jugé, d'une part, que l'administration n'avait pas respecté le taux horaire minimum de rémunération prévu par l'article D. 432-1 du code de procédure pénale, d'autre part, que les cotisations sociales auxquelles était soumise sa rémunération devaient être prises en charge par l'administration, enfin, a retenu l'écart de rémunération non contesté par l'administration pour l'ensemble de la période concernée. En statuant ainsi, alors qu'il ressort des pièces du dossier soumis aux juges du fond que la perte de rémunération admise par l'administration a été calculée notamment en déduisant du montant brut de la rémunération légalement due à M. A... la cotisation salariale pour l'assurance vieillesse, le tribunal administratif, au regard de ce qui a été dit au point 5, a commis une erreur de droit.<br/>
<br/>
              7. En second lieu, en retenant, pour écarter la demande d'indemnisation du préjudice moral, que M. A... n'assortissait pas ses prétentions des précisions nécessaires pour en apprécier le bien-fondé, le tribunal administratif ne s'est pas mépris sur la portée des écritures de l'intéressé et s'est livré à une appréciation souveraine des faits de l'espèce exempte de dénaturation. <br/>
<br/>
              8. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi portant sur ses conclusions tendant à l'indemnisation de son préjudice résultant du calcul erroné de sa rémunération, que M. A... est fondé à demander l'annulation du jugement qu'il attaque en tant qu'il rejette le surplus de ses conclusions à ce titre.<br/>
<br/>
              9. M. A... ayant obtenu le bénéfice de l'aide juridictionnelle, la SCP Bouzidi, Bouhanna peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Dans les circonstances de l'espèce, il y a lieu de mettre à la charge de l'Etat la somme de 1 500 euros à verser à la SCP Bouzidi, Bouhanna, avocat de M. A..., sous réserve que cette dernière renonce à percevoir la somme correspondant à la part contributive de l'Etat. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Marseille du 29 septembre 2015 est annulé en tant qu'il rejette le surplus de la demande de M. A... au titre du préjudice résultant du calcul erroné de sa rémunération.<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, au tribunal administratif de Marseille.<br/>
Article 3 : L'Etat versera à la SCP Bouzidi, Bouhanna, avocat de M. A..., une somme de 1 500 euros en application des dispositions de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991 sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : Le surplus des conclusions du pourvoi de M. A... est rejeté.<br/>
Article 5 : La présente décision sera notifiée à M. B... A... et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">37-05-02-01 JURIDICTIONS ADMINISTRATIVES ET JUDICIAIRES. EXÉCUTION DES JUGEMENTS. EXÉCUTION DES PEINES. SERVICE PUBLIC PÉNITENTIAIRE. - RÉMUNÉRATION DU TRAVAIL DES PERSONNES DÉTENUES - 1) COTISATION D'ASSURANCE MALADIE ET MATERNITÉ ET COTISATION PATRONALE POUR L'ASSURANCE VIEILLESSE - A LA CHARGE DE L'EMPLOYEUR - 2) COTISATION SALARIALE POUR L'ASSURANCE VIEILLESSE - A LA CHARGE DU DÉTENU [RJ1], SAUF TRAVAIL EFFECTUÉ POUR LE COMPTE DE L'ADMINISTRATION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">62-03-02 SÉCURITÉ SOCIALE. COTISATIONS. ASSIETTE, TAUX ET CALCUL DES COTISATIONS. - RÉMUNÉRATION DU TRAVAIL DES PERSONNES DÉTENUES - 1) COTISATION D'ASSURANCE MALADIE ET MATERNITÉ ET COTISATION PATRONALE POUR L'ASSURANCE VIEILLESSE - A LA CHARGE DE L'EMPLOYEUR - 2) COTISATION SALARIALE POUR L'ASSURANCE VIEILLESSE - A LA CHARGE DU DÉTENU [RJ1] SAUF LORSQUE LE TRAVAIL EST EFFECTUÉ POUR LE COMPTE DE L'ADMINISTRATION.
</SCT>
<ANA ID="9A"> 37-05-02-01 La cotisation d'assurance maladie et maternité et la cotisation patronale pour l'assurance vieillesse auxquelles sont soumises les rémunérations versées pour tout travail effectué par une personne détenue sont prises en charge par l'employeur, tandis que la cotisation salariale pour l'assurance vieillesse reste en principe à la charge de la personne détenue sauf dans le cas où celle-ci effectue un travail pour le compte des services généraux de l'administration pénitentiaire.</ANA>
<ANA ID="9B"> 62-03-02 La cotisation d'assurance maladie et maternité et la cotisation patronale pour l'assurance vieillesse auxquelles sont soumises les rémunérations versées pour tout travail effectué par une personne détenue sont prises en charge par l'employeur, tandis que la cotisation salariale pour l'assurance vieillesse reste en principe à la charge de la personne détenue sauf dans le cas où celle-ci effectue un travail pour le compte des services généraux de l'administration pénitentiaire.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., sur la soumission à la CSG et à la CRDS des rémunérations du travail perçues par les détenus, CE, 29 juin 2018,,, n° 409214, T. pp. 653-758.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
