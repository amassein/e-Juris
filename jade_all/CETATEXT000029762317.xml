<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029762317</ID>
<ANCIEN_ID>JG_L_2014_11_000000361482</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/76/23/CETATEXT000029762317.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 14/11/2014, 361482, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-11-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>361482</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>COPPER-ROYER</AVOCATS>
<RAPPORTEUR>Mme Maïlys Lange</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Astrid Nicolazo de Barmon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:361482.20141114</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              M. et Mme A...B...ont demandé au tribunal administratif de Paris la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles ils ont été assujettis au titre des années 1999 et 2000. Par deux jugements n° 0613019 du 22 mars 2010 et n° 0721006 du 18 octobre 2010, le tribunal administratif de Paris a rejeté leurs demandes.<br/>
<br/>
              Par un arrêt n° 10PA02299, 10PA05162 du 21 juin 2012, la cour administrative d'appel de Paris a rejeté les appels formés par M. et Mme B...contre ces jugements. <br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 30 juillet et 23 octobre 2012 au secrétariat du contentieux du Conseil d'Etat, M. et Mme B...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10PA02299, 10PA05162 du 21 juin 2012 de la cour administrative d'appel de Paris ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leurs appels ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - le code civil ; <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Maïlys Lange, auditeur,  <br/>
<br/>
              - les conclusions de Mme Marie-Astrid Nicolazo de Barmon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Copper-Royer, avocat de M. B...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. A...B..., cofondateur et associé de la société Stratelite, a fait donation à son épouse et à son fils mineur, par des actes des 9 novembre et 22 décembre 1999, de 29 actions en pleine propriété et 712 actions en nue-propriété de cette société, la valeur unitaire des titres étant fixée à 32 000 francs dans ces actes de donation ; que 81 des 741 actions ainsi données ont été cédées le 18 novembre 1999, pour un prix unitaire de 28 000 francs, à la société FI System, avec laquelle la société Stratelite a ultérieurement fusionné, en vertu d'un protocole d'accord signé le même jour ; que sur ces 81 actions, 79 provenaient d'actions données au fils de M.B..., dont 10 en pleine propriété et 69 en nue-propriété ; que 643 des autres actions ayant fait l'objet de ces donations ont ensuite été cédées le 27 décembre 1999 à la société civile de portefeuille (SCP) La Fontaine, créée le 9 novembre 1999 entre les membres de la familleB..., pour un prix unitaire de 32 000 francs, et échangées le 30 décembre, jour de la fusion avec la société FI System, contre des actions de cette dernière société ; que le 25 novembre 1999, le compte ouvert au nom du fils de M.B..., créditeur d'une somme de 280 000 francs correspondant au montant de la cession des dix actions détenues en pleine propriété, a été débité de la même somme par un virement émis au bénéfice de M.B... ; que le compte courant ouvert au nom de M. B...en qualité d'usufruitier et de son fils en qualité de nu-propriétaire, initialement créditeur d'une somme de 1 932 000 francs correspondant au montant de la cession des 69 actions démembrées, a d'abord été débité d'une somme de 1 200 000 francs, le 21 décembre 1999, par un virement émis au bénéfice de M.B..., puis du solde de 732 000 francs, le 18 janvier 2000, par un virement émis au bénéfice de la SCI Echos de la Forêt, les fonds correspondants étant inscrits sur le compte courant d'associé de M. B...au sein de cette SCI, dont il est le gérant ; qu'à la suite du contrôle dont M. et Mme B...ont fait l'objet au titre des années 1999 à 2001, l'administration a remis en cause la sincérité de l'ensemble des actes de donation, et a mis en oeuvre la procédure de répression des abus de droit prévue à l'article L. 64 du livre des procédures fiscales ; qu'elle a regardé la cession des titres de la société Stratelite comme ayant été directement effectuée par M. B..., a imposé au titre de l'année 1999 la plus-value réalisée à cette occasion pour une somme égale, pour chaque titre cédé, à la différence entre le prix de cession, soit 28 000 francs, et la valeur nominale, soit 100 francs et a, enfin, remis en cause les moins-values que M. et Mme B...avaient imputées à raison de ces opérations, issues de l'écart entre le prix unitaire de cession de 28 000 francs et le prix stipulé aux actes de donation de 32 000 francs ; que, toutefois, le comité consultatif pour la répression des abus de droit n'a prononcé un avis favorable à la qualification d'abus de droit que pour la seule partie de la donation du 9 novembre 1999 faite au profit du fils de M. B...et correspondant aux dix actions en pleine propriété et aux 69 actions en nue-propriété vendues le 18 novembre à la société FI System ; que l'administration a, alors, limité son redressement à la remise en cause de la moins-value et à l'imposition de la plus-value résultant de la vente de ces 79 actions ; que M. et Mme B...se pourvoient en cassation contre l'arrêt par lequel la cour administrative d'appel de Paris a rejeté leurs appels dirigés contre les jugements du tribunal administratif de Paris ayant rejeté leurs demandes en décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales résultant de ce redressement et des pénalités correspondantes ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 64 du livre des procédures fiscales, dans sa rédaction alors applicable : " Ne peuvent être opposés à l'administration des impôts les actes qui dissimulent la portée véritable d'un contrat ou d'une convention à l'aide de clauses : (...) / b ) (...) qui déguisent soit une réalisation soit un transfert de bénéfices ou de revenus (...) / L'administration est en droit de restituer son véritable caractère à l'opération litigieuse. En cas de désaccord sur les redressements notifiés sur le fondement du présent article, le litige est soumis, à la demande du contribuable, à l'avis du comité consultatif pour la répression des abus de droit. L'administration peut également  soumettre le litige à l'avis du comité (...) / Si l'administration ne s'est pas conformée à l'avis du comité, elle doit apporter la preuve du bien-fondé du redressement. " ; qu'il résulte de ces dispositions que l'administration est fondée à écarter comme ne lui étant pas opposables certains actes passés par le contribuable, dès lors que ces actes ont un caractère fictif, ou que, recherchant le bénéfice d'une application littérale des textes à l'encontre des objectifs poursuivis par leurs auteurs, ils n'ont pu être inspirés par aucun autre motif que celui d'éluder ou d'atténuer les charges fiscales que l'intéressé, s'il n'avait pas passé ces actes, aurait normalement supportées, eu égard à sa situation ou à ses activités réelles ; <br/>
<br/>
              3. Considérant qu'aux termes de l'article 894 du code civil : " La donation entre vifs est un acte par lequel le donateur se dépouille actuellement et irrévocablement de la chose donnée, en faveur du donataire qui l'accepte " ; que dès lors qu'un acte revêt le caractère d'une donation au sens de ces dispositions, l'administration ne peut le regarder comme n'ayant pu être inspiré par aucun autre motif que celui d'éluder ou d'atténuer les charges fiscales que son auteur, s'il ne l'avait pas passé, aurait normalement supportées ; qu'elle n'est, par suite, pas fondée à l'écarter comme ne lui étant pas opposable sur le fondement de l'article L. 64 du livre des procédures fiscales ; qu'en revanche, l'administration peut écarter sur ce fondement comme ne lui étant pas opposable un acte de donation qui ne se traduit pas par un dépouillement immédiat et irrévocable de son auteur et revêt dès lors un caractère fictif ; qu'il en va notamment ainsi lorsque le donateur appréhende, à la suite de l'acte de donation, tout ou partie du produit de la cession de la chose prétendument donnée ;<br/>
<br/>
              4. Considérant que la cour administrative d'appel de Paris a relevé que le produit de la cession des actions précédemment données au fils de M. B..., et inscrit au crédit d'un compte ouvert à son nom propre pour les actions en pleine propriété, et d'un compte ouvert à la fois à son nom en tant que nu-propriétaire et au nom de M. B... père en tant qu'usufruitier pour les actions démembrées, avait été transféré sur des comptes dont ce dernier était seul titulaire, ainsi qu'à la SCI Echos de la Forêt dont il détenait 99 % des parts ; que, devant la cour, M. et Mme B...ont cependant soutenu que le prélèvement des sommes de 280 000 francs sur le produit de la vente des dix actions détenues par leur fils en pleine propriété, puis de 1 200 000 francs sur le produit de la vente des 69 actions démembrées, correspondait au remboursement d'une créance de 1 124 000 francs que M. B...détenait sur son fils, dans la mesure où il lui avait fait l'avance des droits de donation dus, en vertu des conventions de donation des 9 novembre et 22 décembre 1999, par le donataire ; qu'en outre, l'erreur commise de bonne foi par M. B... en prélevant la somme de 1 200 000 francs sur le produit de la vente des 69 actions dont il ne détenait que l'usufruit avait été régularisée le 17 janvier 2002 par le transfert de cette somme sur un compte courant de la SCP La Fontaine ouvert au nom de l'indivision afin de libérer le capital de cette société ; qu'enfin, la somme de 732 000 francs, correspondant au solde du produit de la vente des 69 actions en cause, avait en réalité fait l'objet d'un prêt à la SCI Echos de la Forêt, qui l'avait remboursé en 2002 ; <br/>
<br/>
              5. Considérant qu'après avoir rappelé qu'en raison de l'avis favorable du comité consultatif pour la répression des abus de droit, la charge de la preuve incombait aux requérants, c'est par une appréciation souveraine, exempte de dénaturation, que la cour a estimé que ces dernières allégations n'étaient pas établies, en relevant que le capital de la SCP La Fontaine était intégralement libéré dès la date du 28 décembre 1999, que le contrat de prêt avec la SCI Echos de la Forêt était sans date certaine et que son remboursement avait eu M. B...pour seul bénéficiaire ; que contrairement à ce qui est soutenu, ces mouvements financiers des comptes ouverts au nom du donataire, soit en son nom propre, soit en commun avec son père mentionnant sa qualité de nu-propriétaire, vers le compte personnel du donateur, excédaient les droits conférés à M. B...par ses qualités d'usufruitier et d'administrateur légal des biens de son fils mineur ; qu'en particulier, M. B... ne pouvait, sans méconnaître son obligation de conserver la substance du portefeuille de valeurs mobilières dont son fils était nu-propriétaire, prélever sur le compte ouvert à leurs deux noms, à son seul bénéfice, des sommes issues de la cession des titres Stratelite ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède qu'en jugeant que la donation en litige ne pouvait être regardée comme ayant été irrévocablement consentie, dès lors qu'une fois vendus les titres qui en étaient l'objet, une part substantielle du produit correspondant avait été appréhendée par le donateur en méconnaissance des dispositions de l'article 894 du code civil, la cour administrative d'appel n'a pas inexactement qualifié les faits qui lui étaient soumis ; qu'elle en a, à bon droit, déduit que cette donation n'était pas opposable à l'administration, en application des dispositions précitées de l'article L. 64 du livre des procédures fiscales ; <br/>
<br/>
              7. Considérant que, comme il vient d'être dit, la cour a, à bon droit, jugé qu'une part substantielle du produit de la donation en litige avait été appréhendée par M. B... ; qu'ainsi, la circonstance que le prélèvement du solde de ce produit aurait correspondu à une créance que détenait ce dernier sur son fils, à raison des droits de mutation qu'il aurait acquittés pour son compte, était sans incidence sur l'inopposabilité à l'administration de cette donation ; que, par suite, c'est par une motivation surabondante que la cour a, à tort, jugé que M. B...ne justifiait pas avoir réglé les droits de mutation en question, alors qu'il était constant que ces droits s'élevaient au montant qu'il indiquait et qu'il avait fourni des relevés bancaires attestant de ce que son compte avait été débité de ce même montant aux dates de passation des actes de donation ; <br/>
<br/>
              8. Considérant, enfin, que M. et Mme B...ne peuvent utilement critiquer le motif par lequel la cour a relevé que c'était à bon droit que l'administration avait estimé que la donation litigieuse avait eu pour seul but d'atténuer les charges fiscales des intéressés par la création artificielle d'une moins-value de cession, dès lors que, le caractère fictif de la donation ayant été établi, ainsi qu'il a été dit, ce motif présentait nécessairement un caractère surabondant ;<br/>
<br/>
              9. Considérant qu'il résulte de tout ce qui précède que le pourvoi de M. et Mme B... doit être rejeté, y compris leurs conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : Le pourvoi de M. et Mme B...est rejeté. <br/>
Article 2 : La présente décision sera notifiée à M. et Mme A...B...et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
