<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027752936</ID>
<ANCIEN_ID>JG_L_2013_07_000000339922</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/75/29/CETATEXT000027752936.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Section du Contentieux, 25/07/2013, 339922, Publié au recueil Lebon</TITRE>
<DATE_DEC>2013-07-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>339922</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Section du Contentieux</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP CELICE, BLANCPAIN, SOLTNER ; LE PRADO</AVOCATS>
<RAPPORTEUR>Mme Domitille Duval-Arnould</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESEC:2013:339922.20130725</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 25 mai et 23 août 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. A... B..., demeurant ...; M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 06LY01195 du 23 mars 2010 par lequel la cour administrative d'appel de Lyon a rejeté sa requête tendant à l'annulation du jugement n° 030846 du 7 avril 2006 par lequel le tribunal administratif de Grenoble a rejeté sa demande tendant à la condamnation du centre hospitalier de Chambéry à lui verser une somme de 13 502 euros en réparation des préjudices résultant de la défectuosité de la prothèse totale du genou mise en place par une intervention du 25 janvier 2000 ; <br/>
<br/>
              2°) de mettre à la charge du centre hospitalier de Chambéry la somme de 4 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la directive 85/374/CEE du Conseil du 25 juillet 1985, modifiée, relative au rapprochement des dispositions législatives, règlementaires et administratives des Etats membres en matière de responsabilité des produits défectueux ;<br/>
<br/>
              Vu l'arrêt n° C-495/10 du 21 décembre 2011 rendu par la Cour de justice de l'Union européenne ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Domitille Duval-Arnould, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de M.B..., à Me Le Prado, avocat du centre hospitalier de Chambéry et à la SCP Célice, Blancpain, Soltner, avocat de la société Groupe Lépine ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'à la suite de la luxation d'une prothèse du genou posée le 25 janvier 2000 au centre hospitalier universitaire de Chambéry, M. B...a dû subir, le 27 avril 2000, une intervention chirurgicale de reprise et qu'il a fallu procéder le 8 février 2001 au remplacement de la prothèse ; qu'invoquant une défectuosité de celle-ci, l'intéressé a exercé à l'encontre du centre hospitalier universitaire de Chambéry un recours indemnitaire que le tribunal administratif de Grenoble a rejeté par un jugement du 7 avril 2006 ; qu'il se pourvoit en cassation contre l'arrêt du 23 mars 2010 par lequel la cour administrative d'appel de Lyon a rejeté son appel contre ce jugement ; <br/>
<br/>
              2. Considérant que, dans un arrêt du 21 décembre 2011 par lequel elle s'est prononcée sur une question dont le Conseil d'Etat, statuant au contentieux l'avait saisie à titre préjudiciel, la Cour de justice de l'Union européenne a dit pour droit que " la responsabilité d'un prestataire de services qui utilise, dans le cadre d'une prestation de services telle que des soins dispensés en milieu hospitalier, des appareils ou des produits défectueux dont il n'est pas le producteur au sens des dispositions de l'article 3 de la directive 85/374/CEE du Conseil, du 25 juillet 1985, relative au rapprochement des dispositions législatives, réglementaires et administratives des États membres en matière de responsabilité du fait des produits défectueux, telle que modifiée par la directive 1999/34/CE du Parlement européen et du Conseil, du 10 mai 1999, et cause, de ce fait, des dommages au bénéficiaire de la prestation ne relève pas du champ d'application de cette directive " et que " cette dernière ne s'oppose dès lors pas à ce qu'un État membre institue un régime, tel que celui en cause au principal, prévoyant la responsabilité d'un tel prestataire à l'égard des dommages ainsi occasionnés, même en l'absence de toute faute imputable à celui-ci, à condition, toutefois, que soit préservée la faculté pour la victime et/ou ledit prestataire de mettre en cause la responsabilité du producteur sur le fondement de ladite directive lorsque se trouvent remplies les conditions prévues par celle-ci " ;<br/>
<br/>
              3. Considérant qu'il résulte de l'interprétation ainsi donnée par la Cour de justice de l'Union européenne que la directive du 25 juillet 1985 ne fait pas obstacle à l'application du principe selon lequel, sans préjudice des actions susceptibles d'être exercées à l'encontre du producteur, le service public hospitalier est responsable, même en l'absence de faute de sa part, des conséquences dommageables pour les usagers de la défaillance des produits et appareils de santé qu'il utilise ; que ce principe trouve à s'appliquer lorsque le service public hospitalier implante, au cours de la prestation de soins, un produit défectueux dans le corps d'un patient ; <br/>
<br/>
              4. Considérant, par suite, qu'en faisant application de la directive pour juger que M. B..., qui avait connaissance de l'identité du producteur de la prothèse défectueuse contre lequel il lui appartenait de diriger son action, ne pouvait rechercher la responsabilité du centre hospitalier de Chambéry, la cour administrative d'appel a commis une erreur de droit ; que son arrêt doit dès lors être annulé ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du centre hospitalier de Chambéry, en application des dispositions de l'article L. 761-1 du code de justice administrative la somme de 3 000 euros au titre des frais exposés par M. B... et non compris dans les dépens ; que ces dispositions font obstacle à ce que la somme demandée par la société Groupe Lépine soit mise à la charge du requérant, qui n'est pas la partie perdante ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon du 23 mars 2010 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon.<br/>
Article 3 : Le centre hospitalier de Chambéry versera à M. B...une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative<br/>
Article 4 : Les conclusions présentées par la société Groupe Lépine au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 5 : La présente décision sera notifiée à M. A...B..., au centre hospitalier universitaire de Chambéry, à la caisse primaire d'assurance maladie de Savoie et à la société Groupe Lépine.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">15-03-03-01 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. APPLICATION DU DROIT DE L'UNION EUROPÉENNE PAR LE JUGE ADMINISTRATIF FRANÇAIS. PRISE EN COMPTE DES ARRÊTS DE LA COUR DE JUSTICE. INTERPRÉTATION DU DROIT DE L'UNION. - RESPONSABILITÉ SANS FAUTE DES ÉTABLISSEMENTS PUBLICS DE SANTÉ DU FAIT DES PRODUITS OU APPAREILS DE SANTÉ DÉFECTUEUX [RJ1] - CONSÉQUENCES DE LA DIRECTIVE 85/374/CEE - CONSÉQUENCES À TIRER DE LA RÉPONSE DONNÉE PAR LA CJUE À LA QUESTION PRÉJUDICIELLE [RJ2] - CHAMP D'APPLICATION DU PRINCIPE DE RESPONSABILITÉ SANS FAUTE - IMPLANTATION D'UN PRODUIT DÉFECTUEUX DANS LE CORPS D'UN PATIENT (PAR EX. UNE PROTHÈSE) - INCLUSION [RJ3].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">15-05-21 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. RÈGLES APPLICABLES. SANTÉ PUBLIQUE. - RESPONSABILITÉ SANS FAUTE DES ÉTABLISSEMENTS PUBLICS DE SANTÉ DU FAIT DES PRODUITS OU APPAREILS DE SANTÉ DÉFECTUEUX [RJ1] - CONSÉQUENCES DE LA DIRECTIVE 85/374/CEE - CONSÉQUENCES À TIRER DE LA RÉPONSE DONNÉE PAR LA CJUE À LA QUESTION PRÉJUDICIELLE [RJ2] - CHAMP D'APPLICATION DU PRINCIPE DE RESPONSABILITÉ SANS FAUTE - IMPLANTATION D'UN PRODUIT DÉFECTUEUX DANS LE CORPS D'UN PATIENT (PAR EX. UNE PROTHÈSE) - INCLUSION [RJ3].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">60-02-01-01-005 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICE PUBLIC DE SANTÉ. ÉTABLISSEMENTS PUBLICS D'HOSPITALISATION. RESPONSABILITÉ SANS FAUTE. - RESPONSABILITÉ SANS FAUTE DES ÉTABLISSEMENTS PUBLICS DE SANTÉ DU FAIT DES PRODUITS OU APPAREILS DE SANTÉ DÉFECTUEUX [RJ1] - CONSÉQUENCES DE LA DIRECTIVE 85/374/CEE - CONSÉQUENCES À TIRER DE LA RÉPONSE DONNÉE PAR LA CJUE À LA QUESTION PRÉJUDICIELLE [RJ2] - CHAMP D'APPLICATION DU PRINCIPE DE RESPONSABILITÉ SANS FAUTE - IMPLANTATION D'UN PRODUIT DÉFECTUEUX DANS LE CORPS D'UN PATIENT (PAR EX. UNE PROTHÈSE) - INCLUSION [RJ3].
</SCT>
<ANA ID="9A"> 15-03-03-01 Il résulte de l'interprétation donnée par la Cour de justice de l'Union européenne (CJUE) dans l'arrêt n° C-495/10 du 21 décembre 2011 que la directive 85/374/CEE du 25 juillet 1985 ne fait pas obstacle à l'application du principe selon lequel, sans préjudice des actions susceptibles d'être exercées à l'encontre du producteur, le service public hospitalier est responsable, même en l'absence de faute de sa part, des conséquences dommageables pour les usagers de la défaillance des produits et appareils de santé qu'il utilise.,,,Ce principe trouve à s'appliquer lorsque le service public hospitalier implante, au cours de la prestation de soins, un produit défectueux dans le corps d'un patient.</ANA>
<ANA ID="9B"> 15-05-21 Il résulte de l'interprétation donnée par la Cour de justice de l'Union européenne (CJUE) dans l'arrêt n° C-495/10 du 21 décembre 2011 que la directive 85/374/CEE du 25 juillet 1985 ne fait pas obstacle à l'application du principe selon lequel, sans préjudice des actions susceptibles d'être exercées à l'encontre du producteur, le service public hospitalier est responsable, même en l'absence de faute de sa part, des conséquences dommageables pour les usagers de la défaillance des produits et appareils de santé qu'il utilise.,,,Ce principe trouve à s'appliquer lorsque le service public hospitalier implante, au cours de la prestation de soins, un produit défectueux dans le corps d'un patient.</ANA>
<ANA ID="9C"> 60-02-01-01-005 Il résulte de l'interprétation donnée par la Cour de justice de l'Union européenne (CJUE) dans l'arrêt n° C-495/10 du 21 décembre 2011 que la directive 85/374/CEE du 25 juillet 1985 ne fait pas obstacle à l'application du principe selon lequel, sans préjudice des actions susceptibles d'être exercées à l'encontre du producteur, le service public hospitalier est responsable, même en l'absence de faute de sa part, des conséquences dommageables pour les usagers de la défaillance des produits et appareils de santé qu'il utilise.,,,Ce principe trouve à s'appliquer lorsque le service public hospitalier implante, au cours de la prestation de soins, un produit défectueux dans le corps d'un patient.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 9 juillet 2003, Assistance publique-Hôpitaux de Paris c/ Mme Marzouk, n° 220437, p. 338.,,[RJ2] Cf. CJUE, 21 décembre 2011, Centre hospitalier universitaire de Besançon, C-495/10 ; CE, 12 mars 2012, Centre hospitalier universitaire de Besançon, n° 327449, p. 85.,,[RJ3] Cf. CE, 15 juillet 2004, Dumas, n° 252551, T. pp. 592-805. Comp. Cass. civ. 1ère, 12 juillet 2012, n° 11-17510, Bull. Civ. n° 165.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
