<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000023690708</ID>
<ANCIEN_ID>JG_L_2011_02_000000332837</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/23/69/07/CETATEXT000023690708.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 23/02/2011, 332837</TITRE>
<DATE_DEC>2011-02-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>332837</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP TIFFREAU, CORLAY</AVOCATS>
<RAPPORTEUR>M. Jean Lessi</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Luc Derepas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2011:332837.20110223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 19 octobre 2009 et 19 janvier 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. A...B..., demeurant..., ; M.  B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 07MA01795 du 3 septembre 2009 par lequel la cour administrative d'appel de Marseille a rejeté sa requête tendant à l'annulation d'un jugement n° 0305530 du 21 décembre2006 du tribunal administratif de Montpellier rejetant sa demande d'annulation de la décision du 8 octobre 2003 du préfet du Gard rejetant son recours gracieux formé contre une décision du 31 juillet 2003 prononçant son exclusion à titre définitif du revenu de remplacement à compter du 1er août 2003 ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel et d'annuler la décision du préfet du Gard en date du 8 octobre 2003 ;<br/>
<br/>
              3°) d'enjoindre au préfet du Gard, au besoin sous astreinte, de prendre les mesures d'exécution qu'impliquera la décision à intervenir ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
              Vu le code du travail ; <br/>
<br/>
              Vu la loi n° 2005-32 du 18 janvier 2005 ; <br/>
<br/>
              Vu le décret n° 2005-915 du 2 août 2005 ;<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean Lessi, Auditeur,  <br/>
<br/>
              - les observations de la SCP Tiffreau, Corlay, avocat de M.B..., <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Tiffreau, Corlay, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant qu'aux termes de l'article R. 351-28 du code du travail, en vigueur à la date de la décision d'exclusion litigieuse : " Sont exclues, à titre temporaire ou définitif, du revenu de remplacement (...) les personnes qui : / (...) 2. Ne peuvent justifier de l'accomplissement d'actes positifs de recherche d'emploi au sens du premier alinéa de l'article R. 351-27 (...) " ; que, sur ce fondement, le préfet du Gard, saisi d'un recours administratif préalable obligatoire, a, par une décision du 8 octobre 2003, confirmé une précédente décision du 31 juillet 2003 excluant à titre définitif M. B...du bénéfice du revenu de remplacement à compter du 1er août 2003, au motif que l'intéressé, demandeur d'emploi, n'avait pas accompli d'actes positifs et répétés de recherche d'emploi ;<br/>
<br/>
              Considérant qu'une telle mesure d'exclusion, qui ne se borne pas à tirer les conséquences de ce que l'intéressé ne satisfait pas aux conditions légales auxquelles le revenu de remplacement est subordonné, revêt, en raison de ses motifs et des effets qui lui sont attachés, le caractère d'une sanction ; que le recours formé contre une telle sanction que l'administration inflige à un administré présente le caractère d'un recours de plein contentieux ; qu'il résulte toutefois des termes mêmes de l'arrêt par lequel la cour administrative d'appel de Marseille a statué sur le litige porté devant elle par M. B...que la cour s'est estimée saisie d'un recours pour excès de pouvoir, et qu'elle a statué sur le bien-fondé de la sanction en se plaçant, non à la date de son arrêt, mais à celle de la décision en litige ; qu'il appartient au juge de cassation de relever d'office l'erreur ainsi commise par la cour sur l'étendue de ses pouvoirs ; que, sans qu'il soit besoin d'examiner les moyens du pourvoi, l'arrêt attaqué doit être annulé ;<br/>
<br/>
              Considérant que, dans les circonstances de l'espèce, il y a lieu de faire application des dispositions de l'article L. 821-2 du code de justice administrative et de régler l'affaire au fond ;<br/>
<br/>
              Considérant que M. B...demande l'annulation d'un jugement du tribunal administratif de Montpellier qui a rejeté sa demande dirigée contre la mesure d'exclusion du bénéfice du revenu de remplacement qui le vise ; qu'il ressort des termes mêmes de ce jugement que le tribunal administratif s'est estimé saisi d'un recours pour excès de pouvoir contre la décision en cause alors que, ainsi qu'il a été dit ci-dessus, ce recours relevait du plein contentieux ; que, par suite, et sans qu'il soit besoin d'examiner les moyens de la requête de M. B..., le jugement attaqué doit être annulé ;<br/>
<br/>
              Considérant qu'il y a lieu d'évoquer et de statuer immédiatement sur la demande présentée par M. B...devant le tribunal administratif de Montpellier ;<br/>
<br/>
              En ce qui concerne la décision du 31 juillet 2003 :<br/>
<br/>
              Considérant que la décision du 8 octobre 2003 par laquelle le préfet du Gard a rejeté le recours administratif préalable obligatoire formé par M.  B...contre sa précédente décision du 31 juillet 2003 s'est nécessairement substituée à cette dernière ; que les conclusions de la demande de M. B...dirigées contre la décision du 31 juillet 2003 sont, dès lors, irrecevables ; qu'elles doivent, par suite, être rejetées ; <br/>
<br/>
              En ce qui concerne la décision du 8 octobre 2003 :<br/>
<br/>
              Sur les fins de non-recevoir opposées par le préfet du Gard :<br/>
<br/>
              Considérant que l'exercice par M. B...du recours administratif préalable obligatoire contre la décision du préfet du Gard du 31 juillet 2003 a eu pour effet de conserver le délai de recours contentieux ; qu'il suit de là que, contrairement à ce que soutient le préfet, la demande présentée par l'intéressé devant le tribunal administratif, enregistrée le 24 novembre 2003, soit moins de deux mois après la notification de la décision du préfet du 8 octobre 2003, était recevable ; <br/>
<br/>
              Considérant qu'en réponse à la mise en demeure que lui a adressée le 1er décembre 2003 le président du tribunal administratif de Montpellier en application des dispositions de l'article R. 612-2 alors applicable du code de justice administrative, M.  B...a fourni, le 9 décembre 2003, des copies des décisions du 31 juillet et du 9 octobre 2003 qu'il contestait ; que la fin de non-recevoir opposée par le préfet du Gard et tirée de la violation des prescriptions de l'article R. 412-1 du même code ne peut, dès lors, qu'être écartée ;<br/>
<br/>
              Sur le bien-fondé de la sanction :<br/>
<br/>
              Considérant que les dispositions de l'article L. 5421-3 du code du travail en vigueur à la date de la présente décision, combinées avec celles des articles R. 5411-11 et R. 5411-12 du même code, définissent le contenu et la portée de l'obligation d'accomplir des actes positifs de recherche d'emploi incombant en principe aux personnes inscrites sur la liste des demandeurs d'emploi en des termes analogues aux dispositions des articles R. 351-27 et R. 351-28 du code du travail dont le préfet du Gard a fait application ;<br/>
<br/>
              Considérant, toutefois, que la nature et la sévérité des sanctions administratives associées à la méconnaissance de cette obligation ont été modifiés postérieurement à la décision attaquée en conséquence, notamment, de l'entrée en vigueur de la loi du 18 janvier 2005 de programmation pour la cohésion sociale et du décret du 2 août 2005 relatif au suivi de la recherche d'emploi pris pour son application ; qu'en particulier, si les dispositions précitées de l'article R. 351-28 dont le préfet du Gard a fait application l'habilitaient, en cas de manquement à l'obligation de recherche d'emploi, à prononcer l'exclusion définitive du revenu de remplacement, il résulte des dispositions du 1° de l'article R. 5426-3 du code du travail désormais applicable qu'en cas de premier manquement à cette obligation, l'administration ne peut que réduire de 20 % le montant du revenu de remplacement pendant une durée de deux à six mois ; que ce n'est qu'en cas de réitération de ces mêmes manquements qu'elle peut réduire le montant du revenu de remplacement de 50 % pour une durée de deux à six mois ou mettre définitivement fin au bénéfice du revenu de remplacement ; que, dans ces conditions, ces dispositions nouvelles doivent être regardées comme plus douces que les dispositions anciennes ; que, par suite, il y a lieu pour le Conseil d'Etat, statuant au contentieux comme juge de plein contentieux sur la demande de M. B..., d'appliquer ces dispositions au manquement qui lui est imputé ;<br/>
<br/>
              Considérant qu'il résulte de l'instruction que l'invalidité dont fait état M.  B...ne faisait pas obstacle en l'espèce à ce qu'il accomplisse, comme il lui incombait, des actes positifs de recherche d'emploi ; que l'intéressé, qui se borne à produire une liste d'annonces relatives à des postes susceptibles, selon lui, de correspondre à son profil professionnel, ne soutient pas avoir contacté les employeurs correspondants et ne peut, dès lors, être regardé comme ayant accompli des actes positifs de recherche d'emploi, nonobstant l'absence de proposition émanant des services de l'Agence nationale pour l'emploi alors en charge du placement des demandeurs d'emploi ; que la méconnaissance de cette obligation est de nature à justifier légalement le prononcé d'une sanction administrative à son encontre, sur le fondement de l'article R. 5426-3 du code du travail ;<br/>
<br/>
              Mais considérant que les faits reprochés à M. B...revêtent, pour l'application des dispositions de cet article R. 5426-3 du code du travail, le caractère d'un premier manquement à l'obligation d'accomplir des actes positifs de recherche d'emploi ; qu'il suit de là qu'ils ne sauraient, en application de ce même article, entraîner son exclusion définitive du bénéfice du revenu de remplacement ; que, dans les circonstances de l'espèce, il y a lieu de substituer à la décision litigieuse du préfet du Gard une mesure de réduction de 20 % du revenu de remplacement alloué à M. B...pour une durée de deux mois à compter du 8 octobre 2003 ;<br/>
<br/>
              Sur les conclusions à fin d'injonction :<br/>
<br/>
              Considérant que la présente décision n'implique par elle-même aucune mesure d'exécution ; que, dès lors, les conclusions de M. B...aux fins d'injonction ne peuvent qu'être rejetées ; <br/>
<br/>
              Sur les conclusions de M. B...tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de faire application des dispositions de l'article L. 761-1 du code de justice administrative et de mettre à la charge de l'Etat le versement à M. B...de la somme de 3 000 euros ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 3 septembre 2009 de la cour administrative d'appel de Marseille et le jugement du 21 décembre 2006 du tribunal administratif de Montpellier sont annulés.<br/>
Article 2 : Une mesure de réduction de 20 % du revenu de remplacement alloué à M. B..., pour une durée de deux mois à compter du 8 octobre 2003, est substituée à la mesure d'exclusion définitive prononcée par le préfet du Gard.<br/>
Article 3 : Le surplus des conclusions de la demande présentée par M. B...devant le tribunal administratif de Montpellier est rejeté.<br/>
Article 4 : L'Etat versera à M. B...la somme de 3 000 euros en application de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : La présente décision sera notifiée à M. A...B...et au ministre du travail, de l'emploi et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-08-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. APPLICATION DANS LE TEMPS. TEXTE APPLICABLE. - SANCTION INFLIGÉE PAR L'ADMINISTRATION À UN ADMINISTRÉ - LITIGE DE PLEIN CONTENTIEUX - CONSÉQUENCE - APPLICATION DE LA LOI NOUVELLE PLUS DOUCE ENTRÉE EN VIGUEUR ENTRE LA DATE DES FAITS ET CELLE À LAQUELLE LE JUGE STATUE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-02-02-01 PROCÉDURE. DIVERSES SORTES DE RECOURS. RECOURS DE PLEIN CONTENTIEUX. RECOURS AYANT CE CARACTÈRE. - RECOURS DIRIGÉ CONTRE UNE SANCTION INFLIGÉE PAR L'ADMINISTRATION À UN ADMINISTRÉ - EXCLUSION DU REVENU DE REMPLACEMENT DES DEMANDEURS D'EMPLOI POUR NON JUSTIFICATION D'ACTES POSITIFS DE RECHERCHE D'EMPLOI (ART. R. 351-28 DE L'ANCIEN CODE DU TRAVAIL, DEVENU L'ART. R. 5411-12) - CONSÉQUENCE -  APPLICATION DE LA LOI NOUVELLE PLUS DOUCE ENTRÉE EN VIGUEUR ENTRE LA DATE DES FAITS ET CELLE À LAQUELLE LE JUGE STATUE [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">66-10-02 TRAVAIL ET EMPLOI. POLITIQUES DE L'EMPLOI. INDEMNISATION DES TRAVAILLEURS PRIVÉS D'EMPLOI. - EXCLUSION DU REVENU DE REMPLACEMENT DES DEMANDEURS D'EMPLOI POUR NON JUSTIFICATION D'ACTES POSITIFS DE RECHERCHE D'EMPLOI  (ART. R. 351-28 DE L'ANCIEN CODE DU TRAVAIL, DEVENU L'ART. R. 5411-12) - NATURE DU RECOURS - RECOURS DE PLEIN CONTENTIEUX - CONSÉQUENCE -  APPLICATION DE LA LOI NOUVELLE PLUS DOUCE ENTRÉE EN VIGUEUR ENTRE LA DATE DES FAITS ET CELLE À LAQUELLE LE JUGE STATUE [RJ1].
</SCT>
<ANA ID="9A"> 01-08-03 L'exclusion du revenu de remplacement des demandeurs d'emploi sur le fondement de l'article R. 351-28 de l'ancien code du travail (devenu l'article R. 5411-12) pour non-justification d'actes positifs de recherche d'emploi revêt, en raison de ses motifs et des effets qui lui sont attachés, le caractère d'une sanction. Le recours formé contre une telle sanction que l'administration inflige à un administré présente le caractère d'un recours de plein contentieux. La nature et la sévérité des sanctions administratives associées à la méconnaissance de cette obligation ayant été modifiées postérieurement à la décision attaquée en conséquence, notamment, de l'entrée en vigueur de la loi du 18 janvier 2005 de programmation pour la cohésion sociale et du décret du 2 août 2005 relatif au suivi de la recherche d'emploi pris pour son application, il y a lieu de faire application des dispositions nouvelles plus douces.</ANA>
<ANA ID="9B"> 54-02-02-01 L'exclusion du revenu de remplacement des demandeurs d'emploi sur le fondement de l'article R. 351-28 de l'ancien code du travail (devenu l'article R. 5411-12) pour non justification d'actes positifs de recherche d'emploi revêt, en raison de ses motifs et des effets qui lui sont attachés, le caractère d'une sanction. Le recours formé contre une telle sanction que l'administration inflige à un administré présente le caractère d'un recours de plein contentieux. La nature et la sévérité des sanctions administratives associées à la méconnaissance de cette obligation ayant été modifiées postérieurement à la décision attaquée en conséquence, notamment, de l'entrée en vigueur de la loi du 18 janvier 2005 de programmation pour la cohésion sociale et du décret du 2 août 2005 relatif au suivi de la recherche d'emploi pris pour son application, il y a lieu de faire application des dispositions nouvelles plus douces.</ANA>
<ANA ID="9C"> 66-10-02 L'exclusion du revenu de remplacement des demandeurs d'emploi sur le fondement de l'article R. 351-28 de l'ancien code du travail (devenu l'article R. 5411-12) pour non justification d'actes positifs de recherche d'emploi revêt, en raison de ses motifs et des effets qui lui sont attachés, le caractère d'une sanction. Le recours formé contre une telle sanction que l'administration inflige à un administré présente le caractère d'un recours de plein contentieux. La nature et la sévérité des sanctions administratives associées à la méconnaissance de cette obligation ayant été modifiées postérieurement à la décision attaquée en conséquence, notamment, de l'entrée en vigueur de la loi du 18 janvier 2005 de programmation pour la cohésion sociale et du décret du 2 août 2005 relatif au suivi de la recherche d'emploi pris pour son application, il y a lieu de faire application des dispositions nouvelles plus douces.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 16 février 2009, Société ATOM, n° 274000, p. 26.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
