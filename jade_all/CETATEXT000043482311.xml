<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043482311</ID>
<ANCIEN_ID>JG_L_2021_05_000000434502</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/48/23/CETATEXT000043482311.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 05/05/2021, 434502, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-05-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>434502</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI</AVOCATS>
<RAPPORTEUR>M. Laurent Roulaud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Domingo</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:434502.20210505</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
      M. A... B... a demandé au tribunal administratif de Paris, d'une part, d'annuler la décision par laquelle la garde des sceaux, ministre de la justice, a implicitement refusé de faire droit à sa demande de consultation, de communication, de publication et de réutilisation des minutes civiles du tribunal de grande instance de Paris rendues en audience publique et, d'autre part, d'enjoindre à la garde des sceaux, ministre de la justice, de lui permettre d'accéder à ces documents dans un délai raisonnable et sous astreinte de 200 euros par jours de retard. <br/>
<br/>
      Par un jugement n°1717801/5-3 du 10 juillet 2019, le tribunal administratif de Paris a rejeté ces demandes.<br/>
<br/>
      Par un pourvoi, un mémoire complémentaire et un mémoire en réplique, enregistrés les 10 septembre et 10 décembre 2019 et le 4 février 2021 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
      1°) d'annuler ce jugement ;<br/>
<br/>
      2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
      3°) à titre subsidiaire, de saisir la Cour de justice de l'Union européenne à titre préjudiciel ;<br/>
<br/>
      4°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
      Vu les autres pièces du dossier ;<br/>
<br/>
      Vu :<br/>
      -  la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
      -  le code des relations entre le public et l'administration ; <br/>
      -  le code de justice administrative et le décret n° 2010-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
      Après avoir entendu en séance publique : <br/>
<br/>
      - le rapport de M. Laurent Roulaud, maître des requêtes en service extraordinaire,  <br/>
<br/>
      - les conclusions de M. Laurent Domingo, rapporteur public ;<br/>
<br/>
      La parole ayant été donnée, après les conclusions, à la SCP Spinosi, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un courrier en date du 18 décembre 2016 adressé au greffe du tribunal de grande instance de Paris, M. B... a demandé la communication des minutes civiles des jugements prononcés en audience publique par ce tribunal en vue de la réutilisation des informations publiques contenues dans celles-ci. A la suite du rejet de sa demande, le 9 janvier 2017, par le directeur du greffe du même tribunal, et de l'avis de la commission d'accès aux documents administratifs en date du 7 septembre 2017, M. B... a demandé au tribunal administratif de Paris d'annuler le refus de communication qui lui était opposé. Il se pourvoit en cassation contre le jugement du 10 juillet 2019 par lequel le tribunal administratif de Paris a rejeté sa demande.<br/>
<br/>
              2. En premier lieu, il ressort de l'examen de la minute du jugement attaqué qu'elle a été signée par le président de la formation de jugement, le rapporteur et le greffier d'audience, conformément aux exigences de l'article R. 741-7 du code de justice administrative. Par suite, le moyen tiré de son défaut de signature manque en fait.<br/>
<br/>
              3. En deuxième lieu, le premier alinéa de l'article L. 300-2 du code des relations entre le public et l'administration dispose que : " Sont considérés comme documents administratifs, au sens des titres Ier, III et IV du présent livre, quels que soient leur date, leur lieu de conservation, leur forme et leur support, les documents produits ou reçus, dans le cadre de leur mission de service public, par l'Etat, les collectivités territoriales ainsi que par les autres personnes de droit public ou les personnes de droit privé chargées d'une telle mission. Constituent de tels documents notamment les dossiers, rapports, études, comptes rendus, procès-verbaux, statistiques, instructions, circulaires, notes et réponses ministérielles, correspondances, avis, prévisions, codes sources et décisions ".<br/>
<br/>
              4. Les documents, quelle que soit leur nature, qui se rattachent à la fonction juridictionnelle, et notamment les jugements des juridictions judiciaires, n'ont pas le caractère de documents administratifs pour l'application du droit de communication des documents mentionnés au premier alinéa de l'article L. 300-2 du code des relations entre le public et l'administration. Par suite, le tribunal administratif de Paris n'a pas commis d'erreur de droit en jugeant que les jugements du tribunal de grande instance de Paris dont la communication était demandée ne présentent pas le caractère de documents administratifs communicables sur le fondement de ces dispositions.<br/>
<br/>
              5. En troisième lieu, si M. B... se prévalait des dispositions de l'article L. 321-1 du code des relations entre le public et l'administration, selon lesquelles " les informations publiques figurant dans des documents communiqués ou publiés par les administrations mentionnées au premier alinéa de l'article L. 300-2 peuvent être utilisées par toute personne qui le souhaite à d'autres fins que celles de la mission de service public pour les besoins de laquelle les documents ont été produits ou reçus ", ces dispositions relatives au droit à la réutilisation des informations publiques qui figurent dans des documents communiqués ou publiés par les administrations sont dépourvues d'incidence sur l'étendue du droit à communication de documents administratifs résultant du code des relations entre le public et l'administration. Par suite, le tribunal, par un jugement suffisamment motivé, n'a pas commis d'erreur de droit en jugeant que le droit à réutilisation des informations contenues dans des jugements civils n'ouvrait pas à M. B... un droit à leur communication sur le fondement des dispositions du code des relations entre le public et l'administration. Le requérant ne saurait utilement se prévaloir de ce que le tribunal aurait ainsi méconnu la liberté de recevoir et de communiquer des informations garantie par l'article 10 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. Si le tribunal a aussi jugé que le requérant ne pouvait utilement se prévaloir de l'article L. 111-13 du code de l'organisation judiciaire, ce motif est surabondant de telle sorte que le moyen tiré de ce qu'il serait entaché d'erreur de droit et d'insuffisance de motivation est inopérant. <br/>
<br/>
              6. Il résulte de tout ce qui précède que, sans qu'il y ait lieu de saisir la Cour de justice de l'Union européenne d'une question préjudicielle, M. B... n'est pas fondé à demander l'annulation du jugement qu'il attaque. Par suite, son pourvoi doit être rejeté, y compris les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
      D E C I D E :<br/>
      --------------<br/>
<br/>
Article 1er : Le pourvoi de M. B... est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. A... B... et au garde des sceaux, ministre de la justice.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
