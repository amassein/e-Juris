<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037791410</ID>
<ANCIEN_ID>JG_L_2018_12_000000416596</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/79/14/CETATEXT000037791410.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 10/12/2018, 416596, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-12-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>416596</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT, ROBILLOT ; SCP COUTARD, MUNIER-APAIRE</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:416596.20181210</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Montpellier, d'une part, d'annuler la décision implicite par laquelle le maire de Salses-le-Château a rejeté sa demande de titularisation et, d'autre part, d'enjoindre au maire de Salses-le-Château de procéder à sa titularisation à compter du 1er avril 2015 et à la reconstitution de sa carrière dans le grade d'adjoint technique territorial de 2ème classe. Par un jugement n° 1506436 du 21 décembre 2016, le tribunal administratif de Montpellier a annulé la décision implicite du maire de la commune de Salses-le-Château et enjoint à la commune de procéder à la titularisation de Mme A...à compter du 1er avril 2015 et à la reconstitution de sa carrière dans le grade d'adjoint technique territorial de 2ème classe.<br/>
<br/>
              Par un arrêt n°s 17MA00669, 17MA00712 du 13 juillet 2017, la cour administrative d'appel de Marseille  a, sur appel de la commune de Salses-le-Château, annulé ce jugement et rejeté la demande de Mme A...présentée devant le tribunal administratif de Montpellier.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 18 décembre 2017 et 19 mars 2018 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la commune de Salses-le-Château ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Salses-le-Château la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le décret n° 92-850 du 28 août 1992 ;<br/>
              - le décret n° 92-1194 du 4 novembre 1992 ;<br/>
              - le décret n° 2006-1691 du 22 décembre 2006 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean Sirinelli, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Potier de la Varde, Buk Lament, Robillot, avocat de Mme A...et à la SCP Coutard, Munier-Apaire, avocat de la commune de Salses-le-Château.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 22 novembre 2018, présentée par Mme A... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme A...a été recrutée, de manière discontinue, par le centre communal d'action sociale de la commune de Salses-le-Château en qualité d'agent d'entretien entre le 26 novembre 2003 et le 31 décembre 2006, puis par la commune de Salses-le-Château en la même qualité d'agent d'entretien au cours de la période du 6 septembre 2007 au 31 décembre 2010 et, enfin, en qualité d'agent d'animation au cours de la période du 1er janvier 2011 au 31 mars 2013. Par un arrêté en date du 24 mars 2013, Mme A...a été nommée stagiaire à compter du 1er avril 2013 dans le grade d'adjoint technique territorial de 2ème classe pour une durée d'un an. Après avoir prolongé le stage de Mme A... pour une durée d'un an à compter du 1er avril 2014, le maire de la commune a refusé de procéder à sa titularisation à l'issue de sa période probatoire. Par un courrier du 5 août 2015, Mme A...a présenté au maire de Salses-le-Château une demande tendant à sa titularisation, qu'il a rejetée. Par un jugement du 21 décembre 2016, le tribunal administratif de Montpellier a, d'une part, annulé la décision implicite par laquelle le maire de Salses-le-Château a rejeté la demande de titularisation de Mme A...et, d'autre part, enjoint à celui-ci de procéder à la titularisation de Mme A...à compter du 1er avril 2015 et à la reconstitution de sa carrière dans le grade d'adjoint technique territorial de 2ème classe. Par l'arrêt attaqué du 13 juillet 2017, la cour administrative d'appel de Marseille a annulé ce jugement et rejeté la demande de Mme A....<br/>
<br/>
              2. Aux termes de l'article 46 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale : " La nomination, intervenant dans les conditions prévues aux articles 25,36 ou 38, paragraphes a et d, ou 39 de la présente loi à un grade de la fonction publique territoriale présente un caractère conditionnel. La titularisation peut être prononcée à l'issue d'un stage dont la durée est fixée par le statut particulier ". Selon les dispositions de l'article 1er du décret du 4 novembre 1992 fixant les dispositions communes applicables aux fonctionnaires stagiaires de la fonction publique territoriale : " Est fonctionnaire territorial stagiaire la personne qui, nommée dans un emploi permanent de la hiérarchie administrative des communes, (...) accomplit les fonctions afférentes audit emploi et a vocation à être titularisée dans le grade correspondant à cet emploi ".<br/>
<br/>
              3. En premier lieu, le moyen tiré de ce que Mme A...se serait vu confier des fonctions d'agent territorial spécialisé des écoles maternelles ne correspondant pas à celles qui peuvent être exercées par un adjoint technique territorial de 2ème classe n'a pas été invoqué devant les juges du fond. La cour administrative d'appel de Marseille n'a par ailleurs pas commis d'erreur de droit en ne recherchant pas d'office si  l'exercice de telles fonctions n'avait pas eu pour effet de placer l'intéressée, durant son stage, sur un emploi ne correspondant pas au grade sur lequel elle avait vocation à être nommée.<br/>
<br/>
              4. En deuxième lieu, l'absence d'intervention d'une décision expresse de refus de titularisation et de licenciement  à l'issue de la période de stage ne fait pas naître une obligation de titulariser le fonctionnaire stagiaire. Par suite, Mme A...n'est, en tout état de cause, pas fondée à soutenir que la cour administrative d'appel de Marseille aurait commis une erreur de droit en ne tirant pas les conséquences de l'absence de décision expresse de licenciement ou de refus de titularisation à l'issue de la période de stage de deux ans.<br/>
<br/>
              5. En troisième lieu, il ressort des énonciations de l'arrêt attaqué que la cour administrative d'appel de Marseille s'est fondée, à titre principal, pour estimer que le maire de la commune de Salses-le-Château n'avait pas fait une appréciation manifestement erronée de l'aptitude de MmeA..., sur les constats du rapport de stage du 3 juillet 2014, rédigé à l'issue de la première des périodes probatoires de MmeA..., ainsi que sur les différents manquements relevés par l'autorité administrative à l'issue de sa seconde période probatoire. La cour a également tenu compte de trois appréciations d'élus de la commune de Salses-le-Château, du constat dressé par la directrice du centre de loisirs péri et extrascolaire ainsi que du témoignage d'un animateur qui attestaient des difficultés de l'intéressée à travailler avec ses collègues et en présence d'enfants. Ces témoignages, sur lesquels la cour ne s'est fondée qu'à titre complémentaire, émanent de personnes qui, par leurs fonctions, ont pu apprécier la manière de servir de l'intéressée. Dans ces conditions, la cour n'a pas commis d'erreur de droit ou dénaturé les pièces du dossier en prenant en compte ces témoignages alors même qu'ils portent sur une période qui n'est pas précisément définie.<br/>
<br/>
              6. En quatrième lieu, la cour administrative d'appel de Marseille n'a pas entaché son arrêt d'une contradiction de motifs en estimant que le maire de la commune de Salses-le-Château n'avait pas fait une appréciation manifestement erronée de l'aptitude de Mme A... à exercer les fonctions auxquelles elle était destinée tout en relevant également l'existence de témoignages et attestations favorables à l'intéressée, dès lors qu'elle a relevé que les témoignages et attestations produits par MmeA..., compte tenu de leurs auteurs, n'étaient pas de nature à contredire utilement l'appréciation de l'autorité administrative.<br/>
<br/>
              7. En dernier lieu, eu égard aux appréciations défavorables portées par l'autorité administrative à l'issue des deux périodes probatoires, Mme A...n'est pas fondée à soutenir que la cour administrative d'appel de Marseille aurait dénaturé les pièces du dossier en estimant que le maire de la commune de Salses-le-Château n'a pas fait une appréciation manifestement erronée de son aptitude à exercer les fonctions auxquelles elle était destinée. La cour n'a également pas dénaturé les pièces du dossier en relevant que Mme A... n'avait été employée par la commune que pour répondre à des besoins occasionnels, dès lors qu'il ressort des pièces du dossier soumis aux juges du fond que l'intéressée a été recrutée de manière discontinue et pour exercer des fonctions de nature différente.<br/>
<br/>
              8. Il résulte de tout ce qui précède que Mme A...n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              9. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de la commune de Salses-le-Château  qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de Mme A...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à Mme B...A...et à la commune de Salses-le-Château.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
