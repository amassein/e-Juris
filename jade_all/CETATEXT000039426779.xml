<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039426779</ID>
<ANCIEN_ID>JG_L_2019_11_000000422211</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/42/67/CETATEXT000039426779.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 27/11/2019, 422211, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-11-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>422211</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Marc Pichon de Vendeuil</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:422211.20191127</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A... B..., épouse C..., a saisi le tribunal administratif de Marseille d'une demande tendant à ce que soit reconnue sa situation de longue maladie depuis le 2 octobre 2002. Par un jugement n° 1407338 du 7 novembre 2016, le tribunal administratif de Marseille a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 17MA00038 du 5 juin 2018, la cour administrative d'appel de Marseille a rejeté l'appel formé par Mme B... contre ce jugement.<br/>
<br/>
              Par un pourvoi et un nouveau mémoire, enregistrés les 12 juillet et 26 octobre 2018 au secrétariat du contentieux du Conseil d'Etat, Mme B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
<br/>
<br/>
              Vu :<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Pichon de Vendeuil, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de Mme B... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumises aux juges du fond que Mme B..., épouse C..., entrée dans l'administration pénitentiaire le 1er mars 1978, a exercé les fonctions de secrétaire administrative au centre de détention de Salon-de-Provence du 12 janvier 2000 jusqu'à sa mise à la retraite le 2 novembre 2003. Après avoir été placée en congé de maladie et après qu'un congé de longue maladie lui a été refusé, Mme B... a été déclarée apte à la reprise du travail le 22 juillet 2003 par le comité médical départemental des Bouches-du-Rhône. Le 1er septembre 2003, à sa reprise de fonction, Mme B... a été victime d'un malaise dans le bureau du directeur du centre de détention. La reconnaissance de l'imputabilité au service de ce malaise lui ayant été refusée le 8 avril 2004 par la direction interrégionale des services pénitentiaires de Marseille, Mme B... a saisi le tribunal administratif de Marseille d'une requête tendant à l'annulation de cette décision, qui a été rejetée par un jugement du 8 juin 2006, devenu définitif. Par un courrier du 6 juin 2007, Mme B... a demandé au directeur du centre de détention de Salon-de-Provence que soit reconnue son inaptitude au service à compter du 1er septembre 2003 et que soit prononcée en conséquence sa mise à la retraite pour invalidité avec effet rétroactif au 1er septembre 2003. Cette demande a été rejetée le 19 juin 2007 par le directeur interrégional des services pénitentiaires de Marseille. Par un nouveau courrier en date du 11 juin 2014, Mme B... a demandé au directeur du centre de détention de Salon-de-Provence la reconnaissance de sa situation de longue maladie depuis le 2 octobre 2002. Par un jugement du 7 novembre 2016, le tribunal administratif de Marseille a rejeté la demande de Mme B... tendant à l'annulation de la décision implicite de rejet de sa demande du 11 juin 2014 tendant à être placée en congé de longue maladie. Mme B... se pourvoit en cassation contre l'arrêt du 5 juin 2018 par lequel la cour administrative d'appel de Marseille a rejeté son appel dirigé contre ce jugement.<br/>
<br/>
              2. En présence d'une formulation différente d'un moyen examiné par le tribunal administratif, le juge d'appel peut se prononcer sur ce moyen par adoption des motifs des premiers juges sans méconnaître le principe de motivation des jugements, rappelé à l'article L. 9 du code de justice administrative, dès lors que la réponse du tribunal à ce moyen était elle-même suffisante et n'appelait pas de nouvelles précisions en appel.<br/>
<br/>
              3. Il ressort des pièces du dossier soumis à la cour administrative d'appel de Marseille que si elle a produit des pièces nouvelles et étoffé son argumentation, Mme B... n'a soulevé aucun moyen nouveau par rapport à celui qu'elle avait invoqué en première instance pour contester la décision implicite de rejet de sa demande du 11 juin 2014 tendant à être placée en congé de longue maladie. Dans ces conditions et dès lors que la réponse du tribunal à ce moyen était elle-même suffisante et n'appelait pas de nouvelles précisions en appel, la cour a pu écarter l'argumentation dont elle était saisie par adoption des motifs retenus par le tribunal administratif de Marseille sans entacher son arrêt d'insuffisance de motivation, ni d'erreur de droit, ni enfin de dénaturation des pièces du dossier.<br/>
<br/>
              4. Il résulte de ce qui précède que Mme B... n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              5. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de Mme B... est rejeté.<br/>
Article 2 : La présente décision sera notifiée à Mme A... B..., épouse C..., et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
