<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036673263</ID>
<ANCIEN_ID>JG_L_2018_03_000000416567</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/67/32/CETATEXT000036673263.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 05/03/2018, 416567, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-03-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>416567</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Alexandre  Koutchouk</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:416567.20180305</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société anonyme Vétoquinol, à l'appui de la demande qu'elle a formée devant le tribunal administratif de Besançon tendant à la restitution, assortie des intérêts moratoires prévus à l'article L. 208 du code général des impôts, à concurrence de la prise en compte dans son bénéfice taxable d'une quote-part de 5% des dividendes reçus de ses filiales situées dans l'Union européenne et en Suisse, des cotisations d'impôt sur les sociétés auxquelles elle a été assujettie au titre des années 2008, 2009, 2011, 2012 et 2013, a produit un mémoire et un mémoire complémentaire, enregistrés les 30 octobre 2017 et 27 novembre 2017 au greffe de ce tribunal, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958, par lesquels elle soulève une question prioritaire de constitutionnalité. <br/>
<br/>
              Par une ordonnance n° 1500878 du 12 décembre 2017, enregistrée le 15 décembre 2017 au secrétariat du contentieux du Conseil d'Etat, le président de la 1ère chambre du tribunal administratif de Besançon, a décidé, par application des dispositions de l'article 23-2 de l'ordonnance du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du deuxième alinéa du I de l'article 216 du code général des impôts, dans leur rédaction issue de l'article 10 de la loi n° 2010-1657 du 29 décembre 2010 de finances pour 2011.<br/>
<br/>
              Par la question prioritaire de constitutionnalité transmise et un nouveau mémoire, enregistré le 23 janvier 2018 au secrétariat du contentieux du Conseil d'État, la société Vétoquinol soutient que les dispositions du deuxième alinéa du I de l'article 216 du code général des impôts, applicables au litige, méconnaissent le principe d'égalité devant les charges publiques énoncé à l'article 13 de la Déclaration des droits de l'homme et du citoyen de 1789.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ; <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - la loi n° 2010-1657 du 29 décembre 2010, notamment son article 10 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alexandre Koutchouk, maître des requêtes en service extraodrinaire,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 16 février 2018, présentée par la société Vétoquinol ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement de circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. L'article 216 du code général des impôts prévoit, au premier alinéa de son I, que les produits nets des participations, ouvrant droit à l'application du régime des sociétés mères et visées à l'article 145, perçus au cours d'un exercice par une société mère, peuvent être retranchés du bénéfice total de celle-ci, déduction faite d'une quote-part représentative des frais et charges liés à la détention et à la gestion de ces participations. Le deuxième alinéa de ce même I dispose, dans sa rédaction issue de l'article 10 de la loi du 29 décembre 2010 de finances pour 2011, que : " La quote-part de frais et charges visée au premier alinéa est fixée uniformément à 5% du produit total des participations, crédit d'impôt compris ". La société requérante soutient que ces dispositions, en ce qu'elles soumettent à l'impôt sur les sociétés une quote-part de 5 % des produits de participations perçus par une société mère, sans considération du montant réel des frais et charges exposés pour la gestion de ces participations, méconnaissent le principe d'égalité devant les charges publiques énoncé à l'article 13 de la Déclaration des droits de l'homme et du citoyen. Elle soutient que cette imposition forfaitaire, qui conduit selon elle à taxer des revenus fictifs, est établie en méconnaissance des capacités contributives du contribuable et que l'objectif constitutionnel de lutte contre la fraude fiscale ne dispense pas le législateur, lorsqu'il déroge pour ce motif à l'égalité devant les charges publiques, de réserver la possibilité pour le contribuable d'établir que sa situation ne procède pas d'une volonté d'éluder l'impôt.  <br/>
<br/>
              3. Il résulte de la combinaison des articles 205, 209 et 38 du code général des impôts que l'impôt sur les sociétés est établi sur l'ensemble des bénéfices de toute nature réalisés par chaque société. La base d'imposition à l'impôt sur les sociétés inclut donc, en principe, les produits de participation perçus de filiales. Le législateur a, toutefois, prévu par l'article 216 du code général des impôts, dans le but d'éviter l'imposition successive des mêmes bénéfices entre les mains de la société filiale puis de la société mère, la possibilité pour cette dernière, lorsque les conditions posées par l'article 145 du même code sont respectées, de retrancher de son bénéfice taxable le montant des produits de participation perçus de ses filiales, à l'exception d'une quote-part de 5 % représentative des frais et charges exposés pour acquérir ces revenus. <br/>
<br/>
              4. En premier lieu, il découle de ce qui précède qu'en prévoyant, à l'article 216 du code général des impôts, la soumission à l'impôt sur les sociétés d'une quote-part des produits de participation perçus par une société mère, qui constituent des revenus dont elle a disposé, le législateur s'est borné à préciser la portée de l'exonération qu'il instituait sans soumettre à l'impôt, quel que soit le montant de cette quote-part et contrairement à ce qui est soutenu, des revenus fictifs. <br/>
<br/>
              5. En deuxième lieu, il appartient au législateur de déterminer, dans le respect des principes constitutionnels et compte tenu des caractéristiques de chaque impôt, les règles selon lesquelles doivent être appréciées les facultés contributives en fondant en particulier son appréciation sur des critères objectifs et rationnels en fonction des buts qu'il se propose, sans que cette appréciation ne puisse entraîner de rupture caractérisée de l'égalité devant les charges publiques. En faisant le choix de soumettre à l'impôt sur les bénéfices les produits de participation perçus de filiales à hauteur d'une assiette correspondant à 5 % de leur montant brut, crédit d'impôt compris, le législateur a établi une règle de taxation fondée sur des critères objectifs et en rapport avec le but d'élimination de la double imposition qu'il se fixait et n'a, eu égard à la nature des revenus en cause et quels que soient les motifs pour lesquels il a déterminé l'imposition ainsi instituée, pas fait peser sur les sociétés mères, lesquelles sont toutes traitées de manière identique, une charge excessive au regard de la capacité contributive que leur confère la perception de ces revenus. La société n'est, par suite, pas fondée à soutenir que le législateur ne pouvait, sans méconnaître le principe d'égalité devant les charges publiques, établir une telle imposition sans réserver la possibilité pour le contribuable d'établir que la fraction ainsi taxée des produits bruts de participations excèderait le montant réel des frais et charges exposés en vue de l'obtention de ces revenus.<br/>
<br/>
              6. En troisième lieu, il résulte de la jurisprudence du Conseil constitutionnel qu'il est à tout moment loisible au législateur, statuant dans le domaine de sa compétence, de modifier des textes antérieurs ou d'abroger ceux-ci en leur substituant, le cas échéant, d'autres dispositions, à condition de ne pas priver de garanties légales des exigences constitutionnelles. Le législateur n'a pas méconnu le principe d'égalité devant les charges publiques en supprimant, par l'article 10 de la loi du 29 décembre 2010 de finances pour 2011, à compter des impositions établies au titre de 2010, la limitation du montant de la quote-part d'imposition des produits de participation au montant réel des frais et charges de toute nature exposés par la société participante au cours de la période d'imposition concernée. <br/>
<br/>
              7. Dès lors, la question prioritaire de constitutionnalité soulevée par la société requérante, qui n'est pas nouvelle, ne peut être regardée comme présentant un caractère sérieux. Par suite, il n'y a pas lieu de la renvoyer au Conseil constitutionnel.<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité transmise par le président de la 1ère chambre du tribunal administratif de Besançon.<br/>
Article 2 : La présente décision sera notifiée à la société anonyme Vétoquinol et au ministre de l'action et des comptes publics. <br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre et au tribunal administratif de Besançon.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
