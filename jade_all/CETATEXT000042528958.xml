<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042528958</ID>
<ANCIEN_ID>JG_L_2020_11_000000440418</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/52/89/CETATEXT000042528958.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 16/11/2020, 440418</TITRE>
<DATE_DEC>2020-11-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440418</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SARL DIDIER, PINET</AVOCATS>
<RAPPORTEUR>M. Arnaud Skzryerbak</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:440418.20201116</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 5 mai 2020 au secrétariat du contentieux du Conseil d'Etat, la Confédération générale du travail (CGT), la fédération des services publics - CGT et l'union fédérale des syndicats de l'Etat - CGT demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'article 13 de l'ordonnance n° 2020-306 du 25 mars 2020 relative à la prorogation des délais échus pendant la période d'urgence sanitaire et à l'adaptation des procédures pendant cette même période ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier :<br/>
<br/>
              Vu :<br/>
              - la loi n° 2020-290 du 23 mars 2020 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arnaud Skzryerbak, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SARL Didier, Pinet, avocat de la Confédération générale du travail et autres ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Le 2° du I de l'article 11 de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19 a habilité le Gouvernement, pendant trois mois, à prendre par ordonnances, dans les conditions prévues à l'article 38 de la Constitution, " Afin de faire face aux conséquences, notamment de nature administrative ou juridictionnelle, de la propagation de l'épidémie de covid-19 et des mesures prises pour limiter cette propagation, toute mesure : / a) Adaptant (...) les délais et les modalités de consultation du public ou de toute instance ou autorité, préalables à la prise d'une décision par une autorité administrative et, le cas échéant, les délais dans lesquels cette décision peut ou doit être prise ou peut naître (...) ". Sur le fondement de ces dispositions, le Gouvernement a adopté l'article 13 de l'ordonnance du 25 mars 2020 relative à la prorogation des délais échus pendant la période d'urgence sanitaire et à l'adaptation des procédures pendant cette même période, qui prévoit que : " Sous réserve des obligations résultant du droit international et du droit de l'Union européenne, les projets de texte réglementaire ayant directement pour objet de prévenir les conséquences de la propagation du covid-19 ou de répondre à des situations résultant de l'état d'urgence sanitaire sont dispensés de toute consultation préalable obligatoire prévue par une disposition législative ou réglementaire, à l'exception de celles du Conseil d'Etat et des autorités saisies pour avis conforme ".<br/>
<br/>
              2. Si la loi du 23 mars 2020 habilite le Gouvernement à prendre des mesures relevant du domaine de la loi pour adapter les procédures consultatives préalables à l'édiction des décisions administratives nécessaires pour faire face aux conséquences de l'épidémie de covid-19, cette habilitation ne porte, selon les termes mêmes de la loi, que sur les délais et les modalités de la consultation. Sous réserve des exceptions qu'il mentionne, l'article 13 de l'ordonnance du 25 mars 2020 dispense les projets de texte réglementaire ayant directement pour objet de prévenir les conséquences de la propagation du covid-19 ou de répondre à des situations résultant de l'état d'urgence sanitaire de toute consultation préalable obligatoire prévue par une disposition législative ou réglementaire. Cette disposition ne modifie pas les délais et modalités des consultations normalement applicables mais remet en cause leur principe même. Elle n'entre pas dans le champ de l'habilitation donnée au Gouvernement par la loi du 23 mars 2020. Cependant, il entre dans la compétence du pouvoir réglementaire d'écarter l'application de procédures consultatives elles-mêmes prévues par des dispositions réglementaires. Par suite, les requérants sont fondés à demander l'annulation de l'article 13 de l'ordonnance du 25 mars 2020 en tant seulement qu'il prévoit une dispense de consultations préalables obligatoires prévues par une disposition législative.<br/>
<br/>
              3.  Il ne ressort pas des pièces du dossier que l'annulation de cette disposition soit de nature à emporter des conséquences manifestement excessives en raison des effets qu'elle a produits ou des situations qui ont pu se constituer lorsqu'elle était en vigueur. Ainsi, il n'y a pas lieu, dans les circonstances de l'espèce, de limiter les effets de l'annulation de l'article 13 de l'ordonnance du 25 mars 2020. <br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à la Confédération générale du travail d'une somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 13 de l'ordonnance n° 2020-306 du 25 mars 2020 relative à la prorogation des délais échus pendant la période d'urgence sanitaire et à l'adaptation des procédures pendant cette même période est annulé en tant qu'il prévoit une dispense de consultations préalables obligatoires prévues par une disposition législative. <br/>
Article 2 : L'Etat versera à la Confédération générale du travail une somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Le surplus des conclusions de la requête est rejeté.<br/>
Article 4 : La présente décision sera notifiée à la Confédération générale du travail, première requérante dénommée, pour l'ensemble des requérantes, au Premier ministre, au garde des sceaux, ministre de la justice et à la ministre de la transformation et de la fonction publiques.<br/>
Copie en sera adressée au ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-04-04 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. ACTES LÉGISLATIFS. LOIS D'HABILITATION. - LOI HABILITANT LE GOUVERNEMENT À PRENDRE DES MESURES POUR ADAPTER LES PROCÉDURES CONSULTATIVES PRÉALABLES À L'ÉDICTION DES DÉCISIONS ADMINISTRATIVES NÉCESSAIRES POUR FAIRE FACE AUX CONSÉQUENCES DE L'ÉPIDÉMIE DE COVID-19 - PORTÉE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">01-02-01-04 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - COMPÉTENCE. LOI ET RÈGLEMENT. HABILITATIONS LÉGISLATIVES. - LOI HABILITANT LE GOUVERNEMENT À PRENDRE DES MESURES POUR ADAPTER LES PROCÉDURES CONSULTATIVES PRÉALABLES À L'ÉDICTION DES DÉCISIONS ADMINISTRATIVES NÉCESSAIRES POUR FAIRE FACE AUX CONSÉQUENCES DE L'ÉPIDÉMIE DE COVID-19 - PORTÉE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">01-03-02-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. PROCÉDURE CONSULTATIVE. CONSULTATION OBLIGATOIRE. - LOI HABILITANT LE GOUVERNEMENT À PRENDRE DES MESURES POUR ADAPTER LES PROCÉDURES CONSULTATIVES PRÉALABLES À L'ÉDICTION DES DÉCISIONS ADMINISTRATIVES NÉCESSAIRES POUR FAIRE FACE AUX CONSÉQUENCES DE L'ÉPIDÉMIE DE COVID-19 - PORTÉE - EXCLUSION - DISPENSE DE TOUTE CONSULTATION PRÉALABLE OBLIGATOIRE - CONSÉQUENCE - ANNULATION DE L'ORDONNANCE EN TANT QU'ELLE PRÉVOIT CETTE DISPENSE POUR UNE CONSULTATION PRÉVUE PAR UNE DISPOSITION LÉGISLATIVE.
</SCT>
<ANA ID="9A"> 01-01-04-04 Si la loi n° 2020-290 du 23 mars 2020 habilite le Gouvernement à prendre des mesures relevant du domaine de la loi pour adapter les procédures consultatives préalables à l'édiction des décisions administratives nécessaires pour faire face aux conséquences de l'épidémie de covid-19, cette habilitation ne porte, selon les termes mêmes de la loi, que sur les délais et les modalités de la consultation.... ,,Sous réserve des exceptions qu'il mentionne, l'article 13 de l'ordonnance n° 2020-306 du 25 mars 2020 dispense les projets de texte réglementaire ayant directement pour objet de prévenir les conséquences de la propagation du covid-19 ou de répondre à des situations résultant de l'état d'urgence sanitaire de toute consultation préalable obligatoire prévue par une disposition législative ou réglementaire. Cette disposition ne modifie pas les délais et modalités des consultations normalement applicables mais remet en cause leur principe même. Elle n'entre pas dans le champ de l'habilitation donnée au Gouvernement par la loi du 23 mars 2020. Cependant, il entre dans la compétence du pouvoir réglementaire d'écarter l'application de procédures consultatives elles-mêmes prévues par des dispositions réglementaires.... ,,Par suite, annulation de l'article 13 de l'ordonnance du 25 mars 2020 en tant seulement qu'il prévoit une dispense de consultations préalables obligatoires prévues par une disposition législative.</ANA>
<ANA ID="9B"> 01-02-01-04 Si la loi n° 2020-290 du 23 mars 2020 habilite le Gouvernement à prendre des mesures relevant du domaine de la loi pour adapter les procédures consultatives préalables à l'édiction des décisions administratives nécessaires pour faire face aux conséquences de l'épidémie de covid-19, cette habilitation ne porte, selon les termes mêmes de la loi, que sur les délais et les modalités de la consultation.... ,,Sous réserve des exceptions qu'il mentionne, l'article 13 de l'ordonnance n° 2020-306 du 25 mars 2020 dispense les projets de texte réglementaire ayant directement pour objet de prévenir les conséquences de la propagation du covid-19 ou de répondre à des situations résultant de l'état d'urgence sanitaire de toute consultation préalable obligatoire prévue par une disposition législative ou réglementaire. Cette disposition ne modifie pas les délais et modalités des consultations normalement applicables mais remet en cause leur principe même. Elle n'entre pas dans le champ de l'habilitation donnée au Gouvernement par la loi du 23 mars 2020. Cependant, il entre dans la compétence du pouvoir réglementaire d'écarter l'application de procédures consultatives elles-mêmes prévues par des dispositions réglementaires.... ,,Par suite, annulation de l'article 13 de l'ordonnance du 25 mars 2020 en tant seulement qu'il prévoit une dispense de consultations préalables obligatoires prévues par une disposition législative.</ANA>
<ANA ID="9C"> 01-03-02-02 Si la loi n° 2020-290 du 23 mars 2020 habilite le Gouvernement à prendre des mesures relevant du domaine de la loi pour adapter les procédures consultatives préalables à l'édiction des décisions administratives nécessaires pour faire face aux conséquences de l'épidémie de covid-19, cette habilitation ne porte, selon les termes mêmes de la loi, que sur les délais et les modalités de la consultation.... ,,Sous réserve des exceptions qu'il mentionne, l'article 13 de l'ordonnance n° 2020-306 du 25 mars 2020 dispense les projets de texte réglementaire ayant directement pour objet de prévenir les conséquences de la propagation du covid-19 ou de répondre à des situations résultant de l'état d'urgence sanitaire de toute consultation préalable obligatoire prévue par une disposition législative ou réglementaire. Cette disposition ne modifie pas les délais et modalités des consultations normalement applicables mais remet en cause leur principe même. Elle n'entre pas dans le champ de l'habilitation donnée au Gouvernement par la loi du 23 mars 2020. Cependant, il entre dans la compétence du pouvoir réglementaire d'écarter l'application de procédures consultatives elles-mêmes prévues par des dispositions réglementaires.... ,,Par suite, annulation de l'article 13 de l'ordonnance du 25 mars 2020 en tant seulement qu'il prévoit une dispense de consultations préalables obligatoires prévues par une disposition législative.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
