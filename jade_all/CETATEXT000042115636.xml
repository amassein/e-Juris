<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042115636</ID>
<ANCIEN_ID>JG_L_2020_07_000000429690</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/11/56/CETATEXT000042115636.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 10/07/2020, 429690</TITRE>
<DATE_DEC>2020-07-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>429690</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAMARRE, JEHANNIN ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>Mme Carine Chevrier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Stéphane Hoynck</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:429690.20200710</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A... B... a demandé au tribunal administratif de Dijon d'annuler pour excès de pouvoir la décision du 17 septembre 2015 par laquelle la présidente du conseil régional des notaires de la cour d'appel de Dijon a refusé de lui communiquer, en application de la loi n° 78-753 du 17 juillet 1978, dans sa rédaction alors applicable, la lettre d'opposition à sa nomination en qualité de notaire salariée.<br/>
<br/>
              Par un jugement n° 1800246 du 15 février 2019, le tribunal administratif de Dijon a annulé la décision du 17 septembre 2015 du conseil régional des notaires de la cour d'appel de Dijon par laquelle il a refusé de lui communiquer la lettre d'opposition, enjoint au conseil régional des notaires de la lui communiquer dans un délai d'un mois à compter du jugement et rejeté le surplus des conclusions de la requête.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 12 avril et 8 juillet 2019 au secrétariat du contentieux du Conseil d'Etat, le conseil régional des notaires de la cour d'appel de Dijon demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter les conclusions de Mme B... ;<br/>
<br/>
              3°) de mettre à la charge de Mme B... la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des relations entre le public et l'administration ;<br/>
              - la loi n° 78-753 du 17 juillet 1978 ;<br/>
              - l'ordonnance n° 45-2590 du 2 novembre 1945 modifiée ;<br/>
              - le décret n° 93-82 du 15 janvier 1993 ;<br/>
              - le décret n° 2005-1755 du 30 décembre 2005 ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 modifiée ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Carine Chevrier, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Stéphane Hoynck, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delamarre, Jéhannin, avocat du conseil régional des notaires de la cour d'appel de Dijon et à la SCP Matuchansky, Poupot, Valdelièvre, avocat de Mme B... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme B... a demandé au conseil régional des notaires de la cour d'appel de Dijon communication de la lettre d'opposition à sa nomination émise par l'un de ses confrères, recueillie par le conseil régional à l'occasion de l'avis réservé qu'il a rendu en application de l'article 11 du décret du 15 janvier 1993 portant application de l'article 1er ter de l'ordonnance n° 45-2590 du 2 novembre 1945 et relatif aux notaires salariés, alors applicable sur le projet de nomination de Mme B... en qualité de notaire salariée. Par un courrier du 29 mai 2015, la présidente du conseil régional des notaires de la cour d'appel de Dijon a estimé que cette lettre ne pouvait recevoir la qualification de document administratif au sens de l'article 1er de la loi du 17 juillet 1978 portant diverses mesures d'amélioration des relations entre l'administration et le public et diverses dispositions d'ordre administratif, social et fiscal, dans sa rédaction alors applicable, et en a refusé la communication. Par un avis n° 20152771 du 30 juillet 2015 notifié le 1er septembre, la Commission d'accès aux documents administratifs, saisie le 4 juin 2015 par Mme B..., a émis un avis favorable à la communication de la lettre litigieuse. Par un courrier daté du 17 septembre 2015, le conseil régional des notaires de la cour d'appel de Dijon a refusé une nouvelle fois la communication de ce document. Le tribunal administratif de Dijon a, par un jugement du 15 février 2019 contre lequel le conseil régional des notaires de la cour d'appel de Dijon se pourvoit en cassation, fait droit à la demande de Mme B... et annulé la décision attaquée. <br/>
<br/>
              2. En premier lieu aux termes de l'article 20 de la loi du 17 juillet 1978 portant diverses mesures d'amélioration des relations entre l'administration et le public et diverses dispositions d'ordre administratif, social et fiscal, applicable au litige et repris à l'article L. 342-1 du code des relations entre le public et l'administration : " La saisine pour avis de la commission est un préalable obligatoire à l'exercice d'un recours contentieux ". L'article 19 du décret du 30 décembre 2005 pris pour son application, et applicable au litige, repris aux articles R. 343-3 à R. 343-5 du même code dispose que : " La commission notifie son avis à l'intéressé et à l'autorité mise en cause, dans un délai d'un mois à compter de l'enregistrement de la demande au secrétariat. Cette autorité informe la commission, dans le délai d'un mois qui suit la réception de cet avis, de la suite qu'elle entend donner à la demande. Le silence gardé par l'autorité mise en cause pendant plus de deux mois à compter de l'enregistrement de la demande de l'intéressé par la commission vaut confirmation de la décision de refus ". Enfin, aux termes du premier alinéa de l'article R. 421-1 du code de justice administrative : " La juridiction ne peut être saisie que par voie de recours formé contre une décision, et ce, dans les deux mois à partir de la notification ou de la publication de la décision attaquée. ". <br/>
<br/>
              3. Il ressort des pièces du dossier soumis au juge du fond que Mme B... a attaqué la décision de refus de communication expressément prise par le conseil régional des notaires le 17 septembre 2015, après notification de l'avis de la Commission d'accès aux documents administratifs le 1er septembre 2015. Par suite, c'est sans erreur de droit, contrairement à ce que soutient le pourvoi, que le tribunal administratif s'est prononcé sur la recevabilité de la demande contre cette décision expresse de refus sans rechercher si cette demande aurait été tardive au regard d'une décision implicite qui serait intervenue postérieurement à la décision expresse attaquée, décision implicite qui n'a pu naître puisqu'était intervenue, au préalable, la décision expresse objet du litige.<br/>
<br/>
              4. En deuxième lieu, d'une part, l'article 1er de la loi du 17 juillet 1978 alors applicable et codifié à l'article L. 300-2 du code des relations entre le public et l'administration, dispose que : " Sont considérés comme documents administratifs, au sens des chapitres Ier, III et IV du présent titre, quels que soient leur date, leur lieu de conservation, leur forme et leur support, les documents produits ou reçus, dans le cadre de leur mission de service public, par l'État, les collectivités territoriales ainsi que par les autres personnes de droit public ou les personnes de droit privé chargées d'une telle mission ". D'autre part, aux termes de l'article 3 de l'ordonnance du 2 novembre 1945 relative au statut du notariat : " Les chambres des notaires, les conseils régionaux et le conseil supérieur sont des établissements d'utilité publique ". En vertu des articles 5 et 5-1 de cette ordonnance, le conseil régional des notaires " représente l'ensemble des notaires du ressort de la cour d'appel en ce qui touche à leurs droits et intérêts communs ". Aux termes de l'article 11 du décret du 15 janvier 1993 portant application de l'article 1er ter de l'ordonnance du 2 novembre 1945 précitée, dans sa rédaction applicable au litige : " Le procureur général recueille l'avis motivé du conseil régional des notaires, notamment sur la moralité, les capacités professionnelles du candidat et sur la conformité du contrat de travail avec les règles professionnelles. Si, quarante-cinq jours après sa saisine, par lettre recommandée avec demande d'avis de réception, le conseil régional n'a pas adressé au procureur général l'avis qui lui a été demandé, elle est réputée avoir émis un avis favorable ". Il en résulte que les documents détenus par les conseils régionaux des notaires, organismes de droit privé chargés d'une mission de service public, relevant de cette mission de service public constituent des documents administratifs au sens de l'article 1er de la loi du 17 juin 1978 précitée. Il en va ainsi des documents reçus au titre des avis qu'ils rendent, en application de de l'article 11 du décret du 15 janvier 1993, sur la nomination de personnes en qualité de notaires. <br/>
<br/>
              5. Il ressort des pièces du dossier soumis aux juges du fond que la lettre litigieuse a été adressée par un confrère de la requérante au conseil régional des notaires de la cour d'appel de Dijon dans le cadre de la procédure d'avis prévu par l'article 11 précité. Par suite, le tribunal administratif n'a pas commis d'erreur de droit en jugeant que cette lettre constituait un document administratif au sens de l'article 1er de la loi du 17 juillet 1978, alors même qu'il était soutenu devant lui qu'elle n'était pas nécessaire à l'avis rendu et qu'elle aurait pu ne pas être formalisée. La circonstance que la lettre litigieuse serait restée en la seule possession du conseil régional des notaires de la cour d'appel de Dijon sans transmission au procureur général près de la cour d'appel étant sans incidence sur cette qualification, le moyen tiré de ce que le tribunal aurait dénaturé les pièces du dossier sur ce point ne peut également qu'être écarté.<br/>
<br/>
              6. En dernier lieu, le moyen tiré de ce que la décision litigieuse de refus de communication de la lettre d'opposition était justifiée par le secret professionnel des notaires, qui constitue un secret protégé par la loi en application de 2° du I de l'article 6 de la loi du 17 juillet 1978, codifié à l'article L 311-5 du code des relations entre le public et l'administration, n'a pas été soutenu par le conseil régional des notaires de la cour d'appel de Dijon devant le tribunal administratif et ne présente pas un caractère d'ordre public. Il est, dès lors, nouveau en cassation et inopérant à ce titre. <br/>
<br/>
              7. Il résulte de tout ce qui précède que le pourvoi du conseil régional des notaires de la cour d'appel de Dijon doit être rejeté.<br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du conseil régional des notaires de la cour d'appel de Dijon une somme de 3 000 euros à verser à Mme B... au titre des dispositions de l'article L. 761-1 du code de justice administrative. En revanche, ces mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de Mme B..., qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du conseil régional des notaires de la cour d'appel de Dijon est rejeté.<br/>
Article 2 : Le conseil régional des notaires de la cour d'appel de Dijon versera à Mme B... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : La présente décision sera notifiée à Mme A... B... et au conseil régional des notaires de la cour d'appel de Dijon. <br/>
Copie en sera adressée au Conseil supérieur du notariat et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-06-01-02-01 DROITS CIVILS ET INDIVIDUELS. ACCÈS AUX DOCUMENTS ADMINISTRATIFS. ACCÈS AUX DOCUMENTS ADMINISTRATIFS AU TITRE DE LA LOI DU 17 JUILLET 1978. DROIT À LA COMMUNICATION. NOTION DE DOCUMENT ADMINISTRATIF. - DOCUMENTS DES CONSEILS RÉGIONAUX DES NOTAIRES RELEVANT DE LEUR MISSION DE SERVICE PUBLIC [RJ1] - INCLUSION - DOCUMENTS REÇUS AU TITRE DES AVIS QU'ILS RENDENT SUR LES NOMINATIONS DE NOTAIRES.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">55-015-01-02 PROFESSIONS, CHARGES ET OFFICES. INSTANCES D`ORGANISATION DES PROFESSIONS AUTRES QUE LES ORDRES. NOTAIRES. CONSEILS RÉGIONAUX. - COMMUNICABILITÉ DES DOCUMENTS DES CONSEILS RÉGIONAUX DES NOTAIRES RELEVANT DE LEUR MISSION DE SERVICE PUBLIC [RJ1] - INCLUSION - DOCUMENTS REÇUS AU TITRE DES AVIS QU'ILS RENDENT SUR LES NOMINATIONS DE NOTAIRES.
</SCT>
<ANA ID="9A"> 26-06-01-02-01 Il résulte de l'article 1er de la loi n° 78-753 du 17 juillet 1978, codifié à l'article L. 300-2 du code des relations entre le public et l'administration (CRPA), ainsi que des articles 3, 5 et 5-1 de l'ordonnance n° 45-2590 du 2 novembre 1945 et de l'article 11 du décret n° 93-82 du 15 janvier 1993 que les documents détenus par les conseils régionaux des notaires, organismes de droit privé chargés d'une mission de service public, relevant de cette mission de service public constituent des documents administratifs au sens de l'article 1er de la loi du 17 juin 1978. Il en va ainsi des documents reçus au titre des avis qu'ils rendent, en application de l'article 11 du décret du 15 janvier 1993, sur la nomination de personnes en qualité de notaires.</ANA>
<ANA ID="9B"> 55-015-01-02 Il résulte de l'article 1er de la loi n° 78-753 du 17 juillet 1978, codifié à l'article L. 300-2 du code des relations entre le public et l'administration (CRPA), ainsi que des articles 3, 5 et 5-1 de l'ordonnance n° 45-2590 du 2 novembre 1945 et de l'article 11 du décret n° 93-82 du 15 janvier 1993 que les documents détenus par les conseils régionaux des notaires, organismes de droit privé chargés d'une mission de service public, relevant de cette mission de service public constituent des documents administratifs au sens de l'article 1er de la loi du 17 juin 1978. Il en va ainsi des documents reçus au titre des avis qu'ils rendent, en application de l'article 11 du décret du 15 janvier 1993, sur la nomination de personnes en qualité de notaires.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur la communicabilité des documents d'une personne privée chargée d'une mission de service public, CE, 17 avril 2013, La Poste c/ M.,, n° 342372, T. pp. 601-602.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
