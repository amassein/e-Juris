<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000022057660</ID>
<ANCIEN_ID>JG_L_2010_04_000000319816</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/22/05/76/CETATEXT000022057660.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 02/04/2010, 319816</TITRE>
<DATE_DEC>2010-04-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>319816</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Vigouroux</PRESIDENT>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Constance  Rivière</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Lenica Frédéric</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2010:319816.20100402</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 13 août 2008 et 12 novembre 2008 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SOCIÉTÉ MEDIASERV, dont le siège est Tour Secid, place de la Rénovation à Pointe-à-Pitre (97110) ;  elle demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler la décision du 20 février 2008 par laquelle l'Autorité de régulation des communications et des postes (ARCEP) a rejeté sa demande tendant à ce qu'il soit imposé aux opérateurs 2G/3G des départements et collectivités d'outre-mer de fournir un service d'itinérance aux opérateurs 3G nouveaux entrants sur ces territoires, ainsi que la décision implicite par laquelle cette autorité a rejeté son recours gracieux contre cette décision ;<br/>
<br/>
              2°) d'enjoindre à l'ARCEP d'adopter, dans un délai de trois mois à compter de la notification de la décision à intervenir, une décision imposant aux opérateurs 2G/3G des départements et collectivités d'outre-mer de fournir un service d'itinérance aux opérateurs 3G nouveaux entrants sur ces territoires ;<br/>
<br/>
              3°) de mettre à la charge de l'ARCEP le versement de la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu la note en délibéré, enregistrée le 4 mars 2010, présentée pour la société MEDIASERV ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des postes et communications électroniques ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Constance Rivière, Auditeur, <br/>
<br/>
              - les observations de la SCP Piwnica, Molinié, avocat de la SOCIÉTÉ MEDIASERV, <br/>
<br/>
              - les conclusions de M. Frédéric Lenica, rapporteur public,<br/>
<br/>
              - la parole ayant été à nouveau donnée à la SCP Piwnica, Molinié, avocat de la SOCIÉTÉ MEDIASERV ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'aux termes du II de l'article L. 32-1 du code des postes et communications électroniques : " Dans le cadre de leurs attributions respectives, le ministre chargé des communications électroniques et l'Autorité de régulation des communications électroniques et des postes prennent, dans des conditions objectives et transparentes, des mesures raisonnables et proportionnées aux objectifs poursuivis et veillent : (...) 2° A l'exercice au bénéfice des utilisateurs d'une concurrence effective et loyale entre les exploitants de réseau et les fournisseurs de services de communications électroniques ; (...) 4° A la définition de conditions d'accès aux réseaux ouverts au public et d'interconnexion de ces réseaux qui garantissent la possibilité pour tous les utilisateurs de communiquer librement et l'égalité des conditions de la concurrence ; 9° A l'absence de discrimination, dans des circonstances analogues, dans le traitement des opérateurs ; 11° A l'utilisation et à la gestion efficaces des fréquences radioélectriques et des ressources de numérotation " ; qu'aux termes de l'article L. 36-6 du même code dans sa version applicable : " Dans le respect des dispositions du présent code et de ses règlements d'application, et, lorsque ces décisions ont un effet notable sur la diffusion de services de radio et de télévision, après avis du Conseil supérieur de l'audiovisuel, l'Autorité de régulation des communications électroniques et des postes précise les règles concernant : 1° Les droits et obligations afférents à l'exploitation des différentes catégories de réseaux et de services, en application de l'article L. 33-1 ; / 2° Les prescriptions applicables aux conditions techniques et financières d'interconnexion et d'accès, conformément à l'article L. 34-8 et aux conditions techniques et financières de l'itinérance locale, conformément à l'article L. 34-8-1 ; / 3° Les conditions d'utilisation des fréquences et bandes de fréquences mentionnées à l'article L. 42 ; 4° Les conditions d'établissement et d'exploitation des réseaux mentionnés à l'article L. 33-2 et celles d'utilisation des réseaux mentionnés à l'article L. 33-3 " ; qu'aux termes du 6° de l'article L. 36-7 du même code, l'Autorité de régulation des communications électroniques et des postes (ARCEP) " assigne aux opérateurs et aux utilisateurs les fréquences nécessaires à l'exercice de leur activité dans les conditions prévues à l'article L. 42-1 et veille à leur bonne utilisation " ; qu'aux termes de l'article L. 42-1 du même code : " I. L'Autorité de régulation des communications électroniques et des postes attribue les autorisations d'utilisation des fréquences radioélectriques dans des conditions objectives, transparentes et non discriminatoires tenant compte des besoins d'aménagement du territoire. Ces autorisations ne peuvent être refusées par l'Autorité de régulation des communications électroniques et des postes que pour l'un des motifs suivants : 1° La sauvegarde de l'ordre public, les besoins de la défense nationale ou de la sécurité publique ; 2° La bonne utilisation des fréquences ; 3° L'incapacité technique ou financière du demandeur à faire face durablement aux obligations résultant des conditions d'exercice de son activité ; 4° La condamnation du demandeur à l'une des sanctions mentionnées aux articles L. 36-11, L. 39, L. 39-1 et L. 39-4. / II. - L'autorisation précise les conditions d'utilisation de la fréquence ou de la bande de fréquences qui portent sur : 1° La nature et les caractéristiques techniques des équipements, réseaux et services qui peuvent utiliser la fréquence ou la bande de fréquences ainsi que leurs conditions de permanence, de qualité et de disponibilité et, le cas échéant, leur calendrier de déploiement et leur zone de couverture ; 2° La durée de l'autorisation, qui ne peut être supérieure à vingt ans, ainsi que le délai minimal dans lequel sont notifiés au titulaire les conditions de renouvellement de l'autorisation et les motifs d'un refus de renouvellement ; ce délai doit être proportionné à la durée de l'autorisation et prendre en compte le niveau d'investissement requis pour l'exploitation efficace de la fréquence ou de la bande de fréquences attribuée ; 3° Les redevances dues par le titulaire de l'autorisation, lorsque celles-ci n'ont pas été fixées par décret ; 4° Les conditions techniques nécessaires pour éviter les brouillages préjudiciables et pour limiter l'exposition du public aux champs électromagnétiques ; 5° Les obligations résultant d'accords internationaux ayant trait à l'utilisation des fréquences ; 6° Les engagements pris par le titulaire dans le cadre de l'appel à candidatures prévu à l'article L. 42-2. / Les délais d'octroi des autorisations et de notification des conditions de leur renouvellement, ainsi que les obligations qui s'imposent aux titulaires d'autorisation pour permettre le contrôle par l'Autorité de régulation des communications électroniques et des postes des conditions d'utilisation des fréquences sont fixés par décret " ; qu'aux termes de l'article L. 42-2 du même code dans sa version applicable : " Lorsque la bonne utilisation des fréquences l'exige, l'Autorité de régulation des communications électroniques et des postes peut, après consultation publique, limiter, dans une mesure permettant d'assurer des conditions de concurrence effective, le nombre d'autorisations de les utiliser. / Le ministre chargé des communications électroniques fixe, sur proposition de l'Autorité de régulation des communications électroniques et des postes, les conditions d'attribution et de modification des autorisations d'utilisation correspondant à ces fréquences ainsi que la durée de la procédure d'attribution, qui ne peut excéder un délai fixé par décret. / La sélection des titulaires de ces autorisations se fait par appel à candidatures sur des critères portant sur les conditions d'utilisation mentionnées à l'article L. 42-1 ou sur la contribution à la réalisation des objectifs mentionnés à l'article L. 32-1 / L'Autorité de régulation des communications électroniques et des postes conduit la procédure de sélection et assigne les fréquences correspondantes (...) " ;  <br/>
<br/>
              Considérant que, par une lettre du 22 janvier 2008, la SOCIÉTÉ MEDIASERV a demandé à l'ARCEP d'imposer aux opérateurs qui se verraient autoriser à exploiter un réseau 3G dans la bande 2100 MHz dans les départements d'outre-mer, et qui disposaient déjà d'une autorisation d'exploiter un réseau 2G dans la bande 900 MHz dans ces mêmes départements, de proposer une prestation d'itinérance aux autres opérateurs susceptibles de se porter candidats à l'exploitation d'une licence 3G dans la bande 2100 MHz dans ces mêmes départements ; que, par une lettre du 20 février 2008, l'ARCEP a refusé de faire droit à cette demande ; que l'ARCEP a, en outre, implicitement rejeté le recours gracieux formé le 14 avril 2008 par la SOCIÉTÉ MEDIASERV contre cette décision ;<br/>
<br/>
              Sur le moyen tiré de l'incompétence dont serait entachée la décision attaquée :<br/>
<br/>
              Considérant que si le refus opposé à cette demande par l'ARCEP a été notifié à la SOCIÉTÉ MEDIASERV par une lettre signée du président de l'Autorité en date du 20 février 2008, il n'en résulte pas que cette décision ait été prise par ce dernier en vertu de ses pouvoirs propres ; qu'il ressort des pièces du dossier que le collège de l'Autorité, qui a examiné les conditions dans lesquelles serait conduite la procédure d'attribution des licences 3G dans la bande 2100 MHz dans les départements d'outre-mer, a estimé que la situation de ce marché ne justifiait pas qu'une prestation d'itinérance soit imposée aux opérateurs exploitant un réseau 2G au bénéfice des nouveaux entrants de la bande 2100 MHz ; qu'ainsi, le moyen tiré de ce que la décision attaquée serait entachée d'incompétence doit être écarté ; <br/>
<br/>
              Sur la légalité interne de la décision attaquée :<br/>
<br/>
              Considérant que si l'ARCEP délivre les autorisations d'utilisation des fréquences hertziennes dans les conditions prévues par les articles L. 42-1 et L 42-2 du code des postes et communications électroniques, ces dispositions ne font pas obstacle à ce que l'Autorité use du pouvoir qu'elle tire des articles L. 36-6 et L. 36-7 du même code pour imposer à certains opérateurs disposant d'autorisations d'utilisation de fréquences dans les bandes 900 MHz ou 1800 MHz, exploitées en norme 2G, ainsi que dans la bande 2100 MHz, exploitée en norme 3G, de permettre aux nouveaux opérateurs qui ne disposent d'autorisation d'utilisation des fréquences que dans cette dernière bande d'utiliser les fréquences exploitées en norme 2G dans les bandes 900 Mhz et 1800 Mhz (prestation dite d'itinérance) ; qu'elle ne peut toutefois le faire que s'il ressort de l'analyse du fonctionnement du marché qu'une telle obligation est nécessaire pour assurer une concurrence efficace et loyale entre les différentes catégories d'opérateurs, et proportionnée à cet objectif ; <br/>
<br/>
              Considérant, d'une part, qu'il ressort des pièces du dossier qu'à la date de la décision attaquée, des fréquences restaient disponibles dans les départements d'outre-mer, dans les bandes 1800 MHz à la Réunion, en Martinique et en Guadeloupe, et dans les bandes 900 MHz en Guyane, susceptibles d'être utilisées pour compléter, le cas échéant, le déploiement d'un réseau 3G dans la bande 2100 MHz ; que si les propriétés de la bande 1800 MHz ne sont pas aussi favorables que celles de la bande 900 MHz pour développer la couverture de zones étendues, et si leurs performances de pénétration dans certains bâtiments sont inférieures, il ressort des pièces du dossier qu'eu égard à la densité de population élevée des trois départements dans lesquels seules des fréquences 1800 MHz demeuraient disponibles, et à leurs caractéristiques géographiques, cette différence ne faisait pas obstacle à ce que l'utilisation des seules fréquences 1800 MHz permît l'établissement d'une concurrence loyale et efficace ; que cette analyse a été confirmée par le développement de ce marché et l'attribution de licences dans la bande 1800 MHz à plusieurs opérateurs qui, à la date de la décision attaquée, avaient déjà manifesté leur intérêt pour l'octroi d'une licence 3G sans exiger que celle-ci fût assortie de la garantie de pouvoir bénéficier d'une prestation d'itinérance de la part des opérateurs en place ayant déjà développé un réseau en norme 2G ; <br/>
<br/>
              Considérant, d'autre part, que si, par sa décision du 14 décembre 2001 proposant au ministre chargé des télécommunications les modalités et les conditions d'attribution pour l'introduction en France métropolitaine des systèmes mobiles de troisième génération, l'ARCEP a prévu la possibilité que tout nouvel opérateur 3G puisse bénéficier d'un accord d'itinérance avec l'un des opérateurs 3G disposant également d'une autorisation d'exploiter un réseau en norme 2G dans les bandes 900 et 1800 MHz, lequel serait tenu d'engager des négociations en vue de la conclusion d'un tel accord, et si les décisions ultérieures de l'ARCEP ont précisé l'étendue et les modalités de l'obligation ainsi mise à la charge des opérateurs disposant d'une autorisation d'exploiter des réseaux en norme 2G et en norme 3G, cette obligation trouve sa justification dans la saturation des bandes de fréquences susceptibles d'être utilisées pour l'exploitation d'un réseau en norme 2G sur le territoire de la France métropolitaine, et par la nécessité d'offrir aux nouveaux opérateurs qui se verraient, le cas échéant, attribuer une licence pour l'exploitation d'un réseau de téléphonie mobile en norme 3G, la possibilité de bénéficier d'une telle prestation afin de concurrencer de façon effective les opérateurs qui exploitent conjointement un réseau en norme 2G et un réseau en norme 3G ; que les caractéristiques de ce marché, qui sont différentes de celles des départements d'outre-mer, justifient la différence de traitement des nouveaux opérateurs en norme 3G résultant du refus de l'ARCEP d'imposer, dans les départements d'outre-mer, des dispositions analogues à celles qui sont en vigueur en France métropolitaine ;<br/>
<br/>
              Considérant que, dans ces conditions, il n'était pas nécessaire, au regard de la situation du marché, d'imposer la prestation réclamée par la société requérante pour assurer une concurrence loyale et efficace ou atteindre les autres objectifs fixés par l'article L. 32-1 du code des postes et communications électroniques ; qu'il suit de là que l'ARCEP n'a pas méconnu l'étendue de sa compétence, ni commis d'erreur de droit, ni violé le principe d'égalité ou l'objectif  d'une concurrence loyale et effective en refusant de l'imposer ;<br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que la SOCIÉTÉ MEDIASERV n'est pas fondée à demander l'annulation des décisions qu'elle attaque ;<br/>
<br/>
              Sur les conclusions de la SOCIÉTÉ MEDIASERV tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant que ces dispositions font obstacle à ce que la somme demandée par la SOCIÉTÉ MEDIASERV au titre de ces dispositions soit mise à la charge de l'ARCEP qui n'est pas la partie perdante, dans la présente instance ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de la SOCIÉTÉ MEDIASERV est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la SOCIÉTÉ MEDIASERV et à l'Autorité de régulation des communications électroniques et des postes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">51-02-01 POSTES ET COMMUNICATIONS ÉLECTRONIQUES. COMMUNICATIONS ÉLECTRONIQUES. TÉLÉPHONE. - UTILISATION DES FRÉQUENCES HERTZIENNES EXPLOITÉES EN NORME 2G - POUVOIRS DE L'ARCEP POUR RÉGULER LES PRESTATIONS D'ITINÉRANCE (ART. L. 36-6 ET L. 36-7 DU CODE DES POSTES ET COMMUNICATIONS ÉLECTRONIQUES) - ETENDUE ET CONDITIONS.
</SCT>
<ANA ID="9A"> 51-02-01 Si l'Autorité de régulation des communications électroniques et des postes (ARCEP) délivre les autorisations d'utilisation des fréquences hertziennes dans les conditions prévues par les articles L. 42-1 et L. 42-2 du code des postes et communications électroniques, ces dispositions ne font pas obstacle à ce que l'Autorité use du pouvoir qu'elle tire des articles L. 36-6 et L. 36-7 du même code pour imposer à certains opérateurs disposant d'autorisations d'utilisation de fréquences dans les bandes 900 MHz ou 1800 MHz, exploitées en norme 2G, ainsi que dans la bande 2100 MHz, exploitée en norme 3G, de permettre aux nouveaux opérateurs qui ne disposent d'autorisation d'utilisation des fréquences que dans cette dernière bande d'utiliser les fréquences exploitées en norme 2G dans les bandes 900 Mhz et 1800 Mhz (prestation dite d'itinérance). L'Autorité ne peut toutefois le faire que s'il ressort de l'analyse du fonctionnement du marché qu'une telle obligation est nécessaire pour assurer une concurrence efficace et loyale entre les différentes catégories d'opérateurs, et proportionnée à cet objectif.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
