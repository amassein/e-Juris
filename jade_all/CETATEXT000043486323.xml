<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043486323</ID>
<ANCIEN_ID>JG_L_2021_05_000000421744</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/48/63/CETATEXT000043486323.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 06/05/2021, 421744</TITRE>
<DATE_DEC>2021-05-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421744</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI, REBEYROL ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:421744.20210506</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Procédure contentieuse antérieure <br/>
<br/>
              Mme B... A... a demandé au tribunal administratif de Paris de condamner la Ville de Paris à l'indemniser de préjudices qu'elle estime avoir subis à la suite de sa vaccination contre l'hépatite B. Par un jugement n° 1308695 du 4 décembre 2014, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 15PA00521 du 24 avril 2018, la cour administrative d'appel de Paris a, sur appel de Mme A..., annulé ce jugement, condamné la Ville de Paris à lui verser la somme de 153 000 euros et mis les frais d'expertise à la charge de la Ville de Paris.<br/>
<br/>
              Procédures devant le Conseil d'État<br/>
<br/>
              1° Sous le n° 421744, par un pourvoi, enregistré le 25 juin 2018 au secrétariat du contentieux du Conseil d'Etat, la caisse primaire d'assurance maladie (CPAM) de Paris demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il ne la met pas en mesure d'exercer le recours subrogatoire dont elle dispose au titre des frais de soins et de transport exposés pour Mme A... et, subsidiairement, d'annuler intégralement cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, d'annuler le jugement du 4 décembre 2014 du tribunal administratif de Paris et de condamner la Ville de Paris à lui verser, à titre provisionnel, la somme de 95 883,97 euros et l'indemnité forfaitaire de gestion, augmentées des intérêts ;  <br/>
<br/>
              3°) de mettre à la charge de la Ville de Paris la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              La CPAM de Paris soutient que l'arrêt qu'elle attaque est entaché d'irrégularité et d'erreur de droit en ce que la cour ne lui a transmis le rapport d'expertise qu'à la veille du prononcé de l'arrêt et a omis de lui adresser un avis d'audience.<br/>
<br/>
<br/>
              2° Sous le n° 425597, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 22 novembre et 20 décembre 2018 au secrétariat du contentieux du Conseil d'Etat, Mme A... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler le même arrêt en tant qu'il limite le montant de son indemnisation à 153 000 euros ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;  <br/>
<br/>
              3°) de mettre à la charge de la Ville de Paris la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Mme A... soutient que l'arrêt attaqué est entaché : <br/>
              - d'omission de statuer sur le préjudice ayant résulté pour elle de l'impossibilité de conserver une vie privée normale, y compris sur son préjudice sexuel ;  <br/>
              - de dénaturation des pièces du dossier et d'erreur de droit en ce qu'il rejette ses conclusions tendant à l'indemnisation d'un préjudice professionnel ; <br/>
              - de dénaturation des pièces du dossier dans l'évaluation de ses troubles dans les conditions d'existence. <br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la sécurité sociale ; <br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteure publique.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Gatineau, Fattaccini, Rebeyrol, avocat de la CPAM de Paris et de Mme A... et à la SCP Foussard, Froger, avocat de la Ville de Paris. <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme A..., agent spécialisé des écoles maternelles de la Ville de Paris, a reçu en 1996 et 1997, au titre de ses fonctions, une vaccination contre l'hépatite B. A la suite de l'apparition d'une grave affection rénale (glomérulonéphrite), elle a demandé au tribunal administratif de Paris de condamner la Ville de Paris à l'indemniser des préjudices qu'elle estimait liés à cette vaccination. La caisse primaire d'assurance maladie (CPAM) de Paris, appelée en cause par le tribunal administratif, a demandé que ses droits soient " réservés ", sans présenter de conclusions. Par un jugement du 4 décembre 2014, le tribunal administratif a rejeté la demande de Mme A.... Sur appel de celle-ci, la cour administrative d'appel de Paris, après avoir appelé en cause la CPAM de Paris, qui n'a pas présenté de conclusions, a, par un arrêt du 24 avril 2018, annulé le jugement du tribunal administratif et condamné la Ville de Paris à indemniser l'intéressée de divers préjudices ayant résulté pour elle de sa vaccination, ainsi qu'à payer les frais d'expertise.<br/>
<br/>
              2. Par deux pourvois qu'il y a lieu de joindre pour statuer par une seule décision, Mme A... demande l'annulation de cet arrêt en tant qu'il rejette le surplus de ses conclusions indemnitaires et la CPAM de Paris demande l'annulation du même arrêt au motif qu'il ne l'a pas mise à même d'obtenir le remboursement par la Ville de Paris des dépenses de soins et de transport exposées pour Mme A.... Dans chacune de ces deux affaires, la Ville de Paris conclut au rejet des pourvois et, par des conclusions incidentes, à l'annulation de l'arrêt en tant qu'il la condamne à indemniser Mme A... et à payer les frais d'expertise.<br/>
<br/>
              Sur les pourvois incidents de la Ville de Paris :<br/>
<br/>
              3. En premier lieu, il résulte des termes mêmes de son arrêt que la cour administrative d'appel a estimé, au vu notamment de l'expertise qu'elle avait prescrite, que le lien de causalité entre le vaccin contre l'hépatite B et l'affection rénale dont souffre Mme A... n'était pas exclu par les données acquises de la science, que l'intéressée, antérieurement en bonne santé, ne présentait pas d'antécédents la prédisposant à une affection rénale et que la maladie était apparue dans un bref délai après la troisième injection vaccinale. Sur tous ces points, la cour a porté sur les pièces du dossier qui lui était soumis une appréciation souveraine, qui n'est pas entachée de dénaturation.<br/>
<br/>
              4. En second lieu, en déduisant de ce qui précède qu'un lien direct de causalité devait être regardé comme établi entre la vaccination obligatoire subie par Mme A... et l'affection rénale dont elle était atteinte, la cour n'a pas commis d'erreur de droit. <br/>
<br/>
              5. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur la fin de non-recevoir que lui oppose Mme A..., que la Ville de Paris n'est pas fondée à demander l'annulation de l'arrêt attaqué en tant qu'il a retenu sa responsabilité et mis à sa charge la réparation des préjudices ayant résulté pour Mme A... de sa vaccination.<br/>
<br/>
              Sur le pourvoi principal de Mme A... :<br/>
<br/>
              6. En premier lieu, il ressort des pièces du dossier soumis à la cour administrative d'appel que Mme A... n'avait pas invoqué devant les juges du fond l'existence d'un préjudice d'établissement ou d'un préjudice sexuel. Elle n'est, par suite, pas fondée à soutenir que l'arrêt qu'elle attaque serait irrégulier, faute d'avoir statué sur ces chefs de préjudice. <br/>
<br/>
              7. En deuxième lieu, en estimant qu'à la date de son arrêt, les troubles subis par Mme A... dans ses conditions d'existence, résultant principalement de la répétition de séjours hospitaliers et de l'obligation de subir fréquemment des examens et des dialyses, devaient être évalués à 50 000 euros, la cour s'est livrée à une appréciation souveraine des pièces du dossier qui lui était soumis, exempte de dénaturation. <br/>
<br/>
              8. Enfin, la cour a pu, sans entacher son arrêt d'erreur de droit ni dénaturer les pièces du dossier qui lui était soumis, juger que Mme A... ne justifiait pas de l'existence d'un préjudice professionnel.<br/>
<br/>
              9. Il résulte de tout ce qui précède que Mme A... n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque en tant qu'il rejette le surplus de ses conclusions indemnitaires.<br/>
<br/>
              Sur le pourvoi principal de la caisse primaire d'assurance maladie de Paris : <br/>
<br/>
              10. Aux termes du huitième alinéa de l'article L. 376-1 du code de la sécurité sociale, relatif au recours subrogatoire des caisses de sécurité sociale contre le responsable d'un accident ayant entraîné un dommage corporel : " L'intéressé ou ses ayants droit doivent indiquer, en tout état de la procédure, la qualité d'assuré social de la victime de l'accident ainsi que les caisses de sécurité sociale auxquelles celle-ci est ou était affiliée pour les divers risques. Ils doivent appeler ces caisses en déclaration de jugement commun ou réciproquement (...) ". Il appartient au juge administratif d'assurer, en tout état de la procédure, le respect de ces dispositions. Ainsi, le tribunal administratif, saisi par la victime d'une demande tendant à la réparation du dommage corporel par l'auteur de l'accident doit appeler en cause la caisse à laquelle la victime est affiliée et la cour administrative d'appel, saisie dans le délai légal d'un appel de la victime, doit également appeler en cause cette même caisse, la méconnaissance de ces obligations entachant le jugement ou l'arrêt d'une irrégularité que le juge d'appel ou le juge de cassation doit, au besoin, relever d'office. Toutefois, lorsqu'un jugement ayant statué sur des conclusions indemnitaires de la victime fait l'objet d'un appel de cette dernière, la caisse appelée en cause par la cour administrative d'appel ne peut régulièrement présenter devant le juge d'appel d'autres conclusions que celles de sa demande de première instance, en y ajoutant seulement, le cas échéant, celles tendant au remboursement des prestations servies à la victime postérieurement à l'intervention du jugement. Il n'en va différemment que si le tribunal a, à tort, omis de mettre la caisse en cause devant lui, auquel cas celle-ci peut obtenir, le cas échéant d'office, l'annulation du jugement en tant qu'il statue sur les préjudices au titre desquels elle a exposé des débours et présenter ainsi, pour la première fois devant le juge d'appel, des conclusions tendant au paiement de l'ensemble de ces sommes.<br/>
<br/>
              11. Par suite, lorsqu'une caisse, pourtant régulièrement appelée en cause en première instance, n'a pas présenté de conclusions devant le tribunal administratif, cette circonstance est sans incidence sur l'obligation qui incombe à la cour administrative d'appel, saisie d'un appel contre ce jugement, de la mettre en cause. Elle fait en revanche obstacle à ce que la caisse présente devant la cour administrative d'appel des conclusions tendant au remboursement de sommes exposées par elle antérieurement au jugement de première instance.<br/>
<br/>
              12. Il ressort des pièces du dossier soumis à la cour administrative d'appel que si celle-ci a, conformément aux principes rappelés ci-dessus, invité la caisse primaire d'assurance maladie de Paris à présenter ses conclusions en qualité de partie appelée à l'instance introduite par l'appel de Mme A..., elle a omis de la convoquer à l'audience du 10 avril 2018 au cours de laquelle elle a examiné cet appel et ne lui a, en outre, communiqué certaines pièces du dossier, dont le rapport d'expertise, que postérieurement à l'audience.<br/>
<br/>
              13. La caisse primaire d'assurance maladie de Paris est, par suite, fondée à soutenir que la cour administrative d'appel de Paris ne l'a pas régulièrement appelée en cause en appel et qu'elle a donc, sur ce point, entaché son arrêt d'irrégularité.<br/>
<br/>
              14. Toutefois, il ressort des pièces du dossier soumis aux juges du fond que, bien qu'appelée en cause devant le tribunal administratif de Paris, la caisse primaire n'a, ainsi qu'il est dit au point 1, pas présenté de conclusions subrogatoires en première instance. Elle n'était, par suite, susceptible de se voir régulièrement octroyer en appel que le remboursement des seules prestations nouvelles servies, le cas échéant, à Mme A... postérieurement à l'intervention du jugement du tribunal administratif. Elle n'est donc pas fondée à demander l'annulation de l'arrêt attaqué en tant que, faute qu'elle ait été appelée en cause, il omet de statuer sur le remboursement des frais qu'elle a exposés avant le 4 décembre 2014.<br/>
<br/>
              15. En outre, l'arrêt attaqué ne prononçant pas de condamnation de la Ville de Paris à indemniser Mme A... au titre de ses frais de soins ou de transport, pour lesquels cette dernière ne demandait d'ailleurs aucune réparation, la caisse primaire d'assurance maladie, qui demande l'annulation de l'arrêt attaqué " en tant qu'il ne l'a pas mise en mesure d'exercer le recours subrogatoire dont elle disposait pour obtenir le remboursement des dépenses exposées au titre des frais de soins et de transport ", n'est pas fondée à demander son annulation en tant qu'il statue sur l'indemnisation de Mme A... et les frais d'expertise qui s'y rattachent.<br/>
<br/>
              16. Il résulte de ce qui est dit aux point 13 à 15 que la caisse primaire d'assurance maladie de Paris n'est fondée à demander l'annulation de l'arrêt attaqué qu'en tant que, faute de l'avoir mise en cause, il omet de statuer sur le remboursement des frais qu'elle a exposés au titre des dépenses de soins et de transport de Mme A... à compter du 4 décembre 2014.<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              17. Au titre de ces dispositions il n'y a lieu, dans l'instance introduite par le pourvoi de Mme A..., ni de mettre à la charge de la Ville de Paris les sommes que demande à ce titre Mme A... ni de mettre à la charge de cette dernière les sommes que demande, au même titre, la Ville de Paris.<br/>
<br/>
              18. Dans l'instance introduite par le pourvoi de la caisse primaire d'assurance maladie de Paris, il y a lieu, au titre de ces mêmes dispositions et dans les circonstances de l'espèce, de mettre à la charge de la Ville de Paris une somme de 3 000 euros à verser à la caisse primaire d'assurance maladie de Paris. Il y a également lieu de mettre à la charge de la Ville de Paris une somme de 3 000 euros à verser, au même titre, à Mme A..., laquelle a la qualité de partie en défense à l'égard des conclusions incidentes de la Ville de Paris. Les dispositions de l'article L. 761-1 du code de justice administrative font en revanche obstacle à ce que soit mise à la charge de la caisse primaire d'assurance maladie de Paris, qui n'est pas la partie perdante, la somme que demande, à ce titre, la Ville de Paris. <br/>
<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
               				--------------<br/>
<br/>
Article 1er : L'arrêt du 24 avril 2018 de la cour administrative d'appel de Paris est annulé en tant qu'il omet de statuer sur le préjudice né, pour la caisse primaire d'assurance maladie de Paris, des dépenses de soins et de transport exposées pour Mme A... à compter du 4 décembre 2014.<br/>
<br/>
		Article 2 : Le pourvoi de Mme A... est rejeté.<br/>
<br/>
		Article 3 : Les pourvois incidents de la Ville de Paris sont rejetés.<br/>
<br/>
Article 4 : L'affaire est renvoyée à la cour administrative d'appel de Paris dans la limite de la cassation prononcée.<br/>
<br/>
Article 5 : La Ville de Paris versera la somme de 3 000 euros à la caisse primaire d'assurance maladie de Paris et de 3 000 euros à Mme A... au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
		Article 6 : Le surplus des conclusions des parties est rejeté.<br/>
<br/>
Article 7 : La présente décision sera notifiée à la caisse primaire d'assurance maladie de Paris, à la Ville de Paris et à Mme B... A....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-08-01 PROCÉDURE. VOIES DE RECOURS. APPEL. - RECOURS SUBROGATOIRE DES CAISSES DE SÉCURITÉ SOCIALE (ART. L. 376-1 DU CSS) - OBLIGATION POUR LE JUGE DU FOND D'APPELER EN CAUSE LA CAISSE À LAQUELLE LA VICTIME D'UN DOMMAGE CORPOREL EST AFFILIÉE AFIN QU'ELLE PUISSE EXERCER CE RECOURS [RJ1] - APPEL FORMÉ PAR LA VICTIME - 1) CAISSE APPELÉE EN LA CAUSE PAR LA CAA - CONCLUSIONS RECEVABLES - A) CAS GÉNÉRAL [RJ2] - B) CAS OÙ LE TA A OMIS D'APPELER LA CAISSE EN LA CAUSE - 2) CONSÉQUENCE - CAS OÙ LE TA A APPELÉ LA CAISSE EN LA CAUSE, QUI N'A PAS PRODUIT - A) OBLIGATION POUR LA CAA DE L'APPELER EN LA CAUSE - EXISTENCE - B) FACULTÉ, POUR LA CAISSE, DE DEMANDER LE REMBOURSEMENT DE PRESTATIONS SERVIES ANTÉRIEUREMENT AU JUGEMENT DU TA - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-05-04-01-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RECOURS OUVERTS AUX DÉBITEURS DE L'INDEMNITÉ, AUX ASSUREURS DE LA VICTIME ET AUX CAISSES DE SÉCURITÉ SOCIALE. DROITS DES CAISSES DE SÉCURITÉ SOCIALE. IMPUTATION DES DROITS À REMBOURSEMENT DE LA CAISSE. ARTICLE L. 376-1 (ANCIEN ART. L. 397) DU CODE DE LA SÉCURITÉ SOCIALE. - OBLIGATION POUR LE JUGE DU FOND D'APPELER EN CAUSE LA CAISSE DE SÉCURITÉ SOCIALE À LAQUELLE LA VICTIME D'UN DOMMAGE CORPOREL EST AFFILIÉE AFIN QU'ELLE PUISSE EXERCER SON RECOURS SUBROGATOIRE [RJ1] - APPEL FORMÉ PAR LA VICTIME - 1) CAISSE APPELÉE EN LA CAUSE PAR LA CAA - CONCLUSIONS RECEVABLES - A) CAS GÉNÉRAL [RJ2] - B) CAS OÙ LE TA A OMIS D'APPELER LA CAISSE EN LA CAUSE - 3) CONSÉQUENCE - CAS OÙ LE TA A APPELÉ EN LA CAUSE LA CAISSE , QUI N'A PAS PRODUIT - A) OBLIGATION POUR LA CAA DE L'APPELER EN LA CAUSE - EXISTENCE - B) FACULTÉ, POUR LA CAISSE, DE DEMANDER LE REMBOURSEMENT DE PRESTATIONS SERVIES ANTÉRIEUREMENT AU JUGEMENT DU TA - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">62-05 SÉCURITÉ SOCIALE. CONTENTIEUX ET RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. - RECOURS SUBROGATOIRE DES CAISSES DE SÉCURITÉ SOCIALE (ART. L. 376-1 DU CSS) - OBLIGATION POUR LE JUGE DU FOND D'APPELER EN CAUSE LA CAISSE À LAQUELLE LA VICTIME D'UN DOMMAGE CORPOREL EST AFFILIÉE AFIN QU'ELLE PUISSE EXERCER CE RECOURS [RJ1] - APPEL FORMÉ PAR LA VICTIME - 1) CAISSE APPELÉE EN LA CAUSE PAR LA CAA - CONCLUSIONS RECEVABLES - A) CAS GÉNÉRAL [RJ2] - B) CAS OÙ LE TA A OMIS D'APPELER LA CAISSE EN LA CAUSE - 2) CONSÉQUENCE - CAS OÙ LE TA A APPELÉ LA CAISSE EN LA CAUSE, QUI N'A PAS PRODUIT - A) OBLIGATION POUR LA CAA DE L'APPELER EN LA CAUSE - EXISTENCE - B) FACULTÉ, POUR LA CAISSE, DE DEMANDER LE REMBOURSEMENT DE PRESTATIONS SERVIES ANTÉRIEUREMENT AU JUGEMENT DU TA - ABSENCE.
</SCT>
<ANA ID="9A"> 54-08-01 Il appartient au juge administratif d'assurer, en tout état de la procédure, le respect du huitième alinéa de l'article L. 376-1 du code de la sécurité sociale (CSS), relatif au recours subrogatoire des caisses de sécurité sociale contre le responsable d'un accident ayant entraîné un dommage corporel. Ainsi, le tribunal administratif (TA), saisi par la victime d'une demande tendant à la réparation du dommage corporel par l'auteur de l'accident doit appeler en cause la caisse à laquelle la victime est affiliée et la cour administrative d'appel (CAA), saisie dans le délai légal d'un appel de la victime, doit également appeler en cause cette même caisse.  La méconnaissance de ces obligations entache le jugement ou l'arrêt d'une irrégularité que le juge d'appel ou le juge de cassation doit, au besoin, relever d'office.... ,,1) a) Lorsqu'un jugement ayant statué sur des conclusions indemnitaires de la victime fait l'objet d'un appel de cette dernière, la caisse appelée en cause par la CAA ne peut régulièrement présenter devant le juge d'appel d'autres conclusions que celles de sa demande de première instance, en y ajoutant seulement, le cas échéant, celles tendant au remboursement des prestations servies à la victime postérieurement à l'intervention du jugement.... ,,b) Il n'en va différemment que si le TA a, à tort, omis de mettre la caisse en cause devant lui, auquel cas celle-ci peut obtenir, le cas échéant d'office, l'annulation du jugement en tant qu'il statue sur les préjudices au titre desquels elle a exposé des débours et présenter ainsi, pour la première fois devant le juge d'appel, des conclusions tendant au paiement de l'ensemble de ces sommes.,,,2) a) Par suite, lorsqu'une caisse, pourtant régulièrement appelée en cause en première instance, n'a pas présenté de conclusions devant le TA, cette circonstance est sans incidence sur l'obligation qui incombe à la CAA, saisie d'un appel contre ce jugement, de la mettre en cause.... ,,b) Elle fait en revanche obstacle à ce que la caisse présente devant la CAA des conclusions tendant au remboursement de sommes exposées par elle antérieurement au jugement de première instance.</ANA>
<ANA ID="9B"> 60-05-04-01-01 Il appartient au juge administratif d'assurer, en tout état de la procédure, le respect du huitième alinéa de l'article L. 376-1 du code de la sécurité sociale (CSS), relatif au recours subrogatoire des caisses de sécurité sociale contre le responsable d'un accident ayant entraîné un dommage corporel. Ainsi, le tribunal administratif (TA), saisi par la victime d'une demande tendant à la réparation du dommage corporel par l'auteur de l'accident doit appeler en cause la caisse à laquelle la victime est affiliée et la cour administrative d'appel (CAA), saisie dans le délai légal d'un appel de la victime, doit également appeler en cause cette même caisse.  La méconnaissance de ces obligations entache le jugement ou l'arrêt d'une irrégularité que le juge d'appel ou le juge de cassation doit, au besoin, relever d'office.... ,,1) a) Lorsqu'un jugement ayant statué sur des conclusions indemnitaires de la victime fait l'objet d'un appel de cette dernière, la caisse appelée en cause par la CAA ne peut régulièrement présenter devant le juge d'appel d'autres conclusions que celles de sa demande de première instance, en y ajoutant seulement, le cas échéant, celles tendant au remboursement des prestations servies à la victime postérieurement à l'intervention du jugement.... ,,b) Il n'en va différemment que si le TA a, à tort, omis de mettre la caisse en cause devant lui, auquel cas celle-ci peut obtenir, le cas échéant d'office, l'annulation du jugement en tant qu'il statue sur les préjudices au titre desquels elle a exposé des débours et présenter ainsi, pour la première fois devant le juge d'appel, des conclusions tendant au paiement de l'ensemble de ces sommes.,,,2) a) Par suite, lorsqu'une caisse, pourtant régulièrement appelée en cause en première instance, n'a pas présenté de conclusions devant le TA, cette circonstance est sans incidence sur l'obligation qui incombe à la CAA, saisie d'un appel contre ce jugement, de la mettre en cause.... ,,b) Elle fait en revanche obstacle à ce que la caisse présente devant la CAA des conclusions tendant au remboursement de sommes exposées par elle antérieurement au jugement de première instance.</ANA>
<ANA ID="9C"> 62-05 Il appartient au juge administratif d'assurer, en tout état de la procédure, le respect du huitième alinéa de l'article L. 376-1 du code de la sécurité sociale (CSS), relatif au recours subrogatoire des caisses de sécurité sociale contre le responsable d'un accident ayant entraîné un dommage corporel. Ainsi, le tribunal administratif (TA), saisi par la victime d'une demande tendant à la réparation du dommage corporel par l'auteur de l'accident doit appeler en cause la caisse à laquelle la victime est affiliée et la cour administrative d'appel (CAA), saisie dans le délai légal d'un appel de la victime, doit également appeler en cause cette même caisse.  La méconnaissance de ces obligations entache le jugement ou l'arrêt d'une irrégularité que le juge d'appel ou le juge de cassation doit, au besoin, relever d'office.... ,,1) a) Lorsqu'un jugement ayant statué sur des conclusions indemnitaires de la victime fait l'objet d'un appel de cette dernière, la caisse appelée en cause par la CAA ne peut régulièrement présenter devant le juge d'appel d'autres conclusions que celles de sa demande de première instance, en y ajoutant seulement, le cas échéant, celles tendant au remboursement des prestations servies à la victime postérieurement à l'intervention du jugement.... ,,b) Il n'en va différemment que si le TA a, à tort, omis de mettre la caisse en cause devant lui, auquel cas celle-ci peut obtenir, le cas échéant d'office, l'annulation du jugement en tant qu'il statue sur les préjudices au titre desquels elle a exposé des débours et présenter ainsi, pour la première fois devant le juge d'appel, des conclusions tendant au paiement de l'ensemble de ces sommes.,,,2) a) Par suite, lorsqu'une caisse, pourtant régulièrement appelée en cause en première instance, n'a pas présenté de conclusions devant le TA, cette circonstance est sans incidence sur l'obligation qui incombe à la CAA, saisie d'un appel contre ce jugement, de la mettre en cause.... ,,b) Elle fait en revanche obstacle à ce que la caisse présente devant la CAA des conclusions tendant au remboursement de sommes exposées par elle antérieurement au jugement de première instance.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>1) Cf. CE, Section, 11 octobre 1963, Commune de Seichamps, n° 50633, p. 482 ; CE, Section, 16 décembre 1966, Ministre de la santé publique et de la population c/,, n°s 61614 64800, p. 668 ; CE, 27 novembre 2015, Centre hospitalier de Troyes, n° 374025, T. pp. 810-828-838-883.,,[RJ2] Cf. CE, Section, 1er juillet 2005, S., n° 234403, p. 300 ; CE, 15 novembre 2006, Assistance publique-Hôpitaux de Marseille, n° 279273, T. pp. 1040-1069-1079.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
