<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041705707</ID>
<ANCIEN_ID>JG_L_2020_02_000000436392</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/70/57/CETATEXT000041705707.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 24/02/2020, 436392, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-02-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>436392</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Laurent Domingo</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:436392.20200224</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 2 décembre 2019 au secrétariat du contentieux du Conseil d'État, M. B... A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le paragraphe 130 des commentaires administratifs publiés au Bulletin officiel des finances publiques - impôts le 12 septembre 2012 sous la référence BOI-INT-CVB-BEL-10-10 ;<br/>
<br/>
              2°) de mettre à la charge de l'État la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention signée le 10 mars 1964 entre la France et la Belgique, tendant à éviter les doubles impositions et à établir les règles d'assistance administrative et juridique réciproque en matière d'impôt sur le revenu ;<br/>
              - le code général des impôts ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Domingo, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article 3 de la convention signée le 10 mars 1964 entre la Franc et la Belgique, tendant à éviter les doubles impositions et à établir des règles d'assistance administrative et juridique réciproque en matière d'impôt sur les revenus : " 1. Les revenus provenant des biens immobiliers, y compris les accessoires, ainsi que le cheptel mort ou vif des entreprises agricoles et forestières ne sont imposables que dans l'Etat contractant où ces biens sont situés. / 2. La notion de bien immobilier se détermine d'après les lois de l'Etat contractant où est situé le bien considéré. (...) / 4. Les dispositions des paragraphes 1 à 3 (...) s'appliquent également aux bénéfices résultant de l'aliénation de biens immobiliers ". Aux termes du paragraphe 1 de l'article 15 de cette convention : " Les dividendes ayant leur source dans un Etat contractant qui sont payés à un résident de l'autre Etat contractant sont imposables dans cet autre Etat ". Aux termes de l'article 22 de cette convention : " Tout terme non spécialement défini dans la présente Convention aura, à moins que le contexte n'exige une autre interprétation, la signification que lui attribue la législation régissant, dans chaque Etat contractant, les impôts faisant l'objet de la Convention ".<br/>
<br/>
              2. Le paragraphe 2 du protocole final de cette convention stipule : " L'article 15, paragraphe 1, ne s'oppose pas à ce que la France, conformément aux dispositions de sa loi interne, considère comme des biens immobiliers, au sens de l'article 3 de la Convention, les droits sociaux possédés par les associés ou actionnaires des sociétés qui ont, en fait, pour unique objet, soit la construction ou l'acquisition d'immeubles ou de groupes d'immeubles en vue de leur division par fractions destinées à être attribuées à leurs membres en propriété ou en jouissance, soit la gestion de ces immeubles ou groupes d'immeubles ainsi divisés. La Belgique pourra toutefois imposer, dans les limites fixées aux articles 15, paragraphes 1 et 2, et 19-A, paragraphe 1, les revenus tirés par des résidents de la Belgique de droits sociaux représentés par des actions ou parts dans lesdites sociétés résidentes de la France ".<br/>
<br/>
              3. M. A... demande l'annulation pour excès de pouvoir du paragraphe 130 des commentaires administratifs publiés au Bulletin officiel des finances publiques - impôts le 12 septembre 2012 sous la référence BOI-INT-CVB-BEL-10-10, par lequel l'administration a fait connaître son interprétation des stipulations précitées de l'article 3 de la convention du 10 mars 1964. <br/>
<br/>
              4. Selon le paragraphe 130 des commentaires attaqués : " Le paragraphe 2 du protocole susvisé n'ayant pas un caractère limitatif, il convient de considérer que le même caractère doit être reconnu aux droits détenus dans des sociétés dont l'actif est constitué principalement par des terrains à bâtir ou des biens assimilés, ainsi qu'aux droits détenus dans des sociétés civiles immobilières de toute nature non régies par l'article 1655 ter du code général des impôts et dont le patrimoine est composé essentiellement par des immeubles autres que des terrains à usage agricole ou forestier ".<br/>
<br/>
              5. Eu égard à l'intérêt dont se prévaut M. A..., lequel indique avoir été assujetti à une imposition supplémentaire à raison de la plus-value de cession de parts d'une société civile immobilière qui détenait des biens immobiliers situés en France, ses conclusions ne sont recevables qu'en tant qu'elles sont dirigées contre l'interprétation que donne le paragraphe 130 des commentaires attaqués de la dernière phrase du paragraphe 4 de l'article 3 de la convention. En outre, compte tenu de son argumentation, ses conclusions doivent être regardées comme ne portant que sur les mots " ainsi qu'aux droits détenus dans des sociétés civiles immobilières de toute nature non régies par l'article 1655 ter du CGI et dont le patrimoine est composé essentiellement par des immeubles autres que des terrains à usage agricole ou forestier ".<br/>
<br/>
              6. Pour déterminer la notion de bien immobilier au sens et pour l'application de la dernière phrase du paragraphe 4 de l'article 3 de la convention du 10 mars 1964, il convient, conformément aux stipulations de cet article, de se référer aux lois de l'Etat contractant où est situé le bien considéré et, ainsi qu'il est prévu à l'article 22, de retenir, à moins que le contexte n'exige une autre interprétation, la signification que lui attribue la législation régissant, dans chaque Etat contractant, les impôts faisant l'objet de la convention. Sont dépourvus d'incidence à cet égard, les stipulations du paragraphe 2 du protocole final de cette convention, qui ont pour unique objet de qualifier de biens immobiliers, au sens de la convention, les parts de sociétés relevant de l'article 1655 ter du code général des impôts.<br/>
<br/>
              7. L'article 244 bis A du code général des impôts, applicable aux plus-values immobilières réalisées par les personnes physiques qui ne sont pas fiscalement domiciliées en France au sens de l'article 4 B, soumet à ce régime les plus-values que ces personnes réalisent lors de la cession de parts qu'elles détiennent dans les sociétés ou organismes, quelle qu'en soit la forme, dont l'actif est principalement constitué, directement ou indirectement, de biens ou droits immobiliers. La loi fiscale assimile ainsi à des biens immobiliers, notamment, les parts des sociétés civiles à prépondérance immobilière, lors de leur aliénation par une personne qui n'est pas fiscalement domiciliée en France. Dès lors, le paragraphe 130 des commentaires en litige n'a pas retenu une inexacte interprétation des stipulations de la dernière phrase du paragraphe 4 de l'article 3 de la convention du 10 mars 1964.<br/>
<br/>
              8. Il résulte de ce qui précède que M. A... n'est pas fondé à demander l'annulation des mots cités au point 5 ci-dessus du paragraphe 130 des commentaires administratifs attaqués. Ses conclusions doivent, par suite, être rejetées, y compris celles présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. A... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. B... A... et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
