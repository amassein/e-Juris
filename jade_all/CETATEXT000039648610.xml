<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039648610</ID>
<ANCIEN_ID>JG_L_2019_12_000000418400</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/64/86/CETATEXT000039648610.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 19/12/2019, 418400, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-12-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418400</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ZRIBI, TEXIER ; CORLAY</AVOCATS>
<RAPPORTEUR>M. Florian Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2019:418400.20191219</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme B... A... a demandé au tribunal administratif de Nantes de condamner le centre hospitalier départemental de Vendée à lui verser au titre de ses permanences une indemnité de 5 100,48 euros et d'enjoindre à cet établissement de payer les heures supplémentaires effectuées postérieurement à l'enregistrement de la requête selon les modalités prévues par le décret n° 2002-9 du 4 janvier 2002. Par un jugement n° 1601482 du 20 décembre 2017, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 20 février et 22 mai 2018 au secrétariat du contentieux du Conseil d'Etat, Mme A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande de première instance ;<br/>
<br/>
              3°) de mettre à la charge du centre hospitalier départemental de Vendée la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le décret n° 2002-9 du 4 janvier 2002 relatif au temps de travail et à l'organisation du travail dans les établissements mentionnés à l'article 2 de la loi n° 86-33 du 9 janvier 1986 portant dispositions statutaires relatives à la fonction publique hospitalière ;<br/>
              - le décret n°2002-598 du 25 avril 2002 relatif aux indemnités horaires pour travaux supplémentaires ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Zribi et Texier, avocat de Mme A... et à Me Corlay, avocat du centre hospitalier départemental de Vendée.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme A..., infirmière anesthésiste en service au centre hospitalier départemental de la Vendée, a demandé au tribunal administratif de Nantes de condamner ce centre hospitalier à lui verser une indemnité de 5 100,48 euros en réparation du préjudice qu'elle estime avoir subi dans le paiement de ses heures de gardes effectuées pendant des permanences de vingt-quatre heures. Par un jugement du 20 décembre 2017, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              2. En premier lieu, il ressort des pièces du dossier soumis aux juges du fond que Mme A... demandait l'indemnisation du préjudice résultant de ce que les heures supplémentaires qu'elle avait effectuées la nuit n'avaient pas bénéficié du taux de majoration de 100 % prévu par l'article 7 du décret du 25 avril 2002 relatif aux indemnités horaires pour travaux supplémentaires. En s'abstenant de statuer sur ce chef de préjudice, le tribunal a entaché son jugement d'irrégularité.<br/>
<br/>
              3. En second lieu, l'article 5 du décret du 4 janvier 2002 relatif au temps de travail et à l'organisation du travail dans les établissements mentionnés à l'article 2 de la loi n° 86-33 du 9 janvier 1986 portant dispositions statutaires relatives à la fonction publique hospitalière définit la " durée du travail effectif " comme étant  : " (...) le temps pendant lequel les agents sont à la disposition de leur employeur et doivent se conformer à ses directives sans pouvoir vaquer librement à des occupations personnelles. (...) ". L'article 20 du même décret définit par ailleurs la " période d'astreinte " comme étant : " (...) une période pendant laquelle l'agent, qui n'est pas sur son lieu de travail et sans être à la disposition permanente et immédiate de son employeur, a l'obligation d'être en mesure d'intervenir pour effectuer un travail au service de l'établissement. La durée de chaque intervention, temps de trajet inclus, est considérée comme temps de travail effectif. (...) ". Enfin, l'article 24 de ce décret dispose que : " Les agents assurant leur service d'astreinte doivent pouvoir être joints par tous moyens appropriés, à la charge de l'établissement, pendant toute la durée de cette astreinte. Ils doivent pouvoir intervenir dans un délai qui ne peut être supérieur à celui qui leur est habituellement nécessaire pour se rendre sur le lieu d'intervention. (...) " et son article 25 dispose que : " Le temps passé en astreinte donne lieu soit à compensation horaire, soit à indemnisation. / (...) ". La rémunération des agents en fonction dans les établissements publics de santé distingue ainsi notamment les périodes de travail effectif, durant lesquelles les agents sont à la disposition de leur employeur et doivent se conformer à ses directives sans pouvoir vaquer librement à des occupations personnelles, et les périodes d'astreinte, durant lesquelles les agents ont seulement l'obligation d'être en mesure d'intervenir pour effectuer un travail au service de l'établissement. S'agissant de ces périodes d'astreinte, la seule circonstance que l'employeur mette à la disposition des agents un logement situé à proximité ou dans l'enceinte du lieu de travail pour leur permettre de rejoindre le service dans les délais requis n'implique pas que le temps durant lequel un agent bénéficie de cette convenance soit requalifié en temps de travail effectif, dès lors que cet agent n'est pas tenu de rester à la disposition permanente et immédiate de son employeur et qu'il peut ainsi, en dehors des temps d'intervention, vaquer librement à des occupations personnelles.<br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond que, pour les infirmiers du centre hospitalier départemental de la Vendée, la mise à disposition d'un logement situé dans l'enceinte de l'hôpital pour effectuer leur garde est assortie de la remise d'un récepteur téléphonique par lequel ils doivent pouvoir être contactés pendant toute la durée de cette garde et que ce récepteur ne peut fonctionner qu'à proximité d'un émetteur situé dans l'établissement, les obligeant ainsi à demeurer à disposition immédiate de leur employeur. Par suite, le tribunal administratif a inexactement qualifié les faits qui lui étaient soumis en jugeant que ces agents pouvaient, pendant leurs périodes de garde, librement vaquer à leurs occupations personnelles sans être à la disposition permanente et immédiate de leur employeur et en en déduisant que ces périodes ne constituaient pas un temps de travail effectif.<br/>
<br/>
              5. Il résulte de tout ce qui précède que Mme A... est fondée à demander l'annulation du jugement qu'elle attaque.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du centre hospitalier départemental de Vendée la somme de 500 euros à verser à Mme A... au titre de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font obstacle à ce qu'une somme soit mise, au même titre, à la charge de Mme A... qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
              D E C I D E:<br/>
              --------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Nantes du 20 décembre 2017 est annulé.<br/>
            Article 2 : L'affaire est renvoyée au tribunal administratif de Nantes.<br/>
<br/>
Article 3 : Le centre hospitalier départemental de Vendée versera à Mme A... la somme de 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par le centre hospitalier départemental de Vendée au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à Mme B... A... et au centre hospitalier départemental de Vendée.<br/>
            Copie en sera adressée à la ministre des solidarités et de la santé.<br/>
<br/>
.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
