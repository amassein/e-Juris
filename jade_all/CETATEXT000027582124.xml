<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027582124</ID>
<ANCIEN_ID>JG_L_2013_06_000000357885</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/58/21/CETATEXT000027582124.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 19/06/2013, 357885, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-06-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>357885</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Julia Beurton</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:357885.20130619</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 23 mars 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par la fédération de l'hospitalisation privée médecine, chirurgie, obstétrique (FHP-MCO), dont le siège est 81, rue de Monceau à Paris (75008), représentée par son président ; la fédération requérante demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2011-1209 du 29 septembre 2011 modifiant les dispositions relatives au contrôle de la tarification à l'activité des établissements de santé, en tant qu'il introduit dans le code de la sécurité sociale, par le 1° de son article 1er, les dispositions de l'article R. 133-9-3 et qu'il modifie, par le 5° de son article 1er, les dispositions de l'article R. 162-42-12 du même code, ainsi que la décision née le 24 janvier 2012 du silence gardé par le Premier ministre sur sa demande tendant au retrait de ce décret ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu la Constitution, notamment son Préambule ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
<br/>
              Vu le pacte international relatif aux droits civils et politiques ; <br/>
<br/>
              Vu le code de sécurité sociale ; <br/>
	Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Julia Beurton, Auditeur,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 162-22-18 du code de la sécurité sociale : " Les établissements de santé sont passibles, après qu'ils ont été mis en demeure de présenter leurs observations, d'une sanction financière en cas de manquement aux règles de facturation fixées en application des dispositions de l'article L. 162-22-6, d'erreur de codage ou d'absence de réalisation d'une prestation facturée. / Cette sanction est prise par le directeur général de l'agence régionale de santé, à la suite d'un contrôle réalisé sur pièces et sur place par les médecins inspecteurs de santé publique, les inspecteurs de l'agence régionale de santé ayant la qualité de médecin ou les praticiens-conseils des organismes d'assurance maladie en application du programme de contrôle régional établi par l'agence. (...) / Son montant est fonction du pourcentage des sommes indûment perçues par rapport aux sommes dues et du caractère réitéré des manquements. Il est calculé sur la base des recettes annuelles d'assurance maladie de l'établissement ou, si le contrôle porte sur une activité, une prestation en particulier ou des séjours présentant des caractéristiques communes, sur la base des recettes annuelles d'assurance maladie afférentes à cette activité, cette prestation ou ces séjours, dans la limite de 5 % des recettes annuelles d'assurance maladie de l'établissement. (...) / Les modalités d'application du présent article sont définies par décret en Conseil d'Etat " ; qu'aux termes de l'article R. 133-9-3, inséré dans le code de la sécurité sociale par le 1° de l'article 1er du décret attaqué, pris en application des dispositions citées ci-dessus de l'article L. 162-22-18 : " Lorsqu'à la suite d'un contrôle réalisé en application de l'article L. 162-22-18 les caisses adressent une notification de payer des sommes indûment perçues au regard des dispositions de l'article L. 162-22-6, elles procèdent, avec l'accord de l'établissement, à la compensation entre les sommes indûment perçues par l'établissement et les sommes dues par la caisse au titre des sous-facturations, le cas échéant, constatées à l'occasion du contrôle, en précisant la date, la cause, la nature et le montant de chacune des sommes dues par la caisse au titre de ces sous-facturations " ; qu'aux termes du deuxième alinéa de l'article R. 162-42-10 du code de la sécurité sociale : " Le contrôle porte sur tout ou partie de l'activité de l'établissement et peut être réalisé sur la base d'un échantillon tiré au sort " ; qu'aux termes de l'article R. 162-42-12, dans sa rédaction issue du 5° de l'article 1er du décret attaqué : " Le montant de la sanction résultant du contrôle est déterminé par le directeur général de l'agence régionale de santé après avis de la commission de contrôle. / Le directeur général de l'agence régionale de santé calcule le taux d'anomalies défini comme le quotient, d'une part, des sommes indûment perçues par l'établissement, déduction faite des sommes dues par les caisses au titre des sous-facturations constatées sur l'échantillon, et, d'autre part, des sommes dues par l'assurance maladie au titre de la totalité des facturations de l'échantillon. / La sanction est fixée en fonction de la gravité des manquements constatés et de leur caractère réitéré, à un montant au maximum égal au montant des recettes annuelles d'assurance maladie afférentes aux activités, prestations ou ensembles de séjours ayant fait l'objet du contrôle multiplié par le taux d'anomalies. / Le montant de la sanction ne peut excéder dix fois le montant des sommes indûment perçues par l'établissement, déduction faite des sommes dues par les caisses au titre des sous-facturations constatées sur l'échantillon. Le montant de la sanction est inférieur à la limite de 5 % de la totalité des recettes annuelles d'assurance maladie de l'établissement au titre de l'année civile antérieure au contrôle. (...) " ; que la fédération requérante demande l'annulation du décret attaqué en tant qu'il introduit, dans le code de la sécurité sociale, l'article R. 133-9-3, et modifie, dans ce même code, l'article R. 162-42-12 ;<br/>
<br/>
              2. Considérant, en premier lieu, que l'article R. 162-42-12 du code de la sécurité sociale, issu du décret attaqué, qui définit les modalités d'application de la sanction prévue par l'article L. 162-22-18 du même code, précise les modalités de calcul de cette sanction et détermine le montant maximal de la pénalité financière qui peut être prononcée, lequel est encadré dans la limite de trois seuils définis par le décret ; qu'il dispose notamment que, pour contrôler la tarification à l'activité d'un établissement de santé, le directeur général de l'agence régionale de santé calcule, à partir d'un échantillon de dossiers facturés, au sein d'une activité ou sur l'ensemble des activités de l'établissement, le taux d'anomalies, c'est-à-dire le rapport entre les sommes indûment perçues par l'établissement, déduction faite des sommes dues par les caisses au titre des sous-facturations constatées sur l'échantillon, et les sommes dues par l'assurance maladie au titre de la totalité des facturations sur le même échantillon ; que si l'article R. 162-42-12 prévoit qu'une des limites au montant maximal de la sanction est établie en multipliant le taux d'anomalies constatées sur un échantillon par les recettes annuelles afférentes à l'activité ayant fait l'objet du contrôle, cette disposition ne constitue qu'une modalité technique d'application de l'article L. 162-22-18 du code de la sécurité sociale, qui dispose que la sanction est fonction, notamment, du pourcentage des sommes indûment perçues par rapport aux sommes dues ; que, par suite, la fédération requérante n'est pas fondée à soutenir que l'article R. 162-42-12 du code de la sécurité sociale méconnaît les dispositions de l'article L. 162-22-18 du même code ni qu'il empiète sur le domaine réservé au législateur par l'article 34 de la Constitution et est, à ce titre, entaché d'incompétence ; qu'elle n'est pas davantage fondée à soutenir que l'article R. 162-42-11, qui mentionne également la technique de l'échantillonnage, méconnaît l'article L. 162-22-18 du code de la sécurité sociale ;<br/>
<br/>
              3. Considérant, en deuxième lieu, que les dispositions de l'article R. 162-42-10, citées au point 1, prévoient que le contrôle de l'activité de l'établissement peut être réalisé sur la base d'un échantillon tiré au sort ; que, contrairement à ce qui est soutenu, le décret pouvait légalement s'abstenir de préciser davantage les modalités de constitution de l'échantillon retenu ; que celui-ci doit, au demeurant, être suffisamment représentatif de l'activité contrôlée, au regard notamment du nombre d'actes contrôlés ; que la possibilité de tirer au sort les actes contrôlés ne fait pas, par elle-même, obstacle à cette représentativité ; qu'il est loisible à l'établissement faisant l'objet d'une sanction de contester devant le juge la pertinence de l'échantillon retenu ; que le moyen tiré de ce que la règle de l'échantillonnage méconnaît l'article L. 162-22-18 du code de la sécurité sociale doit, par suite, être écarté ;<br/>
<br/>
              4. Considérant, en troisième lieu, qu'il résulte des dispositions de l'article R. 162-42-10 du code de la sécurité sociale que, contrairement à ce qui est soutenu, le contrôle de la tarification à l'activité d'un établissement de santé peut être effectué sur la base d'un échantillon tiré au sort ; que le moyen doit, par suite, être écarté ;<br/>
<br/>
              5. Considérant, en quatrième lieu, qu'il résulte des dispositions de l'article R. 162-42-12 du code de la sécurité sociale, citées au point 1, que le montant de la sanction est proportionné à la gravité des manquements constatés et à leur caractère réitéré et ne peut, premièrement, être supérieur au montant des recettes annuelles d'assurance maladie afférentes aux activités, prestations ou ensembles de séjours ayant fait l'objet du contrôle multiplié par le taux d'anomalies relevé sur l'échantillon contrôlé, deuxièmement, excéder dix fois le montant des sommes indûment perçues par l'établissement, déduction faite des sommes dues par les caisses au titre des sous-facturations constatées sur l'échantillon, troisièmement, dépasser la limite de 5 % de la totalité des recettes annuelles d'assurance maladie de l'établissement au titre de l'année civile antérieure au contrôle, quel que soit le périmètre des activités contrôlées ; qu'il ne résulte de ces plafonds, et notamment de celui mettant en oeuvre le taux d'anomalies constatées, aucune disproportion manifeste entre la gravité des infractions poursuivies et les sanctions dont elles sont assorties ; que la fédération requérante n'est, par suite, pas fondée à soutenir que les modalités de calcul de la sanction méconnaissent le principe de proportionnalité des peines découlant de l'article 8 de la Déclaration des droits de l'homme et du citoyen ; qu'elle n'est pas non plus fondée à soutenir que les modalités de mise en oeuvre de l'échantillonnage, technique qui, contrairement à ce qui est soutenu, ne fait pas intervenir les médecins chargés du contrôle dans le choix des dossiers analysés, est contraire au principe de proportionnalité des peines ;<br/>
<br/>
              6. Considérant, en cinquième lieu, que, contrairement à ce qui est soutenu, le principe de la présomption d'innocence ne fait pas obstacle à ce que le directeur général de l'agence régionale de santé, après avoir constaté un cas de manquement prévu à l'article L. 162-22-18 du code de la sécurité sociale, prenne une sanction immédiatement exécutoire à l'encontre de l'établissement concerné ; <br/>
<br/>
              7. Considérant, en sixième lieu, que le fait de fixer une des limites au montant maximal de la pénalité financière en se référant notamment au taux d'anomalies constatées sur un échantillon, afin de dissuader de nouveaux manquements aux règles de la tarification à l'activité des établissements de santé, ne méconnaît aucun principe constitutionnel qui gouverne les règles applicables aux sanctions administratives ; qu'ainsi, la fédération requérante n'est pas fondée à soutenir que la formule consistant à multiplier le taux d'anomalies constatées sur un échantillon par les recettes annuelles afférentes à l'activité ayant fait l'objet du contrôle méconnaît le principe de la présomption d'innocence ;<br/>
<br/>
              8. Considérant, en septième lieu, que si l'article R. 133-9-3 du code de la sécurité sociale, issu du décret attaqué, fait référence, dans le cadre de la procédure de répétition d'indu initié par la caisse locale d'assurance maladie, à l'accord de l'établissement pour procéder à la compensation entre les sommes indûment perçues par l'établissement et celles dues par la caisse au titre des sous-facturations, cette mention ne porte pas atteinte, en tout état de cause, au droit de ne pas s'incriminer soi-même ; qu'au demeurant, l'accord de l'établissement ne fait nullement obstacle à ce que, le cas échéant, celui-ci conteste devant le juge le bien-fondé de l'indu et de la sanction ;<br/>
<br/>
              9. Considérant, en huitième lieu, qu'il découle des points 4, 5, 6 et 7 que la fédération requérante n'est pas fondée à soutenir que les articles R. 133-9-3 et R. 162-42-12 du code de la sécurité sociale méconnaissent les principes de proportionnalité des peines et de présomption d'innocence découlant de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; que la fédération requérante ne peut utilement soutenir que ces mêmes articles sont contraires à l'article 14 du pacte international relatif aux droits civils et politiques, qui n'est pas applicable aux sanctions financières en cause ;<br/>
<br/>
              10. Considérant qu'il résulte de ce qui précède que la requête de la fédération de l'hospitalisation privée médecine, chirurgie et obstétrique doit être rejetée, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la fédération de l'hospitalisation privée médecine, chirurgie, obstétrique est rejetée. <br/>
Article 2 : La présente décision sera notifiée à la fédération de l'hospitalisation privée médecine, chirurgie, obstétrique, au Premier ministre et à la ministre des affaires sociales et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
