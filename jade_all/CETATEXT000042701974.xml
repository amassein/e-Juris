<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042701974</ID>
<ANCIEN_ID>JG_L_2020_12_000000421988</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/70/19/CETATEXT000042701974.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 18/12/2020, 421988</TITRE>
<DATE_DEC>2020-12-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421988</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>M. Alain Seban</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:421988.20201218</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Nantes d'annuler, pour excès de pouvoir, la délibération du 20 février 2014 par laquelle le conseil municipal de l'Ile d'Yeu a approuvé le plan local d'urbanisme de la commune, ainsi que la décision implicite de rejet de son recours gracieux. Par un jugement n°1407145 du 10 janvier 2017, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 17NT00854 du 4 mai 2018, la cour administrative d'appel de Nantes a rejeté l'appel formé par M. B... contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 4 juillet et 5 octobre 2018 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de la commune de l'Ile d'Yeu la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alain Seban, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Gaschignard, avocat de M. B....<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. B... est propriétaire de diverses parcelles situées dans la commune de l'île d'Yeu. Trois de ces parcelles ayant été classées en zone ND par une délibération du conseil municipal de l'île d'Yeu du 19 juillet 2000 révisant le plan d'occupation des sols de la commune, le tribunal administratif de Nantes a, à la demande de M. B..., par un jugement du 15 juillet 2004 devenu définitif, annulé ce classement pour erreur manifeste d'appréciation, au motif que ces parcelles devaient " être regardées comme faisant partie d'une zone urbanisée ". Le nouveau plan local d'urbanisme de l'île d'Yeu, adopté par une délibération du conseil municipal du 20 février 2014, ayant classé ces trois mêmes parcelles en zone N, M. B... a demandé l'annulation de cette décision au tribunal administratif de Nantes, qui a rejeté sa demande par un jugement du 10 janvier 2017. Il se pourvoit en cassation contre l'arrêt du 4 mai 2018 par lequel la cour administrative d'appel de Nantes a rejeté son appel contre ce jugement.<br/>
<br/>
              2. Aux termes de l'article R. 123-8 du code de l'urbanisme, alors applicable, devenu l'article R.151-24 du même code : " Les zones naturelles et forestières sont dites "zones N". Peuvent être classés en zone naturelle et forestière, les secteurs de la commune, équipés ou non, à protéger en raison : / a) Soit de la qualité des sites, milieux et espaces naturels, des paysages et de leur intérêt, notamment du point de vue esthétique, historique ou écologique ; / b) Soit de l'existence d'une exploitation forestière ; / c) Soit de leur caractère d'espaces naturels (...) ".<br/>
<br/>
              3. Pour juger que le classement en zone N des parcelles litigieuses ne méconnaissait pas l'autorité absolue de chose jugée s'attachant tant au dispositif du jugement d'annulation du 15 juillet 2004 qu'aux motifs qui en constituent le soutien nécessaire, la cour administrative d'appel s'est fondée sur ce que sur ce que le parti d'aménagement choisi par les auteurs du nouveau plan local d'urbanisme retenait notamment, dans son projet d'aménagement et de développement durables, une limitation  des possibilités de construire, afin de respecter des objectifs de préservation des milieux naturels, de limitation du mitage de l'espace et d'utilisation économe des espaces naturels, de nature à justifier que les parcelles litigieuses ne soient pas classées en zone urbaine alors même que leur configuration et leur aspect n'avaient pas évolué. En se fondant ainsi sur le nouveau parti d'aménagement et de développement du plan local d'urbanisme, elle n'a pas commis d'erreur de droit. M. B... n'est, par suite, pas fondé à demander l'annulation de l'arrêt qu'il attaque.<br/>
<br/>
              4. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de la commune de l'île d'Yeu qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
		Article 1er : Le pourvoi de M. B... est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. A... B... et à la commune de l'Ile d'Yeu.<br/>
Copie en sera adressée à la ministre de la transition écologique et à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-04-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. CHOSE JUGÉE. CHOSE JUGÉE PAR LE JUGE ADMINISTRATIF. - URBANISME - ANNULATION DU CLASSEMENT DE PARCELLES EN ZONE NATURELLE POUR ERREUR MANIFESTE D'APPRÉCIATION - NOUVEAU PLU CLASSANT DE NOUVEAU CES PARCELLES EN ZONE NATURELLE - MÉCONNAISSANCE DE L'AUTORITÉ DE LA CHOSE JUGÉE - ABSENCE, COMPTE TENU DU NOUVEAU PARTI D'AMÉNAGEMENT RETENU [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-06-06-01-03 PROCÉDURE. JUGEMENTS. CHOSE JUGÉE. CHOSE JUGÉE PAR LA JURIDICTION ADMINISTRATIVE. EFFETS. - URBANISME - ANNULATION DU CLASSEMENT DE PARCELLES EN ZONE NATURELLE POUR ERREUR MANIFESTE D'APPRÉCIATION - NOUVEAU PLU CLASSANT DE NOUVEAU CES PARCELLES EN ZONE NATURELLE - MÉCONNAISSANCE DE L'AUTORITÉ DE LA CHOSE JUGÉE - ABSENCE, COMPTE TENU DU NOUVEAU PARTI D'AMÉNAGEMENT RETENU [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">68-01-01-01-03-03-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PLANS D'AMÉNAGEMENT ET D'URBANISME. PLANS D`OCCUPATION DES SOLS (POS) ET PLANS LOCAUX D'URBANISME (PLU). LÉGALITÉ DES PLANS. LÉGALITÉ INTERNE. APPRÉCIATIONS SOUMISES À UN CONTRÔLE D'ERREUR MANIFESTE. CLASSEMENT ET DÉLIMITATION DES ONES. - ANNULATION DU CLASSEMENT DE PARCELLES EN ZONE NATURELLE POUR ERREUR MANIFESTE D'APPRÉCIATION - NOUVEAU PLU CLASSANT DE NOUVEAU CES PARCELLES EN ZONE NATURELLE - MÉCONNAISSANCE DE L'AUTORITÉ DE LA CHOSE JUGÉE - ABSENCE, COMPTE TENU DU NOUVEAU PARTI D'AMÉNAGEMENT RETENU [RJ1].
</SCT>
<ANA ID="9A"> 01-04-04-02 Jugement devenu définitif ayant annulé le classement de trois parcelles en zone ND pour erreur manifeste d'appréciation au motif que ces parcelles devaient être regardées comme faisant partie d'une zone urbanisée. Nouveau plan local d'urbanisme (PLU) ayant classé ces parcelles en zone N.,,,Un tel classement ne méconnaît pas l'autorité absolue de chose jugée s'attachant tant au dispositif du jugement d'annulation qu'aux motifs qui en constituent le soutien nécessaire, dès lors que le parti d'aménagement choisi par les auteurs du nouveau PLU retenait notamment, dans son projet d'aménagement et de développement durables (PADD), une limitation des possibilités de construire, afin de respecter des objectifs de préservation des milieux naturels, de limitation du mitage de l'espace et d'utilisation économe des espaces naturels, de nature à justifier que les parcelles litigieuses ne soient pas classées en zone urbaine alors même que leur configuration et leur aspect n'avaient pas évolué.</ANA>
<ANA ID="9B"> 54-06-06-01-03 Jugement devenu définitif ayant annulé le classement de trois parcelles en zone ND pour erreur manifeste d'appréciation au motif que ces parcelles devaient être regardées comme faisant partie d'une zone urbanisée. Nouveau plan local d'urbanisme (PLU) ayant classé ces parcelles en zone N.,,,Un tel classement ne méconnaît pas l'autorité absolue de chose jugée s'attachant tant au dispositif du jugement d'annulation qu'aux motifs qui en constituent le soutien nécessaire, dès lors que le parti d'aménagement choisi par les auteurs du nouveau PLU retenait notamment, dans son projet d'aménagement et de développement durables (PADD), une limitation des possibilités de construire, afin de respecter des objectifs de préservation des milieux naturels, de limitation du mitage de l'espace et d'utilisation économe des espaces naturels, de nature à justifier que les parcelles litigieuses ne soient pas classées en zone urbaine alors même que leur configuration et leur aspect n'avaient pas évolué.</ANA>
<ANA ID="9C"> 68-01-01-01-03-03-01 Jugement devenu définitif ayant annulé le classement de trois parcelles en zone ND pour erreur manifeste d'appréciation au motif que ces parcelles devaient être regardées comme faisant partie d'une zone urbanisée. Nouveau plan local d'urbanisme (PLU) ayant classé ces parcelles en zone N.,,,Un tel classement ne méconnaît pas l'autorité absolue de chose jugée s'attachant tant au dispositif du jugement d'annulation qu'aux motifs qui en constituent le soutien nécessaire, dès lors que le parti d'aménagement choisi par les auteurs du nouveau PLU retenait notamment, dans son projet d'aménagement et de développement durables (PADD), une limitation des possibilités de construire, afin de respecter des objectifs de préservation des milieux naturels, de limitation du mitage de l'espace et d'utilisation économe des espaces naturels, de nature à justifier que les parcelles litigieuses ne soient pas classées en zone urbaine alors même que leur configuration et leur aspect n'avaient pas évolué.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant de la prise en compte des orientations générales et du PADD pour apprécier le classement de parcelles, CE, 2 octobre 2017, Montpellier Méditerranée Métropole et Commune de Lattes, n° 398322, T. pp. 844-847 ; CE, 3 juin 2020, Société Inerta, n° 429515, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
