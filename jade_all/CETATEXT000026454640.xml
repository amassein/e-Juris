<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026454640</ID>
<ANCIEN_ID>JG_L_2012_10_000000344489</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/45/46/CETATEXT000026454640.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  8ème sous-section jugeant seule, 03/10/2012, 344489, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-10-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>344489</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 8ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Gilles Bachelier</PRESIDENT>
<AVOCATS>SCP DEFRENOIS, LEVIS</AVOCATS>
<RAPPORTEUR>M. Jean-Marc Vié</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:344489.20121003</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 23 novembre 2010 et 23 février 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société Batipro logement intermédiaire, dont le siège est au 190 rue des Deux Canons à Sainte-Clotilde (97490) ; elle demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 0700821 du 26 août 2010 par lequel le tribunal administratif de Saint-Denis a rejeté sa demande tendant à la décharge de la taxe foncière sur les propriétés bâties à laquelle elle a été assujettie au titre des années 2005 et 2006 à raison des immeubles de la résidence " Ile-de-France " dont elle est propriétaire à Saint-André (La Réunion) ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Vié, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Defrenois, Levis avocat de la société Batipro logement intermédiaire,<br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Defrenois, Levis avocat de la société Batipro logement intermédiaire ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société Batipro logement intermédiaire est propriétaire d'un ensemble collectif à usage d'habitation constitué de 74 appartements composant la résidence " Ile-de-France ", situé 505 avenue de l'Ile-de-France et 46 rue du stade sur le territoire de la commune de Saint-André (La Réunion) ; que la société a été assujettie à ce titre à la taxe foncière sur les propriétés bâties au titre des années 2005 et 2006 ; que l'administration, qui a déterminé la valeur locative cadastrale de ces appartements selon la méthode par comparaison prévue à l'article 1496 du code général des impôts pour les locaux affectés à l'habitation, a rattaché ces appartements à la catégorie 6 du procès-verbal de révision des évaluations foncières des locaux d'habitation réalisées dans cette commune en 1976 et a retenu comme local de référence le local n° 37 identifié " AC 278, CD 47 Cambuston " ; que la société requérante a contesté les modalités de détermination de la valeur locative de cet ensemble immobilier ; qu'elle se pourvoit en cassation contre le jugement du 26 août 2010 par lequel le tribunal administratif de Saint-Denis a rejeté sa demande contestant le choix de ce local de référence ;<br/>
<br/>
              2. Considérant qu'en vertu de l'article 1415 du code général des impôts, la taxe foncière sur les propriétés bâties est établie pour l'année entière d'après les faits existants au 1er janvier de l'année de l'imposition ; qu'aux termes de l'article 1496 du même code : " I. La valeur locative des locaux affectés à l'habitation ou servant à l'exercice soit d'une activité salariée à domicile, soit d'une activité professionnelle non commerciale au sens du 1 de l'article 92 est déterminée par comparaison avec celle de locaux de référence choisis, dans la commune, pour chaque nature et catégorie de locaux. II. La valeur locative des locaux de référence est déterminée d'après un tarif fixé par commune ou secteur de commune, pour chaque nature et catégorie de locaux, en fonction du loyer des locaux loués librement à des conditions de prix normales et de manière à assurer l'homogénéité des évaluations dans la commune et de commune à commune (...) " ; qu'aux termes du I de l'article 324 G de l'annexe III du code général des impôts : " La classification communale consiste à rechercher et à définir, par nature de construction (maisons individuelles, immeubles collectifs, dépendances bâties isolées), les diverses catégories de locaux d'habitation ou à usage professionnel existant dans la commune. (...) " ; <br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions que ne peuvent être utilisés comme locaux de référence que des locaux inscrits aux procès-verbaux des opérations d'évaluation foncière des propriétés bâties communales au 1er janvier de l'année au titre de laquelle l'imposition est établie, précisément identifiables à cette date et correspondant à la nature de construction des locaux dont il s'agit de déterminer la valeur locative ;<br/>
<br/>
              4. Considérant que, pour rejeter la demande de la société Batipro logement intermédiaire, le tribunal a estimé que, si la société faisait état, d'une part, de la difficile identification du local de référence retenu par l'administration et produisait à cet égard divers éléments allant dans le sens de la probable disparition de la maison individuelle choisie en 1976 comme local de référence n° 37 relevant de la catégorie 6, et, d'autre part, se prévalait de la circonstance que le procès-verbal ne comportait aucune mention explicite d'une catégorie 6 applicable aux immeubles collectifs, elle ne fondait sa demande sur aucun élément concret inhérent à la consistance même de ses propres appartements afin d'établir le caractère inapproprié du rattachement aux maisons de catégorie 6 et la nécessité de les rattacher à une autre catégorie ; qu'en statuant ainsi, alors que le local de référence retenu doit être précisément identifiable d'après le procès-verbal des opérations d'évaluation au 1er janvier de l'année d'imposition et correspondre à la nature des locaux à évaluer, et, en s'abstenant, alors que les documents produits ne lui permettaient pas de s'assurer que ces conditions étaient satisfaites, d'ordonner un supplément d'instruction, le tribunal a, eu égard à l'argumentation qui lui était soumise, commis une erreur de droit et insuffisamment motivé son jugement ; que le ministre ne peut se prévaloir en défense des pièces produites pour la première fois devant le juge de cassation ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la société Batipro logement intermédiaire est fondée à demander l'annulation du jugement attaqué ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3000 euros à verser à la société Batipro logement intermédiaire au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : Le jugement du 26 août 2010 du tribunal administratif de Saint-Denis est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Saint-Denis.<br/>
<br/>
Article 3 : L'Etat versera à la société Batipro logement intermédiaire la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la société Batipro logement intermédiaire et au ministre de l'économie et des finances.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
