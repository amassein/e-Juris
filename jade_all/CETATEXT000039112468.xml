<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039112468</ID>
<ANCIEN_ID>JG_L_2019_09_000000422962</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/11/24/CETATEXT000039112468.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 18/09/2019, 422962</TITRE>
<DATE_DEC>2019-09-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>422962</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR>Mme Sara-Lou Gerber</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:422962.20190918</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Par une requête et deux mémoires en réplique, enregistrés le 6 août 2018 et les 20 février et 13 juin 2019 au secrétariat du contentieux du Conseil d'Etat, M. A... B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 2 juillet 2018 par laquelle le directeur de l'institut national des sciences appliquées (INSA) de Lyon a déclaré le concours de recrutement pour le poste de professeur des universités " PR-4224 " infructueux et a décidé d'y mettre fin ;<br/>
<br/>
              2°) d'ordonner la reprise de la procédure de recrutement dans le délai de huit jours à compter de la décision à intervenir, sous astreinte de 200 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat et de l'INSA de Lyon la somme de 4 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'éducation ;<br/>
              - la loi n° 2007-1199 du 10 août 2007 ;<br/>
              - la loi n° 2013-660 du 22 juillet 2013 ; <br/>
              - l'ordonnance n° 2014-807 du 17 juillet 2014 ;<br/>
              - le décret n° 84-431 du 6 juin 1984 ;<br/>
              - le décret n° 2014-997 du 2 septembre 2014 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sara-Lou Gerber, auditeur,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de M. A... B... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier que M. B..., maître de conférences en informatique, a présenté sa candidature à un concours de recrutement ouvert par l'institut national des sciences appliquées (INSA) de Lyon pour un emploi de professeur des universités " PR-4224 " au sein de son département d'informatique. Par une délibération du 24 mai 2018, le comité de sélection institué pour ce recrutement a établi, après audition de candidats, une liste de candidats sur laquelle M. B... était classé premier. Lors de sa séance du 31 mai 2018, le conseil d'administration de l'INSA, siégeant en formation restreinte, a émis un avis favorable sur cette liste. Par une décision du 2 juillet 2018, dont M. B... demande l'annulation pour excès de pouvoir, le directeur de l'INSA a toutefois interrompu ce concours et l'a déclaré infructueux, au motif que la délibération du comité de sélection lui apparaissait irrégulière. <br/>
<br/>
              Sur les conclusions à fin d'annulation :<br/>
<br/>
              2. Aux termes de l'article L. 952-6-1 du code de l'éducation, dans sa rédaction résultant de la loi du 22 juillet 2013 relative à l'enseignement supérieur et à la recherche et de l'ordonnance du 17 juillet 2014 modifiant la partie législative du code de l'éducation, qui s'applique à l'INSA de Lyon : " (...) lorsqu'un emploi d'enseignant-chercheur est créé ou déclaré vacant au sein de l'établissement, les candidatures (...) sont soumises à l'examen d'un comité de sélection créé par délibération du (...) conseil d'administration, siégeant en formation restreinte aux représentants élus des enseignants-chercheurs, des chercheurs et des personnels assimilés. / (...) Au vu de son avis motivé, le conseil d'administration, siégeant en formation restreinte (...), transmet au ministre compétent le nom du candidat dont il propose la nomination ou une liste de candidats classés par ordre de préférence (...) ". Aux termes du onzième alinéa de l'article 9-2 du décret du 6 juin 1984 fixant les dispositions statutaires communes applicables aux enseignants-chercheurs et portant statut particulier du corps des professeurs des universités et du corps des maîtres de conférences, dans sa rédaction issue du décret du 2 septembre 2014 : " Sauf dans le cas où le conseil d'administration émet un avis défavorable motivé, le (...) directeur de l'établissement communique au ministre chargé de l'enseignement supérieur le nom du candidat sélectionné ou, le cas échéant, une liste de candidats classés par ordre de préférence. En aucun cas, il ne peut modifier l'ordre de la liste de classement ". <br/>
<br/>
              3. Si les dispositions de l'article L. 952-6-1 du code de l'éducation telles qu'issues de la loi du 10 août 2007 relative aux libertés et responsabilités des universités prévoyaient que le président de l'université, ou pour les instituts et écoles ne faisant pas partie des universités le directeur de l'établissement, pouvait émettre un avis défavorable, pour des motifs tenant à l'administration de l'université ou de l'établissement, sur la proposition de nomination d'un enseignant-chercheur faite par le conseil d'administration, lequel avis avait pour effet de mettre fin à la procédure de recrutement, la loi du 22 juillet 2013 relative à l'enseignement supérieur et à la recherche a supprimé cette possibilité. Dès lors, depuis l'entrée en vigueur de la loi du 22 juillet 2013, s'il est toujours loisible au président de l'université, ou au directeur de l'établissement, lorsqu'il estime que la procédure de recrutement d'un enseignant-chercheur est irrégulière, de demander au conseil d'administration de délibérer à nouveau sur l'avis motivé du comité de sélection ou de faire part de ses observations sur la procédure au ministre chargé de l'enseignement supérieur à l'occasion de la transmission du nom du candidat ou de la liste arrêtée par le comité de sélection, aucune disposition ni aucun principe n'investit le président de l'université ou le directeur de l'établissement du pouvoir de ne pas donner suite à une procédure de recrutement d'un enseignant-chercheur lorsque le conseil d'administration a émis un avis favorable. <br/>
<br/>
              4. Il découle de ce qui précède que le directeur de l'INSA de Lyon n'a pu légalement, par sa décision du 2 juillet 2018, interrompre la procédure de recrutement pour l'emploi de professeur des universités " PR-4224 ", alors que le conseil d'administration de l'institut avait donné un avis favorable à la liste de candidats retenue par le comité de sélection. Par suite, et sans qu'il soit besoin d'examiner les autres moyens de sa requête, M. B... est fondé à demander l'annulation pour excès de pouvoir de la décision du 2 juillet 2018 du directeur de l'INSA de Lyon.<br/>
<br/>
              Sur les conclusions à fin d'injonction :<br/>
<br/>
              5. L'exécution de la présente décision d'annulation implique de reprendre la procédure de recrutement pour l'emploi de professeur des universités " PR-4224 " au stade de la transmission, par le directeur de l'INSA de Lyon, au ministre chargé de l'enseignement supérieur de la liste de candidats classés par ordre de préférence. Il y a donc lieu d'enjoindre au directeur de l'INSA de Lyon de reprendre la procédure à ce stade dans un délai d'un mois à compter de la notification de la présente décision. Il n'y a pas lieu, en revanche, d'assortir cette injonction d'une astreinte. <br/>
<br/>
              Sur les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'INSA de Lyon la somme de 3 000 euros à verser à M. B... au titre des dispositions de l'article L. 761-1 du code de justice administrative. En revanche ces dispositions font obstacle à ce que soient accueillies les conclusions qu'il présente au même titre contre l'Etat, qui n'a pas la qualité de partie dans la présente instance.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La décision du 2 juillet 2018 du directeur de l'INSA de Lyon est annulée.<br/>
Article 2 : Il est enjoint au directeur de l'INSA de Lyon de reprendre la procédure au stade de la communication prévue à l'article 9-2 du décret du 6 juin 1984 dans un délai d'un mois à compter de la notification de la présente décision. <br/>
Article 3 : L'INSA de Lyon versera à M. B... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Le surplus des conclusions de M. B... est rejeté.<br/>
Article 5 : La présente décision sera notifiée à M. A... B... et à l'Institut national des sciences appliquées (INSA) de Lyon.<br/>
Copie en sera adressée à la ministre de l'enseignement supérieur, de la recherche et de l'innovation.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">30-02-05 ENSEIGNEMENT ET RECHERCHE. QUESTIONS PROPRES AUX DIFFÉRENTES CATÉGORIES D'ENSEIGNEMENT. ENSEIGNEMENT SUPÉRIEUR ET GRANDES ÉCOLES. - NOMINATION D'ENSEIGNANTS-CHERCHEURS - POUVOIRS DU PRÉSIDENT DE L'UNIVERSITÉ OU DU DIRECTEUR DE L'ÉTABLISSEMENT (LOI DU 22 JUILLET 2013) - POSSIBILITÉ, LORSQU'IL ESTIME QUE LA PROCÉDURE EST IRRÉGULIÈRE, DE DEMANDER AU CONSEIL D'ADMINISTRATION DE DÉLIBÉRER À NOUVEAU OU DE FAIRE PART DE SES OBSERVATIONS SUR LA PROCÉDURE AU MINISTRE - EXISTENCE - POUVOIR DE NE PAS DONNER SUITE À UNE PROCÉDURE DE RECRUTEMENT LORSQUE LE CONSEIL D'ADMINISTRATION A ÉMIS UN AVIS FAVORABLE - ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 30-02-05 Si les dispositions de l'article L. 952-6-1 du code de l'éducation telles qu'issues de la loi n° 2007-1199 du 10 août 2007 prévoyaient que le président de l'université, ou pour les instituts et écoles ne faisant pas partie des universités le directeur de l'établissement, pouvait émettre un avis défavorable, pour des motifs tenant à l'administration de l'université ou de l'établissement, sur la proposition de nomination d'un enseignant-chercheur faite par le conseil d'administration, lequel avis avait pour effet de mettre fin à la procédure de recrutement, la loi n° 2013-660 du 22 juillet 2013 a supprimé cette possibilité. Dès lors, depuis l'entrée en vigueur de la loi du 22 juillet 2013, s'il est toujours loisible au président de l'université, ou au directeur de l'établissement, lorsqu'il estime que la procédure de recrutement d'un enseignant-chercheur est irrégulière, de demander au conseil d'administration de délibérer à nouveau sur l'avis motivé du comité de sélection ou de faire part de ses observations sur la procédure au ministre chargé de l'enseignement supérieur à l'occasion de la transmission du nom du candidat ou de la liste arrêtée par le comité de sélection, aucune disposition ni aucun principe n'investit le président de l'université ou le directeur de l'établissement du pouvoir de ne pas donner suite à une procédure de recrutement d'un enseignant-chercheur lorsque le conseil d'administration a émis un avis favorable.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp., sur les pouvoirs du président de l'université ou du directeur de l'établissement sous l'empire de la loi du 10 août 2007, CE, 5 décembre 2011,,, n° 333809, p. 606 ; CE, 5 décembre 2011, M.,, n° 334059, p. 611.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
