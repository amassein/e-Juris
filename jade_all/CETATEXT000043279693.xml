<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043279693</ID>
<ANCIEN_ID>JG_L_2021_03_000000443094</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/27/96/CETATEXT000043279693.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 15/03/2021, 443094, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-03-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>443094</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET BRIARD</AVOCATS>
<RAPPORTEUR>M. Jean-Marc Vié</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:443094.20210315</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société anonyme (SA) Goodyear a demandé au tribunal administratif de Montpellier de prononcer la réduction des cotisations de taxe foncière sur les propriétés bâties, de taxe spéciale d'équipement et de taxe d'enlèvement des ordures ménagères auxquelles elle a été assujettie au titre de l'année 2017 dans les rôles de la commune de Mireval (Hérault). Par un jugement n° 1803697 du 24 février 2020, ce tribunal a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 20 août et 20 novembre 2020, au secrétariat du contentieux du Conseil d'Etat, la société Goodyear demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le décret n° 2011-1267 du 10 octobre 2011 ;<br/>
- le code de justice administrative et le décret n° 2020-1406 du 14 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Vié, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme A... B..., rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, au Cabinet Briard, avocat de la société Goodyear ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              Sur la taxe spéciale d'équipement :<br/>
<br/>
              1. Selon le 4° de l'article R. 811-1 du code de justice administrative, le tribunal administratif statue en premier et dernier ressort sur les litiges relatifs aux impôts locaux et à la contribution à l'audiovisuel public, à l'exception des litiges relatifs à la contribution économique territoriale.<br/>
<br/>
              2. La taxe spéciale d'équipement en litige mise à la charge de la société Goodyear a été perçue au profit d'un établissement public de l'État. Par suite, cette taxe ne saurait être regardée comme une imposition locale au sens de l'article R. 811-1 du code de justice administrative. Dès lors, les conclusions de la société Goodyear doivent être regardées, dans cette mesure, comme un appel, dont il appartient à la cour administrative d'appel de Marseille de connaître.<br/>
<br/>
              Sur la taxe foncière et la taxe d'enlèvement des ordures ménagères :<br/>
<br/>
              3. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
<br/>
              4. Pour demander l'annulation du jugement qu'elle attaque, la société Goodyear soutient que le tribunal administratif de Montpellier :<br/>
              - l'a insuffisamment motivé en se bornant à relever que les locaux professionnels en litige n'étaient assimilables à aucune des catégories du sous-groupe VI " établissements et spectacles, de sports et de loisirs " définies à l'article 1er du décret du 10 octobre 2011 et reprises à l'article 310 Q de l'annexe II au code général des impôts ;<br/>
              - a dénaturé les faits et les pièces du dossier et commis une erreur de droit en jugeant que le principe général des droits de la défense n'avait pas été méconnu dès lors que l'administration n'avait pas mis à sa charge des droits excédant ceux qui résultaient de ses déclarations ;<br/>
              - a dénaturé les faits et les pièces du dossier et commis une erreur de droit en jugeant que l'administration, pour déterminer la valeur locative de ses installations, avait appliqué à bon droit la méthode d'appréciation directe et non la méthode tarifaire ;<br/>
              - l'a insuffisamment motivé et a commis une erreur de droit en validant un calcul de la valeur locative de ses installations à partir des valeurs vénales actualisées au 1er janvier 2013 au moyen de l'évolution de l'indice INSEE du coût de la construction, sans prendre en compte les abattements retenus par des arrêts de la cour administrative d'appel de Marseille du 21 novembre 2017 et des jugements rendus par ce même tribunal administratif de Montpellier le 6 novembre 2017 ;<br/>
              - a méconnu l'article L. 80 B du livre des procédures fiscales en jugeant que la fiche de calcul qui lui a été adressée le 5 décembre 2017 ne constituait pas une prise de position formelle de l'administration.<br/>
<br/>
              5. Aucun de ces moyens n'est de nature à permettre l'admission de ces conclusions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les conclusions de la société Goodyear dirigées contre le jugement attaqué en tant qu'il s'est prononcé sur la taxe spéciale d'équipement sont attribuées à la cour administrative d'appel de Marseille.<br/>
      Article 2 : Le surplus des conclusions du pourvoi de la société Goodyear n'est pas admis.<br/>
Article 3 : La présente décision sera notifiée à la société anonyme Goodyear, au ministre de l'économie, des finances et de la relance et au président de la cour administrative d'appel de Marseille.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
