<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044515626</ID>
<ANCIEN_ID>J3_L_2021_12_00020BX00824</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/51/56/CETATEXT000044515626.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>CAA de BORDEAUX, 7ème chambre (formation à 3), 16/12/2021, 20BX00824, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-16</DATE_DEC>
<JURIDICTION>CAA de BORDEAUX</JURIDICTION>
<NUMERO>20BX00824</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre (formation à 3)</FORMATION>
<TYPE_REC>plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. REY-BETHBEDER</PRESIDENT>
<AVOCATS>LE FAOU</AVOCATS>
<RAPPORTEUR>Mme Nathalie  GAY</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme MADELAIGUE</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
      Procédure contentieuse antérieure :<br/>
<br/>
      La société civile immobilière (SCI) Maison des Métiers 3 a demandé au tribunal administratif de Limoges de prononcer le remboursement d'un crédit de taxe sur la valeur ajoutée pour un montant de 80 400 euros au titre de la période du 1er septembre au 31 décembre 2015. <br/>
<br/>
      Par un jugement n° 1701343 du 26 décembre 2019, le tribunal administratif de Limoges a rejeté sa demande. <br/>
<br/>
      Procédure devant la cour : <br/>
<br/>
      Par une requête et un mémoire, enregistrés les 6 mars 2020 et 23 mars 2021, la SCI Maison des Métiers 3, représentée par Me Le Faou puis par Me Cortez et Malmonté, demande à la cour : <br/>
<br/>
      1°) d'annuler ce jugement du tribunal administratif de Limoges du 26 décembre 2019 ;  <br/>
<br/>
<br/>
<br/>
      2°) de prononcer le remboursement d'un crédit de la taxe sur la valeur ajoutée pour un montant de 80 400 euros pour la période du 1er septembre au 31 décembre 2015 et de prononcer la capitalisation desdits intérêts en application des dispositions de l'article 1343-2 du code civil ;<br/>
<br/>
      3°) de mettre à la charge de l'État une somme de 5 000 euros en application de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
      Elle soutient que :<br/>
      - les locations meublées proposées avec mise à disposition des prestations énoncées au b du 4° de l'article 261 D du code général des impôts sont exclues de l'exonération de taxe sur la valeur ajoutée et la taxe sur la valeur ajoutée ayant grevé l'achat de l'immeuble est déductible ; en l'occurrence, les insuffisances rédactionnelles du bail commercial n'ont pas d'incidence au regard du b du 4° de l'article 261 D du code général des impôts ; les prestations de petit-déjeuner, de nettoyage régulier des locaux, de réception et de fourniture de linge de maison sont assurées ; <br/>
      - en refusant la déduction sollicitée à proportion du temps écoulé entre la date de l'acquisition de l'appartement et celle de son exploitation effective, le service méconnaît le principe de neutralité de la taxe sur la valeur ajoutée.     <br/>
<br/>
      Par deux mémoires, enregistrés le 1er octobre 2020 et le 15 octobre 2021, ce dernier n'ayant pas été communiqué, le ministre de l'économie, des finances et de la relance conclut au rejet de la requête. <br/>
<br/>
      Il soutient que les moyens développés par la société ne sont pas fondés. <br/>
<br/>
      Vu les autres pièces du dossier. <br/>
<br/>
      Vu : <br/>
      - le code général des impôts et le livre des procédures fiscales ;<br/>
      - le code de justice administrative.<br/>
<br/>
      Les parties ont été régulièrement averties du jour de l'audience. <br/>
<br/>
      Ont été entendu au cours de l'audience publique : <br/>
      - le rapport de Mme Nathalie Gay; <br/>
      - et les conclusions de Mme Madelaigue, rapporteure publique.<br/>
      Considérant ce qui suit : <br/>
<br/>
      1. La société civile immobilière (SCI) Maison des Métiers 3 a acquis par un acte du 15 octobre 2015, le lot n° 3 d'un immeuble en copropriété situé à Noth au lieu-dit La Fôt pour un montant total de 482 400 euros, dont 80 400 euros de taxe sur la valeur ajoutée. Le 13 mai 2016, elle a sollicité auprès de l'administration fiscale le remboursement d'un crédit de taxe sur la valeur ajoutée d'un montant de 80 400 euros pour la période du 1er septembre au 31 décembre 2015. Par un avis de vérification du 8 juillet 2016, la société a fait l'objet d'une vérification de comptabilité portant sur la taxe de valeur ajoutée au titre de la période du 28 août au 31 décembre 2015. Le 2 août 2017, le service a rejeté sa demande. La société Maison des Métiers 3 relève appel du jugement du 26 décembre 2019 par lequel le tribunal administratif de Limoges a rejeté sa demande tendant au remboursement d'un crédit de taxe sur la valeur ajoutée d'un montant de 80 400 euros pour la période du 1er septembre au 31 décembre 2015. <br/>
<br/>
      Sur le bien-fondé du jugement : <br/>
<br/>
      2. Aux termes de l'article 256 du code général des impôts : " I. Sont soumises à la taxe sur la valeur ajoutée les livraisons de biens et les prestations de services effectuées à titre onéreux par un assujetti agissant en tant que tel (...) ". Aux termes de l'article 261 D du même code : " Sont exonérées de la taxe sur la valeur ajoutée : (...) 4° Les locations occasionnelles, permanentes ou saisonnières de logements meublés ou garnis à usage d'habitation. / Toutefois, l'exonération ne s'applique pas : (...) / b. Aux prestations de mise à disposition d'un local meublé ou garni effectuées à titre onéreux et de manière habituelle, comportant en sus de l'hébergement au moins trois des prestations suivantes, rendues dans des conditions similaires à celles proposées par les établissements d'hébergement à caractère hôtelier exploités de manière professionnelle : le petit déjeuner, le nettoyage régulier des locaux, la fourniture de linge de maison et la réception, même non personnalisée, de la clientèle (...) ". Sous réserve des cas où la loi attribue la charge de la preuve au contribuable, il appartient au juge de l'impôt, au vu de l'instruction et compte tenu, le cas échéant, de l'abstention d'une des parties à produire les éléments qu'elle est seule en mesure d'apporter et qui ne sauraient être réclamés qu'à elle-même, d'apprécier si la situation du contribuable entre dans le champ de l'assujettissement à la taxe sur la valeur ajoutée ou, le cas échéant, s'il remplit les conditions légales d'une exonération.<br/>
<br/>
      3. Il résulte de l'instruction que le bail commercial conclu entre la SCI Maison des Métiers 4 et la société " Château de la Cazine SAS " indique que " le preneur exercera (...) une activité d'exploitant de résidence de tourisme classée consistant en la location meublée desdits biens pour des périodes de temps déterminées, avec la fourniture de différents services ou prestations para hôtelières à sa clientèle " sans apporter de précisions quant aux prestations para hôtelières qui doivent être offertes aux clients. Au surplus, l'administration fait valoir, sans être utilement contredite que ce contrat de bail est un document antidaté dès lors que la SCI Maison des métiers 3 lui a indiqué le 12 septembre 2016 qu'aucun bail n'avait été signé. Il résulte de ce qui précède que la société ne justifie pas avoir eu l'intention de donner à la location l'appartement en litige. Par ailleurs, le procès-verbal d'huissier du 29 novembre 2019 atteste de la possibilité de réserver, depuis le site du château de la Cazine, un appartement au sein de la maison des métiers disposant de deux grandes chambres et deux salles de bain, d'une cuisine équipée et d'un coin salon, et il ne résulte pas de ce document que des prestations para hôtelières sont proposées avec la location de cet hébergement. En outre, les prestations de nettoyage régulier des locaux ne figurent pas sur le site comme étant proposées pour l'appartement mis en location et les bulletins de salaire de personnes employées en qualité de femme de chambre, de gouvernante et de gouvernante en chef produits par la société appelante, ne permettent pas, faute de précision quant au lieu de réalisation des prestations, de tenir pour établi que la société disposait des moyens nécessaires pour répondre aux éventuelles demandes des occupants de l'appartement en cause quant au nettoyage régulier des locaux. Ainsi, l'appelante ne peut être regardée comme ayant mis à la disposition de sa clientèle des prestations de nettoyage quotidien dans des conditions similaires à celles proposées par les établissements d'hébergement à caractère hôtelier exploités de manière professionnelle. Enfin, si la société produit une facture datée du 17 août 2017 attestant de la location de l'appartement avec prestation de petit déjeuner, il est constant que les prestations de petit-déjeuner et de réception ne sont proposées qu'au château de la Cazine, dont il n'est <br/>
pas contesté qu'il se situe à 800 mètres de l'appartement en litige via la route de la Fôt, ce qui ne peut être regardé comme correspondant à la mise à disposition d'une prestation au sens du b du 4° de l'article 261 D du code général des impôts. Dans ces conditions, les prestations de petit-déjeuner, de réception et de nettoyage de l'appartement ne peuvent être regardées comme ayant été proposées dans des conditions similaires à celles proposées par les établissements d'hébergement à caractère hôtelier exploités de manière professionnelle. Il suit de là qu'alors même que la société appelante a la qualité d'assujetti à la taxe sur la valeur ajoutée, le service était en droit de refuser, comme il l'a fait, de lui accorder le remboursement de la taxe sur la valeur ajoutée ayant grevé l'acquisition du lot n° 3 d'un immeuble en copropriété situé à Noth au lieu-dit La Fôt. <br/>
<br/>
      4. L'appelante ne peut pas se prévaloir, sur le fondement de l'article L. 80 A du livre des procédures fiscales, de la doctrine référencée BOI-TVA-CHAMP-10-10-50-20 du 12 septembre 2012, qui ne comporte pas d'interprétation de la loi fiscale différente de celle dont fait application le présent arrêt.  <br/>
<br/>
      Sur les frais liés au litige : <br/>
<br/>
      5. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de l'État, qui n'est pas, dans la présente instance, la partie perdante, le versement d'une somme au titre de ces dispositions.<br/>
						DÉCIDE : <br/>
Article 1er : La requête de la société Maison des Métiers 3 est rejetée. <br/>
Article 2 : Le présent arrêt sera notifié à la SCI Maison des Métiers 3 et au ministre de l'économie, des finances et de la relance. <br/>
Copie en sera adressée à la direction de contrôle fiscal sud-ouest.  <br/>
Délibéré après l'audience du 18 novembre 2021 à laquelle siégeaient : <br/>
M. Éric Rey-Bèthbéder, président,<br/>
Mme Frédérique Munoz-Pauziès, présidente-assesseure,<br/>
Mme Nathalie Gay, première conseillère.<br/>
<br/>
Rendu public par mise à disposition au greffe le 16 décembre 2021. <br/>
<br/>
La rapporteure,<br/>
<br/>
<br/>
<br/>
Nathalie GayLe président,<br/>
<br/>
<br/>
<br/>
Éric Rey-Bèthbéder <br/>
La greffière,<br/>
<br/>
<br/>
<br/>
Angélique Bonkoungou<br/>
La République mande et ordonne au ministre de l'économie, des finances et de la relance en ce qui le concerne et à tous huissiers de justice à ce requis en ce qui concerne les voies de droit commun, contre les parties privées, de pourvoir à l'exécution du présent arrêt.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
5<br/>
N°20BX00824<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">19-06-02-02 Contributions et taxes. - Taxes sur le chiffre d'affaires et assimilées. - Taxe sur la valeur ajoutée. - Exemptions et exonérations.
</SCT>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
