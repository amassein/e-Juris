<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037478642</ID>
<ANCIEN_ID>JG_L_2018_10_000000420540</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/47/86/CETATEXT000037478642.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 08/10/2018, 420540, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-10-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420540</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>M. Vincent Ploquin-Duchefdelaville</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:420540.20181008</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société par actions simplifiée (SAS) Distribution Sanitaire Chauffage a demandé au tribunal administratif de Toulouse de prononcer la décharge des rappels de taxe sur les surfaces commerciales auxquels elle a été assujettie au titre des années 2010 à 2012, à raison des établissements qu'elle exploite à Toulouse, Villeneuve-de-Rivière, Rodez, Montauban et Albi. Par un jugement n° 1600487 du 13 mars 2018, ce tribunal a rejeté cette demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 11 mai, 13 août et 27 septembre 2018 au secrétariat du contentieux du Conseil d'Etat, la société Distribution Sanitaire Chauffage demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement en tant qu'il se prononce sur les impositions établies au titre des années 2011 et 2012 ;<br/>
<br/>
              2°) réglant l'affaire au fond dans cette mesure, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Par un mémoire distinct et un nouveau mémoire, enregistrés les 10 juillet et 2 août 2018, la SAS Distribution Sanitaire Chauffage demande au Conseil d'Etat, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 et à l'appui de son pourvoi en cassation, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution de la première phrase du dix-septième alinéa de l'article 3 de la loi du 13 juillet 1972 dans sa version applicable aux impositions établies au titre des années 2011 et 2012.  <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ; <br/>
              - le code civil ;<br/>
              - le code général des impôts et le livre des procédures fiscales ; <br/>
              - la loi n° 72-657 du 13 juillet 1972 ;<br/>
              - le décret n° 95-85 du 26 janvier 1995 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Ploquin-Duchefdelaville, auditeur,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Soltner, Texidor, Perier, avocat de la société Distribution Sanitaire Chauffage.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              Sur la question prioritaire de constitutionnalité : <br/>
<br/>
              1. Aux termes des premier et dix-septième alinéas de l'article 3 de la loi du 13 juillet 1972 instituant des mesures en faveur de certaines catégories de commerçants et artisans âgés, dans sa rédaction applicable à l'année d'imposition en litige : " Il est institué une taxe sur les surfaces commerciales assise sur la surface de vente des magasins de commerce de détail, dès lors qu'elle dépasse quatre cents mètres carrés des établissements ouverts à partir du 1er janvier 1960 quelle que soit la forme juridique de l'entreprise qui les exploite. (...) / Un décret prévoira (...) des réductions pour les professions dont l'exercice requiert des superficies de vente anormalement élevées (...) ". Aux termes de l'article 3 du décret du 26 janvier 1995 relatif à la taxe sur les surfaces commerciales, pris pour l'application de ces dispositions, dans sa rédaction applicable à l'année d'imposition en litige : "  A. La réduction de taux prévue au dix-septième alinéa de l'article 3 de la loi du 13 juillet 1972 susvisée en faveur des professions dont l'exercice requiert des superficies de vente anormalement élevées est fixée à 30 p. 100 en ce qui concerne la vente exclusive des marchandises énumérées ci-après : / - meubles meublants ; / (...) - matériaux de construction (...) ". Aux termes de l'article 534 du code civil : " Les mots "meubles meublants" ne comprennent que les meubles destinés à l'usage et à l'ornement des appartements, comme tapisseries, lits, sièges, glaces, pendules, tables, porcelaines et autres objets de cette nature. / Les tableaux et les statues qui font partie du meuble d'un appartement y sont aussi compris, mais non les collections de tableaux qui peuvent être dans les galeries ou pièces particulières. / Il en est de même des porcelaines : celles seulement qui font partie de la décoration d'un appartement sont comprises sous la dénomination de "meubles meublants" ".<br/>
<br/>
              2. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances et que la question soit nouvelle ou présente un caractère sérieux. <br/>
<br/>
              3. La société Distribution Sanitaire Chauffage soutient que les dispositions du dix-septième alinéa de l'article 3 de la loi du 13 juillet 1972, en ce qu'elles s'abstiennent de préciser, d'une part, les activités pour lesquelles la superficie de vente doit être regardée comme anormalement élevée et, d'autre part, la notion d'anormalité de la superficie de vente, et en ce qu'elles confient au pouvoir réglementaire le soin de définir, outre le taux de réduction applicable, les bénéficiaires de la réduction au regard de critères indéterminés, méconnaissent la compétence du législateur prévue par l'article 34 de la Constitution dans des conditions affectant le principe d'égalité devant les charges publiques garanti par l'article 13 de la Déclaration des droits de l'homme et du citoyen de 1789.<br/>
<br/>
              4. La méconnaissance par le législateur de l'étendue de sa compétence dans la détermination de l'assiette ou du taux d'une imposition n'affecte par elle-même aucun droit ou liberté que la Constitution garantit. <br/>
<br/>
              5. Aux termes de l'article 34 de la Constitution : " La loi fixe les règles concernant : / (...) - l'assiette, le taux et les modalités de recouvrement des impositions de toutes natures... ". Les dispositions contestées instituent, en faveur des professions dont l'exercice requiert des superficies de vente anormalement élevées, une mesure de réduction de taux dont les modalités doivent être déterminées par le pouvoir réglementaire. Elles n'ont ni pour objet ni pour effet de permettre à l'administration de fixer, contribuable par contribuable, les modalités d'application de cette mesure. Ainsi, l'incompétence négative alléguée n'affecte pas par elle-même le principe d'égalité devant les charges publiques. Dès lors la question prioritaire de constitutionnalité soulevée par la société requérante, qui n'est pas nouvelle, ne présente pas un caractère sérieux. Par suite, il n'y a pas lieu de la renvoyer au Conseil constitutionnel.<br/>
<br/>
              Sur le pourvoi :<br/>
<br/>
              6. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
<br/>
              7. Pour demander l'annulation du jugement qu'elle attaque, la société Distribution Sanitaire Chauffage soutient que le tribunal administratif de Toulouse a :<br/>
              - méconnu les dispositions de l'article 3 du décret du 26 janvier 1995 et de l'article 534 du code civil et inexactement qualifié les faits de l'espèce en jugeant que certains biens qu'elle vendait ne relevaient pas de la qualification de meubles meublants, ce qui faisait obstacle à ce qu'elle bénéficie de la réduction de 30 % du taux de la taxe sur les surfaces commerciales ;<br/>
              - méconnu les principes d'égalité devant la loi et devant les charges publiques protégés par les articles 6 et 13 de la Déclaration des droits de l'homme et du citoyen de 1789 en s'abstenant de censurer l'exercice par le pouvoir réglementaire, en méconnaissance de ces principes, de la compétence qu'il tenait de la loi. <br/>
<br/>
              8. Ces moyens ne sont pas de nature à permettre l'admission du pourvoi.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la société Distribution Sanitaire Chauffage. <br/>
<br/>
Article 2 : Le pourvoi de la société Distribution Sanitaire Chauffage n'est pas admis.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la société par actions simplifiée Distribution Sanitaire Chauffage.<br/>
Copie en sera adressée au ministre de l'action et des comptes publics, au Conseil constitutionnel et au Premier ministre.  <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
