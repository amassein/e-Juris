<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041986864</ID>
<ANCIEN_ID>JG_L_2020_06_000000427630</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/98/68/CETATEXT000041986864.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 10/06/2020, 427630</TITRE>
<DATE_DEC>2020-06-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>427630</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Florian Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:427630.20200610</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'EARL Prest a demandé au tribunal administratif de Poitiers d'annuler pour excès de pouvoir les décisions des 7 septembre et 9 novembre 2018 par lesquelles le préfet de la région Nouvelle Aquitaine lui a refusé l'autorisation d'exploiter les parcelles exploitées par l'EARL La Jauleterie, dans les communes d'Assais-les-Jumeaux et de Thénézay. Par une ordonnance n° 1900105 du 24 janvier 2019, le président du tribunal administratif a, sur le fondement du premier alinéa de l'article R. 351-3 du code de justice administrative, transmis la requête de l'EARL Prest au tribunal administratif de Bordeaux.<br/>
<br/>
              Par une ordonnance n° 1900364 du 28 janvier 2019, enregistrée le 1er février 2019 au secrétariat du contentieux du Conseil d'Etat, le président du tribunal administratif de Bordeaux a, sur le fondement du second alinéa de l'article R. 351-3 du code de justice administrative, transmis le dossier au président de la section du contentieux du Conseil d'Etat.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code rural et de la pêche maritime ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 modifiée ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article R. 312-10 du code de justice administrative : " Les litiges relatifs aux législations régissant les activités professionnelles, notamment les professions libérales, les activités agricoles, commerciales et industrielles, la réglementation des prix, la réglementation du travail, ainsi que la protection ou la représentation des salariés, ceux concernant les sanctions administratives intervenues en application de ces législations relèvent, lorsque la décision attaquée n'a pas un caractère réglementaire, de la compétence du tribunal administratif dans le ressort duquel se trouve soit l'établissement ou l'exploitation dont l'activité est à l'origine du litige, soit le lieu d'exercice de la profession ".<br/>
<br/>
              2. L'EARL Prest demande l'annulation de la décision par laquelle le préfet de la région Nouvelle Aquitaine lui a refusé l'autorisation d'exploiter plusieurs parcelles en application des articles L. 331-1 à L. 331-11 du code rural et de la pêche maritime. Un tel litige est relatif à une législation régissant les activités agricoles, au sens des dispositions citées ci-dessus de l'article R. 312-10 du code de justice administrative. Par suite, le tribunal administratif territorialement compétent pour connaître de la requête est celui dans le ressort duquel se trouvent les parcelles faisant l'objet de la demande d'autorisation d'exploitation, lesquelles doivent être regardées comme constituant le lieu d'exercice de la profession au sens de ces mêmes dispositions. Il y a lieu, par suite, d'attribuer le jugement de la requête au tribunal administratif de Poitiers.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement des conclusions de la requête de l'EARL Prest est attribué au tribunal administratif de Poitiers.<br/>
Article 2 : La présente décision sera notifiée à l'EARL Prest, au président du tribunal administratif de Poitiers et au président du tribunal administratif de Bordeaux.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">03-03-03-01-06 AGRICULTURE ET FORÊTS. EXPLOITATIONS AGRICOLES. CUMULS ET CONTRÔLE DES STRUCTURES. CUMULS D'EXPLOITATIONS. CONTENTIEUX. - REFUS D'AUTORISER L'EXPLOITATION DE PLUSIEURS PARCELLES (L. 331-1 ET S. DU CRPM) - COMPÉTENCE DU TA DANS LE RESSORT DUQUEL SE TROUVENT LES PARCELLES.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">17-05-01-02 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE EN PREMIER RESSORT DES TRIBUNAUX ADMINISTRATIFS. COMPÉTENCE TERRITORIALE. - REFUS D'AUTORISER L'EXPLOITATION DE PLUSIEURS PARCELLES (L. 331-1 ET S. DU CRPM) - LITIGE RELATIF À UNE LÉGISLATION RÉGISSANT LES ACTIVITÉS AGRICOLES (ART. R. 312-10 DU CJA).
</SCT>
<ANA ID="9A"> 03-03-03-01-06 Recours contre le refus d'autorisation d'exploiter plusieurs parcelles en application des articles L. 331-1 à L. 331-11 du code rural et de la pêche maritime (CRPM).... ,,Un tel litige est relatif à une législation régissant les activités agricoles, au sens de l'article R. 312-10 du code de justice administrative (CJA). Par suite, le tribunal administratif (TA) territorialement compétent pour connaître de la requête est celui dans le ressort duquel se trouvent les parcelles faisant l'objet de la demande d'autorisation d'exploitation, lesquelles doivent être regardées comme constituant le lieu d'exercice de la profession au sens de ces mêmes dispositions.</ANA>
<ANA ID="9B"> 17-05-01-02 Recours contre le refus d'autorisation d'exploiter plusieurs parcelles en application des articles L. 331-1 à L. 331-11 du code rural et de la pêche maritime (CRPM).... ,,Un tel litige est relatif à une législation régissant les activités agricoles, au sens de l'article R. 312-10 du code de justice administrative (CJA). Par suite, le tribunal administratif (TA) territorialement compétent pour connaître de la requête est celui dans le ressort duquel se trouvent les parcelles faisant l'objet de la demande d'autorisation d'exploitation, lesquelles doivent être regardées comme constituant le lieu d'exercice de la profession au sens de ces mêmes dispositions.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
