<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037783341</ID>
<ANCIEN_ID>JG_L_2018_12_000000412262</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/78/33/CETATEXT000037783341.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 4ème chambres réunies, 07/12/2018, 412262</TITRE>
<DATE_DEC>2018-12-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412262</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR>M. Thibaut Félix</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:412262.20181207</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 7 et 28 juillet 2017 et le 20 juin 2018 au secrétariat du contentieux du Conseil d'Etat, la société Lafonta santé demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2017-809 du 5 mai 2017 relatif aux dispositifs médicaux remboursables utilisés dans le cadre de certains traitements d'affections chroniques ;<br/>
<br/>
              2°) de mettre à la charge de l'État la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et son premier protocole additionnel ;<br/>
              - la directive (UE) 2015/1535 du Parlement européen et du Conseil du 9 septembre 2015 ;<br/>
              - le code de la sécurité sociale ;<br/>
              - la loi n° 2016-1827 du 23 décembre 2016 ;<br/>
              - la décision du 20 octobre 2017 par laquelle le Conseil d'Etat, statuant au contentieux n'a pas renvoyé au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la société Lafonta santé ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thibaut Félix, auditeur,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de la société Lafonta santé.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 165-1-3 inséré dans le code de la sécurité sociale par la loi du 23 décembre 2016 de financement de la sécurité sociale pour 2017 : " Dans le cadre de la mise en oeuvre de certains traitements d'affections chroniques, dont la liste est fixée par arrêté des ministres chargés de la santé et de la sécurité sociale après avis de la Haute Autorité de santé, les prestataires mentionnés à l'article L. 5232-3 du code de la santé publique peuvent recueillir, avec l'accord du patient, les données issues d'un dispositif médical inscrit sur la liste prévue à l'article L. 165-1 du présent code qu'ils ont mis à la disposition du patient et qui est nécessaire à son traitement. Pour l'application du présent article, le recueil des données s'entend des seules données résultant de l'utilisation par le patient du dispositif médical concerné. / Ces données peuvent, avec l'accord du patient, être télétransmises au médecin prescripteur, au prestataire et au service du contrôle médical mentionné à l'article L. 315-1. Au regard de ces données, le prestataire peut conduire, en lien avec le prescripteur qui réévalue, le cas échéant, sa prescription, des actions ayant pour objet de favoriser une bonne utilisation du dispositif médical inscrit ainsi que ses prestations de services et d'adaptation associées, sur la liste mentionnée à l'article L. 165-1. / (...) / Les tarifs de responsabilité ou les prix mentionnés, respectivement, aux articles L. 165-2 et L. 165-3 peuvent être modulés, sans préjudice des autres critères d'appréciation prévus aux mêmes articles L. 165-2 et L. 165-3, en fonction de certaines données collectées, notamment celles relatives aux modalités d'utilisation du dispositif médical mis à disposition. (...) Cette modulation du tarif de responsabilité ou du prix des produits et prestations mentionnés audit article L. 165-1 ne peut avoir d'incidence sur la qualité de la prise en charge du patient par les prestataires. Une moindre utilisation du dispositif médical ne peut en aucun cas conduire à une augmentation de la participation de l'assuré mentionnée au I de l'article L. 160-13 aux frais afférents à ce dispositif et à ses prestations associées. / Les modalités d'application du présent article sont définies par décret en Conseil d'Etat ".<br/>
<br/>
              2. Par un décret du 5 mai 2017, dont la société Lafonta santé demande l'annulation pour excès de pouvoir, le Premier ministre a, pour l'application de ces dispositions, inséré des articles R. 165-75 à R. 165-77 au sein de la partie réglementaire du code de la sécurité sociale afin de préciser les conditions, d'une part, du recueil et de la transmission des données issues des dispositifs médicaux utilisés dans des traitements d'affections chroniques et, d'autre part, de la modulation des tarifs de responsabilité ou des prix de ces dispositifs médicaux en fonction des données collectées.<br/>
<br/>
              Sur l'intervention des sociétés Souffle 4, Prestat'Air, FB Consult, Pandorma et Medical Plus France :<br/>
<br/>
              3. Les sociétés Souffle 4, Prestat'Air, FB Consult, Pandorma et Medical Plus France, prestataires de service mettant à la disposition des patients des dispositifs médicaux utilisés dans le cadre de traitements d'affections chroniques, justifient d'un intérêt suffisant à l'annulation du décret attaqué. Ainsi, leur intervention au soutien de la requête de la société Lafonta santé est recevable.<br/>
<br/>
              Sur la consultation du Conseil d'Etat :<br/>
<br/>
              4. Lorsqu'un décret doit être pris en Conseil d'Etat, le texte retenu par le Gouvernement ne peut être différent à la fois du projet qu'il avait soumis au Conseil d'Etat et du texte adopté par ce dernier. En l'espèce, il ressort des pièces produites par le ministre des solidarités et de la santé que le décret attaqué ne contient pas de disposition qui différerait à la fois de celles qui figuraient dans le projet soumis par le Gouvernement au Conseil d'Etat et de celles qui ont été adoptées par la section sociale de ce dernier. Par suite, la société requérante n'est pas fondée à soutenir que les règles qui gouvernent l'examen par le Conseil d'Etat des projets de décret ont été méconnues.<br/>
<br/>
              Sur le respect de la directive du 9 septembre 2015 prévoyant une procédure d'information dans le domaine des réglementations techniques et des règles relatives aux services de la société de l'information :<br/>
<br/>
              5. Il résulte des articles 1er et 5 de la directive (UE) 2015/1535 du Parlement européen et du Conseil du 9 septembre 2015 prévoyant une procédure d'information dans le domaine des réglementations techniques et des règles relatives aux services de la société de l'information que l'obligation de communication à la Commission européenne des projets de règles techniques qu'elle prévoit s'applique, s'agissant des services, aux règles relatives aux seuls services de la société de l'information, " c'est-à-dire [à] tout service presté normalement contre rémunération, à distance, par voie électronique et à la demande individuelle d'un destinataire de services ". <br/>
<br/>
              6. L'article L. 165-1-3 du code de la sécurité sociale et le décret attaqué prévoient le recueil et la télétransmission au médecin prescripteur, au prestataire et au service du contrôle médical, de données résultant de l'utilisation par le patient d'un dispositif médical utile au traitement de certaines affections chroniques, dans le but, notamment, de permettre au prestataire de conduire des actions ayant pour objet de favoriser une bonne utilisation du dispositif et de moduler son tarif de responsabilité ou son prix en fonction du niveau d'utilisation constatée. Ces dispositions s'appliquent à des dispositifs médicaux que des prestataires sont chargés de mettre à la disposition des patients, en veillant à leur adaptation et à leur bonne utilisation, conformément à la prescription médicale. Ainsi, le recueil et la télétransmission des données d'utilisation font partie intégrante d'un service global rendu par le prestataire au patient, dont l'élément principal est la mise à disposition et la bonne utilisation du dispositif médical en cause. Un tel service ne peut être qualifié de service de la société de l'information. Par suite, les articles L. 165-1-3 et R. 165-75 à R. 165-77 du code de la sécurité sociale ne comportent pas de règles techniques au sens de la directive et la société requérante n'est pas fondée à soutenir que la loi du 23 décembre 2016 et le décret attaqué auraient dû faire l'objet, en vertu de ce texte, d'une communication préalable à la Commission européenne.<br/>
<br/>
              Sur le respect du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales :<br/>
<br/>
              7. Aux termes de l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " Toute personne physique ou morale a droit au respect de ses biens. (...) / Les dispositions précédentes ne portent pas atteinte au droit que possèdent les États de mettre en vigueur les lois qu'ils jugent nécessaires pour réglementer l'usage des biens conformément à l'intérêt général (...) ". Si les dispositions du quatrième alinéa de l'article L. 165-1-3 et du III de l'article R. 165-75 du code de la sécurité sociale, qui permettent la modulation des tarifs de responsabilité ou des prix des dispositifs médicaux en fonction notamment de leur utilisation par le patient, sont susceptibles d'entraîner une diminution de la rémunération des prestataires, elles répondent aux objectifs de protection de la santé publique, par l'incitation des prestataires à conduire des actions destinées à favoriser une bonne utilisation des dispositifs médicaux concernés, et d'équilibre financier de la sécurité sociale. Elles ne peuvent, en tout état de cause, être regardées comme portant une atteinte disproportionnée au droit au respect des biens. <br/>
<br/>
              Sur les autres moyens :<br/>
<br/>
              8. En premier lieu, aux termes du I de l'article L. 165-2 du code de la sécurité sociale, applicable au remboursement par l'assurance maladie des dispositifs médicaux à usage individuel et aux prestations de services et d'adaptation associées : " Les tarifs de responsabilité de chacun des produits ou prestations mentionnés à l'article L. 165-1 inscrits sous forme de nom de marque ou de nom commercial sont établis par convention entre le fabricant ou le distributeur du produit ou de la prestation concerné et le Comité économique des produits de santé dans les mêmes conditions que les conventions visées à l'article L. 162-17-4 ou, à défaut, par décision du Comité économique des produits de santé ". L'article L. 165-3 du même code prévoit, de même, que le comité économique des produits de santé peut fixer par convention ou, à défaut, par décision unilatérale les prix de ces produits et prestations.<br/>
<br/>
              9. Les dispositions de l'article L. 165-1-3 du code de la sécurité sociale ne faisaient pas obligation au décret en Conseil d'Etat auquel elles renvoyaient leurs modalités d'application de définir lui-même l'amplitude de la modulation du tarif de responsabilité ou du prix des produits et prestations qu'elles prévoient. Par suite, le décret attaqué, dont les dispositions sont suffisamment claires et précises, pouvait légalement s'abstenir d'encadrer la fixation, par le comité économique des produits de santé, auquel il appartient d'exercer sa compétence dans le respect du principe d'égalité et en maintenant un rapport raisonnable de proportionnalité avec les objectifs poursuivis, du montant des décotes susceptibles d'être appliquées aux tarifs de responsabilité ou aux prix de référence en fonction du niveau d'utilisation constatée du dispositif médical. La société requérante n'est ainsi pas fondée à soutenir que le pouvoir réglementaire aurait méconnu, en raison de l'importance des décotes susceptibles d'être fixées, l'article L. 165-1-3 du code de la sécurité sociale, le principe d'égalité devant les charges publiques et la liberté du commerce et de l'industrie.<br/>
<br/>
              10. En deuxième lieu, il résulte des termes de l'article L. 165-1-3 du code de la sécurité sociale que celui-ci s'applique dans le cadre de la mise en oeuvre de certains traitements d'affections chroniques, dont la liste est fixée par arrêté interministériel et que les tarifs de responsabilité ou les prix des produits et prestations peuvent être modulés en fonction de certaines données collectées, notamment des données relatives aux modalités d'utilisation du dispositif médical mis à la disposition de l'assuré, sans que cette modulation ne puisse conduire à une augmentation de la participation de ce dernier aux frais afférents au dispositif et à ses prestations associées. Par suite, il ne peut être utilement soutenu que le décret attaqué, qui ne modifie pas le champ d'application de ces dispositions, méconnaîtrait le principe d'égalité au motif que la modulation du tarif de responsabilité ou du prix dont il précise les conditions n'est susceptible de s'appliquer qu'aux prestations liées à certaines affections. De même, le décret ne peut être utilement critiqué au motif que la modulation ainsi prévue pèse sur les seuls prestataires et non sur les prescripteurs ou sur les patients. <br/>
<br/>
              11. En troisième lieu, le pouvoir réglementaire n'a pas commis d'erreur manifeste d'appréciation en prévoyant, pour l'application de l'article L. 165-1-3, d'une part, que le prestataire transmet les données relatives à la durée ou à la fréquence d'utilisation du dispositif au service du contrôle médical de façon mensuelle ou trimestrielle et au médecin prescripteur, à la demande de ce dernier ou lorsqu'il constate un niveau d'utilisation faible ou insuffisant ou un changement important dans ce niveau, et qu'il l'informe des difficultés d'utilisation exprimées éventuellement par le patient, et, d'autre part, qu'il conduit, dans la limite de ses compétences et en lien avec le prescripteur, des actions ayant pour objet de favoriser une bonne utilisation du dispositif médical, notamment en vérifiant ou en adaptant l'appareillage mis à la disposition du patient lorsque ce dernier fait part de difficultés matérielles d'utilisation. <br/>
<br/>
              12. En dernier lieu, si les sociétés intervenantes soutiennent que le pouvoir réglementaire aurait cherché à favoriser la concentration au sein du secteur des prestataires de santé à domicile, il ne ressort pas des pièces du dossier que le décret attaqué aurait été pris dans un but autre que la mise en oeuvre des dispositions de l'article L. 165-1-3 du code de la sécurité sociale.<br/>
<br/>
              Sur les conclusions relatives aux frais d'instance :<br/>
<br/>
              13. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'État, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'intervention des sociétés Souffle 4, Prestat'Air, FB Consult, Pandorma et Medical Plus France est admise.<br/>
Article 2 : La requête de la société Lafonta santé est rejetée.<br/>
Article 3 : La présente décision sera notifiée à la société Lafonta santé, à la société Souffle 4, première intervenante dénommée, au Premier ministre et à la ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. - OBLIGATION DE COMMUNICATION À LA COMMISSION EUROPÉENNE DES PROJETS DE RÈGLES RELATIVES AUX SERVICES DE LA SOCIÉTÉ DE L'INFORMATION (1 DE L'ART. 5 DE LA DIRECTIVE DU 9 SEPTEMBRE 2015) - RECUEIL ET TÉLÉTRANSMISSION DE DONNÉES RÉSULTANT DE L'UTILISATION PAR LE PATIENT D'UN DISPOSITIF MÉDICAL UTILE AU TRAITEMENT DE CERTAINES AFFECTIONS CHRONIQUES (ART. L. 165-1-3 ET R. 165- 75 À R. 165-77 DU CSS) - RECUEIL ET TÉLÉTRANSMISSION FAISANT PARTIE INTÉGRANTE D'UN SERVICE GLOBAL, DONT L'ÉLÉMENT PRINCIPAL EST LA MISE À DISPOSITION ET LA BONNE UTILISATION DU DISPOSITIF MÉDICAL - EXISTENCE - CONSÉQUENCES - SERVICE DE LA SOCIÉTÉ DE L'INFORMATION (1 DE L'ART. 1ER DE LA DIRECTIVE) - EXCLUSION [RJ1] - COMMUNICATION NÉCESSAIRE À LA COMMISSION DES PROJETS DE TEXTES PRÉVOYANT CE RECUEIL ET CETTE TÉLÉTRANSMISSION - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">15-05-01-04 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. RÈGLES APPLICABLES. LIBERTÉS DE CIRCULATION. LIBRE PRESTATION DE SERVICES. - OBLIGATION DE COMMUNICATION À LA COMMISSION EUROPÉENNE DES PROJETS DE RÈGLES RELATIVES AUX SERVICES DE LA SOCIÉTÉ DE L'INFORMATION (1 DE L'ART. 5 DE LA DIRECTIVE DU 9 SEPTEMBRE 2015) - RECUEIL ET TÉLÉTRANSMISSION DE DONNÉES RÉSULTANT DE L'UTILISATION PAR LE PATIENT D'UN DISPOSITIF MÉDICAL UTILE AU TRAITEMENT DE CERTAINES AFFECTIONS CHRONIQUES (ART. L. 165-1-3 ET R. 165- 75 À R. 165-77 DU CSS) - RECUEIL ET TÉLÉTRANSMISSION FAISANT PARTIE INTÉGRANTE D'UN SERVICE GLOBAL, DONT L'ÉLÉMENT PRINCIPAL EST LA MISE À DISPOSITION ET LA BONNE UTILISATION DU DISPOSITIF MÉDICAL - EXISTENCE - CONSÉQUENCES - SERVICE DE LA SOCIÉTÉ DE L'INFORMATION (1 DE L'ART. 1ER DE LA DIRECTIVE) - EXCLUSION [RJ1] - COMMUNICATION NÉCESSAIRE À LA COMMISSION DES PROJETS DE TEXTES PRÉVOYANT CE RECUEIL ET CETTE TÉLÉTRANSMISSION - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">61-04-01-05 SANTÉ PUBLIQUE. PHARMACIE. PRODUITS PHARMACEUTIQUES. DISPOSITIFS MÉDICAUX. - RECUEIL ET TÉLÉTRANSMISSION DE DONNÉES RÉSULTANT DE L'UTILISATION PAR LE PATIENT D'UN DISPOSITIF MÉDICAL UTILE AU TRAITEMENT DE CERTAINES AFFECTIONS CHRONIQUES (ART. L. 165-1-3 ET R. 165- 75 À R. 165-77 DU CSS) - RECUEIL ET TÉLÉTRANSMISSION FAISANT PARTIE INTÉGRANTE D'UN SERVICE GLOBAL, DONT L'ÉLÉMENT PRINCIPAL EST LA MISE À DISPOSITION ET LA BONNE UTILISATION DU DISPOSITIF MÉDICAL - EXISTENCE - CONSÉQUENCES - SERVICE DE LA SOCIÉTÉ DE L'INFORMATION (1 DE L'ART. 1ER DE LA DIRECTIVE DU 9 SEPTEMBRE 2015) - EXCLUSION [RJ1] - COMMUNICATION NÉCESSAIRE À LA COMMISSION EUROPÉENNE DES PROJETS DE TEXTES PRÉVOYANT CE RECUEIL ET CETTE TÉLÉTRANSMISSION - ABSENCE.
</SCT>
<ANA ID="9A"> 01-03 L'article L. 165-1-3 du code de la sécurité sociale (CSS) et le décret n° 2017-809 du 5 mai 2017 prévoient le recueil et la télétransmission, au médecin prescripteur, au prestataire et au service du contrôle médical, de données résultant de l'utilisation par le patient d'un dispositif médical utile au traitement de certaines affections chroniques, dans le but, notamment, de permettre au prestataire de conduire des actions ayant pour objet de favoriser une bonne utilisation du dispositif et de moduler son tarif de responsabilité ou son prix en fonction du niveau d'utilisation constatée. Ces dispositions s'appliquent à des dispositifs médicaux que des prestataires sont chargés de mettre à la disposition des patients, en veillant à leur adaptation et à leur bonne utilisation, conformément à la prescription médicale. Ainsi, le recueil et la télétransmission des données d'utilisation font partie intégrante d'un service global rendu par le prestataire au patient, dont l'élément principal est la mise à disposition et la bonne utilisation du dispositif médical en cause. Un tel service ne peut être qualifié de service de la société d'information. Par suite, les articles L. 165-1-3 et R. 165-75 à R. 165-77 du CSS, créés par ce décret, ne comportent pas de règles techniques au sens de la directive (UE) 2015/1535 du Parlement européen et du Conseil du 9 septembre 2015 et la loi n° 2016-1827 du 23 décembre 2016 et ce décret n'avait pas à faire l'objet, en vertu de ce texte, d'une communication préalable à la Commission européenne.</ANA>
<ANA ID="9B"> 15-05-01-04 L'article L. 165-1-3 du code de la sécurité sociale (CSS) et le décret n° 2017-809 du 5 mai 2017 prévoient le recueil et la télétransmission, au médecin prescripteur, au prestataire et au service du contrôle médical, de données résultant de l'utilisation par le patient d'un dispositif médical utile au traitement de certaines affections chroniques, dans le but, notamment, de permettre au prestataire de conduire des actions ayant pour objet de favoriser une bonne utilisation du dispositif et de moduler son tarif de responsabilité ou son prix en fonction du niveau d'utilisation constatée. Ces dispositions s'appliquent à des dispositifs médicaux que des prestataires sont chargés de mettre à la disposition des patients, en veillant à leur adaptation et à leur bonne utilisation, conformément à la prescription médicale. Ainsi, le recueil et la télétransmission des données d'utilisation font partie intégrante d'un service global rendu par le prestataire au patient, dont l'élément principal est la mise à disposition et la bonne utilisation du dispositif médical en cause. Un tel service ne peut être qualifié de service de la société d'information. Par suite, les articles L. 165-1-3 et R. 165-75 à R. 165-77 du CSS, créés par ce décret, ne comportent pas de règles techniques au sens de la directive (UE) 2015/1535 du Parlement européen et du Conseil du 9 septembre 2015 et la loi n° 2016-1827 du 23 décembre 2016 et ce décret n'avait pas à faire l'objet, en vertu de ce texte, d'une communication préalable à la Commission européenne.</ANA>
<ANA ID="9C"> 61-04-01-05 L'article L. 165-1-3 du code de la sécurité sociale (CSS) et le décret n° 2017-809 du 5 mai 2017 prévoient le recueil et la télétransmission, au médecin prescripteur, au prestataire et au service du contrôle médical, de données résultant de l'utilisation par le patient d'un dispositif médical utile au traitement de certaines affections chroniques, dans le but, notamment, de permettre au prestataire de conduire des actions ayant pour objet de favoriser une bonne utilisation du dispositif et de moduler son tarif de responsabilité ou son prix en fonction du niveau d'utilisation constatée. Ces dispositions s'appliquent à des dispositifs médicaux que des prestataires sont chargés de mettre à la disposition des patients, en veillant à leur adaptation et à leur bonne utilisation, conformément à la prescription médicale. Ainsi, le recueil et la télétransmission des données d'utilisation font partie intégrante d'un service global rendu par le prestataire au patient, dont l'élément principal est la mise à disposition et la bonne utilisation du dispositif médical en cause. Un tel service ne peut être qualifié de service de la société d'information. Par suite, les articles L. 165-1-3 et R. 165-75 à R. 165-77 du CSS, créés par ce décret, ne comportent pas de règles techniques au sens de la directive (UE) 2015/1535 du Parlement européen et du Conseil du 9 septembre 2015 et la loi n° 2016-1827 du 23 décembre 2016 et ce décret n'avait pas à faire l'objet, en vertu de ce texte, d'une communication préalable à la Commission européenne.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. CJUE, 20 décembre 2017, Asociación Profesional Elite Taxi c/ Uber Systems Spain SL, aff. C-434/15 ; CJUE, 10 avril 2018, Uber France SAS, aff. C-320/16.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
