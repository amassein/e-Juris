<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042006682</ID>
<ANCIEN_ID>JG_L_2020_06_000000440686</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/00/66/CETATEXT000042006682.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 04/06/2020, 440686, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-06-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440686</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP ZRIBI, TEXIER</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:440686.20200604</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au juge des référés du tribunal administratif de Cergy-Pontoise, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre au département des Hauts-de-Seine de procéder à son hébergement dans une structure agréée au titre de la protection de l'enfance adaptée à son âge et à la prévention des risques de propagation du covid-19 et de prendre en charge ses besoins alimentaires, sanitaires et médicaux quotidiens, dans un délai de 24 heures à compter de la décision à intervenir, sous astreinte de 100 euros par jour de retard. Par une ordonnance n° 2004245 du 5 mai 2020, le juge des référés du tribunal administratif de Cergy-Pontoise a enjoint au département de prendre en charge l'hébergement de M. B... dans une structure agréée, adaptée à la prévention des risques de propagation du covid-19 et d'assurer ses besoins alimentaires, sanitaires et médicaux jusqu'à ce que l'autorité judiciaire se prononce sur la question relative à sa minorité, dans un délai de quarante-huit heures à compter de la notification de l'ordonnance. <br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 18 et 25 mai 2020 au secrétariat du contentieux du Conseil d'Etat, le département des Hauts-de-Seine demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de rejeter la demande présentée par M. B... devant le tribunal administratif de Cergy-Pontoise. <br/>
<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - l'ordonnance attaquée est entachée d'insuffisance de motivation, faute de réponse au moyen tiré de ce que les dispositions de l'article L. 222-5 du code de l'action sociale et des familles font obstacle à ce que les services d'aide sociale à l'enfance prennent en charge un mineur en l'absence d'une mesure de placement prise par l'autorité judiciaire et en dehors de la prise en charge provisoire prévue à l'article L. 223-2 du même code ;<br/>
              - le juge des référés a méconnu son office en ne procédant pas à l'examen de la validité des documents d'état civil produits par M. B... et en se bornant à juger qu'ils bénéficiaient d'une présomption de validité ;<br/>
              - l'ordonnance attaquée est entachée d'une erreur de droit, ou, à tout le moins, est fondé sur des faits matériellement inexacts, en ce qu'elle considère que l'acte de naissance et le jugement supplétif produits par M. B..., étaient, en dépit des soupçons relatifs à leur authenticité, présumés valides en application du premier alinéa de l'article L. 111-6 du code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le juge des référés a entaché son ordonnance d'une erreur de droit en estimant que la carence du département dans l'accomplissement de sa mission définie à l'article L. 222-1 du code de l'action sociale et des familles porterait une atteinte grave et manifestement illégale à une liberté fondamentale, dès lors qu'en vertu de article L. 221-5 de ce code et du 3° de l'article 375-3 du code civil, en dehors de la période de prise en charge provisoire prévue par l'article L. 223-2 du code de l'action sociale et des familles, les services de l'aide sociale à l'enfance ne peuvent prendre en charge que les mineurs qui leur ont été confiés par l'autorité judiciaire.<br/>
              Par deux mémoires en défense, enregistrés les 22 et 26 mai 2020, M. B... conclut au rejet de la requête et à ce que soit mis à la charge du département des Hauts-de-Seine la somme de 3 000 euros en application des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il soutient que les moyens soulevés par le département des Hauts-de-Seine ne sont pas fondés et qu'il résulte des dispositions du 3° de l'article L. 221-1 du code de l'action sociale et des familles qu'il incombe au département, en cas de nécessité, de prendre en charge tout mineur isolé, alors même qu'un refus de prise en charge au titre de l'aide sociale à l'enfance lui a été opposé, jusqu'à l'intervention de la décision du juge des enfants.<br/>
<br/>
              Par un mémoire en intervention, enregistré le 26 mai 2020, le Groupe d'information et de soutien des immigré-e-s (GISTI) et l'association Informations sur les mineurs étrangers isolés (InfoMIE) demandent au juge des référés du Conseil d'Etat de déclarer leur intervention recevable et de faire droit aux conclusions de M. B... tendant au rejet de la requête du département des Hauts-de-Seine. Ils soutiennent que les moyens soulevés par le département des Hauts-de-Seine ne sont pas fondés et qu'il résulte des dispositions du 3° de l'article L. 221-1 du code de l'action sociale et des familles qu'il incombe au département, en cas de nécessité, de prendre en charge tout mineur isolé, alors même qu'un refus de prise en charge au titre de l'aide sociale à l'enfance lui a été opposé, jusqu'à l'intervention de la décision du juge des enfants.<br/>
              La requête a été communiquée au ministre des solidarités et de la santé, qui n'a pas produit d'observations.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, et notamment son Préambule ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la convention internationale relative aux droits de l'enfant ;<br/>
              - le code civil ;<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - la loi n° 2020-290 du 23 mars 2020 ;<br/>
              - la loi n° 2020-546 du 11 mai 2020 ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-303 du 25 mars 2020 modifiée ;<br/>
<br/>
              Les parties ont été informées, sur le fondement de l'article 9 de l'ordonnance du 25 mars 2020 portant adaptation des règles applicables devant les juridictions de l'ordre administratif, de ce qu'aucune audience ne se tiendrait et de ce que la clôture de l'instruction serait fixée le 28 mai 2020 à 13 heures.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale (...). "<br/>
<br/>
              Sur l'intervention du Groupe d'information et de soutien des immigré-e-s (GISTI) et de l'association Informations sur les mineurs étrangers isolés (InfoMIE) :<br/>
<br/>
              2. Le GISTI et l'association InfoMIE justifient d'un intérêt suffisant pour intervenir au soutien des conclusions de M. B... tendant au rejet de la requête du département des Hauts-de-Seine. Leur intervention doit, par suite, être admise.<br/>
<br/>
              Sur la demande en référé :<br/>
              3. Il résulte de l'instruction que M. B..., originaire du Mali, s'est présenté le 5 novembre 2019 à la préfecture des Hauts-de-Seine en produisant des documents faisant apparaître qu'il serait né le 18 mars 2004 à Bamako. Il a été recueilli, à titre provisoire, par le service d'aide sociale à l'enfance du département des Hauts-de-Seine par une décision du même jour. Le 8 novembre 2019, la cellule des mineurs non accompagnés du département a estimé que les éléments collectés lors de son évaluation mettaient en cause la minorité alléguée de M. B.... Le président du conseil départemental a alors saisi l'autorité judiciaire afin qu'il soit procédé à un examen radiologique osseux de l'intéressé et a transmis le dossier au service du ministère de l'intérieur en charge de la lutte contre la fraude documentaire. Le département des Hauts-de-Seine a poursuivi l'accueil provisoire de M. B... jusqu'au 8 décembre 2019, puis jusqu'au 8 février 2020, dans l'attente de l'analyse de ses documents d'identité. La division de l'expertise en fraude documentaire et à l'identité du ministère de l'intérieur ayant relevé, le 19 décembre 2019, que les documents produits n'étaient pas recevables en raison des omissions, des inexactitudes et des incohérences les affectant, le département a refusé, par une décision du 7 février 2020, la prise en charge de M. B... par le service de l'aide sociale à l'enfance, au motif que sa minorité, son isolement et sa vulnérabilité n'étaient pas caractérisés. Le 29 février, M. B... a saisi le juge des enfants du tribunal judiciaire de Nanterre afin de solliciter une mesure d'assistance éducative, sur le fondement de l'article 375 du code civil. Le 23 avril 2020, il a saisi le juge des enfants du tribunal judiciaire de Bobigny afin que soit prononcée une ordonnance de placement provisoire dans un centre d'accueil dépendant du département, sur le fondement de l'article 375-5 du même code. M. B... a enfin saisi le juge des référés du tribunal de Cergy-Pontoise, le 27 avril 2020, sur le fondement de l'article L. 521-2 du code de justice administrative, d'une demande tendant à ce qu'il soit enjoint au département des Hauts-de-Seine de procéder à son hébergement dans une structure agréée au titre de la protection de l'enfance adaptée à son âge et à la prévention des risques de propagation du covid-19 et de prendre en charge ses besoins alimentaires, sanitaires et médicaux quotidiens, dans un délai de 24 heures à compter de la décision à intervenir, sous astreinte de 100 euros par jour de retard. Par une ordonnance du 5 mai 2020, dont le département des Hauts-de-Seine relève appel, le juge des référés du tribunal administratif de Cergy-Pontoise a enjoint au département d'assurer cette prise en charge jusqu'à ce que l'autorité judiciaire se prononce sur la question relative à la minorité de M. B..., dans un délai de quarante-huit heures à compter de la notification de l'ordonnance.<br/>
              4. L'article 375 du code civil dispose que : " Si la santé, la sécurité ou la moralité d'un mineur non émancipé sont en danger, ou si les conditions de son éducation ou de son développement physique, affectif, intellectuel et social sont gravement compromises, des mesures d'assistance éducative peuvent être ordonnées par justice à la requête des père et mère conjointement, ou de l'un d'eux, de la personne ou du service à qui l'enfant a été confié ou du tuteur, du mineur lui-même ou du ministère public (...) ". Aux termes de l'article 375-3 du même code : " Si la protection de l'enfant l'exige, le juge des enfants peut décider de le confier : / (...) 3° A un service départemental de l'aide sociale à l'enfance (...) ". Aux termes des deux premiers alinéas de l'article 373-5 du même code : " A titre provisoire mais à charge d'appel, le juge peut, pendant l'instance, soit ordonner la remise provisoire du mineur à un centre d'accueil ou d'observation, soit prendre l'une des mesures prévues aux articles 375-3 et 375-4. / En cas d'urgence, le procureur de la République du lieu où le mineur a été trouvé a le même pouvoir, à charge de saisir dans les huit jours le juge compétent, qui maintiendra, modifiera ou rapportera la mesure. Si la situation de l'enfant le permet, le procureur de la République fixe la nature et la fréquence du droit de correspondance, de visite et d'hébergement des parents, sauf à les réserver si l'intérêt de l'enfant l'exige. " <br/>
<br/>
              5. L'article L. 221-1 du code de l'action sociale et des familles dispose que : " Le service de l'aide sociale à l'enfance est un service non personnalisé du département chargé des missions suivantes : 1° Apporter un soutien matériel, éducatif et psychologique tant aux mineurs et à leur famille ou à tout détenteur de l'autorité parentale, confrontés à des difficultés risquant de mettre en danger la santé, la sécurité, la moralité de ces mineurs ou de compromettre gravement leur éducation ou leur développement physique, affectif, intellectuel et social, qu'aux mineurs émancipés et majeurs de moins de vingt et un ans confrontés à des difficultés familiales, sociales et éducatives susceptibles de compromettre gravement leur équilibre (...) / ; 3° Mener en urgence des actions de protection en faveur des mineurs mentionnés au 1° du présent article ; / 4° Pourvoir à l'ensemble des besoins des mineurs confiés au service et veiller à leur orientation (...) ". L'article L. 222-5 du même code prévoit que : " Sont pris en charge par le service de l'aide sociale à l'enfance sur décision du président du conseil départemental : (...) / 3° Les mineurs confiés au service en application du 3° de l'article 375-3 du code civil (...) ". L'article L. 223-2 de ce code dispose que : " Sauf si un enfant est confié au service par décision judiciaire ou s'il s'agit de prestations en espèces, aucune décision sur le principe ou les modalités de l'admission dans le service de l'aide sociale à l'enfance ne peut être prise sans l'accord écrit des représentants légaux ou du représentant légal du mineur ou du bénéficiaire lui-même s'il est mineur émancipé. / En cas d'urgence et lorsque le représentant légal du mineur est dans l'impossibilité de donner son accord, l'enfant est recueilli provisoirement par le service qui en avise immédiatement le procureur de la République. / (...) Si, dans le cas prévu au deuxième alinéa du présent article, l'enfant n'a pas pu être remis à sa famille ou le représentant légal n'a pas pu ou a refusé de donner son accord dans un délai de cinq jours, le service saisit également l'autorité judiciaire en vue de l'application de l'article 375-5 du code civil. ". L'article R. 221-11 du même code dispose que : " I. - Le président du conseil départemental du lieu où se trouve une personne se déclarant mineure et privée temporairement ou définitivement de la protection de sa famille met en place un accueil provisoire d'urgence d'une durée de cinq jours, à compter du premier jour de sa prise en charge, selon les conditions prévues aux deuxième et quatrième alinéas de l'article L. 223-2. / II. - Au cours de la période d'accueil provisoire d'urgence, le président du conseil départemental procède aux investigations nécessaires en vue d'évaluer la situation de cette personne au regard notamment de ses déclarations sur son identité, son âge, sa famille d'origine, sa nationalité et son état d'isolement. (...) / IV. - Au terme du délai mentionné au I, ou avant l'expiration de ce délai si l'évaluation a été conduite avant son terme, le président du conseil départemental saisit le procureur de la République en vertu du quatrième alinéa de l'article L. 223-2 et du second alinéa de l'article 375-5 du code civil. En ce cas, l'accueil provisoire d'urgence mentionné au I se prolonge tant que n'intervient pas une décision de l'autorité judiciaire. / S'il estime que la situation de la personne mentionnée au présent article ne justifie pas la saisine de l'autorité judiciaire, il notifie à cette personne une décision de refus de prise en charge délivrée dans les conditions des articles L. 222-5 et R. 223-2. En ce cas, l'accueil provisoire d'urgence mentionné au I prend fin ". Le même article dispose que les décisions de refus de prise en charge sont motivées et mentionnent les voies et délais de recours. <br/>
<br/>
              6. Il résulte de ces dispositions qu'il incombe aux autorités du département, le cas échéant dans les conditions prévues par la décision du juge des enfants ou par le procureur de la République ayant ordonné en urgence une mesure de placement provisoire, de prendre en charge l'hébergement et de pourvoir aux besoins des mineurs confiés au service de l'aide sociale à l'enfance. A cet égard, une obligation particulière pèse sur ces autorités lorsqu'un mineur privé de la protection de sa famille est sans abri et que sa santé, sa sécurité ou sa moralité est en danger. Lorsqu'elle entraîne des conséquences graves pour le mineur intéressé, une carence caractérisée dans l'accomplissement de cette mission porte une atteinte grave et manifestement illégale à une liberté fondamentale. Il incombe au juge des référés d'apprécier, dans chaque cas, les diligences accomplies par l'administration en tenant compte des moyens dont elle dispose ainsi que de l'âge, de l'état de santé et de la situation de famille de la personne intéressée.<br/>
<br/>
              7. Il en résulte également que, lorsqu'il est saisi par un mineur d'une demande d'admission à l'aide sociale à l'enfance, le président du conseil départemental peut seulement, au-delà de la période provisoire de cinq jours prévue par l'article L. 223-2 du code de l'action sociale et des familles, décider de saisir l'autorité judiciaire mais ne peut, en aucun cas, décider d'admettre le mineur à l'aide sociale à l'enfance sans que l'autorité judiciaire l'ait ordonné. L'article 375 du code civil autorise le mineur à solliciter lui-même le juge judiciaire pour que soient prononcées, le cas échéant, les mesures d'assistance éducative que sa situation nécessite. Lorsque le département refuse de saisir l'autorité judiciaire à l'issue de l'évaluation mentionnée au point 4, au motif que l'intéressé n'aurait pas la qualité de mineur isolé, l'existence d'une voie de recours devant le juge des enfants par laquelle le mineur peut obtenir son admission à l'aide sociale rend irrecevable le recours formé devant le juge administratif contre la décision du département.<br/>
<br/>
              8. Il appartient toutefois au juge du référé, statuant sur le fondement de l'article L. 521-2, lorsqu'il lui apparaît que l'appréciation portée par le département sur l'absence de qualité de mineur isolé de l'intéressé est manifestement erronée et que ce dernier est confronté à un risque immédiat de mise en en danger de sa santé ou de sa sécurité, d'enjoindre au département de poursuivre son accueil provisoire.<br/>
<br/>
              9. Par ailleurs, il appartient aux autorités de l'Etat de mettre en oeuvre le droit à l'hébergement d'urgence reconnu par la loi à toute personne sans abri qui se trouve en situation de détresse médicale, psychique et sociale. Une carence caractérisée dans l'accomplissement de cette tâche peut faire apparaître, pour l'application de l'article L. 521-2 du code de justice administrative, une atteinte grave et manifestement illégale à une liberté fondamentale lorsqu'elle entraîne des conséquences graves pour la personne intéressée. Il incombe au juge des référés, lorsqu'il est saisi d'un refus de prise en charge par l'Etat, d'apprécier dans chaque cas les diligences accomplies par l'administration en tenant compte des moyens dont elle dispose ainsi que de l'âge, de l'état de la santé et de la situation de famille de la personne intéressée.<br/>
              10. Ainsi qu'il a été dit au point 3, le président du conseil départemental des Hauts-de-Seine, par sa décision du 7 février 2020, a refusé de poursuivre l'accueil provisoire de M. B..., dont celui-ci a bénéficié durant un peu plus de trois mois, et de le prendre en charge au titre de l'aide sociale à l'enfance, au motif que sa minorité, son isolement et sa vulnérabilité n'étaient pas caractérisés. Pour prendre cette décision, il s'est fondé sur l'analyse, par le service compétent du ministère de l'intérieur, des documents d'état-civil produits par M. B..., analyse dont il ressortait qu'ils ne pouvaient être tenus pour authentiques, ainsi que sur les résultats de l'évaluation à laquelle il avait été procédé par ses propres services, selon lesquels tant l'apparence physique que le discours et le comportement de l'intéressée ne corroboraient pas la minorité alléguée. Il résulte de l'instruction que le juge des enfants, qui ne s'est pas encore prononcé sur la demande de M. B..., n'a pas davantage, à ce jour, ordonné l'une des mesures prévues à l'article 375-3 du code civil, notamment en le confiant provisoirement à un service d'aide sociale à l'enfance ainsi que l'article 375-5 du même code le lui permet.<br/>
<br/>
              11. Dans ces conditions, en l'état de l'instruction et à la date de la présente ordonnance, le confinement généralisé des personnes se trouvant sur le territoire français ayant pris fin, la décision du président du conseil départemental des Hauts-de-Seine ne révèle, alors même que M. B... fait valoir qu'il sollicite, non sa prise en charge au titre de l'aide sociale à l'enfance, mais sa seule mise à l'abri dans l'attente de la décision du juge des enfants saisi par ses soins, aucune atteinte grave et manifestement illégale à une liberté fondamentale. Le département des Hauts-de-Seine est ainsi fondé à soutenir que c'est à tort que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Cergy-Pontoise lui a enjoint de prendre en charge l'hébergement de M. B... dans une structure agréée et d'assurer ses besoins alimentaires, sanitaires et médicaux, dans l'attente de la décision du juge des enfants saisi par l'intéressée.<br/>
<br/>
              12. Il résulte de tout ce qui précède qu'il y a lieu d'annuler les articles 2 et 3 de l'ordonnance du juge des référés du tribunal administratif de Cergy-Pontoise, qui prononcent une injonction à l'égard du département des Hauts-de-Seine et mettent à sa charge les frais de l'instance, et de rejeter les conclusions présentées par M. B... en première instance ainsi celles tendant à l'application des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : Les interventions du Groupe d'information et de soutien des immigré-e-s et de l'association Informations sur les mineurs étrangers isolés sont admises.<br/>
Article 2 : Les articles 2 et 3 de l'ordonnance du juge des référés du tribunal administratif de Cergy-Pontoise du 5 mai 2020 sont annulés.<br/>
Article 3 : Les conclusions présentées par M. B... en première instance contre le département des Hauts-de-Seine et celles tendant à l'application des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
Article 4 : La présente ordonnance sera notifiée au département des Hauts-de-Seine, à M. A... B..., au Groupe d'information et de soutien des immigré-e-s et à l'association Informations sur les mineurs étrangers isolés.<br/>
Copie en sera adressée au ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
