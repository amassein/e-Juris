<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032103889</ID>
<ANCIEN_ID>JG_L_2016_02_000000380277</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/10/38/CETATEXT000032103889.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème SSJS, 24/02/2016, 380277, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-02-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>380277</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT ; HAAS</AVOCATS>
<RAPPORTEUR>M. Jean-Marc Anton</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2016:380277.20160224</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Compagnie de transport maritime et M. C...A...ont demandé au tribunal administratif de Basse-Terre de condamner le département de la Guadeloupe à leur verser respectivement les sommes de 11 121 411 euros et de 1 168 371 euros en réparation du préjudice qu'ils estimaient avoir subi du fait de l'intervention de l'arrêté du 10 décembre 1999 du président du conseil général du département de la Guadeloupe portant règlement particulier de police des ports départementaux de Trois-Rivières, Terre-de-Haut, Terre-de-Bas. Par un jugement n° 1000716 du 30 avril 2012, le tribunal administratif a rejeté cette demande. <br/>
<br/>
              Par un arrêt n° 12BX02011 du 11 février 2014, la cour administrative d'appel de Bordeaux a rejeté l'appel formé contre ce jugement par la société Compagnie de transport maritime et M.A....  <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 13 mai et 13 août 2014 et le 21 mai 2015 au secrétariat du contentieux du Conseil d'Etat, la société Compagnie de transport maritime, M. B...A...et M. D... A... venant aux droits de M. C... A..., leur père décédé, demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge du département de la Guadeloupe la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Anton, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Potier de la Varde, Buk Lament, avocat de la société Compagnie de Transport Maritime  et à Me Haas, avocat du département de la Guadeloupe  ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société Compagnie de transport maritime, qui faisait partie d'un groupe dirigé par M. C... A..., avait conclu le 23 novembre 1989 une convention avec le département de la Guadeloupe en vue de l'exploitation du transport de passagers sur les liaisons entre les ports de Trois-Rivières et de Terre-de-Haut dans l'archipel des Saintes ; que cette convention réservait l'exclusivité de l'accostage sur l'appontement de Trois-Rivières entre 7h30 et 9h30 aux deux armateurs signataires de la convention, la Compagnie de transport maritime et la société Princesse Caroline-Armement Lorge ; que, par un arrêté du 10 décembre 1999 portant règlement particulier de police des ports maritimes départementaux de Trois-Rivières, Terre-de-Haut et Terre-de-Bas, le président du conseil général de la Guadeloupe a fixé les créneaux horaires de départ de l'ensemble des vedettes assurant la liaison de service public minimum de desserte de ces trois ports, appartenant à quatre armateurs dont la Compagnie de transport maritime ; que cet arrêté a été annulé par un jugement du tribunal administratif de Basse-Terre confirmé par un arrêt du 6 juin 2006 de la cour administrative d'appel de Bordeaux ; que la Compagnie de transport maritime et M. A...ont demandé au tribunal administratif de Basse-Terre de leur accorder l'indemnisation du préjudice qu'ils estimaient avoir subi à la suite de l'intervention de cet arrêté ; que, par l'arrêt attaqué, la cour administrative d'appel de Bordeaux a rejeté l'appel formé par la Compagnie de transport maritime et M. A...contre le jugement du tribunal administratif de Basse-Terre rejetant cette demande ;<br/>
<br/>
              Sur les moyens relatifs à la responsabilité pour faute : <br/>
<br/>
              2. Considérant, en premier lieu, que la cour a jugé que la Compagnie de transport maritime et M. A...avaient continué à faire partir, sans tenir compte des dispositions de l'arrêté du 10 décembre 1999, plusieurs vedettes dans chacun de leurs anciens créneaux horaires et qu'en refusant de respecter cet arrêté, avant son annulation, ils s'étaient eux-mêmes placés en situation irrégulière, ce qui a conduit à leur condamnation par plusieurs décisions du juge judiciaire ; qu'en estimant que les dispositions de l'arrêté du 10 décembre 1999 étaient claires, la cour a porté sur cette pièce du dossier une appréciation exempte de dénaturation ; qu'en jugeant que la société et M. A...n'apportaient aucun élément dont il ressortirait qu'il leur aurait été reproché d'avoir procédé à l'embarquement de passagers dans plusieurs vedettes à partir du même appontement à l'intérieur du nouveau créneau horaire dont ils disposaient, et non d'avoir procédé à de telles opérations en empiétant sur la plage horaire d'autres armements, la cour n'a pas dénaturé les pièces du dossier qui lui était soumis ;<br/>
<br/>
              3. Considérant, en deuxième lieu, qu'en jugeant que la Compagnie de transport maritime était tenue d'appliquer l'arrêté du 10 décembre 1999 tant qu'il n'avait pas été annulé par le juge administratif et que les requérants n'étaient pas fondés à demander la réparation des indemnités, injonctions et astreintes prononcées à leur encontre par les juridictions judiciaires dès lors que les préjudices allégués résultaient exclusivement de la situation irrégulière dans laquelle ils s'étaient eux-mêmes placés en refusant de respecter cet arrêté avant qu'il ne soit annulé, la cour n'a ni commis d'erreur de droit, ni inexactement qualifié les faits ; <br/>
<br/>
              4. Considérant, en troisième lieu, qu'en jugeant que les requérants n'apportaient aucun élément permettant d'estimer que les préjudices relatifs à la perte de valeur du patrimoine personnel de M. C... A...et à la détérioration de son état de santé, présentaient un lien direct avec l'illégalité de l'arrêté du 10 décembre 1999, la cour a suffisamment motivé son arrêt ;<br/>
<br/>
              5. Considérant en revanche, en quatrième lieu, qu'en écartant l'existence d'un préjudice de perte d'exploitation pour la société requérante au seul motif que les requérants se seraient bornés à faire valoir les conclusions d'une expertise comptable non contradictoire et l'importance de la diminution des produits de leur exploitation entre 1997 et 2001, sans même donner d'évaluation des pertes d'exploitation en résultant, alors qu'ils avaient indiqué le montant des pertes d'exploitation alléguées dans leurs écritures devant le tribunal administratif, en renvoyant au rapport d'expertise qu'ils avaient produit, la cour a dénaturé les pièces du dossier qui lui était soumis ; <br/>
<br/>
              Sur les moyens dirigés contre l'arrêt en tant qu'il a rejeté les conclusions dirigées contre le département de la Guadeloupe sur le fondement de la responsabilité sans faute :<br/>
<br/>
              6. Considérant que la cour a jugé que les requérants n'établissaient pas l'existence d'un préjudice anormal en lien direct avec l'intervention de l'arrêté du 10 décembre 1999 ; que, pour estimer que les préjudices allégués ne présentaient pas un caractère anormal, elle a relevé que les requérants n'établissaient pas qu'une atteinte excessive aurait été portée à l'équilibre financier de la convention du 23 novembre 1989 par l'arrêté du 10 décembre 1999 ni qu'ils auraient subi un préjudice excédant ceux que des tiers peuvent être normalement appelés à supporter sans indemnité dans l'intérêt général ; que, si la Compagnie de transport maritime et M. A...ne pouvaient être regardés comme des tiers à l'arrêté du 10 décembre 1999 qui les concernait directement, cette erreur de plume a été sans incidence sur l'arrêt de la cour ; que celle-ci n'a pas commis d'erreur de droit en recherchant l'effet de l'arrêté du 10 décembre 1999 sur les conditions d'exploitation de la société, par rapport à la situation antérieure, ni inexactement qualifié les faits en estimant que les requérants ne démontraient pas que les préjudices allégués auraient revêtu un caractère anormal ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que les requérants sont seulement fondés à demander l'annulation de l'arrêt qu'ils attaquent en tant qu'il a rejeté leurs conclusions présentées sur le fondement de la responsabilité pour faute tendant à l'indemnisation d'un préjudice de perte d'exploitation pour la société Compagnie de transport maritime ;<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              8. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du département de la Guadeloupe le versement à la société Compagnie de transport maritime, à M. B...A...et à M. D...A...de la somme totale de 1 000 euros au titre de ces dispositions ; que ces dispositions font obstacle à ce qu'une somme soit mise au même titre à la charge de la Compagnie de transport maritime, de M. B...A...et de M. D... A...qui ne sont pas les parties perdantes dans la présente instance ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 11 février 2014 de la cour administrative d'appel de Bordeaux est annulé en tant qu'il a rejeté les conclusions présentées sur le fondement de la responsabilité pour faute tendant à l'indemnisation d'un préjudice de perte d'exploitation pour la société Compagnie de transport maritime. <br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Bordeaux.<br/>
Article 3 : Le département de la Guadeloupe versera à la société Compagnie de transport maritime, à M. B...A...et à M. D...A...la somme totale de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Le surplus des conclusions du pourvoi et les conclusions présentées par le département de la Guadeloupe au titre de l'article L. 761-1 du code de justice administrative sont rejetés.<br/>
Article 5 : La présente décision sera notifiée à la société Compagnie de transport maritime, à M. B... A..., à M. D...A...et au département de la Guadeloupe. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
