<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037783365</ID>
<ANCIEN_ID>JG_L_2018_12_000000420900</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/78/33/CETATEXT000037783365.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 07/12/2018, 420900</TITRE>
<DATE_DEC>2018-12-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420900</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ZRIBI, TEXIER</AVOCATS>
<RAPPORTEUR>Mme Louise Bréhier</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHR:2018:420900.20181207</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par un arrêt n° 17VE03794 du 24 mai 2018, enregistré le même jour au secrétariat du contentieux du Conseil d'Etat, la cour administrative d'appel de Versailles, avant de statuer sur l'appel du préfet de la Seine-Saint-Denis tendant à l'annulation du jugement n° 1710098 du 17 novembre 2017 par lequel le magistrat désigné par le président du tribunal administratif de Montreuil a annulé ses arrêtés du 13 novembre 2017 relatifs au transfert de M. B... A...aux autorités italiennes et à son assignation à résidence, a décidé, par application des dispositions de l'article L. 113-1 du code de justice administrative, de transmettre le dossier de cette demande au Conseil d'Etat, en soumettant à son examen les questions suivantes :<br/>
<br/>
              1°) la décision de transfert prise en application de l'article 26 du règlement (UE) n° 604/2013 du 26 juin 2013 doit-elle mentionner, au titre de l'obligation de motivation prévue à l'article L. 742-3 précité du code de l'entrée et du séjour des étrangers et du droit d'asile, le critère retenu lors de la détermination de l'Etat responsable de la demande d'asile ainsi que les éléments de fait ayant conduit à l'application de ce critère ou bien convient-il de considérer que l'application éventuelle d'un critère de responsabilité relève du processus de détermination de l'Etat responsable de l'examen de la demande d'asile, auquel est d'ailleurs associé le demandeur, mais ne constitue pas l'une des considérations de droit et de fait fondant une décision de transfert qui ne peut être prise qu'après l'acceptation de la prise ou de la reprise en charge par l'Etat requis '<br/>
<br/>
              2°) le cas échéant, convient-il de distinguer entre le cas où la décision de transfert est prise à la suite de l'acceptation d'une demande de prise en charge, le ressortissant étranger ayant pour la première fois sollicité l'asile en France, et le cas où une telle décision fait suite à l'acceptation d'une demande de reprise en charge, le ressortissant étranger ayant sollicité l'asile dans un autre Etat que la France et la détermination de l'Etat en principe responsable de l'examen de sa demande ayant été effectuée une fois pour toutes à l'occasion de la première demande d'asile au vu de la situation prévalant à cette date ' <br/>
<br/>
<br/>
              .........................................................................<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le règlement (UE) n° 604/2013 du Parlement européen et du Conseil du 23 juin 2013 ; <br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative, notamment son article L. 113-1 ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Louise Bréhier, auditrice, <br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Zribi, Texier, avocat de M. A...; <br/>
<br/>
              Vu la note en délibéré, enregistrée le 21 novembre 2018, présentée par M. A...; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
                                    REND L'AVIS SUIVANT<br/>
<br/>
<br/>
              1. En vertu du paragraphe 1 de l'article 3 du règlement (UE) n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013 établissant les critères et mécanismes de détermination de l'Etat membre responsable de l'examen d'une demande de protection internationale introduite dans l'un des Etats membres par un ressortissant de pays tiers ou un apatride, lorsqu'une telle demande est présentée, un seul Etat, parmi ceux auxquels s'applique ce règlement, est responsable de son examen. Cet Etat, dit Etat membre responsable, est déterminé en faisant application des critères énoncés aux articles 7 à 15 du chapitre III du règlement ou, lorsqu'aucun Etat membre ne peut être désigné sur la base de ces critères, du premier alinéa du paragraphe 2 de l'article 3 de son chapitre II. Si l'Etat membre responsable est différent de l'Etat membre dans lequel se trouve le demandeur, ce dernier peut être transféré vers cet Etat, qui a vocation à le prendre en charge. Lorsqu'une personne a antérieurement présenté une demande d'asile sur le territoire d'un autre Etat membre, elle peut être transférée vers cet Etat, à qui il incombe de la reprendre en charge, sur le fondement des b), c) et d) du paragraphe 1 de l'article 18 du chapitre V et du paragraphe 5 de l'article 20 du chapitre VI de ce même règlement. <br/>
<br/>
              En application de l'article L. 742-3 du code de l'entrée et du séjour des étrangers et du droit d'asile, la décision de transfert dont fait l'objet un ressortissant de pays tiers ou un apatride qui a déposé auprès des autorités françaises une demande d'asile dont l'examen relève d'un autre Etat membre ayant accepté de le prendre ou de le reprendre en charge doit être motivée, c'est-à-dire qu'elle doit comporter l'énoncé des considérations de droit et de fait qui en constituent le fondement.<br/>
<br/>
              2. Pour l'application de ces dispositions, est suffisamment motivée une décision de transfert qui mentionne le règlement du Parlement européen et du Conseil du 26 juin 2013 et comprend l'indication des éléments de fait sur lesquels l'autorité administrative se fonde pour estimer que l'examen de la demande présentée devant elle relève de la responsabilité d'un autre Etat membre, une telle motivation permettant d'identifier le critère du règlement communautaire dont il est fait application. <br/>
<br/>
              3. Ainsi, doit notamment être regardée comme suffisamment motivée, s'agissant d'un étranger en provenance d'un pays tiers ou d'un apatride ayant, au cours des douze mois ayant précédé le dépôt de sa demande d'asile, pénétré irrégulièrement au sein de l'espace Dublin par le biais d'un Etat membre autre que la France, la décision de transfert à fin de prise en charge qui, après avoir visé le règlement, fait référence à la consultation du fichier Eurodac sans autre précision, une telle motivation faisant apparaître que l'Etat responsable a été désigné en application du critère énoncé à l'article 13 du chapitre III du règlement.<br/>
<br/>
              Doit de même être regardée comme suffisamment motivée, s'agissant d'un étranger dont un parent s'est vu reconnaître la qualité de réfugié au sein d'un autre Etat membre, la décision de transfert à fin de prise en charge qui, après avoir visé le règlement, se réfère à cette circonstance de fait, une telle motivation faisant apparaître que l'Etat responsable a été désigné en application du critère énoncé à l'article 9 du chapitre III du règlement.<br/>
<br/>
              S'agissant d'un étranger ayant, dans les conditions posées par le règlement, présenté une demande d'asile dans un autre Etat membre et devant, en conséquence, faire l'objet d'une reprise en charge par cet Etat, doit être regardée comme suffisamment motivée la décision de transfert à fin de reprise en charge qui, après avoir visé le règlement, relève que le demandeur a antérieurement présenté une demande dans l'Etat en cause, une telle motivation faisant apparaître qu'il est fait application du b), c) ou d) du paragraphe 1 de l'article 18 ou du paragraphe 5 de l'article 20 du règlement.<br/>
<br/>
<br/>
<br/>
<br/>
              4. Le présent avis sera notifié à la cour administrative d'appel de Versailles, à M. B...A...et au ministre de l'intérieur. Il sera publié au Journal officiel de la République française. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-03-01-02-01-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. QUESTIONS GÉNÉRALES. MOTIVATION. MOTIVATION OBLIGATOIRE. MOTIVATION OBLIGATOIRE EN VERTU D'UN TEXTE SPÉCIAL. - DÉCISION DE TRANSFERT D'UN DEMANDEUR D'ASILE VERS L'ETAT MEMBRE RESPONSABLE DE L'EXAMEN DE SA DEMANDE (2E AL. DE L'ART. L. 742-3 DU CESEDA) [RJ1] - 1) PRINCIPE - ENONCÉ DES CONSIDÉRATIONS DE DROIT ET DE FAIT QUI CONSTITUENT LE FONDEMENT DE LA DÉCISION - 2) PORTÉE - MENTION DU RÈGLEMENT DU 26 JUIN 2013 (DIT DUBLIN III) ET DES ÉLÉMENTS DE FAITS SUR LESQUELS SE FONDE L'AUTORITÉ ADMINISTRATIVE POUR ESTIMER QUE L'EXAMEN DE LA DEMANDE RELÈVE D'UN AUTRE ETAT MEMBRE, CE QUI PERMET D'IDENTIFIER LE CRITÈRE DU RÈGLEMENT DONT IL EST FAIT APPLICATION - 3) APPLICATIONS - CAS D'UN ÉTRANGER OU D'UN APATRIDE AYANT PÉNÉTRÉ IRRÉGULIÈREMENT AU SEIN DE L'ESPACE DUBLIN PAR LE BIAIS D'UN AUTRE ETAT MEMBRE - CAS D'UN ÉTRANGER DONT UN PARENT S'EST VU RECONNAÎTRE LA QUALITÉ DE RÉFUGIÉ AU SEIN D'UN AUTRE ETAT MEMBRE - CAS D'UN ÉTRANGER AYANT PRÉSENTÉ UNE DEMANDE DANS UN AUTRE ETAT MEMBRE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">095-02-03 - TRANSFERT D'UN DEMANDEUR D'ASILE VERS L'ETAT MEMBRE RESPONSABLE DE L'EXAMEN DE SA DEMANDE (RÈGLEMENT DU 26 JUIN 2013, DIT DUBLIN III) - OBLIGATION DE MOTIVATION DE LA DÉCISION DE TRANSFERT (2E AL. DE L'ART. L. 742-3 DU CESEDA) [RJ1] - 1) PRINCIPE - ENONCÉ DES CONSIDÉRATIONS DE DROIT ET DE FAIT QUI CONSTITUENT LE FONDEMENT DE LA DÉCISION - 2) PORTÉE - MENTION DU RÈGLEMENT ET DES ÉLÉMENTS DE FAITS SUR LESQUELS SE FONDE L'AUTORITÉ ADMINISTRATIVE POUR ESTIMER QUE L'EXAMEN DE LA DEMANDE RELÈVE D'UN AUTRE ETAT MEMBRE, CE QUI PERMET D'IDENTIFIER LE CRITÈRE DU RÈGLEMENT DONT IL EST FAIT APPLICATION - 3) APPLICATIONS - CAS D'UN ÉTRANGER OU D'UN APATRIDE AYANT PÉNÉTRÉ IRRÉGULIÈREMENT AU SEIN DE L'ESPACE DUBLIN PAR LE BIAIS D'UN AUTRE ETAT MEMBRE - CAS D'UN ÉTRANGER DONT UN PARENT S'EST VU RECONNAÎTRE LA QUALITÉ DE RÉFUGIÉ AU SEIN D'UN AUTRE ETAT MEMBRE - CAS D'UN ÉTRANGER AYANT PRÉSENTÉ UNE DEMANDE DANS UN AUTRE ETAT MEMBRE.
</SCT>
<ANA ID="9A"> 01-03-01-02-01-02 1) En application de l'article L. 742-3 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), la décision de transfert dont fait l'objet un ressortissant de pays tiers ou un apatride qui a déposé auprès des autorités françaises une demande d'asile dont l'examen relève d'un autre Etat membre ayant accepté de le prendre ou de le reprendre en charge doit être motivée, c'est-à-dire qu'elle doit comporter l'énoncé des considérations de droit et de fait qui en constituent le fondement.,,2) Pour l'application de ces dispositions, est suffisamment motivée une décision de transfert qui mentionne le règlement n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013 et comprend l'indication des éléments de fait sur lesquels l'autorité administrative se fonde pour estimer que l'examen de la demande présentée devant elle relève de la responsabilité d'un autre Etat membre, une telle motivation permettant d'identifier le critère du règlement communautaire dont il est fait application.... ...3) Ainsi, doit notamment être regardée comme suffisamment motivée, s'agissant d'un étranger en provenance d'un pays tiers ou d'un apatride ayant, au cours des douze mois ayant précédé le dépôt de sa demande d'asile, pénétré irrégulièrement au sein de l'espace Dublin par le biais d'un Etat membre autre que la France, la décision de transfert à fin de prise en charge qui, après avoir visé le règlement, fait référence à la consultation du fichier Eurodac sans autre précision, une telle motivation faisant apparaître que l'Etat responsable a été désigné en application du critère énoncé à l'article 13 du chapitre III du règlement.,,Doit de même être regardée comme suffisamment motivée, s'agissant d'un étranger dont un parent s'est vu reconnaître la qualité de réfugié au sein d'un autre Etat membre, la décision de transfert à fin de prise en charge qui, après avoir visé le règlement, se réfère à cette circonstance de fait, une telle motivation faisant apparaître que l'Etat responsable a été désigné en application du critère énoncé à l'article 9 du chapitre III du règlement.,,S'agissant d'un étranger ayant, dans les conditions posées par le règlement, présenté une demande d'asile dans un autre Etat membre et devant, en conséquence, faire l'objet d'une reprise en charge par cet Etat, doit être regardée comme suffisamment motivée la décision de transfert qui, après avoir visé le règlement, relève que le demandeur a antérieurement présenté une demande dans l'Etat en cause, une telle motivation faisant apparaître qu'il est fait application du b), c) ou d) du paragraphe 1 de l'article 18 ou du paragraphe 5 de l'article 20 du règlement.</ANA>
<ANA ID="9B"> 095-02-03 1) En application de l'article L. 742-3 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), la décision de transfert dont fait l'objet un ressortissant de pays tiers ou un apatride qui a déposé auprès des autorités françaises une demande d'asile dont l'examen relève d'un autre Etat membre ayant accepté de le prendre ou de le reprendre en charge doit être motivée, c'est-à-dire qu'elle doit comporter l'énoncé des considérations de droit et de fait qui en constituent le fondement.,,2) Pour l'application de ces dispositions, est suffisamment motivée une décision de transfert qui mentionne le règlement n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013 et comprend l'indication des éléments de fait sur lesquels l'autorité administrative se fonde pour estimer que l'examen de la demande présentée devant elle relève de la responsabilité d'un autre Etat membre, une telle motivation permettant d'identifier le critère du règlement communautaire dont il est fait application.... ...3) Ainsi, doit notamment être regardée comme suffisamment motivée, s'agissant d'un étranger en provenance d'un pays tiers ou d'un apatride ayant, au cours des douze mois ayant précédé le dépôt de sa demande d'asile, pénétré irrégulièrement au sein de l'espace Dublin par le biais d'un Etat membre autre que la France, la décision de transfert à fin de prise en charge qui, après avoir visé le règlement, fait référence à la consultation du fichier Eurodac sans autre précision, une telle motivation faisant apparaître que l'Etat responsable a été désigné en application du critère énoncé à l'article 13 du chapitre III du règlement.,,Doit de même être regardée comme suffisamment motivée, s'agissant d'un étranger dont un parent s'est vu reconnaître la qualité de réfugié au sein d'un autre Etat membre, la décision de transfert à fin de prise en charge qui, après avoir visé le règlement, se réfère à cette circonstance de fait, une telle motivation faisant apparaître que l'Etat responsable a été désigné en application du critère énoncé à l'article 9 du chapitre III du règlement.,,S'agissant d'un étranger ayant, dans les conditions posées par le règlement, présenté une demande d'asile dans un autre Etat membre et devant, en conséquence, faire l'objet d'une reprise en charge par cet Etat, doit être regardée comme suffisamment motivée la décision de transfert qui, après avoir visé le règlement, relève que le demandeur a antérieurement présenté une demande dans l'Etat en cause, une telle motivation faisant apparaître qu'il est fait application du b), c) ou d) du paragraphe 1 de l'article 18 ou du paragraphe 5 de l'article 20 du règlement.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, décision du même jour, Ministre de l'intérieur c/ M.,, n° 416823, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
