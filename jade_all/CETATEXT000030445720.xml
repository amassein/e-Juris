<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030445720</ID>
<ANCIEN_ID>JG_L_2015_03_000000384280</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/44/57/CETATEXT000030445720.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème SSJS, 23/03/2015, 384280, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-03-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384280</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; RICARD</AVOCATS>
<RAPPORTEUR>Mme Charline Nicolas</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:384280.20150323</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le syndicat mixte pour l'aménagement et l'exploitation de la station d'Isola 2000, la commune d'Isola et la société d'aménagement d'Isola 2000 ont engagé devant le tribunal administratif de Nice un contentieux relatif aux conséquences de la résiliation d'une convention d'aménagement.<br/>
<br/>
              Par un jugement n°s 0703648, 1000464 du 9 mars 2012, le tribunal administratif de Nice a enjoint à la société d'aménagement d'Isola 2000 de faire retour de parcelles de terrains à la commune d'Isola, dans un délai de trois mois à compter de la notification du jugement, et sous réserve qu'elle perçoive, au jour de la signature de l'acte matérialisant le transfert de propriété, la somme de 2 196 617 euros.<br/>
<br/>
              Par un arrêt n° 12MA01668 du 7 juillet 2014, la cour administrative d'appel de Marseille a, sur appel de la société d'aménagement d'Isola 2000, notamment enjoint au syndicat mixte pour l'aménagement et l'exploitation de la station d'Isola 2000, en ce qui concerne les parcelles supportant le golf d'altitude de 18 trous et le circuit de glace, de saisir dans un délai de deux mois à compter de la notification de l'arrêt, le service des domaines afin qu'il évalue la plus-value apportée aux parcelles en litige par les travaux régulièrement réalisés, a condamné la société à verser à la commune d'Isola et au syndicat mixte pour l'aménagement et l'exploitation de la station d'Isola 2000 la somme de 2 250 000 euros et a rejeté le surplus des conclusions de la requête de la société. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 5 septembre et 4 décembre 2014 au secrétariat du contentieux du Conseil d'Etat, la société d'aménagement d'Isola 2000 demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ; <br/>
<br/>
              3°) de mettre à la charge de la commune d'Isola et du syndicat mixte pour l'aménagement et l'exploitation de la station d'Isola 2000 le versement d'une somme de 20 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Charline Nicolas, auditeur,  <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Ricard, avocat de la société d'aménagement d'Isola 2000 ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux " ;<br/>
<br/>
              2. Considérant que pour demander l'annulation de l'arrêt attaqué, la société d'aménagement d'Isola 2000 (SAI 2000) soutient que la cour administrative d'appel de Marseille a commis une erreur de droit en retenant la compétence de la juridiction administrative pour statuer sur une action réelle immobilière relevant de la compétence de la juridiction judiciaire ; qu'elle a commis une erreur de droit et dénaturé les écritures des parties en jugeant que le tribunal administratif de Nice n'était pas tenu de se prononcer sur la nature des biens en litige au motif que cette question ne faisait l'objet que d'un argument au soutien du moyen tiré de l'inapplicabilité de l'article 20 de la convention d'aménagement ; qu'elle a commis une erreur de droit en admettant l'intérêt à agir du syndicat mixte pour l'aménagement et l'exploitation de la station d'Isola 2000 pour exercer une action en revendication de propriété ; qu'elle a commis une erreur de droit en admettant la qualité pour agir de la commune d'Isola qui n'était pas partie à la convention de 1992 et ne pouvait invoquer le bénéfice de son article 20 ; qu'elle a commis une erreur de droit en jugeant recevable la requête de la commune d'Isola qui n'avait pas donné de mandat à l'avocat la représentant ; qu'elle a commis une erreur de droit en jugeant que l'article 20 de la convention du 2 juillet 1992 ne contenait pas une stipulation pour autrui entachée de nullité et dépourvue d'effet juridique ; qu'elle a commis une erreur de droit en jugeant que le syndicat mixte et la commune étaient fondés, à l'appui de leur demande de retour des terrains, à se prévaloir de l'article 20 de la convention d'aménagement en litige en cas de résiliation de ladite convention sur la demande du syndicat, alors que la SAI 2000 a acquis ces terrains auprès de la société d'aménagement et que la commune d'Isola est un tiers à cet acte de vente ; qu'elle a commis une erreur de droit et dénaturé les pièces du dossier en jugeant que l'article 20 de la convention était applicable en cas de résiliation de la convention d'aménagement pour un motif d'intérêt général ; qu'elle a commis une erreur de droit en prononçant à son encontre une injonction assortie d'une astreinte ; qu'elle a commis une erreur de droit en refusant de l'indemniser de l'intégralité de son préjudice ; qu'elle a dénaturé les pièces du dossier en jugeant que, parmi les parcelles en litige, seules celles supportant le golf d'altitude et le circuit de glace avaient fait l'objet de travaux régulièrement réalisés ; que la juridiction administrative n'est pas compétente pour statuer sur les conclusions du syndicat mixte et de la commune tendant à la condamnation de la SAI 2000 au paiement du prix de la vente de la parcelle cadastrée section AC n° 86 lieudit Le Hameau ; que la cour a commis une erreur de droit en jugeant que la SAI 2000 avait méconnu ses obligations contractuelles en cédant une des parcelles dont le retour était demandé, alors que la commune et le syndicat mixte se sont désistés de leur instance et de leur action en restitution pour permettre cette cession ;<br/>
<br/>
              3. Considérant qu'eu égard aux moyens soulevés, il y a lieu d'admettre les conclusions du pourvoi dirigées contre l'arrêt en tant qu'il a condamné la SAI 2000 à verser à la commune d'Isola une somme de 2 250 000 euros en réparation du préjudice né de l'impossibilité d'obtenir le retour de la parcelle cadastrée section AC n° 86 lieudit Le Hameau ; qu'en revanche, s'agissant des autres conclusions du pourvoi, aucun de ces moyens n'est de nature à permettre l'admission du pourvoi ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les conclusions du pourvoi de la société d'aménagement d'Isola 2000 dirigées contre l'arrêt en tant qu'il l'a condamnée à verser à la commune d'Isola une somme de 2 250 000 euros en réparation du préjudice né de l'impossibilité d'obtenir le retour de la parcelle cadastrée section AC n° 86 lieudit Le Hameau sont admises. <br/>
Article 2 : Le surplus des conclusions du pourvoi de la société d'aménagement d'Isola 2000 n'est pas admis. <br/>
Article 3 : La présente décision sera notifiée à la société d'aménagement d'Isola 2000. <br/>
Copie en sera adressée pour information au syndicat mixte pour l'aménagement et l'exploitation de la station d'Isola 2000 et à la commune d'Isola.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
