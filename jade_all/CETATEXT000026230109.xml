<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026230109</ID>
<ANCIEN_ID>JG_L_2012_07_000000341926</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/23/01/CETATEXT000026230109.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  9ème sous-section jugeant seule, 27/07/2012, 341926, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-07-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>341926</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 9ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Jean-Pierre Jouguelet</PRESIDENT>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER ; SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>M. Matthieu Schlesinger</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:341926.20120727</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 27 juillet et 27 octobre 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. et Mme Jean-Claude B, demeurant ..., et pour la société civile immobilière BCI, dont le siège est Bel Air, 3 le Bois Joli à Saint-Laurent-sur-Sèvre (85290), représentée par son gérant ; M. et Mme B et la société civile immobilière BCI demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 06NT01057 du 12 mai 2010 par lequel la cour administrative d'appel de Nantes a rejeté leur requête tendant à l'annulation du jugement n° 02-2193 du 27 octobre 2005 du tribunal administratif de Nantes ayant rejeté leur demande tendant à la condamnation de la commune de Saint-Laurent-sur-Sèvre à leur verser la somme de 929 939 euros en réparation du préjudice résultant de l'illégalité de l'arrêté du 24 février 1992 leur ayant délivré un permis de construire et de l'arrêté du 22 juin 1992 ayant retiré ce permis de construire ;<br/>
<br/>
              2°) de mettre à la charge de la commune de Saint-Laurent-sur-Sèvre la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 68-1250 du 31 décembre 1968 ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Matthieu Schlesinger, Auditeur,  <br/>
<br/>
              - les observations de la SCP Célice, Blancpain, Soltner, avocat de la commune de Saint-Laurent-sur-Sèvre et de la SCP Odent, Poulet, avocat de la SCI BCI et de M. et Mme B,<br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Célice, Blancpain, Soltner, avocat de la commune de Saint-Laurent-sur-Sèvre et à la SCP Odent, Poulet, avocat de la SCI BCI et de M. et Mme B ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société civile immobilière BCI, constituée entre M. et Mme B, s'est vue délivrer le 24 février 1992 un permis de construire, à Saint-Laurent-sur-Sèvre, un ensemble immobilier comprenant 21 studios, dont une partie a été vendue en l'état futur d'achèvement ; que ce permis a été retiré le 22 juin 1992 pour illégalité, en raison de la méconnaissance des dispositions du règlement du plan d'occupation des sols relatives à la hauteur maximale des immeubles en bordure de rue ; qu'à la demande de la société civile immobilière, un expert a été désigné par ordonnance du juge des référés du tribunal administratif de Nantes du 11 septembre 1992 en vue d'évaluer l'étendue et le coût des travaux de démolition et de reconstruction nécessaires au respect, par l'immeuble litigieux, des dispositions du plan d'occupation des sols ; que cet expert a rendu son rapport le 30 octobre 1992 ; qu'une instance a ensuite été engagée devant le juge judiciaire par certains des acquéreurs de lots ; que par jugement du 4 avril 1995, le tribunal de grande instance de la Roche-sur-Yon a prononcé la résolution des ventes concernées et a condamné la société civile immobilière BCI à restituer aux acquéreurs le prix de la vente augmenté des frais notariés et à leur verser des dommages et intérêts ; qu'un accord transactionnel a toutefois été trouvé le 16 juillet 1997 ; que M. et Mme B ont demandé à la commune de Saint-Laurent-sur-Sèvre, par courrier reçu le 28 septembre 1999, à être indemnisés du préjudice qu'eux-mêmes et la société civile immobilière BCI estimaient avoir subi du fait de l'illégalité du permis de construire du 24 février 1992 ; qu'ils ont ensuite saisi le tribunal administratif de Nantes, qui, par jugement du 27 octobre 2005, a accueilli l'exception de prescription quadriennale opposée par le maire de la commune et, par suite, rejeté leur demande ; que M. et Mme B et la société civile immobilière BCI se pourvoient en cassation contre l'arrêt du 12 mai 2010 par lequel la cour administrative d'appel de Nantes a confirmé ce jugement ;<br/>
<br/>
              2. Considérant que, pour juger que la prescription quadriennale était opposée à bon droit par le maire de la commune de Saint-Laurent-sur-Sèvre, le tribunal administratif de Nantes a estimé que les requérants avaient nécessairement eu connaissance de l'étendue de leur préjudice à la date de dépôt, le 30 octobre 1992, du rapport de l'expert chargé d'évaluer les coûts des travaux de mise en conformité de l'immeuble avec le plan d'occupation des sols de la commune ; que, dans leur mémoire en réplique enregistré au greffe de la cour administrative d'appel le 29 mars 2010, les requérants soutenaient que les conséquences dommageable de la faute de l'administration n'avaient pu être connues dans leur entier qu'à l'issue du contentieux judiciaire opposant les acquéreurs des appartements à la SCI BCI, c'est-à-dire lors de la conclusion de la transaction passée le 16 juillet 1997, à la suite du jugement du tribunal de grande instance de la Roche-sur-Yon du 4 avril 1995 ; que la cour a jugé que le délai de prescription avait commencé à courir le 1er janvier 1993 sans répondre à ce moyen, qui n'était pas inopérant ; que, par suite, son arrêt doit être annulé, sans qu'il soit besoin d'examiner les autres moyens du pourvoi ;<br/>
<br/>
              3. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Saint-Laurent-sur-Sèvre le versement de la somme de 3 000 euros, pour moitié, à M. et Mme B et, pour moitié, à la société civile immobilière BCI, au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que les dispositions de cet article font, en revanche, obstacle à ce que le versement de la somme demandée par la commune de Saint-Laurent-sur-Sèvre soit mis à la charge de M. et Mme B et de la société civile immobilière BCI, qui ne sont pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nantes du 12 mai 2010 est annulé.<br/>
Article 2 : L'affaire est renvoyée devant la cour administrative d'appel de Nantes.<br/>
Article 3 : La commune de Saint-Laurent-sur-Sèvre versera la somme de 3 000 euros, pour moitié, à M. et Mme B et, pour moitié, à la société civile immobilière BCI au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par la commune de Saint-Laurent-sur-Sèvre sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à M. et Mme Jean-Claude B, à la société civile immobilière BCI et à la commune de Saint-Laurent-sur-Sèvre. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
