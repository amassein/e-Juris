<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030642968</ID>
<ANCIEN_ID>JG_L_2015_05_000000386935</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/64/29/CETATEXT000030642968.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  7ème sous-section jugeant seule, 27/05/2015, 386935, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-05-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>386935</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 7ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT</AVOCATS>
<RAPPORTEUR>M. François Lelièvre</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:386935.20150527</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 6 janvier et 31 mars 2015 au secrétariat du contentieux du Conseil d'Etat, la Commission nationale des comptes de campagne et des financements politiques a saisi le Conseil d'Etat, conformément à l'article L. 52-15 du code électoral, à la suite de sa décision du 17 novembre 2014 de rejet du compte de campagne de M.B..., candidat aux élections provinciales des Iles Loyauté en Nouvelle-Calédonie, en raison de la tardiveté du dépôt de ce compte.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi organique n° 99-209 du 19 mars 1999 ;<br/>
              - le code électoral ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Lelièvre, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Potier de la Varde, Buk Lament, avocat de M. B...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 52-12 du code électoral : " Chaque candidat ou candidat tête de liste soumis au plafonnement prévu à l'article L. 52-11 et qui a obtenu au moins 1 % des suffrages exprimés est tenu d'établir un compte de campagne retraçant, selon leur origine, l'ensemble des recettes perçues et, selon leur nature, l'ensemble des dépenses engagées ou effectuées en vue de l'élection, hors celles de la campagne officielle par lui-même ou pour son compte, au cours de la période mentionnée à l'article L. 52-4. (...) / Au plus tard avant 18 heures le dixième vendredi suivant le premier tour de scrutin, chaque candidat ou candidat tête de liste présent au premier tour dépose à la Commission nationale des comptes de campagne et des financements politiques son compte de campagne et ses annexes accompagné des justificatifs de ses recettes ainsi que des factures, devis et autres documents de nature à établir le montant des dépenses payées ou engagées par le candidat ou pour son compte. (...) " ; qu'en application de l'article L. 52-15 du même code : " (...) Lorsque la Commission nationale des comptes de campagne et des financements politiques a constaté que le compte de campagne n'a pas été déposé dans le délai prescrit, si le compte a été rejeté ou si, le cas échéant après réformation, il fait apparaître un dépassement du plafond des dépenses électorales, la commission saisit le juge de l'élection (...) " ; qu'enfin, aux termes de l'article L. 118-3 du même code, le juge de l'élection saisi sur ce fondement, " peut déclarer inéligible le candidat qui n'a pas déposé son compte de campagne dans les conditions et le délai prescrits à l'article L. 52-12 (...) " ; <br/>
<br/>
              2. Considérant que la Commission nationale des comptes de campagne et des financements politiques est tenue, lorsqu'elle constate le dépôt hors délai du compte de campagne d'un candidat, de saisir le juge de l'élection ; qu'il appartient alors seulement à ce dernier, régulièrement saisi, lorsqu'il estime que la commission a constaté à bon droit le dépôt tardif du compte de campagne, de décider qu'il n'y a pas lieu de prononcer l'inéligibilité de ce candidat ; qu'il ne peut, dans ce cas, rejeter la saisine de la commission ; qu'il doit, pour apprécier s'il y a lieu de faire usage de la faculté donnée par les dispositions précitées de l'article L. 118-3 du code électoral de déclarer inéligible un candidat qui n'a pas déposé son compte de campagne dans les conditions et délai prescrits à l'article L. 52-12 du code électoral, tenir compte de la nature de la règle méconnue, du caractère délibéré ou non du manquement, de l'existence éventuelle d'autres motifs d'irrégularité du compte, du montant des sommes en cause ainsi que de l'ensemble des circonstances de l'espèce ;<br/>
<br/>
              3. Considérant qu'il résulte de l'instruction que M.B..., candidat aux élections provinciales des Iles Loyauté en Nouvelle-Calédonie, qui se sont déroulées le 11 mai 2014, a déposé son compte de campagne le 23 juillet 2014, soit hors du délai prescrit par les dispositions précitées de l'article L. 52-12 du code électoral, qui expirait le 18 juillet 2014 à dix-huit heures ; que, par suite, c'est à bon droit que la Commission nationale des comptes de campagne et des financements politiques a constaté le dépôt tardif du compte de campagne de M. B...; <br/>
<br/>
              4. Considérant qu'il résulte de l'instruction que M.B..., qui a transmis son compte de campagne cinq jours seulement après l'expiration du délai de dépôt prévu par l'article L. 52-12 du code électoral, a connu, pendant la période ayant suivi le scrutin des difficultés d'ordre personnel ; que la Commission nationale des comptes de campagne et des financements politiques n'a relevé aucune autre irrégularité dans l'établissement de son compte de campagne ; que, dans les circonstances de l'espèce, il n'y a pas lieu de déclarer M. B... inéligible ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de déclarer inéligible M. B...en application de l'article L. 118-3 du code électoral.<br/>
Article 2 : La présente décision sera notifiée à la Commission nationale des comptes de campagne et des financements politiques et à M. A...B....<br/>
Copie en sera adressée pour information à la ministre des outre-mer.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
