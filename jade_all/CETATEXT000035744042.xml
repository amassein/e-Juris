<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035744042</ID>
<ANCIEN_ID>JG_L_2017_10_000000407030</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/74/40/CETATEXT000035744042.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 05/10/2017, 407030, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-10-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>407030</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MARLANGE, DE LA BURGADE</AVOCATS>
<RAPPORTEUR>M. Alain Seban</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:407030.20171005</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au juge des référés du tribunal administratif de Paris, sur le fondement de l'article R. 541-1 du code de justice administrative, de condamner l'Etat à lui verser une provision de 9 419,74 euros en réparation des préjudices résultant de son absence de relogement. Par une ordonnance n° 1608373 du 22 décembre 2016, le juge des référés du tribunal administratif de Paris a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 20 janvier, 26 janvier et 5 juillet 2017 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de condamner l'Etat à lui verser une provision de 11 134,54 euros, assortie des intérêts au taux légal ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le code de la construction et de l'habitation ;<br/>
<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alain Seban, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Marlange, de la Burgade, avocat de M.A....<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que, par décision du 7 mars 2014, la commission de médiation de Paris a désigné M. A...comme prioritaire et devant être logé en urgence, au motif qu'il était menacé d'expulsion ; que par jugement du 15 avril 2015, le tribunal administratif de Paris, saisi par M.A..., a enjoint au préfet de la région d'Île-de-France, préfet de Paris, de procéder à son relogement sous astreinte de 200 euros par mois de retard à compter du 1er juillet 2015 ; que cette décision et ce jugement n'ayant pas reçu d'exécution, M. A...a saisi le juge des référés du tribunal administratif de Paris, sur le fondement de l'article R. 541-1 du code de justice administrative, d'une demande tendant à la condamnation de l'Etat à lui verser une provision en réparation des préjudices résultant de son absence de relogement depuis le 7 septembre 2014 ; que, par ordonnance du 22 décembre 2016, le juge des référés a rejeté sa demande ; que M. A...se pourvoit en cassation contre cette ordonnance ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 541-1 du code de justice administrative : " Le juge des référés peut, même en l'absence d'une demande au fond, accorder une provision au créancier qui l'a saisi lorsque l'existence de l'obligation n'est pas sérieusement contestable. Il peut, même d'office, subordonner le versement de la provision à la constitution d'une garantie " ; <br/>
<br/>
              3. Considérant que, lorsqu'une personne a été reconnue par une commission de médiation comme prioritaire et devant être logée ou relogée d'urgence, en application des dispositions de l'article L. 441-2-3 du code de la construction et de l'habitation, la carence fautive de l'Etat à exécuter cette décision dans le délai imparti engage sa responsabilité à l'égard du seul demandeur, au titre des troubles dans les conditions d'existence résultant du maintien de la situation qui a motivé la décision de la commission ; que ces troubles doivent être appréciés en fonction des conditions de logement qui ont perduré du fait de la carence de l'Etat, de la durée de cette carence et du nombre de personnes composant le foyer du demandeur pendant la période de responsabilité de l'Etat ;<br/>
<br/>
              4. Considérant qu'après avoir constaté que M. A...n'avait pas reçu de proposition de relogement dans le délai prévu par le code de la construction et de l'habitation à compter de la décision de la commission de médiation, le juge des référés du tribunal administratif de Paris a estimé que l'intéressé ne justifiait pas d'une menace effective d'expulsion de nature à lui causer un quelconque préjudice, dès lors que les préfets étaient tenus, en application d'une instruction du 26 octobre 2012 du ministre de l'intérieur et du ministre de l'égalité des territoires et du logement, d'assurer le relogement effectif des personnes reconnues prioritaires et devant être relogées en urgence avant de mettre en oeuvre le concours de la force publique ; qu'en statuant ainsi, alors que la situation qui avait motivé la décision de la commission de médiation perdurait à la date de son ordonnance, ce qui créait pour M. A...un préjudice indemnisable tenant aux troubles dans ses conditions d'existence, le juge des référés a dénaturé les pièces du dossier qui lui était soumis ; qu'il a, dès lors, également entaché son ordonnance d'une erreur de qualification juridique en déniant à l'obligation dont se prévalait M. A...un caractère non sérieusement contestable ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, M. A...est fondé à demander l'annulation de l'ordonnance qu'il attaque ;<br/>
<br/>
              5. Considérant que, dans les circonstances de l'espèce, il y a lieu, en application de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée ;<br/>
<br/>
              6. Considérant que si le juge saisi sur le fondement de l'article L. 441-2-3-1 du code de la construction et de l'habitation ne peut statuer sur des conclusions mettant en cause la responsabilité de l'Etat en raison de sa carence dans la mise en oeuvre du droit au logement opposable, ni sur une demande de provision présentée sur ce même fondement, de telles conclusions peuvent en revanche être utilement présentées devant le tribunal administratif ou, comme en l'espèce, le juge des référés statuant selon le droit commun du contentieux administratif ; que la fin de non-recevoir opposée par le ministre de l'intérieur doit, par suite, être écartée ; <br/>
<br/>
              7. Considérant qu'il résulte de l'instruction que le préfet de la région d'Île-de-France, préfet de Paris, n'a pas adressé à M. A...de proposition de logement dans le délai qui lui était imparti pour exécuter la décision de la commission de médiation, lequel expirait le 7 septembre 2014 ; que le motif qui a justifié la décision de la commission, tenant à une menace effective d'expulsion, perdure depuis cette date, ainsi que l'atteste la décision d'octroi du concours de la force publique prise le 6 septembre 2016 et suspendue par une ordonnance du 19 octobre 2016 du juge des référés du tribunal administratif de Paris  ; que, dès lors, M. A... justifie d'un préjudice tenant à l'existence de troubles dans ses conditions d'existence, dont l'évaluation doit, dans les circonstances de l'espèce, prendre en compte le fait que l'absence de relogement l'a contraint à exposer un loyer manifestement disproportionné au regard de ses ressources ; qu'il suit de là que l'obligation dont se prévaut M. A...à l'encontre de l'Etat n'est pas sérieusement contestable ; que compte tenu des troubles de toute nature dans les conditions d'existence subis par le requérant depuis le 7 septembre 2014, il y a lieu de fixer le montant de la provision au versement de laquelle l'Etat doit être condamné à 1 000 euros tous intérêts compris au jour de la présente décision ;<br/>
<br/>
              8. Considérant que M. A...a obtenu le bénéfice de l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que Me Ghislain Foucault, avocat de M. A...en première instance, et la SCP Marlange, de la Burgade, avocat de M. A...devant le Conseil d'Etat, renoncent à percevoir les sommes correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat les sommes de 2 000 euros à verser à Me Ghislain Foucault et 2 000 euros à verser à la SCP Marlange, de la Burgade ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 22 décembre 2016 du juge des référés du tribunal administratif de Paris est annulée.<br/>
<br/>
Article 2 : L'Etat est condamné à verser à M. A...une provision de 1 000 euros tous intérêts compris au jour de la présente décision.<br/>
<br/>
Article 3 : L'Etat versera à Me Ghislain Foucault une somme de 2 000 euros et à la SCP Marlange, de la Burgade une somme de 2 000 euros en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cet avocat et cette société renoncent à percevoir les sommes correspondant à la part contributive de l'Etat.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. B...A...et au ministre de la cohésion des territoires.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
