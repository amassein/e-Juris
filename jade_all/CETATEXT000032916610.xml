<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032916610</ID>
<ANCIEN_ID>JG_L_2016_07_000000396968</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/91/66/CETATEXT000032916610.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 19/07/2016, 396968, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-07-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>396968</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Vincent Uher</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:396968.20160719</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un mémoire, enregistré le 12 février 2016 au secrétariat du contentieux du Conseil d'État, M. et Mme A...B...demandent au Conseil d'Etat, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 et à l'appui de leur requête tendant à l'annulation pour excès de pouvoir du paragraphe 370 de l'instruction BOI-RPPM-PVBMI-30-10-30-10 publiée au bulletin officiel des finances publiques-impôts le 2 juillet 2015 en tant qu'il exclut du champ d'application des abattements pour durée de détention les plus-values placées en report d'imposition en application de l'article 150-0 B ter du code général des impôts, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions de l'article 150-0 D du code général des impôts dans leur rédaction issue de l'article 17 de la loi du 29 décembre 2013 de finances pour 2014 en tant qu'elles excluent du champ d'application des abattements pour durée de détention les plus-values réalisées antérieurement au 1er janvier 2013 et placées en report d'imposition. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ; <br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code général des impôts et le livre des procédures fiscales ; <br/>
              - la loi n° 2013-1278 du 29 décembre 2013, notamment son article 17 ; <br/>
              - la décision n° 2016-538 QPC du 22 avril 2016 du Conseil constitutionnel ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Uher, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant  qu'en vertu du 2 de l'article 200 A du code général des impôts, dans sa rédaction applicable aux revenus perçus à compter du 1er janvier 2013 et résultant de la loi du 29 décembre 2012 de finances pour 2013, les gains nets obtenus dans les conditions prévues à l'article 150-0 A sont  pris en compte pour la détermination du revenu net global soumis au barème progressif de l'impôt sur le revenu ; que l'article 150-0 D du code général des impôts, dans sa rédaction résultant de la loi du 29 décembre 2014 de finances rectificative pour 2014  dispose, au deuxième alinéa de son 1, que :  " Les gains nets de cession à titre onéreux d'actions, de parts de sociétés, de droits portant sur ces actions ou parts, ou de titres représentatifs de ces mêmes actions, parts ou droits, mentionnés au I de l'article 150-0 A, ainsi que les distributions mentionnées aux 7,7 bis et aux deux derniers alinéas du 8 du II du même article, à l'article 150-0 F et au 1 du II de l'article 163 quinquies C sont réduits d'un abattement déterminé dans les conditions prévues, selon le cas, au 1 ter ou au 1 quater du présent article. (...) " ; qu'il définit, à son 1 ter, l'abattement pour durée de détention de droit commun et, à son 1 quater, l'abattement pour durée de détention renforcé applicable à certaines situations ; que le III de l'article 17 de la loi du 29 décembre 2013 de finances pour 2014 prévoit que ces dispositions s'appliquent aux gains réalisés à compter du 1er janvier 2013 ;<br/>
<br/>
              2. Considérant qu'à l'appui de sa demande d'annulation pour excès de pouvoir du paragraphe n° 130 de l'instruction fiscale BOIRPPM-PVBMI-20-20-10 publiée au bulletin officiel des finances publiques le 20 avril 2015, en tant qu'il écarte l'application de l'abattement pour durée de détention prévu par l'article 150-0 D du code général des impôts aux plus-values antérieures au 1er janvier 2013 en report d'imposition en application de l'article 150-0 B ter du même code, M. et Mme B...soulèvent une question prioritaire de constitutionnalité sur les dispositions du III de l'article 17 de la loi du 29 décembre 2013 de finances pour 2014 en tant qu'elles prévoient que l'abattement pour durée de détention ne s'applique qu'à compter du 1er janvier 2013 ; <br/>
<br/>
              3. Considérant, toutefois, que saisi des dispositions des trois premiers alinéas du 1 ter et du A du 1 quater de l'article 150-0 D du code général des impôts, dans sa rédaction issue de la loi du 29 décembre 2013, en tant qu'elles n'appliquent pas les abattements pour durée de détention qu'elles prévoient aux plus-values placées en report d'imposition avant le 1er janvier 2013, le Conseil constitutionnel les a déclarées conformes à la Constitution dans sa décision n° 2016-538 QPC du 22 avril 2016 ; que, par suite, la demande de transmission présentée par M. et Mme B...est devenue sans objet ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de statuer sur la demande présentée par M. et Mme B...tendant à ce que le Conseil d'Etat renvoie au Conseil constitutionnel la question prioritaire de constitutionnalité qu'ils ont soulevée.  <br/>
Article 2 : La présente décision sera notifiée à M. et Mme A...B...et au ministre des finances et des comptes publics. <br/>
Copie en sera adressée au Conseil constitutionnel et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
