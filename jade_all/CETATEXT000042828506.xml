<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042828506</ID>
<ANCIEN_ID>JG_L_2020_12_000000442266</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/82/85/CETATEXT000042828506.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 29/12/2020, 442266, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>442266</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Recours en révision</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR>M. Damien Pons</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:442266.20201229</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              M. A... D... a demandé au tribunal administratif de Grenoble d'annuler pour excès de pouvoir l'arrêté du 11 juin 2015 par lequel le maire de Pont-de-Chéruy a délivré à Mme C... B... un permis de construire modificatif du permis qui lui avait été accordé le 1er juin 2012 en vue de la construction d'un immeuble à usage d'habitation, ainsi que la décision y du 7 juillet 2015 par laquelle le maire a rejeté son recours gracieux contre cet arrêté. Par un jugement n° 1505001 du 15 juin 2017, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par une ordonnance n° 17LY02697 du 15 mai 2019, le président de la 1ère chambre de la cour administrative d'appel de Lyon a rejeté l'appel formé contre ce jugement par M. D....<br/>
<br/>
              Par une décision n° 430896 du 2 juillet 2020, le Conseil d'Etat statuant au contentieux n'a pas admis le pourvoi formé par M. D... contre cette ordonnance.<br/>
<br/>
Recours en révision<br/>
<br/>
              Par une requête et un nouveau mémoire, enregistrés les 28 juillet et 12 novembre 2020 au secrétariat du contentieux du Conseil d'Etat, M. D... demande au Conseil d'État :<br/>
<br/>
              1°) de réviser la décision n° 430896 du 2 juillet 2020 par laquelle il n'a pas admis son pourvoi ;<br/>
<br/>
              2°) d'annuler l'ordonnance du 15 mai 2019 du président de la 1ère chambre de la cour administrative d'appel de Lyon ; <br/>
<br/>
              3°) de mettre à la charge de la commune de Pont-de-Chéruy et de Mme B... la somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative, notamment son article R. 611-8, et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Damien Pons, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de M. D... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article R. 834-1 du code de justice administrative : " Le recours en révision contre une décision contradictoire du Conseil d'Etat (...) peut être présenté (...) si elle a été rendue sur pièces fausses (...) ". <br/>
<br/>
              2. Par une ordonnance du 15 mai 2019, le président de la 1ère chambre de la cour administrative d'appel de Lyon a rejeté comme irrecevable l'appel formé par M. D... contre le jugement du tribunal administratif de Grenoble rejetant sa demande d'annulation du permis de construire modificatif délivré le 11 juin 2015 à Mme B... par le maire de Pont-de-Chéruy, au motif qu'il ne justifiait pas s'être acquitté des formalités de notification de sa requête à l'auteur du permis et à son bénéficiaire, prévues par l'article R. 600-1 du code de l'urbanisme. Par une décision du 2 juillet 2020, le Conseil d'Etat statuant au contentieux n'a pas admis le pourvoi formé par M. D... contre cette ordonnance, au motif que les deux moyens soulevés, tirés de l'atteinte à son droit au recours et de l'erreur de droit à ne pas avoir tenu compte du caractère confus de l'affichage du permis de construire, n'étaient pas de nature à permettre cette admission. Si M. D... soutient, à l'appui de son recours en révision de cette décision, que les dossiers des demandes de permis de construire initial et modificatif présentées par Mme B... comportaient des pièces fausses, en ce qui concerne les surfaces de la future construction, la décision du Conseil d'Etat ne peut être regardée, en tout état de cause, comme ayant été rendue sur le fondement de ces pièces, dépourvues d'incidence sur l'appréciation, seule soumise au juge de cassation, du bien-fondé de la fin de non-recevoir opposée à la requête d'appel de M. D.... Celui-ci n'est donc pas recevable à se pourvoir en révision contre la décision rendue le 2 juillet 2020 par le Conseil d'Etat.<br/>
<br/>
              3. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'il soit fait droit aux conclusions présentées à ce titre par M. D....<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : La requête de M. D... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. A... D....<br/>
Copie en sera adressée à la commune de Pont-de-Chéruy et à Mme C... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
