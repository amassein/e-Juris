<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033163064</ID>
<ANCIEN_ID>JG_L_2016_09_000000400393</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/16/30/CETATEXT000033163064.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 28/09/2016, 400393</TITRE>
<DATE_DEC>2016-09-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>400393</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; SCP GADIOU, CHEVALLIER</AVOCATS>
<RAPPORTEUR>Mme Marie-Anne Lévêque</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:400393.20160928</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              La société Biblioteca a demandé au juge des référés du tribunal administratif de Lyon, sur le fondement de l'article L. 551-1 du code de justice, d'annuler l'ensemble des décisions se rapportant à la procédure de passation du marché ayant pour objet la " conception, impression et livraison de dictionnaires destinés aux collégiens pour les rentrées scolaires 2016-2017 et 2017-2018 " lancée par le département de la Loire et d'enjoindre à celui-ci de reprendre la procédure de passation de ce marché au stade de l'analyse des offres. <br/>
<br/>
              Par une ordonnance n° 1603182 du 19 mai 2016, le juge des référés du tribunal administratif de Lyon a annulé la procédure d'attribution du marché en litige à compter de la phase d'analyse des offres et enjoint au département de la Loire, s'il entend conclure un marché ayant le même objet, de reprendre la procédure de passation au stade de l'analyse des offres en se conformant à ses obligations de publicité et de mise en concurrence. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 3 et 20 juin 2016 au secrétariat du contentieux du Conseil d'Etat, le département de la Loire demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;  <br/>
<br/>
              2°) de mettre à la charge de la société Biblioteca la somme de 3 600 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'éducation ;  <br/>
              - le code des marchés publics ;<br/>
              - la loi n° 81-766 du 10 août 1981 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Anne Lévêque, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat du département de la Loire et à la SCP Gadiou, Chevallier, avocat de la société Biblioteca ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis au juge du référé précontractuel que, par un avis publié le 23 février 2016, le département de la Loire a lancé une consultation pour l'attribution, selon une procédure adaptée, d'un marché portant sur la " conception, impression et livraison de dictionnaires destinés aux collégiens pour les rentrées scolaires 2016-2017 et 2017-2018 " ; que la société Biblioteca, classée seconde, a demandé au juge des référés du tribunal administratif de Lyon, sur le fondement des dispositions de l'article L. 551-1 du code de justice administrative, l'annulation de cette procédure au motif que l'offre de la société Sphère publique, classée première, méconnaissait les dispositions de l'article 3 de la loi du 10 août 1981 relative au prix du livre et que le pouvoir adjudicateur était dès lors tenu de la rejeter comme inacceptable ; que, par l'ordonnance attaquée, le juge du référé précontractuel a annulé, pour ce motif, la procédure d'attribution du marché en litige à compter de la phase d'analyse des offres et enjoint au département de la Loire de reprendre la procédure de passation au stade de cette analyse en se conformant à ses obligations de publicité et de mise en concurrence ; que le département de la Loire se pourvoit en cassation contre cette ordonnance ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article 1er de la loi du 10 août 1981 relative au prix du livre : " Toute personne physique ou morale qui édite ou importe des livres est tenue de fixer, pour les livres qu'elle édite ou importe, un prix de vente au public (...) " ; qu'aux termes de l'article 3 de la même loi : " Le prix effectif de vente des livres peut être compris entre 91 % et 100 % du prix de vente au public lorsque l'achat est réalisé :/ 1° Pour leurs besoins propres, excluant la revente, par l'Etat, les collectivités territoriales, (...) /Le prix effectif de vente des livres scolaires peut être fixé librement dès lors que l'achat est effectué par une association facilitant l'acquisition de livres scolaires par ses membres ou, pour leurs besoins propres, excluant la revente, par l'Etat, une collectivité territoriale ou un établissement d'enseignement (...) " ; qu'aux termes de l'article 5 de la même loi : " Les détaillants peuvent pratiquer des prix inférieurs au prix de vente au public mentionné à l'article 1er sur les livres édités ou importés depuis plus de deux ans, et dont le dernier approvisionnement remonte à plus de six mois " ;  qu'il résulte de ces dispositions que le prix de vente aux collectivités territoriales, pour leurs besoins propres, excluant la revente, d'un livre, n'ayant pas le caractère d'un livre scolaire, édité ou importé depuis moins de deux ans et dont le dernier approvisionnement par le vendeur remonte à moins de six mois, ne peut être inférieur à 91% du prix de vente au public fixé par l'éditeur ;  <br/>
<br/>
              3. Considérant, en premier lieu, qu'il ressort des énonciations de l'ordonnance attaquée que le juge des référés du tribunal administratif de Lyon a relevé que l'objet du marché en litige était le suivant : " Définition du concept et de la maquette, infographie, personnalisation des 1ère et dernière page de couverture, ainsi que de la tranche, mise en page et illustration des 8 pages intérieures personnalisées, intégration des 8 pages personnalisées dans les dictionnaires destinés aux élèves de 6ème des collèges publics et privés de la Loire, photogravure quadrichromie des pages personnalisées, impression, façonnage, emballage et livraison " ; qu'il a estimé, par une appréciation souveraine qui n'est pas arguée de dénaturation, qu'en dépit de la personnalisation demandée par le département de la Loire, ce marché, avait pour objet la vente de livres et non une prestation de services ; qu'il n'a pas commis d'erreur de droit en en déduisant qu'un tel marché était soumis aux dispositions précitées des articles 1er et 3 de la loi du 10 août 1981 ; <br/>
<br/>
              4. Considérant, en second lieu, qu'aux termes du III de l'article 53 du code des marchés publics, dans sa version applicable au litige : " Les offres inappropriées, irrégulières et inacceptables sont éliminées (...) " ; qu'aux termes du 1° du I de l'article 35 du même code : " Une offre est inacceptable si les conditions prévues pour son exécution méconnaissent la législation en vigueur (...) " ; <br/>
<br/>
              5. Considérant que le juge des référés, qui a suffisamment motivé son ordonnance sur ce point, a estimé, par une appréciation souveraine exempte de dénaturation, que la circonstance que les dictionnaires objets du marché en litige comportaient une première et une dernière page de couverture modifiée par rapport à la version du dictionnaire vendue au public et huit pages supplémentaires personnalisées ne suffisait pas à les faire regarder comme des ouvrages distincts du dictionnaire destiné au public ; qu'il n'a pas commis d'erreur de droit en en déduisant que l'offre de la société Sphère publique, qui proposait un prix inférieur à 91% du prix de vente au public de ce dictionnaire, dont il n'est pas contesté qu'il n'avait pas la qualité de livre scolaire, méconnaissait les dispositions précitées de l'article 3 de la loi du 10 août 1981 et devait donc être rejetée comme inacceptable ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que le pourvoi du département de la Loire doit être rejeté, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge du département de la Loire une somme de 3 500 euros à verser à la société Biblioteca sur le même fondement ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du département de la Loire est rejeté.<br/>
Article 2 : Le département de la Loire versera à la société Biblioteca la somme de 3 500 euros en application de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée au département de la Loire, à la société Biblioteca et à la société Sphère publique.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">09-06 ARTS ET LETTRES. LIVRE. - PRIX DU LIVRE - APPLICATION DE LA LOI DU 10 AOÛT 1981 RELATIVE AU PRIX DU LIVRE À UN MARCHÉ PUBLIC - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-02-005 MARCHÉS ET CONTRATS ADMINISTRATIFS. FORMATION DES CONTRATS ET MARCHÉS. FORMALITÉS DE PUBLICITÉ ET DE MISE EN CONCURRENCE. - MARCHÉ PUBLIC RELATIF À DES LIVRES - APPLICATION DE LA LOI DU 10 AOÛT 1981 RELATIVE AU PRIX DU LIVRE - 1) OBLIGATION DU CANDIDAT DE NE PAS PROPOSER UN PRIX INFÉRIEUR À 91% DU PRIX DE VENTE AU PUBLIC FIXÉ PAR L'ÉDITEUR - EXISTENCE - 2) OFFRE PROPOSANT UN PRIX INFÉRIEUR À 91% - OFFRE INACCEPTABLE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">39-05-01-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. EXÉCUTION FINANCIÈRE DU CONTRAT. RÉMUNÉRATION DU CO-CONTRACTANT. PRIX. - PRIX DE LIVRES - APPLICATION DE LA LOI DU 10 AOÛT 1981 RELATIVE AU PRIX DU LIVRE - EXISTENCE.
</SCT>
<ANA ID="9A"> 09-06 Marché public portant sur la conception, impression et livraison de dictionnaires destinés aux collégiens. Il résulte des articles 1, 3 et 5 de la loi n° 81-766 du 10 août 1981 relative au prix du livre que le prix de vente aux collectivités territoriales, pour leurs besoins propres, excluant la revente, d'un livre, n'ayant pas le caractère d'un livre scolaire, édité ou importé depuis moins de deux ans et dont le dernier approvisionnement par le vendeur remonte à moins de six mois, ne peut être inférieur à 91% du prix de vente au public fixé par l'éditeur.</ANA>
<ANA ID="9B"> 39-02-005 1) Il résulte des articles 1, 3 et 5 de la loi n° 81-766 du 10 août 1981 relative au prix du livre que le prix de vente aux collectivités territoriales, pour leurs besoins propres, excluant la revente, d'un livre, n'ayant pas le caractère d'un livre scolaire, édité ou importé depuis moins de deux ans et dont le dernier approvisionnement par le vendeur remonte à moins de six mois, ne peut être inférieur à 91% du prix de vente au public fixé par l'éditeur.,,,2) Marché public portant sur la conception, impression et livraison de dictionnaires destinés aux collégiens. En l'espèce, la circonstance que les dictionnaires objets du marché en litige comportaient une première et une dernière page de couverture modifiée par rapport à la version du dictionnaire déjà vendue au public et huit pages supplémentaires personnalisées ne suffisait pas à les faire regarder comme des ouvrages distincts du dictionnaire destiné au public. Dès lors, une offre proposant un prix inférieur à 91% du prix de vente au public de ce dictionnaire, dont il n'est pas contesté qu'il n'avait pas la qualité de livre scolaire, devait être rejetée comme inacceptable.</ANA>
<ANA ID="9C"> 39-05-01-01 Il résulte des articles 1, 3 et 5 de la loi n° 81-766 du 10 août 1981 relative au prix du livre que le prix de vente aux collectivités territoriales, pour leurs besoins propres, excluant la revente, d'un livre, n'ayant pas le caractère d'un livre scolaire, édité ou importé depuis moins de deux ans et dont le dernier approvisionnement par le vendeur remonte à moins de six mois, ne peut être inférieur à 91% du prix de vente au public fixé par l'éditeur.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
