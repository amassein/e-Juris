<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000020406434</ID>
<ANCIEN_ID>JG_L_2008_02_000000287456</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/20/40/64/CETATEXT000020406434.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 20/02/2008, 287456, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2008-02-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>287456</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Daël</PRESIDENT>
<AVOCATS>SCP BACHELLIER, POTIER DE LA VARDE</AVOCATS>
<RAPPORTEUR>M. Brice  Bohuon</RAPPORTEUR>
<COMMISSAIRE_GVT>Mlle Verot Célia</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 24 novembre 2005 et 24 mars 2006 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. Christian A, demeurant ... ; M. A demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'article 2 de l'arrêt du 20 septembre 2005 par lequel la cour administrative d'appel de Douai, après avoir annulé le jugement du tribunal administratif d'Amiens en date du 25 septembre 2003, a rejeté ses conclusions tendant à la décharge du complément d'impôt sur le revenu auquel il a été assujetti au titre de l'année 1999 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de le décharger de l'imposition litigieuse ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu le code général des impôts ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Brice Bohuon, Auditeur,<br/>
              - les observations de la SCP Bachellier, Potier de la Varde, avocat de M. Christian A, <br/>
<br/>
              - les conclusions de Mlle Célia Verot, Commissaire du gouvernement ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. A est associé de la SCI La Veillère qui, au cours des années 1997 à 1999, louait des immeubles nus à plusieurs sociétés dont la SA « Royal Garage » et la SA « Garage Les Aubivats » ; qu'à l'issue d'une vérification de comptabilité de la SCI La Veillère, l'administration fiscale a remis en cause la déduction, d'une part, des dépenses exposées en 1997 et 1998 pour dédommager les locataires ayant financé des aménagements et agencements à l'intérieur des locaux loués, et, d'autre part, d'une partie des intérêts d'emprunts supportés au cours des années 1997 à 1999 ; que l'administration a rectifié les résultats au titre de l'année 1999, directement, pour partie en remettant en cause les déficits au titre des années 1997 et 1998 reportés sur les résultats au titre de l'année 1999 ; que, par un jugement du 25 septembre 2003, le tribunal administratif d'Amiens a rejeté la demande de M. A tendant à la décharge des compléments d'impôt sur le revenu auxquels il a été assujetti au titre de ces années à raison de la réintégration de ces sommes ; que M. A se pourvoit en cassation contre l'arrêt de la cour administrative d'appel de Douai du 20 septembre 2005 qui a rejeté ses conclusions en décharge ;<br/>
<br/>
              Sur les remboursements aux locataires des dépenses relatives à des aménagements et agencements à l'intérieur des locaux loués :<br/>
<br/>
              Considérant qu'aux termes de l'article 13 du code général des impôts : « I. Le (...) revenu imposable est constitué par l'excédent du produit brut (...) sur les dépenses effectuées en vue de l'acquisition et de la conservation du revenu », qu'aux termes de l'article 28 du même code : « Le revenu net foncier est égal à la différence entre le montant du revenu brut et le total des charges de la propriété » et qu'enfin, aux termes de l'article 31 du même code, dans sa rédaction applicable aux années d'imposition litigieuses : « I. Les charges de la propriété déductibles pour la détermination du revenu net comprennent : 1° Pour les propriétés urbaines : a. Les dépenses de réparation et d'entretien, les frais de gérance et de rémunération des gardes et concierges ; (...) b bis. Les dépenses d'amélioration afférentes aux locaux professionnels et commerciaux destinées à faciliter l'accueil des handicapés ; c. Les impositions, autres que celles incombant normalement à l'occupant, perçues, à raison desdites propriétés ; d. Les intérêts de dettes contractées pour la conservation, l'acquisition, la construction, la réparation ou l'amélioration des propriétés ; e. Une déduction forfaitaire (...) représentant les frais de gestion, l'assurance et l'amortissement » ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la SCI La Veillère a repris des aménagements et agencements réalisés par les locataires à l'intérieur des locaux loués en leur remboursant les emprunts qu'ils avaient contractés à cet effet ou en souscrivant elle-même des emprunts pour les dédommager de leurs dépenses ; que, contrairement à ce que soutient le requérant, ces aménagements et agencements, qui consistaient principalement en l'installation de l'électricité, du chauffage, du téléphone et de la distribution d'eau, ne présentaient aucune spécificité attachée à l'activité de garage ; qu'ainsi, en jugeant que les dépenses exposées par la SCI La Veillère pour dédommager ses locataires avaient eu pour effet d'accroître l'actif immobilisé de la SCI à raison de la reprise de ces aménagements et agencements et n'étaient donc pas déductibles du revenu, sans être assimilables à des indemnités d'éviction versées pour obtenir la libération des lieux et donner ceux-ci en location à des conditions plus avantageuses, la cour n'a pas entaché d'erreur de droit son arrêt, qui est suffisamment motivé ; <br/>
<br/>
              Sur les intérêts d'emprunts dont la déduction a été refusée :<br/>
<br/>
              Considérant que c'est par une appréciation souveraine, exempte de dénaturation, que la cour a jugé, par un arrêt suffisamment motivé, que M. A n'établissait pas que les intérêts réintégrés dans les résultats de la SCI La Veillère au titre des années 1997 à 1999 correspondaient à un emprunt contracté en vue d'améliorer les locaux qu'elle donnait en location ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que M. A n'est pas fondé à demander l'annulation de l'arrêt attaqué ; que ses conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative doivent être en conséquence rejetées ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. A est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. Christian A et au ministre du budget, des comptes publics et de la fonction publique.<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
