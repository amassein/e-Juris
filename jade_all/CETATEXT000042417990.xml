<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042417990</ID>
<ANCIEN_ID>JG_L_2020_10_000000444801</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/41/79/CETATEXT000042417990.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 02/10/2020, 444801, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-10-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>444801</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:444801.20201002</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au juge des référés du tribunal administratif de Melun, statuant sur le fondement de l'article L. 521-4 du code de justice administrative, de modifier l'article 2 de l'ordonnance du juge des référés du tribunal administratif de Melun n° 2005489 en date du 24 juillet 2020 en enjoignant au préfet de Seine-et-Marne de procéder à l'enregistrement de sa demande d'asile en procédure normale et de lui délivrer une attestation de demande d'asile dans des conditions lui permettant de solliciter l'asile auprès de l'Office français de protection des réfugiés et apatrides (OFPRA) dans un délai de quarante-huit heures à compter de la notification de l'ordonnance, sous astreinte de 100 euros par jour de retard. Par une ordonnance nos 2006416, 2006543 du 4 septembre 2020, le juge des référés du tribunal administratif de Melun a rejeté sa demande.<br/>
<br/>
              Par une requête, enregistrée le 22 septembre 2020 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-4 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) d'enjoindre à la préfecture de procéder à l'enregistrement de sa demande d'asile en procédure normale et de lui délivrer une attestation de demande d'asile dans des conditions lui permettant de solliciter l'asile auprès de l'OFPRA dans un délai de trois jours à compter de la notification de la présente ordonnance, sous astreinte de 50 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - la condition d'urgence est remplie dès lors que le refus du préfet de<br/>
Seine-et-Marne d'enregistrer sa demande d'asile en procédure normale le prive, d'une part, de la possibilité de justifier de la régularité de son séjour en France auprès de l'OFPRA et l'expose à tout moment à une mesure d'éloignement et, d'autre part, des conditions matérielles d'accueil, ce qui a pour effet de le placer dans une situation de grande précarité ;<br/>
               - il est porté une atteinte grave et manifestement illégale à son droit à l'asile en refusant d'enregistrer sa demande au motif qu'il serait en situation de fuite au sens de l'article 29 du règlement n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013 et du règlement complémentaire d'application n° 1560/2003, alors lors que, d'une part, il s'est rendu à l'ensemble des convocations qui lui ont été adressées par les services préfectoraux et, d'autre part, l'autorité préfectorale ne fournit aucune preuve de sa convocation du 16 janvier 2020, n'en fait pas référence dans sa requête introductive et ne produit pas la fiche de la direction générale des étrangers en France constatant la fuite du requérant ;<br/>
              - la décision contestée est entachée d'illégalité, dès lors que le préfet n'a pas rempli son obligation d'enregistrer la demande d'asile dans le délai prévu par les articles L. 741-1 et suivants du code de l'entrée et du séjour des étrangers et du droit d'asile.<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, et notamment son Préambule ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le règlement (CE) n° 1560/2003 de la Commission du 2 septembre 2003 portant modalités d'application du règlement (CE) n° 343/2003 du Conseil établissant les critères et mécanismes de détermination de l'Etat membre responsable de l'examen d'une demande d'asile présentée dans l'un des Etats membres par un ressortissant d'un pays tiers ;<br/>
              - la directive n° 2013/33/UE du Parlement européen et du Conseil du 26 juin 2013 établissant les normes pour l'accueil des personnes demandant la protection internationale ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". Aux termes de l'article L. 521-4 du même code : " Saisi par toute personne intéressée, le juge des référés peut, à tout moment, au vu d'un élément nouveau, modifier les mesures qu'il avait ordonnées ou y mettre fin ". Si l'exécution d'une ordonnance prise par le juge des référés, sur le fondement de l'article L. 521-2 du code de justice administrative, peut être recherchée dans les conditions définies par le livre IX du même code, et en particulier les articles L. 911-4 et L. 911-5, la personne intéressée peut également demander au juge des référés, sur le fondement de l'article L. 521-4 du même code, d'assurer l'exécution des mesures ordonnées demeurées sans effet par de nouvelles injonctions et une astreinte.<br/>
<br/>
              2. En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              3. Le droit constitutionnel d'asile, qui a le caractère d'une liberté fondamentale, a pour corollaire le droit de solliciter le statut de réfugié. S'il implique que l'étranger qui sollicite la reconnaissance de la qualité de réfugié soit, en principe, autorisé à demeurer sur le territoire jusqu'à ce qu'il ait été statué sur sa demande, ce droit s'exerce dans les conditions définies par le code de l'entrée et du séjour des étrangers et du droit d'asile. L'article L. 742-3 de ce code prévoit que l'étranger dont l'examen de la demande d'asile relève de la responsabilité d'un autre Etat peut faire l'objet d'un transfert vers l'Etat qui est responsable de cet examen en application des dispositions du règlement n° 604/23 du Parlement européen et du Conseil du 26 juin 2013. Ce transfert peut avoir lieu pendant une période de six mois à compter de l'acceptation de la demande de prise en charge, susceptible d'être portée à dix-huit mois dans les conditions prévues à l'article 29 de ce règlement si la personne concernée prend la fuite. <br/>
<br/>
              4. Il résulte de l'instruction que M. B..., ressortissant gambien né le 11 septembre 2000, a déposé le 6 juin 2019 une demande d'asile auprès de la préfecture de Seine-et-Marne. Les autorités françaises, après consultation du fichier Eurodac faisant apparaître que M.  B... avait transité par l'Italie et y avait déposé ses empreintes avant d'entrer en France, ont saisi les autorités italiennes d'une demande de reprise en charge du requérant, en application du règlement (UE) n° 604/2013 du 26 juin 2013. Le 24 juillet 2019, le préfet de Seine-et-Marne a pris un arrêté portant remise de M. B... aux autorités italiennes en sa qualité de demandeur d'asile, assorti d'une assignation à résidence. Le recours formé par ce dernier contre cette décision devant le tribunal administratif de Melun a été rejeté le 20 août 2019. M. B... s'est présenté à la préfecture de Seine-et-Marne le 21 juillet 2020 afin de faire enregistrer sa demande d'asile en France selon la procédure normale, en faisant valoir que le délai de six mois à compter du jugement mentionnée ci-dessus du tribunal administratif était expiré depuis le 20 février 2020. Cette demande a été rejetée. Saisi par le requérant, le juge des référés du tribunal de Melun a, par une ordonnance du 24 juillet 2020, sur le fondement de l'article L. 521-2 du code de justice administrative, enjoint au préfet de Seine-et-Marne de procéder à l'enregistrement de la demande d'asile de M. B... en procédure normale et de lui délivrer une attestation de demande d'asile dans des conditions lui permettant de solliciter l'asile auprès de l'Office français de protection des réfugiés et apatrides dans un délai de dix jours à compter de la notification de cette ordonnance. Le préfet a demandé, sur le fondement de l'article L. 521-4 du code précité, au juge des référés de mettre fin à cette injonction, le requérant lui demandant, sur le même fondement, de réduire le délai d'injonction de quinze jours à quarante-huit heures et d'assortir cette injonction d'une astreinte de 100 euros par jour de retard. Le requérant relève appel de l'ordonnance du 4 septembre 2020 par laquelle le juge des référés du tribunal administratif de Melun, statuant sur le fondement de l'article L. 521-4 du code de justice administrative, a mis fin à l'injonction figurant à l'article 2 de son ordonnance du 24 juillet 2020.<br/>
<br/>
              5. Le juge des référés du tribunal administratif de Melun a rejeté la requête de M. B... et mis fin à l'injonction prévue à l'article 2 de son ordonnance du 24 juillet 2020 au motif qu'en convoquant l'intéressé le 16 janvier 2020 pour une exécution de son arrêté de transfert, le préfet n'avait pas dépassé le délai de six mois imparti pour procéder à l'exécution de cet arrêté, délai qui expirait le 20 février 2020 et que, par suite, M. B... ne s'étant pas rendu à la convocation du 16 janvier 2020 et ayant ainsi fait obstacle à l'exécution de la mesure de transfert, il s'est placé lui-même dans une situation qui ne lui permet plus d'invoquer l'urgence. Il résulte en effet de l'instruction que M. B..., convoqué le 15 novembre 2019 et informé à cette même date de l'intention de procéder à son transfert aux autorités italiennes par la réservation d'un vol à destination de cet Etat, s'est de nouveau présenté aux services de la préfecture de Seine-et-Marne le 16 décembre 2019, date à laquelle la date et l'heure du vol de transfert en Italie était déjà connue. Dès lors, en ne se rendant pas à la convocation prévue la veille de ce vol, sans motif valable, M. B... doit être regardé comme ayant fait volontairement échec, par son comportement, à la mesure de transfert. Par suite, sans qu'il soit besoin d'examiner les autres moyens de la requête, M. B... n'est pas fondé à demander l'annulation de l'ordonnance du 4 septembre 2020 du juge des référés du tribunal administratif de Melun ayant rejeté sa demande pour défaut d'urgence. <br/>
<br/>
              6. Il résulte de tout ce qui précède qu'il est manifeste que l'appel de M. B... ne peut être accueilli. Il y a lieu par suite de rejeter sa requête, y compris les conclusions présentées au titre de l'article 37 de la loi du 10 juillet 1991 et de l'article L. 761-1 du code de justice administrative, selon la procédure prévue par l'article L. 522-3 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. B... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
