<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038498637</ID>
<ANCIEN_ID>JG_L_2019_05_000000429738</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/49/86/CETATEXT000038498637.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 17/05/2019, 429738, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-05-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>429738</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX ; SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2019:429738.20190517</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 13 avril 2019 et 14 mai 2019, au secrétariat du contentieux du Conseil d'Etat, MM. C... F..., B...A...et E...D...demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution de la décision implicite de refus d'abrogation de l'article D. 211-17 du code de la sécurité intérieure en tant qu'il prévoit que les grenades GLI F4 peuvent être utilisées pour le maintien de l'ordre public ;<br/>
<br/>
              2°) d'enjoindre au ministre de l'intérieur de procéder au réexamen de la demande tendant à l'abrogation des dispositions précitées, sous astreinte de 500 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Ils soutiennent que :<br/>
              - le Conseil d'Etat est compétent ;<br/>
              - ils disposent d'un intérêt à agir ;<br/>
              - l'urgence résulte de ce que les grenades GLI F4, dont la dangerosité est reconnue par le ministre de l'intérieur qui a annoncé leur remplacement à venir par des armes ne contenant pas d'explosifs, sont employées par les forces de l'ordre tous les samedis, à l'occasion de chaque manifestation des " gilets jaunes " ;<br/>
              - elle résulte également de ce que l'usage des grenades GLI F4 les expose à une menace pour leur intégrité physique, de nature à les dissuader d'exercer leur liberté de manifester ;<br/>
              - il existe un doute sérieux quant à la légalité du refus d'abrogation de l'article D. 211-17 du code de la sécurité intérieure, résultant de l'atteinte ni nécessaire, ni adaptée, ni proportionnée portée au droit de ne pas être soumis à des traitements inhumains ou dégradants, au droit au respect de la vie, au droit au respect de la dignité de la personne humaine, et à la liberté de manifestation ;<br/>
              - les grenades GLI F4 sont des armes de guerre, dont la dangerosité, d'une part, résulte de l'effet de souffle et de la projection de débris pouvant mutiler les manifestants et du risque d'atteindre des personnes extérieures à l'attroupement, et d'autre part, est démontrée par les blessures et mutilations subies par des manifestants et est admise par les autorités publiques ;<br/>
              - la France est le seul pays européen à en faire usage et s'est déjà engagée sur la voie de leur retrait ;<br/>
              - il existe par ailleurs des alternatives moins dangereuses.<br/>
              Par un mémoire en défense, enregistré le 9 mai 2019, le ministre de l'intérieur conclut au rejet de la requête de MM. F... et autres. Il soutient qu'il n'y a pas urgence à suspendre la décision attaquée, au sens de l'article L. 521-1 du code de justice administrative, et qu'il n'existe pas de doute sérieux quant à sa légalité.<br/>
<br/>
              Par une intervention, enregistrée le 15 mai 2019, la Ligue des droits de l'homme demande au Conseil d'Etat de faire droit à la requête de MM. F... et autres.<br/>
<br/>
              Par une intervention, enregistrée le 16 mai 2019, l'association " Action des chrétiens pour l'abolition de la torture " (ACAT) demande au Conseil d'Etat de faire droit à la requête de MM. F... et autres.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, MM. F..., A...et D...et, d'autre part, le ministre de l'intérieur ;<br/>
<br/>
              Vu le procès-verbal de l'audience publique du jeudi 16 mai 2019 à 13 heures 30 au cours de laquelle ont été entendus :<br/>
<br/>
              - Maître Duhamel, avocat au Conseil d'Etat et à la Cour de cassation, avocat de MM. F... et autres ;<br/>
<br/>
              - M.F... ;<br/>
<br/>
              - M.D... ;<br/>
<br/>
              - les représentants des requérants ;<br/>
<br/>
              - les représentants du ministre de l'intérieur ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 16 mai 2019, présentée par M. F...et autres ;<br/>
<br/>
<br/>
<br/>
<br/>
Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". Il résulte de ces dispositions que le prononcé de la suspension d'un acte administratif est subordonné notamment à une condition d'urgence. L'urgence justifie la suspension de l'exécution d'un acte administratif lorsque celui-ci porte atteinte de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre. Il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte contesté sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue.<br/>
<br/>
              2. Aux termes de l'article 431-3 du code pénal : " Constitue un attroupement tout rassemblement de personnes sur la voie publique ou dans un lieu public susceptible de troubler l'ordre public. / Un attroupement peut être dissipé par la force publique après deux sommations de se disperser restées sans effet adressées dans les conditions et selon les modalités prévues par l'article L. 211-9 du code de la sécurité intérieure ". L'article L. 211-9 du code de la sécurité intérieure renvoie à un décret en Conseil d'Etat le soin de fixer les conditions d'usage des armes à feu pour le maintien de l'ordre public. Aux termes de l'article R. 211-13 du même code : " L'emploi de la force par les représentants de la force publique n'est possible que si les circonstances le rendent absolument nécessaire au maintien de l'ordre public dans les conditions définies par l'article L 211-9. La force déployée doit être proportionnée au trouble à faire cesser et son emploi doit prendre fin lorsque celui-ci a cessé ". Aux termes de son article R. 211-18 : " Sans préjudice des articles 122-5 et 122-7 du code pénal, peuvent être utilisées dans les deux cas prévus au sixième alinéa de l'article L. 211-9 du présent code (...) les armes à feu des catégories A, B et C adaptées au maintien de l'ordre correspondant aux conditions de ce sixième aliéna, entrant dans le champ d'application de l'article R. 311-2 et autorisées par décret ". Il résulte des dispositions de l'article R. 211-19 du code de la sécurité intérieure que l'arme à feu dénommée " Grenade GLI F4 ", qui constitue une arme de catégorie A 2 visée par les 5° et 6° de l'article R. 311-2 du même code, est susceptible d'être utilisée par les représentants de la force publique pour le maintien de l'ordre public en application de l'article R. 211-18 de ce code. Enfin, en vertu de l'article L. 435-1 du code de la sécurité intérieure, les agents de la police nationale et les militaires de la gendarmerie peuvent faire usage de leurs armes " en cas d'absolue nécessité et de manière proportionnée " dans les cas mentionnés à cet article et à l'article L. 211-9 précité du même code.<br/>
<br/>
              3. La Ligue des droits de l'homme et l'association " Action des chrétiens pour l'abolition de la torture " justifient d'un intérêt suffisant à la suspension demandée par les requérants. Par suite, leurs interventions sont recevables et doivent être admises.<br/>
<br/>
              4. Au cours des débats à l'audience, messieurs F...et autres ont indiqué que le doute sérieux quant à la légalité du refus de retrait des dispositions dont ils demandent suspension résulte uniquement du moyen tiré de ce que, par sa dangerosité intrinsèque, la grenade GLI F4 est une arme dont l'autorisation d'emploi ne peut que méconnaître l'interdiction d'infliger des traitements inhumains et dégradants, le principe de dignité, le droit à la vie et la liberté de manifester, quelles que soient les dispositions légales et réglementaires en régissant l'usage par les forces de l'ordre.<br/>
<br/>
              5. Les caractéristiques intrinsèques d'une arme peuvent conduire à la considérer comme insusceptible d'un emploi quelconque, notamment dans le cadre de conventions internationales. Mais pour la plupart des armes, même plus dangereuses que celles considérées, car, contrairement à elles, visant à infliger des blessures, comme les armes à feu, en dotation au sein des forces de l'ordre, leurs conditions d'emploi sont telles qu'elles permettent de cantonner leur usage à des circonstances dans lesquelles leur dangerosité, maîtrisée, est adaptée aux fins poursuivies. <br/>
<br/>
              6. En l'espèce, la dangerosité de l'arme, attestée par des cas de blessures résultant de son explosion, n'est pas telle qu'une procédure rigoureuse d'emploi ne puisse la maîtriser et permettre de regarder son emploi comme adapté aux objectifs poursuivis si ceux-ci sont eux-mêmes choisis en fonction des effets potentiels, y compris ceux non recherchés par leur emploi. En s'abstenant de critiquer les dispositions légales et réglementaires restrictives limitant les circonstances d'usage à des cas précis, l'emploi à des procédures préalables, par des personnels dûment encadrés et formés à cette fin, les requérants ne permettent pas de regarder le moyen tiré de ce que les caractéristiques de la grenade GLI F4 ne pourraient qu'entraîner son illégalité quelles que soient les restrictions apportées à l'usage de cette arme. Au demeurant, l'ensemble des conditions d'usages, des restrictions et procédures qu'elles comportent, complétées par les instructions du ministère produites aux débats, ne permettent en rien, en l'état de l'instruction, de regarder ces conditions d'emploi comme insuffisantes ou inadaptées à la dangerosité de l'arme, ni par suite le refus de retirer l'autorisation d'usage dont la suspension est demandée comme affectée d'un doute sérieux sur sa légalité. Ni la circonstance que l'Etat ait renoncé à renouveler l'achat de ce type d'arme, qui ne résulte que de l'arrêt de la fabrication de celle-ci, ni celle tirée de ce que l'arme ne sera pas remplacée, ni celle que d'autres Etats ne l'aient pas employée, n'ont d'incidence sur cette appréciation.<br/>
<br/>
              7. L'ACAT, dans son mémoire en intervention, conteste cependant les conditions d'emploi des grenades GLI F4. En soutenant sans l'établir ni même soutenir cette argumentation d'éléments de fait, que la formation des personnels est insuffisante, et que la formule de sommation préalable à l'emploi de ces armes ou les conditions de son prononcé sont entachées d'incertitude sur leur portée ou leur compréhension par les manifestants, l'association borne sa critique à des contestations de principe qui ne permettent pas plus de regarder le moyen tiré de l'insuffisante rigueur des conditions réglementaires d'emploi comme jetant un doute sérieux sur la légalité de la disposition dont suspension est demandée.<br/>
<br/>
              8. Ainsi, à supposer que l'autre condition d'intervention du juge des référés, relative à l'urgence, soit établie, alors que les trois requérants n'ont pu indiquer en quoi le refus de suspension lésait de manière immédiate un intérêt personnel, notamment en les mettant personnellement en danger, n'indiquant en rien dans quelle circonstance ils seraient susceptibles de subir les conséquences de l'emploi effectif de ces grenades, au delà d'indications vagues et de circonstances sur la possibilité de participer à des manifestations susceptibles d'entraîner leur emploi, complétées par une note en délibéré qui pour l'un d'entre eux fait état opportunément mais tardivement d'une intention de manifester sur un parcours qui serait déclaré et dont rien n'indique qu'il pourrait comporter à ses abords un attroupement nécessitant l'emploi de l'arme, il y a lieu, faute de doute sérieux sur la légalité du refus attaqué, de rejeter la demande de suspension et, par suite, les conclusions à fin d'injonction et de versement d'une somme sur le fondement de L. 761-1, le ministre n'étant pas la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de MM. F..., A...et D...est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à MM. C... F..., B...A...et E...D...et au ministre de l'intérieur.<br/>
Copie en sera adressée au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
