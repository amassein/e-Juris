<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033551445</ID>
<ANCIEN_ID>JG_L_2016_12_000000387667</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/55/14/CETATEXT000033551445.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 07/12/2016, 387667, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-12-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>387667</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP COUTARD, MUNIER-APAIRE ; SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR>M. Bastien Lignereux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:387667.20161207</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Electric Industrie a demandé au tribunal administratif de Clermont-Ferrand de condamner la société Electricité de France (EDF) à lui verser la somme de 4 070 836 euros en réparation des conséquences dommageables de la méconnaissance de l'obligation d'achat d'énergie électrique mise à sa charge ainsi que les intérêts au taux légal à compter du 28 décembre 2007. Par un jugement n° 0800579 du 5 juin 2009, le tribunal administratif de Clermont-Ferrand a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 09LY01892 du 7 avril 2011, la cour administrative d'appel de Lyon a rejeté l'appel formé contre ce jugement par Me C...A..., agissant en qualité de mandataire judiciaire de la société Electric Industrie.<br/>
<br/>
              Par une décision n° 349933 du 28 mars 2013, le Conseil d'Etat, statuant au contentieux, a annulé cet arrêt et renvoyé l'affaire à la cour administrative d'appel de Lyon.<br/>
<br/>
              Par un arrêt n° 13LY00943 du 27 janvier 2015, la cour administrative d'appel de Lyon, après avoir prescrit, par un arrêt avant dire droit du 28 novembre 2013, une expertise visant à déterminer le montant du préjudice subi par la société Electric Industrie, a annulé le jugement du tribunal administratif de Clermont-Ferrand du 5 juin 2009 et condamné la société EDF à verser à Me A... la somme de 4 924 023 euros, outre intérêts au taux légal à compter du 1er novembre 2014, et mis les dépens à la charge de cette société.<br/>
<br/>
              Par un pourvoi, un nouveau mémoire et un mémoire en réplique, enregistrés les 4 février et 8 octobre 2015 et le 11 février 2016 au secrétariat du contentieux du Conseil d'Etat, la société EDF demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de MeA... ;<br/>
<br/>
              3°) de mettre à la charge de Me A...la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 46-628 du 8 avril 1946 ;<br/>
              - le décret n° 55-662 du 20 mai 1955 ;<br/>
              - l'arrêté du 23 janvier 1995 du ministre de l'industrie, des postes et télécommunications et du commerce extérieur ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bastien Lignereux, auditeur, <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Coutard, Munier-Apaire, avocat de la société Electricité de France et à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de la société Electric Industrie et de M.B.... Petavy ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 22 novembre 2016, présentée par la société EDF ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. En application de l'article 1er du décret du 20 mai 1955 réglant les rapports entre les établissements visés par les articles 2 et 23 de la loi du 8 avril 1946 et les producteurs autonomes d'énergie électrique, la société Electricité de France (EDF) et les entreprises locales de distribution ont l'obligation d'acheter l'électricité produite par certaines installations mentionnées à l'article 8 de cette loi. Par un arrêté du 23 janvier 1995, le ministre chargé de l'énergie a suspendu cette obligation d'achat pour une durée de trois ans. La SARL Electric Industrie a présenté le 19 janvier 1994, en vue de la construction d'une installation de production d'électricité, une demande de raccordement et de contrat d'achat à la société EDF, qui l'a rejetée le 6 février 1995 au motif que la suspension décidée par l'arrêté du 23 janvier 1995 lui était applicable. La société Electric Industrie lui a ensuite demandé la réparation du préjudice qu'elle estime avoir subi en raison du délai anormal d'examen de sa demande. Par un arrêt du 27 janvier 2015, la cour administrative d'appel de Lyon, après avoir prescrit, par un arrêt avant dire droit du 28 novembre 2013, une expertise visant à déterminer le montant du préjudice subi par la société Electric Industrie, a condamné la société EDF à verser à Me A..., agissant en qualité de mandataire judiciaire de la société Electric Industrie, la somme de 4 924 023 euros, majorée des intérêts au taux légal à compter du 1er novembre 2014. La société EDF se pourvoit en cassation contre cet arrêt.<br/>
<br/>
              2. En premier lieu, la décision d'une juridiction qui a statué en dernier ressort, même si elle peut faire l'objet ou fait effectivement l'objet d'un pourvoi en cassation, est revêtue de l'autorité de la chose jugée. Il en va ainsi notamment des motifs d'un arrêt avant dire droit ayant tranché définitivement une question. Dès lors, la cour n'a pas commis d'erreur de droit en jugeant que la société EDF ne pouvait utilement contester l'existence d'un préjudice subi par la société Electric Industrie, qui avait été reconnue par la cour dans son arrêt avant dire droit du 28 novembre 2013 par un motif qui en constitue le soutien nécessaire et alors même que cet arrêt, faisant l'objet d'un pourvoi en cassation, n'était pas irrévocable. Elle n'a pas non plus commis d'erreur de droit en opposant d'office aux prétentions de la société EDF la chose jugée par cet arrêt avant dire droit.<br/>
<br/>
              3. En second lieu, pour demander l'annulation de l'arrêt qu'elle attaque, la société EDF soutient pour le surplus que la cour a commis une erreur de droit en écartant son moyen tiré de l'absence de caractère certain du préjudice invoqué par la société Electric Industrie et entaché son arrêt de contradictions de motifs s'agissant de l'existence d'un préjudice financier subi par la société Electric Industrie et que son arrêt doit être annulé par voie de conséquence en cas d'annulation de l'arrêt avant dire droit qu'elle a rendu. Aucun de ces moyens n'est de nature à justifier l'annulation de l'arrêt attaqué.<br/>
<br/>
              4. Il résulte de tout ce qui précède que la société EDF n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de Me A... qui n'est pas, dans la présente instance, la partie perdante. Il y a lieu en revanche de mettre à ce titre à la charge de la société EDF une somme de 3 500 euros à verser à Me A...agissant en qualité de mandataire judiciaire de la société Electric Industrie.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société EDF est rejeté.<br/>
Article 2 : La société EDF versera à Me A...agissant en qualité de mandataire judiciaire de la société Electric Industrie une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société EDF et à MeA....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
