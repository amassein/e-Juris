<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034879210</ID>
<ANCIEN_ID>JG_L_2017_06_000000400565</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/87/92/CETATEXT000034879210.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 07/06/2017, 400565, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-06-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>400565</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>Mme Isabelle Lemesle</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:400565.20170607</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Bordeaux d'annuler pour excès de pouvoir la décision par laquelle l'agence régionale de santé (ARS) d'Aquitaine a implicitement refusé de lui transmettre les documents demandés dans son courrier du 17 janvier 2015 et d'enjoindre à l'agence de lui transmettre les documents non encore produits. Par un jugement n° 1502242 du 8 mars 2016, le magistrat désigné par le tribunal administratif de Bordeaux a, d'une part, prononcé un non lieu à statuer sur les conclusions en annulation et en injonction concernant les décisions portant autorisation de pratiquer l'activité de soins de psychiatrie, délivrées au centre hospitalier Charles Perrens au cours des années 2000 à 2010, et, d'autre part, rejeté le surplus des conclusions de la requête.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés au secrétariat du contentieux du Conseil d'Etat les 10 juin 2016 et 12 septembre 2016, M. B...demande au Conseil d'Etat : <br/>
<br/>
              1) d'annuler ce jugement, en tant qu'il lui fait grief ;<br/>
<br/>
              2) de mettre à la charge de l'ARS d'Aquitaine la somme de 2 000 euros, à verser à la SCP Hélène Didier et François Pinet, avocat de M.B..., au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - la loi n° 78-753 du 17 juillet 1978 ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ; <br/>
              - le décret n° 2005-1755 du 30 décembre 2005 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Isabelle Lemesle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de M. A...B...;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par lettre du 17 janvier 2015, M. B...a demandé à l'agence régionale de santé (ARS) d'Aquitaine, la copie de " l'autorisation d'exploitation " en vigueur en 2007 pour l'unité dite " Lescure " et notamment la partie " Lescure 2 " de l'hôpital spécialisé " Charles Perrens " de Bordeaux telle qu'elle était en vigueur en 2006 et 2007, ainsi que d'un certain nombre d'autres documents. Ayant porté le litige, le 19 mai 2015, devant le tribunal administratif de Bordeaux après que la commission d'accès aux documents administratifs (CADA) a rendu le 7 mai 2015 un avis favorable "  si les documents sollicités existent ", M.B..., qui a reçu en cours d'instance les documents sollicités, à l'exception de ceux concernant le mode d'exploitation de la partie " Lescure 2 ", se pourvoit en cassation contre le jugement du tribunal administratif de Bordeaux du 8 mars 2016 en tant qu'il a rejeté les conclusions de sa requête relatives au refus de communiquer les documents précisant le type de fonctionnement correspondant à l'unité " Lescure " et à la partie " Lescure 2 ", ainsi que le type de fonctionnement de toutes les unités de soins du centre hospitalier Charles Perrens.<br/>
<br/>
              2. L'article 1er de la loi du 17 juillet 1978 portant diverses mesures d'amélioration des relations entre l'administration et le public et diverses dispositions d'ordre administratif, social et fiscal, aujourd'hui codifiée aux articles L. 300-1 et suivants du code des relations entre le public et l'administration, dispose que sont considérés comme documents administratifs les documents produits ou reçus, dans le cadre de leur mission de service public, par l'Etat, les collectivités territoriales ainsi que par les autres personnes de droit public ou les personnes de droit privé chargées d'une telle mission. Aux termes de l'article 2 de cette loi : " (...) Lorsqu'une administration mentionnée à l'article 1er est saisie d'une demande de communication portant sur un document administratif qu'elle ne détient pas mais qui est détenu par une autre administration mentionnée au même article, elle la transmet à cette dernière et en avise l'intéressé (...) ". Il résulte de ces dispositions que, lorsqu'une autorité administrative est saisie d'une demande de communication portant sur un document administratif qu'elle ne détient pas et qu'elle estime être détenu par une autre autorité administrative, elle est tenue de la transmettre à cette dernière et d'en aviser l'intéressé. <br/>
<br/>
              3. L'article 17 du décret du 30 décembre 2005 relatif à la liberté d'accès aux documents administratifs et à la réutilisation des informations publiques, aujourd'hui codifié aux articles R. 311-12 et R. 311-13 du code des relations entre le public et l'administration, prévoit que le silence gardé pendant plus d'un mois par l'autorité compétente, saisie d'une demande de communication de documents en application de l'article 2 de la loi du 17 juillet 1978 cité au point 2, vaut décision de refus et que l'intéressé dispose d'un délai de deux mois à compter de la notification du refus ou de l'expiration du délai fixé au premier alinéa pour saisir la CADA. Aux termes de l'article 19 de ce décret aujourd'hui codifié aux articles R. 343-3 et suivants du code des relations entre le public et l'administration : " (...) Le silence gardé par l'autorité mise en cause pendant plus de deux mois à compter de l'enregistrement de la demande de l'intéressé par la commission vaut confirmation de la décision de refus ". A l'issue des délais fixés par ces dispositions, dont le premier court à compter de la date de sa réception par l'administration initialement saisie, la demande est réputée avoir été implicitement rejetée par l'administration qui détient le document en cause, que cette demande lui ait été ou non transmise. L'intéressé dispose alors d'un délai de deux mois pour demander l'annulation de cette décision devant le juge de l'excès de pouvoir. <br/>
<br/>
              4. Pour rejeter la demande de M. B...qui se prévalait pourtant de l'article 2 de la loi du 17 juillet 1978, le magistrat désigné par le tribunal administratif de Bordeaux s'est borné à juger, après avoir relevé que l'ARS d'Aquitaine faisait valoir qu'elle ne disposait pas des documents litigieux dans la mesure où, pour l'accomplissement de leurs missions, les établissements de santé définissent librement leur organisation interne, en vertu des articles L. 146-1 et suivants, ainsi que D. 6146-1 et suivants du code de la santé publique,  que l'activité de la partie " Lescure 2 " n'étant pas rattachée à l'ARS d'Aquitaine, le requérant n'était pas fondé à reprocher à cette dernière de s'être abstenue de procéder à des investigations complémentaires auprès du centre hospitalier pour savoir s'il était effectivement détenteur des documents demandés. Il a, ce faisant, alors qu'il aurait du, conformément aux motifs énoncés au point 3, s'estimer saisi d'une décision émanant du centre hospitalier Charles Perrens, entaché son jugement d'une erreur de droit. <br/>
<br/>
              5. Il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, M. B...est fondé à demander l'annulation du jugement qu'il attaque. M. B... a obtenu le bénéfice de l'aide juridictionnelle. Par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a donc lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Hélène Didier et François Pinet renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat le versement à cette SCP, de la somme de 2 000 euros.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'article 2 du jugement du tribunal administratif de Bordeaux du 8 mars 2016 est annulé.<br/>
Article 2 : Le jugement de l'affaire est renvoyé dans cette mesure au tribunal administratif de Bordeaux.<br/>
Article 3 : L'ARS d'Aquitaine versera à la SCP Hélène Didier et François Pinet, avocat de M. B..., une somme de 2 000 euros sur le fondement des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que la SCP renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : La présente décision sera notifiée à M. A...B...et à la ministre des solidarités et de la santé.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
