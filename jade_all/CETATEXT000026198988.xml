<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026198988</ID>
<ANCIEN_ID>JG_L_2012_07_000000347874</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/19/89/CETATEXT000026198988.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  6ème sous-section jugeant seule, 16/07/2012, 347874, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-07-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>347874</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 6ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>Mme Christine Maugüé</PRESIDENT>
<AVOCATS>SCP LE BRET-DESACHE</AVOCATS>
<RAPPORTEUR>M. Bruno Chavanat</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:347874.20120716</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi, enregistré le 28 mars 2011 au secrétariat du contentieux du Conseil d'Etat, présenté par  le ministre de l'écologie, du développement durable, des transports et du logement ; le ministre  demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09BX01798 du 24 janvier 2011 par lequel la cour administrative d'appel de Bordeaux a, d'une part, annulé le jugement en date du 11 juin 2009 par lequel le tribunal administratif de Limoges a rejeté la demande de M. Jean-François A tendant à l'annulation des décisions du 27 mars 2006 et du 19 juillet 2006 du préfet de la Creuse relatives à la centrale hydroélectrique du " Mas de la Fille " et à ce qu'il soit déclaré que ladite centrale est d'une puissance maximale brute inférieure à 150 kilowatts et demeure autorisée sans limitation de durée conformément à son titre d'origine et que  le débit  réservé associé à son exploitation soit fixé à 2,5 % du débit moyen interannuel de la Mourne, d'autre part, a décidé que les installations de la centrale de " Mas de la Fille " ne sont pas soumises à autorisation et que le débit réservé qui leur est applicable est égal à 2,5 % du débit moyen interannuel de ce  cours jusqu'au 1er janvier 2014 ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appels ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'énergie ;<br/>
<br/>
              Vu le code de l'environnement ;<br/>
<br/>
              Vu la loi du 16 octobre 1919 relative à l'utilisation de l'énergie électrique ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Chavanat, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Le Bret-Desaché, avocat de M.  A ;<br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Le Bret-Desaché, avocat de M. A;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article 1er, alors en vigueur, de la loi du 16 octobre 1919 relative à l'utilisation de l'énergie électrique, désormais codifié à l'article L. 511-1 du code de l'énergie : " Nul ne peut disposer de l'énergie des marées, des lacs et des cours d'eau, quel que soit leur classement, sans une concession ou une autorisation de l'Etat " ; qu'en application de l'article 2 de la même loi, sont placées sous le régime de la concession les entreprises dont la puissance excède 4 500 kilowatts et sous le régime de l'autorisation les autres entreprises ; que, toutefois, l'article 29 de la loi, repris à l'article L. 511-4 de ce code, exempte les usines ayant une existence légale au jour de sa promulgation de la soumission à ces régimes ;<br/>
<br/>
              2. Considérant, en premier lieu, qu'un droit fondé en titre conserve la consistance qui était la sienne à l'origine ; que la détermination de la puissance fondée en titre s'opère au regard de la hauteur de la chute d'eau et du débit du cours d'eau ou du canal d'amenée et que ce débit doit être apprécié au niveau du vannage d'entrée ; que les modifications de l'ouvrage auquel est attaché un droit fondé en titre qui ont pour objet ou pour effet d'accroître la force motrice théoriquement disponible ont pour conséquence de soumettre l'installation au droit commun de l'autorisation ou de la concession pour la partie de la force motrice supérieure à la puissance fondée en titre ; <br/>
<br/>
              3. Considérant que pour juger que la consistance actuelle de la centrale hydroélectrique litigieuse devait être regardée comme étant celle dont disposait à l'origine le titulaire du droit fondé en titre du moulin du " Mas de la Fille ", la cour a estimé que le surcreusement du canal de restitution, dont le ministre soutient qu'il est intervenu après 1789 au moment de l'implantation d'une usine à papier sur le site, n'était pas de nature à entraîner une augmentation de la force motrice de l'installation ; qu'en jugeant que la profondeur du canal de restitution était par elle-même, sans incidence sur cette force motrice alors que le surcreusement a nécessairement eu pour effet d'augmenter la hauteur de la chute, la cour a, par un motif qui n'est pas surabondant,  entaché son arrêt d'une erreur de droit ; qu'il résulte de ce qui précède et sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, que le ministre est fondé à en demander l'annulation ; <br/>
<br/>
              4. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat  qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>
              			D E C I D E :<br/>
              			--------------<br/>
Article1er : L'arrêt de la  cour administrative d'appel de Bordeaux du 24 janvier 2011 est annulé.<br/>
Article 2 : L'affaire est renvoyée devant la cour administrative d'appel  de Bordeaux.<br/>
Article 3 : Les conclusions présentées par M. A au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la ministre de l'écologie, du développement durable et de l'énergie  et à  M Jean- François A.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
