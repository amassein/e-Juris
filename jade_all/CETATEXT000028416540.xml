<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028416540</ID>
<ANCIEN_ID>JG_L_2013_12_000000366165</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/41/65/CETATEXT000028416540.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère sous-section jugeant seule, 30/12/2013, 366165, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>366165</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP RICHARD</AVOCATS>
<RAPPORTEUR>Mme Julia Beurton</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:366165.20131230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>VU LA PROCEDURE SUIVANTE :<br/>
<br/>
 Procédure contentieuse antérieure<br/>
<br/>
              La SELARL Centre médical subaquatique et M. B...A...ont demandé au tribunal administratif de Marseille :<br/>
              - d'annuler pour excès de pouvoir la décision du directeur régional du travail, de l'emploi et de la formation professionnelle de Provence-Alpes-Côte-d'Azur du 3 décembre 2007 en tant qu'elle rejette la demande d'agrément de l'association Expertis au titre de la surveillance médicale des travailleurs soumis au risque hyperbare[j1], ainsi que la décision de la même autorité du 7 février 2008 et la décision du ministre du travail, des relations sociales, de la famille et de la solidarité du 6 juin 2008 rejetant les recours gracieux et hiérarchique formés par l'association contre cette décision ;<br/>
              - d'annuler pour excès de pouvoir la décision implicite née du silence gardé par le directeur régional du travail, de l'emploi et de la formation professionnelle de Provence-Alpes-Côte-d'Azur sur la demande de la même association tendant à ce qu'une dérogation lui soit accordée pour le suivi médical des personnels de l'Agence Travaux subaquatiques ; <br/>
              - d'enjoindre au directeur régional du travail, de l'emploi et de la formation professionnelle de Provence-Alpes-Côte-d'Azur de prendre une nouvelle décision.<br/>
<br/>
              Par un jugement n° 0805261 du 25 janvier 2011, le tribunal administratif de Marseille a rejeté la demande de la SELARL Centre médical subaquatique et de M.A....<br/>
<br/>
              Par un arrêt n° 11MA01122 du 18 décembre 2012, la cour administrative d'appel de Marseille a rejeté l'appel formé par la SELARL Centre médical subaquatique et par M. A...à l'encontre du jugement du tribunal administratif de Marseille du 25 janvier 2011. <br/>
<br/>
 Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 19 février et 21 mai 2013 au secrétariat du contentieux du Conseil d'Etat, la SELARL Centre médical subaquatique et M. A...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt n° 11MA01122 de la cour administrative d'appel de Marseille du 18 décembre 2012 ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Par un mémoire en défense, enregistré le 8 août 2013, le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social conclut au rejet du pourvoi.<br/>
<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Julia Beurton, Auditeur,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Richard, avocat de la SELARL Centre médical subaquatique et de M.A... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>CONSIDERANT CE QUI SUIT :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la SELARL Centre médical subaquatique réalise des expertises hyperbares pour l'association Expertis, qui a la qualité de service de santé au travail et assure notamment le suivi de travailleurs soumis au risque hyperbare en région Provence-Alpes-Côte-d'Azur, et que M.A..., médecin du travail, est gérant de cette société et salarié de l'association Expertis. Pour juger que ni cette société, ni M. A...n'étaient recevables à demander l'annulation pour excès de pouvoir de la décision du directeur régional du travail, de l'emploi et de la formation professionnelle de Provence-Alpes-Côte-d'Azur du 3 décembre 2007 rejetant la demande d'agrément au risque hyperbare de l'association Expertis et des décisions la confirmant sur recours gracieux puis hiérarchique, ainsi que de la décision implicite de rejet de la demande de l'association Expertis tendant à l'obtention d'une dérogation pour assurer le suivi médical des personnels de l'Agence Travaux subaquatiques située dans le Val-de-Marne, la cour administrative d'appel de Marseille a considéré qu'ils ne justifiaient pas d'un intérêt leur donnant qualité pour agir à l'encontre de ces décisions.<br/>
<br/>
              2. Toutefois, d'une part, en jugeant que la seule atteinte à ses intérêts commerciaux ne donnait pas à la SELARL Centre médical subaquatique, prestataire de services de l'association Expertis à laquelle elle était liée par convention, un intérêt lui donnant qualité pour demander l'annulation des décisions litigieuses, alors qu'un tel préjudice est susceptible de conférer un intérêt suffisamment direct et certain y compris pour agir à l'encontre de décisions de la nature de celles qui étaient attaquées, la cour a entaché son arrêt d'erreur de droit. D'autre part, en se bornant à estimer que M.A..., en sa qualité de salarié de l'association, ne justifiait pas d'un intérêt lui donnant qualité pour agir à l'encontre des décisions litigieuses, sans rechercher, comme l'y invitait le requérant, si ces décisions n'étaient pas de nature à affecter son activité dans des conditions telles qu'il devait être regardé comme justifiant d'un intérêt suffisamment direct et certain pour former un recours pour excès de pouvoir, la cour a insuffisamment motivé son arrêt et commis une erreur de droit. <br/>
<br/>
              3. Il résulte de ce qui précède que la SELARL Centre médical subaquatique et M. A...sont fondés à demander l'annulation de l'arrêt attaqué, sans qu'il soit besoin d'examiner l'autre moyen de leur pourvoi, tiré de ce que la cour a insuffisamment motivé son arrêt en tant qu'il concerne le défaut d'intérêt à agir de la SELARL Centre médical subaquatique. <br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 1 500 euros à verser, d'une part, à la SELARL Centre médical subaquatique et, d'autre part, à M. A...au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 18 décembre 2012 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
Article 3 : L'Etat versera à la SELARL Centre médical subaquatique et à M. A...la somme de 1 500 euros chacun au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la SELARL Centre médical subaquatique, à M. B... A...et au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
