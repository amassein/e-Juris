<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036205235</ID>
<ANCIEN_ID>JG_L_2017_12_000000400629</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/20/52/CETATEXT000036205235.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 13/12/2017, 400629</TITRE>
<DATE_DEC>2017-12-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>400629</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC/>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>HAAS ; SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>M. Yves Doutriaux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:400629.20171213</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une décision n° 374635 du 1er octobre 2015, le Conseil d'Etat, statuant au contentieux a, sur pourvoi de Mme A...B..., annulé l'arrêt n° 12MA04397 du 12 novembre 2013 par lequel la cour administrative d'appel de Marseille a confirmé le jugement n° 1101947 du tribunal administratif de Nîmes du 18 octobre 2012 rejetant la demande de Mme B...tendant à l'annulation de la décision du 14 avril 2011 par laquelle le président de La Poste a prononcé sa révocation.<br/>
<br/>
              Par un arrêt n° 15MA04012 du 12 avril 2016, la cour administrative d'appel de Marseille a annulé le jugement du tribunal administratif de Nîmes du 18 octobre 2012, a enjoint à La Poste de réintégrer Mme B...dans le poste qu'elle occupait ou, à défaut, dans un poste équivalent et de procéder à la reconstitution de sa carrière et de ses droits sociaux dans le délai de deux mois suivant la notification de l'arrêt et a condamné La Poste à verser à Mme B...une indemnité de 28 000 euros en réparation des préjudices qu'elle a subis.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 13 juin et 13 septembre 2016 et le 21 novembre 2017 au secrétariat du contentieux du Conseil d'Etat, La Poste demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de Mme B...; <br/>
<br/>
              3°) de mettre à la charge de Mme B...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - la loi n° 90-568 du 2 juillet 1990 ;<br/>
              - le décret n° 90-1111 du 12 décembre 1990 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yves Doutriaux, conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Haas, avocat de La Poste, et à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de Mme B...; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que MmeB..., fonctionnaire en activité à La Poste depuis 1979, conseiller financier au bureau de poste de Bollène depuis 1992, a fait l'objet, le 14 avril 2011, d'une sanction disciplinaire de révocation ; que La Poste se pourvoit en cassation contre l'arrêt du 12 avril 2016 par lequel la cour administrative d'appel de Marseille, sur renvoi du Conseil d'Etat après cassation, a, après avoir annulé le jugement du tribunal administratif de Nîmes du 18 octobre 2012 qui avait rejeté la demande présentée en première instance par Mme B...contre cette sanction, d'une part, prononcé l'annulation pour excès de pouvoir de cette décision, d'autre part, enjoint à La Poste de réintégrer Mme B...dans le poste qu'elle occupait ou, à défaut dans un poste équivalent, et de procéder à la reconstitution de sa carrière et de ses droits sociaux et, enfin, condamné La Poste à verser à Mme B...une indemnité de 28 000 euros en réparation des préjudices subis ;<br/>
<br/>
              2.	Considérant qu'il appartient au juge de l'excès de pouvoir, saisi de moyens en ce sens, de rechercher si les faits reprochés à un agent public ayant fait l'objet d'une sanction disciplinaire constituent des fautes de nature à justifier une sanction et si la sanction retenue est proportionnée à la gravité de ces fautes ; <br/>
<br/>
              3.	Considérant que la constatation et la caractérisation des faits reprochés à l'agent relèvent, dès lors qu'elles sont exemptes de dénaturation, du pouvoir souverain des juges du fond ; que le caractère fautif de ces faits est susceptible de faire l'objet d'un contrôle de qualification juridique de la part du juge de cassation ; que l'appréciation du caractère proportionné de la sanction au regard de la gravité des fautes commises relève, pour sa part, de l'appréciation des juges du fond et n'est susceptible d'être remise en cause par le juge de cassation que dans le cas où la solution qu'ils ont retenue quant au choix, par l'administration, de la sanction est hors de proportion avec les fautes commises ;<br/>
<br/>
              4.	Considérant qu'il ressort des énonciations de l'arrêt attaqué que la sanction de révocation prononcée à l'encontre de Mme B...était motivée par le fait qu'elle aurait signé à la place de sa cliente, une personne âgée résidant, depuis octobre 2007, au sein d'un établissement public d'hébergement pour personnes âgées dépendantes, deux demandes de rachat de contrats d'assurance-vie et une demande d'adhésion à un contrat d'assurance-vie, qu'elle aurait désigné comme bénéficiaires de ce nouveau contrat son fils et sa nièce, qu'elle aurait fait bénéficier son fils d'un prêt, de cadeaux et de libéralités de la part de sa cliente, qu'elle aurait elle-même reçu des cadeaux et gratifications, qu'elle aurait assuré de fait la gestion des comptes de sa cliente et qu'elle aurait organisé le transfert du patrimoine de la cliente au profit de son fils ; que la cour administrative d'appel de Marseille a jugé que ces faits étaient matériellement établis et présentaient le caractère de fautes disciplinaires de nature à justifier une sanction ; <br/>
<br/>
              5.	Considérant néanmoins qu'en se fondant sur la manière de servir de l'intéressée durant l'ensemble de sa carrière, sur l'absence de tout passé disciplinaire, sur ses qualités professionnelles et humaines, sur l'absence de volonté de nuire tant à sa cliente, avec laquelle elle entretenait une relation d'amitié, qu'à La Poste, la cour a estimé que la sanction de révocation était disproportionnée aux faits en cause ; que toutefois, eu égard à la gravité des manquements reprochés à MmeB..., La Poste est fondée à soutenir que l'appréciation portée sur ce point par la cour conduirait en cas de reprise de la procédure disciplinaire au prononcé d'une sanction qui serait, en raison de son caractère insuffisant, hors de proportion avec les fautes commises ; que, par suite, et sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, La Poste est fondée à demander l'annulation de l'arrêt qu'elle attaque ;<br/>
<br/>
              6.	Considérant qu'aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire " ; qu'il incombe au Conseil d'Etat de régler l'affaire au fond ;<br/>
<br/>
              7.	Considérant, en premier lieu, qu'il ressort des pièces du dossier que M. C..., directeur des opérations des ressources humaines, bénéficiait d'une délégation du Président directeur général de La Poste, régulièrement signée et publiée, l'habilitant à signer les décisions de sanction du quatrième groupe en cas d'absence ou d'empêchement du délégué général du groupe La Poste directeur des ressources humaines et des relations sociales du groupe ou du directeur délégué des ressources humaines et des relations sociales ; que la circonstance que cette délégation ne vise pas le décret du 12 décembre 1990 portant statut de La Poste est sans incidence sur sa légalité ; <br/>
<br/>
              8.	Considérant, en deuxième lieu, que si Mme B...soutient qu'elle n'a pas obtenu la communication intégrale de son dossier individuel au cours de la procédure disciplinaire, faute d'avoir eu accès au témoignage recueilli auprès d'un agent stagiaire dans le cadre de l'enquête la concernant, il ne ressort pas des pièces du dossier, en tout état de cause, que cette pièce aurait pu comporter des éléments utiles à sa défense autres que ceux qui figuraient dans l'attestation de ce même agent qu'elle avait elle-même produite devant le conseil de discipline ; que, dans ces conditions, le moyen tiré de ce que le dossier qui lui a été communiqué aurait été incomplet ne peut qu'être écarté ;<br/>
<br/>
              9.	Considérant, en troisième lieu, qu'il ressort des pièces du dossier que la cliente concernée par les faits reprochés à Mme B...a indiqué ne pas être en mesure, eu égard à son état de santé, de se déplacer à Paris, où s'est tenu le conseil de discipline ; que La Poste n'a pas refusé que le conseil de discipline l'entende, en qualité de témoin ; que le témoignage qu'elle a rédigé par écrit a été porté à la connaissance des membres du conseil de discipline ; que le moyen tiré de ce que les droits de la défense, faute pour cette cliente d'avoir pu être entendue, n'auraient pas été respectés au cours de la procédure disciplinaire doit, par suite, être écarté ;<br/>
<br/>
              10.	Considérant, en quatrième lieu, que la décision de révocation contestée énonce les éléments de droit et de faits sur lesquels elle se fonde, qu'elle est ainsi suffisamment motivée ;<br/>
<br/>
              11.	Considérant, en cinquième lieu, que le moyen tiré de l'absence de saisine du comité d'entreprise n'est pas assorti des précisions permettant d'en apprécier le bien-fondé ; <br/>
<br/>
              12.	Considérant, en sixième lieu, qu'il ne ressort pas des pièces du dossier que la révocation de Mme B...aurait procédé, non de la volonté de sanctionner les faits qui lui étaient reprochés, mais de celle de réorganiser le service en supprimant son poste de conseiller financier ;<br/>
<br/>
              13.	Considérant, en septième lieu, qu'il ressort des pièces du dossier que Mme B... a contrefait la signature de sa cliente, une personne âgée résidant dans un établissement public d'hébergement pour personnes âgées dépendantes, afin d'effectuer en son nom deux demandes de rachat de contrats d'assurance-vie et une demande d'adhésion à un nouveau contrat d'assurance-vie ; qu'elle a fait porter comme bénéficiaires de ce nouveau contrat son fils et sa nièce ; qu'elle a par ailleurs fait bénéficier son fils d'un prêt, de cadeaux et de libéralités de la part de sa cliente ; qu'elle a elle-même reçu des cadeaux et gratifications ; que, si elle fait valoir, pour justifier ces faits, les liens anciens, de nature amicale, qui l'unissaient à sa cliente et la circonstance qu'elle s'occupait d'elle depuis son placement, courant 2007, en résidence pour personnes âgées et avait été désignée par elle en qualité de " personne de confiance " sur le fondement de l'article L. 1111-6 du code de la santé publique, ces éléments ne sauraient permettre d'ôter aux actes qui lui sont reprochés, lesquels constituent un manquement à l'obligation de probité, leur caractère fautif ; que, par suite, ces actes, dont la matérialité est établie, doivent être regardés comme des fautes de nature à justifier le prononcé d'une sanction disciplinaire ;<br/>
<br/>
              14.	Considérant que les faits en cause, quel que soit le montant des prêts, cadeaux et gratifications reçus par Mme B...et son fils, sont, compte tenu de la nature des fonctions exercées par l'intéressée, de la nécessaire connaissance qu'elle avait de ses obligations professionnelles et de l'incidence que de tels agissements peuvent avoir pour la réputation de La Poste, d'une particulière gravité ; que les liens d'amitié allégués qui l'auraient unie à sa cliente et le fait qu'elle n'aurait commis les faits qui lui sont reprochés qu'à la demande expresse de cette dernière, ne sauraient atténuer la gravité de tels agissements ; que ni la circonstance que son supérieur hiérarchique, conseiller spécialisé en patrimoine chargé du portefeuille de la cliente, se soit abstenu de prendre en charge le dossier et lui ait remis un exemplaire vierge de contrat d'assurance-vie à soumettre à cette cliente alors même qu'elle n'était pas habilitée à proposer ce type de produit, ni l'absence de dissimulation alléguée vis-à-vis de sa hiérarchie, ni le fait qu'elle ait été affectée par une nouvelle préoccupante relative à sa santé le jour où elle a signé le contrat d'assurance-vie en contrefaisant la signature de sa cliente, ni enfin sa manière de servir et ses qualités humaines, son absence de volonté de nuire et de passé disciplinaire, ne sont davantage de nature à atténuer la gravité des fautes commises ; que, dans les circonstances de l'espèce, l'autorité disciplinaire n'a pas pris une sanction disproportionnée en révoquant l'intéressée de ses fonctions ; <br/>
<br/>
              15.	Considérant qu'il résulte de tout ce qui précède que Mme B...n'est pas fondée à se plaindre de ce que, par le jugement attaqué, le tribunal administratif de Nîmes a rejeté ses conclusions tendant à l'annulation de la sanction de révocation dont elle a fait l'objet ainsi que ses conclusions indemnitaires et à fin d'injonction ; que, par voie de conséquence, les conclusions que Mme B...présente au titre des frais exposés et non compris dans les dépens sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative doivent être rejetées ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de la requérante la somme que La Poste demande au même titre ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 12 avril 2016 est annulé.<br/>
Article 2 : La requête présentée par Mme B...devant la cour administrative d'appel de Marseille est rejetée.<br/>
Article 3 : Les conclusions présentées par La Poste et par Mme B...au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à La Poste et à Mme A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-13-01-03 FONCTIONNAIRES ET AGENTS PUBLICS. CONTENTIEUX DE LA FONCTION PUBLIQUE. CONTENTIEUX DE L'ANNULATION. POUVOIRS DU JUGE. - RECOURS POUR EXCÈS DE POUVOIR CONTRE UNE SANCTION DISCIPLINAIRE INFLIGÉE À UN AGENT PUBLIC - 1) CONTRÔLE DU JUGE DE CASSATION - A) MATÉRIALITÉ DES FAITS REPROCHÉS À L'AGENT - DÉNATURATION - B) CARACTÈRE FAUTIF - CONTRÔLE DE QUALIFICATION JURIDIQUE - C) SANCTION PRONONCÉE - VÉRIFICATION DE CE QUE LA SOLUTION RETENUE PAR LES JUGES DU FOND QUANT AU CHOIX DE LA SANCTION N'EST PAS HORS DE PROPORTION AVEC LES FAUTES COMMISES [RJ1] - 2) ESPÈCE - APPLICATION AU CAS OÙ LE JUGE DU FOND A ANNULÉ LA SANCTION DISCIPLINAIRE - CARACTÈRE HORS DE PROPORTION AVEC LES FAUTES COMMISES DE LA SANCTION QUI SERAIT PRISE EN CAS DE REPRISE DE LA PROCÉDURE DISCIPLINAIRE [RJ2] - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-08-02-02-01 PROCÉDURE. VOIES DE RECOURS. CASSATION. CONTRÔLE DU JUGE DE CASSATION. BIEN-FONDÉ. - RECOURS EN EXCÈS DE POUVOIR CONTRE UNE SANCTION DISCIPLINAIRE D'UN AGENT PUBLIC - 1) NATURE DU CONTRÔLE DU JUGE DE CASSATION - A) MATÉRIALITÉ DES FAITS REPROCHÉS À L'AGENT - DÉNATURATION - B) CARACTÈRE FAUTIF - CONTRÔLE DE QUALIFICATION JURIDIQUE - C) SANCTION PRONONCÉE - VÉRIFICATION DE CE QUE LA SOLUTION RETENUE PAR LES JUGES DU FOND QUANT AU CHOIX DE LA SANCTION N'EST PAS HORS DE PROPORTION AVEC LES FAUTES COMMISES [RJ1] - 2) ESPÈCE - APPLICATION AU CAS OÙ LE JUGE DU FOND A ANNULÉ LA SANCTION DISCIPLINAIRE - CARACTÈRE HORS DE PROPORTION AVEC LES FAUTES COMMISES DE LA SANCTION QUI SERAIT PRISE EN CAS DE REPRISE DE LA PROCÉDURE DISCIPLINAIRE [RJ2] - EXISTENCE.
</SCT>
<ANA ID="9A"> 36-13-01-03 1) a) La constatation et la caractérisation des faits reprochés à l'agent relèvent, dès lors qu'elles sont exemptes de dénaturation, du pouvoir souverain des juges du fond.... ,,b) Le caractère fautif de ces faits est susceptible de faire l'objet d'un contrôle de qualification juridique de la part du juge de cassation.... ,,c) L'appréciation du caractère proportionné de la sanction au regard de la gravité des fautes commises relève, pour sa part, de l'appréciation des juges du fond et n'est susceptible d'être remise en cause par le juge de cassation que dans le cas où la solution qu'ils ont retenue quant au choix, par l'administration, de la sanction est hors de proportion avec les fautes commises.,,,2) Fonctionnaire de la Poste ayant contrefait la signature d'une personne âgée résidant dans un établissement public d'hébergement pour personnes âgées dépendantes, afin d'effectuer au nom de cette personne deux demandes de rachat de contrats d'assurance-vie et une demande d'adhésion à un nouveau contrat d'assurance-vie, ayant fait porter comme bénéficiaires de ce nouveau contrat son fils et sa nièce, ayant fait bénéficier son fils de prêts, cadeaux et libéralités de la part de cette personne âgée, et ayant lui-même reçu des cadeaux et gratifications. Cour ayant jugé que les faits ayant motivé une sanction de révocation du fonctionnaire sont matériellement établis et présentent le caractère de fautes disciplinaires de nature à justifier une sanction, mais estimant que la sanction est disproportionnée aux faits en cause. Eu égard à la gravité des manquements reprochés à l'intéressé, l'appréciation de la cour conduirait en cas de reprise de la procédure disciplinaire au prononcé d'une sanction qui serait, en raison de son caractère insuffisant, hors de proportion avec les fautes commises. Par suite, annulation de l'arrêt.</ANA>
<ANA ID="9B"> 54-08-02-02-01 1) a) La constatation et la caractérisation des faits reprochés à l'agent relèvent, dès lors qu'elles sont exemptes de dénaturation, du pouvoir souverain des juges du fond.... ,,b) Le caractère fautif de ces faits est susceptible de faire l'objet d'un contrôle de qualification juridique de la part du juge de cassation.... ,,c) L'appréciation du caractère proportionné de la sanction au regard de la gravité des fautes commises relève, pour sa part, de l'appréciation des juges du fond et n'est susceptible d'être remise en cause par le juge de cassation que dans le cas où la solution qu'ils ont retenue quant au choix, par l'administration, de la sanction est hors de proportion avec les fautes commises.,,,2) Fonctionnaire de la Poste ayant contrefait la signature d'une personne âgée résidant dans un établissement public d'hébergement pour personnes âgées dépendantes, afin d'effectuer au nom de cette personne deux demandes de rachat de contrats d'assurance-vie et une demande d'adhésion à un nouveau contrat d'assurance-vie, ayant fait porter comme bénéficiaires de ce nouveau contrat son fils et sa nièce, ayant fait bénéficier son fils de prêts, cadeaux et libéralités de la part de cette personne âgée, et ayant lui-même reçu des cadeaux et gratifications. Cour ayant jugé que les faits ayant motivé une sanction de révocation du fonctionnaire sont matériellement établis et présentent le caractère de fautes disciplinaires de nature à justifier une sanction, mais estimant que la sanction est disproportionnée aux faits en cause. Eu égard à la gravité des manquements reprochés à l'intéressé, l'appréciation de la cour conduirait en cas de reprise de la procédure disciplinaire au prononcé d'une sanction qui serait, en raison de son caractère insuffisant, hors de proportion avec les fautes commises. Par suite, annulation de l'arrêt.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 27 février 2015, La Poste, n°s 376598 381828, p. 64. Rappr., s'agissant du contrôle en cassation d'une décision juridictionnelle prononçant une sanction, CE, Assemblée, 30 décembre 2014, M. Bonnemaison, n° 381245, p. 443.,,[RJ2] Cf. CE, 27 juillet 2015, EHPAD de Beuzeville, n° 370414, T. p. 841.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
