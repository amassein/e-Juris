<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044254916</ID>
<ANCIEN_ID>JG_L_2021_10_000000456948</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/25/49/CETATEXT000044254916.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 19/10/2021, 456948, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-10-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>456948</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:456948.20211019</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... D... a demandé au juge des référés du tribunal administratif de C..., statuant sur le fondement de l'article L. 521-2 du code de justice administrative, en premier lieu, de suspendre l'exécution de l'arrêté du 15 septembre 2021 par lequel le préfet de C... lui a fait obligation de quitter sans délai le territoire français et interdiction d'y retourner, en fixant l'Union des Comores comme pays de destination, et en deuxième lieu, d'enjoindre, sous astreinte, à l'administration de lui délivrer un récépissé portant la mention " vie privée et familiale " et l'autorisant à travailler et, en dernier lieu, d'enjoindre, sous astreinte, au préfet de C..., en cas d'exécution de la mesure d'éloignement avant que le juge des référés ne statue, d'organiser son retour aux frais de l'Etat, avec le concours des autorités consulaires françaises aux Comores. Par une ordonnance n° 2103412 du 17 septembre 2021, le juge des référés du tribunal administratif de C... a rejeté sa requête.<br/>
<br/>
              Par une requête et un mémoire complémentaire, enregistrés les 22 et 23 septembre 2021 au secrétariat du contentieux du Conseil d'Etat, M. D... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler l'ordonnance du 17 septembre 2021 ;<br/>
<br/>
              2°) de suspendre l'exécution de l'arrêté du 15 septembre 2021 par lequel le Préfet de C... lui a fait obligation de quitter le territoire, sans délai, et a imposé une interdiction de retour au requérant pendant un délai d'un an ;<br/>
<br/>
              3°) d'enjoindre à l'administration de ramener le requérant sur le territoire français, aux frais de l'Etat, dans un délai maximal de 30 jours à compter de la décision à venir, frais de transport et visa pris en charge par l'Etat ;<br/>
<br/>
              4°) d'enjoindre à l'administration de délivrer au requérant un récépissé portant la mention " vie privée et familiale " et l'autorisant à travailler ; <br/>
<br/>
              5°) d'assortir ces injonctions d'une astreinte de 500 euros par jour de retard et de dire que celle-ci sera intégralement liquidée au profit du requérant tous les sept jours sans autre formalité, au visa de l'article L. 911-1 du code de justice administrative ; <br/>
<br/>
              6°) de mettre à la charge de l'Etat la somme de 4 000 euros à payer au requérant au titre de l'article L 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - l'ordonnance du juge des référés du tribunal administratif de C... méconnaît le principe du contradictoire dès lors que l'avocat du préfet n'a pas communiqué la procédure devant le juge des référés alors même qu'il mentionne que la décision contestée et la procédure policière ont été produites par le centre de rétention administrative ;<br/>
              - l'ordonnance du juge des référés du tribunal administratif de C... ne tient pas compte de son éloignement effectif préalable dès lors que le juge des référés du tribunal administratif a supposé qu'il se trouvait toujours sur le territoire national français pour considérer que la décision fixant à un an la durée de l'interdiction de retour sur le territoire français ne produit par elle-même aucun effet ; <br/>
              - la condition d'urgence est satisfaite dès lors que, d'une part, l'éloignement du territoire français réalisé de manière abusive en dépit du dépôt de sa requête en référé-liberté devant le tribunal administratif de Mamoudzou porte atteinte à ses intérêts et à ceux de sa famille dès lors qu'il est le seul à subvenir à leurs besoins et, d'autre part, l'interdiction de retourner sur le territoire français pendant un an est applicable en ce qu'il se trouve désormais hors du territoire français ;<br/>
              - il est porté une atteinte grave et manifestement illégale aux libertés fondamentales ; <br/>
              - la décision du préfet méconnaît le droit au bénéfice du recours effectif et au droit à la vie privée et familiale dès lors que, d'une part, le préfet a fait procéder à son éloignement forcé du territoire français préalablement à l'audience, ce qui l'a empêché de faire valoir ses moyens à l'audience auprès du juge des libertés et de la détention et, d'autre part, qu'il vit avec sa femme qui est en situation régulière en France ainsi qu'avec leurs six enfants mineurs, dûment scolarisés et dotés de documents de circulation ;<br/>
              - le fait que l'arrêté contesté ait été exécuté le 16 septembre 2021 et qu'il ait été reconduit aux Comores, alors qu'il avait saisi le juge des référés du tribunal administratif de C... contrevient à l'article L. 761-9 du code de l'entrée et du séjour des étrangers et du droit d'asile.<br/>
<br/>
              Par un mémoire en défense, enregistré le 4 octobre 2021, le ministre de l'intérieur conclut au rejet de la requête. Il soutient que la condition d'urgence n'est pas satisfaite, et que les moyens soulevés ne sont pas fondés. <br/>
              Par un nouveau mémoire, enregistré le 6 octobre 2021, le ministre de l'intérieur persiste dans ses conclusions par les mêmes moyens.<br/>
<br/>
              Par deux nouveaux mémoires, enregistrés les 6 et 7 octobre 2021, M. D... persiste dans ses conclusions par les mêmes moyens.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. D..., et d'autre part, le ministre de l'intérieur ;<br/>
<br/>
              Ont été entendus lors de l'audience publique du 6 octobre 2021, à 10 heures : <br/>
<br/>
              - Me Lyon-Caen, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. D... ;<br/>
<br/>
              - les représentants du ministre de l'intérieur ; <br/>
<br/>
              à l'issue de laquelle le juge des référés a reporté la clôture de l'instruction au 7 octobre 2021 à 18 heures.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". <br/>
<br/>
              2. Il résulte de l'instruction que M. B... D..., ressortissant comorien, a fait l'objet d'une obligation de quitter sans délai le territoire français par arrêté du préfet de C... du 15 septembre 2021. Il a demandé au juge des référés du tribunal administratif de C..., sur le fondement de l'article L. 521-2 du code de justice administrative, de suspendre l'exécution de cet arrêté et d'enjoindre au préfet d'examiner sa demande de titre de séjour. Il fait appel de l'ordonnance du 16 septembre 2021 par laquelle le juge des référés du tribunal administratif a rejeté sa demande.<br/>
<br/>
              3. La circonstance que le juge des libertés et de la détention du tribunal judiciaire de Mamoudzou a prononcé le 16 septembre 2021, postérieurement à la saisine du juge des référés du tribunal administratif de C..., la mainlevée de la rétention administrative dont M. D... faisait l'objet par un arrêté préfectoral du 15 septembre 2021, est par elle-même sans effet sur le caractère exécutoire de l'arrêté d'éloignement du même jour, objet de la demande de suspension sur le fondement de l'article L. 521-2 du code de justice administrative. Il est, par ailleurs, constant que cet arrêté n'avait pas été rapporté à la date de l'ordonnance attaquée. En outre, si cette ordonnance se réfère à la " pratique administrative constante " consistant en ce que le préfet de C... renonce à la mise à exécution de la mesure d'éloignement dès lors que l'autorité judiciaire prononce la mainlevée de la mesure de rétention administrative, cette circonstance, à la supposer même établie, est sans effet sur le caractère exécutoire de l'arrêté en litige. C'est, par suite à tort que le premier juge s'est fondé sur ces considérations pour rejeter la demande de M. D... faute d'urgence. Enfin, il résulte de l'instruction que le préfet de C... a procédé à l'exécution de la mesure d'éloignement, le 16 septembre 2021, après l'introduction du recours en référé de M. D..., concomitamment à la réception de l'information qui lui a été adressé par le tribunal administratif concernant l'introduction de ce recours, de sorte que l'ordonnance attaquée est également entachée d'une erreur de fait sur la situation du requérant à la date à laquelle le juge des référés a statué.  <br/>
<br/>
              4. Il appartient au juge des référés du Conseil d'Etat, saisi de l'ensemble du litige par l'effet dévolutif de l'appel, d'examiner les moyens soulevés par M. D... à l'encontre de l'arrêté du 4 février 2021.<br/>
<br/>
              5. M. D..., ressortissant comorien né en 1974, a épousé le 2 août 2009, à Mirontsy, aux Comores, Mme G... E..., de nationalité comorienne. Trois enfants sont nés aux Comores de cette union, en 2009, 2011 et 2013, puis trois autres, à C..., en 2016, 2019 et 2020. Si Mme E... a donné naissance, le 16 février 2015, à une enfant, F... A..., d'un père français, il ne résulte pas de l'instruction que cette circonstance ait durablement interrompu sa communauté de vie avec M. D..., qui réside toujours avec son épouse et ses six enfants à C..., après 12 ans de vie commune. Du fait de la naissance à C... de sa fille F... A..., Mme E... est titulaire depuis mars 2019 d'un titre de séjour, en qualité de parent d'enfants français. Il résulte par ailleurs de l'instruction et des pièces versées au dossier que M. D..., dont la communauté de vie avec son épouse et leurs six enfants est effective participe à leur entretien et notamment aux frais liés à la scolarité de chacun des enfants, ainsi qu'à leur éducation. Dans la mesure où les stipulations de l'article 3-1 de la convention internationale relative aux droits de l'enfant font obligation à l'autorité administrative d'accorder une attention primordiale à l'intérêt supérieur des enfants dans toutes les décisions les concernant, il apparaît qu'en l'état de l'instruction, l'obligation faite à M. D... de quitter le territoire français, avec l'Union des Comores pour pays de destination, et interdiction de revenir en France pendant un an, a porté et continue de porter une atteinte grave et manifestement illégale à son doit de mener une vie privée et familiale normale, qui constitue une liberté fondamentale au sens des dispositions de l'article L. 521-2 du code de justice administrative. Compte-tenu de l'ensemble de ce qui précède, et dans les circonstances de l'espèce, la condition d'urgence posée par l'article L. 521-2 du code de justice administrative doit être également regardée comme satisfaite.<br/>
<br/>
              6. Il résulte de tout ce qui précède que M. D... est fondé à demander la suspension de l'arrêté du 15 septembre 2021 en tant qu'il lui fait obligation de quitter sans délai le territoire français et fixe l'Union des Comores pour pays de destination ainsi que, par voie de conséquence, en tant qu'il lui interdit de revenir sur le territoire français pendant une durée d'un an, et qu'il soit enjoint à l'administration de prendre toutes mesures utiles afin de permettre son retour sur le territoire français dans un délai d'un mois, et de procéder au réexamen de sa situation dans un délai d'un mois à compter de la date de son retour sur le territoire français, sans qu'il y ait lieu d'assortir cette injonction d'une astreinte. <br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de faire application des dispositions de l'article L. 761-1 du code de justice administrative et de mettre à la charge de l'Etat la somme de 3 000 euros au titre des frais exposés par M. B... et non compris dans les dépens.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : L'exécution de l'arrêté du préfet de C... du 15 septembre 2021 par lequel le préfet de C... lui a fait obligation de quitter sans délai le territoire français et interdiction d'y retourner, en fixant l'Union des Comores comme pays de destination, est suspendue. <br/>
Article 2 : Il est enjoint à l'administration de prendre toutes mesures utiles afin de permettre le retour de M. D... à C... dans un délai d'un mois, et de réexaminer sa situation dans un délai d'un mois à compter de la date de ce retour. <br/>
Article 3 : L'Etat versera à M. D... une somme de 3 000 euros au titre de l'article L.761-1 du code de justice administrative<br/>
Article 4 : Le surplus des conclusions de la requête de M. D... est rejeté. <br/>
Article 5 : La présente ordonnance sera notifiée à M. B... D... et au ministre de l'intérieur.<br/>
Fait à Paris, le 19 octobre 2021.<br/>
    Signé : Cyril Roger-Lacan<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
