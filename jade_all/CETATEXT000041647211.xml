<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041647211</ID>
<ANCIEN_ID>JG_L_2020_02_000000437814</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/64/72/CETATEXT000041647211.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 14/02/2020, 437814, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-02-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>437814</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:437814.20200214</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 21 janvier et 7 février 2020 au secrétariat du contentieux du Conseil d'Etat, le collectif des maires antipesticides demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution, d'une part, du décret n° 2019-1500 du 27 décembre 2019 relatif aux mesures de protection des personnes lors de l'utilisation de produits phytopharmaceutiques à proximité des zones d'habitation et, d'autre part, de l'arrêté du 27 décembre 2019 relatif aux mesures de protection des personnes lors de l'utilisation de produits phytopharmaceutiques et modifiant l'arrêté du 4 mai 2017 relatif à la mise sur le marché et à l'utilisation des produits phytopharmaceutiques et de leurs adjuvants visés à l'article L. 253-1 du code rural et de la pêche maritime ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
        Le collectif des maires antipesticides soutient que : <br/>
              - sa requête est recevable dès lors qu'il dispose d'un intérêt à agir lui donnant qualité pour agir ; <br/>
              - la condition d'urgence est remplie dès lors, en premier lieu, que la toxicité non contestée des pesticides impose l'adoption de mesures garantissant la sécurité des riverains et, en second lieu, que la règlementation contestée fait obstacle à l'exercice de la compétence des maires pour prendre au titre de leur pouvoir de police générale les mesures nécessaires à la protection des populations ;<br/>
              - il existe un doute sérieux quant à la légalité des décisions contestées ;<br/>
              - le décret et l'arrêté contestés ont été adoptés au terme d'une procédure de consultation entachée d'irrégularité dès lors, en premier lieu, que le dossier de consultation ne comportait pas la note de présentation qu'imposent les dispositions de l'article L. 123-19-1 du code de l'environnement, en deuxième lieu, que les observations et les propositions déposées par voie électronique n'ont pas été rendues publiques et, en dernier lieu, qu'aucun document ne fait état des motifs de la décision finale du gouvernement ; <br/>
              - les décisions contestées sont entachées d'un vice tenant à l'interprétation erronée de l'avis n° 2019-SA-0020 rendu le 14 juin 2019 par l'Agence nationale de sécurité sanitaire de l'alimentation, de l'environnement et du travail (ANSES) qu'elles visent, dès lors que, en premier lieu, cet avis relève l'insuffisance et l'ancienneté des études évaluant les risques que pose l'utilisation des produits phytopharmaceutiques pour les riverains, en deuxième lieu, il ne préconise des distances de sécurité de cinq mètres et dix mètres qu'à titre de seuil minimal et, en troisième lieu, les décisions contestées ne retiennent pas les recommandations émises par l'ANSES en matière de généralisation des dispositifs limitant la dérive et d'information préalable des résidents ;<br/>
              - les décisions contestées méconnaissent les obligations résultant de la directive 2009/128/CE du 21 octobre 2009 instaurant un cadre d'action communautaire pour parvenir à une utilisation des pesticides compatible avec le développement durable et du règlement 1107/2009/CE concernant la mise sur le marché des produits phytopharmaceutiques, ainsi que de son règlement d'application n° 284/2013 du 1er mars 2013, qui imposent de garantir aux personnes présentes sur les lieux où sont utilisés des produits phytopharmaceutiques un niveau d'exposition qui n'excède pas celui qui est défini pour la substance active de ces produits lors de leur autorisation, alors qu'aucun test n'est effectué sur l'ensemble des éléments entrant dans la composition des produits et leur interaction ;<br/>
              - le décret et l'arrêts contestés méconnaissent également ces dispositions en ce qu'ils ne répondent pas à l'objectif de réduction de l'utilisation des pesticides qu'elles fixent ;<br/>
              - le décret et l'arrêté contestés sont entachés d'une erreur manifeste dans l'appréciation des mesures qu'appelle l'application du principe de précaution garanti tant par l'article 5 de la Charte de l'environnement que par le droit de l'Union européenne, qui impose de proscrire toute exposition des personnes résidentes aux produits phytopharmaceutiques ;<br/>
              - le décret et l'arrêté contestés méconnaissent l'article 17 de la directive du 21 octobre 2009, qui impose de prévoir des sanctions effectives, proportionnées et dissuasives pour en assurer la mise en oeuvre ;<br/>
              - les chartes d'engagement des utilisateurs prévues par le décret contesté méconnaissent également les dispositions du droit de l'Union européenne mentionnées ci-dessus, en ce qu'elles permettent des dérogations aux règles minimales de distances sans apporter des garanties suffisantes sur l'efficacité des mesures de protection qu'elles comportent, et sans prévoir d'information obligatoire des résidents avant l'utilisation des produits phytopharmaceutiques ;<br/>
              - la différence de traitement établie par le décret contesté entre les maires des communes rurales, qui sont associés à la concertation sur les projets de chartes des usages agricoles, et les maires des communes urbaines, qui ne sont pas associés à la concertation sur les chartes d'engagements concernant les usages non agricoles, est contraire au principe d'égalité ; <br/>
              - l'arrêté contesté méconnaît tant le principe de précaution que les dispositions mentionnées ci-dessus du droit de l'Union européenne en ce qu'il fixe des distances minimales de cinq, dix et vingt mètres manifestement insuffisantes pour assurer la protection des résidents, alors qu'il résulte de l'avis de l'ANSES que ces distances correspondent à des niveaux élevés d'exposition et qu'elles ne sont pas assorties de mesures complémentaires de protection suffisantes ni de sanction ;<br/>
              - ces distances sont entachées d'erreur manifeste d'appréciation alors que la distance de 150 mètres retenue par les maires qui ont pris en ce sens des arrêtés municipaux est seule à même de permettre la protection des résidents.<br/>
<br/>
<br/>
              Par un mémoire en défense, enregistré le 6 février 2020, le ministre de l'agriculture et de l'alimentation conclut au rejet de la requête. Il soutient que la condition d'urgence n'est pas remplie et qu'aucun des moyens soulevés n'est propre à créer un doute sérieux quant à la légalité des décisions contestées.<br/>
<br/>
              La requête a été communiquée au Premier ministre, à la ministre de la transition écologique et solidaire, à la ministre des solidarités et de la santé et au ministre de l'économie et des finances, qui n'ont pas produit d'observations.<br/>
<br/>
              Par un mémoire en intervention, enregistré le 7 février 2020, le Comité de recherche et d'information indépendant du génie génétique et l'association Agir pour l'environnement demandent au juge des référés du Conseil d'Etat de faire droit à la requête, ils soutiennent qu'ils ont intérêt à intervenir et que les moyens de la requête sont fondés.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule ;<br/>
              - le règlement (CE) n° 1107/2009 du Parlement européen et du Conseil du 21 octobre 2009 concernant la mise sur le marché des produits phytopharmaceutiques ;<br/>
              - la directive 2009/128/CE du Parlement européen et du Conseil du 21 octobre 2009 instaurant un cadre d'action communautaire pour parvenir à une utilisation des pesticides compatible avec le développement durable ; <br/>
              - le règlement (UE) n° 284/2013 de la Commission européenne du 1er mars 2013 établissant les exigences en matière de données applicables aux produits phytopharmaceutiques conformément au règlement (CE) n°1107/2009 du Parlement européen et du Conseil concernant la mise sur le marché des produits phytopharmaceutiques ;<br/>
              - le code de l'environnement ;<br/>
              - le code rural et de la pêche maritime ;<br/>
              - la loi n° 2019-938 du 30 octobre 2018 ;<br/>
              - la décision du Conseil d'Etat statuant au contentieux n° 415426 du 26 juin 2019 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, le Collectif des maires antipesticides et, d'autre part, le Premier ministre, la ministre de la transition écologique et solidaire, la ministre des solidarités et de la santé, le ministre de l'économie et des finances et le ministre de l'agriculture et de l'alimentation ;<br/>
<br/>
              Ont été entendu au cours de l'audience publique du 10 février 2020 à 10 heures 30 : <br/>
              - les représentants du Collectif des maires antipesticides ;<br/>
<br/>
              - les représentants de l'association Agir pour l'environnement ;<br/>
<br/>
              - les représentants de la ministre de la transition écologique et solidaire ;<br/>
<br/>
              - les représentants du ministre de l'agriculture et de l'alimentation ;<br/>
<br/>
              à l'issue de laquelle le juge des référés a clos l'instruction ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit ;<br/>
<br/>
              1. Le Comité de recherche et d'information indépendant du génie génétique et l'association Agir pour l'environnement justifient d'un intérêt suffisant pour intervenir au soutien de la requête du collectif des maires antipesticides. Leur intervention est, par suite, recevable.<br/>
<br/>
              2. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". <br/>
<br/>
              3. D'une part, aux termes de l'article 12 de la directive du Parlement européen et du Conseil du 21 octobre 2009 instaurant un cadre d'action communautaire pour parvenir à une utilisation des pesticides compatible avec le développement durable : " Les Etats membres, tenant dûment compte des impératifs d'hygiène, de santé publique et de respect de la biodiversité ou des résultats des évaluations des risques appropriées, veillent à ce que l'utilisation de pesticides soit restreinte ou interdite dans certaines zones spécifiques. (...) Les zones spécifiques en question sont : / a) les zones utilisées par le grand public ou par des groupes vulnérables au sens de l'article 3 du règlement (CE) n° 1107/2009 (...) ". L'article 3 du règlement (CE) n° 1107/2009 dispose que font partie de ces groupes vulnérables " les femmes enceintes et les femmes allaitantes, les enfants à naître, les nourrissons et les enfants, les personnes âgées et les travailleurs et habitants fortement exposés aux pesticides sur le long terme ".<br/>
<br/>
              4. D'autre part, aux termes du premier alinéa de l'article L. 253-1 du code rural et de la pêche maritime : " Les conditions dans lesquelles la mise sur le marché et l'utilisation des produits phytopharmaceutiques et des adjuvants vendus seuls ou en mélange et leur expérimentation sont autorisées, ainsi que les conditions selon lesquelles sont approuvés les substances actives, les coformulants, les phytoprotecteurs et les synergistes contenus dans ces produits, sont définies par le règlement (CE) n° 1107/2009 du Parlement européen et du Conseil du 21 octobre 2009 concernant la mise sur le marché des produits phytopharmaceutiques et abrogeant les directives 79/117/ CEE et 91/414/ CEE du Conseil, et par les dispositions du présent chapitre. (...) ". Aux termes du I de l'article L. 253-7 du même code, pris pour la transposition de l'article 12 de la directive du 21 octobre 2009 précitée : " I. - (...) l'autorité administrative peut, dans l'intérêt de la santé publique ou de l'environnement, prendre toute mesure d'interdiction, de restriction ou de prescription particulière concernant la mise sur le marché, la délivrance, l'utilisation et la détention des produits mentionnés à l'article L. 253-1 du présent code et des semences traitées par ces produits. Elle en informe sans délai le directeur général de l'Agence nationale de sécurité sanitaire de l'alimentation, de l'environnement et du travail. / L'autorité administrative peut interdire ou encadrer l'utilisation des produits phytopharmaceutiques dans des zones particulières, et notamment : 1° Sans préjudice des mesures prévues à l'article L. 253-7-1, les zones utilisées par le grand public ou par des groupes vulnérables au sens de l'article 3 du règlement (CE) n° 1107/2009 ; (...) ". Aux termes du III inséré à l'article L. 253-8 du même code par la loi du 30 octobre 2018 pour l'équilibre des relations commerciales dans le secteur agricole et alimentaire et une alimentation saine, durable et accessible à tous : " A l'exclusion des produits de biocontrôle mentionnés au deuxième alinéa de l'article L. 253-6, des produits composés uniquement de substances de base ou de substances à faible risque au sens du règlement (CE) n° 1107/2009 (...), l'utilisation des produits phytopharmaceutiques à proximité des zones attenantes aux bâtiments habités et aux parties non bâties à usage d'agrément contiguës à ces bâtiments est subordonnée à des mesures de protection des personnes habitant ces lieux. Ces mesures tiennent compte, notamment, des techniques et matériels d'application employés et sont adaptées au contexte topographique, pédoclimatique, environnemental et sanitaire. Les utilisateurs formalisent ces mesures dans une charte d'engagements à l'échelle départementale, après concertation avec les personnes, ou leurs représentants, habitant à proximité des zones susceptibles d'être traitées avec un produit phytopharmaceutique. / Lorsque de telles mesures ne sont pas mises en place, ou dans l'intérêt de la santé publique, l'autorité administrative peut, sans préjudice des missions confiées à l'Agence nationale de sécurité sanitaire de l'alimentation, de l'environnement et du travail, restreindre ou interdire l'utilisation des produits phytopharmaceutiques à proximité des zones définies au premier alinéa du présent III. / Un décret précise les conditions d'application du présent III ".<br/>
<br/>
              5. Par une décision n° 415426 du 26 juin 2019, le Conseil d'Etat statuant au contentieux a annulé l'arrêté du 4 mai 2017 relatif à la mise sur le marché et à l'utilisation des produits phytopharmaceutiques et de leurs adjuvants visés à l'article L. 253-1 du code rural et de la pêche maritime, pris sur le fondement des dispositions citées ci-dessus de l'article L. 253-7 de ce code, en tant notamment qu'il ne prévoit pas de dispositions destinées à protéger les riverains des zones traitées par des produits phytopharmaceutiques. Il a enjoint aux ministres compétents de prendre les mesures réglementaires impliquées par sa décision dans un délai de six mois. <br/>
<br/>
              6. L'urgence justifie la suspension de l'exécution d'un acte administratif lorsque celui-ci porte atteinte de manière suffisamment grave et immédiate à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre. Il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte contesté sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue. L'urgence doit être appréciée objectivement et compte tenu de l'ensemble des circonstances de l'affaire.<br/>
<br/>
              Sur le décret du 27 décembre 2019 : <br/>
<br/>
              7. Le décret du 27 décembre 2019 a été pris pour l'application des dispositions citées ci-dessus du III de l'article L. 253-8 du code rural et de la pêche maritime. Il a pour objet de fixer le contenu des chartes d'engagements des utilisateurs que prévoient ces dispositions ainsi que leurs modalités d'élaboration et d'approbation. Pour justifier de l'urgence qui s'attacherait à suspendre l'exécution de ce décret, le collectif requérant se borne à invoquer, d'une part, le risque que posent les pesticides en matière de la santé publique et, d'autre part, la nécessité pour les maires d'exercer leur pouvoir de police générale afin de protéger les populations de ces risques. En déterminant seulement le contenu et les modalités d'élaboration de chartes d'engagements des utilisateurs, qui ne sauraient en tout état de cause avoir d'incidence sur ces intérêts que lorsqu'elles seront adoptées, le décret contesté ne saurait cependant porter atteinte de manière suffisamment grave et immédiate aux intérêts ainsi invoqués pour caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, son exécution soit suspendue. <br/>
<br/>
<br/>
              Sur l'arrêté du 27 décembre 2019 :<br/>
<br/>
              8. L'arrêté contesté modifie l'arrêté du 4 mai 2017 partiellement annulé par la décision du 26 juin 2019 du Conseil d'Etat statuant au contentieux. Il insère dans cet arrêté des article 14-1 et 14-2 qui, en l'absence de distance de sécurité spécifique fixée par l'autorisation de mise sur le marché du produit concerné, imposent des distances de sécurité minimales pour les traitements avec un produit phytopharmaceutique des parties aériennes des plantes qui sont réalisés à proximité notamment des habitations. Ces distances sont fixées à :<br/>
              - 20 mètres pour les produits présentant une des mentions de danger suivantes : H300, H310, H330, H331, H334, H340, H350, H350i, H360, H360F, H360D, H360FD, H360Fd H360Df, H370, H372, ou contenant une substance active considérée comme ayant des effets perturbateurs endocriniens néfastes pour l'homme ;<br/>
              - 10 mètres pour les autres produits lorsqu'ils sont utilisés pour l'arboriculture, la viticulture, les arbres et arbustes, la forêt, les petits fruits et cultures ornementales de plus de 50 cm de hauteur, les bananiers et le houblon ;<br/>
              - 5 mètres pour les autres utilisations agricoles et non agricoles.<br/>
              Le II de l'article 14-2 inséré par l'arrêté contesté dans l'arrêté du 4 mai 2017 prévoit que ces distances peuvent être adaptées lorsque le traitement est réalisé à proximité des lieux mentionnés au III de l'article L. 253-8 du code rural et de la pêche maritime et que des mesures apportant des garanties équivalentes en matière d'exposition des résidents par rapport aux conditions normales d'application des produits sont mises en oeuvre conformément à des chartes d'engagements approuvées par le préfet. L'annexe 4 prévoit dans ce cas des distances de sécurité minimales dérogatoires de 3 à 5 mètres selon les cultures et le niveau de réduction de la dérive.<br/>
<br/>
              9. Si le collectif requérant demande la suspension de l'arrêté du 27 décembre 2019, il résulte cependant de l'ensemble de ses écritures, comme des indications fournies par ses représentants lors de l'audience de référé, que ses conclusions doivent être regardées comme tendant à la suspension de l'exécution de cet arrêté dans la seule mesure où les distances minimales de sécurité qu'il fixe seraient insuffisantes pour garantir le respect du principe de précaution et des dispositions du droit de l'Union européenne qu'il invoque et assurer l'exécution de la décision n° 415426 du 26 juin 2019 du Conseil d'Etat statuant au contentieux. <br/>
<br/>
              10. Pour établir l'urgence qui s'attache à la suspension qu'il demande, le collectif requérant invoque, en premier lieu, le risque pour la santé qui est inhérent à l'utilisation des produits phytopharmaceutiques. Ce risque n'est pas contesté par l'administration et fonde l'ensemble de la réglementation européenne et française en la matière, y compris les mesures de protection prévues par l'arrêté attaqué, que le collectif requérant critique comme insuffisantes. Les éléments avancés sur les dangers de ces produits par le collectif requérant, quel que soit leur bien fondé, ne peuvent dès lors justifier de l'urgence à suspendre comme insuffisantes les mesures établissant des distances minimales de sécurité, que s'ils sont assortis d'éléments de nature à démontrer le risque qui s'attache à l'insuffisance de ces distances minimales pour les personnes concernées. Pour justifier d'un tel risque, le collectif requérant se borne à critiquer de manière très générale les distances de 5, 10 et 20 mètres et les dérogations qui peuvent y être apportées, en indiquant que de telles distances ne peuvent sérieusement être regardées comme satisfaisant à l'obligation de protection des riverains. Il résulte cependant de l'instruction que les distances de 5 mètres et 10 mètres sont les distances minimales préconisées par l'avis de l'Agence nationale de sécurité sanitaire de l'alimentation, de l'environnement et du travail du 4 juin 2019 au vu duquel a été pris l'arrêté, et que la distance de 20 mètres retenue pour l'utilisation de certains produits est le double de la distance minimale préconisée pour ces produits par le même avis. Il résulte également des termes de cet avis comme des échanges lors de l'audience de référé que, d'une part, plusieurs études et travaux d'évaluation sont en cours sur ce sujet en France comme à l'étranger et, d'autre part, les autres Etats membres de l'Union européenne n'imposent pas à ce jour de distances de sécurité d'application générale supérieures à celles prévues par l'arrêté contesté.<br/>
<br/>
              11. En second lieu, l'intérêt qui s'attache à l'adoption par les maires au titre de leur pouvoir de police générale des mesures nécessaires au bon ordre, à la sûreté, à la sécurité et à la salubrité publiques ne saurait suffire à établir l'urgence à suspendre l'arrêté contesté, dès lors que ce pouvoir de police générale doit s'exercer dans le respect des dispositions législatives qui confient au ministre un pouvoir de police spécial en la matière.<br/>
<br/>
              12. Il résulte de ce qui précède que la condition d'urgence prévue par l'article 521-1 du code de justice administrative ne peut être regardée comme remplie. Par suite, et sans qu'il soit besoin d'examiner la condition tenant à l'existence d'un doute sérieux sur la légalité du décret et de l'arrêté contestés, la requête du collectif des maires antipesticides doit être rejetée, y compris, par voie de conséquence, ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : L'intervention du Comité de recherche et d'information indépendant du génie génétique et de l'association Agir pour l'environnement est admise.<br/>
Article 2 : La requête du Collectif des maires antipesticides est rejetée.<br/>
Article 3 : La présente ordonnance sera notifiée au Collectif des maires antipesticides, au Premier ministre, à la ministre de la transition écologique et solidaire, à la ministre des solidarités et de la santé, au ministre de l'économie et des finances, au ministre de l'agriculture et de l'alimentation et au Comité de recherche et d'information indépendant du génie génétique, premier intervenant dénommé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
