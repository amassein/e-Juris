<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039799819</ID>
<ANCIEN_ID>JG_L_2020_01_000000428597</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/79/98/CETATEXT000039799819.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 08/01/2020, 428597, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-01-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428597</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BERTRAND</AVOCATS>
<RAPPORTEUR>M. Jean-Yves Ollier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:428597.20200108</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Toulouse, à titre principal, d'annuler le titre de perception émis par le directeur régional des finances publiques de Midi-Pyrénées et de la Haute-Garonne le 18 mai 2016, modifié le 24 juin 2016, ainsi que la décision implicite de rejet de son recours gracieux et de le décharger de l'obligation de payer la somme de 13 829 euros, et, à titre subsidiaire de modérer les sommes réclamées par le titre de perception. Par un jugement n° 1701617 du 28 décembre 2018, le tribunal administratif de Toulouse a annulé le titre de perception en litige et déchargé M. B... de l'obligation de payer la somme de 13 829 euros.<br/>
<br/>
              Par un pourvoi, enregistré le 5 mars 2019 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'action et des comptes publics demande au Conseil d'Etat d'annuler ce jugement.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code civil ;<br/>
              - le code des pensions civiles et militaires de retraite ;<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Yves Ollier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Bertrand, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 14 octobre 2005 du président du conseil d'administration de La Poste, M. B..., fonctionnaire, a été admis d'office à faire valoir ses droits à la retraite. Il a alors bénéficié d'une pension d'invalidité non imputable au service versée par l'Etat. Par un jugement du 15 octobre 2012, le tribunal administratif de Versailles a annulé cette décision et enjoint au directeur général de La Poste de procéder à la réintégration juridique de l'intéressé et à la reconstitution de sa carrière. Le directeur régional des finances publiques de Midi-Pyrénées et de la Haute-Garonne a émis, le 18 mai 2016, un titre de perception d'un montant de 42 708 euros afin d'obtenir le reversement de la pension servie à M. B... de 2005 à 2016, montant ramené à 13 829 euros, le 24 juin 2016, compte tenu de la prescription d'une partie des sommes dues. Le ministre de l'action et des comptes publics se pourvoit en cassation contre le jugement du 28 décembre 2018 par lequel le tribunal administratif de Toulouse a, à la demande de M. B..., annulé le titre de perception en litige et déchargé l'intéressé de l'obligation de payer la somme correspondante.<br/>
<br/>
              2. Aux termes de l'article L. 55 du code des pensions civiles et militaires de retraite : " (...) la pension et la rente viagère d'invalidité sont définitivement acquises et ne peuvent être révisées ou supprimées à l'initiative de l'administration ou sur demande de l'intéressé que dans les conditions suivantes : / A tout moment en cas d'erreur matérielle ; / Dans un délai d'un an à compter de la notification de la décision de concession initiale de la pension ou de la rente viagère, en cas d'erreur de droit / (...) ". Ces dispositions ne sauraient faire obstacle à ce que l'administration, qui est tenue d'assurer l'exécution des décisions de justice, annule la pension initialement concédée à un agent lorsque celle-ci se trouve, par l'effet d'une décision du juge administratif, privée de base légale.<br/>
<br/>
              3. Aux termes de l'article 1347 du code civil : " La compensation est l'extinction simultanée d'obligations réciproques entre deux personnes./ Elle s'opère, sous réserve d'être invoquée, à due concurrence, à la date où ses conditions se trouvent réunies ".<br/>
<br/>
              4. Il ressort des énonciations du jugement attaqué que le tribunal administratif a estimé qu'en l'absence, d'une part, de reconstitution de carrière de M. B... à la suite de l'annulation de la décision de La Poste l'admettant à faire valoir ses droits à la retraite et, d'autre part, de versement par cette société à l'intéressé de l'indemnité représentative des rémunérations dont il a été indûment privé, l'administration ne pouvait pas légalement demander le reversement de la pension qu'il avait perçue durant la même période. En opérant ainsi, de manière implicite, une compensation entre la créance détenue par M. B... sur la société La Poste, au titre de l'indemnité mentionnée ci-dessus, et la créance détenue par l'Etat sur M. B..., correspondant à la pension qui lui a été indûment versée, alors qu'il s'agit de personnes morales différentes, le tribunal administratif de Toulouse a commis une erreur de droit.<br/>
<br/>
              5. Par suite, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, le ministre de l'action et des comptes publics est fondé à demander l'annulation du jugement qu'il attaque. <br/>
<br/>
              6. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge du ministre de l'action et des comptes publics qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 28 décembre 2018 du tribunal administratif de Toulouse est annulé. <br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Toulouse. <br/>
Article 3 : Les conclusions présentées par M. B... au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au ministre de l'action et des comptes publics et à M. A... B....<br/>
Copie en sera adressée à La Poste.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
