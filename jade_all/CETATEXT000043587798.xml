<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043587798</ID>
<ANCIEN_ID>JG_L_2021_05_000000437429</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/58/77/CETATEXT000043587798.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 28/05/2021, 437429</TITRE>
<DATE_DEC>2021-05-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>437429</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; SCP BORE, SALVE DE BRUNETON, MEGRET</AVOCATS>
<RAPPORTEUR>Mme Manon Chonavel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:437429.20210528</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... J..., M. F... L..., M. et Mme H... et Annie I..., Mme P...-G... et M. N... G... ont demandé au tribunal administratif de Marseille d'annuler pour excès de pouvoir l'arrêté du 28 juin 2013 par lequel le maire de Marseille a délivré un permis de construire à M. B... M..., ainsi que la décision implicite de rejet de leur recours gracieux. Par un jugement n° 1307900 du 16 avril 2015, le tribunal administratif de Marseille a rejeté leur demande, ainsi que les conclusions indemnitaires reconventionnelles présentées par M. M... sur le fondement des dispositions de l'article L. 600-7 du code de l'urbanisme.<br/>
<br/>
              Par une décision n° 391160 du 30 décembre 2016, le Conseil d'Etat, statuant au contentieux a, sur le pourvoi de M. J... et autres, annulé ce jugement en tant qu'il a rejeté la demande de M. J... et autres, renvoyé l'affaire dans cette mesure au tribunal administratif de Marseille et rejeté le pourvoi incident de M. M....<br/>
<br/>
              Par un premier jugement n° 1610353 du 21 juin 2018, le tribunal administratif de Marseille a, sur le fondement de l'article L. 600-5-1 du code de l'urbanisme, sursis à statuer sur la demande de M. J... et autres et a imparti à M. M... et à la commune de Marseille un délai de quatre mois afin de produire un permis de régularisation permettant d'assurer la conformité du projet aux dispositions de l'article L. 431-10 du code de l'urbanisme.<br/>
<br/>
              Après la délivrance à M. M... d'un permis de régularisation par arrêté du 31 janvier 2019, M. J..., M. L..., M. et Mme I..., Mme O...-G... et M. G... ont demandé au tribunal administratif de Marseille d'annuler pour excès de pouvoir ce permis de régularisation. M. C... E... et le comité d'intérêt de quartier des Hauts de Mazargues-La Cayolle sont intervenus au soutien de l'ensemble des conclusions présentées par les demandeurs.<br/>
<br/>
              Par un second jugement n° 1610353 du 7 novembre 2019, le tribunal administratif de Marseille a rejeté l'ensemble des conclusions des demandeurs.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 7 janvier et 2 juin 2020 au secrétariat du contentieux du Conseil d'Etat, M. et Mme I..., M. J..., M. G..., M. L..., Mme O...-G..., M. E... et le comité d'intérêt de quartier des Hauts de Mazargues-La-Cayolle demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement du 7 novembre 2019 du tribunal administratif de Marseille ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur demande ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Marseille et de M. M... la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'urbanisme ;<br/>
              - le décret n° 2019-303 du 10 avril 2019 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme D... K..., auditrice,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Boré, Salve de Bruneton, Mégret, avocat de M. et Mme I..., de M. J..., de M. G..., de M. L..., de M. E..., de Mme O...-G... et du comité d'intérêt de quartier des Hauts de Mazargues-La-Cayolle et à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de M. M... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :	<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 28 juin 2013, le maire de Marseille a délivré à M. M... un permis de construire portant sur la construction de seize villas individuelles sur un terrain situé dans le massif des Calanques en bordure du parc national des Calanques. M. J..., M. et Mme I... et d'autres requérants ont demandé au tribunal administratif d'annuler cet arrêté pour excès de pouvoir. Par une décision du 30 décembre 2016, le Conseil d'Etat, statuant au contentieux, a annulé le jugement du 16 avril 2015 du tribunal administratif de Marseille en tant qu'il a rejeté cette demande et a renvoyé dans cette mesure l'affaire au tribunal administratif de Marseille. Statuant sur ce renvoi, par un premier jugement du 21 juin 2018, le tribunal administratif a, sur le fondement de l'article L. 600-5-1 du code de l'urbanisme, sursis à statuer et a imparti à M. M... et à la commune de Marseille un délai de quatre mois afin de produire un permis de construire de régularisation permettant d'assurer la conformité du projet aux dispositions de l'article L. 431-10 du code de l'urbanisme, relatif à la composition du dossier de demande de permis de construire. Par un arrêté du 31 janvier 2019, le maire de Marseille a délivré un permis de régularisation à M. M..., réduisant le projet de construction à quatorze villas, que les requérants ont également contesté devant le tribunal administratif. Par un jugement du 7 novembre 2019, contre lequel M. et Mme I... et autres se pourvoient en cassation, le tribunal administratif a rejeté l'ensemble de leurs conclusions. Les requérants doivent être regardés comme demandant l'annulation de ce jugement, sauf en tant qu'il s'est prononcé sur leurs conclusions tendant au rejet des conclusions reconventionnelles de M. M....<br/>
<br/>
              Sur le pourvoi :<br/>
<br/>
              2. Aux termes de l'article L. 600-5-1 du code de l'urbanisme : " Sans préjudice de la mise en oeuvre de l'article L. 600-5, le juge administratif qui, saisi de conclusions dirigées contre un permis de construire, de démolir ou d'aménager ou contre une décision de non-opposition à déclaration préalable estime, après avoir constaté que les autres moyens ne sont pas fondés, qu'un vice entraînant l'illégalité de cet acte est susceptible d'être régularisé, sursoit à statuer, après avoir invité les parties à présenter leurs observations, jusqu'à l'expiration du délai qu'il fixe pour cette régularisation, même après l'achèvement des travaux. Si une mesure de régularisation est notifiée dans ce délai au juge, celui-ci statue après avoir invité les parties à présenter leurs observations. Le refus par le juge de faire droit à une demande de sursis à statuer est motivé. "<br/>
<br/>
              3. Lorsque le ou les vices affectant la légalité de l'autorisation d'urbanisme dont l'annulation est demandée sont susceptibles d'être régularisés, le juge administratif doit, en application de l'article L. 600-5-1 du code de l'urbanisme, surseoir à statuer sur les conclusions dont il est saisi contre cette autorisation, sauf à ce qu'il fasse le choix de recourir à l'article L. 600-5 du code de l'urbanisme, si les conditions posées par cet article sont réunies, ou que le bénéficiaire de l'autorisation lui ait indiqué qu'il ne souhaitait pas bénéficier d'une mesure de régularisation. Lorsqu'il décide de recourir à l'article L. 600-5-1, il lui appartient, avant de surseoir à statuer sur le fondement de ces dispositions, de constater préalablement qu'aucun des autres moyens n'est fondé et n'est susceptible d'être régularisé et d'indiquer dans sa décision de sursis pour quels motifs ces moyens doivent être écartés. A compter de la décision par laquelle le juge recourt à l'article L. 600-5-1, seuls des moyens dirigés contre la mesure de régularisation notifiée, le cas échéant, au juge peuvent être invoqués devant ce dernier. A ce titre, les parties peuvent contester la légalité d'un permis de régularisation par des moyens propres et au motif qu'il ne permet pas de régulariser le permis initial.<br/>
<br/>
              4. En l'espèce, par son premier jugement du 21 juin 2018 décidant de recourir à l'article L. 600-5-1, le tribunal administratif, après avoir écarté comme non fondés les autres moyens de la requête, a estimé, d'une part, que la composition du dossier de demande de permis de construire n'était pas conforme aux dispositions de l'article R. 431-10 du code de l'urbanisme s'agissant des documents, notamment photographiques, de nature à permettre à l'administration d'apprécier l'insertion du projet dans son environnement proche et lointain et d'en mesurer l'impact, notamment par rapport aux constructions avoisinantes et aux paysages du massif des Calanques et, d'autre part, que ce vice ne lui permettait pas d'exercer son contrôle sur l'appréciation portée par l'administration quant à la conformité du projet à l'article R. 111-21 du même code, relatif à l'aspect des constructions, et à l'article UI 11 du règlement du plan d'occupation des sols, relatif à l'insertion des constructions dans le site environnant. Il doit être regardé, ce faisant, non comme ayant omis à tort de statuer sur des moyens avant de recourir à l'article L. 600-5-1, mais comme ayant jugé qu'eu égard au vice qu'il avait relevé, ces moyens ne pouvaient être écartés à la date de ce premier jugement et qu'ils demeuraient susceptibles de l'être après régularisation du dossier de demande de permis.<br/>
<br/>
              5. Par le jugement mettant fin à l'instance, seul contesté par les requérants, le tribunal s'est prononcé sur ces trois moyens, qui se rapportaient tous au bien-fondé du permis de construire, au vu du permis de régularisation délivré par un arrêté du 31 janvier 2019 du maire de Marseille. Il a toutefois estimé que les parties n'étaient pas recevables à contester ce permis de régularisation au motif qu'elles n'avaient pas notifié ce recours contentieux conformément à l'article R. 600-1 du code de l'urbanisme.<br/>
<br/>
              6. Dans sa rédaction applicable au litige, cet article dispose que : " En cas de déféré du préfet ou de recours contentieux à l'encontre d'un certificat d'urbanisme, ou d'une décision relative à l'occupation ou l'utilisation du sol régie par le présent code, le préfet ou l'auteur du recours est tenu, à peine d'irrecevabilité, de notifier son recours à l'auteur de la décision et au titulaire de l'autorisation (...) ".<br/>
<br/>
              7. Ces dispositions visent, dans un but de sécurité juridique, à permettre au bénéficiaire d'une autorisation d'urbanisme, ainsi qu'à l'auteur de cette décision, d'être informés à bref délai de l'existence d'un recours contentieux dirigé contre elle. Elles sont sans objet et ne peuvent être regardées comme applicables en cas de contestation d'un permis modificatif, d'une décision modificative ou d'une mesure de régularisation dans les conditions prévues par l'article L. 600-5-2, aux termes duquel " Lorsqu'un permis modificatif, une décision modificative ou une mesure de régularisation intervient au cours d'une instance portant sur un recours dirigé contre le permis de construire, de démolir ou d'aménager initialement délivré ou contre la décision de non-opposition à déclaration préalable initialement obtenue et que ce permis modificatif, cette décision modificative ou cette mesure de régularisation ont été communiqués aux parties à cette instance, la légalité de cet acte ne peut être contestée par les parties que dans le cadre de cette même instance ", ainsi d'ailleurs que le précise désormais l'article R. 600-1 dans sa rédaction issue du décret du 10 avril 2019 pris pour l'application de l'article L. 600-5-2 du code de l'urbanisme. Tel est notamment le cas lorsque, le juge ayant recouru à l'article L. 600-5-1 du code de l'urbanisme, une mesure de régularisation lui est notifiée et que, celui-ci ayant invité comme il le doit les parties à présenter leurs observations, ces dernières contestent la légalité de cette mesure. En revanche, l'obligation de notification résultant de l'article R. 600-1 du code de l'urbanisme est applicable à la contestation d'un acte mentionné à l'article L. 600-5-2 en dehors des conditions prévues par cet article. <br/>
<br/>
              8. Par suite, en jugeant irrecevables les conclusions des demandeurs dirigées contre l'arrêté du 31 janvier 2019 au motif qu'ils ne les avaient pas notifiées conformément à l'article R. 600-1 du code de l'urbanisme, le tribunal administratif a commis une erreur de droit.<br/>
<br/>
              9. L'erreur de droit commise par le tribunal a eu pour conséquence qu'il s'est prononcé sur la légalité du permis de construire en litige au vu du permis de régularisation délivré sans examiner les moyens que les requérants dirigeaient contre cette mesure de régularisation, qui étaient opérants. Il y a lieu, en conséquence, d'annuler le jugement attaqué dans son ensemble en tant qu'il rejette la demande des requérants, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi.<br/>
<br/>
              10. Il y a lieu de régler l'affaire au fond, dans cette mesure, en application de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              Sur la régularisation du permis initial :<br/>
<br/>
              11. Lorsqu'un permis de construire a été délivré en méconnaissance des dispositions législatives ou réglementaires relatives à l'utilisation du sol ou sans que soient respectées des formes ou formalités préalables à la délivrance des permis de construire, l'illégalité qui en résulte peut être régularisée dès lors que le permis modificatif ou de régularisation assure le respect des règles de fond applicables au projet en cause, répond aux exigences de forme ou a été précédé de l'exécution régulière de la ou des formalités qui avaient été omises. Il peut, de même, être régularisé par un permis modificatif ou de régularisation si la règle relative à l'utilisation du sol qui était méconnue par le permis initial a été entretemps modifiée. Un permis de régularisation délivré en vertu de l'article L. 600-5-1 peut revoir l'économie générale du projet, sous réserve de ne pas lui apporter un bouleversement tel qu'il en changerait la nature même.<br/>
<br/>
              12. En premier lieu, aux termes de l'article R. 431-10 du code de l'urbanisme : " Le projet architectural comprend (...) c) Un document graphique permettant d'apprécier l'insertion du projet de construction par rapport aux constructions avoisinantes et aux paysages, son impact visuel ainsi que le traitement des accès et du terrain ; / d) Deux documents photographiques permettant de situer le terrain respectivement dans l'environnement proche et, sauf si le demandeur justifie qu'aucune photographie de loin n'est possible, dans le paysage lointain. Les points et les angles des prises de vue sont reportés sur le plan de situation et le plan de masse ".<br/>
<br/>
              13. Il ressort des pièces du dossier que le dossier accompagnant la demande de permis de régularisation comporte des documents, notamment photographiques, conformes aux prescriptions réglementaires rappelées au point précédent, de nature à permettre à l'administration d'apprécier la situation du projet dans son environnement proche et lointain, son impact visuel ainsi que son insertion par rapport aux constructions avoisinantes et au paysage du massif des Calanques dans lequel est situé le projet. Il ne ressort pas des pièces du dossier que, comme le soutiennent les requérants, ces documents livreraient une vision discordante, faussée ou tendancieuse de la réalité. Le vice tiré de la méconnaissance des dispositions de l'article R. 431-10 du code de l'urbanisme ayant ainsi été régularisé, ce moyen ne peut qu'être écarté.<br/>
<br/>
              14. En deuxième lieu, les dispositions de l'article UI 11 du règlement du plan d'occupation des sols, aux termes desquelles : " Les constructions à édifier s'inscrivent en harmonie avec les composantes, bâties ou non, du site environnant ou dans la perspective de sa valorisation (...) ", n'étaient plus applicables à la date du permis de régularisation, sans avoir été remplacées par des dispositions analogues. Il résulte de ce qui a été dit au point 11 que leur méconnaissance ne peut dès lors plus être invoquée. <br/>
<br/>
              15. En troisième lieu, l'article R. 111-21, devenu R. 111-27 du code de l'urbanisme à la date du permis de régularisation, dispose que : " Le projet peut être refusé ou n'être accepté que sous réserve de l'observation de prescriptions spéciales si les constructions, par leur situation, leur architecture, leurs dimensions ou l'aspect extérieur des bâtiments ou ouvrages à édifier ou à modifier, sont de nature à porter atteinte au caractère ou à l'intérêt des lieux avoisinants, aux sites, aux paysages naturels ou urbains ainsi qu'à la conservation des perspectives monumentales ".<br/>
<br/>
              16. Il ressort des pièces du dossier que le terrain d'assiette du projet est situé en périphérie de la zone urbanisée de la commune de Marseille, au sein du site inscrit des Calanques, à l'ouest d'une très vaste zone naturelle incluse dans le coeur du Parc national des Calanques, dont la limite est située à environ 100 mètres du projet. Par ailleurs, le tissu urbain environnant, situé au nord, à l'est et au sud du projet, est composé majoritairement de maisons individuelles d'architecture hétérogène, de style traditionnel ou contemporain.<br/>
<br/>
              17. Le projet en litige, tel que modifié par l'arrêté du 31 janvier 2019, prévoit la réalisation de quatorze villas en R+1 d'architecture contemporaine avec des toitures terrasses. Il ressort des pièces du dossier que ce projet, situé en fond de parcelle à environ 130 mètres du chemin de Sormiou, sera peu visible depuis la voie publique et que les villas projetées, de 6 mètres de hauteur, d'architecture sobre et contemporaine et entourées d'un espace végétalisé, ne sont pas de nature à porter une atteinte manifeste à l'intérêt ou au caractère du site et des lieux avoisinants, nonobstant la circonstance que l'architecte des Bâtiments de France, consulté lors de l'instruction des demandes de permis de construire initial et de régularisation, ait émis des avis défavorables le 20 avril 2013 et le 26 octobre 2018. Par ailleurs, la seule circonstance que le terrain d'assiette soit situé dans le périmètre d'une zone naturelle d'intérêt écologique, faunistique et floristique ne caractérise pas, en elle-même, l'existence d'un intérêt particulier à préserver d'un point de vue paysager. Dans ces conditions, le moyen tiré de l'existence d'une erreur manifeste d'appréciation au regard des dispositions citées au point 15 doit être écarté.<br/>
<br/>
              Sur les moyens propres au permis de régularisation :<br/>
<br/>
              18. En premier lieu, il ressort des pièces du dossier que le permis de régularisation a apporté au projet des modifications qui ne se bornaient pas à remédier au vice à régulariser. Contrairement à ce que soutiennent les requérants, ces modifications, consistant principalement dans la suppression, sur la partie la plus élevée du terrain, de deux villas et la réduction de la hauteur de deux autres, le reste du projet, situé en contrebas, étant identique à celui pour lequel le permis initial a été obtenu, ne peuvent être regardés comme changeant la nature même du projet. Ils ne sont dès lors pas fondés à soutenir que ces modifications ne pouvaient légalement être apportées par un permis de régularisation et nécessitaient un nouveau permis de construire.<br/>
<br/>
              19. En deuxième lieu, aux termes du dernier alinéa de l'article L. 153-11 du code de l'urbanisme : " L'autorité compétente peut décider de surseoir à statuer, dans les conditions et délai prévus à l'article L. 424-1, sur les demandes d'autorisation concernant des constructions, installations ou opérations qui seraient de nature à compromettre ou à rendre plus onéreuse l'exécution du futur plan dès lors qu'a eu lieu le débat sur les orientations générales du projet d'aménagement et de développement durable". L'article L. 424-1 de ce code dispose que : " Il peut être sursis à statuer sur toute demande d'autorisation concernant des travaux, constructions ou installations dans les cas prévus (...) aux articles L. 153-11 (...) du présent code ".<br/>
<br/>
              20. Un sursis à statuer ne peut être opposé à une demande de permis de construire, sur le fondement de ces dispositions, postérieurement au débat sur les orientations générales du projet d'aménagement et de développement durable, qu'en vertu d'orientations ou de règles que le futur plan local d'urbanisme pourrait légalement prévoir et à la condition que la construction, l'installation ou l'opération envisagée soit de nature à compromettre ou à rendre plus onéreuse son exécution.<br/>
<br/>
              21. Les requérants soutiennent que le maire de Marseille aurait commis une erreur manifeste d'appréciation en ne sursoyant pas à statuer sur la demande de permis de régularisation en application de ces dispositions, en faisant valoir que le projet comportait une emprise au sol supérieure à celle permise par le plan local d'urbanisme intercommunal en cours d'adoption. Ce moyen est toutefois inopérant, eu égard aux droits que le pétitionnaire tient du permis initial à compter du jugement ayant eu recours à l'article L. 600-5-1, dès lors que la surface d'emprise au sol du projet modifié n'est pas accrue par rapport au permis initial. Au demeurant, la seule circonstance que son emprise au sol soit supérieure à celle projetée par le futur plan ne pouvait être regardée comme de nature à en compromettre l'exécution. <br/>
<br/>
              22. Enfin, les moyens tirés de la méconnaissance par le projet des règles du plan local d'urbanisme entré en vigueur à la date du permis de régularisation, relatives à l'implantation des constructions en zone de frange urbaine, aux aires de retournement à aménager pour permettre les manoeuvres des véhicules de lutte contre l'incendie et de secours et à la distance d'implantation des moyens de défense extérieure contre l'incendie par rapport aux maisons se rapportent, non au permis de régularisation, mais au permis initial. Ils ne peuvent dès lors être utilement invoqués.<br/>
<br/>
              23. Il résulte de tout ce qui précède que la requête de M. et Mme I... et autres doit être rejetée.<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              24. Aux termes de l'article L. 761-1 du code de justice administrative : " Dans toutes les instances, le juge condamne la partie tenue aux dépens ou, à défaut, la partie perdante, à payer à l'autre partie la somme qu'il détermine, au titre des frais exposés et non compris dans les dépens (...) ". Il résulte de ces dispositions que le paiement des sommes exposées et non comprises dans les dépens ne peut être mis à la charge que de la partie qui perd pour l'essentiel. La circonstance qu'au vu de la régularisation intervenue en cours d'instance, le juge rejette finalement les conclusions dirigées contre la décision initiale, dont le requérant était fondé à soutenir qu'elle était illégale et dont il est, par son recours, à l'origine de la régularisation, ne doit pas à elle seule, pour l'application de ces dispositions, conduire le juge à mettre les frais à sa charge ou à rejeter les conclusions qu'il présente à ce titre. Il y a lieu, dans les circonstances de l'espèce, de rejeter l'ensemble des conclusions des parties présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le jugement du 7 novembre 2019 du tribunal administratif de Marseille est annulé en tant qu'il a rejeté les conclusions des demandeurs autres que celles portant sur les conclusions reconventionnelles de M. M....<br/>
Article 2 : La requête de M. et Mme I... et autres est rejetée.<br/>
Article 3 : Les conclusions présentées par les parties au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à M. et Mme H... et Annie I..., premiers dénommés, à M. B... M... et à la commune de Marseille.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-06-05-11 PROCÉDURE. JUGEMENTS. FRAIS ET DÉPENS. REMBOURSEMENT DES FRAIS NON COMPRIS DANS LES DÉPENS. - PARTIE PERDANTE (ART. L. 761-1 DU CJA) - 1) PARTIE QUI PERD POUR L'ESSENTIEL [RJ1] - 2) CAS D'UNE REQUÊTE CONTRE UN PERMIS DE CONSTRUIRE REJETÉE À LA SUITE D'UNE RÉGULARISATION INTERVENUE EN COURS D'INSTANCE - CIRCONSTANCE PERMETTANT DE REGARDER LES REQUÉRANTS COMME LA PARTIE QUI PERD POUR L'ESSENTIEL - ABSENCE [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-06-04 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. POUVOIRS DU JUGE. - SURSIS À STATUER EN VUE DE LA RÉGULARISATION D'UNE AUTORISATION D'URBANISME (ART. L. 600-5-1 DU CODE DE L'URBANISME) - RÉGULARISATION EN COURS D'INSTANCE - CONSÉQUENCE - REQUÉRANTS DEVANT ÊTRE REGARDÉS COMME LA PARTIE PERDANTE AU SENS DE L'ARTICLE L. 761-1 DU CJA - ABSENCE [RJ2].
</SCT>
<ANA ID="9A"> 54-06-05-11 1) Il résulte de l'article L. 761-1 du code de justice administrative (CJA) que le paiement des sommes exposées et non comprises dans les dépens ne peut être mis à la charge que de la partie qui perd pour l'essentiel.... ,,2) Recours contre une autorisation d'urbanisme.,,,La circonstance qu'au vu de la régularisation intervenue en cours d'instance, le juge rejette finalement les conclusions dirigées contre la décision initiale, dont le requérant était fondé à soutenir qu'elle était illégale et dont il est, par son recours, à l'origine de la régularisation, ne doit pas à elle seule, pour l'application de ces dispositions, conduire le juge à mettre les frais à sa charge ou à rejeter les conclusions qu'il présente à ce titre. Il y a lieu, dans les circonstances de l'espèce, de rejeter l'ensemble des conclusions des parties présentées au titre de l'article L. 761-1 du CJA.</ANA>
<ANA ID="9B"> 68-06-04 La circonstance qu'au vu de la régularisation intervenue en cours d'instance, le juge rejette finalement les conclusions dirigées contre la décision initiale, dont le requérant était fondé à soutenir qu'elle était illégale et dont il est, par son recours, à l'origine de la régularisation, ne doit pas à elle seule, pour l'application de ces dispositions, conduire le juge à mettre les frais à sa charge ou à rejeter les conclusions qu'il présente à ce titre. Il y a lieu, dans les circonstances de l'espèce, de rejeter l'ensemble des conclusions des parties présentées au titre de l'article L. 761-1 du code de justice administrative (CJA).</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 13 mars 1992, Ministre de l'agriculture et de la forêt c/ Groupement foncier agricole de la Noë, n° 106680, T. p. 1229 ; CE, 19 juin 2017, Syndicat des copropriétaires de la résidence Butte Stendhal et autres, n°s 394677 397149, T. pp. 525-743-750-756-857-859-962.,,[RJ2] Ab. jur., sur ce point, CE, 19 juin 2017, Syndicat des copropriétaires de la résidence Butte Stendhal et autres, n°s 394677 397149, T. pp. 525-743-750-756-857-859-962.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
