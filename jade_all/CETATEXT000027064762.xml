<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027064762</ID>
<ANCIEN_ID>JG_L_2013_02_000000357947</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/06/47/CETATEXT000027064762.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 13/02/2013, 357947, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-02-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>357947</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean-Baptiste de Froment</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:357947.20130213</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 26 mars 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par M. et Mme B...A..., demeurant...,; M. et Mme A...demandent au Conseil d'Etat d'annuler la décision du 19 mars 2012 par laquelle le président de l'ordre des avocats au Conseil d'Etat et à la Cour de cassation a refusé de faire droit à leur demande de désignation d'un avocat en vue de présenter une demande de rectification pour erreur matérielle ou d'un recours en révision des décisions nos 336605, 336606, 336607, 336608 et 336609 du Conseil d'Etat statuant au contentieux ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 24 janvier 2013, présentée par M. et Mme A... ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule et son article 61-1 ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu l'ordonnance du 10 septembre 1817 modifiée ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ; <br/>
<br/>
              Vu la loi n° 79-587 du 11 juillet 1979 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Baptiste de Froment, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
<br/>
<br/>Sur la question prioritaire de constitutionnalité :<br/>
<br/>
              1. Considérant qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) " ; qu'il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2. Considérant que, pour les motifs énoncés par la décision n° 358426 du 22 juin 2012 du Conseil d'Etat statuant au contentieux, la question soulevée par les requérants de la conformité aux droits et libertés garantis par la Constitution des dispositions de l'article L. 111-1 du code de justice administrative, qui n'est pas nouvelle, ne présente pas un caractère sérieux ;<br/>
<br/>
              3. Considérant qu'il résulte de ce qui précède qu'il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ; <br/>
<br/>
              Sur la décision du président de l'ordre des avocats au Conseil d'Etat et à la Cour de cassation :<br/>
<br/>
              4. Considérant que l'ordre des avocats au Conseil d'Etat et à la Cour de cassation est un auxiliaire du service public de la justice ; qu'à ce titre, il incombe à son président d'apprécier, sous le contrôle du juge de la légalité, s'il y a lieu de faire droit à une demande de désignation d'un avocat de cet ordre pour former devant le Conseil d'Etat une requête en vue de laquelle l'intéressé n'a obtenu l'accord d'aucun avocat pour l'assister ; qu'une telle demande a pour effet d'interrompre le délai du recours que l'intéressé envisageait d'introduire ; qu'elle ne peut être rejetée que si la requête projetée est manifestement dépourvue de chances raisonnables de succès ; qu'il appartient au Conseil d'Etat, saisi par un pourvoi de l'intéressé lui-même dispensé du ministère d'avocat, de statuer sur la légalité de la décision prise au nom de l'ordre ; que, compte tenu de ces garanties, la circonstance que l'ordre refuse de désigner l'un de ses membres, alors même que la recevabilité de la requête est subordonnée à sa présentation par un avocat au Conseil d'Etat et à la Cour de cassation, ne constitue pas, par elle-même, une méconnaissance du principe constitutionnel du droit pour les personnes intéressées d'exercer un recours effectif devant une juridiction, rappelé par les stipulations de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              5. Considérant, en premier lieu, que la décision par laquelle le président de l'ordre des avocats au Conseil d'Etat et à la Cour de cassation refuse de désigner un avocat de cet ordre pour former devant le Conseil d'Etat une requête en vue de laquelle le demandeur n'a obtenu l'accord d'aucun avocat pour l'assister ne saurait être regardée comme l'exercice d'une prérogative de puissance publique se rattachant à la mission de service public dont est investi l'ordre ; qu'ainsi, une telle décision n'a pas le caractère d'une décision administrative et n'est dès lors, en tout état de cause, pas au nombre des décisions qui doivent être motivées en application de l'article 1er de la loi du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public ; <br/>
<br/>
              6. Considérant, toutefois, qu'eu égard aux effets attachés à une telle décision, qui est susceptible d'affecter l'exercice du droit au recours, la décision du président de l'ordre des avocats au Conseil d'Etat et à la Cour de cassation doit comporter une motivation de nature à éclairer les personnes intéressées sur les motifs du refus de l'ordre de désigner l'un de ses membres pour présenter un recours ; que, contrairement à ce qui est soutenu, en indiquant, dans les circonstances de l'espèce, à M. et Mme A...que leur projet de présenter une demande de rectification pour erreur matérielle devant le Conseil d'Etat était manifestement dépourvu de chances raisonnables de succès, le président de l'ordre des avocats au Conseil d'Etat et à la Cour de cassation a suffisamment motivé sa décision ;<br/>
<br/>
              7. Considérant, en second lieu, qu'au soutien de leur demande adressée le 15 mars 2012 au président de l'ordre des avocats au Conseil d'Etat et à la Cour de cassation, tendant à la désignation d'office d'un avocat de cet ordre en vue de les assister pour former un recours en rectification d'erreur matérielle contre la décision du Conseil d'Etat statuant au contentieux n° 336605 et autres du 11 janvier 2012, M. et Mme A...se sont bornés à renvoyer à des écritures déjà entièrement rédigées par leurs soins dont ils entendaient qu'elles fussent " soutenues en temps opportun " par l'avocat dont ils demandaient la désignation ; que, eu égard à la teneur des écritures que les requérants entendaient soumettre à la juridiction tout en faisant obstacle à la mission de conseil des avocats au Conseil d'Etat et à la Cour de cassation, le recours projeté était manifestement dépourvu de chances raisonnables de succès ; que, par suite, le président de l'ordre n'a pas méconnu le principe à valeur constitutionnelle du droit pour les personnes intéressées d'exercer un recours effectif devant la juridiction, rappelé par les stipulations de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, en rejetant, par une décision du 19 mars 2012, la demande de M. et Mme A...tendant à la désignation d'un avocat de cet ordre pour les assister dans ce recours ; <br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que M. et Mme A...ne sont pas fondés à demander l'annulation de la décision attaquée ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M. et MmeA.... <br/>
<br/>
      Article 2 : La requête de M. et Mme A...est rejetée. <br/>
Article 3 : La présente décision sera notifiée à M. et Mme B...A...et à l'ordre des avocats au Conseil d'Etat et à la Cour de cassation. Copie en sera adressée au Premier ministre et au Conseil constitutionnel.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
