<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028781996</ID>
<ANCIEN_ID>JG_L_2014_03_000000355324</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/78/19/CETATEXT000028781996.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  3ème sous-section jugeant seule, 26/03/2014, 355324, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-03-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>355324</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 3ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE ; LE PRADO</AVOCATS>
<RAPPORTEUR>Mme Agnès Martinel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:355324.20140326</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 29 décembre 2011 et 29 mars 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. A... C..., demeurant au... ; M. C... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 09PA02720 du 29 novembre 2011 de la cour administrative d'appel de Paris en tant qu'il a rejeté le surplus des conclusions de sa requête tendant à l'annulation du jugement n° 0316950/2 du 12 mars 2009 par lequel le tribunal administratif de Paris a rejeté sa demande tendant à la décharge des cotisations supplémentaires d'impôt sur le revenu et de prélèvements sociaux ainsi que des pénalités correspondantes auxquelles il a été assujetti au titre des années 1994, 1995 et 1996 ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention fiscale franco-suisse du 9 septembre 1966 en matière d'impôts sur le revenu et sur la fortune modifiée ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Agnès Martinel, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me B...et à la SCP Rousseau, Tapie, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'à l'issue d'un examen contradictoire de leur situation fiscale personnelle au titre des années 1994 à 1996, divers rehaussements de leur revenu imposable ont été notifiés à M. et Mme C... ; que M. C... se pourvoit en cassation contre l'arrêt du 29 novembre 2011 de la cour administrative d'appel de Paris rejetant, après avoir constaté un non lieu partiel à statuer, le surplus des conclusions de sa requête tendant à l'annulation du jugement du 12 mars 2009 par lequel le tribunal administratif de Paris a rejeté sa demande tendant à la décharge des cotisations supplémentaires d'impôt sur le revenu et de prélèvements sociaux ainsi que des pénalités correspondantes auxquelles il a été assujetti au titre des trois années vérifiées ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 4 de la convention fiscale franco-suisse du 9 septembre 1966 : " 1. Au sens de la présente convention, l'expression "résident d'un Etat contractant" désigne toute personne qui, en vertu de la législation dudit Etat, est assujettie à l'impôt dans cet Etat en raison de son domicile, de sa résidence, de son siège de direction ou de tout autre critère de nature analogue. /2. Lorsque, selon la disposition du paragraphe 1, une personne est considérée comme résident de chacun des Etats contractants, le cas est résolu d'après les règles suivantes : /a) Cette personne est considérée comme résident de l'Etat contractant où elle dispose d'un foyer d'habitation permanent, cette expression désignant le centre des intérêts vitaux, c'est-à-dire le lieu avec lequel les relations personnelles sont les plus étroites (...) " ;<br/>
<br/>
              3. Considérant qu'il ressort des énonciations de l'arrêt attaqué que la cour a notamment relevé qu'au cours des années vérifiées, M. C...avait exercé la fonction de président directeur général de la société S.A Copechim France dont le siège est établi en France et qu'il avait perçu à ce titre des salaires à hauteur de 357 309 F pour 1994, 428 439 F pour 1995 et 351 005 F pour 1996 ; qu'elle a estimé qu'il ne résultait pas de l'instruction que les fonctions de dirigeant de sociétés suisses exercées par M. C...et dont il se prévalait pour soutenir qu'il avait en Suisse le centre de ses intérêts vitaux au sens de l'article 4 précité de la convention franco-suisse, auraient été prépondérantes par rapport à celles exercées en France ; qu'elle en a déduit que l'intéressé n'était pas fondé à soutenir qu'il aurait eu en Suisse le centre de ses intérêts vitaux au sens de l'article 4 de cette convention ; <br/>
<br/>
              4. Considérant toutefois qu'il ressort des pièces du dossier soumis aux juges du fond que le contribuable avait produit des documents attestant de la perception  de salaires versés par la société suisse Comser pour un montant annuel près de neuf fois supérieur à celui des salaires perçus en France par l'intéressé ; qu'en écartant ainsi le caractère prépondérant des fonctions exercées en Suisse par le contribuable en dépit de cet écart important de rémunérations, la cour a dénaturé les pièces du dossier ; que par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, M. C... est fondé à demander l'annulation de l'article 2 de l'arrêt qu'il attaque ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à M. C..., au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 2 de l'arrêt du 29 novembre 2011 de la cour administrative d'appel de Paris est annulé. <br/>
<br/>
Article 2 : L'Etat versera la somme de 3 000 euros à M. C...au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. A...C...et au ministre de l'économie et des finances. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
