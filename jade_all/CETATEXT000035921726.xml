<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035921726</ID>
<ANCIEN_ID>JG_L_2017_10_000000410018</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/92/17/CETATEXT000035921726.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 23/10/2017, 410018, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-10-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>410018</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Pauline Jolivet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:410018.20171023</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une ordonnance n° 1700001 du 10 avril 2017, enregistrée au secrétariat du contentieux du Conseil d'Etat le 21 avril 2017, le président du tribunal administratif de Saint-Martin a transmis au Conseil d'Etat, en application des dispositions de l'article R. 351-2 du code de justice administrative, la requête du préfet délégué auprès du représentant de l'Etat à Saint-Barthélemy et Saint-Martin.<br/>
<br/>
              Par cette requête, enregistrée au greffe du tribunal administratif le 3 janvier 2017, le préfet délégué auprès du représentant de l'Etat à Saint-Barthélemy et Saint-Martin demande l'annulation de la délibération du conseil territorial de la collectivité de Saint-Martin n° CT 28-05-2016 du 30 juin 2016. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - la Constitution, notamment son article 74 ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code général de la propriété des personnes publiques ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Jolivet, maître des requêtes,  en service extraordinaire,<br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. La collectivité de Saint-Martin, créée par la loi organique du 21 février 2007 portant dispositions statutaires et institutionnelles relatives à l'outre-mer, est, en vertu de l'article L.O. 6311-1 du code général des collectivités territoriales, une collectivité d'outre-mer régie par l'article 74 de la Constitution et dotée de l'autonomie. Aux termes de l'article 74 de la Constitution : " Les collectivités d'outre-mer régies par le présent article ont un statut qui tient compte des intérêts propres de chacune d'elles au sein de la République. Ce statut est défini par une loi organique, adoptée après avis de l'assemblée délibérante, qui fixe:/ (...) - les compétences de cette collectivité ; (...)/ La loi organique peut également déterminer, pour celles de ces collectivités qui sont dotées de l'autonomie, les conditions dans lesquelles:/ - le Conseil d'État exerce un contrôle juridictionnel spécifique sur certaines catégories d'actes de l'assemblée délibérante intervenant au titre des compétences qu'elle exerce dans le domaine de la loi ; (...) ". Il résulte des dispositions combinées des articles L.O. 6342-1, L.O. 6343-1, L.O. 6343-2, L.O. 6343-4 et L.O. 6351-2 du même code que le Conseil d'Etat, saisi par le représentant de l'Etat dans la collectivité d'un recours contre un acte du conseil territorial de la collectivité intervenant dans le domaine de la loi, statue sur la conformité de cet acte au regard de la Constitution, des lois organiques, des engagements internationaux de la France et des principes généraux du droit.<br/>
<br/>
              2. Aux termes de l'article L.O. 6313-4 du code général des collectivités territoriales : " Les lois, ordonnances et décrets intervenus avant l'entrée en vigueur de la loi organique n° 2007-223 du 21 février 2007 portant dispositions statutaires et institutionnelles relatives à l'outre-mer dans des matières qui relèvent de la compétence des autorités de la collectivité peuvent être modifiés ou abrogés, en tant qu'ils s'appliquent à Saint-Martin, par les autorités de la collectivité selon les procédures prévues par le présent livre. / Lorsqu'elles usent de la faculté qui leur est offerte par le premier alinéa, les autorités de la collectivité doivent prononcer l'abrogation expresse de la disposition législative ou réglementaire précédemment en vigueur et procéder à l'édiction formelle d'une nouvelle disposition ". Sur le fondement de cette disposition, par la délibération attaquée, le conseil territorial de Saint-Martin a, à son article 2, abrogé, dans la mesure où il est applicable à Saint-Martin, le dernier alinéa de l'article L. 2111-4 du code général de la propriété des personnes publiques, qui prévoit que " les terrains soustraits artificiellement à l'action du flot demeurent... : " Les terrains soustraits artificiellement à l'action du flot sur décision de la Collectivité de Saint-Martin sont classés dans le domaine privé de la Collectivité pour autant qu'ils ne sont pas, ou ne seront pas de façon certaine eu égard aux circonstances de droit et de fait, soit affectés à l'usage direct du public, soit affectés à un service public pourvu qu'en ce cas ils fassent l'objet d'un aménagement indispensable à l'exécution des missions de ce service public ". <br/>
<br/>
              3. D'une part, l'article L. 2111-4 du code général de la propriété des personnes publiques dispose que : " Le domaine public maritime naturel de l'Etat comprend :/ 1° Le sol et le sous-sol de la mer entre la limite extérieure de la mer territoriale et, côté terre, le rivage de la mer. (...) ". D'autre part, l'article L.O. 6314-6 du code général des collectivités territoriales dispose, à son troisième alinéa, que : " Le domaine public maritime de la collectivité comprend, sous réserve des droits de l'Etat et des tiers, la zone dite des cinquante pas géométriques, les rivages de la mer, le sol et le sous-sol des eaux intérieures, en particulier les rades et les lagons, ainsi que le sol et le sous-sol des eaux territoriales/(...) ". Il résulte de ces dispositions que, par exception aux dispositions du 1° de l'article L. 2111-4 du code général de la propriété des personnes publiques, les éléments énumérés par ces dernières dispositions relèvent, à Saint-Martin, du domaine public maritime de la collectivité et non de celui de l'Etat. Il s'ensuit que la collectivité de Saint-Martin n'était pas compétente pour abroger le dernier alinéa de l'article L. 2111-4 du code général de la propriété des personnes publiques, aux termes duquel : " Les terrains soustraits artificiellement à l'action du flot demeurent compris dans le domaine public maritime naturel ", et, à son article 3, adopté la disposition suivante(... " qui, ne régissant que le domaine public maritime naturel de l'Etat, ne s'applique pas au domaine public maritime naturel de Saint-Martin. L'article 2 de la délibération n° CT 28-05-2016 du 30 juin 2016 du conseil territorial de Saint-Martin doit donc être annulé.<br/>
<br/>
              4. Contrairement à ce qui est soutenu, l'article 3 de la délibération attaquée, adopté par la collectivité de Saint-Martin en vertu de l'article L.O. 6351-2 du code général des collectivités territoriales, qui dispose que : " Le conseil territorial fixe les règles applicables à Saint-Martin dans les matières énumérées à l'article L.O. 6314-3 ", au nombre desquelles figurent le droit domanial et des biens de la collectivité, définit de manière suffisamment précise les modalités du déclassement des terres exondées du rivage de la mer en prévoyant de classer les terrains soustraits artificiellement à l'action du flot dans le domaine privé de la collectivité tout en réservant le cas des terrains affectés à l'usage du public ou à un service public.<br/>
<br/>
              5. Il résulte de ce qui précède que le préfet n'est fondé à demander, par les moyens qu'il invoque, l'annulation de la délibération attaquée qu'en tant qu'elle abroge le dernier alinéa de l'article L. 2111-4 du code général de la propriété des personnes publiques.<br/>
<br/>
              6. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la collectivité de Saint-Martin au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'article 2 de la délibération n° CT 28-05-2016 du 30 juin 2016 du conseil territorial de Saint-Martin est annulé.<br/>
Article 2 : Le surplus des conclusions de la requête du préfet délégué auprès du représentant de l'Etat à Saint-Barthélemy et Saint-Martin est rejeté.<br/>
<br/>
Article 3 : Les conclusions de la collectivité de Saint-Martin au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au préfet délégué de Saint-Barthélemy et de Saint-Martin et à la collectivité de Saint-Martin. <br/>
Copie en sera adressée à la ministre des outre-mer.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
