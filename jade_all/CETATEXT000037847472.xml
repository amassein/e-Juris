<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037847472</ID>
<ANCIEN_ID>JG_L_2018_12_000000410187</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/84/74/CETATEXT000037847472.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 21/12/2018, 410187, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-12-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>410187</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Cédric  Zolezzi</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:410187.20181221</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              1°) Sous le n°410187, par une requête et un mémoire en réplique, enregistrés les 27 avril 2017 et 11 juin 2018 au secrétariat du contentieux du Conseil d'Etat, le Syndicat national autonome des orthoptistes, la Fédération française des diabétiques, M. C... D...et Mme A...B...demandent au Conseil d'Etat :<br/>
<br/>
              1°) à titre principal, d'annuler pour excès de pouvoir d'une part le 2° de l'article 1er du décret n° 2016-1670 du 5 décembre 2016 relatif à la définition des actes d'orthoptie et aux modalités d'exercice de la profession d'orthoptiste et d'autre part la décision implicite de rejet née du silence gardé par le Premier ministre et la ministre des affaires sociales et de la santé sur la demande qu'ils ont reçue le 3 février 2017 tendant au retrait de ces dispositions ; <br/>
<br/>
              2°) à titre subsidiaire, d'annuler l'ensemble de ce décret ainsi que la décision implicite rejetant leur demande de retrait ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement d'une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              2°) Sous le n°411257, par une requête et un mémoire en réplique, enregistrés les 6 juin 2017 et 14 mai 2018 au secrétariat du contentieux du Conseil d'Etat, l'Association des optométristes de France demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2016-1670 du 5 décembre 2016 relatif à la définition des actes d'orthoptie et aux modalités d'exercice de la profession d'orthoptiste ;<br/>
<br/>
              2°) à titre subsidiaire, d'annuler le refus né du silence gardé par la ministre des solidarités et de la santé sur la demande d'abrogation de ce décret dont il l'a saisi et d'enjoindre à la ministre des solidarités et de la santé de réexaminer cette demande dans un délai d'un mois à compter de la décision du Conseil d'Etat sous  astreinte de 300 euros, par jour de retard au-delà de ce délai, à verser à l'association des optométristes de France ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement d'une somme de 5 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
<br/>
              - la Constitution, notamment son  Préambule ;<br/>
<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              - le code de la santé publique ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Cédric Zolezzi, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que les requêtes du syndicat national autonome des orthoptistes et autres et de l'association des optométristes de France sont dirigées contre le même décret ; qu'il y a lieu de les joindre pour statuer par une même décision ;<br/>
<br/>
              Sur les moyens tirés de vices qui entacheraient la procédure d'adoption du décret attaqué :<br/>
<br/>
              2. Considérant en premier lieu qu'il ressort des pièces versées au dossier par la ministre des solidarités et de la santé, que le décret attaqué, pris après consultation de la section sociale du Conseil d'Etat, qui s'est prononcée dans sa séance du 8 novembre 2016, ne contient aucune disposition différant de celles qui ont été adoptées par le Conseil d'Etat ; que, par suite, le moyen tiré d'une méconnaissance des règles d'examen des projets de décret par le Conseil d'Etat doit être écarté ;<br/>
<br/>
              3. Considérant en deuxième lieu que ce décret n'appelle aucune mesure d'exécution relevant de la compétence ni du ministre chargé de l'économie, ni du ministre chargé du budget ni du ministre chargé de l'enseignement supérieur ; que le moyen tiré de ce qu'il devait comporter le contreseing de ces ministres doit, par suite, être écarté ;<br/>
<br/>
              4. Considérant, en troisième lieu, qu'aucune disposition ni aucun principe n'imposait que l'adoption du décret fût précédée de la consultation du Conseil national de l'ordre des médecins ; <br/>
<br/>
              5. Considérant, en quatrième lieu, que le moyen tiré de ce que la consultation du Haut conseil des professions paramédicales entacherait sa légalité ne peut qu'être écarté, le Gouvernement ayant toujours la faculté de procéder à une consultation non obligatoire, alors au surplus que la consultation de cette instance était rendue obligatoire par les dispositions des articles D. 4381-1 et D. 4382-2 du code de la santé publique ; <br/>
<br/>
              Sur les moyens tirés de l'incompétence de l'auteur du décret attaqué : <br/>
<br/>
              6. Considérant qu'aux termes de l'article L. 4342-1 du code de la santé publique : " La pratique de l'orthoptie comporte la promotion de la santé, la prévention, le bilan orthoptique et le traitement des altérations de la vision fonctionnelle sur les plans moteur, sensoriel et fonctionnel ainsi que l'exploration de la vision. / L'orthoptiste pratique son art sur prescription médicale ou, dans le cadre notamment du cabinet d'un médecin ophtalmologiste, sous la responsabilité d'un médecin. / Il dépiste, évalue, rééduque, réadapte et explore les troubles de la vision, du nourrisson à la personne âgée. Il participe à la prévention des risques et incapacités potentiels. / L'orthoptiste exerce son activité en toute indépendance et en pleine responsabilité, conformément aux règles professionnelles prévues au 1° de l'article L. 4342-7 /. La définition des actes d'orthoptie est précisée par un décret en Conseil d'Etat pris après avis de l'Académie nationale de médecine. " ; qu'aux termes de l'article L. 4342-7 du même code : " Sont déterminés par décret en Conseil d'Etat : / 1° Les règles professionnelles (...) " ;<br/>
<br/>
              7. Considérant que le décret attaqué, pris pour l'application de ces dispositions, modifie les règles d'exercice de la profession d'orthoptiste, en permettant notamment à certains orthoptistes de réaliser les actes prévus aux articles R. 4342-2 et  R. 4342-4 à R. 4342-7 du code de la santé publique sans prescription médicale préalable lorsqu'ils s'inscrivent dans la mise en oeuvre d'un protocole organisationnel, accord conclu avec un ou plusieurs médecins ophtalmologistes dont le décret institue le principe, définit le champ d'application et décrit le contenu et la procédure de conclusion ; que le décret réserve cette possibilité aux seuls orthoptistes mentionnés  au II de l'article R. 4342-1-1 et exerçant soit dans le cadre du cabinet d'un médecin ophtalmologiste, soit au sein d'un établissement de santé, soit dans les services de santé décrits au titre III du livre III de la sixième partie du code de la santé publique, relatif notamment aux réseaux de santé, centres de santé et maisons de santé, soit dans les hôpitaux et centres médicaux des armées ; qu'il résulte des termes mêmes du décret attaqué, qui, contrairement à ce que soutient le syndicat requérant, n'est entaché sur ce point d'aucune méconnaissance des objectifs de clarté et d'intelligibilité de la norme ni du principe de sécurité juridique, que les orthoptistes libéraux ne répondant pas à ces conditions ne peuvent réaliser ces actes que sur prescription médicale préalable ; <br/>
<br/>
              8. Considérant, en premier lieu, que les protocoles organisationnels prévus par les dispositions du décret attaqué entre certains orthoptistes et les médecins ophtalmologistes diffèrent, tant par leur contenu que par leur champ d'application et leur procédure d'adoption, des protocoles de coopération prévus aux articles L. 4011-1 à L. 4011-3 du code de la santé publique dont l'objet est de permettre aux professionnels de santé d'organiser entre eux des transferts d'activité ou d'actes de soins ou une réorganisation de leurs modes d'intervention, après autorisation de l'agence régionale de santé et avis de la Haute autorité de santé ; que, dès lors, le moyen tiré de ce que le décret attaqué serait entaché d'incompétence au regard des dispositions applicables à ces protocoles de coopération est inopérant ; <br/>
<br/>
              9. Considérant en deuxième lieu, que le décret attaqué a inséré au code de la santé publique un article R. 4342-1-1 dont le III dispose que " En cas d'urgence et en l'absence d'un médecin, l'orthoptiste est habilité à accomplir les premiers actes de soins nécessaires en orthoptie. Un compte-rendu des actes accomplis dans ces conditions est transmis au médecin dès son intervention. " ; qu'une telle disposition relève des règles professionnelles au sens des dispositions citées ci-dessus de l'article L. 4342-7 du code de la santé publique, et n'est dès lors pas entachée d'incompétence au regard des dispositions précitées du code de la santé publique ; <br/>
<br/>
              Sur la légalité interne du décret :<br/>
<br/>
              10. Considérant en premier lieu, que les dispositions précitées de l'article L. 4342-1 du code de la santé publique ne font pas obstacle à ce que le pouvoir réglementaire, en définissant les actes d'orthoptie et les conditions d'exercice de la profession d'orthoptiste, rende possible la conclusion de protocoles organisationnels entre certains orthoptistes et des médecins ophtalmologistes, ni d'imposer que la conclusion de tels protocoles soit autorisée à tous les praticiens orthoptistes ; que le moyen tiré de la méconnaissance de ces dispositions doit dès lors être écarté ;<br/>
<br/>
              11. Considérant en deuxième lieu, que le syndicat requérant soutient que le décret attaqué méconnaitrait le principe de protection de la santé publique ; qu'il critique à ce titre l'absence de procédure de contrôle préalable du contenu des protocoles organisationnels par l'agence régionale de santé et la Haute autorité de santé, à la différence de la procédure applicable aux protocoles de coopération prévus aux articles L. 4011-1 à L. 4011-3 du code de la santé publique ; qu'il ressort de ce qui a été dit ci-dessus que les deux types de protocole répondent à un objet différent ; que, eu égard à la définition précise par le décret attaqué de la portée, du champ d'application et des procédures applicables aux protocoles organisationnels, l'absence de contrôle préalable de leur contenu par l'agence régionale de santé et la haute autorité de santé n'est pas de nature à porter atteinte au principe de protection de la santé publique et aux dispositions de l'article L.1110-1 du code de la santé publique, alors que la création de ce dispositif de protocole organisationnel est au contraire fondée sur la recherche d'une meilleure organisation de l'accès des patients aux soins visuels ; que pour le même motif le moyen tiré de l'article 2 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales doit, en tout état de cause, être écarté ; que, si le syndicat requérant soutient que la grande majorité des orthoptistes libéraux ne remplissent pas les conditions prévues par le décret attaqué pour conclure des protocoles organisationnels, une telle circonstance est sans incidence sur la légalité du décret attaqué ; qu'eu égard à l'objet du décret attaqué, l'absence alléguée de mesures transitoires n'est en elle-même pas de nature à porter atteinte au principe de protection de la santé publique ;<br/>
<br/>
              12. Considérant, en troisième lieu, que le principe d'égalité ne s'oppose pas à ce que l'autorité investie du pouvoir réglementaire règle de façon différente des situations différentes ni à ce qu'elle déroge à l'égalité pour des raisons d'intérêt général, pourvu que, dans l'un comme dans l'autre cas, la différence de traitement qui en résulte soit en rapport avec l'objet de la norme qui l'établit et ne soit pas manifestement disproportionnée au regard des motifs susceptibles de la justifier ;<br/>
<br/>
              13. Considérant que si le décret attaqué réserve aux seuls orthoptistes exerçant dans les conditions décrites au point 7 ci-dessus, qui ont en commun l'établissement d'une relation de travail pérenne avec un ou plusieurs médecins ophtalmologistes, la possibilité de réaliser les actes prévus aux articles R. 4342-2 et  R. 4342-4 à R. 4342-7 dans le cadre d'un protocole organisationnel signé avec ce ou ces médecins ophtalmologistes, et s'il en exclut les praticiens libéraux ne répondant pas à ces conditions, la différence de traitement ainsi instituée entre les orthoptistes est en rapport avec l'objet de la norme qui l'établit, destinée à favoriser, dans l'intérêt de la santé publique, une coopération pluridisciplinaire structurée et de proximité entre professionnels de santé ; que, dès lors que l'ensemble des actes d'orthoptie restent susceptibles d'être réalisés par l'ensemble des orthoptistes sur prescription médicale, et que, contrairement à ce qui est soutenu, elle n'exclut pas par elle-même la conclusion de protocoles organisationnels par des orthoptistes libéraux, à qui il est possible de s'inscrire dans certains des cadres de coopération mentionnés au point 7 ci-dessus, la différence de traitement n'est pas manifestement disproportionnée au regard des motifs susceptibles de la justifier ;<br/>
<br/>
              14. Considérant, en quatrième lieu, que, si les requérants invoquent l'atteinte que porterait le décret attaqué à la liberté d'entreprendre et à la liberté du commerce et de l'industrie, il résulte de ce qui a été dit ci-dessus que le décret, s'il réserve, dans le respect du principe d'égalité, à certains praticiens la réalisation de certains actes sans prescription médicale préalable dans le cadre de protocoles organisationnels, ouvre à l'ensemble des praticiens la possibilité de réaliser l'ensemble des actes sur prescription médicale ; que ces dispositions, fondées sur un objectif d'intérêt général tiré de la protection de la santé publique, ne portent ainsi pas une atteinte disproportionnée à la liberté d'entreprendre ni à la liberté du commerce et de l'industrie ; <br/>
<br/>
              15. Considérant, en cinquième lieu, que le décret attaqué, qui ouvre à tous les orthoptistes la faculté de réaliser les actes qu'il prévoit, est sans incidence sur le libre choix du praticien par le patient posé à L. 1110-8 du code de la santé publique ;<br/>
<br/>
              16. Considérant, en sixième lieu, que le premier alinéa de l'article R. 4342-2 du code de la santé publique issu du décret attaqué dispose que : " L'orthoptiste est seul habilité, sur prescription médicale ou dans le cadre d'un protocole organisationnel défini à la présente section, à établir un bilan qui comprend le diagnostic orthoptique, l'objectif et le plan de soins. Ce bilan, accompagné du choix des actes et des techniques appropriées, est communiqué au médecin prescripteur. " ; que, contrairement à ce que soutient l'association requérante, la mention par ces dispositions d'un diagnostic orthoptique ne caractérise pas une méconnaissance des dispositions de l'article L. 4161-1 du même code relatif à l'exercice illégal de la médecine, eu égard au caractère spécialisé du diagnostic en cause, à sa réalisation sur prescription médicale ou en application d'un protocole organisationnel conclu par l'orthoptiste avec un ou des médecins ophtalmologistes et à sa communication au médecin prescripteur ;  que dès lors le moyen tiré de la méconnaissance par ces dispositions réglementaires des dispositions combinées de l'article L.4161-1 du code de la santé publique et du 3ème alinéa de l'article L. 4342-1, qui définit les missions de l'orthoptiste, ne peut qu'être écarté ;<br/>
<br/>
              17. Considérant, en septième lieu, que le III de l'article R. 4342-2 du code de la santé publique issu du décret attaqué dispose que : " En cas d'urgence et en l'absence d'un médecin, l'orthoptiste est habilité à accomplir les premiers actes de soins nécessaires en orthoptie. Un compte-rendu des actes accomplis dans ces conditions est transmis au médecin dès son intervention. " ; que, contrairement à ce que soutient l'association requérante, cette disposition, relative aux seuls soins en orthoptie,  n'a ni pour objet ni pour effet de faire participer les orthoptistes à l'organisation des soins de premier recours prévus par les dispositions de l'article L. 1411-11 du code de la santé publique ; que, par suite, le moyen tiré de la méconnaissance de ces dispositions ne peut qu'être écarté ;<br/>
<br/>
              18. Considérant, en huitième lieu, que le 1° et le 4° de l'article R. 4342-4 du code la santé publique issus du décret attaqué autorisent l'orthoptiste, sur prescription médicale ou en application d'un protocole organisationnel, respectivement à " déterminer l'acuité visuelle et la réfraction (...) " et à " réaliser les séances d'apprentissage à la manipulation et à la pose des lentilles de contact oculaire et des verres scléraux " ; que le moyen tiré de ce que ces dispositions méconnaitraient les objectifs d'accessibilité et d'intelligibilité de la norme en raison des questions qu'elles soulèveraient quant à l'interprétation des dispositions de l'article L. 4362-10 du code de la santé publique, relatives aux missions des opticiens lunetiers, dont elles n'ont ni pour objet ni pour effet d'affecter la portée, est inopérant ; que la circonstance alléguée que les dispositions applicables aux opticiens lunetiers seraient plus exigeantes que celles applicables aux orthoptistes est sur ce point sans incidence ; que le moyen tiré, pour le même motif, du principe de confiance légitime doit en tout état de cause également être écarté ;<br/>
<br/>
              19. Considérant qu'il résulte de tout ce qui précède que le syndicat national autonome des orthoptistes et autres et l'association des optométristes de France ne sont pas fondés à demander l'annulation du décret qu'ils attaquent et de la décision implicite de rejet de leurs recours gracieux ; que leurs conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative ainsi que les conclusions de l'association des optométristes de France aux fins d'injonction et d'astreinte doivent, par suite, également être rejetées ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Les requêtes du syndicat national autonome des orthoptistes et autres et de l'association des optométristes de France sont rejetées.<br/>
Article 2 : La présente décision sera notifiée au syndicat national autonome des orthoptistes, représentant unique désigné, pour l'ensemble des requérants sous le n° 410187, à l'association des optométristes de France, à la ministre des solidarités et de la santé et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
