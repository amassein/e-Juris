<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028217619</ID>
<ANCIEN_ID>JG_L_2013_11_000000352955</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/21/76/CETATEXT000028217619.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème / 8ème SSR, 19/11/2013, 352955</TITRE>
<DATE_DEC>2013-11-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>352955</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème / 8ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GADIOU, CHEVALLIER ; SCP GATINEAU, FATTACCINI ; SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>M. Christian Fournier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:352955.20131119</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 26 septembre et 23 décembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. C... A...et M. et Mme B...A..., demeurant... ; les requérants demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10NT00327 du 18 juillet 2011 par lequel la cour administrative d'appel de Nantes, statuant sur renvoi du Conseil d'Etat, a rejeté leur requête d'appel contre le jugement n° 034581 et 042974 du 15 mars 2007 par lequel le tribunal administratif de Rennes a rejeté leur demande tendant, en premier lieu, à ce que la commune d'Etables-sur-Mer soit déclarée responsable de l'accident dont a été victime M. C... A...le 24 juillet 1999 et soit condamnée, à titre de provision, au paiement à ce dernier de la somme de 100 000 euros et à ses parents de la somme de 15 000 euros et, en second lieu, à ce qu'il soit ordonné une expertise afin de déterminer l'ensemble des préjudices que M. C... A...a subis ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune d'Etables-sur-Mer le versement d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 25 octobre 2013, présentée pour la commune d'Etables-sur-Mer ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu le code de la sécurité sociale ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christian Fournier, Maître des Requêtes, <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gadiou, Chevallier, avocat de M. C...A..., de M. B...A...et de Mme D...A...et à la SCP Didier, Pinet, avocat de la commune d'Etables-sur-Mer ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. C... A...a été victime, le 24 juillet 1999, d'un accident grave à la suite d'un plongeon réalisé depuis une plate-forme flottante aménagée par la commune d'Etables-sur-Mer sur la plage des Gobelins ; qu'à la suite de cet accident, M. C... A..., ses parents, M. et MmeA..., et la caisse primaire d'assurance maladie de Seine-et-Marne, sur le fondement de l'article L. 376-1 du code de la sécurité sociale, ont engagé une action en responsabilité contre la commune ; que, par jugement du 15 mars 2007, le tribunal administratif de Rennes a rejeté leur demande ; que M. C... A..., M. B...A..., Mme D...A...et la caisse primaire d'assurance maladie de Seine-et-Marne se pourvoient en cassation contre l'arrêt du 18 juillet 2011 par lequel la cour administrative d'appel de Nantes, statuant sur renvoi du Conseil d'Etat après une première cassation, a rejeté leur requête d'appel ;<br/>
<br/>
              Sur la recevabilité des conclusions de la caisse primaire d'assurance maladie de Seine-et-Marne :<br/>
<br/>
              2. Considérant que la caisse primaire d'assurance maladie de Seine-et-Marne avait la qualité de partie à l'instance d'appel introduite par M. C... A...et M. et Mme A...devant la cour administrative d'appel de Nantes ; que son mémoire tendant à l'annulation de l'arrêt attaqué doit être regardé comme un pourvoi en cassation ; que, compte tenu, d'une part, du lien qu'établissent les dispositions de l'article L. 376 -1 du code de la sécurité sociale entre la détermination des droits de la victime et celle des droits de la caisse et, d'autre part, de l'obligation qu'elles instituent de mettre en cause la caisse de sécurité sociale à laquelle est affiliée la victime en tout état de la procédure, afin de la mettre en mesure de poursuivre le remboursement de ses débours par l'auteur de l'accident, une caisse partie à l'instance d'appel mais qui n'a pas formé de pourvoi en cassation dans le délai de recours contentieux est néanmoins recevable à le faire lorsque la victime a elle-même régulièrement exercé cette voie de recours ; qu'ainsi, les conclusions de la caisse primaire d'assurance maladie de Seine-et-Marne tendant à l'annulation de l'arrêt du 18 juillet 2011 de la cour administrative d'appel de Nantes sont recevables ;<br/>
<br/>
              Sur le bien fondé de l'arrêt attaqué :<br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 2213-23 du code général des collectivités territoriales : " Le maire exerce la police des baignades et des activités nautiques pratiquées à partir du rivage avec des engins de plage et des engins non immatriculés. Cette police s'exerce en mer jusqu'à une limite fixée à 300 mètres à compter de la limite des eaux. / Le maire réglemente l'utilisation des aménagements réalisés pour la pratique de ces activités. Il pourvoit d'urgence à toutes les mesures d'assistance et de secours. / Le maire délimite une ou plusieurs zones surveillées dans les parties du littoral présentant une garantie suffisante pour la sécurité des baignades et des activités mentionnées ci-dessus. Il détermine des périodes de surveillance. Hors des zones et des périodes ainsi définies, les baignades et activités nautiques sont pratiquées aux risques et périls des intéressés. / Le maire est tenu d'informer le public par une publicité appropriée, en mairie et sur les lieux où elles se pratiquent, des conditions dans lesquelles les baignades et les activités nautiques sont réglementées, ainsi que des résultats des contrôles de la qualité des eaux de ces baignades accompagnés des précisions nécessaires à leur interprétation " ; qu'en vertu de ces dispositions, il incombe au maire de la commune d'assurer la sécurité des baigneurs sur les plages et notamment de signaler les dangers qui excèdent ceux contre lesquels les intéressés doivent normalement se prémunir ;<br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la commune d'Etables-sur-Mer a installé sur la plage des Gobelins une plate-forme flottante destinée au divertissement des baigneurs et habituellement utilisée par des adolescents et des enfants pour effectuer des plongeons ; qu'en jugeant que le maire, qui n'avait ni averti les usagers du danger que pouvait présenter l'utilisation de cette installation comme plongeoir, ni pris une réglementation concernant l'accès et l'usage de la plate-forme flottante ni encore mis en place une surveillance particulière de cette installation, n'avait commis aucune faute dans l'exercice de ses pouvoirs de police aux motifs, d'une part, que l'usage de la plate-forme flottante comme plongeoir ne présentait pas de risque autre que celui lié aux conséquences normales du phénomène des marées et que les horaires de marées avaient été affichés sur la plage des Gobelins et, d'autre part, que cette plage faisait l'objet d'une surveillance pour la baignade, la cour administrative d'appel de Nantes a inexactement qualifié les faits qui lui étaient soumis ; que par suite, les requérants sont fondés à demander, pour ce motif et sans qu'il soit besoin d'examiner les autres moyens de leurs pourvois, l'annulation de l'arrêt attaqué ;<br/>
<br/>
              5. Considérant que l'affaire faisant l'objet d'un second recours en cassation, il y a lieu de la régler au fond par application des dispositions du deuxième alinéa de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              6. Considérant que l'action engagée par M. C... A...et M. et Mme A...est dirigée contre le refus de la commune d'Etables-sur-Mer de faire droit à la demande d'indemnisation exprimée par lettre du 28 juin 2004 ; que, par suite, la fin de non-recevoir tirée de ce que leur recours contentieux n'aurait pas été précédé d'une demande adressée à la commune doit être écartée ;<br/>
<br/>
              7. Considérant qu'il résulte de l'instruction que l'utilisation de la plate-forme flottante installée par la commune d'Etables-sur-Mer sur la plage publique des Gobelins présentait un danger particulier dès lors qu'elle permettait à des adolescents et à des enfants d'effectuer des plongeons, quelle que soit la profondeur de l'eau ; que, par suite, il incombait au maire de prendre, en application des dispositions de l'article L. 2213-23 du code général des collectivités territoriales rappelées au point 3, les mesures appropriées à l'usage de cette plate-forme flottante ; qu'ainsi, le maire d'Etables-sur-Mer, en s'abstenant de prendre de telles mesures, a commis une faute dans l'exercice de ses pouvoirs de police ; qu'aucune imprudence ne pouvant, contrairement à ce que soutient la commune, être imputée en l'espèce à la victime, cette faute est de nature à engager la responsabilité de la commune ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens d'appel, les requérants sont fondés à soutenir que c'est à tort que le tribunal administratif de Rennes a rejeté leurs demandes ;<br/>
<br/>
              8. Considérant que l'état du dossier ne permet pas de déterminer le montant des préjudices subis par M. C... A...et par ses parents ni celui des droits de la caisse primaire d'assurance maladie de Seine-et-Marne ; qu'il y a lieu d'ordonner une expertise en vue de déterminer l'ensemble des préjudices subis à raison de l'accident survenu à Etables-sur-Mer le 24 juillet 1999 et d'évaluer le montant de la réparation due à ce titre ;<br/>
<br/>
              9. Considérant toutefois que, dans les circonstances de l'affaire, il y a lieu d'octroyer une indemnité provisionnelle de 25 000 euros à M. C... A...et une indemnité provisionnelle de 4 000 euros à M. B...A...et Mme D...A... ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er L'arrêt de la cour administrative d'appel de Nantes du 18 juillet 2011 et le jugement du tribunal administratif de Rennes du 15 mars 2007 sont annulés.<br/>
Article 2 : La commune d'Etables-sur-Mer est condamnée à verser une indemnité provisionnelle de 25 000 euros à M. C... A...et une indemnité provisionnelle de 4 000 euros à M. B... A...et Mme D...A....<br/>
Article 3 : Il sera, avant de statuer sur le surplus des conclusions de M. C... A...et de M. et MmeA..., procédé par un expert désigné par le président de la section du contentieux du Conseil d'Etat à une expertise en vue de déterminer l'ensemble des préjudices qu'ils ont subis à raison de l'accident survenu à Etables-sur-Mer le 24 juillet 1999 et d'évaluer le montant de la réparation due à ce titre.<br/>
Article 4 : L'expert prêtera serment par écrit ou devant le secrétaire du contentieux du Conseil d'Etat. Le rapport d'expertise sera déposé au secrétariat du contentieux dans le délai de quatre mois suivant la prestation de serment.<br/>
Article 5 : Les frais d'expertise, les conclusions de la caisse primaire d'assurance maladie de Seine-et-Marne ainsi que les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative sont réservés pour y être statué en fin d'instance.<br/>
Article 6 : La présente décision sera notifiée à M. C... A..., à M. B... A...et Mme D...A..., à la caisse primaire d'assurance maladie de Seine-et-Marne et à la commune d'Etables-sur-Mer.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-02-03-02-02-01-01 COLLECTIVITÉS TERRITORIALES. COMMUNE. ATTRIBUTIONS. POLICE. POLICE DE LA SÉCURITÉ. POLICE DES LIEUX DANGEREUX. LIEUX DE BAIGNADE. - 1) OBLIGATIONS DU MAIRE EN MATIÈRE DE SÉCURITÉ DES BAIGNEURS - SIGNALEMENT DES DANGERS EXCÉDANT CEUX CONTRE LESQUELS LES BAIGNEURS DOIVENT NORMALEMENT SE PRÉMUNIR - INCLUSION [RJ1] - 2) RESPONSABILITÉ DE LA COMMUNE POUR CARENCE DU MAIRE DANS L'EXERCICE DE SES POUVOIRS - FAUTE DE NATURE À ENGAGER LA RESPONSABILITÉ DE LA COMMUNE - MAIRE N'AYANT PAS AVERTI DES DANGERS LIÉS À L'UTILISATION D'UN PONTON MIS EN PLACE PAR LA COMMUNE ET DESTINÉ À PERMETTRE LA DISTRACTION DES BAIGNEURS, NI RÈGLEMENTÉ OU SURVEILLÉ SON USAGE [RJ2] - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">49-04-03-01-01 POLICE. POLICE GÉNÉRALE. SÉCURITÉ PUBLIQUE. POLICE DES LIEUX DANGEREUX. LIEUX DE BAIGNADE. - 1) OBLIGATIONS DU MAIRE EN MATIÈRE DE SÉCURITÉ DES BAIGNEURS - SIGNALEMENT DES DANGERS EXCÉDANT CEUX CONTRE LESQUELS LES BAIGNEURS DOIVENT NORMALEMENT SE PRÉMUNIR - INCLUSION [RJ1] - 2) RESPONSABILITÉ DE LA COMMUNE POUR CARENCE DU MAIRE DANS L'EXERCICE DE SES POUVOIRS - FAUTE DE NATURE À ENGAGER LA RESPONSABILITÉ DE LA COMMUNE - MAIRE N'AYANT PAS AVERTI DES DANGERS LIÉS À L'UTILISATION D'UN PONTON MIS EN PLACE PAR LA COMMUNE ET DESTINÉ À PERMETTRE LA DISTRACTION DES BAIGNEURS, NI RÈGLEMENTÉ OU SURVEILLÉ SON USAGE [RJ2] - EXISTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">60-02-03-02-01-02 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICES DE POLICE. POLICE MUNICIPALE. POLICE DE LA SÉCURITÉ. BAIGNADE. - 1) OBLIGATIONS DU MAIRE EN MATIÈRE DE SÉCURITÉ DES BAIGNEURS - SIGNALEMENT DES DANGERS EXCÉDANT CEUX CONTRE LESQUELS LES BAIGNEURS DOIVENT NORMALEMENT SE PRÉMUNIR - INCLUSION [RJ1] - 2) RESPONSABILITÉ DE LA COMMUNE POUR CARENCE DU MAIRE DANS L'EXERCICE DE SES POUVOIRS - FAUTE DE NATURE À ENGAGER LA RESPONSABILITÉ DE LA COMMUNE - MAIRE N'AYANT PAS AVERTI DES DANGERS LIÉS À L'UTILISATION D'UN PONTON MIS EN PLACE PAR LA COMMUNE ET DESTINÉ À PERMETTRE LA DISTRACTION DES BAIGNEURS, NI RÈGLEMENTÉ OU SURVEILLÉ SON USAGE [RJ2] - EXISTENCE.
</SCT>
<ANA ID="9A"> 135-02-03-02-02-01-01 1) En vertu de l'article L. 2213-23 du code général des collectivités territoriales, il incombe au maire d'assurer la sécurité des baigneurs sur les plages et notamment de signaler les dangers qui excèdent ceux contre lesquels les intéressés doivent normalement se prémunir.... ,,2) Commet une faute dans l'exercice de ses pouvoirs de police le maire qui, alors que la commune a installé sur une plage une plate-forme flottante destinée au divertissement des baigneurs et habituellement utilisée par des adolescents et des enfants pour effectuer des plongeons, n'a ni averti les usagers du danger que pouvait présenter l'utilisation de cette installation comme plongeoir, ni  pris une réglementation concernant l'accès et l'usage de la plate-forme flottante ni encore mis en place une surveillance particulière de cette installation.</ANA>
<ANA ID="9B"> 49-04-03-01-01 1) En vertu de l'article L. 2213-23 du code général des collectivités territoriales, il incombe au maire d'assurer la sécurité des baigneurs sur les plages et notamment de signaler les dangers qui excèdent ceux contre lesquels les intéressés doivent normalement se prémunir.... ,,2) Commet une faute dans l'exercice de ses pouvoirs de police le maire qui, alors que la commune a installé sur une plage une plate-forme flottante destinée au divertissement des baigneurs et habituellement utilisée par des adolescents et des enfants pour effectuer des plongeons, n'a ni averti les usagers du danger que pouvait présenter l'utilisation de cette installation comme plongeoir, ni  pris une réglementation concernant l'accès et l'usage de la plate-forme flottante ni encore mis en place une surveillance particulière de cette installation.</ANA>
<ANA ID="9C"> 60-02-03-02-01-02 1) En vertu de l'article L. 2213-23 du code général des collectivités territoriales, il incombe au maire d'assurer la sécurité des baigneurs sur les plages et notamment de signaler les dangers qui excèdent ceux contre lesquels les intéressés doivent normalement se prémunir.... ,,2) Commet une faute dans l'exercice de ses pouvoirs de police le maire qui, alors que la commune a installé sur une plage une plate-forme flottante destinée au divertissement des baigneurs et habituellement utilisée par des adolescents et des enfants pour effectuer des plongeons, n'a ni averti les usagers du danger que pouvait présenter l'utilisation de cette installation comme plongeoir, ni  pris une réglementation concernant l'accès et l'usage de la plate-forme flottante ni encore mis en place une surveillance particulière de cette installation.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 30 janvier 1980, Consorts Quiniou, n° 12928, T. p. 877.,,[RJ2] Comp., pour le cas d'un plongeon à partir d'un ponton non destiné à cet effet, CE, 9 février 1972, Dame Edel, T. p. 998.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
