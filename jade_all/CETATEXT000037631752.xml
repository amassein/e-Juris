<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037631752</ID>
<ANCIEN_ID>JG_L_2018_11_000000408120</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/63/17/CETATEXT000037631752.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 5ème chambres réunies, 21/11/2018, 408120, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-11-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>408120</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Cyrille Beaufils</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Julie Burguburu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:408120.20181121</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 17 février, 16 mai et 18 septembre 2017 au secrétariat du contentieux du Conseil d'Etat, l'Union nationale des professions libérales et l'Union des entreprises de proximité demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 22 novembre 2016 du ministre de l'économie et des finances fixant les modèles de conventions prévues aux articles 371 C, 371 O, 371 Z quater et 371 bis B de l'annexe II au code général des impôts conclues entre les centres de gestion agréés, les associations de gestion agréées, les organismes mixtes de gestion agréés, les professionnels de l'expertise comptable, et l'administration fiscale ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code civil ;<br/>
              - le code général des impôts ; <br/>
              - la loi n° 78-17 du 6 janvier 1978 ; <br/>
              - le décret n° 2005-1309 du 20 octobre 2005 ; <br/>
              - le décret n° 2016-1356 du 11 octobre 2016 ;<br/>
              - la décision nos 405932,405943 du Conseil d'Etat, statuant au contentieux, du 21 juin 2017 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Cyrille Beaufils, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Julie Burguburu, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1.	Le chapitre I ter du titre premier de la troisième partie du livre premier du code général des impôts organise le régime des centres et associations de gestion agréés dont l'objet est, notamment, de fournir aux industriels, commerçants, artisans et agriculteurs ainsi qu'aux membres des professions libérales et titulaires des charges et offices une assistance en matière de gestion et une analyse des informations économiques, comptables et financières en matière de prévention des difficultés économiques et financières et de faciliter l'accomplissement de leurs obligations administratives et fiscales. L'adhésion à ces organismes de gestion agréés emporte, pour leurs membres, divers avantages fiscaux, notamment l'exemption de la majoration de leurs revenus professionnels prévue au point 7 de l'article 158 du code général des impôts. Les articles 1649 quater E et H de ce code prévoient que l'administration fiscale apporte son assistance technique à l'organisme de gestion agréé, dans les conditions prévues par une convention passée entre l'organisme et l'administration. Aux termes de l'article 371 C de l'annexe II au code général des impôts : " En application de l'article 1649 quater E du code général des impôts, les centres doivent conclure avec l'administration fiscale une convention précisant le rôle du ou des agents de cette administration chargés d'apporter leur assistance technique au centre. Un modèle de cette convention est fixé par arrêté du ministre chargé du budget. (...) ", les articles 371 O et 371 Z quater comportant des dispositions identiques applicables aux associations et organismes mixtes de gestion agréés. L'arrêté du 22 novembre 2016 attaqué a notamment pour objet de fixer les modèles de conventions conclues entre les organismes de gestion agréés et l'administration fiscale en application de ces dispositions.<br/>
<br/>
              2.	En premier lieu, aux termes de l'article 2 de la loi du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés : " La présente loi s'applique aux traitements automatisés de données à caractère personnel, ainsi qu'aux traitements non automatisés de données à caractère personnel contenues ou appelées à figurer dans des fichiers, à l'exception des traitements mis en oeuvre pour l'exercice d'activités exclusivement personnelles, lorsque leur responsable remplit les conditions prévues à l'article 5. / Constitue une donnée à caractère personnel toute information relative à une personne physique identifiée ou qui peut être identifiée, directement ou indirectement, par référence à un numéro d'identification ou à un ou plusieurs éléments qui lui sont propres. Pour déterminer si une personne est identifiable, il convient de considérer l'ensemble des moyens en vue de permettre son identification dont dispose ou auxquels peut avoir accès le responsable du traitement ou toute autre personne. / Constitue un traitement de données à caractère personnel toute opération ou tout ensemble d'opérations portant sur de telles données, quel que soit le procédé utilisé, et notamment la collecte, l'enregistrement, l'organisation, la conservation, l'adaptation ou la modification, l'extraction, la consultation, l'utilisation, la communication par transmission, diffusion ou toute autre forme de mise à disposition, le rapprochement ou l'interconnexion, ainsi que le verrouillage, l'effacement ou la destruction. / Constitue un fichier de données à caractère personnel tout ensemble structuré et stable de données à caractère personnel accessibles selon des critères déterminés. (...) ". Aux termes de son article 3 : " I. - Le responsable d'un traitement de données à caractère personnel est, sauf désignation expresse par les dispositions législatives ou réglementaires relatives à ce traitement, la personne, l'autorité publique, le service ou l'organisme qui détermine ses finalités et ses moyens. (...) ". Son article 22 impose la déclaration préalable auprès de la Commission nationale de l'informatique et des libertés des traitements de données, à l'exception de ceux qui relèvent des dispositions prévues aux articles 25, 26 et 27 ou qui sont visés au deuxième alinéa de l'article 36 de la même loi. Enfin l'article 8 du décret du 20 octobre 2005 pris pour l'application de cette loi prévoit que l'accomplissement de cette formalité incombe au responsable du traitement. <br/>
<br/>
              3.	Si le 5° des modèles de conventions annexés à l'arrêté attaqué prévoit que l'organisme de gestion agréé " tient à la disposition de l'administration un registre mentionnant le nom des adhérents, la date de leur adhésion, leur profession et le lieu d'exercice de celle-ci ainsi que, le cas échéant, le nom et l'adresse du professionnel de l'expertise comptable dont ils utilisent les services " et impose diverses modalités de tenue et de transmission de ces registres à l'administration fiscale, l'arrêté attaqué n'a, par lui-même, ni pour objet ni pour effet d'autoriser la création ou la mise en oeuvre, par les organismes de gestion agréés, de ces traitements de données à caractère personnel. Ces dernières incombent en effet à chacun des organismes, qui déterminent, dans le cadre prévu par ces modèles de convention, les finalités et les moyens de ces traitements et en sont les responsables pour l'application de l'article 22 de la loi du 6 janvier 1978 précitée, la circonstance que l'administration fiscale est destinataire des informations contenues dans ces registres étant sans incidence sur ce point. Le moyen tiré de ce que l'arrêté attaqué aurait dû faire l'objet d'une déclaration préalable à la Commission nationale de l'informatique et des libertés doit, par suite, être écarté.<br/>
<br/>
              4.	En deuxième lieu, par une décision nos 405932, 405943 du 21 juin 2017, le Conseil d'Etat, statuant au contentieux, a rejeté les requêtes de l'Union nationale des professions libérales et autres tendant à l'annulation pour excès de pouvoir du décret du 11 octobre 2016 relatif aux centres de gestion, associations et organismes mixtes de gestion agréés, aux professionnels de l'expertise comptable et aux certificateurs à l'étranger. Les associations requérantes ne sont, par suite, pas fondées à demander l'annulation de l'arrêté attaqué par voie de conséquence de l'annulation de ce décret. <br/>
<br/>
              5.	En troisième lieu, les articles 1649 quater E et H du code général des impôts disposent que les centres et associations de gestion agréés sont " habilités à élaborer, pour le compte de leurs adhérents placés sous un régime réel d'imposition, les déclarations destinées à l'administration fiscale " mais " doivent recevoir mandat de leurs adhérents pour transmettre les informations correspondant à leurs obligations déclaratives ". Les articles 371 A et M de l'annexe II de ce code précisent que, si ces organismes ne peuvent en principe agir en qualité de mandataires de leurs membres, ils " doivent recevoir mandat de leurs membres en vue de la télétransmission des déclarations de résultats, de leurs annexes et des autres documents les accompagnant selon la procédure prévue par le système de transfert des données fiscales et comptables ". Les articles 371 E et Q prévoient que leurs statuts comportent des stipulations aux termes desquelles ces organismes élaborent, pour ceux de leurs adhérents qui relèvent d'un régime réel d'imposition et qui en font la demande, les déclarations relatives à leur activité professionnelle destinées à l'administration fiscale. Ces mêmes articles imposent en outre que les statuts précisent que l'adhésion à l'organisme de gestion agréé implique " l'autorisation de communiquer à l'administration fiscale, dans le cadre de l'assistance que cette dernière lui apporte, les documents mentionnés à ces articles, à l'exception des documents, quels qu'ils soient, fournissant une vision exhaustive des opérations comptables de l'entreprise ". Il résulte de ces dispositions que les adhérents des organismes de gestion agréés qui relèvent d'un régime réel d'imposition et demandent que ces organismes élaborent les déclarations relatives à leur activité professionnelle consentent non seulement à l'élaboration de leurs déclarations mais aussi à ce que les organismes concernés transmettent à l'administration fiscale, dans le cadre de l'assistance que cette dernière leur apporte, les documents mentionnés aux articles 371 E et Q de l'annexe II au code général des impôts. La faculté, pour les organismes de gestion agréés, de poser à l'administration fiscale des questions relatives à la situation individuelle de tels adhérents demeure en revanche subordonnée à la condition d'avoir au préalable recueilli leur accord individuel.<br/>
<br/>
              6.	Le 2° des modèles de conventions annexés à l'arrêté attaqué prévoit, d'une part, que les organismes de gestion agréés " peuvent poser à l'administration fiscale toute question de réglementation relative aux impositions dues à raison de l'activité professionnelle de ses adhérents ", d'autre part, que, lorsque ces questions sont de nature individuelle, elles portent sur la situation actuelle des adhérents et mentionnent leur identité, enfin, que la réponse écrite de l'administration est transmise aux adhérents concernés. Le 3° de ces modèles prévoit en outre que, dans les déclarations qu'il élabore pour le compte de ses adhérents postérieurement à la réception de la réponse de l'administration, l'organisme doit se conformer aux solutions exposées dans la réponse ou indiquer expressément dans une note annexe les motifs de droit ou de fait le conduisant à ne pas retenir ces solutions. Ces dispositions de l'arrêté attaqué ne sauraient en revanche être interprétées, à la lumière des dispositions rappelées au point précédent, comme permettant à un organisme de gestion agréé de poser à l'administration fiscale des questions relatives à la situation individuelle d'un adhérent identifié, lorsqu'il relève d'un régime réel d'imposition et a demandé que cet organisme élabore les déclarations relatives à son activité professionnelle, sans avoir au préalable recueilli l'accord de cet adhérent. Le moyen tiré de ce que l'arrêté attaqué méconnaitrait les articles 1649 quater E et H et 371 A et M mentionnés au point précédent ainsi que les articles 1984 et 1989 du code civil en ce qu'il permettrait aux organismes de gestion agréés de représenter leurs adhérents auprès de l'administration fiscale, dans le cadre de ces questions individuelles, sans mandat ou en excédant leur mandat doit, par suite, être écarté. Il en va de même, pour les mêmes motifs, du moyen tiré de ce que ces dispositions méconnaîtraient les règles relatives au secret professionnel fixées par les articles 371 EB et QA de l'annexe II au code général des impôts, en ce qu'il prévoit que les organismes agréés peuvent transmettre ces informations à l'administration fiscale dans le cadre de questions portant sur la situation individuelle de tels adhérents. <br/>
<br/>
              7.	Considérant qu'il résulte de tout ce qui précède, sans qu'il soit besoin d'examiner la fin de non recevoir opposée par le ministre, que l'Union nationale des professions libérales et autre ne sont pas fondées à demander l'annulation de l'arrêté qu'elles attaquent. Leur requête doit, par suite, être rejetée, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de l'Union nationale des professions libérales et autre est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à l'Union nationale des professions libérales, représentant désigné, pour l'ensemble des requérantes, et au ministre de l'action et des comptes publics.  <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
