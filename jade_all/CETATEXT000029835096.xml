<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029835096</ID>
<ANCIEN_ID>JG_L_2014_12_000000364113</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/83/50/CETATEXT000029835096.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  9ème sous-section jugeant seule, 03/12/2014, 364113, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-12-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>364113</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 9ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean-Luc Matt</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:364113.20141203</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 26 novembre 2012 au secrétariat du contentieux du Conseil d'État, présenté par le ministre délégué chargé du budget ; le ministre demande au Conseil d'État d'annuler l'arrêt n° 11NC00709 du 27 septembre 2012 par lequel la cour administrative d'appel de Nancy, après avoir annulé le jugement n° 0900980 du 31 mars 2011 du tribunal administratif de Châlons-en-Champagne, a déchargé la SAS Eurodif, venant aux droits de la SAS Stockarmor, de la cotisation supplémentaire de taxe professionnelle mise à sa charge au titre de l'année 2006 à concurrence de 4 611 euros ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Matt, maître des requêtes,<br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article 1478 du code général des impôts dans sa rédaction applicable à l'imposition en litige : " I. La taxe professionnelle est due pour l'année entière par le redevable qui exerce l'activité le 1er  janvier. / Toutefois, le contribuable qui cesse toute activité dans un établissement n'est pas redevable de la taxe pour les mois restant à courir, sauf en cas de cession de l'activité exercée dans l'établissement ou en cas de transfert d'activité (...). / II. En cas de création d'un établissement autre que ceux mentionnés au III, la taxe professionnelle n'est pas due pour l'année de la création (...) " ; <br/>
<br/>
              2. Considérant qu'il résulte des dispositions citées au point 1 que, lorsque la fermeture d'un établissement s'accompagne de l'ouverture, par le même contribuable, d'un nouvel établissement dans une commune différente, il y a lieu de distinguer selon qu'il s'agit de la poursuite de la même activité professionnelle dans des locaux différents, le cas échéant avec des moyens différents, ou de la fermeture définitive de l'établissement dans le cadre d'une cessation d'activité sans cession ; dans ce second cas, alors même que le contribuable poursuit une activité professionnelle de même nature, l'opération ne peut être regardée comme un transfert d'activité au sens des dispositions précitées de l'article 1478 du code général des impôts lorsque des modifications substantielles interviennent dans l'organisation et les moyens de l'exploitation ou lorsque la clientèle à laquelle celle-ci s'adresse est entièrement nouvelle ; il y a alors lieu de faire application du dégrèvement prévu par les dispositions précitées du second alinéa du I de l'article 1478 du code général des impôts en ce qui concerne la cotisation de taxe professionnelle due au titre de l'établissement fermé en cours d'année ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la SAS Stockarmor, qui exerçait une activité d'entreposage et de stockage non frigorifique, a, le 3 juin 2006, fermé son établissement situé à La Chapelle-Saint-Luc, dans l'agglomération de Troyes (Aube) ; que cette société a, au mois de juin 2006, débuté une activité de même nature dans une commune limitrophe, à Lavau ; que pour estimer que le changement auquel la société Stockarmor a procédé devait être regardé, non comme un simple transfert d'activité mais comme une cessation d'activité au sens de l'article 1478 du code général des impôts, suivie de la création d'un nouvel établissement, la cour a relevé que la poursuite de l'activité professionnelle dans le nouvel établissement s'était accompagnée d'une augmentation significative des moyens d'exploitation " quasiment doublés " par rapport à ceux de l'ancien établissement, et des moyens humains, " passés de 15 à 26 salariés " ; que la cour a ainsi donné aux faits de l'espèce une qualification juridique erronée, dès lors que la société n'avait pas cessé son activité mais l'avait poursuivie dans des locaux différents avec des moyens différents ; que par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'arrêt attaqué doit être annulé ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de faire application des dispositions de l'article L. 821-2 du code de justice administrative et de régler l'affaire au fond ;<br/>
<br/>
              5. Considérant que, comme il a été dit au point 3, la fermeture de l'établissement de La Chapelle-Saint-Luc, suivie de l'ouverture d'un établissement à Lavau, doit être regardée comme un transfert d'activité au sens de l'article 1478 du code général des impôts ; qu'il en résulte que la société requérante n'est pas fondée à soutenir que c'est à tort que le tribunal administratif de Châlons-en-Champagne, dont le jugement est suffisamment motivé, a, rejeté sa demande tendant à la décharge de la cotisation supplémentaire de taxe professionnelle à laquelle elle a été assujettie au titre de l'année 2006 ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'État qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>
              D E C I D E :<br/>
              --------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nancy du 27 septembre 2012 est annulé.<br/>
Article 2 : La requête de la SAS Eurodif présentée devant la cour administrative d'appel de Nancy est rejetée.<br/>
Article 3 : La présente décision sera notifiée au ministre des finances et des comptes publics et à la SAS Eurodif.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
