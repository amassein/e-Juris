<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027990498</ID>
<ANCIEN_ID>JG_L_2013_06_000000358651</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/99/04/CETATEXT000027990498.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  5ème sous-section jugeant seule, 24/06/2013, 358651, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-06-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>358651</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 5ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:358651.20130624</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 18 avril 2012 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration ; le ministre de l'intérieur de l'outre-mer, des collectivités territoriales et de l'immigration demande au Conseil d'Etat d'annuler le jugement n° 1100663 du 8 mars 2012 par lequel le tribunal administratif de Cayenne a annulé sa décision implicite refusant à M. A...B...le bénéfice d'une promotion au grade de brigadier et lui a enjoint de réexaminer la situation de l'intéressé dans un délai de deux mois  ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 79-587 du 11 juillet 1979 ;<br/>
<br/>
              Vu la loi n° 2000-321 du 12 avril 2000 ;<br/>
<br/>
              Vu le décret n° 95-654 du 9 mai 1995 ;<br/>
<br/>
              Vu le décret n° 2004-1439 du 23 décembre 2004 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M.B..., gardien de la paix affecté à Cayenne, a sollicité une promotion au grade de brigadier de police le 14 novembre 2008 auprès du directeur de l'administration de la police nationale ; que, le 18 octobre 2010, M. B...a formé un recours gracieux contre la décision implicite de rejet née du silence gardé par l'administration sur sa demande pendant plus de deux mois ; que, par lettre en date du 18 avril 2011, il a formé un recours hiérarchique auquel le ministre de l'intérieur a opposé un refus implicite ; que M. B...a saisi le tribunal administratif de Cayenne de conclusions tendant à l'annulation de cette décision et à l'indemnisation du préjudice qu'il estimait avoir subi du fait de ce refus ; que, par un jugement du 8 mars 2012, le tribunal administratif a annulé la décision du ministre de l'intérieur refusant de le promouvoir et a enjoint à celui-ci, sous astreinte, de réexaminer la demande de l'intéressé dans un délai de deux mois ; que le ministre de l'intérieur se pourvoit en cassation contre ce jugement ;<br/>
<br/>
              2. Considérant, d'une part, qu'aux termes de l'article 1er de la loi du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public : " Les personnes physiques ou morales ont le droit d'être informées sans délai des motifs des décisions administratives individuelles défavorables qui les concernent. / A cet effet, doivent être motivées les décisions qui : (...) -refusent un avantage dont l'attribution constitue un droit pour les personnes qui remplissent les conditions légales pour l'obtenir " ; qu'aux termes de l'article 5 de la même loi : " Une décision implicite intervenue dans les cas où la décision explicite aurait dû être motivée n'est pas illégale du seul fait qu'elle n'est pas assortie de cette motivation. Toutefois, à la demande de l'intéressé, formulée dans les délais du recours contentieux, les motifs de toute décision implicite de rejet devront lui être communiqués dans le mois suivant cette demande (...)  " ; <br/>
<br/>
              3. Considérant, d'autre part, qu'aux termes de l'article 12 du décret du 23 décembre 2004 portant statut particulier du corps d'encadrement et d'application de la police nationale : " Peuvent être inscrits au tableau d'avancement pour l'accès au grade de brigadier de police : 1-1. Les gardiens de la paix qui comptent, au 1er janvier de l'année pour laquelle le tableau d'avancement a été arrêté, quatre ans de services effectifs depuis leur titularisation dans ce grade, et qui, soit ont reçu par arrêté interministériel la qualité d'officier de police judiciaire, soit ont satisfait aux obligations d'un examen professionnel dont le contenu et les modalités sont fixés par arrêté du ministre de l'intérieur et du ministre chargé de la fonction publique ; / 1-2. Dans la limite du dixième de l'ensemble des promotions de grade de l'année à réaliser au titre du présent article, les gardiens de la paix affectés dans l'un des secteurs ou unités d'encadrement prioritaire, ayant satisfait aux obligations d'un examen professionnel dont le contenu et les modalités sont fixés par arrêté du ministre de l'intérieur et du ministre chargé de la fonction publique, et qui comptent, au 1er janvier de l'année pour laquelle le tableau d'avancement a été arrêté, soit quatre ans au moins de services effectifs depuis leur titularisation dans ce grade dont une année au moins dans un des secteurs ou unités d'encadrement prioritaire, soit six années au moins de services effectifs depuis leur titularisation " ;<br/>
<br/>
              4. Considérant qu'il résulte de ces dispositions que la décision implicite refusant la promotion de M. B...au grade de brigadier de police n'a pas privé celui-ci d'un avantage dont l'attribution constitue un droit pour les personnes qui remplissent les conditions légales pour l'obtenir ; que cette décision n'entrant dans aucune des catégories de décisions mentionnées à l'article 1er de la loi du 11 juillet 1979, elle n'avait pas à être motivée ; qu'en estimant que l'administration était tenue d'en communiquer les motifs à M. B... dans les conditions fixées à l'article 5 de cette même loi, et en annulant la décision au motif qu'elle avait manqué à cette obligation, le tribunal administratif de Cayenne a entaché son jugement d'une erreur de droit ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, que le ministre de l'intérieur est fondé à demander l'annulation du jugement attaqué ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement n° 1100663 du tribunal administratif de Cayenne du 8 mars 2012 est annulé.<br/>
<br/>
		Article 2 : L'affaire est renvoyée au tribunal administratif de Cayenne.<br/>
<br/>
Article 3 : La présente décision sera notifiée au ministre de l'intérieur et à M. A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
