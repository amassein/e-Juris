<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044178585</ID>
<ANCIEN_ID>JG_L_2021_10_000000447436</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/17/85/CETATEXT000044178585.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 06/10/2021, 447436</TITRE>
<DATE_DEC>2021-10-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>447436</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET ROUSSEAU ET TAPIE ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Clément Tonon</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Philippe Ranquet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:447436.20211006</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés le 10 décembre 2020 et le 15 juin 2021 au secrétariat du contentieux du Conseil d'Etat, Mme G... C... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision n° CS 2020-48 du 2 novembre 2020 de la commission des sanctions de l'Agence française de lutte contre le dopage (AFLD) qui :<br/>
              - lui a interdit, pendant une durée de quatre ans, d'une part, de participer directement ou indirectement à l'organisation et au déroulement de toute manifestation sportive donnant lieu à une remise de prix en argent ou en nature, et des manifestations sportives autorisées par une fédération délégataire ou organisées par une fédération agréée ou par une ligue sportive professionnelle ainsi qu'aux entraînements y préparant organisés par une fédération agréée ou une ligue professionnelle ou l'un des membres de celles-ci, d'autre part, de prendre part à toute autre activité organisée par une fédération sportive, une ligue professionnelle ou l'un de leurs membres, ou le Comité national olympique et sportif français, ainsi qu'aux activités sportives impliquant des sportifs de niveau national ou international et financées par une personne publique, à moins que ces activités ne s'inscrivent dans des programmes ayant pour objet la prévention du dopage, enfin, d'exercer les fonctions définies à l'article L. 212-1 du code du sport ainsi que toute fonction d'encadrement ou toute activité administrative au sein d'une fédération agréée ou d'une ligue professionnelle ou d'un de leurs membres ; <br/>
              - a demandé aux organisateurs compétents d'annuler les résultats qu'elle a obtenus le 30 juin 2019, à l'occasion de la manifestation de, ainsi qu'entre le 30 juin et le 21 octobre 2019, avec toutes conséquences en découlant, y compris le retrait de médailles, points, prix et gains ; <br/>
              - a décidé qu'un résumé en serait publié sur le site internet de l'AFLD ;<br/>
<br/>
              2°) de mettre à la charge de l'AFLD la somme de 3 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du sport ;<br/>
              - l'ordonnance n° 2014-1329 du 6 novembre 2014 ;<br/>
              - l'ordonnance n° 2020-347 du 27 mars 2020 ;<br/>
              - l'ordonnance n° 2020-1507 du 2 décembre 2020 ;<br/>
              - le décret n° 2018-1283 du 27 décembre 2018 ;<br/>
              - le décret n° 2020-1310 du 29 octobre 2020 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Clément Tonon, auditeur,<br/>
<br/>
              - les conclusions de M. Philippe Ranquet, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, après les conclusions, au cabinet Rousseau et Tapie, et à la SCP Matuchansky, Poupot, Valdelièvre, avocat de l'agence française de lutte contre le dopage ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1.	Il résulte de l'instruction qu'à la suite d'un contrôle antidopage réalisé le 30 juin 2019, à l'occasion d'une manifestation de, la commission des sanctions de l'Agence française de lutte contre le dopage a prononcé, le 2 novembre 2020, à l'encontre de Mme C... une sanction d'interdiction, pendant une durée de quatre ans, de participer directement ou indirectement à l'organisation et au déroulement de manifestations sportives, aux entraînements y préparant ainsi qu'à des activités sportives, et d'exercer les fonctions définies à l'article L. 212-1 du code du sport ainsi que toute fonction d'encadrement ou toute activité administrative au sein d'une fédération agréée ou d'une ligue professionnelle ou d'un de leurs membres. Mme C... demande au Conseil d'Etat l'annulation de cette décision.<br/>
<br/>
              2.	Aux termes du II de l'article L. 232-22 du code du sport, relatif à la procédure disciplinaire devant la commission des sanctions de l'Agence française de lutte contre le dopage, dans sa version applicable en l'espèce : " (...) La personne concernée est convoquée à l'audience. Elle peut y présenter ses observations. / La commission des sanctions délibère hors la présence des parties et du représentant du collège de l'agence ". L'article R. 232-95-1 du même code, dans sa version alors applicable, disposait que : " Pour tenir compte de l'éloignement géographique ou de contraintes professionnelles ou médicales, à la demande des personnes à l'encontre desquelles une procédure disciplinaire est engagée, des moyens de conférence audiovisuelle peuvent être mis en place par la commission des sanctions avec l'accord du président de la formation appelée à se prononcer (...) ". <br/>
<br/>
              3.	Il résulte de l'instruction que l'audience de la commission des sanctions de l'Agence française de lutte contre le dopage du 2 novembre 2020, à laquelle Mme C... avait été convoquée le 1er octobre 2020 pour pouvoir y présenter ses observations, a été tenue par visio-conférence, sans que l'intéressée n'ait formulé de demande en ce sens. La procédure s'est ainsi déroulée en méconnaissance des dispositions des articles L. 232-22 et R. 232-95-1 du code du sport.<br/>
<br/>
              4.	Pour soutenir qu'en dépit de cette circonstance la procédure suivie devant la commission des sanctions a été régulière, l'Agence française de lutte contre le dopage se prévaut des dispositions de l'ordonnance du 6 novembre 2014 relative aux délibérations à distance des instances administratives à caractère collégial, de celles du règlement intérieur de la commission des sanctions ainsi que des circonstances exceptionnelles qui auraient prévalu à la date de l'audience.<br/>
<br/>
              5.	Toutefois, en premier lieu, si le II de l'article 1er de l'ordonnance du 6 novembre 2014 dispose que " Les autorités publiques et administratives indépendantes peuvent décider de recourir aux formes de délibérations collégiales à distance prévues par la présente ordonnance, dans des conditions et selon des modalités précisées par ces autorités et conformément aux règles qui les régissent " et si l'article 2 de la même ordonnance prévoit que " Sous réserve de la préservation, le cas échéant, du secret du vote, le président du collège d'une autorité mentionnée à l'article 1er peut décider qu'une délibération sera organisée au moyen d'une conférence téléphonique ou audiovisuelle ", ces dispositions, qui concernent les délibérations des membres des instances collégiales des autorités publiques et administratives indépendantes, ne sont pas applicables aux audiences qui sont tenues par la commission des sanctions de l'Agence française de lutte contre le dopage pour entendre les observations des personnes poursuivies devant elle.<br/>
<br/>
              6.	En deuxième lieu, si l'article 15-2 du règlement intérieur de la commission des sanctions de l'Agence, dans sa version applicable en l'espèce, prévoyait que le président de la commission puisse " décider, en cas d'urgence ou de nécessité absolue, que la séance de la commission se tiendra en utilisant un moyen de communication audiovisuelle permettant de s'assurer de l'identité des participants et garantissant la qualité de la transmission et la confidentialité des débats ", les dispositions de ce règlement intérieur, dont l'intervention était alors prévue à l'article R. 232-12 du code du sport ne pouvaient que préciser les règles de fonctionnement de la commission sans pouvoir légalement priver d'effet les dispositions de l'article R. 232-95-1 du code du sport subordonnant la mise en œuvre de moyens de conférence audiovisuelle lors des audiences à la demande des personnes poursuivies devant la commission.<br/>
<br/>
              7.	Enfin, il ne résulte pas de l'instruction que la situation sanitaire résultant de l'épidémie de covid-19 prévalant aux mois d'octobre et novembre 2020 pouvait être regardée comme faisant obstacle à ce que soient respectées les garanties procédurales prévues par les dispositions du code du sport pour la tenue des audiences devant la commission des sanctions de l'Agence française de lutte contre le dopage. L'Agence ne saurait, en tout état de cause, utilement invoquer les dispositions des ordonnances du 27 mars 2020 et du 2 décembre 2020, adaptant le droit applicable au fonctionnement des établissements publics et des instances collégiales administratives pendant l'état d'urgence sanitaire, lesquelles n'étaient, respectivement, plus ou pas encore en vigueur à la date de l'audience en cause.<br/>
<br/>
              8.	Il résulte de ce qui précède que la procédure suivie en l'espèce par la commission des sanctions de l'Agence, dont l'audience s'est tenue le 2 novembre 2020 par des moyens de conférence audiovisuelle alors que Mme C... ne l'avait pas demandé, a été irrégulière. Cette irrégularité a privé l'intéressée de la garantie, prévue par les articles L. 232-22 et R. 232-95-1 du code du sport, tenant à ce qu'elle puisse être entendue en personne, sauf demande de sa part, pour présenter ses observations devant la commission des sanctions.<br/>
<br/>
              9.	Mme C... est, par suite, fondée, sans qu'il soit besoin de se prononcer sur les autres moyens de sa requête, à demander l'annulation de la décision qu'elle attaque. <br/>
<br/>
              10.	Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Agence française de lutte contre le dopage une somme de 3 000 euros à verser à Mme C... au titre de l'article L. 761-1 du code de justice administrative. Les dispositions de cet article font, en revanche, obstacle à ce que soit accueillies les conclusions présentées par l'Agence au même titre.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La décision n° CS 2020-48 du 2 novembre 2020 de la commission des sanctions de l'Agence française de lutte contre le dopage est annulée.<br/>
Article 2 : L'Agence française de lutte contre le dopage versera une somme de 3 000 euros à Mme C... au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Les conclusions présentées par l'Agence française de lutte contre le dopage au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à Mme G... C... et à l'Agence française de lutte contre le dopage.<br/>
<br/>
<br/>
              Délibéré à l'issue de la séance du 10 septembre 2021 où siégeaient : M. Jacques-Henri Stahl, président adjoint de la section du contentieux, présidant ; M. F... I..., M. Olivier Japiot, présidents de chambre ; Mme N... M..., Mme A... J..., M. E... K..., M. D... L..., M. Jean-Yves Ollier, conseillers d'Etat et M. Clément Tonon, auditeur-rapporteur. <br/>
<br/>
              Rendu le 6 octobre 2021.<br/>
<br/>
                 Le Président : <br/>
                 Signé : M. Jacques-Henri Stahl<br/>
 		Le rapporteur : <br/>
      Signé : M. Clément Tonon<br/>
                 La secrétaire :<br/>
                 Signé : Mme H... B...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">52-046 POUVOIRS PUBLICS ET AUTORITÉS INDÉPENDANTES. - AFLD - AUDIENCE DE LA COMMISSION DES SANCTIONS S'ÉTANT TENUE EN VISIOCONFÉRENCE SANS QUE LA PERSONNE POURSUIVIE EN AIT FAIT LA DEMANDE, EN MÉCONNAISSANCE DE L'ARTICLE R. 232-95-1 DU CODE DU SPORT - 1) IRRÉGULARITÉ - EXISTENCE - 2) PRIVATION D'UNE GARANTIE AU SENS DE LA JURISPRUDENCE DANTHONY [RJ1] - EXISTENCE [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">59-02-02-02 RÉPRESSION. - DOMAINE DE LA RÉPRESSION ADMINISTRATIVE - RÉGIME DE LA SANCTION ADMINISTRATIVE. - RÉGULARITÉ. - AFLD - AUDIENCE DE LA COMMISSION DES SANCTIONS S'ÉTANT TENUE EN VISIOCONFÉRENCE SANS QUE LA PERSONNE POURSUIVIE EN AIT FAIT LA DEMANDE, EN MÉCONNAISSANCE DE L'ARTICLE R. 232-95-1 DU CODE DU SPORT - 1) IRRÉGULARITÉ - EXISTENCE - 2) PRIVATION D'UNE GARANTIE AU SENS DE LA JURISPRUDENCE DANTHONY [RJ1] - EXISTENCE [RJ2].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">63-05-05 SPORTS ET JEUX. - SPORTS. - AFLD - AUDIENCE DE LA COMMISSION DES SANCTIONS S'ÉTANT TENUE EN VISIOCONFÉRENCE SANS QUE LA PERSONNE POURSUIVIE EN AIT FAIT LA DEMANDE, EN MÉCONNAISSANCE DE L'ARTICLE R. 232-95-1 DU CODE DU SPORT - 1) IRRÉGULARITÉ - EXISTENCE - 2) PRIVATION D'UNE GARANTIE AU SENS DE LA JURISPRUDENCE DANTHONY [RJ1] - EXISTENCE [RJ2].
</SCT>
<ANA ID="9A"> 52-046 Article R. 232-95-1 du code du sport, relatif à la procédure disciplinaire devant la commission des sanctions de l'Agence française de lutte contre le dopage (AFLD), prévoyant qu'il peut être recouru à des moyens de conférence audiovisuelle à la demande des personnes à l'encontre desquelles une procédure disciplinaire est engagée.......1) La procédure suivie en l'espèce par la commission des sanctions de l'Agence, dont l'audience s'est tenue par des moyens de conférence audiovisuelle alors que la personne convoquée ne l'avait pas demandé, a été irrégulière. ......2) Cette irrégularité a privé l'intéressée de la garantie, prévue par les articles L. 232-22 et R. 232-95-1 du code du sport, tenant à ce qu'elle puisse être entendue en personne, sauf demande de sa part, pour présenter ses observations devant la commission des sanctions.</ANA>
<ANA ID="9B"> 59-02-02-02 Article R. 232-95-1 du code du sport, relatif à la procédure disciplinaire devant la commission des sanctions de l'Agence française de lutte contre le dopage (AFLD), prévoyant qu'il peut être recouru à des moyens de conférence audiovisuelle à la demande des personnes à l'encontre desquelles une procédure disciplinaire est engagée.......1) La procédure suivie en l'espèce par la commission des sanctions de l'Agence, dont l'audience s'est tenue par des moyens de conférence audiovisuelle alors que la personne convoquée ne l'avait pas demandé, a été irrégulière. ......2) Cette irrégularité a privé l'intéressée de la garantie, prévue par les articles L. 232-22 et R. 232-95-1 du code du sport, tenant à ce qu'elle puisse être entendue en personne, sauf demande de sa part, pour présenter ses observations devant la commission des sanctions.</ANA>
<ANA ID="9C"> 63-05-05 Article R. 232-95-1 du code du sport, relatif à la procédure disciplinaire devant la commission des sanctions de l'Agence française de lutte contre le dopage (AFLD), prévoyant qu'il peut être recouru à des moyens de conférence audiovisuelle à la demande des personnes à l'encontre desquelles une procédure disciplinaire est engagée.......1) La procédure suivie en l'espèce par la commission des sanctions de l'Agence, dont l'audience s'est tenue par des moyens de conférence audiovisuelle alors que la personne convoquée ne l'avait pas demandé, a été irrégulière. ......2) Cette irrégularité a privé l'intéressée de la garantie, prévue par les articles L. 232-22 et R. 232-95-1 du code du sport, tenant à ce qu'elle puisse être entendue en personne, sauf demande de sa part, pour présenter ses observations devant la commission des sanctions.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 23 décembre 2011, M. Danthony et autres, n° 335033, p. 649....[RJ2] Rappr., s'agissant des conditions dans lesquelles le recours à la visioconférence peut être organisé devant une juridiction administrative, CE, 21 décembre 2020, Syndicat de la juridiction administrative, n° 441399, T. pp. 551-810-924-927-961-962 ; s'agissant de l'inconstitutionnalité d'une disposition permettant au juge pénal d'imposer le recours à la visioconférence, Cons. const., 15 janvier 2021, n° 2020-872 QPC ; s'agissant de son inconventionnalité au regard de l'article 6§1 de la convention EDH, CE, 5 mars 2021, Ordre des avocats du Conseil d'Etat et à la Cour de cassation et M. Beshoret, n°s 440037 440165, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
