<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035803976</ID>
<ANCIEN_ID>JG_L_2017_10_000000399752</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/80/39/CETATEXT000035803976.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème chambres réunies, 02/10/2017, 399752, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-10-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>399752</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP RICHARD ; RICARD</AVOCATS>
<RAPPORTEUR>M. Frédéric Pacoud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:399752.20171002</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...C...a demandé au tribunal administratif de Nantes d'annuler pour excès de pouvoir la délibération du 15 juin 2012 par laquelle le conseil municipal de Mesquer a approuvé le plan local d'urbanisme de la commune. Par un jugement n°s 1204680, 1207849 du 10 juin 2014, le tribunal administratif de Nantes a rejeté cette demande.<br/>
<br/>
              Mme B...C...a demandé au tribunal administratif de Nantes d'annuler pour excès de pouvoir la délibération du 15 juin 2012 par laquelle le conseil municipal de Mesquer a approuvé le plan local d'urbanisme et la décision implicite de rejet de son recours gracieux. Par un jugement n° 1211444 du 10 juin 2014, le tribunal administratif de Nantes a rejeté cette demande.<br/>
<br/>
              Par un arrêt n°s 14NT02138 et 14NT02154 du 11 mars 2016, la cour administrative d'appel de Nantes a rejeté les appels formés par M. et MmeC....<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 12 mai 2016, 12 août 2016 et 29 mars 2017 au secrétariat du contentieux du Conseil d'Etat, M. et Mme C...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de la commune de Mesquer la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de l'urbanisme ;<br/>
              - la loi n° 86-2 du 3 janvier 1986 ;<br/>
              - l'ordonnance n° 2015-1341 du 23 octobre 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Pacoud, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Richard, avocat de M. et Mme C...et à Me Ricard, avocat de la commune de Mesquer.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que, par une délibération du 3 décembre 2001, le conseil municipal de Mesquer (Loire-Atlantique) a prescrit l'élaboration du plan local d'urbanisme de la commune. Une enquête publique s'est tenue du 27 octobre au 26 novembre 2011 et le plan local d'urbanisme a été approuvé par une délibération du conseil municipal du 24 février 2012. Toutefois, le préfet de la Loire-Atlantique ayant, à l'occasion du contrôle de la légalité de cette délibération, demandé le 10 mai 2012 que des modifications soient apportées au plan local d'urbanisme tel qu'approuvé le 24 février 2012, le conseil municipal a, par délibération du 15 juin 2012, approuvé un plan local d'urbanisme modifié après avoir retiré sa délibération précédente. M. et Mme C...se pourvoient en cassation contre l'arrêt du 11 mars 2016 par lequel la cour administrative d'appel de Nantes a rejeté leurs appels formés contre les jugements du tribunal administratif de Nantes rejetant leurs demandes d'annulation pour excès de pouvoir de la délibération du 15 juin 2012.<br/>
<br/>
              Sur les moyens relatifs à la procédure d'adoption du plan local d'urbanisme :<br/>
<br/>
              2. En premier lieu, pour écarter comme manquant en fait le moyen tiré de ce que ni l'objet figurant sur la convocation du conseil municipal du 15 juin 2012 ni les pièces qui y étaient jointes n'avaient permis aux membres du conseil municipal de délibérer en toute connaissance de cause des modifications apportées au plan local d'urbanisme, en méconnaissance des dispositions des articles L. 2121-10 et 2121-13 du code général des collectivités territoriales imposant que les questions portées à l'ordre du jour soient indiquées lors de la convocation et que tout conseiller municipal soit informé de l'objet des délibérations, la cour a relevé que l'ordre du jour joint à la convocation du conseil municipal mentionnait l'approbation du plan local d'urbanisme et que la commune de Mesquer faisait valoir sans être contredite sur ce point qu'était joint à cette convocation le projet de délibération, mentionnant que le conseil municipal serait amené à annuler la délibération du 24 février 2012 ayant approuvé le plan local d'urbanisme et à approuver le plan local d'urbanisme tel qu'annexé à ce projet. En statuant ainsi, par des motifs non argués de dénaturation, la cour n'a pas commis d'erreur de droit. <br/>
<br/>
              3. En deuxième lieu, il ressort des pièces du dossier soumis aux juges du fond que la cour a pu, sans se méprendre sur la portée des écritures de Mme C...ni, par suite, méconnaître son office, écarter comme non assorti de précisions suffisantes pour permettre au juge d'en apprécier le bien-fondé le moyen tiré de l'absence de consultation du Conservatoire du littoral et des rivages lacustres et de la commission des sites, perspectives et paysages. <br/>
<br/>
              4. En troisième lieu, il ressort des pièces du dossier soumis aux juges du fond que le rapport de présentation du plan local d'urbanisme comportait, conformément aux dispositions alors applicables des articles L. 121-10 et R. 123-2-1 du code de l'urbanisme, un résumé non technique de l'évaluation environnementale. Par suite, c'est sans dénaturer les pièces du dossier que la cour a écarté comme manquant en fait le moyen tiré de l'absence de ce résumé.<br/>
<br/>
              5. En quatrième lieu, aux termes de l'article L. 123-10 du code de l'urbanisme, dans sa rédaction en vigueur à la date de la délibération du conseil municipal de la commune de Mesquer : " Le projet de plan local d'urbanisme est soumis à enquête publique réalisée conformément au chapitre III du titre II du livre Ier du code de l'environnement par (...) le maire. (...). Le dossier soumis à enquête  comprend, en annexe, les avis des personnes publiques consultées. / Après l'enquête publique (...), le plan local d'urbanisme, éventuellement modifié, est approuvé par délibération (...) du conseil municipal (...) ". Il résulte de ces dispositions que le projet de plan ne peut subir de modifications, entre la date de sa soumission à l'enquête publique et celle de son approbation, qu'à la double condition que ces modifications ne remettent pas en cause l'économie générale du projet et procèdent de l'enquête. Doivent être regardées comme procédant de l'enquête les modifications destinées à tenir compte des réserves et recommandations de la commission d'enquête, des observations du public et des avis émis par les autorités, collectivités et instances consultées et joints au dossier de l'enquête.<br/>
<br/>
              6. La cour, qui a relevé que l'avis du préfet de la Loire-Atlantique émis le 13 octobre 2011 était joint au dossier d'enquête publique, n'a pas commis d'erreur de droit en se fondant, pour juger que les modifications apportées au projet de plan local d'urbanisme devaient être regardées comme procédant de l'enquête publique, sur ce que l'avis du préfet dont elles résultaient était antérieur à l'enquête. En estimant en outre que les modifications apportées au projet de plan local d'urbanisme afin de tenir compte de cet avis ne bouleversaient pas l'économie générale du projet, la cour a suffisamment motivé son arrêt sur ce point.<br/>
<br/>
              7. En cinquième lieu, d'une part, aux termes du premier alinéa de l'article L. 2131-1 du code général des collectivités territoriales : " Les actes pris par les autorités communales sont exécutoires de plein droit dès qu'il a été procédé à leur publication ou affichage ou à leur notification aux intéressés ainsi qu'à leur transmission au représentant de l'Etat dans le département ou à son délégué dans l'arrondissement. (...) ". Par dérogation à ces dispositions, l'article L. 123-12 du code de l'urbanisme, dans sa rédaction applicable à la délibération en litige, prévoit que, dans les seules communes non couvertes par un schéma de cohérence territoriale, l'acte publié approuvant le plan local d'urbanisme devient en principe exécutoire un mois suivant sa transmission au préfet, mais ne le devient qu'après l'intervention des modifications demandées par le préfet lorsque celui-ci, dans le délai d'un mois ainsi prévu, notifie à la commune les modifications qu'il estime nécessaire d'apporter au plan, dans un certain nombre d'hypothèses. D'autre part, l'article L. 123-13 du même code dispose, dans sa rédaction alors applicable, que : " Le plan local d'urbanisme est modifié ou révisé par délibération (...) du conseil municipal après enquête publique réalisée conformément au chapitre III du titre II du livre Ier du code de l'environnement ". Par ailleurs, le premier alinéa de l'article L. 2131-6 du code général des collectivités territoriales prévoit que le représentant de l'Etat dans le département défère au tribunal administratif, dans les deux mois suivant leur transmission, les actes pris par les autorités communales qu'il estime contraires à la légalité.<br/>
<br/>
              8. L'autorité administrative peut légalement retirer un texte réglementaire illégal dans le délai qui lui est ouvert pour ce faire et en a l'obligation si elle est saisie d'un recours gracieux dans ce délai, correspondant, pour les actes intervenus avant le 1er juin 2016, ainsi que le prévoit le II de l'article 9 de l'ordonnance du 23 octobre 2015 relative aux dispositions législatives du code des relations entre le public et l'administration, au délai pendant lequel le texte n'est pas devenu définitif, c'est-à-dire tant que le délai du recours contentieux n'est pas expiré ou qu'un recours gracieux ou contentieux formé dans ce délai est pendant. Il en résulte que le conseil municipal d'une commune couverte par un schéma de cohérence territoriale peut légalement retirer la délibération par laquelle il a adopté le plan local d'urbanisme si elle est illégale, alors même qu'elle est devenue exécutoire en application de l'article L. 2131-1 du code général des collectivités territoriales, tant que n'est pas expiré le délai dont il dispose pour ce faire. Après avoir procédé à un tel retrait, il peut légalement approuver le nouveau plan local d'urbanisme destiné à remédier aux illégalités constatées, sans engager la procédure de modification après enquête publique prévue à l'article L. 123-13 du code de l'urbanisme, ni même procéder à une nouvelle enquête publique, dès lors que les rectifications visant à assurer sa légalité ne remettent pas en cause l'économie générale du projet de plan et procèdent de l'enquête publique à laquelle celui-ci a été soumis.<br/>
<br/>
              9. Il ressort des pièces du dossier soumis aux juges du fond, ainsi que l'a relevé la cour, par un arrêt suffisamment motivé sur ce point, que la commune de Mesquer est couverte par le schéma de cohérence territoriale de la communauté d'agglomération de la presqu'île de Guérande-Atlantique, de sorte que les dispositions de l'article L. 123-12 du code de l'urbanisme mentionné au point 7 ne lui sont pas applicables et qu'en vertu de l'article L. 2131-1 du code général des collectivités territoriales, le plan local d'urbanisme, approuvé par une délibération du conseil municipal du 24 février 2012, était devenu exécutoire de plein droit dès sa publication et sa transmission, le 9 mars 2012, au préfet de la Loire-Atlantique. Toutefois, par une lettre du 10 mai 2012, ce dernier a invité le maire de Mesquer à procéder à des rectifications et à solliciter une nouvelle délibération d'approbation du plan pour remédier à son illégalité sur certains points, formant ainsi un recours gracieux, exercé dans le délai de recours contentieux. Par suite, il résulte de ce qui a été dit au point 8 que la cour n'a pas commis d'erreur de droit en jugeant, après avoir relevé que les rectifications apportées lors de cette nouvelle délibération devaient être regardées comme procédant de l'enquête publique et ne portaient pas atteinte à l'économie générale du projet, qu'alors même que le plan local d'urbanisme approuvé par délibération du 24 février 2012 était devenu exécutoire, le conseil municipal de Mesquer pouvait légalement, par sa délibération du 15 juin 2012, rapporter sa précédente délibération, dont il n'est pas contesté qu'elle était illégale et qui n'était pas devenue définitive, et approuver le nouveau plan local d'urbanisme prenant en compte les observations formulées par le préfet afin de remédier à son illégalité, sans engager la procédure de modification prévue par l'article L. 123-13 du code de l'urbanisme et sans le soumettre à une nouvelle enquête publique. <br/>
<br/>
              Sur les moyens relatifs à la légalité interne du plan local d'urbanisme :<br/>
<br/>
              10. En premier lieu, aux termes de l'article L. 121-1 du code de l'urbanisme, dans sa rédaction en vigueur à la date de la délibération en litige : " (...) les plans locaux d'urbanisme (...) déterminent les conditions permettant d'assurer, dans le respect des objectifs du développement durable : / 1° L'équilibre entre : / a) Le renouvellement urbain, le développement urbain maîtrisé, la restructuration des espaces urbanisés, la revitalisation des centres urbains et ruraux ; / b) L'utilisation économe des espaces naturels, la préservation des espaces affectés aux activités agricoles et forestières, et la protection des sites, des milieux et paysages naturels ; (...) ". Aux termes de l'article L. 123-1 du même code : " Le plan local d'urbanisme respecte les principes énoncés aux articles L. 110 et L. 121-1. Il comprend un rapport de présentation, un projet d'aménagement et de développement durables, des orientations d'aménagement et de programmation, un règlement et des annexes. (...) ". Enfin, aux termes du premier alinéa de l'article L. 123-1-5 de ce code : " Le règlement fixe, en cohérence avec le projet d'aménagement et de développement durables, les règles générales et les servitudes d'utilisation des sols permettant d'atteindre les objectifs mentionnés à l'article L. 121-1, qui peuvent notamment comporter l'interdiction de construire, délimitent les zones urbaines ou à urbaniser et les zones naturelles ou agricoles et forestières à protéger et définissent, en fonction des circonstances locales, les règles concernant l'implantation des constructions ".<br/>
<br/>
              11. Si le projet d'aménagement et de développement durables du plan local d'urbanisme en litige prévoit de " réaliser des espaces verts communs au sein des opérations d'aménagement d'ensemble (20 %) ", d'une part, ces dispositions n'imposent pas que 20 % de la surface de l'opération soient plantés d'arbres, d'autre part, il ne ressort pas des termes de l'article 1AU13, portant sur la réalisation d'espaces libres, d'aires de jeux et de loisirs et de plantations, du règlement de la zone AU, selon lequel " les opérations comportant plus de 5 logements doivent obligatoirement comporter des espaces communs, à disposition de l'ensemble des colotis (exemples : aires de jeux, plantations, cheminements pour piétons...) représentant 20 %, voirie comprise, de la superficie du terrain intéressé par l'opération. Dans tous les cas, 10 % au moins de la surface de l'opération doivent être plantés d'arbres de haute tige ", que la voirie y serait regardée comme un espace vert commun. Par suite, la cour n'a pas commis d'erreur de droit en écartant le moyen tiré de ce que le règlement du plan local d'urbanisme n'était pas en cohérence avec le projet d'aménagement et de développement durables.<br/>
<br/>
              12. En second lieu, aux termes du I de l'article L. 146-4 du code de l'urbanisme alors applicables, désormais repris à l'article L. 121-8 du même code : " L'extension de l'urbanisation doit se réaliser soit en continuité avec les agglomérations et villages existants, soit en hameaux nouveaux intégrés à l'environnement (...) ". Il résulte de ces dispositions, éclairées par les travaux préparatoires de la loi du 3 janvier 1986 relative à l'aménagement, la protection et la mise en valeur du littoral dont elles sont issues, que le plan local d'urbanisme d'une commune littorale peut prévoir l'extension de l'urbanisation soit en continuité avec les agglomérations et villages existants, c'est-à-dire avec les zones déjà urbanisées, caractérisées par un nombre et une densité significatifs de constructions, soit en délimitant une zone destinée à l'accueil d'un hameau nouveau intégré à l'environnement.<br/>
<br/>
              13. Pour juger que les secteurs dit de Ker Croisé et du Kerlagadec se trouvaient en continuité d'une agglomération, la cour, reprenant les motifs des premiers juges, a relevé qu'ils s'inséraient, l'un entre deux zones largement urbanisées, l'autre dans une partie urbanisée de la commune, à deux cents mètres du bourg, et qu'ils devaient être regardés comme situés en continuité d'un secteur aggloméré, caractérisé par une densité significative de constructions. En écartant ainsi les moyens tirés de la méconnaissance du I de l'article L. 146-4 du code de l'urbanisme et en jugeant que le classement de ces deux secteurs en zone 1 AU du plan local d'urbanisme, définie par le règlement comme une zone insuffisamment équipée destinée à accueillir des urbanisations nouvelles à vocation d'habitat, sous réserve de la réalisation des équipements nécessaires à sa viabilisation, n'était pas entaché d'erreur manifeste d'appréciation, la cour s'est livrée à une appréciation souveraine des pièces du dossier exempte de dénaturation.<br/>
<br/>
              14. Il résulte de tout ce qui précède que M. et Mme C...ne sont pas fondés à demander l'annulation de l'arrêt de la cour administrative d'appel de Nantes qu'ils attaquent.<br/>
<br/>
              15. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'il soit fait droit aux conclusions présentées à ce titre par M. et MmeC.... Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. et Mme C...une somme de 1 000 euros à verser à la commune de Mesquer au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. et Mme C...est rejeté. <br/>
Article 2 : M. et Mme C...verseront à la commune de Mesquer une somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à Mme B...C..., première requérante dénommée, et à la commune de Mesquer. <br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
