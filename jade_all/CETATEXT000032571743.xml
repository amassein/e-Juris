<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032571743</ID>
<ANCIEN_ID>JG_L_2016_05_000000390927</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/57/17/CETATEXT000032571743.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 20/05/2016, 390927, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-05-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>390927</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER, TEXIDOR</AVOCATS>
<RAPPORTEUR>Mme Célia Verot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:390927.20160520</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              La société Compagnie des transports strasbourgeois a demandé au tribunal administratif de Strasbourg la restitution des droits de taxe sur la valeur ajoutée qu'elle a acquittés à concurrence de la somme de 3 233 616 euros au titre de la période allant du 1er janvier au 31 décembre 2002 et la condamnation de l'Etat à lui verser les intérêts moratoires prévus à l'article L. 208 du livre des procédures fiscales sur cette somme. Par un jugement n° 0901937 du 23 avril 2013, le tribunal administratif de Strasbourg a jugé qu'il n'y avait plus lieu de statuer sur les conclusions tendant à la restitution de la taxe sur la valeur ajoutée collectée à tort et rejeté le surplus des conclusions.  <br/>
<br/>
              Par un arrêt n°13NC01030 du 21 avril 2015, la cour administrative d'appel de Nancy, faisant partiellement droit à l'appel formé par la société Compagnie des transports strasbourgeois contre ce jugement, a condamné l'Etat à verser à la société Compagnie des transports strasbourgeois les intérêts moratoires sur la somme de 3 233 616 euros pour la période du 23 avril 2010 au 28 mai 2010 et rejeté le surplus des conclusions.<br/>
<br/>
              Par un pourvoi et un nouveau mémoire, enregistrés les 10 juin et 14 octobre 2015 au secrétariat du contentieux du Conseil d'Etat, le ministre des finances et des comptes publics demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il lui fait grief ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter entièrement l'appel de la société Compagnie des transports strasbourgeois. <br/>
<br/>
<br/>
              Vu  les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et son premier protocole additionnel ; <br/>
              - le code général des impôts et le livre des procédures fiscales ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Célia Verot, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Blancpain, Soltner, Texidor, avocat de la société Compagnie des transports strasbourgeois ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Compagnie des transports strasbourgeois a demandé, le 18 décembre 2008, la restitution de la taxe sur la valeur ajoutée qu'elle avait collectée à tort au cours de l'année 2002. L'administration fiscale n'ayant fait que partiellement droit à sa demande compte tenu de sa situation créditrice au regard de cette taxe sur la période considérée, la société a saisi le tribunal administratif de Strasbourg d'une demande tendant, d'une part, à la restitution du montant correspondant à la différence entre la taxe collectée à tort et la restitution qui lui a été accordée et, d'autre part, au versement des intérêts moratoires prévus par l'article L. 208 du livre des procédures fiscales. En cours d'instance, sur autorisation de l'administration fiscale, la société a imputé le montant de la taxe restant en litige sur sa déclaration de chiffre d'affaires du mois de mars 2010 souscrite le 23 avril 2010. Il est résulté de cette opération un crédit de taxe dont la société a obtenu le remboursement le 28 mai 2010. Par jugement du 23 avril 2013, le tribunal administratif de Strasbourg a jugé qu'il n'y avait plus lieu de statuer sur la demande de la société tendant à la restitution de la taxe sur la valeur ajoutée collectée à tort au cours de l'année 2002, sans faire droit à ses conclusions tendant au versement d'intérêts moratoires. Le ministre des finances et des comptes publics demande l'annulation de l'arrêt du 21 avril 2015 de la cour administrative d'appel de Nancy en tant qu'il a ordonné le versement des intérêts moratoires au titre de la période courant du 23 avril 2010, date de souscription de la déclaration de chiffres d'affaires du mois de mars 2010, au 28 mai 2010, date du remboursement du crédit de taxe sur la valeur ajoutée, et réformé le jugement du tribunal administratif de Strasbourg en ce sens.  <br/>
<br/>
              2. D'une part, aux termes du IV de l'article 271 du code général des impôts : " IV. La taxe déductible dont l'imputation n'a pu être opérée peut faire l'objet d'un remboursement dans les conditions, selon les modalités et dans les limites fixées par décret en Conseil d'Etat ". Aux termes du II de l'article 208 de l'annexe II à ce code : " Lorsque, sur une déclaration, le montant de la taxe déductible excède le montant de la taxe due, l'excédent de taxe dont l'imputation ne peut être faite est reporté, jusqu'à épuisement, sur les déclarations suivantes. Toutefois, cet excédent peut faire l'objet de remboursements dans les conditions fixées par les articles 242-0 A à 242-0 K ". L'article 242-0 A de la même annexe prévoit : " Le remboursement de la taxe sur la valeur ajoutée déductible dont l'imputation n'a pu être opérée doit faire l'objet d'une demande des assujettis. Le remboursement porte sur le crédit de taxe déductible constaté au terme de chaque année civile ".  Les demandes de remboursement de crédits de taxe sur la valeur ajoutée présentées sur le fondement du IV de l'article 271 du code général des impôts constituent des réclamations contentieuses qui sont soumises à des conditions et délais particuliers fixés par les articles 242-0 A et suivants de l'annexe II à ce code. Lorsque l'administration n'a pas statué sur cette réclamation dans le délai de six mois qui lui est imparti, elle est considérée comme ayant rejeté implicitement la réclamation dont elle était saisie. <br/>
<br/>
              3. D'autre part, aux termes de l'article L. 208 du livre des procédures fiscales : " Quand l'Etat est condamné à un dégrèvement d'impôt par un tribunal ou quand un dégrèvement est prononcé par l'administration des impôts à la suite d'une réclamation tendant à la réparation d'une erreur commise dans l'assiette ou le calcul de l'imposition, les sommes déjà perçues sont remboursées au contribuable et donnent lieu au paiement d'intérêts moratoires dont le taux est celui de l'intérêt légal. Les intérêts courent du jour du paiement (...) ". Il résulte de ces dispositions que les remboursements de taxe sur la valeur ajoutée obtenus par une société après le rejet par l'administration d'une réclamation ont le caractère de dégrèvement contentieux de la même nature que celui prononcé par un tribunal au sens de ces dispositions. Ces remboursements doivent, dès lors, donner lieu au paiement d'intérêts moratoires qui courent, s'agissant de la procédure de remboursement de crédits de taxe sur la valeur ajoutée, pour laquelle il n'y a pas de paiement antérieur de la part du redevable, à compter de la date de la réclamation qui fait apparaître le crédit remboursable. <br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond que la société Compagnie des transports strasbourgeois a demandé le 23 avril 2010 le remboursement d'un crédit de taxe sur la valeur ajoutée sur le fondement du IV de l'article 271 du code général des impôts, auquel l'administration fiscale a fait droit le 28 mai 2010. En accordant à la société le versement des intérêts moratoires prévus à l'article L. 208 du livre des procédures fiscales au titre de la période courant du 23 avril 2010 au 28 mai 2010, alors que le remboursement du crédit de taxe sur la valeur ajoutée n'avait pas, en l'absence de rejet préalable explicite ou implicite de la réclamation de la société, le caractère d'un dégrèvement contentieux de la même nature que celui prononcé par un tribunal au sens des dispositions précitées de l'article L. 208 du livre des procédures fiscales, la cour a commis une erreur de droit. <br/>
<br/>
              5. Il résulte de ce qui précède et sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, que le ministre est fondé à demander l'annulation des articles 1er et 2 de l'arrêt qu'il attaque. <br/>
<br/>
              6. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge l'Etat qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 1er et 2 de l'arrêt du 21 avril 2015 de la cour administrative d'appel de Nancy sont annulés. <br/>
Article 2 : L'affaire est renvoyée dans cette mesure à la cour administrative d'appel de Nancy.<br/>
Article 3 : Les conclusions présentées par la société Compagnie des transports strasbourgeois au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 4 : La présente décision sera notifiée au ministre des finances et des comptes publics et à la société Compagnie des transports strasbourgeois. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
