<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027244259</ID>
<ANCIEN_ID>JG_L_2013_03_000000338289</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/24/42/CETATEXT000027244259.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 5ème sous-sections réunies, 28/03/2013, 338289</TITRE>
<DATE_DEC>2013-03-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>338289</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 5ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARTHELEMY, MATUCHANSKY, VEXLIARD</AVOCATS>
<RAPPORTEUR>M. Jérôme Marchand-Arvier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Keller</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:338289.20130328</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 1er avril et 1er juin 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société Selling attitude, dont le siège est 4, Cité Popincourt à Paris (75011) ; la société Selling attitude demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09PA00569 du 21 janvier 2010 par lequel la cour administrative d'appel de Paris a rejeté sa requête tendant à l'annulation du jugement n° 0406020/3-3 du 20 novembre 2008 du tribunal administratif de Paris annulant la décision du 22 décembre 2003 du ministre des affaires sociales, du travail et de la solidarité autorisant le licenciement de M. A... B... ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de M. B...la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code du travail ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jérôme Marchand-Arvier, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Gatineau, Fattaccini, avocat de la société Selling attitude et de la SCP Barthelemy, Matuchansky, Vexliard, avocat de M.B...,<br/>
<br/>
              - les conclusions de M. Rémi Keller, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Gatineau, Fattaccini, avocat de la société Selling attitude et de la SCP Barthelemy, Matuchansky, Vexliard, avocat de M.B...,<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'en vertu du cinquième alinéa de l'article L. 425-1 du code du travail, alors en vigueur, désormais codifié à l'article L. 2411-7 du code du travail, la procédure soumettant le licenciement d'un salarié à l'autorisation de l'inspecteur du travail s'applique lorsque la lettre du syndicat notifiant à l'employeur la candidature du salarié aux fonctions de délégué du personnel a été reçue par l'employeur ou lorsque le salarié a fait la preuve que l'employeur a eu connaissance de l'imminence de sa candidature avant que le candidat n'ait été convoqué à l'entretien préalable au licenciement ; que cette procédure s'applique également aux candidats, au premier tour comme au second tour, aux fonctions de délégués du personnel ; que la protection ainsi instituée est valable, en vertu du même article, pendant une durée de six mois ; qu'en vertu des dispositions de l'article L. 423-14 du code du travail alors en vigueur, le scrutin pour les élections aux fonctions de délégués du personnel est à deux tours ; que le premier tour est réservé aux listes établies par les organisations syndicales représentatives ; que si aucune liste n'a été établie par ces organisations ou si le nombre des votants est inférieur à la moitié des électeurs inscrits, il est procédé à un second tour de scrutin, pour lequel les électeurs peuvent voter pour les listes autres que celles présentées par les organisations syndicales ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. B...a, le 12 mars 2003, adressé à son employeur, la société Selling attitude, une lettre lui demandant d'organiser des élections de délégués du personnel et l'informant de son intention de se porter candidat à ces élections ; que, le 14 mai 2003, la société Selling attitude a convoqué M.  B... à un entretien préalable en vue de son licenciement pour faute grave ; qu'en l'absence de liste établie par les organisations syndicales représentatives au premier tour de scrutin des élections des délégués du personnel, qui devait avoir lieu le 28 mai 2003, un second tour de scrutin a été organisé le 11 juillet 2003, auquel M. B...a été candidat, ainsi qu'en atteste notamment le procès-verbal établi à l'issue de l'élection ; que, saisi par la société Selling attitude, l'inspecteur du travail a, le 7 juillet 2003, autorisé le licenciement de M.B... ; que, saisi d'un recours hiérarchique formé par M.B..., le ministre des affaires sociales, du travail et de la solidarité a, le 22 décembre 2003, annulé la décision de l'inspecteur du travail mais autorisé à son tour le licenciement ;<br/>
<br/>
              3. Considérant qu'ainsi qu'il a été dit, M. B...était candidat au second tour de l'élection des délégués du personnel qui s'est tenu le 11 juillet 2003 ; que la circonstance qu'il a initialement bénéficié de la protection résultant des dispositions alors en vigueur du cinquième alinéa de l'article L. 425-1 du code du travail, due au salarié qui a fait la preuve que l'employeur a eu connaissance de l'imminence de sa candidature avant que le candidat n'ait été convoqué à l'entretien préalable au licenciement, protection qui lui était due quand bien même sa candidature avait été présentée avant le premier tour de scrutin et ne figurait pas sur une liste établie par une organisation syndicale représentative, ne saurait le priver de la nouvelle protection due au titre de sa candidature aux élections ; que, dès lors, la cour qui, en reconnaissant comme seule protection à M. B...celle dont il bénéficiait au titre de l'imminence de sa candidature, a implicitement mais nécessairement estimé qu'il ne bénéficiait pas d'une nouvelle protection en tant que candidat au second tour, a commis une erreur de droit ; que, par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt doit être annulé ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui a été dit ci-dessus que M. B...bénéficie, en tant que candidat au second tour de l'élection des délégués du personnel, de la même protection que les délégués du personnel pendant une durée de six mois ; qu'aux termes du septième alinéa de l'article L. 425-1 du code du travail, alors en vigueur, la durée de six mois court à partir de l'envoi, par lettre recommandée à l'employeur, des listes de candidature ; qu'à défaut de cette lettre recommandée, les candidats ne sauraient, en tout état de cause, bénéficier de cette protection plus de six mois après l'élection à laquelle ils ont participé ; qu'en l'espèce, le second tour de l'élection des délégués du personnel s'est tenue le 11 juillet 2003 ; que, dans ces conditions, la période pendant laquelle M. B...bénéficiait de la protection instaurée par les dispositions susrappelées expirait le 11 janvier 2004 ; qu'ainsi, le ministre était compétent, le 22 décembre 2003, pour accorder l'autorisation de licencier M.B... ; qu'il résulte de ce qui précède que c'est à tort que le tribunal administratif de Paris s'est fondé sur l'incompétence du ministre pour annuler sa décision ;<br/>
<br/>
              6. Considérant, toutefois, qu'il appartient au Conseil d'Etat, saisi de l'ensemble du litige par l'effet dévolutif de l'appel, d'examiner les moyens soulevés par M. B...devant le tribunal administratif de Paris ;<br/>
<br/>
              7. Considérant que la décision du ministre des affaires sociales, du travail et de la solidarité accordant l'autorisation de licencier M. B...énonce les motifs de fait et de droit sur lesquels elle se fonde ; qu'ainsi, elle est suffisamment motivée ;<br/>
<br/>
              8. Considérant que, dans le cas où la demande de licenciement d'un salarié bénéficiant de la protection instaurée par les dispositions alors en vigueur de l'article L. 425-1 du code du travail est motivée par un comportement fautif, il appartient à l'inspecteur du travail saisi et, le cas échéant, au ministre compétent de rechercher, sous le contrôle du juge de l'excès de pouvoir, si les faits reprochés au salarié sont d'une gravité suffisante pour justifier son licenciement ; qu'en l'espèce, M.B..., responsable comptable de la société, a refusé d'assurer plusieurs opérations comptables concernant des filiales contrôlées par la société Selling attitude, opérations participant à la préparation de la consolidation des comptabilités des sociétés du groupe, alors que cette mission lui incombait en vertu de son contrat de travail ; qu'il a refusé de communiquer à son employeur son code d'accès au logiciel de comptabilité ; qu'il a menacé, même d'une manière ponctuelle et hypothétique, de ne pas procéder au virement des paies s'il n'obtenait pas de réponse à sa demande d'autorisation de congés payés ; qu'il n'a pas donné suite aux candidatures des assistants comptables qu'il demandait à recruter, alors que l'Agence nationale pour l'emploi lui avait adressé des candidats ; que ces faits sont constitutifs de fautes d'une gravité suffisante pour justifier le licenciement de M. B...; que la circonstance invoquée par M. B...selon laquelle la plupart des griefs qui lui seraient reprochés seraient postérieurs à sa demande d'organisation des élections de délégués du personnel ne suffit pas à établir que le licenciement serait en relation avec sa candidature à ces élections ; que, par suite, le ministre des affaires sociales, du travail et de la solidarité a légalement pu autoriser le licenciement de M.B... ;<br/>
<br/>
              9. Considérant qu'il résulte de ce qui précède que la société Selling attitude est fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Paris a annulé la décision en date du 22 décembre 2003 du ministre des affaires sociales, du travail et de la solidarité lui accordant l'autorisation de licencier M.B... ;<br/>
<br/>
              10. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la société Selling attitude et par M. B...au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
 Article 1er : L'arrêt de la cour administrative d'appel de Paris du 21 janvier 2010 est annulé.<br/>
<br/>
 Article 2 : Le jugement du tribunal administratif de Paris du 20 novembre 2008 est annulé.<br/>
<br/>
 Article 3 : La demande présentée par M. B...devant le tribunal administratif de Paris est rejetée.<br/>
<br/>
 Article 4 : Les conclusions présentées par la société Selling attitude et par M. B...au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
 Article 5 : La présente décision sera notifiée à la société Selling attitude, à M. A...B...et au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">66-07-01-01 TRAVAIL ET EMPLOI. LICENCIEMENTS. AUTORISATION ADMINISTRATIVE - SALARIÉS PROTÉGÉS. BÉNÉFICE DE LA PROTECTION. - CANDIDATS À L'ÉLECTION DES DÉLÉGUÉS DU PERSONNEL (5ÈME ET 8ÈME AL. DE L'ART. L. 425-1 DU CODE DU TRAVAIL) - 1) SALARIÉ AYANT ADRESSÉ À SON EMPLOYEUR UNE LETTRE LUI DEMANDANT D'ORGANISER DES ÉLECTIONS DE DÉLÉGUÉS DU PERSONNEL ET L'INFORMANT DE SON INTENTION DE SE PORTER CANDIDAT À CES ÉLECTIONS - EXISTENCE, QUAND BIEN MÊME SA CANDIDATURE AVAIT ÉTÉ PRÉSENTÉE AVANT LE PREMIER TOUR DE SCRUTIN ET NE FIGURAIT PAS SUR UNE LISTE ÉTABLIE PAR UNE ORGANISATION SYNDICALE REPRÉSENTATIVE [RJ1] - 2) NOUVELLE PROTECTION EN CAS DE SECOND TOUR DES ÉLECTIONS - EXISTENCE.
</SCT>
<ANA ID="9A"> 66-07-01-01 1) En vertu du cinquième alinéa de l'article L. 425-1 du code du travail, alors en vigueur, la procédure soumettant le licenciement d'un salarié à l'autorisation de l'inspecteur du travail s'applique dans le cas où un salarié a adressé à son employeur une lettre lui demandant d'organiser des élections de délégués du personnel et l'informant de son intention de se porter candidat à ces élections, quand bien même sa candidature avait été présentée avant le premier tour de scrutin et ne figurait pas sur une liste établie par une organisation syndicale représentative.,,2) Le salarié ainsi protégé en vertu du cinquième alinéa de l'article L. 425-1 du code du travail bénéficie d'une nouvelle période de protection s'il se porte candidat au second tour des élections, en vertu du septième alinéa du même article.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Ab. jur. CE, 5 novembre 1993, SA Socochare, n° 100132, T. p. 1067. Rappr. Cass. soc., 18 novembre 1992, n° 91-60.289, Bull. V n° 557 ; Cass. crim., 3 décembre 1996, n° 94-82.953, Bull. crim n° 444 ; Cass. soc., 21 décembre 2006, n° 04-47.426, Bull. V. n° 411.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
