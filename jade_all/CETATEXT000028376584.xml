<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028376584</ID>
<ANCIEN_ID>JG_L_2013_12_000000354856</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/37/65/CETATEXT000028376584.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  4ème sous-section jugeant seule, 23/12/2013, 354856, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-12-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>354856</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 4ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON ; SCP TIFFREAU, CORLAY, MARLANGE</AVOCATS>
<RAPPORTEUR>Mme Florence Chaltiel-Terral</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Keller</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:354856.20131223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et les mémoires complémentaires, enregistrés les 13 décembre 2011, 13 mars et 5 juin 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société Lionel Dufour Investissement dont le siège est 6, route de Moince BP 40 à Verny (57420) ; la société Lionel Dufour Investissement demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 10NC00600 du 13 octobre 2011 par lequel la cour administrative d'appel de Nancy a rejeté sa requête tendant à l'annulation du jugement n° 0801912 du 2 mars 2010 par lequel le tribunal administratif de Strasbourg a rejeté sa demande tendant à l'annulation de la décision du 30 novembre 2007 par laquelle le ministre du travail, des relations sociales et de la solidarité a, d'une part, annulé la décision de l'inspectrice du travail du 6 juin 2007 autorisant le licenciement de M. A...Mager et, d'autre part, refusé d'autoriser ce licenciement ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de M. Mager la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code du travail ; <br/>
<br/>
              Vu la décision n° 2012-242 QPC du 14 mai 2012 du Conseil constitutionnel ;<br/>
<br/>
              Vu le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Florence Chaltiel-Terral, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Rémi Keller, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boré, Salve de Bruneton, avocat de la SELARL Gangloff-Nardi et à la SCP Tiffreau, Corlay, Marlange, avocat de M. Mager ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. Mager, secrétaire général au sein de la société Lionel Dufour Investissement, exerçant les mandats de conseiller prud'homme et d'administrateur de la caisse primaire d'assurance maladie de Longwy, a fait l'objet d'une procédure de licenciement pour faute ; que, par une décision du 6 juin 2007, l'inspectrice du travail a autorisé le licenciement de M. Mager ; que, saisi d'un recours hiérarchique formé par ce dernier, le ministre du travail, des relations sociales et de la solidarité a annulé la décision de l'inspectrice du travail au motif qu'elle n'avait pas pris en compte le mandat d'administrateur de la caisse primaire d'assurance maladie de Longwy exercé par M. Mager puis a refusé d'autoriser le licenciement, par une décision du 30 novembre 2007 ; que, par un jugement du 2 mars 2010, le tribunal administratif de Strasbourg a rejeté la requête de la société Lionel Dufour Investissement tendant à l'annulation de la décision du ministre ; que la société a vu son appel à l'encontre de ce jugement rejeté par la cour administrative d'appel de Nancy dans un arrêt du 13 octobre 2011 ; que la société se pourvoit en cassation contre cet arrêt ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 2411-1 du code du travail : " Bénéficie de la protection contre le licenciement prévue par le présent chapitre, y compris lors d'une procédure de sauvegarde, de redressement ou de liquidation judiciaire, le salarié investi de l'un des mandats suivants : (...) / " 13° Membre du conseil ou administrateur d'une caisse de sécurité sociale mentionné à l'article L. 231-11 du code de la sécurité sociale ; (...) " ; qu'aux termes de l'article L. 2411-18 du même code : " Conformément à l'article L. 231-11 du code de la sécurité sociale, la procédure d'autorisation de licenciement et les périodes et durées de protection du salarié membre du conseil ou administrateur d'une caisse de sécurité sociale sont celles applicables au délégué syndical, prévues par l'article L. 2411-3 ; " ; qu'aux termes de l'article L. 2411-3 du même code : " Le licenciement d'un délégué syndical ne peut intervenir qu'après autorisation de l'inspecteur du travail. (...) " ; que la protection assurée au salarié par les dispositions précitées découle de l'exercice d'un mandat extérieur à l'entreprise ; que conformément à la réserve d'interprétation énoncée par la décision QPC du Conseil constitutionnel du 14 mai 2012, ces dispositions ne sauraient, sans porter une atteinte disproportionnée à la liberté d'entreprendre et à la liberté contractuelle, permettre au salarié protégé de se prévaloir d'une telle protection dès lors qu'il est établi qu'il n'en a pas informé son employeur au plus tard lors de l'entretien préalable au licenciement ;<br/>
<br/>
              3. Considérant que, pour rejeter la requête de la société, la cour administrative d'appel de Nancy a jugé que le ministre du travail avait pu légalement, annuler la décision de l'inspectrice du travail autorisant le licenciement de M. Mager, au motif qu'elle n'avait pas fait état de son mandat d'administrateur de la caisse primaire d'assurance maladie de Longwy alors même que l'employeur et l'inspectrice du travail en auraient ignoré l'existence, et refuser l'autorisation ; qu'il résulte de ce qui précède qu'en statuant ainsi, sans rechercher si le salarié avait informé son employeur de ce second mandat extérieur à l'entreprise, au plus tard lors de l'entretien préalable au licenciement, et s'il pouvait ainsi se prévaloir de la protection découlant de ce mandat, la cour a commis une erreur de droit ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt doit être annulé ; <br/>
<br/>
              4. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société Lionel Dufour Investissement qui n'est pas, dans la présente instance, la partie perdante ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la société Lionel Dufour Investissement au titre de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nancy du 13 octobre 2011 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nancy.<br/>
Article 3 : Le surplus des conclusions du pourvoi de la société Lionel Dufour Investissement est rejeté.<br/>
Article 4 : Les conclusions de M. Mager présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la SELARL Gangloff-Nardi, administrateur judiciaire de la société Lionel Dufour Investissement et à M. A...Mager. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
