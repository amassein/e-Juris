<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033237393</ID>
<ANCIEN_ID>JG_L_2016_10_000000392390</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/23/73/CETATEXT000033237393.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 12/10/2016, 392390</TITRE>
<DATE_DEC>2016-10-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>392390</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT</AVOCATS>
<RAPPORTEUR>M. Yves Doutriaux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:392390.20161012</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Paris d'annuler pour excès de pouvoir l'arrêté en date du 11 juin 2014 par lequel le préfet de police a refusé de lui délivrer un titre de séjour, l'a obligé à quitter le territoire français dans un délai de trente jours et a fixé le pays de destination.<br/>
<br/>
              Par un jugement n° 1410546/3-3 du 9 décembre 2014, le tribunal administratif a annulé cet arrêté et enjoint au préfet de police de procéder au réexamen de la demande de titre de séjour de M. A...dans un délai de trois mois.<br/>
<br/>
              Par un arrêt n° 15PA00111 du 28 mai 2015, la cour administrative d'appel de Paris a rejeté l'appel formé par le préfet de police contre ce jugement.<br/>
<br/>
              Par un pourvoi, enregistré le 5 août 2015 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à l'appel du préfet de police.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yves Doutriaux, conseiller d'Etat,<br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Potier de La Varde, Buk Lament, avocat de M.A.... <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'il ressort des énonciations de l'arrêt attaqué que M.A..., ressortissant égyptien, a été titulaire d'une carte de séjour temporaire portant la mention <br/>
" vie privée et familiale " délivrée sur le fondement du 11° de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile entre le 14 avril 2011 et le 13 avril 2012 ; qu'ayant sollicité le renouvellement de ce titre de séjour, il a reçu un récépissé de sa demande dont la validité a été prorogée jusqu'au 10 juillet 2014 ; qu'il a, durant l'instruction de sa demande, sollicité son admission au séjour " à défaut de pouvoir prétendre au renouvellement de son titre de séjour " sur le fondement des articles L. 313-10 et L. 313-14 du code de l'entrée et du séjour des étrangers et du droit d'asile ; que le préfet de police, par l'arrêté du 11 juin 2014 contesté, a refusé son admission au séjour en se fondant sur le motif que l'article L. 313-14 <br/>
" ne peut être sollicité en changement de statut dans le cadre d'un renouvellement d'un titre de séjour " ; que le tribunal administratif de Paris, jugeant ce motif entaché d'erreur de droit, a prononcé l'annulation de l'arrêté du 11 juin 2014 par un jugement du 9 décembre 2014 ; que la cour administrative d'appel de Paris, par l'arrêt contre lequel se pourvoit le ministre de l'intérieur, a rejeté l'appel formé par le préfet de police contre le jugement du tribunal administratif ; <br/>
<br/>
              2.	Considérant qu'aux termes de l'article L. 313-14 du code de l'entrée et du séjour des étrangers et du droit d'asile : " La carte de séjour temporaire mentionnée à l'article L. 313-11 ou la carte de séjour temporaire mentionnée au 1° de l'article L. 313-10 peut être délivrée, sauf si sa présence constitue une menace pour l'ordre public, à l'étranger ne vivant pas en état de polygamie dont l'admission au séjour répond à des considérations humanitaires ou se justifie au regard des motifs exceptionnels qu'il fait valoir, sans que soit opposable la condition prévue à l'article L. 311-7 (...) " ; que ces dispositions, qui ne font d'ailleurs et en tout état de cause nullement obstacle à l'exercice par le préfet du pouvoir discrétionnaire qui lui permet de régulariser la situation d'un étranger compte tenu de l'ensemble des éléments caractérisant sa situation personnelle, peuvent être invoquées, à l'appui d'une demande de renouvellement de titre de séjour, par un étranger pour le cas où il ne remplirait pas les conditions de renouvellement de ce titre ; <br/>
<br/>
              3.	Considérant que la cour administrative d'appel, pour rejeter l'appel formé par le préfet de police, a retenu que la circonstance que M. A...ait été encore en situation régulière lorsqu'il a sollicité son admission au séjour en invoquant l'article L. 313-14 du code de l'entrée et du séjour des étrangers et du droit d'asile n'interdisait pas à l'intéressé de se prévaloir de cet article, ce qu'il avait fait à titre subsidiaire au cas où sa demande de renouvellement de titre sur le fondement de l'article L. 313-11 du code aurait été rejetée ; qu'en statuant ainsi, la cour administrative d'appel n'a pas commis d'erreur de droit ; qu'il en résulte que le pourvoi du ministre de l'intérieur ne peut qu'être rejeté ;<br/>
<br/>
              4.	Considérant que M. A...a obtenu le bénéfice de l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Potier de la Varde, Buk Lament renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat la somme de 2 000 euros à verser à cette société ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi du ministre de l'intérieur est rejeté.<br/>
<br/>
Article 2 : L'Etat versera à la SCP Potier de la Varde, Buk Lament une somme de 2 000 euros en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
<br/>
Article 3 : La présente décision sera notifiée au ministre de l'intérieur et à M. B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">335-01-02-01 Étrangers. Séjour des étrangers. Autorisation de séjour. Demande de titre de séjour.
</SCT>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
