<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030459161</ID>
<ANCIEN_ID>JG_L_2015_04_000000367573</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/45/91/CETATEXT000030459161.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème / 8ème SSR, 02/04/2015, 367573</TITRE>
<DATE_DEC>2015-04-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>367573</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème / 8ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX ; SCP HEMERY, THOMAS-RAQUIN</AVOCATS>
<RAPPORTEUR>Mme Agnès Martinel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:367573.20150402</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Nantes d'annuler la décision du 17 juillet 2008 par laquelle la directrice de l'agence pour l'enseignement français à l'étranger (AEFE) a rejeté son recours gracieux tendant à la décharge de l'obligation de payer un trop-perçu de majorations familiales pour la période du 1er août au 31 décembre 2007. Par un jugement n° 0805858 du 30 décembre 2011, le tribunal administratif de Nantes a rejeté sa requête. <br/>
<br/>
              Par une ordonnance n° 12NT00648  du 18 mars 2013, le président de la cour administrative d'appel de Nantes a transmis au Conseil d'Etat le pourvoi de M. A...enregistrée le 2 mars 2012 au greffe de la cour administrative d'appel et tendant à l'annulation du jugement du 30 décembre 2011 du tribunal administratif de Nantes. <br/>
<br/>
              Par ce pourvoi et par un mémoire en réplique, enregistré le 3 février 2015 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa requête ;<br/>
<br/>
              3°) de mettre à la charge de l'Agence pour l'enseignement français à l'étranger la somme de 2000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la sécurité sociale ; <br/>
              - le décret n° 2002-22 du 4 janvier 2002 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Agnès Martinel, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Hémery, Thomas-Raquin, avocat de M. A...et à la SCP Baraduc, Duhamel, Rameix, avocat de l'Agence pour l'enseignement français à l'étranger ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article 4 du décret du 4 janvier 2002 relatif à la situation administrative et financière des personnels des établissements d'enseignement français à l'étranger : " Les émoluments des personnels visés à l'article 2 sont versés par l'AEFE en France, en euros. Ils sont exclusifs de tout autre élément de rémunération. Ils comportent : / A.-Pour les personnels expatriés / (...) e) Des majorations familiales pour enfants à charge, lesquelles sont attribuées en lieu et place des avantages familiaux accordés aux personnels en service en France et tiennent compte en outre des frais de scolarité des établissements français d'enseignement primaire et secondaire de référence au sein du pays ou de la zone d'affectation des agents. Les majorations familiales sont attribuées quel que soit le lieu de résidence des enfants, déduction faite des avantages de même nature dont peut bénéficier l'agent ou son conjoint, au titre des mêmes enfants et qui sont dus au titre de la législation ou de la réglementation française ou de tout accord communautaire ou international. / (...) / La notion d'enfant à charge s'apprécie selon les critères retenus en France pour l'attribution des prestations familiales par les articles L. 513-1 et L. 521-2 du code de la sécurité sociale. Les majorations familiales peuvent éventuellement être versées à une tierce personne physique ou morale dans les conditions prévues par l'article L. 552-6 du code de la sécurité sociale. " ; qu'aux termes de l'article L. 513-1 de ce code : "  Les prestations familiales sont, sous réserve des règles particulières à chaque prestation, dues à la personne physique qui assume la charge effective et permanente de l'enfant. " ; que, selon le premier alinéa de l'article L. 521-2 du même code, les allocations sont versées à la personne qui assume, dans quelques conditions que ce soit, la charge effective et permanente de l'enfant ; qu'aux termes du dernier alinéa de l'article R. 513-1 du même code : " en cas de divorce, de séparation de droit ou de fait des époux ou de cessation de la vie commune des concubins, et si l'un et l'autre ont la charge effective et permanente de l'enfant, l'allocataire est celui des membres du couple au foyer duquel vit l'enfant " ; <br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. A...est professeur titulaire détaché auprès de l'agence pour l'enseignement français à l'étranger (AEFE) en poste au lycée français Sadi Carnot d'Antsiranana à Madagascar ; qu'en août 2007, il s'est séparé de son épouse, laquelle a introduit une requête en divorce et s'est installée en France avec leurs deux enfants ; que, par décision du 17 juillet 2008, la directrice de l'AEFE a rejeté son recours gracieux contre un ordre de reversement des majorations familiales pour la période du 1er août au 31 décembre 2007 ; que, par le jugement attaqué du 30 décembre 2011, le tribunal administratif de Nantes a rejeté la demande de M. A...tendant à l'annulation de cette décision ; <br/>
<br/>
              3. Considérant que la notion de " charge effective et permanente de l'enfant " au sens des articles précités du code de la sécurité sociale et du décret du 4 janvier 2002 s'entend de la direction tant matérielle que morale de l'enfant ; que, dès lors, ne peut être regardé comme assumant cette direction matérielle et morale un père qui, alors même qu'il assume la totalité des frais d'entretien de l'enfant, n'en a pas la garde effective, la résidence de l'enfant ayant été fixée chez la mère ; que le tribunal administratif, après avoir souverainement estimé, sans inverser la charge de la preuve, que M.A..., qui vivait à Madagascar, n'avait pas la charge effective et permanente des enfants qui résidaient en France avec leur mère, alors même qu'il avait contribué financièrement à l'entretien des enfants, en a déduit qu'il n'avait, par suite, pas droit à l'avantage familial prévu à l'article 4 du décret du 4 janvier 2002 ; qu'en statuant ainsi, après avoir écarté, par un motif surabondant, le moyen tiré de ce que le requérant avait, au cours de la période litigieuse, assumé non pas certaines des dépenses en l'acquit de son épouse séparée mais la totalité de celles-ci, le tribunal administratif n'a pas commis d'erreur de droit ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que M. A...n'est pas fondé à demander l'annulation du jugement qu'il attaque ;  qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à sa charge la somme que demande l'Agence pour l'enseignement français à l'étranger au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces mêmes dispositions font obstacle à ce qu'une somme soit mise à la charge de cette agence qui n'est pas, dans la présente espèce, la partie perdante ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A...est rejeté. <br/>
Article 2 : Les conclusions de l'Agence pour l'enseignement français à l'étranger au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 3 : La présente décision sera notifiée à M. B...A...et à l'Agence pour l'enseignement français à l'étranger.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-08-03 FONCTIONNAIRES ET AGENTS PUBLICS. RÉMUNÉRATION. INDEMNITÉS ET AVANTAGES DIVERS. - AVANTAGES FAMILIAUX - NOTION DE  CHARGE EFFECTIVE ET PERMANENTE DE L'ENFANT  (ART. L. 513-1, L. 521-2 ET R. 513-1  DU CSS) - DIRECTION MATÉRIELLE ET MORALE DE L'ENFANT [RJ1].
</SCT>
<ANA ID="9A"> 36-08-03 La notion de  charge effective et permanente de l'enfant  au sens des articles L. 513-1, L. 521-2 et R. 513-1 du code de la sécurité sociale (CSS) s'entend de la direction tant matérielle que morale de l'enfant. Dès lors, ne peut être regardé comme assumant cette direction matérielle et morale un père qui, alors même qu'il assume la totalité des frais d'entretien de l'enfant, n'en a pas la garde effective, la résidence de l'enfant ayant été fixée chez la mère.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 20 juin 1980, Ministre de la santé et de la famille c/ Lamolère, n° 18002, p. 283. Comp. CE, Section, avis, 14 juin 2002, Mme Mouthe, n° 241036, p. 219.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
