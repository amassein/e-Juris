<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034823466</ID>
<ANCIEN_ID>JG_L_2017_05_000000395281</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/82/34/CETATEXT000034823466.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 29/05/2017, 395281, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-05-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>395281</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY ; SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>M. Bruno Bachini</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:395281.20170529</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Strasbourg d'annuler pour excès de pouvoir la décision du 15 octobre 2012 par laquelle le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social a autorisé la société d'Haussy Strasbourg à le licencier. Par un jugement n° 1205620 du 25 novembre 2014, le tribunal administratif a fait droit à sa demande. <br/>
<br/>
              Par un arrêt n° 14NC02212 du 15 octobre 2015, la cour administrative d'appel de Nancy a rejeté l'appel formé par la société d'Haussy Strasbourg contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 15 décembre 2015 et 14 mars 2016 au secrétariat du contentieux du Conseil d'Etat, la société d'Haussy Strasbourg demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de M. B...la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Bachini, maître des requêtes, <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, avocat de la société d'Haussy Strasbourg et à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M.B..., employé par la société d'Haussy Strasbourg en qualité de conducteur de ligne et exerçant les mandats de délégué du personnel et de délégué syndical ainsi que les fonctions de conseiller prud'homal, a fait l'objet d'une demande d'autorisation de licenciement pour faute, accordée le 15 octobre 2012 par le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social ; que la société d'Haussy Strasbourg se pourvoit en cassation contre l'arrêt par lequel la cour administrative d'appel de Nancy a, le 15 octobre 2015, rejeté son appel formé contre le jugement du 25 novembre 2014 par lequel le tribunal administratif de Strasbourg a, sur la demande de M.B..., annulé pour excès de pouvoir cette décision du 15 octobre 2012 ; <br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le moyen tiré de ce que le mémoire en défense de M. B...n'aurait pas été communiqué à la société requérante et que l'arrêt attaqué aurait, par suite, été pris au terme d'une procédure irrégulière manque en fait ; <br/>
<br/>
              3. Considérant qu'en application des dispositions du code du travail, les salariés légalement investis de fonctions représentatives bénéficient, dans l'intérêt de l'ensemble des salariés qu'ils représentent, d'une protection exceptionnelle ; que lorsque le licenciement d'un de ces salariés est envisagé, ce licenciement ne doit pas être en rapport avec les fonctions représentatives normalement exercées ou l'appartenance syndicale de l'intéressé ; que, dans le cas où la demande de licenciement est motivée par un comportement fautif, il appartient à l'inspecteur du travail de rechercher, sous le contrôle du juge de l'excès de pouvoir, si les faits reprochés au salarié sont d'une gravité suffisante pour justifier son licenciement, compte tenu de l'ensemble des règles applicables au contrat de travail de l'intéressé et des exigences propres à l'exécution normale du mandat dont il est investi ; <br/>
<br/>
              4. Considérant qu'il ressort des termes mêmes de l'arrêt attaqué que, pour juger que les faits reprochés a M. B...ne revêtaient pas, dans les circonstances de l'espèce, un caractère de gravité suffisant pour justifier son licenciement, la cour administrative d'appel de Nancy a relevé que, malgré la violence du geste par lequel l'intéressé avait, le 23 février 2012 en fin de journée, délibérément blessé l'un de ses collègues, cet acte fautif était intervenu en réponse à des provocations de la victime, dans un climat de tension et alors que M.B..., qui connaissait à l'époque d'importantes difficultés personnelles, n'avait, jusque là, jamais commis d'actes analogues ; qu'en statuant ainsi, par une décision suffisamment motivée, la cour administrative d'appel n'a pas inexactement qualifié les faits et n'a pas davantage dénaturé les pièces du dossier qui lui était soumis ;<br/>
<br/>
              5. Considérant qu'il résulte de tout ce qui précède que la société d'Haussy Strasbourg n'est pas fondée à demander l'annulation de l'arrêt attaqué ; <br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. B...qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de la société d'Haussy Strasbourg la somme de 3 500 euros à verser à ce dernier au titre de ces mêmes dispositions ; <br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Le pourvoi de la société d'Haussy Strasbourg est rejeté.<br/>
Article 2 : La société d'Haussy Strasbourg versera à M. B...une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : La présente décision sera notifiée à la société d'Haussy Strasbourg et à M. A...B....<br/>
Copie en sera adressée à la ministre du travail.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
