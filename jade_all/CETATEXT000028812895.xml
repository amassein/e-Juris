<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028812895</ID>
<ANCIEN_ID>JG_L_2014_03_000000364728</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/81/28/CETATEXT000028812895.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 31/03/2014, 364728</TITRE>
<DATE_DEC>2014-03-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>364728</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>Mme Esther de Moustier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:364728.20140331</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 21 décembre 2012 et 21 mars 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour le service départemental d'incendie et de secours (SDIS) de la Loire, dont le siège est rue ChanoineB..., BP 541 à Saint-Etienne (42007 Cedex 1) ; il demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 12LY00724 du 23 octobre 2012 par lequel la cour administrative d'appel de Lyon a rejeté le surplus des conclusions de sa requête tendant à l'annulation du jugement n° 1000379 du 14 décembre 2011 du tribunal administratif de Lyon le condamnant à verser à M. A...C...une somme de 22 000 euros ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de M. C...une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 96-370 du 3 mai 1996 ;<br/>
<br/>
              Vu la loi n° 2011-851 du 20 juillet 2011 ;<br/>
<br/>
              Vu le décret n° 99-1039 du 10 décembre 1999 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Esther de Moustier, auditeur,  <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la SDIS de la Loire et à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de M. C...;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un jugement devenu définitif du 30 juin 2009, le tribunal administratif de Lyon a annulé les décisions du président du SDIS de la Loire des 24 décembre 2003 et 1er mars 2006 résiliant d'office l'engagement de M. C... en qualité de sapeur-pompier volontaire et refusant de procéder à sa réintégration ; que, par un arrêt du 23 octobre 2012, la cour administrative d'appel de Lyon a condamné le SDIS de la Loire à verser à M. C...une indemnité de 18 351,04 euros en réparation du préjudice subi du fait de la résiliation illégale de son engagement de sapeur-pompier volontaire et du refus illégal de le réintégrer ; que le SDIS de la Loire se pourvoit en cassation contre cet arrêt, qui n'a que partiellement fait droit à sa requête tendant à l'annulation du jugement du tribunal administratif de Lyon  du 14 décembre 2011 le condamnant à verser à M. C...une somme de 22 000 euros ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article 1er de la loi du 3 mai 1996 relative au développement du volontariat dans les corps de sapeurs-pompiers, dans sa rédaction applicable au litige : " Les sapeurs-pompiers volontaires participent aux missions de sécurité civile de toute nature qui sont confiées sur l'ensemble du territoire aux services d'incendie et de secours " ; qu'aux termes de l'article 11 de la même loi  :  " Le sapeur-pompier volontaire a droit, pour les missions mentionnées à l'article 1er, les actions de formation auxquelles il participe et l'exercice de responsabilités administratives, à des vacations horaires. (...) / Ces vacations ne sont assujetties à aucun impôt ni soumises aux prélèvements prévus par la législation sociale. / Elles sont incessibles et insaisissables. Elles sont cumulables avec tout revenu ou prestation sociale " ; qu'aux termes du premier alinéa de l'article 8 du décret du 10 décembre 1999 relatif aux sapeurs-pompiers volontaires, alors applicable : " Les sapeurs-pompiers volontaires sont engagés pour une période de cinq ans, tacitement reconduite " ;<br/>
<br/>
              3. Considérant que lorsqu'un sapeur pompier volontaire a fait l'objet d'une résiliation illégale de son engagement et d'un refus illégal de le réintégrer, il est en droit de demander à être indemnisé du préjudice résultant de la chance sérieuse qu'il a perdue, pour la période en cause, de bénéficier des vacations horaires mentionnées à l'article 11 de la loi du 3 mai 1996 ; qu'ainsi, la cour n'a pas commis d'erreur de droit en jugeant que les vacations horaires versées aux sapeurs-pompiers volontaires ont le caractère d'indemnités dont la perte peut donner lieu à réparation ; <br/>
<br/>
              4. Considérant, toutefois, qu'en s'abstenant de rechercher s'il y avait lieu de déduire du montant de l'indemnisation les sommes correspondant à la rémunération des activités professionnelles que M. C...avait pu exercer au cours de sa période d'éviction, du fait de celle-ci, la cour a commis une erreur de droit ; que, par suite et sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, le SDIS de la Loire est fondé à demander l'annulation de l'arrêt qu'il attaque, en tant qu'il n'a que partiellement fait droit à sa requête ; <br/>
<br/>
              5. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par le SDIS de la Loire au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge du SDIS de la Loire qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon du 23 octobre 2012 est annulé en tant qu'il n'a que partiellement fait droit à l'appel du SDIS de la Loire.<br/>
Article 2 : L'affaire est renvoyée dans cette mesure à la cour administrative d'appel de Lyon.<br/>
Article 3 : Les conclusions du SDIS de la Loire et de M. C...présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 4 : La présente décision sera notifiée au service départemental d'incendie et de secours de la Loire et à M. A... C....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-01-04-02-03 COLLECTIVITÉS TERRITORIALES. DISPOSITIONS GÉNÉRALES. SERVICES PUBLICS LOCAUX. DISPOSITIONS PARTICULIÈRES. SERVICES D'INCENDIE ET SECOURS. - SAPEUR-POMPIER VOLONTAIRE AYANT FAIT L'OBJET D'UNE RÉSILIATION ILLÉGALE DE SON ENGAGEMENT ET D'UN REFUS ILLÉGAL DE LE RÉINTÉGRER - DROIT À LA RÉPARATION DU PRÉJUDICE RÉSULTANT DE LA PERTE DE CHANCE SÉRIEUSE DE BÉNÉFICIER DES VACATIONS HORAIRES MENTIONNÉES À L'ARTICLE 11 DE LA LOI DU 3 MAI 1996 - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-04-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RÉPARATION. PRÉJUDICE. - SAPEUR-POMPIER VOLONTAIRE AYANT FAIT L'OBJET D'UNE RÉSILIATION ILLÉGALE DE SON ENGAGEMENT ET D'UN REFUS ILLÉGAL DE LE RÉINTÉGRER - DROIT À LA RÉPARATION DU PRÉJUDICE RÉSULTANT DE LA PERTE DE CHANCE SÉRIEUSE DE BÉNÉFICIER DES VACATIONS HORAIRES MENTIONNÉES À L'ARTICLE 11 DE LA LOI DU 3 MAI 1996 - EXISTENCE.
</SCT>
<ANA ID="9A"> 135-01-04-02-03 Lorsqu'un sapeur pompier volontaire a fait l'objet d'une résiliation illégale de son engagement et d'un refus illégal de le réintégrer, il est en droit de demander à être indemnisé du préjudice résultant de la chance sérieuse qu'il a perdue, pour la période en cause, de bénéficier des vacations horaires mentionnées à l'article 11 de la loi n° 96-370 du 3 mai 1996.</ANA>
<ANA ID="9B"> 60-04-01 Lorsqu'un sapeur pompier volontaire a fait l'objet d'une résiliation illégale de son engagement et d'un refus illégal de le réintégrer, il est en droit de demander à être indemnisé du préjudice résultant de la chance sérieuse qu'il a perdue, pour la période en cause, de bénéficier des vacations horaires mentionnées à l'article 11 de la loi n° 96-370 du 3 mai 1996.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
