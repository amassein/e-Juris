<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042143082</ID>
<ANCIEN_ID>JG_L_2020_07_000000427042</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/14/30/CETATEXT000042143082.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 22/07/2020, 427042</TITRE>
<DATE_DEC>2020-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>427042</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP OHL, VEXLIARD</AVOCATS>
<RAPPORTEUR>Mme Fanélie Ducloz</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier  Fuchs</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:427042.20200722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 14 janvier, 15 avril et 21 octobre 2019 au secrétariat du contentieux du Conseil d'Etat, M. A... B... et la SELAFA MJA, agissant en qualité de mandataire liquidateur de la société Signatures, demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision de la commission des sanctions de l'Autorité des marchés financiers du 13 novembre 2018 en tant qu'elle a prononcé à l'encontre de M. B... une sanction pécuniaire de 50 000 euros et une interdiction d'exercer l'activité d'intermédiaire en biens divers pendant dix ans, et à l'encontre de  la société Signatures une interdiction d'exercer l'activité d'intermédiaire en biens divers pendant dix ans, et qu'elle a ordonné la publication de cette décision sur le site internet de l'Autorité des marchés financiers pour une durée de cinq ans ; <br/>
<br/>
              2°) de mettre à la charge de l'Autorité des marchés financiers la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Ils soutiennent que la décision de la commission des sanctions de l'Autorité des marchés financiers qu'ils attaquent :<br/>
              - est entachée d'une erreur de droit et d'une erreur de qualification juridique en ce qu'elle les qualifie d'intermédiaires en biens divers au sens de l'article L. 550-1 du code monétaire et financier, alors qu'ils ne peuvent être regardés comme ayant, d'une part, assuré la gestion des droits acquis par leurs clients sur les oeuvres d'art, d'autre part, proposé à un ou des clients potentiels d'acquérir des droits sur les oeuvres d'art par voie de publicité et de communication à caractère promotionnel ;<br/>
              - a retenu à tort que la communication à caractère promotionnel diffusée à un ou des clients potentiels n'était ni inexacte ni trompeuse ;<br/>
              - est insuffisamment motivée quant aux sanctions prononcées ;<br/>
              - prononce des sanctions disproportionnées.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code monétaire et financier ;<br/>
              - la loi n° 2003-706 du 1er août 2003 ;<br/>
              - la loi n° 2014-344 du 17 mars 2014 ;<br/>
              - le règlement général de l'Autorité des marchés financiers ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Fanélie Ducloz, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Olivier Fuchs, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de M. B... et autre, et à la SCP Ohl, Vexliard, avocat de l'Autorité des marchés financiers ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article L. 550-1 du code monétaire et financier, devenu l'article L. 551-1 du même code, dans sa rédaction issue de la loi du 1er août 2003 de sécurité financière : " Est soumise aux dispositions des articles L. 550-2, L. 550-3, L. 550-4, L. 550-5 et L. 573-8 : / 1. Toute personne qui, directement ou indirectement, par voie de publicité ou de démarchage, propose à titre habituel à des tiers de souscrire des rentes viagères ou d'acquérir des droits sur des biens mobiliers ou immobiliers lorsque les acquéreurs n'en assurent pas eux-mêmes la gestion ou lorsque le contrat offre une faculté de reprise ou d'échange et la revalorisation du capital investi ; / 2. Toute personne qui recueille des fonds à cette fin ; / 3. Toute personne chargée de la gestion desdits biens. / Ces articles ne s'appliquent pas aux opérations déjà régies par des dispositions particulières et notamment aux opérations d'assurance et de capitalisation régies par le code des assurances, aux opérations de crédit différé, aux opérations régies par le code de la mutualité et par le code de la sécurité sociale, aux opérations donnant normalement droit à l'attribution en propriété ou en jouissance de parties déterminées d'un ou plusieurs immeubles bâtis. / Les personnes mentionnées au présent article sont soumises aux dispositions des articles L. 341-1 à L. 341-17 et L. 353-1 à L. 353-5 lorsqu'elles agissent par voie de démarchage ". Aux termes du même article, dans sa rédaction issue de la loi du 17 mars 2014 relative à la consommation : " I.- Est un intermédiaire en biens divers : / 1° Toute personne qui, directement ou indirectement, par voie de communication à caractère promotionnel ou de démarchage, propose à titre habituel à un ou plusieurs clients ou clients potentiels de souscrire des rentes viagères ou d'acquérir des droits sur des biens mobiliers ou immobiliers lorsque les acquéreurs n'en assurent pas eux-mêmes la gestion ou lorsque le contrat leur offre une faculté de reprise ou d'échange et la revalorisation du capital investi ; / 2° Toute personne qui recueille des fonds à cette fin ; / 3° Toute personne chargée de la gestion desdits biens. (...) III.- Les communications à caractère promotionnel portant sur les propositions mentionnées aux I et II adressées à des clients ou des clients potentiels : / 1° Sont clairement identifiables en tant que telles ; / 2° Présentent un contenu exact, clair et non trompeur ; / 3° Permettent raisonnablement de comprendre les risques afférents au placement. / IV.- Sans préjudice des compétences de l'autorité administrative chargée de la concurrence et de la consommation mentionnée à l'article L. 141-1 du code de la consommation, l'Autorité des marchés financiers peut se faire communiquer tous documents, quel qu'en soit le support, afin de s'assurer de la conformité des propositions mentionnées aux I et II du présent article aux dispositions relevant du présent titre. / V.- Les personnes mentionnées au I du présent article sont soumises aux articles L. 550-2, L. 550-3, L. 550-4, L. 550-5 et L. 573-8 du présent code. (...) ". Aux termes de l'article L. 550-2 du même code, dans sa rédaction alors en vigueur : " Seules des sociétés par actions peuvent, à l'occasion des opérations mentionnées à l'article L. 550-1, recevoir des sommes correspondant aux souscriptions des acquéreurs ou aux versements des produits de leurs placements. Ces sociétés doivent justifier, avant toute publicité ou démarchage, qu'elles disposent d'un capital intégralement libéré d'un montant au moins égal à celui exigé par l'article L. 224-2 du code de commerce ". L'article L. 550-3 du même code, dans sa rédaction alors en vigueur, précise les conditions dans lesquelles un document destiné à donner toute information utile au public sur l'opération proposée par l'intermédiaire en biens divers, sur la personne qui en a pris l'initiative et sur le gestionnaire, est établi préalablement à toute publicité, à toute communication à caractère promotionnel, ou à tout démarchage et impose, notamment, le dépôt, auprès de l'Autorité des marchés financiers, des projets de documents d'information et de contrat type afin que cette Autorité exerce son contrôle auprès de l'ensemble des entreprises participant à l'opération et détermine si celle-ci présente le minimum de garanties exigé d'un placement destiné au public. Enfin, l'article L. 550-4 du même code dans sa rédaction alors en vigueur impose au gestionnaire, à la clôture de chaque exercice annuel, d'établir, outre ses propres comptes, contrôlés et certifiés par un commissaire aux comptes désigné dans les conditions prévues par l'article L. 550-5 du même code, l'inventaire des biens dont il assure la gestion ainsi qu'un rapport sur son activité et la gestion des biens, et de dresser l'état des sommes perçues au cours de l'exercice pour le compte des titulaires de droits, le bilan, le compte de résultat et l'annexe, l'ensemble de ces documents étant transmis l'Autorité des marchés financiers dans les trois mois suivant la clôture de l'exercice.<br/>
<br/>
              2. Il résulte de l'instruction que la société par actions simplifiées Signatures, anciennement Artecosa, dont l'activité principale est l'achat et la vente de lettres, manuscrits, documents autographes et photographies anciennes, et son dirigeant, M. B..., proposaient à des clients, dans le cadre de cette activité, entre le 1er janvier 2014 et le 29 février 2016, " d'acquérir une oeuvre d'art ou une collection d'oeuvres d'art dans des conditions permettant de procéder à un investissement intéressant ". A cette fin, et au terme d'un ensemble contractuel formé de " conditions générales de vente ", d'un " contrat de vente assorti d'un contrat de garde " et d'un " bon de commande ", la société Signatures s'engageait à constituer sous soixante à quatre-vingt-dix jours et à céder à son client une collection d'oeuvres pour un prix fixé dans le contrat de vente, incluant des frais de garde, des frais d'expertise et des frais d'assurance. L'article III du contrat de vente assorti d'un contrat de garde stipulait que la garde des oeuvres d'art ainsi acquises pouvait être confiée à la société Signatures pour une durée d'une année renouvelable par tacite reconduction. Enfin, jusqu'au 31 décembre 2015, les contrats de vente comportaient une " promesse de vente " selon laquelle la société Signatures pouvait racheter la collection cédée au client au terme du contrat de garde à un prix égal au prix de vente " majoré de 7,5 % par année de garde et de conservation si le dépôt a une durée au moins de 5 années pleines et entières ". La société Signatures commercialisait cette offre auprès de conseillers en gestion du patrimoine et de conseillers en investissement financier. Le secrétaire général de l'Autorité des marchés financiers a, en application de l'article L. 621-9 du code monétaire et financier, diligenté, le 11 avril 2016, une procédure de contrôle du respect, par la société Signatures, des règles applicables aux intermédiaires en biens divers. Sur le fondement de l'article L. 621-15 du code monétaire et financier, le président de l'Autorité des marchés financiers a notifié, le 13 mars 2017, des griefs à la société Signatures et à M. B..., tirés de la méconnaissance des obligations applicables aux intermédiaires en biens divers résultant des articles L. 550-1 à L. 550-5 du code monétaire et financier, dans leur rédaction alors applicable. Le 13 novembre 2018, la commission des sanctions de l'Autorité des marchés financiers a prononcé à l'encontre de la société Signatures une interdiction d'exercer l'activité d'intermédiaire en biens divers pendant dix ans et, à l'encontre de M. B..., une sanction pécuniaire de 50 000 euros ainsi qu'une interdiction d'exercer l'activité d'intermédiaire en biens divers pendant dix ans. Elle a, en outre, décidé de publier cette décision sur le site internet de l'Autorité des marchés financiers. M. B... et la SELAFA MJA, agissant en qualité de mandataire liquidateur de la société Signatures, demandent au Conseil d'Etat l'annulation de cette décision, le président de l'Autorité des marchés financiers demandant, par un recours incident, de la réformer en portant la sanction pécuniaire infligée à M. B... à 150 000 euros. <br/>
<br/>
              Sur le bien-fondé de la décision attaquée :<br/>
<br/>
              3. La décision attaquée retient la qualité d'intermédiaires en biens divers de la société Signatures et de M. B..., et leur reproche notamment d'avoir manqué à leur obligation d'une communication à caractère promotionnel exacte et non trompeuse en application du 2° du III de l'article L. 550-1 du code monétaire et financier.<br/>
<br/>
              En ce qui concerne la qualité d'intermédiaire en biens divers :<br/>
<br/>
              4. La décision attaquée a retenu la qualité d'intermédiaire en biens divers de la société Signatures et de M. B... en ce qu'ils ont proposé, à titre habituel, directement ou indirectement, par voie de publicité et de communication à caractère promotionnel, l'acquisition de droits de propriété sur des oeuvres d'art dont les acquéreurs n'assuraient pas eux-mêmes la gestion. <br/>
<br/>
              S'agissant de la gestion des droits acquis sur les oeuvres d'art :<br/>
<br/>
              5. Il résulte de l'instruction, en particulier des conditions générales de vente et des brochures " Artecosa référent sur le marché de l'art " et " L'art investit le patrimoine " réalisées par la société Signatures, que celle-ci choisissait elle-même les oeuvres d'art constituant la collection du client, qu'elle les soumettait au préalable à une expertise et qu'elle s'engageait, dans le cadre de l'exécution du contrat de garde, à valoriser les oeuvres d'art ainsi acquises en les exposant lors de manifestations culturelles organisées par elle ou par des tiers. En outre, en application de l'article I du contrat de vente assorti d'un contrat de garde, l'acheteur n'assurait lui-même la gestion de sa collection que s'il en conservait la garde et, en application de l'article IV du même contrat, l'acheteur ne pouvait disposer de sa collection que pour une période de quinze jours s'il en confiait la garde à la société Signatures. Il résulte également de l'instruction que l'essentiel des acheteurs confiaient leurs oeuvres à la société Signature afin qu'elle en assure la gestion. Ainsi, contrairement à ce qui est soutenu, cette société ne se bornait pas à acheter des oeuvres d'art pour le compte de ses clients et à les garder, mais les choisissait, les expertisait et les exploitait en vue de leur valorisation culturelle, dans l'objectif de dégager, à terme, une plus-value au profit de ses clients. Dès lors, en estimant que l'activité de la société Signatures et de son dirigeant consistait à proposer à des investisseurs potentiels, à titre habituel, d'acquérir des droits sur des biens mobiliers dont les acquéreurs n'assuraient pas eux-mêmes la gestion, et qu'elle entrait ainsi dans le champ de l'article L. 550-1 du code monétaire et financier, devenu L. 551-1 du même code, et était soumise aux obligations résultant des articles L. 550-2, L. 550-3, L. 550-4 et L. 550-5 de ce code, la commission des sanctions a fait une exacte application de ces dispositions. <br/>
<br/>
              6. Sont sans incidence à cet égard, d'une part, l'absence formelle d'un contrat de gestion, lequel n'est pas exigé par les dispositions du code et n'empêche pas la caractérisation d'actes de gestion, d'autre part, la circonstance que le contrat de garde pouvait être résilié à tout moment, laquelle n'est pas de nature à empêcher l'accomplissement d'actes de gestion jusqu'à la résiliation du contrat. Les requérants ne sauraient, enfin, utilement se prévaloir, pour soutenir que les dispositions de l'article L. 550-1 du code monétaire et financier dans leur version alors en vigueur ne leur étaient pas applicables, du communiqué de presse de l'Autorité des marchés financiers du 26 mars 2014 relatif à une autre société dont il n'est pas établi qu'elle aurait une activité similaire à celle de la société Signatures. <br/>
<br/>
S'agissant de la proposition d'acquérir des droits sur des biens divers par voie de publicité et de communication à caractère promotionnel :<br/>
<br/>
              7. En premier lieu, il résulte de l'instruction que la brochure intitulée " L'art investit le patrimoine " réalisée par la société Signatures indique que l'opération qu'elle propose porte sur des oeuvres d'art, en particulier des lettres, des manuscrits et des photographies, qu'elle a pour objectif " la constitution d'un patrimoine au même titre qu'un autre investissement " et est " un investissement alternatif par rapport aux produits généralement proposés " et que, dans le cadre de cette opération, la société Signatures achètera des oeuvres d'art pour le compte de ses clients et les valorisera en organisant ou en participant à des expositions culturelles. Cette brochure mentionne, en outre, sous forme de tableaux pour chaque catégorie d'oeuvres d'art, leur prix d'achat en 1993, leur prix d'achat en 2013 et leur évolution annuelle en pourcentage, laquelle est toujours positive. Elle invite enfin les investisseurs potentiels intéressés par l'offre à se mettre en relation avec la société Signatures. Cette brochure ne se borne en conséquence pas, contrairement à ce qui est soutenu, à exposer en des termes généraux et neutres les règles du courtage en art, mais vise à promouvoir, à des fins commerciales, auprès d'investisseurs potentiels, l'offre de la société Signatures dont elle présente les caractéristiques principales. Par suite, la commission des sanctions n'a pas méconnu l'article L. 550-1 du code monétaire et financier, dans sa version alors applicable, en retenant que cette brochure devait être regardée comme une publicité et une communication à caractère promotionnel au sens de ses dispositions. Les requérants ne sauraient utilement faire état, sur ce point, d'une décision de la cour d'appel de Paris du 11 janvier 2018, laquelle n'a pas d'autorité de chose jugée dans le présent litige et ne statue au demeurant pas sur le point de savoir si la brochure " L'art investit le patrimoine " a le caractère d'une publicité ou d'une communication à caractère promotionnel au sens de l'article L. 550-1 du code monétaire et financier.<br/>
<br/>
              8. En second lieu, si les requérants font valoir, pour soutenir que les dispositions de l'article L. 550-1 du code monétaire et financier dans leur version en vigueur ne leur étaient pas applicables, qu'ils ont transmis la brochure " L'art investit le patrimoine " non à des investisseurs potentiels mais à des conseillers en gestion du patrimoine et à des conseillers en investissement financier, les dispositions de cet article visent les transmissions directes comme les transmissions indirectes. Il résulte en l'espèce de l'instruction que la transmission de cette brochure à des conseillers en gestion du patrimoine et à des conseillers en investissement financier était faite par la société Signatures à des fins commerciales et à l'attention d'investisseurs potentiels. <br/>
<br/>
              En ce qui concerne le manquement à l'obligation d'une communication à caractère promotionnel exacte et non trompeuse : <br/>
<br/>
              9. La brochure " L'art investit le patrimoine " réalisée par la société Signatures mentionnait que l'expertise des oeuvres d'art était réalisée " soit en interne, soit par des experts indépendants de renom, chacun dans leur domaine de compétence ", que la société disposait " de toutes les garanties nécessaires pour satisfaire la confiance de nos clients et de nos partenaires ", en particulier d'une " garantie bancaire ", et qu'elle valorisait les oeuvres d'art en participant " à des expositions temporaires dans des lieux prestigieux ". Toutefois, il résulte de l'instruction que les expertises des oeuvres d'art ont été réalisées exclusivement par des salariés de la société Signatures, que cette société ne disposait d'aucune garantie bancaire, l'existence alléguée d'un compte séquestre sur lequel était porté un pourcentage de son capital social et d'une assurance responsabilité civile professionnelle ne pouvant être regardée comme une garantie bancaire, laquelle a pour objet de substituer une banque au débiteur lorsque celui-ci se révèle défaillant, et que, pendant la période en litige, aucune exposition des oeuvres d'art dans des lieux prestigieux n'a été organisée. Par suite, en retenant que l'information contenue dans la brochure " L'art investit le patrimoine ", transmise aux investisseurs potentiels, avait un caractère trompeur et inexact et que la société Signatures et M. B... avaient en conséquence manqué, en leur qualité d'intermédiaire en biens divers, à l'obligation pesant sur eux d'une information exacte et non trompeuse en application des dispositions du 2° du III de l'article L. 550-1 du code monétaire et financier, la commission des sanctions n'a pas commis d'erreur d'appréciation.<br/>
<br/>
              En ce qui concerne les sanctions prononcées :<br/>
<br/>
              10. Aux termes du III de l'article L. 621-15 du code monétaire et financier dans sa rédaction applicable aux sanctions en litige : " Les sanctions applicables sont : / a) Pour les personnes mentionnées aux 1° à 8°, 11°, 12°, 15° à 17° du II de l'article L. 621-9, l'avertissement, le blâme, l'interdiction à titre temporaire ou définitif de l'exercice de tout ou partie des services fournis, la radiation du registre mentionné à l'article L. 546-1 ; la commission des sanctions peut prononcer soit à la place, soit en sus de ces sanctions une sanction pécuniaire dont le montant ne peut être supérieur à 100 millions d'euros ou au décuple du montant des profits éventuellement réalisés ; les sommes sont versées au fonds de garantie auquel est affiliée la personne sanctionnée ou, à défaut, au Trésor public ". Il résulte également des dispositions du même article que le montant de la sanction doit être fixé en fonction de la gravité et de la durée des manquements commis, des avantages ou des profits éventuellement tirés de ces manquements et de la situation et de la capacité financières de la personne en cause, au vu notamment de son patrimoine et, s'agissant d'une personne physique, de ses revenus annuels.<br/>
<br/>
              11. Les manquements ont consisté, pour la société Signatures et M. B..., son dirigeant, à s'affranchir de l'ensemble des obligations pesant sur les intermédiaires en biens divers, dont celle de transmettre à l'Autorité des marchés financiers, préalablement à toute publicité et à toute communication promotionnelle, un document destiné à donner toute information utile au public sur l'opération proposée, sur la personne qui en a pris l'initiative et sur le gestionnaire, privant ainsi l'Autorité de la possibilité d'exercer son contrôle et de prévenir la diffusion, auprès des investisseurs, d'informations inexactes ou trompeuses. Les manquements ont également consisté à diffuser, par voie de communication à caractère promotionnel, des informations inexactes et trompeuses portant notamment sur des éléments déterminants de l'offre, tels que la qualité des personnes chargées d'expertiser les oeuvres acquises, leur valorisation par la société Signatures et l'existence d'une garantie bancaire, ces informations inexactes et trompeuses étant destinées à encourager les investisseurs potentiels à souscrire à l'offre. Ces manquements, qui revêtent une particulière gravité, se sont échelonnés sur plus de deux ans et le montant des sommes investies s'élève, pour la période en litige, à 25 millions d'euros. <br/>
<br/>
              12. Il y a lieu, compte tenu de l'ensemble de ces circonstances et des ressources dont a fait état M. B... devant la commission des sanctions, sans apporter d'autres éléments dans le cadre de l'instruction devant le Conseil d'Etat, de porter le montant de la sanction pécuniaire qui lui a été infligée à 100 000 euros. Cette sanction s'ajoute à l'interdiction d'exercer l'activité d'intermédiaire en biens divers pendant une durée de dix ans, prononcée à son encontre et à l'encontre de la société Signatures par la décision, suffisamment motivée sur ce point, de la commission des sanctions et qui, dans les circonstances de l'espèce, n'a pas un caractère disproportionné.<br/>
<br/>
              13. Il y a lieu, dans les circonstances de l'espèce, d'ordonner la publication de la présente décision sur le site internet de l'Autorité des marchés financiers, dans les mêmes conditions que la décision de la commission des sanctions.<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              14. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de l'Autorité des marchés financiers qui n'est pas, dans la présente instance, la partie perdante. Il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de M. B... et de la SELAFA MJA, en sa qualité de mandataire liquidateur de la société Signatures, une somme de 1 500 euros chacun à verser à l'Autorité des marchés financiers, au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La sanction pécuniaire prononcée à l'encontre de M. B... est portée à 100 000 euros.<br/>
Article 2 : La décision de la commission des sanctions de l'Autorité des marchés financiers est réformée en ce qu'elle a de contraire à la présente décision. La présente décision sera publiée sur le site internet de l'Autorité des marchés financiers, dans les mêmes conditions que la décision de la commission des sanctions.<br/>
Article 3 : La requête de M. B... et de la SELAFA MJA, agissant en qualité de mandataire liquidateur de la société Signatures, et le surplus des conclusions du recours incident du président de l'Autorité des marchés financiers sont rejetés.<br/>
Article 4 : M. B... et la SELAFA MJA verseront chacun à l'Autorité des marchés financiers une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : La présente décision sera notifiée à M. A... B..., à la SELAFA MJA et à l'Autorité des marchés financiers.<br/>
Copie en sera adressée au ministre de l'économie, des finances et de la relance.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">13-01-02-01 CAPITAUX, MONNAIE, BANQUES. CAPITAUX. OPÉRATIONS DE BOURSE. AUTORITÉ DES MARCHÉS FINANCIERS. - INTERMÉDIAIRES EN BIENS DIVERS (ART. L. 550-1 DU CMF) [RJ1] - NOTION DE COMMUNICATION À CARACTÈRE PROMOTIONNEL OU DE DÉMARCHAGE - ILLUSTRATION.
</SCT>
<ANA ID="9A"> 13-01-02-01 Brochure intitulée L'art investit le patrimoine réalisée par la société mise en cause indiquant que l'opération qu'elle propose porte sur des oeuvres d'art, en particulier des lettres, des manuscrits et des photographies, qu'elle a pour objectif la constitution d'un patrimoine au même titre qu'un autre investissement et est un investissement alternatif par rapport aux produits généralement proposés et que, dans le cadre de cette opération, la société achètera des oeuvres d'art pour le compte de ses clients et les valorisera en organisant ou en participant à des expositions culturelles. Brochure mentionnant, en outre, sous forme de tableaux pour chaque catégorie d'oeuvres d'art, leur prix d'achat en 1993, leur prix d'achat en 2013 et leur évolution annuelle en pourcentage, laquelle est toujours positive, et invitant enfin les investisseurs potentiels intéressés par l'offre à se mettre en relation avec la société.... ,,Cette brochure ne se borne en conséquence pas à exposer en des termes généraux et neutres les règles du courtage en art, mais vise à promouvoir, à des fins commerciales, auprès d'investisseurs potentiels, l'offre de la société mise en cause dont elle présente les caractéristiques principales. Cette brochure constitue donc une publicité et une communication à caractère promotionnel au sens de l'article L. 550-1 du code monétaire et financier (CMF).</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., sur cette notion, CE, 27 juillet 2016, M.,et autres, n°s 381019 et autres, T. p. 653.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
