<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032883011</ID>
<ANCIEN_ID>JG_L_2016_07_000000391262</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/88/30/CETATEXT000032883011.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 11/07/2016, 391262, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-07-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>391262</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOUTET, HOURDEAUX ; SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>M. Camille Pascal</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:391262.20160711</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Marseille d'annuler pour excès de pouvoir l'arrêté du 17 décembre 2009 par lequel le préfet des Alpes-de-Haute-Provence a déclaré la cessibilité d'immeubles situés sur le territoire de la commune de Méailles en vue de la réalisation d'un aménagement de sécurité dans le centre du village. Par un jugement n° 1002150 du 6 avril 2012, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 12MA02803 du 24 avril 2015, la cour administrative d'appel de Marseille a rejeté l'appel formé par M. A...contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 23 juin et 21 septembre 2015 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet  arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond de faire droit à son appel ;<br/>
<br/>
              3°) de mettre solidairement à la charge de l'Etat et de la commune de Méailles la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'expropriation pour cause d'utilité publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Camille Pascal, conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boutet, Hourdeaux, avocat de M.A..., et la SCP Gaschignard, avocat de la commune de Méailles ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'aux termes de l'article L. 12-1 du code de l'expropriation pour cause d'utilité publique, en vigueur à la date de l'arrêté contesté : " Le transfert de propriété des immeubles ou de droits réels immobiliers est opéré par voie, soit d'accord amiable, soit d'ordonnance, (...) " ; qu'aux termes de l'article L. 12-2 du même code, alors en vigueur : " L'ordonnance d'expropriation éteint, par elle-même et à sa date, tous droits réels ou personnels existant sur les immeubles expropriés. (...) " ; qu'aux termes de l'article L. 12-5 de ce même code, alors applicable : " (...) En cas d'annulation par une décision définitive du juge administratif de la déclaration d'utilité publique ou de l'arrêté de cessibilité, tout exproprié peut faire constater par le juge de l'expropriation que l'ordonnance portant transfert de propriété est dépourvue de base légale " ; qu'enfin aux termes de l'article R. 12-5-4 du même code, en vigueur à la date de l'arrêté contesté  : " Le juge constate, par jugement, l'absence de base légale du transfert de propriété et en précise les conséquences de droit. a) Si le bien exproprié n'est pas en état d'être restitué, l'action de l'exproprié se résout en dommages et intérêts ; b) S'il peut l'être, le juge désigne chaque immeuble ou fraction d'immeuble dont la propriété est restituée " ;<br/>
<br/>
              2.	Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par deux arrêtés du 30 janvier 2004, le préfet des Alpes-de-Haute-Provence a déclaré d'utilité publique l'acquisition d'immeubles pour la réalisation d'un alignement sur le territoire de la commune de Méailles et a déclaré cessible au profit de la commune de Méailles une partie de la parcelle appartenant à M. A...; que l'expropriation de ce terrain a été prononcée au profit de la commune par une ordonnance du 4 juin 2007 du juge de l'expropriation ; que la cour administrative d'appel de Marseille a, par un arrêt du 24 novembre 2008, annulé les arrêtés déclarant l'opération d'utilité publique et la cessibilité de la parcelle litigieuse ; que, par deux nouveaux arrêtés du 17 décembre 2009, le préfet des Alpes-de-Haute-Provence a déclaré d'utilité publique l'acquisition d'immeubles en vue de la réalisation d'un aménagement sur le territoire de la commune de Méailles et déclaré la cessibilité des deux parcelles issues du démembrement de la parcelle initialement expropriée ; que le juge de l'expropriation, saisi par M. A...sur le fondement des dispositions du second alinéa de l'article L. 12-5 du code de l'expropriation pour cause d'utilité publique, a constaté, par une ordonnance du 7 janvier 2010, que son ordonnance d'expropriation du 4 juin 2007 était privée de base légale et que rien ne faisait obstacle à ce que soit ordonnée la restitution de la parcelle à son ancien propriétaire ; que M. A...se pourvoit en cassation contre l'arrêt de la cour administrative d'appel de Marseille du 24 avril 2015 rejetant son appel formé contre le jugement du tribunal administratif de Marseille du 6 avril 2012 rejetant sa demande tendant à l'annulation pour excès de pouvoir de l'arrêté de cessibilité du 17 décembre 2009 ;<br/>
<br/>
              3.	Considérant qu'en jugeant, après avoir relevé les faits rappelés ci-dessus, que l'autorité expropriante pouvait reprendre un arrêté de cessibilité portant sur les parcelles litigieuses alors même que la décision du juge de l'expropriation, saisi par M. A...d'une demande de restitution de ces biens, n'était pas encore intervenue et que les droits et garanties dont pouvait se prévaloir M. A...en vertu du code de l'expropriation pour cause d'utilité publique n'avaient pas été méconnus, la cour, qui a suffisamment motivé son arrêt, n'a pas commis d'erreur de droit ;<br/>
<br/>
              4.	Considérant qu'en jugeant que les variantes proposées par M. A...ne permettaient pas de réaliser l'opération dans des conditions équivalentes sans recourir à l'expropriation, la cour a porté sur les faits qui lui étaient soumis une appréciation souveraine exempte de dénaturation ;<br/>
<br/>
              5.	Considérant qu'il résulte de tout ce qui précède que M. A...n'est pas fondé à demander l'annulation de l'arrêt attaqué ; <br/>
<br/>
              6.	Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de la commune de Méailles qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. A...la somme de 3 000 euros à verser à la commune de Méailles au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A...est rejeté.<br/>
<br/>
Article 2 : M. A...versera à la commune de Méailles une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. B...A..., à la commune de Méailles et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
