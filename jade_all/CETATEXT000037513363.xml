<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037513363</ID>
<ANCIEN_ID>JG_L_2018_10_000000413592</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/51/33/CETATEXT000037513363.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 4ème chambres réunies, 22/10/2018, 413592</TITRE>
<DATE_DEC>2018-10-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>413592</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; CARBONNIER</AVOCATS>
<RAPPORTEUR>M. Frédéric Pacoud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:413592.20181022</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...B...et M. C...D...ont demandé au tribunal administratif de Paris d'annuler la décision du 25 mai 2016 par laquelle le président du conseil de Paris a, sur le recours administratif formé par MmeB..., confirmé la décision de la caisse d'allocations familiales de Paris du 5 septembre 2016 ayant réduit le montant de son allocation de revenu de solidarité active à compter du 1er septembre 2015. <br/>
<br/>
              Par un jugement n° 1610323 du 22 juin 2017, le tribunal administratif de Paris a annulé la décision du 25 mai 2016 du président du conseil de Paris et renvoyé Mme B...devant celui-ci pour qu'il procède, conformément aux motifs du jugement, à la détermination de ses droits à l'allocation de revenu de solidarité active depuis septembre 2015. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 22 août et 21 novembre 2017 au secrétariat du contentieux du Conseil d'Etat, le département de Paris demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de Mme B...et M.D....<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Pacoud, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat du département de Paris et à Me Carbonnier, avocat de Mme A...B....<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que Mme A...B..., allocataire du revenu de solidarité active en tant que personne seule depuis le mois d'octobre 2013, a déclaré le 28 juillet 2015 à la caisse d'allocations familiales de Paris qu'elle vivait en couple depuis le 8 juillet 2015 avec M. C...D..., de nationalité colombienne. Par un courrier du 5 septembre 2015, confirmé le 25 mai 2016 par une décision du président du conseil de Paris prise sur recours administratif de MmeB..., la caisse d'allocations familiales de Paris l'a informée d'une diminution en conséquence du montant mensuel de son allocation de revenu de solidarité active à compter du 1er septembre 2015 au motif que, si les ressources du foyer devaient inclure les revenus de son concubin, celui-ci ne pouvait en revanche être pris en compte au titre de ses droits à cette allocation car il n'était pas titulaire depuis au moins cinq ans d'un titre de séjour l'autorisant à travailler. Le département de Paris se pourvoit en cassation contre le jugement par lequel le tribunal administratif de Paris a annulé la décision du 25 mai 2016 du président du conseil de Paris et renvoyé Mme B...devant celui-ci pour qu'il procède à la détermination de ses droits à l'allocation de revenu de solidarité active depuis septembre 2015, pour la durée de la période en litige.<br/>
<br/>
              2. En vertu de l'article L. 262-1 du code de l'action sociale et des familles, le revenu de solidarité active a pour objet d'assurer à ses bénéficiaires des moyens convenables d'existence, de lutter contre la pauvreté et de favoriser l'insertion sociale et professionnelle. Le premier alinéa de l'article L. 262-2 du même code dispose que : " Toute personne résidant en France de manière stable et effective, dont le foyer dispose de ressources inférieures " à un certain montant, " a droit au revenu de solidarité active dans les conditions définies au présent chapitre ". Aux termes de l'article L. 262-4 de ce code : " Le bénéfice du revenu de solidarité active est subordonné au respect, par le bénéficiaire, des conditions suivantes : / (...) 2° Etre français ou titulaire, depuis au moins cinq ans, d'un titre de séjour autorisant à travailler. Cette condition n'est pas applicable : / a) Aux réfugiés, aux bénéficiaires de la protection subsidiaire, aux apatrides et aux étrangers titulaires de la carte de résident ou d'un titre de séjour prévu par les traités et accords internationaux et conférant des droits équivalents ; (...) ". Le premier alinéa de l'article L. 262-5 du même code prévoit que : " Pour être pris en compte au titre des droits du bénéficiaire, le conjoint, concubin ou partenaire lié par un pacte civil de solidarité du bénéficiaire doit remplir les conditions mentionnées aux 2° et 4° de l'article L. 262-4 ". Il résulte de ces dispositions que le revenu de solidarité active a notamment pour objet de favoriser l'insertion professionnelle et que le législateur a estimé que la stabilité de la présence sur le territoire national, dans une situation l'autorisant à occuper un emploi, du demandeur de cette prestation, était de nature à contribuer à cet objectif. Il a ainsi subordonné le bénéfice du revenu de solidarité active pour les étrangers, sous réserve de certaines exceptions, à une condition de détention d'un titre de séjour autorisant à travailler depuis au moins cinq ans à la date de la demande. Si cette période doit être continue, le respect de cette condition ne saurait toutefois être affecté en principe par une interruption correspondant à la durée nécessaire à l'examen d'une demande de renouvellement ou d'obtention d'un nouveau titre de séjour permettant l'exercice d'une activité professionnelle.<br/>
<br/>
              3. Le tribunal administratif de Paris a relevé qu'il résultait de l'instruction, notamment de l'attestation du préfet de police du 2 février 2017, que M.D..., de nationalité colombienne, entré en France le 27 janvier 2007, avait été titulaire du 21 février 2007 au 19 septembre 2014 de titres de séjour mention " étudiant ", lesquels donnent droit à l'exercice, à titre accessoire, d'une activité professionnelle salariée en vertu de l'article L. 313-7 du code de l'entrée et du séjour des étrangers et du droit d'asile, puis d'un titre de séjour mention " profession libérale " valable du 3 décembre 2014 au 26 mars 2016. Il ressort des pièces du dossier soumis au juge du fond que si M. D... n'établissait pas avoir été titulaire d'un titre de séjour l'autorisant à travailler entre le 20 septembre et le 2 décembre 2014, cette interruption correspondait à la durée nécessaire à l'examen de sa demande de changement de statut, en vue de l'exercice d'une activité non salariée, à laquelle il a été fait droit. Par suite, en jugeant que M. D... remplissait au 1er septembre 2015 la condition, posée à l'article L. 262-4 du code de l'action sociale et des familles, de cinq années de détention d'un titre de séjour autorisant à travailler, le tribunal administratif, qui n'a pas dénaturé les pièces du dossier, n'a pas commis d'erreur de droit. <br/>
<br/>
              4. Il résulte de ce qui précède que le département de Paris n'est pas fondé à demander l'annulation du jugement du tribunal administratif de Paris qu'il attaque. <br/>
<br/>
              5. Mme B...ayant obtenu le bénéfice de l'aide juridictionnelle, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce, et sous réserve que Me Carbonnier, avocat de MmeB..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge du département de Paris une somme de 1 500 euros à verser à Me Carbonnier.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du département de Paris est rejeté. <br/>
Article 2 : Le département de Paris versera à Me Carbonnier, avocat de MmeB..., une somme de 1 500 euros en application des dispositions des articles 37 de la loi du 10 juillet 1991 et L. 761-1 du code de justice administrative, sous réserve que celui-ci renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 3 : La présente décision sera notifiée au département de Paris et à Mme A...B....<br/>
Copie en sera adressée à la ministre des solidarités et de la santé. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-02-06 AIDE SOCIALE. DIFFÉRENTES FORMES D'AIDE SOCIALE. REVENU MINIMUM D'INSERTION (RMI). - RSA - ETRANGER - CONDITION DE DÉTENTION D'UN TITRE DE SÉJOUR AUTORISANT À TRAVAILLER DEPUIS AU MOINS 5 ANS À LA DATE DE LA DEMANDE - OBLIGATION TENANT AU CARACTÈRE CONTINU DE CETTE DURÉE - EXISTENCE [RJ1] - EXCEPTION - INTERRUPTION TENANT À LA DURÉE NÉCESSAIRE À L'EXAMEN D'UNE DEMANDE DE RENOUVELLEMENT OU D'OBTENTION DU NOUVEAU TITRE DE SÉJOUR [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">335-01-02 ÉTRANGERS. SÉJOUR DES ÉTRANGERS. AUTORISATION DE SÉJOUR. - RSA - ETRANGER - CONDITION DE DÉTENTION D'UN TITRE DE SÉJOUR AUTORISANT À TRAVAILLER DEPUIS AU MOINS 5 ANS À LA DATE DE LA DEMANDE - OBLIGATION TENANT AU CARACTÈRE CONTINU DE CETTE DURÉE - EXISTENCE [RJ1] - EXCEPTION - INTERRUPTION TENANT À LA DURÉE NÉCESSAIRE À L'EXAMEN D'UNE DEMANDE DE RENOUVELLEMENT OU D'OBTENTION DU NOUVEAU TITRE DE SÉJOUR [RJ2].
</SCT>
<ANA ID="9A"> 04-02-06 En vertu de l'article L. 262-1 du code de l'action sociale et des familles (CASF), le revenu de solidarité active (RSA) a pour objet d'assurer à ses bénéficiaires des moyens convenables d'existence, de lutter contre la pauvreté et de favoriser l'insertion sociale et professionnelle. Il résulte du premier alinéa de l'article L. 262-2, de l'article L. 262-4 et du premier alinéa de l'article L. 262-5 du même code que le RSA a notamment pour objet de favoriser l'insertion professionnelle et que le législateur a estimé que la stabilité de la présence sur le territoire national, dans une situation l'autorisant à occuper un emploi, du demandeur de cette prestation, était de nature à contribuer à cet objectif. Il a ainsi subordonné le bénéfice du revenu de solidarité active pour les étrangers, sous réserve de certaines exceptions, à une condition de détention d'un titre de séjour autorisant à travailler depuis au moins cinq ans à la date de la demande. Si cette période doit être continue, le respect de cette condition ne saurait toutefois être affecté en principe par une interruption correspondant à la durée nécessaire à l'examen d'une demande de renouvellement ou d'obtention d'un nouveau titre de séjour permettant l'exercice d'une activité professionnelle.</ANA>
<ANA ID="9B"> 335-01-02 En vertu de l'article L. 262-1 du code de l'action sociale et des familles (CASF), le revenu de solidarité active (RSA) a pour objet d'assurer à ses bénéficiaires des moyens convenables d'existence, de lutter contre la pauvreté et de favoriser l'insertion sociale et professionnelle. Il résulte du premier alinéa de l'article L. 262-2, de l'article L. 262-4 et du premier alinéa de l'article L. 262-5 du même code que le RSA a notamment pour objet de favoriser l'insertion professionnelle et que le législateur a estimé que la stabilité de la présence sur le territoire national, dans une situation l'autorisant à occuper un emploi, du demandeur de cette prestation, était de nature à contribuer à cet objectif. Il a ainsi subordonné le bénéfice du revenu de solidarité active pour les étrangers, sous réserve de certaines exceptions, à une condition de détention d'un titre de séjour autorisant à travailler depuis au moins cinq ans à la date de la demande. Si cette période doit être continue, le respect de cette condition ne saurait toutefois être affecté en principe par une interruption correspondant à la durée nécessaire à l'examen d'une demande de renouvellement ou d'obtention d'un nouveau titre de séjour permettant l'exercice d'une activité professionnelle.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 10 juillet 2015,,, n° 375886, T. p. 553.,,[RJ2] Rappr. CE, 10 juillet 2015,,n° 375886, T. p. 553.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
