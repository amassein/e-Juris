<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036765345</ID>
<ANCIEN_ID>JG_L_2018_03_000000418711</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/76/53/CETATEXT000036765345.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 15/03/2018, 418711, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-03-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418711</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2018:418711.20180315</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 2 mars 2018 au secrétariat du contentieux du Conseil d'Etat, M. B...A...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) de suspendre l'exécution de l'arrêté du 29 novembre 2017 relatif à la commission d'exercice en pharmacie à usage intérieur mentionnée à l'article 7 du décret n° 2017-883 du 9 mai 2017 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - la condition d'urgence est remplie dès lors que l'arrêté contesté a pour effet de le priver d'accéder à l'emploi de pharmacien au sein d'une pharmacie à usage intérieur ;<br/>
              - il existe un doute sérieux quant à la légalité de l'arrêté contesté ;<br/>
              - il est entaché d'incompétence ;<br/>
              - il est entaché d'un vice de procédure ;<br/>
              - il a été pris sur le fondement du décret n° 2015-9 du 7 janvier 2015 relatif aux conditions d'exercice et de remplacement au sein des pharmacies à usage intérieur, modifié par décret n° 2017-73 en date du 9 mai 2017, qui méconnaît les dispositions de l'article L. 5126-3 du code de la santé publique en ce que le législateur a entendu limiter l'accès à la gérance d'une pharmacie à usage intérieur aux seuls pharmaciens sans autres conditions ;<br/>
              - il est porté atteinte à sa liberté fondamentale de travailler ;<br/>
              - l'arrêté contesté méconnaît les dispositions de l'article R. 5126-101-2 du même code en ce qu'il exige des candidats à l'exercice au sein d'une pharmacie à usage intérieur qu'ils justifient d'un exercice au sein d'une pharmacie à usage intérieur avant le 31 décembre 2015 et non d'un exercice de deux années avant le 1er juin 2017.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu :<br/>
- le code de la santé publique ;<br/>
- le décret n° 2017-883 du 9 mai 2017, notamment son article 7 ;<br/>
- le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". L'article L. 522-3 de ce code prévoit que le juge des référés peut rejeter une requête par une ordonnance motivée, sans instruction contradictoire ni audience publique, lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. M. A...demande la suspension de l'exécution de l'arrêté du 29 novembre 2017 relatif à la commission d'exercice en pharmacie à usage intérieur mentionnée à l'article 7 du décret n° 2017-883 du 9 mai 2017.<br/>
<br/>
              3. Aux termes de l'article 7 du décret du 9 mai 2017 modifiant les conditions d'exercice et de remplacement au sein des pharmacies à usage intérieur et les modalités d'organisation du développement professionnel continu des professions de santé, aux termes duquel : " I. - Les pharmaciens en exercice au sein d'une pharmacie à usage intérieur avant le 31 décembre 2015 et ne remplissant pas les conditions prévues aux articles R. 5126-101-1 à R. 5126-101-4, dans leur rédaction issue du présent décret peuvent présenter jusqu'au 31 décembre 2017 un dossier en vue d'obtenir une autorisation d'exercice en pharmacie à usage intérieur. Les dossiers sont examinés par une commission dont la composition et les modalités de fonctionnement sont définies par arrêté du ministre chargé de la santé. Cet arrêté fixe également la composition du dossier de demande (...) ". L'arrêté litigieux du 29 novembre 2017, pris en application de cet article, crée la commission chargée d'examiner les demandes d'exercice à titre dérogatoire et dispose, à son article 4, que : " Les candidats à l'autorisation d'exercice au sein d'une pharmacie à usage intérieur fournissent les pièces justificatives suivantes : / (...) 7° Une attestation, établie par le ou les employeurs, justifiant d'un exercice au sein d'une pharmacie à usage intérieur avant le 31 décembre 2015, indiquant les fonctions, le temps de travail et les périodes concernées ; (...) ".<br/>
<br/>
              4. Pour justifier de l'urgence à suspendre cet arrêté, le requérant se prévaut de ce que, par courrier du 27 décembre 2017, la commission créée par l'arrêté litigieux a refusé d'instruire, en l'état, sa demande tendant à être autorisé à exercer dans une pharmacie à usage intérieur en application de l'article 7 du décret du 9 mai 2017, au motif qu'il n'avait pas produit l'attestation prévue au 7° de l'article 4 de l'arrêté litigieux. Cette circonstance, qui trouve son origine dans les conditions posées par l'article 7 du décret du 9 mai 2017 et tient aux effets d'une décision prise en application de ce dernier, n'est pas de nature à justifier l'urgence à suspendre les dispositions contestées de l'arrêté litigieux, qui se bornent à tirer les conséquences de la condition prévue par l'article 7 du décret du 9 mai 2017. Il est ainsi manifeste que la requête ne remplit pas la condition d'urgence prévue par l'article L. 521-1 du code de justice administrative.<br/>
<br/>
              5. Il résulte de ce qui précède que la requête de M. A..., y compris les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative, doit être rejetée selon la procédure prévue à l'article L. 522-3 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. A... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. B... A....<br/>
Copie en sera adressée pour information à la ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
