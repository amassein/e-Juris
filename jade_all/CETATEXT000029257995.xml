<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029257995</ID>
<ANCIEN_ID>JG_L_2014_07_000000382145</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/25/79/CETATEXT000029257995.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 09/07/2014, 382145</TITRE>
<DATE_DEC>2014-07-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>382145</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2014:382145.20140709</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 3 juillet 2014 au secrétariat du contentieux du Conseil d'Etat, présentée pour M. C...A..., demeurant ...; le requérant demande au juge des référés du Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1405274 du 24 juin 2014 par laquelle le juge des référés du tribunal administratif de Nantes, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, a rejeté sa demande tendant, d'une part, à ce qu'il soit enjoint au consul général de France à Casablanca de lui délivrer un visa d'entrée et court séjour sur le territoire français dès la notification de l'ordonnance à intervenir, sous astreinte de 500 euros par jour de retard, ou subsidiairement de réexaminer sa demande dans un délai de 5 jours ;<br/>
<br/>
              2°) de faire droit à sa demande de première instance ou, subsidiairement, d'enjoindre au consul général de France à Casablanca de réexaminer sa demande dans un délai de 3 jours ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              il soutient que : <br/>
              - la condition d'urgence est remplie, dès lors que son mariage doit être célébré le 12 juillet 2014 ;<br/>
              - le consul général de France à Casablanca a porté une atteinte grave et manifestement illégale à la liberté du mariage et au droit au respect de la vie privée et familiale en lui refusant le visa qu'il avait sollicité ;<br/>
<br/>
<br/>
<br/>
                          Vu l'ordonnance attaquée ;<br/>
<br/>
              Vu le mémoire en défense, enregistré le 7 juillet 2014, présenté par le ministre de l'intérieur, qui conclut au rejet de la requête ; <br/>
              il soutient que :<br/>
              - il existe un risque de détournement de l'objet du visa sollicité, compte tenu des antécédents migratoires de M. A...;<br/>
              - aucun élément du dossier n'établit la sincérité de l'union du requérant et de son compagnon ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. A...et, d'autre part, le ministre de l'intérieur ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 8 juillet 2014 à 16 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Coudray, avocat au Conseil d'Etat et à la Cour de Cassation, avocat de M. A... ;<br/>
<br/>
              - les représentantes de M. A... ;<br/>
<br/>
              - le représentant du ministre de l'intérieur ; <br/>
              et à l'issue de laquelle le juge des référés a clôturé l'instruction ;<br/>
                          Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              Vu le code civil, notamment son article 171-9 issu de la loi n°2013-404 du 17 mai 2013 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme privé chargé d'une mission de service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures. " ; <br/>
<br/>
              Sur l'urgence :<br/>
<br/>
              2. Considérant que, sauf circonstances particulières, le refus des autorités consulaires de délivrer un visa d'entrée en France ne constitue pas une situation d'urgence caractérisée rendant nécessaire l'intervention dans les quarante-huit heures du juge des référés ; <br/>
<br/>
              3. Considérant toutefois que, ayant engagé dès l'été 2013 des démarches en vue de son mariage en France avec un ressortissant français, et déposé le 26 mai 2014 une demande de visa en vue de sa célébration le 12 juillet suivant en mairie de Creil, M. A...s'est vu refuser ce visa par une décision du consul général de France à Casablanca le 16 juin 2014 ; que, compte tenu de la proximité de la date du mariage, cette décision préjudicie de manière suffisamment grave et immédiate aux intérêts de M. A...pour que la condition d'urgence soit, en l'espèce, regardée comme remplie ;<br/>
<br/>
              Sur l'atteinte à une liberté fondamentale :<br/>
<br/>
              4. Considérant qu'aux termes de l'article 171-9 inséré dans le code civil par la loi du 17 mai 2013 ouvrant le mariage aux couples de personnes du même sexe : " par dérogation aux articles 74 et 165, lorsque les futurs époux de même sexe, dont l'un au moins a la nationalité française, ont leur domicile ou leur résidence dans un pays qui n'autorise pas le mariage entre deux personnes de même sexe et dans lequel les autorités diplomatiques et consulaires françaises ne peuvent procéder à sa célébration, le mariage est célébré publiquement par l'officier de l'état civil de la commune de naissance ou de la dernière résidence de l'un des époux ou de la commune dans laquelle l'un de leurs parents a son domicile ou sa résidence établie dans les conditions prévues à l'article 74. A défaut, le mariage est célébré par l'officier de l'état civil de la commune de leur choix. " ;<br/>
<br/>
              5. Considérant qu'il est constant que le mariage de M.A..., citoyen sénégalais, et de M.B..., ressortissant français, ne peut être légalement célébré sur le territoire marocain où résident les deux futurs époux, ni par les autorités marocaines, ni par les autorités consulaires françaises, en raison de ce qu'il s'agit d'un mariage entre deux personnes de même sexe ; que, par suite, en faisant obstacle à la faculté de se marier en France qu'ouvrent à M. A...et M. B...les dispositions citées ci-dessus de l'article 171-9 du code civil, le refus de visa opposé par le consul général de France à Casablanca porte une atteinte grave à l'exercice par M. A...de sa liberté de se marier, laquelle est une liberté fondamentale au sens des dispositions citées ci-dessus de l'article L. 521-2 du code de justice administrative ; <br/>
<br/>
              6. Considérant qu'il résulte de l'instruction, et notamment de plusieurs témoignages circonstanciés et concordants, que M. A...et M. B...partagent depuis quatre ans le même appartement à Casablanca, ville dans laquelle M. A...dispose d'un emploi stable ; qu'il résulte également de l'instruction ainsi que des échanges lors de l'audience publique que les visas de court séjour sollicités par le requérant depuis 2010 étaient motivés par l'intention de M. A...et de M. B...de passer ensemble des vacances en France ; que, dans ces conditions, ni la circonstance que M. A...serait plus jeune de 35 ans que son compagnon, ni celles qu'il aurait fait l'objet d'une obligation de quitter le territoire français en 2007 ou qu'il se serait vu refuser, en 2013, un visa par les autorités belges à la suite d'un refus opposé par les autorités françaises, ne sont sérieusement de nature à faire regarder sa demande de visa comme ayant un autre objet que celui de la célébration de son mariage sur le territoire français ; que l'atteinte portée à sa liberté de se marier par le refus qui lui a été opposé est, par suite, grave et manifestement illégale ;<br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que c'est à tort que le juge des référés du tribunal administratif de Nantes a rejeté la demande de M.A... ; qu'il y a lieu d'annuler cette ordonnance ; que M. A...disposant d'un billet de retour pour le Maroc le 29 juillet suivant, il y a lieu d'enjoindre au ministre de l'intérieur de lui délivrer, dans un délai de vingt-quatre heures à compter de la notification de la présente ordonnance, un visa d'entrée en France d'une durée lui permettant de résider sur le territoire jusqu'à cette date ; qu'il n'y a pas lieu, en revanche, d'assortir cette injonction d'une astreinte ; <br/>
<br/>
              8. Considérant que, dans les circonstances de l'espèce, il y a lieu, sur le fondement des dispositions de l'article L.761-1 du code de justice administrative, de mettre à la charge de l'Etat une somme de 5 000 euros à verser à M. A...au titre des frais exposés par lui tant en première instance qu'en appel ;<br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : L'ordonnance du 24 juin 2014 du juge des référés du tribunal administratif de Nantes est annulée.<br/>
Article 2 : Il est enjoint au ministre de l'intérieur de délivrer à M. C...A..., dans un délai de vingt-quatre heures à compter de la notification de la présente ordonnance, un visa lui permettant d'entrer sur le territoire français et d'y résider jusqu'au 29 juillet 2014.<br/>
Article 3 : L'Etat versera à M. A...la somme de 5 000 euros sur le fondement de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Le surplus des conclusions présentées par M. A...en première instance et en appel est rejeté.<br/>
Article 5 : La présente ordonnance sera notifiée à M. C...A...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-03 DROITS CIVILS ET INDIVIDUELS. LIBERTÉS PUBLIQUES ET LIBERTÉS DE LA PERSONNE. - LIBERTÉ DE SE MARIER - LIBERTÉ FONDAMENTALE AU SENS DU RÉFÉRÉ LIBERTÉ - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">35 FAMILLE. - LIBERTÉ DE SE MARIER - LIBERTÉ FONDAMENTALE AU SENS DU RÉFÉRÉ LIBERTÉ - EXISTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-035-03-03-01-01 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ TENDANT AU PRONONCÉ DE MESURES NÉCESSAIRES À LA SAUVEGARDE D'UNE LIBERTÉ FONDAMENTALE (ART. L. 521-2 DU CODE DE JUSTICE ADMINISTRATIVE). CONDITIONS D'OCTROI DE LA MESURE DEMANDÉE. ATTEINTE GRAVE ET MANIFESTEMENT ILLÉGALE À UNE LIBERTÉ FONDAMENTALE. LIBERTÉ FONDAMENTALE. - INCLUSION - LIBERTÉ DE SE MARIER.
</SCT>
<ANA ID="9A"> 26-03 La liberté de se marier constitue une liberté fondamentale au sens des dispositions de l'article L. 521-2 du code de justice administrative.</ANA>
<ANA ID="9B"> 35 La liberté de se marier constitue une liberté fondamentale au sens des dispositions de l'article L. 521-2 du code de justice administrative.</ANA>
<ANA ID="9C"> 54-035-03-03-01-01 La liberté de se marier constitue une liberté fondamentale au sens des dispositions de l'article L. 521-2 du code de justice administrative.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
