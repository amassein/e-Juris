<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025678363</ID>
<ANCIEN_ID>JG_L_2012_04_000000342052</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/67/83/CETATEXT000025678363.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème SSJS, 11/04/2012, 342052, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-04-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>342052</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELVOLVE, DELVOLVE ; SCP COUTARD, MUNIER-APAIRE</AVOCATS>
<RAPPORTEUR>M. Romain Victor</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:342052.20120411</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 30 juillet et 29 octobre 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour l'ASSOCIATION SYNDICALE AUTORISEE DU HAMEAU DE LA JONCHERE, dont le siège est 6, rue de l'Etang à Rueil-Malmaison (92500), représentée par son président ; l'ASSOCIATION SYNDICALE AUTORISEE DU HAMEAU DE LA JONCHERE demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 08VE02263 du 27 mai 2010 par lequel la cour administrative d'appel de Versailles a rejeté l'appel qu'elle a interjeté du jugement n° 0608279-0608864 du 17 avril 2008 par lequel le tribunal administratif de Versailles, faisant droit à la demande de MM. C...etA..., a annulé, d'une part, la délibération du 28 juin 2006 de l'assemblée générale de cette association en tant qu'elle a décidé d'implanter une barrière permettant de procéder à la fermeture de l'entrée principale du hameau durant la nuit et, d'autre part, la décision du 12 juillet 2006 en tant que, par cette décision, le conseil syndical a décidé de faire entreprendre les travaux par une société ayant déjà posé une barrière clôturant l'avenue Joséphine ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de M. C...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu l'ordonnance n° 2004-632 du 1er juillet 2004 ;<br/>
<br/>
              Vu le décret n° 2006-504 du 3 mai 2006 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Romain Victor, chargé des fonctions de Maître des Requêtes,<br/>
<br/>
              - les observations de la SCP Coutard, Munier-Apaire, avocat de l'ASSOCIATION SYNDICALE AUTORISÉE DU HAMEAU DE LA JONCHERE et de la SCP Delvolvé, Delvolvé, avocat de M. B...C..., <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Coutard, Munier-Apaire, avocat de l'ASSOCIATION SYNDICALE AUTORISEE DU HAMEAU DE LA JONCHERE et à la SCP Delvolvé, Delvolvé, avocat de M. B...C... ; <br/>
<br/>
<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que l'ASSOCIATION SYNDICALE AUTORISEE DU HAMEAU DE LA JONCHERE, dont la création a été autorisée par un arrêté du 16 décembre 1971 du préfet des Hauts-de-Seine et qui fait suite à une association syndicale libre fondée en 1878, a pour objet l'aménagement et l'entretien du lotissement du Hameau de la Jonchère, situé sur le territoire des communes de La Celle Saint-Cloud (78) et de Rueil-Malmaison (92) ; que, par une délibération du 28 juin 2006, l'assemblée générale ordinaire de l'association a décidé de mettre en place une barrière automatique équipée de digicodes afin d'assurer la fermeture de l'entrée principale du lotissement entre vingt heures et sept heures ; que, par une décision du 12 juillet 2006, le conseil syndical de l'association a décidé d'entreprendre les travaux de mise en place de la barrière et de confier leur exécution à une entreprise ayant effectué la pose d'une autre barrière au sein du Hameau ; que, par un jugement du 17 avril 2008, le tribunal administratif de Versailles a annulé la délibération et la décision litigieuses à la demande de MM.  C...etA..., propriétaires de biens situés au Hameau de la Jonchère ; que l'ASSOCIATION SYNDICALE AUTORISEE DU HAMEAU DE LA JONCHERE se pourvoit en cassation contre l'arrêt du 27 mai 2010 par lequel la cour administrative d'appel de Versailles a rejeté l'appel qu'elle a formé contre ce jugement ;<br/>
<br/>
              Considérant, d'une part, qu'aux termes de l'article 1er de l'ordonnance du 1er juillet 2004 susvisée, dans sa rédaction en vigueur à la date des décisions attaquées : " Peuvent faire l'objet d'une association syndicale de propriétaires la construction, l'entretien ou la gestion d'ouvrages ou la réalisation de travaux en vue : / a) De prévenir les risques naturels ou sanitaires, les pollutions et les nuisances ; / b) De préserver, de restaurer ou d'exploiter des ressources naturelles ; / c) D'aménager ou d'entretenir des cours d'eau, lacs ou plans d'eau, voies et réseaux divers ; / d) De mettre en valeur des propriétés " ; qu'aux termes de l'article 58 de cette ordonnance : " La loi du 21 juin 1865 relative aux associations syndicales et la loi du 5 août 1911 relative aux associations syndicales autorisées sont abrogées (...) " ; qu'aux termes de l'article 60 de la même ordonnance : " I. - Les associations syndicales de propriétaires constituées en vertu des lois des 12 et 20 août 1790, 14 floréal an XI, 16 septembre 1807, 21 juin 1865 et 8 avril 1898 sont régies par les dispositions de la présente ordonnance. / Toutefois, leurs statuts en vigueur à la date de publication de la présente ordonnance demeurent.applicables jusqu'à leur mise en conformité avec les dispositions de celle-ci Cette mise en conformité doit intervenir dans un délai de deux ans à compter de la publication du décret en Conseil d'Etat prévu à l'article 62. Elle est approuvée par un acte de l'autorité administrative. A défaut et après mise en demeure adressée au président de l'association et restée sans effet à l'expiration d'un délai de trois mois, l'autorité administrative procède d'office aux modifications statutaires nécessaires (...) " ; qu'aux termes de l'article 62 de la même ordonnance : " Les modalités d'application de la présente ordonnance sont fixées par décret en Conseil d'Etat (...) " ; que les modalités d'application de l'ordonnance du 1er juillet 2004 ont été fixées par le décret visé ci-dessus du 3 mai 2006, qui a été publié le 5 mai 2006 au Journal officiel de la République française ; qu'aux termes de l'article 102 dudit décret : " La mise en conformité des statuts des associations syndicales autorisées (...) prescrite à l'article 60 de l'ordonnance du 1er juillet 2004 susvisée est adoptée, sur proposition du syndicat, par l'assemblée des propriétaires (...). / L'arrêté préfectoral approuvant cette mise en conformité fait l'objet des mesures de publicité et de notification prévues à l'article 13 " ; qu'il résulte de ces dispositions que les statuts d'une association syndicale en vigueur à la date de publication de l'ordonnance du 1er juillet 2004 restent applicables jusqu'à la notification de l'arrêté préfectoral approuvant leur mise en conformité ;<br/>
<br/>
              Considérant, d'autre part, que le propriétaire d'une voie privée ouverte à la circulation du public est en droit d'en interdire à tout moment l'usage au public ;<br/>
<br/>
              Considérant que la cour a jugé illégales la délibération du 28 juin 2006 par laquelle l'association requérante a décidé d'implanter une barrière en vue de procéder à la fermeture au public du hameau durant la nuit ainsi que la décision du 12 juillet 2006 de son conseil syndical au motif qu'une association syndicale autorisée, qui a le caractère d'un établissement public soumis au principe de spécialité, n'est pas compétente pour prendre des mesures de police ayant pour but de restreindre pour des motifs de sécurité l'accès aux propriétés qu'elle a été chargée d'aménager ou d'entretenir ; qu'en jugeant ainsi sans rechercher si, d'une part, les voies dont l'association a décidé la fermeture au public avaient la nature de voies privées et si, d'autre part, ces voies étaient la propriété de l'association, la cour a, comme le soutient l'association requérante, entaché son arrêt d'erreur de droit ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'ASSOCIATION SYNDICALE AUTORISEE DU HAMEAU DE LA JONCHERE est fondée à en demander l'annulation ;<br/>
<br/>
              Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'association requérante, qui n'est pas, dans la présente instance, la partie perdante ; qu'en revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. C...la somme de 1 500 euros à verser à l'ASSOCIATION SYNDICALE AUTORISEE DU HAMEAU DE LA JONCHERE au titre de ces dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt du 27 mai 2010 de la cour administrative d'appel de Versailles est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Versailles.<br/>
Article 3 : M. C...versera à l'ASSOCIATION SYNDICALE AUTORISEE DU HAMEAU DE LA JONCHERE une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de M. C...tendant à l'application de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à l'ASSOCIATION SYNDICALE AUTORISEE DU HAMEAU DE LA JONCHERE et à M. B... C.applicables jusqu'à leur mise en conformité avec les dispositions de celle-ci<br/>
Copie en sera adressée au ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
