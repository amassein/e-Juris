<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042844869</ID>
<ANCIEN_ID>JG_L_2020_12_000000430322</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/84/48/CETATEXT000042844869.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 30/12/2020, 430322, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>430322</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>Mme Rose-Marie Abel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:430322.20201230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au tribunal administratif de la Guyane, à titre principal, d'annuler la décision par laquelle le recteur de l'académie de la Guyane a rejeté implicitement le recours gracieux qu'il a présenté le 3 juin 2016 contre la décision du 16 février 2015 refusant de lui accorder l'indemnité de sujétion géographique (ISG) et, à titre subsidiaire, de condamner l'administration à lui verser une somme égale à cette indemnité en réparation du préjudice subi du fait d'une promesse fautive. Par jugement n° 1600546 du 29 décembre 2016, tribunal administratif de la Guyane a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 17BX00234 du 5 février 2019, la cour administrative de Bordeaux, après avoir annulé ce jugement en tant qu'il s'était prononcé sur les conclusions indemnitaires, a rejeté ces conclusions ainsi que le surplus des conclusions de l'appel formée par M. A... contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 30 avril et 30 juillet 2019 et 31 octobre 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il lui est défavorable ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire entièrement droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le décret n° 2013-314 du 15 avril 2013 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Rose-Marie Abel, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat de M. A... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. A... professeur d'éducation physique et sportive, fonctionnaire de l'éducation nationale, a été affecté en Guyane le 1er septembre 2014 après avoir réalisé une année de stage au sein de l'académie d'Aix-Marseille. Le recteur de la Guyane a refusé à M. A..., par décision expresse du 16 février 2015, notifiée le 20 février 2015, le versement de la première fraction de l'indemnité de sujétion géographique. M. A... a demandé au recteur, par l'intermédiaire de son avocat, le 3 juin 2016, le retrait de cette décision et le versement des deuxième et troisième fractions de l'indemnité. Par jugement du 29 décembre 2016, le tribunal administratif de la Guyane a rejeté comme irrecevable la demande de M. A... tendant, à titre principal, à l'annulation du rejet implicite opposé à sa demande du 3 juin 2016, et, à titre subsidiaire, à la condamnation de l'Etat à lui verser une somme égale à l'indemnité de sujétion géographique en réparation du préjudice subi du fait d'une promesse fautive. L'intéressé se pourvoit en cassation contre l'arrêt du 5 février 2019 par lequel la cour administrative de Bordeaux, après avoir annulé ce jugement en tant qu'il s'était prononcé sur les conclusions indemnitaires, a rejeté l'ensemble de ses conclusions. <br/>
<br/>
              Sur l'arrêt en tant qu'il a rejeté les conclusions d'excès de pouvoir :<br/>
<br/>
              2. En premier lieu, il ressort des pièces du dossier soumis aux juges du fond que, dans son mémoire en défense devant le tribunal administratif, le recteur de la Guyane a opposé une fin de non-recevoir aux conclusions tendant à l'annulation du rejet implicite de la demande du 3 juin 2016, tirée de ce qu'eu égard à la date à laquelle elle a été formée, cette demande n'avait pas prorogé le délai de recours contentieux à l'encontre de la décision du 16 février 2015. M. A... a, ainsi, été mis en mesure de présenter utilement ses observations sur le caractère définitif de cette décision et le caractère confirmatif de ce rejet. Par suite, en jugeant que le tribunal administratif avait pu régulièrement se fonder sur le caractère confirmatif du rejet litigieux pour rejeter comme irrecevables les conclusions tendant à son annulation, sans inviter préalablement les parties à présenter leurs observations sur ce point, la cour n'a pas commis d'erreur de droit.<br/>
<br/>
              3. En second lieu, une deuxième décision dont l'objet est le même que la première revêt un caractère confirmatif, dès lors que ne s'est produit entre temps aucun changement dans les circonstances de droit ou de fait de nature à emporter des conséquences sur l'appréciation des droits ou prétentions en litige. Après avoir relevé, d'une part, par un motif non contesté, que la décision du 16 février 2015 refusant à M. A... le versement de la première fraction de l'indemnité de sujétion géographique était devenue définitive à la date à laquelle il a formé sa demande du 3 juin 2016 et, d'autre part, que cette décision s'était prononcée sur le droit de l'intéressé à percevoir l'indemnité, la cour a pu sans commettre ni erreur de droit ni erreur de qualification juridique, dès lors que cette décision était fondée sur un motif valable pour l'une quelconque des fractions de l'indemnité, et alors même que la demande tendait également au versement des deux dernières fractions et que la prescription s'apprécie séparément pour chacune d'entre elles, en déduire que son rejet présentait, en l'absence de circonstance de fait ou de droit nouvelle, un caractère confirmatif.<br/>
<br/>
              Sur l'arrêt en tant qu'il a rejeté les conclusions indemnitaires :<br/>
<br/>
              4. En premier lieu, aux termes de l'article 8 du décret du 15 avril 2013 portant création d'une indemnité de sujétion géographique, dans sa rédaction alors applicable : " Une affectation ouvrant droit à l'indemnité de sujétion géographique prévue ne peut être sollicitée qu'à l'issue d'une affectation d'une durée minimale de deux ans hors de la Guyane, de Saint-Martin, de Saint-Pierre-et-Miquelon, de Saint-Barthélemy ou de Mayotte. ". En jugeant que la circonstance que la condition d'affectation de deux ans hors de la Guyane ne soit pas remplie faisait obstacle, non pas à une nomination en Guyane comme le soutient le requérant, mais au versement de l'indemnité de sujétion géographique des agents qui y sont affectés, la cour n'a pas commis d'erreur de droit.<br/>
<br/>
              5. En deuxième lieu, en jugeant, pour écarter l'existence de toute promesse fautive ayant causé un préjudice, qu'il ne résultait de l'instruction ni que l'Etat aurait assuré à M. A... de façon ferme et précise qu'il bénéficierait, au titre de son affectation en Guyane, de l'indemnité, ni que son choix de rejoindre ce département avait pour motif déterminant l'information relative à ce régime indemnitaire figurant dans diverses circulaires et sur le site internet de l'académie de la Guyane, la cour n'a pas dénaturé les pièces du dossier.<br/>
<br/>
              6. En troisième et dernier lieu, il ressort des pièces du dossier soumis aux juges du fond que si M. A... a invoqué, par la voie de l'exception, l'illégalité des dispositions citées au point 4 au regard du principe d'égalité, ce moyen n'était présenté qu'à l'appui de ses conclusions d'excès de pouvoir, lesquelles ont été, ainsi qu'il a été dit, rejetées comme irrecevables. Par suite, le moyen tiré de ce que la cour aurait entaché son arrêt d'insuffisance de motivation en ne répondant pas à ce moyen, ne peut qu'être écarté.<br/>
<br/>
              7. Il résulte de tout ce qui précède que M. A... n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque. Ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi de M. A... est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. B... A..., et au ministre de l'éducation nationale, de la jeunesse et des sports.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
