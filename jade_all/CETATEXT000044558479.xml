<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044558479</ID>
<ANCIEN_ID>JG_L_2021_12_000000449254</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/55/84/CETATEXT000044558479.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 23/12/2021, 449254, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>449254</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SARL DIDIER-PINET</AVOCATS>
<RAPPORTEUR>Mme Agnès Pic</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette-fs</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:449254.20211223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'association Alliance pour la recherche en cancérologie (APREC) a demandé au tribunal administratif de Paris d'annuler pour excès de pouvoir le courrier du 22 mai 2017 par lequel le directeur général de l'Assistance publique-Hôpitaux de Paris a refusé de la labelliser en tant que structure tierce pour la conduite de recherches biomédicales. Par un jugement n° 1712217 du 4 juillet 2019, le tribunal administratif de Paris a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 19PA02902 du 1er décembre 2020, la cour administrative d'appel de Paris a rejeté l'appel de cette association contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés le 1er février, le 30 avril et le 30 septembre 2021 au secrétariat du contentieux du Conseil d'Etat, l'association Alliance pour la recherche en cancérologie demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Assistance publique-Hôpitaux de Paris la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Agnès Pic, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Piwnica, Molinié, avocat de l'association Alliance pour la recherche en cancérologie, et à la SARL Didier-Pinet, avocat de l'Assistance publique-Hôpitaux de Paris ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. En vertu du IV de l'article L. 1121-16-1 du code de la santé publique, lorsqu'une recherche interventionnelle à finalité commerciale impliquant la personne humaine est réalisée dans des établissements de santé, le promoteur de cette recherche prend en charge les frais supplémentaires liés à d'éventuels fournitures ou examens spécifiquement requis par le protocole. Cette prise en charge " fait l'objet d'une convention conclue entre le promoteur, le représentant légal [de chacun des établissements de santé concernés] et, le cas échéant, le représentant légal des structures destinataires des contreparties versées par le promoteur. " Cette convention, dénommée " convention unique " en application du I de l'article R. 1121-3-1 du même code, peut prévoir, selon le II de cet article, que des contreparties financières soient versées par le promoteur à des " structures distinctes tierces participant à la recherche mais ne relevant pas de l'autorité du représentant légal de l'établissement (...) où se déroule également la recherche ".<br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que, par une note du 24 janvier 2017 publiée sur son site internet, l'Assistance publique-Hôpitaux de Paris (AP-HP) a lancé un appel à candidatures en vue de la " labellisation des structures tierces à la convention unique dans le cadre des essais industriels ". Par un courrier du 22 mai 2017, le directeur général de l'AP-HP a informé le président de l'association Alliance pour la recherche en cancérologie (APREC) que la candidature de cette association à la labellisation n'avait pas été retenue. Par un jugement du 4 juillet 2019, le tribunal administratif de Paris a rejeté la demande de l'APREC tendant à l'annulation pour excès de pouvoir de cette décision. L'APREC se pourvoit en cassation contre l'arrêt du 1er décembre 2020 par lequel la cour administrative d'appel de Paris a rejeté son appel contre le jugement du tribunal administratif de Paris au motif que cette décision était dépourvue d'effet juridique et, par suite, insusceptible de faire l'objet d'un recours pour excès de pouvoir.<br/>
<br/>
              3. Il ressort toutefois des termes mêmes de cette note que seules les structures labellisées seront habilitées à recevoir des contreparties négociées avec les promoteurs des recherches impliquant la personne humaine en application des dispositions précitées, pour les recherches impliquant l'AP-HP. Eu égard à la portée attachée par l'AP-HP à la labellisation, décrite par la note du 24 janvier 2017 comme un préalable nécessaire à la conclusion d'une convention unique portant sur des recherches impliquant la personne humaine se déroulant à l'AP-HP, le refus d'octroi du label ne peut être regardé comme insusceptible de produire des effets sur la situation de l'association requérante, dont le but statutaire unique est de " promouvoir, soutenir et développer la recherche (...) contre le cancer (...) selon des protocoles de coopération établis entre les services du GHU Paris-Est, de Cancer-Est, de l'Assistance publique des hôpitaux de Paris et des groupes de recherche nationaux et internationaux " et dont il ressort, au demeurant, du  bilan comptable pour l'exercice 2015 qu'entre le quart et la moitié de ses ressources financières proviennent de sa participation à des recherches impliquant la personne humaine. Il ressort d'ailleurs des termes mêmes du courrier en litige qu'il invite cette association à se rapprocher de l'AP-HP " pour déterminer les nouvelles modalités de collaboration, notamment à propos de vos salariés dont le changement d'employeur devrait être envisagé, le cas échéant ".<br/>
<br/>
              4. Il résulte de ce qui précède qu'en jugeant que le courrier du 22 mai 2017, qui ne peut être regardé comme présentant le caractère d'un acte préparatoire dès lors qu'il n'est pas contesté qu'il ne s'inscrivait dans le cadre de la conclusion d'aucune convention unique en particulier, ne faisait pas grief à l'association requérante, la cour administrative d'appel a commis une erreur de droit. Par suite, sans qu'il soit besoin de se prononcer sur l'autre moyen du pourvoi, son arrêt doit être annulé.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Assistance publique-Hôpitaux de Paris une somme de 3 000 euros à verser à l'association Alliance pour la recherche en cancérologie au titre de l'article L. 761-1 du code de justice administrative. Les mêmes dispositions font obstacle à ce qu'une somme soit mise à la charge de l'association Alliance pour la recherche en cancérologie, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 1er décembre 2020 de la cour administrative d'appel de Paris est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br/>
Article 3 : L'Assistance publique - Hôpitaux de Paris versera à l'association Alliance pour la recherche en cancérologie une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par l'Assistance publique-Hôpitaux de Paris au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à l'association Alliance pour la recherche en cancérologie et à l'Assistance publique-Hôpitaux de Paris.<br/>
              Délibéré à l'issue de la séance du 10 décembre 2021 où siégeaient : Mme Gaëlle Dumortier, présidente de chambre, présidant ; M. Damien Botteghi, conseiller d'Etat et Mme Agnès Pic, maître des requêtes en service extraordinaire-rapporteure. <br/>
<br/>
              Rendu le 23 décembre 2021.<br/>
<br/>
                 La présidente : <br/>
                 Signé : Mme Gaëlle Dumortier<br/>
 		La rapporteure : <br/>
      Signé : Mme Agnès Pic<br/>
                 La secrétaire :<br/>
                 Signé : Mme A... B...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
