<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028110484</ID>
<ANCIEN_ID>JG_L_2013_10_000000359919</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/11/04/CETATEXT000028110484.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  4ème sous-section jugeant seule, 23/10/2013, 359919, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-10-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>359919</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 4ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Florence Chaltiel-Terral</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:359919.20131023</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 1er juin 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par Mme A...B..., demeurant ... ; Mme B...demande au Conseil d'Etat : <br/>
              1°) d'annuler pour excès de pouvoir la décision du 15 décembre 2011 par laquelle le directeur de l'institut universitaire de technologie de Metz a émis un avis défavorable sur sa candidature au poste de professeur des universités en algorithmique parallèle et problèmes combinatoires n° 193 (section 27 du Conseil national des universités en informatique) ; <br/>
<br/>
              2°) d'enjoindre au président de l'université Paul Verlaine de Metz de transmettre au ministre compétent les délibérations du comité de sélection, du Conseil national des universités et du conseil d'administration des 14 mai 2009, 29 juin 2009 et 6 décembre 2011 en vue de sa nomination ou, subsidiairement, d'enjoindre au directeur de l'institut universitaire de technologie de Metz de statuer de nouveau sur le poste de professeur des universités n° 193 avec obligation de transmettre l'ensemble des délibérations au ministre compétent ; <br/>
<br/>
              3°) de mettre à la charge de l'université Paul Verlaine la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'éducation ; <br/>
<br/>
              Vu le décret n° 84-431 du 6 juin 1984 ; <br/>
<br/>
              Vu la décision du Conseil constitutionnel n° 2010-20/21du 6 août 2010 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Florence Chaltiel-Terral, Maître des Requêtes en service extraordinaire, <br/>
<br/>
- les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
<br/>1. Considérant qu'en vertu des articles L. 952-6-1 du code de l'éducation et 9-2 du décret du 6 juin 1984, lorsqu'un emploi d'enseignant-chercheur est créé ou déclaré vacant, les candidatures des personnes dont la qualification est reconnue par l'instance nationale prévue à l'article L. 952-6 sont soumises à l'examen d'un comité de sélection créé par délibération du conseil d'administration siégeant en formation restreinte aux représentants élus des enseignants-chercheurs, des chercheurs et des personnels assimilé ; que le comité de sélection, qui agit en qualité de jury de concours, dresse la liste des candidats qu'il retient, le conseil d'administration, siégeant en formation restreinte, ne pouvant proposer au ministre chargé de l'enseignement supérieur la nomination d'un candidat non sélectionné par le comité ; qu'en vertu de l'article L. 714-9 du code de l'éducation, aucune affectation ne peut être prononcée dans un institut faisant partie d'une université si le directeur de l'institut émet un avis défavorable ; que, toutefois, un tel avis ne saurait être fondé sur des motifs étrangers à l'administration de l'institut ; <br/>
<br/>
              2. Considérant qu'un poste de professeur des universités en algorithmique parallèle et problèmes combinatoires (n° 193) à l'institut universitaire de technologie (IUT) de Metz, rattaché à la section d'informatique du Conseil national des universités (section 27), a été ouvert au concours et publié au Journal officiel de la République française le 15 septembre 2008 ; que le Conseil d'Etat statuant au contentieux a, par une décision du 26 octobre 2011, d'une part annulé la décision par laquelle le conseil d'administration de l'université Paul Verlaine de Metz avait, dans sa proposition, inversé l'ordre de classement des deux candidatures retenues par le comité de spécialistes ainsi que, par voie de conséquence, la décision du directeur de l'IUT émettant un avis défavorable sur la candidature de Mme B...et, d'autre part, enjoint au conseil d'administration de se prononcer à nouveau sur les candidatures retenues par le comité de sélection ; qu'à la suite de cette décision, le conseil d'administration, examinant les candidatures retenues par le comité de sélection pour ce poste, a proposé la candidature de Mme B...; que, toutefois, le directeur de l'institut universitaire de technologie de Metz a émis le 15 décembre 2011 un avis défavorable à cette nomination au motif que le profil de la candidate retenue " concerne la géométrie discrète et n'a pas de rapport avec l'algorithmique parallèle et les problèmes combinatoires " et " qu'aucune des publications de Mme B...ne concerne ces deux domaines " ; <br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier, notamment de la motivation de la liste de candidats retenus par le comité de sélection ainsi que des attestations de professeurs des universités fournies par la requérante, d'une part, que ses recherches en géométrie discrète ont conduit la requérante à se spécialiser dans l'étude de certains problèmes combinatoires particulièrement présents dans la discipline informatique, d'autre part, que celle-ci a travaillé sur des questions d'algorithmique parallèle appliquées à l'informatique, au sujet desquelles elle a, d'ailleurs, publié plusieurs articles dans des revues spécialisées ; que, par suite, les motifs de l'avis défavorable du directeur de l'institut universitaire de technologie de Metz sont erronés ; que Mme B...est, sans qu'il soit besoin d'examiner les autres moyens de la requête, fondée à demander son annulation ; <br/>
<br/>
              4. Considérant que cette annulation implique seulement que soit enjoint au directeur de l'institut universitaire de technologie de Metz de réexaminer la candidature de Mme B... au poste en cause ; qu'il y a lieu de faire droit, dans cette mesure, aux conclusions à fin d'injonction de la requérante ; <br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'université de Lorraine la somme de 3 500 euros à verser à MmeB..., au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : L'avis défavorable du directeur de l'institut universitaire de technologie de Metz rendu le 15 décembre 2011 sur la candidature de Mme B...est annulé. <br/>
Article 2 : Il est enjoint au directeur de l'institut universitaire de technologie de Metz de réexaminer la candidature de Mme B...au poste de professeur des universités en algorithmique parallèle et problèmes combinatoires n° 193. <br/>
Article 3 : L'université de Lorraine versera à Mme B...une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : Le surplus des conclusions de Mme B...est rejeté. <br/>
Article 5 : La présente décision sera notifiée à Mme A...B..., à l'université de Lorraine et à la ministre de l'enseignement supérieur et de la recherche.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
