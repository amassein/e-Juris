<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028143326</ID>
<ANCIEN_ID>JG_L_2013_10_000000339260</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/14/33/CETATEXT000028143326.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 5ème sous-sections réunies, 23/10/2013, 339260</TITRE>
<DATE_DEC>2013-10-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>339260</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 5ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Louis Dutheillet de Lamothe</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:339260.20131023</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu 1°, sous le n° 339260, la requête, enregistrée le 5 mai 2010 au secrétariat du contentieux du Conseil d'Etat, présentée par M.DN..., demeurant au ...; M. DF...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir les décrets des 2 et 3 mars 2010 portant respectivement inscription à un tableau d'avancement et promotion au grade de premier conseiller dans le corps des conseillers des tribunaux administratifs et cours administratives d'appel au titre de l'année 2010 ; <br/>
<br/>
              2°) d'enjoindre à l'administration de reprendre la procédure tendant à l'établissement du tableau d'avancement au grade de premier conseiller au titre de l'année 2010, de proposer son inscription au tableau d'avancement, d'établir un nouveau tableau d'avancement et de procéder à nouveau aux nominations des premiers conseillers au titre de l'année 2010 ; <br/>
<br/>
<br/>
              Vu 2°, sous le n° 347129, la requête, enregistrée le 28 février 2011, présentée par M. DF...; M. DF...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir les décrets des 29 et 30 décembre 2010 portant respectivement inscription à un tableau d'avancement complémentaire au titre de l'année 2010 et promotion au grade de premier conseiller dans le corps des conseillers des tribunaux administratifs et cours administratives d'appel ; <br/>
<br/>
              2°) d'enjoindre à l'administration de reprendre la procédure tendant à l'établissement du tableau d'avancement complémentaire au grade de premier conseiller au titre de l'année 2010, de proposer son inscription au tableau d'avancement, d'établir un nouveau tableau d'avancement et de procéder à nouveau aux nominations des premiers conseillers au titre de l'année 2010 ; <br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ; <br/>
<br/>
              Vu la Constitution ; <br/>
<br/>
              Vu la charte des droits fondamentaux de l'Union européenne ; <br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
<br/>
              Vu la loi n° 84-16 du 11 janvier 1984 ; <br/>
<br/>
              Vu le décret n° 2002-682 du 29 avril 2002 ; <br/>
<br/>
              Vu le décret n° 2007-1365 du 17 septembre 2007 ; <br/>
<br/>
              Vu l'arrêté du 12 mai 2009 relatif à l'entretien professionnel des membres du corps des tribunaux administratifs et des cours administratives d'appel ; <br/>
<br/>
              Vu l'arrêté du 1er juin 2004 relatif aux conditions générales d'évaluation et de notation des membres du corps des tribunaux administratifs et des cours administratives d'appel ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Louis Dutheillet de Lamothe, Auditeur,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que les requêtes visées ci-dessus présentent à juger les mêmes questions ; qu'il y a lieu de les joindre pour statuer par une seule décision ; <br/>
<br/>
              Sur les exceptions d'illégalité soulevées par le requérant : <br/>
<br/>
              2. Considérant qu'en vertu des dispositions de l'article R. 311-1 du code de justice administrative, le Conseil d'Etat est compétent pour connaître en premier et dernier ressort des litiges relatifs aux décrets du Président de la République ; que ces dispositions ne sauraient faire obstacle à l'application de la règle générale de procédure, selon laquelle aucun membre d'une juridiction administrative ne peut participer au jugement d'un recours dirigé contre une décision administrative ou juridictionnelle dont il est l'auteur ; qu'il en résulte que la formation de jugement d'un litige relatif à un membre du corps des tribunaux administratifs et des cours administratives d'appel ne peut être composée de membres du Conseil d'Etat ayant préparé ou pris des actes relatifs à ce litige ; que, dès lors, M. DF...n'est pas fondé à soutenir que la compétence donnée en premier et dernier ressort au Conseil d'Etat par les dispositions de l'article R. 311-1 méconnaîtrait le principe constitutionnel d'indépendance des membres des tribunaux administratifs et des cours administratives d'appel, le droit à un procès équitable garanti par la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ou l'article 9 de la loi du 11 janvier 1984, aux termes duquel la loi fixe les règles garantissant l'indépendance des membres des tribunaux administratifs ; qu'aucun de ces textes ni principes n'exige un double degré de juridiction pour le jugement des litiges relatifs aux membres de ce corps ; <br/>
<br/>
              3. Considérant que le requérant n'est pas davantage fondé à soutenir que les dispositions de l'article R. 311-1 du code de justice administrative relèvent du domaine législatif au titre de l'article 34 de la Constitution, dès lors que, précisant la liste des dérogations, dont la possibilité est ouverte par l'article L. 311-1 du même code, à la compétence de premier ressort des tribunaux administratifs, il ne se rapporte à aucune des matières réservées au législateur par la Constitution ; <br/>
<br/>
              4. Considérant que M. DF...ne peut utilement soutenir que l'article R. 311-1 du code de justice administrative méconnaîtrait l'article 47 de la charte des droits fondamentaux de l'Union européenne dès lors que l'article 51 de cette charte prévoit qu'elle n'est applicable aux États membres que lorsqu'ils mettent en oeuvre le droit de l'Union et que le droit de l'Union ne régit pas la répartition des compétences au sein de la juridiction administrative ; <br/>
<br/>
              5. Considérant qu'il résulte également de ce qui a été dit au point 2 que M. DF... n'est pas fondé à soutenir que le fait que le vice-président du Conseil d'Etat soit chargé, en vertu de l'article R. 231-3 du code de justice administrative, de la gestion du corps des tribunaux administratifs et des cours administratives d'appel et qu'il préside le conseil supérieur des tribunaux administratifs et des cours administratives d'appel méconnaîtrait l'article 9 de la loi du 11 janvier 1984 ; que ces dispositions n'instaurent pas non plus une garantie qui relèverait du domaine de la loi en application de ce même article ; que les moyens tirés de l'illégalité, par voie de conséquence, de la proposition du conseil supérieur des tribunaux administratifs et des cours administratives d'appel et des décrets attaqués ne peuvent, par suite, qu'être écartés ;<br/>
<br/>
              6. Considérant que le requérant soutient que le décret du 17 septembre 2007 et les arrêtés des 1er juin 2004 et 12 mai 2009 visés-ci-dessus, qui régissent les procédures d'évaluation de la valeur professionnelle ayant permis l'établissement des tableaux d'avancement attaqués, sont entachés d'illégalité ; que, toutefois et en tout état de cause, le décret du 17 septembre 2007 pouvait, sans méconnaître sa compétence renvoyer à des arrêtés ministériels le soin de préciser quels fonctionnaires seraient soumis à la procédure expérimentale d'entretien professionnel, appelée à remplacer la procédure de notation alors en vigueur, dès lors que l'article 55 bis de la loi du 11 janvier 1984 alors en vigueur prévoyait que les administrations de l'État pouvaient " être autorisées, à titre expérimental (...) à se fonder sur un entretien professionnel pour apprécier la valeur professionnelle des fonctionnaires " ; qu'en tout état de cause, l'arrêté du 12 mai 2009 et celui du 1er juin 2004, applicable aux évaluations portant sur une période antérieure au 1er juillet 2009, pouvaient légalement préciser la période de référence prise en compte pour chaque entretien professionnel en application, respectivement, des articles 5 du décret du 17 septembre 2007 et 7 du décret du 29 avril 2002 sur le fondement desquels ces arrêtés ont été édictés ;  que le moyen tiré de ce que l'arrêté du 12 mai 2009 instituerait une procédure de signature du compte rendu de l'entretien professionnel qui méconnaîtrait le décret du 17 septembre 2007 n'est pas assorti des précisions suffisantes permettant d'en apprécier le bien-fondé ; <br/>
<br/>
              7. Considérant que, si le requérant invoque par voie d'exception l'illégalité des dispositions de l'article R. 234-1 du code de justice administrative relatif à l'avancement d'échelon, ce moyen est inopérant contre les décrets attaqués, qui ne sont pris ni sur le fondement de cet article, ni en application de décisions individuelles d'avancement d'échelon ; <br/>
<br/>
              Sur les autres moyens des requêtes : <br/>
<br/>
              8. Considérant qu'aux termes de l'article 17 du décret du 29 avril 2002 : " Le tableau d'avancement doit être arrêté le 15 décembre au plus tard de l'année précédant celle pour laquelle il est établi. Il cesse d'être valable à l'expiration de cette même année. / En cas d'épuisement du tableau, il est procédé à l'établissement d'un tableau complémentaire, qui doit être arrêté le 1er décembre au plus tard de l'année pour laquelle il est dressé. Il cesse d'être valable à l'expiration de cette même année. " ; que si ces dispositions prévoient que les tableaux d'avancements pour une année donnée doivent être arrêtés au plus tard le 15 décembre de l'année qui précède pour le tableau initial et le 1er décembre de l'année au titre de laquelle il est établi pour le tableau complémentaire, le respect de ces délais n'est pas prescrit à peine de nullité ; que, cependant, dans le cas où ces tableaux sont arrêtés après ces dates, ils doivent être établis dans l'ordre de succession ainsi fixé et dans le respect des dispositions statutaires en vigueur respectivement le 15 décembre de l'année précédente ou le 1er décembre de l'année en cause ; <br/>
<br/>
              9. Considérant qu'il résulte de ce qui précède que le fait que les tableaux attaqués, qui n'ont pas d'effet rétroactif, ont été établis postérieurement aux dates indiquées par l'article 17 du décret du 29 avril 2002 est sans incidence sur leur légalité ; que, si le requérant soutient que ces tableaux méconnaissent les dispositions en vigueur à la date à laquelle ils auraient dû au plus tard être arrêtés, il n'apporte pas à son moyen les précisions permettant d'en apprécier le bien-fondé ; qu'enfin, contrairement à ce qu'il soutient, les conditions statutaires à remplir pour être promu doivent être remplies, non à la date d'édiction du tableau, mais au moment de la nomination, soit au plus tard le 31 décembre de l'année au titre de laquelle les promotions sont effectuées ; <br/>
<br/>
              10. Considérant que les moyens tirés de ce que la composition du conseil supérieur des tribunaux administratifs et des cours administratives d'appel lors de sa réunion du 25 janvier 2010 était irrégulière et de ce que son avis aurait dû faire l'objet d'une publicité pour être légalement émis ne sont pas assortis des précisions suffisantes pour en apprécier le bien-fondé ; <br/>
<br/>
              11. Considérant qu'il ne résulte d'aucune disposition législative ou réglementaire, ni d'aucun principe, notamment pas du principe de l'indépendance de la juridiction administrative, que les membres du corps des conseillers des tribunaux administratifs et des cours administratives d'appel susceptibles d'être promus doivent recevoir communication de leur dossier et des motifs de l'avis émis par leur chef de juridiction afin d'être mis à même d'y répondre ; que, leur indépendance n'impliquant pas que les procédures de promotion au choix soient contradictoires, le requérant n'est pas non plus fondé à exciper de l'illégalité de l'article 18 du décret du 29 avril 2002 en tant qu'il n'institue qu'une possibilité et non une obligation d'audition des fonctionnaires susceptibles d'être promus ; <br/>
<br/>
              12. Considérant que le refus d'inscription au tableau d'avancement n'est pas au nombre des décisions individuelles refusant aux intéressés un avantage auquel ils ont droit qui, en application de l'article 1er de la loi du 11 juillet 1979,  doivent être motivées ; <br/>
<br/>
              13. Considérant que l'absence, dans les visas des décrets attaqués, de la mention des avis donnés par le conseil supérieur, des dispositions applicables et, s'agissant du décret du 3 mars 2010, du décret du 2 mars 2010, est sans influence sur la légalité de ces décrets ; <br/>
<br/>
              14. Considérant qu'il ressort des pièces du dossier, notamment des procès-verbaux des réunions du conseil supérieur des tribunaux administratifs et des cours administratives d'appel des 26 janvier et 24 novembre 2010 que, contrairement à ce que soutient le requérant, ce conseil a procédé, sans s'appuyer sur des critères illégaux et sans se fonder exclusivement sur l'avis du chef de juridiction, à un examen approfondi de la valeur professionnelle et du mérite de chacun des conseillers des tribunaux administratifs et des cours administratives d'appel susceptibles d'être promus premier conseiller, en prenant en compte l'ensemble des éléments mentionnés à l'article 18 du décret du 29 avril 2002 ; qu'il en ressort, en particulier, que le conseil supérieur a relevé que M.DF..., qui a été placé depuis 2006 en congé de longue durée, a fait l'objet d'appréciations défavorables relatives à son activité antérieure à son congé et a examiné les fondements de ces appréciations ; que si le requérant conteste ces appréciations, notamment s'agissant du grief de retard dans le traitement des dossiers, les éléments qu'il avance ne sont pas de nature à infirmer les constatations faites par le conseil et figurant aux procès-verbaux ; qu'en particulier, le fait que d'autres conseillers pouvaient se voir reprocher des retards au sein du même tribunal n'est pas, en tout état de cause, de nature à remettre en cause l'appréciation portée sur le requérant ; que l'appréciation portée en 2010 étant relative à la période d'activité de M. DF...antérieure à son congé de longue durée de 2006, le fait que la motivation figurant au procès-verbal de ces réunions soit en partie identique à celle des procès-verbaux des réunions du même conseil en 2008 ne révèle aucune automaticité ; que, dès lors, M. DF...n'est pas fondé à soutenir que la décision de ne pas proposer son inscription au tableau d'avancement serait entachée d'erreur manifeste d'appréciation ni que celle-ci constituerait une sanction déguisée, une discrimination ou un harcèlement moral ; <br/>
<br/>
              15. Considérant que le Président de la République ne pouvait inscrire au tableau d'avancement des conseillers ne figurant pas sur la proposition faite par le conseil supérieur des tribunaux administratifs et des cours administratives d'appel ; qu'en adoptant les tableaux proposés et en procédant aux nominations, il n'a pas méconnu l'étendue de ses compétences ;<br/>
<br/>
              16. Considérant qu'il résulte de tout ce qui précède que les requêtes de M. DF... doivent être rejetées, y compris leurs conclusions à fin d'injonction ; <br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : Les requêtes de M. DF...sont rejetées. <br/>
Article 2 : La présente décision sera notifiée à M.DN..., au Premier ministre et à la garde des sceaux, ministre de la justice. <br/>
Copie en sera adressée à M. M...AS..., à M. AQ...L'hirondel, à M. FranckJozek, à Mme CF...CQ..., à M. BO...BW..., à Mlle CL...BR..., à M. BB...P..., à M.DI..., à M. BA...AT..., à M. DH...BC..., à M. BK...BL..., à Mme O...CJ..., à Mme AF...DM..., à Mlle DG...B..., à Mme AP...AV..., à M. DB...L..., à M. BE...S..., à M. AL...CC..., à Mlle E...AG..., à M. BK...CK..., à Mlle BF...AI..., à Mlle AnneCU..., à Mme Anne-LaureChavrier, à M. PierreCB..., à M. DK...BQ..., à Mlle BJ...Z..., à Mme BS...BD..., à M. CM...U..., à M. BP...I..., à M. N...AU..., à Mlle K...AJ..., à M. CR...BH..., à M. CX...AQ..., à M. AL...H..., à Mme V...CV..., à M. PierreCT..., à Mme CF...BI..., à Mlle BZ...BX..., à M. W... Q..., à MmeDO..., à M. BT...BN..., à Mlle J...BY..., à MmeDP..., à Mlle CS...X..., à M. Y...CI..., à Mme CD...G..., à Mme CR...AA..., à M. M...F..., à M. DH...DJ..., à Mlle AY...AC..., à M. BU...CN..., à M. BM...DC..., à M. Pierre BU..., à M. BV...C..., à M. BT... BG..., Mme AD...AE..., à M. AQ...AX..., à Mme CO...DA..., à M. BM...T..., à M. BV...AN..., à M. BM...DR...AB..., à Mme AM...DL..., à Mme CH...DD..., à Mme DS...BV...-DT..., à M. AO...CP..., à Mme AZ...CW..., à M. CE... CG..., à M. BB...AR..., à Mme CZ...AH...et à M. DE... CY....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-035-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. ACTES RÉGLEMENTAIRES. VIOLATION D'UN DÉCRET. - AVANCEMENT DES FONCTIONNAIRES DE L'ETAT - DATES LIMITES D'ÉTABLISSEMENT DES TABLEAUX INITIAL ET COMPLÉMENTAIRE FIXÉES PAR LE DÉCRET DU 29 AVRIL 2002 - 1) DÉLAIS PRESCRITS À PEINE D'ILLÉGALITÉ - ABSENCE [RJ1] - 2) CONDITIONS DE LÉGALITÉ DES TABLEAUX EN CAS DE DÉPASSEMENT DE CES DÉLAIS - RESPECT DE L'ORDRE DE SUCCESSION ENTRE TABLEAUX ET DES DISPOSITIONS STATUTAIRES EN VIGUEUR À CES MÊMES DATES.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-06-02-01-01 FONCTIONNAIRES ET AGENTS PUBLICS. NOTATION ET AVANCEMENT. AVANCEMENT. AVANCEMENT DE GRADE. TABLEAUX D'AVANCEMENT. - FONCTION PUBLIQUE DE L'ETAT - DATES LIMITES D'ÉTABLISSEMENT DES TABLEAUX INITIAL ET COMPLÉMENTAIRE FIXÉES PAR LE DÉCRET DU 29 AVRIL 2002 - 1) DÉLAIS PRESCRITS À PEINE D'ILLÉGALITÉ - ABSENCE [RJ1] - 2) CONDITIONS DE LÉGALITÉ DES TABLEAUX EN CAS DE DÉPASSEMENT DE CES DÉLAIS - RESPECT DE L'ORDRE DE SUCCESSION ENTRE TABLEAUX ET DES DISPOSITIONS STATUTAIRES EN VIGUEUR À CES MÊMES DATES.
</SCT>
<ANA ID="9A"> 01-04-035-01 1) S'il résulte de l'article 17 du décret n° 2002-682 du 29 avril 2002 relatif aux conditions générales d'évaluation, de notation et d'avancement des fonctionnaires de l'Etat que les tableaux d'avancement pour une année donnée doivent être arrêtés au plus tard le 15 décembre de l'année qui précède pour le tableau initial et le 1er décembre de l'année au titre de laquelle il est établi pour le tableau complémentaire, le respect de ces délais n'est pas prescrit à peine de nullité.... ,,2) Cependant, dans le cas où ces tableaux sont arrêtés après ces dates, ils doivent être établis dans l'ordre de succession ainsi fixé et dans le respect des dispositions statutaires en vigueur respectivement le 15 décembre de l'année précédente ou le 1er décembre de l'année en cause.</ANA>
<ANA ID="9B"> 36-06-02-01-01 1) S'il résulte de l'article 17 du décret n° 2002-682 du 29 avril 2002 relatif aux conditions générales d'évaluation, de notation et d'avancement des fonctionnaires de l'Etat que les tableaux d'avancement pour une année donnée doivent être arrêtés au plus tard le 15 décembre de l'année qui précède pour le tableau initial et le 1er décembre de l'année au titre de laquelle il est établi pour le tableau complémentaire, le respect de ces délais n'est pas prescrit à peine de nullité.... ,,2) Cependant, dans le cas où ces tableaux sont arrêtés après ces dates, ils doivent être établis dans l'ordre de succession ainsi fixé et dans le respect des dispositions statutaires en vigueur respectivement le 15 décembre de l'année précédente ou le 1er décembre de l'année en cause.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., en ce qui concerne le tableau initial, CE, 2 février 1968, Ministre des affaires sociales c/ Dussutour, n° 73122, p. 86.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
