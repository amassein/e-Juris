<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044316274</ID>
<ANCIEN_ID>JG_L_2021_10_000000457628</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/31/62/CETATEXT000044316274.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 27/10/2021, 457628, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-10-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>457628</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:457628.20211027</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au juge des référés du tribunal administratif de Strasbourg, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'ordonner les mesures nécessaires pour organiser, aux frais de l'Etat, son retour effectif sur le territoire français et à ce qu'il soit muni d'un document destiné à la police bulgare et à la compagnie aérienne confirmant qu'il est autorisé à se rendre en France, dans un délai de cinq jours à compter de la notification de l'ordonnance à intervenir, sous astreinte de 500 euros par jour de retard. <br/>
<br/>
              Par une ordonnance n° 2106737 du 5 octobre 2021, le juge des référés du tribunal administratif de Strasbourg a rejeté sa demande.<br/>
<br/>
              Par une requête, enregistrée le 18 octobre 2021 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) de surseoir à statuer et de poser à la Cour de justice de l'Union européenne des questions préjudicielles portant sur l'interprétation du règlement (UE) n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013, en particulier, en cas de reprise en charge par un Etat membre sur le fondement des dispositions de l'article 18-1-d) de ce règlement, sur les obligations pesant sur l'Etat membre requérant en termes de demande de garanties à l'Etat membre déterminé comme responsable s'agissant de la recevabilité d'une procédure de réexamen de la demande d'asile et de l'octroi de conditions matérielles d'accueil, ainsi que sur la portée et les conditions des échanges d'informations préalables à l'exécution du transfert, tels que prévus notamment par les article 31 et 32 du règlement ;<br/>
<br/>
              2°) d'interpréter le droit national pour dire si le juge des libertés et de la détention peut, en vertu des pouvoirs qui lui sont conférés en qualité de garant des libertés individuelles, procéder à un contrôle de conformité de la mesure de placement en rétention administrative avec les stipulations de l'article 3 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales dans le cas où des éléments nouveaux de fait ou de droit justifient ce type de contrôle et notamment en tenant compte de l'imminence de l'exécution de la mesure de transfert et de l'impossibilité pour la personne retenue de saisir la juridiction administrative d'un recours suspensif et ce indépendamment du fait que l'étranger ait déjà exercé ou non le recours prévu aux articles L. 572-4 à L. 572-6 du code de l'entrée et du séjour des étrangers et du droit d'asile devant la juridiction administrative ;<br/>
<br/>
              3°) d'annuler l'ordonnance du 5 octobre 2021 ;<br/>
<br/>
              4°) d'enjoindre à la préfète du Bas-Rhin de prendre toutes les mesures nécessaires pour organiser dans les meilleurs délais et aux frais de l'Etat son retour en France, dans un délai de cinq jours à compter de la notification de l'ordonnance à intervenir et sous astreinte de 500 euros par jour de retard ;<br/>
<br/>
              5°) d'enjoindre à l'Etat de lui délivrer un document destiné à la police bulgare et à la compagnie aérienne confirmant qu'il est autorisé à se rendre en France, dans un délai de cinq jours à compter de la notification de l'ordonnance à intervenir et sous astreinte de 500 euros par jour de retard ;<br/>
<br/>
              6°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Il soutient que :<br/>
              - le juge des référés du tribunal administratif de Strasbourg a entaché son ordonnance d'une omission à statuer faute d'avoir répondu à son moyen tiré de l'atteinte grave et manifestement illégale portée à plusieurs libertés fondamentales en le maintenant illégalement, durant plusieurs heures, dans le centre de rétention administrative dans le seul but d'exécuter l'arrêté de transfert, et ce avant l'issue de la procédure d'appel qui ne revêtait pas un caractère suspensif ;<br/>
              - le juge des référés a estimé à tort que les circonstances de l'exécution de la décision de transfert ne relevaient pas de son office, alors que son maintien en rétention par l'autorité préfectorale, hors de tout cadre légal, a porté une atteinte grave et manifestement illégale à sa liberté d'aller et venir et à son droit à la sûreté, ainsi qu'à son droit à un recours effectif, à un accès au juge et à un procès équitable, et a constitué une manœuvre permettant de procéder à cette exécution forcée en méconnaissance de la décision du juge des libertés et de la détention ;<br/>
              - le juge des référés a commis une erreur de droit et inexactement apprécié les faits de l'espèce en estimant qu'il n'appartenait pas à la préfète du Bas-Rhin de s'assurer auprès des autorités bulgares que sa demande d'asile serait réexaminée dans ce pays, que son transfert n'impliquerait pas son renvoi vers l'Afghanistan sans qu'il puisse contester cette mesure et qu'il ne sera pas exposé à une atteinte grave et manifestement illégale à son droit d'asile et à un risque de traitement inhumain et dégradant en cas de transfert en Bulgarie, y compris au regard des conditions matérielles d'accueil, et en ne retenant pas le manquement de celle-ci à son obligation de diligences ;<br/>
              - la carence préfectorale constitue une atteinte grave et manifestement illégale au droit d'asile, dès lors qu'il existe des indices sérieux de croire que les autorités de l'Etat membre responsable ne permettront pas au demandeur d'asile de se trouver dans une situation permettant, à la suite de sa remise, de jouir des droits fondamentaux découlant de sa qualité et de bénéficier d'une instruction nouvelle de sa demande d'asile.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              	Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la Charte des droits fondamentaux de l'Union européenne ;<br/>
              - le règlement (UE) n° 604/2013 du 26 juin 2013 ;<br/>
              - la directive n° 2013/32/UE du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du même code : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. Le droit constitutionnel d'asile, qui a le caractère d'une liberté fondamentale, a pour corollaire le droit de solliciter le statut de réfugié. S'il implique que l'étranger qui sollicite la reconnaissance de la qualité de réfugié soit, en principe, autorisé à demeurer sur le territoire jusqu'à ce qu'il ait été statué sur sa demande, ce droit s'exerce dans les conditions définies par le code de l'entrée et du séjour des étrangers et du droit d'asile. L'article L. 572-1 prévoit que l'étranger dont l'examen de la demande d'asile relève de la responsabilité d'un autre Etat peut faire l'objet d'un transfert vers l'Etat qui est responsable de cet examen en application des dispositions du règlement (UE) n° 604/2013 du 26 juin 2013.<br/>
<br/>
              3. Aux termes du I de l'article L. 572-5 du code de l'entrée et du séjour des étrangers et du droit d'asile dans sa rédaction applicable au litige : " Lorsque la décision de transfert est notifiée sans assignation à résidence ou placement en rétention de l'étranger, le président du tribunal administratif peut être saisi dans le délai de quinze jours suivant la notification de la décision. Aucun autre recours ne peut être introduit contre la décision de transfert. Il est statué dans un délai de quinze jours à compter de la saisine du président du tribunal administratif, selon les conditions prévues à l'article L. 614-5. Toutefois, si en cours d'instance l'étranger est assigné à résidence en application de l'article L. 751-2, ou placé en rétention en application de l'article L. 751-9, il est fait application de l'article L. 572-6 ".<br/>
<br/>
              4. Il résulte de ces dispositions que le législateur a entendu organiser une procédure spéciale applicable au cas où un étranger fait l'objet d'une décision de transfert vers un autre Etat responsable de l'examen de sa demande d'asile. La procédure spéciale prévue par le code de l'entrée et du séjour des étrangers et du droit d'asile présente des garanties au moins équivalentes à celles des procédures régies par le livre V du code de justice administrative, eu égard aux pouvoirs confiés au juge par ces dispositions, aux délais qui lui sont impartis pour se prononcer, et aux conditions de son intervention. Elle est dès lors exclusive de ces procédures. Il n'en va autrement que dans le cas où les modalités selon lesquelles il est procédé à l'exécution d'une décision de transfert emportent des effets qui, en raison de changements dans les circonstances de droit ou de fait survenus depuis l'intervention de cette mesure et après que le juge, saisi sur le fondement de l'article L. 614-7 du code de l'entrée et du séjour des étrangers et du droit d'asile, a statué ou que le délai prévu pour le saisir a expiré, excèdent ceux qui s'attachent normalement à l'exécution d'une telle décision.<br/>
<br/>
              5. Il résulte de l'instruction conduite par le juge des référés du tribunal administratif de Strasbourg que M. A..., ressortissant afghan, a formulé une demande d'asile auprès des services de la préfecture du Bas-Rhin le 20 avril 2021. La consultation du fichier " Eurodac " ayant établi qu'il avait préalablement demandé l'asile auprès des autorités bulgares, une demande de prise en charge a été adressée à celles-ci et a été acceptée le 24 juin 2021, en application du d) du 1 de l'article 18 du règlement (UE) n° 604/2013 du 26 juin 2013. Par un arrêté du 30 juillet 2021, la préfète du Bas-Rhin a décidé de transférer M. A... vers la Bulgarie. Cet arrêté a été exécuté le 27 septembre 2021. M. A... relève appel de l'ordonnance du 5 octobre 2021 par laquelle le juge des référés du tribunal administratif de Strasbourg, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, a rejeté sa demande tendant à ce qu'il soit enjoint à la préfète du Bas-Rhin de prendre les mesures nécessaires pour organiser, aux frais de l'Etat, son retour effectif sur le territoire français.<br/>
<br/>
              6. En premier lieu, M. A..., qui n'a pas contesté la légalité de la décision de transfert, fait valoir que l'exécution de cette décision emporte des effets excédant ceux qui s'attachent normalement à sa mise en exécution. Il se prévaut à ce titre du changement de circonstances lié à la prise du pouvoir en Afghanistan par les Talibans à l'été 2021. S'il fait valoir diverses atteintes à ses libertés fondamentales à raison de son transfert en Bulgarie, notamment en termes de conditions matérielles d'accueil et d'accès à un médecin, il ne fait pas état d'éléments nouveaux sur les conditions d'accueil et de prise en charge dans ce pays depuis la décision de transfert, qui soient en rapport avec le changement de circonstances invoqué.<br/>
<br/>
              7. En deuxième lieu, les dispositions du règlement (UE) n° 604/2013 du 26 juin 2013 et du code de l'entrée et du séjour des étrangers et du droit d'asile, régissant la détermination de l'Etat membre responsable de l'examen d'une demande d'asile et le transfert des demandeurs, doivent être appliquées dans le respect des droits garantis par la Charte des droits fondamentaux de l'Union européenne et la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. Par ailleurs, eu égard au niveau de protection des libertés et des droits fondamentaux dans les Etats membres de l'Union européenne, lorsque la demande de protection internationale a été introduite dans un Etat autre que la France, que cet Etat a accepté de prendre ou de reprendre en charge le demandeur et en l'absence de sérieuses raisons de croire qu'il existe dans cet État membre des défaillances systémiques dans la procédure d'asile et les conditions d'accueil des demandeurs, qui entraînent un risque de traitement inhumain ou dégradant au sens de l'article 4 de la Charte des droits fondamentaux de l'Union européenne, les craintes dont le demandeur fait état quant au défaut de protection dans cet Etat membre doivent en principe être présumées non fondées, sauf à ce que l'intéressé apporte, par tout moyen, la preuve contraire.<br/>
<br/>
              8. Si M. A... fait valoir que sa demande d'asile a fait l'objet d'un premier rejet, que le taux d'admission au statut de réfugié de demandeurs d'asile afghans serait plus faible en Bulgarie que dans d'autres Etats membres et que les autorités bulgares ne sont pas au nombre de celles ayant fait connaître leur intention de suspendre tout renvoi vers l'Afghanistan au vu de la dégradation de la situation dans ce pays, les éléments produits lors de l'instruction menée par le juge des référés du tribunal administratif de Strasbourg et au soutien de sa requête d'appel ne permettent pas de caractériser des raisons sérieuses de croire qu'il existe en Bulgarie des défaillances systémiques dans le traitement des demandeurs d'asile. Dans ces conditions et en tout état de cause, c'est à bon droit que le premier juge a estimé qu'il n'appartenait pas à la préfète du Bas-Rhin de s'assurer auprès des autorités bulgares des conditions de réexamen de la demande d'asile de l'intéressé. <br/>
<br/>
              9. En troisième lieu, si M. A... conteste les conditions dans lesquelles il a été maintenu en rétention jusqu'à l'exécution de l'arrêté de transfert, et ce malgré l'ordonnance du juge de libertés et de la détention ayant prononcé sa remise en liberté, une telle circonstance se rapporte, ainsi que l'a jugé le juge des référés du tribunal, par une ordonnance qui n'est pas entachée d'omission à statuer, aux atteintes par la mesure de rétention à la liberté d'aller et venir de l'intéressé et à son droit au recours effectif contre la mesure privative de liberté. Elle est par elle-même sans incidence sur la possibilité légale de procéder à l'exécution de la décision de transfert et n'impose pas de réexaminer la situation administrative de l'intéressé à ce titre. En tout état de cause, l'injonction demandée par M. A..., que soit ordonné son retour en France, n'est pas au nombre des mesures d'urgence que la situation permet de prendre utilement pour remédier aux atteintes en cause.<br/>
<br/>
              10. Enfin, M. A... demande au juge des référés du Conseil d'Etat d'interpréter le droit national pour préciser l'office du juge des libertés et de la détention quant au contrôle de conformité d'une mesure de placement en rétention administrative avec les stipulations de l'article 3 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. Une telle demande n'est pas au nombre de celles dont peut connaître le juge des référés, statuant sur le fondement de l'article L. 521-2 du code de justice administrative.<br/>
<br/>
              11. Il résulte de tout ce qui précède que, sans qu'il soit besoin de saisir la Cour de justice de l'Union européenne d'une question préjudicielle, il est manifeste que l'appel de M. A... ne peut être accueilli. Par suite, il y a lieu de rejeter sa requête selon la procédure prévue à l'article L. 522-3 du code de justice administrative, y compris les conclusions présentées sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de M. A... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. B... A....<br/>
Copie en sera adressée à la préfète du Bas-Rhin.<br/>
Fait à Paris, le 27 octobre 2021<br/>
Signé : Anne Courrèges<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
