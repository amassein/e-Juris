<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042499822</ID>
<ANCIEN_ID>JG_L_2020_11_000000426526</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/49/98/CETATEXT000042499822.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 04/11/2020, 426526, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-11-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>426526</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Pierre Vaiss</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Raphaël Chambon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:426526.20201104</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 23 décembre 2018 au secrétariat du contentieux du Conseil d'Etat, l'Union syndicale solidaires demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 12 novembre 2018 par laquelle le Premier ministre a refusé sa demande tendant au remplacement de M. A... C..., membre du Conseil économique, social et environnemental ; <br/>
<br/>
              2°) d'enjoindre au Premier ministre de l'autoriser à désigner un représentant en remplacement de M. C... au sein du Conseil économique, social et environnemental dans un délai d'un mois à compter de la décision à intervenir ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - l'ordonnance n° 58-1360 du 29 décembre 1958 ; <br/>
              - le décret n° 84-558 du 4 juillet 1984 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pierre Vaiss, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier que M. C... a été désigné par l'Union syndicale Solidaires le 9 octobre 2015 pour siéger au sein du Conseil économique, social et environnemental. Par courrier du 30 juillet 2018, elle a demandé au Premier ministre de notifier au président du Conseil économique, social et environnemental le remplacement de M. C... par M. B..., estimant qu'il avait perdu la qualité au titre de laquelle il avait été désigné, n'étant plus membre de l'Union, suite à sa radiation du Syndicat national des journalistes. Par courrier du 15 octobre 2018, l'Union syndicale Solidaires a également demandé au Premier ministre de constater la vacance du siège de M. C... et de prendre un décret nommant M. B... en remplacement de M. C.... Par une décision du 12 novembre 2018, dont l'Union syndicale Solidaires demande l'annulation, le Premier ministre a refusé de faire droit à ces demandes. <br/>
<br/>
              2. Aux termes de l'article 9 de l'ordonnance du 29 décembre 1958 portant loi organique relative au Conseil économique, social et environnemental : " Les membres du Conseil économique, social et environnemental sont désignés pour cinq ans. Ils ne peuvent accomplir plus de deux mandats consécutifs. Si, au cours de cette période, un membre du Conseil vient à perdre la qualité au titre de laquelle il a été désigné, il est déclaré démissionnaire d'office et remplacé ". Aux termes de l'article 7 de la même ordonnance : " Le Conseil économique, social et environnemental comprend (...) 1° : (...) soixante-neuf représentants des salariés (...) ". Le décret du 4 juillet 1984, pris, en application de l'article 7 de la même ordonnance, pour préciser la répartition et les conditions de désignation des membres du Conseil économique, social et environnemental, a prévu en son article 2 que les soixante-neuf représentants des salariés seraient désignés pour deux d'entre eux par l'Union syndicale Solidaires. Selon l'article 15 du même décret, il revient au Premier ministre de transmettre au président du Conseil économique, social et environnemental le nom de ces représentants. Enfin, selon l'article 17 de ce même décret, " en cas de vacance d'un siège, par suite de décès, de démission ou pour tout autre cause, il est procédé à la désignation d'un nouveau titulaire dans les conditions où avait été désigné le représentant à remplacer ". <br/>
<br/>
              3. Les membres du Conseil économique, social et environnemental siègent dans cette assemblée consultative comme représentants des différentes activités du pays. Il ressort de l'ensemble des dispositions précitées de l'ordonnance du 29 décembre 1958 et du décret du 4 juillet 1984 qu'alors même que certaines organisations et associations interviennent dans la procédure de désignation des membres nommés au titre de la représentation des diverses activités économiques, sociales et culturelles, ceux-ci ne peuvent, dans l'exercice de leur mandat, être regardés comme les représentants de ces organisations ou associations. Par suite, s'ils peuvent être déclarés démissionnaires d'office dans l'hypothèse, prévue à l'article 9 précité de l'ordonnance du 29 décembre 1958, où ils viendraient à perdre la qualité au titre de laquelle ils ont été désignés, ils ne sauraient voir leur mandat remis en cause par ces organisations ou associations. <br/>
<br/>
              4. Il résulte de ce qui est dit au point précédent que la circonstance que M. C... ait cessé, postérieurement à sa désignation en qualité de membre représentant les salariés au Conseil économique et social par l'Union syndicale Solidaires, d'être membre de cette Union, n'a pu lui faire perdre la qualité de représentant des salariés, au titre de laquelle il avait été désigné par cette organisation syndicale. Par suite, le Premier ministre n'a commis aucune erreur de droit en refusant de notifier au président du Conseil économique, social et environnemental la désignation de M. B... en remplacement de M. C..., ou de prendre un décret pour procéder à ce remplacement. Dès lors, l'Union syndicale Solidaires n'est pas fondée à demander l'annulation de la décision du 12 novembre 2018 du Premier ministre. Ses conclusions à fin d'injonction doivent, par voie de conséquence, être rejetées, ainsi que ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de l'Union syndicale Solidaires est rejetée.<br/>
Article 2 : La présente décision sera notifiée à l'Union syndicale Solidaires et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
