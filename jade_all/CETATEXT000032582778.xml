<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032582778</ID>
<ANCIEN_ID>JG_L_2016_05_000000393692</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/58/27/CETATEXT000032582778.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 25/05/2016, 393692, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-05-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>393692</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP VINCENT, OHL ; SCP RICHARD</AVOCATS>
<RAPPORTEUR>M. Frédéric Dieu</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:393692.20160525</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              La société civile immobilière (SCI) du Bien Tombé a demandé au tribunal administratif d'Amiens, à titre principal, de condamner le syndicat intercommunal pour l'aménagement de l'Oise moyenne et de ses affluents à lui verser une somme de 1 947 500 euros en réparation des préjudices subis du fait du comblement de l'étang lui appartenant et d'enjoindre à ce syndicat de modifier les aménagements réalisés afin de faire cesser les désordres et de procéder aux travaux nécessaires au rétablissement d'une ligne d'eau suffisante dans l'étang, à titre subsidiaire, de reconnaître la responsabilité de l'Etat du fait de la carence du préfet de l'Aisne dans l'exercice de ses pouvoirs de police des cours d'eau non domaniaux.<br/>
<br/>
              Par un jugement avant-dire droit n° 1202548 du 13 mai 2014, le tribunal administratif d'Amiens a déclaré le syndicat intercommunal pour l'aménagement de l'Oise moyenne et de ses affluents responsable de la moitié des désordres affectant l'étang de Châtillon-sur-Oise et ordonné une expertise avant de statuer sur l'indemnisation des préjudices subis par la SCI du Bien Tombé.<br/>
<br/>
              Par un arrêt n° 14DA01182 du 21 juillet 2015, la cour administrative d'appel de Douai a rejeté l'appel formé par le syndicat ainsi que l'appel incident présenté par la SCI du Bien Tombé.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 23 septembre et 9 décembre 2015 au secrétariat du contentieux du Conseil d'Etat, le syndicat intercommunal pour l'aménagement de l'Oise moyenne et de ses affluents demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de la SCI du Bien Tombé le versement d'une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'environnement ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Dieu, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Vincent, Ohl, avocat du syndicat intercommunal pour l'aménagement de l'Oise moyenne et de ses affluents et à la SCP Richard, avocat de la société civile immobilière (SCI) du Bien Tombé ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que, durant l'hiver 1982-1983, une partie des eaux de l'Oise s'est déversée accidentellement dans l'étang de Châtillon-sur-Oise, à la suite de la rupture de la digue le séparant de la rivière ; qu'en application d'une convention du 30 août 1986, conclue entre les anciens propriétaires du plan d'eau et le syndicat intercommunal pour l'aménagement de l'Oise moyenne et de ses affluents, chargé de l'entretien du cours d'eau, des travaux de rétablissement du cours initial du lit de l'Oise ont été entrepris ; que le syndicat a supprimé en 2010 un barrage de bastaings sur la rivière, qu'il avait installé pour réguler le niveau de l'étang ; que la SCI du Bien Tombé, qui a acquis ce plan d'eau le 30 octobre 2009, a demandé au syndicat de remédier aux désordres résultant de cette intervention, au motif qu'elle aurait eu pour effet d'abaisser encore le niveau de l'eau de l'étang et d'aggraver en conséquence le phénomène de sédimentation, et de l'indemniser des préjudices subis du fait des travaux publics ainsi réalisés ; que le syndicat se pourvoit en cassation contre l'arrêt du 21 juillet 2015 par lequel la cour administrative d'appel de Douai a confirmé le jugement du 13 mai 2014 par lequel le tribunal administratif d'Amiens, saisi par la SCI, a retenu la responsabilité du syndicat à hauteur de la moitié des préjudices subis par cette dernière et ordonné une expertise afin d'évaluer ceux-ci ; que la SCI du Bien Tombé demande, par la voie du pourvoi incident, l'annulation de cet arrêt en tant qu'il a laissé à sa charge 50 % de la réparation du préjudice ;<br/>
<br/>
              Sur le pourvoi principal du syndicat intercommunal pour l'aménagement de l'Oise moyenne et de ses affluents :<br/>
<br/>
              2. Considérant que, pour confirmer la responsabilité du syndicat intercommunal pour l'aménagement de l'Oise moyenne et de ses affluents, la cour administrative d'appel de Douai a retenu, contrairement au tribunal administratif d'Amiens, que la SCI du Bien Tombé avait la qualité de tiers par rapport aux ouvrages publics constitués par les aménagements des berges de l'Oise et aux travaux publics entrepris sur ces ouvrages, et non celle d'usager de ces mêmes ouvrages ; qu'elle en a déduit que la responsabilité du syndicat ne pouvait être engagée qu'à raison, non d'une faute de sa part, mais du caractère anormal et spécial du préjudice subi par la SCI du Bien Tombé ;<br/>
<br/>
              3. Considérant toutefois que si la cour a retenu l'existence d'un lien de causalité entre les travaux entrepris par le syndicat et les préjudices subis par la SCI du Bien Tombé, elle s'est abstenue de préciser les éléments sur lesquels elle se fondait pour estimer que le préjudice subi par cette dernière revêtait un caractère anormal et spécial de nature à engager la responsabilité sans faute du syndicat ; qu'en statuant ainsi, la cour a entaché son arrêt d'une insuffisance de motivation et commis une erreur de droit ; que, par suite et sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, le syndicat intercommunal pour l'aménagement de l'Oise moyenne et de ses affluents est fondé à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              Sur le pourvoi incident de la SCI du Bien Tombé :<br/>
<br/>
              4. Considérant que l'annulation, sur le pourvoi principal du syndicat intercommunal pour l'aménagement de l'Oise moyenne et de ses affluents, de l'ensemble de l'arrêt de la cour administrative d'appel de Douai prive d'objet le pourvoi incident de la SCI du Bien Tombé dirigé contre cet arrêt en tant seulement qu'il a statué sur le montant de l'indemnité lui étant due par le syndicat ; <br/>
<br/>
              5. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire application des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Douai du 21 juillet 2015 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Douai.<br/>
Article 3 : Il n'y a pas lieu de statuer sur le pourvoi incident de la SCI du Bien Tombé.<br/>
Article 4 : Les conclusions présentées par le syndicat intercommunal pour l'aménagement de l'Oise moyenne et de ses affluents et par la SCI du Bien Tombé au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée au syndicat intercommunal pour l'aménagement de l'Oise moyenne et de ses affluents, à la société civile immobilière du Bien Tombé et à la ministre de l'environnement, de l'énergie et de la mer, chargée des relations internationales sur le climat.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
