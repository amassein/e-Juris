<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038472124</ID>
<ANCIEN_ID>JG_L_2019_05_000000430377</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/47/21/CETATEXT000038472124.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 06/05/2019, 430377, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-05-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>430377</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Thibaut Félix</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:430377.20190506</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
<br/>
              Vu la procédure suivante : <br/>
<br/>
              Par une requête, enregistrée le 4 mai 2019 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat de déclarer irrégulière la déclaration de candidature à l'élection des représentants de la France au Parlement européen, présentée pour la liste intitulée " Une Europe au service des Peuples ", conduite par M. B...D....<br/>
<br/>
<br/>
<br/>
	Vu la déclaration de candidature ; <br/>
	Vu les autres pièces du dossier ;<br/>
Vu :<br/>
- la loi n° 77-729 du 7 juillet 1977 ;<br/>
- la loi n° 2018-51 du 31 janvier 2018 ;<br/>
- le décret n° 79-160 du 28 février 1979 ; <br/>
- le code électoral ;<br/>
	- le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thibaut Félix, auditeur,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Aux termes de l'article 9 de la loi du 7 juillet 1977 relative à l'élection des représentants au Parlement européen, dans sa rédaction issue de la loi du 31 janvier 2018 relative aux modalités de dépôt de candidature aux élections : " I. - La déclaration de candidature résulte du dépôt au ministère de l'intérieur d'une liste comprenant un nombre de candidats égal au nombre de sièges à pourvoir. La liste est composée alternativement d'un candidat de chaque sexe. / La déclaration de candidature est faite collectivement pour chaque liste par le candidat tête de liste ou par un mandataire désigné par lui. Elle est accompagnée de la copie d'un justificatif d'identité de chacun des candidats. / Elle comporte la signature de chaque candidat et indique expressément : / 1° Le titre de la liste ; / 2° Les nom, prénoms, sexe, date et lieu de naissance, nationalité, domicile et profession de chacun des candidats. / A la suite de sa signature, chaque candidat appose la mention manuscrite suivante : "La présente signature marque mon consentement à me porter candidat à l'élection au Parlement européen sur la liste menée par (indication des nom et prénoms du candidat tête de liste)." ". Selon le premier alinéa de l'article 12 de la même loi : " Si une déclaration de candidatures ne remplit pas les conditions prévues aux articles 7 à 10, le ministre de l'intérieur saisit dans les vingt-quatre heures le Conseil d'Etat, qui statue dans les trois jours. / Si, en application de cette disposition, une liste n'est plus complète, elle dispose d'un délai de quarante-huit heures pour se compléter ".<br/>
<br/>
              2. Il résulte de ces dispositions que chaque candidat figurant sur une liste de candidats pour l'élection des représentants au Parlement européen doit signer la déclaration de candidature, et y apposer la mention manuscrite qu'elles prévoient, qui exprime son consentement à se présenter aux suffrages et dont la rédaction ne peut pas être déléguée à un tiers. <br/>
<br/>
              3. La liste intitulée " Une Europe au service des Peuples ", conduite par M. B... D...et déposée au ministère de l'intérieur le 3 mai 2019, comporte soixante-dix-neuf candidats qui ont joint à la déclaration de candidature collective un formulaire signé, sur lequel est apposée la mention manuscrite : " La présente signature marque mon consentement à me porter candidat à l'élection au Parlement européen sur la liste menée par Nagib D...". Il résulte toutefois de l'instruction d'une part qu'une même personne a rédigé plusieurs des mentions manuscrites à la suite de la signature du candidat, d'autre part que Mme C...A..., candidate sur cette liste, n'a pas signé la déclaration de candidature. <br/>
<br/>
              4. Il résulte ce qui précède que le ministre de l'intérieur est fondé à soutenir qu'en l'état où elle a été déposée, la déclaration de candidature de la liste intitulée " Une Europe au service des Peuples " ne remplit pas les conditions prévues par la loi du 7 juillet 1977.<br/>
<br/>
              5. La présente décision ne fait pas obstacle à ce que, sur le fondement du second alinéa de l'article 12 de la même loi et dans le délai qu'il fixe, la déclaration de candidature de la liste intitulée " Une Europe au service des Peuples " soit complétée par M. B... D...ou par le mandataire désigné en application de l'article 9.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La déclaration de candidature à l'élection des représentants de la France au Parlement européen déposée le 3 mai 2019 pour la liste intitulée " Une Europe au service des Peuples " ne remplit pas les conditions prévues par la loi du 7 juillet 1977. <br/>
Article 2 : La présente décision sera notifiée au ministre de l'intérieur et à M. B...D....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
