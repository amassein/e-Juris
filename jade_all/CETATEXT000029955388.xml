<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029955388</ID>
<ANCIEN_ID>JG_L_2014_12_000000373115</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/95/53/CETATEXT000029955388.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  4ème sous-section jugeant seule, 23/12/2014, 373115, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-12-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>373115</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 4ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI ; FOUSSARD ; SCP BARTHELEMY, MATUCHANSKY, VEXLIARD, POUPOT</AVOCATS>
<RAPPORTEUR>M. Philippe Orban</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:373115.20141223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 4 novembre et 6 décembre 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme A...B..., demeurant ... ; Mme B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision n° 4943 du 19 septembre 2013 de la section des assurances sociales du Conseil national de l'ordre des médecins en tant qu'elle a prononcé à son encontre la sanction de l'interdiction du droit de donner des soins aux assurés sociaux pendant une durée de six mois, dont trois mois avec sursis, et l'a condamnée à verser à la caisse primaire d'assurance maladie de la Gironde la somme de 1 693,21 euros ;<br/>
<br/>
              2°) réglant l'affaire au fond, d'annuler la décision n° 331 du 5 janvier 2012 de la section des assurances sociales du conseil régional de l'ordre des médecins d'Aquitaine ;<br/>
<br/>
              3°) de mettre à la charge de la caisse primaire d'assurance maladie de la Gironde une somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative ainsi qu'une somme de 35 euros au titre de l'article R. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la sécurité sociale ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Philippe Orban, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, avocat de MmeB..., à Me Foussard, avocat de la caisse primaire d'assurance maladie de la Gironde et à la SCP Barthélemy, Matuchansky, Vexliard, Poupot, avocat du Conseil national de l'ordre des médecins ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article R. 145-8 du code de la sécurité sociale en vigueur à la date de la plainte de la caisse primaire d'assurance maladie de la Gironde : " Les dispositions sur le contentieux du contrôle technique des médecins, prévues aux articles L. 145-1 à L. 145-3, L. 145-6 à L. 145-8, R. 145-15 à R. 145-27, sont applicables en cas de fautes, abus, fraudes et tous faits intéressant l'exercice de la profession, relevés à l'encontre des auxiliaires médicaux à l'occasion de soins dispensés aux assurés sociaux. " ;<br/>
<br/>
              2. Considérant que, si l'article L. 145-5-1 du code de la sécurité sociale, dans sa rédaction issue de la loi n° 2006-1668 du 21 décembre 2006, dispose que " Les fautes, abus, fraudes et tous faits intéressant l'exercice de la profession relevés à l'encontre (...) des infirmiers à l'occasion des soins dispensés aux assurés sociaux sont soumis en première instance à (...) une section de la chambre disciplinaire de première instance des infirmiers dites " section des assurances sociales de la chambre disciplinaire de première instance ", cette disposition n'était pas applicable, dans cette rédaction, au jugement de la plainte par la chambre disciplinaire de première instance de l'ordre des médecins dès lors qu'à la date de ce jugement, le décret en Conseil d'Etat édictant les mesures nécessaires à son application et fixant notamment les règles de la procédure, prévu par l'article L. 145-4 du code de la sécurité sociale, n'avait pas été pris, et que par suite, à supposer même que, comme le soutient la requérante, le délai raisonnable pour prendre le décret en Conseil d'Etat édictant les mesures nécessaires à l'application de l'article L. 145-5-1 du code de la sécurité sociale eût expiré à la date à laquelle le juge de première instance s'est prononcé, était resté en vigueur l'article R. 145-8 précité, duquel il résulte que les sections des assurances sociales des instances ordinales de l'ordre des médecins étaient compétentes pour connaître des griefs formulés à l'encontre des infirmiers au titre des soins dispensés aux assurés sociaux ; que le moyen tiré de l'erreur de droit à ne pas avoir soulevé d'office l'incompétence de la section des assurances sociales de la chambre disciplinaire de première instance de l'ordre des médecins d'Aquitaine pour connaître de la plainte de la caisse primaire d'assurance maladie de la Gironde doit par suite être écarté ;<br/>
<br/>
              3. Considérant en revanche qu'aux termes de l'article R. 145-17 du code de la sécurité sociale, devenu l'article R. 145-22 : " Les sections des assurances sociales des conseils régionaux ou interrégionaux des médecins (...) sont saisies (...) dans le délai de trois ans à compter de la date des faits " ; qu'il ressort des pièces du dossier soumis aux juges du fond que les factures relatives aux soins prodigués à Mme L. du 17 au 24 mai 2007 et à M. G. du 18 au 22 mai 2007, qui ont été émises au plus tard le 4 juillet 2007, date du dernier mandatement effectué par la caisse primaire d'assurance maladie de la Gironde à ce titre, étaient antérieures de plus de trois ans à la saisine de la section des assurances sociales de la chambre disciplinaire de première instance, qui est intervenue le 27 septembre 2010 ; qu'ainsi, les éventuelles fautes, abus ou fraudes commis à l'occasion de ces facturations étaient atteints par la forclusion prévue par l'article R. 145-17 du code de la sécurité sociale ; que par suite, la section des assurances sociales du Conseil national de l'ordre des médecins, qui a condamné Mme B...au titre des fraudes commises à l'occasion des facturations concernant Mme L. et M. G., a entaché sa décision d'une erreur de droit en ne relevant pas d'office le moyen, qui ressortait des pièces du dossier qui lui était soumis, tiré de l'irrecevabilité de la plainte de la caisse primaire d'assurance maladie de la Gironde en tant qu'elle portait sur ces faits ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, Mme B...est fondée à demander l'annulation de la décision attaquée ;<br/>
<br/>
              4. Considérant que, dans les circonstances de l'espèce, il y a lieu de mettre à la charge de la caisse primaire d'assurance maladie de la Gironde une somme de 3 000 euros à verser à Mme B...sur le fondement des dispositions des articles L. 761-1 et R. 761-1 du code de justice administrative ; qu'en revanche, les dispositions de l'article L. 761-1 font obstacle à ce qu'une somme soit mise à la charge de MmeB..., qui n'est pas la partie perdante dans la présente instance, au titre des frais exposés par la caisse primaire d'assurance maladie de la Gironde pour les besoins de la présente instance et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La décision de la section des assurances sociales du Conseil national de l'ordre des médecins du 19 septembre 2013 est annulée.<br/>
<br/>
Article 2 : L'affaire est renvoyée devant la section des assurances sociales du Conseil national de l'ordre des médecins.<br/>
<br/>
Article 3 : La caisse primaire d'assurance maladie de la Gironde versera une somme de 3 000 euros à Mme B...au titre des dispositions des articles L. 761-1 et R. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à Mme A...B...et à la caisse primaire d'assurance maladie de la Gironde.<br/>
Copie en sera adressée pour information au Conseil national de l'ordre des médecins.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
