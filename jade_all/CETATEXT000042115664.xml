<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042115664</ID>
<ANCIEN_ID>JG_L_2020_07_000000434506</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/11/56/CETATEXT000042115664.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 03/07/2020, 434506, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-07-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>434506</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>Mme Françoise Tomé</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:434506.20200703</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire complémentaire et un mémoire en réplique, enregistrés les 10 septembre et 10 décembre 2019 et le 8 juin 2020 au secrétariat du contentieux du Conseil d'Etat, M. B... A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 10 juillet 2019 par laquelle le Conseil national de l'ordre des médecins, statuant en formation restreinte, a refusé de l'inscrire au tableau de la Moselle de l'ordre des médecins ; <br/>
<br/>
              2°) de mettre à la charge du Conseil national de l'ordre des médecins la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Françoise Tomé, Conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouvenin, Coudray, Grévy, avocat de M. A... et à la SCP Matuchansky, Poupot, Valdelièvre, avocat du Conseil national de l'ordre des médecins ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier que M. A..., titulaire d'un diplôme de docteur en médecine obtenu en 1986 au Cameroun, d'un diplôme interuniversitaire de spécialité de pédiatrie délivré par l'université Paul Sabatier de Toulouse en 1993 et d'un diplôme interuniversitaire de cardiologie infantile et pédiatrique délivré par l'université de Paris VII en 1995, a été autorisé, par une décision du ministre chargé de la santé du 20 février 2002, à exercer la médecine en France. Inscrit au tableau de l'ordre des médecins entre 2002 et 2007, il a pratiqué la médecine générale dans l'Aveyron. Depuis lors, il exerce en qualité de faisant fonction d'interne dans des établissements publics de santé. En 2018, il a demandé au conseil départemental de la Moselle de l'ordre des médecins à être inscrit au tableau de l'ordre en vue de pouvoir exercer la médecine générale. Par une décision du 18 octobre 2018, le conseil départemental de la Moselle a décidé, sur le fondement de l'article R. 4112-2 du code de la santé publique, de saisir le conseil régional de Lorraine de l'ordre des médecins d'une demande d'expertise relative à une éventuelle insuffisance professionnelle. Au vu de cette expertise, le conseil départemental a, le 21 février 2019, refusé d'inscrire M. A... au tableau de l'ordre, estimant ses compétences professionnelles insuffisantes. Par une décision du 23 mai 2019, le conseil régional Grand Est de l'ordre des médecins a, pour le même motif, également refusé l'inscription sollicitée. M. A... demande l'annulation pour excès de pouvoir de la décision du 10 juillet 2019 par laquelle la formation restreinte du Conseil national de l'ordre des médecins a refusé de l'inscrire au tableau de l'ordre et a décidé qu'une nouvelle demande d'inscription au tableau ne pourrait être acceptée qu'à la condition qu'il justifie avoir suivi une formation de remise à niveau en médecine générale.<br/>
<br/>
              2. Aux termes de l'article L. 4112-1 du code de la santé publique : " Les médecins (...) qui exercent dans un département sont inscrits sur un tableau établi et tenu à jour par le conseil départemental de l'ordre dont ils relèvent. (...) / Nul ne peut être inscrit sur ce tableau s'il ne remplit pas les conditions requises par le présent titre et notamment les conditions nécessaires de moralité, d'indépendance et de compétence ". Aux termes de l'article L. 4112-3 du même code : " Le conseil départemental de l'ordre statue sur la demande d'inscription au tableau dans un délai maximum de trois mois à compter de la réception de la demande, accompagnée d'un dossier complet. / Les modalités selon lesquelles le conseil départemental vérifie que l'intéressé ne présente pas d'insuffisance professionnelle, d'infirmité ou d'état pathologique rendant dangereux l'exercice de la profession sont prévues par décret en Conseil d'Etat ". Aux termes de l'article R. 4112-2 du même code : " I. A la réception de la demande [d'inscription au tableau de l'ordre], le président du conseil départemental désigne un rapporteur parmi les membres du conseil. Ce rapporteur procède à l'instruction de la demande et fait un rapport écrit. / Le conseil vérifie les titres du candidat et demande communication du bulletin n°2 du casier judiciaire de l'intéressé. Il refuse l'inscription si le demandeur est dans l'un des trois cas suivants : (...) / 2°) Il est établi, dans les conditions fixées au II, qu'il ne remplit pas les conditions nécessaires de compétence (...) / II. En cas de doute sérieux sur la compétence professionnelle du demandeur, le conseil départemental saisit, par une décision non susceptible de recours, le conseil régional ou interrégional qui diligente une expertise. Le rapport d'expertise est établi dans les conditions prévues aux II, III, IV, VI et VII de l'article R. 4124-3-5 et il est transmis au conseil départemental. / S'il est constaté, au vu du rapport d'expertise, une insuffisance professionnelle rendant dangereux l'exercice de la profession, le conseil départemental refuse l'inscription et précise les obligations de formation du praticien. La notification de cette décision mentionne qu'une nouvelle demande d'inscription ne pourra être acceptée sans que le praticien ait au préalable justifié avoir rempli les obligations de formation fixées par la décision du conseil départemental (...) ". Enfin, aux termes de l'article R. 4124-3-5 du même code : " (...) II. - La suspension ne peut être ordonnée que sur un rapport motivé établi à la demande du conseil régional ou interrégional dans les conditions suivantes : / 1° Pour les médecins, le rapport est établi par trois médecins qualifiés dans la même spécialité que celle du praticien concerné désignés comme experts, le premier par l'intéressé, le deuxième par le conseil régional ou interrégional et le troisième par les deux premiers experts. Ce dernier est choisi parmi les personnels enseignants et hospitaliers titulaires de la spécialité. Pour la médecine générale, le troisième expert est choisi parmi les personnels enseignants titulaires ou les professeurs associés ou maîtres de conférences associés des universités (...). / IV. Les experts procèdent ensemble, sauf impossibilité manifeste, à l'examen des connaissances théoriques et pratiques du praticien. Le rapport d'expertise est déposé au plus tard dans le délai de six semaines à compter de la saisine du conseil. Il indique les insuffisances relevées au cours de l'expertise, leur dangerosité et préconise les moyens de les pallier par une formation théorique et, si nécessaire, pratique. / Si les experts ne peuvent parvenir à la rédaction de conclusions communes, le rapport comporte l'avis motivé de chacun d'eux (...) ".<br/>
<br/>
              3. En premier lieu, il ressort des pièces du dossier que le conseil départemental de la Moselle de l'ordre des médecins a estimé, au vu de l'entretien entre son président et M. A..., qu'il y avait un doute sérieux sur la compétence professionnelle de M. A... justifiant de saisir le conseil régional en vue qu'il diligente une expertise sur celle-ci dans les conditions prévues par les dispositions de l'article R. 4124-3-5 du code de la santé publique. Dans ces conditions, M. A... n'est pas fondé à soutenir que le recours à cette expertise aurait été décidé dans des conditions méconnaissant celles énoncées aux dispositions du II de l'article R. 4112-2 du même code, ni qu'il résulterait d'un détournement de procédure. <br/>
<br/>
              4. En deuxième lieu, les médecins désignés pour réaliser l'expertise peuvent, à la demande de l'instance ordinale ou de leur propre initiative, faire porter cette expertise sur les éléments qu'ils estiment utiles dans le cadre de la procédure prévue à l'article R. 4124-3-5 du code de la santé publique, dès lors qu'ils sont de nature à les éclairer sur les compétences professionnelles du praticien qui en est l'objet. Dès lors, M. A... n'est pas fondé à soutenir que les experts qui ont procédé à l'examen de ses connaissances théoriques et pratiques ne pouvaient faire porter les questions qu'ils lui ont posées sur un cas pratique ou sur des connaissances médicales générales dès lors qu'elles étaient de nature à les éclairer sur son aptitude à exercer en qualité de médecin généraliste.  <br/>
<br/>
              5. En troisième lieu, il ressort des pièces du dossier qu'en estimant, au vu notamment du rapport d'expertise établi conformément aux dispositions de l'article R. 4124-3-5 du code de santé publique, lequel concluait que la reprise par M. A... d'un exercice en médecine générale n'était pas possible en l'état de ses compétences professionnelles, alors, notamment, qu'il avait des lacunes dans certains domaines de la médecine générale et qu'il adoptait des stratégies thérapeutiques non conformes aux recommandations de bonnes pratiques, que M. A... présente une insuffisance professionnelle rendant dangereux qu'il exerce comme médecin généraliste, la formation restreinte du Conseil national de l'ordre des médecins a fait une exacte application des dispositions du code de la santé publique mentionnées au point 2. <br/>
<br/>
              6. En dernier lieu, en imposant à M. A... de suivre, avant de présenter une nouvelle demande d'inscription au tableau de l'ordre en qualité de médecin généraliste, une formation de remise à niveau dans le cadre du diplôme interuniversitaire de médecine générale, se traduisant soit par l'obtention du diplôme, soit par une évaluation sous forme d'une attestation établie par le responsable du diplôme constatant l'assiduité à l'enseignement et procédant à une évaluation théorique des acquis, la formation restreinte du Conseil national de l'ordre des médecins n'a pas entaché sa décision d'erreur de droit. <br/>
<br/>
              7. Il résulte de tout ce qui précède que M. A... n'est pas fondé à demander l'annulation de la décision qu'il attaque. Les dispositions de l'article L. 761-1 du code de justice administrative font, par suite, obstacle à ce qu'une somme soit mise, à ce titre, à la charge du Conseil national de l'ordre des médecins, qui n'est pas, dans la présente instance, la partie perdante. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par le Conseil national de l'ordre des médecins au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. A... est rejetée.<br/>
Article 2 : Les conclusions présentées par le Conseil national de l'ordre des médecins au titre de l'article L. 761-1 du code de la justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. B... A... et au Conseil national de l'ordre des médecins.<br/>
Copie en sera adressée au ministre des solidarités et de la santé. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
