<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042311086</ID>
<ANCIEN_ID>JG_L_2020_09_000000443155</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/31/10/CETATEXT000042311086.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 02/09/2020, 443155, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-09-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>443155</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:443155.20200902</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. C... B... a demandé au juge des référés du tribunal administratif de Versailles, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'ordonner la suspension de l'exécution de la décision du préfet de l'Essonne du 15 juin 2020 accordant le concours de la force publique pour son expulsion des locaux de son habitation principale située 21 rue d'Estienne d'Orves à Bretigny-sur-Orge.<br/>
<br/>
              Par une ordonnance n° 2005254 du 20 août 2020, le juge des référés du tribunal administratif de Versailles a rejeté sa demande. <br/>
<br/>
              Par une requête, un mémoire complémentaire et un mémoire en réplique, enregistrés les 21, 25 et 31 août 2020 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de faire droit à ses demandes de première instance.<br/>
<br/>
<br/>
<br/>
              Il soutient que : <br/>
              - la condition d'urgence est remplie, dès lors que l'exécution de la décision du préfet de l'Essonne accordant l'octroi de la force publique en vue de son expulsion aurait des conséquences manifestement excessives par rapport au but poursuivi, eu égard notamment à la perte de son emploi suite à la liquidation judiciaire de sa société et à la réduction de ses revenus en raison de la fermeture des établissements d'enseignement dans lesquels il effectue des vacations, à son état de santé et aux difficultés de se reloger en raison de l'épidémie du virus covid-19 ;<br/>
              - la décision contestée est entachée d'illégalité externe en ce qu'elle ne mentionne pas les voies et délais de recours, qu'elle ne lui a pas été notifiée, qu'elle est insuffisamment motivée, qu'il n'a pas été préalablement convoqué devant les services préfectoraux et qu'il n'a pas été fait droit à ses demandes de présenter des observations orales en se faisant représenter par un conseil ;<br/>
              - elle porte une atteinte grave et manifestement illégale au droit au logement et à la dignité de la personne humaine ;<br/>
              - elle est entachée d'erreur manifeste d'appréciation, eu égard en particulier à la situation sanitaire, à son état de santé et à sa situation financière, à la procédure pendante devant la cour d'appel de Paris qui ne permet pas de considérer que le jugement par lequel le tribunal d'instance de Longjumeau du 27 septembre 2018 a prononcé son expulsion et l'a condamné au remboursement de sommes impayées est définitif, à l'absence d'obligation faite à son encontre de régler les sommes dues ou de respecter un échéancier de remboursement et à l'absence de solution de relogement.<br/>
<br/>
              Par un mémoire en défense, enregistré le 31 août 2020, le ministre de l'intérieur conclut au rejet de la requête. Il soutient que la condition d'urgence n'est pas remplie, dès lors que M. B... a indiqué le 26 août 2020 son intention de quitter volontairement les lieux à compter du 1er septembre 2020, et qu'aucune atteinte n'est portée à une liberté fondamentale.<br/>
<br/>
              La requête a été transmise pour observations à la ministre de la transition écologique, qui n'a pas produit de mémoire.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code des procédures civiles d'exécution ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. B..., et d'autre part, le ministre de l'intérieur et la ministre de la transition écologique ;<br/>
<br/>
              Ont été entendus lors de l'audience publique du 1er septembre 2020, à 14 heures : <br/>
<br/>
              - Me Froger, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. B... ;<br/>
              - M. B... ;<br/>
<br/>
              - les représentantes du ministre de l'intérieur ;<br/>
<br/>
              à l'issue de laquelle le juge des référés a prononcé la clôture de l'instruction.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". <br/>
<br/>
              2. Aux termes de l'article L. 153-1 du code des procédures civiles d'exécution : " L'Etat est tenu de prêter son concours à l'exécution des jugements et des autres titres exécutoires. Le refus de l'Etat de prêter son concours ouvre droit à réparation ". Il résulte de ces dispositions que le représentant de l'Etat, saisi d'une demande en ce sens, doit prêter le concours de la force publique en vue de l'exécution des décisions de justice ayant force exécutoire. Seules des considérations impérieuses tenant à la sauvegarde de l'ordre public, ou des circonstances postérieures à une décision de justice ordonnant l'expulsion d'occupants d'un local, faisant apparaître que l'exécution de cette décision serait de nature à porter atteinte à la dignité de la personne humaine, peuvent légalement justifier, sans qu'il soit porté atteinte au principe de la séparation des pouvoirs, le refus de prêter le concours de la force publique. <br/>
<br/>
              3. Il résulte de l'instruction que, par un jugement du 27 septembre 2018, dont l'exécution provisoire a été ordonnée, le tribunal d'instance de Longjumeau a constaté la résiliation de plein droit du contrat de location conclu le 9 juin 2016 entre M. et Mme A..., d'une part, et M. C... B..., d'autre part, portant sur le logement situé 21 rue d'Estienne d'Orves à Brétigny-sur-Orge, ordonné le départ des locataires et de tous occupants de leur chef dans un délai de deux mois à compter de la signification du jugement et rappelé qu'à défaut, ils pourraient être expulsés par la force publique dans les deux mois de la signification du commandement, le montant de la dette locative étant arrêté fin décembre 2017 à la somme de 10 700,35 euros. Par une ordonnance du 20 août 2020, le juge des référés du tribunal administratif de Versailles, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, a rejeté la demande de M. B... tendant à la suspension de l'exécution de la décision du 15 juin 2020 par laquelle le préfet de l'Essonne a autorisé, à compter du 11 juillet 2020, la commissaire de police d'Arpajon à assister l'huissier en vue de l'exécution forcée du jugement du 27 septembre 2018. M. B... relève appel de cette ordonnance. <br/>
<br/>
              4. Par l'ordonnance attaquée, le juge des référés du tribunal administratif de Versailles a, d'une part, rappelé qu'il n'appartient pas au juge administratif d'apprécier la régularité ou le bien-fondé du jugement d'un tribunal d'instance et, d'autre part, constaté que M. B... ne justifiait pas de circonstances postérieures au jugement du tribunal d'instance de Longjumeau, qu'elles soient relatives à ses ressources financières ou à son état de santé, faisant apparaître que l'exécution de la décision du préfet serait de nature à porter atteinte à la dignité de la personne humaine.<br/>
<br/>
              5. En premier lieu, aucun des moyens de légalité externe soulevés par M. B... n'est, en tout état de cause, susceptible de caractériser une illégalité manifeste dont serait entachée la décision du préfet de l'Essonne.<br/>
<br/>
              6. En second lieu, sur le fond, M. B... se borne, pour l'essentiel, à reprendre en appel son argumentation de première instance qui, compte tenu de l'ensemble des éléments du dossier, a été écartée à bon droit par le juge des référés du tribunal administratif de Versailles. Il soutient, il est vrai, en appel que celui-ci aurait inexactement interprété ses écritures et ses déclarations orales en estimant que ses revenus lui permettaient de trouver une solution d'hébergement temporaire, alors qu'il ne dispose que d'une promesse d'embauche avec prise d'effet au 14 septembre 2020 et qu'il est en réalité, à l'heure actuelle, dépourvu de toutes ressources, de même que sa conjointe. Il fait également valoir qu'il recherche activement un logement et que, compte tenu de sa nouvelle activité professionnelle, il aura, selon toute vraisemblance, quitté son logement actuel au plus tard dans les prochaines semaines. Toutefois, ces circonstances, à les supposer établies, ne suffisent pas à faire regarder l'exécution de la décision du préfet d'accorder le concours de la force publique à l'expulsion de M. B... comme de nature à porter une atteinte grave et manifestement illégale à la dignité de la personne humaine, d'autant que les représentants du ministre de l'intérieur ont indiqué à l'audience qu'une solution d'hébergement provisoire serait proposée à M. B... et à sa conjointe dans le cadre du " Samu Social ".  <br/>
<br/>
              7. Il résulte de ce qui précède qu'il y a lieu de rejeter la requête de M. B..., <br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. B... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. C... B... et au ministre de l'intérieur.<br/>
Copie en sera adressée à la ministre de la transition écologique.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
