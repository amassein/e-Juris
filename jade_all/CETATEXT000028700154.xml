<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028700154</ID>
<ANCIEN_ID>JG_L_2014_03_000000374288</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/70/01/CETATEXT000028700154.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 07/03/2014, 374288, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-03-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>374288</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE NERVO, POUPET</AVOCATS>
<RAPPORTEUR>Mme Sophie Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:374288.20140307</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance n° 1304309 du 23 décembre 2013, enregistrée le 30 décembre 2013 au secrétariat du contentieux du Conseil d'Etat, par laquelle le vice-président de la 7ème section du tribunal administratif de Paris, avant qu'il soit statué sur la demande de la Fédération environnement durable et autres tendant à l'annulation de l'arrêté du 28 septembre 2012 par lequel le préfet de la région Ile-de-France, préfet de Paris, a approuvé le schéma régional éolien d'Ile-de-France, a décidé, par application des dispositions de l'article 23-2 de l'ordonnance         n° 58-1067 du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution des dispositions des articles L. 222-1 à L. 222-3 du code de l'environnement, dans leur rédaction issue de la loi n° 2010-788 du 12 juillet 2010 portant engagement national pour l'environnement ;<br/>
<br/>
              Vu le mémoire, enregistré le 25 novembre 2013 au greffe du tribunal administratif de Paris, présenté pour la Fédération environnement durable, dont le siège est 3, rue des Eaux à Paris (75016), la Fédération nationale des associations de sauvegarde des sites, l'association Ligue urbaine et rurale, dont le siège est 20, rue du Borrego à Paris (75020), la Société pour la protection des paysages et l'esthétique de la France, dont le siège est 39, avenue de la Motte-Picquet à Paris (75007), l'association de défense de l'environnement et de la région d'Egreville, dont le siège est " Les Canas " à Egreville (77620), l'association Vent de colère en Visandre, dont le siège est 9, rue du Lavoir à Pécy (77970), l'association Vent de force 77, dont le siège est 17, rue du Poirier Coral à Saâcy-sur-Marne (77730), l'association Vent de vérité, dont le siège est Le Montcel à Verdelot (77510), en application de l'article 23-1 de l'ordonnance      n° 58-1067 du 7 novembre 1958 ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule et son article 61-1 ; <br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ; <br/>
<br/>
              Vu le code de l'environnement, notamment ses articles L. 222-1 à L. 222-3 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie Roussel, auditeur,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP de Nervo, Poupet, avocat de la Fédération environnement durable et autres ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2. Considérant que I de l'article L. 222-1 du code de l'environnement, qui définit le contenu du schéma régional du climat, de l'air et de l'énergie, dispose que : " (...) Ce schéma fixe, à l'échelon du territoire régional et à l'horizon 2020 et 2050 : / 1° Les orientations permettant d'atténuer les effets du changement climatique et de s'y adapter, conformément à l'engagement pris par la France, à l'article 2 de la loi n° 2005-781 du 13 juillet 2005 de programme fixant les orientations de la politique énergétique, de diviser par quatre ses émissions de gaz à effet de serre entre 1990 et 2050, et conformément aux engagements pris dans le cadre européen. A ce titre, il définit notamment les objectifs régionaux en matière de maîtrise de l'énergie ; / 2° Les orientations permettant, pour atteindre les normes de qualité de l'air mentionnées à l'article L. 221-1, de prévenir ou de réduire la pollution atmosphérique ou d'en atténuer les effets. A ce titre, il définit des normes de qualité de l'air propres à certaines zones lorsque les nécessités de leur protection le justifient ; / 3° Par zones géographiques, les objectifs qualitatifs et quantitatifs à atteindre en matière de valorisation du potentiel énergétique terrestre, renouvelable et de récupération et en matière de mise en oeuvre de techniques performantes d'efficacité énergétique telles que les unités de cogénération, notamment alimentées à partir de biomasse, conformément aux objectifs issus de la législation européenne relative à l'énergie et au climat. A ce titre, le schéma régional du climat, de l'air et de l'énergie vaut schéma régional des énergies renouvelables au sens du III de l'article 19 de la loi              n° 2009-967 du 3 août 2009 de programmation relative à la mise en oeuvre du Grenelle de l'environnement. Un schéma régional éolien qui constitue un volet annexé à ce document définit, en cohérence avec les objectifs issus de la législation européenne relative à l'énergie et au climat, les parties du territoire favorables au développement de l'énergie éolienne. " ; que l'article L. 222-2 du même code prévoit que le projet de schéma régional du climat, de l'air et de l'énergie est, " après avoir été mis pendant une durée minimale d'un mois à la disposition du public sous des formes, notamment électroniques, de nature à permettre sa participation ", soumis à l'approbation du conseil régional puis arrêté par le préfet de région ; que ce même article définit les conditions dans lesquelles le plan climat-énergie territorial peut être intégré au schéma régional du climat, de l'air et de l'énergie, celles dans lesquelles ce schéma peut être révisé ainsi que ses modalités particulières d'adoption en Corse ; qu'enfin, l'article L. 222-3 du code précise le calendrier d'adoption des schémas régionaux du climat, de l'air et de l'énergie et renvoie à des décrets en Conseil d'Etat les modalités d'application des articles L. 222-1 et         L. 222-2 ; <br/>
<br/>
              3. Considérant que la Fédération nature environnement et autres soutiennent, à l'appui du recours pour excès de pouvoir qu'ils ont formé contre l'arrêté du 28 septembre 2012 par lequel le préfet de la région Ile-de-France a approuvé le schéma régional éolien d'Ile-de-France, que les articles L. 222-1 à L. 222-3 du code de l'environnement portent atteinte aux droits et libertés garantis par la Constitution ; que ces dispositions sont applicables au litige dont le tribunal administratif de Paris est saisi, au sens et pour l'application de l'article 23-4 de l'ordonnance du 7 novembre 1958 ; que ces dispositions n'ont pas déjà été déclarées conformes à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel ;<br/>
<br/>
              4. Considérant qu'à l'appui de la question prioritaire de constitutionnalité qu'ils soulèvent, les requérants soutiennent notamment que, faute de prévoir des modalités suffisantes d'information et de participation du public lors de l'élaboration des schémas régionaux du climat, de l'air et de l'énergie et des schémas régionaux de l'éolien qui y sont annexés, les dispositions des articles L. 222-1 à L. 222-3 du code de l'environnement méconnaissent le droit de toute personne " de participer à l'élaboration des décisions publiques ayant une incidence sur l'environnement ", énoncé à l'article 7 de Charte de l'environnement ; que ce moyen soulève une question présentant un caractère sérieux ; qu'il y a lieu, par suite, de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La question de la conformité à la Constitution des articles L. 222-1 à L. 222-3 du code de l'environnement, dans leur rédaction issue de la loi n° 2010-788 du 12 juillet 2010 portant engagement national pour l'environnement, est renvoyée au Conseil constitutionnel.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la Fédération environnement durable, premier requérant dénommé et au ministre de l'écologie, du développement durable et de l'énergie.<br/>
Les autres requérants seront informés de la présente décision par la SCP de Nervo et Poupet, avocat au Conseil d'Etat et à la Cour de cassation, qui les représente devant le Conseil d'Etat. <br/>
Copie en sera adressée au Premier ministre ainsi qu'au tribunal administratif de Paris. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
