<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038742979</ID>
<ANCIEN_ID>JG_L_2019_06_000000430903</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/74/29/CETATEXT000038742979.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 04/06/2019, 430903, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-06-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>430903</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2019:430903.20190604</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au juge des référés du tribunal administratif de Bordeaux, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre à l'Etat et au département de la Gironde, de lui indiquer un lieu d'hébergement stable pour les accueillir avec sa fille. Par une ordonnance n° 1902143 du 7 mai 2019, le juge des référés du tribunal administratif de Bordeaux a enjoint au préfet de la Gironde d'indiquer à Mme B... un hébergement d'urgence pour elle et sa fille dans les quarante-huit heures à compter de son ordonnance.<br/>
<br/>
              Par une requête, enregistrée le 21 mai 2019 au secrétariat du contentieux du Conseil d'Etat, la ministre des solidarités et de la santé demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de rejeter les conclusions de première instance présentées par Mme B... à l'encontre de l'Etat.<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - la prise en charge de Mme B...et de sa fille relève de la compétence du département de la Gironde au titre de l'aide sociale à l'enfance ;<br/>
              - il appartient au département de la Gironde de garantir la prise en charge de Mme B...et de sa fille dès lors que l'intervention de l'Etat ne revêt qu'un caractère supplétif ;<br/>
              - l'obligation de mise à l'abri des femmes isolées avec un enfant de moins de trois ans qui incombe au département n'est pas conditionnée au recueil d'une information préoccupante relative à la situation de l'enfant en danger ;<br/>
              - nonobstant l'accroissement du nombre de places d'hébergement pour les personnes sans abri, le dispositif d'hébergement mis en oeuvre par l'Etat dans le département de la Gironde demeure sous tension. <br/>
<br/>
              Par un mémoire en défense, enregistré le 27 mai 2019, Mme B...conclut, à titre principal, au rejet de la requête, à titre subsidiaire, en premier lieu, à ce qu'il soit fait injonction à l'Etat, dans l'attente de l'octroi d'un hébergement stable par le département de la Gironde, de maintenir sa prise en charge actuelle, en second lieu, à ce qu'il soit fait injonction au département de la Gironde de lui attribuer un hébergement stable, et en toute hypothèse, d'une part, de l'admettre au bénéfice de l'aide juridictionnelle provisoire et, d'autre part, à ce que soit mis à la charge de l'Etat et/ou du département de la Gironde, la somme de 2 500 euros en application des dispositions de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991.<br/>
<br/>
              La requête a été communiquée au département de la Gironde qui n'a pas produit d'observations.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'action sociale et des familles ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, Mme B... et, d'autre part, la ministre des solidarités et de la santé ainsi que le président du conseil départemental de la Gironde ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 31 mai 2019 à 11 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Rousseau avocat au Conseil d'Etat et à la Cour de cassation, avocat de Mme B...;<br/>
<br/>
              - les représentants de la ministre des solidarités et de la santé ;<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures".<br/>
<br/>
              2. Il résulte de l'instruction menée par le juge des référés du tribunal administratif de Bordeaux que MmeB..., de nationalité camerounaise, est arrivée en France en 2016 et a noué une relation avec un ressortissant français dont elle a eu une fille née en mai 2018. Etant séparée de son compagnon et sans ressources, elle a bénéficié d'un hébergement dans différentes structures à compter de sa sortie de la maternité, d'abord à l'initiative des services de l'Etat puis du département de la Gironde jusqu'à la fin avril 2019, date à laquelle il a été mis fin à cet hébergement, sa situation n'ayant pas évolué. Elle a déposé, en juillet 2018, une demande de titre de séjour en qualité de parent d'enfant français et a obtenu, dans l'attente de l'instruction de sa demande, un récépissé l'autorisant à travailler jusqu'au 18 juillet 2019. Il est constant que la requérante et sa fille de 11 mois hébergées jusqu'en mai 2019, n'ont bénéficié à compter de cette date d'aucune solution d'hébergement en dépit des demandes adressées tant à l'Etat qu'au département de la Gironde. La ministre des solidarités et de la santé relève appel de l'ordonnance du 7 mai 2019 par laquelle le juge des référés du tribunal administratif de Bordeaux a enjoint à l'Etat, sur le fondement de l'article L. 521-2 du code de justice administrative, d'indiquer à Mme B...un hébergement d'urgence pour elle et sa fille dans les quarante-huit heures à compter de la notification de son ordonnance. <br/>
<br/>
              3. D'une part, aux termes de l'article L. 221-1 du code de l'action sociale et des familles : " Le service de l'aide sociale à l'enfance est un service non personnalisé du département chargé des missions suivantes : / 1° Apporter un soutien matériel, éducatif et psychologique (...) aux mineurs et à leur famille ou à tout détenteur de l'autorité parentale, confrontés à des difficultés risquant de mettre en danger la santé, la sécurité, la moralité de ces mineurs ou de compromettre gravement leur éducation ou leur développement physique, affectif, intellectuel et social (...) / (...) / 3° Mener en urgence des actions de protection en faveur des mineurs mentionnés au 1° du présent article ; / (...) ". Aux termes de l'article L. 222-1 du même code : " (...) les prestations d'aide sociale à l'enfance mentionnées au présent chapitre sont accordées par décision du président du conseil général du département où la demande est présentée ". Aux termes de l'article L. 222-5 de ce code : " Sont pris en charge par le service de l'aide sociale à l'enfance sur décision du président du conseil général : / (...) 4° Les femmes enceintes et les mères isolées avec leurs enfants de moins de trois ans qui ont besoin d'un soutien matériel et psychologique, notamment parce qu'elles sont sans domicile. (...) ". Enfin, il résulte de l'article L. 221-2 de ce code que le département doit notamment disposer de " possibilités d'accueil d'urgence " ainsi que de " structures d'accueil pour les femmes enceintes et les mères avec leurs enfants " et de son article L. 222-3 que les prestations d'aide sociale à l'enfance peuvent prendre la forme du versement d'aides financières.<br/>
<br/>
              4. D'autre part, aux termes de l'article L. 121-7 du code de l'action sociale et des familles : " Sont à la charge de l'Etat au titre de l'aide sociale : (...) 8° Les mesures d'aide sociale en matière de logement, d'hébergement et de réinsertion, mentionnées aux articles L. 345-1 à L. 345-3 ". Aux termes de l'article L. 345-1 du même code : " Bénéficient, sur leur demande, de l'aide sociale pour être accueillies dans des centres d'hébergement et de réinsertion sociale publics ou privés les personnes et les familles qui connaissent de graves difficultés, notamment économiques, familiales, de logement, de santé ou d'insertion, en vue de les aider à accéder ou à recouvrer leur autonomie personnelle et sociale (...) ". Aux termes de l'article L. 345-2 de ce code : " Dans chaque département est mis en place, sous l'autorité du représentant de l'Etat, un dispositif de veille sociale chargé d'accueillir les personnes sans abri ou en détresse, de procéder à une première évaluation de leur situation médicale, psychique et sociale et de les orienter vers les structures ou services qu'appelle leur état. Cette orientation est assurée par un service intégré d'accueil et d'orientation, dans les conditions définies par la convention conclue avec le représentant de l'Etat dans le département prévue à l'article L. 345-2-4. (...) ". Aux termes de l'article L. 345-2-2 du même code : " Toute personne sans abri en situation de détresse médicale, psychique ou sociale a accès, à tout moment, à un dispositif d'hébergement d'urgence. (...) ".<br/>
<br/>
              5. S'il résulte de ces dispositions que sont en principe à la charge de l'Etat les mesures d'aide sociale relatives à l'hébergement des personnes qui connaissent de graves difficultés, notamment économiques ou de logement, ainsi que l'hébergement d'urgence des personnes sans abri en situation de détresse médicale, psychique ou sociale, il en résulte également que la prise en charge, qui inclut l'hébergement, le cas échéant en urgence, des femmes enceintes ou des mères isolées avec leurs enfants de moins de trois ans qui ont besoin d'un soutien matériel et psychologique, notamment parce qu'elles sont sans domicile, incombe au département en vertu de l'article L. 222-5 du code de l'action sociale et des familles. Si toute personne peut s'adresser au service intégré d'accueil et d'orientation prévu par l'article L. 345-2 du même code et si l'Etat ne pourrait légalement refuser à ces femmes un hébergement d'urgence au seul motif qu'il incombe en principe au département d'assurer leur prise en charge, l'intervention de l'Etat ne revêt qu'un caractère supplétif, dans l'hypothèse où le département n'aurait pas accompli les diligences qui lui reviennent et ne fait d'ailleurs pas obstacle à ce que puisse être recherchée la responsabilité du département en cas de carence avérée et prolongée.<br/>
<br/>
              6. Par suite, et sans qu'il soit besoin d'examiner les autres moyens de sa requête, la ministre  des solidarités et de la santé est fondée à soutenir que c'est à tort que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Bordeaux, après avoir constaté les multiples demandes d'hébergement adressées notamment au département ainsi que la compétence de celui-ci, a enjoint au préfet de la Gironde d'indiquer à Mme B...un hébergement d'urgence pour elle et sa fille de onze mois dans les quarante-huit heures suivant la notification de son ordonnance.<br/>
<br/>
              7. Il appartient au juge des référés du Conseil d'Etat d'examiner, au titre de l'effet dévolutif de l'appel, les conclusions présentées par Mme B...devant le juge des référés du tribunal administratif de Bordeaux.<br/>
<br/>
              8. Si l'article L. 223-1 du code de l'action sociale et des familles prévoit que l'attribution d'une prestation d'aide sociale à l'enfance " est précédée d'une évaluation de la situation prenant en compte l'état du mineur, la situation de la famille et les aides auxquelles elle peut faire appel dans son environnement ", ces dispositions ne s'opposent pas à ce que le service de l'aide sociale à l'enfance réalise en urgence des actions de protection nécessaires à l'égard des personnes qu'il doit prendre en charge, ainsi que le prévoit le 3° de l'article L. 221-1 du même code cité au point 3. <br/>
<br/>
              9. Il résulte des observations orales présentées par le département de la Gironde lors de l'audience tenue le 7 mai 2019 par le juge des référés du tribunal administratif de Bordeaux qu'il n'a pas contesté que MeB..., qui est, ainsi qu'il a été dit, mère isolée d'un enfants de moins de trois ans, a besoin d'un soutien matériel et psychologique parce qu'elle est sans domicile, mais s'est borné à indiquer que les services sociaux suivant l'intéressée n'avaient pas transmis de rapport de situation. Il y a lieu, dès lors, d'enjoindre à ce département, qui n'a pas produit de nouvelles observations en appel, d'attribuer à Mme B...un hébergement, au titre de l'article L. 222-5 du code de l'action sociale et des familles, dans un délai de quarante-huit heures à compter de la notification de l'ordonnance à intervenir. <br/>
<br/>
              10. Si Mme B...a demandé, dans l'hypothèse où l'appel formé par l'Etat était accueilli, qu'il soit enjoint à celui-ci de maintenir l'hébergement d'urgence dont elle bénéficie de sa part en exécution de l'ordonnance du juge des référés de tribunal administratif de Bordeaux, la ministre de la solidarité et de la santé s'est engagée, lors de l'audience tenue devant le juge des référés du Conseil d'Etat, à continuer à l'héberger jusqu'à ce que l'intéressée soit relogée par le département de la Gironde. Il n'y a donc pas lieu de statuer sur ces conclusions.<br/>
<br/>
              11. Il y a lieu, sans qu'il soit besoin de se prononcer sur la demande de Mme B... d'être provisoirement admise au bénéfice de l'aide juridictionnelle, de faire droit aux conclusions qu'elle a présentées au titre de l'article L. 761-1 du code de justice administrative et de mettre à la charge du département de la Gironde la somme de 2 500 euros en application de ces dispositions.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : L'ordonnance du 7 mai 2019 du juge des référés du tribunal administratif de Bordeaux est annulée.<br/>
Article 2 : Il est enjoint au département de la Gironde d'attribuer à Mme B...et à sa fille un hébergement, au titre de l'article L. 222-5 du code de l'action sociale et des familles, dans les quarante-huit heures à compter de la notification de la présente ordonnance.<br/>
Article 3 : Il n'y a pas lieu de statuer sur les conclusions de Mme B...tendant à ce qu'il soit enjoint à l'Etat de continuer à l'héberger jusqu'à ce qu'elle soit relogée par le département de la Gironde.<br/>
Article 4 : Le département de la Gironde versera à Mme B...la somme de 2 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : Le surplus des conclusions présentées par Mme B...est rejeté.<br/>
Article 6 : La présente ordonnance sera notifiée à la ministre des solidarités et de la santé, à Mme A...B...et au département de la Gironde.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
