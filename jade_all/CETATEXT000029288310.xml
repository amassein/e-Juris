<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029288310</ID>
<ANCIEN_ID>JG_L_2014_07_000000375853</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/28/83/CETATEXT000029288310.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème SSJS, 23/07/2014, 375853, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-07-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>375853</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>Mme Séverine Larere</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:375853.20140723</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un mémoire distinct, enregistré le 26 mai 2014 au secrétariat du contentieux du Conseil d'État, le syndicat national des industries des peintures, enduits et vernis (SIPEV) et l'association française des industries, colles, adhésifs et mastics (AFICAM) demandent au Conseil d'Etat, à l'appui de leur requête tendant à l'annulation pour excès de pouvoir du décret n° 2013-1264 du 23 décembre 2013 relatif à la déclaration environnementale de certains produits de construction destinés à un usage dans les ouvrages de bâtiment et en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution du 10° de l'article L. 214-1 du code de la consommation. <br/>
<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - la Constitution, notamment son Préambule et ses articles 34 et 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code de la consommation ;<br/>
              - la loi n° 2010-788 du 12 juillet 2010 ; <br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Séverine Larere, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat du syndicat national des industries des peintures enduits et vernis et de l'association française des industries, colles, adhésifs et mastics ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 10 juillet 2014, présentée par le syndicat national des industries des peintures, enduits et vernis (SIPEV) et l'association française des industries, colles, adhésifs et mastics (AFICAM).<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'État (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. Les requérants soutiennent que les dispositions du 3° du I de l'article 228 de la loi du 12 juillet 2010 portant engagement national pour l'environnement, codifiées au 10° de l'article L. 214-1 du code de la consommation, portent atteinte à la liberté d'entreprendre garantie par l'article 4 de la Déclaration des droits de l'homme et du citoyen, sont entachées d'incompétence négative et méconnaissent l'objectif à valeur constitutionnelle d'intelligibilité et d'accessibilité de la loi.<br/>
<br/>
              3. Si l'objectif de valeur constitutionnelle d'intelligibilité et d'accessibilité de la loi impose au législateur d'adopter des dispositions suffisamment précises et des formules non équivoques, sa méconnaissance ne peut, en elle-même, être invoquée à l'appui d'une question prioritaire de constitutionnalité sur le fondement de l'article 61-1 de la Constitution.<br/>
<br/>
              4. Les dispositions du 3° du I de l'article 228 de la loi du 22 juillet 2010 mentionnées au point 2, qui prévoient qu'il sera statué par des décrets en Conseil d'Etat sur les mesures à prendre pour assurer l'exécution des chapitres II à VI du titre Ier du livre II du code de la consommation, ne contredisent pas les dispositions du 1° du même article, codifiées à l'article L. 112-10 du même code et qui prévoient que des décrets en Conseil d'Etat précisent ces dispositions du livre Ier de ce code. Elles ne sont pas entachées d'incompétence négative dès lors qu'elles indiquent la finalité, les éléments d'information et le champ d'application de l'obligation pesant sur les opérateurs économiques concernés par la mise sur le marché de produits destinés à la vente. En tout état de cause, le moyen tiré de ce que la disposition législative litigieuse porterait atteinte à la liberté d'entreprendre n'est pas assorti de précisions suffisantes permettant d'en apprécier le bien-fondé. <br/>
<br/>
              5. Par suite, la question prioritaire de constitutionnalité soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux.<br/>
<br/>
              6. Il résulte de ce qui précède que, sans qu'il soit besoin de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée, le moyen tiré de ce que les dispositions du 3° du I de l'article 228 de la loi 12 juillet 2010 portant engagement national pour l'environnement, codifiées au 10° de l'article L. 214-1 du code de la consommation, portent atteinte aux droits et libertés garantis par la Constitution doit être écarté.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par le SIPEV et l'AFICAM.<br/>
Article 2 : La présente décision sera notifiée au syndicat national des industries des peintures, enduits et vernis, à l'association française des industries, colles, adhésifs et mastics, à la ministre de l'écologie, du développement durable et de l'énergie et à la ministre du logement et de l'égalité des territoires.<br/>
Copie en sera adressée au Conseil constitutionnel et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
