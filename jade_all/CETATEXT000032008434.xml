<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032008434</ID>
<ANCIEN_ID>JG_L_2016_02_000000361179</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/00/84/CETATEXT000032008434.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème SSJS, 10/02/2016, 361179, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-02-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>361179</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ORTSCHEIDT</AVOCATS>
<RAPPORTEUR>M. Mathieu Herondart</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2016:361179.20160210</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              La société Groupe Bruxelles Lambert a demandé au tribunal administratif de Paris de prononcer la restitution des retenues à la source d'un montant de 90 011 167,72 euros opérées sur les dividendes qu'elle a reçus de la société Total au titre des années 1999 à 2005. Par un jugement n° 0610351 du 3 juin 2010, le tribunal a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 10PA04218 du 21 mars 2012, la cour administrative d'appel de Paris a rejeté l'appel formé par la société Groupe Bruxelles Lambert contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire, quatre nouveaux mémoires et trois mémoires en réplique, enregistrés les 19 juillet et 12 octobre 2012, 29 janvier, 4 avril et 24 octobre 2013, 23 janvier, 17 juin et 11 décembre 2014 et le 7 janvier 2016 au secrétariat du contentieux du Conseil d'Etat, la société Groupe Bruxelles Lambert demande au Conseil d'Etat :<br/>
<br/>
               1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de saisir la Cour de justice de l'Union européenne de questions préjudicielles portant en premier lieu, sur le refus d'accorder aux actionnaires non résidents le bénéfice des garanties procédurales accordées aux actionnaires résidents pour lesquels le délai de réclamation ne commence à courir qu'à partir du moment où ils ont été informés, par l'avis d'imposition, du délai imparti pour présenter la réclamation, en deuxième lieu, sur le délai de réclamation limité à deux ans pour les non résidents soumis à un impôt prélevé par voie de retenue à la source, même s'ils sont en situation fiscalement déficitaire, alors que ce délai ne commence à courir, pour les résidents soumis à l'impôt par voie d'enrôlement, s'ils sont en situation fiscalement déficitaire, qu'au moment où ils redeviennent en situation bénéficiaire, en troisième lieu, sur la compatibilité avec les principes communautaires d'équivalence et d'effectivité, de la mise à la charge de l'actionnaire non résident qui sollicite la restitution d'une retenue à la source, de la preuve du paiement effectif de cette retenue dont il ne connaît pas l'auteur et avec lequel il n'a aucun lien contractuel, en quatrième lieu, sur la violation du principe communautaire de libre prestation de services née de l'obligation faite aux actionnaires non résidents d'employer les services d'une banque établie en France pour l'encaissement de leurs dividendes de source française dans le seul but de se préserver la preuve du paiement effectif des retenues à la source, en cinquième lieu, sur l'obligation de l'administration fiscale d'un Etat membre de faire usage de son pouvoir discrétionnaire de dégrèvement d'office, lorsqu'elle est saisie d'une réclamation introduite après l'expiration des délais ordinaires contre une imposition qui se révèle contraire au droit communautaire, en sixième lieu, sur l'interprétation de son arrêt C-379/05 Amurta du 8 novembre 2007, au regard de la situation de sociétés résidentes et non résidentes ne remplissant pas les conditions du régime mère-fille, se trouvant dans une situation fiscalement déficitaire, en septième lieu, sur la compatibilité avec le droit européen et la monnaie unique, de mesures fiscales prises par un Etat membre visant à soumettre la distribution de dividendes à des régimes distincts, pour l'un d'exonération de retenue à la source s'agissant des dividendes versés par des sociétés établis dans cet Etat membre et pour l'autre de taxation s'agissant des dividendes versés aux actionnaires de ces mêmes sociétés établis dans un autre Etat membre, en huitième lieu, sur la méconnaissance du principe de coopération loyale entre Etats membres, né du refus des autorités françaises d'accorder foi à un document délivré par les autorités fiscales belges précisant le régime d'une " société à portefeuille " de droit belge, en neuvième lieu, sur la comparabilité de la situation d'une " société à portefeuille " avec une SICAV, toutes de droit belge, aux fins d'application de son arrêt C-338/11 Santander et Consorts, et en dixième lieu, sur la transposition de son raisonnement adopté à propos de sociétés résidentes et non résidentes fiscalement bénéficiaires dans son ordonnance C-384/11 du 12 juillet 2012, Tate et Lyle, à l'hypothèse de sociétés résidentes fiscalement déficitaires ;<br/>
<br/>
              4°) d'enjoindre au ministre de l'économie et des finances de verser aux débats la lettre de mise en demeure de la Commission européenne du 16 février 2011, la réponse de l'Etat français du 9 juin 2011, l'avis motivé de la Commission du 4 novembre 2013, la réponse de l'Etat français à cet avis, et subsidiairement de surseoir à statuer sur le pourvoi jusqu'à l'issue de la procédure d'infraction initiée par la Commission contre la France ;<br/>
<br/>
              5°) de mettre à la charge de l'Etat la somme de 10 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la traité instituant la Communauté européenne ;<br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - la convention fiscale signée entre la France et la Belgique le 10 mars 1964 ;<br/>
              - le code général des impôts et le livre des procédures fiscales ; <br/>
              - le code de justice administrative ;		<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Mathieu Herondart, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Ortscheidt, avocat de la société Groupe Bruxelles Lambert ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société de droit belge Groupe Bruxelles Lambert a perçu au cours des années 1999 à 2005 des dividendes de la société Total, établie en France, à raison desquels elle a supporté des retenues à la source en application des dispositions des articles 119 bis et 187 du code général des impôts et de la convention fiscale signée entre la France et la Belgique le 10 mars 1964 ; qu'elle en a demandé en vain le remboursement ; qu'elle se pourvoit en cassation contre l'arrêt du 21 mars 2012 par lequel la cour administrative d'appel de Paris a rejeté sa requête tendant à l'annulation du jugement du 3 juin 2010 du tribunal administratif de Paris ayant rejeté sa demande de restitution de ces impositions ;<br/>
<br/>
              Sur la régularité du jugement : <br/>
<br/>
              2. Considérant que la cour a relevé que les premiers juges avaient écarté le moyen de la société requérante tiré de ce qu'elle était déficitaire au motif qu'un tel moyen n'était pas de nature à permettre l'identification d'une discrimination au regard de l'article 56 du traité instituant la Communauté européenne ; qu'elle a ensuite jugé, pour écarter le moyen tiré de ce que le tribunal administratif se serait fondé, pour rejeter sa demande, sur un moyen relevé d'office sans avoir invité les parties à présenter leurs observations, que le motif qu'il a ajouté selon lequel la société n'établissait pas avoir présenté des résultats déficitaires était surabondant ; que si la société requérante qualifie ce motif de subsidiaire en maintenant que le tribunal ne pouvait le relever d'office sans en avoir préalablement informé les parties dès lors que l'administration n'avait pas opposé cette argumentation, son caractère surabondant ressort du jugement soumis aux juges d'appel ; qu'il est, en conséquence, resté sans influence sur sa régularité ; qu'ainsi, les moyens tirés d'une violation de l'article R. 611-7 du code de justice administrative et du principe du caractère contradictoire de la procédure doivent être écartés ;<br/>
<br/>
              Sur le bien-fondé de l'arrêt en ce qui concerne les retenues à la source acquittées au titre des années 1999 à 2001 :<br/>
<br/>
              3. Considérant, en premier lieu, qu'aux termes de l'article R. 196-1 du livre des procédures fiscales : " Pour être recevables, les réclamations relatives aux impôts autres que les impôts directs locaux et les taxes annexes à ces impôts, doivent être présentées à l'administration au plus tard le 31 décembre de la deuxième année suivant celle, selon le cas :/ a) De la mise en recouvrement du rôle ou de la notification d'un avis de mise en recouvrement (...)  / Toutefois, dans les cas suivants, les réclamations doivent être présentées au plus tard le 31 décembre de l'année suivant celle, selon le cas :/ (...) b) Au cours de laquelle les retenues à la source et les prélèvements ont été opérés s'il s'agit de contestations relatives à l'application de ces retenues (...) " ; qu'aux termes de l'article R. 421-5 du code de justice administrative : " Les délais de recours ne sont opposables qu'à la condition d'avoir été mentionnés, ainsi que les voies de recours, dans la notification de la décision " ; qu'il résulte de ces dispositions que l'absence de mention sur un avis d'imposition adressé par l'administration au contribuable du caractère obligatoire de la réclamation préalable, ainsi que des délais dans lesquels le contribuable doit exercer cette réclamation, fait obstacle à ce que les délais de réclamation lui soient opposables ; qu'en revanche, ces dispositions ne sont pas applicables lorsque le contribuable demande la restitution d'impositions versées par lui ou acquittées par un tiers sans qu'un titre d'imposition ait été émis ; <br/>
<br/>
              4. Considérant, d'une part, qu'en jugeant que, faute d'un titre d'imposition, la société n'était pas fondée à soutenir qu'en l'absence de la mention des voies et délais de recours sur un tel titre, aucun délai de forclusion n'était opposable à sa demande tendant à la restitution de la retenue à la source acquittée spontanément pour son compte par la société française distributrice, la cour n'a pas commis d'erreur de droit ;<br/>
<br/>
              5. Considérant, d'autre part, que la cour a suffisamment motivé son arrêt et n'a pas commis d'erreur de droit en jugeant que les sociétés résidant en France soumises à l'impôt sur les sociétés et les sociétés non-résidentes soumises à la retenue à la source à raison de la perception de dividendes de source française n'étaient pas placées dans une situation identique au regard des modalités de recouvrement de l'impôt sur les dividendes et que cette diversité de techniques d'imposition était, d'une part, liée et proportionnée à la différence de situation entre ces deux catégories de sociétés et, d'autre part, justifiée par la nécessité de garantir l'efficacité du recouvrement de l'impôt ; qu'elle a suffisamment motivé son arrêt en écartant l'argumentation tiré de l'existence du régime des sociétés mères ; qu'elle n'a pas non plus commis d'erreur de droit en relevant que les sociétés non résidentes, qui faisaient l'objet d'une imposition effective par voie de retenue à la source, se trouvaient dans une situation différente de celle des sociétés françaises qui, en raison de leur caractère déficitaire, n'étaient soumises à aucune imposition au titre de l'exercice au cours duquel un déficit a été constaté et pouvaient, en application de l'article L. 190 du livre des procédures fiscales, contester sans condition de délai le montant de leur résultat déficitaire ; <br/>
<br/>
              6. Considérant, en outre, que la cour n'a pas commis d'erreur de droit en jugeant que la société n'était pas fondée à invoquer une discrimination entre les sociétés non-résidentes et les sociétés résidentes au regard de l'obligation de mention des voies et délais de recours prévue par l'article R. 421-5 du code de justice administrative,  dès lors que l'impôt sur les dividendes est acquitté spontanément par ces deux catégories de sociétés, sans émission préalable d'un titre d'imposition, qu'il s'agisse de l'impôt sur les sociétés pour les sociétés résidentes ou de la retenue à la source pour les sociétés non-résidentes ; que le moyen tiré de ce que la cour aurait méconnu les principes d'équivalence des garanties procédurales et, par suite, de non-discrimination entre résidents et non résidents dans l'exercice de la libre circulation des capitaux garantie par l'article 63 du traité sur le fonctionnement de l'Union européenne ne peut, dès lors, qu'être écarté ;<br/>
<br/>
              7. Considérant, en second lieu, qu'aux termes de l'article R. 211-1 du livre des procédures fiscales : " L'administration des impôts peut prononcer d'office le dégrèvement ou la restitution d'impositions qui n'étaient pas dues jusqu'au 31 décembre de la quatrième année suivant celle au cours de laquelle le délai de réclamation a pris fin (...) " ; que la cour n'a ni commis d'erreur de droit ni insuffisamment motivé son arrêt en jugeant qu'aucun principe du droit communautaire n'imposait à l'administration fiscale d'instruire au fond la réclamation présentée par la société et de faire usage du pouvoir gracieux qui lui est conféré par l'article R. 211-1 du code général des impôts ; qu'elle n'a pas non plus dénaturé les pièces du dossier en estimant que la lettre de réclamation du 23 septembre 2005 présentée par la société requérante tendant à la restitution de la retenue à la source litigieuse avait le caractère d'une réclamation contentieuse soumise aux conditions de recevabilité prévues par l'article R. 196-1 du livre des procédures fiscales et ne tendait pas à la mise en oeuvre de l'article R. 211-1 du même livre ; <br/>
<br/>
<br/>
              Sur le bien-fondé de l'arrêt en tant qu'il concerne la retenue à la source acquittée au titre de l'année 2002 :<br/>
<br/>
              8. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que les documents produits par la société Groupe Bruxelles Lambert pour justifier du versement de l'impôt qu'elle contestait au titre de l'année 2002 n'établissaient ni la date, ni le montant de la retenue à la source qui aurait été versée au Trésor public lors de la distribution de dividendes par la société Total au cours de l'année en litige ; que, dès lors, c'est sans dénaturer ces pièces ni méconnaître les dispositions de l'article R. 197-3 du livre des procédures fiscales, que la cour, après avoir à juste titre relevé que le contribuable peut produire toutes pièces établissant le versement de la retenue litigieuse pour peu qu'elles en précisent la date et l'établissement payeur au sens des dispositions combinées de l'article 381 A de l'annexe III au code général des impôts et de l'article 188-0 H de l'annexe IV au même code, a, par une appréciation souveraine, retenu qu'aucun document du dossier ne permettait d'établir la date du versement dont la restitution était demandée ; <br/>
<br/>
              Sur le bien-fondé de l'arrêt en ce qui concerne les retenues à la source acquittées au titre des années 2003 à 2005 :<br/>
<br/>
              9. Considérant qu'aux termes du 2 de l'article 119 bis du code général des impôts, dans sa rédaction applicable aux impositions en litige : " Les produits visés aux articles 108 à 117 bis donnent lieu à l'application d'une retenue à la source dont le taux est fixé par le 1 de l'article 187 lorsqu'ils bénéficient à des personnes qui n'ont pas leur domicile fiscal ou leur siège en France. (...) " ; que les dividendes distribués par une société établie en France à une société établie dans un autre Etat de la Communauté européenne, notamment en Belgique, sont au nombre des produits soumis à cette retenue ; qu'aux termes du 1 de l'article 187 du même code, dans sa rédaction applicable au litige : " Le taux de la retenue à la source prévue à l'article 119 bis est fixé à : 12 % pour les intérêts des obligations négociables (...) ; / 25 % pour tous les autres revenus (...) " ; que la convention fiscale franco-belge fixe ce taux à 15 % pour les dividendes ; <br/>
<br/>
              10. Considérant, d'une part, qu'une société non résidente en situation déficitaire et qui ne relève pas du régime fiscal des sociétés mères et une société établie en France placée dans la même situation ne peuvent être regardées comme étant dans une situation objectivement comparable, dès lors notamment que la détermination du résultat imposable de ces deux sociétés procède des règles fiscales propres à la législation de chacun de ces Etats membres ; d'autre part, qu'aucune disposition du droit interne français ne prévoit une exonération des dividendes reçus par une société résidente qui ne relève pas du régime fiscal des sociétés mères lorsque ses résultats sont déficitaires ; qu'en effet, ces dividendes sont effectivement compris dans le résultat de cette société et viennent en diminution du déficit reportable ; que, lorsque le résultat de cette société redevient bénéficiaire, la diminution de ce déficit reportable implique que ces dividendes seront effectivement imposés à l'impôt sur les sociétés au titre d'une année ultérieure au taux de droit commun alors applicable ; que, s'il en résulte un décalage dans le temps entre la perception de la retenue à la source afférente aux dividendes payés à la société non résidente et l'impôt établi à l'encontre de la société établie en France au titre de l'exercice où ses résultats redeviennent bénéficiaires, ce décalage procède d'une technique différente d'imposition des dividendes perçus par la société selon qu'elle est non résidente ou résidente ; que le seul désavantage de trésorerie que comporte la retenue à la source pour la société non résidente ne peut ainsi être regardé comme constituant une différence de traitement caractérisant une restriction à la liberté de circulation des capitaux ; que, par suite, en jugeant que les retenues à la source acquittées en application des dispositions du 2 de l'article 119 bis du code général des impôts par la société requérante sur les dividendes distribués par la société Total au cours des années 2003 à 2005, n'étaient pas constitutives d'une restriction au principe de libre circulation des capitaux, la cour n'a pas commis d'erreur de droit ;<br/>
<br/>
              11. Considérant, en deuxième lieu, que si la société requérante soutient que la cour s'est méprise sur la portée de ses écritures et commis une erreur de droit au regard de l'application de l'article 145 du code général des impôts en jugeant que la suppression de la condition alternative relative au prix de revient des titres de participations ne constituait pas une aggravation de la situation des sociétés fiscalement déficitaires établies dans un Etat membre autre que la France, ces moyens doivent être écartés dès lors que les écritures de la société requérante devant la cour ne mentionnaient pas l'article 145 du code général des impôts dont elle affirme que la cour aurait fait une mauvaise application et ne permettaient pas de connaître précisément le raisonnement qui la conduisait à soutenir que la suppression de la condition du prix d'acquisition constituait un retour en arrière incompatible avec la monnaie unique ; <br/>
<br/>
              12. Considérant, en troisième lieu, que si la société requérante soutient que la cour aurait commis une erreur de droit en refusant de faire droit à sa demande de restitution, alors qu'elle se trouvait dans une situation objectivement comparable, bien qu'elle n'ait pas relevé de la directive 2014/91/UE du Parlement européen et du Conseil du 23 juillet 2014 modifiant la directive 2009/65/CE portant coordination des dispositions législatives, réglementaires et administratives concernant certains organismes de placement collectif en valeurs mobilières (OPCVM), des sociétés d'investissement ordinaires (SIO), sociétés d'investissement à capital fixe (SICAF) et sociétés d'investissement à capital variable (SICAV), qui sont exonérées de retenue à la source et d'impôt sur les sociétés à raison des dividendes qu'elles perçoivent des sociétés françaises, ce moyen, nouveau en cassation et qui n'est pas d'ordre public, est sans incidence sur l'arrêt attaqué ;<br/>
<br/>
              13. Considérant qu'il résulte de tout ce qui précède, sans qu'il soit besoin de faire droit aux conclusions de la société Groupe Bruxelles Lambert tendant à ce qu'il soit ordonné au ministre de verser aux débats les courriers produits dans le cadre de la procédure d'infraction engagée par la Commission européenne, à ce que la Cour de justice de l'Union européenne soit saisie de questions préjudicielles et à ce qu'il soit sursis à statuer dans l'attente d'une procédure engagée contre la France, que la société Groupe Bruxelles Lambert n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque ; qu'il y a lieu, par voie de conséquence, de rejeter ses conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice ;<br/>
<br/>
<br/>
<br/>                  D E C I D E :<br/>
                                 --------------<br/>
<br/>
Article 1er : Le pourvoi de la société Groupe Bruxelles Lambert est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la société Groupe Bruxelles Lambert et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
