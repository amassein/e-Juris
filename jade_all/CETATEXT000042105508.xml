<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042105508</ID>
<ANCIEN_ID>JG_L_2020_07_000000439547</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/10/55/CETATEXT000042105508.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 09/07/2020, 439547, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-07-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>439547</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP COLIN-STOCLET</AVOCATS>
<RAPPORTEUR>M. Olivier Gariazzo</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:439547.20200709</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au tribunal administratif d'Amiens, d'une part, de prononcer la décharge des cotisations supplémentaires d'impôt sur le revenu, de contribution exceptionnelle sur les hauts revenus et de contributions sociales auxquelles il a été assujetti au titre de l'année 2011, d'autre part, de prescrire le remboursement, assorti des intérêts moratoires, des sommes versées par lui à ce titre. Par un jugement n° 1600933 du 22 novembre 2018, ce tribunal a prononcé la décharge de l'ensemble des impositions contestées et ordonné le remboursement à M. A... des sommes versées par lui à ce titre, majorées des intérêts moratoires.<br/>
<br/>
              Par un arrêt n° 18DA02492 du 16 janvier 2020, la cour administrative d'appel de Douai a rejeté l'appel formé par le ministre de l'action et des comptes publics contre ce jugement.<br/>
<br/>
              Par un pourvoi, enregistré le 13 mars 2020 au secrétariat du contentieux du Conseil d'État, le ministre de l'action et des comptes publics demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
- le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Gariazzo, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP  Colin-Stoclet, avocat de M. A... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. A... a cédé, le 4 juillet 2011, les parts de la SARL Technopolis qu'il détenait et qui étaient inscrites sur le plan d'épargne en actions ouvert à son nom le 8 février 1993. Estimant que la plus-value réalisée à l'occasion de cette cession ne pouvait bénéficier de l'exonération attachée au régime fiscal des titres inscrits sur un plan d'épargne en actions dès lors que ce plan ne respectait pas les conditions auxquelles il était soumis par les dispositions du code monétaire et financier, l'administration a assujetti M. A... à des cotisations supplémentaires d'impôt sur le revenu, de contributions sociales et de contribution exceptionnelle sur les hauts revenus. Par un jugement du 22 novembre 2018, le tribunal administratif d'Amiens a fait droit à la demande de M. A... tendant à la décharge de ces impositions et a ordonné le remboursement des sommes versées, majorées des intérêts moratoires. Le ministre de l'action et des comptes publics se pourvoit en cassation contre l'arrêt du 13 mars 2020 par lequel la cour administrative d'appel de Douai a rejeté l'appel qu'il avait formé contre ce jugement. <br/>
<br/>
              2. Pour rejeter l'appel du ministre, la cour administrative d'appel s'est fondée sur ce que l'administration n'établissait pas que le plan d'épargne en actions dont était titulaire M. A... avait été clôturé dès le 17 mars 1993, en application de l'article 1765 du code général des impôts, du fait de l'irrégularité de l'inscription sur ce plan, à cette date, des parts de la SARL Technopolis, de sorte que la plus-value résultant de la cession de ces mêmes titres, en 2011, ne pouvait être exonérée, en se bornant à faire valoir, d'une part, que l'intéressé n'avait pas contesté le bien-fondé de précédents rehaussements notifiés au titre des années 1996 et 1997, qui avaient donné lieu à des transactions et qui procédaient de la remise en cause de l'éligibilité au régime du plan d'épargne en actions des parts de la SARL Technopolis et, d'autre part, qu'il ne pouvait ignorer que ce plan devait être regardé comme clos depuis le 17 mars 1993. <br/>
<br/>
              3. En jugeant ainsi, alors qu'il ressortait des pièces du dossier soumis aux juges du fond que si le ministre se prévalait en appel du bien-fondé des précédents rehaussements, lesquels procédaient de ce que l'inscription sur le plan des actions de la SARL Technopolis, le 17 mars 1993, caractérisait un abus de droit en raison de l'absence de véritable activité économique de cette société, il était constant que l'administration n'avait pas mis en oeuvre, dans la procédure de rectification relative à la plus-value de cession réalisée en 2011, la procédure de l'article L.64 du livre des procédures fiscales pour écarter comme ne lui étant pas opposable l'inscription sur le plan en litige des titres de la SARL Technopolis, la cour ne s'est pas méprise sur la portée de l'argumentation qui lui était soumise et n'a pas insuffisamment motivé son arrêt.<br/>
<br/>
              4. Il résulte de tout ce qui précède que le pourvoi du ministre de l'action et des comptes publics doit être rejeté.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'État la somme de 1 000 euros à verser à M. A... au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du ministre de l'action et des comptes publics est rejeté.<br/>
Article 2 : L'État versera à M. A... une somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée au ministre de l'économie, des finances et de la relance et à M. B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
