<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033222603</ID>
<ANCIEN_ID>JG_L_2016_10_000000401556</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/22/26/CETATEXT000033222603.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 07/10/2016, 401556</TITRE>
<DATE_DEC>2016-10-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>401556</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP DELVOLVE ET TRICHET</AVOCATS>
<RAPPORTEUR>Mme Marie-Anne Lévêque</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:401556.20161007</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              L'office public de l'habitat interdépartemental de l'Essonne, du Val-d'Oise et des Yvelines, en défense à la demande de M. D...C...tendant à l'annulation de ses délibérations n° 15/DJ/C016 et n° 15/DJ/C017 du 15 septembre 2015, relatives respectivement à la composition du conseil d'administration en tant qu'elle porte sur la désignation de M. B... A... et à l'élection de celui-ci en qualité de président du conseil d'administration, a produit un mémoire, enregistré le 3 février 2016 au greffe du tribunal administratif de Versailles, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958, par lequel il soulève une question prioritaire de constitutionnalité.<br/>
<br/>
              Par un jugement n° 1507478 du 12 juillet 2016, enregistré le 18 juillet 2016 au secrétariat du contentieux du Conseil d'Etat, le tribunal administratif de Versailles, avant qu'il soit statué sur la demande de M.C..., a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution des articles L. 241-3 et L. 423-12 du code de la construction et de l'habitation. <br/>
<br/>
              Dans la question prioritaire de constitutionnalité transmise et dans un mémoire puis un mémoire complémentaire enregistrés le 10 août et 29 septembre 2016 au secrétariat du contentieux du Conseil d'Etat, l'office public de l'habitat interdépartemental de l'Essonne, du Val-d'Oise et des Yvelines soutient que les articles L. 241-3 et L. 423-12 du code de la construction et de l'habitation, applicables au litige, méconnaissent, en raison de leur caractère automatique et dès lors qu'ils instituent une interdiction présentant le caractère d'une punition, le principe d'individualisation des peines garanti par l'article 8 de la déclaration des droits de l'homme et du citoyen et portent atteinte, à titre subsidiaire, au principe de la séparation des pouvoirs, qu'ils méconnaissent le principe constitutionnel d'égale admissibilité aux emplois publics garanti par l'article 6 de la Déclaration des droits de l'homme et du citoyen et qu'ils sont contraires aux dispositions de l'article 16 de la même déclaration emportant droit à un procès équitable et à un recours effectif. <br/>
<br/>
              Par un mémoire et un mémoire en réplique, enregistrés le 16 août et le 21 septembre 2016, M. C...soutient que les conditions posées par l'article 23-5 de l'ordonnance du 7 novembre 1958 ne sont pas remplies, et, en particulier que les questions posées ne présentent pas un caractère sérieux.<br/>
<br/>
              Par un mémoire, enregistré le 29 août 2016, la ministre du logement et de l'habitat durable a présenté des observations.<br/>
<br/>
              Vu les autres pièces du dossier<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son préambule et son article 61-1 ; <br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code de la construction et de l'habitation; <br/>
              - le code pénal ; <br/>
              - le code de procédure pénale ; <br/>
              - la loi n° 47-1635 du 30 août 1947 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Anne Lévêque, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delvolvé et Trichet, avocat de l'office public de l'habitat interdépartemental de L'Essonne, du Val-d'Oise et des Yvelines et à la SCP Foussard, Froger, avocat de M. C...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2. Considérant que, par un jugement du 12 juillet 2016, enregistré le 18 juillet 2016 au secrétariat du contentieux du Conseil d'Etat, le tribunal administratif de Versailles, avant qu'il soit statué sur la demande de M. C...tendant à l'annulation de délibérations du 15 septembre 2015 du conseil d'administration de l'office public de l'habitat interdépartemental de l'Essonne, du Val-d'Oise et des Yvelines relative à l'élection de M. A...à la présidence du conseil d'administration, a décidé, en application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution des articles L. 241-3 et L. 423-12 du code de la construction et de l'habitation ;<br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 423-12 du code de la construction et de l'habitation : " Nul ne peut être membre du conseil d'administration ou exercer une fonction de direction dans un organisme d'habitations à loyer modéré :/- s'il tombe sous le coup des interdictions prévues aux articles L. 241-3 et L. 241-4 (...) " ; que l'article L. 241-3 du même code prévoit des interdictions d'exercice professionnel pour les personnes ayant fait l'objet de l'une des condamnations énumérées à l'article 1er de la loi n° 47-1635 du 30 août 1947 relative à l'assainissement des professions commerciales et industrielles ou d'une condamnation à une peine d'emprisonnement avec ou sans sursis pour l'une des infractions qu'il mentionne ;<br/>
<br/>
              4. Considérant, en premier lieu, que les dispositions des articles L. 241-3 et L. 423-12 du code de la construction et de l'habitation ont pour objet d'assurer, à titre préventif, que les personnes désignées en tant que membres du conseil d'administration d'un organisme d'habitations à loyer modéré et susceptibles, le cas échéant, d'être élues à la présidence de ce même conseil, présentent les garanties d'intégrité et de moralité indispensables à l'exercice des fonctions d'administration, de gestion et de direction de ces organismes ; que, dépourvues de caractère répressif, elles ne sauraient être regardées comme instituant une sanction ayant le caractère d'une punition ; que, par suite, l'office public de l'habitat interdépartemental de l'Essonne, du Val-d'Oise et des Yvelines  ne peut utilement se prévaloir de la non-conformité de ces dispositions au principe constitutionnel de l'individualisation des peines ; qu'il ne peut davantage soutenir, en tout état de cause, que ces dispositions institueraient une peine contraire au principe de séparation des pouvoirs ;<br/>
<br/>
              5. Considérant, en deuxième lieu, qu'en prévoyant que les personnes ayant fait l'objet de certaines condamnations pénales limitativement énumérées ne peuvent être membres du conseil d'administration ou exercer une fonction de direction dans un organisme d'habitations à loyer modéré, le législateur a institué, sans méconnaître le principe d'égalité, une exigence de capacités pour l'exercice de fonctions publiques qui, par sa portée, n'excède pas manifestement ce qui est nécessaire pour garantir l'intégrité et la moralité des fonctions concernées ; que les dispositions contestées n'ont, en tout état de cause, ni pour effet ni pour objet d'instituer des incompatibilités entre mandats électoraux ou fonctions électives et activités ou fonctions professionnelles ; que, par suite, les articles L. 241-3 et L. 423-12 du code de la construction et de l'habitation ne méconnaissent pas les exigences découlant de l'article 6 de la Déclaration des droits de l'homme et du citoyen de 1789 ;  <br/>
<br/>
              6. Considérant, en troisième lieu, que toute décision prise en application de ces articles est susceptible de recours devant la juridiction compétente, devant laquelle peut être notamment discutée l'existence ou le caractère opposable de la condamnation judiciaire fondant l'incapacité professionnelle, condamnation dont la personne concernée peut, au surplus, demander à être relevée en application de l'article 702-1 du code de procédure pénale ; que, par suite, il ne peut être sérieusement soutenu que ces dispositions portent atteinte au principe constitutionnel du droit à un recours juridictionnel effectif, lequel ne saurait faire obstacle à ce que le législateur, dans un but préventif, attache certaines incapacités d'exercice professionnel à des condamnations pénales ; que les dispositions contestées ne portent, par elles-mêmes, aucune atteinte au principe constitutionnel du droit à un procès équitable ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que la question prioritaire de constitutionnalité soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux ; que, par suite, il n'y a pas lieu de la renvoyer au Conseil constitutionnel ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par l'office public de l'habitat interdépartemental de l'Essonne, du Val-d'Oise et des Yvelines. <br/>
Article 2 : La présente décision sera notifiée à l'office public de l'habitat interdépartemental de l'Essonne, du Val-d'Oise et des Yvelines et à M. D...C....<br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre, à la ministre du logement et de l'habitat durable, au ministre de la justice, garde des sceaux ainsi qu'au tribunal administratif de Versailles.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">38-04-01-005 Logement. Habitations à loyer modéré. Organismes d'habitation à loyer modéré.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">59-02-01 Répression. Domaine de la répression administrative Nature de la sanction administrative.
</SCT>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
