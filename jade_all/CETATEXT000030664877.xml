<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030664877</ID>
<ANCIEN_ID>JG_L_2015_06_000000373834</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/66/48/CETATEXT000030664877.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère / 6ème SSR, 01/06/2015, 373834, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-06-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>373834</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère / 6ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Laurent Cytermann</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:373834.20150601</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 9 décembre 2013 et 4 août 2014 au secrétariat du contentieux du Conseil d'Etat, la Fédération de l'hospitalisation privée - Médecine chirurgie obstétrique (FHP-MCO) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite par laquelle le ministre des affaires sociales et de la santé a rejeté sa demande présentée le 16 octobre 2012 tendant à l'abrogation des dispositions de la fiche " Activité libérale des praticiens statutaires " annexée à la circulaire du ministre de la santé et des sports n° DHOS/F4/2009/319 du 19 octobre 2009 relative aux règles de facturation des soins dispensés dans les établissements de santé, ainsi que les dispositions de cette fiche ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - le code de la sécurité sociale ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Cytermann, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant, en premier lieu, que l'article L. 6154-1 du code de la santé publique dispose que : " Dès lors que l'exercice des missions de service public définies à l'article L. 6112-1 dans les conditions prévues à l'article L. 6112-3 n'y fait pas obstacle, les praticiens statutaires exerçant à temps plein dans les établissements publics de santé sont autorisés à exercer une activité libérale dans les conditions définies au présent chapitre " ; que selon l'article L. 6154-2 du même code, cette activité libérale " peut comprendre des consultations, des actes et des soins en hospitalisation " ; qu'aux termes de l'article R. 6154-6 de ce code : " Lorsqu'un malade traité au titre de l'activité libérale d'un praticien est hospitalisé, ses frais de séjour sont calculés, en fonction du régime choisi, selon les dispositions tarifaires normalement applicables " ; qu'en vertu du I de l'article L. 162-22-10 du code de la sécurité sociale : " Chaque année, l'Etat fixe, selon les modalités prévues au II de l'article L. 162-22-9, les éléments suivants : / 1° Les tarifs nationaux des prestations mentionnées au 1° de l'article L. 162-22-6 servant de base au calcul de la participation de l'assuré, qui peuvent être différenciés par catégories d'établissements, notamment en fonction des conditions d'emploi du personnel médical ; (...) " ; que l'article R. 162-32 du même code dispose que : " Les catégories de prestations d'hospitalisation donnant lieu à une prise en charge par les régimes obligatoires de sécurité sociale mentionnées au 1° de l'article L. 162-22-6 sont les suivantes : / 1° Le séjour et les soins avec ou sans hébergement, représentatifs de la mise à disposition de l'ensemble des moyens nécessaires à l'hospitalisation du patient, à l'exception de ceux faisant l'objet d'une prise en charge distincte en application des dispositions de l'article R. 162-32-1 (...) " ; que l'article R. 162-32-1 du même code, s'il exclut des forfaits des établissements de santé privés à but lucratif les honoraires des praticiens, ne prévoit pas l'exclusion de la rémunération des praticiens pour les forfaits des prestations d'hospitalisation assurées par les établissements publics ;<br/>
<br/>
              2. Considérant qu'il résulte de ces dispositions que lorsque l'activité libérale d'un praticien d'un établissement public de santé donne lieu à des prestations d'hospitalisation, celles-ci sont prises en charge par les régimes obligatoires de sécurité sociale dans les mêmes conditions que si ces prestations avaient eu lieu dans le cadre de l'activité statutaire du praticien ; que les tarifs nationaux versés aux établissements publics de santé sont représentatifs de la mise à disposition de l'ensemble des moyens nécessaires à l'hospitalisation du patient, y compris la rémunération statutaire versée au praticien par l'établissement, la circonstance que le praticien perçoive, en complément, des honoraires dus par le patient étant sans incidence sur le calcul de ces tarifs ;<br/>
<br/>
              3. Considérant que, par suite, en indiquant que : " Le dossier d'admission est constitué dans les mêmes conditions que celles applicables aux admissions dans le secteur public. Les frais d'hospitalisation sont également calculés et valorisés selon les mêmes modalités, le praticien encaissant ses honoraires en sus ", la fiche " Activité libérale des praticiens statutaires " annexée à la circulaire du 19 octobre 2009 relative aux règles de facturation des soins dispensés dans les établissements de santé n'a pas ajouté à la réglementation applicable ; que les moyens tirés de ce qu'elle serait entachée d'incompétence et méconnaîtrait les dispositions législatives et réglementaires mentionnées ci-dessus doivent donc être écartés ;<br/>
<br/>
              4. Considérant, en second lieu, que le principe d'égalité ne s'oppose pas à ce que l'autorité investie du pouvoir réglementaire règle de façon différente des situations différentes ni à ce qu'elle déroge à l'égalité pour des raisons d'intérêt général pourvu que, dans l'un comme dans l'autre cas, la différence de traitement qui en résulte soit en rapport avec l'objet de la norme qui l'établit et ne soit pas manifestement disproportionnée au regard des différences de situation susceptibles de la justifier ; <br/>
<br/>
              5. Considérant que les établissements de santé privés à but lucratif ne versent pas de rémunération aux praticiens qui exercent leur activité à titre libéral en leur sein, à la différence des établissements de santé publics qui versent à leurs praticiens leur rémunération statutaire, y compris lorsque ceux-ci exercent une activité libérale en sus de leur service ; que les établissements de santé privés à but lucratif et les établissements de santé publics ne sont ainsi pas placés dans la même situation ; que la différence de traitement qui résulte de la prise en compte de la rémunération des praticiens dans le calcul des frais de séjour des malades traités au titre de l'activité libérale d'un praticien dans un établissement public de santé est en rapport avec l'objet des tarifs, qui est de couvrir les frais représentatifs de l'ensemble des moyens nécessaires à l'hospitalisation du patient supportés par l'établissement ; qu'eu égard au mode de calcul des tarifs, il ne peut être soutenu qu'elle serait manifestement disproportionnée ; que, par suite, le moyen tiré de ce que les dispositions réglementaires réitérées par la circulaire méconnaîtraient le principe d'égalité entre ces catégories d'établissements doit être écarté ;<br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que la requête de la FHP-MCO doit être rejetée, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la FHP-MCO est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la Fédération de l'hospitalisation privée - Médecine chirurgie obstétrique et à la ministre des affaires sociales, de la santé et des droits des femmes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
