<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044545404</ID>
<ANCIEN_ID>JG_L_2021_12_000000454008</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/54/54/CETATEXT000044545404.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 21/12/2021, 454008, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>454008</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Olivier Saby</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:454008.20211221</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La Commission nationale des comptes de campagne et des financements politiques, sur le fondement de l'article L. 52-15 du code électoral, a saisi le tribunal administratif de Mayotte de sa décision du 8 novembre 2020 par laquelle elle a rejeté le compte de campagne de M. F... A..., candidat tête de liste aux élections municipales qui se sont tenues les 15 mars et 28 juin 2020 dans la commune de Tsingoni (Mayotte).<br/>
<br/>
              Par un jugement n° 2100487 du 27 mai 2021, ce tribunal administratif a confirmé le bien-fondé du rejet du compte de campagne, déclaré M. A... inéligible à tout mandat pour une durée de trois ans, démissionnaire d'office de son mandat de conseiller municipal et de conseiller communautaire, et proclamé élus en ces qualités, respectivement, Mme C... et M. E....<br/>
<br/>
              Par une requête enregistrée le 28 juin 2021 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat d'annuler ce jugement en tant qu'il l'a déclaré inéligible pour une durée de trois ans, et a proclamé élus à sa place Mme C... et M. E....<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Saby, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteure publique ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il résulte de l'instruction que par une décision du 8 février 2021, la Commission nationale des comptes de campagne et des financements politiques a rejeté le compte de campagne de M. A..., tête de la liste " MDM-S'UNIR POUR LE DEVELOPPEMENT DE LA COMMUNE DE TSINGONI ", qui a recueilli 30,76 % des suffrages exprimés, cinq sièges au conseil municipal et un siège au conseil communautaire à l'issue des opérations électorales qui se sont tenues le 28 juin 2020 dans la commune de Tsingoni (Mayotte) au motif que ce compte de campagne n'avait pas été présenté par un membre de l'ordre des experts-comptables. Saisi par cette commission sur le fondement de l'article L. 52-15 du code électoral, le tribunal administratif de Mayotte a, par un jugement du 27 mai 2021, déclaré M. A... inéligible à tout mandat pour une durée de trois ans à compter de la date à laquelle sa décision deviendrait définitive, démissionnaire d'office de son mandat de conseiller municipal et de conseiller communautaire, et a en conséquence proclamé élus en ces qualités, respectivement, Mme C... et M. E.... M. A... relève appel de ce jugement.<br/>
<br/>
              2. Eu égard au moyen qu'il soulève, le requérant doit être regardé comme se bornant à contester la sanction d'inéligibilité prononcée à son encontre.<br/>
<br/>
              3. D'une part, aux termes de l'article L. 52-12 du code électoral dans sa version applicable au litige : " Chaque candidat ou candidat tête de liste soumis au plafonnement prévu à l'article L. 52-11 et qui a obtenu au moins 1 % des suffrages exprimés est tenu d'établir un compte de campagne retraçant, selon leur origine, l'ensemble des recettes perçues et, selon leur nature, l'ensemble des dépenses engagées ou effectuées en vue de l'élection, hors celles de la campagne officielle par lui-même ou pour son compte, au cours de la période mentionnée à l'article L. 52-4. La même obligation incombe au candidat ou au candidat tête de liste dès lors qu'il a bénéficié de dons de personnes physiques conformément à l'article L. 52-8 du présent code selon les modalités prévues à l'article 200 du code général des impôts. (...) / (...) Le compte de campagne est présenté par un membre de l'ordre des experts-comptables et des comptables agréés ; celui-ci met le compte de campagne en état d'examen et s'assure de la présence des pièces justificatives requises. Cette présentation n'est pas nécessaire lorsque aucune dépense ou recette ne figure au compte de campagne. Dans ce cas, le mandataire établit une attestation d'absence de dépense et de recette. (...) ". <br/>
<br/>
              4. D'autre part, aux termes de l'article L. 118-3 du même code, dans sa rédaction issue de la loi du 2 décembre 2019 visant à clarifier diverses dispositions du droit électoral : " Lorsqu'il relève une volonté de fraude ou un manquement d'une particulière gravité aux règles de financement des campagnes électorales, le juge de l'élection, saisi par la Commission nationale des comptes de campagne et des financements politiques, peut déclarer inéligible : / 1° Le candidat qui n'a pas déposé son compte de campagne dans les conditions et le délai prescrits à l'article L. 52-12 ; / 2° Le candidat dont le compte de campagne, le cas échéant après réformation, fait apparaître un dépassement du plafond des dépenses électorales ; / 3° Le candidat dont le compte de campagne a été rejeté à bon droit. / L'inéligibilité mentionnée au présent article est prononcée pour une durée maximale de trois ans et s'applique à toutes les élections. (...) ".<br/>
<br/>
              5. En application des dispositions précitées de l'article L. 118-3 du code électoral, dans sa rédaction issue de la loi du 2 décembre 2019, en dehors des cas de fraude, le juge de l'élection ne peut prononcer l'inéligibilité d'un candidat sur le fondement de ces dispositions que s'il constate un manquement d'une particulière gravité aux règles de financement des campagnes électorales. Il lui incombe à cet effet de prendre en compte l'ensemble des circonstances de l'espèce et d'apprécier s'il s'agit d'un manquement caractérisé à une règle substantielle relative au financement des campagnes électorales et s'il présente un caractère délibéré. <br/>
<br/>
              6. Il ressort des termes du jugement attaqué que, pour justifier la sanction d'inéligibilité prononcée à l'encontre de M. A... pour une période de trois années, le tribunal administratif de Mayotte a relevé qu'il avait méconnu une règle substantielle relative au financement des campagnes électorales tenant à l'absence de présentation du compte de campagne de sa liste par un membre de l'ordre des experts-comptables, et fait établir le 20 octobre 2020 une attestation par son mandataire financier certifiant une absence de dépenses et de recettes, alors qu'il ressortait du compte de campagne déposé par le requérant des dépenses et des recettes pour un montant respectif de 20 094 euros et 20 200 euros.<br/>
<br/>
              7. Eu égard à ce manquement d'une particulière gravité à la règle substantielle de présentation des comptes de campagne par un membre de l'ordre des experts-comptables prévue par l'article L. 52-12 du code électoral, M. A... n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Mayotte l'a déclaré inéligible à tout mandat. Toutefois, dans les circonstances de l'espèce, la période d'inéligibilité, fixée à trois ans par le tribunal, doit être ramenée à un an. Cette période commencera à courir à compter de la date de la présente décision. Par suite, M. A... est fondé à demander que le jugement du tribunal administratif de Mayotte soit réformé sur ce point. <br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : M. A... est déclaré inéligible à tout mandat pendant un an à compter de la date de la présente décision.<br/>
Article 2 : Le jugement du tribunal administratif de Mayotte du 27 mai 2021 est réformé en ce qu'il a de contraire à la présente décision. <br/>
Article 3 : Le surplus des conclusions de la requête de M. A... est rejeté.<br/>
Article 4 : La présente décision sera notifiée à M. F... A... et à la Commission nationale des comptes de campagne et des financements politiques.<br/>
Copie en sera adressée au ministre de l'intérieur.<br/>
              Délibéré à l'issue de la séance du 13 décembre 2021 où siégeaient : M. Thomas Andrieu, conseiller d'Etat, présidant ; Mme Anne Egerszegi, conseillère d'Etat et M. Olivier Saby, maître des requêtes-rapporteur. <br/>
<br/>
              Rendu le 21 décembre 2021.<br/>
<br/>
<br/>
                 Le président : <br/>
                 Signé : M. Thomas Andrieu<br/>
 		Le rapporteur : <br/>
      Signé : M. Olivier Saby<br/>
                 La secrétaire :<br/>
                 Signé : Mme B... D...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
