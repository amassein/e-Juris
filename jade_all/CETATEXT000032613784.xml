<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032613784</ID>
<ANCIEN_ID>JG_L_2016_05_000000396033</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/61/37/CETATEXT000032613784.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 30/05/2016, 396033, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-05-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>396033</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>Mme Charline Nicolas</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:396033.20160530</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Guintoli, la société Géotechnique travaux spéciaux (GTS) et la société Alberti ont demandé au tribunal administratif de Nice la condamnation du département des Alpes-Maritimes, aux droits duquel est venue la métropole Nice Côte d'Azur, à leur verser diverses sommes en paiement des prestations réalisées pour la rectification et le calibrage de la chaussée de la RD 2205 entre les communes de Marie et Saint-Sauveur. Par un jugement n° 1003945 du 22 mars 2013, le tribunal administratif de Nice a condamné la métropole Nice Côte d'Azur à verser 541 009,19 euros HT à la société Guintoli, 467 899,84 euros HT à la société Géotechnique travaux spéciaux et 453 277,97 euros à la société Alberti.<br/>
<br/>
              Par un arrêt n° 13MA02003 du 9 novembre 2015, la cour administrative d'appel de Marseille a, d'une part, sur appel de la métropole Nice Côte d'Azur, ramené respectivement à 285 790,51 euros, 247 170,17 euros et 239 446,10 euros le montant des sommes à verser à la société Guintoli, à la société Géotechnique travaux spéciaux et à la société Alberti et, d'autre part, rejeté l'appel incident de ces sociétés.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 11 janvier et 11 avril 2016 au secrétariat du contentieux du Conseil d'Etat, la société Guintoli, la société Géotechnique travaux spéciaux et la société Alberti demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il a rejeté leurs conclusions d'appel ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de la métropole Nice Côte d'Azur la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code civil ;<br/>
              - le code des marchés publics ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Charline Nicolas, auditeur,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la société Guintoli et autres ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux " ;<br/>
<br/>
              2. Considérant que pour demander l'annulation de l'arrêt qu'elles attaquent, la société Guintoli et autres soutiennent que la cour administrative d'appel de Marseille a dénaturé les pièces du dossier en estimant que le chiffrage de leur préjudice incluait à tort une part de bénéfices et une part d'amortissement des matériels ; que la cour a méconnu son office et commis une erreur de droit en s'abstenant de déterminer elle-même le préjudice subi, quitte à ordonner une mesure d'instruction ; que la cour a commis une erreur de droit en rejetant leur prétention au titre de la résiliation du marché au motif qu'elles n'établissaient pas un préjudice supérieur à l'indemnité à laquelle elles avaient droit aux termes de l'arrêt attaqué, alors que les indemnités dues en cas de sujétions imprévues et celles dues au titre du manque à gagner résultant de la résiliation du marché dans l'intérêt général sont distinctes ; que la cour a commis une erreur de droit en rejetant les conclusions incidentes de la société GTS relatives à l'application de l'article 1153 du code civil au motif qu'elles étaient relatives à un litige distinct de l'appel principal, dès lors que le jugement du tribunal administratif de Nice n'ayant pas été notifié à cette société GTS, le délai d'appel n'avait pas couru à son égard et ses conclusions devaient s'analyser comme un appel principal ;<br/>
<br/>
              3. Considérant qu'eu égard aux moyens soulevés, il y a lieu d'admettre les conclusions du pourvoi dirigées contre l'arrêt attaqué, d'une part, en tant qu'il rejette les prétentions des sociétés Guintoli, GTS et Alberti au titre du manque à gagner consécutif à la résiliation du marché et d'autre part, en tant qu'il rejette les conclusions de la société GTS relatives à l'application de l'article 1153 du code civil ; qu'en revanche, s'agissant des autres conclusions du pourvoi, aucun de ces moyens n'est de nature à permettre l'admission du pourvoi ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les conclusions du pourvoi qui sont dirigées contre l'arrêt attaqué en tant, d'une part, qu'il rejette les prétentions des sociétés Guintoli, GTS et Alberti au titre du manque à gagner consécutif à la résiliation du marché et d'autre part, en tant qu'il rejette les conclusions de la société GTS relatives à l'application de l'article 1153 du code civil sont admises.<br/>
Article 2 : Le surplus des conclusions du pourvoi des sociétés Guintoli, GTS et Alberti n'est pas admis.<br/>
Article 3 : La présente décision sera notifiée à la société Guintoli, premier requérant dénommé. Les autres requérants seront informés de la présente décision par la SCP Garreau Bauer-Violas Feschotte-Desbois, avocats au Conseil d'Etat et à la Cour de cassation, qui les représente devant le Conseil d'Etat. <br/>
Copie en sera adressée pour information à la métropole Nice Côte d'Azur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
