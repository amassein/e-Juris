<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030787999</ID>
<ANCIEN_ID>JG_L_2015_06_000000374286</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/78/79/CETATEXT000030787999.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème / 10ème SSR, 24/06/2015, 374286, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-06-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>374286</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème / 10ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BALAT</AVOCATS>
<RAPPORTEUR>M. Jean-Luc Matt</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Astrid Nicolazo de Barmon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:374286.20150624</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              La SA Institut de participation de l'Ouest (IPO) a demandé au tribunal administratif de Nantes la décharge des cotisations supplémentaires de taxe professionnelle auxquelles elle a été assujettie au titre des années 2001 à 2003 ainsi que des pénalités correspondantes. Par un jugement n° 0804187 du 12 mars 2009, le tribunal administratif de Nantes a fait droit à sa demande.<br/>
<br/>
              Par un arrêt n° 09NT01626 du 17 mai 2010, la cour administrative d'appel de Nantes a fait droit au recours formé par le ministre du budget, des comptes publics, de la fonction publique et de la réforme de l'État contre ce jugement et remis à la charge de la SA IPO les cotisations litigieuses.<br/>
<br/>
              Par une décision n° 341889 du 10 juin 2013, le Conseil d'Etat a annulé cet arrêt et renvoyé l'affaire à la même cour.<br/>
<br/>
              Par un arrêt n° 13NT01781 du 24 octobre 2013, la cour administrative d'appel de Nantes a fait droit au recours formé par le ministre du budget, des comptes publics, de la fonction publique et de la réforme de l'État contre le jugement du tribunal administratif de Nantes et remis à la charge de la société CM-CIC Investissement, venant aux droits de la SA IPO, les cotisations litigieuses.<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 30 décembre 2013, 31 mars 2014 et 1er juin 2015 au secrétariat du contentieux du Conseil d'Etat, la société CM-CIC Investissement demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt du 24 octobre 2013 de la cour administrative d'appel de Nantes ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter le recours du ministre ;<br/>
<br/>
              3°) de mettre à la charge de l'État la somme de 10 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              4°) de mettre à la charge de l'État la somme de 35 euros correspondant à la contribution pour l'aide juridique.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - la loi n° 85-695 du 11 juillet 1985 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Matt, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Marie-Astrid Nicolazo de Barmon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Balat, avocat de la société CM-CIC Investissement ;<br/>
<br/>
<br/>
<br/>1. Considérant, d'une part, qu'aux termes de l'article 1-1 de la loi du 11 juillet 1985 portant diverses dispositions d'ordre économique et financier, dans sa rédaction alors applicable, les sociétés de capital-risque sont des sociétés par actions ayant pour objet social " la gestion d'un portefeuille de valeurs mobilières " ; qu'en vertu de l'article 1er de cette loi, elle doivent procéder à des investissements dans des sociétés non cotées pour pouvoir bénéficier d'un régime de faveur au regard de l'imposition des sociétés ; qu'il en résulte que ces sociétés doivent être regardées comme exerçant à titre habituel une activité professionnelle au sens des dispositions, alors en vigueur, du I de l'article 1447 du code général des impôts relatives à l'assujettissement à la taxe professionnelle ;<br/>
<br/>
              2. Considérant, d'autre part, qu'aux termes du I de l'article 1647 E du code général des impôts, dans sa rédaction applicable aux années d'imposition en litige : " La cotisation de taxe professionnelle des entreprises dont le chiffre d'affaires est supérieur à 7 600 000 euros est au moins égale à 1,5 % de la valeur ajoutée produite par l'entreprise, telle que définie au II de l'article 1647 B sexies (...) " ; que selon le II de l'article 1647 B sexies du même code, dans sa rédaction alors en vigueur : " 1. La valeur ajoutée (...) est égale à l'excédent hors taxe de la production sur les consommations de biens et services en provenance de tiers (...). / 2. Pour la généralité des entreprises, la production de l'exercice est égale à la différence entre : / d'une part, les ventes, les travaux, les prestations de services ou les recettes, les produits accessoires ; les subventions d'exploitation ; les ristournes, rabais et remises obtenus ; les travaux faits par l'entreprise pour elle-même ; les stocks à la fin de l'exercice ; / et, d'autre part, les achats de matières et marchandises, droits de douane compris ; les réductions sur ventes ; les stocks au début de l'exercice. (...) / 3. La production des établissements de crédit, des entreprises ayant pour activité exclusive la gestion de valeurs mobilières est égale à la différence entre : / d'une part, les produits d'exploitation bancaires et produits accessoires ; / et, d'autre part, les charges d'exploitation bancaires. (...) " ; qu'eu égard à l'objet de ces dispositions, qui est de tenir compte de la capacité contributive des entreprises en fonction de leur activité, les entreprises ayant pour activité exclusive la gestion de valeurs mobilières ne s'entendent, pour leur application, que des seules entreprises qui exercent cette activité pour leur propre compte ; que les sociétés de capital-risque, qui gèrent pour leur propre compte des participations financières, sont, dès lors, soumises aux modalités de calcul de la valeur ajoutée prévues au 3 du II de l'article 1647 B sexies du code général des impôts pour les entreprises ayant pour activité exclusive la gestion de valeurs mobilières ;<br/>
<br/>
              3. Considérant que les dispositions de l'article 1647 B sexies du code général des impôts fixent la liste limitative des catégories d'éléments comptables qui doivent être pris en compte dans le calcul de la valeur ajoutée servant de base à la cotisation minimale de taxe professionnelle ; que pour l'application de ces dispositions, la production d'une entreprise ayant pour activité exclusive la gestion de valeurs mobilières doit être calculée, comme celle des établissements de crédit, en fonction des règles comptables fixés par le règlement du comité de la réglementation bancaire n° 91-01 du 16 janvier 1991 relatif à l'établissement et à la publication des comptes individuels annuels des établissements de crédit ; que l'annexe à ce règlement contient un modèle de compte de résultat et des commentaires de chacun des postes de ce compte ; qu'en vertu de ces dispositions, les produits d'exploitation bancaires et produits accessoires incluent les " gains et pertes sur opérations des portefeuilles de négociation " et les " gains et pertes sur opérations des portefeuilles de placement et assimilés ", mais non les " gains et pertes sur actifs immobilisés ", ce dernier poste étant défini comme " le solde en bénéfice ou perte des opérations sur titres de participation, sur autres titres détenus à long terme et sur parts dans les entreprises liées " ;<br/>
<br/>
              4. Considérant que, pour remettre à la charge de la société CM-CIC Investissement, venant aux droits de la société Institut de Participation de l'Ouest, les cotisations supplémentaires de taxe professionnelle auxquelles cette société avait été assujettie au titre des années 2001 à 2003, la cour administrative d'appel de Nantes a jugé que l'administration était fondée à prendre en compte, pour la détermination de la valeur ajoutée servant au calcul de la cotisation minimale de taxe professionnelle de cette société de capital-risque, l'ensemble des plus-values de cessions de valeurs mobilières, sans exclure les plus-values sur titres de participation ; qu'en statuant ainsi, la cour a commis une erreur de droit ; que par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt doit être annulé ;<br/>
<br/>
              5. Considérant qu'aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire " ; qu'il y a lieu, par suite, de régler l'affaire au fond ;<br/>
<br/>
              6. Considérant que, pour l'application de l'article 1647 E du code général des impôts, une société de capital-risque est, ainsi qu'il a été dit au point 3, soumise aux modalités de calcul du chiffre d'affaires et de la valeur ajoutée prévues pour les entreprises ayant pour activité exclusive la gestion de valeurs mobilières et, selon le règlement du comité de la réglementation bancaire n° 91-01 du 16 janvier 1991, les produits d'exploitation bancaires et produits accessoires ne comprennent pas les plus-values sur titres de participation et autres titres détenus à long terme ;<br/>
<br/>
              7. Considérant qu'il résulte de l'instruction qu'en excluant du calcul du chiffre d'affaires mentionné à l'article 1647 E du code général des impôts les plus-values résultant de la cession de titres de participation, le chiffre d'affaires de la société Institut de Participation de l'Ouest n'atteignait pas, au titre des années en litige, le seuil d'assujettissement à la cotisation minimale de taxe professionnelle prévu par cet article ; que, par suite, le ministre chargé du budget n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Nantes a déchargé cette société des cotisations supplémentaires de taxe professionnelle auxquelles elle a été assujettie au titre des années 2001 à 2003 ;<br/>
<br/>
              8. Considérant que les intérêts moratoires prévus par l'article L. 208 du livre des procédures fiscales, sont, en vertu de l'article R. 208-1 de ce code, " payés d'office en même temps que les sommes remboursées par le comptable chargé du recouvrement des impôts " ; qu'il n'existe aucun litige né et actuel entre le comptable et la société requérante au sujet de ces intérêts ; que, dès lors, les conclusions incidentes qui tendent au versement par l'État d'intérêts moratoires ne sont pas recevables ;<br/>
<br/>
              9. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'État la somme globale de 5 000 euros à verser à la SA CM-CIC Investissement au titre des dispositions de l'article L. 761-1 du code de justice administrative et de celles de l'article R. 761-1 du même code relatives au remboursement de la contribution pour l'aide juridique ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : L'arrêt du 24 octobre 2013 de la cour administrative d'appel de Nantes est annulé.<br/>
Article 2 : Le recours du ministre du budget, des comptes publics, de la fonction publique et de la réforme de l'État ainsi que les conclusions incidentes présentées par la SA CM-CIC Investissement devant la cour administrative d'appel de Nantes sont rejetés.<br/>
Article 3 : L'État versera à la SA CM-CIC Investissement une somme de 5 000 euros au titre des articles L. 761-1 et R. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la SA CM-CIC Investissement et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
