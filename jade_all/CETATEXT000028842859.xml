<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028842859</ID>
<ANCIEN_ID>JG_L_2014_04_000000359791</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/84/28/CETATEXT000028842859.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème SSJS, 11/04/2014, 359791, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-04-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>359791</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER</AVOCATS>
<RAPPORTEUR>M. Charles Touboul</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:359791.20140411</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 29 mai 2012 au secrétariat du contentieux du Conseil d'Etat, présenté pour M. B... A..., demeurant... ; M. A... demande au Conseil d'Etat :  <br/>
<br/>
              1°) d'annuler l'ordonnance n° 12PA00187 du 29 mars 2012 par laquelle le président de la 9ème chambre de la cour administrative d'appel de Paris a rejeté son appel tendant, d'une part, à l'annulation de l'ordonnance n° 1005321/6-2 du 10 novembre 2011 de la vice-présidente de la 6ème section du tribunal administratif de Paris rejetant sa demande tendant à l'annulation de la décision du 24 novembre 2006 du préfet de police lui enjoignant de restituer son permis de conduire et, d'autre part, à l'annulation des décisions ministérielles portant, respectivement, retrait de trois points de son permis de conduire à la suite de l'infraction relevée le 21 février 2002 et invalidation de ce titre pour solde de points nul ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;  <br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu le code de la route ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Charles Touboul, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Fabiani, Luc-Thaler, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que, par une requête enregistrée le 19 mars 2010 au greffe du tribunal administratif de Paris, M. A... a demandé l'annulation pour excès de pouvoir de la décision du 24 novembre 2006 du préfet de police lui enjoignant de restituer son permis de conduire à la suite de la perte de validité de ce titre ; que, par une ordonnance du 10 novembre 2011, la vice-présidente de la 6ème section du tribunal a rejeté sa demande en application des dispositions du 7° de l'article R. 222-1 du code de justice administrative ; que, par une ordonnance du 29 mars 2012 contre laquelle l'intéressé se pourvoit en cassation, le président de la 9ème chambre de la cour administrative d'appel de Paris a rejeté l'appel de M. A...au motif que sa demande de première instance était tardive ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 421-1 du code de justice administrative : "  (...) Sauf en matière de travaux publics, la juridiction ne peut être saisie que par voie de recours formé contre une décision et ce, dans les deux mois à partir de la notification ou de la publication de la décision attaquée (...) " ; qu'aux termes de l'article R. 421-5 du même code : "  Les délais de recours ne sont opposables qu'à la condition d'avoir été mentionnés, ainsi que les voies de recours, dans la notification de la décision " ;<br/>
<br/>
              3. Considérant que, pour juger que la demande présentée par M. A...devant le tribunal administratif de Paris était tardive, le juge d'appel a relevé qu'il ressortait des pièces du dossier et, notamment, des " mentions du relevé d'information intégral " que la décision contestée lui avait été notifiée par le préfet de police le 6 décembre 2006, jour du retrait de son titre de conduite ;  <br/>
<br/>
              4. Considérant que s'il est procédé, dans les conditions prévues à l'article L. 225-1 du code de la route, à l'enregistrement, dans le traitement automatisé dénommé " système national des permis de conduire ", de toutes décisions administratives relatives aux permis de conduire, les mentions du relevé d'information intégral relatif à la situation individuelle de chaque conducteur, extrait de ce traitement, ne sont pas, à elles seules, de nature à établir la régularité de la notification de ces décisions ; qu'ainsi, en se fondant sur les mentions du relevé d'information intégral relatif à la situation de M. A... pour en déduire que la décision contestée du 24 novembre 2006 lui avait été régulièrement notifiée, le juge d'appel a commis une erreur de droit ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que M. A...est fondé à demander l'annulation de l'ordonnance qu'il attaque ; <br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 1 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font obstacle à ce qu'une somme soit mise à la charge de M. A..., qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 29 mars 2012 du président de la 9ème chambre de la cour administrative d'appel de Paris est annulée.<br/>
<br/>
		Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris. <br/>
<br/>
Article 3 : L'Etat versera à M. A...la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 4 : Les conclusions présentées par l'Etat au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 5 : La présente décision sera notifiée à M. B...A...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
