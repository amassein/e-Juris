<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043754056</ID>
<ANCIEN_ID>JG_L_2021_07_000000432802</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/75/40/CETATEXT000043754056.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 02/07/2021, 432802</TITRE>
<DATE_DEC>2021-07-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>432802</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Olivier Rousselle</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:432802.20210702</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
               M. A... C... et M. B... C... ont demandé au tribunal administratif de Nantes d'annuler les deux décisions du 23 juin 2015 par lesquelles la commission des recours en matière de contrôle des structures des exploitations agricoles de la région des Pays-de-la-Loire a rejeté leurs recours formés contre les sanctions du 27 novembre 2014 que leur avait infligées le préfet de la Sarthe. Par un jugement n°s 1506888, 1506890 du 21 avril 2017, le tribunal administratif de Nantes a réduit le montant des deux sanctions et rejeté le surplus de leur demande.<br/>
<br/>
               Par un arrêt n°17NT01860 du 24 mai 2019, la cour administrative d'appel de Nantes a rejeté l'appel formé par MM. Xavier et Laurent C... contre ce jugement en tant qu'il a rejeté le surplus de leur demande.<br/>
<br/>
               Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 22 juillet et 18 octobre 2019 au secrétariat du contentieux du Conseil d'Etat, MM. Xavier et Laurent C... demandent au Conseil d'Etat :<br/>
<br/>
               1°) d'annuler cet arrêt ;<br/>
<br/>
               2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
               3°) de mettre à la charge de l'Etat la somme de 4 800 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
               Vu les autres pièces du dossier ;<br/>
<br/>
               Vu : <br/>
               - le code de rural et de la pêche maritime ;<br/>
               - le code de justice administrative ;<br/>
<br/>
<br/>
               Après avoir entendu en séance publique :<br/>
<br/>
               - le rapport de M. Olivier Rousselle, conseiller d'Etat,  <br/>
<br/>
               - les conclusions de Mme Cécile Barrois de Sarigny, rapporteure publique ;<br/>
<br/>
               La parole ayant été donnée, après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de MM. C... ;<br/>
<br/>
<br/>
<br/>
               Considérant ce qui suit :<br/>
<br/>
               1. Il ressort des pièces du dossier soumis aux juges du fond que MM. Xavier et Laurent C... mettent en valeur, au sein de l'exploitation agricole à responsabilité limitée (EARL) Lauxa dont ils sont les deux associés, un ensemble de terres à Béthon (Sarthe). A la suite du refus du préfet de la Sarthe d'autoriser un agrandissement de cette exploitation portant sur environ 92 hectares de terres, MM. Xavier et Laurent C... ont décidé d'exploiter chacun, à titre personnel et sans solliciter d'autorisation d'exploitation, une partie de cette surface supplémentaire. Par deux décisions du 18 juin 2014, le préfet, estimant que ces deux exploitations excédaient, chacune, le seuil fixé par le schéma directeur départemental des structures agricoles, a mis en demeure MM. Xavier et Laurent C... de régulariser leur situation en déposant, chacun, une demande d'autorisation d'exploiter dans un délai d'un mois puis, faute de régularisation, a prononcé contre chacun d'eux, par deux décisions du 27 novembre 2014, une sanction pécuniaire au taux de 900 euros par hectare exploité sans autorisation. Sur recours des intéressés, ces sanctions ont été confirmées par la commission des recours en matière de contrôle des structures des exploitations agricoles des Pays-de-la-Loire, par deux décisions du 23 juin 2015. MM. Xavier et Laurent C... se pourvoient en cassation contre l'arrêt du 24 mai 2019 par lequel la cour administrative d'appel de Nantes a rejeté leur appel contre le jugement du 21 avril 2017 du tribunal administratif de Nantes en tant que, saisi de demandes d'annulation de ces deux sanctions, le tribunal n'en a que réduit le montant à hauteur de 800 euros par hectare. <br/>
<br/>
               2. En vertu de l'article L. 331-7 du code rural et de la pêche maritime, si, à l'expiration du délai imparti à un exploitant agricole par l'autorité administrative pour qu'il cesse une exploitation qui ne respecte pas les dispositions faisant l'objet du contrôle des structures des exploitations agricoles, la même autorité constate que l'exploitation se poursuit dans des conditions irrégulières, elle peut prononcer à l'encontre de l'intéressé une sanction pécuniaire d'un montant compris entre 304,90 et 914,70 euros par hectare. L'article L. 331-8 de ce code dispose que : " La décision prononçant la sanction pécuniaire mentionnée à l'article L. 331-7 est notifiée à l'exploitant concerné, qui peut la contester, avant tout recours contentieux, dans le mois de sa réception, devant une commission des recours dont la composition et les règles de fonctionnement sont fixées par décret en Conseil d'Etat. / (...) / La décision de la commission peut faire l'objet, de la part de l'autorité administrative ou de l'intéressé, d'un recours de pleine juridiction devant le tribunal administratif ". <br/>
<br/>
               3. Il résulte de ces dispositions que le recours organisé devant la commission prévue à l'article L. 331-8 du code rural et de la pêche maritime contre la sanction prononcée par l'autorité préfectorale sur le fondement de l'article L. 331-7 du même code constitue un préalable obligatoire à tout recours contentieux. La décision prise par la commission sur ce recours préalable se substitue à la sanction prononcée par le préfet. Il s'ensuit qu'il appartient à la commission, si elle confirme la sanction, d'en fixer le montant en proportion des manquements commis et compte tenu de l'ensemble du comportement de la personne sanctionnée tel qu'il peut être appréhendé à la date à laquelle elle statue.<br/>
<br/>
               4. Par suite, en jugeant, ainsi qu'il ressort des termes de son arrêt, que les changements opérés par MM. Xavier et Laurent C... dans les conditions de leur exploitation en avril 2015, postérieurement aux décisions du préfet du 27 novembre 2014, étaient sans incidence sur le litige, alors qu'une éventuelle régularisation spontanée de leur situation par les deux exploitants était susceptible d'être prise en compte par la commission des recours pour fixer, par ses décisions du 23 juin 2015, le niveau des sanctions qui pouvaient leur être infligées, la cour administrative d'appel a commis une erreur de droit.<br/>
<br/>
               5. Il résulte de ce qui précède que, sans qu'il soit besoin de se prononcer sur l'autre moyen de leur pourvoi, MM. Xavier et Laurent C... sont fondés à demander l'annulation de l'arrêt qu'ils attaquent.<br/>
<br/>
               6. Il y a lieu de mettre à la charge de l'Etat une somme de 1 500 euros à verser à M. A... C... ainsi qu'une somme de même montant à M. B... C..., au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
               --------------<br/>
<br/>
Article 1er : L'arrêt du 24 mai 2019 de la cour administrative d'appel de Nantes est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nantes.<br/>
Article 3 : L'Etat versera à M. A... C... et à M. B... C... la somme de 1 500 euros chacun au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. B... C..., à M. A... C... et au ministre de l'agriculture et de l'alimentation. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">03-03-03-01-06 AGRICULTURE ET FORÊTS. - EXPLOITATIONS AGRICOLES. - CUMULS ET CONTRÔLE DES STRUCTURES. - CUMULS D'EXPLOITATIONS. - CONTENTIEUX. - RECOURS ADMINISTRATIF CONTRE LA SANCTION POUR EXPLOITATION IRRÉGULIÈRE D'UN FONDS AGRICOLE (ART. L. 331-8 DU CRPM) - 1) RAPO - EXISTENCE - 2) CONSÉQUENCES [RJ1] - SUBSTITUTION DE LA DÉCISION PRISE SUR RECOURS À LA DÉCISION INITIALE - APPRÉCIATION  DU COMPORTEMENT DE LA PERSONNE SANCTIONNÉE À LA DATE DE LA DÉCISION PRISE SUR RECOURS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-02-01 PROCÉDURE. - INTRODUCTION DE L'INSTANCE. - LIAISON DE L'INSTANCE. - RECOURS ADMINISTRATIF PRÉALABLE. - RAPO  - EXISTENCE - RECOURS CONTRE LA SANCTION POUR EXPLOITATION IRRÉGULIÈRE D'UN FONDS AGRICOLE (ART. L. 331-8 DU CRPM) - CONSÉQUENCES [RJ1].
</SCT>
<ANA ID="9A"> 03-03-03-01-06 1) Le recours organisé devant la commission prévue à l'article L. 331-8 du code rural et de la pêche maritime (CRPM) contre la sanction prononcée par l'autorité préfectorale sur le fondement de l'article L. 331-7 du même code constitue un préalable obligatoire (RAPO) à tout recours contentieux. ......2) La décision prise par la commission sur ce recours préalable se substitue à la sanction prononcée par le préfet. Il s'ensuit qu'il appartient à la commission, si elle confirme la sanction, d'en fixer le montant en proportion des manquements commis et compte tenu de l'ensemble du comportement de la personne sanctionnée tel qu'il peut être appréhendé à la date à laquelle elle statue.</ANA>
<ANA ID="9B"> 54-01-02-01 Le recours organisé devant la commission prévue à l'article L. 331-8 du code rural et de la pêche maritime (CRPM) contre la sanction prononcée par l'autorité préfectorale sur le fondement de l'article L. 331-7 du même code constitue un préalable obligatoire (RAPO) à tout recours contentieux. ......La décision prise par la commission sur ce recours préalable se substitue à la sanction prononcée par le préfet. Il s'ensuit qu'il appartient à la commission, si elle confirme la sanction, d'en fixer le montant en proportion des manquements commis et compte tenu de l'ensemble du comportement de la personne sanctionnée tel qu'il peut être appréhendé à la date à laquelle elle statue.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section 18 novembre 2005, Houlbreque, n° 270075, p. 514 ; CE, 13 novembre 1991, M. Girer, n° 119095, p. 392.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
