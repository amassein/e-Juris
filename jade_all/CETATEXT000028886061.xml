<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028886061</ID>
<ANCIEN_ID>JG_L_2014_04_000000359101</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/88/60/CETATEXT000028886061.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère SSJS, 30/04/2014, 359101, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-04-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>359101</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX ; DELAMARRE</AVOCATS>
<RAPPORTEUR>Mme Julia Beurton</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:359101.20140430</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>VU LA PROCEDURE SUIVANTE :<br/>
<br/>
              Par une requête, enregistrée le 3 mai 2012 au secrétariat du contentieux du Conseil d'Etat, le syndicat Union collégiale et l'Association pour l'utilisation raisonnée des médecines alternatives demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 5 mars 2012 du ministre du travail, de l'emploi et de la santé et du ministre du budget, des comptes publics et de la réforme de l'Etat portant approbation des avenants n° 3, n° 5 et n° 6 à la convention nationale organisant les rapports entre les médecins libéraux et l'assurance maladie signée le 26 juillet 2011 ; <br/>
<br/>
              2°) d'enjoindre à l'Etat d'abroger l'arrêté du 22 septembre 2011 portant approbation de cette convention et d'adopter un nouveau texte ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Par un mémoire en défense, enregistré le 13 septembre 2012, l'Union nationale des caisses d'assurance maladie conclut au rejet de la requête et à ce que la somme de 4 000 euros soit mise à la charge du syndicat Union collégiale et de l'Association pour l'utilisation raisonnée des médecines alternatives au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Par un mémoire en défense, enregistré le 23 novembre 2012, le ministre des affaires sociales et de la santé conclut au rejet de la requête. <br/>
<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Julia Beurton, Auditeur,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Delamarre, avocat du syndicat Union collégiale et de l'Association pour l'utilisation raisonnée des médecines alternatives.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>CONSIDERANT CE QUI SUIT :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 162-5 du code de la sécurité sociale : " Les rapports entre les organismes d'assurance maladie et les médecins sont définis par des conventions nationales conclues séparément pour les médecins généralistes et les médecins spécialistes (...) ou par une convention nationale conclue par l'Union nationale des caisses d'assurance maladie et au moins une organisation syndicale représentative pour l'ensemble du territoire de médecins généralistes et une organisation syndicale représentative pour l'ensemble du territoire de médecins spécialistes ". Aux termes de l'article L. 162-15 du même code : " Les conventions, (...) leurs annexes et avenants sont transmis, au nom des parties signataires, lors de leur conclusion ou d'une tacite reconduction, par l'Union nationale des caisses d'assurance maladie aux ministres chargés de la santé et de la sécurité sociale. Le Conseil national de l'ordre des médecins (...) est consulté par l'Union nationale des caisses d'assurance maladie sur les dispositions conventionnelles relatives à la déontologie (...). / (...) les conventions, annexes et avenants sont approuvés par les ministres chargés de la santé et de la sécurité sociale (...) ".<br/>
<br/>
              2. En premier lieu, d'une part, il résulte des dispositions citées ci-dessus que l'arrêté litigieux, portant approbation des avenants n° 3, n° 5 et n° 6 à la convention nationale organisant les rapports entre les médecins libéraux et l'assurance maladie signée le 26 juillet 2011, devait être signé par les ministres chargés de la santé et de la sécurité sociale et n'avait pas à l'être, contrairement à ce que soutiennent les organisations requérantes, par le ministre chargé du travail.<br/>
<br/>
              3. D'autre part, il résulte des dispositions du décret n° 2005-850 du 27 juillet 2005 relatif aux délégations de signature des membres du Gouvernement que le directeur de la sécurité sociale, nommé à compter du 13 février 2012 par un décret publié au Journal officiel de la République française le 27 janvier 2012, avait du fait de cette nomination compétence pour signer l'arrêté attaqué au nom du ministre chargé de la sécurité sociale. Contrairement à ce qui est soutenu, il n'a pas signé l'arrêté attaqué pour le directeur général de la santé, lequel a également signé l'arrêté, au nom du ministre chargé de la santé. Par suite, les organisations requérantes ne sont pas fondées à soutenir que l'arrêté attaqué n'aurait pas été valablement signé par le directeur de la sécurité sociale et serait, ainsi, entaché d'incompétence.<br/>
<br/>
              4. En deuxième lieu, il ne résulte d'aucun texte, et notamment pas de l'article L. 162-15 cité au point 1, ni d'aucun principe, que les organisations syndicales de médecins devraient être consultées préalablement à l'adoption d'un arrêté approuvant une convention médicale ou un avenant à cette convention. Par suite, les organisations requérantes ne sont pas fondées à soutenir que l'arrêté aurait été pris à l'issue d'une procédure irrégulière, faute de consultation de certains syndicats de médecins.<br/>
<br/>
              5. En troisième lieu, l'illégalité d'un acte administratif, qu'il soit ou non réglementaire, ne peut être utilement invoquée par voie d'exception à l'appui de conclusions dirigées contre une décision administrative ultérieure que si cette dernière a été prise pour l'application du premier acte ou s'il en constitue la base légale. Si l'avenant n° 3, approuvé par l'arrêté attaqué, modifie les articles 26.2 et 26.4 de la convention nationale organisant les rapports entre les médecins libéraux et l'assurance maladie signée le 26 juillet 2011, approuvée par un arrêté du 22 septembre 2011, il n'a pas été pris pour l'application de cette convention ou de cet arrêté, lesquels ne constituent pas davantage sa base légale. Par suite, les organisations requérantes ne peuvent utilement invoquer, à l'encontre de l'arrêté attaqué, l'illégalité de l'arrêté du 22 septembre 2011.<br/>
<br/>
              6. Il résulte de ce qui précède que les conclusions du syndicat Union collégiale et de l'Association pour l'utilisation raisonnée des médecines alternatives tendant à l'annulation de l'arrêté du 5 mars 2012 doivent être rejetées, ainsi que, par voie de conséquence, leurs conclusions à fin d'injonction. <br/>
<br/>
              7. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance. En revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge tant du syndicat Union collégiale que de l'Association pour l'utilisation raisonnée des médecines alternatives le versement d'une somme de 1 500 euros chacun à l'Union nationale des caisses d'assurance maladie au titre des mêmes dispositions. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête du syndicat Union collégiale et autres est rejetée.<br/>
Article 2 : Le syndicat Union collégiale et l'Association pour l'utilisation raisonnée des médecines alternatives verseront chacun une somme de 1 500 euros à l'Union nationale des caisses d'assurance maladie.<br/>
Article 3 : La présente décision sera notifiée au syndicat Union collégiale, à l'Association pour l'utilisation raisonnée des médecines alternatives, à l'Union nationale des caisses d'assurance maladie et à la ministre des affaires sociales et de la santé.<br/>
Copie en sera adressée pour information au ministre des finances et des comptes publics, à la Confédération des syndicats médicaux français, au Syndicat des médecins libéraux et à la Fédération française des médecins généralistes.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
