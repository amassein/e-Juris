<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000024329335</ID>
<ANCIEN_ID>JG_L_2011_07_000000350486</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/24/32/93/CETATEXT000024329335.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 08/07/2011, 350486</TITRE>
<DATE_DEC>2011-07-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>350486</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jacques Arrighi de Casanova</RAPPORTEUR>
<COMMISSAIRE_GVT/>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu, 1° sous le n° 350486, le recours enregistré le 29 juin 2011 au secrétariat du contentieux du Conseil d'Etat, présenté par le MINISTRE D'ÉTAT, MINISTRE DES AFFAIRES ÉTRANGÈRES ET EUROPÉENNES ; le ministre demande au juge des référés du Conseil d'État : <br/>
<br/>
              1°) à titre principal, d'annuler l'ordonnance n° 1111000/9 du 24 juin 2011 par laquelle le juge des référés du tribunal administratif de Paris, faisant droit à la demande présentée par M. David Joël A sur le fondement de l'article L. 521-2 du code de justice administrative, lui a enjoint de faire bénéficier les jeunes Auxane et Mathis A d'un document de voyage leur permettant d'entrer sur le territoire national, dans les trois jours suivant la notification de l'ordonnance ;<br/>
<br/>
              2°) à titre subsidiaire, de surseoir à statuer jusqu'à ce que l'autorité judiciaire se soit prononcée sur les effets attachés par l'article 47 du code civil aux actes de l'état civil indien ; <br/>
<br/>
<br/>
              il soutient que l'attestation fournie par M. A s'est révélée mensongère et que la personne présentée comme étant la mère des enfants a déclaré le contraire ; qu'ainsi et alors que les tests ADN de M. A ne sont pas valables, les actes de naissance indiens que ce dernier a fournis ne sont pas probants au regard des exigences de l'article 47 du code civil ; que l'acte de renonciation des droits maternels et d'autorisation de ramener les enfants en France est frauduleux ; que, dès lors, il ne permet pas d'apprécier l'intérêt supérieur des enfants ; que le départ des enfants méconnaît les stipulations de l'article 7 de la convention de New York relative aux droits de l'enfant en les privant de leurs racines indiennes et du droit d'être élevés par leur mère ; que l'appréciation des effets attachés, par l'article 47 du code civil, aux actes d'état civil produits par l'intéressé soulève une difficulté sérieuse qui justifierait qu'une question préjudicielle soit posée à la juridiction judiciaire ; <br/>
<br/>
<br/>
<br/>
              Vu l'ordonnance attaquée ;<br/>
<br/>
              Vu le mémoire en défense, enregistré le 6 juillet 2011, présenté par M. David Joël A, qui conclut au rejet du recours et à ce que la somme de 3588 euros soit mise à la charge de l'Etat au titre de l'article L. 761-1 du code de justice administrative ; il demande en outre d'assortir l'injonction de délivrer des documents de voyage d'une astreinte de 200 euros par jour de retard à compter de la notification de l'ordonnance et de décider que celle-ci sera exécutoire aussitôt qu'elle aura été rendue, en application de l'article R. 522-13 du code de justice administrative ; il soutient que le refus de délivrer les documents de voyage à Auxane et Mathis est contraire au décret du 30 décembre 2004 relatif aux attributions des chefs de poste consulaire en matière de titres de voyage, dès lors qu'en tant que ressortissant français, il les a reconnus ; que la filiation de ces enfants à son égard est établie par des actes de naissance, des actes de reconnaissance et des tests génétiques et ne fait l'objet d'aucune action en contestation de paternité ; que la tentative de contestation de la filiation maternelle de ses enfants est sans incidence sur la réalité de leur filiation paternelle ; que ce refus porte une atteinte grave à l'intérêt supérieur de ses enfants garanti par les stipulations de l'article 3-1 de la convention de New York relative aux droits de l'enfant, dès lors qu'étant français par application de l'article 18 du code civil, leur intérêt est de pouvoir entrer sur le territoire français et d'y être élevé dans des conditions assurant leur épanouissement, leur développement et une prise en charge adéquate de leur état de santé ; qu'il porte une atteinte grave au droit au respect de la vie privée et familiale de sa famille garanti par l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et à la liberté d'aller et venir ; que la condition d'urgence est remplie, dès lors que l'état de santé des enfants est précaire et que, son visa arrivant à expiration le 6 août 2011, il serait contraint de les abandonner en Inde et que cette situation lui cause des difficultés financières considérables ;<br/>
<br/>
<br/>
              Vu, 2° sous le n° 350487, le recours enregistré le 29 juin 2011 au secrétariat du contentieux du Conseil d'Etat, par lequel le même ministre demande au juge des référés du Conseil d'État d'ordonner, sur le fondement de l'article R. 811-17 du code de justice administrative, le sursis à l'exécution de la même ordonnance du juge des référés du tribunal administratif de Paris ;<br/>
<br/>
              il soutient que l'exécution de l'ordonnance contestée risque d'entraîner des conséquences difficilement réparables, dès lors que les titres de voyage une fois délivrés seront utilisés et finiront de produire leurs effets juridiques et que le départ des enfants pour la France pourrait priver la véritable mère de ses droits à leurs égards ; que les moyens développés dans son recours en appel doivent être regardés comme sérieux ; <br/>
<br/>
<br/>
              Vu l'ordonnance attaquée ;<br/>
<br/>
              Vu le mémoire en défense, enregistré le 6 juillet 2011, présenté par M. A, qui conclut au rejet du recours et à ce que la somme de 3 588 euros soit mise à la charge de l'Etat au titre de l'article L. 761-1 du code de justice administrative ; il soutient, pour les motifs développés dans son mémoire présenté sous le n° 350486, que les moyens soulevés au fond ne sont pas suffisamment sérieux pour justifier l'application des dispositions de l'article R. 811-17 du code de justice administrative ; qu'aucune femme n'étant venue revendiquer un quelconque droit à l'égard des enfants, leur voyage ne pourra pas entraîner de conséquences difficilement réparables ; <br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu la convention internationale relative aux droits de l'enfant ; <br/>
<br/>
              Vu le code civil ;<br/>
<br/>
              Vu le décret n° 2004-1543 du 30 décembre 2004 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, le MINISTRE D'ÉTAT, MINISTRE DES AFFAIRES ÉTRANGÈRES ET EUROPÉENNES et, d'autre part, M. A ;<br/>
<br/>
              Vu le procès-verbal de l'audience du 7 juillet 2011 à 15 heures, au cours de laquelle ont été entendus :<br/>
<br/>
               - les représentants du MINISTRE D'ÉTAT, MINISTRE DES AFFAIRES ÉTRANGÈRES ET EUROPÉENNES ;<br/>
<br/>
              - Me Vexliard, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. A ; <br/>
<br/>
              - la représentante de M. A ;<br/>
<br/>
<br/>
<br/>
              Considérant que l'appel du MINISTRE D'ÉTAT, MINISTRE DES AFFAIRES ÉTRANGÈRES ET EUROPÉENNES et sa demande de sursis à exécution sont dirigés contre la même ordonnance ; qu'il y a lieu de les joindre pour statuer par une seule ordonnance ;<br/>
<br/>
              Sur l'appel dirigé contre l'ordonnance contestée :<br/>
<br/>
              Considérant qu'aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures. " ; <br/>
<br/>
              Considérant qu'il résulte de l'instruction que M. A, ressortissant français, a demandé le 23 mai 2011 au consul général de France à Bombay (Inde) de délivrer aux jeunes Auxane et Mathis, nés le 12 mai 2011 à Mumbai, le laissez-passer prévu par le décret du 30 décembre 2004 afin de lui permettre de les ramener en France, où il réside habituellement ; qu'après plusieurs investigations, le consul général lui a transmis, le 14 juin 2011, une lettre du 10 juin par laquelle le directeur des Français à l'étranger et de l'administration consulaire faisait connaître à son avocate qu'une vérification plus poussée était nécessaire avant de faire droit à la demande de laissez-passer ; que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Paris, saisi par M. A sur le fondement des dispositions de l'article L. 521-2 du code de justice administrative, a enjoint aux autorités compétentes de faire bénéficier les jeunes Auxane et Mathis, dans les trois jours de la notification de l'ordonnance, d'un document de voyage leur permettant d'entrer sur le territoire national ; <br/>
<br/>
              Considérant, en premier lieu, que la délivrance du laissez-passer prévue par le décret du 30 décembre 2004 ne peut avoir lieu, en application de l'article 7 de ce décret, qu'après vérification de l'identité et de la nationalité française de la personne concernée ; que si tout acte de l'état civil des Français fait en pays étranger fait en principe foi en vertu de l'article 47 du code civil, c'est, selon les termes mêmes de cet article, sous réserve qu'il ne soit pas établi, le cas échéant après toutes vérifications utiles, que cet acte est irrégulier, falsifié ou que les faits qui y sont déclarés ne correspondent pas à la réalité ; qu'à cet égard, les documents produits en appel par le ministre, notamment les traductions certifiées conformes de pièces dont l'administration n'avait produit en première instance que les originaux en langue anglaise, font apparaître des contradictions, d'une part, entre l'acte notarié du 25 mai 2011 par lequel la personne désignée par l'état civil indien comme étant la mère a déclaré renoncer à ses droits parentaux et l'attestation revêtue d'une signature qui apparaît comme étant la sienne, certifiant n'avoir jamais été enceinte ni accouché d'aucun enfant et, d'autre part, l'attestation de M. A déclarant sur l'honneur qu'il n'avait pas eu recours à une gestation pour autrui et les termes de la lettre adressée par le médecin, directeur de l'hôpital où sont nés les enfants en cause, attestant, d'après sa traduction certifiée conforme, qu'ils " sont nés le 12 mai 2011 dans cet hôpital d'une mère porteuse " dont il indique ne pouvoir révéler l'identité ; qu'au vu de ces éléments, notamment des incertitudes quant à l'identité et la volonté exactes de la mère des enfants en cause, la position de l'administration, faisant connaître dans les courriers des 10 et 14 juin 2011 qu'une vérification plus poussée était nécessaire avant de faire droit à la demande présentée le 23 mai précédent par M. A, ne fait pas apparaître d'illégalité manifeste au regard des dispositions combinées du décret du 30 décembre 2004 et de l'article 47 du code civil ni, par suite, de la liberté d'aller et venir dont l'intéressé se prévaut ;<br/>
<br/>
              Considérant, en second lieu, qu'eu égard à ces mêmes contradictions et incertitudes, il n'apparaît pas davantage, en l'état du dossier soumis au juge des référés, que le fait, pour l'administration, d'estimer que ces circonstances ne lui permettent pas d'apprécier sans autre vérification l'intérêt supérieur des enfants, traduise une méconnaissance manifeste des stipulations de l'article 3-1 de la convention relative aux droits de l'enfant ni, pour les même motifs, de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède qu'en l'absence d'atteinte manifestement illégale aux libertés fondamentales invoquées par M. A, le ministre est fondé à soutenir que c'est à tort que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Paris a fait droit à la demande qui lui était présentée ;<br/>
<br/>
              Sur le recours aux fins de sursis à exécution :<br/>
<br/>
              Considérant que, la présente ordonnance statuant sur l'appel du ministre, son recours tendant à ce qu'il soit sursis à l'exécution de l'ordonnance attaquée est devenu sans objet ;<br/>
<br/>
              Sur l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que les conclusions présentées à ce titre par M. A ne peuvent qu'être rejetées ;<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Paris du 24 juin 2011 est annulée.<br/>
Article 2 : La demande présentée par M. A et ses conclusions présentées devant le Conseil d'Etat sont rejetées.<br/>
Article 3 : Il n'y a pas lieu de statuer sur les conclusions du recours n° 350487. <br/>
Article 4 : La présente ordonnance sera notifiée au MINISTRE D'ÉTAT, MINISTRE DES AFFAIRES ÉTRANGÈRES ET EUROPÉENNES et à M. David Joël A. <br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-035-03-03-01-02 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ TENDANT AU PRONONCÉ DE MESURES NÉCESSAIRES À LA SAUVEGARDE D'UNE LIBERTÉ FONDAMENTALE (ART. L. 521-2 DU CODE DE JUSTICE ADMINISTRATIVE). CONDITIONS D'OCTROI DE LA MESURE DEMANDÉE. ATTEINTE GRAVE ET MANIFESTEMENT ILLÉGALE À UNE LIBERTÉ FONDAMENTALE. ATTEINTE GRAVE ET MANIFESTEMENT ILLÉGALE. - ABSENCE - REFUS DE DÉLIVRANCE D'UN LAISSEZ-PASSER POUR L'ENTRÉE SUR LE TERRITOIRE FRANÇAIS D'ENFANTS NÉS À L'ÉTRANGER, AU REGARD DES INCERTITUDES QUANT À L'IDENTITÉ ET LA VOLONTÉ EXACTES DE LA MÈRE DES ENFANTS EN CAUSE [RJ1].
</SCT>
<ANA ID="9A"> 54-035-03-03-01-02 Ressortissant français souhaitant obtenir un document de voyage permettant à ses deux enfants, nés en Inde, d'entrer sur le territoire français. Eu égard aux pièces fournies en appel par l'administration, faisant apparaître des incertitudes et des contradictions quant à l'identité de la mère, à un éventuel recours à une mère porteuse, à son éventuel renoncement à son autorité parentale, la position de l'administration, qui a fait connaître qu'une vérification plus poussée était nécessaire avant de faire droit à la demande, ne fait pas apparaître d'illégalité manifeste, ni au regard des dispositions combinées du décret du 30 décembre 2004 et de l'article 47 du code civil ni, par suite, de la liberté d'aller et venir, et n'est pas entachée de méconnaissance manifeste des stipulations de l'article 3-1 de la convention relative aux droits de l'enfant ni, pour les même motifs, de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 4 mai 2011, Ministre des affaires étrangères et européennes c/ M. Morin, n° 348778, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
