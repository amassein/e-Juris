<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043358784</ID>
<ANCIEN_ID>JG_L_2021_04_000000441998</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/35/87/CETATEXT000043358784.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 08/04/2021, 441998, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-04-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>441998</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>M. Clément Tonon</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:441998.20210408</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés le 20 juillet 2020 et le 25 janvier 2021 au secrétariat du contentieux du Conseil d'Etat, M. B... A... demande au Conseil d'Etat d'annuler pour excès de pouvoir le décret en date du <br/>
10 mars 2020 accordant son extradition aux autorités américaines.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le traité d'extradition entre la France et les Etats-Unis d'Amérique signé à Paris le 23 avril 1996 ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le code de justice administrative et le décret n°2020-1406 du 18 novembre<br/>
2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Clément Tonon, auditeur,<br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Célice, Texidor, Périer, avocat de M. A... ; <br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Par le décret du 20 mars 2020, le Premier ministre a accordé aux autorités américaines l'extradition du requérant pour l'exécution d'un mandat d'arrêt décerné le 31 janvier 2018 par le juge fédéral au tribunal fédéral pour le district central de Californie pour des faits qualifiés d'association de malfaiteurs en vue de distribuer et posséder avec l'intention de distribuer de la méthamphétamine, de possession de méthamphétamine à bord d'un aéronef enregistré aux Etats-Unis aux fins de distribution et de participation à une entreprise criminelle continue. <br/>
<br/>
              2. En premier lieu, s'il ressort des pièces du dossier que l'intéressé est également recherché pour distribution de méthamphétamine aux fins d'importation illicite, l'absence de cette précision ne saurait conduire à regarder le décret attaqué, qui comporte l'énoncé des considérations de droit et de fait qui en constituent le fondement, comme insuffisamment motivé au regard des exigences de l'article L. 211-2 du code des relations entre le public et l'administration. <br/>
<br/>
              3. En deuxième lieu, l'article 3 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales stipule que : " Nul ne peut être soumis à la torture ni à des peines ou traitements inhumains ou dégradants ". Ces stipulations font obstacle à l'extradition d'une personne exposée à une peine incompressible de réclusion perpétuelle, sans possibilité de réexamen et, le cas échéant, d'élargissement. <br/>
<br/>
              4. M. A... soutient qu'en cas d'exécution du décret attaqué, il risque d'être exposé à des traitements inhumains ou dégradants en raison du caractère incompressible de la peine de réclusion criminelle à perpétuité qu'il encourt. Toutefois, par note verbale du 5 février 2021 transmise par voie diplomatique, le Gouvernement des Etats-Unis d'Amérique a assuré les autorités françaises que si l'extradition de M. A... était accordée, ce dernier ne serait plus poursuivi que sur le fondement de trois chefs d'accusations tenant à l'association de malfaiteurs en vue de la distribution et de la possession avec l'intention de distribuer au moins 50 grammes d'un mélange et d'une substance contenant une quantité détectable de méthamphétamine, à la distribution d'au moins 50 grammes d'un mélange et d'une substance contenant une quantité détectable de méthamphétamine et à la possession avec l'intention de distribuer au moins 50 grammes d'un mélange et d'une substance contenant une quantité détectable de la méthamphétamine à bord d'un aéronef immatriculé aux États-Unis, dont il n'est pas contesté par le requérant qu'aucun n'est punissable d'une peine de réclusion à perpétuité, contrairement aux quatre chefs d'accusation qui figuraient sur l'acte d'accusation initial fondant la demande d'extradition. Il résulte de cet engagement des autorités américaines à poursuivre <br/>
M. A... sur le fondement de chefs d'accusations inférieurs à ceux précédemment retenus contre lui et à ne plus poursuivre l'infraction de participation à une entreprise criminelle continue que l'intéressé n'encourt plus la peine de réclusion à perpétuité pour les faits qui lui sont reprochés. En outre, si M. A... déclare redouter son incarcération aux Etats-Unis eu égard aux conditions de détention dans ce pays et craindre d'être exposé à des traitements contraires à l'article 3 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, les pièces du dossier ne permettent pas de tenir pour établis les risques personnels qu'il allègue. Par suite, le moyen tiré de la méconnaissance des stipulations de l'article 3 de la convention européenne des droits de l'homme et des libertés fondamentales ne peut qu'être écarté.<br/>
<br/>
              5. En dernier lieu, M. A... soutient que le décret attaqué pourrait avoir pour lui des conséquences d'une exceptionnelle gravité dès lors qu'il risque d'être extradé vers les Philippines après avoir été jugé par les juridictions américaines ou, le cas échéant, après avoir purgé sa peine dans ce pays, et qu'il peut craindre d'être exposé à des traitements inhumains et dégradants aux Philippines compte tenu des accusations de trafic de stupéfiants portées contre lui dans ce pays. Toutefois, il résulte de l'article 20 du traité d'extradition entre la France et les Etats-Unis d'Amérique signé à Paris le 23 avril 1996 que l'Etat contractant à qui une personne a été livrée ne peut la livrer à un Etat tiers sans le consentement de l'Etat requis, sauf dans le cas où cette personne n'a pas quitté le territoire de l'Etat requérant dans les trente jours de son élargissement définitif ou si elle y est retournée après l'avoir quitté. Ainsi, le moyen tiré de ce que le décret serait entaché d'erreur de droit pour avoir été pris sans avoir obtenu des autorités américaines la garantie que M. A... ne ferait pas l'objet d'une extradition vers les Philippines ne peut qu'être écarté.<br/>
<br/>
              6. Il résulte de tout ce qui précède que M. A... n'est pas fondé à demander l'annulation du décret du 10 mars 2020 accordant son extradition aux autorités américaines. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. A... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. B... A... et au garde des sceaux, ministre de la justice. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
