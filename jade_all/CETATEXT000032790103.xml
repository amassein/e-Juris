<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032790103</ID>
<ANCIEN_ID>JG_L_2016_06_000000385091</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/79/01/CETATEXT000032790103.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 27/06/2016, 385091, Publié au recueil Lebon</TITRE>
<DATE_DEC>2016-06-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>385091</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON</AVOCATS>
<RAPPORTEUR>M. Jacques Reiller</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:385091.20160627</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par sa délibération n° 2014-219 du 22 mai 2014, la Commission nationale de l'informatique et des libertés (CNIL) a refusé d'autoriser la commune de Gujan-Mestras à mettre en oeuvre un dispositif de vidéo-protection urbaine comportant des dispositifs de lecture automatisée des plaques d'immatriculation des véhicules circulant sur son territoire.<br/>
<br/>
              Par une requête, un mémoire complémentaire et un mémoire en réplique, enregistrés les 13 octobre 2014, 12 janvier 2015 et 22 mai 2015 au secrétariat du contentieux du Conseil d'Etat, la commune de Gujan-Mestras demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler, pour excès de pouvoir, cette délibération ;<br/>
<br/>
              2°) de mettre à la charge de la CNIL la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
              Vu :<br/>
              - le code de la sécurité intérieure ; <br/>
              - la loi n° 78-17 du 6 janvier 1978 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jacques Reiller, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boré, Salve de Bruneton, avocat de la commune de Gujan-Mestras ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 1er juin 2016, présentée par la commune de Gujan-Mestras ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier, d'une part, que la commune de Gujan-Mestras, en vue de se doter d'un système de vidéo-protection, a adressé au préfet de la Gironde, le 12 septembre 2011, en application des dispositions du III de l'article 10 de la loi du 21 janvier 1995 d'orientation et de programmation relative à la sécurité, alors en vigueur et désormais reprises à l'article L. 252-1 du code de la sécurité intérieure, une demande d'autorisation d'un dispositif composé de 15 caméras et de 7 lecteurs automatisés de plaques d'immatriculation ; que, par un arrêté du 21 octobre 2011, le préfet de la Gironde a autorisé, pour une période de 5 ans renouvelable, la mise en oeuvre de ce dispositif à l'exception des 7 lecteurs automatisés pour lesquels était requise une autorisation de la Commission nationale de l'informatique et des libertés ;<br/>
<br/>
              2. Considérant, d'autre part, que le 7 août 2013, la commune de Gujan-Mestras a adressé à la Commission une demande d'autorisation concernant un dispositif d'exploitation des images des véhicules circulant sur son territoire par le biais de caméras de vidéoprotection ; que le dispositif envisagé, géré par la police municipale de la commune, avait pour objet de collecter et conserver, durant 21 jours, les données relatives aux plaques d'immatriculation de tous les véhicules circulant sur la voie publique aux fins de mises à disposition, sur réquisition judiciaire, de la gendarmerie nationale pour identification des auteurs d'infraction ; que la commune de Gujan-Mestras demande l'annulation pour excès de pouvoir de la délibération n° 2014-219 du 22 mai 2014 par laquelle la Commission nationale de l'informatique et des libertés a refusé d'autoriser la mise en oeuvre par la commune de ce traitement automatisé de contrôle des données signalétiques des véhicules ; <br/>
<br/>
              3. Considérant, en premier lieu, qu'aux termes de l'article L. 251-2 du code de la sécurité intérieure, dans sa rédaction applicable à la date du refus litigieux : " La transmission et l'enregistrement d'images prises sur la voie publique par le moyen de la vidéoprotection peuvent être mis en oeuvre par les autorités publiques compétentes aux fins d'assurer :/ 1° La protection des bâtiments et installations publics et de leurs abords ;/ 2° La sauvegarde des installations utiles à la défense nationale ; /3° La régulation des flux de transport ;/ 4° La constatation des infractions aux règles de la circulation ;/ 5° La prévention des atteintes à la sécurité des personnes et des biens dans des lieux particulièrement exposés à des risques d'agression, de vol ou de trafic de stupéfiants ainsi que la prévention, dans des zones particulièrement exposées à ces infractions, des fraudes douanières prévues par le dernier alinéa de l'article 414 du code des douanes et des délits prévus à l'article 415 du même code portant sur des fonds provenant de ces mêmes infractions ;/ 6° La prévention d'actes de terrorisme, dans les conditions prévues au chapitre III du titre II du présent livre ;/ 7° La prévention des risques naturels ou technologiques ;/ 8° Le secours aux personnes et la défense contre l'incendie ;/ 9° La sécurité des installations accueillant du public dans les parcs d'attraction./ Il peut être également procédé à ces opérations dans des lieux et établissements ouverts au public aux fins d'y assurer la sécurité des personnes et des biens lorsque ces lieux et établissements sont particulièrement exposés à des risques d'agression ou de vol " ; que, si en vertu du premier alinéa de l'article L. 252-1 du même code, l'installation des systèmes de vidéoprotection relevant de l'article L. 251-2 est subordonnée à une autorisation du représentant de l'Etat dans le département, le dernier alinéa du même article prévoit que doivent être autorisés par la Commission nationale de l'informatique et des libertés, en application de la loi du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, les systèmes "installés sur la voie publique ou dans des lieux ouverts au public dont les enregistrements sont utilisés dans des traitements automatisés ou contenus dans des fichiers structurés selon des critères permettant d'identifier, directement ou indirectement, des personnes physiques " ; <br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier, et notamment de la demande adressée par la commune de Gujan-Mestras à la Commission nationale de l'informatique et des libertés, que le dispositif litigieux avait pour seule finalité de mettre les données collectées à la disposition de la gendarmerie nationale pour l'exercice de ses missions de police judiciaire ; qu'il suit de là que la Commission a fait une exacte application de l'article L. 252-1 du code de la sécurité intérieure en retenant que cette finalité, qui n'est pas au nombre de celles visées à l'article L. 251-2 du même code, n'était pas légitime et en refusant, pour ce motif, l'autorisation sollicitée ;<br/>
<br/>
              5. Considérant, en deuxième lieu, qu'aux termes de l'article L. 233-1 du code de la sécurité intérieure : " Afin de prévenir et de réprimer le terrorisme, de faciliter la constatation des infractions s'y rattachant, de faciliter la constatation des infractions criminelles ou liées à la criminalité organisée au sens de l'article 706-73 du code de procédure pénale, des infractions de vol et de recel de véhicules volés, des infractions de contrebande, d'importation ou d'exportation commises en bande organisée, prévues et réprimées par le dernier alinéa de l'article 414 du code des douanes, ainsi que la constatation, lorsqu'elles portent sur des fonds provenant de ces mêmes infractions, de la réalisation ou de la tentative de réalisation des opérations financières définies à l'article 415 du même code et afin de permettre le rassemblement des preuves de ces infractions et la recherche de leurs auteurs, les services de police et de gendarmerie nationales et des douanes peuvent mettre en oeuvre des dispositifs fixes ou mobiles de contrôle automatisé des données signalétiques des véhicules prenant la photographie de leurs occupants, en tous points appropriés du territoire, en particulier dans les zones frontalières, portuaires ou aéroportuaires ainsi que sur les grands axes de transit national ou international. L'emploi de tels dispositifs est également possible, par les services de police et de gendarmerie nationales, à titre temporaire, pour la préservation de l'ordre public, à l'occasion d'événements particuliers ou de grands rassemblements de personnes, par décision de l'autorité administrative " ; qu'aux termes de l'article L. 233-2 du même code : " Pour les finalités mentionnées à l'article L. 233-1, les données à caractère personnel collectées à l'occasion des contrôles susmentionnés peuvent faire l'objet de traitements automatisés mis en oeuvre par les services de police et de gendarmerie nationales et les services des douanes et soumis aux dispositions de la loi du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés.(...) " ;<br/>
<br/>
              6. Considérant que la Commission nationale de l'informatique et des libertés n'a pas méconnu l'article L. 233-1 du code de la sécurité intérieure, qui autorise les seuls services des douanes, de police et de gendarmerie nationales à mettre en oeuvre les dispositifs de contrôle automatisé des données signalétiques des véhicules prenant la photographie de leurs occupants pour les finalités qu'il prévoit, en estimant qu'il ne pouvait servir de fondement légal à l'autorisation sollicitée ; qu'en effet, si la commune requérante soutient que le dispositif litigieux doit être regardé comme mis en oeuvre par la gendarmerie nationale, alors même qu'il devait être géré par les services de police municipale, dès lors que les données collectées, conservées durant 21 jours, étaient destinées à être mises à la disposition de la gendarmerie nationale à des fins d'aide à l'identification des auteurs d'infractions, la personne chargée de la mise en oeuvre d'un dispositif, qui correspond au responsable du traitement pour l'application de la loi du 6 janvier 1978, est nécessairement celle qui est chargée de le gérer ; que, dans ces conditions, la Commission a fait une exacte application des dispositions précitées en estimant qu'elles s'opposaient à l'autorisation du dispositif litigieux, qui devait être regardé comme mis en oeuvre par les services de police municipale de la commune de Gujan-Mestras ; <br/>
<br/>
              7. Considérant, en troisième lieu, que la délibération de la Commission nationale de l'informatique et des libertés retient, à titre surabondant, plusieurs autres motifs tirés respectivement de ce que le traitement sollicité ne relevait pas du régime de l'autorisation mais d'un acte réglementaire pris sur son avis, conduisait à effectuer une collecte de données excessive et avait retenu une durée de conservation méconnaissant l'article 6 de la loi du 6 janvier 1978 ; que les moyens articulés par la commune requérante à l'encontre de ces motifs, sur lesquels la Commission ne s'est pas fondée pour prendre la délibération attaquée, doivent être écartés comme inopérants ; qu'il en va de même du moyen tiré de ce que le dispositif litigieux ne saurait être qualifié, eu égard à ses caractéristiques techniques, comme un dispositif de contrôle automatisé des données signalétiques des véhicules prenant la photographie de leurs occupants; qu'en tout état de cause, il n'appartenait pas à la Commission de transmettre à l'autorité réglementaire la demande dont la commune requérante l'avait saisie ; <br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que la commune de Gujan-Mestras n'est pas fondée à demander l'annulation de la délibération de la Commission nationale de l'informatique et des libertés  du 22 mai 2014 ; que sa requête, y compris les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative doit donc être rejetée ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de la commune de Gujan-Mestras est rejetée. <br/>
<br/>
Article 2 : La présente décision sera notifiée à la commune de Gujan-Mestras et à la Commission nationale de l'informatique et des libertés. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-07-06 DROITS CIVILS ET INDIVIDUELS. - 1) FINALITÉS POUR LESQUELLES LA TRANSMISSION ET L'ENREGISTREMENT D'IMAGES PRISES SUR LA VOIE PUBLIQUE PAR LE MOYEN DE LA VIDÉOPROTECTION PEUVENT ÊTRE MISES EN OEUVRE (ART. L. 251-2 DU CSI) - EXCLUSION - MISE À LA DISPOSITION DE LA GENDARMERIE NATIONALE DES DONNÉES COLLECTÉES - 2) DISPOSITIFS DE CONTRÔLE AUTOMATISÉ DES DONNÉES SIGNALÉTIQUES DES VÉHICULES PRENANT LA PHOTOGRAPHIE DE LEURS OCCUPANTS (ART. L. 233-1 DU CSI) - GESTIONNAIRES AUTORISÉS - SERVICES DES DOUANES, DE POLICE ET DE GENDARMERIE NATIONALES UNIQUEMENT.
</SCT>
<ANA ID="9A"> 26-07-06 1) L'article L. 251-2 du code de la sécurité intérieure (CSI) liste les finalités pour lesquelles la transmission et l'enregistrement d'images prises sur la voie publique par le moyen de la vidéoprotection peuvent être mis en oeuvre par les autorités publiques compétentes. Mettre les données collectées à la disposition de la gendarmerie nationale pour l'exercice de ses missions de police judiciaire, qui n'est pas aux nombres des finalités visées par cet article, ne constitue pas, pour un dispositif de transmission et d'enregistrement d'images prises sur la voie publique par le moyen de la vidéoprotection, une finalité légitime.... ,,2) L'article L. 233-1 du CSI autorise les seuls services des douanes, de police et de gendarmerie nationales à mettre en oeuvre les dispositifs de contrôle automatisé des données signalétiques des véhicules prenant la photographie de leurs occupants pour les finalités qu'il prévoit. Par suite, une commune ne saurait mettre en oeuvre un tel dispositif, alors mêmes que les données collectées seraient destinées à être mises à la disposition de la gendarmerie nationale à des fins d'aide à l'identification des auteurs d'infractions.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
