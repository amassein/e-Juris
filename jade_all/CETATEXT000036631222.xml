<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036631222</ID>
<ANCIEN_ID>JG_L_2018_02_000000409221</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/63/12/CETATEXT000036631222.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 21/02/2018, 409221, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-02-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>409221</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Alain Seban</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:409221.20180221</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. et Mme C...B...ont demandé au tribunal administratif de Paris de condamner l'Etat à leur verser la somme de 2 000 euros chacun en leur nom personnel et la somme de 2 000 euros en qualité de représentants légaux de leur enfant mineur, en réparation du préjudice subi du fait de l'absence de proposition de relogement. Par un jugement n° 1607561/6-1 du 17 mars 2017, le tribunal administratif a condamné l'Etat à verser la somme de 200 euros à Mme B... et rejeté le surplus des conclusions de la demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 22 mars et 20 juillet 2017 au secrétariat du contentieux du Conseil d'Etat, M. et Mme B... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leurs conclusions de première instance.<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu :<br/>
<br/>
                          - le code de la construction et de l'habitation ;<br/>
	- le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alain Seban, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de M. et MmeB....<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme B...a été reconnue prioritaire et devant être relogée en urgence, sur le fondement de l'article L. 411-2-3 du code de la construction et de l'habitation, par une décision du 20 juin 2014 de la commission de médiation de Paris, au motif qu'elle était sans logement et hébergée chez un tiers ; que, par un jugement du 16 avril 2015, le tribunal administratif de Paris, saisi par MmeB...  sur le fondement du I de l'article L. 441-2-3-1 du même code, a enjoint au préfet de la région Ile-de-France, préfet de Paris, d'assurer son relogement ; qu'en l'absence d'offre de relogement, M. et Mme B...ont, le 14 mai 2016, demandé au tribunal administratif de condamner l'Etat à leur verser des indemités de 2 000 euros chacun en leur nom personnel et de 2 000 euros en leur qualité de représentant légaux de leur enfant mineur ; que, par le jugement du 17 mars 2017 contre lequel ils se pourvoient en cassation, le tribunal administratif a, d'une part, rejeté les conclusions présentées par M. B... en son nom propre et par M. et Mme B...en leur qualité de représentants légaux de leur enfant mineur, et, d'autre part, condamné l'Etat à verser à Mme B...la somme de 200 euros en réparation des troubles de toute nature dans les conditions d'existence résultant du maintien de la situation ayant motivé la décision de la commission de médiation ;<br/>
<br/>
              2. Considérant que, lorsqu'une personne a été reconnue comme prioritaire et comme devant être logée ou relogée d'urgence par une commission de médiation, en application des dispositions de l'article L. 441-2-3 du code de la construction et de l'habitation, et que le juge administratif a ordonné son logement ou son relogement par l'Etat, en application de l'article L. 441-2-3-1 de ce code, la carence fautive de l'Etat à exécuter ces décisions dans le délai imparti engage sa responsabilité à l'égard du seul demandeur, au titre des troubles dans les conditions d'existence résultant du maintien de la situation qui a motivé la décision de la commission ; que ces troubles doivent être appréciés en fonction des conditions de logement qui ont perduré du fait de la carence de l'Etat, de la durée de cette carence et du nombre de personnes composant le foyer du demandeur pendant la période de responsabilité de l'Etat ; <br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la demande que la commission de médiation a accueillie par sa décision du 20 juin 2014 avait été présentée par MmeB... ; que, par suite, le tribunal administratif n'a pas commis d'erreur de droit en retenant que la responsabilité de l'Etat n'était engagée qu'à son égard et en rejetant, en conséquence, les demandes d'indemnité présentées par M. B...en son nom propre et par M. et Mme B...en leur qualité de représentants légaux de leur enfant mineur ;<br/>
<br/>
              4. Mais considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme B...a justifié du maintien de la situation d'hébergement par un tiers qui avait motivé la décision de la commission de médiation et a indiqué sans être contredite occuper avec son enfant en bas âge le salon de la famille qui les héberge ; qu'en accordant une indemnité de 200 euros en réparation des troubles que cette situation avait entraînés entre l'expiration, le 20 décembre 2014, du délai imparti au préfet pour assurer le relogement de la famille et la date du jugement, soit pendant une période de deux ans et trois mois, le tribunal administratif ne peut être regardé comme ayant apprécié ces troubles en tenant compte, notamment, du nombre de personnes composant le foyer de l'intéressée ; que son jugement est, par suite, entaché d'erreur de droit et doit être annulé en tant qu'il statue sur la responsabilité de l'Etat à l'égard de Mme B..., sans qu'il soit besoin d'examiner les autres moyens du pourvoi ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 17 mars 2017 du tribunal administratif de Paris est annulé en tant qu'il statue sur la responsabilité de l'Etat à l'égard de MmeB....<br/>
<br/>
Article 2 : L'affaire est renvoyée, dans la mesure de la cassation prononcée ci-dessus, au tribunal administratif de Paris.<br/>
<br/>
		Article 3 : Le surplus des conclusions de la requête est rejeté.<br/>
<br/>
Article 4 : La présente décision sera notifiée à Mme A...B..., à M. C...B...et au ministre de la cohésion des territoires.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
