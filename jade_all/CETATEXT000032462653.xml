<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032462653</ID>
<ANCIEN_ID>JG_L_2016_04_000000398782</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/46/26/CETATEXT000032462653.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 21/04/2016, 398782, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-04-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>398782</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2016:398782.20160421</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société de droit espagnol Goizuetako Estructuras SL, disposant d'un établissement à Hendaye (64700), représentée par son gérant, a demandé au juge des référés du tribunal administratif de Pau, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'ordonner la suspension de l'exécution de l'arrêté du préfet des Pyrénées-Atlantiques du 31 mars 2016 ordonnant la fermeture de ses chantiers pour une durée d'un mois et son exclusion des contrats administratifs pour une durée de deux mois. Par une ordonnance n° 1600616 du 8 avril 2016, le juge des référés du tribunal administratif de Pau a rejeté sa demande. <br/>
<br/>
              Par une requête, enregistrée le 14 avril 2016 au secrétariat du contentieux du Conseil d'Etat, la société Goizuetako Estructuras SL, représentée par son gérant, demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de faire droit à sa demande de première instance et d'enjoindre à l'Etat de cesser toute poursuite, y compris pénale, à son égard ;<br/>
              3°) de mettre à la charge de l'Etat la somme de 8 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Elle soutient que : <br/>
              - la condition d'urgence est remplie, dès lors que l'arrêté de fermeture préjudicie de manière grave et immédiate à ses intérêts économiques et financiers ; <br/>
              - l'arrêté contesté porte une atteinte grave et manifestement illégale aux libertés fondamentales que sont la liberté d'entreprendre et la liberté du commerce et de l'industrie ;<br/>
              - aucune infraction ne pouvait être retenue à son encontre dès lors que les salariés étaient en situation de détachement au sein de leur propre entreprise ;<br/>
              - l'arrêté préfectoral a été pris en méconnaissance des dispositions de l'article R. 8272-9 du code du travail dès lors qu'il concerne tous les chantiers de l'établissement et que les avis des maitres d'ouvrage des chantiers non concernés par les infractions n'ont pas été requis ;<br/>
              - l'arrêté a été pris en méconnaissance du principe du caractère contradictoire de la procédure dès lors qu'elle n'a pas été informée que la décision de fermeture pourrait concerner des chantiers pour lesquels aucune infraction n'avait été relevée ;<br/>
              - l'auteur de l'ordonnance attaquée a commis une erreur de droit et méconnu l'exigence d'impartialité en faisant référence aux conditions de travail sur les chantiers, qu'il a qualifiées de déplorables ;<br/>
              - le préfet a méconnu les dispositions des articles L. 8272-2 et R. 8272-8 du code du travail en omettant de rapporter le nombre de salariés concernés à l'effectif global de l'entreprise et en lui infligeant une sanction manifestement disproportionnée au regard de la faible proportion des salariés concernés, de la régularisation opérée et du caractère conjoncturel de l'activité exercée en France.<br/>
<br/>
              	Par un mémoire en défense, enregistré le 19 avril 2016, la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social conclut au rejet de la requête. Elle soutient que les moyens soulevés par la société Goizuetako Estructuras SL ne sont pas fondés.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le règlement (CE) n° 987/2009 du Parlement européen et du Conseil du 16 septembre 2009 ;<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, la société Goizuetako Estructuras SL et, d'autre part, la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social ;<br/>
<br/>
              Vu le procès-verbal de l'audience publique du 19 avril 2016 à 15 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Lyon-Caen, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la société Goizuetako Estructuras SL ;<br/>
<br/>
              - les représentants de la société requérante ;<br/>
<br/>
              - les représentants de la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social ;<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction.<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. En vertu de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ".<br/>
<br/>
              2. Par un arrêté du 31 mars 2016, le préfet des Pyrénées-Atlantiques a prononcé la sanction administrative de fermeture des chantiers de l'établissement Goizuetako Estructuras SL pour une durée d'un mois à compter de la notification de cet arrêté, ainsi que son exclusion, pour une durée de deux mois, des contrats administratifs. La société Goizuetako Estructuras SL a introduit, sur le fondement des dispositions de l'article L. 521-2 du code de justice administrative, une demande tendant à la suspension de l'exécution de cet arrêté. Elle fait appel de l'ordonnance par laquelle le juge des référés du tribunal administratif de Pau a rejeté sa demande pour défaut d'atteinte grave et manifestement illégale à une liberté fondamentale.<br/>
              3. Aux termes de l'article L. 8272-2 du code du travail : " Lorsque l'autorité administrative a connaissance d'un procès-verbal relevant une infraction prévue aux 1° à 4° de l'article L. 8211-1 ou d'un rapport établi par l'un des agents de contrôle mentionnés à l'article L. 8271-1-2 constatant un manquement prévu aux mêmes 1° à 4°, elle peut, si la proportion de salariés concernés le justifie, eu égard à la répétition et à la gravité des faits constatés et à la proportion de salariés concernés, ordonner par décision motivée la fermeture de l'établissement ayant servi à commettre l'infraction, à titre provisoire et pour une durée ne pouvant excéder trois mois. (...) / Les modalités d'application du présent article ainsi que les conditions de sa mise en oeuvre aux chantiers du bâtiment et des travaux publics sont fixées par décret en Conseil d'Etat. ". Aux termes de l'article L. 8211-1 du même code : " Sont constitutives de travail illégal, dans les conditions prévues par le présent livre, les infractions suivantes : 1° Travail dissimulé (...) ". Il résulte de ces dispositions combinées que le travail dissimulé constitue une infraction de nature à justifier le prononcé de la sanction administrative de fermeture provisoire de l'établissement où cette infraction a été relevée.<br/>
<br/>
              4. Aux termes des deuxième et troisième alinéas de l'article R. 8272-9 du code du travail, pris pour l'application de l'article L. 8272-2 du même code, " Pour les chantiers du bâtiment ou de travaux publics, la fermeture administrative, décidée par le préfet du département dans le ressort duquel a été constatée l'infraction (...), prend la forme d'un arrêt de l'activité de l'entreprise sur le site concerné, après avis du maître d'ouvrage le cas échéant ou, à défaut, du responsable du chantier. (...) / La décision du préfet est portée à la connaissance du public par voie d'affichage sur les lieux du chantier ". Il résulte de ces dispositions que, dans le cas d'une entreprise du bâtiment et des travaux publics dont l'activité s'exerce sur des chantiers, la fermeture administrative ne peut porter que sur les chantiers où l'infraction aux 1° à 4° de l'article L. 8211-1 de ce code a été constatée.<br/>
<br/>
              5. Aux termes de l'article 1er de l'arrêté contesté : " Est prononcée pour une durée d'un mois, à compter de la notification du présent arrêté, la fermeture et l'arrêt des chantiers de l'établissement Goizuetako Estructuras SL (...) ". Il ressort toutefois des motifs de cet arrêté, qui n'analyse que la situation des chantiers de Saint-Jean-de-Luz et d'Hendaye, ainsi que du courrier du préfet des Pyrénées-Atlantiques, en date du 31 mars 2016, notifiant l'arrêté à la société requérante, qui prescrit un affichage sur le seul chantier d'Hendaye, et il a été confirmé à l'audience, que la fermeture et l'arrêt des chantiers de l'entreprise ne concerne que les chantiers dans lesquels l'administration a relevé des faits qu'elle a qualifiés de travail dissimulé et qui étaient encore en activité à la date de cet arrêté. Dès lors, l'arrêté contesté n'a eu pour effet que d'arrêter le seul chantier  de la résidence Lauturu à Hendaye et non les autres chantiers de la société requérante. Il en résulte que les moyens tirés de la méconnaissance de l'article R. 8272-9 du code du travail et de la méconnaissance de la procédure contradictoire en ce qui concerne les chantiers d'Anglet doivent être écartés comme inopérants.<br/>
<br/>
              6. Il résulte des énonciations non contestées de l'arrêté du 31 mars 2016 qu'il a été pris après la transmission au préfet d'un rapport établi le 12 novembre 2015 par la direction régionale des entreprises, de la concurrence, de la consommation, du travail et de l'emploi (DIRECCTE), indiquant que les contrôleurs du travail avaient constaté la présence de huit salariés détachés de la société Goizuetako Estructuras SL sur son chantier de Saint-Jean-de-Luz et de quatre salariés détachés sur celui d'Hendaye. Il ressort de l'instruction, et notamment des indications données à l'audience, que ces salariés composaient à cette date la totalité du personnel ouvrier de la société, qui depuis plusieurs années n'exploitait plus aucun chantier en Espagne. <br/>
<br/>
              7. Aux termes de l'article L. 1262-3 du code du travail dans sa rédaction issue de la loi du 6 août 2015 pour la croissance, l'activité et l'égalité des chances économiques, " Un employeur ne peut se prévaloir des dispositions applicables au détachement de salariés lorsqu'il exerce, dans l'Etat dans lequel il est établi, des activités relevant uniquement de la gestion interne ou administrative, ou lorsque son activité est réalisée sur le territoire national de façon habituelle, stable et continue. (...) / Dans ces situations, l'employeur est assujetti aux dispositions du code du travail applicables aux entreprises établies sur le territoire national ". Il résulte des faits énoncés au point 6 que l'activité de la société Goizuetako Estructuras SL doit être regardée comme réalisée en France de façon habituelle, stable et continue. La société ne peut dès lors se prévaloir des dispositions applicables au détachement de salariés.<br/>
<br/>
              8. Aux termes de l'article L. 8221-5 du code du travail, " Est réputé travail dissimulé par dissimulation d'emploi salarié le fait pour tout employeur : / 1° Soit de se soustraire intentionnellement à l'accomplissement de la formalité prévue à l'article L. 1221-10, relatif à la déclaration préalable à l'embauche ; / 2° Soit de se soustraire intentionnellement à l'accomplissement de la formalité prévue à l'article L. 3243-2, relatif à la délivrance d'un bulletin de paie (...) / 3° Soit de se soustraire intentionnellement aux déclarations relatives aux salaires ou aux cotisations sociales assises sur ceux-ci auprès des organismes de recouvrement des contributions et cotisations sociales ou de l'administration fiscale en vertu des dispositions légales ". Il résulte de ce qui est dit au point 7 que l'emploi en France par la société Goizuetako Estructuras SL de salariés, sans avoir accompli les formalités mentionnées aux 1° et 2° de l'article L. 8221-5 cité ci-dessus, est susceptible de constituer l'infraction de travail dissimulé, sans qu'y fasse obstacle la circonstance, invoquée par la société requérante, que les salariés concernés étaient titulaires de l'imprimé E101 devenu A1 attestant de la régularité de leur inscription au régime de sécurité sociale espagnol.<br/>
<br/>
              9. Aux termes du premier alinéa de l'article R. 8272-8 du code du travail, " Le préfet tient compte, pour déterminer la durée de fermeture d'au plus trois mois de l'établissement relevant de l'entreprise où a été constatée l'infraction conformément à l'article L. 8272-2, de la nature, du nombre, de la durée de la ou des infractions relevées, du nombre de salariés concernés ainsi que de la situation économique, sociale et financière de l'entreprise ou de l'établissement ".  Pour fixer à un mois la durée de la fermeture prononcée par l'arrêté contesté, alors que la DIRECCTE demandait que la durée de cette sanction soit fixée à trois mois, le préfet a pris en compte le nombre de salariés concernés, la gravité et la persistance des infractions relevées, mais aussi " la mise en conformité progressive de la situation d'une partie des salariés au regard du droit du travail français (...) ". Il ne s'est pas fondé sur les conditions de travail des salariés, qui avaient été qualifiées de " déplorables " par la DIRECCTE dans le rapport mentionné au point 6. En fixant ainsi le quantum de la sanction, le préfet des Pyrénées-Atlantiques n'a commis ni une erreur de droit, ni une erreur manifeste d'appréciation qui soient de nature à caractériser une atteinte grave et manifestement illégale aux libertés fondamentales que constituent la liberté d'entreprendre et la liberté du commerce et de l'industrie qui en est une composante.<br/>
<br/>
              10. Il résulte de tout ce qui précède que, sans qu'il soit besoin d'examiner si l'arrêt d'un chantier à Hendaye pour une durée d'un mois et l'exclusion des contrats administratifs pour une durée de deux mois sont de nature à caractériser une situation d'urgence, la société Goizuetako Estructuras SL n'est pas fondée à soutenir que c'est à tort que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Pau a rejeté sa demande. <br/>
<br/>
              11. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance, la somme réclamée par la société Goizuetako Estructuras SL au titre des frais exposés par elle et non compris dans les dépens. <br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de la société Goizuetako Estructuras SL est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à la société Goizuetako Estructuras SL et à la ministre du travail, de l'emploi et du dialogue social.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
