<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036411936</ID>
<ANCIEN_ID>JG_L_2017_12_000000415434</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/41/19/CETATEXT000036411936.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 28/12/2017, 415434, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-12-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>415434</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR>Mme Louise Bréhier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:415434.20171228</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes : <br/>
<br/>
              1°/ Sous le numéro 415434, M. C...A...B..., à l'appui de la demande en référé qu'il a présentée sur le fondement des dispositions de l'article L. 521-2 du code de justice administrative et tendant à la suspension de l'exécution de l'arrêté du ministre de l'intérieur du 31 octobre 2017 prenant à son encontre une mesure de contrôle administratif et de surveillance, a produit un mémoire, enregistré le 2 novembre 2017 au greffe du tribunal administratif de Grenoble, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958, par lequel il soulève une question prioritaire de constitutionnalité. <br/>
<br/>
              Par une ordonnance n° 1706079 du 3 novembre 2017, enregistrée le même jour au secrétariat du contentieux du Conseil d'Etat, le juge des référés du tribunal administratif de Grenoble a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution des articles L. 228-1 et L. 228-2 du code de la sécurité intérieure. <br/>
<br/>
              Par la question prioritaire de constitutionnalité transmise et par un mémoire, enregistré le 29 novembre 2017 au secrétariat du contentieux du Conseil d'Etat, M. A...B...soutient que les articles L. 228-1 et L. 228-2 du code de la sécurité intérieure, applicables au litige, méconnaissent la liberté de conscience et d'opinion, l'objectif à valeur constitutionnelle d'intelligibilité et d'accessibilité de la loi, la liberté d'aller et venir, le droit au recours effectif et sont entachés d'incompétence négative.<br/>
<br/>
<br/>
<br/>
              2°/ Sous le numéro 415697, par quatre mémoires enregistrés le 15 novembre 2017 au secrétariat du contentieux du Conseil d'Etat, la Ligue des droits de l'homme demande au Conseil d'Etat, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 et à l'appui de sa requête tendant à l'annulation pour excès de pouvoir de la circulaire du ministre de l'intérieur du 31 octobre 2017 relative à la mise en oeuvre de la loi n° 2017-1510 du 30 octobre 2017, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution :<br/>
<br/>
              1°) de l'article L. 226-1 du code de la sécurité intérieure et des mots " ou à celle des périmètres de protection institués en application de l'article L. 226-1 " figurant au sixième alinéa de l'article L. 511-1, " y compris dans les périmètres de protection institués en application de l'article L. 226-1 " figurant au premier alinéa de l'article L. 613-1 et " ou lorsqu'un périmètre de protection a été institué en application de l'article L. 226-1 " figurant au deuxième alinéa de l'article L. 613-2 du même code ; <br/>
<br/>
              Elle soutient que ces dispositions, applicables au litige, méconnaissent la liberté d'aller et venir, le droit au respect de la vie privée, le droit au recours effectif et sont entachées d'incompétence négative. <br/>
<br/>
              2°) des articles L. 227-1 et L. 227-2 du code de la sécurité intérieure ;<br/>
<br/>
              Elle soutient que ces dispositions, applicables au litige, méconnaissent la liberté religieuse, la liberté d'expression et de communication, la liberté d'association, le droit d'expression collective des idées et des opinions ainsi que le droit au recours effectif et sont entachées d'incompétence négative. <br/>
<br/>
              3°) des articles L. 228-1 à L. 228-7 du code de la sécurité intérieure ; <br/>
<br/>
              Elle soutient que ces dispositions, applicables au litige, méconnaissent la liberté d'aller et venir, le droit au respect de la vie privée, le droit au recours effectif, le principe de légalité des délits et des peines et sont entachées d'incompétence négative. <br/>
<br/>
              4°) des articles L. 229-1 à L. 229-6 du code de la sécurité intérieure. <br/>
<br/>
              Elle soutient que ces dispositions, applicables au litige, méconnaissent le droit au respect de la vie privée, l'inviolabilité du domicile et le droit au recours effectif et sont entachées d'incompétence négative. <br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ; <br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son article 61-1 ; <br/>
              - l'ordonnance du n° 58-1067 du 7 novembre 1958 ; <br/>
              - le code de la sécurité intérieure, modifié notamment par la loi n° 2017-1510 du 30 octobre 2017 ; <br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Louise Bréhier, auditrice,  <br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de M. A...B..., et à la SCP Spinosi, Sureau, avocat de la Ligue des droits de l'homme ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant que les questions prioritaires de constitutionnalité soulevées par M. A...B...et par la Ligue des droits de l'homme mettent en cause la conformité aux droits et libertés garantis par la Constitution de différentes dispositions du code de la sécurité intérieure résultant de la loi du 30 octobre 2017 renforçant la sécurité intérieure et la lutte contre le terrorisme ; qu'il y a lieu de joindre ces questions pour statuer sur leur renvoi au Conseil constitutionnel par une seule décision ; <br/>
<br/>
              2.	Considérant qu'il résulte des dispositions des articles 23-4 et 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, ou lorsqu'une telle question est soulevée à l'occasion d'une instance devant le Conseil d'Etat en application de l'article 23-5 de cette même ordonnance, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              En ce qui concerne les périmètres de protection régis par l'article L. 226-1 du code de la sécurité intérieure : <br/>
<br/>
              3.	Considérant que l'article L. 226-1 du code de la sécurité intérieure permet au représentant de l'Etat dans le département ou, à Paris, au préfet de police d'instituer, afin d'assurer la sécurité d'un lieu ou d'un événement exposé à un risque d'actes de terrorisme à raison de sa nature et de l'ampleur de sa fréquentation, des périmètres de protection au sein desquels l'accès et la circulation des personnes sont réglementés ; que l'article, qui précise les conditions de mise en oeuvre de ce dispositif, prévoit en particulier que l'accès à ces périmètres peut être subordonné à l'accomplissement de palpations de sécurité, à l'inspection visuelle ou la fouille des bagages ou à la visite des véhicules ; que les dispositions du sixième alinéa de l'article L. 511-1 du même code, du premier alinéa de l'article L. 613-1 ou du deuxième alinéa de l'article L. 613-2 font référence au dispositif institué par l'article L. 226-1 ; <br/>
<br/>
              4.	Considérant que, par la circulaire du 31 octobre 2017 attaquée par la Ligue des droits de l'homme, le ministre de l'intérieur a adressé aux préfets des instructions quant à la mise en oeuvre des périmètres de protection susceptibles d'être ordonnés par application de l'article L. 226-1 du code de la sécurité intérieure ; que les dispositions de cet article, ainsi que les références qui y sont faites au sixième alinéa de l'article L. 511-1 de ce code, au premier alinéa de l'article L. 613-1 et au deuxième alinéa de l'article L. 613-2 sont ainsi applicables au litige au sens et pour l'application de l'article 23-2 de l'ordonnance du 7 novembre 1958 ; que ces dispositions n'ont pas déjà été déclarées conformes à la Constitution par le Conseil constitutionnel ; que le moyen tiré de ce qu'elles portent atteinte aux droits et libertés garantis par la Constitution, et notamment à la liberté d'aller et venir, soulève une question qui présente un caractère sérieux ; qu'ainsi, il y a lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ;<br/>
<br/>
              En ce qui concerne la fermeture des lieux de culte susceptible d'être ordonnée sur le fondement des articles L. 227-1 et L. 227-2 du code de la sécurité intérieure :  <br/>
<br/>
              5.	Considérant que l'article L. 227-1 du code de la sécurité intérieure permet au représentant de l'Etat dans le département ou, à Paris, au préfet de police de prononcer, aux seules fins de prévenir la commission d'actes de terrorisme, la fermeture des lieux de culte dans lesquels les propos qui sont tenus, les idées ou théories qui sont diffusées ou les activités qui se déroulent provoquent à la violence, à la haine ou à la discrimination, provoquent à la commission d'actes de terrorisme ou font l'apologie de tels actes ; que cet article précise les conditions dans lesquelles peut être prononcée une telle fermeture et en prévoit l'exécution d'office, sauf saisine du juge des référés du tribunal administratif ; <br/>
<br/>
              6.	Considérant que, par la circulaire du 31 octobre 2017 attaquée par la Ligue des droits de l'homme, le ministre de l'intérieur a adressé aux préfets des instructions quant à la mise en oeuvre de telles mesures de fermeture de lieux de culte, susceptibles d'être ordonnées par application de l'article L. 227-1 du code de la sécurité intérieure ; que ces dispositions sont ainsi applicables au litige ; qu'elles n'ont pas déjà été déclarées conformes à la Constitution par le Conseil constitutionnel ; que le moyen tiré de ce qu'elles portent atteinte aux droits et libertés garantis par la Constitution, notamment à la liberté de culte, soulève une question sérieuse ; qu'ainsi, il y a lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée en ce qu'elle met en cause la conformité à la Constitution de l'article L. 227-1 du code de la sécurité intérieure ;<br/>
<br/>
              7.	Considérant, en revanche, que l'article L. 227-2 du code de la sécurité intérieure détermine les peines encourues en cas de violation d'une mesure de fermeture d'un lieu de culte prise en application de l'article L. 227-1 du même code ; que la requête formée devant le Conseil d'Etat tend à l'annulation pour excès de pouvoir d'une circulaire adressée aux préfets par le ministre d'Etat, ministre de l'intérieur, qui n'a pas pour objet et ne saurait avoir pour effet de prescrire à ses destinataires de mettre en oeuvre ces dispositions pénales ; que, dès lors, les dispositions de l'article L. 227-2 du code de la sécurité intérieure, qui sont dissociables de l'article L. 227-1 de ce code, ne sont pas applicables au litige dont le Conseil d'Etat est saisi ; que, dès lors, il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée à l'encontre de l'article L. 227-2 du code de la sécurité intérieure ; <br/>
<br/>
              En ce qui concerne les mesures individuelles de contrôle administratif et de surveillance régies par les dispositions des articles L. 228-1 à L. 228-7 du code de la sécurité intérieure : <br/>
<br/>
              8.	Considérant qu'en vertu de l'article L. 228-1 du code de la sécurité intérieure, le ministre de l'intérieur peut, aux seules fins de prévenir la commission d'actes de terrorisme, prescrire les obligations prévues par les articles L. 228-2, L. 228-3, L. 228-4, L. 228-5 de ce code à toute personne à l'égard de laquelle il existe des raisons sérieuses de penser que son comportement constitue une menace d'une particulière gravité pour la sécurité et l'ordre publics et qui soit entre en relation de manière habituelle avec des personnes ou des organisations incitant, facilitant ou participant à des actes de terrorisme, soit soutient, diffuse, lorsque cette diffusion s'accompagne d'une manifestation d'adhésion à l'idéologie exprimée, ou adhère à des thèses incitant à la commission d'actes de terrorisme ou faisant l'apologie de tels actes ; que l'article L. 228-2 permet au ministre de l'intérieur, après en avoir informé le procureur de la République de Paris et le procureur de la République territorialement compétent, de faire obligation à la personne mentionnée à l'article L. 228-1 de ne pas se déplacer à l'extérieur d'un périmètre géographique déterminé, de se présenter périodiquement aux services de police ou aux unités de gendarmerie, dans la limite d'une fois par jour, et de déclarer son lieu d'habitation et tout changement de lieu d'habitation ; que l'article L. 228-3 permet au ministre de l'intérieur, après en avoir informé le procureur de la République de Paris et le procureur de la République territorialement compétent, de placer sous surveillance électronique la personne faisant l'objet d'une mesure prise en application de l'article L. 228-2, alternativement à l'obligation de présentation périodique prévue à cet article et avec l'accord écrit de l'intéressé ; que l'article L. 228-4 permet au ministre de l'intérieur, s'il ne fait pas application des mesures prévues aux articles L. 228-2 et L. 228-3, après en avoir informé le procureur de la République de Paris et le procureur de la République territorialement compétent, de faire obligation à toute personne mentionnée à l'article L. 228-1 de déclarer son domicile et tout changement de domicile, de signaler ses déplacements à l'extérieur d'un périmètre déterminé ne pouvant être plus restreint que le territoire de la commune de son domicile et de ne pas paraître dans un lieu déterminé, qui ne peut inclure le domicile de l'intéressé ; que l'article L. 228-5 permet au ministre de l'intérieur, après en avoir informé le procureur de la République de Paris et le procureur de la République territorialement compétent, de faire obligation à toute personne mentionnée à l'article L. 228-1, y compris lorsqu'il est fait application des articles L. 228-2 à L. 228-4, de ne pas se trouver en relation directe ou indirecte avec certaines personnes, nommément désignées, dont il existe des raisons sérieuses de penser que leur comportement constitue une menace pour la sécurité publique ; que l'article L. 228-6 précise les conditions dans lesquelles doivent être édictées les décisions prises en application des articles L. 228-2 à L. 228-5 ; <br/>
<br/>
              9.	Considérant que M. A...B...conteste l'arrêté du ministre de l'intérieur du 31 octobre 2017 édictant à son encontre une mesure de contrôle administratif et de surveillance, prise sur le fondement des articles L. 228-1 et L. 228-2 du code de la sécurité intérieure ; que ces articles sont ainsi applicables au litige dont était saisi le juge des référés du tribunal administratif de Grenoble ; que, par la circulaire du 31 octobre 2017 attaquée par la Ligue des droits de l'homme, le ministre de l'intérieur a adressé aux préfets des instructions quant à la mise en oeuvre des mesures individuelles de contrôle administratif et de surveillance susceptibles d'être ordonnées par application des articles L. 228-1, L. 228-2, L. 228-3, L. 228-4, L. 228-5 et dans les conditions prévues à l'article L. 228-6 de ce code ; que ces dispositions sont ainsi applicables au litige formé par la Ligue des droits de l'homme ; que ces différentes dispositions n'ont pas été déclarées conformes à la Constitution par le Conseil constitutionnel ; que le moyen tiré de ce qu'elles portent atteinte aux droits et libertés garantis par la Constitution, notamment à la liberté d'aller et venir, soulève une question qui présente un caractère sérieux ; qu'ainsi, il y a lieu de renvoyer au Conseil constitutionnel les questions prioritaires de constitutionnalité invoquées à l'encontre de ces dispositions ; <br/>
<br/>
              10.	Considérant, en revanche, que l'article L. 228-7 du code de sécurité intérieure détermine les peines encourues en cas de violation d'une mesure individuelle de contrôle administratif et de surveillance prise en application des articles L. 228-2 à L. 228-5 de ce code ; que la requête formée par la Ligue des droits de l'homme devant le Conseil d'Etat tend à l'annulation pour excès de pouvoir d'une circulaire adressée aux préfets par le ministre d'Etat, ministre de l'intérieur, qui n'a pas pour objet et ne saurait avoir pour effet de prescrire à ses destinataires de mettre en oeuvre ces dispositions pénales ; que, par suite, les dispositions de l'article L. 228-7 de ce code, qui sont dissociables des articles L. 228-2 à L. 228-5 de ce code, ne sont pas applicables au litige dont est saisi le Conseil d'Etat ; que, dès lors, il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée à l'encontre de l'article L. 228-7 du code de la sécurité intérieure ; <br/>
<br/>
              En ce qui concerne les visites et saisies régies par les dispositions des articles L. 229-1 à L. 229-6 du code de la sécurité intérieure : <br/>
<br/>
              11.	Considérant que l'article L. 229-1 du code de la sécurité intérieure prévoit que le juge des libertés et de la détention du tribunal de grande instance de Paris peut, sur saisine motivée du représentant de l'Etat dans le département ou, à Paris, du préfet de police, et après avis du procureur de la République de Paris, autoriser la visite d'un lieu ainsi que la saisie des documents, objets ou données qui s'y trouvent, aux seules fins de prévenir la commission d'actes de terrorisme et lorsqu'il existe des raisons sérieuses de penser qu'un lieu est fréquenté par une personne dont le comportement constitue une menace d'une particulière gravité pour la sécurité et l'ordre publics et qui soit entre en relation de manière habituelle avec des personnes ou des organisations incitant, facilitant ou participant à des actes de terrorisme, soit soutient, diffuse, lorsque cette diffusion s'accompagne d'une manifestation d'adhésion à l'idéologie exprimée, ou adhère à des thèses incitant à la commission d'actes de terrorisme ou faisant l'apologie de tels actes ; que les articles L. 229-2, L. 229-4 et L. 229-5 déterminent les conditions de mise en oeuvre de ces mesures et, le cas échéant, de la retenue sur place de la personne concernée ; que l'article L. 229-3 du code de la sécurité intérieure détermine les voies de recours ouvertes aux personnes faisant l'objet d'une mesure prise sur le fondement des articles L. 229-1, L. 229-2 et L. 229-5 ; que l'article L. 229-6 prévoit que les juridictions judiciaires sont compétentes pour connaître du contentieux indemnitaire relatif à ces mesures ;<br/>
<br/>
              12.	Considérant que, par la circulaire du 31 octobre 2017 attaquée par la Ligue des droits de l'homme, le ministre de l'intérieur a adressé aux préfets des instructions quant à la mise en oeuvre de visites et de saisies sur le fondement des articles L. 229-1, L. 229-2, L. 229-4 et L. 229-5 du code de la sécurité intérieure ; que ces dispositions sont ainsi applicables au litige dont est saisi le Conseil d'Etat ; que ces dispositions n'ont pas déjà été déclarées conformes à la Constitution par le Conseil constitutionnel ; que le moyen tiré de ce qu'elles portent atteinte aux droits et libertés garantis par la Constitution, notamment au droit au respect de la vie privée, soulève une question sérieuse ; qu'ainsi, il y a lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée à l'encontre des articles L. 229-1, L. 229-2, L. 229-4 et L. 229-5 du code de la sécurité intérieure ;<br/>
<br/>
              13.	Considérant, en revanche, que la circulaire contestée devant le Conseil d'Etat n'évoque pas l'exercice des voies de recours organisées par l'article L. 229-3 du code de la sécurité intérieure, non plus que le contentieux indemnitaire que l'article L. 229-6 attribue aux juridictions judiciaires ; qu'ainsi les dispositions des articles L. 229-3 et L. 229-6 de ce code, qui sont dissociables des articles L. 229-1, L. 229-2, L. 229-4 et L. 229-5 du même code, ne sont pas applicables au litige dont est saisi le Conseil d'Etat ; que, dès lors, il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée à l'encontre des articles L. 229-3 et L. 229-6 du code de la sécurité intérieure ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La question de la conformité à la Constitution de l'article L. 226-1 du code de la sécurité intérieure et des mots " ou à celle des périmètres de protection institués en application de l'article L. 226-1 " figurant au sixième alinéa de l'article L. 511-1 de ce code, " y compris dans les périmètres de protection institués en application de l'article L. 226-1 " figurant au premier alinéa de l'article L. 613-1 de ce code et " ou lorsqu'un périmètre de protection a été institué en application de l'article L. 226-1 " figurant au deuxième alinéa de l'article L. 613-2 du même code est renvoyée au Conseil constitutionnel. <br/>
<br/>
Article 2 : La question de la conformité à la Constitution de l'article L. 227-1 du code de la sécurité intérieure est renvoyée au Conseil constitutionnel.<br/>
<br/>
Article 3 : La question de la conformité à la Constitution des articles L. 228-1, L. 228-2, L. 228-3, L. 228-4, L. 228-5 et L. 228-6 du code de la sécurité intérieure est renvoyée au Conseil constitutionnel.<br/>
<br/>
Article 4 : La question de la conformité à la Constitution des articles L. 229-1, L. 229-2, L. 229-4 et L. 229-5 du code de la sécurité intérieure est renvoyée au Conseil constitutionnel.<br/>
<br/>
Article 5 : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question de la conformité à la Constitution des articles L. 227-2, L. 228-7, L. 229-3 et L. 229-6 du code de la sécurité intérieure.<br/>
Article 6 : Il est sursis à statuer sur la requête de la Ligue des droits de l'homme jusqu'à ce que le Conseil constitutionnel ait tranché les questions de constitutionnalité ainsi soulevées. <br/>
<br/>
Article 7 : La présente décision sera notifiée à M. C...A...B..., à la Ligue des droits de l'homme et au ministre d'Etat, ministre de l'intérieur. Copie en sera adressée au Premier ministre et au tribunal administratif de Grenoble. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
