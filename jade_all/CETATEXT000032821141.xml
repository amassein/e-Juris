<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032821141</ID>
<ANCIEN_ID>JG_L_2016_06_000000393438</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/82/11/CETATEXT000032821141.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 30/06/2016, 393438, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-06-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>393438</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP LESOURD</AVOCATS>
<RAPPORTEUR>M. Luc Briand</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:393438.20160630</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une décision n° 375121 du 21 novembre 2014, le Conseil d'Etat, statuant au contentieux, a, d'une part, annulé l'arrêt n° 12MA04965 du 3 décembre 2013, par lequel la cour administrative d'appel de Marseille a, sur appel de Mme A...B...annulé le jugement n° 1004792 du 25 octobre 2012, du tribunal administratif de Nice rejetant la demande qu'elle avait formée contre la décision du 26 octobre 2010, par laquelle le président de la chambre de commerce et d'industrie Nice-Côte d'Azur avait prononcé à son encontre la sanction disciplinaire de la révocation et, d'autre part, renvoyé à la cour administrative d'appel de Marseille le jugement de la requête présentée par MmeB....<br/>
<br/>
              Par un arrêt n° 14MA04704 du 10 juillet 2015, la cour administrative d'appel de Marseille a annulé le jugement du tribunal administratif du 25 octobre 2012 et la décision du 26 octobre 2010.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 10 septembre et 10 décembre 2015 au secrétariat du contentieux du Conseil d'Etat, la chambre de commerce et d'industrie Nice-Côte d'Azur demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de Mme B...; <br/>
<br/>
              3°) de mettre à la charge de Mme B...la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - l'arrêté du 25 juillet 1997 relatif au statut du personnel de l'Assemblée des chambres françaises de commerce et d'industrie, des chambres régionales de commerce et d'industrie et des groupements interconsulaires ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Luc Briand, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la chambre de commerce et d'industrie Nice-Côte d'Azur, et à la SCP Lesourd, avocat de Mme B...;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 14 juin 2016, présentée par Mme B...;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que MmeB..., agent de la chambre de commerce et d'industrie Nice-Côte d'Azur, a été affectée le 15 mai 2007 au poste de chef de service " parcs et domaines du port de Cannes ", plus particulièrement responsable du parc de stationnement Pantiéro ; que, par décision du 26 octobre 2010, la chambre de commerce et d'industrie l'a révoquée pour des " faits d'une extrême gravité portés à sa connaissance en avril 2010, concernant un comportement pouvant s'analyser comme du harcèlement moral (...) à l'encontre d'agents placés sous ses ordres ainsi que d'autres agents de la société SAAS ", entreprise sous-traitante chargée de la sécurité du parc ; que, par arrêt du 3 décembre 2013, la cour administrative d'appel de Marseille a annulé le jugement du 25 octobre 2012 par lequel le tribunal administratif de Nice a rejeté la demande de Mme B...tendant à l'annulation pour excès de pouvoir de cette décision ; que, par décision n° 375121 du 21 novembre 2014, le Conseil d'Etat statuant au contentieux a annulé cet arrêt au motif qu'en jugeant que les agissements reprochés à Mme B...ne présentaient pas le caractère d'actes de harcèlement moral, la cour administrative d'appel avait inexactement qualifié les faits de l'espèce ; que, statuant sur renvoi, par un arrêt du 10 juillet 2015 objet du présent pourvoi, la cour administrative d'appel de Marseille a annulé le jugement du tribunal administratif de Nice et la décision du 26 octobre 2010, jugeant que la sanction de la révocation prononcée à l'encontre de Mme B...était disproportionnée au regard de la gravité des fautes qui lui étaient reprochées ;<br/>
<br/>
              2.	Considérant que pour juger que Mme B...était fondée à demander l'annulation de la décision prononçant sa révocation, la cour administrative d'appel de Marseille a estimé que le choix de cette sanction avait principalement été motivé par des violences physiques commises sur une ancienne stagiaire dont la réalité n'était pourtant pas établie et que les autres agissements s'étaient déroulés sur une période limitée à un trimestre, et que, pour ces motifs, le choix de la sanction la plus sévère, bien que l'employeur ait pu disposer d'un éventail de sanctions de natures et de portées différentes, était disproportionné avec les faits commis ; qu'en statuant ainsi, alors qu'il ressort des pièces du dossier soumis aux juges du fond que des agissements de MmeB..., constitutifs de harcèlement moral, ont été dénoncés dès le mois de janvier 2009 par les responsables d'une société sous traitante ainsi que dans le courant du mois d'avril 2010 par une ancienne stagiaire et se sont déroulés sur une longue période, la cour administrative d'appel a dénaturé les pièces du dossier sur lesquels elle a fondé son arrêt ;<br/>
<br/>
              3.	Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, la chambre de commerce et d'industrie Nice-Côte d'Azur est fondée à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              4.	Considérant qu'aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire " ; qu'il incombe, par suite, de régler l'affaire au fond ;<br/>
<br/>
              5.	Considérant qu'aux termes de l'article 33 bis de l'annexe à l'arrêté du 25 juillet 1997 relatif au statut du personnel de l'assemblée des chambres françaises de commerce et d'industrie, des chambres régionales de commerce et d'industrie et des groupements interconsulaires : " (...) Le licenciement ou la révocation de tout agent ayant la qualité de délégué syndical ou de représentant du personnel en Commission Paritaire Locale ou en Commission Paritaire Nationale ne peut intervenir, après avis de la Commission Paritaire Locale dans les conditions prévues à l'article 33 paragraphes 4, 5 et 6, que sur avis conforme des Ministres de Tutelle. Si la demande de licenciement n'a pas reçu de réponse dans un délai d'un mois à compter de sa date de réception par lesdits Ministres, l'avis conforme est réputé avoir été donné. " ; qu'aucune disposition et aucun principe ne faisait obligation au ministre de tutelle de recueillir les observations de l'intéressée, qui exerçait un mandat de représentante du personnel suppléante, avant de rendre son avis sur le fondement de ces dispositions ni de lui communiquer le contenu de celui-ci ; que par suite le moyen tiré de l'irrégularité de l'avis émis le 13 octobre 2010 par le ministre de l'économie doit être écarté ; que par ailleurs, le moyen tiré de ce que la chambre de commerce et d'industrie, qui avait diligenté une enquête sur les agissements de MmeB..., n'aurait pas suivi la procédure d'enquête spécifique applicable aux cas de harcèlement moral et susceptible d'aboutir à la réunion du comité d'hygiène et de sécurité à la seule demande de la victime des faits, est inopérant car sans incidence sur la régularité de la procédure disciplinaire ;<br/>
<br/>
              6.	Considérant qu'il appartient au juge de l'excès de pouvoir, saisi de moyens en ce sens, de rechercher si les faits reprochés à un agent public ayant fait l'objet d'une sanction disciplinaire constituent des fautes de nature à justifier une sanction et si la sanction retenue est proportionnée à la gravité de ces fautes ; <br/>
<br/>
              7.	Considérant que, par sa décision n° 375121 du 21 novembre 2014, le Conseil d'Etat statuant au contentieux a qualifié de harcèlement moral le comportement de Mme B... à l'égard de certains des agents placés sous son autorité ; qu'un tel comportement est constitutif d'une faute de nature à justifier une sanction disciplinaire ; qu'ainsi qu'il a été dit ci-dessus, il résulte des pièces du dossier que ces agissements ont été commis à l'encontre de plusieurs personnes, pendant une longue période de temps ; qu'ils revêtaient une particulière gravité ; qu'ainsi, eu égard à la marge d'appréciation dont disposait l'administration, celle-ci n'a pas prononcé une sanction disproportionnée en faisant le choix de la révocation de Mme B...;<br/>
<br/>
              8.	Considérant, enfin, que le détournement de pouvoir allégué n'est pas établi ; <br/>
<br/>
              9.	Considérant qu'il résulte de ce qui précède que Mme B...n'est pas fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Nice a rejeté sa demande ;<br/>
<br/>
              10.	Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme B...le versement à la chambre de commerce et d'industrie Nice-Côte d'Azur de la somme de 3 000 euros en application de l'article L. 761-1 du code de justice administrative ; qu'en revanche, ces dispositions font obstacle à ce qu'il soit fait droit aux conclusions présentées à ce titre par Mme B...;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 10 juillet 2015 est annulé.<br/>
<br/>
Article 2 : La requête présentée par Mme B...devant la cour administrative d'appel de Marseille et ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : Mme B...versera à la chambre de commerce et d'industrie Nice-Côte d'Azur la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 4 : La présente décision sera notifiée à la chambre de commerce et d'industrie Nice-Côte d'Azur et à Mme A...B.... <br/>
Copie en sera adressée au ministre de l'économie, de l'industrie et du numérique.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
