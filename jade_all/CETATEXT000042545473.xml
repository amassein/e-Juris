<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042545473</ID>
<ANCIEN_ID>JG_L_2020_11_000000432678</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/54/54/CETATEXT000042545473.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 20/11/2020, 432678</TITRE>
<DATE_DEC>2020-11-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>432678</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP BARADUC, DUHAMEL, RAMEIX ; CABINET COLIN-STOCLET</AVOCATS>
<RAPPORTEUR>Mme Cécile Renault</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:432678.20201120</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              La société Veolia Eau - Compagnie générale des eaux a demandé au tribunal administratif de Grenoble de condamner solidairement les sociétés Artelia Ville et Transport et Bauland Travaux publics à lui payer une indemnité de 428 428,57 euros. Par un jugement n° 1400269 du 30 décembre 2016, le tribunal administratif de Grenoble a condamné solidairement les sociétés Artelia Ville et Transport et Bauland Travaux publics à payer à la société Veolia Eau - Compagnie générale des eaux une indemnité totale de 111 576,82 euros et a condamné la société Bauland Travaux publics à garantir la société Artelia Ville et Transport à hauteur de 25 % des condamnations prononcées à son encontre par le jugement et a condamné la société Artelia Ville et Transport à garantir la société Bauland Travaux publics à hauteur de 75 % des condamnations prononcées à son encontre par le jugement.<br/>
<br/>
              Par un arrêt n° 17LY01270 du 16 mai 2019, la cour administrative d'appel de Lyon a rejeté l'appel formé par la société Veolia Eau - Compagnie générale des eaux contre ce jugement et, sur appel incident des sociétés Artelia Ville et Transport et Bauland Travaux publics, annulé ce jugement et rejeté la demande de la société Veolia Eau - Compagnie générale des eaux.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et deux nouveaux mémoires, enregistrés les 16 juillet et 16 octobre 2019 et les 15 avril et 20 mai 2020 au secrétariat du contentieux du Conseil d'Etat, la société Veolia Eau - Compagnie générale des eaux demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions présentées en appel ;<br/>
<br/>
              3°) de mettre à la charge solidaire des sociétés Artelia Ville et Transport et Bauland Travaux publics la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code civil ;<br/>
              - la loi n° 2008-561 du 17 juin 2008 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alexis Goin, auditeur,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de la société Veolia Eau - Compagnie générale des eaux, à la SCP Baraduc, Duhamel, Rameix, avocat de la société Artelia Ville et Transport et au Cabinet Colin-Stoclet, avocat de Me A..., liquidateur de la société Bauland Travaux publics ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que le 31 août 1999, la commune de Bourg de Péage a conclu avec la société Sogreah, devenue la société Artelia Ville et Transport, puis la société Artelia, un marché public de maîtrise d'oeuvre pour la réalisation d'un collecteur d'eaux usées comportant notamment la traversée sous fluviale de l'Isère. Par un marché public conclu en février 2000, la commune de Bourg-de-Péage a confié les travaux de construction de ce collecteur à un groupement d'entreprises comprenant notamment la société Bauland Travaux publics, chargée de la réalisation de la traversée sous-fluviale. La réception de ces travaux est intervenue sans réserve le 15 octobre 2002. La commune de Bourg de Péage a, par contrat conclu en décembre 2003, affermé à la société Veolia Eau - Compagnie générale des eaux la gestion et l'exploitation du service public communal de collecte, de transport et de traitement des eaux usées et pluviales. A la suite de la rupture de la canalisation sous-fluviale survenue le 14 septembre 2008, le tribunal administratif de Grenoble a, par jugement du 24 juillet 2014 devenu définitif et rendu sur demande de la commune de Bourg de Péage, condamné solidairement la société Artelia Ville et Transport et la société Bauland Travaux publics à verser à cette commune, au titre de leur responsabilité décennale, une indemnité de 186 772,80 euros en réparation des conséquences dommageables de cette rupture pour la commune. La société Veolia Eau - Compagnie générale des eaux a elle aussi recherché la responsabilité des sociétés Artelia Ville et Transport et Bauland Travaux publics pour obtenir réparation des conséquences dommageables pour elle de la rupture du collecteur. Par un arrêt du 16 mai 2019 contre lequel la société Veolia Eau se pourvoit en cassation, la cour administrative d'appel de Lyon a annulé le jugement du 30 décembre 2016 du tribunal administratif de Grenoble condamnant ces sociétés à lui verser une indemnité totale de 111 576,82 euros et rejeté la demande de la société Veolia Eau, au motif que l'action de cette dernière, engagée le 10 janvier 2014, était prescrite en application de l'article 2224 du code civil. <br/>
<br/>
              2. Aux termes de l'article 2224 du code civil dans sa rédaction résultant de la loi du 17 juin 2008 : " Les actions personnelles ou mobilières se prescrivent par cinq ans à compter du jour où le titulaire d'un droit a connu ou aurait dû connaître les faits lui permettant de l'exercer ". <br/>
<br/>
              3. Aux termes de l'article 2241 du même code : " La demande en justice, même en référé, interrompt le délai de prescription (...) ", l'article 2242 du même code, dans sa rédaction issue de la loi du 17 juin 2008, prévoyant que " l'interruption résultant de la demande en justice produit ses effets jusqu'à l'extinction de l'instance ". En outre, aux termes de l'article 2239 du même code, dans sa rédaction issue de la même loi : " La prescription est également suspendue lorsque le juge fait droit à une demande de mesure d'instruction présentée avant tout procès. / Le délai de prescription recommence à courir, pour une durée qui ne peut être inférieure à six mois, à compter du jour où la mesure a été exécutée ". Il résulte de ce qui précède que la demande adressée à un juge de diligenter une expertise interrompt le délai de prescription jusqu'à l'extinction de l'instance et que, lorsque le juge fait droit à cette demande, le même délai est suspendu jusqu'à la remise par l'expert de son rapport au juge.<br/>
<br/>
              4. Aux termes de l'article 2244 du code civil, dans sa version antérieure à la loi du 17 juin 2008 : " Une citation en justice, même en référé, un commandement ou une saisie, signifiés à celui qu'on veut empêcher de prescrire, interrompent la prescription ainsi que les délais pour agir ". Alors même que l'article 2244 du code civil dans sa rédaction antérieure à la loi du 17 juin 2008 réservait ainsi un effet interruptif aux actes " signifiés à celui qu'on veut empêcher de prescrire ", termes qui n'ont pas été repris par le législateur aux nouveaux articles 2239 et 2241 de ce code, il ne résulte ni des dispositions de la loi du 17 juin 2008 ni de ses travaux préparatoires que la réforme des règles de prescription résultant de cette loi aurait eu pour effet d'étendre le bénéfice de la suspension ou de l'interruption du délai de prescription à d'autres personnes que le demandeur à l'action, et notamment à l'ensemble des participants à l'opération d'expertise. La suspension de la prescription, en application de l'article 2239 du code civil, lorsque le juge accueille une demande de mesure d'instruction présentée avant tout procès, le cas échéant faisant suite à l'interruption de cette prescription au profit de la partie ayant sollicité cette mesure en référé, tend à préserver les droits de cette partie durant le délai d'exécution de cette mesure et ne joue qu'à son profit, et non, lorsque la mesure consiste en une expertise, au profit de l'ensemble des parties à l'opération d'expertise, sauf pour ces parties à avoir expressément demandé à être associées à la demande d'expertise et pour un objet identique.<br/>
<br/>
<br/>
              5. Il résulte de ce qui précède qu'en jugeant que la saisine du juge des référés du tribunal administratif de Grenoble par la commune de Bourg-de-Péage, postérieurement au 14 septembre 2008, date à laquelle la société Veolia Eau - Compagnie générale des eaux a eu connaissance de la rupture du collecteur, aux fins de voir ordonner une expertise relative à la rupture du collecteur, demande à laquelle il a été fait droit par ordonnance du 6 février 2009, n'a pu ni interrompre ni suspendre la prescription à l'égard de la société Veolia Eau - Compagnie générale des eaux, dès lors que cette saisine n'émanait pas de cette société elle-même, et alors que cette société n'a pas demandé expressément à être associée à cette demande d'expertise, la cour administrative d'appel de Lyon n'a pas commis d'erreur de droit.<br/>
<br/>
              6. Par suite, la société Veolia Eau - Compagnie générale des eaux n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Veolia Eau - Compagnie générale des eaux le versement de la somme de 1 500 euros respectivement à la société Artelia et à Me A..., liquidateur de la société Bauland Travaux publics, sur le fondement de l'article L. 761-1 du code de justice administrative. Les mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société Artelia et de Me A... qui ne sont pas, dans la présente instance, les parties perdantes.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société Veolia Eau - Compagnie générale des eaux est rejeté.<br/>
Article 2 : La société Veolia Eau - Compagnie générale des eaux versera respectivement à la société Artelia et à Me A..., liquidateur de la société Bauland Travaux publics, une somme de 1 500 euros, au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : La présente décision sera notifiée à la société Veolia Eau - Compagnie générale des eaux, à la société Artelia et à Me A..., liquidateur de la société Bauland Travaux publics.<br/>
Copie en sera adressée à la commune de Bourg-de-Péage.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-06-01-04-02-02 MARCHÉS ET CONTRATS ADMINISTRATIFS. RAPPORTS ENTRE L'ARCHITECTE, L'ENTREPRENEUR ET LE MAÎTRE DE L'OUVRAGE. RESPONSABILITÉ DES CONSTRUCTEURS À L'ÉGARD DU MAÎTRE DE L'OUVRAGE. RESPONSABILITÉ DÉCENNALE. DÉLAI DE MISE EN JEU. INTERRUPTION DU DÉLAI. - DEMANDE D'EXPERTISE JUDICIAIRE - INTERRUPTION PUIS SUSPENSION DU DÉLAI - EXISTENCE, AU BÉNÉFICE DES SEULS DEMANDEURS [RJ1].
</SCT>
<ANA ID="9A"> 39-06-01-04-02-02 Il résulte des articles 2224, 2239, 2241 et 2242 du code civil que la demande adressée à un juge de diligenter une expertise interrompt le délai de prescription jusqu'à l'extinction de l'instance et que, lorsque le juge fait droit à cette demande, le même délai est suspendu jusqu'à la remise par l'expert de son rapport au juge.,,,Alors même que l'article 2244 du code civil dans sa rédaction antérieure à la loi n° 2008-561  du 17 juin 2008 réservait un effet interruptif aux actes  signifiés à celui qu'on veut empêcher de prescrire , termes qui n'ont pas été repris par le législateur aux nouveaux articles 2239 et 2241 de ce code, il ne résulte ni des dispositions de la loi du 17 juin 2008 ni de ses travaux préparatoires que la réforme des règles de prescription résultant de cette loi aurait eu pour effet d'étendre le bénéfice de la suspension ou de l'interruption du délai de prescription à d'autres personnes que le demandeur à l'action, et notamment à l'ensemble des participants à l'opération d'expertise.... ,,La suspension de la prescription, en application de l'article 2239 du code civil, lorsque le juge accueille une demande de mesure d'instruction présentée avant tout procès, le cas échéant faisant suite à l'interruption de cette prescription au profit de la partie ayant sollicité cette mesure en référé, tend à préserver les droits de cette partie durant le délai d'exécution de cette mesure et ne joue qu'à son profit, et non, lorsque la mesure consiste en une expertise, au profit de l'ensemble des parties à l'opération d'expertise, sauf pour ces parties à avoir expressément demandé à être associées à la demande d'expertise et pour un objet identique.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>1. Cf., avant l'entrée en vigueur de la loi du 17 juin 2008, CE, 7 octobre 2009, Société atelier des maîtres d'oeuvre Atmo et compagnie les souscripteurs du Lloyd's de Londres, n° 308163, T. p. 837 ; CE, 12 mars 2014, Société Ace Insurance, n° 364429, T. p. 744 ; CE, 19 avril 2017, Communauté urbaine de Dunkerque, 395328, T. p. 680. Rappr. Cass. civ 2e, 31 janvier 2019, Société Navaron, n° 18-10.011, à publier au Bulletin ; Cass. civ 3e, 19 mars 2020, Société de travaux publics et de construction du littoral, n° 19-13.459, à publier au Bulletin.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
