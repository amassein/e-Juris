<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043834077</ID>
<ANCIEN_ID>JG_L_2021_07_000000451308</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/83/40/CETATEXT000043834077.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 20/07/2021, 451308, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>451308</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI</AVOCATS>
<RAPPORTEUR>Mme Cécile Nissen</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Céline  Guibé</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:451308.20210720</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un mémoire distinct et deux autres mémoires, enregistrés les 8 mai,15 juin et 1er juillet 2021 au secrétariat du contentieux du Conseil d'Etat, la société Crédit mutuel Arkéa demande au Conseil d'Etat, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 et à l'appui de sa requête tendant à l'annulation de la décision de caractère général n° 1-2021 relative au renforcement de la cohésion du Groupe Crédit mutuel adoptée par le conseil d'administration du 2 février 2021 de la Confédération nationale du Crédit mutuel (CNCM), de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des deux premières phrases du deuxième alinéa de l'article L. 511-31 du code monétaire et financier et du dernier alinéa de l'article L. 512-56 du même code. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ; <br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code monétaire et financier ; <br/>
              - l'ordonnance n°2013-544 du 27 juin 2013 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 7 juillet 2021, présentée par la société Crédit mutuel Arkéa ; <br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Nissen, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Céline Guibé, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Spinosi, avocat de la Confédération nationale du crédit mutuel ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux. <br/>
<br/>
              2. Aux termes du II de l'article L. 511-20 du code monétaire et financier : " (...) Les établissements et sociétés de financement affiliés à un réseau et l'organe central au sens de l'article L. 511-31 sont considérés comme faisant partie d'un même groupe pour l'application du présent code ", y compris ses dispositions relatives aux procédures d'agrément, à la mise en oeuvre des mesures de prévention et de gestion des crises bancaires ou aux obligations relatives à la lutte contre le blanchiment des capitaux et le financement du terrorisme. Aux termes de l'article L. 511-30 du code monétaire et financier : " Pour l'application des dispositions du présent code relatives aux établissements de crédit et aux sociétés de financement, sont considérées comme organes centraux : / (...) la Confédération nationale du crédit mutuel ". Aux termes de l'article L. 511-31 du même code, dans sa rédaction issue de l'article 4 de l'ordonnance du 27 juin 2013 relative aux établissements de crédit et aux sociétés de financement : " Les organes centraux représentent les établissements de crédit et les sociétés de financement qui leur sont affiliés auprès de la Banque de France et de l'Autorité de contrôle prudentiel et de résolution. / Ils sont chargés de veiller à la cohésion de leur réseau et de s'assurer du bon fonctionnement des établissements et sociétés qui leur sont affiliés. A cette fin, ils prennent toutes mesures nécessaires, notamment pour garantir la liquidité et la solvabilité de chacun de ces établissements et sociétés comme de l'ensemble du réseau. Ils peuvent également décider d'interdire ou de limiter la distribution d'un dividende aux actionnaires ou d'une rémunération des parts sociales aux sociétaires des établissements de crédit, des sociétés de financement ou des entreprises d'investissement qui leur sont affiliés. / (...) Ils veillent à l'application des dispositions législatives et réglementaires propres à ces établissements et sociétés et exercent un contrôle administratif, technique et financier sur leur organisation et leur gestion. Les contrôles sur place des organes centraux peuvent être étendus à leurs filiales directes ou indirectes, ainsi qu'à celles des établissements et sociétés qui leur sont affiliés. / Dans le cadre de ces compétences, ils peuvent prendre les sanctions prévues par les textes législatifs et réglementaires qui leur sont propres. / La perte de la qualité d'établissement ou de société affilié doit être notifiée par l'organe central à l'Autorité de contrôle prudentiel et de résolution, qui se prononce sur l'agrément de l'établissement ou de la société en cause. / (...) / Après en avoir informé l'Autorité de contrôle prudentiel et de résolution, les organes centraux peuvent, lorsque la situation financière des établissements et des sociétés concernés le justifie, et nonobstant toutes dispositions ou stipulations contraires, décider la fusion de deux ou plusieurs personnes morales qui leur sont affiliées, la cession totale ou partielle de leur fonds de commerce ainsi que leur dissolution. Les organes dirigeants des personnes morales concernées doivent au préalable avoir été consultés par les organes centraux. Ces derniers sont chargés de la liquidation des établissements de crédit et des sociétés de financement qui leur sont affiliés ou de la cession totale ou partielle de leur fonds de commerce. / Les organes centraux notifient toute décision d'affiliation ou de retrait d'affiliation à l'établissement ou la société concerné et à l'Autorité de contrôle prudentiel et de résolution. ". L'article L. 511-32 du même code dispose que : " Sans préjudice des pouvoirs de contrôle sur pièces et sur place conférés à l'Autorité de contrôle prudentiel et de résolution sur les établissements et sociétés qui leur sont affiliés, les organes centraux concourent, chacun pour ce qui le concerne, à l'application des dispositions européennes directement applicables, législatives et réglementaires régissant les établissements de crédit et les sociétés de financement. / A ce titre, ils saisissent l'Autorité de contrôle prudentiel et de résolution des infractions à ces dispositions ". Enfin, selon l'article L. 512-56 du même code : " Chaque caisse de crédit mutuel doit adhérer à une fédération régionale et chaque fédération régionale doit adhérer à la confédération nationale du crédit mutuel dont les statuts sont approuvés par le ministre chargé de l'économie. / La confédération nationale du crédit mutuel est chargée : / 1.  De représenter collectivement les caisses de crédit mutuel pour faire valoir leurs droits et intérêts communs ; / 2. D'exercer un contrôle administratif, technique et financier sur l'organisation et la gestion de chaque caisse de crédit mutuel ; / 3. De prendre toutes mesures nécessaires au bon fonctionnement du crédit mutuel, notamment en favorisant la création de nouvelles caisses ou en provoquant la suppression de caisses existantes, soit par voie de fusion avec une ou plusieurs caisses, soit par voie de liquidation amiable ". <br/>
<br/>
              3. A l'appui de sa question prioritaire de constitutionnalité, la société requérante soutient que les deux premières phrases du deuxième alinéa de l'article L. 511-31 et le dernier alinéa de l'article L. 512-56 précités sont entachés d'une incompétence négative affectant la liberté d'entreprendre, le droit de propriété et la liberté contractuelle des affiliés du réseau Crédit mutuel. Il est ainsi soutenu que par l'imprécision des termes " cohésion ", " bon fonctionnement " et " toutes mesures nécessaires " employés à ces articles, le législateur a laissé aux organes centraux en général et à la CNCM en particulier, une latitude excessive dans l'appréciation des mesures à adopter et des motifs susceptibles de les justifier, dans des conditions portant atteinte aux droits et libertés invoqués. <br/>
<br/>
              4. Toutefois, il ressort de l'ensemble des dispositions citées au point 2 que le législateur, pour garantir la stabilité du système financier et la protection des déposants, sociétaires et investisseurs, a notamment permis la surveillance prudentielle des établissements de crédit appartenant à des groupes bancaires mutualistes et coopératifs sur une base consolidée, et a confié ainsi aux organes centraux, dont la CNCM, les pouvoirs nécessaires pour garantir à tout instant la liquidité et la solvabilité de leur groupe. Le législateur pouvait, sans méconnaître l'étendue de sa compétence dans des conditions affectant la liberté d'entreprendre, la liberté contractuelle ou le droit de propriété des entités affiliées, et à cette seule fin de garantir la stabilité du système financier et la protection des déposants, sociétaires et investisseurs, doter la CNCM des pouvoirs d'organisation et de gestion sur les caisses qu'elle représente et sur les fédérations régionales auxquelles elles sont tenues d'adhérer.<br/>
<br/>
              5. Il résulte de ce qui précède qu'il n'y a pas lieu de transmettre au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée, qui n'est pas nouvelle et ne présente pas un caractère sérieux. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la société Crédit mutuel Arkéa.<br/>
Article 2 : La présente décision sera notifiée à la société Crédit mutuel Arkéa, à la Confédération nationale du crédit mutuel, et au ministre de l'économie, des finances et de la relance.<br/>
Copie en sera adressée au Conseil constitutionnel et au Premier ministre. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
