<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043296871</ID>
<ANCIEN_ID>JG_L_2021_03_000000445505</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/29/68/CETATEXT000043296871.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 26/03/2021, 445505, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-03-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>445505</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MARLANGE, DE LA BURGADE ; CABINET MUNIER-APAIRE</AVOCATS>
<RAPPORTEUR>M. Frédéric Pacoud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:445505.20210326</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. J... N... a demandé au tribunal administratif de Montpellier d'annuler les opérations électorales qui se sont déroulées le 15 mars 2020 dans la commune de Montgaillard en vue de l'élection des conseillers municipaux. Par un jugement n° 2001482 du 22 septembre 2020, le tribunal administratif de Montpellier a rejeté cette protestation. <br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 20 octobre 2020 et 5 janvier 2021 au secrétariat du contentieux du Conseil d'Etat, M. N... demande au Conseil d'Etat :  <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) d'annuler ces opérations électorales ;<br/>
<br/>
              3°) de mettre à la charge de M. P..., Mme G..., M. I..., Mme D..., M. A..., Mme O... et Mme K...-Q... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Pacoud, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Marlange, de la Burgade, avocat de M. N... et au cabinet MUNIER-APAIRE, avocat de Mme K..., de Mme O..., de M. I..., de M. A..., de D..., de Mme G..., et de M. P... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. A l'issue des opérations électorales qui se sont déroulées le 15 mars 2020 à Montgaillard, les sept sièges de conseillers municipaux ont été pourvus, le dernier élu recueillant 26 voix sur les 37 suffrages exprimés. M. N... relève appel du jugement du 22 septembre 2020 par lequel le tribunal administratif de Montpellier a rejeté sa protestation.<br/>
<br/>
              2. En premier lieu, l'article R. 42 du code électoral prévoit que : " Chaque bureau de vote est composé d'un président, d'au moins deux assesseurs et d'un secrétaire choisi par eux parmi les électeurs de la commune. / (...) / Deux membres du bureau au moins doivent être présents pendant tout le cours des opérations électorales. (...) ". La circonstance que, pendant deux périodes de deux heures, le bureau de vote n'aurait pas été tenu par deux de ses membres, en méconnaissance de ces dispositions, n'est pas susceptible, en l'absence de toute manoeuvre, d'entraîner l'annulation de l'élection.<br/>
<br/>
              3. En deuxième lieu, les trois premiers alinéas de l'article L. 65 du code électoral disposent que : " Dès la clôture du scrutin, il est procédé au dénombrement des émargements. Ensuite, le dépouillement se déroule de la manière suivante : (...) Le bureau désigne parmi les électeurs présents un certain nombre de scrutateurs sachant lire et écrire, lesquels se divisent par tables de quatre au moins. (...). / Les enveloppes contenant les bulletins sont regroupées par paquet de 100. Ces paquets sont introduits dans des enveloppes spécialement réservées à cet effet. Dès l'introduction d'un paquet de cent bulletins, l'enveloppe est cachetée et y sont apposées les signatures du président du bureau de vote et d'au moins deux assesseurs représentant, sauf liste ou candidat unique, des listes ou des candidats différents./ A chaque table, l'un des scrutateurs extrait le bulletin de chaque enveloppe et le passe déplié à un autre scrutateur ; celui-ci le lit à haute voix ; les noms portés sur les bulletins sont relevés par deux scrutateurs au moins sur des listes préparées à cet effet. (...) ". Aux termes de l'article R. 64 du même code : " " Le dépouillement est opéré par des scrutateurs sous la surveillance des membres du bureau. / A défaut de scrutateurs en nombre suffisant, le bureau de vote peut y participer " et aux termes de l'article R. 65-1 de ce code : " Si à la fin du regroupement des enveloppes électorales par paquets de cent prévu au deuxième alinéa de l'article L. 65, le bureau constate qu'il reste des enveloppes électorales en nombre inférieur à cent, il introduit ces enveloppes dans une enveloppe de centaine qui doit porter, outre les signatures énumérées audit alinéa, la mention du nombre des enveloppes électorales qu'elle contient. / Le président répartit entre les diverses tables de dépouillement les enveloppes de centaine. / Après avoir vérifié que les enveloppes de centaine sont conformes aux dispositions du deuxième alinéa de l'article L. 65, les scrutateurs les ouvrent, en extraient les enveloppes électorales et procèdent comme il est dit au troisième alinéa dudit article ".<br/>
<br/>
              4. La seule circonstance qu'au cours des opérations de dépouillement, certaines des prescriptions fixées par ces dispositions n'ont pas été respectées n'est pas de nature à justifier l'annulation des opérations électorales dès lors que les irrégularités commises n'ont pas été constitutives de manoeuvres ayant pu conduire à fausser les résultats du scrutin. Il résulte en particulier de l'instruction que les corrections apportées sur la feuille de pointage ne portent que sur la comptabilisation du nombre total des voix apportées à chaque candidat, non sur le pointage des voix, et que le dépouillement a été opéré publiquement, sans que les personnes présentes aient été empêchées de vérifier que le pointage opéré correspondait aux bulletins lus. En outre, l'obligation de regrouper les enveloppes électorales par paquets de cent est sans objet lorsque, comme en l'espèce, le nombre total de bulletins à dépouiller est inférieur à ce seuil.<br/>
              5. En troisième lieu, l'article R. 67 du code électoral dispose que : " Immédiatement après la fin du dépouillement, le procès-verbal des opérations électorales est rédigé par le secrétaire dans la salle de vote, en présence des électeurs (...) ". L'article R. 67 de ce code prévoit que : " Dès l'établissement du procès-verbal, le résultat est proclamé en public par le président du bureau et affiché en toutes lettres par ses soins dans la salle de vote " et le second alinéa de l'article R. 68 que : " Les bulletins autres que ceux qui, en application de la législation en vigueur, doivent être annexés au procès-verbal sont détruits en présence des électeurs. " Enfin, aux termes de l'article R. 119 du même code : " Les réclamations contre les opérations électorales doivent être consignées au procès-verbal (...) ". Ni la circonstance que le procès-verbal aurait été rédigé par la secrétaire de mairie et non par le secrétaire du bureau, ni celle que les ratures figurant sur la feuille de pointage n'y auraient pas été mentionnées, ni celles que les bulletins n'auraient pas été détruits en présence des électeurs mais dans la pièce attenante ou que les résultats auraient été proclamés avant l'établissement du procès-verbal, ne sont de nature, dès lors qu'il n'est pas soutenu qu'elles auraient été constitutives de manoeuvres, à entraîner l'annulation du scrutin, sans que le tribunal ait, dans ces conditions, entaché son jugement d'irrégularité en ne se prononçant pas dans le détail sur chacune.<br/>
<br/>
              6. Les irrégularités invoquées par le requérant n'étant ainsi, même prises dans leur ensemble, pas de nature à conduire à l'annulation du scrutin, M. N... n'est pas fondé à soutenir que c'est à tort que le tribunal administratif a rejeté sa protestation. <br/>
<br/>
              7. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'il soit fait droit aux conclusions présentées à ce titre par le requérant. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions des défendeurs tendant à ce qu'une somme soit mise à sa charge sur le fondement des mêmes dispositions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : La requête de M. N... est rejetée. <br/>
Article 2 : Les conclusions de Mme K..., Mme O..., M. I..., M. A..., Mme D..., Mme G... et M. P... présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. J... N..., Mme L... K..., Mme H... O..., M. B... I..., M. C... A..., Mme M... D..., Mme F... G... et M. E... P..., ainsi qu'au ministre de l'intérieur<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
