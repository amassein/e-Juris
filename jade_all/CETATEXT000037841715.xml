<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037841715</ID>
<ANCIEN_ID>JG_L_2018_12_000000418637</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/84/17/CETATEXT000037841715.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 20/12/2018, 418637</TITRE>
<DATE_DEC>2018-12-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418637</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>M. Florian Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2018:418637.20181220</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La commune de Chessy a demandé au tribunal administratif de Melun, par une première requête, d'annuler l'arrêté du 24 mars 2014 du préfet de Seine-et-Marne fixant, au titre de l'inventaire de l'année 2013, le montant du prélèvement prévu à l'article L. 302-7 du code de la construction et de l'habitation, ainsi que la décision rejetant son recours gracieux présenté le 17 avril 2014, et de la décharger de l'obligation de verser la somme de 59 296,77 euros. Par une seconde requête, la commune a demandé au tribunal administratif d'annuler l'arrêté du préfet de Seine-et-Marne du 20 février 2015 fixant le montant du même prélèvement au titre de l'inventaire de l'année 2014, ainsi que la décision rejetant son recours gracieux présenté le 24 avril 2015, et de la décharger de l'obligation de verser la somme de 71 363,58 euros. Par un jugement n° 1407231,1506200 du 13 mai 2006, le tribunal administratif a rejeté ces demandes. <br/>
<br/>
              Par une ordonnance n° 16PA02556 du 19 avril 2017, le président de la 3ème chambre de la cour administrative d'appel de Paris a refusé de transmettre au Conseil d'Etat une question prioritaire de constitutionnalité visant les articles L. 302-5, L. 302-7 et L. 302-9-1-1 du code de la construction et de l'habitation, présentée par la commune de Chessy à l'appui de son appel contre ce jugement. Par un arrêt n° 16PA02556 du 29 décembre 2017, la cour administrative d'appel a rejeté cet appel.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 28 février et 24 mai 2018 au secrétariat du contentieux du Conseil d'Etat, la commune de Chessy demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Par un mémoire, enregistré le 28 février 2018, présenté en application de l'article R. 771-16 du code de justice administrative, la commune de Chessy conteste l'ordonnance du 19 avril 2017 du président de la 3ème chambre de la cour administrative d'appel de Paris refusant de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution, d'une part, de l'article L. 302-5 du code de la construction et de l'habitation en tant que ses dispositions, telles qu'interprétées par la jurisprudence du Conseil d'Etat, définissent les agglomérations de plus de 50 000 habitants, au sein desquelles le prélèvement institué par l'article L. 302-7 est applicable, par référence à la notion d'unité urbaine retenue par l'Institut national de la statistique et des études économiques (INSEE) et, d'autre part, des articles L. 302-5, L. 302-7 et L. 302-9-1-1 du même code, en tant que leurs dispositions ne permettent pas à une commune se trouvant dans l'impossibilité juridique et matérielle de réaliser les objectifs légaux de réalisation de logements sociaux d'être exonérée du prélèvement institué par l'article L. 302-7.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution du 4 octobre 1958, notamment son Préambule ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 modifiée ;<br/>
              - le code de la construction et de l'habitation ;<br/>
              - la loi n° 2014-58 du 27 janvier 2014 ;<br/>
              - la loi n° 2015-292 du 16 mars 2015 ;<br/>
              - la loi n° 2015-991 du 7 août 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de la commune de Chessy.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que, par un arrêté du 24 mars 2014, remplaçant un arrêté du 24 février précédent, le préfet de Seine-et-Marne a fixé le montant du prélèvement sur les ressources fiscales de la commune de Chessy au titre de l'inventaire des logements sociaux de l'année 2013 ; que, par une décision du 6 juin 2014, il a rejeté le recours gracieux formé par la commune contre cet arrêté ; que, par un arrêté du 20 février 2015, le préfet de Seine-et-Marne a fixé le montant du même prélèvement au titre de l'année 2014 ; que le recours gracieux formé par la commune a été implicitement rejeté ; que, par un jugement du 13 mai 2006, le tribunal administratif de Melun a rejeté les requêtes de la commune tendant à l'annulation de ces arrêtés et décisions et à la décharge des sommes mises à sa charge ; que la commune a fait appel de ce jugement devant la cour administrative d'appel de Paris ; qu'elle a soulevé devant la cour, par un mémoire distinct, la question de la conformité à la Constitution des articles L. 302-5, L. 302-7 et L. 302-9-1-1 du code de la construction et de l'habitation ; que, par une ordonnance du 19 avril 2017, le président de la 3ème chambre de la cour a refusé de transmettre la question prioritaire de constitutionnalité ; que, par un arrêt du 29 décembre 2017, la cour a rejeté l'appel formé par la commune ; que cette dernière se pourvoit en cassation contre cet arrêt et conteste, par un mémoire distinct, le refus de transmission de la question prioritaire de constitutionnalité ;<br/>
<br/>
              Sur la contestation de l'ordonnance portant refus de transmission de la question prioritaire de constitutionnalité :<br/>
<br/>
              2. Considérant que les dispositions de l'article 23-2 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel prévoient que, lorsqu'une juridiction relevant du Conseil d'Etat est saisie de moyens contestant la conformité d'une disposition législative aux droits et libertés garantis par la Constitution, elle transmet au Conseil d'Etat la question de constitutionnalité ainsi posée à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et qu'elle ne soit pas dépourvue de caractère sérieux ;<br/>
<br/>
              3. Considérant, d'une part, qu'en posant une question prioritaire de constitutionnalité, tout justiciable a le droit de contester la constitutionnalité de la portée effective qu'une interprétation jurisprudentielle constante du Conseil d'Etat ou de la Cour de cassation confère à une disposition législative ; qu'il suit de là que l'adoption d'une telle interprétation, intervenant postérieurement à la décision par laquelle le Conseil constitutionnel a déclaré la disposition législative en cause conforme à la Constitution, est susceptible de constituer une circonstance nouvelle de nature à permettre que soit posée une question prioritaire de constitutionnalité relative à cette disposition ; qu'ainsi, en jugeant que les décisions du Conseil d'Etat, étant dépourvues de portée normative, ne pouvaient constituer une telle circonstance nouvelle, le président de la 3ème chambre de la cour administrative d'appel de Paris a commis une erreur de droit ;<br/>
<br/>
              4. Considérant, d'autre part, qu'aux termes de l'article 16 de la Déclaration du 26 août 1789 : " Toute société dans laquelle la garantie des droits n'est pas assurée, ni la séparation des pouvoirs déterminée, n'a point de Constitution " ; qu'il résulte de cette disposition qu'il ne doit pas être porté d'atteinte substantielle au droit des personnes intéressées d'exercer un recours effectif devant une juridiction ; que, par suite, en retenant que le droit à un recours effectif n'était pas garanti par la Constitution, le président de la 3ème chambre de la cour a commis une autre erreur de droit ;<br/>
<br/>
              5. Mais considérant que les dispositions des articles L. 302-5 et L. 302-7 du code de la construction et de l'habitation ont déjà été déclarées conformes à la Constitution par le Conseil constitutionnel dans ses décisions n° 2000-436 DC du 7 décembre 2000 et n° 2012-660 DC du 17 janvier 2013 ; que la décision du Conseil d'Etat n° 350071 du 17 avril 2013, qui retient, en se référant aux travaux préparatoires de la loi " solidarité et renouvellement urbain " du 13 décembre 2000, qu'il y a lieu, pour déterminer si des communes sont " comprises, au sens du recensement général de la population, dans une agglomération de plus de 50 000 habitants " de se référer à la notion d'unité urbaine retenue par l'Institut national de la statistique et des études économiques (INSEE), ne constitue pas, eu égard à sa portée, une circonstance nouvelle de nature à justifier que la conformité de ces dispositions à la Constitution soit à nouveau examinée par le Conseil constitutionnel ; que ne présentent pas davantage un tel caractère les lois du 27 janvier 2014 de modernisation de l'action publique territoriale et d'affirmation des métropoles, du 16 mars 2015 relative à l'amélioration du régime de la commune nouvelle, pour des communes fortes et vivantes et du 7 août 2015 portant nouvelle organisation territoriale de la République, qui n'ont eu ni pour objet ni pour effet de modifier le champ d'application des dispositions litigieuses ni de modifier les compétences des communes concernant la réalisation de logements sociaux ; que, par ailleurs, l'article L. 302-9-1-1 du code de la construction et de l'habitation, qui institue une procédure de carence qui n'a pas été mise en oeuvre à l'encontre de la commune de Chessy, n'est pas applicable au litige porté devant les juges du fond ;<br/>
<br/>
              6. Considérant que ces motifs, dont l'examen n'implique l'appréciation d'aucune circonstance de fait et qui justifient le dispositif de l'ordonnance attaquée en ce qu'elle refuse de transmettre au Conseil d'Etat la question prioritaire de constitutionnalité soulevée, doivent être substitués aux motifs retenu par le président de la 3ème chambre de la cour administrative d'appel de Paris ; que, dès lors, la commune de Chessy n'est pas fondée à contester son ordonnance ; <br/>
<br/>
              Sur le pourvoi en cassation :<br/>
<br/>
              7. Considérant qu'aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux " ; <br/>
<br/>
              8. Considérant que, pour demander l'annulation de l'arrêt de la cour administrative d'appel de Paris qu'elle attaque, la commune de Chessy soutient que la cour :<br/>
              - a méconnu le principe du contradictoire en s'abstenant de lui transmettre le second mémoire en défense du ministre, alors qu'il contenait des éléments nouveaux sur lesquels elle s'est fondée pour juger qu'aucun des documents qu'elle avait produits n'était de nature à remettre en cause l'appréciation portée par l'INSEE sur la continuité entre sa zone bâtie et celle de la commune de Montevrain ;<br/>
              - a commis une erreur de droit et méconnu son office en jugeant que les éléments qu'elle avait produits n'étaient pas de nature à remettre en cause l'appréciation portée par l'INSEE sur la continuité entre sa zone bâtie et celle de la commune de Montevrain, en s'en remettant ainsi à la liste des unités urbaines établie par l'INSEE et en retenant implicitement que l'administration pouvait se dispenser de porter sa propre appréciation sur cette question ;<br/>
              - a insuffisamment motivé son arrêt, commis une erreur de droit dans l'application du régime de la preuve et dénaturé les pièces du dossier en retenant qu'elle n'avait pas été intégrée à tort dans l'agglomération parisienne alors qu'elle apportait des éléments suffisamment étayés démontrant que la distance entre la zone bâtie où réside plus de la moitié de la population de la commune de Montevrain, avec laquelle sa zone bâtie est continue, et les zones bâties où résident plus de la moitié des populations des communes de Lagny-sur-Marne et de Chanteloup-en-Brie, n'excédait pas 200 mètres ; <br/>
              - a dénaturé les pièces du dossier en relevant qu'elle n'avait pas présenté le moyen tiré de l'absence de conformité à la Constitution de l'article L. 302-7 du code de la construction et de l'habitation au regard du principe de libre administration des communes garanti par l'article 72 de la Constitution, alors que cette question avait été posée et que le président de la 3ème chambre de la cour y avait répondu dans son ordonnance du 19 avril 2017 ;<br/>
              - a insuffisamment motivé son arrêt en s'abstenant de répondre aux moyens tirés de ce qu'une situation de force majeure et la théorie du fait du prince étaient de nature à l'exonérer du respect des articles L. 302-5 et L. 302-7 du code de la construction et de l'habitation et commis une erreur de droit si elle a estimé que ces moyens étaient inopérants ;<br/>
              - a commis une erreur de droit en jugeant que la procédure de fixation du montant du prélèvement institué par l'article L. 302-7 du code de la construction et de l'habitation était indépendante de la procédure de carence prévue à l'article L. 302-9-1-1 de ce code, alors que la mise en oeuvre de cette procédure est susceptible d'avoir une incidence sur le montant de ce prélèvement ;<br/>
<br/>
              9. Considérant qu'aucun de ces moyens n'est de nature à permettre l'admission du pourvoi ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La contestation du refus de transmission de la question prioritaire de constitutionnalité opposé à la commune de Chessy est écartée.<br/>
Article 2 : Le pourvoi de la commune de Chessy n'est pas admis.<br/>
Article 3 : La présente décision sera notifiée à la commune de Chessy à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales et au Premier ministre.<br/>
Copie en sera adressée au préfet de Seine-et-Marne et au Conseil constitutionnel.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-10-05-02-04 PROCÉDURE. - 1) PRINCIPE - POSSIBILITÉ DE CONTESTER LA PORTÉE EFFECTIVE QU'UNE INTERPRÉTATION JURISPRUDENTIELLE CONSTANTE CONFÈRE À LA LOI - EXISTENCE [RJ1] - CONSÉQUENCE - ADOPTION D'UNE TELLE INTERPRÉTATION POUVANT CONSTITUER, POSTÉRIEUREMENT À UNE DÉCISION DU CONSEIL CONSTITUTIONNEL, UNE CIRCONSTANCE NOUVELLE, PERMETTANT QUE SOIT POSÉE UNE QPC - EXISTENCE - 2) ESPÈCE - REFUS DE TRANSMISSION, PAR UNE COUR, D'UNE QPC DIRIGÉE CONTRE DES DISPOSITIONS DÉJÀ DÉCLARÉES CONFORMES À LA CONSTITUTION PAR LE CONSEIL CONSTITUTIONNEL - A) ERREUR DE DROIT À JUGER QUE DES DÉCISIONS DU CONSEIL D'ETAT NE PEUVENT CONSTITUER UNE CIRCONSTANCE NOUVELLE - B) DÉCISION DU CONSEIL D'ETAT NE CONSTITUANT PAS, EU ÉGARD À SA PORTÉE, UNE CIRCONSTANCE NOUVELLE - LOIS NOUVELLES NE CONSTITUANT PAS DAVANTAGE UNE CIRCONSTANCE NOUVELLE - CONSÉQUENCE - REFUS DE TRANSMISSION, AU TERME D'UNE SUBSTITUTION DE MOTIFS.
</SCT>
<ANA ID="9A"> 54-10-05-02-04 1) En posant une question prioritaire de constitutionnalité (QPC), tout justiciable a le droit de contester la constitutionnalité de la portée effective qu'une interprétation jurisprudentielle constante du Conseil d'Etat ou de la Cour de cassation confère à une disposition législative. Il suit de là que l'adoption d'une telle interprétation, intervenant postérieurement à la décision par laquelle le Conseil constitutionnel a déclaré la disposition législative en cause conforme à la Constitution, est susceptible de constituer une circonstance nouvelle de nature à permettre que soit posée une QPC relative à cette disposition.... ...2 a) Ainsi, en jugeant que les décisions du Conseil d'Etat, étant dépourvues de portée normative, ne pouvaient constituer une telle circonstance nouvelle, une cour administrative d'appel commet une erreur de droit.,,b) Les articles L. 302-5 et L. 302-7 du code de la construction et de l'habitation ont déjà été déclarées conformes à la Constitution par le Conseil constitutionnel dans ses décisions n° 2000-346 DC du 7 décembre 2000 et n° 2012-660 DC du 17 janvier 2013. La décision du Conseil d'Etat n° 350071 du 17 avril 2013, qui retient qu'il y a lieu, pour déterminer si des communes sont comprises, au sens du recensement général de la population, dans une agglomération de plus de 50 000 habitants de se référer à la notion d'unité urbaine retenue par l'Institut national de la statistique et des études économiques, ne constitue pas, eu égard à sa portée, une circonstance nouvelle de nature à justifier que la conformité de ces dispositions à la Constitution soit à nouveau examinée par le Conseil constitutionnel. Ne présentent pas davantage un tel caractère les lois n° 2014-58 du 27 janvier 2014, n° 2015-292 du 16 mars 2015 et n° 2015-991 du 7 août 2015, qui n'ont eu ni pour objet ni pour effet de modifier le champ d'application des dispositions litigieuses ni de modifier les compétences des communes concernant la réalisation de logements sociaux. Ces motifs, dont l'examen n'implique l'appréciation d'aucune circonstance de fait et qui justifient le refus de transmettre au Conseil d'Etat la QPC soulevée, doivent être substitués aux motifs retenus par la cour administrative d'appel.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. Cons. const., 14 octobre 2010, n° 2010-52 QPC, cons. 4.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
