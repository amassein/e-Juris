<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035608403</ID>
<ANCIEN_ID>JG_L_2017_09_000000398310</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/60/84/CETATEXT000035608403.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 22/09/2017, 398310</TITRE>
<DATE_DEC>2017-09-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>398310</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BALAT ; SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>M. Thomas Odinot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:398310.20170922</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Melun d'annuler la décision du 17 septembre 2013 du directeur de la Caisse nationale de retraites des agents des collectivités locales (CNRACL) lui refusant de pouvoir cumuler entièrement la pension de retraite qu'il perçoit avec la rémunération qui lui est versée par le syndicat intercommunal d'assainissement de Marne-la-Vallée, ainsi que celle du 25 octobre 2013 rejetant son recours gracieux. Par un jugement n° 1309587 du 31 décembre 2015, le tribunal administratif de Melun a rejeté sa demande.<br/>
<br/>
              Par une ordonnance n° 16PA00750 du 24 mars 2016, enregistrée le 29 mars 2016 au secrétariat du contentieux du Conseil d'Etat, le président de la cour administrative d'appel de Paris a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi, enregistré le 18 février 2016 au greffe de cette cour, présenté par M.B.... <br/>
<br/>
              Par ce pourvoi et par un nouveau mémoire, enregistré le 20 juin 2016 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de la Caisse des dépôts et consignations la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code des pensions civiles et militaires de retraite ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thomas Odinot, auditeur,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Balat, avocat de M. B...et à la SCP Odent, Poulet, avocat de la Caisse des dépôts et consignations ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, M.B..., directeur général des services du syndicat intercommunal d'assainissement de Marne-la-Vallée (SIAM), a été radié des cadres et a liquidé ses droits à la retraite le 31 août 2011 ; qu'il a poursuivi une activité professionnelle à partir du 1er septembre 2011 en exerçant les mêmes fonctions dans le cadre d'un contrat à durée déterminée à mi-temps ; que M. B..., par ailleurs, a été élu en qualité d'adjoint au maire de Villeparisis et exerçait ce mandat depuis mars 2008 ; que, par une décision du 17 septembre 2013, le directeur de la Caisse nationale de retraites des agents des collectivités locales (CNRACL) lui a refusé la possibilité de cumuler entièrement sa pension de retraite avec la rémunération qui lui est versée par le syndicat intercommunal, au motif qu'il n'avait pas liquidé auprès de l'institution de retraite complémentaire des agents non titulaires de l'Etat et des collectivités locales (IRCANTEC) la pension pour laquelle il a cotisé au titre de son mandat d'adjoint ; que, par un jugement du 31 décembre 2015, le tribunal administratif de Melun a rejeté la demande de M. B...tendant à l'annulation de cette décision ; que ce dernier se pourvoit en cassation contre ce jugement ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 84 du code des pensions civiles et militaires de retraite, dans sa version applicable au litige : " (...) Si, à compter de la mise en paiement d'une pension civile ou militaire, son titulaire perçoit des revenus d'activité de l'un des employeurs mentionnés à l'article L. 86-1, il peut cumuler sa pension dans les conditions fixées aux articles L. 85, L. 86 et L. 86-1. / Par dérogation au précédent alinéa, et sous réserve que l'assuré ait liquidé ses pensions de vieillesse personnelles auprès de la totalité des régimes légaux ou rendus légalement obligatoires, de base et complémentaires, français et étrangers, ainsi que des régimes des organisations internationales dont il a relevé, une pension peut être entièrement cumulée avec une activité professionnelle :/ a) A partir de l'âge prévu au 1° de l'article L. 351-8 du code de la sécurité sociale ;/ b) A partir de l'âge prévu au premier alinéa de l'article L. 351-1 du même code, lorsque l'assuré justifie d'une durée d'assurance et de périodes reconnues équivalentes mentionnée au deuxième alinéa du même article au moins égale à la limite mentionnée au même alinéa " ; qu'aux termes de l'article L. 85 du même code : " Le montant brut des revenus d'activité mentionnés au deuxième alinéa de l'article L. 84 ne peut, par année civile, excéder le tiers du montant brut de la pension pour l'année considérée./ Lorsqu'un excédent est constaté, il est déduit de la pension après application d'un abattement (...) " ;  qu'aux termes de l'article L. 86-1 du même code : " Les employeurs mentionnés au deuxième alinéa de l'article L. 84 sont les suivants :/ (...) 2° Les collectivités territoriales et les établissements publics ne présentant pas un caractère industriel ou commercial qui leur sont rattachés (...) " ;<br/>
<br/>
              3. Considérant que l'exercice effectif des fonctions d'adjoint au maire ouvre droit au versement d'indemnités dans les limites et conditions fixées par le code général des collectivités territoriales ; qu'aux termes de l'article L. 2123-28 de ce code : " Les élus qui perçoivent une indemnité de fonction en application des dispositions du présent code ou de toute autre disposition régissant l'indemnisation de leurs fonctions sont affiliés au régime complémentaire de retraite institué au profit des agents non titulaires des collectivités publiques./ Les pensions versées en exécution du présent article sont cumulables sans limitation avec toutes autres pensions ou retraites (...) " ; qu'aux termes de l'article L. 2123-29 du même code : " Les cotisations des communes et celles de leurs élus résultant de l'application des articles L. 2123-27 et L. 2123-28 sont calculées sur le montant des indemnités effectivement perçues par ces derniers en application des dispositions du présent code ou de toute autre disposition régissant l'indemnisation de leurs fonctions. / Les cotisations des élus ont un caractère personnel et obligatoire " ; <br/>
<br/>
              4. Considérant qu'il résulte des dispositions de l'article L. 84 du code des pensions civiles et militaires de retraite que le bénéfice de la dérogation prévue par le troisième alinéa de cet article, permettant à un assuré de pouvoir entièrement cumuler sa pension avec les revenus d'une activité professionnelle exercée pour l'un des employeurs mentionnés à l'article L. 86-1, est subordonné à la condition que l'intéressé ait préalablement liquidé ses pensions de vieillesse personnelles auprès de la totalité des régimes légaux ou rendus légalement obligatoires, de base et complémentaires ; que, toutefois, le législateur n'a pas entendu, eu égard à l'objet de ces dispositions, inclure dans les régimes visés le régime spécifique de retraite assis sur les cotisations versées au titre de l'exercice d'un mandat d'élu local, organisé par le code général des collectivités territoriales qui prévoit notamment que les pensions servies à ce titre sont cumulables sans limitation avec toutes les autres pensions ; qu'il s'ensuit qu'en jugeant que la circonstance que M. B...cotisait à l'IRCANTEC au titre de son mandat d'adjoint au maire et qu'il n'avait dès lors pas liquidé l'intégralité de ses droits à pension au sens du troisième alinéa de l'article L. 84 du code des pensions civiles et militaires de retraite faisait obstacle à ce qu'il bénéficiât de la dérogation prévue par ces dispositions, le tribunal administratif de Melun a commis une erreur de droit ; que, par suite, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, M. B...est fondé à demander l'annulation du jugement qu'il attaque ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui a été dit au point 4 que la circonstance que M. B...n'a pas liquidé ses droits auprès de l'IRCANTEC à raison de son mandat d'élu local n'est, en tout état de cause, pas de nature à le faire regarder comme n'ayant pas liquidé ses pensions de vieillesse personnelles auprès de la totalité des régimes légaux ou rendus légalement obligatoires, de base et complémentaires, pour l'application du troisième alinéa de l'article L. 84 du code des pensions civiles et militaires de retraite ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens de sa demande, M. B...est fondé à demander l'annulation de la décision du 17 septembre 2013 du directeur de la Caisse nationale de retraites des agents des collectivités locales, ainsi que celle du 25 octobre 2013 rejetant son recours gracieux ; <br/>
<br/>
              7. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la Caisse des dépôts et consignations la somme de 5 000 euros à verser à M. B...au titre des dispositions de l'article L. 761-1 du code de justice administrative pour l'ensemble de la procédure ; qu'en revanche, ces mêmes dispositions font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de M. B...qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 31 décembre 2015 du tribunal administratif de Melun est annulé.<br/>
Article 2 : Les décisions des 17 septembre et 25 octobre 2013 du directeur de la Caisse nationale de retraites des agents des collectivités locales sont annulées.<br/>
Article 3 : La Caisse des dépôts et consignations versera à M. B...une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de la Caisse des dépôts et consignations présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à M. A...B...et à la Caisse des dépôts et consignations.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">48-02-01-08 PENSIONS. PENSIONS CIVILES ET MILITAIRES DE RETRAITE. QUESTIONS COMMUNES. CUMULS. - REVENUS D'ACTIVITÉ VERSÉS PAR UN EMPLOYEUR MENTIONNÉ À L'ART. L. 86-1 DU CPCMR POUVANT ÊTRE ENTIÈREMENT CUMULÉS AVEC UNE PENSION (ART. L. 84 DU CPCMR) - CONDITION DE LIQUIDATION PRÉALABLE INTÉGRALE DES DROITS À PENSION - CONDITION NON OPPOSABLE EN CE QUI CONCERNE LES COTISATIONS VERSÉES AU TITRE D'UN MANDAT D'ÉLU LOCAL.
</SCT>
<ANA ID="9A"> 48-02-01-08 Il résulte de l'article L. 84 du code des pensions civiles et militaires de retraite (CPCMR) que le bénéfice de la dérogation prévue par le troisième alinéa de cet article, permettant à un assuré de pouvoir entièrement cumuler sa pension avec les revenus d'une activité professionnelle exercée pour l'un des employeurs mentionnés à l'article L. 86-1, est subordonné à la condition que l'intéressé ait préalablement liquidé ses pensions de vieillesse personnelles auprès de la totalité des régimes légaux ou rendus légalement obligatoires, de base et complémentaires. Toutefois, le législateur n'a pas entendu, eu égard à l'objet de ces dispositions, inclure dans les régimes visés le régime spécifique de retraite assis sur les cotisations versées au titre de l'exercice d'un mandat d'élu local, organisé par le code général des collectivités territoriales qui prévoit notamment que les pensions servies à ce titre sont cumulables sans limitation avec toutes les autres pensions.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
