<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031519854</ID>
<ANCIEN_ID>JG_L_2015_11_000000364757</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/51/98/CETATEXT000031519854.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème / 10ème SSR, 23/11/2015, 364757</TITRE>
<DATE_DEC>2015-11-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>364757</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème / 10ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LEVIS</AVOCATS>
<RAPPORTEUR>M. Olivier Japiot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:364757.20151123</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La SARL New Margin a demandé au tribunal administratif de Paris la décharge des cotisations supplémentaires d'impôt sur les sociétés et de contribution sur cet impôt auxquelles la SA New Margin a été assujettie au titre des exercices 2000 et 2001. Par un jugement n° 0915215 du 6 décembre 2011, le tribunal administratif de Paris a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 12PA00867 du 25 octobre 2012, la cour administrative d'appel de Paris a rejeté l'appel formé par la SARL New Margin contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 24 décembre 2012, 25 mars 2013 et 28 mai 2015, au secrétariat du contentieux du Conseil d'Etat, la SARL New Margin demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ainsi que la somme de 35 euros correspondant au montant de la contribution à l'aide juridique en application de l'article R. 761-1 du même code.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Japiot, conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lévis, avocat de la SARL New Margin ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond, qu'au titre des années 2000 et 2001, l'administration a mis à la charge de la SA New Margin des cotisations supplémentaires d'impôt sur les sociétés et de contribution sur cet impôt en conséquence de la réintégration, dans le bénéfice imposable d'une société de personnes dont elle détenait la quasi-totalité des parts, d'honoraires et de provisions que l'administration estimait injustifiés ; que la SARL New Margin a saisi le tribunal administratif de Paris d'une demande de décharge de ces impositions, que le tribunal, par un jugement du 6 décembre 2011, a jugé irrecevable au motif que, malgré l'invitation à régulariser qui lui avait été faite, cette société n'avait justifié ni de sa qualité ni de son intérêt à agir contre les impositions mises à la charge de la SA New Margin ; que la SARL New Margin se pourvoit en cassation contre l'arrêt du 25 octobre 2012 par lequel la cour administrative d'appel de Paris a rejeté sa requête, au motif que, si la société produisait devant elle la délibération par laquelle l'assemblée générale de la société anonyme New Margin avait décidé de transformer la société en société à responsabilité limitée, la production de cette délibération devant la cour n'était pas de nature à régulariser la demande présentée devant le tribunal administratif ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 612-1 du code de justice administrative : " Lorsque des conclusions sont entachées d'une irrecevabilité susceptible d'être couverte après l'expiration du délai de recours, la juridiction ne peut les rejeter en relevant d'office cette irrecevabilité qu'après avoir invité leur auteur à les régulariser. / (...) / La demande de régularisation mentionne que, à défaut de régularisation, les conclusions pourront être rejetées comme irrecevables dès l'expiration du délai imparti qui, sauf urgence, ne peut être inférieur à quinze jours. (...)" ; <br/>
<br/>
              3. Considérant qu'un requérant peut justifier à tout moment de la procédure devant les juges du fond, y compris pour la première fois en appel, de la qualité qui lui donnait intérêt pour agir ; que, par suite, s'il n'appartenait pas à la cour, en l'absence de contestation sur ce point, de rechercher si la demande de régularisation mentionnée dans le jugement du tribunal administratif contenait toutes les mentions requises par l'article R. 612-1 du code de justice administrative, ni si cette demande avait été régulièrement notifiée à la société, la cour a commis une erreur de droit en jugeant que la production devant elle de la délibération citée au point 1 n'était pas de nature à régulariser la demande présentée par la société requérante devant le tribunal administratif sans rechercher si elle pouvait permettre à la société de justifier d'une qualité lui donnant intérêt pour agir ; que la SARL New Margin est, dès lors, et sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, fondée à demander l'annulation de l'arrêt qu'elle attaque ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3 000 euros à verser à la SARL New Margin au titre des articles L. 761-1 et R. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : L'arrêt du 25 octobre 2012 de la cour administrative d'appel de Paris est annulé.<br/>
Article 2 : L'affaire est renvoyée devant la cour administrative d'appel de Paris.<br/>
Article 3 : L'Etat versera une somme de 3 000 euros à la SARL New Margin au titre des articles L. 761-1 et R. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la SARL New Margin et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-01-04 PROCÉDURE. INTRODUCTION DE L'INSTANCE. INTÉRÊT POUR AGIR. - POSSIBILITÉ DE JUSTIFIER À TOUT MOMENT DE L'INTÉRÊT POUR AGIR - EXISTENCE, DEVANT LES JUGES DU FOND [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-01-03-02 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. CONCLUSIONS. CONCLUSIONS IRRECEVABLES. - INVITATION À RÉGULARISER (ART. R. 612-1 DU CJA) - OBLIGATION DU JUGE D'APPEL DE VÉRIFIER D'OFFICE LA RÉGULARITÉ DE L'INVITATION À RÉGULARISER FAITE PAR LE PREMIER JUGE - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-07-01-04-01-01 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. MOYENS D'ORDRE PUBLIC À SOULEVER D'OFFICE. ABSENCE. - RÉGULARITÉ DE L'INVITATION À RÉGULARISER DES CONCLUSIONS IRRECEVABLES (ART. R. 612-1 DU CJA) FAITE PAR LE JUGE DE PREMIÈRE INSTANCE.
</SCT>
<ANA ID="9A"> 54-01-04 Un requérant peut justifier à tout moment de la procédure devant les juges du fond, y compris pour la première fois en appel, de la qualité qui lui donnait intérêt pour agir.</ANA>
<ANA ID="9B"> 54-07-01-03-02 Lorsqu'un tribunal administratif a invité, en application de l'article R. 612-1 du code de justice administrative (CJA), l'une des parties à régulariser ses conclusions entachées d'une irrecevabilité susceptible d'être couverte après l'expiration du délai de recours, il n'appartient pas au juge d'appel, en l'absence de contestation sur ce point, de rechercher si la demande de régularisation contenait toutes les mentions requises par l'article R. 612-1 du CJA, ni si cette demande avait été régulièrement notifiée à la partie intéressée.</ANA>
<ANA ID="9C"> 54-07-01-04-01-01 Lorsqu'un tribunal administratif a invité, en application de l'article R. 612-1 du code de justice administrative (CJA), l'une des parties à régulariser ses conclusions entachées d'une irrecevabilité susceptible d'être couverte après l'expiration du délai de recours, il n'appartient pas au juge d'appel, en l'absence de contestation sur ce point, de rechercher si la demande de régularisation contenait toutes les mentions requises par l'article R. 612-1 du CJA, ni si cette demande avait été régulièrement notifiée à la partie intéressée.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 3 mai 1993,  Société industrielle de construction, n° 124888, T. p. 941-981-1116.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
