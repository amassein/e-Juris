<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042504504</ID>
<ANCIEN_ID>JG_L_2020_11_000000441887</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/50/45/CETATEXT000042504504.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 04/11/2020, 441887, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-11-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>441887</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Action en astreinte</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Martin Guesdon</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:441887.20201104</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une lettre enregistrée le 15 juin 2020 au secrétariat de la section du rapport et des études du Conseil d'Etat, M. A... B... a demandé au Conseil d'Etat de prendre les mesures qu'implique l'exécution de la décision n°421093 du 24 février 2020 par laquelle le Conseil d'Etat a annulé la décision implicite de rejet de sa demande de modification de l'arrêté du 29 novembre 2001 fixant la liste des emplois ouvrant droit à la nouvelle bonification indiciaire (NBI) au titre de la politique de la ville à certains personnels du ministère de l'écologie, de l'énergie, du développement durable et de la mer, en charge des technologies vertes et des négociations sur le climat et a mis à la charge de l'Etat la somme de 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Par décision du 2 juillet 2020, la présidente de la section du rapport et des études du Conseil d'Etat a classé la demande de M. A... B....<br/>
<br/>
              Par trois mémoires enregistrés les 2 et 16 juillet 2020 et le 10 septembre 2020 au secrétariat de la section du contentieux du Conseil d'Etat, M. A... B... conteste ce classement et demande au Conseil d'Etat de prescrire par voie juridictionnelle les mesures d'exécution de sa décision  du 24 février 2020, en enjoignant à l'administration de donner à l'arrêté du 3 juin 2020 modifiant l'arrêté du 29 décembre 2001 fixant la liste des emplois ouvrant droit à la nouvelle bonification indiciaire au titre de la politique de la ville à certains personnels du ministère de l'écologie, de l'énergie, du développement durable et de la mer, en charge des technologies vertes et des négociations sur le climat, une portée rétroactive au 13 mai 2018, de prononcer à l'encontre de l'Etat, s'il ne justifie pas avoir pris les mesures de nature à assurer l'exécution complète de la décision du 24 février 2020 dans un délai d'un mois à compter de la notification de la présente décision, une astreinte de 100 euros par jour de retard et de mettre à la charge de l'Etat le versement de la somme de 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Par une ordonnance n° 441887 du 27 août 2020, le président de la section du contentieux a décidé l'ouverture d'une procédure juridictionnelle.<br/>
<br/>
              La demande a été communiquée à la ministre de la transition écologique, qui n'a pas produit de mémoire.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
               Vu :<br/>
              - la loi n° 91-73 du 18 janvier 1991 ;<br/>
              - le décret n° 2001-1129 du 29 novembre 2001 ;<br/>
              - l'arrêté du 29 novembre 2001 fixant les conditions d'attribution de la nouvelle bonification indiciaire au titre de la mise en oeuvre de la politique de la ville à certains personnels du ministère de l'équipement, des transports et du logement; <br/>
              - l'arrêté du 29 novembre 2001 fixant la liste des emplois ouvrant droit à la nouvelle bonification indiciaire au titre de la politique de la ville à certains personnels du ministère de l'écologie, de l'énergie, du développement durable et de la mer, en charge des technologies vertes et des négociations sur le climat modifié ; <br/>
              - l'arrêté du 3 juin 2020 modifiant l'arrêté du 29 décembre 2001 fixant la liste des emplois ouvrant droit à la nouvelle bonification indiciaire au titre de la politique de la ville à certains personnels du ministère de l'écologie, de l'énergie, du développement durable et de la mer, en charge des technologies vertes et des négociations sur le climat;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Martin Guesdon, auditeur,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 911-5 du code de justice administrative : " En cas d'inexécution d'une de ses décisions ou d'une décision rendue par une juridiction administrative autre qu'un tribunal administratif ou une cour administrative d'appel, le Conseil d'Etat peut, même d'office, lorsque cette décision n'a pas défini les mesures d'exécution, procéder à cette définition, fixer un délai d'exécution et prononcer une astreinte contre les personnes morales en cause. ". L'article R. 931-2 du même code prévoit, en outre, que : " Les parties intéressées peuvent demander au Conseil d'Etat de prescrire les mesures nécessaires à l'exécution d'une de ses décisions ou d'une décision d'une juridiction administrative spéciale, en assortissant le cas échéant ces prescriptions d'une astreinte (...) ". <br/>
<br/>
              2. Par une décision du 24 février 2020, le Conseil d'Etat, statuant au contentieux a annulé pour excès de pouvoir la décision implicite de rejet de la demande de M. A... B... de modification de l'arrêté du 29 novembre 2001 fixant la liste des emplois ouvrant droit à la nouvelle bonification indiciaire (NBI) au titre de la politique de la ville à certains personnels du ministère de l'écologie, de l'énergie, du développement durable et de la mer, en charge des technologies vertes et des négociations sur le climat en tant que son annexe n'incluait pas l'emploi d'adjoint à la cheffe d'unité " hébergement-logement " du service de la cohésion sociale à la direction départementale de la cohésion sociale et de la protection des populations de Haute-Corse. <br/>
<br/>
              3. Il résulte de l'instruction que, par arrêté du 3 juin 2020, le ministre de la transition écologique et solidaire a modifié l'arrêté du 29 novembre 2001 en y ajoutant l'emploi d'adjoint à la cheffe d'unité " hébergement-logement " du service de la cohésion sociale. <br/>
<br/>
              4. Si M. A... B... soutient que le ministre aurait dû donner à cet arrêté modificatif une portée rétroactive au 13 mai 2018, date à laquelle est née la décision implicite lui refusant la modification qu'il sollicitait, l'exécution de la décision annulant pour excès de pouvoir cette décision implicite impliquait seulement que le ministre apporte pour l'avenir à l'arrêté réglementaire du 29 novembre 2001 cette modification. Par suite, en prenant l'arrêté du 3 juin 2020, le ministre doit être regardé comme ayant pris les mesures propres à assurer l'exécution de la décision du 24 février 2020.  <br/>
<br/>
              5. Par suite, la demande de M. A... B... doit être rejetée, ainsi que ses conclusions à fin d'injonction et les conclusions qu'il présente au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. A... B... est rejetée. <br/>
Article 2  : La présente décision sera notifiée à M. C... B... et à la ministre de la transition écologique.<br/>
Copie en sera adressée à la section du rapport et des études du Conseil d'Etat. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
