<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042434208</ID>
<ANCIEN_ID>JG_L_2020_10_000000427620</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/43/42/CETATEXT000042434208.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 16/10/2020, 427620</TITRE>
<DATE_DEC>2020-10-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>427620</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Paul Bernard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:427620.20201016</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A... C... et M. B... C... ont demandé au tribunal administratif de Bastia d'annuler l'arrêté du 20 mars 2015 par lequel le maire de l'Ile Rousse (Haute-Corse) a refusé de leur délivrer un permis de construire un immeuble collectif de trente neuf logements sur une surface de plancher de 2538 m². <br/>
<br/>
              Par un jugement n° 1500421 du 13 avril 2017, le tribunal administratif a annulé cet arrêté. <br/>
<br/>
              Par un arrêt n° 17MA02469 du 3 décembre 2018, la cour administrative d'appel de Marseille, saisie par la ministre de la cohésion des territoires et des relations avec les collectivités territoriales, a annulé ce jugement et rejeté la demande de M. et Mme C.... <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 4 février et 6 mai 2019 et le 22 septembre 2020 au secrétariat du contentieux du Conseil d'Etat, M. et Mme C... demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, d'annuler l'arrêté du 20 mars 2015 du maire de l'Ile Rousse ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 3 500 euros en application des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Paul Bernard, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de M. et Mme C... ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un jugement du 13 avril 2017, le tribunal administratif de Bastia a annulé l'arrêté du 20 mars 2015 par lequel le maire de l'Ile Rousse a rejeté la demande de permis de construire de M. et Mme C.... Par un arrêt du 3 décembre 2018, rendu sur appel de la ministre de la cohésion des territoires et des relations avec les collectivités territoriales, la cour administrative d'appel de Marseille a annulé ce jugement et rejeté la demande d'annulation de l'arrêté du maire. M. et Mme C... se pourvoient en cassation contre cet arrêt.<br/>
<br/>
              2. En premier lieu, aux termes de l'article L. 422-5 du code de l'urbanisme : " Lorsque le maire ou le président de l'établissement public de coopération intercommunale est compétent, il recueille l'avis conforme du préfet si le projet, [objet de la demande de permis de construire ou de déclaration préalable] est situé : a) Sur une partie du territoire communal non couverte par une carte communale, un plan local d'urbanisme ou un document d'urbanisme en tenant lieu (...) ". L'annulation de la décision par laquelle le maire ou le président de l'établissement public de coopération intercommunale, se conformant, en application de ces dispositions, à l'avis négatif du préfet, rejette la demande de permis ou s'oppose à la déclaration préalable, est susceptible de préjudicier aux intérêts dont le législateur a confié la défense au représentant de l'Etat en subordonnant la réalisation du projet à son accord. Par suite, la cour administrative d'appel n'a pas commis d'erreur de droit en jugeant que l'Etat avait la qualité de défendeur à l'instance devant le tribunal administratif et que, par suite, la ministre de la cohésion des territoires et des relations avec les collectivités territoriales avait qualité pour relever appel du jugement ayant annulé le refus opposé par le maire, sur avis négatif du préfet, à la demande de M. et Mme C.... <br/>
<br/>
              3. En second lieu, aux termes du I de l'article L. 146-4 du code de l'urbanisme, dans sa rédaction applicable : " L'extension de l'urbanisation doit se réaliser soit en continuité avec les agglomérations et villages existants, soit en hameaux nouveaux intégrés à l'environnement. " Aux termes du II du même article : " L'extension limitée de l'urbanisation des espaces proches du rivage ou des rives des plans d'eau intérieurs désignés à l'article 2 de la loi n° 86-2 du 3 janvier 1986 précitée doit être justifiée et motivée, dans le plan local d'urbanisme, selon des critères liés à la configuration des lieux ou à l'accueil d'activités économiques exigeant la proximité immédiate de l'eau ". Le schéma d'aménagement de la Corse, qui vaut schéma de mise en valeur de la mer et produit les mêmes effets que les directives territoriales d'aménagement, prescrit que l'urbanisation du littoral demeure limitée. Pour en prévenir la dispersion, il privilégie la densification des zones urbaines existantes en évitant une urbanisation linéaire diffuse. Pour juger que le projet des requérants, distant d'environ cent mètres du rivage, méconnaissait ces dispositions, la cour administrative d'appel a notamment relevé, au terme d'une appréciation souveraine des faits de l'espèce, que la parcelle en cause était séparée du centre urbain de l'Ile Rousse par une route, bordée à l'ouest par une zone naturelle, au sud par une parcelle dépourvue de construction et à l'est par une station d'épuration et que le projet avait pour objet l'édification de trente-neuf logements pour une surface de 2 538 mètres carrés. En se fondant sur ces éléments pour juger que le terrain d'assiette du projet n'était pas situé en continuité d'un espace déjà urbanisé et que le projet ne constituait pas une extension limitée de l'urbanisation, la cour administrative d'appel n'a pas dénaturé les pièces du dossier qui lui était soumis.<br/>
<br/>
              4. Il résulte de ce qui précède que M. et Mme C... ne sont pas fondés à demander l'annulation de l'arrêt qu'ils attaquent. <br/>
<br/>
              5. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi de M. et Mme C... est rejeté. <br/>
Article 2 : La présente décision sera notifiée à Mme A... C... et à M. B... C..., à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales et à la commune de l'Ile Rousse.  <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-08-01-01 PROCÉDURE. VOIES DE RECOURS. APPEL. RECEVABILITÉ. - AUTORISATION D'URBANISME SOUMISE À L'AVIS CONFORME DU PRÉFET (ART. L. 422-5 DU CODE DE L'URBANISME) - AUTORITÉ COMPÉTENTE SE CONFORMANT À UN AVIS NÉGATIF - QUALITÉ DE PARTIE EN PREMIÈRE INSTANCE DE L'ETAT - EXISTENCE - CONSÉQUENCE - RECEVABILITÉ DE L'APPEL DU MINISTRE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-03-02-03 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. PROCÉDURE D'ATTRIBUTION. AUTORITÉ COMPÉTENTE POUR STATUER SUR LA DEMANDE. - AVIS CONFORME DU PRÉFET (ART. L. 422-5 DU CODE DE L'URBANISME) - AUTORITÉ COMPÉTENTE SE CONFORMANT À UN AVIS NÉGATIF - QUALITÉ DE PARTIE EN PREMIÈRE INSTANCE DE L'ETAT - EXISTENCE - CONSÉQUENCE - RECEVABILITÉ DE L'APPEL DU MINISTRE [RJ1].
</SCT>
<ANA ID="9A"> 54-08-01-01 L'annulation de la décision par laquelle le maire ou le président de l'établissement public de coopération intercommunale, se conformant, en application de l'article L. 422-5 du code de l'urbanisme, à l'avis négatif du préfet, rejette la demande de permis ou s'oppose à la déclaration préalable, est susceptible de préjudicier aux intérêts dont le législateur a confié la défense au représentant de l'Etat en subordonnant la réalisation du projet à son accord.,,,Par suite, l'Etat a la qualité de défendeur à l'instance devant le tribunal administratif et, dès lors, le ministre chargé de l'urbanisme a qualité pour relever appel du jugement ayant annulé le refus opposé par le maire, sur avis négatif du préfet, à une demande de permis construire.</ANA>
<ANA ID="9B"> 68-03-02-03 L'annulation de la décision par laquelle le maire ou le président de l'établissement public de coopération intercommunale, se conformant, en application de l'article L. 422-5 du code de l'urbanisme, à l'avis négatif du préfet, rejette la demande de permis ou s'oppose à la déclaration préalable, est susceptible de préjudicier aux intérêts dont le législateur a confié la défense au représentant de l'Etat en subordonnant la réalisation du projet à son accord.,,,Par suite, l'Etat a la qualité de défendeur à l'instance devant le tribunal administratif et, dès lors, le ministre chargé de l'urbanisme a qualité pour relever appel du jugement ayant annulé le refus opposé par le maire, sur avis négatif du préfet, à une demande de permis construire.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur les critères de recevabilité à faire appel, CE, Section, 9 janvier 1959, Sieur de Harenne, n° 41383, p. 23.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
