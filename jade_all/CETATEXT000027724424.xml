<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027724424</ID>
<ANCIEN_ID>JG_L_2013_07_000000356063</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/72/44/CETATEXT000027724424.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème sous-section jugeant seule, 17/07/2013, 356063, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-07-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>356063</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>Mme Véronique Rigal</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:356063.20130717</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>		VU LA PROCEDURE SUIVANTE :<br/>
<br/>
<br/>
 Procédure contentieuse antérieure<br/>
<br/>
              M. et Mme A...B...ont demandé au tribunal administratif de Rennes de prononcer la décharge des cotisations supplémentaires d'impôt sur le revenu auxquelles ils ont été assujettis au titre des années 2003 et 2004, ainsi que des contributions sociales et des pénalités correspondantes. Par un jugement n° 0704262 du 12 mai 2010, le tribunal administratif de Rennes a partiellement fait droit à leur demande en les déchargeant des compléments d'impôt sur le revenu et de contributions sociales mis à leur charge au titre des années 2003 et 2004 à concurrence de ceux résultant de l'imposition des produits de cession des droits d'exploitation de logiciels à la société Progica ainsi que des contributions sur les revenus locatifs mises à leur charge au titre des années 2003 à 2005. <br/>
<br/>
              Le ministre du budget, des comptes publics et de la réforme de l'Etat a demandé à la cour administrative d'appel de Nantes d'annuler ce jugement en tant qu'il a partiellement déchargé M. et Mme B...des cotisations supplémentaires d'impôt sur le revenu et contributions sociales auxquelles ils étaient assujettis. <br/>
<br/>
              Par un appel incident, M. et Mme B...ont demandé à la cour d'annuler ce même jugement en tant qu'il n'a fait que partiellement droit à leur demande de décharge des cotisations supplémentaires d'impôt sur le revenu et contributions sociales ainsi que des pénalités correspondantes. <br/>
<br/>
              Par un arrêt n° 10NT01676 du 17 novembre 2011, la cour administrative d'appel de Nantes, faisant droit à l'appel du ministre, a remis à la charge de M. et Mme B...les cotisations supplémentaires d'impôt sur le revenu et de contributions sociales dont ils avaient été déchargés, ainsi que les pénalités correspondantes, et a rejeté l'appel incident de M. et Mme B.... <br/>
<br/>
<br/>
 Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 23 janvier et 23 avril 2012 au secrétariat du contentieux du Conseil d'Etat, M. et MmeB..., représentés par la SCP Nicolaÿ-de Lanouvelle-Hannotin, demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 10NT01676 du 17 novembre 2011 de la cour administrative d'appel de Nantes ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel du ministre et de faire droit à leurs conclusions d'appel incident;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Par un mémoire en défense, enregistré le 29 octobre 2012, le ministre délégué chargé du budget conclut au rejet du pourvoi. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Véronique Rigal, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de M. et MmeB... ;<br/>
<br/>
<br/>
<br/>
<br/>			CONSIDERANT CE QUI SUIT :<br/>
<br/>
<br/>
              Sur le moyen tiré de l'irrégularité de la procédure : <br/>
<br/>
              1. Aux termes de l'article R. 611-1 du code de justice administrative : " La requête et les mémoires, ainsi que les pièces produites par les parties, sont déposés ou adressés au greffe. / La requête, le mémoire complémentaire annoncé dans la requête et le premier mémoire de chaque défendeur sont communiqués aux parties avec les pièces jointes dans les conditions prévues aux articles R. 611-3, R. 611-5 et R. 611-6. / Les répliques, autres mémoires et pièces sont communiqués s'ils contiennent des éléments nouveaux. (...) ". L'article R. 613-2 de ce même code dispose que : " Si le président de la formation de jugement n'a pas pris une ordonnance de clôture, l'instruction est close trois jours francs avant la date de l'audience indiquée dans l'avis d'audience prévu à l'article R. 711-2. Cet avis le mentionne. (...) ". Selon l'article R. 613-3 du même code : " Les mémoires produits après la clôture de l'instruction ne donnent pas lieu à communication et ne sont pas examinés par la juridiction. / Si les parties présentent avant la clôture de l'instruction des conclusions nouvelles ou des moyens nouveaux, la juridiction ne peut les adopter sans ordonner un supplément d'instruction. ". Enfin, aux termes de l'article R. 613-4 du code de justice administrative : " Le président de la formation de jugement peut rouvrir l'instruction par une décision qui n'est pas motivée et ne peut faire l'objet d'aucun recours. (...) / La réouverture de l'instruction peut également résulter d'un jugement ou d'une mesure d'investigation ordonnant un supplément d'instruction. / Les mémoires qui auraient été produits pendant la période comprise entre la clôture et la réouverture de l'instruction sont communiqués aux parties. ".<br/>
<br/>
              2. Lorsqu'il décide de verser au contradictoire après la clôture de l'instruction un mémoire qui a été produit par les parties avant ou après celle-ci, le président de la formation de jugement du tribunal administratif ou de la cour administrative d'appel doit être regardé comme ayant rouvert l'instruction. Il lui appartient, dans tous les cas, de clore l'instruction ainsi rouverte et, le cas échéant, de fixer une nouvelle date d'audience. <br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond que le mémoire en réplique du ministre délégué chargé du budget a été produit le 17 octobre 2011, c'est-à-dire après la clôture de l'instruction qui est intervenue trois jours francs avant l'audience, conformément à l'article R. 613-2 du code de justice administrative. Ce mémoire a été communiqué à M. et Mme B... le 18 octobre 2011, soit deux jours avant l'audience tenue devant la cour administrative d'appel de Nantes le 20 octobre 2011. Cette communication a eu pour effet de rouvrir l'instruction. En s'abstenant de la clore à nouveau, la cour administrative d'appel a statué irrégulièrement. M. et Mme B...sont fondés, pour ce motif, à demander l'annulation de l'arrêt qu'ils attaquent. <br/>
<br/>
              Sur les autres moyens du pourvoi : <br/>
<br/>
              4. M. et Mme B...soutiennent également que : <br/>
<br/>
              - la cour a commis une erreur de droit en soulevant d'office un moyen tiré de ce que M. B...n'aurait pas exploité et affecté les logiciels en cause à son activité professionnelle pendant au moins cinq ans avant la date de leur concession, en méconnaissance des dispositions du II de l'article 151 septies du code général des impôts ; <br/>
<br/>
              - la cour a inversé la charge de la preuve en estimant qu'il incombait à M. et Mme B...de prouver qu'ils avaient exploité les logiciels en cause pendant au moins cinq ans avant la date de leur concession ; <br/>
<br/>
              - la cour a commis une erreur de droit en jugeant que la condition d'exploitation pendant plus de cinq ans s'appliquait au logiciel à l'origine de la plus-value. <br/>
<br/>
              Le moyen tiré de l'irrégularité de la procédure suffisant à entraîner l'annulation de l'arrêt attaqué, il n'y a pas lieu pour le Conseil d'Etat de répondre à ces moyens.<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              5. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par M. et Mme B...au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>				D E C I D E  :<br/>
              				--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nantes du 17 novembre 2011 est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée devant la cour administrative d'appel de Nantes. <br/>
<br/>
 Article 3 : Le surplus des conclusions du pourvoi est rejeté.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. et Mme A...B...et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
