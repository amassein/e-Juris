<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043358776</ID>
<ANCIEN_ID>JG_L_2021_04_000000436663</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/35/87/CETATEXT000043358776.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 12/04/2021, 436663</TITRE>
<DATE_DEC>2021-04-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>436663</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; SCP ROCHETEAU, UZAN-SARANO ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Thomas Pez-Lavergne</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Marc Pichon de Vendeuil</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:436663.20210412</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Ile de Sein Energies (IDSE) a demandé au tribunal administratif de Rennes de constater l'illégalité de la convention de concession pour le service public de la distribution d'énergie électrique conclue le 2 mars 1993 par le syndicat départemental d'énergie et d'équipement du Finistère avec Electricité de France (EDF), de mettre fin à l'exécution de ce contrat, d'enjoindre au président de ce syndicat de prendre les mesures imposées par la fin de l'exécution du contrat, sous astreinte, et, le cas échéant, de transmettre à la Cour de justice de l'Union européenne des questions préjudicielles. Par un jugement n° 1701166 du 5 novembre 2018, le tribunal administratif de Rennes a rejeté la demande de la société Ile de Sein Energies.<br/>
<br/>
              Par un arrêt n°19NT00073 du 11 octobre 2019, la cour administrative d'appel de Nantes a rejeté l'appel formé par la société Ile de Sein Energies contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 11 décembre 2019, 12 mars 2020 et 22 mars 2021 au secrétariat du contentieux du Conseil d'Etat, la société Ile de Sein Energies demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
<br/>
              3°) de mettre à la charge du syndicat départemental d'énergie et d'équipement du Finistère et d'EDF la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - la directive 2009/72/CE du Parlement Européen et du Conseil du 13 juillet 2009 ;<br/>
              - la directive 2009/28/CE du Parlement européen et du Conseil du 23 avril 2009 ;<br/>
              - la directive (UE) 2019/944 du Parlement européen et du Conseil du 5 juin 2019 ;<br/>
              - le code de l'énergie ; <br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 25 mars 2021 présentée par la société Ile de Sein Energies ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thomas Pez-Lavergne, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Marc Pichon de Vendeuil, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la société Ile de Sein Energies, à la SCP Rocheteau, Uzan-Sarano, avocat du syndicat départemental d'énergie et d'équipement du Finistère et à la SCP Piwnica, Molinié, avocat de la société Electricité de France ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que le 2 mars 1993, le syndicat départemental d'énergie et d'équipement du Finistère (SDEF) a conclu avec Electricité de France (EDF) une " convention de concession pour le service public de la distribution d'énergie électrique " dont la durée était fixée à 30 ans. Le champ d'application territorial de cette convention a été étendu à l'île de Sein par un avenant du 4 juin 1993. Par un courrier du 2 novembre 2016, la société Ile de Sein Energies (IDSE) a demandé au SDEF qu'il soit mis fin à l'exécution de cette convention en tant qu'elle concernait l'île de Sein et que " la concession du réseau de distribution de l'électricité sur l'île " lui soit " transférée ". Par un courrier du 14 février 2017, le SDEF a rejeté cette demande au motif qu'EDF tenait du 3° de l'article L. 111-52 du code de l'énergie l'exclusivité de la gestion du réseau de distribution de l'électricité sur l'île. La société IDSE a alors demandé au tribunal administratif de Rennes de constater l'illégalité de la convention de concession en tant qu'elle portait sur l'île de Sein et de mettre fin à son exécution dans cette même mesure. Par un jugement du 5 novembre 2018, cette demande a été rejetée. La société IDSE a relevé appel de ce jugement en tant qu'il a rejeté ses conclusions tendant à la résiliation de la convention. Par un arrêt du 11 octobre 2019, contre lequel la société IDSE se pourvoit en cassation, la cour administrative d'appel de Nantes a rejeté sa requête.<br/>
<br/>
              Sur le cadre juridique du litige :<br/>
<br/>
              En ce qui concerne le régime juridique des concessions de distribution d'électricité :<br/>
<br/>
              2. D'une part, aux termes de l'article 24 de la directive 2009/72/CE du Parlement européen et du Conseil du 13 juillet 2009 concernant les règles communes pour le marché intérieur de l'électricité et abrogeant la directive 2003/54/CE : " Les Etats membres désignent, ou demandent aux entreprises propriétaires ou responsables de réseaux de distribution de désigner, pour une durée à déterminer par les Etats membres en fonction de considérations d'efficacité et d'équilibre économique, un ou plusieurs gestionnaires de réseau de distribution. Les Etats membres veillent à ce que les gestionnaires de réseau de distribution agissent conformément aux articles 25, 26 et 27 ". Selon l'article 26 de cette directive : " 1. Lorsque le gestionnaire de réseau de distribution fait partie d'une entreprise verticalement intégrée, il est indépendant, au moins sur le plan de la forme juridique, de l'organisation et de la prise de décision, des autres activités non liées à la distribution. Ces règles ne créent pas d'obligation de séparer la propriété des actifs du gestionnaire de réseau de distribution, d'une part, de l'entreprise verticalement intégrée, d'autre part. / 2. En plus des exigences visées au paragraphe 1, lorsque le gestionnaire de réseau de distribution fait partie d'une entreprise verticalement intégrée, il est indépendant, sur le plan de l'organisation et de la prise de décision, des autres activités non liées à la distribution. (...) / 3. Lorsque le gestionnaire de réseau de distribution fait partie d'une entreprise verticalement intégrée, les États membres veillent à ce que ses activités soient surveillées par les autorités de régulation ou d'autres organes compétents afin que le gestionnaire de réseau de distribution ne puisse pas tirer profit de son intégration verticale pour fausser la concurrence. En particulier, les gestionnaires de réseau de distribution appartenant à une entreprise verticalement intégrée s'abstiennent, dans leurs pratiques de communication et leur stratégie de marque, de toute confusion avec l'identité distincte de la branche " fourniture " de l'entreprise verticalement intégrée. / 4. Les États membres peuvent décider de ne pas appliquer les paragraphes 1, 2 et 3 aux entreprises intégrées d'électricité qui approvisionnent moins de 100 000 clients connectés ou approvisionnent de petits réseaux isolés ".<br/>
<br/>
              3. D'autre part, aux termes de l'article L. 111-52 du code de l'énergie : " Les gestionnaires des réseaux publics de distribution d'électricité sont, dans leurs zones de desserte exclusives respectives : / 1° La société gestionnaire des réseaux publics de distribution issue de la séparation entre les activités de gestion de réseau public de distribution et les activités de production ou de fourniture exercées par Electricité de France en application de l'article L. 111- 57 ; / 2° Les entreprises locales de distribution définies à l'article L. 111-54 ou les entreprises locales de distribution issues de la séparation entre leurs activités de gestion de réseau public de distribution et leurs activités de production ou de fourniture, en application de l'article L. 111-57 ou de l'article L. 111-58 ; / 3° Le gestionnaire du réseau public de distribution d'électricité est, dans les zones non interconnectées au réseau métropolitain continental, l'entreprise Electricité de France ainsi que la société mentionnée à l'article L. 151-2 ".<br/>
<br/>
              En ce qui concerne les règles contentieuses :<br/>
<br/>
              4. Un tiers à un contrat administratif susceptible d'être lésé dans ses intérêts de façon suffisamment directe et certaine par une décision refusant de faire droit à sa demande de mettre fin à l'exécution du contrat, est recevable à former devant le juge du contrat un recours de pleine juridiction tendant à ce qu'il soit mis fin à l'exécution du contrat. Les tiers ne peuvent utilement soulever, à l'appui de leurs conclusions tendant à ce qu'il soit mis fin à l'exécution du contrat, que des moyens tirés de ce que la personne publique contractante était tenue de mettre fin à son exécution du fait de dispositions législatives applicables aux contrats en cours, de ce que le contrat est entaché d'irrégularités qui sont de nature à faire obstacle à la poursuite de son exécution et que le juge devrait relever d'office ou encore de ce que la poursuite de l'exécution du contrat est manifestement contraire à l'intérêt général. A cet égard, les requérants peuvent se prévaloir d'inexécutions d'obligations contractuelles qui, par leur gravité, compromettent manifestement l'intérêt général. En revanche, ils ne peuvent se prévaloir d'aucune autre irrégularité, notamment pas celles tenant aux conditions et formes dans lesquelles la décision de refus a été prise. En outre, les moyens soulevés doivent, sauf lorsqu'ils le sont par le représentant de l'Etat dans le département ou par les membres de l'organe délibérant de la collectivité territoriale ou du groupement de collectivités territoriales compte tenu des intérêts dont ils ont la charge, être en rapport direct avec l'intérêt lésé dont le tiers requérant se prévaut.<br/>
<br/>
              Sur la régularité de l'arrêt :<br/>
<br/>
              5. En premier lieu, l'article 267 du traité sur le fonctionnement de l'Union européenne fait obligation aux seules " juridictions nationales dont les décisions ne sont pas susceptibles d'un recours juridictionnel de droit interne " de saisir à titre préjudiciel la Cour de justice de l'Union européenne en cas de difficulté sérieuse d'interprétation d'un traité. Il suit de là qu'en s'abstenant de saisir la Cour de justice de l'Union européenne de la question de l'interprétation des dispositions notamment des articles 24 et 26 précités de la directive 2009/72/CE du 13 juillet 2009, la cour administrative d'appel de Nantes, qui n'était pas tenue de saisir la Cour de justice de l'Union européenne, a suffisamment motivé sa décision sur ce point et n'a pas commis d'erreur de droit en estimant que le refus du tribunal administratif de Nantes de faire droit à une telle demande de renvoi n'avait pas à être motivé.<br/>
<br/>
              6. En second lieu, il appartient au juge administratif de se prononcer sur le bien-fondé des moyens dont il est saisi et, le cas échéant, d'écarter de lui-même, quelle que soit l'argumentation du défendeur, un moyen qui lui paraît infondé, au vu de l'argumentation qu'il incombe au requérant de présenter au soutien de ses prétentions. Dès lors, en se référant aux dispositions du paragraphe 4 de l'article 26 de la directive 2009/72/CE du 13 juillet 2009 concernant des règles communes pour le marché intérieur de l'électricité pour écarter le moyen tiré de ce que le caractère permanent des droits exclusifs conférés à EDF méconnaîtrait les dispositions de l'article 24 de la même directive, la cour administrative d'appel n'a pas relevé d'office un moyen qu'elle serait tenue de communiquer aux parties en application des dispositions de l'article R. 611-7 du code de justice administrative. Par suite, le moyen soulevé par la société IDSE tiré d'une violation du principe du contradictoire ne peut qu'être écarté.<br/>
<br/>
              Sur les moyens tirés de ce que la poursuite de l'exécution de la concession en litige est manifestement contraire à l'intérêt général :<br/>
<br/>
              7. Aux termes de l'article 16 de la directive 2009/28/CE du 23 avril 2009 relative à la promotion de l'utilisation de l'énergie produite à partir de sources renouvelables et modifiant puis abrogeant les directives 2001/77/CE et 2003/30/CE : " 1. Les États membres prennent les mesures appropriées pour développer (...) le réseau électrique de manière à (...) tenir compte des progrès dans le domaine de la production d'électricité à partir de sources d'énergie renouvelables (...). / 2. (...) / a) les Etats membres veillent à ce que les opérateurs de systèmes de transport et de distribution présents sur leur territoire garantissent le transport et la distribution de l'électricité produite à partir de sources d'énergie renouvelables ; / b) les Etats membres prévoient, en outre, soit un accès prioritaire, soit un accès garanti au réseau pour l'électricité produite à partir de sources d'énergie renouvelables ; / (...) ".<br/>
<br/>
              8. La société requérante soutient que ces dispositions et les motifs de la directive imposent, compte tenu de l'incidence environnementale de la production électrique sur l'île de Sein par EDF et du frein à la réalisation des objectifs de la directive du 23 avril 2009 que constitue le fait de désigner EDF comme seul et unique gestionnaire du réseau de distribution électrique sur l'île, qu'il soit mis fin à l'exécution de la convention en litige. Toutefois, ni ces dispositions, qui ne fixent aux Etats membres que des objectifs, ni les motifs de la directive dont se prévaut la société requérante, qui sont au surplus dépourvus de valeur juridique contraignante, ne constituent un motif d'intérêt général imposant la résiliation d'une convention légalement conclue, à moins qu'il ne soit établi que la poursuite de son exécution fait évidemment et immédiatement obstacle à ce que l'Etat puisse se conformer aux obligations résultant du traité sur le fonctionnement de l'Union européenne. La société IDSE, qui n'établit pas que l'exécution de la concession en litige constitue un obstacle à ce que l'Etat atteigne les objectifs fixés par la directive 2009/28/CE du 23 avril 2009, dans le délai qu'elle prescrit, n'est pas fondée à soutenir qu'en jugeant que l'application de ces dispositions n'imposerait pas en l'espèce la résiliation de la convention litigieuse, la cour administrative d'appel, qui a suffisamment motivé son arrêt, aurait commis une erreur de droit ou dénaturé les pièces du dossier.<br/>
<br/>
              9. Par ailleurs, la cour administrative d'appel a pu estimer, sans commettre d'erreur de droit ni dénaturer les écritures, eu égard à l'argumentation qui lui était soumise, que les allégations de la société IDSE selon lesquelles le réseau électrique sur l'île de Sein n'est pas convenablement entretenu et que sa gestion par EDF est dispendieuse, ce qui aurait pour conséquence de rendre l'exécution de la convention de concession en litige manifestement contraire à l'intérêt général, ne sont corroborées par aucun élément de preuve suffisant.<br/>
<br/>
<br/>
<br/>
<br/>
              Sur les moyens tirés de ce que la concession a été attribuée sans mise en concurrence :<br/>
<br/>
              10. Il ressort des énonciations de l'arrêt attaqué que la société IDSE soutenait à l'appui de ses conclusions tendant à ce qu'il soit mis fin à l'exécution de la convention litigieuse qu'elle avait été irrégulièrement attribuée à EDF sans mise en concurrence, les droits exclusifs dont bénéficie EDF en application du 3° de l'article L. 111-52 du code de l'énergie sans limitation de durée pour la gestion du réseau public de distribution d'électricité dans les zones non interconnectées au réseau métropolitain continental ne remplissant pas les conditions posées par les dispositions du paragraphe 2 de l'article 106 du traité sur le fonctionnement de l'Union européenne et méconnaissant l'exigence posée par l'article 24 de la directive 2009/72/CE du 13 juillet 2009 de la désignation par les Etats membres du gestionnaire de réseau de distribution d'électricité pour une durée à déterminer en fonction de considérations d'efficacité et d'équilibre économique.<br/>
<br/>
              11. En premier lieu, la directive 2009/72/CE du 13 juillet 2009, qui n'était pas applicable lors de la conclusion de la concession en litige en 1993, n'a pas d'effet direct sur les contrats en cours, pas davantage que la directive 2003/54/CE du 26 juin 2003 qu'elle a abrogé ni que la directive 96/92/CE du 19 décembre 1996 qui l'a précédée.<br/>
<br/>
              12. En second lieu, si la méconnaissance des règles de publicité et de mise en concurrence peut, le cas échéant, être utilement invoquée à l'appui d'un référé précontractuel d'un concurrent évincé ou du recours d'un tiers contestant devant le juge du contrat la validité d'un contrat ou de certaines de ses clauses non réglementaires qui en sont divisibles, cette méconnaissance n'est en revanche pas susceptible, en l'absence de circonstances particulières, d'entacher un contrat d'un vice d'une gravité de nature à faire obstacle à la poursuite de son exécution et que le juge devrait relever d'office. Par suite, la société IDSE, qui n'invoquait aucune circonstance particulière impliquant que le juge du contrat mette fin à l'exécution du contrat, ne pouvait utilement soutenir que la convention litigieuse avait été irrégulièrement attribuée à EDF sans mise en concurrence.<br/>
<br/>
              13. Ces motifs, qui n'emportent l'appréciation d'aucune circonstance de fait, doivent être substitués aux motifs retenus par l'arrêt attaqué pour écarter les moyens soulevés par la société IDSE relatifs à l'irrégularité de la convention. Il en résulte que sont sans incidence sur le bien-fondé de l'arrêt attaqué les moyens tirés de ce que la cour administrative d'appel a insuffisamment motivé son arrêt et commis une erreur de droit en estimant que l'exclusivité dont bénéficie EDF pour la gestion du réseau public de distribution d'électricité sur l'île de Sein ne méconnaît pas les règles de transparence et de publicité qui résultent du traité sur le fonctionnement de l'Union européenne, a entaché son arrêt d'une contradiction de motifs en affirmant que l'attribution à EDF de la concession est imposée par une disposition législative sans durée limitée et que la désignation d'EDF comme gestionnaire du réseau de distribution est la conséquence de la convention de concession d'une durée limitée, a commis une erreur de droit en estimant que les droits exclusifs attribués à EDF sont la contrepartie des missions de service d'intérêt économique général que la loi lui impose et en jugeant, de manière au demeurant surabondante, que le paragraphe 4 de l'article 26 de la directive 2009/72/CE du 13 juillet 2009 qui permet de déroger, s'agissant des entreprises intégrées d'électricité qui approvisionnent de petits réseaux isolés ou moins de 100 000 clients connectés, à l'obligation de dissociation du gestionnaire du réseau de distribution prévue par les paragraphes 1 à 3 du même article, permettait également de déroger à l'exigence, posée par l'article 24 de la même directive, de désignation du gestionnaire de réseau de distribution pour une durée à déterminer.<br/>
<br/>
              Sur les frais non compris dans les dépens :<br/>
<br/>
              14. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de société IDSE la somme de 3 000 euros à verser respectivement au syndicat départemental d'énergie et d'équipement du Finistère (SDEF) et à la société EDF, au titre des dispositions de l'article L. 761-1 du code de justice administrative. En revanche, ses dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de ce syndicat et d'EDF qui ne sont pas, dans la présente instance, les parties perdantes.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi de la société Ile de Sein Energies est rejeté.<br/>
<br/>
Article 2 : La société Ile de Sein Energies versera au syndicat départemental d'énergie et d'équipement du Finistère (SDEF) et à la société EDF une somme de 3 000 euros chacun au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la société Ile de Sein Energies, au syndicat départemental d'énergie et d'équipement du Finistère et à Electricité de France (EDF).<br/>
Copie en sera adressée à la ministre de la transition écologique.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-04-02 MARCHÉS ET CONTRATS ADMINISTRATIFS. FIN DES CONTRATS. RÉSILIATION. - CONTESTATION PAR UN TIERS D'UNE DÉCISION REFUSANT DE METTRE FIN À L'EXÉCUTION DU CONTRAT - MOYENS INVOCABLES [RJ1] - MÉCONNAISSANCE DES RÈGLES DE PUBLICITÉ ET DE MISE EN CONCURRENCE - ABSENCE, EN PRINCIPE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-08 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. - CONTESTATION PAR UN TIERS D'UNE DÉCISION REFUSANT DE METTRE FIN À L'EXÉCUTION DU CONTRAT - MOYENS INVOCABLES [RJ1] - MÉCONNAISSANCE DES RÈGLES DE PUBLICITÉ ET DE MISE EN CONCURRENCE - ABSENCE, EN PRINCIPE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-02-02 PROCÉDURE. DIVERSES SORTES DE RECOURS. RECOURS DE PLEIN CONTENTIEUX. - CONTESTATION PAR UN TIERS D'UNE DÉCISION REFUSANT DE METTRE FIN À L'EXÉCUTION DU CONTRAT - MOYENS INVOCABLES [RJ1] - MÉCONNAISSANCE DES RÈGLES DE PUBLICITÉ ET DE MISE EN CONCURRENCE - ABSENCE, EN PRINCIPE.
</SCT>
<ANA ID="9A"> 39-04-02 Si la méconnaissance des règles de publicité et de mise en concurrence peut, le cas échéant, être utilement invoquée à l'appui du référé précontractuel d'un concurrent évincé ou du recours d'un tiers contestant devant le juge du contrat la validité d'un contrat ou de certaines de ses clauses non réglementaires qui en sont divisibles, cette méconnaissance n'est en revanche pas susceptible, en l'absence de circonstances particulières, d'entacher un contrat d'un vice d'une gravité de nature à faire obstacle à la poursuite de son exécution et que le juge devrait relever d'office.</ANA>
<ANA ID="9B"> 39-08 Si la méconnaissance des règles de publicité et de mise en concurrence peut, le cas échéant, être utilement invoquée à l'appui du référé précontractuel d'un concurrent évincé ou du recours d'un tiers contestant devant le juge du contrat la validité d'un contrat ou de certaines de ses clauses non réglementaires qui en sont divisibles, cette méconnaissance n'est en revanche pas susceptible, en l'absence de circonstances particulières, d'entacher un contrat d'un vice d'une gravité de nature à faire obstacle à la poursuite de son exécution et que le juge devrait relever d'office.</ANA>
<ANA ID="9C"> 54-02-02 Si la méconnaissance des règles de publicité et de mise en concurrence peut, le cas échéant, être utilement invoquée à l'appui du référé précontractuel d'un concurrent évincé ou du recours d'un tiers contestant devant le juge du contrat la validité d'un contrat ou de certaines de ses clauses non réglementaires qui en sont divisibles, cette méconnaissance n'est en revanche pas susceptible, en l'absence de circonstances particulières, d'entacher un contrat d'un vice d'une gravité de nature à faire obstacle à la poursuite de son exécution et que le juge devrait relever d'office.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. en précisant, CE, Section, 30 juin 2017, Syndicat mixte de promotion de l'activité transmanche, n° 398445, p. 209.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
