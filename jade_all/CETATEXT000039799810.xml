<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039799810</ID>
<ANCIEN_ID>JG_L_2019_12_000000436941</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/79/98/CETATEXT000039799810.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 30/12/2019, 436941, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>436941</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2019:436941.20191230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu les procédures suivantes :<br/>
<br/>
              1° Sous le n° 436941, par une requête, enregistrée le 20 décembre 2019 au secrétariat du contentieux du Conseil d'Etat, le Conseil national des barreaux, la Conférence des bâtonniers, l'Ordre des avocats au barreau de Paris, l'Association des avocats conseils d'entreprise, la Confédération nationale des avocats et la Fédération nationale des unions de jeunes avocats demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) de suspendre l'exécution du décret n° 2019-1333 du 11 décembre 2019 réformant la procédure civile ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Le Conseil national des barreaux et autres soutiennent que :<br/>
              - la condition d'urgence est remplie dès lors que, en prévoyant une date d'entrée en vigueur au 1er janvier 2020 alors que la modification substantielle des règles de la procédure civile qu'il induit nécessite un temps d'adaptation suffisant, le décret litigieux préjudicie de manière suffisamment grave et immédiate aux intérêts de la profession d'avocat ;<br/>
              - il existe un doute sérieux quant à la légalité du décret contesté ; <br/>
              - il est entaché d'un vice de procédure dès lors que la Commission nationale de l'informatique et des libertés n'a pas été consultée préalablement à son édiction alors qu'il prévoit la collecte de données à caractère personnel par la nécessité, sous peine de nullité, de mentionner, dans la demande initiale formée par voie électronique, l'adresse électronique et le numéro de téléphone mobile du demandeur ou de son avocat ;<br/>
              - il méconnaît le droit au respect de la vie privée dès lors qu'il impose pour toute demande initiale formée par voie électronique, à peine de nullité, la mention de l'adresse électronique et du numéro de téléphone mobile du demandeur ou de son avocat ;<br/>
              - il est susceptible de causer une rupture d'égalité entre avocats et entre justiciables dès lors qu'ils ne sont pas tous équipés d'un téléphone mobile ni même dotés d'une adresse électronique ;<br/>
              - il est entaché d'erreur matérielle, en ce que, en premier lieu, il prévoit la dispense du ministère d'avocat dans les matières relevant de la compétence du juge de l'exécution alors qu'il devrait s'agir du juge du contentieux de la protection, en deuxième lieu, le dernier alinéa de l'article 761 du code de procédure civile devrait être placé à l'article 762 du même code, étant relatif à la représentation devant les tribunaux judiciaires, en troisième lieu, les articles 834 et 835 du code de procédure civile qu'il modifie évoque le juge du contentieux de la protection au lieu d'évoquer le juge des contentieux de la protection et, en dernier lieu, il précise au 22° de son article 29 que le juge des contentieux de la protection serait compétent en matière d'injonction de payer alors que seules les chambres de proximité le sont ;<br/>
              - il est entaché de contradiction, d'une part, entre le nouvel article 514 du code de procédure civile qu'il édicte, prévoyant le principe de l'exécution provisoire de droit, et l'article 539 du même code relatif aux cas dans lesquels l'exécution du jugement est suspendue de droit, d'autre part, entre les conditions de mise à l'écart de l'exécution provisoire du jugement par le juge de première instance et les conditions d'arrêt de l'exécution provisoire d'une décision en cas d'appel ou d'opposition par le premier président d'une cour d'appel et, enfin, entre l'autorité de chose jugée au principal par la décision du juge de première instance ayant écarté l'exécution provisoire de droit et l'ordonnance du premier président statuant en référé lorsqu'il est saisi d'une demande de rétablissement de l'exécution provisoire de droit selon le nouvel article 514-6 du code de procédure civile ;<br/>
              - il est entaché d'incompétence dès lors que seul le législateur était compétent pour prescrire un principe d'exécution provisoire de droit des décisions de première instance, un tel principe portant atteinte aux droits de la défense, qui résultent d'un principe fondamental reconnu par les lois de la République ;<br/>
              - il est entaché d'une erreur manifeste d'appréciation, en particulier eu égard au taux d'infirmation en appel des décisions de première instance, dont il est demandé, à titre de mesure d'instruction, d'ordonner la production dans l'instance de référé ;<br/>
              - il méconnaît le principe d'intelligibilité de la norme dès lors que les conditions permettant d'écarter l'exécution provisoire d'une décision par le juge de première instance sont insuffisamment précises ;<br/>
              - il méconnaît l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales dès lors que l'édiction d'un principe d'exécution provisoire des décisions de première instance constitue une barrière procédurale manifestement disproportionnée au droit à un recours effectif.<br/>
<br/>
<br/>
<br/>
              2° Sous le n° 437005, par une requête enregistrée le 23 décembre 2019, le Syndicat des avocats de France et le Syndicat de la magistrature demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution du même décret du 11 décembre 2019.<br/>
<br/>
<br/>
<br/>
              Le Syndicat des avocats de France et le Syndicat de la magistrature soutiennent que :<br/>
              - leur requête est recevable ;<br/>
              - la condition d'urgence est remplie dès lors que, en prévoyant une date d'entrée en vigueur au 1er janvier 2020 alors que la modification substantielle des règles de la procédure civile qu'il induit nécessite pour cette profession un temps d'adaptation suffisant, le décret litigieux préjudicie de manière suffisamment grave et immédiate aux intérêts de la profession d'avocat ;<br/>
              - il existe un doute sérieux quant à la légalité du décret litigieux ;<br/>
              - il est entaché d'un vice de procédure dès lors que la Commission nationale de l'informatique et des libertés n'a pas été consultée préalablement à sa publication alors qu'il prévoit la collecte de données à caractère personnel ;<br/>
              - il méconnaît tant le règlement du 27 avril 2016 relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel que le droit au respect à la vie privée garanti par l'article 8 de la convention européenne des droits de l'homme dès lors qu'il impose pour toute demande initiale formée par voie électronique, à peine de nullité, la mention de l'adresse électronique et du numéro de téléphone mobile du demandeur ou de son avocat ;<br/>
              - il est entaché d'une contradiction en ce qu'il introduit le principe de l'exécution provisoire de droit des décisions de première instance à l'article 514 du code de procédure civile, alors que l'article 539 du même code prévoit l'effet suspensif des recours ordinaires contre ces mêmes décisions ;<br/>
              - il porte atteinte au droit à un procès équitable énoncé à l'article 6 de la convention européenne des droits de l'homme, dès lors que l'introduction du principe de l'exécution provisoire de droit des décisions de première instance limitera dans les faits la possibilité pour les requérants de relever appel ;<br/>
              - il méconnaît les droits de la défense ;<br/>
              - il est entaché d'incompétence dès lors que le pouvoir réglementaire ne pouvait pas décréter, à défaut d'avoir été prévu par la loi du 23 mars 2019, le principe de l'exécution provisoire de droit des décisions de première instance ;<br/>
              - il est contraire au principe d'égalité, dès lors que le principe de l'exécution provisoire de droit des décisions de première instance n'est pas prévu en droit du travail ;<br/>
              - il méconnaît l'article 16 de la Déclaration des droits de l'homme et du citoyen, dès lors que son article 3 précise que les ordonnances prises par le président de la cour d'appel selon les procédures prévues aux articles 514-3 et 514-4 du code de procédure civile ne sont pas susceptibles d'un pourvoi en cassation ;<br/>
              - il est entaché d'erreur matérielle en ce que, d'une part, l'article 761 du code de procédure civile prévoit une dispense d'avocat pour les matières relevant du juge de l'exécution, en contradiction avec l'article R. 121-6 du code des procédures civiles d'exécution, alors qu'il s'agit des matières relevant du juge des contentieux de la protection, d'autre part, le dernier alinéa de l'article 761 précité devrait figurer au sein de l'article 762 puisqu'il relève de la représentation devant les tribunaux judiciaires et, enfin, l'article 853 du code de procédure civile qu'il institue omet de mentionner la dispense d'avocat lorsque la demande devant le tribunal de commerce est indéterminée ;<br/>
              - il a été pris en méconnaissance du principe de sécurité juridique dès lors que l'article 768 du code de procédure civile qu'il modifie est susceptible de s'appliquer aux instances en cours ;<br/>
              - il est entaché d'erreur matérielle aux articles 834 et 835 du code de procédure civile ;<br/>
              - il est entaché d'une erreur manifeste d'appréciation ;<br/>
              - il est entaché d'une erreur de droit dès lors qu'il supprime la présentation volontaire des parties devant le conseil des prud'hommes pour homologuer un accord sur simple déclaration.<br/>
<br/>
              Par deux mémoires en défense, enregistré les 26 et 27 décembre 2019, la garde des sceaux, ministre de la justice, conclut au rejet des requêtes. Elle soutient que la condition d'urgence n'est pas remplie et que les moyens soulevés par les requérants ne sont pas de nature à faire naître un doute sérieux quant à la légalité des dispositions contestées du décret attaqué.<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de l'organisation judiciaire ;<br/>
              - le code de procédure civile ;<br/>
              - la loi n° 2019-222 du 23 mars 2019 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, le Conseil national des barreaux, la Conférence des bâtonniers, l'Ordre des avocats au barreau de Paris, l'Association des avocats conseils d'entreprise, la Confédération nationale des avocats et la Fédération nationale des unions de jeunes avocats, le Syndicat des avocats de France, le Syndicat de la magistrature et, d'autre part, la garde des sceaux, ministre de la justice ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du vendredi 27 décembre 2019 à 14 heures au cours de laquelle ont été entendus :<br/>
<br/>
              - Me Hannotin, avocat au Conseil d'Etat et à la Cour de cassation, avocat du Conseil national des barreaux, de la Conférence des bâtonniers, de l'Ordre des avocats au barreau de Paris, de l'Association des avocats conseils d'entreprise, de la Confédération nationale des avocats et de la Fédération nationale des unions de jeunes avocats ;<br/>
<br/>
              - les représentants du Conseil national des barreaux, de l'Ordre des avocats au barreau de Paris et de la Confédération nationale des avocats ;<br/>
<br/>
              - Me Meier-Bourdeau, avocat au Conseil d'Etat et à la Cour de cassation, avocat du Syndicat des avocats de France et du Syndicat de la magistrature ;<br/>
<br/>
              - le représentant du Syndicat des avocats de France ;<br/>
<br/>
              - les représentants de la garde des sceaux, ministre de la justice ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Les requêtes visées ci-dessus tendent à la suspension de l'exécution du même décret du 11 décembre 2019 réformant la procédure civile. Il y a lieu de les joindre pour statuer par une seule ordonnance.<br/>
              2. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
<br/>
              3. L'urgence justifie la suspension de l'exécution d'un acte administratif lorsque celui-ci porte atteinte de manière suffisamment grave et immédiate à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre. Il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte contesté sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue. L'urgence doit être appréciée objectivement et compte tenu de l'ensemble des circonstances de l'affaire.<br/>
<br/>
              4. Les requérants demandent, en premier lieu, la suspension de l'exécution des dispositions de l'article 54 du code de procédure civile, résultant de l'article 1er du décret contesté, en ce qu'elles imposent que la demande initiale en matière contentieuse, " lorsqu'elle est formée par voie électronique ", comporte, à peine de nullité, l'adresse électronique et le numéro de téléphone mobile du requérant lorsqu'il consent à la dématérialisation ou de son avocat. <br/>
<br/>
              5. Il résulte toutefois des termes mêmes des dispositions contestées que cette exigence n'est prévue par le décret attaqué qu'en ce qui concerne les requêtes " formées " par voie électronique, c'est-à-dire, ainsi qu'il ressort des indications qui ont été données par les représentants de la garde des sceaux, ministre de la justice, lors de l'audience de référé, seulement celles pour lesquelles la saisine de la juridiction se fera directement en ligne sur un portail accessible par internet et non celles pour lesquelles les actes de procédure sont simplement remis ou transmis à la juridiction par voie électronique. Or il ressort des pièces versées au dossier que la saisine en ligne des juridictions judiciaires est subordonnée au déploiement de fonctionnalités du portail internet accessible aux justiciables, lesquelles ne sont pas encore prêtes. Leur mise en oeuvre supposera, en outre, l'intervention d'autres actes relatifs aux traitements correspondants, définissant notamment la durée de conservation des données et les mesures de sécurité permettant d'assurer leur confidentialité. Au surplus, une telle saisine en ligne ne présentera qu'un caractère facultatif. Dans ces conditions, et alors que l'ouverture des premières possibilités de saisine en ligne, pour les constitutions de partie civile et certaines mesures de tutelle, n'est pas envisagée avant le mois de mars ou d'avril 2020, la condition d'urgence justifiant que soit suspendue immédiatement l'exécution des dispositions critiquées de l'article 54 du code de procédure civile issues du décret attaqué ne peut être regardée comme remplie.<br/>
<br/>
              6. En deuxième lieu, les requérants demandent la suspension de l'exécution de l'article 3 du décret contesté, dont les dispositions prévoient, tout en l'aménageant, le principe de l'exécution provisoire de droit des décisions rendues en première instance. Il résulte toutefois des dispositions du II de l'article 55 du décret contesté que les dispositions résultant de l'article 3 du décret ne sont susceptibles de s'appliquer qu'aux décisions qui seront rendues par les juridictions de première instance statuant sur des instances introduites après le 1er janvier 2020. Il s'ensuit que la condition d'urgence, requise pour que puisse être ordonnée immédiatement la suspension de l'exécution des dispositions critiquées n'est pas satisfaite à la date de la présente ordonnance, alors que les dispositions de l'article 3 du décret ne trouveront pas à s'appliquer avant plusieurs mois. S'il est vrai qu'il est possible au président du tribunal d'autoriser le demandeur à assigner le défendeur à jour fixe dans des délais plus brefs que ceux observés dans le cadre des procédures de droit commun, les dispositions de l'article 788 du code de procédure civile subordonnent cette possibilité à une condition tenant à l'urgence, laquelle conduisait déjà le juge, ainsi qu'il ressort des pièces du dossier, à ordonner l'exécution provisoire du jugement en vertu des dispositions antérieures au décret contesté lorsqu'il faisait droit à la demande qui lui était présentée. Enfin, la circonstance que l'avocat puisse être conduit, avant même le prononcé du jugement, à conseiller le justiciable quant à une demande présentée au juge, tendant à écarter l'exécution provisoire, n'est pas de nature à caractériser une urgence justifiant d'ordonner la suspension immédiate de l'exécution des dispositions en cause.<br/>
<br/>
              7. En troisième lieu, si les requérants critiquent la légalité des dispositions des articles 761, 768, 834, 835 et 853 du code de procédure civile, tels qu'issus de l'article 4 du décret contesté, du 22° de l'article 29 ou de l'article 36 de ce décret, ils ne justifient pas de l'urgence qui s'attacheraient à ce que, sans attendre le jugement des requêtes au fond, l'exécution de ces dispositions soit suspendue.<br/>
<br/>
              8. En quatrième lieu, l'exercice du pouvoir réglementaire implique pour son détenteur la possibilité de modifier à tout moment les normes qu'il définit sans que les personnes auxquelles sont, le cas échéant, imposées de nouvelles contraintes, puissent invoquer un droit au maintien de la réglementation existante. En principe, les nouvelles normes ainsi édictées ont vocation à s'appliquer immédiatement, dans le respect des exigences attachées au principe de non-rétroactivité des actes administratifs. Toutefois, il incombe à l'autorité investie du pouvoir réglementaire, agissant dans les limites de sa compétence et dans le respect des règles qui s'imposent à elle, d'édicter, pour des motifs de sécurité juridique, les mesures transitoires qu'implique, s'il y a lieu, cette réglementation nouvelle. Il en va ainsi lorsque l'application immédiate de celle-ci entraîne, au regard de l'objet et des effets de ses dispositions, une atteinte excessive aux intérêts publics ou privés en cause. Ces mesures transitoires peuvent résider dans le report de l'entrée en vigueur de cette réglementation nouvelle.<br/>
<br/>
              9. En l'espèce, le décret contesté, réformant la procédure civile, a été pris le 11 décembre 2019 et publié au Journal officiel du 12 décembre. Son article 55 prévoit que ses dispositions, pour la plupart, entrent en vigueur le 1er janvier 2020, sous réserve des dérogations prévues au II et au III de cet article, en particulier celle relative à l'exécution provisoire de droit des décisions de première instance mentionnée au point 6 ci-dessus. Si l'article 55 précise, en outre, que les dispositions entrant en vigueur le 1er janvier 2020 sont applicables aux instance en cours à cette date, cette précision ne peut s'entendre que dans le respect des exigences découlant du principe de non-rétroactivité des actes administratifs.<br/>
<br/>
              10. En différant ainsi l'entrée en vigueur de la plupart des dispositions du décret contesté au 1er janvier 2020, l'auteur du décret du 11 décembre 2019 a retenu un report de l'entrée en vigueur de cette réglementation nouvelle qui s'avère bref eu égard à l'ampleur des modifications apportées à la procédure civile. Il reste, toutefois, que nombre de ces dispositions, définissant les règles de procédure civile applicables devant le tribunal judiciaire, devaient entrer en vigueur pour le 1er janvier 2020, date fixée par le législateur, selon le XXIII de l'article 109 de la loi du 23 mars 2019 de programmation 2018-2022 et de réforme pour la justice, pour la création de cette nouvelle juridiction de première instance. Dans ces conditions, si l'on peut regretter qu'une adoption plus précoce du décret n'ait pas été possible, il n'apparaît pas, en l'état de l'instruction et au vu de l'ensemble des intérêts en cause, qu'en retenant la date du 1er janvier 2020 pour l'entrée en vigueur de la plupart des dispositions du décret contesté, l'auteur de ce décret ait fixé un délai trop bref au regard de l'exigence tenant à l'édiction, pour des motifs de sécurité juridique, des mesures transitoires qu'implique, s'il y a lieu, une réglementation nouvelle.<br/>
<br/>
              11. Il résulte de tout ce qui précède, sans qu'il y ait lieu d'ordonner la mesure d'instruction sollicitée, que les conclusions aux fins de suspension des requêtes doivent être rejetées. Les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, en conséquence, qu'être également rejetées.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête du Conseil national des barreaux et autres et la requête du Syndicat des avocats de France et autre sont rejetées.<br/>
Article 2 : La présente ordonnance sera notifiée au Conseil national des barreaux, à la Conférence des bâtonniers, à l'Ordre des avocats au barreau de Paris, à l'Association des avocats conseils d'entreprise, à la Confédération nationale des avocats, à la Fédération nationale des unions de jeunes avocats, au Syndicat des avocats de France, au Syndicat de la magistrature et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
