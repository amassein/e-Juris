<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039274952</ID>
<ANCIEN_ID>JG_L_2019_10_000000425274</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/27/49/CETATEXT000039274952.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 23/10/2019, 425274, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-10-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>425274</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ZRIBI, TEXIER ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Thomas Andrieu</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Anne Iljic</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:425274.20191023</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé à la Cour nationale du droit d'asile d'annuler la décision du 27 février 2018 par laquelle l'Office français de protection des réfugiés et apatrides (OFPRA) a refusé de réexaminer sa demande d'asile après le rejet de sa précédente demande de réexamen par une décision de la Cour nationale du droit d'asile du 22 juin 2017 devenue définitive. <br/>
<br/>
              Par une décision n° 18013743 du 7 septembre 2018, la Cour nationale du droit d'asile a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire, trois mémoires complémentaires et un mémoire en réplique, enregistrés le 7 novembre 2018, le 8 février 2019, le 25 mars 2019, le 11 avril 2019 et le 4 octobre 2019 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) de mettre à la charge de l'OFPRA la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention de Genève du 28 juillet 1951 et le protocole signé à New York le 31 janvier 1967 relatifs au statut des réfugiés ; <br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Réda Wadjinny-Green, auditeur, <br/>
<br/>
              - les conclusions de Mme Anne Iljic, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Zribi, Texier, avocat de M. B... A... et à la SCP Foussard, Froger, avocat de l'OFPRA ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. En vertu de l'article L. 723-2 du code de l'entrée et du séjour des étrangers et du droit d'asile, l'OFPRA se prononce sur la reconnaissance de la qualité de réfugié ou l'octroi de la protection subsidiaire au terme d'une instruction unique au cours de laquelle le demandeur d'asile est mis en mesure de présenter les éléments à l'appui de sa demande. L'article L. 723-3 du même code dispose que : " L'office convoque le demandeur à une audition. Il peut s'en dispenser s'il apparaît que : / a) L'office s'apprête à prendre une décision positive à partir des éléments en sa possession ; / b) Le demandeur d'asile a la nationalité d'un pays pour lequel ont été mises en oeuvre les stipulations du 5 du C de l'article 1er de la convention de Genève du 28 juillet 1951 relative au statut des réfugiés ; / c) Les éléments fournis à l'appui de la demande sont manifestement infondés ; / d) Des raisons médicales interdisent de procéder à l'entretien ". Aux termes de l'article L. 723-16 du même code : " A l'appui de sa demande de réexamen, le demandeur indique par écrit les faits et produit tout élément susceptible de justifier un nouvel examen de sa demande d'asile./ L'office procède à un examen préliminaire des faits ou des éléments nouveaux présentés par le demandeur intervenus après la décision définitive prise sur une demande antérieure ou dont il est avéré qu'il n'a pu en avoir connaissance qu'après cette décision./ Lors de l'examen préliminaire, l'office peut ne pas procéder à un entretien./ Lorsque, à la suite de cet examen préliminaire, l'office conclut que ces faits ou éléments nouveaux n'augmentent pas de manière significative la probabilité que le demandeur justifie des conditions requises pour prétendre à une protection, il peut prendre une décision d'irrecevabilité ". <br/>
<br/>
              2. En vertu de l'article L. 731-2 du même code, la Cour nationale du droit d'asile statue sur les recours formés contre les décisions de l'OFPRA prises en application des articles L. 711-1, L. 712-1 à L. 712-3 et L. 723-1 à L. 723-3.<br/>
<br/>
              3. Il appartient, en principe, à la Cour nationale du droit d'asile, qui est saisie d'un recours de plein contentieux, non d'apprécier la légalité de la décision du directeur général de l'OFPRA qui lui est déférée, mais de se prononcer elle-même sur le droit de l'intéressé à la qualité de réfugié ou au bénéfice de la protection subsidiaire au vu de l'ensemble des circonstances de fait dont elle a connaissance au moment où elle statue. Toutefois, lorsque le recours dont est saisie la Cour est dirigé contre une décision du directeur général de l'Office qui a statué sur une demande d'asile sans procéder à l'audition du demandeur prévue par l'article L. 723-3, il revient à la Cour, eu égard au caractère essentiel et à la portée de la garantie en cause, si elle juge que l'Office n'était pas dispensé par la loi de convoquer le demandeur à une audition et que le défaut d'audition est imputable à l'Office, d'annuler la décision qui lui est déférée et de renvoyer l'examen de la demande d'asile à l'Office, sauf à ce qu'elle soit en mesure de prendre immédiatement une décision positive sur la demande de protection au vu des éléments établis devant elle. <br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond que M. A..., de nationalité srilankaise, a présenté une demande de réexamen de sa demande d'asile qui a été rejetée comme irrecevable par une décision de l'OFPRA en date du 27 février 2018 sans que l'intéressé ait été reçu à un entretien par l'Office. Dès lors que la cour nationale du droit d'asile a jugé que M. A... avait présenté, à l'appui de sa demande de réexamen, un élément nouveau augmentant de manière significative la probabilité qu'il justifie des conditions requises pour bénéficier d'une protection, elle devait annuler la décision d'irrecevabilité prise par l'OFPRA et lui renvoyer la demande de réexamen sauf à ce qu'elle soit en mesure de prendre immédiatement une décision positive sur la demande de protection au vu des éléments établis devant elle. En rejetant le recours de M. A... sans renvoyer l'examen de sa demande à l'OFPRA afin qu'il soit mis à même de bénéficier de la garantie que constitue son audition, la cour a méconnu son office. <br/>
<br/>
              5. Il résulte de ce qui précède que, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, M. A... est fondé à demander l'annulation de la décision qu'il attaque. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'OFPRA la somme de 2 500 euros à verser à M. A... au titre de l'article L. 761-1 du code de justice administrative.   <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                            --------------<br/>
<br/>
Article 1er : La décision de la cour nationale du droit d'asile du 7 septembre 2018 est annulée. <br/>
Article 2 : L'affaire est renvoyée devant la Cour nationale du droit d'asile. <br/>
Article 3 : L'OFPRA versera une somme de 2 500 euros à M. A... au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. B... A... et à l'Office français de protection des réfugiés et apatrides.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
