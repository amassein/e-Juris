<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028047755</ID>
<ANCIEN_ID>JG_L_2013_09_000000352112</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/04/77/CETATEXT000028047755.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 25/09/2013, 352112, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-09-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>352112</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER ; SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR>M. Marc Pichon de Vendeuil</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:352112.20130925</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 23 août et 23 novembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune d'Eveux, représentée par son maire ; la commune d'Eveux demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09LY02619 du 28 juin 2011 par lequel la cour administrative d'appel de Lyon a annulé, à la demande de la société Abos, d'une part, le jugement n° 0804193 du 17 septembre 2009 par lequel le tribunal administratif de Lyon a rejeté sa demande tendant à l'annulation du permis de construire délivré le 25 mars 2008 par le maire d'Eveux à M. et MmeA..., d'autre part, ce permis de construire ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la société Abos ;<br/>
<br/>
              3°) de mettre à la charge de la société Abos la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu la loi n° 2000-1208 du 13 décembre 2000 ;<br/>
<br/>
              Vu la loi n° 2003-590 du 2 juillet 2003 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Pichon de Vendeuil, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Blancpain, Soltner, avocat de la commune d'Eveux et à la SCP Delaporte, Briard, Trichet, avocat de la société Abos ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le maire d'Eveux (Rhône) a délivré le 25 mars 2008 à M. et Mme A...un permis de construire une maison d'habitation sur un terrain d'une superficie de 2 500 m² situé en zone Ub d'habitat pavillonnaire de faible densité et comportant déjà une construction ; que, par un jugement du 17 septembre 2009, le tribunal administratif de Lyon a rejeté la demande d'annulation de ce permis présentée par la société Abos ; que, toutefois, par un arrêt du 28 juin 2011, contre lequel la commune d'Eveux se pourvoit en cassation, la cour administrative d'appel de Lyon a, sur appel de la société, annulé ce jugement ainsi que le permis de construire litigieux ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 123-19 du code de l'urbanisme, dans sa rédaction en vigueur à la date de la décision attaquée : " Les plans d'occupation des sols approuvés avant l'entrée en vigueur de la loi n° 2000-1208 du 13 décembre 2000 précitée ont les mêmes effets que les plans locaux d'urbanisme. Ils sont soumis au régime juridique des plans locaux d'urbanisme défini par les articles L. 123-1-1 à L. 123-18. Les dispositions de l'article   L. 123-1, dans leur rédaction antérieure à cette loi, leur demeurent.applicables " ; qu'aux termes de l'article L. 123-1 du même code, dans sa rédaction antérieure à la loi du 13 décembre 2000 relative à la solidarité et au renouvellement urbains : " Les plans d'occupation des sols fixent (...) les règles générales et les servitudes d'utilisation des sols (...) " ; qu'il résulte de ces dispositions que si le plan d'occupation des sols peut fixer au titre de l'article L. 123-1, dans sa rédaction antérieure à la loi du 13 décembre 2000, des règles relatives à la superficie minimale des terrains, elles ne permettent pas que de telles règles aient pour objet ou pour effet d'interdire au propriétaire d'édifier, si d'autres prescriptions du règlement du plan d'occupation des sols n'y font pas obstacle, plusieurs constructions sur une même unité foncière ;<br/>
<br/>
              3. Considérant qu'aux termes de l'article Ub5 du règlement du plan d'occupation des sols de la commune d'Eveux, auquel s'appliquent les dispositions de l'article L. 123-19 du code de l'urbanisme rappelées ci-dessus : " Le minimum de surface est fixé à 1800 m² (...). / Toute partie détachée d'un terrain qui a déjà été prise en compte pour la détermination des surfaces minimales définies ci-dessus ou qui constitue autour d'une construction existante une surface au moins égale à la surface minimale définie ci-dessus, deviendra inconstructible et ne pourra constituer en tout ou en partie une nouvelle surface minimale constructible " ; qu'il résulte de ce qui a été dit au point 2 que ces dispositions ont seulement pour objet de fixer la superficie minimale à compter de laquelle une unité foncière est constructible et ne sauraient par elles-mêmes interdire l'édification de plusieurs constructions sur un même terrain ; que, dès lors, en déduisant de l'article Ub5 du règlement du plan d'occupation des sols de la commune d'Eveux que la construction présente sur le terrain d'assiette du projet devait être prise en compte pour apprécier si le permis de construire litigieux avait pu être légalement accordé, au regard de ces dispositions, la cour administrative d'appel de Lyon a commis une erreur de droit ; que, par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la commune d'Eveux est fondée à demander l'annulation de l'arrêt du 28 juin 2011 ;<br/>
<br/>
              4. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de la commune d'Eveux, qui n'est pas, dans la présente instance, la partie perdante, la somme que demande la société Abos au titre des frais exposés par elle et non compris dans les dépens ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de la société Abos la somme de 3 000 euros à verser à la commune d'Eveux au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon du 28 juin 2011 est annulé.<br/>
<br/>
      Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon.<br/>
<br/>
Article 3 : La société Abos versera à la commune d'Eveux une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions de la société Abos présentées au titre des dispositions de l'article     L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à la commune d'Eveux, à la société Abos et à M. et MmeA.applicables<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
