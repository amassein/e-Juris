<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043100596</ID>
<ANCIEN_ID>JG_L_2021_02_000000439255</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/10/05/CETATEXT000043100596.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 04/02/2021, 439255, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-02-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>439255</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP DELAMARRE, JEHANNIN</AVOCATS>
<RAPPORTEUR>Mme Dominique Agniau-Canel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Domingo</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:439255.20210204</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B... A... a demandé au tribunal administratif de Toulon d'annuler les deux permis de construire tacitement délivrés le 10 février 2013 par le maire de Saint-Raphaël à la SCI Huilhout et à la SCI Cap Zen, ainsi que les décisions du maire en date du 30 novembre 2016 rejetant ses recours gracieux contre les certificats attestant de ces permis tacites. Par un jugement n°s 1700251, 1700252 du 3 janvier 2020, le tribunal administratif a rejeté ses demandes.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 3 mars et 24 avril 2020 au secrétariat du contentieux du Conseil d'Etat, Mme A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, d'annuler les deux permis de construire tacites et les décisions du maire qui ont rejeté ses recours gracieux ; <br/>
<br/>
              3°) de mettre à la charge de la commune de Saint-Raphaël, de la SCI Cap Zen et de la SCI Huilhout prises solidairement la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Par un mémoire en défense, enregistré le 6 août 2020, la SCI Huilhout et la SCI Cap Zen concluent au rejet du pourvoi et à ce que la somme de 5 000 euros soit mise à la charge de Mme A... au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Le pourvoi a été communiqué au maire de Saint-Raphaël, qui n'a pas produit de mémoire.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'urbanisme ; <br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Dominique Agniau-Canel, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Laurent Domingo, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de Mme B... A... et à la SCP Delamarre, Jéhannin, avocat de la SCI Cap Zen et de la SCI Huilhout ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. En vertu des dispositions de l'article R. 811-1-1 du code de justice administrative, issues du décret du 1er octobre 2013 relatif au contentieux de l'urbanisme, dans sa version applicable au litige, les tribunaux administratifs statuent en premier et dernier ressort sur les recours, introduits entre le 1er décembre 2013 et le 1er décembre 2018, dirigés contre " les permis de construire ou de démolir un bâtiment à usage principal d'habitation ou contre les permis d'aménager un lotissement lorsque le bâtiment ou le lotissement est implanté en tout ou partie sur le territoire d'une des communes mentionnées à l'article 232 du code général des impôts et son décret d'application ".<br/>
<br/>
              2. Ces dispositions, qui ont pour objectif, dans les zones où la tension entre l'offre et la demande de logements est particulièrement vive, de réduire le délai de traitement des recours pouvant retarder la réalisation d'opérations de construction de logements, dérogent aux dispositions du premier alinéa de l'article R. 811-1 du code de justice administrative qui prévoient que " toute partie présente dans une instance devant le tribunal administratif... peut interjeter appel contre toute décision juridictionnelle rendue dans cette instance " et doivent donc s'interpréter strictement. Si ces dispositions sont susceptibles de s'appliquer aux permis de construire autorisant la réalisation de travaux sur une construction existante, c'est à la condition que ces travaux aient pour objet la réalisation de logements supplémentaires. Il ne peut en aller différemment que lorsque les travaux sur une construction existante ont fait l'objet d'un permis de construire modificatif, lequel, pour l'application des dispositions de l'article R. 811-1-1 du code de justice administrative, obéit nécessairement aux mêmes règles de procédure contentieuse que le permis de construire initial auquel il se rattache.<br/>
<br/>
              3. Il ressort des pièces du dossier soumis au tribunal administratif de Toulon que les demandes formées par Mme A... devant ce tribunal, enregistrées le 30 janvier 2017,  tendent à l'annulation pour excès de pouvoir des décisions tacites du 10 février 2013 par lesquelles le maire de Saint-Raphaël, commune figurant sur la liste annexée au décret du 10 mai 2013 relatif au champ d'application de la taxe annuelle sur les logements vacants instituée par l'article 232 du code général des impôts, a délivré des permis de construire à la SCI Cap Zen et à la SCI Huilhout pour l'extension et la surélévation d'une villa. Ces travaux n'ayant pas pour objet la réalisation de logements supplémentaires, ils n'entrent pas dans le champ d'application des dispositions de l'article R. 811-1-1 du code de justice administrative.<br/>
<br/>
              4. Il résulte de ce qui précède que le jugement du tribunal administratif de Toulon du 3 janvier 2020 est susceptible d'appel. Il y a lieu, en conséquence, d'attribuer le jugement de la requête de Mme A... à la cour administrative d'appel de Marseille.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                                   ----------------<br/>
<br/>
Article 1er : Le jugement de la requête de Mme A... est attribué à la cour administrative d'appel de Marseille.<br/>
Article 2 : La présente décision sera notifiée à Mme B... A..., au maire de Saint-Raphaël, à la SCI Cap Zen, à la SCI Huilhout et au président de la cour administrative d'appel de Marseille.   <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
