<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039274930</ID>
<ANCIEN_ID>JG_L_2019_10_000000420036</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/27/49/CETATEXT000039274930.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 24/10/2019, 420036, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-10-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420036</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Sylvain Monteillet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:420036.20191024</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Par une requête et un nouveau mémoire, enregistrés les 20 avril et 20 juillet 2018 au secrétariat du contentieux du Conseil d'Etat, Mme B... A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la circulaire du 15 février 2018 relative au non-versement de la rémunération au titre du premier jour de congé de maladie des agents publics civils et militaires; <br/>
<br/>
              2°) d'enjoindre au ministre de l'action et des comptes publics d'abroger cette circulaire dans un délai de deux mois à compter de la décision à intervenir, sous astreinte de 1 000 euros par jour de retard;<br/>
<br/>
              3°) de mettre à la charge de l'Etat, d'une part, la somme de 100 euros au titre de l'article L. 761-1 du code de justice administrative et, d'autre part, les entiers frais et dépens de la procédure.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code civil local applicable aux départements du Bas-Rhin, du Haut-Rhin et de la Moselle;<br/>
              - le code du travail ;<br/>
              - la loi n° 2017-1837 du 30 décembre 2017 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Sylvain Monteillet, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Mme B... A... demande l'annulation pour excès de pouvoir de la circulaire du 15 février 2018 du ministre de l'action et des comptes publics et du secrétaire d'Etat auprès de ce ministre relative au non-versement de la rémunération au titre du premier jour de congé de maladie des agents publics civils et militaires. Cette circulaire commente l'application de l'article 115 de la loi du 30 décembre 2017 de finances pour 2018 qui a réintroduit un jour de carence pour le versement de la rémunération au titre du congé de maladie des agents publics civils et militaires. <br/>
<br/>
              2. En premier lieu, si Mme A... demande au Conseil d'Etat de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions figurant à l'article 115 de la loi de finances pour 2018 et à l'article L. 1226-23 du code du travail, cette question est irrecevable faute d'avoir été introduite par mémoire distinct dans les formes prescrites par l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel et par l'article R. 771-13 du code de justice administrative. <br/>
<br/>
              3. En deuxième lieu, aux termes de l'article 115 de la loi du 30 décembre 2017 de finances pour 2018 : " I. - Les agents publics civils et les militaires en congé de maladie et les salariés en congé de maladie pour lesquels l'indemnisation de ce congé n'est pas assurée par un régime obligatoire de sécurité sociale ou est assurée par un régime spécial de sécurité sociale mentionné à l'article L. 711-1 du code de la sécurité sociale ne bénéficient du maintien de leur traitement ou de leur rémunération, ou du versement de prestations en espèces par l'employeur qu'à compter du deuxième jour de ce congé. / II. - Le I du présent article ne s'applique pas : / 1° Lorsque la maladie provient de l'une des causes exceptionnelles prévues aux articles L. 27 et L. 35 du code des pensions civiles et militaires de retraite ; / 2° Au deuxième congé de maladie, lorsque la reprise du travail entre deux congés de maladie accordés au titre de la même cause n'a pas excédé 48 heures ; / 3° Au congé pour invalidité temporaire imputable au service, au congé du blessé prévu à l'article L. 4138-3-1 du code de la défense, aux congés pour accident de service ou accident du travail et maladie professionnelle, au congé de longue maladie, au congé de longue durée et au congé de grave maladie ; / 4° Aux congés de maladie accordés postérieurement à un premier congé de maladie au titre d'une même affection de longue durée, au sens de l'article L. 324-1 du code de la sécurité sociale, pour une période de trois ans à compter de ce premier congé de maladie ".<br/>
<br/>
              4. Il résulte de ces dispositions que le législateur a entendu déroger à l'ensemble des dispositions législatives ou réglementaires qui prévoient que les agents publics civils et militaires placés en congé de maladie ordinaire perçoivent leur rémunération au titre du premier jour de ce congé. Faute d'être assorti des précisions permettant d'en apprécier le bien-fondé, le moyen tiré de ce que la circulaire du 15 février 2018 fixerait des règles nouvelles portant notamment sur les situations de congé de maladie auxquelles le délai de carence est applicable et sur les éléments de rémunération faisant l'objet d'un non-versement et serait, en conséquence, entachée d'incompétence ne peut qu'être écarté. <br/>
<br/>
              5. En troisième lieu, à la date d'édiction de la circulaire du 15 février 2018, aucune disposition législative particulière en vigueur dans les départements du Bas-Rhin, du Haut-Rhin et de la Moselle ne régissait les conditions dans lesquelles les agents publics civils et militaires placés en congé de maladie percevaient leur rémunération. Par suite, le moyen tiré de ce que les auteurs de cette circulaire ont méconnu le particularisme du droit applicable aux agents publics civils et militaires en fonction dans ces trois départements et violé les dispositions de l'article 616 du code civil local est inopérant. <br/>
<br/>
              6. Il résulte de ce qui précède que Mme A... n'est pas fondée à demander l'annulation de la circulaire qu'elle attaque. Par voie de conséquence, doivent également être rejetées ses conclusions à fin d'injonction et d'astreinte et celles présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de Mme A... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à Mme B... A..., au ministre de l'action et des comptes publics et au secrétaire d'Etat auprès du ministre de l'action et des comptes publics. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
