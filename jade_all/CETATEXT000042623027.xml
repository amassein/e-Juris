<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042623027</ID>
<ANCIEN_ID>JG_L_2020_12_000000446395</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/62/30/CETATEXT000042623027.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 03/12/2020, 446395, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>446395</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:446395.20201203</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
              Par une requête, enregistrée le 12 novembre 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... B... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, d'ordonner la suspension de l'exécution du décret n° 2020-1310 du 29 octobre 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire en ce qu'il rend obligatoire le port du masque pour les enfants de plus de 6 ans dans les établissements d'enseignement. <br/>
<br/>
<br/>
              Il soutient que :<br/>
              - la condition d'urgence est remplie dès lors que le port du masque produit des effets dangereux pour la santé des enfants ;<br/>
              - il existe un doute sérieux quant à la légalité des dispositions contestées ; <br/>
              - les dispositions contestées méconnaissent le préambule et l'article 3 de la convention internationale relative aux droits de l'enfant dès lors qu'elles imposent à l'enfant de plus de 6 ans le port obligatoire du masque sans en préciser les conditions spécifiques, détaillées et adaptées à son jeune âge. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention internationale relative aux droits de l'enfant ;<br/>
              - le code de la santé publique ;<br/>
              - le décret n° 2020-1257 du 14 octobre 2020 ;<br/>
              - le décret n° 2020-1310 du 29 octobre 2020 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1.  Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision. ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              Sur le cadre juridique : <br/>
<br/>
              2. Aux termes de l'article L. 3131-12 du code de la santé publique, issu de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de Covid-19 : " L'état d'urgence sanitaire peut être déclaré sur tout ou partie du territoire métropolitain ainsi que du territoire des collectivités régies par les articles 73 et 74 de la Constitution et de la Nouvelle-Calédonie en cas de catastrophe sanitaire mettant en péril, par sa nature et sa gravité, la santé de la population ". L'article L. 3131-13 du même code précise que " L'état d'urgence sanitaire est déclaré par décret en conseil des ministres pris sur le rapport du ministre chargé de la santé. Ce décret motivé détermine la ou les circonscriptions territoriales à l'intérieur desquelles il entre en vigueur et reçoit application. Les données scientifiques disponibles sur la situation sanitaire qui ont motivé la décision sont rendues publiques. / (...) / La prorogation de l'état d'urgence sanitaire au-delà d'un mois ne peut être autorisée que par la loi, après avis du comité de scientifiques prévu à l'article L. 3131-19 ". Aux termes de l'article L. 3131-15 du même code : " Dans les circonscriptions territoriales où l'état d'urgence sanitaire est déclaré, le Premier ministre peut, par décret réglementaire pris sur le rapport du ministre chargé de la santé, aux seules fins de garantir la santé publique : / (...) 5° Ordonner la fermeture provisoire et réglementer l'ouverture, y compris les conditions d'accès et de présence, d'une ou plusieurs catégories d'établissements recevant du public. " Ces mesures doivent être " strictement proportionnées aux risques sanitaires encourus et appropriées aux circonstances de temps et de lieu. Il y est mis fin sans délai lorsqu'elles ne sont plus nécessaires. " <br/>
<br/>
              3. L'émergence d'un nouveau coronavirus, responsable de la maladie à coronavirus 2019 ou Covid-19 et particulièrement contagieux, a été qualifiée d'urgence de santé publique de portée internationale par l'Organisation mondiale de la santé le 30 janvier 2020, puis de pandémie le 11 mars 2020. La propagation du virus sur le territoire français a conduit le ministre chargé de la santé puis le Premier ministre à prendre, à compter du 4 mars 2020, des mesures de plus en plus strictes destinées à réduire les risques de contagion. Pour faire face à l'aggravation de l'épidémie, la loi du 23 mars 2020 a créé un régime d'état d'urgence sanitaire, défini aux articles L. 3131-12 à L. 3131-20 du code de la santé publique, et a déclaré l'état d'urgence sanitaire pour une durée de deux mois à compter du 24 mars 2020. La loi du 11 mai 2020 prorogeant l'état d'urgence sanitaire et complétant ces dispositions, a prorogé cet état d'urgence sanitaire jusqu'au 10 juillet 2020. L'évolution de la situation sanitaire a conduit à un assouplissement des mesures prises et la loi du 9 juillet 2020 a organisé un régime de sortie de cet état d'urgence.<br/>
<br/>
              4. Une nouvelle progression de l'épidémie au cours des mois de septembre et d'octobre, dont le rythme n'a cessé de s'accélérer au cours de cette période, a conduit le Président de la République à prendre le 14 octobre dernier, sur le fondement des articles L. 3131-12 et L. 3131-13 du code de la santé publique, un décret déclarant l'état d'urgence sanitaire à compter du 17 octobre sur l'ensemble du territoire national. Le 29 octobre 2020, le Premier ministre a pris, sur le fondement de l'article L. 3131-15 du code de la santé publique, le décret dont l'article 36 est ici contesté et prescrivant les mesures générales nécessaires pour faire face à l'épidémie de Covid-19 dans le cadre de l'état d'urgence sanitaire. L'article 1er de la loi du 14 novembre 2020 autorisant la prorogation de l'état d'urgence sanitaire et portant diverses mesures de gestion de la crise sanitaire a prorogé l'état d'urgence sanitaire jusqu'au 16 février 2021 inclus.<br/>
<br/>
              Sur la demande en référé : <br/>
<br/>
              5. M. B... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, d'ordonner la suspension du décret n° 2020-1257 du 29 octobre 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire en ce qu'il rend obligatoire le port du masque pour les enfants de plus de 6 ans dans les établissements d'enseignement. <br/>
<br/>
              6. Si le requérant soutient qu'il existe un doute sérieux quant à la légalité de ces dispositions dès lors qu'elles imposent le port du masque à tous sans prendre en compte la spécificité des enfants, et évoque en outre " les effets dangereux prouvés sur la santé des enfants du fait du port du masque ", ce moyen n'est assorti d'aucune précision permettant d'en apprécier le bien-fondé. <br/>
<br/>
              7. Il suit de là que, sans qu'il soit besoin d'examiner la condition d'urgence, la requête de M. B... doit être rejetée, selon la procédure prévue à l'article L. 522-3 du code de justice administrative. <br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. B... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. A... B.... <br/>
Copie en sera envoyée au Premier ministre et au ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
