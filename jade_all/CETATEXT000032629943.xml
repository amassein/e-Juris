<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032629943</ID>
<ANCIEN_ID>JG_L_2014_12_000000385932</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/62/99/CETATEXT000032629943.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 16/12/2014, 385932, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-12-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>385932</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>DELAMARRE</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2014:385932.20141216</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 24 novembre 2014 au secrétariat du contentieux du Conseil d'Etat, présentée par la commune d'Argenteuil, représentée par son maire ; la commune requérante demande au juge des référés du Conseil d'Etat :<br/>
<br/>
              1°) d'ordonner, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de la décision implicite par laquelle le Premier ministre a rejeté sa demande tendant à la dissolution de la communauté d'agglomération " Argenteuil-Bezons " ;<br/>
<br/>
              2°) d'enjoindre au Premier ministre de prononcer cette dissolution ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              elle soutient que :<br/>
<br/>
              - la condition d'urgence est remplie dès lors que le maintien de la commune d'Argenteuil dans la communauté d'agglomération " Argenteuil-Bezons " préjudicie de manière grave et immédiate à sa situation financière, l'empêche de rejoindre la communauté d'agglomération " Seine-Défense " pour l'année 2015 et par là de choisir le territoire au sein duquel elle rejoindra la métropole du Grand Paris au 1er janvier 2016 ;<br/>
              - il existe une présomption d'urgence dès lors que la décision contestée porte atteinte au principe de libre administration des collectivités territoriales ;<br/>
              - il existe un doute sérieux quant à la légalité de la décision contestée ;<br/>
              - le Premier ministre était en situation de compétence liée et aurait dû prononcer la dissolution de la communauté d'agglomération en application de l'article L. 5216-9 du code général des collectivités territoriales ;<br/>
              - même en l'absence de compétence liée du Premier ministre, la dissolution de la communauté d'agglomération aurait dû être prononcée ;<br/>
<br/>
<br/>
     Vu la décision dont la suspension de l'exécution est demandée ;<br/>
<br/>
           Vu la copie de la requête à fin d'annulation de cette décision ;<br/>
<br/>
           Vu le mémoire en défense, enregistré le 8 décembre 2014, présenté par le ministre de l'intérieur, qui conclut au rejet de la requête ;<br/>
<br/>
           il soutient que :<br/>
<br/>
           - la requête n'est pas recevable dès lors que la demande de dissolution de la communauté d'agglomération " Argenteuil-Bezons " n'a pas été effectuée dans les conditions prévues à l'article L. 5216-9 du code général des collectivités territoriales ;<br/>
           - la condition d'urgence n'est pas remplie, dès lors que le rattachement de la commune d'Argenteuil à la communauté d'agglomération " Seine-Défense " n'est pas possible en l'absence de continuité territoriale entre les deux collectivités ;<br/>
           - il n'existe pas de doute sérieux sur la légalité de la décision contestée ;<br/>
           - le Premier ministre n'est pas en situation de compétence liée pour prononcer la dissolution d'une communauté d'agglomération lorsque la dissolution demandée est de nature à méconnaître l'obligation légale de couverture intégrale du territoire par les établissements publics de coopération intercommunale à fiscalité propre ;<br/>
           - le maintien de la communauté d'agglomération " Argenteuil-Bezons " au 1er janvier 2015 ne fait pas obstacle à la demande d'intégration de la commune à la métropole du Grand Paris au 1er janvier 2016 ;<br/>
<br/>
<br/>
           Après avoir convoqué à une audience publique, d'une part, la commune d'Argenteuil, d'autre part, le Premier ministre et le ministre de l'intérieur ;<br/>
<br/>
           Vu le procès-verbal de l'audience publique du 10 décembre 2014 à 10 heures au cours de laquelle ont été entendus :<br/>
<br/>
           - Me Delamarre, avocat au Conseil d'Etat et à la Cour de cassation avocat de la commune d'Argenteuil ;<br/>
<br/>
           - les représentants de la commune d'Argenteuil ;<br/>
<br/>
           - les représentants du ministre de l'intérieur ;<br/>
<br/>
           et à l'issue de laquelle le juge des référés a prolongé l'instruction jusqu'au 11 décembre 2014 à 10 heures ;<br/>
<br/>
           Vu les observations complémentaires, enregistrées le 11 décembre 2014, présentées par la commune d'Argenteuil, qui persiste dans ses précédentes conclusions ;<br/>
<br/>
           Vu les observations complémentaires, enregistrées le 11 décembre 2014, présentées par le ministre de l'intérieur, qui persiste dans ses précédentes conclusions ;<br/>
<br/>
           Vu les autres pièces du dossier ;<br/>
<br/>
           Vu la Constitution ;<br/>
<br/>
           Vu le code général des collectivités territoriales ;<br/>
<br/>
           Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; qu'il résulte de ces dispositions que le prononcé de la suspension d'un acte administratif est subordonné notamment à une condition d'urgence ; que l'urgence justifie la suspension de l'exécution d'un acte administratif lorsque celui-ci porte atteinte de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ; qu'il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte contesté sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue ; que l'urgence doit être appréciée objectivement et compte tenu de l'ensemble des circonstances de l'affaire ;<br/>
<br/>
              2. Considérant que la commune d'Argenteuil, membre de la communauté d'agglomération " Argenteuil-Bezons " depuis le 25 décembre 2005, a, par une délibération du 18 juillet 2014 de son conseil municipal, approuvé l'intégration de la commune d'Argenteuil à la métropole du Grand Paris ; que  la commune requérante, en continuité avec au moins une commune du département des Hauts de Seine, sera intégrée de droit à la métropole du Grand Paris au 1er janvier 2016 en application du 4° de l'article L. 5219-1 du code général des collectivités territoriales ; que, par la même délibération, le conseil municipal de la commune d'Argenteuil a demandé la dissolution de la communauté d'agglomération " Argenteuil-Bezons " au 1er janvier 2015 en application de l'article L. 5216-9 du code général des collectivités territoriales, et que la commune d'Argenteuil soit intégrée en 2015 à la communauté d'agglomération " Seine-Défense ", qui rejoindra elle-même la métropole du Grand Paris au 1er janvier 2016 ; que, par une délibération du 16 octobre 2014, le même conseil municipal a demandé l'adhésion de la commune requérante à la communauté d'agglomération " Seine-Défense " à compter du 1er janvier 2015 ;<br/>
<br/>
              3. Considérant que pour justifier de l'urgence qui s'attacherait à la suspension de l'exécution de la décision contestée, la commune d'Argenteuil soutient que son maintien au sein de la communauté d'agglomération " Argenteuil-Bezons " au-delà du 31 décembre 2014 porterait atteinte au principe constitutionnel de libre administration des collectivités territoriale, préjudicierait gravement et immédiatement à ses intérêts financiers et empêcherait son adhésion à la communauté d'agglomération " Seine-Défense " à compter du 1er janvier 2015, la privant par là même de la liberté de choisir le territoire au sein duquel elle pourra être intégrée à la métropole du Grand Paris le 1er janvier 2016, en application  de l'article L. 5219-2 du code général des collectivités territoriales, selon lequel : " Les communes appartenant à un même établissement public de coopération intercommunale à fiscalité propre existant au 31 décembre 2014 ne peuvent appartenir à des territoires distincts " ;<br/>
<br/>
              4. Considérant cependant, en premier lieu, que, quand bien même la  situation dégradée des finances de la commune d'Argenteuil serait imputable à la gestion de la communauté d'agglomération " Argenteuil-Bezons " au cours des années précédentes, le préjudice financier qui résulterait du maintien de la commune d'Argenteuil au sein de cette communauté d'agglomération pour une année supplémentaire, du 1er janvier 2015 au 31 décembre 2015, apparaît hypothétique ; qu'en deuxième lieu, l'adhésion de la commune d'Argenteuil à la communauté d'agglomération " Seine-Défense " au 1er janvier 2015 serait en tout état de cause incertaine car subordonnée à l'adhésion préalable des communes d'Asnières-sur-Seine et de Colombes à cette communauté d'agglomération pour permettre l'existence d'une continuité territoriale entre celle-ci et la commune d'Argenteuil, conformément à l'article L. 5216-1 du code général des collectivités territoriales ; qu'en troisième lieu, l'absence d'adhésion de la commune d'Argenteuil à la communauté d'agglomération " Seine-Défense ", par elle-même, ne priverait pas la commune requérante, lors de son adhésion à la métropole du Grand Paris, de la possibilité d'intégrer le territoire comprenant notamment les communes de la communauté d'agglomération " Seine-Défense " et les communes d'Asnières-sur-Seine et de Colombes ; qu'en quatrième lieu, la dissolution de la communauté d'agglomération " Argenteuil-Bezons " serait susceptible d'aboutir à ce que les communes d'Argenteuil et de Bezons ne soient rattachées à aucun établissement de coopération intercommunale au cours de l'année 2015, ce qui irait à l'encontre du but d'intérêt général de renforcement de la carte de l'intercommunalité ; qu'enfin et au surplus, la commune requérante, qui ne peut se prévaloir de l'atteinte que le refus litigieux porterait à la libre administration des collectivités territoriales pour justifier l'urgence, sera intégrée à la métropole du Grand Paris dès le 1er janvier 2016, ce qui s'accompagnera de la dissolution de la communauté d'agglomération " Argenteuil Bezons " ; qu'ainsi, il n'apparait pas, en l'état de l'instruction, que ce refus du Premier Ministre aurait des effets de la nature de ceux caractérisant une situation d'urgence au sens de l'article L. 521-1 du code de justice administrative ;<br/>
<br/>
              5. Considérant qu'il résulte de tout ce qui précède, sans qu'il soit besoin de statuer sur la fin de non recevoir opposée par le ministre de l'intérieur, ni de se prononcer sur les moyens de légalité soulevés par la commune d'Argenteuil, que ses conclusions à fin de suspension de l'exécution de la décision contestée et à fins d'injonction ne peuvent être accueillies ; que, par voie de conséquence, ses conclusions présentées sur le fondement de l'article L. 761-1 du code de justice administrative doivent également être rejetées ;<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de la commune d'Argenteuil est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à la commune d'Argenteuil, au Premier ministre et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
