<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042175688</ID>
<ANCIEN_ID>JG_L_2020_07_000000430934</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/17/56/CETATEXT000042175688.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 29/07/2020, 430934, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-07-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>430934</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>Mme Bénédicte Fauvarque-Cosson</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie Sirinelli</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:430934.20200729</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme C... B... née A... a demandé au tribunal administratif de Marseille, d'une part, d'annuler la décision du 29 mai 2018 par laquelle le président du conseil départemental des Bouches-du-Rhône, rejetant son recours administratif contre la décision du 27 mars 2018 de la caisse d'allocations familiales des Bouches-du-Rhône, a mis à sa charge la récupération d'un indu de revenu de solidarité active d'un montant de 18 074,30 euros au titre de la période du 1er janvier 2016 au 31 mars 2018 et, d'autre part, de lui accorder la décharge ou, subsidiairement, la remise gracieuse de cet indu. Par une ordonnance n° 1806405 du 11 décembre 2018, la présidente du tribunal administratif de Marseille a rejeté sa demande. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 21 mai  et 16 août 2019 au secrétariat du contentieux du Conseil d'Etat, Mme B... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge du département des Bouches-du-Rhône la somme de 3 000 euros à verser à la SCP Rousseau, Tapie, son avocat, en application des dispositions des articles 37 de la loi du 10 juillet 1991 et L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'action sociale et des familles ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme D... E..., conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Marie Sirinelli, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de Mme B..., et à la SCP Lyon-Caen, Thiriez, avocat du département des Bouches-du-Rhône ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que, le 27 mars 2018, la caisse d'allocations familiales des Bouches-du-Rhône a décidé de récupérer auprès de Mme B..., bénéficiaire du revenu de solidarité active, un indu, s'élevant à 17 245,76 euros, d'allocations versées au titre de cette prestation pour la période de mars 2016 à février 2018. Le président du conseil départemental des Bouches-du-Rhône a rejeté le recours administratif formé par Mme B..., par une décision du 29 mai 2018, qui a porté l'indu à 18 074,30 euros pour la période de janvier 2016 à mars 2018. Mme B... se pourvoit en cassation contre l'ordonnance du 11 décembre 2018 par laquelle la présidente du tribunal administratif de Marseille a rejeté sa demande d'annulation de cette décision.<br/>
<br/>
              2. Aux termes du premier alinéa de l'article L. 262-47 du code de l'action sociale et des familles : " Toute réclamation dirigée contre une décision relative au revenu de solidarité active fait l'objet, préalablement à l'exercice d'un recours contentieux, d'un recours administratif auprès du président du conseil départemental. Ce recours est, dans les conditions et limites prévues par la convention mentionnée à l'article L. 262-25, soumis pour avis à la commission de recours amiable qui connaît des réclamations relevant de l'article L. 142-1 du code de la sécurité sociale (...) ", laquelle est composée et constituée au sein du conseil d'administration de la caisse d'allocations familiales. Aux termes du I de l'article L. 262-25 du code de l'action sociale et des familles : " Une convention est conclue entre le département et chacun des organismes mentionnés à l'article L. 262-16. / Cette convention précise en particulier : / 1° Les conditions dans lesquelles le revenu de solidarité active est servi et contrôlé ; / 2° Les modalités d'échange des données entre les parties ; / 3° La liste et les modalités d'exercice et de contrôle des compétences déléguées, le cas échéant, par le département aux organismes mentionnés à l'article L. 262-16 (...) ". Aux termes de l'article R. 262-60 de ce code : " La convention prévue à l'article L. 262-25 comporte des dispositions générales relatives à : / (...) 4° Les conditions et limites dans lesquelles la commission de recours amiable de ces organismes rend un avis sur les recours administratifs adressés au président du conseil départemental ; ces stipulations portent notamment sur l'objet et le montant des litiges dont la commission est saisie et les conditions financières de cette intervention (...) ". Enfin, aux termes de l'article R. 262-90 du même code : " Lorsqu'elle est saisie, la commission de recours amiable se prononce dans un délai d'un mois à compter de la date de saisine. A réception de l'avis, le président du conseil départemental statue, sous un mois, sur le recours administratif qui lui a été adressé. / Si elle ne s'est pas prononcée au terme du délai mentionné au précédent alinéa, son avis est réputé rendu et le président du conseil départemental statue, sous un mois, sur le recours administratif qui lui a été adressé. (...) ". <br/>
<br/>
              3. Si les actes administratifs doivent être pris selon les formes et conformément aux procédures prévues par les lois et règlements, un vice affectant le déroulement d'une procédure administrative préalable, suivie à titre obligatoire ou facultatif, n'est de nature à entacher d'illégalité la décision prise que s'il ressort des pièces du dossier qu'il a été susceptible d'exercer, en l'espèce, une influence sur le sens de la décision prise ou qu'il a privé les intéressés d'une garantie. L'application de ce principe n'est pas exclue en cas d'omission d'une procédure obligatoire, à condition qu'une telle omission n'ait pas pour effet d'affecter la compétence de l'auteur de l'acte.<br/>
<br/>
              4. Dans ce cadre, il appartient au tribunal administratif, saisi d'un moyen tiré du défaut de consultation de la commission de recours amiable de l'organisme chargé du service du revenu de solidarité active, de s'assurer du caractère obligatoire de cette consultation dans l'hypothèse en litige, en vertu des clauses réglementaires de la convention conclue entre le département et l'organisme. En revanche, la circonstance que le législateur ait entendu permettre à chaque département, agissant par voie de convention avec cet organisme, de déterminer les hypothèses dans lesquelles les réclamations dirigées contre des décisions relatives au revenu de solidarité active sont soumises pour avis à sa commission de recours amiable n'a pas pour effet de retirer à la consultation de cette commission, eu égard à sa nature et à sa composition, le caractère d'une garantie apportée, lorsqu'elle est prévue, au bénéficiaire du revenu de solidarité active. <br/>
<br/>
              5. Dans ces conditions, en se bornant à juger, pour écarter le moyen soulevé par Mme B..., tiré du défaut de consultation de la commission de recours amiable de la caisse d'allocations familiales des Bouches-du-Rhône sur son recours administratif, que ni les dispositions de l'article L. 262-47 du code de l'action sociale et des familles, ni celles de l'article R. 262-90 du même code n'imposaient la consultation de cette commission, sans avoir recherché si les clauses réglementaires de la convention conclue entre le département et la caisse dispensaient de cette consultation, la présidente du tribunal administratif de Marseille a commis une erreur de droit. <br/>
<br/>
              6. Il résulte de ce qui précède que l'ordonnance de la présidente du tribunal administratif de Marseille doit être annulée. Le moyen retenu suffisant à entraîner cette annulation, il n'est pas nécessaire d'examiner les autres moyens du pourvoi de Mme B....<br/>
<br/>
              7. Mme B... ayant obtenu le bénéfice de l'aide juridictionnelle, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Rousseau, Tapie, son avocat, renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge du département des Bouches-du-Rhône une somme de 1 500 euros à verser à cette SCP.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'ordonnance de la présidente du tribunal administratif de Marseille du 11 décembre 2018 est annulée. <br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Marseille. <br/>
Article 3 : Le département de des Bouches-du-Rhône versera à la SCP Rousseau, Tapie, avocat de Mme B..., une somme de 1 500 euros en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette SCP renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : La présente décision sera notifiée à Mme C... B... née A... et au département des Bouches-du-Rhône.<br/>
Copie en sera adressée à la caisse d'allocations familiales des Bouches-du-Rhône. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
