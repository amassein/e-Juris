<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037220737</ID>
<ANCIEN_ID>JG_L_2018_07_000000416888</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/22/07/CETATEXT000037220737.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 18/07/2018, 416888, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-07-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>416888</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOUTET-HOURDEAUX</AVOCATS>
<RAPPORTEUR>M. Frédéric Pacoud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:416888.20180718</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              M. B...a demandé au juge des référés du tribunal administratif de Limoges de condamner l'Etat à lui verser, sur le fondement de l'article R. 541-1 du code de justice administrative, une provision d'un montant de 6 000 euros, assorti des intérêts, en réparation du préjudice qu'il estime avoir subi en raison de l'absence de versement de l'allocation temporaire d'attente pour la période comprise entre mars 2012 et septembre 2014. Par une ordonnance n° 1700063 du 29 mars 2017, le juge des référés du tribunal administratif de Limoges a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 17BX01891 du 19 décembre 2017, enregistré le 27 décembre 2017 au secrétariat du contentieux du Conseil d'Etat, la cour administrative d'appel de Bordeaux a transmis au Conseil d'Etat le pourvoi, enregistré le 15 juin 2017 au greffe de cette cour, présenté par M.A.... Par ce pourvoi et par un nouveau mémoire, enregistré le 24 novembre 2017 au greffe de cette cour, M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance du juge des référés du tribunal administratif de Limoges ;<br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande de provision ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 320 euros à verser à son avocat au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu <br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le décret n° 2016-1481 du 2 novembre 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Pacoud, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boutet, Hourdeaux, avocat de M.A....<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article R. 541-1 du code de justice administrative : " Le juge des référés peut, même en l'absence d'une demande au fond, accorder une provision au créancier qui l'a saisi lorsque l'existence de l'obligation n'est pas sérieusement contestable (...) ".<br/>
<br/>
              2. Il ressort des pièces du dossier soumis au juge des référés que, sur le fondement de ces dispositions, M. A...a demandé que l'Etat soit condamné à lui verser une provision en réparation des préjudices qu'il estime avoir subis en raison de l'absence de versement, entre mars 2012 et septembre 2014, de l'allocation temporaire d'attente, dont il a bénéficié à compter du 19 septembre 2014 seulement, après l'enregistrement de sa demande d'asile le 26 août 2014. Par une ordonnance du 29 mars 2017, contre laquelle M. A...se pourvoit en cassation, le juge des référés du tribunal administratif de Limoges a rejeté sa demande comme manifestement irrecevable.<br/>
<br/>
              3. Aux termes du troisième alinéa de l'article R. 414-3 du code de justice administrative, issu du décret du 2 novembre 2016 relatif à l'utilisation des téléprocédures devant le Conseil d'Etat, les cours administratives d'appel et les tribunaux administratifs, applicable aux mémoires et pièces produits à compter du 1er janvier 2017 : " Lorsque le requérant transmet, à l'appui de sa requête, un fichier unique comprenant plusieurs pièces, chacune d'entre elles doit être répertoriée par un signet la désignant conformément à l'inventaire mentionné ci-dessus. S'il transmet un fichier par pièce, l'intitulé de chacun d'entre eux doit être conforme à cet inventaire. Le respect de ces obligations est prescrit à peine d'irrecevabilité de la requête ".<br/>
<br/>
              4. Il ressort des pièces de la procédure devant le juge des référés du tribunal administratif de Limoges, notamment de l'accusé de réception d'un dépôt de requête émis le 13 janvier 2017 à 17 h 22 par l'application informatique mentionnée à l'article R. 414-1 du code de justice administrative, que M. A...a transmis, à l'appui de sa requête, neuf pièces enregistrées chacune dans un fichier distinct. En jugeant que le requérant avait " joint à sa requête (...) un fichier comportant neuf pièces " et que ces neuf pièces étaient " comprises dans un seul fichier transmis à l'appui de la requête ", le juge des référés a dénaturé les pièces du dossier. Dès lors, l'obligation, prévue par le troisième alinéa de l'article R. 414-3 du code de justice administrative, de répertorier chacune des pièces contenues dans un même fichier par un signet la désignant ne pouvait trouver à s'appliquer et le juge des référés a fait une inexacte application de ces dispositions en rejetant la requête comme irrecevable au motif que les pièces jointes n'étaient pas répertoriées par de tels signets.<br/>
<br/>
              5. Il résulte de ce qui précède que M. A...est fondé à demander l'annulation de l'ordonnance qu'il attaque. Le moyen retenu suffisant à entraîner cette annulation, il n'est pas nécessaire d'examiner les autres moyens de son pourvoi.<br/>
<br/>
              6. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme à verser à l'avocat de M. A...sur le fondement des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Limoges du 29 mars 2017 est annulée.<br/>
Article 2 : L'affaire est renvoyée au juge des référés du tribunal administratif de Limoges.<br/>
Article 3 : Les conclusions de M. A...présentées au titre du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à M.B..., à Pôle emploi et au ministre d'Etat, ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
