<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000008223010</ID>
<ANCIEN_ID>JG_L_2006_07_000000271055</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/08/22/30/CETATEXT000008223010.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème / 3ème SSR, 13/07/2006, 271055</TITRE>
<DATE_DEC>2006-07-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>271055</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème / 3ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PEIGNOT, GARREAU</AVOCATS>
<RAPPORTEUR>M. Jean  Courtial</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olléon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2006:271055.20060713</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 10 août  et 12 décembre 2004 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B... A..., demeurant... ; M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt du 3 juin 2004 par lequel la cour administrative d'appel de Paris a rejeté l'appel qu'il a formé contre le jugement du 26 avril 2000 du tribunal administratif de Paris rejetant ses demandes tendant à la décharge des compléments d'impôt sur le revenu auxquels il a été assujetti au titre des années 1987 à 1989 et du rappel de taxe sur la valeur ajoutée mis en recouvrement à son nom au titre de la période du 1er janvier 1987 au 31 décembre 1989, à concurrence des montants de droits et pénalités maintenus à sa charge après un dégrèvement partiel ;<br/>
<br/>
              2°) statuant au fond, d'annuler le jugement du 26 avril 2000 du tribunal administratif de Paris et de lui accorder la décharge des impositions contestées ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Patrick Quinqueton, Maître des Requêtes,<br/>
<br/>
              - les observations de la SCP Peignot, Garreau, avocat de M.A..., <br/>
<br/>
              - les conclusions de M. Laurent Olléon, Commissaire du gouvernement ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que l'administration fiscale, au vu de déclarations, émanant de tiers, d'honoraires versés à M. A...en rémunération d'une activité de conseil en édition, a évalué d'office les bénéfices non commerciaux que ce dernier a tirés de cette activité au titre des années 1987, 1988 et 1989 et l'a taxé d'office à la taxe sur la valeur ajoutée au titre de la période correspondant à ces années ; que M. A...se pourvoit en cassation contre l'arrêt du 3 juin 2004 par lequel la cour administrative d'appel de Paris, confirmant un jugement du tribunal administratif de Paris du 26 avril 2000, a rejeté sa requête tendant à la décharge des compléments d'impôt sur le revenu et des rappels de taxe sur la valeur ajoutée liés à l'activité de conseil en édition susmentionnée ;<br/>
<br/>
              Considérant, en premier lieu, que la cour administrative d'appel a adopté les motifs du jugement du tribunal administratif pour écarter des moyens articulés, exactement dans les mêmes termes, dans les écritures de première instance et d'appel ; que le requérant soutient qu'en s'appropriant ainsi les motifs retenus par les premiers juges, la cour a insuffisamment motivé son arrêt ; que toutefois, en retenant ces motifs, la cour a répondu aux moyens d'appel du requérant ; que son arrêt est, par suite, suffisamment motivé ;<br/>
<br/>
              Considérant, en deuxième lieu, qu'aux termes de l'article L. 73 du livre des procédures fiscales, dans sa rédaction applicable en l'espèce : "Peuvent être évalués d'office : (...) / 2° Le bénéfice imposable des contribuables qui perçoivent des revenus non commerciaux ou des revenus assimilés, quel que soit leur régime d'imposition, lorsque la déclaration annuelle prévue à l'article 97... du code général des impôts n'a pas été déposée dans le délai légal ; (...) /  Les dispositions de l'article L. 68 sont applicables dans les cas d'évaluation d'office prévus aux 1° et 2°" ; qu'en vertu des dispositions de l'article L. 68 du même livre, dans sa rédaction applicable en l'espèce, la procédure d'imposition d'office "n'est applicable que si le contribuable n'a pas régularisé sa situation dans les trente jours de la notification d'une première mise en demeure" ; que l'article 97 du code général des impôts  dispose : "Les contribuables soumis obligatoirement ou sur option au régime de la déclaration contrôlée sont tenus de souscrire chaque année, dans des conditions et délais prévus aux articles 172 et 175, une déclaration..." ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que l'administration a adressé le 19 juillet 1990 à M. A...des mises en demeure l'invitant à déposer pour chacune des années en cause, dans un délai de trente jours, la déclaration prévue à l'article 97 précité ; que M. A...n'a pas déféré à cette invitation ; que l'administration n'était pas tenue de préciser dans les mises en demeure les motifs de droit ou de fait pour lesquels elle estimait que M. A...était titulaire de revenus non commerciaux et tenu aux obligations déclaratives fixées par les dispositions de l'article 97 du code général des impôts ; que, par suite, la cour administrative d'appel n'a pas commis d'erreur de droit en jugeant qu'une procédure d'évaluation d'office de ses revenus non commerciaux avait été régulièrement engagée à l'égard du requérant ;<br/>
<br/>
              Considérant, enfin, qu'en jugeant, par adoption des motifs retenus par les premiers juges, que les notifications adressées à M. A...répondaient aux exigences de l'article L. 76 du livre des procédures fiscales relatives à l'information du contribuable sur la méthode de détermination des bases d'imposition dès lors qu'elles comportaient le nom des tiers ayant déclaré les honoraires versés à ce dernier et le montant de ces honoraires, la cour a porté sur les mentions de ces notifications une appréciation souveraine exempte d'erreur de droit ;<br/>
<br/>
              Sur les conclusions de M. A...tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de l'Etat le versement à M. A...de la somme qu'il demande au titre des frais exposés par lui et non compris dans les dépens ;<br/>
<br/>
<br/>
              			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : La requête de M. A...est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. B...A...et au ministre de l'économie, des finances et de l'industrie.<br/>
''<br/>
''<br/>
''<br/>
''<br/>
N° 271055- 4 -<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-08-02-02-005-03-01 PROCÉDURE. VOIES DE RECOURS. CASSATION. CONTRÔLE DU JUGE DE CASSATION. RÉGULARITÉ EXTERNE. FORME. MOTIVATION. - ADOPTION PAR LES JUGES D'APPEL DES MOTIFS RETENUS PAR LES PREMIERS JUGES.
</SCT>
<ANA ID="9A"> 54-08-02-02-005-03-01 Le Conseil d'Etat exerce un contrôle sur le caractère suffisant, au regard des moyens d'appel, de la motivation d'un arrêt adoptant les motifs du jugement du tribunal administratif pour écarter des moyens articulés, exactement dans les mêmes termes, dans les écritures de première instance et d'appel. Motivation suffisante en l'espèce.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
