<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032790099</ID>
<ANCIEN_ID>JG_L_2016_06_000000384398</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/79/00/CETATEXT000032790099.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 27/06/2016, 384398, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-06-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384398</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, VEXLIARD, POUPOT ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>Mme Florence Marguerite</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean Lessi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:384398.20160627</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une décision du 27 avril 2015, le Conseil d'Etat, statuant au contentieux, a prononcé l'admission des conclusions du pourvoi de M. B...A...dirigées contre le jugement du tribunal administratif de Toulouse n° 1102195 du 25 juin 2014 en tant seulement que ce jugement a omis de se prononcer sur les conclusions du requérant dirigées contre la décision implicite de rejet de son recours administratif du 12 juillet 2010.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
- le code de l'action sociale et des familles ;<br/>
- la loi n° 91-647 du 10 juillet 1991 ;<br/>
- le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Florence Marguerite, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Jean Lessi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Vexliard, Poupot, avocat de M.A..., et à la SCP Lyon-Caen, Thiriez, avocat du département de Tarn-et-Garonne ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 262-47 du code de l'action sociale et des familles, dans sa rédaction applicable au litige : " Toute réclamation dirigée contre une décision relative au revenu de solidarité active fait l'objet, préalablement à l'exercice d'un recours contentieux, d'un recours administratif auprès du président du conseil général (...) ". Aux termes du premier alinéa de l'article R. 262-88 du même code : " Le recours administratif préalable mentionné à l'article L. 262-47 est adressé par le bénéficiaire au président du conseil général dans un délai de deux mois à compter de la notification de la décision contestée (...) ". L'institution par ces dispositions d'un recours administratif, préalable obligatoire à la saisine du juge, a pour effet de laisser à l'autorité compétente pour en connaître le soin d'arrêter définitivement la position de l'administration. Il s'ensuit que la décision prise à la suite du recours se substitue en principe à la décision initiale, et qu'elle est seule susceptible d'être déférée au juge.<br/>
<br/>
               2. Toutefois, s'il est saisi de conclusions tendant à l'annulation d'une décision qui ne peut donner lieu à un recours devant le juge qu'après l'exercice d'un recours administratif préalable et si le requérant indique, de sa propre initiative ou le cas échéant à la demande du juge, avoir exercé ce recours et, le cas échéant après que le juge l'y ait invité, produit la preuve de l'exercice de ce recours ainsi que, s'il en a été pris une, la décision à laquelle il a donné lieu, le juge doit regarder les conclusions dirigées formellement contre la décision initiale comme tendant à l'annulation de la décision, née de l'exercice du recours, qui s'y est substituée.<br/>
<br/>
              3. Il ressort des pièces du dossier soumis au juge du fond que M. A...a, par une requête enregistrée le 12 mai 2011, demandé au tribunal administratif de Toulouse, notamment, d'annuler la décision de la caisse d'allocations familiales de Tarn-et-Garonne du 22 juin 2010 de récupération d'un indu de 3 746,60 euros, en tant que cet indu comprenait des sommes perçues au titre du revenu de solidarité active. Il indiquait toutefois avoir formé, par courrier du 12 juillet 2010, un recours administratif préalable auprès du président du conseil général, en produisant une copie de ce courrier et de la preuve de sa réception par le département le 13 juillet 2010. Au surplus, après que le président de la formation de jugement eut informé les parties, en application de l'article R. 611-7 du code de justice administrative, que le jugement à intervenir était susceptible d'être fondée sur le moyen, relevé d'office, tiré de l'irrecevabilité des conclusions dirigées contre la décision du 22 juin 2010, M.A..., par un mémoire enregistré le 30 mai 2014, a indiqué que ses conclusions devaient être regardées comme dirigées contre la décision implicite par laquelle le président du conseil général avait rejeté son recours préalable. <br/>
<br/>
              4. Il suit de là qu'en se bornant à rejeter comme irrecevables les conclusions de M. A...dirigées contre la décision du 22 juin 2010, au motif que la décision rejetant son recours gracieux du 12 juillet suivant s'y était substituée, au lieu de regarder les conclusions comme dirigées contre cette seconde décision, le tribunal administratif de Toulouse a commis une erreur de droit.<br/>
<br/>
              5. Il résulte de ce qui précède que M. A...est fondé à demander l'annulation du jugement du tribunal administratif de Toulouse du 25 juin 2014 en tant que celui-ci ne s'est pas prononcé sur ses conclusions dirigées contre la décision implicite de rejet du recours administratif qu'il avait formé le 12 juillet 2010. Le moyen d'erreur de droit retenu suffisant à entraîner cette annulation, il n'est pas nécessaire d'examiner les autres moyens du pourvoi soulevés à l'appui des mêmes conclusions.<br/>
<br/>
              6. M. A...a obtenu le bénéfice de l'aide juridictionnelle. Par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce et sous réserve que la SCP Matuchansky, Vexliard, Poupot, avocat de M.A..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge du département de Tarn-et-Garonne une somme de 1 000 euros à verser à cette SCP. En revanche, les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. A...qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Toulouse du 25 juin 2014 est annulé en tant qu'il ne s'est pas prononcé sur les conclusions de M. A...dirigées contre la décision implicite de rejet du recours administratif formé par celui-ci le 12 juillet 2010.<br/>
Article 2 : L'affaire est renvoyée, dans la mesure de la cassation prononcée, au tribunal administratif de Toulouse.<br/>
Article 3 : Le département de Tarn-et-Garonne versera à la SCP Matuchansky, Vexliard, Poupot, avocat de M.A..., une somme de 1 000 euros en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : Les conclusions du département de Tarn-et-Garonne présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à M. B...A...et au département de Tarn-et-Garonne.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
