<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029225101</ID>
<ANCIEN_ID>JG_L_2014_07_000000360227</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/22/51/CETATEXT000029225101.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère / 6ème SSR, 11/07/2014, 360227</TITRE>
<DATE_DEC>2014-07-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>360227</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère / 6ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP TIFFREAU, MARLANGE, DE LA BURGADE</AVOCATS>
<RAPPORTEUR>M. Philippe Combettes</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:360227.20140711</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 14 juin et 13 septembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour le fonds national d'assurance formation de l'industrie hôtelière (F.A.F.I.H.), dont le siège est 3, rue de la Ville l'Evêque à Paris (75008), représenté par son président ; le fonds national d'assurance formation de l'industrie hôtelière demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le deuxième alinéa de l'article 1er de l'arrêté du ministre du travail, de l'emploi et de la santé du 6 avril 2012 portant extension d'un accord professionnel relatif aux objectifs, priorités et moyens de la formation professionnelle dans l'hôtellerie, la restauration et les activités de loisirs ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code du travail ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Philippe Combettes, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Tiffreau, Marlange, de la Burgade, avocat du fonds national d'assurance formation de l'industrie hôtelière (F.A.F.I.H.) ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que le fonds national d'assurance formation de l'industrie hôtelière demande l'annulation pour excès de pouvoir du deuxième alinéa de l'article 1er de l'arrêté du 6 avril 2012 du ministre chargé du travail portant extension de l' accord professionnel relatif aux objectifs, priorités et moyens de la formation professionnelle dans l'hôtellerie, la restauration et les activités de loisirs conclu le 10 janvier 2011, en ce qu'il exclut les termes " aux activités de loisirs " qui figurent dans le titre de l'accord, au motif " qu'ils constituent une ambigüité de rédaction susceptible de créer un chevauchement de champ avec la convention collective nationale des espaces de loisirs, d'attractions et culturels du 5 janvier 1994, étendue par arrêté du 25 juillet 1994 "  ;<br/>
<br/>
              Sur les interventions du syndicat professionnel Casinos de France, de la fédération nationale de l'hôtellerie restauration sport loisirs et casinos CFE-CGC, INOVA, de la confédération des professionnels indépendants de l'hôtellerie et de la fédération autonome générale de l'industrie hôtelière touristique :<br/>
<br/>
              2. Considérant que le syndicat professionnel Casinos de France, la fédération nationale de l'hôtellerie restauration sport loisirs et casinos CFE-CGC, INOVA, la confédération des professionnels indépendants de l'hôtellerie et la fédération autonome générale de l'industrie hôtelière touristique ont intérêt à l'annulation de la décision attaquée ; qu'ainsi leurs interventions sont recevables ;<br/>
<br/>
              Sur la requête du fonds national d'assurance formation de l'industrie hôtelière :<br/>
<br/>
              3. Considérant, en premier lieu, qu'aux termes de l'article D. 2261-3 du code du travail : " Lorsqu'un arrêté d'extension ou d'élargissement est envisagé, il est précédé de la publication au Journal officiel de la République française d'un avis. Cet avis invite les organisations et personnes intéressées à faire connaître leurs observations (...) " ; qu'il ne résulte toutefois d'aucune disposition que le ministre chargé du travail serait tenu de mentionner dans l'arrêté d'extension le sens et les motifs de telles observations ou l'identité de leurs auteurs ; que le moyen tiré de ce que la procédure suivie aurait été irrégulière sur ce point ne peut, dès lors, qu'être écarté ;<br/>
<br/>
              4. Considérant, en deuxième lieu, que l'article L. 2261-15 du code du travail prévoit qu'un arrêté d'extension doit être précédé de l'avis motivé de la commission nationale de la négociation collective ; qu'il ne résulte toutefois d'aucune disposition que le ministre chargé du travail devrait reprendre dans son arrêté d'extension d'une convention ou d'un accord collectif les motifs de l'avis rendu par cette commission ; que, par suite, le moyen tiré de l'irrégularité qui résulterait de l'absence d'une telle mention ne peut qu'être écarté ;<br/>
<br/>
              5. Considérant, en troisième lieu, qu'en vertu de l'article L. 2261-15 du code du travail, les stipulations d'une convention de branche ou d'un accord professionnel ou interprofessionnel répondant à certaines conditions peuvent être rendues obligatoires pour tous les salariés et employeurs compris dans le champ d'application de cette convention ou de cet accord, par arrêté du ministre chargé du travail ; qu'en vertu de l'article D. 2261-13 du même code, le ministre peut, de sa propre initiative, abroger un arrêté d'extension en vue de mettre fin à l'extension d'une convention ou d'un accord ou de certaines de ses stipulations lorsqu'il apparaît que les textes en cause ne répondent plus à la situation de la branche ou des branches dans le champ d'application en cause ; qu'en vertu de ces dispositions, il appartient également au ministre, lorsqu'il apparaît que le champ d'application professionnel défini par une convention ou un accord collectif dont l'extension est envisagée recoupe celui d'une autre convention ou accord collectif étendu par arrêté, préalablement à l'extension projetée, soit d'exclure du champ de cette extension les activités économiques déjà couvertes par la convention ou l'accord collectif précédemment étendu, soit d'abroger l'arrêté d'extension de  cette convention ou de cet accord collectif en tant qu'il s'applique à ces activités ;<br/>
<br/>
              6. Considérant que si l'intitulé d'une convention ou d'un accord est dépourvu, par lui-même, d'effet juridique, une convention ou un accord est habituellement désigné par son titre ; que, compte tenu de l'intérêt qui s'attache à ce que les employeurs et les salariés soient aisément en mesure de savoir de quelle convention collective ils relèvent, le ministre chargé du travail peut, lorsqu'il procède à l'extension d'une convention ou d'un accord, exclure de son intitulé les termes qui sont en contradiction ou qui créent une ambiguïté avec les stipulations de cette convention ou de cet accord qui en définissent le champ d'application et qui, le cas échéant, sont de nature à créer une confusion avec une autre convention ou un autre accord ; <br/>
<br/>
              7. Considérant qu'il ressort des stipulations de l'accord professionnel du 10 janvier 2011 que les bowlings, les établissements de thalassothérapie et les casinos sont au nombre des activités comprises dans son  champ d'application professionnel ; que, si elles peuvent être regardées comme des activités de loisir, les termes " activités de loisirs " dans l'intitulé de l'accord sont de nature à créer une ambiguïté, en ce qu'ils peuvent donner à penser que toutes les activités de loisir entrent dans le champ d'application de l'accord, et à créer une  confusion avec la convention collective nationale des espaces de loisirs, d'attractions et culturels du 5 janvier 1994, étendue par arrêté du 25 juillet 1994, qui est applicable à l'essentiel des activités de loisir ; que, par suite, en décidant d'exclure les termes " activités de loisirs " de l'intitulé de l'accord étendu par l'arrêté attaqué, le ministre chargé du travail n'a pas commis d'erreur de droit ; <br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que le fonds national d'assurance formation de l'industrie hôtelière n'est pas fondé à demander l'annulation du deuxième alinéa de l'article 1er de l'arrêté du 6 avril 2012 ; que ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative doivent par suite être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les interventions du syndicat professionnel Casinos de France, de la fédération nationale de l'hôtellerie restauration sport loisirs et casinos CFE-CGC,  INOVA, de la confédération des professionnels indépendants de l'hôtellerie et de la fédération autonome générale de l'industrie hôtelière touristique sont admises.<br/>
Article 2 : La requête du fonds national d'assurance formation de l'industrie hôtelière est rejetée. <br/>
Article 3 : La présente décision sera notifiée au fonds national d'assurance formation de l'industrie hôtelière, au syndicat professionnel Casinos de France, à la fédération nationale de l'hôtellerie restauration sport loisirs et casinos CFE-CGC, INOVA, à la confédération des professionnels indépendants de l'hôtellerie, à la fédération autonome générale de l'industrie hôtelière touristique et au ministre du travail, de l'emploi et du dialogue social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">66-02-02-02 TRAVAIL ET EMPLOI. CONVENTIONS COLLECTIVES. EXTENSION DES CONVENTIONS COLLECTIVES. POUVOIRS DU MINISTRE. - INCLUSION - POSSIBILITÉ DE MODIFIER L'INTITULÉ D'UNE CONVENTION OU D'UN ACCORD - CONDITIONS.
</SCT>
<ANA ID="9A"> 66-02-02-02 Si l'intitulé d'une convention ou d'un accord est dépourvu, par lui-même, d'effet juridique, une convention ou un accord est habituellement désigné par son titre. Compte tenu de l'intérêt qui s'attache à ce que les employeurs et les salariés soient aisément en mesure de savoir de quelle convention collective ils relèvent, le ministre chargé du travail peut, lorsqu'il procède à l'extension d'une convention ou d'un accord, exclure de son intitulé les termes qui sont en contradiction ou qui créent une ambiguïté avec les stipulations de cette convention ou de cet accord qui en définissent le champ d'application et qui, le cas échéant, sont de nature à créer une confusion avec une autre convention ou un autre accord.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
