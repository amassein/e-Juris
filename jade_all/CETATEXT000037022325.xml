<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037022325</ID>
<ANCIEN_ID>JG_L_2018_06_000000417890</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/02/23/CETATEXT000037022325.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  4ème chambre jugeant seule, 06/06/2018, 417890, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-06-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417890</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 4ème chambre jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAMARRE, JEHANNIN ; SCP LESOURD</AVOCATS>
<RAPPORTEUR>Mme Françoise Tomé</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:417890.20180606</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au juge des référés du tribunal administratif de Paris de suspendre, sur le fondement de l'article L. 521-1 du code de justice administrative, l'exécution de la décision du 11 janvier 2018 du président de l'université Paris-Dauphine décidant son ajournement sans possibilité de redoublement et d'enjoindre au président de l'université de réunir à nouveau un jury de concours. Par une ordonnance n° 1800716 du 22 janvier 2018, prise sur le fondement de l'article L. 522-3 du code de justice administrative, le juge des référés a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 5 et 16 février 2018 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'éducation ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Françoise Tomé, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delamarre, Jéhannin, avocat de M. A...et à la SCP Lesourd, avocat de l'université Paris-Dauphine ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; que l'article L. 522-3 du même code dispose : " Lorsque  la demande ne présente pas un caractère d'urgence ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée, le juge des référés peut la rejeter par une ordonnance motivée sans qu'il y ait lieu d'appliquer les deux premiers alinéas de l'article L. 522-1 " ; <br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Paris que M.A..., étudiant en Master 2 " audit and financial advisory " à l'université Paris-Dauphine au cours de l'année universitaire 2016-2017, s'est vu refuser la délivrance de ce diplôme par la délibération du 11 janvier 2018 du jury d'admission ; qu'il se pourvoit en cassation contre l'ordonnance du 22 janvier 2018 par laquelle le juge des référés a, sur le fondement des dispositions citées ci-dessus de l'article L. 522-3 du code de justice administrative, rejeté sa demande tendant à ce que l'exécution de cette décision soit suspendue ;<br/>
<br/>
              3. Considérant, en premier lieu, que, pour écarter le moyen tiré de ce que l'université Paris-Dauphine avait omis d'organiser certains examens de rattrapage permettant à M. A...d'être admis, le juge des référés a estimé que l'absence d'organisation de ces examens de rattrapage n'était pas établie ; que, dès lors qu'il ressortait des pièces du dossier qui lui était soumis, d'une part que les seuls examens qui n'avaient pas été organisés étaient ceux que l'intéressé devait expressément solliciter et que, d'autre part, M. A...n'avait pas sollicité la tenue de ces examens, le juge des référés a pu, sans dénaturer les faits ni entacher son ordonnance d'erreur de droit, juger que ce moyen n'était pas de nature à créer un doute sérieux quant à la légalité de la décision contestée ;<br/>
<br/>
              4. Considérant, en deuxième lieu, que le juge des référés n'a pas dénaturé les pièces du dossier ni commis d'erreur de droit en jugeant que la présence, sur la copie de la délibération du jury communiquée à M.A..., de signatures reproduites par voie électronique, n'était pas de nature à créer un doute sérieux quant à la régularité de cette décision ;<br/>
<br/>
              5. Considérant, enfin, qu'en jugeant qu'il était manifeste, au vu des deux moyens ainsi soulevés devant lui, que la demande de M. A...n'était pas fondée et en statuant, par suite, sans instruction, sur le fondement de l'article L. 522-3 du code de justice administrative, le juge des référés n'a ni dénaturé les pièces du dossier qui lui était soumis ni commis d'erreur de droit ;<br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que le pourvoi de M. A...doit être rejeté, y compris, par voie de conséquence, ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ; qu'en revanche il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par l'université Paris-Dauphine sur le fondement des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. A...est rejetée.<br/>
Article 2 : Le surplus des conclusions de l'université Paris-Dauphine présenté au titre de l'article L. 761-1 du code de la justice administrative est rejeté. <br/>
Article 3 : La présente décision sera notifiée à M. B...A...et à l'université Paris-Dauphine.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
