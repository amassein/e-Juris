<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044462331</ID>
<ANCIEN_ID>JG_L_2021_12_000000444882</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/46/23/CETATEXT000044462331.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Formation spécialisée, 08/12/2021, 444882, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>444882</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Formation spécialisée</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Thomas Andrieu</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois De Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CEFSP:2021:444882.20211208</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un jugement n° 1903036 du 25 septembre 2020, le tribunal administratif de Paris a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, en tant que serait concernée la sûreté de l'Etat, la requête de M. A... B.... <br/>
<br/>
              Par cette requête, enregistrée au greffe du tribunal administratif de Versailles le 14 février 2019, M. B... demande :<br/>
<br/>
              1°) l'annulation de la décision du 9 janvier 2019 par laquelle le ministre de l'intérieur a refusé de lui communiquer les informations le concernant enregistrées dans le fichier des personnes recherchées (FPR) sur le fondement du 8° du III de l'article 2 du décret du 28 mai 2010 et intéressant la sûreté de l'Etat ;<br/>
<br/>
              2°) d'enjoindre audit ministre de faire droit à sa demande de communication des données le concernant figurant au FPR ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 1 500 euros en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la convention pour la protection des personnes à l'égard du traitement automatisé des données à caractère personnel ;<br/>
              - le code de la sécurité intérieure ;<br/>
              - la loi n° 78-17 du 6 janvier 1978, modifiée notamment par la loi n° 2018-693 du 20 juin 2018 et l'ordonnance n° 2018-1125 du 12 décembre 2018 ; <br/>
              - le décret n° 2010-569 du 28 mai 2010 ;<br/>
              - le décret n° 2018-687 du 1er août 2018 ; <br/>
              - le décret n° 2019-536 du 29 mai 2019 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une séance à huis-clos, d'une part, M. B... et, d'autre part, le ministre de l'intérieur et la Commission nationale de l'informatique et des libertés, qui ont été mis à même de prendre la parole avant les conclusions ;<br/>
<br/>
              Et après avoir entendu en séance :<br/>
<br/>
              - le rapport de Thomas Andrieu, conseiller d'Etat,<br/>
<br/>
              - et, hors la présence des parties, les conclusions de Mme Cécile Barrois de Sarigny, rapporteure publique ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. En vertu de l'article 31 de la loi du 6 janvier 1978, les traitements de données à caractère personnel mis en œuvre pour le compte de l'Etat et intéressant la sûreté de l'Etat, la défense ou la sécurité publique sont autorisés par arrêté du ou des ministres compétents, pris après avis motivé de la Commission nationale de l'informatique et des libertés (CNIL), publié avec l'arrêté autorisant le traitement. Ceux de ces traitements qui portent sur des données mentionnées au I de l'article 6 de la même loi doivent être autorisés par décret en Conseil d'Etat pris après avis motivé de la Commission, publié avec ce décret. Un décret en Conseil d'Etat peut dispenser de publication l'acte réglementaire autorisant la mise en œuvre de ces traitements. Le sens de l'avis émis par la CNIL est alors publié avec ce décret.<br/>
<br/>
              2. L'article L. 841-2 du code de la sécurité intérieure prévoit que le Conseil d'Etat est compétent pour connaître, dans les conditions prévues au chapitre III bis du titre VII du livre VII du code de justice administrative, des requêtes concernant la mise en œuvre du droit d'accès aux données à caractère personnel et intéressant la sûreté de l'Etat qui sont contenues dans les traitements mis en œuvre pour le compte de l'Etat, dont la liste est fixée par décret en Conseil d'Etat. En vertu de l'article R. 841-2 du même code, figurent notamment au nombre de ces traitements le fichier des personnes recherchées (FPR) pour les seules données intéressant la sûreté de l'Etat mentionnées au 8° du III de l'article 2 du décret du 28 mai 2010 susvisé.<br/>
<br/>
              3. L'article L. 773-8 du code de justice administrative dispose que, lorsqu'elle traite des requêtes mentionnées au point 2 : " la formation de jugement se fonde sur les éléments contenus, le cas échéant, dans le traitement sans les révéler ni révéler si le requérant figure ou non dans le traitement. Toutefois, lorsqu'elle constate que le traitement ou la partie de traitement faisant l'objet du litige comporte des données à caractère personnel le concernant qui sont inexactes, incomplètes, équivoques ou périmées, ou dont la collecte, l'utilisation, la communication ou la conservation est interdite, elle en informe le requérant, sans faire état d'aucun élément protégé par le secret de la défense nationale. Elle peut ordonner que ces données soient, selon les cas, rectifiées, mises à jour ou effacées. Saisie de conclusions en ce sens, elle peut indemniser le requérant ". L'article R. 773-20 du même code précise que : " Le défendeur indique au Conseil d'Etat, au moment du dépôt de ses mémoires et pièces, les passages de ses productions et, le cas échéant, de celles de la Commission nationale de contrôle des techniques de renseignement, qui sont protégés par le secret de la défense nationale. / Les mémoires et les pièces jointes produits par le défendeur et, le cas échéant, par la Commission nationale de contrôle des techniques de renseignement sont communiqués au requérant, à l'exception des passages des mémoires et des pièces qui, soit comportent des informations protégées par le secret de la défense nationale, soit confirment ou infirment la mise en œuvre d'une technique de renseignement à l'égard du requérant, soit divulguent des éléments contenus dans le traitement de données, soit révèlent que le requérant figure ou ne figure pas dans le traitement. / Lorsqu'une intervention est formée, le président de la formation spécialisée ordonne, s'il y a lieu, que le mémoire soit communiqué aux parties, et à la Commission nationale de contrôle des techniques de renseignement, dans les mêmes conditions et sous les mêmes réserves que celles mentionnées à l'alinéa précédent ".<br/>
<br/>
              4. Il résulte de l'instruction que M. B... a saisi le ministre de l'intérieur, en application des articles 70-19 et 70-20 de la loi du 6 janvier 1978, alors applicables et repris désormais aux articles 105 et 106 de la loi du 6 janvier 1978, d'une demande d'accès aux données susceptibles de le concerner figurant dans le fichier des personnes recherchées. Par une décision du 9 janvier 2019, le ministre de l'intérieur a refusé de lui communiquer les données demandées. M. B... demande devant le Conseil d'Etat l'annulation de cette décision et d'enjoindre à ce ministre de faire droit à sa demande de communication des données le concernant figurant au FPR, en tant qu'elles sont relatives à la sûreté de l'Etat.<br/>
<br/>
              5. Les circonstances, à les supposer fondées, que la décision attaquée ne serait pas suffisamment motivée et que les voies de recours n'y auraient pas été présentées de manière intelligible, ne peuvent être utilement soulevées à l'appui de la contestation de la légalité d'une décision du ministre de l'intérieur refusant l'accès aux informations contenues le cas échéant dans un traitement de données mentionné à l'article R. 841-2 du code de la sécurité intérieure.<br/>
<br/>
              6. Aux termes des dispositions figurant aujourd'hui à l'article 107 de la loi du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés : " I.-Les droits de la personne physique concernée peuvent faire l'objet de restrictions selon les modalités prévues au II du présent article dès lors et aussi longtemps qu'une telle restriction constitue une mesure nécessaire et proportionnée dans une société démocratique en tenant compte des droits fondamentaux et des intérêts légitimes de la personne pour : / (...) 4° Protéger la sécurité nationale ; (...) / II.-Lorsque les conditions prévues au I sont remplies, le responsable de traitement peut : / (...) 2° Refuser ou limiter le droit d'accès de la personne concernée prévu à l'article 105 ; / (...) III.-Dans les cas mentionnés au 2° du II du présent article, le responsable de traitement informe la personne concernée, dans les meilleurs délais, de tout refus ou de toute limitation d'accès ainsi que des motifs du refus ou de la limitation. Ces informations peuvent ne pas être fournies lorsque leur communication risque de compromettre l'un des objectifs énoncés au I (...). " Il résulte de ces dispositions, qui ne sont contraires ni aux stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, ni à celles de l'article 1er de la convention pour la protection des personnes à l'égard du traitement automatisé des données à caractère personnel, que le ministre de l'intérieur pouvait sans atteinte excessive à la vie privée refuser de communiquer les motifs du refus, qu'il opposait à l'accès aux données relatives à M. B... contenues le cas échéant dans le traitement.<br/>
<br/>
              7. Le ministre de l'intérieur a communiqué au Conseil d'Etat, dans les conditions prévues à l'article R. 773-20 du code de justice administrative, les éléments relatifs à la situation de l'intéressé.<br/>
<br/>
              8. Il appartient à la formation spécialisée, créée par l'article L. 773-2 du code de justice administrative précité, saisie de conclusions dirigées contre le refus de communiquer les données relatives à une personne qui allègue être mentionnée dans un fichier figurant à l'article R. 841-2 du code de la sécurité intérieure, de vérifier, au vu des éléments qui lui ont été communiqués hors la procédure contradictoire, si le requérant figure ou non dans le fichier litigieux. Dans l'affirmative, il lui appartient d'apprécier si les données y figurant sont pertinentes au regard des finalités poursuivies par ce fichier, adéquates et proportionnées. Pour ce faire, elle peut relever d'office tout moyen ainsi que le prévoit l'article L. 773-5 du code de justice administrative. Lorsqu'il apparaît soit que le requérant n'est pas mentionné dans le fichier litigieux, soit que les données à caractère personnel le concernant qui y figurent ne sont entachées d'aucune illégalité, la formation de jugement rejette les conclusions du requérant sans autre précision. Dans le cas où des informations relatives au requérant figurent dans le fichier litigieux et apparaissent entachées d'illégalité soit que les données à caractère personnel le concernant sont inexactes, incomplètes, équivoques ou périmées, soit que leur collecte, leur utilisation, leur communication ou leur consultation est interdite, elle en informe le requérant sans faire état d'aucun élément protégé par le secret de la défense nationale. Cette circonstance, le cas échéant relevée d'office par le juge dans les conditions prévues à l'article R. 773-21 du code de justice administrative, implique nécessairement que l'autorité gestionnaire du fichier rétablisse la légalité en effaçant ou en rectifiant, dans la mesure du nécessaire, les données illégales. Dans pareil cas, doit être annulée la décision implicite refusant de procéder à un tel effacement ou à une telle rectification.<br/>
<br/>
              9. La formation spécialisée a procédé à l'examen des éléments fournis par le ministre. Il résulte de cet examen, qui s'est déroulé selon les modalités décrites au point précédent, qui n'ont révélé aucune illégalité et qui ont ainsi permis à M. B... d'exercer effectivement son droit au recours, que les conclusions de M. B..., y compris ses conclusions à fin d'injonction, doivent être rejetées.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : La requête de M. B... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. A... B... et au ministre de l'intérieur. Copie en sera adressée à la Commission nationale de l'informatique et des libertés.<br/>
<br/>
              Délibéré à l'issue de la séance du 22 novembre 2021 où siégeaient : M. Rémy Schwartz, président de la formation spécialisée, présidant ; Mme Nathalie Escaut, conseillère d'Etat et M. Thomas Andrieu, conseiller d'Etat-rapporteur.<br/>
<br/>
                 Le président : <br/>
                 Signé : M. Rémy Schwartz<br/>
<br/>
 		Le rapporteur : <br/>
      Signé : M. Thomas Andrieu<br/>
<br/>
                 Le secrétaire :<br/>
                 Signé : Mme D... C...<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
