<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044339719</ID>
<ANCIEN_ID>JG_L_2021_10_000000450102</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/33/97/CETATEXT000044339719.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 15/10/2021, 450102</TITRE>
<DATE_DEC>2021-10-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>450102</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:450102.20211015</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un jugement du 18 février 2021, enregistré le 24 février 2021 au secrétariat du contentieux du Conseil d'Etat, le tribunal administratif de Dijon, avant de statuer sur la demande de Mme F... C... tendant, d'une part, à l'annulation de la décision du 28 août 2020 par laquelle le directeur de l'établissement d'hébergement pour personnes âgées dépendantes (EHPAD) Auguste-Arvier de Bligny-sur-Ouche a décidé son placement en congé de maladie ordinaire à compter du 17 mars 2020 et le prélèvement sur son traitement d'un trop perçu de rémunération de 980 euros, d'autre part, à ce qu'il soit enjoint au directeur de cet établissement de reconnaître l'imputabilité au service de ses arrêts de travail à compter du 17 mars 2020 et de procéder au remboursement des sommes indument retenues sur ses traitements, a décidé, par application des dispositions de l'article L. 113-1 du code de justice administrative, de transmettre le dossier de cette demande au Conseil d'Etat, en soumettant à son examen les questions suivantes :<br/>
<br/>
              1°) Les dispositions combinées de l'article 41 de la loi du 9 janvier 1986 portant dispositions statutaires relatives à la fonction publique hospitalière, qui exclut de son champ d'application, à compter du 21 janvier 2017, les accidents ou maladies " contractées ou aggravées en service ", de l'article 16 du décret du 19 avril 1988 relatif aux conditions d'aptitude physique et aux congés de maladie des agents de la fonction publique hospitalière et de l'article 1er de l'arrêté du 4 août 2004 relatif aux commissions de réforme des agents de la fonction publique territoriale et de la fonction publique hospitalière font-elles obstacle à l'application aux congés pour invalidité temporaire imputable au service (CITIS) institués par l'article 21 bis de la loi du 13 juillet 1983 portant droits et obligations des fonctionnaire des règles procédurales et modalités d'octroi antérieures à l'entrée en vigueur du décret du 13 mai 2020 relatif au congé pour invalidité temporaire imputable au service dans la fonction publique hospitalière '<br/>
<br/>
              En cas de réponse négative à cette première question, s'agissant de la période comprise entre le 21 janvier 2017 et le 16 mai 2020, les anciennes règles procédurales et en particulier celles relatives à l'obligation de saisine de la commission de réforme pour toute décision de refus d'imputabilité au service d'un accident, sont-elles toutes applicables aux CITIS '<br/>
<br/>
              En cas de réponse positive à la première question, selon quelles modalités y a-t-il lieu de combler, le cas échéant, le vide procédural '<br/>
<br/>
              2°) Lorsqu'un accident de service survient antérieurement à l'entrée en vigueur du décret du 13 mai 2020 et que l'autorité administrative est amenée, postérieurement à son entrée en vigueur, à se prononcer sur les droits de l'agent au CITIS, y a-t-il lieu de faire application des règles procédurales en vigueur à la date à laquelle l'autorité administrative se prononce, dès lors que l'article 16 de ce décret dispose que toute prolongation de congé postérieure à son entrée en vigueur " est accordée dans les conditions prévues " à son chapitre Ier, ou convient-il, au contraire, de faire application des textes procéduraux en vigueur à la date de l'accident ou du diagnostic de la maladie professionnelle '<br/>
<br/>
<br/>
              Le ministre de la transformation et de la fonction publiques a indiqué, par un mémoire enregistré le 8 mars 2021, ne pas avoir d'observations à formuler.<br/>
<br/>
              La demande d'avis a été communiquée à l'EHPAD Auguste-Arvier de Bligny-sur-Ouche et au ministre des solidarités et de la santé, qui n'ont pas produit d'observations.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu :<br/>
- la loi n° 83-634 du 13 juillet 1983 ; <br/>
- la loi n° 86-33 du 9 janvier 1986 ; <br/>
- la loi n° 2018-771 du 5 septembre 2018 ; <br/>
- l'ordonnance n° 2017-53 du 19 janvier 2017 ; <br/>
- le décret n° 2003-1306 du 26 décembre 2003 ; <br/>
- le décret n° 88-386 du 19 avril 1988 ;<br/>
- le décret n° 2020-566 du 13 mai 2020 ; <br/>
- l'arrêté du 4 août 2004 relatif aux commissions de réforme des agents de la fonction publique territoriale et de la fonction publique hospitalière ; <br/>
- le code de justice administrative, notamment son article L. 113-1 ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
- le rapport de M. Jean-Dominique Langlais, conseiller d'Etat, <br/>
<br/>
- les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              REND L'AVIS SUIVANT : <br/>
<br/>
<br/>
              1. L'article 10 de l'ordonnance du 19 janvier 2017 portant diverses dispositions relatives au compte personnel d'activité, à la formation et à la santé et la sécurité au travail dans la fonction publique a institué un " congé pour invalidité temporaire imputable au service " en insérant dans la loi du 13 juillet 1983 portant droits et obligations des fonctionnaires un article 21 bis aux termes duquel : " I. - Le fonctionnaire en activité a droit à un congé pour invalidité temporaire imputable au service lorsque son incapacité temporaire de travail est consécutive à un accident reconnu imputable au service, à un accident de trajet ou à une maladie contractée en service définis aux II, III et IV du présent article. Ces définitions ne sont pas applicables au régime de réparation de l'incapacité permanente du fonctionnaire. / Le fonctionnaire conserve l'intégralité de son traitement jusqu'à ce qu'il soit en état de reprendre son service ou jusqu'à la mise à la retraite. Il a droit, en outre, au remboursement des honoraires médicaux et des frais directement entraînés par la maladie ou l'accident. La durée du congé est assimilée à une période de service effectif. L'autorité administrative peut, à tout moment, vérifier si l'état de santé du fonctionnaire nécessite son maintien en congé pour invalidité temporaire imputable au service.  / II.- Est présumé imputable au service tout accident survenu à un fonctionnaire, quelle qu'en soit la cause, dans le temps et le lieu du service, dans l'exercice ou à l'occasion de l'exercice par le fonctionnaire de ses fonctions ou d'une activité qui en constitue le prolongement normal, en l'absence de faute personnelle ou de toute autre circonstance particulière détachant l'accident du service. / III.- Est reconnu imputable au service, lorsque le fonctionnaire ou ses ayants droit en apportent la preuve ou lorsque l'enquête permet à l'autorité administrative de disposer des éléments suffisants, l'accident de trajet dont est victime le fonctionnaire qui se produit sur le parcours habituel entre le lieu où s'accomplit son service et sa résidence ou son lieu de restauration et pendant la durée normale pour l'effectuer, sauf si un fait personnel du fonctionnaire ou toute autre circonstance particulière étrangère notamment aux nécessités de la vie courante est de nature à détacher l'accident du service. / IV.- Est présumée imputable au service toute maladie désignée par les tableaux de maladies professionnelles mentionnés aux articles L. 461-1 et suivants du code de la sécurité sociale et contractée dans l'exercice ou à l'occasion de l'exercice par le fonctionnaire de ses fonctions dans les conditions mentionnées à ce tableau. / Si une ou plusieurs conditions tenant au délai de prise en charge, à la durée d'exposition ou à la liste limitative des travaux ne sont pas remplies, la maladie telle qu'elle est désignée par un tableau peut être reconnue imputable au service lorsque le fonctionnaire ou ses ayants droit établissent qu'elle est directement causée par l'exercice des fonctions. / Peut également être reconnue imputable au service une maladie non désignée dans les tableaux de maladies professionnelles mentionnés aux articles L. 461-1 et suivants du code de la sécurité sociale lorsque le fonctionnaire ou ses ayants droit établissent qu'elle est essentiellement et directement causée par l'exercice des fonctions et qu'elle entraîne une incapacité permanente à un taux déterminé et évalué dans les conditions prévues par décret en Conseil d'Etat. / V.- L'employeur public est subrogé dans les droits éventuels du fonctionnaire victime d'un accident provoqué par un tiers jusqu'à concurrence du montant des charges qu'il a supportées ou supporte du fait de cet accident. Il est admis à poursuivre directement contre le responsable du dommage ou son assureur le remboursement des charges patronales afférentes aux rémunérations maintenues ou versées audit fonctionnaire pendant la période d'indisponibilité de celui-ci par dérogation aux dispositions de l'article 2 de l'ordonnance n° 59-76 du 7 janvier 1959 relative aux actions en réparation civile de l'Etat et de certaines autres personnes publiques. / VI.- Un décret en Conseil d'Etat fixe les modalités du congé pour invalidité temporaire imputable au service mentionné au premier alinéa et détermine ses effets sur la situation administrative des fonctionnaires. Il fixe également les obligations auxquelles les fonctionnaires demandant le bénéfice de ce congé sont tenus de se soumettre en vue, d'une part, de l'octroi ou du maintien du congé et, d'autre part, du rétablissement de leur santé, sous peine de voir réduire ou supprimer le traitement qui leur avait été conservé. / VII.- Les employeurs publics fournissent les données nécessaires à la connaissance des accidents de service et des maladies professionnelles. Un arrêté du ministre chargé de la fonction publique fixe les modalités pratiques de la collecte et du traitement de ces données. " <br/>
<br/>
              L'article 10 de l'ordonnance du 19 janvier 2017 a aussi, en conséquence de l'institution du congé pour invalidité temporaire imputable au service à l'article 21 bis de la loi du 13 juillet 1983, modifié des dispositions des lois du 11 janvier 1984, du 26 janvier 1984 et du 9 janvier 1986 régissant respectivement la fonction publique de l'Etat, la fonction publique territoriale et la fonction publique hospitalière. Le IV de l'article 10, pour la fonction publique hospitalière, dispose ainsi que " A l'article 41 de la loi du 9 janvier 1986 susvisée : a) Au deuxième alinéa du 2°, les mots : " ou d'un accident survenu dans l'exercice ou à l'occasion de l'exercice de ses fonctions " sont remplacés par les mots : ", à l'exception des blessures ou des maladies contractées ou aggravées en service " ; b) Au 4°, le deuxième alinéa est supprimé ; c) Après le quatrième alinéa du 4°, est inséré un alinéa ainsi rédigé : " Les dispositions du quatrième alinéa du 2° du présent article sont applicables au congé de longue durée. "<br/>
<br/>
              2. L'application de ces dispositions résultant de l'ordonnance du 19 janvier 2017 était manifestement impossible en l'absence d'un texte réglementaire fixant, notamment, les conditions de procédure applicables à l'octroi de ce nouveau congé pour invalidité temporaire imputable au service.<br/>
<br/>
              Les dispositions de l'article 21 bis de la loi du 13 juillet 1983 portant droits et obligations des fonctionnaires ne sont donc entrées en vigueur, en tant qu'elles s'appliquent à la fonction publique hospitalière, qu'à la date d'entrée en vigueur, le 16 mai 2020, du décret du 13 mai 2020 relatif au congé pour invalidité temporaire imputable au service dans la fonction publique hospitalière, décret par lequel le pouvoir réglementaire a pris les dispositions réglementaires nécessaires pour cette fonction publique et dont l'intervention était, au demeurant, prévue, sous forme de décret en Conseil d'Etat, par le VI de l'article 21 bis de la loi du 13 juillet 1983 résultant de l'article 10 de l'ordonnance du 19 janvier 2017.<br/>
<br/>
              Il en résulte que les dispositions de l'article 41 de la loi du 9 janvier 1986 dans leur rédaction antérieure à celle résultant de l'ordonnance du 19 janvier 2017 sont demeurées applicables jusqu'à l'entrée en vigueur du décret du 13 mai 2020.<br/>
<br/>
              3. En outre, aux termes des dispositions transitoires figurant à l'article 16 du décret du 13 mai 2020 : " Le fonctionnaire en congé à la suite d'un accident ou d'une maladie imputable au service continue de bénéficier de ce congé jusqu'à son terme. Toute prolongation de ce congé postérieure à l'entrée en vigueur du présent décret est accordée dans les conditions prévues au chapitre Ier. / Les conditions de forme et de délais prévues aux articles 35-2 à 35-7 du décret du 19 avril 1988 susvisé dans sa rédaction issue du présent décret ne sont pas applicables aux fonctionnaires ayant déposé une déclaration d'accident ou de maladie professionnelle avant l'entrée en vigueur du présent décret. / Les délais mentionnés à l'article 35-3 du même décret courent à compter du premier jour du deuxième mois suivant la publication du présent décret lorsqu'un accident ou une maladie n'a pas fait l'objet d'une déclaration avant cette date ".<br/>
<br/>
              Il résulte de ces dispositions que les conditions de forme et de délai prévues aux articles 35-2 à 35-7 du décret du 19 avril 1988 relatif aux conditions d'aptitude physique et aux congés de maladie des agents de la fonction publique hospitalière, dans sa rédaction issue du décret du 13 mai 2020, sont uniquement applicables, d'une part, aux demandes de prolongation d'un congé pour accident de service ou pour maladie imputable au service pour une période débutant après le 16 mai 2020 et, d'autre part, aux demandes initiales de congé pour invalidité temporaire imputable au service motivées par un accident ou une maladie dont la déclaration a été déposée après cette date. <br/>
<br/>
<br/>
<br/>
<br/>
              Le présent avis sera notifié au tribunal administratif de Dijon, à Mme F... C..., à l'établissement d'hébergement pour personnes âgées dépendantes Auguste-Arvier de Bligny-sur-Ouche, au ministre de la transformation et de la fonction publiques et au ministre des solidarités et de la santé. <br/>
<br/>
              Il sera publié au Journal officiel de la République française. <br/>
<br/>
<br/>
Délibéré à l'issue de la séance du 27 septembre 2021 où siégeaient : M. Jacques-Henri Stahl, président adjoint de la section du contentieux, Présidant ; M. Denis Piveteau, M. Fabien Raynaud, présidents de chambre ; Mme Sophie-Caroline de Margerie, M. Olivier Rousselle, M. Jean-Philippe Mochon, M. Olivier Yeznikian, M. Cyril Roger-Lacan, conseillers d'Etat et M. Jean-Dominique Langlais, conseiller d'Etat -rapporteur.<br/>
<br/>
              Rendu le 15 octobre 2021 <br/>
<br/>
              Le président : <br/>
              Signé : M. Jacques-Henri Stahl<br/>
<br/>
<br/>
<br/>
              Le rapporteur :<br/>
              Signé : M. Jean-Dominique Langlais<br/>
<br/>
<br/>
<br/>
              La secrétaire :<br/>
              Signé : Mme Anne-Lise Calvaire<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-08-01-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. - APPLICATION DANS LE TEMPS. - ENTRÉE EN VIGUEUR. - ENTRÉE EN VIGUEUR SUBORDONNÉE À L'INTERVENTION DE MESURES D'APPLICATION. - DISPOSITION INSTITUANT UN CONGÉ POUR INVALIDITÉ TEMPORAIRE IMPUTABLE AU SERVICE (ART. 10 DE L'ORDONNANCE DU 19 JANVIER 2017) - 1) APPLICATION MANIFESTEMENT IMPOSSIBLE EN L'ABSENCE D'UN TEXTE RÉGLEMENTAIRE D'APPLICATION [RJ1] - 2) FONCTION PUBLIQUE HOSPITALIÈRE - A) ENTRÉE EN VIGUEUR AVEC LE DÉCRET DU 13 MAI 2020 - B) APPLICATION DE CE DÉCRET DANS LE TEMPS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-05-04-01-03 FONCTIONNAIRES ET AGENTS PUBLICS. - POSITIONS. - CONGÉS. - CONGÉS DE MALADIE. - ACCIDENTS DE SERVICE. - DISPOSITION INSTITUANT UN CONGÉ POUR INVALIDITÉ TEMPORAIRE IMPUTABLE AU SERVICE (ART. 10 DE L'ORDONNANCE DU 19 JANVIER 2017) - 1) APPLICATION MANIFESTEMENT IMPOSSIBLE EN L'ABSENCE D'UN TEXTE RÉGLEMENTAIRE D'APPLICATION [RJ1] - 2) FONCTION PUBLIQUE HOSPITALIÈRE - A) ENTRÉE EN VIGUEUR AVEC LE DÉCRET DU 13 MAI 2020 - B) APPLICATION DE CE DÉCRET DANS LE TEMPS.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">36-07-01-01 FONCTIONNAIRES ET AGENTS PUBLICS. - STATUTS, DROITS, OBLIGATIONS ET GARANTIES. - STATUT GÉNÉRAL DES FONCTIONNAIRES DE L'ÉTAT ET DES COLLECTIVITÉS LOCALES. - DROITS ET OBLIGATIONS DES FONCTIONNAIRES (LOI DU 13 JUILLET 1983). - DISPOSITION INSTITUANT UN CONGÉ POUR INVALIDITÉ TEMPORAIRE IMPUTABLE AU SERVICE (ART. 10 DE L'ORDONNANCE DU 19 JANVIER 2017) - APPLICATION MANIFESTEMENT IMPOSSIBLE EN L'ABSENCE D'UN TEXTE RÉGLEMENTAIRE D'APPLICATION [RJ1].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">36-07-01-04 FONCTIONNAIRES ET AGENTS PUBLICS. - STATUTS, DROITS, OBLIGATIONS ET GARANTIES. - STATUT GÉNÉRAL DES FONCTIONNAIRES DE L'ÉTAT ET DES COLLECTIVITÉS LOCALES. - DISPOSITIONS STATUTAIRES RELATIVES À LA FONCTION PUBLIQUE HOSPITALIÈRE (LOI DU 9 JANVIER 1986). - APPLICATION MANIFESTEMENT IMPOSSIBLE, EN L'ABSENCE D'UN TEXTE RÉGLEMENTAIRE D'APPLICATION [RJ1], DE LA DISPOSITION INSTITUANT UN CONGÉ POUR INVALIDITÉ TEMPORAIRE IMPUTABLE AU SERVICE (ART. 10 DE L'ORDONNANCE DU 19 JANVIER 2017) - 1) ENTRÉE EN VIGUEUR AVEC LE DÉCRET DU 13 MAI 2020 - 2) APPLICATION DE CE DÉCRET DANS LE TEMPS.
</SCT>
<ANA ID="9A"> 01-08-01-02 Insertion par l'article 10 de l'ordonnance n° 2017-53 du 19 janvier 2017 d'un article 21 bis dans la loi n° 83-634 du 13 juillet 1983 instituant un congé pour invalidité temporaire imputable au service. En conséquence, modification des lois n° 84-16 du 11 janvier 1984, n° 84-53 du 26 janvier 1984 et n° 86-33 du 9 janvier 1986 régissant respectivement la fonction publique de l'Etat, la fonction publique territoriale et la fonction publique hospitalière. En particulier, modification par le IV de l'article 10, pour la fonction publique hospitalière, de l'article 41 de la loi du 9 janvier 1986.......1) L'application de ces dispositions résultant de l'ordonnance du 19 janvier 2017 est manifestement impossible en l'absence d'un texte réglementaire fixant, notamment, les conditions de procédure applicables à l'octroi de ce nouveau congé pour invalidité temporaire imputable au service.......2) a) L'article 21 bis de la loi du 13 juillet 1983 n'est donc entré en vigueur, en tant qu'il s'applique à la fonction publique hospitalière, qu'à la date d'entrée en vigueur, le 16 mai 2020, du décret n° 2020-566 du 13 mai 2020 par lequel le pouvoir réglementaire a pris les dispositions réglementaires nécessaires pour cette fonction publique et dont l'intervention était, au demeurant, prévue, sous forme de décret en Conseil d'Etat, par le VI de l'article 21 bis de la loi du 13 juillet 1983 résultant de l'article 10 de l'ordonnance du 19 janvier 2017.......Il en résulte que l'article 41 de la loi du 9 janvier 1986 dans sa rédaction antérieure à celle résultant de l'ordonnance du 19 janvier 2017 est demeuré applicable jusqu'à l'entrée en vigueur du décret du 13 mai 2020.......b) Il résulte des dispositions transitoires figurant à l'article 16 du décret n° 2020-566 du 13 mai 2020 que les conditions de forme et de délai prévues aux articles 35-2 à 35-7 du décret n° 88-386 du 19 avril 1988, dans sa rédaction issue du décret du 13 mai 2020, sont uniquement applicables, d'une part, aux demandes de prolongation d'un congé pour accident de service, ou pour maladie imputable au service, pour une période débutant après le 16 mai 2020 et, d'autre part, aux demandes initiales de congé pour invalidité temporaire imputable au service motivées par un accident ou une maladie dont la déclaration a été déposée après cette date.</ANA>
<ANA ID="9B"> 36-05-04-01-03 Insertion par l'article 10 de l'ordonnance n° 2017-53 du 19 janvier 2017 d'un article 21 bis dans la loi n° 83-634 du 13 juillet 1983 instituant un congé pour invalidité temporaire imputable au service. En conséquence, modification des lois n° 84-16 du 11 janvier 1984, n° 84-53 du 26 janvier 1984 et n° 86-33 du 9 janvier 1986 régissant respectivement la fonction publique de l'Etat, la fonction publique territoriale et la fonction publique hospitalière. En particulier, modification par le IV de l'article 10, pour la fonction publique hospitalière, de l'article 41 de la loi du 9 janvier 1986.......1) L'application de ces dispositions résultant de l'ordonnance du 19 janvier 2017 est manifestement impossible en l'absence d'un texte réglementaire fixant, notamment, les conditions de procédure applicables à l'octroi de ce nouveau congé pour invalidité temporaire imputable au service.......2) a) L'article 21 bis de la loi du 13 juillet 1983 n'est donc entré en vigueur, en tant qu'il s'applique à la fonction publique hospitalière, qu'à la date d'entrée en vigueur, le 16 mai 2020, du décret n° 2020-566 du 13 mai 2020 par lequel le pouvoir réglementaire a pris les dispositions réglementaires nécessaires pour cette fonction publique et dont l'intervention était, au demeurant, prévue, sous forme de décret en Conseil d'Etat, par le VI de l'article 21 bis de la loi du 13 juillet 1983 résultant de l'article 10 de l'ordonnance du 19 janvier 2017.......Il en résulte que l'article 41 de la loi du 9 janvier 1986 dans sa rédaction antérieure à celle résultant de l'ordonnance du 19 janvier 2017 est demeuré applicable jusqu'à l'entrée en vigueur du décret du 13 mai 2020.......b) Il résulte des dispositions transitoires figurant à l'article 16 du décret n° 2020-566 du 13 mai 2020 que les conditions de forme et de délai prévues aux articles 35-2 à 35-7 du décret n° 88-386 du 19 avril 1988, dans sa rédaction issue du décret du 13 mai 2020, sont uniquement applicables, d'une part, aux demandes de prolongation d'un congé pour accident de service, ou pour maladie imputable au service, pour une période débutant après le 16 mai 2020 et, d'autre part, aux demandes initiales de congé pour invalidité temporaire imputable au service motivées par un accident ou une maladie dont la déclaration a été déposée après cette date.</ANA>
<ANA ID="9C"> 36-07-01-01 Insertion par l'article 10 de l'ordonnance n° 2017-53 du 19 janvier 2017 d'un article 21 bis dans la loi n° 83-634 du 13 juillet 1983 instituant un congé pour invalidité temporaire imputable au service. En conséquence, modification des lois n° 84-16 du 11 janvier 1984, n° 84-53 du 26 janvier 1984 et n° 86-33 du 9 janvier 1986 régissant respectivement la fonction publique de l'Etat, la fonction publique territoriale et la fonction publique hospitalière. En particulier, modification par le IV de l'article 10, pour la fonction publique hospitalière, de l'article 41 de la loi du 9 janvier 1986.......L'application de ces dispositions résultant de l'ordonnance du 19 janvier 2017 est manifestement impossible en l'absence d'un texte réglementaire fixant, notamment, les conditions de procédure applicables à l'octroi de ce nouveau congé pour invalidité temporaire imputable au service.</ANA>
<ANA ID="9D"> 36-07-01-04 Insertion par l'article 10 de l'ordonnance n° 2017-53 du 19 janvier 2017 d'un article 21 bis dans la loi n° 83-634 du 13 juillet 1983 instituant un congé pour invalidité temporaire imputable au service. En conséquence, modification des lois n° 84-16 du 11 janvier 1984, n° 84-53 du 26 janvier 1984 et n° 86-33 du 9 janvier 1986 régissant respectivement la fonction publique de l'Etat, la fonction publique territoriale et la fonction publique hospitalière. En particulier, modification par le IV de l'article 10, pour la fonction publique hospitalière, de l'article 41 de la loi du 9 janvier 1986. L'application de ces dispositions résultant de l'ordonnance du 19 janvier 2017 est manifestement impossible en l'absence d'un texte réglementaire fixant, notamment, les conditions de procédure applicables à l'octroi de ce nouveau congé pour invalidité temporaire imputable au service.......1) L'article 21 bis de la loi du 13 juillet 1983 n'est donc entré en vigueur, en tant qu'il s'applique à la fonction publique hospitalière, qu'à la date d'entrée en vigueur, le 16 mai 2020, du décret n° 2020-566 du 13 mai 2020 par lequel le pouvoir réglementaire a pris les dispositions réglementaires nécessaires pour cette fonction publique et dont l'intervention était, au demeurant, prévue, sous forme de décret en Conseil d'Etat, par le VI de l'article 21 bis de la loi du 13 juillet 1983 résultant de l'article 10 de l'ordonnance du 19 janvier 2017.......Il en résulte que l'article 41 de la loi du 9 janvier 1986 dans sa rédaction antérieure à celle résultant de l'ordonnance du 19 janvier 2017 est demeuré applicable jusqu'à l'entrée en vigueur du décret du 13 mai 2020.......2) Il résulte des dispositions transitoires figurant à l'article 16 du décret n° 2020-566 du 13 mai 2020 que les conditions de forme et de délai prévues aux articles 35-2 à 35-7 du décret n° 88-386 du 19 avril 1988, dans sa rédaction issue du décret du 13 mai 2020, sont uniquement applicables, d'une part, aux demandes de prolongation d'un congé pour accident de service, ou pour maladie imputable au service, pour une période débutant après le 16 mai 2020 et, d'autre part, aux demandes initiales de congé pour invalidité temporaire imputable au service motivées par un accident ou une maladie dont la déclaration a été déposée après cette date.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur les conditions d'entrée en vigueur immédiate d'une loi, CE, Assemblée, 27 janvier 1984, Cochin, n° 16546, p. 23 ; CE, Section, 4 juin 2007, Lagier et Consorts Guignon, n°s 303422 304214, p. 228.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
