<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042612681</ID>
<ANCIEN_ID>JG_L_2020_11_000000446484</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/61/26/CETATEXT000042612681.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 27/11/2020, 446484, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-11-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>446484</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SOLTNER</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:446484.20201127</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              I. Sous le n° 446484, par une requête, enregistrée le 16 novembre 2020 au secrétariat du contentieux du Conseil d'Etat, M. D... E... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution de la décision n° 054-2019 rendue le 14 septembre 2020 par la chambre disciplinaire du conseil national de l'ordre national des masseurs-kinésithérapeutes ayant confirmé une décision de la chambre disciplinaire de première instance du conseil de l'ordre des masseurs-kinésithérapeutes d'Ile-de-France ayant prononcé une sanction d'interdiction d'exercice d'une année à son encontre ;<br/>
<br/>
              2°) de mettre à la charge de Mme B... C... la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Il soutient que :<br/>
              - la condition d'urgence est remplie dès lors que la décision contestée le prive du droit d'exercer son activité professionnelle pendant une durée significative et lui cause ainsi un préjudice financier suffisamment grave et immédiat ;<br/>
              - il existe un doute sérieux quant à la légalité de la décision contestée ;<br/>
              - la décision contestée méconnaît le droit à un procès équitable dès lors que le conseil national de l'ordre n'a procédé à aucune appréciation rigoureuse de la crédibilité des affirmations de la plaignante, à laquelle il n'a pas été confronté ;<br/>
              - elle est entachée d'une erreur manifeste d'appréciation et de dénaturation des écritures du requérant dès lors qu'il n'a été tenu aucun compte des éléments de défense qu'il a invoqué, le conseil national de l'ordre affirmant au contraire qu'il n'avait apporté " aucune explication " ;<br/>
              - elle est manifestement disproportionnée eu égard à la gravité des faits en cause et au professionnalisme du requérant, qui n'a jamais été mis à défaut.<br/>
<br/>
<br/>
              II. Sous le n° 446486, par une requête, enregistrée le 16 novembre 2020 au secrétariat du contentieux du Conseil d'Etat, M. D... E... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative : <br/>
<br/>
              1°) d'ordonner la suspension de l'exécution de la décision n° 052-2019 rendue le 14 septembre 2020 par la chambre disciplinaire du conseil national de l'ordre national des masseurs-kinésithérapeutes ayant annulé une décision de la chambre disciplinaire de première instance du conseil de l'ordre des masseurs-kinésithérapeutes d'Ile-de-France du 20 novembre 2019 et ayant prononcé une sanction d'interdiction d'exercice de quatre mois à son encontre ;<br/>
<br/>
              2°) de mettre à la charge de Mme A... C... la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Il soutient que : <br/>
              - la condition d'urgence est remplie dès lors que la décision contestée le prive du droit d'exercer son activité professionnelle pendant une durée significative et lui cause ainsi un préjudice financier suffisamment grave et immédiat ;<br/>
              - il existe un doute sérieux quant à la légalité de la décision contestée ;<br/>
              - la décision contestée est insuffisamment motivée et est entachée d'une erreur manifeste d'appréciation et de dénaturation des faits dès lors, d'une part, que le conseil national de l'ordre s'est borné à affirmer que les faits avancés par la plaignante résultaient de pièces " non sérieusement contestées " et, d'autre part, qu'il a omis de répondre au moyen du requérant relatif au dispositif contractuel de provision et de compensation qui avait été instauré entre les parties ;<br/>
              - elle est manifestement disproportionnée eu égard au montant des sommes en cause et à l'engagement de restitution pris par le requérant. <br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Les requêtes formées par M. E... sont dirigées contre des décisions de la chambre disciplinaire du conseil national de l'ordre des masseurs-kinésithérapeutes prises à son encontre, lesquelles présentent à juger des mêmes questions. Il y a donc lieu de les joindre pour statuer par une seule ordonnance. <br/>
<br/>
              2. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée. <br/>
<br/>
              3. M. E... demande la suspension de l'exécution des sanctions prises à son encontre par la chambre disciplinaire du conseil national de l'ordre national des masseurs-kinésithérapeutes qui, du fait de leur caractère juridictionnel, ne peuvent faire l'objet de la voie de droit prévue par l'article L. 521-1 du code de justice administrative. Il est, par suite, manifeste que les demandes de M. E... sont irrecevables. Ses requêtes, y compris les conclusions présentées sur le fondement de l'article L. 761-1 du code de justice administrative, doivent, dès lors, être rejetées selon la procédure prévue par l'article L. 522-3 du code de justice administrative. <br/>
<br/>
<br/>
<br/>			O R D O N N E :<br/>
              			------------------<br/>
<br/>
Article 1er : Les requêtes de M. E... sont rejetées. <br/>
Article 2 : La présente ordonnance sera notifiée à M. D... E.... <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
