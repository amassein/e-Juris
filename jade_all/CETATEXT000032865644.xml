<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032865644</ID>
<ANCIEN_ID>JG_L_2016_07_000000352901</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/86/56/CETATEXT000032865644.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème chambres réunies, 08/07/2016, 352901</TITRE>
<DATE_DEC>2016-07-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>352901</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER, TEXIDOR ; SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>M. Frédéric Puigserver</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:352901.20160708</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une décision du 30 décembre 2013, le Conseil d'Etat, statuant au contentieux sur la requête de l'Union des syndicats de l'immobilier (UNIS) tendant à l'annulation pour excès de pouvoir de l'arrêté du ministre du travail, de l'emploi et de la santé du 13 juillet 2011 portant extension d'avenants à la convention collective nationale de l'immobilier, a annulé cet arrêté en tant qu'il étend l'avenant n° 48 à la convention collective nationale de l'immobilier sans exclure du champ de cette extension les voyageurs représentants placiers entrant dans le champ de l'article 7 de la convention collective nationale de retraite et de prévoyance des cadres du 14 mars 1947 et a sursis à statuer sur le surplus des conclusions de la requête jusqu'à ce que la Cour de justice de l'Union européenne se soit prononcée sur la question de savoir si le respect de l'obligation de transparence qui découle de l'article 56 du traité sur le fonctionnement de l'Union européenne est une condition préalable obligatoire à l'extension, par un Etat membre, à l'ensemble des entreprises d'une branche, d'un accord collectif confiant à un unique opérateur, choisi par les partenaires sociaux, la gestion d'un régime de prévoyance complémentaire obligatoire institué au profit des salariés.<br/>
<br/>
              Par un arrêt C-25/14 et C-26/14 du 17 décembre 2015, la Cour de justice de l'Union européenne s'est prononcée sur cette question.<br/>
<br/>
              Vu les autres pièces du dossier, y compris celles visées par la décision du Conseil d'Etat du 30 décembre 2013 ;<br/>
<br/>
              Vu :<br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - le code du travail ;<br/>
              - le code de la sécurité sociale ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Puigserver, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Blancpain, Soltner, Texidor, avocat de l'Union des syndicats de l'immobilier, et à la SCP Gatineau, Fattaccini, avocat de la Fédération des sociétés immobilières et foncières et autres ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'en vertu de l'article L. 911-1 du code de la sécurité sociale, les " garanties collectives dont bénéficient les salariés ", qui ont notamment pour objet, aux termes de l'article L. 911-2 du même code, de prévoir " la couverture du risque décès, des risques portant atteinte à l'intégrité physique de la personne ou liés à la maternité, des risques d'incapacité de travail ou d'invalidité " en complément de celles qui résultent de l'organisation de la sécurité sociale, peuvent notamment être déterminées par voie de conventions ou d'accords collectifs ; qu'en vertu de l'article L. 911-3 du même code, ces accords peuvent être étendus dans les conditions prévues par le code du travail, sous réserve des dispositions spécifiques applicables lorsqu'ils ont pour objet exclusif la détermination de telles garanties collectives ; qu'en vertu de l'article L. 912-1 de ce code, dans sa rédaction antérieure à la loi du 23 décembre 2013 de financement de la sécurité sociale pour 2014 restée applicable au litige, conformément à la décision du Conseil constitutionnel n° 2013-672 DC du 13 juin 2013, ces accords peuvent prévoir une mutualisation des risques dont ils organisent la couverture auprès d'une entreprise régie par le code des assurances, d'une institution relevant du titre III du livre IX du code de la sécurité sociale ou d'une mutuelle relevant du code de la mutualité, à laquelle adhèrent alors obligatoirement les entreprises relevant du champ d'application de ces accords, sous réserve de comporter une clause fixant les conditions et la périodicité, ne pouvant excéder cinq ans, du réexamen des modalités d'organisation de la mutualisation des risques ; <br/>
<br/>
              2. Considérant que, par un arrêté du 13 juillet 2011, le ministre du travail, de l'emploi et de la santé a étendu l'avenant n° 48 du 23 novembre 2010 à la convention collective nationale de l'immobilier, qui institue pour l'ensemble des salariés de la branche un régime obligatoire de prévoyance couvrant les risques décès, incapacité de travail et invalidité et un régime obligatoire de remboursement de frais de santé, ainsi que les avenants n° 49 et 50 du 17 mai 2011, modifiant respectivement l'avenant n° 48 et le règlement intérieur de la commission paritaire de suivi prévu par cet avenant ; que, par son article 17, l'avenant n° 48 désigne, pour une période de trois ans, l'Institution de prévoyance du groupe Mornay en tant qu'unique organisme assureur des garanties de ces deux régimes ; que, par une décision du 30 décembre 2013, le Conseil d'Etat, statuant au contentieux sur la requête de l'Union des syndicats de l'immobilier (UNIS) tendant à l'annulation pour excès de pouvoir de l'arrêté du 13 juillet 2011, a annulé partiellement cet arrêté et a sursis à statuer sur le surplus des conclusions de la requête jusqu'à ce que la Cour de justice de l'Union européenne se soit prononcée sur la question de savoir si le respect de l'obligation de transparence qui découle de l'article 56 du traité sur le fonctionnement de l'Union européenne est une condition préalable obligatoire à l'extension, par un Etat membre, à l'ensemble des entreprises d'une branche, d'un accord collectif confiant à un unique opérateur, choisi par les partenaires sociaux, la gestion d'un régime de prévoyance complémentaire obligatoire institué au profit des salariés ;<br/>
<br/>
              Sur la légalité de l'arrêté attaqué :<br/>
<br/>
              3. Considérant qu'aux termes du premier alinéa de l'article 56 du traité sur le fonctionnement de l'Union européenne : " Dans le cadre des dispositions ci-après, les restrictions à la libre prestation des services à l'intérieur de l'Union sont interdites à l'égard des ressortissants des États membres établis dans un État membre autre que celui du destinataire de la prestation " ;<br/>
<br/>
              4. Considérant que, dans l'arrêt du 17 décembre 2015 par lequel elle s'est prononcée sur la question dont le Conseil d'Etat, statuant au contentieux, l'avait saisie à titre préjudiciel, après avoir écarté les autres moyens de la requête de l'UNIS, la Cour de justice de l'Union européenne a rappelé, s'agissant des prestations de services qui impliquent une intervention des autorités nationales, que l'obligation de transparence découlant de l'article 56 du traité sur le fonctionnement de l'Union européenne s'applique non pas à toute opération, mais uniquement à celles qui présentent un intérêt transfrontalier certain, du fait qu'elles sont objectivement susceptibles d'intéresser des opérateurs économiques établis dans d'autres Etats membres ; qu'elle a dit pour droit que cette obligation de transparence s'oppose à l'extension, par un Etat membre, à l'ensemble des employeurs et des travailleurs salariés d'une branche d'activité, d'un accord collectif, conclu par les organisations représentatives d'employeurs et de travailleurs salariés pour une branche d'activité, qui confie à un unique opérateur économique, choisi par les partenaires sociaux, la gestion d'un régime de prévoyance complémentaire obligatoire institué au profit des travailleurs salariés, sans que la réglementation nationale prévoie une publicité adéquate permettant à l'autorité publique compétente de tenir pleinement compte des informations soumises, relatives à l'existence d'une offre plus avantageuse ;<br/>
<br/>
              5. Considérant, en premier lieu, que les prestations objet de l'avenant du 23 novembre 2010 peuvent être légalement proposées par des entreprises d'assurance établies dans d'autres Etats membres de l'Union européenne ; que la circonstance que le marché de la protection sociale complémentaire présente aujourd'hui une faible ouverture aux entreprises établies dans d'autres Etats membres de l'Union européenne est sans incidence sur la faculté pour ces entreprises de présenter leurs offres ; que l'extension de l'avenant du 23 novembre 2010 donne vocation à l'organisme désigné à assurer durant trois ans, pour l'ensemble des salariés du secteur de l'immobilier, au nombre d'environ 140 000, la couverture des risques décès, incapacité de travail et invalidité et le remboursement de frais de santé, en complément des prestations de la sécurité sociale ; qu'eu égard à l'importance des montants que représentent les cotisations des employeurs et des salariés à ces régimes, à la taille nationale du marché considéré et à l'avantage que représente la désignation pour proposer d'autres services d'assurance, l'octroi du droit de gérer ces régimes présente, en dépit de la nécessité pour les entreprises intéressées de s'adapter aux contraintes réglementaires existantes, un intérêt transfrontalier certain ; que, par suite, l'obligation de transparence découlant de l'article 56 du traité sur le fonctionnement de l'Union européenne lui est applicable ;<br/>
<br/>
              6. Considérant, en second lieu, d'une part, qu'il ressort des pièces du dossier que la désignation de l'organisme assureur par les parties à l'avenant du 23 novembre 2010 n'a pas été précédée d'une publicité de nature à permettre de recueillir, ainsi que l'exige la jurisprudence précitée de la Cour de justice de l'Union européenne,  des informations relatives à l'existence, le cas échéant, d'une offre plus avantageuse ;<br/>
<br/>
              7. Considérant, d'autre part, il est vrai, que l'article D. 2261-3 du code du travail prévoit que l'adoption d'un arrêté d'extension doit être précédée de la publication au Journal officiel de la République française d'un avis, qui indique le lieu où l'accord a été déposé et invite les organisations et personnes intéressées à faire connaître leurs observations dans un délai de quinze jours ; que, toutefois, ni la mise à disposition du public de l'avenant, ni la publication au Journal officiel de la République française du 5 février 2011 de l'avis prévu par l'article D. 2261-3 du code du travail, eu égard notamment aux mentions dont il était assorti et au délai imparti pour soumettre des observations, et alors même que la décision d'extension n'a été, en l'espèce, prise que cinq mois plus tard, ne peuvent, même prises ensemble, être regardées comme ayant permis aux opérateurs intéressés de manifester leur intérêt pour la gestion des régimes de prévoyance considérés avant l'adoption de la décision d'extension ; qu'en outre, en l'absence de disposition explicite ou de pratique en ce sens, les opérateurs ne pouvaient raisonnablement envisager que le ministre chargé du travail refuse l'extension sollicitée par les signataires de l'accord au motif qu'une offre plus avantageuse aurait été portée à sa connaissance ; que, par suite, l'arrêté attaqué n'a pas été précédé d'une publicité adéquate permettant au ministre de tenir compte de l'existence d'une telle offre ;<br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que le ministre chargé du travail, saisi d'une demande tendant à ce qu'il étende un accord collectif, étant notamment tenu de s'assurer que l'extension demandée n'entraînerait pas, par elle-même, la violation de ces règles issues du droit de l'Union tel qu'interprété par la Cour de justice de l'Union européenne, ne pouvait légalement étendre l'avenant n° 48 à la convention collective nationale de l'immobilier, dont l'article 17 désigne, sur le fondement de l'article L. 912-1 du code de la sécurité sociale, l'Institution de prévoyance de groupe Mornay comme seul organisme assureur pour assurer la mutualisation des garanties prévues par l'avenant ; que les dispositions de l'arrêté étendant les stipulations de l'article 17 de cet avenant ne sont pas divisibles des autres dispositions de l'arrêté ;<br/>
<br/>
              9. Considérant qu'il résulte de tout ce qui précède que l'union requérante est fondée à demander l'annulation de l'arrêté qu'elle attaque, dans toutes celles de ses dispositions qui n'ont pas déjà été annulées par la décision du Conseil d'Etat du 30 décembre 2013 ;<br/>
<br/>
              Sur les conséquences de l'illégalité de l'arrêté attaqué :<br/>
<br/>
              10. Considérant que, par un arrêté du 21 décembre 2015, publié au Journal officiel de la République française le 24 décembre suivant, le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social a étendu l'avenant n° 65 à la convention collective nationale de l'immobilier, du 20 juillet 2015, relatif aux garanties collectives de prévoyance, qui prévoit qu'il se substitue notamment à l'accord du 23 novembre 2010 et qu'il prend effet le premier jour du quatrième mois suivant la publication au Journal officiel de l'arrêté ministériel procédant à son extension ; qu'il en résulte que l'arrêté attaqué a produit des effets jusqu'au 31 mars 2016 ;<br/>
<br/>
              11. Considérant que, dans son arrêt du 17 décembre 2015, la Cour de justice de l'Union européenne a dit pour droit que les effets de cet arrêt ne concernent pas les accords collectifs portant désignation d'un organisme unique pour la gestion d'un régime de prévoyance complémentaire ayant été rendus obligatoires par une autorité publique pour l'ensemble des employeurs et des travailleurs salariés d'une branche d'activité avant la date de prononcé de cet arrêt, sans préjudice des recours juridictionnels introduits avant cette date ; qu'elle a précisé au point 52 de son arrêt que le maintien des effets des décisions d'extension en cause au principal, au nombre desquelles figure l'arrêté attaqué, se justifiait, essentiellement, au regard de la situation des employeurs et des travailleurs salariés qui ont souscrit, sur le fondement des conventions collectives étendues en cause, un contrat de prévoyance complémentaire s'inscrivant dans un contexte social particulièrement sensible et qui ont ainsi conclu des engagements contractuels leur accordant des garanties de prévoyance complémentaire en se fondant sur une situation juridique que la Cour n'a précisée, en ce qui concerne la portée concrète de l'obligation de transparence découlant de l'article 56 du traité sur le fonctionnement de l'Union européenne, que dans cet arrêt ; que, dès lors, eu égard à ces motifs, il y a lieu de prévoir que les effets des dispositions de l'arrêté attaqué annulées par la présente décision doivent être regardés comme définitifs, sous réserve des actions contentieuses mettant en cause des actes pris sur son fondement, engagées avant l'arrêt de la Cour de justice de l'Union européenne, soit le 17 décembre 2015 ;<br/>
<br/>
              Sur les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              12. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 6 000 euros à verser à l'UNIS, pour l'ensemble de la procédure, au titre de l'article L. 761-1 du code de justice administrative ; que les dispositions de cet article font obstacle à ce qu'une somme soit mise au même titre à la charge de l'UNIS, qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêté du ministre du travail, de l'emploi et de la santé du 13 juillet 2011 portant extension d'avenants à la convention collective nationale de l'immobilier est annulé dans toutes celles de ses dispositions qui n'ont pas déjà été annulées par la décision du Conseil d'Etat du 30 décembre 2013.<br/>
Article 2 : Les effets produits par les dispositions de l'arrêté du 13 juillet 2011 annulées par la présente décision sont regardés comme définitifs, sous réserve des actions contentieuses mettant en cause des actes pris sur son fondement engagées avant le 17 décembre 2015. <br/>
Article 3 : L'Etat versera à l'UNIS la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions du Syndicat national des résidences de tourisme, du Syndicat national des professionnels immobiliers, de la Fédération des sociétés immobilières et foncières, du Syndicat national de l'urbanisme, de l'habitat et des administrateurs de biens CFE-CGC et de la Fédération des employés et des cadres Force ouvrière présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à l'Union des syndicats de l'immobilier, au Syndicat national des résidences de tourisme et à la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
Les autres défendeurs représentés par la SCP Gatineau, Fattaccini, avocat au Conseil d'Etat et à la Cour de cassation, seront informés de la présente décision par cette SCP.<br/>
Copie en sera adressée à la Fédération des services CFDT, à la Fédération des personnels du commerce, de la distribution et des services-CGT, à la CFTC des commerces, du service et de la force de vente, à la Fédération nationale des agents immobiliers, à la Fédération des entreprises publiques locales, à l'Institution de prévoyance du groupe Mornay et à Klésia prévoyance.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">15-05-01-04 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. RÈGLES APPLICABLES. LIBERTÉS DE CIRCULATION. LIBRE PRESTATION DE SERVICES. - CHAMP D'APPLICATION - PRESTATIONS DE SERVICES IMPLIQUANT UNE INTERVENTION DES AUTORITÉS NATIONALES - OPÉRATIONS PRÉSENTANT UN INTÉRÊT TRANSFRONTALIER CERTAIN - OCTROI À UN UNIQUE OPÉRATEUR D'UN RÉGIME DE PRÉVOYANCE COMPLÉMENTAIRE OBLIGATOIRE POUR UNE BRANCHE D'ACTIVITÉ - EXISTENCE EN L'ESPÈCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">62-01 SÉCURITÉ SOCIALE. ORGANISATION DE LA SÉCURITÉ SOCIALE. - INSTITUTION D'UN RÉGIME OBLIGATOIRE DE PROTECTION SOCIALE COMPLÉMENTAIRE DANS UNE BRANCHE D'ACTIVITÉ - APPLICATION DE L'OBLIGATION DE TRANSPARENCE DÉCOULANT DE LA LIBRE PRESTATION DE SERVICE (ART. 56 DU TFUE) - EXISTENCE EN L'ESPÈCE.
</SCT>
<ANA ID="9A"> 15-05-01-04 Selon la jurisprudence de la Cour de justice de l'Union européenne, s'agissant des prestations de services qui impliquent une intervention des autorités nationales, l'obligation de transparence découlant de l'article 56 du traité sur le fonctionnement de l'Union européenne (TFUE) s'applique non pas à toute opération, mais uniquement à celles qui présentent un intérêt transfrontalier certain, du fait qu'elles sont objectivement susceptibles d'intéresser des opérateurs économiques établis dans d'autres États membres [RJ1].... ,,En l'espèce, l'avenant du 23 novembre 2010  à la convention collective nationale de l'immobilier, étendu par arrêté, institue pour l'ensemble des salariés de la branche un régime obligatoire de prévoyance couvrant les risques décès, incapacité de travail et invalidité et un régime obligatoire de remboursement de frais de santé, et désigne pour trois ans l'unique organisme assureur des garanties de ces deux régimes. Les prestations objet de cet avenant peuvent être légalement proposées par des entreprises d'assurance établies dans d'autres Etats membres de l'Union européenne. La circonstance que le marché de la protection sociale complémentaire présente aujourd'hui une faible ouverture aux entreprises établies dans d'autres Etats membres de l'Union européenne est sans incidence sur la faculté pour ces entreprises de présenter leurs offres. Eu égard à l'importance des montants que représentent les cotisations des employeurs et des salariés à ces régimes, à la taille nationale du marché considéré et à l'avantage que représente la désignation pour proposer d'autres services d'assurance, l'octroi du droit de gérer ces régimes présente, en dépit de la nécessité pour les entreprises intéressées de s'adapter aux contraintes réglementaires existantes, un intérêt transfrontalier certain. Par suite, l'obligation de transparence découlant de l'article 56 du TFUE est applicable.</ANA>
<ANA ID="9B"> 62-01 Selon la jurisprudence de la Cour de justice de l'Union européenne, s'agissant des prestations de services qui impliquent une intervention des autorités nationales, l'obligation de transparence découlant de l'article 56 du traité sur le fonctionnement de l'Union européenne (TFUE) s'applique non pas à toute opération, mais uniquement à celles qui présentent un intérêt transfrontalier certain, du fait qu'elles sont objectivement susceptibles d'intéresser des opérateurs économiques établis dans d'autres États membres [RJ1].... ,,En l'espèce, l'avenant du 23 novembre 2010  à la convention collective nationale de l'immobilier, étendu par arrêté, institue pour l'ensemble des salariés de la branche un régime obligatoire de prévoyance couvrant les risques décès, incapacité de travail et invalidité et un régime obligatoire de remboursement de frais de santé, et désigne pour trois ans l'unique organisme assureur des garanties de ces deux régimes. Les prestations objet de cet avenant peuvent être légalement proposées par des entreprises d'assurance établies dans d'autres Etats membres de l'Union européenne. La circonstance que le marché de la protection sociale complémentaire présente aujourd'hui une faible ouverture aux entreprises établies dans d'autres Etats membres de l'Union européenne est sans incidence sur la faculté pour ces entreprises de présenter leurs offres. Eu égard à l'importance des montants que représentent les cotisations des employeurs et des salariés à ces régimes, à la taille nationale du marché considéré et à l'avantage que représente la désignation pour proposer d'autres services d'assurance, l'octroi du droit de gérer ces régimes présente, en dépit de la nécessité pour les entreprises intéressées de s'adapter aux contraintes réglementaires existantes, un intérêt transfrontalier certain. Par suite, l'obligation de transparence découlant de l'article 56 du TFUE est applicable.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., sur l'application de cette obligation lorsqu'un Etat membre confie à un unique opérateur un régime de prévoyance complémentaire obligatoire pour une branche d'activité, CJUE, 17 décembre 2015, aff. C-25/14 et C-26/14.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
