<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039217408</ID>
<ANCIEN_ID>JG_L_2019_10_000000409951</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/21/74/CETATEXT000039217408.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 14/10/2019, 409951, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-10-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>409951</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>Mme Tiphaine Pinault</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:409951.20191014</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au tribunal administratif de Nîmes de condamner l'Etat à lui verser une indemnité de 80 451,10 euros en réparation des préjudices moral et matériel qu'il estime avoir subis du fait de son licenciement. Par un jugement n° 1401926 du 8  juillet 2016, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n°16MA03351 du 21 février 2017, la cour administrative d'appel de Marseille a rejeté l'appel formé par M. A... contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés au secrétariat du contentieux du Conseil d'Etat les 21 avril et 21 juillet 2017, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'éducation ;<br/>
              - le code du travail ;<br/>
              - la loi n°84-16 du 11 janvier 1984 ;<br/>
              - le décret n°86-83 du 17 janvier 1986 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Tiphaine Pinault, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de M. A....<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. A..., agent contractuel en fonction au centre de formation des apprentis Victor Hugo géré par l'établissement public local d'enseignement Lycée Victor Hugo à Carpentras, a été licencié par une décision en date du 20 septembre 2012 de la proviseure de ce lycée. Par un jugement du 8 juillet 2016, le tribunal administratif de Nîmes a rejeté la demande de M. A... tendant à la condamnation de l'Etat à réparer les préjudices consécutifs à ce licenciement. Par le présent pourvoi, celui-ci demande l'annulation de l'arrêt du 21 février 2017 par lequel la cour administrative d'appel de Marseille a, au motif que les conclusions indemnitaires présentées contre l'Etat étaient mal dirigées, rejeté les conclusions de M. A....<br/>
<br/>
              2. En application des dispositions de l'article L. 6232-1 du code du travail auxquelles renvoie l'article L. 431-1 du code de l'éducation, la création des centres de formation d'apprentis fait l'objet de conventions qui peuvent être conclues, sur le territoire régional, entre la région et des établissements publics. Les lycées qui ont, en vertu des dispositions de l'article L. 421-1 du code de l'éducation, le statut d'" établissements publics locaux d'enseignement " et sont dotés de la personnalité morale, peuvent, à ce titre, être signataires d'une convention avec la région en vue de la création d'un centre de formation d'apprentis. La convention de création du centre de formation d'apprentis prévoit notamment les conditions dans lesquelles est établi le budget du centre qui, selon les dispositions de l'article R. 6233-2 du code du travail, est distinct de celui de l'organisme gestionnaire. En vertu de l'article R. 6233-5 du même code, la comptabilité d'un centre de formation d'apprentis est distincte de celle de l'organisme gestionnaire. Aux termes de l'article R. 6233-27 du code du travail, " Le directeur d'un centre de formation d'apprentis est responsable du fonctionnement pédagogique et administratif du centre, sous réserve des pouvoirs d'ordre administratif et financier appartenant à l'organisme gestionnaire précisés par la convention de création du centre ". L'article R. 6233-28 du même code dispose que " le personnel du centre est recruté sur proposition du directeur et est placé sous son autorité ".<br/>
<br/>
              3. Il résulte de la combinaison de ces dispositions qu'un établissement public local d'enseignement qui conclut, avec la région, une convention de création d'un centre de formation d'apprentis, peut être désigné comme établissement public support chargé d'en assurer la gestion administrative, financière et comptable, au moyen d'un budget et d'une comptabilité distinctes, et que le personnel contractuel du centre de formation d'apprentis, recruté sur proposition de son directeur, est placé sous son autorité. Dans ces conditions, alors même que, le cas échéant, le chef de l'établissement public local d'enseignement peut également exercer les fonctions de directeur du centre de formation d'apprentis et que les agents contractuels du centre formation d'apprentis relèvent, pour leur gestion, des dispositions de la loi du 11 janvier 1984 et de celles du décret du 17 janvier 1986 applicables aux agents non titulaires de l'Etat, ils sont des agents de l'établissement support du centre formation d'apprentis et non des agents de l'Etat, et les sommes qui leur sont dues à raison du contrat qui les lie à l'établissement support du centre formation d'apprentis, y compris l'indemnisation des fautes imputables à cet employeur lors de la conclusion, de la mise en oeuvre ou de la rupture de leur contrat, incombent à ce dernier.<br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond que M. A... a été recruté le 1er janvier 1985 pour exercer des fonctions d'enseignant au sein du centre formation d'apprentis créé sur le site de Carpentras et annexé au lycée professionnel Philippe de Girard d'Avignon, géré en dernier lieu par le lycée Victor Hugo de Carpentras. Il a bénéficié de contrats successifs à durée déterminée puis, à compter du 1er septembre 2006, d'un contrat à durée indéterminée signé avec le proviseur du lycée Victor Hugo en sa qualité de chef de l'établissement public support du centre formation d'apprentis. Dès lors, en jugeant que les fautes qui auraient été commises par la proviseure du lycée Victor Hugo à l'occasion de la procédure de licenciement dont M. A... a fait l'objet n'étaient pas de nature à engager la responsabilité de l'Etat, mais celle de l'établissement public local d'enseignement, la cour administrative d'appel de Marseille n'a pas commis d'erreur de droit. <br/>
<br/>
              5. Par ailleurs, en relevant que le requérant qui, en réponse au moyen d'ordre public soulevé par la cour, invoquait que la responsabilité de l'Etat pouvait en tout état de cause être engagée à raison de la collaboration de certains de ses services à la décision de licenciement, se prévalait d'un nouveau fait générateur distinct de celui précédemment invoqué, la cour, qui a suffisamment motivé son arrêt, n'a commis aucune erreur de droit en estimant qu'il présentait une demande nouvelle en appel et, à ce titre, irrecevable.<br/>
<br/>
              6. Il résulte de tout ce qui précède que M. A... n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque. Les dispositions de l'article L. 761-1 du code de justice administrative font, par suite, obstacle à ce qu'une somme soit mise, à ce titre, à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A... est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. B... A... et au ministre de l'éducation nationale et de la jeunesse.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
