<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042671433</ID>
<ANCIEN_ID>JG_L_2020_12_000000428621</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/67/14/CETATEXT000042671433.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 14/12/2020, 428621, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428621</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Sébastien Gauthier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:428621.20201214</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé à la Cour nationale du droit d'asile d'annuler la décision du 10 août 2018 par laquelle le directeur général de l'Office français de protection des réfugiés et des apatrides (OFPRA) a mis fin à son statut de réfugié sur le fondement du 2° de l'article L. 711-6 du code de l'entrée et du séjour des étrangers et du droit d'asile et de le rétablir dans ce statut. <br/>
<br/>
              Par une décision n° 18043129 du 21 décembre 2018, la Cour nationale du droit d'asile a annulé cette décision et a rétabli M. B... dans le statut de réfugié.<br/>
<br/>
              Par un pourvoi, enregistré le 5 mars 2019 au secrétariat du contentieux du Conseil d'Etat, l'OFPRA demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) de renvoyer l'affaire devant la Cour nationale du droit d'asile. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention de Genève du 28 juillet 1951 et le protocole signé à New York le 31 janvier 1967 relatifs au statut des réfugiés ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
              - le code de procédure pénale ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - Le rapport de M. Sébastien Gauthier, maître des requêtes en service extraordinaire, <br/>
<br/>
              - Les conclusions de M. Guillaume Odinet, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Foussard, Froger, avocat de l'Office français de protection des réfugiés et apatrides ; <br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par une décision en date du 10 août 2018, prise sur le fondement du 2° de l'article L. 711-6 du code de l'entrée et du séjour des étrangers et du droit d'asile, l'Office français de protection des réfugiés et apatrides (OFPRA) a mis fin au statut de réfugié dont bénéficiait, depuis le 10 décembre 2009, M. B..., de nationalité kosovare. L'OFPRA se pourvoit en cassation contre la décision en date du 21 décembre 2018 par laquelle la Cour nationale du droit d'asile a annulé cette décision et rétabli M. B... dans le statut de réfugié.<br/>
<br/>
              2. L'article L. 711-6 du code de l'entrée et du séjour des étrangers et du droit d'asile, dans sa rédaction applicable au litige, dispose que : " Le statut de réfugié peut être refusé ou il peut être mis fin à ce statut lorsque : (...) 2° La personne concernée a été condamnée en dernier ressort en France soit pour un crime, soit pour un délit constituant un acte de terrorisme ou puni de dix ans d'emprisonnement, et sa présence constitue une menace grave pour la société ".<br/>
<br/>
              3. Pour juger que M. B... ne pouvait être regardé comme ayant été jugé en dernier ressort et en déduire que le statut de réfugié ne pouvait lui être retiré en application des dispositions précitées du 2° de l'article L. 711-6 du code de l'entrée et du séjour des étrangers et du droit d'asile, la Cour nationale du droit d'asile a estimé que le jugement du tribunal correctionnel de Nancy du 9 octobre 2015, rendu par défaut, n'était pas définitif faute pour <br/>
M. B... d'avoir eu connaissance de la signification de ce jugement. En statuant ainsi, alors qu'elle ne pouvait, pour écarter l'existence d'une telle signification, s'en tenir aux seules déclarations de l'intéressé sans user de ses pouvoirs d'instruction pour recueillir toutes les informations pertinentes, la cour a commis une erreur de droit.<br/>
<br/>
              4. Il résulte de ce qui précède que, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, l'OFPRA est fondé à demander l'annulation de la décision de la Cour nationale du droit d'asile qu'il attaque.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La décision de la Cour nationale du droit d'asile en date du 21 décembre 2018 est annulée. <br/>
Article 2 : L'affaire est renvoyée à la Cour nationale du droit d'asile.<br/>
Article 3 : La présente décision sera notifiée à l'Office français de protection des réfugiés et des apatrides et à M. A... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
