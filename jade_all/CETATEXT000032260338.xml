<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032260338</ID>
<ANCIEN_ID>JG_L_2016_03_000000388762</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/26/03/CETATEXT000032260338.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème SSR, 16/03/2016, 388762, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-03-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>388762</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Bastien Lignereux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:388762.20160316</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une requête et un nouveau mémoire, enregistrés les 17 mars et 13 novembre 2015 au secrétariat du contentieux du Conseil d'Etat, l'association UFC Que Choisir demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 11 janvier 2015 du ministre de l'écologie, du développement durable et de l'énergie et du ministre de l'économie, de l'industrie et du numérique fixant le montant de la prime versée aux opérateurs d'effacement ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros sur le fondement de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le traité sur le fonctionnement de l'Union européenne ; <br/>
              - le règlement de la Commission européenne n° 1407/2013 du 18 décembre 2013 ;<br/>
              - le code de l'énergie ; <br/>
              - la loi n° 2013-312 du 15 avril 2013 ;<br/>
              - le décret n° 2014-764 du 3 juillet 2014 ;<br/>
              - la loi n° 2015-992 du 17 août 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bastien Lignereux, auditeur,<br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 271-1 du code de l'énergie, dans sa rédaction issue de la loi du 15 avril 2013 visant à préparer la transition vers un système énergétique sobre et portant diverses dispositions sur la tarification de l'eau et sur les éoliennes : " Un décret en Conseil d'Etat, pris sur proposition de la Commission de régulation de l'énergie, fixe la méthodologie utilisée pour établir les règles permettant la valorisation des effacements de consommation d'électricité sur les marchés de l'énergie et sur le mécanisme d'ajustement mentionné à l'article L. 321-10. / Ces règles prévoient la possibilité, pour un opérateur d'effacement, de procéder à des effacements de consommation, indépendamment de l'accord du fournisseur d'électricité des sites concernés, et de les valoriser sur les marchés de l'énergie ou sur le mécanisme d'ajustement mentionné au même article L. 321-10, ainsi qu'un régime de versement de l'opérateur d'effacement vers les fournisseurs d'électricité des sites effacés. (...)  / Une prime est versée aux opérateurs d'effacement, prenant en compte les avantages de l'effacement pour la collectivité, dans les conditions précisées au chapitre III du titre II du livre Ier " ; qu'aux termes de l'article 14 du décret du 3 juillet 2014 relatif aux effacements de consommation d'électricité, pris pour l'application de ces dispositions : " Un arrêté des ministres chargés de l'économie et de l'énergie, pris après avis de la Commission de régulation de l'énergie, fixe, par catégorie d'effacements, le montant de la prime prévue par les dispositions de l'article L. 123-1 du code de l'énergie, versée aux opérateurs d'effacement pour les effacements réalisés l'année civile suivant la publication de l'arrêté et certifiés par le gestionnaire du réseau de transport d'électricité. / Le montant de la prime fait l'objet d'un réexamen annuel par les ministres compétents après avis de la Commission de régulation de l'énergie émis avant le 1er novembre. / L'absence d'arrêté modificatif avant la fin de l'année civile en cours vaut reconduction pour l'année suivante " ;<br/>
<br/>
              2. Considérant qu'en application de ces dispositions, les ministres chargés de l'énergie et de l'économie ont, par un arrêté du 11 janvier 2015, fixé le montant de la prime versée aux opérateurs d'effacement pour les effacements réalisés jusqu'au 31 décembre 2015 ; que l'association UFC Que Choisir en demande l'annulation pour excès de pouvoir ;<br/>
<br/>
              Sur les conclusions tendant à l'annulation de l'arrêt attaqué :<br/>
<br/>
              3. Considérant qu'aux termes du paragraphe 1 de l'article 107 du traité sur le fonctionnement de l'Union européenne : " Sauf dérogations prévues par les traités, sont incompatibles avec le marché intérieur, dans la mesure où elles affectent les échanges entre États membres, les aides accordées par les États ou au moyen de ressources d'État sous quelque forme que ce soit qui faussent ou qui menacent de fausser la concurrence en favorisant certaines entreprises ou certaines productions " ; qu'aux termes de l'article 108 du même traité : " (...) 2. Si, après avoir mis les intéressés en demeure de présenter leurs observations, la Commission constate qu'une aide accordée par un État ou au moyen de ressources d'État n'est pas compatible avec le marché intérieur (...), elle décide que l'État intéressé doit la supprimer ou la modifier dans le délai qu'elle détermine. (...) / 3. La Commission est informée, en temps utile pour présenter ses observations, des projets tendant à instituer ou à modifier des aides. Si elle estime qu'un projet n'est pas compatible avec le marché intérieur, aux termes de l'article 107, elle ouvre sans délai la procédure prévue au paragraphe précédent. L'État membre intéressé ne peut mettre à exécution les mesures projetées, avant que cette procédure ait abouti à une décision finale. / 4. La Commission peut adopter des règlements concernant les catégories d'aides d'État que le Conseil a déterminées, conformément à l'article 109, comme pouvant être dispensées de la procédure prévue au paragraphe 3 du présent article " ;<br/>
<br/>
              4. Considérant qu'il résulte de ces stipulations que, s'il ressortit à la compétence exclusive de la Commission européenne de décider, sous le contrôle de la Cour de justice de l'Union européenne, si une aide de la nature de celles mentionnées à l'article 107 précité est ou non, compte tenu des dérogations prévues par le traité, compatible avec le marché intérieur, il incombe, en revanche, aux juridictions nationales de sanctionner, le cas échéant, l'invalidité de dispositions de droit national qui auraient institué ou modifié une telle aide en méconnaissance de l'obligation, qu'impose aux Etats membres le paragraphe 3 de l'article 108 du traité, d'en notifier le projet à la Commission, préalablement à toute mise à exécution ; qu'en vertu de la jurisprudence de la Cour de justice, la qualification d'aide d'État au sens de l'article 107 du traité suppose la réunion de quatre conditions, à savoir qu'il existe une intervention de l'État ou au moyen de ressources d'État, que cette intervention soit susceptible d'affecter les échanges entre les États membres, qu'elle accorde un avantage sélectif à son bénéficiaire et qu'elle fausse ou menace de fausser la concurrence ;<br/>
<br/>
              En ce qui concerne le critère de l'intervention de l'Etat ou au moyen de ressources d'Etat :<br/>
<br/>
              5. Considérant que, d'une part, la prime versée aux opérateurs d'effacement a été instituée par la loi du 15 avril 2013 et doit, dès lors, être considérée comme imputable à l'Etat ; que, d'autre part, en application de l'article L. 123-2 du code de l'énergie, la charge résultant de cette prime est assurée par la contribution au service public de l'électricité due par les consommateurs finals d'électricité installés sur le territoire national et mentionnée à l'article L. 121-10 de ce code ; qu'il résulte de l'interprétation des stipulations de l'article 107 du traité donnée par la Cour de justice de l'Union européenne, notamment dans l'arrêt du 19 décembre 2013 Association Vent de Colère ! Fédération nationale (aff. C-262/12), que " l'article 107, paragraphe 1, TFUE englobe tous les moyens pécuniaires que les autorités publiques peuvent effectivement utiliser pour soutenir des entreprises, sans qu'il soit pertinent que ces moyens appartiennent ou non de manière permanente au patrimoine de l'État ", qu' " en conséquence, même si les sommes correspondant à la mesure en cause ne sont pas de façon permanente en possession du Trésor public, le fait qu'elles restent constamment sous contrôle public, et donc à la disposition des autorités nationales compétentes, suffit pour qu'elles soient qualifiées de ressources d'État " et qu'enfin, " des fonds alimentés par des contributions obligatoires imposées par la législation de l'État membre, gérés et répartis conformément à cette législation peuvent être considérés comme des ressources d'État au sens de l'article 107, paragraphe 1, TFUE, même s'ils sont gérés par des entités distinctes de l'autorité publique " ; que la Cour en a déduit, dans cet arrêt, que les sommes issues de la contribution au service public de l'électricité, dès lors qu'elles sont sous le contrôle de la Caisse des dépôts et consignation, organisme contrôlé par l'Etat, doivent être qualifiées de ressources d'Etat ; qu'il en résulte que le versement de la prime litigieuse constitue une intervention au moyen de ressources d'Etat ;<br/>
<br/>
              En ce qui concerne les critères de l'octroi d'un avantage sélectif, de l'affectation des échanges entre Etats membres et de l'incidence sur la concurrence :<br/>
<br/>
              6. Considérant que la prime litigieuse est susceptible de favoriser les opérateurs d'effacement par rapport aux producteurs d'électricité, qui sont susceptibles d'être placés en situation de concurrence avec ces opérateurs, d'une part sur le mécanisme d'ajustement prévu aux articles L. 321-10 et suivants du code de l'énergie, sur lequel ils présentent également des offres d'ajustement, et d'autre part sur le marché de gros de l'électricité ; qu'eu égard à la libéralisation du secteur de l'électricité au niveau de l'Union européenne, cet avantage est susceptible d'affecter les échanges entre Etats membres et d'avoir une incidence sur la concurrence ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que le versement de la prime litigieuse aux opérateurs d'effacement a le caractère d'une aide d'Etat ; que cette aide n'est pas exemptée de notification sur le fondement des stipulations précitées du paragraphe 4 de l'article 108 du traité ; qu'en particulier, contrairement à ce que soutient le ministre chargé de l'énergie, elle ne peut être regardée comme exemptée sur le fondement de l'article 3 du règlement de la Commission européenne du 18 décembre 2013 relatif à l'application des articles 107 et 108 du traité sur le fonctionnement de l'Union européenne aux aides de minimis, dès lors qu'aucune disposition en vigueur, notamment dans l'arrêté attaqué, ne prévoit de plafonnement du montant total de primes versées pour un même bénéficiaire ; que le ministre chargé de l'énergie ne peut utilement invoquer à cet égard la circonstance qu'au total, moins de 5 euros de primes auraient été versés à l'ensemble des opérateurs d'effacement jusqu'à l'abrogation de l'article L. 123-1 du code de l'énergie par la loi du 17 août 2015 relative à la transition énergétique pour la croissance verte ;<br/>
<br/>
              8. Considérant que, dès lors que des aides pouvaient être octroyées individuellement à des opérateurs d'effacement sur le fondement de l'arrêté attaqué, sans qu'il soit besoin de mesures d'application supplémentaires, le projet d'institution de ces aides aurait dû être notifié à la Commission européenne en application des stipulations du paragraphe 3 de l'article 108 du traité ; qu'à défaut d'une telle notification préalable, l'arrêté attaqué est entaché d'illégalité ; <br/>
<br/>
              9. Considérant qu'il résulte de ce qui précède, et sans qu'il soit besoin d'examiner l'autre moyen de la requête, que la requérante est fondée à demander l'annulation de l'arrêté qu'elle attaque ; <br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              10. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 2 000 euros à verser à l'UFC Que Choisir, au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : L'arrêté du 11 janvier 2015 fixant le montant de la prime versée aux opérateurs d'effacement est annulé.<br/>
Article 2 : L'Etat versera à l'UFC Que Choisir une somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à l'UFC Que Choisir, à la ministre de l'environnement, de l'énergie et de la mer, chargée des relations internationales sur le climat et au ministre de l'économie, de l'industrie et du numérique.<br/>
Copie en sera adressée pour information à la Commission de régulation de l'énergie et à la société Réseau Transport Electricité.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
