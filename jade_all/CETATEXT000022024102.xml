<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000022024102</ID>
<ANCIEN_ID>JG_L_2010_03_000000326370</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/22/02/41/CETATEXT000022024102.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère sous-section jugeant seule, 26/03/2010, 326370, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2010-03-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>326370</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère sous-section jugeant seule</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Chantepy</PRESIDENT>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN</AVOCATS>
<RAPPORTEUR>M. Alain  Boulanger</RAPPORTEUR>
<COMMISSAIRE_GVT/>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 23 mars et 23 juin 2009 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. Pierre A, demeurant ... ; M. A demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêté du 6 juin 1995 liquidant sa pension de retraite en tant qu'il ne prend pas en compte la bonification d'ancienneté mentionnée au b) de l'article L. 12 du code des pensions civiles et militaires de retraite et de réviser cette pension ;<br/>
<br/>
              2°) d'enjoindre à l'Etat de modifier, dans un délai de deux mois à compter de la décision à intervenir, les conditions dans lesquelles la pension a été liquidée pour tenir compte de la bonification d'ancienneté et d'ordonner le versement des sommes dues en conséquence de cette revalorisation, outre les intérêts de droit à compter de la demande et les intérêts capitalisés ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le traité instituant la Communauté économique européenne, notamment son article 141 ; <br/>
<br/>
              Vu le code des pensions civiles et militaires de retraite ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alain Boulanger, chargé des fonctions de Maître des requêtes,  <br/>
<br/>
              - les observations de la SCP Masse-Dessen, Thouvenin, avocat de M. A, <br/>
<br/>
              - les conclusions de Mlle Anne Courrèges, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Masse-Dessen, Thouvenin, avocat de M. A ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant que M. A, ancien préfet à qui une pension de retraite avait été concédée par un arrêté du 6 juin 1995, se prévaut de l'absence de notification régulière de cet arrêté pour en demander l'annulation, au motif qu'il ne prend pas en compte la bonification pour enfants prévue par les dispositions, alors en vigueur, du b) de l'article L. 12 du code des pensions civiles et militaires de retraite ;<br/>
<br/>
              Considérant que ces dispositions, dans leur rédaction antérieure à l'intervention de la loi du 21 août 2003 portant réforme des retraites, prévoyaient, pour le calcul de la pension, une bonification d'ancienneté d'un an par enfant pour les personnes qui ont assuré l'éducation de leurs enfants, et en réservaient le bénéfice aux  femmes fonctionnaires  ; que, toutefois, le principe d'égalité des rémunérations, tel qu'il est affirmé par l'article 119 devenu 141 du traité instituant la Communauté européenne, aujourd'hui repris à l'article 157 du traité sur le fonctionnement de l'Union européenne, qui est applicable aux pensions servies par le régime de retraite des fonctionnaires, s'oppose à ce que l'avantage ainsi accordé aux personnes qui ont assuré l'éducation de leurs enfants soit réservé aux femmes, alors que les hommes ayant assuré l'éducation de leurs enfants en seraient exclus ; <br/>
<br/>
              Considérant qu'il suit de là qu'en tant qu'il ne prend pas en compte la bonification prévue par ce texte, alors qu'il n'est pas contesté que M. A a assuré l'éducation de ses deux enfants, l'arrêté du 6 juin 1995 portant concession de sa pension de retraite est entaché d'illégalité ; que, dès lors, sans que le ministre de la défense puisse utilement opposer à cet égard les dispositions de l'article L. 55 du code des pensions civiles et militaires de retraite, le requérant est fondé à en demander, pour ce motif, l'annulation dans cette mesure ;<br/>
<br/>
              Considérant, toutefois, qu'aux termes de l'article L. 53 du même code :  Lorsque, par suite du fait personnel du pensionné, la demande de liquidation ou de révision de la pension est déposée postérieurement à l'expiration de la quatrième année qui suit celle de l'entrée en jouissance normale de la pension, le titulaire ne peut prétendre qu'aux arrérages afférents à l'année au cours de laquelle la demande a été déposée et aux quatre années antérieures  ; <br/>
<br/>
              Considérant qu'un recours contentieux directement formé contre un arrêté de concession de pension en vue d'en remettre en cause le montant implique nécessairement, s'il est accueilli, que l'administration procède, en prenant un nouvel arrêté, à une nouvelle liquidation de la pension ; que par suite lorsque, comme en l'espèce, le titulaire d'une pension, qui n'en a pas demandé la révision dans le délai d'un an prévu à l'article L. 55 du code des pensions civiles et militaires de retraite, est néanmoins recevable à saisir directement le juge d'un recours contre un arrêté de concession qui n'avait pas fait l'objet d'une notification comportant l'indication des voies de recours, la demande ainsi présentée doit être regardée comme une demande de liquidation de pension, au sens de l'article L. 53 de ce code ; qu'il suit de là que l'administration est en pareille hypothèse en droit de lui opposer la prescription résultant de cette disposition, hormis le cas où le délai mis par l'intéressé à présenter une telle demande ne serait pas imputable à son fait personnel ; qu'en l'espèce, aucune circonstance ne faisait obstacle à ce que M. A présente, dès la notification de son titre de pension, un recours tendant à obtenir une nouvelle liquidation de sa pension de retraite, en contestant l'absence de la bonification dont il revendique le bénéfice ; <br/>
<br/>
              Considérant qu'il résulte de ce qui précède que M. A, qui a présenté sa requête tendant à obtenir une nouvelle liquidation de sa pension le 3 décembre 2001, ne peut prétendre aux arrérages de cette pension qu'à compter du 1er janvier 1997 ; qu'il y a lieu, dès lors, de prescrire au ministre de modifier les conditions dans lesquelles la pension de M. A lui a été concédée et de revaloriser rétroactivement cette pension à compter du 1er janvier 1997 ;<br/>
<br/>
              Considérant que M. A a droit aux intérêts des sommes qui lui sont dues à compter de la réception par l'administration, le 6 décembre 2001, de sa demande du 3 décembre 2001 ; qu'à la date du 23 mars 2009, à laquelle M. A a présenté des conclusions à fin de capitalisation des intérêts, il était dû plus d'une année d'intérêts ; que, dès lors, conformément aux dispositions de l'article 1154 du code civil, il y a lieu de faire droit à cette demande tant à cette date qu'à chaque échéance annuelle à compter de cette date ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de faire application des dispositions de l'article L. 761-1 du code de justice administrative et de mettre à la charge de l'Etat le versement à M. A de la somme de 3 000 euros à ce titre ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêté du 6 juin 1995 concédant à M. A sa pension est annulé en tant qu'il ne comporte pas le bénéfice de la bonification pour enfants.<br/>
Article 2 : Le ministre du budget, des comptes publics, de la fonction publique et de la réforme de l'Etat modifiera les conditions dans lesquelles la pension de M. A lui a été concédée et revalorisera rétroactivement cette pension à compter du 1er janvier 1997. <br/>
Article 3 : Les sommes dues à M. A porteront intérêt au taux légal à compter du 6 décembre 2001. Les intérêts échus le 23 mars 2009 seront capitalisés à cette date et à chaque échéance annuelle ultérieure pour produire eux-mêmes intérêts.<br/>
Article 4 : L'Etat versera une somme de 3 000 euros à M. A au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 5 : La présente décision sera notifiée à M. Pierre A, au ministre de l'intérieur, de l'outre-mer et des collectivités territoriales et au ministre du budget, des comptes publics et de la réforme de l'Etat.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
