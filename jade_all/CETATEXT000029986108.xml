<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029986108</ID>
<ANCIEN_ID>JG_L_2014_12_000000382898</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/98/61/CETATEXT000029986108.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème / 7ème SSR, 29/12/2014, 382898, Publié au recueil Lebon</TITRE>
<DATE_DEC>2014-12-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>382898</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème / 7ème SSR</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Paul Bernard</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CESSR:2014:382898.20141229</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le jugement n° 1406150 du 21 juillet 2014, enregistré le 22 juillet 2014 au secrétariat du contentieux du Conseil d'Etat,  par lequel le tribunal administratif de Melun, avant de statuer sur la  requête de M. B...A...tendant à l'annulation de la décision du 2 juillet 2014 du préfet du Pas-de-Calais lui faisant obligation de quitter le territoire français sans délai de départ volontaire sous réserve que sa demande d'asile soit rejetée par le directeur de l'Office français de protection des réfugiés et apatrides et ordonnant son maintien en rétention dans les locaux de la direction départementale de la police aux frontières du Pas-de-Calais ou de tout autre centre de rétention administrative durant cinq jours, a décidé, par application des dispositions de l'article L. 113-1 du code de justice administrative, de transmettre le dossier de cette demande au Conseil d'Etat, en soumettant à son examen les questions suivantes: <br/>
<br/>
              1°) Lorsque l'autorité administrative a, dans un premier temps, successivement ou concomitamment, décidé de l'éloignement et du placement en rétention d'un étranger, justifiant la mise en oeuvre de la procédure contentieuse prévue au III de l'article L. 512-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, puis a ultérieurement, mais préalablement à la tenue de l'audience, elle-même mis fin à la mesure de rétention, en abrogeant, sans le retirer et sans intervention du juge des libertés et de la détention, son arrêté de maintien en rétention, le traitement contentieux du dossier soumis par l'étranger à l'encontre duquel a été prise une décision portant obligation de quitter le territoire français sans délai de départ volontaire, relève-t-il, selon le cas, du I ou du II de l'article L. 512-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, et donc de la formation collégiale disposant d'un délai de trois mois pour statuer, ou, au contraire, du III du même article, et donc de la compétence du magistrat désigné, statuant dans les 72 heures et sans conclusions du rapporteur public ' <br/>
<br/>
              2°) La même solution est-elle applicable dans l'hypothèse où la fin de la rétention ne serait pas décidée par l'administration mais par le juge des libertés et de la détention ' <br/>
<br/>
              3°) Lorsque la cessation de la rétention a mis fin à la situation d'urgence que créait la perspective d'exécution d'office de la mesure d'éloignement, le juge des 72 heures reste-t-il compétent pour juger de la légalité de la décision de rétention ' <br/>
<br/>
              4°) Dans l'hypothèse où la compétence resterait, pour tout ou partie des décisions, celle du juge des 72 heures, le jugement pourrait-il intervenir alors que les conditions de la libération de l'étranger ne lui permettent matériellement pas d'être présent à l'audience, en méconnaissance de l'article L. 512-1-III du code de l'entrée et du séjour des étrangers et du droit d'asile qui prévoit la présence de l'intéressé ' <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
<br/>
              Vu le code de justice administrative, notamment son article L. 113-1 ;  <br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Paul Bernard, maître des requêtes, <br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              REND L'AVIS SUIVANT : <br/>
<br/>
<br/>
<br/>
              1. L'article L. 512-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, dans sa rédaction issue de la loi du 16 juin 2011 relative à l'immigration, à l'intégration et à la nationalité, détermine les conditions dans lesquelles le tribunal administratif statue sur les recours formés contre certaines des décisions qui visent à procéder à l'éloignement d'un étranger du territoire français. <br/>
<br/>
              Le paragraphe I de cet article dispose que l'étranger qui fait l'objet d'une obligation de quitter le territoire français avec un délai de départ volontaire peut, dans les trente jours suivant sa notification, demander au tribunal administratif d'annuler cette mesure ainsi que la décision relative au séjour, celle mentionnant le pays de destination et celle d'interdiction de retour qui l'accompagnent le cas échéant. Le tribunal administratif statue alors dans un délai de trois mois. Toutefois, si l'étranger fait ensuite l'objet d'un placement en rétention ou d'une assignation à résidence en application de l'article L. 561-2, le paragraphe I précise qu'il est alors statué selon la procédure prévue au III du même article.<br/>
<br/>
              Le paragraphe II, applicable à la contestation des mesures qui se rattachent à une obligation de quitter le territoire français qui ne laisse pas de délai de départ volontaire, prévoit que les mêmes décisions, ainsi que celle refusant un délai de départ volontaire, peuvent être contestées, dans un délai limité à quarante-huit heures, devant le tribunal administratif qui statue dans un délai de trois mois. Le paragraphe II prévoit, comme le paragraphe I, que si l'étranger fait ensuite l'objet d'un placement en rétention ou d'une assignation à résidence, il est alors statué selon la procédure prévue au III.<br/>
<br/>
              Le paragraphe III, applicable en cas de décision de placement en rétention ou d'assignation à résidence, prévoit que l'étranger peut demander au président du tribunal administratif l'annulation de cette décision dans les quarante-huit heures suivant sa notification, ainsi que l'annulation de l'obligation de quitter le territoire français, de la décision refusant un délai de départ volontaire, de la décision mentionnant le pays de destination et de la décision d'interdiction de retour sur le territoire français qui l'accompagnent le cas échéant, lorsque ces décisions sont notifiées avec la décision de placement en rétention ou d'assignation. Toutefois, si l'étranger est assigné à résidence, son recours en annulation peut porter directement sur l'obligation de quitter le territoire ainsi que, le cas échéant, sur la décision refusant un délai de départ volontaire, la décision mentionnant le pays de destination et la décision d'interdiction de retour sur le territoire français.<br/>
<br/>
              Dans le cadre de la procédure prévue au III, le président du tribunal administratif ou le magistrat qu'il délègue statue au plus tard soixante douze heures après sa saisine, à l'issue d'une audience publique qui se déroule sans conclusions du rapporteur public.<br/>
<br/>
              2. Il ressort des dispositions du III de l'article L. 512-1 du code de l'entrée et du séjour des étrangers et du droit d'asile que le législateur a entendu organiser une procédure spéciale afin que le juge administratif statue rapidement sur la légalité des mesures relatives à l'éloignement des étrangers, hors la décision refusant le séjour, lorsque ces derniers sont placés en rétention ou assignés à résidence. A cet effet, il a prévu que le président du tribunal administratif ou le magistrat qu'il délègue statue en soixante douze heures sur les conclusions tendant à l'annulation des décisions de placement en rétention ainsi que sur les conclusions qui lui sont concomitamment soumises et qui tendent à l'annulation de l'une ou de plusieurs des décisions mentionnées au III de l'article L. 512-1. S'agissant d'une assignation à résidence, il appartient au président du tribunal administratif ou au magistrat qu'il délègue de statuer dans ce cadre même s'il n'est pas saisi de conclusions dirigées contre l'assignation.<br/>
<br/>
              Il en résulte que la procédure spéciale du III de l'article L. 512-1 cesse d'être applicable dès lors qu'il est mis fin, pour quelque raison que ce soit, à la rétention ou l'assignation à résidence de l'étranger. Le jugement de l'ensemble des conclusions dont l'étranger avait saisi le tribunal relève alors d'une formation collégiale du tribunal administratif statuant dans le délai prévu au I de l'article L. 512-1.<br/>
<br/>
              3. Dans un souci de bonne administration de la justice, compte tenu notamment de la brièveté du délai imparti par les dispositions du I de l'article L. 512-1 pour le jugement de la demande, le tribunal administratif régulièrement saisi, par application des dispositions de l'article R. 776-16 du code de justice administrative, pour statuer selon la procédure du III de l'article L. 512-1 conserve compétence pour statuer sur le fondement du I de cet article. Toutefois, le président de ce tribunal peut transmettre le dossier au tribunal dans le ressort duquel se trouve le lieu de résidence de l'étranger, notamment lorsque celui-ci dispose d'un domicile stable.<br/>
<br/>
              4. Il résulte de ce qui précède que la quatrième question posée par le tribunal administratif de Melun est sans objet. <br/>
<br/>
<br/>
<br/>
<br/>
              Le présent avis sera notifié au tribunal administratif de Melun, à M. B...A...et au ministre de l'intérieur. <br/>
<br/>
              Il sera publié au Journal officiel de la République française.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-05-01 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE EN PREMIER RESSORT DES TRIBUNAUX ADMINISTRATIFS. - PROCÉDURE CONTENTIEUSE SPÉCIALE PRÉVUE PAR LE III DE L'ARTICLE L. 512-1 DU CESEDA (INTERVENTION DU JUGE DES 72 HEURES) - CHAMP D'APPLICATION DANS LE TEMPS - FIN - 1) PROCÉDURE CESSANT D'ÊTRE APPLICABLE DÈS QU'IL EST MIS FIN À LA RÉTENTION OU À L'ASSIGNATION À RÉSIDENCE - EXISTENCE - 2) CONSÉQUENCE SUR LA DÉTERMINATION DU TRIBUNAL ADMINISTRATIF TERRITORIALEMENT COMPÉTENT - PRINCIPE - MAINTIEN DE LA COMPÉTENCE DU TRIBUNAL RÉGULIÈREMENT SAISI EN APPLICATION DE L'ARTICLE R. 776-16 DU CJA - EXISTENCE - POSSIBILITÉ DE TRANSFÉRER LE DOSSIER AU TRIBUNAL DU LIEU DE RÉSIDENCE - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">335-01-04 ÉTRANGERS. SÉJOUR DES ÉTRANGERS. RESTRICTIONS APPORTÉES AU SÉJOUR. - PROCÉDURE CONTENTIEUSE SPÉCIALE PRÉVUE PAR LE III DE L'ARTICLE L. 512-1 DU CESEDA (INTERVENTION DU JUGE DES 72 HEURES) - CHAMP D'APPLICATION DANS LE TEMPS - FIN - 1) PROCÉDURE CESSANT D'ÊTRE APPLICABLE DÈS QU'IL EST MIS FIN À LA RÉTENTION OU À L'ASSIGNATION À RÉSIDENCE - EXISTENCE - 2) CONSÉQUENCE SUR LA DÉTERMINATION DU TRIBUNAL ADMINISTRATIF TERRITORIALEMENT COMPÉTENT - PRINCIPE - MAINTIEN DE LA COMPÉTENCE DU TRIBUNAL RÉGULIÈREMENT SAISI EN APPLICATION DE L'ARTICLE R. 776-16 DU CJA - EXISTENCE - POSSIBILITÉ DE TRANSFÉRER LE DOSSIER AU TRIBUNAL DU LIEU DE RÉSIDENCE - EXISTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">335-01-04-01 ÉTRANGERS. SÉJOUR DES ÉTRANGERS. RESTRICTIONS APPORTÉES AU SÉJOUR. ASSIGNATION À RÉSIDENCE. - PROCÉDURE CONTENTIEUSE SPÉCIALE PRÉVUE PAR LE III DE L'ARTICLE L. 512-1 DU CESEDA (INTERVENTION DU JUGE DES 72 HEURES) - CHAMP D'APPLICATION DANS LE TEMPS - FIN - 1) PROCÉDURE CESSANT D'ÊTRE APPLICABLE DÈS QU'IL EST MIS FIN À LA RÉTENTION OU À L'ASSIGNATION À RÉSIDENCE - EXISTENCE - 2) CONSÉQUENCE SUR LA DÉTERMINATION DU TRIBUNAL ADMINISTRATIF TERRITORIALEMENT COMPÉTENT - PRINCIPE - MAINTIEN DE LA COMPÉTENCE DU TRIBUNAL RÉGULIÈREMENT SAISI EN APPLICATION DE L'ARTICLE R. 776-16 DU CJA - EXISTENCE - POSSIBILITÉ DE TRANSFÉRER LE DOSSIER AU TRIBUNAL DU LIEU DE RÉSIDENCE - EXISTENCE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">335-03-03 ÉTRANGERS. OBLIGATION DE QUITTER LE TERRITOIRE FRANÇAIS (OQTF) ET RECONDUITE À LA FRONTIÈRE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. - PROCÉDURE CONTENTIEUSE SPÉCIALE PRÉVUE PAR LE III DE L'ARTICLE L. 512-1 DU CESEDA (INTERVENTION DU JUGE DES 72 HEURES) - CHAMP D'APPLICATION DANS LE TEMPS - FIN - 1) PROCÉDURE CESSANT D'ÊTRE APPLICABLE DÈS QU'IL EST MIS FIN À LA RÉTENTION OU À L'ASSIGNATION À RÉSIDENCE - EXISTENCE - 2) CONSÉQUENCE SUR LA DÉTERMINATION DU TRIBUNAL ADMINISTRATIF TERRITORIALEMENT COMPÉTENT - PRINCIPE - MAINTIEN DE LA COMPÉTENCE DU TRIBUNAL RÉGULIÈREMENT SAISI EN APPLICATION DE L'ARTICLE R. 776-16 DU CJA - EXISTENCE - POSSIBILITÉ DE TRANSFÉRER LE DOSSIER AU TRIBUNAL DU LIEU DE RÉSIDENCE - EXISTENCE.
</SCT>
<ANA ID="9A"> 17-05-01 Il ressort des dispositions du III de l'article L. 512-1 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA) que le législateur a entendu organiser une procédure spéciale afin que le juge administratif statue rapidement sur la légalité des mesures relatives à l'éloignement des étrangers, hors la décision refusant le séjour, lorsque ces derniers sont placés en rétention ou assignés à résidence. A cet effet, il a prévu que le président du tribunal administratif ou le magistrat qu'il délègue statue en soixante douze heures sur les conclusions tendant à l'annulation des décisions de placement en rétention ainsi que sur les conclusions qui lui sont concomitamment soumises et qui tendent à l'annulation de l'une ou de plusieurs des décisions mentionnées au III de l'article L. 512-1. S'agissant d'une assignation à résidence, il appartient au président du tribunal administratif ou au magistrat qu'il délègue de statuer dans ce cadre même s'il n'est pas saisi de conclusions dirigées contre l'assignation.,,,1) Il en résulte que la procédure spéciale du III de l'article L. 512-1 cesse d'être applicable dès lors qu'il est mis fin, pour quelque raison que ce soit, à la rétention ou l'assignation à résidence de l'étranger. Le jugement de l'ensemble des conclusions dont l'étranger avait saisi le tribunal relève alors d'une formation collégiale du tribunal administratif statuant dans le délai prévu au I de l'article L. 512-1.,,,2) Dans un souci de bonne administration de la justice, compte tenu notamment de la brièveté du délai imparti par les dispositions du I de l'article L. 512-1 pour le jugement de la demande, le tribunal administratif régulièrement saisi, par application des dispositions de l'article R. 776-16 du code de justice administrative (CJA), pour statuer selon la procédure du III de l'article L. 512-1 du CESEDA conserve compétence pour statuer sur le fondement du I de cet article. Toutefois, le président de ce tribunal peut transmettre le dossier au tribunal dans le ressort duquel se trouve le lieu de résidence de l'étranger, notamment lorsque celui-ci dispose d'un domicile stable.</ANA>
<ANA ID="9B"> 335-01-04 Il ressort des dispositions du III de l'article L. 512-1 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA) que le législateur a entendu organiser une procédure spéciale afin que le juge administratif statue rapidement sur la légalité des mesures relatives à l'éloignement des étrangers, hors la décision refusant le séjour, lorsque ces derniers sont placés en rétention ou assignés à résidence. A cet effet, il a prévu que le président du tribunal administratif ou le magistrat qu'il délègue statue en soixante douze heures sur les conclusions tendant à l'annulation des décisions de placement en rétention ainsi que sur les conclusions qui lui sont concomitamment soumises et qui tendent à l'annulation de l'une ou de plusieurs des décisions mentionnées au III de l'article L. 512-1. S'agissant d'une assignation à résidence, il appartient au président du tribunal administratif ou au magistrat qu'il délègue de statuer dans ce cadre même s'il n'est pas saisi de conclusions dirigées contre l'assignation.,,,1) Il en résulte que la procédure spéciale du III de l'article L. 512-1 cesse d'être applicable dès lors qu'il est mis fin, pour quelque raison que ce soit, à la rétention ou l'assignation à résidence de l'étranger. Le jugement de l'ensemble des conclusions dont l'étranger avait saisi le tribunal relève alors d'une formation collégiale du tribunal administratif statuant dans le délai prévu au I de l'article L. 512-1.,,,2) Dans un souci de bonne administration de la justice, compte tenu notamment de la brièveté du délai imparti par les dispositions du I de l'article L. 512-1 pour le jugement de la demande, le tribunal administratif régulièrement saisi, par application des dispositions de l'article R. 776-16 du code de justice administrative (CJA), pour statuer selon la procédure du III de l'article L. 512-1 du CESEDA conserve compétence pour statuer sur le fondement du I de cet article. Toutefois, le président de ce tribunal peut transmettre le dossier au tribunal dans le ressort duquel se trouve le lieu de résidence de l'étranger, notamment lorsque celui-ci dispose d'un domicile stable.</ANA>
<ANA ID="9C"> 335-01-04-01 Il ressort des dispositions du III de l'article L. 512-1 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA) que le législateur a entendu organiser une procédure spéciale afin que le juge administratif statue rapidement sur la légalité des mesures relatives à l'éloignement des étrangers, hors la décision refusant le séjour, lorsque ces derniers sont placés en rétention ou assignés à résidence. A cet effet, il a prévu que le président du tribunal administratif ou le magistrat qu'il délègue statue en soixante douze heures sur les conclusions tendant à l'annulation des décisions de placement en rétention ainsi que sur les conclusions qui lui sont concomitamment soumises et qui tendent à l'annulation de l'une ou de plusieurs des décisions mentionnées au III de l'article L. 512-1. S'agissant d'une assignation à résidence, il appartient au président du tribunal administratif ou au magistrat qu'il délègue de statuer dans ce cadre même s'il n'est pas saisi de conclusions dirigées contre l'assignation.,,,1) Il en résulte que la procédure spéciale du III de l'article L. 512-1 cesse d'être applicable dès lors qu'il est mis fin, pour quelque raison que ce soit, à la rétention ou l'assignation à résidence de l'étranger. Le jugement de l'ensemble des conclusions dont l'étranger avait saisi le tribunal relève alors d'une formation collégiale du tribunal administratif statuant dans le délai prévu au I de l'article L. 512-1.,,,2) Dans un souci de bonne administration de la justice, compte tenu notamment de la brièveté du délai imparti par les dispositions du I de l'article L. 512-1 pour le jugement de la demande, le tribunal administratif régulièrement saisi, par application des dispositions de l'article R. 776-16 du code de justice administrative (CJA), pour statuer selon la procédure du III de l'article L. 512-1 du CESEDA conserve compétence pour statuer sur le fondement du I de cet article. Toutefois, le président de ce tribunal peut transmettre le dossier au tribunal dans le ressort duquel se trouve le lieu de résidence de l'étranger, notamment lorsque celui-ci dispose d'un domicile stable.</ANA>
<ANA ID="9D"> 335-03-03 Il ressort des dispositions du III de l'article L. 512-1 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA) que le législateur a entendu organiser une procédure spéciale afin que le juge administratif statue rapidement sur la légalité des mesures relatives à l'éloignement des étrangers, hors la décision refusant le séjour, lorsque ces derniers sont placés en rétention ou assignés à résidence. A cet effet, il a prévu que le président du tribunal administratif ou le magistrat qu'il délègue statue en soixante douze heures sur les conclusions tendant à l'annulation des décisions de placement en rétention ainsi que sur les conclusions qui lui sont concomitamment soumises et qui tendent à l'annulation de l'une ou de plusieurs des décisions mentionnées au III de l'article L. 512-1. S'agissant d'une assignation à résidence, il appartient au président du tribunal administratif ou au magistrat qu'il délègue de statuer dans ce cadre même s'il n'est pas saisi de conclusions dirigées contre l'assignation.,,,1) Il en résulte que la procédure spéciale du III de l'article L. 512-1 cesse d'être applicable dès lors qu'il est mis fin, pour quelque raison que ce soit, à la rétention ou l'assignation à résidence de l'étranger. Le jugement de l'ensemble des conclusions dont l'étranger avait saisi le tribunal relève alors d'une formation collégiale du tribunal administratif statuant dans le délai prévu au I de l'article L. 512-1.,,,2) Dans un souci de bonne administration de la justice, compte tenu notamment de la brièveté du délai imparti par les dispositions du I de l'article L. 512-1 pour le jugement de la demande, le tribunal administratif régulièrement saisi, par application des dispositions de l'article R. 776-16 du code de justice administrative (CJA), pour statuer selon la procédure du III de l'article L. 512-1 du CESEDA conserve compétence pour statuer sur le fondement du I de cet article. Toutefois, le président de ce tribunal peut transmettre le dossier au tribunal dans le ressort duquel se trouve le lieu de résidence de l'étranger, notamment lorsque celui-ci dispose d'un domicile stable.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
