<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032940946</ID>
<ANCIEN_ID>JG_L_2016_07_000000390366</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/94/09/CETATEXT000032940946.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 27/07/2016, 390366, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-07-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>390366</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI ; SCP CELICE, BLANCPAIN, SOLTNER, TEXIDOR</AVOCATS>
<RAPPORTEUR>Mme Dorothée Pradines</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:390366.20160727</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              1° Sous le n° 390366, par une requête sommaire et un mémoire complémentaire, enregistrés les 22 mai et 24 août 2015 au secrétariat du contentieux du Conseil d'Etat, la Confédération nationale des avocats employeurs (CNAE), le Syndicat Avenir des barreaux de France patronal (ABFP), la Fédération des syndicats CFTC commerce, services et force de vente, la Chambre nationale des avocats en droit des affaires (CNADA) et le Syndicat national du personnel d'encadrement et assimilés, des avocats salariés des cabinets d'avocats, autres professions du droit et activités connexes (SPAAC-CGC) demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite de rejet née du silence gardé par le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social sur la demande de la SPAAC-CGC du 6 mai 2013 tendant à ce que le ministre chargé du travail engage la procédure d'extension de l'avenant n° 110 du 15 mars 2013 à la convention nationale du 20 février 1979 du personnel des cabinets d'avocats, relatif à l'instauration d'un régime de remboursement complémentaire de frais de santé ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Sous le n° 394156, par une requête, deux nouveaux mémoires et un mémoire en réplique, enregistrés les 20 octobre 2015, 19 novembre 2015, 4 décembre 2015 et 11 janvier 2016 au secrétariat du contentieux du Conseil d'Etat, la Confédération nationale des avocats employeurs, le Syndicat Avenir des barreaux de France patronal, la Fédération des syndicats CFTC commerce, services et force de vente, la Chambre nationale des avocats en droit des affaires, le Syndicat national du personnel d'encadrement et assimilés, des avocats salariés des cabinets d'avocats, autres professions du droit et activités connexes CGC et la Fédération des employés et cadres - Force ouvrière demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 21 août 2015 par laquelle le directeur général du travail a rejeté la demande de la SPAAC-CGC du 6 mai 2013 tendant à ce que le ministre chargé du travail engage la procédure d'extension de l'avenant n° 110 du 15 mars 2013 à la convention nationale du 20 février 1979 du personnel des cabinets d'avocats, relatif à l'instauration d'un régime de remboursement complémentaire de frais de santé ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - la Constitution ;<br/>
              - le code de la sécurité sociale ;<br/>
              - le code du travail ;<br/>
              - la décision du Conseil constitutionnel n° 2013-672 DC du 13 juin 2013 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Dorothée Pradines, auditeur,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, avocat de la Confédération nationale des avocats employeurs et autres, et à la SCP Célice, Blancpain, Soltner, Texidor, avocat du Syndicat employeur des cabinets d'avocats conseils d'entreprises ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces des dossiers que, le 6 mai 2013, le Syndicat national du personnel d'encadrement et assimilés, des avocats salariés des cabinets d'avocats, autres professions du droit et activités connexes CFE-CGC a demandé au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social d'engager la procédure d'extension de l'avenant n° 110 du 15 mars 2013, relatif au régime " frais de santé ", à la convention collective nationale des avocats et de leur personnel du 20 février 1979. Par deux requêtes qu'il y a lieu de joindre, la Confédération nationale des avocats employeurs, le Syndicat national du personnel d'encadrement et assimilés, des avocats salariés des cabinets d'avocats, autres professions du droit et activités connexes CFE-CGC et d'autres organisations syndicales demandent l'annulation pour excès de pouvoir, d'une part, de la décision implicite de rejet née du silence gardé par l'autorité administrative sur cette demande et, d'autre part, de la décision du 21 août 2015 par laquelle le directeur général du travail l'a expressément rejetée.<br/>
<br/>
              Sur la requête tendant à l'annulation de la décision implicite de refus d'extension :<br/>
<br/>
              2. Ainsi qu'il a été dit ci-dessus, le ministre chargé du travail a pris, le 21 août 2015, une décision expresse par laquelle il a refusé d'engager la procédure d'extension de l'avenant n° 110 du 15 mars 2013 à la convention collective nationale des avocats et de leur personnel. Les organisations requérantes ayant contesté cette dernière décision, les conclusions de leur requête dirigées contre la décision implicite de rejet de la demande du Syndicat national du personnel d'encadrement et assimilés, des avocats salariés des cabinets d'avocats, autres professions du droit et activités connexes CFE-CGC tendant à l'engagement de la procédure d'extension sont devenues sans objet. Il n'y a, dès lors, plus lieu d'y statuer et il n'y a pas lieu de faire droit aux conclusions qu'elles présentent au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Sur la requête tendant à l'annulation de la décision expresse du 21 août 2015 :<br/>
<br/>
              En ce qui concerne l'intervention du Syndicat employeur des cabinets d'avocats conseils d'entreprises : <br/>
<br/>
              3. Le Syndicat employeur des cabinets d'avocats conseils d'entreprises, dont les adhérents seraient tenus d'appliquer l'avenant litigieux si l'autorité administrative en prononçait l'extension, justifie d'un intérêt suffisant au maintien de la décision attaquée. Ainsi, son intervention est recevable.<br/>
<br/>
              En ce qui concerne la légalité de la décision du 21 août 2015 :<br/>
<br/>
              4. D'une part, aux termes de l'article L. 911-1 du code de la sécurité sociale : " A moins qu'elles ne soient instituées par des dispositions législatives ou réglementaires, les garanties collectives dont bénéficient les salariés, anciens salariés et ayants droit en complément de celles qui résultent de l'organisation de la sécurité sociale sont déterminées soit par voie de conventions ou d'accords collectifs, soit à la suite de la ratification à la majorité des intéressés d'un projet d'accord proposé par le chef d'entreprise, soit par une décision unilatérale du chef d'entreprise constatée dans un écrit remis par celui-ci à chaque intéressé ". Aux termes de l'article L. 2261-15 du code du travail, applicable à l'avenant objet du présent litige en vertu de l'article L. 911-3 du code de la sécurité sociale : " Les stipulations d'une convention de branche ou d'un accord professionnel ou interprofessionnel, répondant aux conditions particulières déterminées par la sous-section 2, peuvent être rendues obligatoires pour tous les salariés et employeurs compris dans le champ d'application de cette convention ou de cet accord, par arrêté du ministre chargé du travail, après avis motivé de la Commission nationale de la négociation collective. (...) ".<br/>
<br/>
              5. D'autre part, aux termes de l'article L. 912-1 du code de la sécurité sociale, dans sa rédaction antérieure à sa modification par la loi du 23 décembre 2013 de financement de la sécurité sociale pour 2014 : " Lorsque les accords professionnels ou interprofessionnels mentionnés à l'article L. 911-1 prévoient une mutualisation des risques dont ils organisent la couverture auprès d'un ou plusieurs organismes mentionnés à l'article 1er de la loi n° 89-1009 du 31 décembre 1989 renforçant les garanties offertes aux personnes assurées contre certains risques ou d'une ou plusieurs institutions mentionnées à l'article L. 370-1 du code des assurances, auxquels adhèrent alors obligatoirement les entreprises relevant du champ d'application de ces accords, ceux-ci comportent une clause fixant dans quelles conditions et selon quelle périodicité les modalités d'organisation de la mutualisation des risques peuvent être réexaminées. La périodicité du réexamen ne peut excéder cinq ans. / Lorsque les accords mentionnés ci-dessus s'appliquent à une entreprise qui, antérieurement à leur date d'effet, a adhéré ou souscrit un contrat auprès d'un organisme différent de celui prévu par les accords pour garantir les mêmes risques à un niveau équivalent, les dispositions du second alinéa de l'article L. 132-23 du code du travail sont applicables ".<br/>
<br/>
              6. Par sa décision n° 2013-672 DC du 13 juin 2013, le Conseil constitutionnel a déclaré l'article L. 912-1 du code de la sécurité sociale, dans sa rédaction alors en vigueur, contraire à la Constitution. Il a énoncé que cette déclaration d'inconstitutionnalité prenait effet à compter de la publication de sa décision mais n'était pas applicable aux contrats pris sur ce fondement, en cours lors de cette publication et liant les entreprises à celles qui sont régies par le code des assurances, aux institutions relevant du titre III du code de la sécurité sociale et aux mutuelles relevant du code de la mutualité.<br/>
<br/>
              7. Le Conseil constitutionnel a ainsi entendu préserver l'application jusqu'à leur terme, tel qu'il résulte de leurs stipulations et des dispositions du premier alinéa de l'article L. 912-1 du code de la sécurité sociale, des actes contractuels déjà conclus sur le fondement de cet article, en vertu desquels les entreprises ont une obligation d'adhésion à un organisme ou une institution chargé de la mutualisation des risques dont ces accords organisent la couverture. En revanche, sa décision fait obstacle à ce que l'autorité ministérielle puisse légalement, après la date à laquelle sa déclaration d'inconstitutionnalité a pris effet, étendre les stipulations d'un accord prévoyant une telle obligation d'adhésion et ainsi l'imposer à des entreprises qui, n'étant pas adhérentes à l'une des organisations d'employeurs signataires de l'accord, n'étaient pas liées par celui-ci. <br/>
<br/>
              8. Les parties à l'avenant n° 110 du 15 mars 2013 à la convention collective nationale des avocats et de leur personnel du 20 février 1979 ont, au point I.7 de cet avenant, désigné l'institution de prévoyance CREPA comme organisme assureur du régime institué par l'avenant en matière de remboursement de frais de soins de santé, en précisant au point I.8 que " sont obligatoirement affiliés au régime CREPA tous les cabinets, y compris, à compter de la date de publication au Journal officiel de l'arrêté d'extension de l'avenant, ceux non adhérents à l'une des organisations patronales signataires de la convention collective ou y ayant par la suite adhéré en totalité et sans réserve " et, au point III.3, que " ces dispositions s'appliquent pour tous les cabinets, y compris pour les cabinets ayant un contrat " Frais de santé " auprès d'un autre organisme assureur, avec des garanties identiques ou supérieures à celles définies par le présent avenant ".<br/>
<br/>
              9. Il résulte de ce qui a été dit ci-dessus que, contrairement à ce que soutiennent les organisations requérantes, le ministre chargé du travail, à la date du 21 août 2015 à laquelle il s'est prononcé, postérieure à la publication de la décision du Conseil constitutionnel du 13 juin 2013, était tenu de refuser d'étendre l'avenant n° 110 prévoyant l'obligation, pour les entreprises relevant du champ d'application de la convention,  d'adhérer à l'organisme assureur qu'il désigne, alors même que cet avenant a été conclu le 15 mars 2013. Il suit de là que les autres moyens de la requête sont sans incidence sur la légalité de la décision de refus attaquée.<br/>
<br/>
              10. Il résulte de ce qui précède que les requérants ne sont pas fondés à demander l'annulation de la décision attaquée du 21 août 2015.<br/>
<br/>
              En ce qui concerne les frais exposés par les parties à l'occasion du litige : <br/>
<br/>
              11. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle, d'une part, à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante, et, d'autre part, à ce qu'il soit fait droit aux conclusions présentées au même titre par le Syndicat employeur des cabinets d'avocats conseils d'entreprises qui, intervenant en défense, n'a pas la qualité de partie à la présente instance.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de statuer sur la requête n° 390366.  <br/>
Article 2 : L'intervention du Syndicat employeur des cabinets d'avocats conseils d'entreprises sur la requête n° 394156 est admise. <br/>
Article 3 : La requête n° 394156 est rejetée.<br/>
Article 4 : Les conclusions du Syndicat employeur des cabinets d'avocats conseils d'entreprises présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la Confédération nationale des avocats employeurs, premier requérant dénommé, au Syndicat employeur des cabinets d'avocats conseils d'entreprises et à la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
Les autres requérants en seront informés par la SCP Gatineau, Fattaccini, avocat au Conseil d'Etat et à la Cour de cassation, qui les représente devant le Conseil d'Etat.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
