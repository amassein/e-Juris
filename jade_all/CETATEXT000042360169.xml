<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042360169</ID>
<ANCIEN_ID>JG_L_2020_09_000000439572</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/36/01/CETATEXT000042360169.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 23/09/2020, 439572, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-09-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>439572</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR>M. Jean-Yves Ollier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:439572.20200923</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un mémoire, enregistré le 24 juin 2020 au secrétariat du contentieux du Conseil d'Etat, M. B... A... demande au Conseil d'Etat, à l'appui de son pourvoi tendant à l'annulation de l'arrêt n°s 17PA02241, 19PA00063 du 19 septembre 2019 de la cour administrative d'appel de Paris, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions de l'article 41 de la loi n° 2000-231 du 12 avril 2000, en tant qu'elles ne permettent pas aux militaires affectés en Polynésie française de bénéficier du principe de la prescription biennale de la répétition des créances résultat de paiement indus.  <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ; <br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - la loi organique n° 2004-192 du 27 février 2004 ;<br/>
              - les articles 37-1 et 41 de la loi n° 2000-321 du 12 avril 2000 ;<br/>
              - la loi n° 2011-1978 du 28 décembre 2011 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Yves Ollier, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de M. A... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. Aux termes de l'article 37-1 de la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations, issu du I de l'article 94 de la loi du 28 décembre 2011 de finances rectificative pour 2011, qui figure dans un titre intitulé " Dispositions relatives la fonction publique " : " Les créances résultant de paiements indus effectués par les personnes publiques en matière de rémunération de leurs agents peuvent être répétées dans un délai de deux années à compter du premier jour du mois suivant celui de la date de mise en paiement du versement erroné, y compris lorsque ces créances ont pour origine une décision créatrice de droits irrégulière devenue définitive. / Toutefois, la répétition des sommes versées n'est pas soumise à ce délai dans le cas de paiements indus résultant soit de l'absence d'information de l'administration par un agent de modifications de sa situation personnelle ou familiale susceptibles d'avoir une incidence sur le montant de sa rémunération, soit de la transmission par un agent d'informations inexactes sur sa situation personnelle ou familiale. / Les deux premiers alinéas ne s'appliquent pas aux paiements ayant pour fondement une décision créatrice de droits prise en application d'une disposition réglementaire ayant fait l'objet d'une annulation contentieuse ou une décision créatrice de droits irrégulière relative à une nomination dans un grade lorsque ces paiements font pour cette raison l'objet d'une procédure de recouvrement ". L'article 41 de la loi du 12 avril 2000, qui rend certaines dispositions de la loi de 2000 applicables en Polynésie française, ne comporte aucune mention sur l'applicabilité de l'article 37-1 dans ce territoire.<br/>
<br/>
              3. M. A... soutient que, par suite, les dispositions de l'article 41 de la loi du 12 avril 2000, en ce qu'elles ne rendent pas applicables aux militaires affectés en Polynésie française le bénéfice de la prescription biennale prévue par l'article 37-1 de la même loi, méconnaissent les principes d'égalité devant la loi et devant les charges publiques garantis par les articles 6 et 13 de la Déclaration des droits de l'homme et du citoyen.<br/>
<br/>
              4. L'article 7 de la loi organique du 27 février 2004 portant statut d'autonomie de la Polynésie française dispose que : " Dans les matières qui relèvent de la compétence de l'Etat, sont applicables en Polynésie française les dispositions législatives et réglementaires qui comportent une mention expresse à cette fin. / Par dérogation au premier alinéa, sont applicables de plein droit en Polynésie française, sans préjudice de dispositions les adaptant à son organisation particulière, les dispositions législatives et réglementaires qui sont relatives : (...) 5° Aux statuts des agents publics de l'Etat (...) ". <br/>
<br/>
              5. En tant qu'elles s'appliquent aux agents publics de l'Etat, les dispositions de l'article 37-1 de la loi du 12 mars 2000 entrent dans le champ d'application du 5° de l'article 7 de la loi organique du 27 février 2004. Elles sont en conséquence applicables de plein droit en Polynésie française, indépendamment de l'absence de toute mention à cet égard dans l'article 41 de la loi. <br/>
<br/>
              6. Il résulte de ce qui précède que la prescription biennale instaurée par l'article 37-1 de la loi du 12 avril 2000 s'applique de la même façon aux rémunérations perçues par un militaire lors d'une affectation en Polynésie française qu'à celles perçues lors d'une affectation en métropole ou dans un département d'outre-mer. Par suite, la question des atteintes portées par les dispositions de l'article 41 de la loi du 12 avril 2000 aux principes d'égalité devant la loi et devant les charges publiques garantis par les articles 6 et 13 de la Déclaration des droits de l'homme et du citoyen, qui n'est pas nouvelle, ne présente pas un caractère sérieux. Ainsi, sans qu'il soit besoin de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée, le moyen tiré de ce que l'article 41 de la loi du 12 avril 2000 porte atteinte aux droits et libertés garantis par la Constitution doit être écarté.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M. A....<br/>
Article 2: La présente décision sera notifiée à M. B... A... et à la ministre des armées. <br/>
Copie en sera adressée au Conseil constitutionnel et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
