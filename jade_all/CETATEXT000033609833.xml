<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033609833</ID>
<ANCIEN_ID>JG_L_2016_12_000000391717</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/60/98/CETATEXT000033609833.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 14/12/2016, 391717, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-12-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>391717</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Yannick Faure</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean Lessi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:391717.20161214</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              La Fédération des employés et cadres de la CGT Force ouvrière, l'Union départementale des syndicats de la CGT Force ouvrière du Val-d'Oise et le Syndicat Force ouvrière des employés et cadres du commerce du Val-d'Oise ont demandé au tribunal administratif de Cergy-Pontoise d'annuler pour excès de pouvoir l'arrêté du 5 mars 2010 par lequel le préfet du Val-d'Oise a créé un périmètre d'usage de consommation exceptionnel au sein de la zone des allées de Cormeilles, ZAC du Bois de Rochefort, située sur le territoire de la commune de Cormeilles-en-Parisis. Par un jugement n° 1007103 du 13 mai 2013, le tribunal administratif de Cergy-Pontoise a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 13VE02373 du 12 mai 2015, la cour administrative d'appel de Versailles a rejeté l'appel formé contre ce jugement du tribunal administratif de Cergy-Pontoise par la Fédération des employés et cadres de la CGT Force ouvrière, l'Union départementale des syndicats de la CGT Force ouvrière du Val-d'Oise et le Syndicat Force ouvrière des employés et cadres du commerce du Val-d'Oise.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 13 juillet et 13 octobre 2015 au secrétariat du contentieux du Conseil d'Etat, la Fédération des employés et cadres de la CGT Force ouvrière, l'Union départementale des syndicats de la CGT Force ouvrière du Val-d'Oise et le Syndicat Force ouvrière des employés et cadres du commerce du Val-d'Oise demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt de la cour administrative d'appel de Versailles du 12 mai 2015 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - la loi n° 2009-974 du 10 août 2009 ;<br/>
              - la loi n° 2015-990 du 6 août 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yannick Faure, auditeur,  <br/>
<br/>
              - les conclusions de M. Jean Lessi, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de la Fédération des employés et cadres de la CGT-Force ouvrière, de l'Union départementale des syndicats de la CGT Force ouvrière du Val-d'Oise et du Syndicat Force ouvrière des employées et cadres du commerce du Val-d'Oise.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 5 mars 2010, le préfet du Val-d'Oise a créé et délimité, sur demande du conseil municipal de la commune de Cormeilles-en-Parisis et après avis du conseil communautaire de la communauté de communes du Parisis, un périmètre d'usage de consommation exceptionnel correspondant au secteur des allées de Cormeilles - zone d'aménagement concerté du Bois de Rochefort, situé sur le territoire de cette commune. Par un jugement du 13 mai 2013, le tribunal administratif de Cergy-Pontoise a rejeté la demande de la Fédération des employés et cadres de la CGT Force ouvrière, de l'Union départementale des syndicats de la CGT Force ouvrière du Val-d'Oise et du Syndicat Force ouvrière des employés et cadres du commerce du Val-d'Oise tendant à l'annulation pour excès de pouvoir de cet arrêté. Par un arrêt du 12 mai 2015, contre lequel ces mêmes organisations syndicales se pourvoient en cassation, la cour administrative d'appel de Versailles a rejeté l'appel qu'elles avaient formé contre ce jugement.<br/>
<br/>
              2. Aux termes de l'article L. 3132-25-1, inséré dans le code du travail par la loi du 10 août 2009, dans sa rédaction applicable au litige, antérieure à l'intervention de la loi du 6 août 2015 : " (...) dans les unités urbaines de plus de 1 000 000 d'habitants, le repos hebdomadaire peut être donné, après autorisation administrative, par roulement, pour tout ou partie du personnel, dans les établissements de vente au détail qui mettent à disposition des biens et des services dans un périmètre d'usage de consommation exceptionnel caractérisé par des habitudes de consommation dominicale, l'importance de la clientèle concernée et l'éloignement de celle-ci de ce périmètre ".<br/>
<br/>
              3. En premier lieu, il résulte de ces dispositions, éclairées par les travaux préparatoires de la loi du 10 août 2009, que le législateur a entendu créer un régime de dérogation au repos dominical adapté à des situations locales particulières marquées par des usages de consommation de fin de semaine. Pour l'application de ce régime, la délimitation d'un tel périmètre est subordonnée à la constatation d'usages, en matière de consommation dominicale, suffisamment anciens, durables et réguliers pour être constitutifs d'habitudes, quelles que soient les conditions dans lesquelles celles-ci se sont formées. <br/>
<br/>
              4. Pour juger qu'existaient, au sens et pour l'application de ces dispositions, des habitudes de consommation dominicale, à la date de l'arrêté attaqué, soit le 5 mars 2010, la cour s'est fondée sur l'importance du chiffre d'affaires réalisé le dimanche par les trois principaux établissements de vente au détail du périmètre, hors secteur alimentaire, entre juin 2008, date d'ouverture du centre commercial, et décembre 2008, ainsi que, d'une part, sur la proximité du périmètre avec un centre commercial connaissant une importante activité dominicale et, d'autre part, sur la circonstance que les produits commercialisés au sein du périmètre litigieux " sont privilégiés par les consommateurs le dimanche ". En statuant ainsi, alors qu'il lui appartenait, pour apprécier l'existence d'habitudes de consommation dominicale dans le périmètre considéré, de vérifier le caractère durable et régulier des pratiques de consommation dominicale préexistantes, eu égard notamment à leur ancienneté, la cour a commis une erreur de droit.<br/>
<br/>
              5. En second lieu, si, en vertu des dispositions combinées des articles L. 3132-25-1 et L. 3132-25-3 du code du travail dans leur rédaction alors en vigueur, la dérogation au repos dominical résulte d'une autorisation administrative accordée à chacun des établissements de vente au détail situés au sein du périmètre d'usage de consommation exceptionnel, la création de ce périmètre a pour objet de délimiter la zone au sein de laquelle les établissements de vente au détail pourront, s'ils le souhaitent, bénéficier de plein droit d'une telle dérogation, sur la seule présentation d'un accord collectif ou, à défaut, d'une décision unilatérale de l'employeur prise après référendum. Par suite, la cour a également commis une erreur de droit en écartant les moyens tirés de la méconnaissance tant du principe d'égalité que du principe de libre concurrence, au seul motif que l'arrêté attaqué ne créait par lui-même aucune dérogation au repos dominical.<br/>
<br/>
              6. Il résulte de tout ce qui précède, sans qu'il soit besoin d'examiner les autres moyens de leur pourvoi, que les organisations syndicales requérantes sont fondées à demander l'annulation de l'arrêt qu'elles attaquent. <br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement d'une somme de 1 000 euros à chacune des organisations syndicales requérantes, au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Versailles du 12 mai 2015 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Versailles.<br/>
Article 3 : L'Etat versera une somme de 1 000 euros à la Fédération des employés et cadres de la CGT Force ouvrière, à l'Union départementale des syndicats de la CGT Force ouvrière du Val-d'Oise et au Syndicat Force ouvrière des employés et cadres du commerce du Val-d'Oise au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la Fédération des employés et cadres de la CGT Force ouvrière, premier requérant dénommé, et à la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
Les autres requérants seront informés de la présente décision par la SCP Rocheteau, Uzan-Sarano, avocat au Conseil d'Etat et à la Cour de cassation, qui les représente devant le Conseil d'Etat. <br/>
Copie en sera adressée à la commune de Cormeilles-en-Parisis.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
