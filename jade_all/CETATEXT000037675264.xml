<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037675264</ID>
<ANCIEN_ID>JG_L_2018_11_000000420849</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/67/52/CETATEXT000037675264.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 30/11/2018, 420849, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-11-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420849</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LEDUC, VIGAND</AVOCATS>
<RAPPORTEUR>M. Arnaud Skzryerbak</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:420849.20181130</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Paris d'annuler la décision du 10 août 2017 par laquelle le directeur de l'agence de Pôle emploi de Carpentras n'a que partiellement fait droit à sa demande d'aide à la mobilité, ainsi que la décision implicite de rejet de son recours gracieux, et de condamner Pôle emploi à lui verser la somme de 100 euros en réparation de son préjudice. Par une ordonnance n° 1801267 du 22 février 2018, le président du tribunal administratif de Paris a rejeté sa demande comme portée devant un ordre de juridiction incompétent pour en connaître.<br/>
<br/>
              Par un pourvoi, enregistré le 23 mai 2018 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de Pôle Emploi la somme de 3 500 euros à verser à son avocat, la SCP Leduc, Vigand, au titre des dispositions des articles 37 de la loi du 10 juillet 1991 et L. 761-1 du code de justice administrative. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arnaud Skzryerbak, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Leduc, Vigand, avocat de M.B....<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que M.B..., demandeur d'emploi domicilié.... Il a saisi le tribunal administratif de Paris d'une demande tendant, d'une part, à l'annulation de la décision du 10 août 2017 par laquelle il estime que l'agence de Pôle emploi de Carpentras n'a fait que partiellement droit à sa demande et, d'autre part, à la condamnation de Pôle emploi à l'indemniser à hauteur de 100 euros. Par une ordonnance du 22 février 2018, contre laquelle M. B... se pourvoit en cassation, le président du tribunal administratif de Paris a rejeté sa demande comme ne relevant manifestement pas de la compétence de la juridiction administrative, sur le fondement du 2° de l'article R. 222-1 du code de justice administrative.<br/>
<br/>
              2. Aux termes de l'article L. 5312-1 du code du travail, dans sa rédaction applicable au litige : " Pôle emploi est une institution nationale publique dotée de la personnalité morale et de l'autonomie financière qui a pour mission de : (...) / 2° Accueillir, informer, orienter et accompagner les personnes, qu'elles disposent ou non d'un emploi, à la recherche d'un emploi, d'une formation ou d'un conseil professionnel, prescrire toutes actions utiles pour développer leurs compétences professionnelles et améliorer leur employabilité, favoriser leur reclassement et leur promotion professionnelle, faciliter leur mobilité géographique et professionnelle et participer aux parcours d'insertion sociale et professionnelle (...) ". L'article L. 5312-7 du même code dispose que le budget de Pôle emploi comporte quatre sections parmi lesquelles " 3° La section " Intervention " comporte en dépenses les dépenses d'intervention concourant au placement, à l'orientation, à l'insertion professionnelle, à la formation et à l'accompagnement des demandeurs d'emploi ". Enfin, l'article R. 5312-6 de ce code prévoit que le conseil d'administration de Pôle emploi délibère notamment sur " 2° Les mesures destinées à faciliter les opérations de recrutement des entreprises, à favoriser l'insertion, le reclassement, la promotion professionnelle et la mobilité géographique et professionnelle des personnes, qu'elles disposent ou non d'un emploi, en application de la convention tripartite mentionnée à l'article L. 5312-3 ". <br/>
<br/>
              3. L'aide en litige constitue une aide à la recherche d'emploi, créée par la délibération n° 2008/04 du 19 décembre 2008 du conseil d'administration de Pôle emploi, établissement public à caractère administratif, dans le cadre de ses compétences et de sa mission propres de service public telles que prévues au 2° de l'article L. 5312-1 et au 3° de l'article L. 5312-7 du code du travail. Il en résulte que le contentieux portant sur l'attribution et le service de cette aide et la responsabilité encourue par l'établissement public du fait d'une décision afférente à une telle prestation ressortissent à la compétence de la juridiction administrative. Dès lors, le président du tribunal administratif de Paris a commis une erreur de droit en rejetant la demande de M. B...comme portée devant un ordre de juridiction incompétent pour en connaître. <br/>
<br/>
              4. Aux termes du premier alinéa de l'article L. 821-2 du code de justice administrative : " S'il prononce l'annulation d'une décision d'une juridiction administrative statuant en dernier ressort, le Conseil d'Etat peut soit renvoyer l'affaire devant la même juridiction statuant, sauf impossibilité tenant à la nature de la juridiction, dans une autre formation, soit renvoyer l'affaire devant une autre juridiction de même nature, soit régler l'affaire au fond si l'intérêt d'une bonne administration de la justice le justifie ".<br/>
<br/>
              5. Il ressort des pièces du dossier soumis au juge du fond que la décision attaquée a été prise par le directeur de l'agence de Pôle emploi de Carpentras. Par suite, eu égard aux dispositions de l'article R. 312-1 du code de justice administrative, il y a lieu de renvoyer l'affaire au tribunal administratif de Nîmes.<br/>
<br/>
              6. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions du pourvoi présentées au titre des dispositions des articles 37 de la loi du 10 juillet 1991 et L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du président du tribunal administratif de Paris du 22 février 2018 est annulée.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Nîmes.<br/>
Article 3 : Les conclusions du pourvoi présentées au titre des dispositions des articles 37 de la loi du 10 juillet 1991 et L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à M. A...B...et à Pôle emploi.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
