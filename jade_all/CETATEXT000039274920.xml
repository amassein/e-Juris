<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039274920</ID>
<ANCIEN_ID>JG_L_2019_10_000000417367</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/27/49/CETATEXT000039274920.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 24/10/2019, 417367, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-10-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417367</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Sylvain Monteillet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:417367.20191024</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. et Mme A... B... ont demandé au tribunal administratif de Strasbourg de prononcer la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles ils ont été assujettis au titre de l'année 2009 ainsi que des pénalités correspondantes. <br/>
<br/>
              Par un jugement n° 1305103 du 31 mai 2016, le tribunal administratif de Strasbourg a rejeté leur demande. <br/>
<br/>
              Par un arrêt n° 16NC01390 du 16 novembre 2017, la cour administrative d'appel de Nancy a ramené le montant des revenus distribués imposables entre leurs mains au titre de l'année 2009 de 48 990 euros à 42 425 euros, les a déchargés des impositions et pénalités correspondant à la réduction en base ainsi prononcée et a rejeté le surplus des conclusions de leur requête. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 16 janvier et 16 avril 2018 et le 9 octobre 2019 au secrétariat du contentieux du Conseil d'Etat, M. et Mme B... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'article 4 de cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire entièrement droit à leur appel.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ; <br/>
              - la décision n° 2014-404 QPC du 20 juin 2014 du Conseil constitutionnel ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Sylvain Monteillet, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de M. et Mme B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que le 10 décembre 2004, en vue d'augmenter le capital de la société civile Eden, dont ils étaient associés, M. B... et son épouse ont apporté chacun à cette société 500 parts de la société civile GMTN d'une valeur unitaire de 15,25 euros, soit un montant total de 15 250 euros. En contrepartie, ils ont reçu 200 000 parts de la société civile Eden d'une valeur nominale unitaire d'un euro. La plus-value d'apport, réalisée à l'occasion de cette opération, a bénéficié du régime du sursis d'imposition prévu à l'article 150-0 B du code général des impôts. Le 27 août 2009, l'assemblée générale des actionnaires de la société civile Eden a décidé une réduction du capital social de 178 900 euros pour ramener celui-ci de 201 000 euros à 22 110 euros par voie de diminution de la valeur de la part sociale unitaire d'un euro à 0,11 centimes d'euro. A cette occasion, la même assemblée a prévu que cette réduction de capital s'effectuerait sous la forme d'un remboursement partiel aux associés du prix de souscription des parts à hauteur de 0,89 euro par part pour un total de 178 890 euros. Estimant que cette opération de réduction du capital, par voie de diminution du montant nominal des titres de la société civile Eden devait s'analyser, au regard du droit fiscal, comme une distribution au profit des associés en application du 2° du 1 de l'article 109 du code général des impôts, l'administration fiscale a informé, par une proposition de rectification du 8 août 2012, M. et Mme B... qu'elle envisageait d'imposer la somme correspondante dans la catégorie des revenus distribués. M. et Mme B... ont saisi le tribunal administratif de Strasbourg d'une demande tendant à la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles ils ont été assujettis au titre de l'année 2009 ainsi que des pénalités correspondantes. Par un jugement du 31 mai 2016, le tribunal administratif de Strasbourg a rejeté leur demande. La cour administrative d'appel de Nancy a ramené le montant des revenus distribués imposables de 48 990 à 42 425 euros, a déchargé M. et Mme B... des impositions et pénalités correspondant à la réduction en base qu'elle avait prononcée, a réformé le jugement du tribunal en ce qu'il avait de contraire à son arrêt et a rejeté le surplus des conclusions de la requête. M. et Mme B... demandent l'annulation de l'article 4 de son arrêt.  <br/>
<br/>
              Sur la régularité de l'arrêt : <br/>
<br/>
              2. Si la cour a opéré un nouveau calcul des revenus distribués qui l'a conduite à prononcer une décharge très partielle, elle a confirmé le bien-fondé du redressement et les motifs retenus par le tribunal. Dans ces conditions, la cour, qui n'était pas saisie d'une contestation des pénalités pour manquement délibéré, les requérants s'étant bornés à indiquer que, dans le cadre de l'effet dévolutif de l'appel, ils reprenaient leur argumentation de première instance, n'était pas tenue de se prononcer à nouveau sur le bien-fondé des pénalités. Le moyen tiré de l'irrégularité de l'arrêt attaqué doit, par suite, être écarté. <br/>
<br/>
              Sur le bien-fondé de l'arrêt :<br/>
<br/>
              3. D'une part, aux termes de l'article 109 du code général des impôts : " 1. Sont considérés comme revenus distribués : / (...) / 2° Toutes les sommes ou valeurs mises à la disposition des associés, actionnaires ou porteurs de parts et non prélevées sur les bénéfices (...) ". Aux termes de l'article 112 du même code, dans sa rédaction applicable en 2009 : " Ne sont pas considérés comme revenus distribués : 1° Les répartitions présentant pour les associés ou actionnaires le caractère de remboursements d'apports ou de primes d'émission. Toutefois, une répartition n'est réputée présenter ce caractère que si tous les bénéfices et les réserves autres que la réserve légale ont été auparavant répartis. Les dispositions prévues à la deuxième phrase ne s'appliquent pas lorsque la répartition est effectuée au titre du rachat par la société émettrice de ses propres titres (...) ".<br/>
<br/>
              4. D'autre part, aux termes de l'article 150-0 A du code général des impôts : " I. - 1. Sous réserve des dispositions propres aux bénéfices industriels et commerciaux, aux bénéfices non commerciaux et aux bénéfices agricoles ainsi que des articles 150 UB et 150 UC, les gains nets retirés des cessions à titre onéreux, effectuées directement, par personne interposée ou par l'intermédiaire d'une fiducie, de valeurs mobilières, de droits sociaux, de titres mentionnés au 1° de l'article 118 et aux 6° et 7° de l'article 120, de droits portant sur ces valeurs, droits ou titres ou de titres représentatifs des mêmes valeurs, droits ou titres ou de titres représentatifs des mêmes valeurs, droits ou titres, sont soumis à l'impôt sur le revenu. (...) ". Aux termes de l'article 150-0 B du même code, dans sa rédaction applicable au litige : " Les dispositions de l'article 150-0 A ne sont pas applicables, au titre de l'année de l'échange des titres, aux plus-values réalisées dans le cadre d'une opération d'offre publique, de fusion, de scission, d'absorption d'un fonds commun de placement par une société d'investissement à capital variable, de conversion, de division, ou de regroupement, réalisée conformément à la réglementation en vigueur ou d'un apport de titres à une société soumise à l'impôt sur les sociétés (...) ". <br/>
<br/>
              5. En adoptant les dispositions précitées de l'article 150-0-B, le législateur a entendu faciliter les opérations de restructuration d'entreprises, en vue de favoriser la création et le développement de celles-ci, par l'octroi automatique d'un sursis d'imposition pour les plus-values résultant de certaines de ces opérations, notamment d'échanges de titres. Il a, pour ce faire, entendu assurer la neutralité au plan fiscal de ces opérations d'échanges de titres et, à cette fin, sauf lorsqu'il en a disposé autrement, regarder de telles opérations comme des opérations intercalaires. Il en résulte qu'eu égard à cet objectif et en l'absence de dispositions contraires, lorsque les titres d'une société sont apportés par un contribuable soumis à l'impôt sur le revenu qui reçoit, en échange, des titres de la société bénéficiaire de l'apport et bénéficie, s'agissant du gain le cas échéant réalisé à cette occasion, du régime du sursis automatique d'imposition prévu par l'article 150-0 B, les titres reçus en rémunération de l'apport doivent être réputés être entrés dans le patrimoine de l'apporteur aux conditions dans lesquelles y étaient entrés les titres dont il a fait apport. <br/>
<br/>
              6. Si la société bénéficiaire de l'apport procède à une réduction de son capital social, non motivée par des pertes, par réduction de la valeur nominale de ses titres, les sommes mises en conséquence à la disposition d'un associé qui a acquis ces titres en rémunération de l'apport de titres d'une autre société ne peuvent constituer des remboursements d'apports non constitutifs de revenus distribués, au sens du 1° de l'article 112 du code général des impôts et sous réserve du respect des conditions auxquelles ces dispositions subordonnent leur application, que dans la limite des apports initialement consentis par cet associé à la société dont il a apporté les titres.<br/>
<br/>
              7. En premier lieu, la cour, pour juger que la somme reçue par les associés dans le cadre de l'opération de réduction du capital social de la société Eden constituait un revenu distribué, imposable dans la catégorie des revenus de capitaux mobiliers, s'est fondée sur la circonstance qu'ils ne pouvaient utilement soutenir qu'ils avaient conservé leurs titres au sein de cette société et que cette opération ne pouvait être regardée comme un événement ayant mis fin au régime de sursis d'imposition de l'article 150-0 B du code général des impôts sous lequel les requérants avaient placé la plus-value d'apport précédemment réalisée en 2004. En statuant ainsi, la cour n'a ni dénaturé les termes du litige ni commis d'erreur de droit dès lors que la circonstance que la plus-value réalisée à l'occasion de l'apport a été placée en sursis d'imposition est sans incidence sur l'imposition dans la catégorie des revenus distribués. <br/>
<br/>
              8. En second lieu, M. et Mme B... soutiennent que la cour aurait dû faire application d'office de la réserve d'interprétation posée par le Conseil constitutionnel dans sa décision n° 2014-404 QPC du 20 juin 2014. Toutefois, cette réserve portait sur l'interprétation du 6° de l'article 112 du code général des impôts qui portait sur le rachat de titres. La cour n'a commis aucune erreur de droit en ne faisant pas application de cette réserve d'interprétation dès lors que le 1° de l'article 112 du code général des impôts, qui est le fondement légal de l'imposition des requérants, est relatif aux répartitions présentant pour les associés ou actionnaires le caractère de remboursements d'apports ou de primes d'émission.<br/>
<br/>
              9. Il résulte de ce qui précède que le pourvoi de M. et Mme B... ne peut qu'être rejeté. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. et Mme B... est rejeté. <br/>
Article 2 : La présente décision sera notifiée à M. et Mme A... B... et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
