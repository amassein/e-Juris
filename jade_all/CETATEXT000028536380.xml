<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028536380</ID>
<ANCIEN_ID>JG_L_2014_01_000000373218</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/53/63/CETATEXT000028536380.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 27/01/2014, 373218, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-01-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>373218</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Christian Fournier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:373218.20140127</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance n° 1301128 du 4 novembre 2013, enregistrée le 8 novembre 2013 au secrétariat du contentieux du Conseil d'Etat, par laquelle le président de la troisième chambre du tribunal administratif d'Amiens, avant qu'il soit statué sur le déféré du préfet de l'Oise tendant à l'annulation de la décision du 21 mars 2013 par laquelle le président du centre de gestion de la fonction publique territoriale de l'Oise a indiqué qu'il refusait de fournir directement aux agents des collectivités territoriales et établissements affiliés l'assistance juridique statutaire prévue par les dispositions du 14° du II de l'article 23 de la loi n° 84-53 du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale, a décidé, par application de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution de l'article 23 de la loi du 26 janvier 1984, dans sa rédaction issue de l'article 113 de la loi n° 2012-347 du 12 mars 2012 ;<br/>
<br/>
              Vu le mémoire, enregistré le 25 juillet 2013 au greffe du tribunal administratif d'Amiens, présenté par le centre de gestion de la fonction publique territoriale de l'Oise, représenté par son président, en application de l'article 23-1 de l'ordonnance du 7 novembre 1958 ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule et son article 61-1 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
              Vu la loi n° 84-53 du 26 janvier 1984 ; <br/>
<br/>
              Vu la loi n° 2012-347 du 12 mars 2012, notamment son article 113 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christian Fournier, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2. Considérant que le centre de gestion de la fonction publique territoriale de l'Oise conteste la conformité à la Constitution des dispositions ajoutées par l'article 113 de la loi du 12 mars 2012 au II de l'article 23 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale, en vertu desquelles les centres de gestion de la fonction publique territoriale sont tenus d'assurer, pour leurs propres fonctionnaires et pour l'ensemble des fonctionnaires des collectivités territoriales et établissements publics qui leur sont affiliés, les six nouvelles missions suivantes : " 9° bis Le secrétariat des commissions de réforme ", " 9° ter Le secrétariat des comités médicaux ", " 13° Un avis consultatif dans le cadre de la procédure du recours administratif préalable dans les conditions prévues à l'article 23 de la loi du 30 juin 2000 relative au référé devant les juridictions administratives ", " 14° Une assistance juridique statutaire ", " 15° Une assistance au recrutement et un accompagnement individuel de la mobilité des agents hors de leur collectivité ou établissement d'origine " et " 16° Une assistance à la fiabilisation des comptes de droits en matière de retraite " ;<br/>
<br/>
              3. Considérant, en premier lieu, que le centre de gestion de la fonction publique territoriale de l'Oise a posé la question prioritaire de constitutionnalité à l'occasion d'un litige relatif aux conditions de mise en oeuvre de la seule mission " assistance juridique statutaire " mentionnée au 14° du II de l'article 23 de la loi du 26 janvier 1984 ; que les dispositions définissant les cinq autres missions mentionnées aux 9° bis, 9° ter, 13°, 15° et 16° du II de cet article, qui sont distinctes de la mission d'assistance juridique statutaire assignée aux centres de gestion de la fonction publique territoriale, sont dénuées de rapport avec les termes de ce litige ; que si le IV de l'article 23 de la loi du 26 janvier 1984 prévoit que les missions mentionnées aux 9° bis, 9° ter et 13° à 16° du II constituent un " appui technique indivisible à la gestion des ressources humaines ", cette disposition, qui a pour seul objet d'obliger les collectivités territoriales et établissements non affiliés souhaitant confier l'exercice de ces missions à un centre de gestion de le faire sans pouvoir choisir entre elles, n'a pas pour effet de rendre l'ensemble des dispositions définissant ces missions applicables au litige au sens et pour l'application des dispositions de l'article 23-5 de l'ordonnance du 7 novembre 1958 ;<br/>
<br/>
              4. Considérant, en deuxième lieu, qu'aux termes du troisième alinéa de l'article 14 de la loi du 26 janvier 1984 : " Sous réserve des dispositions des I et III de l'article 23 de la même loi, les collectivités territoriales et établissements non affiliés à un centre de gestion assurent par eux-mêmes les missions confiées aux centres de gestion (...) " ; qu'il résulte de ces dispositions que la mission d'assistance juridique statutaire aux fonctionnaires territoriaux mentionnée au 14° du II de l'article 23 de cette même loi est assurée soit par les centres de gestion de la fonction publique territoriale pour le compte des collectivités territoriales et établissements publics qui leur sont affiliés, soit directement par les collectivités territoriales et établissements publics qui ne sont pas affiliés à un centre de gestion ; que, par suite, en réservant aux fonctionnaire territoriaux relevant d'un employeur affilié à un centre de gestion de la fonction publique territoriale le bénéfice du droit à obtenir une assistance juridique statutaire, les dispositions du 14° du II de l'article 23 de la loi du 26 janvier 1984 ne méconnaissent pas le principe d'égalité de traitement entre fonctionnaires appartenant à un même cadre d'emplois ;<br/>
<br/>
              5. Considérant, en troisième lieu, que si les dispositions contestées sont susceptibles d'imposer aux centres de gestion de la fonction publique territoriale une contrainte en termes d'organisation de leurs services et aux collectivités territoriales et établissements publics une limitation à la liberté de gestion de leur personnel, cette contrainte, à la supposer établie, et cette limitation visent à accorder des garanties communes à l'ensemble des fonctionnaires territoriaux ; qu'eu égard, d'une part, à cet objectif d'intérêt général et, d'autre part, à la portée très limitée des contraintes et limitations imposées aux centres de gestion et aux collectivités territoriales et établissements publics en relevant, le requérant ne saurait sérieusement soutenir que les dispositions du 14° du II de 1'article 23 de la loi du 26 janvier 1984 méconnaissent le principe de la libre administration des collectivités territoriales ni, en tout état de cause, des centres de gestion de la fonction publique territoriale, et instaurent une tutelle des centres de gestion sur les collectivités territoriales ;<br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux ; que, par suite, il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité transmise par le tribunal administratif d'Amiens.<br/>
Article 2 : La présente décision sera notifiée au centre de gestion de la fonction publique territoriale de l'Oise, au ministre de l'intérieur et à la ministre de la réforme de l'Etat, de la décentralisation et de la fonction publique.<br/>
Copie en sera communiquée au Conseil constitutionnel, au Premier ministre ainsi qu'au tribunal administratif d'Amiens.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
