<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031259740</ID>
<ANCIEN_ID>JG_L_2015_09_000000384868</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/25/97/CETATEXT000031259740.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème SSJS, 30/09/2015, 384868, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-09-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384868</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>M. Bruno Bachini</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:384868.20150930</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés les 29 septembre et 24 décembre 2014 au secrétariat du contentieux du Conseil d'Etat, la commune de Colombes, représentée par son maire, demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite née du silence gardé par le Premier ministre sur sa demande du 28 mai 2014 tendant à l'abrogation du décret n° 2013-77 du 24 janvier 2013 relatif à l'organisation du temps scolaire dans les écoles maternelles et élémentaires ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - la Constitution, notamment ses articles 72 et 72-2 ;<br/>
              - le code de l'éducation ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - la loi n° 2000-321 du 12 avril 2000 ;<br/>
              - le décret n° 2013-77 du 24 janvier 2013 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Bachini, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la commune de Colombes ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que le décret du 24 janvier 2013, dont l'abrogation a été demandée au Premier ministre par la commune requérante, modifie l'organisation de la semaine scolaire des écoles maternelles et élémentaires, régie par les articles D. 521-10 à D. 521-13 du code de l'éducation ; qu'il prévoit que cette semaine comporte vingt-quatre heures d'enseignement, réparties sur neuf demi-journées, et que le conseil d'école, la commune ou l'établissement public de coopération intercommunale peuvent transmettre un projet d'organisation de la semaine scolaire au directeur académique des services de l'éducation nationale qui est chargé, par délégation du recteur d'académie, d'arrêter l'organisation de la semaine scolaire dans chaque école du département dont il a la charge ; qu'aux termes du deuxième alinéa de l'article D. 521-10 du code de l'éducation, dans sa rédaction issue de ce décret : " Les heures d'enseignement sont organisées les lundi, mardi, jeudi et vendredi et le mercredi matin, à raison de cinq heures trente maximum par jour et de trois heures trente maximum par demi-journée " ; que les élèves peuvent, en vertu de l'article D. 521-13 du code, bénéficier d'activités pédagogiques complémentaires arrêtées par l'inspecteur de l'éducation nationale et destinées à aider les élèves rencontrant des difficultés à accompagner leur travail personnel ou à les encadrer dans le cadre d'une activité prévue par le projet d'école ;<br/>
<br/>
              2. Considérant que le décret dont l'abrogation a été demandée a pour seul objet de répartir un nombre d'heures d'enseignement inchangé sur neuf demi-journées au lieu des huit demi-journées prévues par la réglementation antérieure, mais ne régit pas l'organisation des activités périscolaires, qui conservent un caractère facultatif pour les communes ; qu'il n'opère ainsi aucun transfert de compétences vers les communes qui aurait impliqué, en vertu de l'article 72-2 de la Constitution, une compensation financière ; que le moyen tiré de la méconnaissance de cette disposition constitutionnelle et des dispositions de l'article L. 1614-1-1 du code général des collectivités territoriales doit, par suite, être écarté ; <br/>
<br/>
              3. Considérant que le décret n'ayant, comme il a été dit ci-dessus, opéré aucun transfert de compétences vers les collectivités territoriales, le moyen tiré de ce qu'il opérerait un tel transfert sans respecter les principes de clarté, d'accessibilité et d'intelligibilité de la norme de droit ne peut qu'être écarté ;<br/>
<br/>
              4. Considérant que le principe d'égalité n'oblige pas à traiter différemment des personnes se trouvant dans des situations différentes ; que la commune de Colombes n'est, dès lors, pas fondée à soutenir que le pouvoir réglementaire aurait méconnu ce principe en omettant de prévoir des règles distinctes suivant la situation financière des communes ; qu'en outre, le décret  n'ayant ni pour objet ni pour effet de régir l'organisation des activités périscolaires, le moyen tiré de ce qu'il créerait une rupture d'égalité entre élèves selon que ces derniers ont ou non la possibilité de bénéficier de telles activités ne peut, en tout état de cause, qu'être écarté ;  <br/>
<br/>
              5. Considérant que si, aux termes de l'article L. 521-2 du code de l'éducation : " Les rythmes scolaires tiennent compte des besoins d'expression physique, d'éducation et de pratique corporelle et sportive des élèves ", il ne ressort pas des pièces du dossier que le pouvoir réglementaire aurait commis une erreur manifeste dans la prise en compte de ces besoins ; <br/>
<br/>
              6. Considérant, enfin, que le moyen tiré de ce que le décret litigieux méconnaîtrait les dispositions de l'article L. 521-4 du code de l'éducation relatif à l'architecture scolaire n'est pas assorti des précisions permettant d'en apprécier le bien fondé ; <br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que la commune requérante n'est fondée à soutenir ni que la décision qu'elle attaque méconnaît le principe, rappelé par l'article 16-1 de la loi du 12 avril 2000 sur les droits des citoyens dans leurs relations avec l'administration, selon lequel l'autorité administrative est tenue de faire droit à une demande d'abrogation d'un règlement illégal, ni à demander l'annulation de ce refus ;<br/>
<br/>
              8. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La requête de la commune de Colombes est rejetée. <br/>
Article 2 : La présente décision sera notifiée à la commune de Colombes et à la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche.<br/>
Copie en sera adressée au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
