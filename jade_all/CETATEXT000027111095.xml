<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027111095</ID>
<ANCIEN_ID>JG_L_2013_01_000000361809</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/11/10/CETATEXT000027111095.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 5ème sous-sections réunies, 23/01/2013, 361809</TITRE>
<DATE_DEC>2013-01-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>361809</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 5ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Florence Chaltiel-Terral</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:361809.20130123</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête, enregistrée le 10 août 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par M. A...C..., demeurant à... ; M. C...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 1203365 du 17 juillet 2012 par lequel le tribunal administratif de Marseille a rejeté sa protestation tendant à l'annulation de l'élection du 13 mai 2012 à la commission syndicale de la section de Tournoux à Saint-Paul-sur-Ubaye (Alpes-de-Haute-Provence) de Mme D...H..., Mme F...E..., M. K...B...et M. J...G...;   <br/>
<br/>
              2°) d'annuler cette élection ; <br/>
<br/>
              3°) de mettre à la charge de la commune de Saint-Paul-sur-Ubaye la somme de 2 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code électoral ; <br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Florence Chaltiel-Terral, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Sur la recevabilité de la requête de M.C... :<br/>
<br/>
              1. Considérant que l'acquittement de la contribution à l'aide juridique par voie électronique lorsque l'instance est introduite par un auxiliaire de justice, prévu par le V de l'article 1635 bis Q du code général des impôts, n'est pas prescrit à peine d'irrecevabilité ; qu'ainsi, la seule circonstance que l'avocat ayant introduit la requête au nom de M. C...ait acquitté cette contribution non par voie électronique mais par l'apposition de timbres mobiles sur la requête ne rend pas cette requête irrecevable ; que, par suite, la fin de non-recevoir soulevée par les défendeurs à l'instance doit être écartée ;<br/>
<br/>
              Sur les conclusions tendant à l'annulation des élections du 13 mai 2012 à la commission syndicale de la section de Tournoux :<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 2411-1 du code général des collectivités territoriales : " Constitue une section de commune toute partie d'une commune possédant à titre permanent et exclusif des biens ou des droits distincts de ceux de la commune. / La section de commune a la personnalité juridique " ; qu'aux termes de l'article L. 2411-3 du même code : " La commission syndicale comprend des membres élus dont le nombre, qui s'élève à 4, 6, 8 ou 10, est fixé par l'arrêté du représentant de l'Etat dans le département convoquant les électeurs. / Les membres de la commission syndicale, choisis parmi les personnes éligibles au conseil municipal de la commune de rattachement, sont élus selon les mêmes règles que les conseillers municipaux des communes de moins de 2 500 habitants (...). Après chaque renouvellement général des conseils municipaux, lorsque la moitié des électeurs de la section ou le conseil municipal lui adressent à cette fin une demande dans les six mois suivant l'installation du conseil municipal, le représentant de l'Etat dans le département convoque les électeurs de la section dans les trois mois suivant la réception de la demande. / Les membres de la commission syndicale sont élus pour une durée égale à celle du conseil municipal. Toutefois, le mandat de la commission syndicale expire lors de l'installation de la commission syndicale suivant le renouvellement général des conseils municipaux (...) " ; qu'aux termes du quatrième alinéa de ce même article : " Sont électeurs, lorsqu'ils sont inscrits sur les listes électorales de la commune, les habitants ayant un domicile réel et fixe sur le territoire de la section et les propriétaires de biens fonciers sis sur le territoire de la section " ;<br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions que la qualité d'électeur de la commission syndicale d'une section de commune est subordonnée aux seules conditions fixées par la loi, et non à l'inscription sur une liste établie par le préfet à l'occasion de la convocation des électeurs ; que, par suite, un propriétaire qui n'est pas inscrit sur une telle liste mais est inscrit sur les listes électorales de la commune ne saurait être empêché de prendre part au vote dès lors qu'il justifie devant le bureau de vote de sa qualité de propriétaire sur le territoire de la section de commune par la présentation de ses titres de propriété, même si ceux-ci n'ont pas fait l'objet d'une publication ;<br/>
<br/>
              4. Considérant qu'il a été procédé le 13 mai 2012 à l'élection des quatre membres de la commission syndicale de la section de Tournoux, sur la commune de Saint-Paul-sur-Ubaye ; que MmeI..., MmeE..., M. B... et M. G...ont été élus au premier tour après avoir recueilli une majorité absolue de 12 voix  sur 19 votants ; que M. C... a demandé au tribunal administratif d'annuler cette élection en faisant valoir que plusieurs électeurs de la section de Tournoux avaient été empêchés de voter ; que, par un jugement du 17 juillet 2012, le tribunal administratif a rejeté sa protestation en jugeant que, dès lors que le nombre de personnes ayant été irrégulièrement écartées du vote n'avait été que de trois, et qu'ainsi le nombre de votants devait être fixé à 22, l'irrégularité des opérations de vote n'avait pas été de nature à influencer le niveau de la majorité absolue et, par suite, le résultat du scrutin ;<br/>
<br/>
              5. Considérant toutefois qu'il résulte de l'instruction que dix personnes inscrites sur les listes électorales de la commune et justifiant d'un titre de propriété sur le territoire de la section de commune n'ont pas été admises à voter par le bureau de vote lors de l'élection contestée du 13 mai 2012 ; qu'il résulte de ce qui a été dit ci-dessus que la publication de ces titres de propriété, au demeurant...,; que le moyen tiré de ce que ces titres de propriétés résulteraient de donations faites dans le seul but de faire élire M. C...n'est, en tout état de cause, pas établi ; qu'il en résulte que si les intéressés avaient pu voter, comme ils en avaient le droit, le nombre de suffrages exprimés aurait été de 29 et que la majorité absolue des suffrages exprimés requise pour l'élection au premier tour aurait donc été de 15, alors que les candidats proclamés élus n'ont obtenu chacun que 12 voix ; que, dès lors, l'irrégularité des opérations de vote a été de nature à influencer le résultat du scrutin ; que, par suite, M. C...est fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Marseille a rejeté sa protestation tendant à l'annulation de l'élection de Mmes H...et E...et de MM. B...et G...à la commission syndicale de la section de Tournoux et à demander l'annulation de l'élection du 13 mai 2012 ;<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              6. Considérant que les dispositions de cet article font obstacle à ce qu'une somme soit mise à ce titre à la charge de M.C..., qui n'est pas, dans la présente instance, la partie perdante ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions à ce titre de M. C...tendant au versement d'une somme par la commune de Saint-Paul-sur-Ubaye ;<br/>
<br/>
<br/>
<br/>
<br/>
              			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Marseille du 17 juillet 2012 est annulé.<br/>
<br/>
Article 2 : Les opérations électorales qui se sont déroulées le 13 mai 2012 pour l'élection des membres de la commission syndicale de la section de commune de Tournoux sur la commune de Saint-Paul-sur-Ubaye sont annulées.<br/>
<br/>
Article 3 : Le surplus des conclusions de M. C...et les conclusions de la commune de Saint-Paul-sur-Ubaye, de Mmes H... etE..., MM. B...et G...présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetés.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. A...C..., à la commune de Saint-Paul-sur-Ubaye, à Mme D...H..., à Mme F...E..., à M. K... B...et à M. J... G.établie par les intéressés, n'était pas requise pour qu'ils s'en prévalent utilement devant le bureau de vote<br/>
            Copie en sera adressée pour information au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-02-02-03-01 COLLECTIVITÉS TERRITORIALES. COMMUNE. BIENS DE LA COMMUNE. INTÉRÊTS PROPRES À CERTAINES CATÉGORIES D'HABITANTS. SECTIONS DE COMMUNE. - ELECTION DE LA COMMISSION SYNDICALE - QUALITÉ D'ÉLECTEUR - HABITANTS DOMICILIÉS DANS LA COMMUNE ET PROPRIÉTAIRES DE BIENS FONCIERS SITUÉS SUR LE TERRITOIRE DE LA SECTION INSCRITS SUR LES LISTES ÉLECTORALES DE LA COMMUNE - LISTE DES ÉLECTEURS ÉTABLIE PAR LE PRÉFET EN VUE DE LA CONVOCATION DES ÉLECTEURS - 1) POSSIBILITÉ DE SUBORDONNER LE DROIT DE VOTER DES PERSONNES AYANT QUALITÉ D'ÉLECTEUR À LEUR INSCRIPTION SUR CETTE LISTE - ABSENCE - 2) MODALITÉS DE VÉRIFICATION, LE JOUR DU VOTE, DE LA QUALITÉ D'ÉLECTEUR DES PERSONNES NON INSCRITES SUR LA LISTE PRÉFECTORALE - CAS DES PROPRIÉTAIRES DE BIENS FONCIERS SITUÉS SUR LE TERRITOIRE DE LA COMMUNE - PRÉSENTATION DES TITRES DE PROPRIÉTÉ, MÊME S'ILS N'ONT PAS FAIT L'OBJET D'UNE PUBLICATION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">28-07-03 ÉLECTIONS ET RÉFÉRENDUM. ÉLECTIONS DIVERSES. ÉLECTIONS LOCALES DIVERSES. - ELECTION DE LA COMMISSION SYNDICALE D'UNE SECTION DE COMMUNE - QUALITÉ D'ÉLECTEUR - HABITANTS DOMICILIÉS DANS LA COMMUNE ET PROPRIÉTAIRES DE BIENS FONCIERS SITUÉS SUR LE TERRITOIRE DE LA SECTION INSCRITS SUR LES LISTES ÉLECTORALES DE LA COMMUNE - LISTE DES ÉLECTEURS ÉTABLIE PAR LE PRÉFET EN VUE DE LA CONVOCATION DES ÉLECTEURS - 1) POSSIBILITÉ DE SUBORDONNER LE DROIT DE VOTER DES PERSONNES AYANT QUALITÉ D'ÉLECTEUR À LEUR INSCRIPTION SUR CETTE LISTE - ABSENCE - 2) MODALITÉS DE VÉRIFICATION, LE JOUR DU VOTE, DE LA QUALITÉ D'ÉLECTEUR DES PERSONNES NON INSCRITES SUR LA LISTE PRÉFECTORALE - CAS DES PROPRIÉTAIRES DE BIENS FONCIERS SITUÉS SUR LE TERRITOIRE DE LA COMMUNE - PRÉSENTATION DES TITRES DE PROPRIÉTÉ, MÊME S'ILS N'ONT PAS FAIT L'OBJET D'UNE PUBLICATION.
</SCT>
<ANA ID="9A"> 135-02-02-03-01 1) La qualité d'électeur de la commission syndicale d'une section de commune est subordonnée aux seules conditions fixées par l'article L. 2411-3 du code général des collectivités territoriales (être inscrit sur les listes électorales de la commune et être soit domicilié dans la commune, soit propriétaire d'un bien foncier sur le territoire de la section), et non à l'inscription sur une liste établie par le préfet à l'occasion de la convocation des électeurs. 2) Par suite, le propriétaire d'un bien foncier sur le territoire de la section qui n'est pas inscrit sur une telle liste mais est inscrit sur les listes électorales de la commune ne saurait être empêché de prendre part au vote dès lors qu'il justifie devant le bureau de vote de sa qualité par la présentation de ses titres de propriété, même si ceux-ci n'ont pas fait l'objet d'une publication.</ANA>
<ANA ID="9B"> 28-07-03 1) La qualité d'électeur de la commission syndicale d'une section de commune est subordonnée aux seules conditions fixées par l'article L. 2411-3 du code général des collectivités territoriales (être inscrit sur les listes électorales de la commune et être soit domicilié dans la commune, soit propriétaire d'un bien foncier sur le territoire de la section), et non à l'inscription sur une liste établie par le préfet à l'occasion de la convocation des électeurs. 2) Par suite, le propriétaire d'un bien foncier sur le territoire de la section qui n'est pas inscrit sur une telle liste mais est inscrit sur les listes électorales de la commune ne saurait être empêché de prendre part au vote dès lors qu'il justifie devant le bureau de vote de sa qualité par la présentation de ses titres de propriété, même si ceux-ci n'ont pas fait l'objet d'une publication.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
