<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043310014</ID>
<ANCIEN_ID>JG_L_2021_03_000000438858</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/31/00/CETATEXT000043310014.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 30/03/2021, 438858, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-03-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>438858</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BUK LAMENT - ROBILLOT</AVOCATS>
<RAPPORTEUR>M. François Weil</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:438858.20210330</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le préfet de Saône-et-Loire a demandé au juge des référés du tribunal administratif de Dijon d'ordonner l'expulsion de M. D... A... et Mme B... C... du logement géré par le dispositif d'hébergement d'urgence des demandeurs d'asile qu'ils occupent à Ciry-le-Noble (Saône-et-Loire). <br/>
<br/>
              Par une ordonnance n° 1903577 du 31 décembre 2019, le juge des référés, statuant sur le fondement de l'article L. 521-3 du code de justice administrative, a enjoint à M. A... et à Mme C..., et à toute personne les accompagnant, de libérer les lieux dans un délai de quinze jours à compter de la notification de l'ordonnance. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 20 février et 9 mars 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... et Mme C... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande du préfet ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros à verser à la SCP Potier de la Varde, Buk Lament, Robillot, leur avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ; <br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Weil, conseiller d'Etat, <br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteure publique,<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Buk Lament, Robillot, avocat de M. A... et Mme C... ; <br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1.	Aux termes de l'article L. 521-3 du code de justice administrative : " En cas d'urgence et sur simple requête qui sera recevable même en l'absence de décision administrative préalable, le juge des référés peut ordonner toutes autres mesures utiles sans faire obstacle à l'exécution d'aucune décision administrative. " L'article R. 711-2 du même code dispose : " Toute partie est avertie, par une notification faite par lettre recommandée avec demande d'avis de réception ou par la voie administrative mentionnée à l'article R. 611-4, du jour où l'affaire sera appelée à l'audience. (...) ". En vertu de l'article R. 611-4 du même code : " La notification peut également être effectuée dans la forme administrative. Il est donné récépissé de cette notification et, à défaut de récépissé, il est dressé procès-verbal de la notification par l'agent qui l'a faite. Le récépissé ou le procès-verbal est transmis immédiatement au greffe ".<br/>
<br/>
              2.	Le juge des référés, saisi en application de l'article L. 521-3 cité au point précédent, peut se prononcer sans tenir d'audience publique. Toutefois, s'il décide de tenir une telle audience, il lui appartient, compte tenu des caractéristiques de cette procédure, d'en aviser les parties par tous moyens utiles, dans le respect du caractère contradictoire de la procédure.<br/>
<br/>
              3.	Il ressort des pièces du dossier soumis au juge des référés que le préfet de Saône-et-Loire a saisi le 20 décembre 2019 le juge des référés du tribunal administratif de Dijon d'une requête tendant, sur le fondement de l'article L. 521-3 du code de justice administrative, à l'expulsion de M. D... A... et Mme B... C... du logement géré par le dispositif d'hébergement d'urgence des demandeurs d'asile qu'ils occupaient à Ciry-le-Noble (Saône-et-Loire). Par une ordonnance du 31 décembre 2019, contre laquelle M. A... et Mme C... se pourvoient en cassation, le juge des référés a fait droit à la demande du préfet.<br/>
<br/>
              4.	Il n'est pas contesté que le préfet a pris connaissance de la demande de notification administrative à M. A... et Mme C... de sa requête et de la date de l'audience le 31 décembre 2019 à 10h26, soit 34 minutes avant le début de celle-ci fixée à 11heures le même jour et que, par suite, M. A... et Mme C... n'ont pas été informés de l'existence de cette audience avant sa tenue. Les requérants sont ainsi fondés à soutenir que l'ordonnance attaquée a été rendue à la suite d'une procédure irrégulière. Par suite, et sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, elle doit être annulée.<br/>
<br/>
              5.	M. A... et Mme C... ont obtenu l'aide juridictionnelle. Par suite, leur avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.  Il y a lieu dans les circonstances de l'espèce de mettre à la charge de l'Etat la somme de 1 500 euros à verser à la SCP Potier de la Varde, Buk Lament, Robillot, avocat de M. A... et Mme C... au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve qu'elle renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'ordonnance du 31 décembre 2019 du juge des référés du tribunal administratif de Dijon est annulée.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Dijon. <br/>
Article 3 : L'Etat versera à la SCP Potier de la Varde, Buk Lament, Robillot, avocat de M. A... et Mme C..., une somme de 1 500 euros au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat. <br/>
Article 4 : Le surplus des conclusions de M. A... et Mme C... est rejeté.<br/>
Article 5 : La présente décision sera notifiée à M. D... A..., premier requérant nommé, et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
