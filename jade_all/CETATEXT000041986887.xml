<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041986887</ID>
<ANCIEN_ID>JG_L_2020_06_000000433738</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/98/68/CETATEXT000041986887.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 10/06/2020, 433738, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-06-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>433738</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BAUER-VIOLAS, FESCHOTTE-DESBOIS, SEBAGH ; SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>Mme Marie Walazyc</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:433738.20200610</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une décision du 10 février 2020, le Conseil d'Etat, statuant au contentieux, a prononcé l'admission des conclusions du pourvoi de la commune d'Antibes dirigées contre le jugement n° 1700861 du 20 juin 2019 du tribunal administratif de Nice en tant que ce jugement a annulé l'arrêté du maire d'Antibes du 31 août 2016 en ce qu'il vaut permis de démolir et la décision implicite rejetant le recours gracieux contre ce permis.<br/>
<br/>
              Par un mémoire en défense, enregistré le 11 mars 2020 au secrétariat du contentieux du Conseil d'Etat, le syndicat des copropriétaires de l'immeuble " Le parc d'Elvina " conclut, à titre principal, au rejet du pourvoi et, à titre subsidiaire, en cas d'annulation et de règlement de l'affaire au fond, à ce qu'il soit fait droit à sa demande de première instance tendant à l'annulation de l'arrêté du 31 août 2016 en ce qu'il vaut permis de démolir ainsi qu'à l'annulation de la décision implicite rejetant son recours gracieux contre ce permis. Il conclut en outre à ce que soit mise à la charge de la commune d'Antibes la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 modifiée ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Walazyc, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Bauer-Violas, Feschotte-Desbois, Sebagh, avocat de la commune d'Antibes ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que, par un arrêté du 31 août 2016, le maire d'Antibes a accordé à la société Kaufman et Broad Côte d'Azur un permis de construire et un permis de démolir en vue de la construction d'un immeuble d'habitation comprenant 57 logements et 95 places de stationnement sur un terrain situé chemin des Plateaux fleuris. Par un jugement du 20 juin 2019, le tribunal administratif de Nice a fait droit à la demande du syndicat des copropriétaires de l'immeuble " Le parc d'Elvina " tendant à l'annulation de cet arrêté ainsi que de la décision implicite rejetant son recours gracieux. Pour annuler ces décisions, il a retenu que les conditions d'accès des véhicules d'incendie et de secours au projet de construction méconnaissaient, d'une part, les dispositions de l'article UC 3 du règlement du plan local d'urbanisme, d'autre part, les dispositions de l'article R. 111-2 du code de l'urbanisme.<br/>
<br/>
              2. Aux termes de l'article L. 421-6 du code de l'urbanisme : " Le permis de construire ou d'aménager ne peut être accordé que si les travaux projetés sont conformes aux dispositions législatives et réglementaires relatives à l'utilisation des sols, à l'implantation, la destination, la nature, l'architecture, les dimensions, l'assainissement des constructions et à l'aménagement de leurs abords et s'ils ne sont pas incompatibles avec une déclaration d'utilité publique. / Le permis de démolir peut être refusé ou n'être accordé que sous réserve de l'observation de prescriptions spéciales si les travaux envisagés sont de nature à compromettre la protection ou la mise en valeur du patrimoine bâti ou non bâti, du patrimoine archéologique, des quartiers, des monuments et des sites ". Aux termes de l'article R. 431-21 du code de l'urbanisme : " Lorsque les travaux projetés nécessitent la démolition de bâtiments soumis au régime du permis de démolir, la demande de permis de construire ou d'aménager doit : / a) Soit être accompagnée de la justification du dépôt de la demande de permis de démolir ; / b) Soit porter à la fois sur la démolition et sur la construction ou l'aménagement. ".  Il résulte de ces dispositions que, si le permis de construire et le permis de démolir peuvent être accordés par une même décision, au terme d'une instruction commune, ils constituent des actes distincts comportant des effets propres.<br/>
<br/>
              3. Les motifs du jugement attaqué, mentionnés au point 1, étaient de nature à justifier son dispositif en tant que l'arrêté du 31 août 2016 et la décision rendue sur recours gracieux qu'il annule accordent un permis de construire. Ce dispositif est, dans cette mesure, devenu définitif à la suite de la décision du 10 février 2020 par laquelle le Conseil d'Etat, statuant au contentieux, n'a pas admis les conclusions du pourvoi de la commune d'Antibes contre ce jugement en tant qu'il se rapporte à ces décisions. La commune d'Antibes est en revanche fondée à soutenir que ces mêmes motifs n'étaient pas de nature à justifier l'annulation de l'arrêté du 31 août 2016 en tant qu'il accorde un permis de démolir, ainsi que de la décision rendue sur recours gracieux contre ce permis de démolir. Contrairement à ce que soutient le syndicat des copropriétaires de l'immeuble " Le parc d'Elvina ", la circonstance que le permis de construire ait été assorti de prescriptions par l'arrêté du 31 août 2016 est à cet égard sans incidence.<br/>
<br/>
              4. Il résulte de ce qui précède que la commune d'Antibes est fondée à demander l'annulation du jugement du tribunal administratif de Nice en tant qu'il annule l'arrêté du maire d'Antibes du 31 août 2016 en ce qu'il vaut permis de démolir et la décision implicite rejetant le recours gracieux contre ce permis.<br/>
<br/>
              5. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge du syndicat des copropriétaires de l'immeuble " Le Parc d'Elvina ", qui n'est pas la partie perdante pour l'essentiel dans la présente instance. Il n'y a pas lieu de mettre à la charge de la commune d'Antibes la somme que demande le syndicat des copropriétaires de l'immeuble " Le Parc d'Elvina " au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Nice du 20 juin 2019 est annulé en tant qu'il annule l'arrêté du maire d'Antibes du 31 août 2016 en ce qu'il vaut permis de démolir et la décision implicite rejetant le recours gracieux contre ce permis.<br/>
Article 2 : Les conclusions des parties présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : L'affaire est renvoyée au tribunal administratif de Nice dans la mesure de la cassation prononcée. <br/>
Article 4 : La présente décision sera notifiée à la commune d'Antibes et au syndicat des copropriétaires de l'immeuble " Le Parc d'Elvina ".<br/>
Copie en sera adressée à la société Kaufman et Broad Côte d'Azur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
