<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039409930</ID>
<ANCIEN_ID>JG_L_2019_11_000000430797</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/40/99/CETATEXT000039409930.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 21/11/2019, 430797, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-11-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>430797</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ZRIBI, TEXIER ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Vincent Daumas</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:430797.20191121</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme B... A... a demandé au juge des référés du tribunal administratif de Toulon, sur le fondement de l'article R. 541-1 du code de justice administrative, de condamner la commune de Néoules à lui payer les sommes de 994,17 euros et de 409,99 euros à titre de provision. Par une ordonnance n° 1800098 du 8 février 2019, le juge des référés du tribunal administratif de Toulon a fait droit à sa demande.<br/>
<br/>
              Par une ordonnance n° 19MA01020 du 3 mai 2019, enregistrée le 15 mai 2019 au secrétariat du contentieux du Conseil d'Etat, la présidente de la cour administrative d'appel de Marseille a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête et le mémoire en réplique, enregistrés les 28 février et 8 avril 2019 au greffe de cette cour, présentés par la commune de Néoules. <br/>
<br/>
              Par cette requête et un nouveau mémoire, enregistré le 12 juillet 2019 au secrétariat du contentieux du Conseil d'Etat, la commune de Néoules demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance du juge des référés du tribunal administratif de Toulon ; <br/>
<br/>
              2°) réglant l'affaire au titre de la procédure de référé engagée, de rejeter la demande de Mme A... ;<br/>
<br/>
              3°) de mettre à la charge de Mme A... la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Daumas, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP lyon-Caen, Thiriez, avocat de la commune de Néoulles ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. D'une part, aux termes de l'article R. 541-1 du code de justice administrative   : " Le juge des référés peut, même en l'absence d'une demande au fond, accorder une provision au créancier qui l'a saisi lorsque l'existence de l'obligation n'est pas sérieusement contestable (...) ". En vertu de l'article R. 541-3 du même code : " Sous réserve des dispositions du douzième alinéa de l'article R. 811-1, l'ordonnance rendue par le président du tribunal administratif ou par son délégué est susceptible d'appel devant la cour administrative d'appel (...) ". D'autre part, l'article R. 811-1 du même code prévoit que toute partie présente dans une instance devant le tribunal administratif peut interjeter appel contre toute décision juridictionnelle rendue dans cette instance, le tribunal administratif statuant toutefois en premier et dernier ressort dans certaines catégories de litiges énumérées par les 1° à 9° de cet article, qui sont également ses troisième à onzième alinéas. Aux termes du douzième alinéa de ce même article : " Les ordonnances prises sur le fondement du titre IV du livre V sont également rendues en premier et dernier ressort lorsque l'obligation dont se prévaut le requérant pour obtenir le bénéfice d'une provision porte sur un litige énuméré aux alinéas précédents ". Il résulte de ces dispositions que les ordonnances rendues par le juge des référés du tribunal administratif statuant sur une demande de provision sur le fondement de l'article R. 541-1 du code de justice administrative sont rendues en dernier ressort lorsque l'obligation dont se prévaut le requérant pour obtenir le bénéfice d'une provision se rattache à l'un des litiges énumérés aux 1° à 9° de l'article R. 811-1. <br/>
<br/>
              2. Une demande d'un agent public tendant au versement de rémunérations impayées, sans que soit mise en cause la responsabilité de la personne publique qui l'emploie, ne soulève pas un litige entrant dans l'une des catégories énumérées aux 1° à 9° de l'article R. 811-1 du code de justice administrative et, notamment, ne constitue pas une action indemnitaire au sens des dispositions du 8° de cet article. Il suit de là que la requête de la commune de Néoules, dirigée contre une ordonnance du juge des référés du tribunal administratif de Toulon rendue sur la demande de l'un de ses agents tendant au versement d'une provision de 1 404,16 euros au titre de la bonification indiciaire, a le caractère d'un appel qui ne ressortit pas à la compétence du Conseil d'Etat, juge de cassation, mais à celle de la cour administrative d'appel de Marseille. Il y a lieu, dès lors, d'en attribuer le jugement à cette cour.  <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement de la requête de la commune de Néoules est attribué à la cour administrative d'appel de Marseille. <br/>
Article 2 : La présente décision sera notifiée à la commune de Néoules.<br/>
Copie en sera adressée pour information à Mme B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
