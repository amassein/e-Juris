<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037816039</ID>
<ANCIEN_ID>JG_L_2018_12_000000405259</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/81/60/CETATEXT000037816039.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 14/12/2018, 405259, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-12-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>405259</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MARLANGE, DE LA BURGADE</AVOCATS>
<RAPPORTEUR>Mme Cécile Viton</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:405259.20181214</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société à responsabilité limitée Volume(s)... Etc... a demandé au tribunal administratif de Montreuil de prononcer la réduction des sommes auxquelles elle a été assujettie au titre de la taxe locale d'équipement et de la redevance d'archéologie préventive respectivement à hauteur de 20 256 euros et 855 euros ainsi que la décharge de la majoration et des intérêts de retard. Par un jugement n° 1600683 du 15 septembre 2016, le tribunal a fait droit à sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 21 novembre 2016 et 22 février 2017 au secrétariat du contentieux du Conseil d'Etat, le ministre du logement et de l'habitat durable demande au Conseil d'Etat d'annuler ce jugement.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts ;<br/>
              - le code du patrimoine ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Viton, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Marlange, de la Burgade, avocat de la société Volumes...etc....<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que le permis de construire portant sur un ensemble immobilier situé au Pré-Saint-Gervais délivré à la société ECM/Volumes a été transféré totalement le 10 mai 2013 à la société Volume(s)... Etc... puis, le 19 février 2014, partiellement à la commune du Pré-Saint-Gervais qui l'a transféré le 12 septembre 2014 à la société Osica. Par deux avis d'imposition du 15 mai 2014, la société Volume(s)... Etc... a été assujettie à la taxe locale d'équipement et à d'autres taxes d'urbanisme pour un montant de 100 537 euros et à la redevance d'archéologie préventive pour un montant de 6 886 euros, ces impositions ayant pour assiette la surface créée autorisée par le permis de construire initial. Le ministre du logement et de l'habitat durable se pourvoit en cassation contre le jugement du 15 septembre 2016 par lequel le tribunal administratif de Montreuil a réduit ces sommes d'un montant respectif de 20 256 euros et 855 euros.<br/>
<br/>
              Sur la redevance d'archéologie préventive :<br/>
<br/>
              2. Le produit de la redevance d'archéologie préventive était, en vertu de l'article L 524-11 du code du patrimoine, dans sa rédaction applicable à la date de l'avis d'imposition en litige, reversé à l'Institut national des recherches archéologiques préventives, établissement public national à caractère administratif ou, après prélèvement d'un pourcentage au profit du Fonds national pour l'archéologie préventive, à une collectivité territoriale ou à un groupement de collectivités territoriales dans le cas où ils ont confié à leur propre service archéologique l'ensemble des opérations d'aménagement ou de travaux réalisés sur leur territoire. Compte tenu de ces conditions d'affectation, un litige relatif à cette redevance ne pouvait être regardé comme étant relatif à un impôt local au sens de l'article R. 811-1 du code de justice administrative aux termes duquel : " le tribunal administratif statue en premier et dernier ressort : / (...) 4° Sur les litiges relatifs aux impôts locaux et à la contribution à l'audiovisuel public, à l'exception des litiges relatifs à la contribution économique territoriale ; ". Dès lors, le recours du ministre du logement et de l'habitat durable dirigé contre le jugement du 15 septembre 2016 du tribunal administratif de Montreuil en tant qu'il accorde à la société Volume(s)... Etc... une décharge partielle de la redevance d'archéologie préventive ainsi que des intérêts de retard et pénalités correspondantes doit être regardé comme un appel relevant de la compétence de la cour administrative d'appel de Versailles. Il y a lieu, dès lors, d'en attribuer le jugement à cette cour.<br/>
<br/>
              Sur la taxe locale d'équipement et les taxes annexes :<br/>
<br/>
              3. Aux termes de l'article 1723 quater du code général des impôts, dans sa rédaction applicable à l'imposition en litige : " 1. La taxe locale d'équipement visée à l'article 1585 A est due par le bénéficiaire de l'autorisation de construire. (...) En cas de modification apportée au permis de construire ou à l'autorisation tacite de construire, le complément de taxe éventuellement exigible doit être acquitté dans le délai d'un an à compter de la modification ". Aux termes des dispositions de l'article 406 ter de l'annexe III à ce code, dans sa rédaction applicable à l'imposition en litige : " (...) Lorsque l'autorité administrative autorise le transfert d'un permis de construire qui a rendu exigible la taxe locale d'équipement, elle doit en informer sans délai le préfet pour émission d'un nouveau titre ". Il résulte de ces dispositions que, lorsque l'administration autorise le transfert d'un permis de construire à une personne autre que le titulaire initial, celle-ci devient le bénéficiaire, au nom duquel les titres de perception doivent être émis, de l'autorisation de construire prévue par ces dispositions.<br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond que par un arrêté du 19 février 2014, le maire du Pré-Saint-Gervais a autorisé le transfert partiel à cette commune, à hauteur de 913,30 m² de surface de plancher, du permis de construire dont la société Volume(s)... Etc... était précédemment bénéficiaire. Cette surface a ensuite été transférée le 12 septembre 2014 à la société Osica. En jugeant que ces transferts avaient eu pour conséquence de rendre la commune puis la société Osica redevables de la taxe locale d'équipement et des taxes annexes à laquelle la société Volume(s)... Etc... a été assujettie à raison de la surface de construction ayant fait l'objet de ces transferts et que de nouveaux titres de perception auraient dû être émis afin de mettre à sa charge les montants afférents à la part de la surface dont elle restait bénéficiaire, le tribunal administratif de Montreuil n'a pas commis d'erreur de droit. Il y a lieu, par suite, de rejeter les conclusions du ministre du logement et de l'habitat à l'encontre du jugement du tribunal en tant qu'il a accordé une décharge partielle à la société Volume(s)... Etc... de la taxe locale d'équipement et des taxes annexes.<br/>
<br/>
              5. Il y a lieu dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la société Volume(s)... Etc... au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement des conclusions présentées par le ministre du logement et de l'habitat durable tendant à l'annulation du jugement du tribunal administratif de Montreuil du 15 septembre 2016 en tant qu'il a accordé à la SARL Volume(s)... Etc... une décharge partielle de la redevance d'archéologie préventive ainsi que des intérêts de retard et pénalités correspondantes est attribué à la cour administrative d'appel de Versailles.<br/>
Article 2 : Les conclusions présentées par le ministre du logement et de l'habitat tendant à l'annulation du jugement du tribunal administratif de Montreuil du 15 septembre 2016 en tant qu'il a accordé une décharge partielle de la taxe locale d'équipement et des taxes annexes à la SARL Volume(s)... Etc... sont rejetées.<br/>
Article 3 : L'Etat versera à la SARL Volume(s)... Etc... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales, à la SARL Volume(s)...Etc... et au président de la cour administrative d'appel de Versailles.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
