<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028471765</ID>
<ANCIEN_ID>JG_L_2014_01_000000371585</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/47/17/CETATEXT000028471765.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème SSR, 15/01/2014, 371585, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-01-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>371585</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SPINOSI ; SCP BARTHELEMY, MATUCHANSKY, VEXLIARD</AVOCATS>
<RAPPORTEUR>Mme Marie-Gabrielle Merloz</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Claire Legras</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2014:371585.20140115</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le mémoire, enregistré le 15 octobre 2013 au secrétariat du contentieux du Conseil d'Etat, présenté pour la société UBS (France) SA, dont le siège est 69 boulevard Haussmann à Paris (75008), représentée par son président-directeur général en exercice, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 ; la société UBS (France) SA demande au Conseil d'Etat, à l'appui de sa requête tendant à l'annulation de la décision du 26 août 2013 par laquelle la commission des sanctions de l'Autorité de contrôle prudentiel a prononcé à son encontre un blâme ainsi qu'une sanction pécuniaire d'un montant de 10 millions d'euros, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions des articles 33 et 51 de la loi du 24 janvier 1984 relative à l'activité et au contrôle des établissements de crédit et des articles L. 511-41, L. 611-1, L. 611-7, L. 612-1 et L. 612-39 du code monétaire et financier ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule et ses articles 34 et 61-1 ; <br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
              Vu le code monétaire et financier ;<br/>
<br/>
              Vu la loi n° 84-46 du 26 janvier 1984, notamment ses articles 33 et 51 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Gabrielle Merloz, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Claire Legras, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Spinosi, avocat de la Société UBS (France) SA et à la SCP Barthélemy, Matuchansky, Vexliard, avocat de l'Autorité de contrôle prudentiel et de résolution ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé, y compris pour la première fois en cassation, à l'occasion d'une instance devant le Conseil d'Etat (...) " ; qu'il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2. Considérant que la société UBS (France) SA soutient que les dispositions des articles 33 et 51 de la loi du 24 janvier 1984 relative à l'activité et au contrôle des établissements de crédit et des articles L. 511-41, L. 611-1, L. 611-7, L. 612-1 et L. 612-39 du code monétaire et financier méconnaissent les droits et libertés que la Constitution garantit ;<br/>
<br/>
              3. Considérant que le litige soulevé par la société UBS (France) SA tend à l'annulation par le Conseil d'Etat de la décision du 25 janvier 2013 de la commission des sanctions de l'Autorité de contrôle prudentiel par laquelle celle-ci lui a infligé un blâme et une sanction financière de dix millions d'euros et a décidé de publier cette même décision au registre de l'Autorité de contrôle prudentiel sous une forme faisant apparaître son nom ; que seules peuvent être regardées comme applicables à ce litige les dispositions, qui n'ont pas déjà été déclarées conformes à la Constitution, des quatrième et cinquième alinéas de l'article L. 511-41, du 10° de l'article L. 611-1 et des articles L. 611-7, L. 612-1 et L. 612-39 du code monétaire et financier ; qu'en revanche, ni les articles 33 et 51 de la loi du 24 janvier 1984 relative à l'activité et au contrôle des établissements de crédit, abrogés avant les faits et la procédure concernés par ce litige et n'ayant, dès lors, pu servir de base légale à la décision en cause, ni les dispositions des articles L. 511-41 et L. 611-1 du même code autres que celles visées ci-dessus, ne lui sont applicables ;<br/>
<br/>
              4. Considérant que la société UBS (France) SA soutient, d'une part, que les dispositions du code monétaire et financier visées au point 3 ci-dessus contreviennent à l'exigence de clarté et de précision découlant du principe de légalité des délits et des peines tel que garanti par l'article 8 de la Déclaration des droits de l'homme et du citoyen et, d'autre part, que le législateur, en les adoptant, a méconnu la compétence qu'il tient de l'article 34 de la Constitution pour définir lui-même les obligations mises à la charge des établissements de crédit en matière de contrôle interne et de conformité, alors que cette méconnaissance affecte à la fois le respect du principe de légalité des délits et des peines et celui de la liberté d'entreprendre ;<br/>
<br/>
              5. Considérant, toutefois, en premier lieu, que les exigences qui découlent du principe à valeur constitutionnelle de légalité des délits et des peines, appliqué en dehors du droit pénal, se trouvent satisfaites, en matière administrative, par la référence aux obligations auxquelles l'intéressé est soumis en vertu des lois et règlements en raison de l'activité qu'il exerce, de la profession à laquelle il appartient, de l'institution dont il relève ou de la qualité qu'il revêt ; qu'il résulte de la combinaison des articles L. 612-1, qui précise les missions de contrôle de l'Autorité de contrôle prudentiel, et L. 612-39 du code monétaire et financier, qui fixe la liste des sanctions que la commission des sanctions peut prononcer en fonction de la gravité des manquements constatés, que les établissements de crédit sont susceptibles d'être sanctionnés s'ils enfreignent une disposition du code monétaire et financier ainsi que des dispositions réglementaires prévues pour son application ; que l'article L. 511-41 du même code impose en particulier aux établissements de crédit de " disposer d'un système adéquat de contrôle interne leur permettant notamment de mesurer les risques et la rentabilité de leurs activités, y compris lorsqu'ils confient à des tiers des fonctions ou autres tâches opérationnelles essentielles ou importantes " et confie au ministre chargé de l'économie la définition des conditions d'application de ces dispositions ; que, conformément au 10° de l'article L. 611-1 du même code, le ministre chargé de l'économie définit notamment les règles applicables aux procédures de contrôle interne  ; que l'article L. 611-7 de ce code l'autorise également dans ce cadre à modifier ou abroger les règlements du comité de la réglementation bancaire et financière en vigueur antérieurement à la loi du 1er août 2003 de sécurité financière ; que les règles relatives au contrôle interne ont été précisées par le règlement n° 97-02 du 21 février 1997 du comité de la réglementation bancaire et financière relatif au contrôle interne des établissements de crédit et des entreprises d'investissement ; qu'il suit de là que les dispositions litigieuses font référence aux obligations auxquelles les établissements de crédit sont soumis en raison de l'exercice de la profession à laquelle ils appartiennent  ; <br/>
<br/>
              6. Considérant, en second lieu, que la société requérante ne saurait utilement soutenir que le législateur n'aurait pas exercé la compétence qu'il tient de l'article 34 de la Constitution en matière de détermination des crimes et délits ainsi que des peines qui leur sont applicables et de détermination du régime des obligations civiles et commerciales, dès lors que les obligations pesant sur les établissements de crédit en matière de contrôle interne, objet du présent litige, n'en relèvent pas ; que, par ailleurs, l'article L. 511-41 du code monétaire et financier détermine le champ d'application de l'obligation imposée aux établissements de crédits en matière de contrôle interne et ne laisse au ministre chargé de l'économie que le soin d'assurer l'application de ces dispositions, notamment en lui confiant la définition des procédures de contrôle interne conformément au 10° de l'article L. 611-1 du même code ; que, contrairement à ce que soutient la société requérante, l'habilitation ainsi confiée au ministre chargé de l'économie concerne des mesures limitées tant par leur champ d'application que par leur contenu et a pour seul objet de déterminer les modalités selon lesquelles le système de contrôle interne prévu par la loi doit être mis en oeuvre ; que, dans ces conditions, le législateur n'a pas délégué au pouvoir réglementaire la fixation de règles ou de principes que la Constitution place dans le domaine de la loi ; que, par suite, la société requérante n'est pas fondée à soutenir que le législateur n'aurait pas édicté les garanties permettant qu'il ne soit pas porté atteinte à la liberté d'entreprendre ni, en tout état de cause, au principe de légalité des délits et des peines ;<br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que la question, qui n'est pas nouvelle, tirée de ce que les quatrième et cinquième alinéas de l'article L. 511-41, le 10° de l'article L. 611-1 et les articles L. 611-7, L. 612-1 et L. 612-39 du code monétaire et financier méconnaîtraient le principe de légalité des délits et des peines et l'article 34 de la Constitution ne présente pas un caractère sérieux ; que, dès lors, sans qu'il soit besoin de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée, le moyen tiré de ce que ces dispositions porteraient atteinte aux droits et libertés garantis par la Constitution doit être écarté ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la société UBS (France) SA.<br/>
Article 2 : La présente décision sera notifiée à la société UBS (France) SA, à l'Autorité de contrôle prudentiel et de résolution et au ministre de l'économie et des finances.<br/>
Copie en sera adressée au Conseil constitutionnel et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
