<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038088249</ID>
<ANCIEN_ID>JG_L_2019_02_000000423644</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/08/82/CETATEXT000038088249.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 01/02/2019, 423644, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-02-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>423644</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR>M. Olivier  Fuchs</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:423644.20190201</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La fédération des syndicats des arts, des spectacles, de l'audiovisuel, de la presse, de la communication et du multimédia Force Ouvrière (FASAP-FO) a demandé à la cour administrative d'appel de Paris d'annuler pour excès de pouvoir l'arrêté du ministre du travail du 10 novembre 2017 fixant la liste des organisations syndicales reconnues représentatives dans la convention collective nationale pour les entreprises artistiques et culturelles. Par un arrêt n° 18PA00100 du 26 juin 2018, la cour administrative d'appel a rejeté sa requête.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 27 août et 27 novembre 2018 au secrétariat du contentieux du Conseil d'Etat, la FASAP-FO demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa requête ;<br/>
<br/>
              3°) de mettre à la charge l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
                          Vu les autres pièces du dossier ;<br/>
<br/>
                          Vu : <br/>
                          - la Constitution, notamment son article 61-1 ;<br/>
                          - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
                           - le code du travail ;<br/>
                          - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Fuchs, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Sevaux, Mathonnet, avocat de la Fédération des syndicats des arts, des spectacles, de l'audiovisuel, de la presse de la communication et du multimedia Force Ouvrière ;<br/>
<br/>
<br/>
<br/>
<br/>Sur le mémoire intitulé " question prioritaire de constitutionnalité " :<br/>
<br/>
              1. Aux termes de l'article 23-1 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Devant les juridictions relevant du Conseil d'Etat ou de la Cour de cassation, le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution est, à peine d'irrecevabilité, présenté dans un écrit distinct et motivé ". Aux termes de l'article 23-2 de la même ordonnance : " (...) Le refus de transmettre la question ne peut être contesté qu'à l'occasion d'un recours contre la décision réglant tout ou partie du litige ". Selon l'article 23-5 de cette ordonnance : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé, y compris pour la première fois en cassation, à l'occasion d'une instance devant le Conseil d'Etat ou la Cour de cassation. Le moyen est présenté, à peine d'irrecevabilité, dans un mémoire distinct et motivé (...) ". Enfin, aux termes du premier alinéa de l'article R. 771-16 du code de justice administrative : " Lorsque l'une des parties entend contester devant le Conseil d'Etat, à l'appui d'un appel ou d'un pourvoi en cassation formé contre la décision qui règle tout ou partie du litige, le refus de transmission d'une question prioritaire de constitutionnalité précédemment opposé, il lui appartient, à peine d'irrecevabilité, de présenter cette contestation avant l'expiration du délai de recours dans un mémoire distinct et motivé, accompagné d'une copie de la décision de refus de transmission ". Il résulte de ces dispositions que, lorsqu'une cour administrative d'appel a refusé de transmettre au Conseil d'Etat la question prioritaire de constitutionnalité qui lui a été soumise, il appartient à l'auteur de cette question de contester ce refus, à l'occasion du pourvoi en cassation formé contre l'arrêt qui statue sur le litige, dans le délai de recours contentieux et par un mémoire distinct et motivé, que le refus de transmission précédemment opposé l'ait été par une décision distincte de l'arrêt, dont il joint alors une copie, ou directement par cet arrêt. Les dispositions de l'article 23-5 de l'ordonnance du 7 novembre 1958 n'ont ni pour objet ni pour effet de permettre à celui qui a déjà présenté une question prioritaire de constitutionnalité devant une juridiction statuant en dernier ressort de s'affranchir des conditions, définies par les dispositions citées plus haut de la loi organique et du code de justice administrative, selon lesquelles le refus de transmission peut être contesté devant le juge de cassation.<br/>
<br/>
              2. Le mémoire par lequel le requérant demande au Conseil d'Etat de transmettre au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions de l'article L. 2122-10-2 du code du travail doit, par suite, être regardé comme tendant à l'annulation de l'arrêt attaqué de la cour administrative d'appel de Paris en tant qu'il refuse de transmettre cette même question au Conseil d'Etat.<br/>
<br/>
              3. L'article L. 2122-10-2 du code du travail, dont la constitutionnalité était contestée devant la cour administrative d'appel, dispose que : " Sont électeurs les salariés des entreprises qui emploient moins de onze salariés au 31 décembre de l'année précédant le scrutin, titulaires d'un contrat de travail en cours de ce mois de décembre, âgés de seize ans révolus et ne faisant l'objet d'aucune interdiction, déchéance ou incapacité relative à leurs droits civiques ". Si ces dispositions déterminent la qualité d'électeur au scrutin mentionnée à l'article L. 2122-5 du même code et sont, par suite, susceptibles de voir, le cas échéant, leur constitutionnalité contestée au soutien de conclusions dirigées contre les opérations électorales, une telle contestation ne peut être utilement soulevée au soutien d'un recours en excès de pouvoir dirigé contre l'arrêté qui, en application des dispositions de l'article L. 2122-11 du même code, fixe la liste des organisations syndicales reconnues représentatives en se fondant notamment sur leur audience, mesurée en fonction des résultats obtenus à ces élections.<br/>
<br/>
              4. Il résulte de ce qui précède que la cour administrative d'appel n'a pas commis d'erreur de droit en jugeant, pour refuser de transmettre la question prioritaire de constitutionnalité dont elle était saisie, que l'article L. 2122-10-2 n'était pas applicable au litige par lequel le syndicat requérant la saisissait de la légalité de l'arrêté du ministre du travail du 10 novembre 2017 fixant la liste des organisations syndicales reconnues représentatives dans la convention collective nationale pour les entreprises artistiques et culturelles.<br/>
<br/>
              Sur le surplus des conclusions du pourvoi :<br/>
<br/>
              5. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
<br/>
              6. Pour demander l'annulation de l'arrêt de la cour administrative d'appel de Paris qu'elle attaque, la fédération des syndicats des arts, des spectacles, de l'audiovisuel, de la presse, de la communication et du multimédia Force Ouvrière soutient :<br/>
<br/>
              - qu'il se méprend  sur la portée de ses écritures ;<br/>
              - qu'il est entaché de dénaturation des pièces du dossier et d'erreur de droit en ce que, sans faire usage des pouvoirs d'instruction du juge administratif, il juge que, malgré plusieurs erreurs matérielles constatées dans les décomptes de l'audience, elle n'atteint pas le seuil de 8% des suffrages exprimés.<br/>
<br/>
               7. Aucun de ces moyens n'est de nature à permettre l'admission du pourvoi.<br/>
<br/>
<br/>
<br/>
              D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : La contestation du refus de transmission au Conseil d'Etat de la question prioritaire de constitutionnalité mettant en cause l'article L. 2122-10-2 du code du travail est écartée.<br/>
Article 2 : Le pourvoi de la fédération des syndicats des arts, des spectacles, de l'audiovisuel, de la presse, de la communication et du multimédia Force Ouvrière n'est pas admis.<br/>
Article 3 : La présente décision sera notifiée à la fédération des syndicats des arts, des spectacles, de l'audiovisuel, de la presse, de la communication et du multimédia Force Ouvrière.<br/>
Copie en sera adressée au syndicat national des techniciens et travailleurs de la production cinématographique et de la télévision, à la confédération française démocratique du travail, à la confédération générale du travail et à la ministre du travail.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
