<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035179884</ID>
<ANCIEN_ID>JG_L_2017_07_000000406170</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/17/98/CETATEXT000035179884.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 12/07/2017, 406170, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-07-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>406170</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BERTRAND</AVOCATS>
<RAPPORTEUR>M. Luc Briand</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:406170.20170712</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le préfet de la Seine-Maritime a demandé au juge des référés du tribunal administratif de Rouen, statuant sur le fondement de l'article L. 521-3 du code de justice administrative, d'ordonner l'expulsion du centre d'accueil pour demandeurs d'asile " Adome Graville " du Havre, au besoin avec le concours de la force publique, de MmeA.... <br/>
<br/>
              Par une ordonnance n° 1603827 du 9 décembre 2016, le juge des référés a rejeté cette demande. <br/>
<br/>
              Par un pourvoi, enregistré le 21 décembre 2016 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) statuant en référé, de faire droit à la demande d'expulsion de MmeA.... <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code des procédures civiles d'exécution ;<br/>
              - la loi n° 2015-925 du 29 juillet 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Luc Briand, maître des requêtes en service extraordinaire, <br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public, <br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Bertrand, avocat de Mme A...; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant, d'une part, que selon l'article L. 744-5 du code de l'entrée et du séjour des étrangers et du droit d'asile, issu de la loi du 29 juillet 2015 relative à la réforme du droit d'asile, les lieux d'hébergement pour demandeurs d'asile " accueillent les demandeurs d'asile pendant la durée d'instruction de leur demande d'asile ou jusqu'à leur transfert effectif vers un autre Etat européen. Cette mission prend fin à l'expiration du délai de recours contre la décision de l'Office français de protection des réfugiés et apatrides ou à la date de la notification de la décision de la Cour nationale du droit d'asile ou à la date du transfert effectif vers un autre Etat, si sa demande relève de la compétence de cet Etat. (...) / Un décret en Conseil d'Etat détermine les conditions dans lesquelles les personnes s'étant vu reconnaître la qualité de réfugié ou accorder le bénéfice de la protection subsidiaire et les personnes ayant fait l'objet d'une décision de rejet définitive peuvent être maintenues dans un lieu d'hébergement mentionné au même article L. 744-3 à titre exceptionnel et temporaire. / Lorsque, après une décision de rejet définitive, le délai de maintien dans un lieu d'hébergement mentionné audit article L. 744-3 prend fin, l'autorité administrative compétente peut, après mise en demeure restée infructueuse, demander en justice qu'il soit enjoint à cet occupant sans titre d'évacuer ce lieu. / Le quatrième alinéa du présent article est applicable aux personnes qui ont un comportement violent ou commettent des manquements graves au règlement du lieu d'hébergement. / La demande est portée devant le président du tribunal administratif, qui statue sur le fondement de l'article L. 521-3 du code de justice administrative et dont l'ordonnance est immédiatement exécutoire " ; <br/>
<br/>
              2.	Considérant, d'autre part, qu'aux termes de l'article L. 521-3 du code de justice administrative : " En cas d'urgence et sur simple requête qui sera recevable même en l'absence de décision administrative préalable, le juge des référés peut ordonner toutes autres mesures utiles sans faire obstacle à l'exécution d'aucune décision administrative " ; <br/>
<br/>
              3.	Considérant qu'il résulte de ces dispositions que, saisi par le préfet d'une demande tendant à ce que soit ordonnée l'expulsion d'un lieu d'hébergement pour demandeurs d'asile d'un demandeur d'asile dont la demande a été définitivement rejetée, le juge des référés du tribunal administratif y fait droit dès lors que la demande d'expulsion ne se heurte à aucune contestation sérieuse et que la libération des lieux présente un caractère d'urgence et d'utilité ;<br/>
<br/>
              4.	Considérant que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Rouen a rejeté la demande présentée par le préfet de la Seine-Maritime sur le fondement de l'article L. 744-5 du code de l'entrée et du séjour des étrangers et du droit d'asile, tendant à l'expulsion immédiate, au besoin avec le concours de la force publique, de Mme B...A..., bénéficiaire d'un hébergement au sein du centre d'accueil pour demandeurs d'asile " Adome Graville " du Havre ; que le juge des référés s'est fondé sur les dispositions de l'article L. 412-6 du code des procédures civiles d'exécution pour rejeter la demande du préfet ;<br/>
<br/>
              5.	Considérant qu'aux termes de l'article L. 412-6 du code des procédures civiles d'exécution, dans sa rédaction en vigueur à la date de l'ordonnance attaquée : " Nonobstant toute décision d'expulsion passée en force de chose jugée et malgré l'expiration des délais accordés en vertu de l'article L. 412-3, il est sursis à toute mesure d'expulsion non exécutée à la date du 1er novembre de chaque année jusqu'au 31 mars de l'année suivante, à moins que le relogement des intéressés soit assuré dans des conditions suffisantes respectant l'unité et les besoins de la famille. / Toutefois, le juge peut supprimer le bénéfice du sursis prévu au premier alinéa lorsque les personnes dont l'expulsion a été ordonnée sont entrées dans les locaux par voie de fait " ; <br/>
<br/>
              6.	Considérant que ces dispositions du code des procédures civiles d'exécution ne sont pas applicables, en l'absence de disposition législative expresse, à la procédure d'expulsion des personnes se maintenant dans un lieu d'hébergement pour demandeurs d'asile organisée par l'article L. 744-5 du code de l'entrée et du séjour des étrangers et du droit d'asile ; que, par suite, en retenant que les dispositions de l'article L. 412-6 du code des procédures civiles d'exécution étaient applicables et faisaient obstacle à la demande d'expulsion présentée par le préfet de la Seine-Maritime, le juge des référés a commis une erreur de droit ; que le ministre de l'intérieur est, dès lors et sans qu'il soit besoin d'examiner l'autre moyen de son pourvoi, fondé à demander l'annulation de l'ordonnance qu'il attaque ; <br/>
<br/>
              7.	Considérant qu'il y a lieu, dans les circonstances de l'espèce et par application des dispositions de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée ;<br/>
<br/>
              8.	Considérant qu'il résulte de l'instruction que la demande d'asile présentée par MmeA..., ressortissante chinoise, hébergée au sein du centre d'accueil pour demandeurs d'asile " Adome Graville " du Havre, a été rejetée par décision du directeur général de l'Office français de protection des réfugiés et apatrides le 30 juin 2015 ; que le recours dirigé contre ce refus a été rejeté par la Cour nationale du droit d'asile le 14 juin 2016 ; qu'après que l'intéressée a été informée de la fin de sa prise en charge par le gestionnaire du centre d'accueil le 19 juillet 2016, le préfet de la Seine-Maritime l'a mise en demeure de quitter les lieux, par lettre du 8 septembre 2016 ; que le préfet a, le 1er décembre 2016, saisi le juge des référés en vue d'ordonner l'expulsion de l'intéressée ; <br/>
<br/>
              9.	Considérant, en premier lieu, qu'il ressort des pièces du dossier que le secrétaire général de la préfecture de la Seine-Maritime disposait d'une délégation de signature l'habilitant à saisir le juge des référés de la demande d'expulsion au nom du préfet de la Seine-Maritime ; <br/>
<br/>
              10.	Considérant, en deuxième lieu, qu'il résulte de l'instruction que Mme A...se maintient dans un lieu d'hébergement pour demandeurs d'asile alors que sa demande d'asile a été définitivement rejetée ; que la mesure demandée par le préfet ne se heurte à aucune contestation sérieuse ; que si la saisine du juge des référés ne peut intervenir qu'après une mise en demeure restée infructueuse, aucune disposition n'impose de délivrer une information particulière à l'intéressé avant cette saisine ;<br/>
<br/>
              11.	Considérant, en troisième lieu, qu'ainsi qu'il a été dit au point 6, les dispositions de l'article L. 412-6 du code des procédures d'exécution ne font pas obstacle au prononcé de l'expulsion demandée par le préfet sur le fondement de l'article L. 744-5 du code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
<br/>
              12.	Considérant, en quatrième lieu, que la libération des lieux présente, eu égard aux besoins d'accueil des demandeurs d'asile et au nombre de places disponibles dans les lieux d'hébergement pour demandeurs d'asile en Seine-Maritime, un caractère d'urgence et d'utilité que la circonstance que l'intéressée soit parent d'un enfant né en 2012 ne remet en l'espèce pas en cause ; <br/>
<br/>
              13.	Considérant qu'il résulte de ce qui précède qu'il y a lieu d'ordonner la libération par Mme A...des lieux qu'elle occupe dans le centre d'accueil pour demandeurs d'asile " Adome Graville " du Havre au besoin avec le concours de la force publique ;<br/>
<br/>
              14.	Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat, qui n'est pas dans la présence instance la partie perdante, au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 9 décembre 2016 du juge des référés du tribunal administratif de Rouen est annulée. <br/>
<br/>
Article 2 : Il est enjoint à Mme A...de libérer les lieux qu'elle occupe dans le centre d'accueil pour demandeurs d'asile " Adome Graville " du Havre. <br/>
<br/>
Article 3 : Le préfet de la Seine-Maritime est autorisé à procéder, dans un délai de huit jours à compter de la notification de la présente décision, avec le concours de la force publique, à l'expulsion de MmeA.... <br/>
<br/>
Article 4 : Les conclusions présentées au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée au ministre d'Etat, ministre de l'intérieur et à Mme B...A.... <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
