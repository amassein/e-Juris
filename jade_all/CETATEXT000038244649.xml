<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038244649</ID>
<ANCIEN_ID>JG_L_2019_03_000000417270</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/24/46/CETATEXT000038244649.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 6ème chambres réunies, 18/03/2019, 417270, Publié au recueil Lebon</TITRE>
<DATE_DEC>2019-03-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417270</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUIN-PALAT, BOUCARD</AVOCATS>
<RAPPORTEUR>Mme Louise Cadin</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:417270.20190318</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Montreuil d'annuler les décisions implicites par lesquelles le préfet du Val-de-Marne a refusé de procéder à l'échange de son permis de conduire camerounais contre un permis de conduire français, d'enjoindre au préfet du Val-de-Marne de lui délivrer un permis de conduire français et de mettre à la charge de l'Etat une somme de 40 000 euros en réparation du préjudice qu'il estime avoir subi du fait du refus illégal opposé à sa demande d'échange de permis. Par un jugement n° 1502626 du 9 novembre 2017, le tribunal administratif de Melun a rejeté cette demande qui lui avait été transmise par le tribunal administratif de Montreuil en application de l'article R. 351-3 du code de justice administrative.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 12 janvier, 12 avril et 5 novembre 2018 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 800 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code des relations entre le public et l'administration ;<br/>
<br/>
              - le code de la route ;<br/>
<br/>
              - loi n° 2000-321 du 12 avril 2000 ;<br/>
<br/>
              - le décret n° 2001-492 du 6 juin 2001 ;<br/>
<br/>
              - le décret n° 2008-1281 du 8 décembre 2008 ; <br/>
<br/>
              - l'arrêté interministériel du 12 janvier 2012 du ministre de l'équipement, des transports et du logement fixant les conditions de reconnaissance et d'échange des permis de conduire délivrés par les Etats n'appartenant ni à l'Union européenne, ni à l'Espace économique européen ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Louise Cadin, auditrice,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouin-Palat, Boucard, avocat de M.B....<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumises au juge du fond que M. B... a, le 27 janvier 2004, sollicité du préfet de la Seine-Saint-Denis l'échange de son permis de conduire camerounais contre un permis de conduire français. Le 23 juillet 2004, le préfet de la Seine-Saint-Denis, ne s'estimant pas compétent pour statuer sur cette demande alors que l'intéressé avait entretemps fixé son domicile dans le Val-de-Marne, l'a transmise au préfet de ce département. Ce dernier a estimé que le titre dont l'échange était demandé n'était pas authentique et l'a transmis au procureur de la République de Bobigny qui a diligenté une enquête pour " faux et tentative d'obtention frauduleuse d'un document administratif ", laquelle a débouché le 13 août 2010 sur un classement sans suite. Face au silence gardé par l'administration, M. B...a formé auprès du préfet du Val-de-Marne, le 14 août 2014, une nouvelle demande d'échange de son permis de conduire auprès du préfet du Val-de-Marne qui l'a transmise au préfet de la Seine-Saint-Denis, lequel s'est à nouveau estimé incompétent et l'a renvoyée au préfet du Val-de-Marne. M. B...a demandé au tribunal administratif de Montreuil d'annuler les décisions implicites du préfet du Val-de-Marne rejetant ses demandes formées en 2004 et en 2014, d'enjoindre à l'administration de procéder à l'échange de son permis de conduire camerounais contre un permis de conduire français et de condamner l'Etat à réparer le préjudice qu'il avait subi. Par un jugement du 9 novembre 2017, contre lequel M. B...se pourvoit en cassation, le tribunal administratif de Melun a rejeté cette demande qui lui avait été transmise par le tribunal administratif de Montreuil en application de l'article R. 351-3 du code de justice administrative.<br/>
<br/>
              En ce qui concerne les conclusions dirigées contre le rejet de la demande présentée le 27 janvier 2004 : <br/>
<br/>
              2. Aux termes de l'article 21 de la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations, en vigueur à la date de la première décision attaquée : " Sauf dans les cas où un régime de décision implicite d'acceptation est institué dans les conditions prévues à l'article 22, le silence gardé pendant plus de deux mois par l'autorité administrative sur une demande vaut décision de rejet (...) ". Aux termes de l'article R. 421-2 du code de justice administrative, dans sa rédaction alors applicable : " Sauf disposition législative ou réglementaire contraire, le silence gardé pendant plus de deux mois sur une réclamation par l'autorité compétente vaut décision de rejet. / Les intéressés disposent, pour se pourvoir contre cette décision implicite, d'un délai de deux mois à compter du jour de l'expiration de la période mentionnée au premier alinéa (...) ". Aux termes de l'article 19 de la loi du 12 avril 2000 déjà citée : " Toute demande adressée à une autorité administrative fait l'objet d'un accusé de réception délivré dans des conditions définies par décret en Conseil d'Etat. (...) / Les délais de recours ne sont pas opposables à l'auteur d'une demande lorsque l'accusé de réception ne lui a pas été transmis ou ne comporte pas les indications prévues par le décret mentionné au premier alinéa. ( ...) ". Aux termes de l'article 1er du décret du 6 juin 2001 pris pour l'application du chapitre II du titre II de la loi n° 2000-321 du 12 avril 2000 et relatif à l'accusé de réception des demandes présentées aux autorités administratives, en vigueur à la date de la décision attaquée et dont les dispositions sont désormais reprises à l'article R. 112-11-1 du code des relations entre le public et l'administration : " L'accusé de réception prévu par l'article 19 de la loi du 12 avril 2000 susvisée comporte les mentions suivantes : 1° La date de réception de la demande et la date à laquelle, à défaut d'une décision expresse, celle-ci sera réputée acceptée ou rejetée ; / 2° La désignation, l'adresse postale et, le cas échéant, électronique, ainsi que le numéro de téléphone du service chargé du dossier. / L'accusé de réception indique si la demande est susceptible de donner lieu à une décision implicite de rejet ou à une décision implicite d'acceptation. Dans le premier cas, l'accusé de réception mentionne les délais et les voies de recours à l'encontre de la décision. Dans le second cas, il mentionne la possibilité offerte au demandeur de se voir délivrer l'attestation prévue à l'article 22 de la loi du 12 avril 2000 susvisée ". Il résulte de ces dispositions qu'en l'absence d'un accusé de réception comportant les mentions prévues par ces dernières dispositions, les délais de recours contentieux contre une décision implicite de rejet ne sont pas opposables à son destinataire.<br/>
<br/>
              3. Le principe de sécurité juridique, qui implique que ne puissent être remises en cause sans condition de délai des situations consolidées par l'effet du temps, fait obstacle à ce que puisse être contestée indéfiniment une décision administrative individuelle qui a été notifiée à son destinataire, ou dont il est établi, à défaut d'une telle notification, que celui-ci a eu connaissance. En une telle hypothèse, si le non-respect de l'obligation d'informer l'intéressé sur les voies et les délais de recours, ou l'absence de preuve qu'une telle information a bien été fournie, ne permet pas que lui soient opposés les délais de recours fixés par le code de justice administrative, le destinataire de la décision ne peut exercer de recours juridictionnel au-delà d'un délai raisonnable. En règle générale et sauf circonstances particulières dont se prévaudrait le requérant, ce délai ne saurait, sous réserve de l'exercice de recours administratifs pour lesquels les textes prévoient des délais particuliers, excéder un an à compter de la date à laquelle une décision expresse lui a été notifiée ou de la date à laquelle il est établi qu'il en a eu connaissance.<br/>
<br/>
              4. Les règles énoncées au point 3, relatives au délai raisonnable au-delà duquel le destinataire d'une décision ne peut exercer de recours juridictionnel, qui ne peut en règle générale excéder un an sauf circonstances particulières dont se prévaudrait le requérant, sont également applicables à la contestation d'une décision implicite de rejet née du silence gardé par l'administration sur une demande présentée devant elle, lorsqu'il est établi que le demandeur a eu connaissance de la décision. La preuve d'une telle connaissance ne saurait résulter du seul écoulement du temps depuis la présentation de la demande. Elle peut en revanche résulter de ce qu'il est établi, soit que l'intéressé a été clairement informé des conditions de naissance d'une décision implicite lors de la présentation de sa demande, soit que la décision a par la suite été expressément mentionnée au cours de ses échanges avec l'administration, notamment à l'occasion d'un recours gracieux dirigé contre cette décision. Le demandeur, s'il n'a pas été informé des voies et délais de recours dans les conditions prévues par les textes cités au point 2, dispose alors, pour saisir le juge, d'un délai raisonnable qui court, dans la première hypothèse, de la date de naissance de la décision implicite et, dans la seconde, de la date de l'événement établissant qu'il a eu connaissance de la décision. <br/>
<br/>
              5. Pour rejeter comme tardives les conclusions de M. B...tendant à l'annulation de la décision implicite de rejet née du silence observé par l'administration sur sa demande présentée le 27 janvier 2004, le jugement attaqué retient que le principe de sécurité juridique fait obstacle à ce que cette décision implicite puisse être contestée devant le juge administratif au-delà d'un délai d'un an à compter de sa naissance, intervenue deux mois après le dépôt de la demande auprès de l'administration. <br/>
<br/>
              6. Il ressort des pièces du dossier soumis au juge du fond que l'administration se bornait à soutenir devant lui que les conclusions tendant à l'annulation de la décision implicite rejetant la demande d'échange du permis de conduire de M. B...étaient tardives faute d'avoir été présentées dans le délai de recours contentieux de deux mois prévu à l'article R. 421-1 du code de justice administrative. En soulevant d'office le moyen d'ordre public distinct, tiré de ce que ces conclusions n'avaient pas été présentées dans le délai raisonnable mentionné au point 3 ci-dessus, sans en informer au préalable les parties comme l'exigeaient les dispositions de l'article R. 611-7 du code de justice administrative, le tribunal administratif a entaché son jugement d'irrégularité.<br/>
<br/>
              7. Au surplus, en rejetant ces mêmes conclusions comme irrecevables au motif qu'elles avaient n'avait pas été présentées dans un délai raisonnable, sans rechercher s'il était établi que M. B...avait eu connaissance de l'existence d'une décision implicite de rejet, et en faisant courir ce délai de la date à laquelle la décision était née, alors qu'il était constant que l'administration n'avait pas informé l'intéressé lors de la présentation de sa demande des conditions de naissance d'une décision implicite, le tribunal administratif a méconnu les règles énoncées au point 4, entachant ainsi son jugement d'erreur de droit.<br/>
<br/>
              8. Il résulte de ce qui précède que le jugement attaqué doit être annulé en tant qu'il rejette les conclusions de M. B...relatives à la décision implicite rejetant sa demande présentée le 27 janvier 2004.<br/>
<br/>
              En ce qui concerne les conclusions dirigées contre le rejet de la demande présentée le 14 août 2014 : <br/>
<br/>
              9. Aux termes de l'article R. 222-3 du code de la route : " Tout permis de conduire national, en cours de validité, délivré par un Etat ni membre de la Communauté européenne, ni partie à l'accord sur l'Espace économique européen, peut être reconnu en France jusqu'à l'expiration d'un délai d'un an après l'acquisition de la résidence normale de son titulaire. Pendant ce délai, il peut être échangé contre le permis français, sans que son titulaire soit tenu de subir les examens prévus au premier alinéa de l'article  R. 221-3. Les conditions de cette reconnaissance et de cet échange sont définies par arrêté du ministre chargé des transports, après avis du ministre de la justice, du ministre de l'intérieur et du ministre chargé des affaires étrangères. Au terme de ce délai, ce permis n'est plus reconnu et son titulaire perd tout droit de conduire un véhicule pour la conduite duquel le permis de conduire est exigé ". Aux termes de l'article 5 de l'arrêté du 12 janvier 2012 visé ci-dessus, pris pour l'application de ces dispositions : " I. - Pour être échangé contre un titre français, tout permis de conduire délivré par un Etat n'appartenant ni à l'Union européenne, ni à l'Espace économique européen doit répondre aux conditions suivantes : / A.  Avoir été délivré au nom de l'Etat dans le ressort duquel le conducteur avait alors sa résidence normale, sous réserve qu'il existe un accord de réciprocité entre la France et cet Etat conformément à l'article R. 222-1 du code de la route. / (...)  ". Aux termes de l'article 14 du même arrêté : " Une liste des Etats dont les permis de conduire nationaux sont échangés en France contre un permis français est établie conformément aux articles R. 222-1 et R. 222-3 du code de la route. Cette liste précise pour chaque Etat la ou les catégories de permis de conduire concernée(s) par l'échange contre un permis français. Elle ne peut inclure que des Etats qui procèdent à l'échange des permis de conduire français de catégorie équivalente et dans lesquels les conditions effectives de délivrance des permis de conduire nationaux présentent un niveau d'exigence conforme aux normes françaises dans ce domaine. / Les demandes d'échange de permis introduites avant la date de publication au JORF de la liste prévue au premier alinéa du présent article sont traitées sur la base de la liste prévue à l'article 14 de l'arrêté du 8 février 1999 modifié fixant les conditions de reconnaissance et d'échange des permis de conduire délivrés par les Etats n'appartenant ni à l'Union européenne, ni à l'Espace économique européen ". L'article 14 de l'arrêté du  8 février 1999 dispose que le ministre chargé des transports établit, après consultation du ministre des affaires étrangères, la liste des Etats qui procèdent à l'échange des permis de conduire français.<br/>
<br/>
              10. Il résulte des termes du premier alinéa de l'article 14 de l'arrêté du 12 janvier 2012 cité ci-dessus que la liste d'Etats qu'il prévoit doit être établie conformément aux articles R. 222-1 et R. 222 3 du code de la route, à savoir " par arrêté du ministre chargé des transports, après avis du ministre de l'intérieur et du ministre chargé des affaires étrangères ". Aucune liste n'a été établie par le ministre des transports en application de ces dispositions. Le second alinéa du même article prévoit qu'en pareil cas, les demandes d'échange sont traitées sur la base de la liste prévue à l'article 14 de l'arrêté du 8 février 1999. Si une circulaire du 22 septembre 2006 du ministre des transports avait fixé une liste d'Etats sur le fondement de cet article, l'annexe de cette circulaire fixant la liste n'a pas été mise en ligne sur le site internet relevant du Premier ministre prévu au premier alinéa de l'article 1er du décret du 8 décembre 2008 relatif aux conditions de publication des instructions et circulaires, repris à l'article R. 312-8 du code des relations entre le public et l'administration. Par suite, en application de l'article 2 du même décret, aux termes duquel les instructions et circulaire déjà signées " sont regardées comme abrogées si elles ne sont pas reprises sur le site mentionné à l'article 1er ", la liste doit être regardée comme abrogée. Dans ces conditions, pour déterminer si un permis de conduire délivré par un Etat n'appartenant ni à l'Union européenne, ni à l'Espace économique européen est susceptible d'être échangé contre un permis français, il y a seulement lieu de vérifier si, conformément aux dispositions précitées du I de l'article 5 de l'arrêté du 12 janvier 2012, cet Etat est lié à la France par un accord de réciprocité en matière d'échange de permis de conduire.<br/>
<br/>
              11. Il résulte de ce qui précède qu'en se fondant, pour rejeter les conclusions de M. B...dirigées contre le refus opposé à sa demande du 14 août 2014, sur la circonstance que la circulaire du 22 septembre 2006 faisait obstacle à l'échange d'un permis de conduire camerounais, alors que cette circulaire devait être regardée comme abrogée, le tribunal a entaché son jugement d'une erreur de droit. Il suit de là, sans qu'il soit besoin d'examiner les moyens du pourvoi relatifs à cette partie du jugement, que celui-ci doit également être annulé en tant qu'il rejette les conclusions dirigées contre cette seconde décision implicite de rejet.<br/>
<br/>
              En ce qui concerne les conclusions indemnitaires :<br/>
<br/>
              12. Considérant que le jugement attaqué se borne sur ce point à énoncer " qu'en l'absence d'illégalité, les conclusions à fin d'indemnisation présentées par le requérant doivent être également rejetées ". Toutefois, le tribunal administratif, en rejetant comme irrecevables les conclusions tendant à l'annulation de la décision rejetant la demande présentée le 27 janvier 2004, n'avait pas pris parti sur la légalité de cette décision. Or, pour statuer sur les conclusions tendant à la réparation des préjudices qu'elle avait causés, il était tenu, alors même qu'il estimait que la décision était devenue définitive, de se prononcer sur sa légalité. Le jugement est donc entaché d'erreur de droit sur ce point. S'agissant des conclusions tendant à la réparation des conséquences dommageables de la décision rejetant la demande présentée le 14 août 2014, il doit être annulé par voie de conséquence de l'annulation prononcée au point 11.<br/>
<br/>
              13. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à M.B..., au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement n° 1502626 du 9 novembre 2017 du tribunal administratif de Melun est annulé. <br/>
Article 2 : L'affaire est renvoyée devant le tribunal administratif de Melun.<br/>
Article 3 : L'Etat versera la somme de 3 000 euros à M. B...au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : La présente décision sera notifiée à M. A...B...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-08 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. DÉCISIONS IMPLICITES. - DÉCISION IMPLICITE DE REJET NÉE DU SILENCE GARDÉ PAR L'ADMINISTRATION SUR UNE DEMANDE - OBLIGATION D'EXERCER UN RECOURS JURIDICTIONNEL DANS UN DÉLAI RAISONNABLE [RJ1] - 1) EXISTENCE, LORSQU'IL EST ÉTABLI QUE LE DEMANDEUR A EU CONNAISSANCE DE LA DÉCISION - 2) CONDITIONS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">01-04-03-07 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. PRINCIPES INTÉRESSANT L'ACTION ADMINISTRATIVE. - PRINCIPE DE SÉCURITÉ JURIDIQUE - IMPOSSIBILITÉ D'EXERCER UN RECOURS JURIDICTIONNEL AU-DELÀ D'UN DÉLAI RAISONNABLE - CAS DES DÉCISIONS IMPLICITES DE REJET [RJ1] - 1) PRINCIPE - APPLICATION, LORSQU'IL EST ÉTABLI QUE LE DEMANDEUR A EU CONNAISSANCE DE LA DÉCISION - 2) CONDITIONS.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-01-07 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉLAIS. - DÉCISION IMPLICITE DE REJET NÉE DU SILENCE GARDÉ PAR L'ADMINISTRATION SUR UNE DEMANDE - OBLIGATION D'EXERCER UN RECOURS JURIDICTIONNEL DANS UN DÉLAI RAISONNABLE [RJ1] - 1) EXISTENCE, LORSQU'IL EST ÉTABLI QUE LE DEMANDEUR A EU CONNAISSANCE DE LA DÉCISION - 2) CONDITIONS.
</SCT>
<ANA ID="9A"> 01-01-08 1) Les règles relatives au délai raisonnable au-delà duquel le destinataire d'une décision ne peut exercer de recours juridictionnel, qui ne peut en règle générale excéder un an sauf circonstances particulières dont se prévaudrait le requérant, sont également applicables à la contestation d'une décision implicite de rejet née du silence gardé par l'administration sur une demande présentée devant elle, lorsqu'il est établi que le demandeur a eu connaissance de la décision.... ,,2) La preuve d'une telle connaissance ne saurait résulter du seul écoulement du temps depuis la présentation de la demande. Elle peut en revanche résulter de ce qu'il est établi, soit que l'intéressé a été clairement informé des conditions de naissance d'une décision implicite lors de la présentation de sa demande, soit que la décision a par la suite été expressément mentionnée au cours de ses échanges avec l'administration, notamment à l'occasion d'un recours gracieux dirigé contre cette décision. Le demandeur, s'il n'a pas été informé des voies et délais de recours dans les conditions prévues par l'article 19 de la loi n° 2000-321 du 12 avril 2000 et l'article R. 112-11-1 du code des relations entre le public et l'administration (CRPA) dispose alors, pour saisir le juge, d'un délai raisonnable qui court, dans la première hypothèse, de la date de naissance de la décision implicite et, dans la seconde, de la date de l'événement établissant qu'il a eu connaissance de la décision.</ANA>
<ANA ID="9B"> 01-04-03-07 1) Les règles relatives au délai raisonnable au-delà duquel le destinataire d'une décision ne peut exercer de recours juridictionnel, qui ne peut en règle générale excéder un an sauf circonstances particulières dont se prévaudrait le requérant, sont également applicables à la contestation d'une décision implicite de rejet née du silence gardé par l'administration sur une demande présentée devant elle, lorsqu'il est établi que le demandeur a eu connaissance de la décision.... ,,2) La preuve d'une telle connaissance ne saurait résulter du seul écoulement du temps depuis la présentation de la demande. Elle peut en revanche résulter de ce qu'il est établi, soit que l'intéressé a été clairement informé des conditions de naissance d'une décision implicite lors de la présentation de sa demande, soit que la décision a par la suite été expressément mentionnée au cours de ses échanges avec l'administration, notamment à l'occasion d'un recours gracieux dirigé contre cette décision. Le demandeur, s'il n'a pas été informé des voies et délais de recours dans les conditions prévues par l'article 19 de la loi n° 2000-321 du 12 avril 2000 et l'article R. 112-11-1 du code des relations entre le public et l'administration (CRPA) dispose alors, pour saisir le juge, d'un délai raisonnable qui court, dans la première hypothèse, de la date de naissance de la décision implicite et, dans la seconde, de la date de l'événement établissant qu'il a eu connaissance de la décision.</ANA>
<ANA ID="9C"> 54-01-07 1) Les règles relatives au délai raisonnable au-delà duquel le destinataire d'une décision ne peut exercer de recours juridictionnel, qui ne peut en règle générale excéder un an sauf circonstances particulières dont se prévaudrait le requérant, sont également applicables à la contestation d'une décision implicite de rejet née du silence gardé par l'administration sur une demande présentée devant elle, lorsqu'il est établi que le demandeur a eu connaissance de la décision.... ,,2) La preuve d'une telle connaissance ne saurait résulter du seul écoulement du temps depuis la présentation de la demande. Elle peut en revanche résulter de ce qu'il est établi, soit que l'intéressé a été clairement informé des conditions de naissance d'une décision implicite lors de la présentation de sa demande, soit que la décision a par la suite été expressément mentionnée au cours de ses échanges avec l'administration, notamment à l'occasion d'un recours gracieux dirigé contre cette décision. Le demandeur, s'il n'a pas été informé des voies et délais de recours dans les conditions prévues par l'article 19 de la loi n° 2000-321 du 12 avril 2000 et l'article R. 112-11-1 du code des relations entre le public et l'administration (CRPA) dispose alors, pour saisir le juge, d'un délai raisonnable qui court, dans la première hypothèse, de la date de naissance de la décision implicite et, dans la seconde, de la date de l'événement établissant qu'il a eu connaissance de la décision.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., s'agissant des décisions expresses, CE, Assemblée, 13 juillet 2016, M.,, n° 387763, p. 340. Comp., s'agissant des rejets implicites de réclamations présentées sur le fondement de l'article R. 199-1 du LPF, CE, 8 février 2019, SARL Nick Danese Applied Research, n° 406555, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
