<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039655725</ID>
<ANCIEN_ID>JG_L_2019_12_000000410771</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/65/57/CETATEXT000039655725.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 20/12/2019, 410771</TITRE>
<DATE_DEC>2019-12-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>410771</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU ; SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR>Mme Sophie Baron</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Raphaël Chambon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:410771.20191220</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire complémentaire et un mémoire en réplique, enregistrés les 22 mai et 22 août 2017 et le 26 février 2018 au secrétariat du contentieux du Conseil d'Etat, la société Adomvet, M. E... B..., M. D... C... et Mme G... F... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du Conseil national de l'ordre des vétérinaires, prise en sa session des 21 et 22 mars 2017, annulant la décision du conseil régional Provence-Alpes-Côte-d'Azur-Corse de l'ordre des vétérinaires du 19 octobre 2016 et énonçant que l'activité de vétérinaire à domicile des vétérinaires collaborateurs d'Adomvet est illégale ;<br/>
<br/>
              2°) de mettre à la charge du Conseil national de l'ordre des vétérinaires la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la directive 2006/123/CE du Parlement européen et du Conseil du 12 décembre 2006 relative aux services dans le marché intérieur ; <br/>
              - le code rural et de la pêche maritime ;<br/>
              - la loi n° 2005-882 du 2 août 2005 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie Baron, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de la société Adomvet, de M. E... B..., de M. D... C... et de Mme G... F... et à la SCP Rousseau, Tapie, avocat du Conseil national de l'ordre des vétérinaires.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 6 décembre 2019, présentée par le Conseil national de l'ordre des vétérinaires. <br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article R. 242-53 du code rural et de la pêche maritime : " Le domicile professionnel d'exercice est le lieu d'implantation de locaux professionnels où s'exerce la profession de vétérinaire, accessibles à tout moment par le ou les vétérinaires qui y exercent. / Tout domicile professionnel d'exercice fait l'objet d'une déclaration au conseil régional de l'ordre dans le ressort duquel sont inscrits le ou les vétérinaires qui y exercent, et ce préalablement à son ouverture. Le conseil régional destinataire de cette déclaration informe le ou les conseils régionaux de la circonscription où se situent, le cas échéant, les autres domiciles professionnels d'exercice. / Tout vétérinaire inscrit à l'ordre et en exercice a au moins un domicile professionnel d'exercice. / Un vétérinaire ou un groupe de vétérinaires ayant pour but l'exercice professionnel en commun peuvent avoir plusieurs domiciles professionnels d'exercice (...) " et aux termes de l'article R. 242-54 de ce code : " L'établissement situé au domicile professionnel d'exercice, où sont amenés les animaux pour y être soignés, est dénommé établissement de soins vétérinaires ". Enfin, aux termes de l'article R. 242-64 du même code : " Un vétérinaire ou une société d'exercice peut s'adjoindre les services de vétérinaires salariés ou de collaborateurs libéraux ".<br/>
<br/>
              2. Il ressort des pièces du dossier que la société Adomvet, dont le domicile professionnel d'exercice se trouve à Lyon (Rhône), est inscrite au tableau de la région Auvergne-Rhône-Alpes de l'ordre des vétérinaires. En 2016, elle a déclaré auprès des instances ordinales de la région Auvergne-Rhône-Alpes et de la région Provence-Alpes Côte d'Azur-Corse la création d'un second domicile professionnel d'exercice situé à Carros (Alpes-Maritimes). Ce domicile professionnel d'exercice étant destiné à l'approvisionnement en matériel et médicaments vétérinaires d'un service vétérinaire de garde intervenant la nuit et le week-end, exclusivement aux domiciles des clients, la société Adomvet s'est à cette fin, assurée de la collaboration libérale de six vétérinaires inscrits au tableau de la région Provence-Alpes-Côte-d'Azur-Corse de l'ordre des vétérinaires. Par la présente requête, la société Adomvet et MM. B... et C... et A... F..., ses associés, demandent l'annulation pour excès de pouvoir de la décision des 21 et 22 mars 2017 du Conseil national de l'ordre des vétérinaires annulant la décision du conseil régional Provence-Alpes-Côte-d'Azur-Corse de l'ordre des vétérinaires du 19 octobre 2016 et indiquant que l'activité de vétérinaire à domicile des vétérinaires collaborateurs d'Adomvet dans le ressort de ce conseil régional est illégale. <br/>
<br/>
              3. En premier lieu, aucune disposition législative ou réglementaire ni aucun principe général n'impose que les décisions du Conseil national de l'ordre des vétérinaires portent mention de sa composition ou de ce qu'elles ont été adoptées à la majorité des voix. Ainsi, la société Adomvet et autres ne sont pas fondés à soutenir que la décision contestée serait entachée d'irrégularité faute de comporter de telles mentions. <br/>
<br/>
              4. En deuxième lieu, aux termes du premier alinéa de l'article R. 242-57 du code rural et de la pêche maritime : " Est dénommée vétérinaire à domicile la personne physique ou morale habilitée à exercer la médecine et la chirurgie des animaux qui, n'exerçant pas dans un établissement de soins vétérinaires, exerce sa profession au domicile du client. Le vétérinaire à domicile ne peut exercer cette activité pour le compte d'un vétérinaire ou d'une société possédant par ailleurs un ou plusieurs établissements de soins vétérinaires ". Les dispositions de cet article, qui trouvent à s'appliquer uniquement lorsque l'activité du vétérinaire à domicile et celles de la société pour le compte de laquelle il exerce éventuellement régissent des situations purement internes, font obstacle à ce qu'un vétérinaire ou une société possédant un ou plusieurs établissements de soins vétérinaires puisse s'adjoindre les services de vétérinaires salariés ou de collaborateurs libéraux en vue de leur confier une activité de vétérinaire à domicile qu'ils exerceraient à titre exclusif.<br/>
<br/>
              5. Il résulte de ce qui précède que les requérants ne peuvent utilement soutenir que les dispositions de l'article R. 242-57 sont incompatibles avec les objectifs de l'article 16 de la directive 2006/123/CE du Parlement européen et du Conseil du 12 décembre 2006 relative aux services dans le marché intérieur prévoyant que les Etats membres respectent le droit des prestataires de fournir des services dans un Etat membre autre que celui dans lequel ils sont établis. De plus, le moyen tiré de ce que ces mêmes dispositions seraient incompatibles avec les objectifs fixés à l'article 25 de cette même directive, qui est relatif à l'exercice de plusieurs activités différentes, est inopérant, dès lors que seul l'exercice de la médecine vétérinaire est en cause dans la présente affaire. Par suite, les requérants ne sont pas fondés à soutenir que le Conseil national de l'ordre des vétérinaires ne pouvait légalement se fonder, pour prendre la décision attaquée, sur les dispositions de l'article R. 242-57 du code rural et de la pêche maritime.<br/>
<br/>
              6. En outre, en estimant que les vétérinaires liés à la société Adomvet par un contrat de collaboration libérale exerçaient au domicile professionnel d'exercice de cette société situé à Carros en qualité de vétérinaires à domicile et en en déduisant que, compte tenu de ce que Adomvet dispose également d'un établissement de soins vétérinaires situé à son domicile professionnel d'exercice de Lyon, une telle activité de vétérinaires à domicile n'est pas autorisée, le Conseil national de l'ordre des vétérinaires a fait une application exacte des dispositions précitées de l'article R. 242-57.<br/>
<br/>
              7.  Enfin, le Conseil national de l'ordre des vétérinaires ayant énoncé à titre surabondant que les contrats de collaboration libérale en cause n'étaient pas conformes aux conditions fixées par l'article 18 de la loi du 2 août 2005 en faveur des petites et moyennes entreprises, le moyen des requérants tiré de l'erreur d'appréciation dont ce motif serait entaché est sans incidence sur le bien-fondé de la décision contestée et doit être écarté.  <br/>
<br/>
              8. Il résulte de tout ce qui précède que la société Adomvet et autres ne sont pas fondés à demander l'annulation pour excès de pouvoir de la décision des 21 et 22 mars 2017 qu'ils attaquent. <br/>
<br/>
              9. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge du Conseil national de l'ordre des vétérinaires qui n'est pas la partie perdante dans cette instance. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Adomvet et autres la somme de 3 000 euros que demande le Conseil national de l'ordre des vétérinaires au titre des mêmes dispositions. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la société Adomvet et autres est rejetée. <br/>
Article 2 : La société Adomvet, M. B..., M. C... et Mme F... verseront une somme de 3 000 euros au Conseil national de l'ordre des vétérinaires au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société Adomvet, première dénommée pour l'ensemble des requérants et au Conseil national de l'ordre des vétérinaires. <br/>
Copie en sera adressée pour information au ministre de l'agriculture et de l'alimentation.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">55-03-042 PROFESSIONS, CHARGES ET OFFICES. CONDITIONS D'EXERCICE DES PROFESSIONS. VÉTÉRINAIRES. - INTERDICTION POUR LES VÉTÉRINAIRES OU SOCIÉTÉS POSSÉDANT DES ÉTABLISSEMENTS DE SOINS VÉTÉRINAIRES DE S'ADJOINDRE LES SERVICES DE VÉTÉRINAIRES EXERÇANT EXCLUSIVEMENT À DOMICILE - 1) CHAMP D'APPLICATION - SITUATIONS PUREMENT INTERNES AU REGARD DU DROIT DE L'UNION EUROPÉENNE - CONSÉQUENCE - INOPÉRANCE DE L'INVOCATION DE LA DIRECTIVE SERVICES - 2) ILLUSTRATION.
</SCT>
<ANA ID="9A"> 55-03-042 1) L'article R. 242-57 du code rural et de la pêche maritime (CRPM), qui trouve à s'appliquer uniquement lorsque l'activité du vétérinaire à domicile et celles de la société pour le compte de laquelle il exerce éventuellement régissent des situations purement internes, fait obstacle à ce qu'un vétérinaire ou une société possédant un ou plusieurs établissements de soins vétérinaires puisse s'adjoindre les services de vétérinaires salariés ou de collaborateurs libéraux en vue de leur confier une activité de vétérinaire à domicile qu'ils exerceraient à titre exclusif.,,,Il résulte de ce qui précède que les requérants ne peuvent utilement soutenir que l'article R. 242-57 serait incompatible avec les objectifs de l'article 16 de la directive 2006/123/CE du Parlement européen et du Conseil du 12 décembre 2006 (dite Services).,,,2) En estimant que les vétérinaires liés à la société requérante par un contrat de collaboration libérale exerçaient au domicile professionnel d'exercice de cette société situé à Carros en qualité de vétérinaires à domicile et en en déduisant que, compte tenu de ce que la société dispose également d'un établissement de soins vétérinaires situé à son domicile professionnel d'exercice de Lyon, une telle activité de vétérinaires à domicile n'est pas autorisée, le Conseil national de l'ordre des vétérinaires a fait une application exacte de l'article R. 242-57.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
