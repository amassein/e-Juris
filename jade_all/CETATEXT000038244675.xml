<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038244675</ID>
<ANCIEN_ID>JG_L_2019_03_000000418985</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/24/46/CETATEXT000038244675.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 6ème chambres réunies, 18/03/2019, 418985</TITRE>
<DATE_DEC>2019-03-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418985</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BALAT ; LE PRADO</AVOCATS>
<RAPPORTEUR>M. Florian Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:418985.20190318</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme F...C..., Mme E...A...et M. B...A...ont demandé au tribunal administratif de la Guadeloupe de condamner le centre hospitalier universitaire (CHU) de Pointe-à-Pitre à leur verser les sommes respectives de 3 318 043 euros, 46 496,50 euros et 100 000 euros en réparation de leurs préjudices consécutifs à la prise en charge de Laure-Anne A...dans cet établissement le 4 mars 2010. Par un jugement n° 1400046 du 22 octobre 2015, le tribunal administratif a rejeté leurs demandes.<br/>
<br/>
              Par un arrêt n° 15BX04243 du 12 décembre 2017, la cour administrative d'appel de Bordeaux a rejeté l'appel formé  par les consorts A...et C...contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 13 mars et 13 juin 2018 au secrétariat du contentieux du Conseil d'Etat, MmeC..., Mme A..., et M. A...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge du CHU de Pointe-à-Pitre la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le code de la santé publique ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Balat, avocat de Mme D...C..., de Mme E...A...et de M. B...A...et à Me Le Prado, avocat du centre hospitalier universitaire de Pointe-à-Pitre/Abymes.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond qu'à la suite d'une tentative de suicide par voie médicamenteuse, Laure-AnneA..., alors âgée de 16 ans, a été admise au service des urgences du centre hospitalier universitaire de Pointe-à-Pitre le 4 mars 2010 à 9 heures. En raison de son état particulièrement agité et agressif, le psychiatre de l'établissement a prescrit l'administration d'un tranquillisant ainsi que son placement dans une chambre de dégrisement avec contention. Quelques minutes après son placement dans cette chambre, la patiente a provoqué un incendie en essayant de brûler ses liens avec un briquet qu'elle avait conservé dans la poche de son short. Atteinte de brûlures au troisième degré, Mme A...a été transférée dans le service de réanimation de l'hôpital, avant d'être transférée, le 10 mars 2010, au service des brûlés de l'hôpital Cochin à Paris. Une amputation des doigts de sa main droite avec greffe a dû être réalisée le 12 mars. Elle a ensuite été prise en charge, du 25 mars au 1er juin 2010, dans un centre de rééducation fonctionnelle, avant de pouvoir rentrer en Guadeloupe. Le juge des référés du tribunal administratif de la Guadeloupe, saisi par la mère de MmeA..., MmeC..., a ordonné, le 26 juillet 2011, une expertise médicale. A la suite de la remise par l'expert de son rapport, le 20 juillet 2012, Mme A...et ses parents ont introduit une requête devant le tribunal administratif tendant à l'indemnisation de leurs préjudices consécutifs à la prise en charge de Mme A...au centre hospitalier universitaire de Pointe-à-Pitre. Par un jugement du 22 octobre 2015, le tribunal administratif a rejeté leur requête. Par un arrêt du 12 décembre 2017 contre lequel ils se pourvoient en cassation, la cour administrative d'appel de Bordeaux a rejeté l'appel qu'ils avaient formé par contre ce jugement.<br/>
<br/>
              Sur la régularité de l'arrêt attaqué : <br/>
<br/>
              2. En premier lieu, la circonstance que le mémoire en défense produit par le centre hospitalier universitaire de Pointe-à-Pitre devant la cour n'a été communiqué à Mme A... et à ses parents que huit jours avant la date de clôture d'instruction n'est, dans les circonstances de l'espèce, pas de nature à entacher d'irrégularité la procédure d'appel. Par suite, le moyen tiré de l'atteinte au principe du contradictoire doit être écarté.<br/>
<br/>
              3. En second lieu, en jugeant que le centre hospitalier universitaire de Pointe-à-Pitre n'avait pas commis de faute en plaçant MmeA..., sous sédation et contention physique, dans une chambre d'isolement ouverte donnant sur le couloir, sans prendre expressément parti sur l'absence alléguée d'un détecteur de fumée dans cette chambre, la cour, qui n'était pas tenue de répondre à tous les arguments des requérants, n'a pas entaché son arrêt d'une insuffisance de motivation.<br/>
<br/>
              Sur le bien-fondé de l'arrêt :<br/>
<br/>
              4. Pour retenir qu'aucun manquement aux règles de l'art n'avait été commis dans la prise en charge de Laure-Anne A...lors de son admission au service des urgences, la cour a retenu, en se référant aux conclusions de l'expert judiciaire, que le psychiatre de l'établissement qui l'avait examinée dès son arrivée avait défini des mesures adaptées à l'état de l'adolescente en la faisant placer, après administration d'un tranquillisant, dans une chambre d'isolement ouverte, donnant sur le couloir du service, avec mise en place d'une contention physique, et qu'eu égard aux obligations incombant à un service d'urgence et dans les circonstances de l'espèce, il ne pouvait être reproché au personnel soignant de ne pas avoir préalablement déshabillé et fouillé l'adolescente, qui portait un tee-shirt, un short et des sandales. En jugeant ainsi, en tenant compte des moyens dont disposait le service, qui n'était pas spécialisé en psychiatrie, et de l'état de la patiente lors de son admission, que la circonstance que celle-ci avait pu conserver un briquet, qui se trouvait dans la poche de son short, ne suffisait pas à établir un manquement fautif dans sa prise en charge, la cour n'a pas commis d'erreur de droit et n'a pas inexactement qualifié les faits de l'espèce.<br/>
<br/>
              5. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge du CHU de Pointe-à-Pitre le versement à Mme A...et autres d'une somme au titre de ces dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de Mme A...et autres est rejeté.<br/>
Article 2 : La présente décision sera notifiée à Mme F...C..., Mme E... A...et M. B...A...et au centre hospitalier universitaire de Pointe-à-Pitre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-02-01-01-01-02-05 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICE PUBLIC DE SANTÉ. ÉTABLISSEMENTS PUBLICS D'HOSPITALISATION. RESPONSABILITÉ POUR FAUTE SIMPLE : ORGANISATION ET FONCTIONNEMENT DU SERVICE HOSPITALIER. ABSENCE DE FAUTE. SURVEILLANCE. - ABSENCE DE FOUILLE D'UNE ADOLESCENTE ADMISE AU SERVICE DES URGENCES - ESPÈCE.
</SCT>
<ANA ID="9A"> 60-02-01-01-01-02-05 Pour retenir qu'aucun manquement aux règles de l'art n'avait été commis dans la prise en charge d'une adolescente lors de son admission au service des urgences, la cour a retenu, en se référant aux conclusions de l'expert judiciaire, que le psychiatre de l'établissement qui l'avait examinée dès son arrivée avait défini des mesures adaptées à l'état de l'intéressée en la faisant placer, après administration d'un tranquillisant, dans une chambre d'isolement ouverte, donnant sur le couloir du service, avec mise en place d'une contention physique, et qu'eu égard aux obligations incombant à un service d'urgence et dans les circonstances de l'espèce, il ne pouvait être reproché au personnel soignant de ne pas avoir préalablement déshabillé et fouillé l'adolescente, qui portait un tee-shirt, un short et des sandales. En jugeant ainsi, en tenant compte des moyens dont disposait le service, qui n'était pas spécialisé en psychiatrie, et de l'état de la patiente lors de son admission, que la circonstance que celle-ci avait pu conserver un briquet, qui se trouvait dans la poche de son short, ne suffisait pas à établir un manquement fautif dans sa prise en charge, la cour n'a pas commis d'erreur de droit et n'a pas inexactement qualifié les faits de l'espèce.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
