<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000022203499</ID>
<ANCIEN_ID>JG_L_2010_04_000000323179</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/22/20/34/CETATEXT000022203499.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Assemblée, 29/04/2010, 323179, Publié au recueil Lebon</TITRE>
<DATE_DEC>2010-04-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>323179</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Assemblée</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Sauvé</PRESIDENT>
<AVOCATS>SCP COUTARD, MAYER, MUNIER-APAIRE</AVOCATS>
<RAPPORTEUR>Mme Delphine  Hedary</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guyomar Mattias</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CEASS:2010:323179.20100429</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le jugement du 8 décembre 2008, enregistré le 12 décembre 2008 au secrétariat du contentieux du Conseil d'Etat, par lequel le tribunal administratif de Marseille, avant de statuer sur la demande de M. et Mme A tendant à la condamnation de la Société Electricité de France - Energie Méditerranée à les indemniser des dommages qu'ils soutiennent subir en raison de la présence et du fonctionnement de la centrale thermique de Martigues-Ponteau, a décidé, en application des dispositions de l'article L. 113-1 du code de justice administrative, de transmettre le dossier de cette demande au Conseil d'Etat en soumettant à son examen la question de savoir si, en raison de l'intervention des lois des 10 février 2000 et 9 août 2004 qui ont donné une nouvelle définition du service public de l'électricité et modifié le statut d'Electricité de France, les établissements de production électrique détenus par cette société conservent leur caractère d'ouvrage public ;<br/>
.....................................................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la directive 2003/54/CE du Parlement européen et du Conseil, du 26 juin 2003, concernant des règles communes pour le marché intérieur de l'électricité et abrogeant la directive 96/92/CE ;<br/>
<br/>
              Vu la directive 2009/72/CE du Parlement européen et du Conseil du 13 juillet 2009 concernant des règles communes pour le marché intérieur de l'électricité et abrogeant la directive 2003/54/CE ;<br/>
<br/>
              Vu la loi du 16 octobre 1919 relative à l'utilisation de l'énergie hydraulique ;<br/>
<br/>
              Vu la loi n° 2000-108 du 10 février 2000 modifiée relative à la modernisation et au développement du service public de l'électricité ;<br/>
<br/>
              Vu la loi n° 2004-803 du 9 août 2004 relative au service public de l'électricité et du gaz et aux entreprises électriques et gazières ;<br/>
<br/>
              Vu le décret n° 2008-386 du 23 avril 2008 relatif aux prescriptions techniques générales de conception et de fonctionnement pour le raccordement d'installations de production aux réseaux publics d'électricité ;<br/>
<br/>
              Vu l'arrêté du 23 avril 2008 relatif aux prescriptions techniques de conception et de fonctionnement pour le raccordement au réseau public de transport d'électricité d'une installation de production d'énergie électrique ;<br/>
<br/>
              Vu l'arrêté du 23 avril 2008 relatif aux prescriptions techniques de conception et de fonctionnement pour le raccordement à un réseau public de distribution d'électricité en basse tension ou en moyenne tension d'une installation de production d'énergie électrique ;<br/>
<br/>
              Vu le code de justice administrative, notamment ses articles L. 113-1, R. 621-1 à R. 621-7 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Delphine Hedary, maître des requêtes,<br/>
<br/>
              - les observations de la SCP Coutard, Mayer, Munier-Apaire, avocat d'Electricité de France, <br/>
<br/>
              - les conclusions de M. Mattias Guyomar, rapporteur public,<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Coutard, Mayer, Munier-Apaire, avocat d'Electricité de France ;<br/>
<br/>
<br/>
<br/>
              REND L'AVIS SUIVANT :<br/>
<br/>
              La qualification d'ouvrage public peut être déterminée par la loi. Présentent aussi le caractère d'ouvrage public notamment les biens immeubles résultant d'un aménagement, qui sont directement affectés à un service public, y compris s'ils appartiennent à une personne privée chargée de l'exécution de ce service public.<br/>
<br/>
              S'agissant des ouvrages de production d'électricité, il se déduit de l'article 2 de la loi du 16 octobre 1919 relative à l'utilisation de l'énergie hydraulique, aux termes duquel " Sont placées sous le régime de la concession les entreprises dont la puissance (...) excède 4 500 kilowatts ", et de l'article 10 de cette même loi, qui prévoit que des obligations sont imposées aux exploitants de ces centrales, que cette loi a entendu donner à l'ensemble des ouvrages de production d'énergie hydroélectrique concédés, que la personne qui en est propriétaire soit publique ou privée, le caractère d'ouvrage public.<br/>
<br/>
              Le statut des autres ouvrages de production d'électricité n'a été déterminé ni par la loi du 10 février 2000 qui a défini le service public de l'électricité, ni par celle du 9 août 2004 qui a transformé Electricité de France en société de droit privé. Il faut donc rechercher, dans le cas où des personnes privées sont propriétaires d'ouvrages de production d'électricité, si elles sont chargées de l'exécution d'un service public et si les ouvrages en cause sont directement affectés à ce service public.<br/>
<br/>
              L'article 1er de la loi du 10 février 2000, qui n'a pas été modifié sur ce point par la loi du 9 août 2004, dispose que : " Le service public de l'électricité a pour objet de garantir l'approvisionnement en électricité sur l'ensemble du territoire, dans le respect de l'intérêt général. / Dans le respect de la politique énergétique, il contribue à l'indépendance et à la sécurité de l'approvisionnement (...) ". L'article 2 de la même loi prévoit que : " Selon les principes et conditions énoncés à l'article 1er, le service public de l'électricité assure le développement équilibré de l'approvisionnement en électricité, le développement et l'exploitation des réseaux publics de transport et de distribution d'électricité ainsi que la fourniture d'électricité, dans les conditions définies ci-après. / I.- La mission de développement équilibré de l'approvisionnement en électricité vise : / 1° A réaliser les objectifs définis par la programmation pluriannuelle des investissements de production arrêtée par le ministre chargé de l'énergie ; / 2° A garantir l'approvisionnement des zones du territoire non interconnectées au réseau métropolitain continental. / Les producteurs, et notamment Electricité de France, contribuent à la réalisation de ces objectifs. Les charges qui en découlent, notamment celles résultant des articles 8 et 10, font l'objet d'une compensation intégrale dans les conditions prévues au I de l'article 5. ". <br/>
<br/>
              Il résulte de ces dispositions que la sécurité de l'approvisionnement sur l'ensemble du territoire national constitue le principal objet du service public de l'électricité. Cette sécurité d'approvisionnement exige, eu égard aux caractéristiques physiques de l'énergie électrique, qui ne peut être stockée, que soit assuré à tout moment l'équilibre entre la production et la consommation dont résultent la sécurité et la fiabilité du réseau de transport. De plus, dans les zones interconnectées du territoire métropolitain, la limite des capacités d'importation des réseaux transfrontières, qui ne représentent qu'une faible part du volume de la consommation maximale, impose que l'essentiel de la production soit réalisée sur ce territoire. Dans les zones non interconnectées, la production locale doit actuellement couvrir l'intégralité des besoins de la consommation.<br/>
<br/>
              A ces fins, la loi du 10 février 2000 prévoit, conformément à ce que permet la directive du 26 juin 2003, comme celle du 13 juillet 2009 qui entrera en vigueur le 3 mars 2011, que des obligations soient imposées aux ouvrages de production d'électricité dont le fonctionnement est indispensable à l'équilibre entre la production et la consommation et donc à la sécurité et à la fiabilité du réseau public de transport. <br/>
<br/>
              L'article 14 de la loi du 10 février 2000 prévoit ainsi que des " prescriptions techniques générales de conception et de fonctionnement pour le raccordement au réseau public de transport " s'imposent aux installations de production raccordées à ce réseau, afin d'assurer la sécurité et la sûreté du réseau et la qualité de son fonctionnement. Le III de l'article 15 impose également que " la totalité de la puissance non utilisée techniquement disponible " de chacune de ces mêmes installations soit mise à la disposition du gestionnaire du Réseau de Transport d'Electricité (RTE) pour permettre à celui-ci d'assurer l'ajustement entre la production et la consommation d'électricité. Il résulte de l'instruction, et notamment des indications données au cours de l'audience d'instruction, que ces prescriptions et contraintes s'imposent, en l'état actuel de la réglementation, aux ouvrages de production d'électricité dont la puissance est supérieure à 12 MW. Les prescriptions techniques générales de conception et de fonctionnement, qui résultent du décret du 23 avril 2008 et des arrêtés ministériels du même jour visés ci-dessus, sont plus contraignantes pour les ouvrages de production d'électricité dont la puissance est supérieure à 40 MW. En effet, ceux-ci ont l'obligation d'être équipés de mécanismes automatiques permettant de réguler leur puissance active en fonction des variations de la fréquence sur ce réseau, laquelle doit rester comprise entre 49,5 et 50,5 Hertz pour assurer la sécurité et la sûreté du réseau et, par voie de conséquence, la sécurité de l'approvisionnement. Ils doivent également pouvoir, en cas de déconnexion fortuite du réseau, s'y raccorder " sans délai " à la demande de RTE, tandis que les ouvrages de production d'électricité raccordés au réseau dont la puissance est inférieure à 40 MW ont seulement l'obligation de pouvoir le faire " rapidement ". <br/>
<br/>
              Il résulte de ce qui précède que la sécurité de l'approvisionnement en électricité sur l'ensemble du territoire national implique nécessairement que soient imposées à certains ouvrages de production d'électricité des contraintes particulières quant à leurs conditions de fonctionnement, afin d'assurer l'équilibre, la sécurité et la fiabilité de l'ensemble du système. Les ouvrages auxquels sont imposées ces contraintes en raison de la contribution déterminante qu'ils apportent à l'équilibre du système d'approvisionnement en électricité doivent être regardés comme directement affectés au service public et ils ont par suite le caractère d'ouvrage public. Leurs propriétaires, même privés, sont ainsi, dans cette mesure, chargés d'exécuter ce service public. En l'état actuel des techniques et eu égard aux caractéristiques d'ensemble du système électrique, présentent le caractère d'ouvrage public les ouvrages d'une puissance supérieure à 40 MW qui sont installés dans les zones interconnectées du territoire métropolitain.<br/>
<br/>
              Il ressort des pièces du dossier et des éléments recueillis lors de l'audience d'instruction que, dans les zones non interconnectées, l'ensemble des ouvrages dont la production est entièrement destinée de façon permanente aux réseaux de transport ou de distribution sont nécessaires pour garantir la sécurité d'approvisionnement. Dès lors, de tels ouvrages doivent être regardés comme affectés au service public de la sécurité de l'approvisionnement et ont, par suite, le caractère d'ouvrage public. <br/>
<br/>
<br/>
<br/>
<br/>
              Le présent avis sera notifié au président du tribunal administratif de Marseille, à M. et Mme A, à la société Electricité de France et au ministre d'Etat, ministre de l'écologie, de l'énergie, du développement durable et de la mer, en charge des technologies vertes et des négociations sur le climat.<br/>
<br/>
              Une copie du présent avis sera adressée à la Commission de Régulation de l'Energie et au Réseau de Transport d'Electricité.<br/>
<br/>
              Il sera publié au Journal officiel de la République française.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">29-01-01 ENERGIE. ÉLECTRICITÉ DE FRANCE. STATUT. - LOI DU 9 AOÛT 2004 TRANSFORMANT EDF EN SOCIÉTÉ DE DROIT PRIVÉ - CONSÉQUENCE - OUVRAGES DE PRODUCTION D'ÉLECTRICITÉ LUI APPARTENANT AUXQUELS SONT IMPOSÉES DES CONTRAINTES PARTICULIÈRES DE FONCTIONNEMENT AFIN D'ASSURER L'ÉQUILIBRE, LA SÉCURITÉ ET LA FIABILITÉ DU SYSTÈME D'APPROVISIONNEMENT EN ÉLECTRICITÉ - OUVRAGES AFFECTÉS AU SERVICE PUBLIC DE LA SÉCURITÉ DE L'APPROVISIONNEMENT EN ÉLECTRICITÉ -  OUVRAGES PRÉSENTANT LE CARACTÈRE D'OUVRAGE PUBLIC [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">67-01-02 TRAVAUX PUBLICS. NOTION DE TRAVAIL PUBLIC ET D'OUVRAGE PUBLIC. OUVRAGE PUBLIC. - 1) NOTION - 2) OUVRAGES DE PRODUCTION D'ÉLECTRICITÉ AUXQUELS SONT IMPOSÉES DES CONTRAINTES PARTICULIÈRES DE FONCTIONNEMENT AFIN D'ASSURER L'ÉQUILIBRE, LA SÉCURITÉ ET LA FIABILITÉ DU SYSTÈME D'APPROVISIONNEMENT EN ÉLECTRICITÉ [RJ1] - OUVRAGES AFFECTÉS AU SERVICE PUBLIC DE LA SÉCURITÉ DE L'APPROVISIONNEMENT EN ÉLECTRICITÉ - OUVRAGE PRÉSENTANT LE CARACTÈRE D'OUVRAGE PUBLIC.
</SCT>
<ANA ID="9A"> 29-01-01 La qualification d'ouvrage public peut être déterminée par la loi. Présentent aussi le caractère d'ouvrage public notamment les biens immeubles résultant d'un aménagement, qui sont directement affectés à un service public, y compris s'ils appartiennent à une personne privée chargée de l'exécution de ce service public.... ...S'agissant des ouvrages de production d'électricité, si la loi du 16 octobre 1919 relative à l'utilisation de l'énergie hydraulique a entendu donner le caractère d'ouvrage public aux ouvrages de production d'énergie hydroélectrique concédés, que la personne qui en est propriétaire soit publique ou privée, le statut des autres ouvrages de production d'électricité n'a été déterminé ni par la loi n° 2000-108 du 10 février 2000 qui a défini le service public de l'électricité, ni par la loi n° 2004-803 du 9 août 2004 qui a transformé Electricité de France en société de droit privé. Il faut donc rechercher, dans le cas où des personnes privées sont propriétaires d'ouvrages de production d'électricité, si elles sont chargées de l'exécution d'un service public et si les ouvrages en cause sont directement affectés à ce service public. Il résulte des dispositions des articles 1er et 2 de la loi du 10 février 2000 que la sécurité de l'approvisionnement sur l'ensemble du territoire national constitue le principal objet du service public de l'électricité. Cette sécurité d'approvisionnement exige, eu égard aux caractéristiques physiques de l'énergie électrique, qui ne peut être stockée, que soit assuré à tout moment l'équilibre entre la production et la consommation dont résultent la sécurité et la fiabilité du réseau de transport. A ces fins, la loi du 10 février 2000 prévoit, conformément à ce que permet la directive 2003/54/CE du 26 juin 2003, que des obligations soient imposées aux ouvrages de production d'électricité dont le fonctionnement est indispensable à l'équilibre entre la production et la consommation et donc à la sécurité et à la fiabilité du réseau public de transport. Il en résulte que la sécurité de l'approvisionnement en électricité sur l'ensemble du territoire national implique nécessairement que soient imposées à certains ouvrages de production d'électricité des contraintes particulières quant à leurs conditions de fonctionnement, afin d'assurer l'équilibre, la sécurité et la fiabilité de l'ensemble du système. Les ouvrages auxquels sont imposées ces contraintes en raison de la contribution déterminante qu'ils apportent à l'équilibre du système d'approvisionnement en électricité doivent être regardés comme directement affectés au service public et ont, par suite, le caractère d'ouvrage public. Leurs propriétaires, même privés, sont ainsi, dans cette mesure, chargés d'exécuter ce service public. En l'état actuel des techniques et eu égard aux caractéristiques d'ensemble du système électrique, présentent le caractère d'ouvrage public les ouvrages d'une puissance supérieure à 40 mégawatts qui sont installés dans les zones interconnectées du territoire métropolitain.</ANA>
<ANA ID="9B"> 67-01-02 1) La qualification d'ouvrage public peut être déterminée par la loi. Présentent aussi le caractère d'ouvrage public notamment les biens immeubles résultant d'un aménagement, qui sont directement affectés à un service public, y compris s'ils appartiennent à une personne privée chargée de l'exécution de ce service public.,,2) S'agissant des ouvrages de production d'électricité, si la loi du 16 octobre 1919 relative à l'utilisation de l'énergie hydraulique a entendu donner le caractère d'ouvrage public aux ouvrages de production d'énergie hydroélectrique concédés, que la personne qui en est propriétaire soit publique ou privée, le statut des autres ouvrages de production d'électricité n'a été déterminé ni par la loi n° 2000-108 du 10 février 2000 qui a défini le service public de l'électricité, ni par la loi n° 2004-803 du 9 août 2004 qui a transformé Electricité de France en société de droit privé. Il faut donc rechercher, dans le cas où des personnes privées sont propriétaires d'ouvrages de production d'électricité, si elles sont chargées de l'exécution d'un service public et si les ouvrages en cause sont directement affectés à ce service public. Il résulte des dispositions des articles 1er et 2 de la loi du 10 février 2000 que la sécurité de l'approvisionnement sur l'ensemble du territoire national constitue le principal objet du service public de l'électricité. Cette sécurité d'approvisionnement exige, eu égard aux caractéristiques physiques de l'énergie électrique, qui ne peut être stockée, que soit assuré à tout moment l'équilibre entre la production et la consommation dont résultent la sécurité et la fiabilité du réseau de transport. A ces fins, la loi du 10 février 2000 prévoit, conformément à ce que permet la directive 2003/54/CE du 26 juin 2003, que des obligations soient imposées aux ouvrages de production d'électricité dont le fonctionnement est indispensable à l'équilibre entre la production et la consommation et donc à la sécurité et à la fiabilité du réseau public de transport. Il en résulte que la sécurité de l'approvisionnement en électricité sur l'ensemble du territoire national implique nécessairement que soient imposées à certains ouvrages de production d'électricité des contraintes particulières quant à leurs conditions de fonctionnement, afin d'assurer l'équilibre, la sécurité et la fiabilité de l'ensemble du système. Les ouvrages auxquels sont imposées ces contraintes en raison de la contribution déterminante qu'ils apportent à l'équilibre du système d'approvisionnement en électricité doivent être regardés comme directement affectés au service public et ont, par suite, le caractère d'ouvrage public. Leurs propriétaires, même privés, sont ainsi, dans cette mesure, chargés d'exécuter ce service public. En l'état actuel des techniques et eu égard aux caractéristiques d'ensemble du système électrique, présentent le caractère d'ouvrage public les ouvrages d'une puissance supérieure à 40 mégawatts qui sont installés dans les zones interconnectées du territoire métropolitain.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., pour les ouvrages affectés au service public de distribution de l'électricité, TC, 12 avril 2010, Société ERDF c/ M. et Mme Michel, n° 3718, à publier au Recueil. Comp., s'agissant des ouvrages détenus par France Télécom, Section, 11 juillet 2001, Adelée, n° 229486, p. 372.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
