<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030192200</ID>
<ANCIEN_ID>JG_L_2015_02_000000365212</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/19/22/CETATEXT000030192200.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 04/02/2015, 365212, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-02-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>365212</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LEVIS</AVOCATS>
<RAPPORTEUR>Mme Anne Iljic</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:365212.20150204</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés au secrétariat du contentieux du Conseil d'Etat les 15 janvier 2013 et 12 avril 2013, présentés pour la région de La Réunion ; la région de La Réunion demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 11BX03333 du 16 octobre 2012 par lequel la cour administrative d'appel de Bordeaux, après avoir annulé le jugement n° 0800125 du 6 octobre 2011 du tribunal administratif de Saint-Denis condamnant l'Etat à lui verser la somme de 9 714 263 euros avec intérêts au taux légal à compter du 3 septembre 2007 correspondant au solde de la dotation de continuité territoriale dû au titre des années 2005 et 2006, a rejeté sa demande présentée en première instance ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel du ministre des outre-mer ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 2003-660 du 21 juillet 2003 ;<br/>
<br/>
              Vu le décret n° 2004-100 du 30 janvier 2004 ;<br/>
<br/>
              Vu la décision n° 2003-474 DC du Conseil constitutionnel ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anne Iljic, auditeur,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lévis, avocat de la région de La Réunion ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'en application des dispositions de l'article 60 de la loi du 21 juillet 2003 de programme pour l'outre-mer et du décret du 30 janvier 2004 pris pour son application, le montant de la dotation de continuité territoriale allouée à la région de La Réunion pour les années 2005 et 2006 a été respectivement fixé, par arrêtés ministériels, à 8 148 888 euros pour l'année 2005 et à 8 611 697 euros pour l'année 2006 ; que, dans la mesure où la région de La Réunion ne justifiait pas de l'utilisation de l'ensemble des crédits qui lui étaient attribués au titre de cette dotation, l'Etat n'a toutefois versé à cette dernière qu'une somme de 1 423 297 euros pour l'année 2005 et de 5 893 225 euros pour l'année 2006, soit une différence de 9 714 263 euros par rapport aux montants initialement prévus ; que, par un jugement du 6 octobre 2011, le tribunal administratif de Saint-Denis, faisant droit à la demande de la région, a condamné l'Etat à verser à cette dernière cette somme de 9 714 263 euros, assortie des intérêts au taux légal à compter du 3 septembre 2007 ; que la région de La Réunion se pourvoit en cassation contre l'arrêt du 16 octobre 2012 par lequel la cour administrative d'appel de Bordeaux a annulé ce jugement du 6 octobre 2011 et rejeté la demande qu'elle avait présentée en première instance ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article 60 de la loi du 21 juillet 2003 de programmation pour l'outre-mer, applicable à la date des faits en litige : " L'Etat verse aux régions de la Guadeloupe, de la Guyane, de la Martinique et de la Réunion (...) une dotation de continuité territoriale dont le montant évolue comme la dotation globale de fonctionnement. / Cette dotation est destinée à faciliter les déplacements des résidents de ces collectivités entre celles-ci et le territoire métropolitain. Elle contribue à financer une aide au passage aérien des résidents dans des conditions déterminées par la collectivité. / Un décret en Conseil d'Etat fixe les modalités de répartition de cette dotation entre les collectivités en tenant compte notamment de l'éloignement de chacune d'entre elles avec la métropole ainsi que les modalités d'établissement par chaque collectivité du bilan annuel et des statistiques liées à cette aide qui seront communiqués au représentant de l'Etat " ; que le décret du 30 janvier 2004, pris pour l'application des dispositions précitées de la loi du 21 juillet 2003, prévoit que la dotation de continuité territoriale est calculée sur la base de critères déterminés au nombre desquels figurent la distance de la collectivité concernée à la métropole, sa population, ainsi que les caractéristiques du trafic aérien, auxquels sont appliqués un coefficient correcteur tenant compte de la qualité et de la diversité des liaisons de transport disponibles ; que les articles 4 et 5 de ce même texte prévoient en outre qu'il est rendu compte au représentant de l'Etat de l'utilisation de la dotation de continuité territoriale allouée au moyen de rapports semestriels et annuels relatifs aux caractéristiques du régime d'aide mis en place, à ses bénéficiaires et à l'état d'utilisation de la dotation ; <br/>
<br/>
              3. Considérant qu'il résulte de ce qui précède que la dotation de continuité territoriale a, ainsi que l'a jugé le Conseil constitutionnel dans sa décision n° 2003-474 DC du 17 juillet 2003, été instituée dans un but déterminé, consistant à faciliter les déplacements des personnes résidant outre-mer en direction du territoire métropolitain ; que si son montant évolue en référence à celui de la dotation globale de fonctionnement et est en principe déterminé, chaque année, en fonction des critères objectifs énoncés par le décret du 30 janvier 2004, aucune disposition législative ou réglementaire ni aucun principe ne fait obstacle à ce que, compte tenu de la finalité qu'elle poursuit, le pouvoir réglementaire procède à la modulation du montant de la dotation de continuité territoriale allouée en fonction de son utilisation effective, retracée dans les bilans annuels et semestriels remis au représentant de l'Etat ; qu'il suit de là que la cour administrative d'appel de Bordeaux, qui n'a pas entaché son arrêt de dénaturation, n'a pas commis d'erreur de droit en jugeant que le pouvoir réglementaire pouvait légalement subordonner le versement de la dotation de continuité territoriale à la justification de son utilisation effective par les collectivités ; <br/>
<br/>
              4. Considérant qu'il résulte de tout ce qui précède que la région de La Réunion n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque, qui est suffisamment motivé ; <br/>
<br/>
              5. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante ; qu'il n y a pas lieu, dans les circonstances de l'espèce, de mettre une somme à la charge de la région de La Réunion au titre de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la région de La Réunion est rejeté.<br/>
<br/>
Article 2 : Les conclusions présentées par l'Etat sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à la région de La Réunion et à la ministre des outre-mer.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
