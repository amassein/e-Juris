<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035317253</ID>
<ANCIEN_ID>JG_L_2017_07_000000398275</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/31/72/CETATEXT000035317253.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème chambres réunies, 28/07/2017, 398275, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-07-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>398275</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>Mme Marie Sirinelli</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:398275.20170728</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Lille d'annuler pour excès de pouvoir la décision du 26 octobre 2012 par laquelle le directeur général délégué chargé de l'offre de soins de l'agence régionale de santé du Nord - Pas-de-Calais a refusé d'enregistrer son diplôme d'ostéopathe, ainsi que la décision par laquelle il a implicitement rejeté son recours gracieux formé le 26 janvier 2013. Par un jugement n° 1303277 du 26 février 2015, le tribunal administratif de Lille a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 15DA00726 du 26 janvier 2016, la cour administrative d'appel de Douai a rejeté l'appel formé par M. B...contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 29 mars 2016, 29 juin 2016 et 23 mai 2017 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'agence régionale de santé du Nord - Pas-de-Calais la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - la loi n° 2002-303 du 4 mars 2002 ;<br/>
              - les décrets n° 2007-435 et n° 2007-437 du 25 mars 2007 ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Sirinelli, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat de M.B....<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 26 octobre 2012, le directeur général délégué chargé de l'offre de soins de l'agence régionale de santé du Nord - Pas-de-Calais a refusé d'enregistrer le diplôme universitaire d'ostéopathie délivré le 10 septembre 2012 par la faculté de médecine de l'université Lille II à M.B..., masseur-kinésithérapeute. Ce dernier se pourvoit en cassation contre l'arrêt du 26 janvier 2016 par lequel la cour administrative d'appel de Douai a rejeté l'appel qu'il avait formé contre le jugement du tribunal administratif de Lille du 26 février 2015 rejetant sa demande tendant à l'annulation de cette décision et du rejet de son recours gracieux.<br/>
<br/>
              2. D'une part, l'article 75 de la loi du 4 mars 2002 relative aux droits des malades et à la qualité du système de santé, dans sa rédaction alors applicable, dispose : " L'usage professionnel du titre d'ostéopathe (...) est réservé aux personnes titulaires d'un diplôme sanctionnant une formation spécifique à l'ostéopathie (...) délivrée par un établissement de formation agréé par le ministre chargé de la santé dans des conditions fixées par décret. (...) / Ces praticiens ne peuvent exercer leur profession que s'ils sont inscrits sur une liste dressée par le directeur général de l'agence régionale de santé de leur résidence professionnelle, qui enregistre leurs diplômes, certificats, titres ou autorisations ". L'article 5 du décret du 25 mars 2007 relatif aux actes et aux conditions d'exercice de l'ostéopathie précise que " L'autorisation de faire usage professionnel du titre d'ostéopathe est subordonnée à l'enregistrement sans frais des diplômes, certificats, titres ou autorisations de ces professionnels auprès du directeur général de l'agence régionale de santé de leur résidence professionnelle. (...) / Lors de l'enregistrement, ils doivent préciser la nature des études suivies ou des diplômes leur permettant l'usage du titre d'ostéopathe et, s'ils sont professionnels de santé, les diplômes d'Etat, titres, certificats ou autorisations mentionnés au présent décret dont ils sont également titulaires. / Il est établi, pour chaque département, par le directeur général de l'agence régionale de santé, une liste des praticiens habilités à faire un usage de ces titres, portée à la connaissance du public ".<br/>
<br/>
              3. D'autre part, l'article 4 du décret du 25 mars 2007 relatif aux actes et aux conditions d'exercice de l'ostéopathie, dans sa rédaction alors en vigueur, dispose : " L'usage professionnel du titre d'ostéopathe est réservé : / 1° Aux médecins, sages-femmes, masseurs-kinésithérapeutes et infirmiers autorisés à exercer, titulaires d'un diplôme universitaire ou interuniversitaire sanctionnant une formation suivie au sein d'une unité de formation et de recherche de médecine délivré par une université de médecine et reconnu par le Conseil national de l'ordre des médecins ; / 2° Aux titulaires d'un diplôme délivré par un établissement agréé dans les conditions prévues aux articles 5 à 9 du décret du 25 mars 2007 [relatif à la formation des ostéopathes et à l'agrément des établissements de formation] ;  / 3° Aux titulaires d'une autorisation d'exercice de l'ostéopathie ou d'user du titre d'ostéopathe délivrée par le directeur général de l'agence régionale de santé (...) ". L'article 9 du décret du 25 mars 2007 relatif à la formation des ostéopathes et à l'agrément des établissements de formation, dans sa rédaction alors en vigueur, dispose quant à lui : " La condition d'agrément mentionnée à l'article 75 de la loi n° 2002-203 du 4 mars 2002 (...) est remplie pour les universités qui délivrent des diplômes universitaires ou des diplômes interuniversitaires d'ostéopathie à des titulaires de diplômes, certificats, titres ou autorisations leur permettant d'exercer une profession médicale ou d'auxiliaires médicaux ".<br/>
<br/>
              4. Il résulte du 1° et du 2° de l'article 4 précité du décret du 25 mars 2007 relatif aux actes et aux conditions d'exercice de l'ostéopathie que, si l'usage professionnel du titre d'ostéopathe par les titulaires d'un diplôme universitaire ou interuniversitaire sanctionnant une formation suivie au sein d'une unité de formation et de recherche de médecine délivré par une université de médecine et reconnu par le Conseil national de l'ordre des médecins est réservé aux médecins, sages-femmes, masseurs-kinésithérapeutes et infirmiers autorisés à exercer, cet usage est en revanche ouvert à toute personne titulaire d'un diplôme délivré par un établissement de formation agréé par le ministre chargé de la santé dans les conditions alors prévues par le décret du 25 mars 2007 relatif à la formation des ostéopathes et à l'agrément des établissements de formation. <br/>
<br/>
              5. Toutefois, les dispositions précitées de l'article 9 de ce dernier décret n'ont pu avoir pour objet ou pour effet, eu égard aux conditions posées par le 1° de l'article 4 du décret relatif aux actes et aux conditions d'exercice de l'ostéopathie, de permettre que les professionnels de santé titulaires d'un diplôme universitaire ou interuniversitaire sanctionnant une formation suivie au sein d'une unité de formation et de recherche de médecine délivré par une université de médecine qui ne serait pas reconnu par le Conseil national de l'ordre des médecins soient regardés comme satisfaisant à la condition mentionnée au 2° de cet article 4. <br/>
<br/>
              6. Dans ce cadre, en jugeant que les dispositions du 2° de l'article 4 précité ne s'appliquaient qu'aux ostéopathes exerçant cette profession à titre exclusif, et non aux professionnels de santé souhaitant en outre utiliser le titre d'ostéopathe, et que la situation de ces derniers était régie exclusivement par le 1° de cet article, la cour administrative d'appel de Douai a commis une erreur de droit.<br/>
<br/>
              7. Il résulte de ce qui précède que M. B...est fondé, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, à demander l'annulation de l'arrêt qu'il attaque. <br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              9. Il est constant, d'une part, que le diplôme universitaire d'ostéopathie délivré à M. B...par la faculté de médecine de l'université de Lille II n'a pas été reconnu par le Conseil national de l'ordre des médecins. Dans ces conditions, l'intéressé ne remplissait pas la condition posée au 1° de l'article 4 précité du décret du 25 mars 2007 relatif aux actes et aux conditions d'exercice de l'ostéopathie pour pouvoir faire un usage professionnel du titre d'ostéopathe.<br/>
<br/>
              10. D'autre part, si M. B...soutient qu'il devrait être regardé comme étant titulaire d'un diplôme délivré par un établissement agréé au sens du 2° de cet article 4, en application des dispositions précitées de l'article 9 du décret du 25 mars 2007 relatif à la formation des ostéopathes et à l'agrément des établissements de formation, ces dernières dispositions, ainsi qu'il a été dit au point 5, n'ont pu avoir pour objet ou pour effet de permettre que les professionnels de santé titulaires d'un diplôme universitaire ou interuniversitaire sanctionnant une formation suivie au sein d'une unité de formation et de recherche de médecine délivré par une université de médecine qui ne serait pas reconnu par le Conseil national de l'ordre des médecins soient regardés comme satisfaisant à la condition de détention d'un diplôme délivré par un établissement agréé. Par suite, le requérant n'est pas davantage fondé à soutenir qu'il remplissait la condition posée par les dispositions du 2° de l'article 4 précité du décret du 25 mars 2007 relatif aux actes et aux conditions d'exercice de l'ostéopathie.<br/>
<br/>
              11. Il suit de là que le diplôme de M. B...ne remplissait pas les conditions posées par l'article 4 du décret du 25 mars 2007 relatif aux actes et aux conditions d'exercice de l'ostéopathie et que le directeur général de l'agence régionale de santé Nord - Pas-de-Calais était tenu de refuser de procéder à son enregistrement. En conséquence, les moyens tirés de l'incompétence de l'auteur de la décision du 26 octobre 2012 et de l'insuffisance de sa motivation sont inopérants.<br/>
<br/>
              12. Il résulte de tout ce qui précède que M. B...n'est pas fondé à se plaindre de ce que, par le jugement attaqué, le tribunal administratif de Lille a rejeté sa demande.<br/>
<br/>
              13. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'agence régionale de santé des Hauts-de-France, venant aux droits de l'agence régionale de santé de Nord - Pas-de-Calais, qui, en tout état de cause, n'est pas partie à la présente instance.<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : L'arrêt du 26 janvier 2016 de la cour administrative d'appel de Douai est annulé.<br/>
Article 2 : La requête présentée par M. B...devant la cour administrative d'appel de Douai est rejetée.<br/>
Article 3 : Le surplus des conclusions du pourvoi de M. B...est rejeté.<br/>
Article 4 : La présente décision sera notifiée à M. A...B...et à la ministre des solidarités et de la santé.<br/>
Copie en sera adressée à l'agence régionale de santé des Hauts-de-France.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
