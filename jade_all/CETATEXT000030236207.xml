<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030236207</ID>
<ANCIEN_ID>JG_L_2015_02_000000385750</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/23/62/CETATEXT000030236207.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  4ème sous-section jugeant seule, 13/02/2015, 385750, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-02-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>385750</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 4ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. David Moreau</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:385750.20150213</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              Par décision du 2 juin 2009 le ministre de la défense a rejeté la demande de Mme A...B..., veuveC..., tendant à l'attribution du droit de pension de réversion de son défunt mari. Celle-ci a contesté cette décision devant le tribunal des pensions de Bordeaux. <br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un jugement n° 14/00019 du 7 novembre 2014, enregistré le 17 novembre 2014 au secrétariat du contentieux du Conseil d'Etat, le tribunal des pensions de Bordeaux, avant de statuer sur la demande de MmeC..., a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution de l'article L. 43, 3° et de L. 45 du code des pensions militaires d'invalidité et des victimes de la guerre.<br/>
<br/>
<br/>
              Vu  les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son article 61-1 ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code des pensions d'invalidité et des victimes de la guerre, notamment ses articles L. 43 et L. 45 ;<br/>
              - la décision n° 2013-324 QPC du 21 juin 2013 ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. David Moreau, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et qu'elle soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 43 du code des pensions militaires d'invalidité et des victimes de la guerre : " Ont droit à pension : (...) 3° Les conjoints survivants des militaires et marins morts en jouissance d'une pension définitive ou temporaire correspondant à une invalidité égale ou supérieure à 60 % ou en possession de droits à cette pension (...) " ; qu'aux termes de l'article L. 45 du même code : " Les demandes de pension autres que les pensions de réversion, formulées par les conjoints survivants ou orphelins de militaires décédés dans leur foyer, doivent être accompagnées d'un rapport médico-légal, établi par le médecin qui a soigné l'ancien militaire ou marin pendant la dernière maladie ou, à défaut de soins donnés pendant la dernière maladie, par le médecin qui a constaté le décès (...) " ; <br/>
<br/>
              3. Considérant, en premier lieu, que dans les motifs et le dispositif de sa décision n° 2013-324 QPC du 21 juin 2013, le Conseil constitutionnel a déclaré les dispositions citées ci-dessus de l'article L. 43 du code des pensions militaires d'invalidité et des victimes de la guerre conformes à la Constitution ; que si Mme C...soutient qu'un changement des circonstances est intervenu depuis cette décision, elle se borne à invoquer les faits particuliers de l'espèce et une décision de la Cour européenne des droits de l'homme en tout état de cause antérieure à la décision du Conseil constitutionnel mentionnée ci-dessus ; <br/>
<br/>
              4. Considérant, en second lieu, que Mme C...ne peut utilement soutenir, pour contester la constitutionnalité de l'article L. 45 du code des pensions militaires d'invalidité et des victimes de la guerre, que cet article serait contraire à l'article 14 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ou à l'article 1er du protocole additionnel n° 1 à cette convention ; que si elle soutient que ces dispositions méconnaissent les articles 6 et 17 de la Déclaration des droits de l'homme et du citoyen, elle n'assortit pas ce grief des précisions permettant d'apprécier s'il y lieu de renvoyer au Conseil constitutionnel une question prioritaire de constitutionnalité ; <br/>
<br/>
              5. Considérant qu'il résulte de tout ce qui précède qu'il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par Mme C...devant le tribunal des pensions ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité transmise par le tribunal des pensions de Bordeaux.<br/>
Article 2 : La présente décision sera notifiée à Mme A...B..., veuveC..., et au ministre de la défense.<br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre et au tribunal des pensions de Bordeaux.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
