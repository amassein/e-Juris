<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042078279</ID>
<ANCIEN_ID>JG_L_2020_07_000000430131</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/07/82/CETATEXT000042078279.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 02/07/2020, 430131, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-07-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>430131</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BAUER-VIOLAS, FESCHOTTE-DESBOIS, SEBAGH</AVOCATS>
<RAPPORTEUR>M. Alain Seban</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:430131.20200702</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. et Mme A... B... et la SCI " Le Merlanson " ont demandé au tribunal administratif de Nice d'annuler pour excès de pouvoir l'arrêté du 28 septembre 2016 par lequel le préfet des Alpes-Maritimes a ordonné la fermeture définitive du centre équestre " Le Merlanson " sur le territoire de la commune de Sospel en ce qui concerne les surfaces incluses dans la zone rouge du plan de prévention des risques de mouvements de terrain. Par un jugement n° 1604156 du 23 mai 2018, le tribunal administratif a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 18MA03529 et 18MA03565 du 25 février 2019, la cour administrative d'appel de Marseille a rejeté l'appel formé par M. et Mme B... et la SCI " Le Merlanson " contre ce jugement.<br/>
<br/>
              Par un pourvoi, enregistré au secrétariat du contentieux du Conseil d'Etat le 25 avril 2019, M. et Mme B... et la SCI " Le Merlanson " demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code rural et de la pêche maritime ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alain Seban, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Bauer-Violas, Feschotte-Desbois, Sebagh, avocat de M. et Mme B... et de la société Le Merlanson ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. et Mme B... ont créé en 2013 un centre équestre sur un ensemble de parcelles situées sur le territoire de la commune de Sospel par la société civile immobilière (SCI) " Le Merlanson ". Les époux B... et la SCI " Le Merlanson " se pourvoient en cassation contre l'arrêt de la cour administrative d'appel de Marseille du 25 février 2019 rejetant l'appel dirigé contre le jugement du 23 mai 2018 par lequel le tribunal administratif de Nice a rejeté leur demande tendant à l'annulation pour excès de pouvoir de l'arrêté du 28 septembre 2016 du préfet des Alpes-Maritimes ordonnant la fermeture des surfaces situées en zone rouge du plan de prévention des risques (PPR) de mouvements naturels de terrain du 7 août 2012. <br/>
<br/>
              Sur la régularité de l'arrêt attaqué :<br/>
<br/>
              2. La cour administrative d'appel, par des énonciations non contestées de son arrêt, après avoir annulé le jugement de première instance pour une irrégularité tenant à la méconnaissance du principe du caractère contradictoire de la procédure, a évoqué l'affaire puis rejeté la demande de M. et Mme B... et de la SCI " Le Merlanson ". Toutefois, l'article 2 de l'arrêt se borne, par une erreur matérielle qu'il y a lieu de rectifier, à rejeter la requête d'appel de ces derniers.<br/>
<br/>
              Sur les autres moyens de cassation :<br/>
<br/>
              3. En premier lieu, la cour administrative d'appel qui n'avait pas à répondre à tous les arguments des parties, et n'était pas tenue d'indiquer les raisons pour lesquelles elle ne reprenait pas les conclusions des rapports géologiques produits par les requérants, a suffisamment motivé sa décision.<br/>
<br/>
              4. En deuxième lieu, si les dispositions du 5° de l'article L. 2212-2 du code général des collectivités territoriales, confient à la police municipale en particulier le soin de prévenir, par des précautions convenables, et de faire cesser, par la distribution des secours nécessaires, notamment les éboulements de terre ou de rochers, ou autres accidents naturels et si l'article L. 2212-4 du même code confie au maire, en cas de danger grave ou imminent, tel que les accidents naturels prévus au 5° de l'article L. 2212-2, le soin de prescrire l'exécution des mesures de sûreté exigées par les circonstances, l'article L. 2215-1 du même code prévoit que le représentant de l'Etat dans le département se substitue au maire, autorité de police municipale, en cas de défaillance de ce dernier dans les conditions prévues par ces dispositions, notamment après mise en demeure restée sans résultat.<br/>
<br/>
              5. Il ressort des pièces du dossier soumis aux juge du fond que l'arrêté du 2 septembre 2013 par lequel le maire de Sospel avait interdit l'habitation et la fréquentation de l'unité foncière appartenant aux époux B..., par ailleurs ultérieurement annulé par un jugement du 28 octobre 2014 du tribunal administratif de Nice, n'avait pas la même portée que l'arrêté préfectoral en litige. Par suite, c'est sans erreur de droit que la cour administrative d'appel a retenu que les conditions de la substitution du maire par le préfet étaient réunies après avoir estimé, sur la base de faits non contestés, qu'à la date de l'arrêté préfectoral, soit le 24 septembre 2016, la mise en demeure adressée par le préfet au maire le 6 septembre 2016, était restée sans résultat.<br/>
<br/>
              6. En troisième lieu, l'administration peut, en première instance comme en appel, faire valoir devant le juge de l'excès de pouvoir que la décision dont l'annulation est demandée est légalement justifiée par un motif, de droit ou de fait, autre que celui initialement indiqué, mais également fondé sur la situation existant à la date de cette décision. Il appartient alors au juge, après avoir mis à même l'auteur du recours de présenter ses observations sur la substitution ainsi sollicitée, de rechercher si un tel motif est de nature à fonder légalement la décision, puis d'apprécier s'il résulte de l'instruction que l'administration aurait pris la même décision si elle s'était fondée initialement sur ce motif. Dans l'affirmative, sous réserve toutefois qu'elle ne prive pas le requérant d'une garantie procédurale liée au motif substitué, le juge peut procéder à la substitution demandée.<br/>
<br/>
              7. Il ressort des énonciations de l'arrêt attaqué que la cour administrative d'appel a substitué, à la demande du ministre de l'intérieur, au motif retenu par l'autorité préfectorale tiré de ce que le plan de prévention des risques (PPR) interdisait l'accueil du public sur la partie de la propriété située en zone rouge, celui tiré de ce que l'accueil du public méconnaissait les dispositions du règlement du PPR dont l'article II. 4 a) prévoit que ne peuvent être autorisés que les ouvrages ou constructions qui n'ont pas pour effet d'aggraver les risques ou leurs effets, et notamment d'augmenter significativement le nombre de personnes exposées.<br/>
<br/>
              8. Il ressort des termes mêmes de l'arrêt, contrairement à ce qui est soutenu, que les juges du fond ont, d'une part, recherché si l'administration aurait pris la même décision en se fondant sur le motif présenté en défense par l'administration et se sont, d'autre part, fondés non sur des éboulements survenus postérieurement à l'arrêté attaqué, soit ceux du 15 avril 2018 survenus dans les quartiers de " Saint Sabine " et de " Béroulf ", mais uniquement sur des éboulements de terrain, antérieurs à celui-ci, et particulièrement celui du quartier " Barbon " de janvier 2014. C'est également sans dénaturer les pièces du dossier qui leur était soumis qu'ils ont estimé que ce quartier, quoique distant d'environ un kilomètre, se situait à proximité du centre équestre.<br/>
<br/>
              9. En quatrième lieu, il ressort des termes de l'arrêt attaqué que les juges du fond ont recherché, ainsi qu'il leur revenait de le faire, si les mesures édictées par l'arrêté attaqué étaient nécessaires, adapatées et proportionnées aux faits qui les justifiaient. En estimant que tel était le cas, compte tenu, d'une part, de l'interprétation qu'ils ont faite de l'arrêté, dont ils ont jugé qu'il ne visait qu'à interdire l'accès du public, et non l'accès des requérants et de leurs employés, sur les parcelles situées en zone rouge du PPR, et, compte tenu, d'autre part, des risques de mouvement de terrain susceptibles de se produire, alors même que le caractère immédiat de ces risques ne pouvait être tenu pour avéré, la cour administrative d'appel n'a pas donné aux faits de l'espèce une qualification juridique erronée.<br/>
<br/>
              10. Il résulte de tout ce qui précède que les requérants ne sont pas fondés à demander l'annulation de l'arrêt qu'ils attaquent mais uniquement sa rectification pour erreur matérielle ainsi qu'il a été dit au point 3.<br/>
<br/>
              Sur les frais d'instance :<br/>
<br/>
              11. Les dispositions de l'article L. 761-1 du code de justice font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 25 février 2019 est rectifié ainsi qu'il suit :<br/>
- l'article 2 est remplacé par les dispositions suivantes : " Le jugement du tribunal administratif de Nice du 23 mai 2018 est annulé " ;<br/>
- il est ajouté après l'article 2 un nouvel article 3 ainsi rédigé : " La demande présentée par M. B... et autres devant le tribunal administratif de Nice est rejetée. " ;<br/>
		- l'article 3 devient l'article 4.<br/>
<br/>
		Article 2 : Le surplus des conclusions de la requête est rejeté. <br/>
Article 3 : La présente décision sera notifiée à M. A... B..., premier requérant dénommé, à la commune de Sospel et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
