<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035701499</ID>
<ANCIEN_ID>JG_L_2017_09_000000404921</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/70/14/CETATEXT000035701499.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 22/09/2017, 404921</TITRE>
<DATE_DEC>2017-09-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>404921</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLEZ</AVOCATS>
<RAPPORTEUR>Mme Marie-Anne Lévêque</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:404921.20170922</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire, un mémoire en réplique et un nouveau mémoire, enregistrés le 7 novembre 2016 et les 7 février, 22 juin et 1er septembre 2017 au secrétariat du contentieux du Conseil d'Etat, M. B...A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du 23 août 2016 par lequel le Président de la République l'a radié des cadres par mesure disciplinaire ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - le code de la défense ; <br/>
              - le code des relations entre le public et l'administration ; <br/>
              - le code de justice administrative ;		<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Anne Lévêque, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boullez, avocat de M. A...;<br/>
<br/>
<br/>1. Considérant que, par un décret du Président de la République du 23 août 2016, M.A..., général de corps d'armée en deuxième section, a été radié des cadres par mesure disciplinaire pour manquement aux obligations de réserve et de loyauté auxquelles il était astreint à l'occasion d'une manifestation qui a eu lieu le 6 février 2016 à Calais ; que M. A... demande au Conseil d'Etat d'annuler ce décret pour excès de pouvoir ;  <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 4121-1 du code de la défense : " Les militaires jouissent de tous les droits et libertés reconnus aux citoyens. Toutefois, l'exercice de certains d'entre eux est soit interdit, soit restreint dans les conditions fixées au présent livre " ; qu'aux termes de l'article L. 4121-2 du même code : " Les opinions ou croyances, notamment philosophiques, religieuses ou politiques, sont libres/ Elles ne peuvent cependant être exprimées qu'en dehors du service et avec la réserve exigée par l'état militaire. Cette règle s'applique à tous les moyens d'expression (...) " ; qu'aux termes de l'article L. 4141-1 du même code : " Les officiers généraux sont répartis en deux sections:/(...)°2o La deuxième section comprend les officiers généraux qui, n'appartenant pas à la première section, sont maintenus à la disposition du ministre de la défense / (...) Les officiers généraux peuvent être radiés des cadres. " ;  qu'aux termes de l'article L. 4141-4 du même code : " Les dispositions de l'article L. 4121-2,(...) et du b du 3° de l'article L. 4137-2 sont applicables à l'officier général de la deuxième section lorsqu'il n'est pas replacé en première section par le ministre de la défense " ; qu'aux termes du 3° de l'article L. 4137-2 du même code :  " 3° Les sanctions du troisième groupe sont : (...) /b) La radiation des cadres ou la résiliation du contrat. " ;  qu'il résulte de ces dispositions, d'une part, que les officiers généraux placés dans la deuxième section sont soumis à l'obligation de réserve exigée par l'état militaire et, d'autre part, que seule la sanction disciplinaire de radiation des cadres peut leur être appliquée ; <br/>
<br/>
              Sur la légalité externe de la décision attaquée : <br/>
<br/>
              3. Considérant, en premier lieu, qu'aux termes de l'article L. 4141-7 du code de la défense, dans sa rédaction alors en vigueur : " Pour l'application à un officier général des sanctions prévues au 3o de l'article L. 4137-2, l'avis du conseil d'enquête mentionné à l'article L. 4137-3 est remplacé par celui du conseil supérieur de l'armée ou de la formation rattachée à laquelle il appartient. La décision entraîne, en cas de radiation des cadres, la radiation de la première ou de la deuxième section des officiers généraux. " ; qu'il résulte de ces dispositions que M. A...ne peut utilement se prévaloir des dispositions de l'article R. 4137-72 du code de la défense, qui prévoient que le rapporteur devant le conseil d'enquête mentionné à l'article L. 4137-3 doit détenir un grade supérieur à celui du militaire déféré devant ce conseil, qui ne sont pas applicables à la procédure disciplinaire devant le conseil supérieur de l'armée ou de la formation rattachée siégeant disciplinairement, qui est régie par les articles R. 4137-93 et suivants du code de la défense ; <br/>
<br/>
              4. Considérant, en deuxième lieu, que la circonstance que l'avis du conseil supérieur de l'armée de terre n'aurait pas précisé les motifs pour lesquels M. A...était susceptible d'être rappelé en première section est, en tout état de cause, dépourvue d'incidence sur la légalité de la décision attaquée ;<br/>
<br/>
              5. Considérant, en dernier lieu, qu'il ressort des pièces du dossier que la décision de sanction dont a fait l'objet M. A...comporte l'énoncé des motifs de droit et de fait qui en constituent le fondement ; qu'ainsi, et alors même qu'elle ne retranscrit pas le détail des propos reprochés au requérant, elle est, contrairement à ce qui est soutenu, suffisamment motivée au regard des exigences de l'article L. 211-2 du code des relations entre le public et l'administration ; <br/>
<br/>
              Sur la légalité interne de la décision attaquée : <br/>
<br/>
              6. Considérant, en premier lieu, que la conformité à la Constitution des dispositions de l'article L. 4141-4 du code de la défense, citées au point 2, ne peut être utilement contestée devant le Conseil d'Etat, statuant au contentieux en dehors de la procédure prévue à l'article 61-1 de la Constitution ; <br/>
<br/>
              7. Considérant, en deuxième lieu, qu'il ressort des pièces du dossier que l'autorité investie du pouvoir disciplinaire ne s'est pas fondée sur la circonstance que M. A... aurait eu la qualité d'organisateur ou d'instigateur de la manifestation du 6 février 2016 mais sur le fait qu'il a participé à cette manifestation alors même qu'elle avait été interdite par arrêté préfectoral et que, n'ignorant pas cette interdiction, il a appelé à maintenir la participation à cette manifestation ; que, par suite, le moyen tiré de ce que la décision attaquée méconnaîtrait l'autorité de la chose jugée attachée au jugement du 26 mai 2016 par lequel le tribunal correctionnel de Boulogne-sur-Mer a relaxé M. A...du chef de poursuite de délit d'organisation d'une manifestation sur la voie publique ayant été interdite dans les conditions fixées par la loi ne peut qu'être écarté ; <br/>
<br/>
              8. Considérant, en troisième lieu, que les officiers généraux placés dans la deuxième section sont, ainsi qu'il a été dit, soumis à l'obligation de réserve qui s'impose à tout militaire ; qu'il ressort des pièces du dossier que M. A...a participé à une manifestation interdite par l'autorité préfectorale et a appelé au maintien de la participation à cette dernière alors qu'il n'ignorait pas cette interdiction, ainsi qu'il l'a reconnu dans le cadre de l'enquête disciplinaire ; qu'il a pris publiquement la parole, devant la presse, au cours de cette manifestation pour critiquer de manière virulente l'action des pouvoirs publics, notamment la décision d'interdire la manifestation, et l'action des forces de l'ordre, en se prévalant de sa qualité d'officier général et des responsabilités qu'il a exercées dans l'armée, alors même qu'il ne pouvait ignorer, contrairement à ce qu'il soutient, le fort retentissement médiatique de ses propos ; que s'il soutient qu'il n'était pas en service et qu'il portait une tenue civile, que la manifestation a été brève et qu'il a déféré à la sommation de dispersion des forces de l'ordre, qu'il n'a tenu que des propos oraux, qui ne présentaient aucun caractère injurieux, irrespectueux ou violent à l'égard des institutions, enfin qu'il n'était animé d'aucune volonté de déloyauté à l'égard de sa hiérarchie, les faits rappelés ci-dessus caractérisent des manquements de M. A... à ses obligations, à l'occasion de la manifestation du 6 février,  de nature à justifier une sanction disciplinaire ; <br/>
<br/>
              9. Considérant, en quatrième lieu,  que, eu égard à la gravité de ces manquements, et en dépit des états de service de M. A...et du fait qu'il n'a jamais fait l'objet d'une sanction disciplinaire, l'autorité disciplinaire n'a pas pris une sanction disproportionnée en prononçant la  radiation des cadres prévue par les dispositions précitées  du 3° de l'article L. 4137-2 du code de la défense ; <br/>
<br/>
              10. Considérant, en cinquième lieu, que M. A...n'est pas fondé à soutenir qu'en sanctionnant les manquements mentionnés au point 8 en application de ces dispositions, l'autorité disciplinaire aurait méconnu les stipulations de l'article 10 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, qui garantissent à toute personne le droit à la liberté d'expression et celui de recevoir ou de communiquer des informations ou des idées, dès lors que la restriction apportée à sa liberté d'expression par l'obligation de réserve qui s'imposait à lui poursuit un but légitime au sens de ces stipulations ;  que, eu égard à la situation particulière des officiers généraux placés dans la deuxième section, qui n'exercent plus d'activité militaire, M. A...n'est pas non plus fondé à soutenir que les dispositions de l'article L. 4141-4 du code de la défense, citées au point 2 ci-dessus, méconnaîtraient les stipulations des articles 8 à 11 et 14 de la même convention en tant qu'elles prévoient comme seule sanction disciplinaire pour ces officiers généraux la radiation des cadres ;<br/>
<br/>
              11. Considérant qu'il résulte de tout ce qui précède que M. A...n'est pas fondé à demander l'annulation pour excès de pouvoir du décret qu'il attaque ; que, par suite, ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent qu'être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. A...est rejetée.  <br/>
Article 2 : La présente décision sera notifiée à M. B...A...et à la ministre des armées. <br/>
Copie en sera adressée au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">08-01-01-05 ARMÉES ET DÉFENSE. PERSONNELS MILITAIRES ET CIVILS DE LA DÉFENSE. QUESTIONS COMMUNES À L'ENSEMBLE DES PERSONNELS MILITAIRES. DISCIPLINE. - CONTRÔLE DE LA PROPORTIONNALITÉ DE LA SANCTION DISCIPLINAIRE À LA GRAVITÉ DES FAITS [RJ1] - CAS D'UN OFFICIER GÉNÉRAL PLACÉ DANS LA DEUXIÈME SECTION À QUI NE PEUT ÊTRE APPLIQUÉE QU'UNE SANCTION DE RADIATION DES CADRES (3° DE L'ART. L. 4127-2 DU CODE DE LA DÉFENSE) - SANCTION PROPORTIONNÉE EN L'ESPÈCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-02-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR. APPRÉCIATIONS SOUMISES À UN CONTRÔLE NORMAL. - CONTRÔLE DE LA PROPORTIONNALITÉ DE LA SANCTION DISCIPLINAIRE À LA GRAVITÉ DES FAITS [RJ1] - CAS D'UN OFFICIER GÉNÉRAL PLACÉ DANS LA DEUXIÈME SECTION À QUI NE PEUT ÊTRE APPLIQUÉE QU'UNE SANCTION DE RADIATION DES CADRES - SANCTION PROPORTIONNÉE EN L'ESPÈCE.
</SCT>
<ANA ID="9A"> 08-01-01-05 Sanction de radiation des cadres de l'armée d'un officier général placé dans la deuxième section pour des manquements au devoir de réserve et à la loyauté lors d'une manifestation interdite par l'autorité préfectorale à qui ne peut être appliquée qu'une sanction de radiation des cadres. Eu égard à la gravité de ces manquements et en dépit des états de service de l'intéressé et du fait qu'il n'a jamais fait l'objet d'une sanction disciplinaire, l'autorité disciplinaire n'a pas pris une sanction disproportionnée en prononçant la radiation des cadres prévue par le 3° de l'article L. 4127-2 du code de la défense.</ANA>
<ANA ID="9B"> 54-07-02-03 Sanction de radiation des cadres de l'armée d'un officier général placé dans la deuxième section pour des manquements au devoir de réserve et à la loyauté lors d'une manifestation interdite par l'autorité préfectorale à qui ne peut être appliquée qu'une sanction de radiation des cadres. Eu égard à la gravité de ces manquements et en dépit des états de service de l'intéressé et du fait qu'il n'a jamais fait l'objet d'une sanction disciplinaire, l'autorité disciplinaire n'a pas pris une sanction disproportionnée en prononçant la radiation des cadres prévues par le 3° de l'article L. 4127-2 du code de la défense.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 13 novembre 2013, M.,, n° 347704, p. 279 ; CE, 25 janvier 2016, M.,, n° 391178, T. pp. 643-904 ; CE, 14 mars 2016, M.,, n° 389361, T. pp. 643-904.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
