<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043524697</ID>
<ANCIEN_ID>JG_L_2021_05_000000383070</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/52/46/CETATEXT000043524697.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 21/05/2021, 383070, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-05-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>383070</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Arnaud Skzryerbak</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie Sirinelli</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:383070.20210521</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une décision du 24 février 2016, le Conseil d'Etat, statuant au contentieux, après avoir annulé la décision implicite par laquelle le Premier ministre avait refusé de prendre le décret d'application prévu au deuxième alinéa de l'article L. 146-5 du code de l'action sociale et des familles et enjoint au Premier ministre de prendre ce décret, a prononcé une astreinte de cent euros par jour à l'encontre de l'Etat s'il ne justifiait pas, dans les neuf mois suivant sa notification, avoir exécuté cette décision.<br/>
<br/>
              Par deux décisions des 31 mars 2017 et 24 octobre 2018, le Conseil d'Etat, statuant au contentieux, a procédé à la liquidation provisoire de l'astreinte au titre respectivement de la période comprise entre le 2 décembre 2016 et le 24 mars 2017, puis de la période comprise entre le 25 mars 2017 et le 15 octobre 2018. <br/>
<br/>
              La section du rapport et des études du Conseil d'Etat a exécuté les diligences qui lui incombent en vertu du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 2020-220 du 6 mars 2020 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arnaud Skzryerbak, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Marie Sirinelli, rapporteure publique ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Par une décision du 24 février 2016, sur la demande de M. A... et de l'Association nationale pour l'intégration des personnes handicapées moteurs, le Conseil d'Etat statuant au contentieux a annulé la décision implicite par laquelle le Premier ministre a refusé de prendre le décret d'application prévu au deuxième alinéa de l'article L. 146-5 du code de l'action sociale et des familles, enjoint au Premier ministre de prendre ce décret dans le délai de neuf mois à compter de la notification de cette décision et prononcé une astreinte de cent euros par jour à l'encontre de l'Etat s'il ne justifiait pas, dans ce délai, l'avoir exécutée. <br/>
<br/>
              2. Aux termes du premier alinéa de l'article L. 911-7 du code de justice administrative : " En cas d'inexécution totale ou partielle ou d'exécution tardive, la juridiction procède à la liquidation de l'astreinte qu'elle avait prononcée ". En vertu du premier alinéa de l'article L. 911-8 du code de justice administrative, la juridiction a la faculté de décider, afin d'éviter un enrichissement indu, qu'une fraction de l'astreinte liquidée ne sera pas versée au requérant, le second alinéa prévoyant que cette fraction est alors affectée au budget de l'Etat. Toutefois, l'astreinte ayant pour finalité de contraindre la personne morale de droit public ou l'organisme de droit privé chargé de la gestion d'un service public à exécuter les obligations qui lui ont été assignées par une décision de justice, ces dispositions ne trouvent pas à s'appliquer lorsque l'Etat est débiteur de l'astreinte en cause. Dans ce dernier cas, lorsque cela apparaît nécessaire à l'exécution effective de la décision juridictionnelle, la juridiction peut, même d'office, après avoir recueilli sur ce point les observations des parties ainsi que de la ou des personnes morales concernées, décider d'affecter cette fraction à une personne morale de droit public disposant d'une autonomie suffisante à l'égard de l'Etat et dont les missions sont en rapport avec l'objet du litige ou à une personne morale de droit privé, à but non lucratif, menant, conformément à ses statuts, des actions d'intérêt général également en lien avec cet objet.<br/>
<br/>
              3. La décision du Conseil d'Etat du 24 février 2016 a été notifiée au Premier ministre le 2 mars 2016. Par deux décisions du 31 mars 2017 et du 24 octobre 2018, le Conseil d'Etat a procédé à la liquidation provisoire de l'astreinte prononcée par cette décision au titre des périodes comprises entre le 2 décembre 2016 et le 24 mars 2017 puis entre le 25 mars 2017 et le 15 octobre 2018. A la date du 6 mai 2021, le Premier ministre n'avait pas communiqué à la section du rapport et des études du Conseil d'Etat copie des actes justifiant des mesures prises pour exécuter la décision du 24 février 2016. Le Premier ministre doit être, par suite, regardé comme n'ayant pas, à cette date, exécuté cette décision. Il y a lieu, dès lors, en dépit de la modification entrée en vigueur le 8 mars 2020 du deuxième alinéa de l'article L. 146-5 du code de l'action sociale et des familles par le V de l'article 2 de la loi du 6 mars 2020 visant à améliorer l'accès à la prestation de compensation du handicap, de procéder au bénéfice de M. A..., de l'Association nationale pour l'intégration des personnes handicapées moteurs et de la fondation MMA Solidarité, abritée par la Fondation de France, à la liquidation de l'astreinte pour la période du 16 octobre 2018 au 6 mai 2021, au taux de cent euros par jour, soit 93 400 euros, dont 3 000 euros pour M. A..., 22 600 euros pour l'Association nationale pour l'intégration des personnes handicapées moteurs et 67 800 euros à la fondation MMA Solidarité, abritée par la Fondation de France, pour le financement d'équipements destinés à faciliter les loisirs et la pratique du sport par les personnes handicapées.<br/>
<br/>
              4. Il y a lieu de majorer le taux de l'astreinte fixé par la décision du 24 février 2016 à 250 euros par jour de retard à compter de la présente décision.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'Etat est condamné à verser la somme de 3 000 euros à M. A..., 22 600 euros à l'Association nationale pour l'intégration des personnes handicapées moteurs et 67 800 euros à la fondation MMA Solidarité, abritée par la Fondation de France, pour le financement d'équipements destinés à faciliter les loisirs et la pratique du sport par les personnes handicapées.  <br/>
<br/>
Article 2 : L'astreinte décidée à l'encontre de l'Etat par la décision du 24 février 2016 est portée à 250 euros par jour de retard.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. B... A..., à l'Association nationale pour l'intégration des personnes handicapées moteurs, au Premier ministre et au ministre des solidarités et de la santé.<br/>
Copie en sera adressée au ministère public près la Cour de discipline budgétaire et financière, à la section du rapport et des études du Conseil d'Etat, à la Fondation de France et à la fondation MMA Solidarité.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
