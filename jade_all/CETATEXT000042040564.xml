<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042040564</ID>
<ANCIEN_ID>JG_L_2020_06_000000437590</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/04/05/CETATEXT000042040564.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 17/06/2020, 437590</TITRE>
<DATE_DEC>2020-06-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>437590</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GADIOU, CHEVALLIER</AVOCATS>
<RAPPORTEUR>Mme Coralie Albumazard</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHR:2020:437590.20200617</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un jugement n° 1705661 du 30 décembre 2019, enregistré le 13 janvier 2020 au secrétariat du contentieux du Conseil d'Etat, le tribunal administratif de Grenoble, avant de statuer sur la demande de M. A... tendant à l'annulation pour excès de pouvoir de l'arrêté du 17 mai 2017 par lequel le maire d'Huez-en-Oisans (Isère) a délivré à Mme D... un permis de construire portant sur l'extension et la surélévation d'un bâtiment existant situé avenue de l'Étendard, a décidé, par application des dispositions de l'article L. 113-1 du code de justice administrative, de transmettre le dossier de cette demande au Conseil d'Etat en soumettant à son examen la question suivante : <br/>
<br/>
              L'inopérance du moyen tiré de l'exception d'illégalité du plan local d'urbanisme prévue par l'article L. 600-12-1 du code de l'urbanisme, entré en vigueur le 1er janvier 2019, s'applique-t-elle immédiatement dans les instances en cours ou faut-il prendre en compte la date de délivrance du permis de construire, la date d'introduction de la requête ou la date à laquelle le moyen a été soulevé ' <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ; <br/>
              - la loi n° 2018-1021 du 23 novembre 2018 ;<br/>
              - le code de justice administrative, notamment son article L. 113-1 et l'ordonnance n° 2020-305 du 25 mars 2020 modifiée ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
- le rapport de Mme Coralie Albumazard, maître des requêtes en service extraordinaire, <br/>
- les conclusions de M. Stéphane Hoynck, rapporteur public ;<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gadiou, Chevalier, avocat de Mme D... ;<br/>
<br/>
REND L'AVIS SUIVANT<br/>
<br/>
<br/>
<br/>
              1. L'article 80 de la loi du 23 novembre 2018 portant évolution du logement, de l'aménagement et du numérique a créé l'article L. 600-12-1 du code de l'urbanisme, aux termes duquel : " L'annulation ou la déclaration d'illégalité d'un schéma de cohérence territoriale, d'un plan local d'urbanisme, d'un document d'urbanisme en tenant lieu ou d'une carte communale sont par elles-mêmes sans incidence sur les décisions relatives à l'utilisation du sol ou à l'occupation des sols régies par le présent code délivrées antérieurement à leur prononcé dès lors que ces annulations ou déclarations d'illégalité reposent sur un motif étranger aux règles d'urbanisme applicables au projet. / Le présent article n'est pas applicable aux décisions de refus de permis de construire ou d'opposition à déclaration préalable. Pour ces décisions, l'annulation ou l'illégalité du document d'urbanisme leur ayant servi de fondement entraîne l'annulation de ladite décision. ". En vertu des dispositions du V de l'article 80 de la même loi, les dispositions de l'article L. 600-12-1 sont entrées en vigueur le 1er janvier 2019.<br/>
<br/>
              2. Les dispositions de l'article L. 600-12-1 du code de l'urbanisme précédemment citées contribuent à la définition des conditions dans lesquelles le juge apprécie, à l'occasion du recours pour excès de pouvoir contre une autorisation d'urbanisme, l'opérance des moyens dirigés, par la voie de l'exception d'illégalité, contre un document d'urbanisme existant ou tirés de ce que l'annulation d'un tel document, sur le fondement duquel l'autorisation a été délivrée, entraîne par voie de conséquence l'annulation de cette dernière. Ces dispositions, qui n'affectent pas la substance du droit de former un recours pour excès de pouvoir contre une décision administrative, sont, en l'absence de dispositions contraires expresses, immédiatement applicables aux instances en cours.<br/>
<br/>
              3. Le présent avis sera notifié au tribunal administratif de Grenoble, à M. B... A..., à la commune d'Huez-en-Oisans, à Mme C... D... et à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales. <br/>
<br/>
              Il sera publié au Journal officiel de la République française. <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-08-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. APPLICATION DANS LE TEMPS. ENTRÉE EN VIGUEUR. - ARTICLE L. 600-12-1 DU CODE DE L'URBANISME RELATIF À L'INCIDENCE SUR UNE AUTORISATION D'URBANISME DE L'ILLÉGATÉ D'UN DOCUMENT D'URBANISME - APPLICATION IMMÉDIATE AUX INSTANCES EN COURS [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-06-04 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. POUVOIRS DU JUGE. - RECOURS CONTRE UNE AUTORISATION D'URBANISME - CONDITIONS D'OPÉRANCE DES MOYENS DIRIGÉS CONTRE UN DOCUMENT D'URBANISME (ART. L. 600-12-1 DU CODE DE L'URBANISME, ISSU DE L'ARTICLE 80 DE LA LOI N° 2018-1021 DU 23 NOVEMBRE 2018) - APPLICATION IMMÉDIATE AUX INSTANCES EN COURS [RJ1].
</SCT>
<ANA ID="9A"> 01-08-01 L'article L. 600-12-1 du code de l'urbanisme, issu de l'article 80 de la loi n° 2018-1021 du 23 novembre 2018, contribue à la définition des conditions dans lesquelles le juge apprécie, à l'occasion du recours pour excès de pouvoir contre une autorisation d'urbanisme, l'opérance des moyens dirigés, par la voie de l'exception d'illégalité, contre un document d'urbanisme existant ou tirés de ce que l'annulation d'un tel document, sur le fondement duquel l'autorisation a été délivrée, entraîne par voie de conséquence l'annulation de cette dernière.... ,,Ces dispositions, qui n'affectent pas la substance du droit de former un recours pour excès de pouvoir contre une décision administrative, sont, en l'absence de dispositions contraires expresses, immédiatement applicables aux instances en cours.</ANA>
<ANA ID="9B"> 68-06-04 L'article L. 600-12-1 du code de l'urbanisme, issu de l'article 80 de la loi n° 2018-1021 du 23 novembre 2018, contribue à la définition des conditions dans lesquelles le juge apprécie, à l'occasion du recours pour excès de pouvoir contre une autorisation d'urbanisme, l'opérance des moyens dirigés, par la voie de l'exception d'illégalité, contre un document d'urbanisme existant ou tirés de ce que l'annulation d'un tel document, sur le fondement duquel l'autorisation a été délivrée, entraîne par voie de conséquence l'annulation de cette dernière.... ,,Ces dispositions, qui n'affectent pas la substance du droit de former un recours pour excès de pouvoir contre une décision administrative, sont, en l'absence de dispositions contraires expresses, immédiatement applicables aux instances en cours.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant de l'article L. 600-9 du code de l'urbanisme, CE, Section, 22 décembre 2017, Commune de Sempy, n° 395963, p. 380 ; s'agissant de l'article L. 600-5-1 du même code, CE, 18 juin 2014, Société Batimalo et autre, n° 376760, p. 164 ; s'agissant des articles L. 600-5 et L. 600-7 du même code, CE, 18 juin 2014, SCI Mounou et autres, n° 376113, p. 163. Comp., s'agissant des dispositions relatives à l'intérêt pour agir des articles L. 600-1-2 et L. 600-1-3 du même code, même décision.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
