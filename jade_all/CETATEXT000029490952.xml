<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029490952</ID>
<ANCIEN_ID>JG_L_2014_09_000000364124</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/49/09/CETATEXT000029490952.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème / 1ère SSR, 22/09/2014, 364124</TITRE>
<DATE_DEC>2014-09-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>364124</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème / 1ère SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; BERTRAND</AVOCATS>
<RAPPORTEUR>M. Samuel Gillis</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:364124.20140922</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 27 novembre 2012 et 27 février 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B...A..., demeurant ... ; M. A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler la décision du 18 septembre 2012 par laquelle la chambre nationale de discipline auprès du conseil supérieur de l'ordre des experts-comptables, d'une part, a infirmé la décision du 25 novembre 2011 de la chambre régionale de discipline près le conseil régional de l'ordre des experts-comptables d'Aquitaine le relaxant des poursuites engagées contre lui à l'initiative de la société d'expertise comptable Robert Moréreau, d'autre part, a prononcé à son encontre, la sanction de blâme avec inscription au dossier ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel formé par la société Robert Moréreau devant la chambre nationale de discipline auprès du conseil supérieur de l'ordre des experts-comptables ;<br/>
<br/>
              3°) de mettre à la charge de la société Robert Moréreau la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative et de 35 euros au titre de l'article R. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu l'ordonnance n° 45-2138 du 19 septembre 1945 ;<br/>
<br/>
              Vu le décret n° 70-147 du 19 février 1970 ;<br/>
<br/>
              Vu le décret n° 2007-1387 du 27 septembre 2007 ;<br/>
<br/>
              Vu le décret n° 2012-432 du 30 mars 2012 ;<br/>
<br/>
              Vu le code des devoirs professionnels des experts-comptables ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Samuel Gillis, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de M.A..., à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat du conseil supérieur de l'ordre des experts-comptables, et à Me Bertrand, avocat de la société d'expertise comptable Robert Moréreau ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la chambre régionale de discipline près le conseil régional de l'ordre des experts-comptables de l'Aquitaine a, par une décision du 25 novembre 2011, relaxé M. A...des fins des poursuites disciplinaires engagées à son encontre à la suite de la plainte, fondée sur la méconnaissance des prescriptions du code des devoirs professionnels proscrivant toute démarche ou manoeuvre susceptible de nuire à la situation de leurs confrères, déposée par la société Robert Moréreau ; que, par une décision du 18 septembre 2012, contre laquelle M. A...se pourvoit en cassation, la chambre nationale de discipline, estimant que l'obligation précitée avait été méconnue, a infirmé la décision de la chambre régionale et infligé à l'intéressé la sanction du blâme avec inscription au dossier ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 184 du décret du 30 mars 2012 relatif à l'exercice de l'activité d'expertise comptable, applicable à la convocation devant la chambre régionale de discipline : " Trente jours au moins avant l'audience, le président convoque, par lettre recommandée avec avis de réception, l'intéressé et la personne qui a saisi l'instance disciplinaire. / La convocation comporte, à peine de nullité, l'indication des obligations législatives ou réglementaires auxquelles il est reproché à la personne poursuivie d'avoir contrevenu et des faits à l'origine des poursuites. (...) " ; qu'en vertu du dernier alinéa de l'article 192 du même décret, l'instruction des appels des décisions de la chambre régionale de discipline et leur jugement sont assurés dans les conditions prévues aux articles 182 à 185 ; qu'il en résulte que  la convocation à l'audience d'appel devant la chambre nationale de discipline doit, à peine de nullité, comporter l'indication des obligations législatives ou réglementaires auxquelles il est reproché à la personne poursuivie d'avoir contrevenu et des faits à l'origine des poursuites ; qu'il ne ressort pas des pièces de la procédure que la convocation adressée à M. A... comportait ces indications ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, M. A... est fondé, pour ce motif, à demander l'annulation de la décision du 18 septembre 2012 qu'il attaque ;<br/>
<br/>
              3. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Robert Moréreau la somme globale de 1 500 euros qui sera versée à M. A...au titre des dispositions des articles L. 761-1 et R. 761-1 du code de justice administrative ; que les dispositions de l'article L. 761-1 du code de justice administrative font, en revanche, obstacle à ce qu'une somme soit mise à la charge de M.A...,  qui n'est pas la partie perdante dans la présente instance, au titre des frais exposés par la société Robert Moréreau et non compris dans les dépens ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La décision du 18 septembre 2012 de la chambre nationale de discipline auprès du conseil supérieur de l'ordre des experts-comptables est annulée.<br/>
<br/>
Article 2 : L'affaire est renvoyée devant la chambre nationale de discipline auprès du conseil supérieur de l'ordre des experts-comptables.<br/>
<br/>
Article 3 : La société Robert Moréreau versera à M. A...une somme de 1 500 euros au titre des articles L. 761-1 et R. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions de la société Robert Moréreau tendant à l'application de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à M. B...A..., à la société Robert Moréreau et au conseil supérieur de l'ordre des experts-comptables.<br/>
Copie en sera adressée, pour information, au ministre de l'économie, de l'industrie et du numérique. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">55-04-01-02 PROFESSIONS, CHARGES ET OFFICES. DISCIPLINE PROFESSIONNELLE. PROCÉDURE DEVANT LES JURIDICTIONS ORDINALES. JUGEMENTS. - MENTIONS OBLIGATOIRES DE LA CONVOCATION À L'AUDIENCE D'APPEL - INCLUSION - INDICATION DES OBLIGATIONS AUXQUELLES IL EST REPROCHÉ À LA PERSONNE POURSUIVIE D'AVOIR CONTREVENU ET DES FAITS À L'ORIGINE DES POURSUITES.
</SCT>
<ANA ID="9A"> 55-04-01-02 Il résulte des dispositions des articles 184 et 192 du décret n° 2012-432 du 30 mars 2012 relatif à l'exercice de l'activité d'expertise comptable que la convocation à l'audience d'appel devant la chambre nationale de discipline doit à peine de nullité, comme la convocation à l'audience de première instance, comporter l'indication des obligations législatives ou réglementaires auxquelles il est reproché à la personne poursuivie d'avoir contrevenu et des faits à l'origine des poursuites.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
