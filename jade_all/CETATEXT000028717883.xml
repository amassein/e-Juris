<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028717883</ID>
<ANCIEN_ID>JG_L_2014_03_000000368285</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/71/78/CETATEXT000028717883.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 5ème sous-sections réunies, 12/03/2014, 368285, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-03-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>368285</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 5ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY ; SCP DE CHAISEMARTIN, COURJON</AVOCATS>
<RAPPORTEUR>M. Benjamin de Maillard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Keller</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:368285.20140312</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 6 mai et 5 août 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme A...B..., demeurant ... ; Mme B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 11PA03514 du 4 mars 2013 par lequel la cour administrative d'appel de Paris, d'une part, a annulé le jugement n° 0919193/3-1 du 14 juin 2011 par lequel le tribunal administratif de Paris avait rejeté la demande de la société HetM tendant à l'annulation de la décision du 2 octobre 2009 du ministre chargé du travail annulant la décision du 30 mars 2009 de l'inspecteur du travail autorisant son licenciement et refusant à la société HetM l'autorisation de la licencier, et, d'autre part, a annulé la décision du 2 octobre 2009 du ministre du travail ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la société HetM ;<br/>
<br/>
              3°) de mettre à la charge de la société HetM une somme de 3 500 euros sur le fondement de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code du travail ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Benjamin de Maillard, auditeur,  <br/>
<br/>
              - les conclusions de M. Rémi Keller, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de Mme B...et à la SCP de Chaisemartin, Courjon, avocat de la société HetM ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme B...exerçait les mandats de membre titulaire de la délégation unique du personnel et du comité d'hygiène, de sécurité et des conditions de travail au sein de la société Bouchara Hausmann ; que cette société a fait l'objet d'une fusion absorption le 13 octobre 2008 par la société HetM à laquelle le contrat de travail de Mme B...a été transféré en application de l'article L. 1224-1 du code du travail ; que la société HetM a proposé aux cent trois anciens salariés de la société Bouchara Haussman une modification de leur contrat de travail comprenant notamment leur transfert sur d'autres sites de vente de la société pendant la fermeture de l'établissement pour travaux pour une durée prévisionnelle d'un an ; que quarante-trois salariés, dont MmeB..., ont refusé cette modification ; que, le 10 mars 2009, la société HetM a sollicité de l'inspectrice du travail de la section 9 B de Paris l'autorisation de licencier Mme B... pour motif économique ; que, par une décision du 30 mars 2009, l'inspectrice du travail a autorisé la société HetM à procéder au licenciement de l'intéressée ; que, sur recours hiérarchique de MmeB..., le ministre chargé du travail a, par décision du 2 octobre 2009, d'une part, annulé la décision du 30 mars 2009 de l'inspectrice du travail, et, d'autre part, refusé d'autoriser le licenciement de l'intéressée ; que, par un jugement du 14 juin 2011, le tribunal administratif de Paris a rejeté la demande de la société HetM tendant à l'annulation de cette décision ; que Mme B...se pourvoit en cassation contre l'arrêt du 4 mars 2013 par lequel la cour administrative d'appel de Paris a, d'une part, annulé le jugement du 14 juin 2011 du tribunal administratif de Paris et, d'autre part, annulé la décision du ministre chargé du travail refusant à la société HetM l'autorisation de la licencier ;<br/>
<br/>
              2. Considérant qu'en vertu des dispositions du code du travail, le licenciement des délégués syndicaux et des membres du comité d'entreprise, qui bénéficient, dans l'intérêt de l'ensemble des travailleurs qu'ils représentent, d'une protection exceptionnelle, est subordonné à une autorisation de l'inspecteur du travail dont dépend l'établissement ; que, lorsque le licenciement d'un de ces salariés est envisagé, ce licenciement ne doit pas être en rapport avec les fonctions représentatives normalement exercées ou l'appartenance syndicale de l'intéressé ; que, dans le cas où la demande d'autorisation de licenciement est fondée sur un motif de caractère économique, il appartient à l'inspecteur du travail et, le cas échéant, au ministre de rechercher, sous le contrôle du juge de l'excès de pouvoir, si la situation de l'entreprise justifie le licenciement, en tenant compte notamment de la nécessité des réductions envisagées d'effectifs et de la possibilité d'assurer le reclassement du salarié dans l'entreprise ou au sein du groupe auquel appartient cette dernière ; qu'en outre, pour refuser l'autorisation sollicitée, l'autorité administrative a la faculté de retenir des motifs d'intérêt général relevant de son pouvoir d'appréciation de l'opportunité, sous réserve qu'une atteinte excessive ne soit pas portée à l'un ou l'autre des intérêts en présence ;<br/>
<br/>
              3. Considérant qu'aux termes du premier alinéa de l'article L. 1233-3 du code du travail : " Constitue un licenciement pour motif économique le licenciement effectué par un employeur pour un ou plusieurs motifs non inhérents à la personne du salarié résultant d'une suppression ou transformation d'emploi ou d'une modification, refusée par le salarié, d'un élément essentiel du contrat de travail, consécutives notamment à des difficultés économiques ou à des mutations technologiques " ; que, lorsque l'employeur sollicite une autorisation de licenciement pour motif économique fondée sur le refus du salarié d'accepter une modification de son contrat de travail, il appartient à l'inspecteur du travail et, le cas échéant, au ministre, de rechercher, sous le contrôle du juge de l'excès de pouvoir, si cette modification était justifiée par un motif économique ; que si la sauvegarde de la compétitivité de l'entreprise peut constituer un tel motif, c'est à la condition que soit établie une menace pour la compétitivité de l'entreprise, laquelle s'apprécie, lorsque l'entreprise appartient à un groupe, au niveau du secteur d'activité dont relève l'entreprise en cause au sein du groupe ; <br/>
<br/>
              4. Considérant que pour juger que le motif économique du licenciement de Mme B...était réel, la cour administrative d'appel de Paris a retenu que " l'obligation de rémunérer 43 salariées sans contrepartie de travail constituait une charge anormale pour la société requérante de nature à affecter la compétitivité de l'entreprise ", sans rechercher si ces faits caractérisaient l'existence d'une menace pour la compétitivité du secteur d'activité auquel appartient l'entreprise HetM France  au sein du groupe HetM ; que la cour a donc entaché son arrêt d'une erreur de droit ; que Mme B...est, par suite, fondée, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, à en demander l'annulation ; <br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société HetM une somme de 1 000 euros à verser à Mme B...au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font en revanche obstacle à ce qu'une somme soit mise à la charge de MmeB..., qui n'est pas la partie perdante dans la présente instance ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt n° 11PA03514 de la cour administrative d'appel de Paris du 4 mars 2013 est annulé.<br/>
Article 2 : L'affaire est renvoyée devant la cour administrative d'appel de Paris.<br/>
Article 3 : La société HetM versera une somme de 1 000 euros à Mme B...au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de la société HetM présentées sur le fondement de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à Mme A...B..., à la société HetM et au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
