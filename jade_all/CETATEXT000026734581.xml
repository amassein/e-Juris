<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026734581</ID>
<ANCIEN_ID>JG_L_2012_12_000000352063</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/73/45/CETATEXT000026734581.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 06/12/2012, 352063</TITRE>
<DATE_DEC>2012-12-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>352063</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PEIGNOT, GARREAU, BAUER-VIOLAS ; SCP FABIANI, LUC-THALER</AVOCATS>
<RAPPORTEUR>Mme Sophie Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne Von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:352063.20121206</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 22 août et 22 novembre 2011 au secrétariat du contentieux du Conseil d'État, présentés pour M.B..., demeurant...,; M. B...demande au Conseil d'État : <br/>
<br/>
              1°) d'annuler la décision du 16 juin 2011 par laquelle la chambre nationale de discipline auprès du conseil supérieur de l'Ordre des experts-comptables a rejeté sa requête tendant à l'annulation et à la réformation de la décision du 22 juin 2010 par laquelle la chambre régionale de discipline de l'Ordre des experts-comptables de Lyon/Rhône-Alpes a prononcé à son encontre la peine de radiation du tableau comportant interdiction définitive d'exercer la profession d'expert-comptable ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
<br/>
              Vu le code de commerce ; <br/>
<br/>
              Vu l'ordonnance n° 45-3138 du 19 septembre 1945 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie Roussel, Auditeur,  <br/>
<br/>
              - les observations de la SCP Fabiani, Luc-Thaler, avocat de M. A...B..., et de la SCP Peignot, Garreau, Bauer-Violas, avocat du conseil supérieur de l'Ordre des experts-comptables, <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Fabiani, Luc-Thaler, avocat de M. A...B...et à la SCP Peignot, Garreau, Bauer-Violas, avocat du conseil supérieur de l'Ordre des experts-comptables ;<br/>
<br/>
<br/>
<br/>
<br/>Sur la régularité de la décision attaquée : <br/>
<br/>
              En ce qui concerne la composition de la chambre nationale de discipline :<br/>
<br/>
              1. Considérant qu'aux termes de l'article 50 de l'ordonnance du 19 septembre 1945 portant institution de l'Ordre des experts-comptables et réglementant le titre et la profession d'expert-comptable, la chambre nationale de discipline auprès du conseil supérieur de l'Ordre des experts-comptables est notamment composée " d'un conseiller référendaire à la Cour des comptes et d'un fonctionnaire, désignés par le ministre de l'économie et des finances " ; que, selon le requérant, cette composition ne satisferait pas aux exigences attachées au principe d'impartialité garanti par les stipulations de l'article 6§1 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, dès lors que la plainte à l'origine de la procédure disciplinaire engagée à son encontre a été déposée par le directeur des services fiscaux du Rhône, fonctionnaire placé sous l'autorité du même ministre que celui ayant désigné deux des membres de la chambre nationale de discipline auprès du conseil supérieur de l'ordre des experts-comptables ;<br/>
<br/>
              2. Considérant qu'en vertu des principes généraux applicables à la fonction de juger, toute personne appelée à siéger dans une juridiction, y compris disciplinaire, doit se prononcer en toute indépendance et sans recevoir quelque instruction de la part de quelque autorité que ce soit ; que, dès lors, la présence de fonctionnaires de l'Etat parmi les membres d'une juridiction ayant à connaître de litiges auxquels celui-ci peut être partie ne peut, par elle-même, être de nature à faire naître un doute objectivement justifié sur l'impartialité de celle-ci ; qu'il peut toutefois en aller différemment, d'une part, lorsqu'un fonctionnaire représente le ministre dans la formation de jugement alors que l'auteur de la plainte est un agent du même ministère, d'autre part, lorsque, sans que des garanties appropriées assurent son indépendance, les fonctions exercées par un fonctionnaire appelé à siéger dans la chambre nationale de discipline le font participer à l'activité des services ayant déposé la plainte à l'origine de la procédure disciplinaire engagée ;<br/>
<br/>
              3. Considérant, en premier lieu, qu'il résulte des termes mêmes de l'article 50 de l'ordonnance du 19 septembre 1945 cité ci-dessus que le conseiller référendaire à la Cour des comptes et le fonctionnaire membres de la chambre nationale de discipline auprès du conseil supérieur de l'ordre des experts-comptables ne siègent pas en qualité de représentants du ministre de l'économie et des finances ; que le simple fait qu'ils soient désignés par le ministre de l'économie et des finances ne permet pas, en l'absence de tout pouvoir pour le ministre de leur adresser des instructions, de les regarder comme représentant les intérêts de ce ministre, cette fonction incombant, en vertu de l'article 56 de l'ordonnance du 19 septembre 1945, à un commissaire du Gouvernement auprès du conseil supérieur de l'ordre ; <br/>
<br/>
              4. Considérant, en second lieu, qu'il ressort des énonciations de la décision attaquée que la formation de la chambre nationale de discipline auprès du conseil supérieur de l'ordre des experts-comptables qui a statué sur l'appel de M. B...comptait en son sein deux membres désignés par le ministre de l'économie, de l'industrie et de l'emploi ; que le premier, conseiller référendaire à la Cour des comptes, comme le second, en fonction à la direction de la législation fiscale à la date à laquelle la chambre nationale de discipline a statué, exerçaient l'un et l'autre des activités sans lien avec celles de contrôle fiscal exercées par le directeur des service fiscaux du Rhône ayant déposé la plainte à l'origine de la procédure disciplinaire engagée à l'encontre de M. B...; que, par suite, la circonstance que ces deux personnes, toutes deux désignées par le ministre de l'économie et des finances, aient été appelées à statuer sur la régularité et le bien-fondé de la sanction prononcée par la chambre régionale de discipline à la suite d'une plainte déposée par un agent placé sous l'autorité du même ministre n'était pas de nature à faire obstacle, par elle-même, à ce que la chambre nationale de discipline auprès du conseil supérieur des experts-comptables pût être regardée comme un tribunal indépendant et impartial, au sens des stipulations de l'article 6§1 de la convention européenne ni à créer un doute objectivement justifié sur l'impartialité de cette juridiction ; <br/>
<br/>
<br/>
              En ce qui concerne les autres moyens de régularité :<br/>
<br/>
               5. Considérant que si le requérant soutient que, faute pour la personne poursuivie de savoir si des instructions ont été adressées aux personnes désignées par le ministre et, dans l'hypothèse où il y en aurait eu, d'avoir pu en prendre connaissance, le principe du caractère contradictoire de la procédure a été méconnu, il résulte de ce qui a été dit ci-dessus que le moyen doit, en tout état de cause, être écarté ; <br/>
<br/>
              6. Considérant que les éléments avancés par le requérant devant la chambre nationale de discipline pour minorer l'importance des faits qui lui étaient reprochés et ainsi remettre en cause la radiation du tableau avec interdiction définitive d'exercer la profession d'expert-comptable prononcée par la chambre régionale de discipline ne constituent pas des conclusions mais de simples arguments venant au soutien du moyen d'erreur de qualification juridique des faits, invoqué devant la chambre nationale de discipline, à avoir jugé que les faits de fraude fiscale reprochés à M. B...justifiaient la sanction de radiation prononcée à son encontre ; que, dès lors, le moyen selon lequel la chambre nationale de discipline aurait omis de répondre à plusieurs des conclusions d'appel du requérant ne peut qu'être écarté ;<br/>
<br/>
              Sur le bien-fondé de la décision attaquée : <br/>
<br/>
              7. Considérant, en premier lieu, que l'obligation de mise en cause du mandataire judiciaire et de l'administrateur pour les actions en justice et les procédures d'exécution autres que les actions en paiement et les actions en résolution mentionnées à l'article L. 622-21 du code de commerce, prévue par les dispositions de l'article L. 622-23 du même code, ne s'applique que dans le champ des procédures de sauvegarde des entreprises en difficulté du titre II du livre VI du code de commerce ; que, par suite, en jugeant que les dispositions de l'article L. 622-23 du code de commerce ne s'appliquaient pas à la procédure disciplinaire engagée à l'encontre de M. B...et que, par suite, ce dernier n'était pas fondé à soutenir que la procédure disciplinaire était irrégulière faute de mise en cause des mandataires de justice désignés dans le cadre de la procédure de redressement judiciaire ouverte à son encontre, la chambre nationale de discipline auprès du conseil supérieur de l'ordre des experts-comptables, qui a suffisamment motivé sa décision sur ce point, n'a pas commis d'erreur de droit; <br/>
<br/>
              8. Considérant, en deuxième lieu, qu'en jugeant, après avoir relevé dans sa décision, d'une part, que M. B...avait été condamné pour diverses fraudes fiscales pour les années 2000 à 2003, par une décision du tribunal correctionnel de Saint-Etienne le 26 janvier 2006, confirmée par un arrêt de la cour d'appel de Lyon du 13 septembre 2006, d'autre part, qu'il avait déjà fait l'objet d'une mesure de suspension d'une durée d'un mois, prononcée le 7 février 2003 par la chambre régionale de discipline du Rhône pour des faits similaires, que les manquements graves et réitérés de M. B...à ses obligations fiscales caractérisaient un manque de conscience et de probité, contraires aux articles 1er et 2 du code des devoirs professionnels alors applicable, la chambre nationale de discipline auprès du conseil supérieur de l'ordre des experts-comptables n'a pas inexactement qualifié les faits de l'espèce  ; que l'erreur de plume à s'être référée au non dépôt de déclarations et au défaut de déclarations de M. B...en matière de bénéfices non commerciaux et d'impôts sur le revenu pour les exercices 2000 à 2002, alors que l'année 2001 n'était pas en cause, est sans incidence sur cette appréciation ; <br/>
<br/>
              9. Considérant, enfin, que l'appréciation, par la juridiction disciplinaire, de la proportionnalité de la sanction aux manquements retenus ne peut être utilement discutée devant le juge de cassation qu'en cas de dénaturation ; qu'en l'espèce, en estimant que les manquements ainsi retenus à l'encontre de M. B...justifiaient la sanction de radiation du tableau comportant interdiction définitive d'exercer la profession, la chambre nationale de discipline auprès du conseil supérieur de l'ordre des experts-comptables n'a pas entaché de dénaturation l'appréciation à laquelle elle s'est livrée ; <br/>
<br/>
               10. Considérant qu'il résulte de tout ce qui précède que M. B...n'est pas fondé à demander l'annulation de la décision de la chambre nationale de discipline auprès du conseil supérieur de l'ordre des experts-comptables du 16 mai 2011 ; que ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ; que les dispositions de cet article font également obstacle à ce que soit mise à la charge de M. B...la somme que demande le conseil supérieur de l'Ordre des experts-comptables, mis en cause dans l'instance pour observations et qui n'a pas la qualité de partie ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
 Article 1er : Le pourvoi de M. B...est rejeté.<br/>
<br/>
 Article 2 : Les conclusions du conseil supérieur de l'Ordre des experts-comptables tendant à l'application de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
 Article 3 : La présente décision sera notifiée à M. A...B..., au conseil supérieur de l'Ordre des experts-comptables et au ministre de l'économie et des finances.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-06 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. PRINCIPES RÉGISSANT L'ORGANISATION ET LE FONCTIONNEMENT DES JURIDICTIONS. - PRINCIPE D'IMPARTIALITÉ - JURIDICTION COMPORTANT DES FONCTIONNAIRES DE L'ETAT - 1) RESPECT - CONDITIONS [RJ1] - 2) APPLICATION AU CONSEIL SUPÉRIEUR DE L'ORDRE DES EXPERTS-COMPTABLES.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">26-055-01-06-02 DROITS CIVILS ET INDIVIDUELS. CONVENTION EUROPÉENNE DES DROITS DE L'HOMME. DROITS GARANTIS PAR LA CONVENTION. DROIT À UN PROCÈS ÉQUITABLE (ART. 6). VIOLATION. - CONSEIL SUPÉRIEUR DE L'ORDRE DES EXPERTS-COMPTABLES - CHAMBRE NATIONALE DE DISCIPLINE EXAMINANT EN APPEL UNE SANCTION PRONONCÉE SUR PLAINTE D'UN AGENT PLACÉ SOUS L'AUTORITÉ DU MINISTRE CHARGÉ DE L'ÉCONOMIE - PARTICIPATION DE FONCTIONNAIRES DÉSIGNÉS PAR CE MINISTRE - ABSENCE, DÈS LORS QU'ILS NE REPRÉSENTENT PAS LES INTÉRÊTS DU MINISTRE ET EXERCENT DES FONCTIONS SANS LIEN AVEC CELLES DE L'AUTEUR DE LA PLAINTE [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-06-03 PROCÉDURE. JUGEMENTS. COMPOSITION DE LA JURIDICTION. - JURIDICTION COMPORTANT DES FONCTIONNAIRES DE L'ETAT - RESPECT DU PRINCIPE D'IMPARTIALITÉ - CONDITIONS [RJ1].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">55-04-01-02 PROFESSIONS, CHARGES ET OFFICES. DISCIPLINE PROFESSIONNELLE. PROCÉDURE DEVANT LES JURIDICTIONS ORDINALES. JUGEMENTS. - COMPOSITION DE LA FORMATION DE JUGEMENT - PRÉSENCE DE FONCTIONNAIRES DE L'ETAT - RESPECT DU PRINCIPE D'IMPARTIALITÉ - 1) CONDITIONS [RJ1] - 2) APPLICATION AU CONSEIL SUPÉRIEUR DE L'ORDRE DES EXPERTS-COMPTABLES.
</SCT>
<ANA ID="9A"> 01-04-03-06 1) En vertu des principes généraux applicables à la fonction de juger, toute personne appelée à siéger dans une juridiction, y compris disciplinaire, doit se prononcer en toute indépendance et sans recevoir quelque instruction de la part de quelque autorité que ce soit. Dès lors, la présence de fonctionnaires de l'Etat parmi les membres d'une juridiction ayant à connaître de litiges auxquels celui-ci peut être partie ne peut, par elle-même, être de nature à faire naître un doute objectivement justifié sur l'impartialité de celle-ci. Il peut toutefois en aller différemment, d'une part, lorsqu'un fonctionnaire représente le ministre dans la formation de jugement alors que l'auteur de la plainte est un agent du même ministère, d'autre part, lorsque, sans que des garanties appropriées assurent son indépendance, les fonctions exercées par un fonctionnaire appelé à siéger dans la chambre nationale de discipline le font participer à l'activité des services ayant déposé la plainte à l'origine de la procédure disciplinaire engagée.,,2) Cas d'une plainte déposée par un agent placé sous l'autorité du ministre chargé de l'économie, examinée en appel par la chambre nationale de discipline auprès du conseil supérieur de l'Ordre des experts-comptables comportant un conseiller référendaire à la Cour des comptes et un fonctionnaire désignés par ce ministre. Dès lors, d'une part, qu'il résulte des termes mêmes de l'article 50 de l'ordonnance n° 45-3138 du 19 septembre 1945 que ces derniers ne siègent pas en qualité de représentants du ministre, qui n'a aucun pouvoir de leur adresser des instructions, et, d'autre part, qu'ils exercent l'un et l'autre des activités sans lien avec celles de contrôle fiscal exercées par le directeur des service fiscaux ayant déposé la plainte à l'origine de la procédure disciplinaire, la circonstance qu'ils aient été appelés à statuer sur la régularité et le bien-fondé de la sanction prononcée par la chambre régionale de discipline n'était pas de nature à faire obstacle, par elle-même, à ce que la chambre nationale de discipline auprès du conseil supérieur de l'Ordre des experts-comptables pût être regardée comme un tribunal indépendant et impartial, au sens des stipulations de l'article 6§1 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ni à créer un doute objectivement justifié sur l'impartialité de cette juridiction.</ANA>
<ANA ID="9B"> 26-055-01-06-02 Cas d'une plainte déposée par un agent placé sous l'autorité du ministre chargé de l'économie, examinée en appel par la chambre nationale de discipline auprès du conseil supérieur de l'Ordre des experts-comptables comportant un conseiller référendaire à la Cour des comptes et un fonctionnaire désignés par ce ministre. Dès lors, d'une part, qu'il résulte des termes mêmes de l'article 50 de l'ordonnance n° 45-3138 du 19 septembre 1945 que ces derniers ne siègent pas en qualité de représentants du ministre, qui n'a aucun pouvoir de leur adresser des instructions, et, d'autre part, qu'ils exercent l'un et l'autre des activités sans lien avec celles de contrôle fiscal exercées par le directeur des service fiscaux ayant déposé la plainte à l'origine de la procédure disciplinaire, la circonstance qu'ils aient été appelés à statuer sur la régularité et le bien-fondé de la sanction prononcée par la chambre régionale de discipline n'était pas de nature à faire obstacle, par elle-même, à ce que la chambre nationale de discipline auprès du conseil supérieur de l'Ordre des experts-comptables pût être regardée comme un tribunal indépendant et impartial, au sens des stipulations de l'article 6§1 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ni à créer un doute objectivement justifié sur l'impartialité de cette juridiction.</ANA>
<ANA ID="9C"> 54-06-03 En vertu des principes généraux applicables à la fonction de juger, toute personne appelée à siéger dans une juridiction, y compris disciplinaire, doit se prononcer en toute indépendance et sans recevoir quelque instruction de la part de quelque autorité que ce soit. Dès lors, la présence de fonctionnaires de l'Etat parmi les membres d'une juridiction ayant à connaître de litiges auxquels celui-ci peut être partie ne peut, par elle-même, être de nature à faire naître un doute objectivement justifié sur l'impartialité de celle-ci. Il peut toutefois en aller différemment, d'une part, lorsqu'un fonctionnaire représente le ministre dans la formation de jugement alors que l'auteur de la plainte est un agent du même ministère, d'autre part, lorsque, sans que des garanties appropriées assurent son indépendance, les fonctions exercées par un fonctionnaire appelé à siéger dans la chambre nationale de discipline le font participer à l'activité des services ayant déposé la plainte à l'origine de la procédure disciplinaire engagée.</ANA>
<ANA ID="9D"> 55-04-01-02 1) En vertu des principes généraux applicables à la fonction de juger, toute personne appelée à siéger dans une juridiction, y compris disciplinaire, doit se prononcer en toute indépendance et sans recevoir quelque instruction de la part de quelque autorité que ce soit. Dès lors, la présence de fonctionnaires de l'Etat parmi les membres d'une juridiction ayant à connaître de litiges auxquels celui-ci peut être partie ne peut, par elle-même, être de nature à faire naître un doute objectivement justifié sur l'impartialité de celle-ci. Il peut toutefois en aller différemment, d'une part, lorsqu'un fonctionnaire représente le ministre dans la formation de jugement alors que l'auteur de la plainte est un agent du même ministère, d'autre part, lorsque, sans que des garanties appropriées assurent son indépendance, les fonctions exercées par un fonctionnaire appelé à siéger dans la chambre nationale de discipline le font participer à l'activité des services ayant déposé la plainte à l'origine de la procédure disciplinaire engagée.,,2) Cas d'une plainte déposée par un agent placé sous l'autorité du ministre chargé de l'économie, examinée en appel par la chambre nationale de discipline auprès du conseil supérieur de l'Ordre des experts-comptables comportant un conseiller référendaire à la Cour des comptes et un fonctionnaire désignés par ce ministre. Dès lors, d'une part, qu'il résulte des termes mêmes de l'article 50 de l'ordonnance n° 45-3138 du 19 septembre 1945 que ces derniers ne siègent pas en qualité de représentants du ministre, qui n'a aucun pouvoir de leur adresser des instructions, et, d'autre part, qu'ils exercent l'un et l'autre des activités sans lien avec celles de contrôle fiscal exercées par le directeur des service fiscaux ayant déposé la plainte à l'origine de la procédure disciplinaire, la circonstance qu'ils aient été appelés à statuer sur la régularité et le bien-fondé de la sanction prononcée par la chambre régionale de discipline n'était pas de nature à faire obstacle, par elle-même, à ce que la chambre nationale de discipline auprès du conseil supérieur de l'Ordre des experts-comptables pût être regardée comme un tribunal indépendant et impartial, au sens des stipulations de l'article 6§1 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ni à créer un doute objectivement justifié sur l'impartialité de cette juridiction.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., CE, Section, 6 décembre 2002, Aïn-Lhout, n° 221319, p. 430 ; CE, Assemblée, 6 décembre 2002, Trognon, n° 240028, p. 427 ; CE, 8 décembre 2000, Mongauze, n° 198372, T. pp. 997-1213.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
