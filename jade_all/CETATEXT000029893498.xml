<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029893498</ID>
<ANCIEN_ID>JG_L_2014_12_000000365211</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/89/34/CETATEXT000029893498.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème / 4ème SSR, 12/12/2014, 365211</TITRE>
<DATE_DEC>2014-12-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>365211</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème / 4ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO ; SCP HEMERY, THOMAS-RAQUIN ; SCP ROGER, SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR>M. Lionel Collet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:365211.20141212</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
		Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Nantes de mettre à la charge de l'Office national d'indemnisation des accidents médicaux (ONIAM) la réparation des préjudices qu'elle a subis à la suite d'une intubation pratiquée le 10 août 2004 au centre hospitalier de Saint-Nazaire. Par un jugement n° 0706398 du 1er juin 2011, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 11NT01968 du 15 novembre 2012, la cour administrative d'appel de Nantes a rejeté l'appel formé contre ce jugement par MmeA....<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 15 janvier et 15 avril 2013 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt n° 11NT01968 du 15 novembre 2012 de la cour administrative d'appel de Nantes ;<br/>
<br/>
              2°) de mettre à la charge de l'ONIAM la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu :<br/>
<br/>
              - les autres pièces du dossier ;<br/>
<br/>
              - le code de la santé publique notamment son article L. 1142-1 ;<br/>
<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Lionel Collet, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Hémery, Thomas-Raquin, avocat de MmeA..., à la SCP Roger, Sevaux, Mathonnet, avocat de l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales et à Me Le Prado, avocat du centre hospitalier de Saint-Nazaire ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que MmeA..., diabétique insulino-dépendante, a été victime le 10 août 2004 d'un coma diabétique acido-cétosique qui a rendu nécessaire une intubation pratiquée en urgence durant son transfert au centre hospitalier de Saint-Nazaire ; qu'en raison de cette intubation elle a présenté une sténose laryngée, provoquant des difficultés respiratoires ainsi que des troubles de la phonation et de la déglutition ; qu'elle a demandé au tribunal administratif de Nantes de mettre la réparation de ce dommage à la charge de l'ONIAM au titre de la solidarité nationale ; que cette demande a été rejetée par un jugement du 1er juin 2011 confirmé par un arrêt du 15 novembre 2012 de la cour administrative d'appel de Nantes contre lequel elle se pourvoit en cassation ;<br/>
<br/>
              2. Considérant qu'aux termes du II de l'article L. 1142-1 du code de la santé publique, dans sa rédaction applicable au litige : " Lorsque la responsabilité d'un professionnel, d'un établissement, service ou organisme mentionné au I ou d'un producteur de produits n'est pas engagée, un accident médical, une affection iatrogène ou une infection nosocomiale ouvre droit à la réparation des préjudices du patient et, en cas de décès, de ses ayants droit au titre de la solidarité nationale, lorsqu'ils sont directement imputables à des actes de prévention, de diagnostic ou de soins et qu'ils ont eu pour le patient des conséquences anormales au regard de son état de santé comme de l'évolution prévisible de celui-ci et présentent un caractère de gravité, fixé par décret, apprécié au regard de la perte de capacités fonctionnelles et des conséquences sur la vie privée et professionnelle mesurées en tenant notamment compte du taux d'atteinte permanente à l'intégrité physique ou psychique, de la durée de l'arrêt temporaire des activités professionnelles ou de celle du déficit fonctionnel temporaire " ; que l'article D. 1142-1 du même code définit le seuil de gravité prévu par ces dispositions législatives ;<br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions que l'ONIAM doit assurer, au titre de la solidarité nationale, la réparation des dommages résultant directement d'actes de prévention, de diagnostic ou de soins à la double condition qu'ils présentent un caractère d'anormalité au regard de l'état de santé du patient comme de l'évolution prévisible de cet état et que leur gravité excède le seuil défini à l'article D. 1142-1 ; <br/>
<br/>
              4. Considérant que la condition d'anormalité du dommage prévue par ces dispositions doit toujours être regardée comme remplie lorsque l'acte médical a entraîné des conséquences notablement plus graves que celles auxquelles le patient était exposé de manière suffisamment probable en l'absence de traitement ; <br/>
<br/>
              5. Considérant que, lorsque les conséquences de l'acte médical ne sont pas notablement plus graves que celles auxquelles le patient était exposé  par sa pathologie en l'absence de traitement, elles ne peuvent être regardées comme anormales sauf si, dans les conditions où l'acte a été accompli, la survenance du dommage présentait une probabilité faible ; qu'ainsi, elles ne peuvent être regardées comme anormales au regard de l'état du patient lorsque la gravité de cet état a conduit à pratiquer un acte comportant des risques élevés dont la réalisation est à l'origine du dommage ; <br/>
<br/>
              6. Considérant que, pour juger que les conséquences dommageables de l'intubation subie par Mme A...ne pouvaient être regardées comme anormales au regard de son état comme de l'évolution prévisible de celui-ci, la cour administrative d'appel a relevé, en se fondant notamment sur le rapport de l'expert désigné par le tribunal administratif de Nantes, d'une part, que l'intubation, pratiquée in extremis, présentait un caractère vital eu égard à l'état de coma diabétique et, d'autre part, que la complication survenue, " bien qu'exceptionnelle, est favorisée par divers facteurs, tenant en particulier aux conditions d'intervention en urgence lorsque le pronostic vital est engagée " ; que la cour a ainsi fait apparaître que les conséquences de l'intubation n'étaient pas plus graves que celles auxquelles la patiente était exposée par sa pathologie et que si le risque de sténose laryngée inhérent à cet acte médical revêtait, en principe, un caractère exceptionnel, il en était allé autrement dans les circonstances de l'espèce, compte tenu notamment du fait qu'il avait dû être pratiqué en urgence ; qu'elle a ainsi suffisamment motivé son arrêt et n'a pas commis d'erreur de droit ; qu'en estimant que la condition d'anormalité n'était pas remplie, au vu d'un dossier dont il ressortait que la survenue d'une sténose laryngée entraînant des séquelles durables avait été favorisée non seulement par la réalisation en urgence de l'intubation mais également par un collapsus tensionnel et le diabète, et qu'elle ne pouvait être regardée comme résultant en l'espèce de la réalisation d'un risque présentant une probabilité faible, la cour n'a pas inexactement qualifié les faits de l'espèce ; <br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de l'ONIAM, qui n'est pas partie perdante dans la présente instance, le versement d'une somme au titre des frais exposés et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
		Article 1er : Le pourvoi de Mme A... est rejeté.<br/>
<br/>
		Article 2 : La présente décision sera notifiée à Mme B...A.... <br/>
Copie en sera adressée pour information à l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales, au centre hospitalier de Saint-Nazaire et à la caisse primaire d'assurance maladie des Hauts de Seine. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-02-01-01-005-02 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICE PUBLIC DE SANTÉ. ÉTABLISSEMENTS PUBLICS D'HOSPITALISATION. RESPONSABILITÉ SANS FAUTE. ACTES MÉDICAUX. - PRISE EN CHARGE PAR LA SOLIDARITÉ NATIONALE DES CONSÉQUENCES ANORMALES ET GRAVES DES ACTES MÉDICAUX (II DE L'ART. L. 1142-1 DU CODE DE LA SANTÉ PUBLIQUE) - CONDITION D'ANORMALITÉ [RJ1] - CONDITION NON REMPLIE EN L'ESPÈCE - CONSÉQUENCES N'ÉTANT PAS NOTABLEMENT PLUS GRAVES QUE CELLES AUXQUELLES LE PATIENT ÉTAIT EXPOSÉ EN L'ABSENCE DE TRAITEMENT, ET SURVENANCE DU DOMMAGE NE PRÉSENTANT PAS UNE PROBABILITÉ FAIBLE.
</SCT>
<ANA ID="9A"> 60-02-01-01-005-02 Personne ayant subi une intubation, pratiquée in extremis, qui présentait un caractère vital eu égard à son état de coma diabétique. Par ailleurs, la complication survenue, bien qu'exceptionnelle, était favorisée par divers facteurs, tenant en particulier aux conditions d'intervention en urgence lorsque le pronostic vital est engagée. Ainsi, les conséquences de l'intubation n'étaient pas plus graves que celles auxquelles la patiente était exposée par sa pathologie. Si le risque de sténose laryngée inhérent à cet acte médical revêt, en principe, un caractère exceptionnel, il en était allé autrement dans les circonstances de l'espèce où, compte tenu notamment du fait qu'il avait dû être pratiqué en urgence, de la survenue d'un collapsus tensionnel et du diabète dont souffrait l'intéressé, la complication ne pouvait être regardée comme présentant une probabilité faible. Par suite, la condition d'anormalité à laquelle le II de l'article L. 1142-1 du code de la santé publique subordonne la prise en charge par la solidarité nationale des conséquences graves des actes médicaux n'est pas remplie en l'espèce.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. décision du même jour, ONIAM c/ M. Bondoni, n° 355052, à publier au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
