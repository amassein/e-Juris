<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000024448403</ID>
<ANCIEN_ID>JG_L_2011_07_000000349020</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/24/44/84/CETATEXT000024448403.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 26/07/2011, 349020</TITRE>
<DATE_DEC>2011-07-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>349020</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Christian Vigouroux</PRESIDENT>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN ; SCP BORE ET SALVE DE BRUNETON ; FOUSSARD</AVOCATS>
<RAPPORTEUR>M. Jean Lessi</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Claire Landais</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2011:349020.20110726</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête sommaire et le mémoire complémentaire, enregistrés les 5 et 23 mai 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. Julien B, demeurant ... ; M. B demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler la décision n° 1101797 du 5 avril 2011 du tribunal administratif de Paris rejetant sa demande tendant à ce qu'il soit autorisé, en application de l'article L. 2132-5 du code général des collectivités territoriales, d'une part, à se constituer partie civile au nom de la ville de Paris, y compris en retirant tout acte de désistement déjà pris, devant le tribunal correctionnel de Paris dans l'affaire relative aux poursuites engagées contre M. Jacques A et d'autres personnes des chefs, notamment, de détournement de fonds publics, de prise illégale d'intérêt et de recel de ces infractions à raison des conditions d'emploi de certains personnels par la ville de Paris entre 1992 et 1996 et, d'autre part, à réclamer des dommages et intérêts en réparation du préjudice subi par la ville à ce titre pour un montant supérieur à la somme initialement demandée de 2 218 072,46 euros ;<br/>
<br/>
              2°) de l'autoriser à exercer cette action au nom de la ville de Paris ;<br/>
<br/>
              3°) de lui accorder une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu le code de procédure pénale ; <br/>
<br/>
              Vu le code civil ;<br/>
<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean Lessi, Auditeur,  <br/>
<br/>
              - les observations de la SCP Masse-Dessen, Thouvenin, avocat de M. B, de la SCP Boré et Salve de Bruneton, avocat de M. Jacques A et de Me Foussard, avocat de la ville de Paris, <br/>
<br/>
              - les conclusions de Mme Claire Landais, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Masse-Dessen, Thouvenin, avocat de M. B, à la SCP Boré et Salve de Bruneton, avocat de M. Jacques A et à Me Foussard, avocat de la ville de Paris ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'aux termes de l'article L. 2132-5 du code général des collectivités territoriales : " Tout contribuable inscrit au rôle de la commune a le droit d'exercer, tant en demande qu'en défense, à ses frais et risques, avec l'autorisation du tribunal administratif, les actions qu'il croit appartenir à la commune et que celle-ci, préalablement appelée à en délibérer, a refusé ou négligé d'exercer. " ; qu'il appartient au tribunal administratif statuant comme autorité administrative, et au Conseil d'Etat, saisi d'un recours de pleine juridiction dirigé contre la décision du tribunal administratif, lorsqu'ils examinent une demande présentée par un contribuable sur le fondement de ces dispositions, de vérifier, sans se substituer au juge de l'action, et au vu des éléments qui leur sont fournis, que l'action envisagée présente un intérêt matériel suffisant pour la commune et qu'elle a une chance de succès ;<br/>
<br/>
              Considérant que M. B a demandé au tribunal administratif de Paris de l'autoriser, d'une part, à exercer les droits de la partie civile au nom de la ville de Paris, au besoin en retirant tout désistement déjà pris, devant le tribunal correctionnel de Paris saisi, à la suite de deux informations judiciaires ouvertes aux tribunaux de grande instance de Nanterre et de Paris, de poursuites des chefs, notamment, de détournement de fonds publics, d'abus de confiance et de recel, contre M. Jacques A et plusieurs autres prévenus pour des faits relatifs aux conditions d'emploi par la ville d'un certain nombre de personnes entre 1992 et 1996 sans contrepartie pour celle-ci et, d'autre part, à réclamer pour le compte de la ville de Paris le versement de dommages et intérêts en réparation du préjudice subi en raison de ces faits ; que, par la décision attaquée du 5 avril 2011, le tribunal administratif a rejeté cette demande au motif que l'action envisagée ne présentait pas un intérêt suffisant pour la ville de Paris ;<br/>
<br/>
              Considérant que, par une délibération du 27 septembre 2010, le conseil de Paris a autorisé la conclusion avec M. A, à raison des faits mentionnés ci-dessus, ainsi qu'avec l'Union pour la majorité présidentielle (U.M.P.), à qui la ville avait fait part de son intention de la citer au cours de l'audience à venir du tribunal correctionnel de Paris aux fins de la voir condamnée à garantir le paiement de toutes sommes susceptibles de lui être accordées, un " protocole d'indemnisation " par lequel ces derniers s'engagent à verser à la ville, en contrepartie de son désistement du procès en cours et de sa renonciation à toute action en réparation, une somme de 2 218 072,46 euros, correspondant aux rémunérations perçues par dix-huit des dix-neuf personnes visées dans les ordonnances de renvoi devant le tribunal correctionnel des 30 octobre 2009 et 8 novembre 2010, ainsi qu'aux intérêts au taux légal et aux débours ; <br/>
<br/>
              Considérant qu'il ne résulte pas de l'instruction que, à supposer qu'elle aboutisse, l'action envisagée par M. B, qui estime que la ville de Paris pourrait prétendre à des dommages et intérêts au moins égaux à 4 194 320 euros, puisse permettre à la ville d'obtenir un montant significativement supérieur aux sommes qui lui sont contractuellement garanties, et auxquelles elle serait nécessairement appelée à renoncer dès lors que cette action suppose, sauf à être regardée comme irrecevable en raison de l'effet extinctif de l'action civile attaché au protocole - qui a la nature d'une transaction au sens de l'article 2044 du code civil - que M. B obtienne préalablement qu'en soit constatée la nullité et que la ville restitue ainsi les sommes déjà perçues ; qu'en effet, premièrement, le préjudice généré par des faits non retenus, car prescrits, par les ordonnances de renvoi devant le tribunal correctionnel de Paris, ne peut pas faire l'objet d'une réparation ; que, deuxièmement, M. B ne fournit aucun élément de nature à remettre sérieusement en cause ni l'évaluation, par les parties au protocole, des charges exposées par la ville au titre des emplois ayant donné lieu au procès en cours, ni l'appréciation ayant conduit la ville à abandonner ses prétentions s'agissant de l'un de ces emplois au motif qu'il n'a pas été sans contrepartie réelle pour elle ; que, troisièmement, la minoration des sommes versées à la ville résultant selon M. B du choix des parties de faire courir les intérêts légaux à partir du 1er janvier 1998, et non d'une date antérieure, apparaît purement hypothétique, dès lors que le juge de l'action civile pourrait discrétionnairement fixer ce point de départ, en vertu de l'article 1153-1 du code civil, à la date de prononcé de son jugement ou à toute autre date ; qu'enfin, la capitalisation des intérêts n'est, contrairement à ce que soutient le requérant, pas de droit ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que l'action envisagée par M. B ne présente pas d'intérêt suffisant pour la ville de Paris ; qu'est sans incidence à cet égard, à la date de la présente décision, le risque allégué par le requérant que la nullité de ce protocole soit ultérieurement constatée par le juge civil ;<br/>
<br/>
              Considérant qu'il suit de là que M. B n'est pas fondé à demander l'annulation de la décision du tribunal administratif de Paris du 5 avril 2011, qui est suffisamment motivée ; que sa requête doit dès lors être rejetée, y compris, par voie de conséquence, en ce qu'elle comporte des conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions que la ville de Paris présente au même titre ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de M. B est rejetée.<br/>
<br/>
Article 2 : Les conclusions présentées par la ville de Paris au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. Julien B, à la ville de Paris, à M. Jacques A et au ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-02-05-01-04 COLLECTIVITÉS TERRITORIALES. COMMUNE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. EXERCICE PAR UN CONTRIBUABLE DES ACTIONS APPARTENANT À LA COMMUNE. CONDITIONS DE FOND. - INTÉRÊT SUFFISANT - ABSENCE - TRANSACTION CONCLUE ENTRE LA COMMUNE ET SON DÉBITEUR.
</SCT>
<ANA ID="9A"> 135-02-05-01-04 Ne présente pas un intérêt suffisant l'action envisagée par un contribuable pour se constituer partie civile au nom de la ville de Paris dans une affaire de détournement de fonds et de prise illégale d'intérêts pour des faits d'emplois de personnes par la ville sans contrepartie dès lors qu'il n'apparaît pas que cette action puisse permettre à la ville d'obtenir une somme supérieure à celle qu'elle a obtenue des mis en cause par la conclusion d'un protocole d'indemnisation en contrepartie de son désistement dans le procès en cours.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
