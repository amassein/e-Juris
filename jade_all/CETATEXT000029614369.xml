<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029614369</ID>
<ANCIEN_ID>JG_L_2014_10_000000361906</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/61/43/CETATEXT000029614369.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 20/10/2014, 361906, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-10-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>361906</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE ; SCP VINCENT, OHL</AVOCATS>
<RAPPORTEUR>M. Christian Fournier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:361906.20141020</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 14 août et 18 octobre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B... A..., demeurant au... ; M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 11LY00815 du 19 juin 2012 par lequel la cour administrative d'appel de Lyon a rejeté sa requête tendant à l'annulation du jugement n°s 1000289-1000296 du 25 janvier 2011 par lequel le tribunal administratif de Clermont-Ferrand a rejeté sa demande tendant à l'annulation de l'arrêté du préfet de la Haute-Loire du 28 décembre 2009 transférant à la commune de Séneujols des biens de la section de commune des habitants du bourg de Séneujols cadastrés A1 n° 79 et 158 ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Séneujols le versement d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christian Fournier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de M. A...et à la SCP Vincent, Ohl, avocat de la commune de Séneujols ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier que, par un arrêté du 28 décembre 2009, le préfet de la Haute-Loire a prononcé au profit de la commune de Séneujols, en application de l'article L. 2411-11 du code général des collectivités territoriales, le transfert de deux parcelles de la section de commune de Séneujols cadastrées A1 n° 79 et 158 ; que la cour administrative d'appel de Lyon a, par l'arrêt attaqué du 19 juin 2012, confirmé le rejet par le tribunal administratif de Clermont-Ferrand de la demande d'annulation pour excès de pouvoir de l'arrêté préfectoral du 28 décembre 2009 ; que M. A... se pourvoit en cassation contre l'arrêt de la cour administrative d'appel de Lyon du 19 juin 2012 ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 2411-3 du code général des collectivités territoriales, dans sa rédaction alors en vigueur : " Les membres de la commission syndicale, choisis parmi les personnes éligibles au conseil municipal de la commune de rattachement, sont élus selon les mêmes règles que les conseillers municipaux des communes de moins de 2 500 habitants (...) / Les membres de la commission syndicale sont élus pour une durée égale à celle du conseil municipal (...) / Sont électeurs, lorsqu'ils sont inscrits sur les listes électorales de la commune, les habitants ayant un domicile réel et fixe sur le territoire de la section et les propriétaires de biens fonciers sis sur le territoire de la section " ; qu'aux termes de l'article L. 2411-11 du même code :  " Le transfert à la commune de tout ou partie des biens, droits et obligations d'une section est prononcé par le représentant de l'Etat dans le département sur demande conjointe du conseil municipal et de la commission syndicale se prononçant à la majorité de ses membres ou, si la commission syndicale n'a pas été constituée, sur demande conjointe du conseil municipal et de la moitié des électeurs de la section " ; <br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions que la qualité d'électeur d'une section de commune est subordonnée aux seules conditions fixées par la loi, et non à l'inscription sur une liste des électeurs de la section ; que, par suite, en jugeant que le représentant de l'Etat avait pu se fonder sur la liste des électeurs de la section concernée arrêtée par le maire de la commune de Séneujols pour s'assurer de l'existence d'une demande de transfert émanant de la moitié des électeurs de la section au motif que " la liste des électeurs pouvait valablement être établie par le maire ", la cour administrative d'appel de Lyon a commis une erreur de droit ; que, dès lors, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, M. A...est fondé à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              4. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par M. A... au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que les mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. A... qui n'est pas, dans la présente instance, la partie <br/>
perdante ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1 : L'arrêt n° 11LY00815 de la cour administrative d'appel de Lyon du 19 juin 2012 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon.<br/>
Article 3 : Les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative par M. A... et la commune de Séneujols sont rejetées.<br/>
.<br/>
Article 4 : La présente décision sera notifiée à M. B...A..., à la commune de Séneujols et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
