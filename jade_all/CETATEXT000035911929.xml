<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035911929</ID>
<ANCIEN_ID>JG_L_2017_10_000000410409</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/91/19/CETATEXT000035911929.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 25/10/2017, 410409, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-10-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>410409</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SEVAUX, MATHONNET ; HAAS</AVOCATS>
<RAPPORTEUR>M. Etienne de Lageneste</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:410409.20171025</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Grenoble de prononcer la décharge de la cotisation de taxe d'enlèvement des ordures ménagères à laquelle il a été assujetti au titre de l'année 2014 dans les rôles de la commune de Quaix-en-Chartreuse (Isère). Par un jugement n° 1406969 du 9 mars 2017, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 10 mai et 8 août 2017 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de la communauté d'agglomération Grenoble-Alpes Métropole la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Par un mémoire, enregistré le 8 août 2017 au secrétariat du contentieux du Conseil d'État, M. B... demande au Conseil d'État, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 et à l'appui de son pourvoi tendant à l'annulation du jugement n° 1406969 du 9 mars 2017 du tribunal administratif de Grenoble, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du 4 du III de l'article 1521 du code général des impôts.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - la Déclaration des droits de l'homme et du citoyen de 1789 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code général des impôts ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Etienne de Lageneste, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Haas, avocat de M. B...et à la SCP Sevaux, Mathonnet, avocat de la communauté d'agglomération Grenoble-Alpes Metropole.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              Sur la question prioritaire de constitutionnalité :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé, y compris pour la première fois en cassation, à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte de ces dispositions que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. Aux termes du 4 du III de l'article 1521 du code général des impôts : " Sauf délibération contraire des communes ou des organes délibérants de leurs groupements, les locaux situés dans la partie de la commune où ne fonctionne pas le service d'enlèvement des ordures sont exonérés de la taxe ". La taxe d'enlèvement des ordures ménagères revêt, à la différence de la redevance que l'article L. 2333-76 du code général des collectivités territoriales permet d'instituer, le caractère non d'une redevance pour service rendu mais d'une imposition de toute nature.<br/>
<br/>
              3. M. B...soutient, d'une part, que ces dispositions méconnaitraient le principe d'égalité devant la loi consacré à l'article 6 de la Déclaration des droits de l'homme et du citoyen, en ce qu'elles instituent une différence de traitement, qui ne trouve pas sa justification dans un motif d'intérêt général, entre des contribuables placés dans une situation identique au regard du service d'enlèvement des ordures ménagères et, d'autre part, qu'elles entraînent une rupture caractérisée de l'égalité devant les charges publiques contraire à l'article 13 de cette Déclaration en ce qu'elles permettent l'assujettissement à la taxe d'enlèvement des ordures ménagères de personnes qui ne bénéficient pas du service correspondant.<br/>
<br/>
              4. En premier lieu, si, en règle générale, le principe d'égalité devant la loi impose de traiter de la même façon des personnes qui se trouvent dans la même situation, il n'en résulte pas pour autant qu'il oblige à traiter différemment des personnes se trouvant dans des situations différentes. Par suite, en autorisant l'organe délibérant de la commune ou du groupement compétent en matière de collecte des ordures ménagères à supprimer, dans tout le périmètre de la collectivité en cause et pour l'ensemble des contribuables concernés, l'exonération de la taxe d'enlèvement des ordures ménagères dont jouissent, par défaut, les locaux situés dans la partie de la commune ou des communes où ne fonctionne pas le service d'enlèvement des ordures et, par suite, à traiter de manière identique l'ensemble des habitants de la commune indépendamment de leur accès à ce service, les dispositions contestées ne méconnaissent pas le principe d'égalité devant la loi garanti par l'article 6 de la Déclaration des droits de l'homme et du citoyen. <br/>
<br/>
              5. En second lieu, la taxe d'enlèvement des ordures ménagères revêt, ainsi qu'il a été dit, le caractère d'une imposition de toute nature. L'article 13 de la Déclaration des droits de l'homme et du citoyen impose seulement qu'une telle imposition soit établie en tenant compte des capacités contributives des redevables. Par suite, un redevable de cette taxe ne peut utilement se prévaloir, pour soutenir que les dispositions qui la régissent méconnaissent l'article 13 de cette Déclaration, de ce qu'elles autorisent les communes ou groupements compétents à assujettir l'ensemble des locaux situés sur leur territoire, que ceux-ci bénéficient ou non du service d'enlèvement des ordures ménagères.<br/>
<br/>
              6. Par suite, la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux au sens de l'article 23-5 de l'ordonnance du 7 novembre 1958. Ainsi, sans qu'il soit besoin de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée, le moyen tiré de ce que les dispositions du 4 du III de l'article 1521 du code général des impôts porte atteinte aux droits et libertés garantis par le Constitution doit être écarté. <br/>
<br/>
              Sur les autres moyens du pourvoi :<br/>
<br/>
              7. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
<br/>
              8. Pour demander l'annulation du jugement qu'il attaque, M. B...soutient que le tribunal administratif de Grenoble a :<br/>
              - omis de statuer sur ses conclusions tendant à l'annulation de la décision de refus de Grenoble-Alpes Métropole de créer un point de collecte à 200 mètres au plus de son habitation ou de pratiquer la collecte en porte à porte et à ce que ce tribunal ordonne à la communauté d'agglomération de la métropole de mettre en oeuvre les dispositions des articles L. 2224-13 à L. 2224-17, de l'article R. 2224-3 et des I à IV de l'article R. 2224-24 du code général des collectivités territoriales ainsi que l'arrêté préfectoral n° 78-1368 du 7 février 1978 ;<br/>
              - commis une erreur de droit en ne relevant pas d'office l'incompétence de la communauté d'agglomération Grenoble-Alpes Métropole pour rejeter la réclamation préalable qu'il avait formée à l'encontre des cotisations de taxe d'enlèvement des ordures ménagères litigieuses.<br/>
<br/>
              9. Aucun de ces moyens n'est de nature à permettre l'admission du pourvoi.<br/>
<br/>
<br/>
<br/>                          D E C I D E :<br/>
                                        --------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M.B....<br/>
<br/>
Article 2 : Le pourvoi de M. B...n'est pas admis.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. A...B....<br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre, au ministre de l'action et des comptes publics et à la communauté d'agglomération Grenoble-Alpes-Métropole.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
