<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042614255</ID>
<ANCIEN_ID>JG_L_2020_12_000000446666</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/61/42/CETATEXT000042614255.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 03/12/2020, 446666, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>446666</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:446666.20201203</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 19 novembre 2020 au secrétariat du contentieux du Conseil d'Etat, Mme J... B..., Mme C... D..., Mme F... H..., l'association Liberté Environnement Bretagne, Mme A... I... et Mme G... E... demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative : <br/>
<br/>
              1°) d'ordonner la suspension de l'exécution de la décision n° 2020-0329 de l'Autorité de régulation des communications électroniques, des postes et de la distribution de la presse (ARCEP) en date du 31 mars 2020, relative au compte-rendu de l'instruction des dossiers de candidatures reçues et aux résultats de la phase d'attribution des blocs de 50 MHz, dans le cadre de la procédure d'attribution d'autorisations d'utilisation de fréquences dans la bande 3,4 - 3,8 GHz en France métropolitaine pour établir et exploiter un réseau radioélectrique mobile ouvert au public ; <br/>
<br/>
              2°) d'ordonner la suspension de l'exécution de la décision n° 2020-1160 de l'ARCEP en date du 20 octobre 2020, relative au compte-rendu et au résultat de la procédure d'attribution d'autorisations d'utilisation de fréquences dans la bande 3,4 - 3,8 GHz en France métropolitaine pour établir et exploiter un réseau radioélectrique mobile ouvert au public ; <br/>
<br/>
              3°) d'annuler, en conséquence de la nullité des décisions n° 2020-0329 et n° 2020-1160, les décisions n° 2020-1254, n° 2020-1255, n° 2020-1256 et n° 2020-1257 autorisant respectivement chaque lauréat de la procédure à utiliser les fréquences pour défaut de base légale ;<br/>
<br/>
              4°) à titre subsidiaire, d'enjoindre à l'ARCEP de communiquer les décisions, procès-verbaux et rapports relatifs à l'attribution des fréquences pour la 5G suite aux enchères avec une astreinte provisoire d'un montant de 500 euros par jour de retard ;<br/>
<br/>
              5°) de mettre à la charge de l'Etat une somme de 1 euro au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Ils soutiennent que : <br/>
              - leur requête est recevable ; <br/>
              - ils justifient d'un intérêt leur donnant qualité pour agir ; <br/>
              - la condition d'urgence est satisfaite eu égard à la gravité et à l'immédiateté de l'atteinte portée sur la santé ;<br/>
              - il existe un doute sérieux quant à la légalité des décisions contestées : <br/>
              - les décisions contestées sont entachées d'un vice de compétence dès lors, en premier lieu, que l'ARCEP a méconnu l'étendue de ses compétences en ce qu'elle n'a pas pris en compte le principe de précaution entrant dans ses attributions légales, en deuxième lieu, qu'elle outrepasse ses prérogatives en intervenant dans la définition des prix pour l'achat des fréquences, en troisième lieu, qu'il n'est pas démontré qu'elles aient été prises par la formation plénière de l'ARCEP ;  <br/>
              - elles sont entachées d'un vice de procédure en ce qu'elles méconnaissent l'article L. 32-1 du code des postes et des communications électroniques ; <br/>
              - elles méconnaissent les principes de sécurité juridique et de confiance légitime ainsi que les divers engagements de la France auprès, d'une part, de la Convention Citoyenne pour le Climat concernant le moratoire sur le déploiement de la 5G et, d'autre part, des instances internationales et européennes sur la réduction des émissions de gaz à effet de serre ; <br/>
              - elles méconnaissent le principe de la concurrence effective et loyale entre les candidats dès lors que les autorisations d'occupation du domaine public pourront être prolongées de cinq années en l'absence de toute mise en concurrence ; <br/>
              - elles sont entachées d'une erreur d'appréciation dès lors que, en premier lieu, l'ARCEP a limité l'effet de la méthode des enchères dans le cadre de la 5G et atténué les obligations des opérateurs qu'ils avaient prises lors de l'attribution des fréquences 4G, en deuxième lieu, qu'elle a autorisé les poursuites des enchères en septembre 2020 et, en dernier lieu, que le lancement de la procédure d'attribution s'est fait sans les études nécessaires en amont ni les modalités de surveillance une fois la technologie déployée ; <br/>
              - elles sont entachées d'illégalité dès lors qu'elles procèdent d'un détournement de pouvoirs.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              la Constitution ; <br/>
              la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la convention cadre des Nations Unies sur les changements climatiques du 9 mai 1992 et son protocole signé à Kyoto le 11 décembre 1997 ;<br/>
              - l'accord de Paris, adopté le 12 décembre 2015 ;<br/>
              - la décision 94/69/CE du Conseil du 15 décembre 1993 ;<br/>
              - la décision 406/2009/CE du Parlement Européen et du Conseil du 23 avril 2009 ;<br/>
              - la directive 2009/28/CE du Parlement européen et du Conseil du 23 avril ;<br/>
              - la directive 2012/27/UE du Parlement européen et du Conseil du 25 octobre ;<br/>
              - le règlement (UE) 2018/842 du Parlement européen et du Conseil du 30 mai ;<br/>
              - le code de l'énergie ;<br/>
              - le code de l'environnement ;<br/>
              - la loi n° 2009-967 du 3 aout 2009 ;<br/>
              - la loi n° 2015-992 du 17 aout 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              Par deux décisions 2020 - 03 29, et 2020 - 1160, du 31 mars et du 20 octobre 2020, l'ARCEP a rendu compte de la procédure d'instruction et d'attribution d'autorisations d'utilisation de fréquences, par blocs de 50 MHz puis de manière définitive, dans la bande de 3,4 - 3,8 GHz. Mme B..., et d'autres personnes physiques et morales, demandent au juge des référés du conseil d'Etat, sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de ces deux décisions, et d'annuler par voie de conséquence les décisions autorisant l'utilisation des fréquences aux opérateurs concernés, ainsi que d'enjoindre à l'ARCEP sous astreinte de leur communiquer divers documents.<br/>
<br/>
              Sur les conclusions à fin de suspension :<br/>
<br/>
              Pour établir l'urgence qui s'attache, selon eux, à la suspension demandée, les requérants se prévalent de ce que le déploiement des réseaux dits de cinquième génération augmenterait les émissions de gaz à effet de serre et des atteintes à la santé qui résulteraient du déploiement de ces réseaux. Toutefois, les décisions prises par l'ARCEP ne peuvent recevoir qu'une exécution progressive, au fur et à mesure de la construction des infrastructures nécessaires et de leur mise en service. La requête n'allègue même pas que ce déploiement aurait commencé, se bornant à indiquer que la commercialisation en aurait débuté, qui par elle-même n'a aucune incidence en termes de consommation énergétique ou de santé. La requête, qui se borne à des considérations générales, n'apporte aucun élément précis qui permettrait d'établir que dans l'intervalle de temps raisonnable qui s'écoulera avant que le juge du fond puisse normalement examiner les requêtes, des conséquences significatives pourraient découler du début d'exécution des actes attaqués, qui pourraient être regardées comme créant une urgence justifiant leur suspension. Dès lors, à supposer même que les qualités dont se prévalent les requérants leur donnent intérêt pour agir contre les décisions en cause, leur requête ne peut qu'être rejetée, faute que la condition d'urgence posée par l'article L. 521-1 soit satisfaite.<br/>
              Sur les autres conclusions :<br/>
<br/>
               En application des décisions dont suspension est demandée, par quatre décisions distinctes, l'ARCEP a autorisé les lauréats à utiliser les fréquences ainsi attribuées. Les requérants demandent l'annulation de ces décisions par voie de conséquence de la suspension qu'ils demandent. De telles conclusions sont irrecevables devant le juge des référés et n'auraient, en tout état de cause, pu qu'être rejetées par voie de conséquence du rejet de la demande de suspension.<br/>
<br/>
              Les requérants demandent également qu'il soit enjoint à l'ARCEP de communiquer les décisions, procès-verbaux et rapports relatifs à l'attribution des fréquences considérées, sous astreinte de 500 euros par jour de retard. Ces conclusions qui en tout état de cause n'auraient pu aussi qu'être rejetées par voie de conséquence, sont irrecevable devant le juge des référés statuant sur le fondement de l'article L. 521-1.<br/>
<br/>
              Il résulte de tout ce qui précède que la requête de Madame B... et autres doit être rejetée, y compris en tant qu'elle demande que l'ARCEP leur verse la somme de 1 euro sur le fondement de l'article L. 761-1 du code de justice administrative dont, dès lors que l'ARCEP n'est pas la partie perdante, les dispositions font obstacle à ce qu'il y soit fait droit.<br/>
<br/>
<br/>
<br/>			O R D O N N E :<br/>
              			------------------<br/>
<br/>
Article 1er : La requête de Mme B... et autres est rejetée. <br/>
Article 2 : La présente ordonnance sera notifiée à Mme J... B..., premier requérant dénommé.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
