<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031355830</ID>
<ANCIEN_ID>JG_L_2015_10_000000370858</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/35/58/CETATEXT000031355830.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème SSJS, 22/10/2015, 370858, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-10-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>370858</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>M. Guillaume Déderen</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:370858.20151022</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 5 août et 6 novembre 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société anonyme (SA) Réserve africaine de Sigean, dont le siège est RN 9 à Sigean (11130), représentée par son président directeur général en exercice ; la société Réserve africaine de Sigean demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09MA00885 et 10MA00301 du 3 juin 2013 par lequel la cour administrative d'appel de Marseille a annulé, à la demande du syndicat intercommunal pour l'aménagement hydraulique (SIAH) du bassin de la Berre et du Rieu, d'une part, le jugement avant dire droit n° 0604622 du 30 décembre 2008 par lequel le tribunal administratif de Montpellier a déclaré ce syndicat responsable de l'aggravation des dommages subis par la société lors des inondations des 3 décembre 2003, 15 novembre 2005 et des 29 et 30 janvier 2006 et a ordonné une expertise visant à évaluer les préjudices qui ont été consécutifs, d'autre part, le jugement n° 0604622 du 1er décembre 2009 par lequel ce même tribunal a condamné ce syndicat à lui verser la somme de 302 563,07 euros et mis à sa charge les frais d'expertise, a rejeté la demande de première instance présentée par la société Réserve africaine de Sigean et a mis à la charge de cette société les frais d'expertise ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge du SIAH du bassin de la Berre et du Rieu la somme de 8 000 euros au titre de l'article L. 761-1 du code de justice administrative, ainsi que la somme de 35 euros au titre de la contribution à l'aide juridique prévue à l'article R. 761-1 du même code ; <br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des collectivités territoriales ; <br/>
              - le code rural et de la pêche maritime ; <br/>
              - le code de l'environnement ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Déderen, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de la Réserve africaine de Sigean et à la SCP Didier, Pinet, avocat du syndicat intercommunal pour l'aménagement hydraulique du bassin de la Berre et du Rieu ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des termes mêmes du jugement du 30 décembre 2008 que le tribunal administratif de Montpellier avait retenu la responsabilité du syndicat intercommunal pour l'aménagement hydraulique du bassin de la Berre et du Rieu dans les désordres subis par la Réserve africaine de Sigean à raison non pas des conditions d'exécution de ces travaux mais de la carence du syndicat dans l'entretien du cours et des digues de la Berre ; qu'en se fondant, pour annuler le jugement attaqué, sur le motif tiré de ce qu'il n'existait, contrairement à ce qu'a jugé le tribunal administratif, aucun lien de causalité entre l'exécution par le syndicat intercommunal pour l'aménagement hydraulique des travaux de curage, de recalibrage et de reprofilage de la Berre visant à améliorer le libre écoulement des eaux, à réguler et à accélérer son débit afin de prévenir le retour d'inondations et les dommages subis par la Réserve africaine de Sigean, la cour administrative d'appel de Marseille a dénaturé les termes de ce jugement ; qu'il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que la requérante est fondée à demander l'annulation de l'arrêt qu'elle attaque ; <br/>
<br/>
              2. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de la société Réserve africaine de Sigean, qui n'est pas dans la présente instance la partie perdante, la somme que demande le syndicat intercommunal pour l'aménagement hydraulique du bassin de la Berre et du Rieu au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'il y a lieu en revanche, dans les circonstances de l'espèce, de mettre à la charge de ce syndicat une somme globale de 3 000 euros qui sera versée à la société Réserve africaine de Sigean au titre de ces dispositions et de celles de l'article R. 761-1 du même code ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 3 juin 2013 est annulé. <br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille. <br/>
Article 3 : Le syndicat intercommunal pour l'aménagement hydraulique du bassin de la Berre et du Rieu versera une somme globale de 3 000 euros à la Réserve africaine de Sigean au titre des dispositions des articles L. 761-1 et R. 761-1 du code de justice administrative. <br/>
Article 4 : Les conclusions présentées par le syndicat intercommunal pour l'aménagement hydraulique du bassin de la Berre et du Rieu au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la Réserve africaine de Sigean et au syndicat intercommunal pour l'aménagement hydraulique du bassin de la Berre et du Rieu. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
