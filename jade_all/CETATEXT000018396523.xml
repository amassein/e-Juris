<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000018396523</ID>
<ANCIEN_ID/>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/18/39/65/CETATEXT000018396523.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 21/03/2008, 279074, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2008-03-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>279074</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Delarue</PRESIDENT>
<AVOCATS>FOUSSARD ; SCP VIER, BARTHELEMY, MATUCHANSKY</AVOCATS>
<RAPPORTEUR>Mme Laure  Bédier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mlle Courrèges Anne</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 29 mars et 29 juillet 2005 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SOCIETE TERRES ET DEMEURES, dont le siège est 93, rue de Maubeuge à Paris (75010) ; la SOCIETE TERRES ET DEMEURES demande au Conseil d'Etat :
              
              1°) d'annuler l'arrêt du 27 janvier 2005 par lequel la cour administrative d'appel de Marseille a rejeté sa requête tendant à l'annulation du jugement du 1er février 2001 du tribunal administratif de Marseille ayant rejeté sa demande tendant à la condamnation du département des Bouches-du-Rhône à lui verser la somme de 5 756 328 F (877 546 euros), avec intérêts au taux légal à compter du 18 juillet 1997, en réparation du préjudice résultant des délibérations des 1er mars 1991 et 26 juin 1992 par lesquelles le conseil général des Bouches&#143;du&#143;Rhône a fait usage de son droit de préemption, et la somme de 1 496 796 F (228 185 euros) au titre d'un redressement fiscal corrélatif ;
              
              2°) réglant l'affaire au fond, de faire droit à son appel ;
              
              3°) de mettre à la charge du département des Bouches-du-Rhône le versement de la somme de 3 600 euros au titre de l'article L. 761&#143;1 du code de justice administrative ;
     
	
              Vu les autres pièces du dossier ;
              
              Vu le code de l'urbanisme ;
              
              Vu le code de justice administrative ;

     
              Après avoir entendu en séance publique :
              
              - le rapport de Mme Laure Bédier, Maître des Requêtes,  
              
              - les observations de la SCP Vier, Barthélemy, Matuchansky, avocat de la SOCIETE TERRES ET DEMEURES et de Me Foussard, avocat du département des Bouches&#143;du-Rhône, 
              
              - les conclusions de Mlle Anne Courrèges, Commissaire du gouvernement ;
              
              
              
     
     <br/>Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par deux décisions successives des 12 février 1991 et 8 juillet 1992, le département des Bouches&#143;du-Rhône a fait usage du droit de préemption qu'il tient des dispositions des articles L. 142&#143;1 et L. 142&#143;3 du code de l'urbanisme en vue d'acquérir la propriété dénommée « La Pauline » située à Aix&#143;en&#143;Provence ; que ces décisions n'ont cependant pas abouti, le prix proposé étant inférieur à celui fixé dans la déclaration d'intention d'aliéner et la SOCIETE TERRES ET DEMEURES, propriétaire, ayant à chaque fois renoncé à la vente ; que, par lettre du 15 novembre 1993, le département a informé la société qu'il renonçait à exercer ce droit sur ce bien, finalement revendu en 1995 par la SOCIETE TERRES ET DEMEURES à un prix inférieur à celui figurant dans la première déclaration d'intention d'aliéner ; que la SOCIETE TERRES ET DEMEURES a sollicité l'indemnisation des préjudices qu'elle estime avoir subis du fait des décisions de préemption ; que, par un jugement en date du 1er février 2000, le tribunal administratif de Marseille a rejeté sa demande ; que, saisie en appel, la cour administrative d'appel de Marseille a confirmé ce jugement par l'arrêt attaqué du 27 janvier 2005 ;
              
              Considérant qu'aux termes de L. 142&#143;1 du code de l'urbanisme, dans sa rédaction alors en vigueur : « Afin de préserver la qualité des sites, des paysages et des milieux naturels, et selon les principes posés à l'article L. 110, le département est compétent pour élaborer et mettre en oeuvre une politique de protection, de gestion et d'ouverture au public des espaces naturels sensibles, boisés ou non. (&#133;) » ; que, pour la mise en oeuvre de cette politique, le conseil général peut, en application de l'article L. 142&#143;3 du même code, créer des zones de préemption ; que le droit de préemption dont, sur le fondement de cet article, le département dispose à l'intérieur de ces zones, porte sur tout terrain ou ensemble de droits sociaux donnant vocation à l'attribution en propriété ou en jouissance de terrains qui font l'objet d'une aliénation volontaire, à titre onéreux, sous quelque forme que ce soit ; qu'un terrain comportant une construction peut, « à titre exceptionnel », donner lieu à l'exercice du droit de préemption à condition que le terrain soit de dimension suffisante pour justifier son ouverture au public et qu'il soit, par sa localisation, nécessaire à la mise en oeuvre de la politique des espaces naturels sensibles du département ; que, dans le cas où la construction acquise est conservée, elle est affectée à un usage permettant la fréquentation du public et la connaissance des milieux naturels ;
              
              Considérant, en premier lieu, d'une part, qu'il ressort des pièces du dossier soumis aux juges du fond que la propriété « La Pauline » est incluse dans la zone de préemption délimitée par l'arrêté du 29 décembre 1982 du préfet des Bouches&#143;du&#143;Rhône, alors compétent, au profit du département des Bouches&#143;du&#143;Rhône au titre de la protection des espaces naturels sensibles, et que la légalité de cet arrêté n'est pas contestée par la requérante ; que, d'autre part, ainsi qu'il a été dit ci-dessus, le dernier alinéa de l'article L. 142&#143;3 du code de l'urbanisme permet, sous certaines conditions, au département d'exercer son droit de préemption sur un terrain comportant une construction ; qu'en l'espèce, en jugeant qu'eu égard à ses caractéristiques, à sa localisation et à ses dimensions, la préemption de cette propriété était nécessaire à la mise en oeuvre de la politique des espaces naturels sensibles du département des Bouches-du-Rhône, la cour administrative d'appel de Marseille a porté sur les pièces du dossier qui lui était soumis une appréciation souveraine qui est exempte de dénaturation ; qu'ainsi, les moyens tirés de ce que la cour aurait commis des erreurs de droit et entaché son appréciation de dénaturation en jugeant que les décisions de préemption litigieuses n'ont pas méconnu les dispositions des articles L. 142&#143;1 et L. 142&#143;3 du code de l'urbanisme doivent être écartés ;
              
              Considérant, en second lieu, que si toute illégalité qui entache une décision de préemption constitue en principe une faute de nature à engager la responsabilité de la collectivité au nom de laquelle cette décision a été prise, une telle faute ne peut donner lieu à la réparation du préjudice subi par le vendeur ou l'acquéreur évincé lorsque, les circonstances de l'espèce étant de nature à justifier légalement la décision de préemption, le préjudice allégué ne peut être regardé comme la conséquence du vice dont cette décision est entachée ; qu'ainsi, en jugeant que les décisions prises par le département dans le cadre de l'exercice de son droit de préemption étant justifiées légalement, l'illégalité formelle de ces décisions tenant à leur absence de motivation n'était pas de nature, en l'espèce, à ouvrir droit à réparation du préjudice invoqué au profit du vendeur, la cour administrative d'appel de Marseille n'a pas commis d'erreur de droit et a, sur ce point, suffisamment motivé son arrêt ;
              
              Considérant qu'il résulte de ce qui précède que les conclusions de la SOCIETE TERRES ET DEMEURES tendant à l'annulation de l'arrêt de la cour administrative d'appel de Marseille doivent être rejetées ; que doivent être rejetées, par voie de conséquence, ses conclusions présentées au titre de l'article L. 761&#143;1 du code de justice administrative ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à sa charge, sur le fondement de ces dispositions, le versement d'une somme au département des Bouches-du-Rhône ;
              
     
     <br/>D E C I D E :
--------------
Article 1er : La requête de la SOCIETE TERRES ET DEMEURES est rejetée.
Article 2 : Les conclusions du département des Bouches-du-Rhône au titre de l'article L. 761&#143;1 du code de justice administrative sont rejetées.
Article 3 : La présente décision sera notifiée à la SOCIETE TERRES ET DEMEURES et au département des Bouches-du-Rhône.
                 
                 <br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
