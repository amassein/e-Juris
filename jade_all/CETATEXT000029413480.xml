<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029413480</ID>
<ANCIEN_ID>JG_L_2014_08_000000366168</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/41/34/CETATEXT000029413480.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère sous-section jugeant seule, 27/08/2014, 366168, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-08-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>366168</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère sous-section jugeant seule</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE ; SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>M. Denis Rapone</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:366168.20140827</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Paris, d'une part, d'annuler la décision du 3 décembre 2010 par laquelle le directeur général de la Caisse des dépôts et consignations, agissant en qualité de gestionnaire de la Caisse nationale de retraite des agents des collectivités locales, a rejeté la demande de révision de sa pension de retraite et, d'autre part, d'enjoindre à la Caisse nationale de retraite des agents des collectivités locales de calculer ses droits à pension sur la base du dernier traitement dans son grade d'origine à l'imprimerie municipale de la ville de Paris, revalorisé en fonction de l'augmentation du barème des salaires de la presse quotidienne parisienne.<br/>
<br/>
              Par un jugement n° 1101679 du 20 décembre 2012, le tribunal administratif de Paris a rejeté la demande de M.B....<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 19 février et 14 mai 2013 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement n° 1101679 du tribunal administratif de Paris du 20 décembre 2012 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la Caisse nationale de retraite des agents des collectivités locales la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative, ainsi que les dépens.<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le décret n° 94-415 du 24 mai 1994 ;<br/>
              - le décret n° 2003-1306 du 26 décembre 2003 ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Denis Rapone, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de M.B..., et à la SCP Odent, Poulet, avocat de la Caisse des dépôts et consignations.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. B..., fonctionnaire des administrations parisiennes titularisé le 1er avril 1976 en qualité de mécanicien de conduite adjoint au sein du service de l'imprimerie municipale, a été détaché, après la suppression de ce service en 1985, sur des emplois contractuels de surveillant, d'agent de maîtrise puis d'agent supérieur d'exploitation de la direction de la voirie et des déplacements de la ville de Paris. Pour garantir à M. B...un niveau de rémunération mensuelle équivalent à celui détenu dans son grade d'origine, revalorisé en fonction de l'augmentation du barème des salaires de la presse quotidienne parisienne, la rémunération afférente à ses emplois de détachement était complétée par une " indemnité de reclassement ". Par un arrêté du 16 mars 2010, M. B...a été admis par le maire de Paris à faire valoir ses droits à la retraite à compter du 10 juillet 2010. Par une lettre du 3 décembre 2010, le directeur général de la Caisse des dépôts et consignations, agissant en qualité de gestionnaire de la Caisse nationale de retraite des agents des collectivités locales, a rejeté la demande de la ville de Paris tendant à ce que la pension de M. B... soit liquidée sur la base de la rémunération correspondant à son grade d'origine et a décidé de liquider sa pension sur la base de la seule rémunération afférente à son dernier emploi de détachement. Par un jugement du 20 décembre 2012 contre lequel M. B... se pourvoit en cassation, le tribunal administratif de Paris a rejeté sa demande d'annulation de cette décision.<br/>
<br/>
              2. Aux termes du premier alinéa de l'article 64 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale : " Le détachement est la position du fonctionnaire placé hors de son cadre d'emploi, emploi ou corps d'origine mais continuant à bénéficier, dans ce corps, de ses droits à l'avancement et à la retraite ". Aux termes de l'article 65 de la même loi, dans sa rédaction applicable au litige : " Le fonctionnaire détaché ne peut, sauf dans le cas où le détachement a été prononcé dans une administration ou un organisme implanté sur le territoire d'un Etat étranger ou auprès d'organismes internationaux ou pour exercer une fonction publique élective, être affilié au régime de retraite dont relève la fonction de détachement, ni acquérir, à ce titre, des droits quelconques à pensions ou allocations, sous peine de la suspension de la pension de la Caisse nationale de retraite des agents des collectivités locales. / (...) Il reste tributaire de la Caisse nationale de retraite des agents des collectivités locales et effectue les versements fixés par le règlement de cette caisse sur le traitement afférent à son grade et à son échelon dans le service dont il est détaché. / Dans le cas ou le fonctionnaire est détaché dans un emploi conduisant à pension du régime de retraite des fonctionnaires affiliés à la Caisse nationale de retraite des agents des collectivités locales ou relevant du code des pensions civiles et militaires de retraite, la retenue pour pension est calculée sur le traitement afférent à l'emploi de détachement (...) ". <br/>
<br/>
              3. Il résulte de ces dispositions que la constitution des droits à pension d'un agent titulaire de la fonction publique territoriale détaché sur un emploi d'agent contractuel, relevant du régime général, est régie par les dispositions applicables à son corps ou cadre d'emplois d'origine et que sa pension doit être liquidée sur la base du traitement afférent à l'emploi ou au grade détenu dans ce corps ou cadre d'emplois et non sur la base de la rémunération afférente à l'emploi d'agent contractuel occupé en détachement. Par suite, en jugeant que la pension de M.B..., fonctionnaire des administrations parisiennes affilié à la Caisse nationale de retraite des agents des collectivités locales, devait être liquidée sur la base de la rémunération afférente à son dernier emploi contractuel de détachement en qualité d'agent supérieur d'exploitation de la direction de la voirie et des déplacements, le tribunal administratif a commis une erreur de droit.<br/>
<br/>
              4. Il résulte de ce qui précède que M. B...est fondé à demander l'annulation du jugement qu'il attaque. Le moyen tiré de l'erreur de droit ainsi commise suffisant à entraîner cette annulation, il n'est pas nécessaire d'examiner les autres moyens du pourvoi. <br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la Caisse des dépôts et consignations le versement à M. B...d'une somme globale de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative et de l'article R. 761-1 du même code dans sa rédaction en vigueur à la date d'introduction du pourvoi.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Paris du 20 décembre 2012 est annulé.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Paris.<br/>
Article 3 : La Caisse des dépôts et consignations versera une somme de 3 000 euros à M. B... au titre des articles L. 761-1 et R. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. A...B...et à la Caisse des dépôts et consignations.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
