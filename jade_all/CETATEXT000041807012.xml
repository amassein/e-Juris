<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041807012</ID>
<ANCIEN_ID>JG_L_2020_04_000000439899</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/80/70/CETATEXT000041807012.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 14/04/2020, 439899, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-04-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>439899</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:439899.20200414</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au juge des référés du tribunal administratif de Melun, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre à l'administration du centre pénitentiaire de Fresnes, d'une part, de lui fournir un masque, une paire de gants ainsi qu'une solution hydroalcoolique et, d'autre part, de prendre toute mesure nécessaire à assurer sa protection, notamment en faisant respecter une distance sanitaire entre co-détenus. Par une ordonnance n° 2002735 du 30 mars 2020, le juge des référés du tribunal administratif de Melun a rejeté sa demande. <br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 1er et 10 avril 2020 au secrétariat du contentieux du Conseil d'Etat, M. B... A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
              1°) à titre principal, d'annuler l'ordonnance du 30 mars 2020 et de faire droit à ses conclusions de première instance ;<br/>
<br/>
              2°) à titre subsidiaire, d'ordonner toutes mesures d'instruction qu'il estimera utiles ainsi que la désignation d'un expert médical afin d'examiner, le cas échéant en se transportant sur les lieux, d'une part, la compatibilité de son état de santé avec les règles sanitaires mises en place par le centre pénitentiaire de Fresnes et, d'autre part, la conformité de ces règles sanitaires aux directives imposées dans le cadre de la lutte contre le Covid-19. <br/>
<br/>
<br/>
              Il soutient que :<br/>
              - la condition d'urgence est remplie eu égard, en premier lieu, à l'apparition et à l'augmentation du nombre de cas de covid-19 au sein de la population carcérale du centre pénitentiaire de Fresnes, en deuxième lieu, à l'absence, faute d'une généralisation des tests de dépistage, de stratégie thérapeutique adéquate de protection des personnes détenues, en troisième lieu, aux conséquences de la décision de suspension des activités et des promenades, en quatrième lieu, à l'insuffisance des mesures de régulation carcérale et, en dernier lieu, à son impossibilité d'établir qu'il souffre de pathologie respiratoire dès lors que son dossier médical est retenu par les services pénitentiaires de Fresnes ;<br/>
              - l'insuffisance des mesures prises par le centre pénitentiaire pour assurer la protection des personnes détenues porte une atteinte grave et manifestement illégale au droit à la vie rappelé notamment par l'article 2 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales.  <br/>
<br/>
              Par un mémoire en défense et deux nouveaux mémoires, enregistrés les 6 et 10 avril 2020, la garde des sceaux, ministre de la justice, conclut au rejet de la requête. Elle soutient que la condition d'urgence n'est pas remplie et qu'il n'est porté aucune atteinte grave et manifestement illégale à une liberté fondamentale.<br/>
              Par une intervention, enregistrée le 7 avril 2020, l'association Robin des lois déclare intervenir au soutien de la requête.<br/>
<br/>
              Par un mémoire, enregistré le 10 avril 2020, le ministre des solidarités et de la santé a produit des observations.<br/>
<br/>
              La requête a été communiquée au Premier ministre qui n'a pas produit d'observations.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ; <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de la santé publique ;<br/>
              - le code de procédure pénale ;<br/>
              - la loi n° 2009-1436 du 24 novembre 2009 pénitentiaire ;<br/>
              - la loi n° 2020-290 du 23 mars 2020 ;<br/>
              - l'ordonnance n° 2020-303 du 25 mars 2020 ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
              Les parties ont été informées sur le fondement de l'article 9 de l'ordonnance du 25 mars 2020 portant adaptation des règles applicables devant les juridictions de l'ordre administratif, de ce qu'aucune audience ne se tiendrait et de ce que la clôture de l'instruction serait fixée le 11 avril 2020 à 17 heures.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. L'article L. 511-1 du code de justice administrative dispose que : " Le juge des référés statue par des mesures qui présentent un caractère provisoire. Il n'est pas saisi du principal et se prononce dans les meilleurs délais. " Aux termes de l'article L. 521-2 du même code : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures. ". <br/>
              Sur l'intervention :<br/>
<br/>
              2. L'association " Robin des lois " justifie d'un intérêt suffisant pour intervenir au soutien de la requête de M. A.... Son intervention est, par suite, recevable.<br/>
<br/>
              Sur les circonstances : <br/>
<br/>
              3. L'émergence d'un nouveau coronavirus (covid-19), de caractère pathogène et particulièrement contagieux et sa propagation sur le territoire français ont conduit le ministre des solidarités et de la santé à prendre, par plusieurs arrêtés à compter du 4 mars 2020, des mesures sur le fondement des dispositions de l'article L. 3131-1 du code de la santé publique. En particulier, par un arrêté du 14 mars 2020, un grand nombre d'établissements recevant du public ont été fermés au public, les rassemblements de plus de 100 personnes ont été interdits et l'accueil des enfants, élèves et étudiants dans les établissements les recevant et les établissements scolaires et universitaires a été suspendu. Puis, par un décret du 16 mars 2020 motivé par les circonstances exceptionnelles découlant de l'épidémie de covid-19, modifié par décret du 19 mars, le Premier ministre a interdit le déplacement de toute personne hors de son domicile, sous réserve d'exceptions limitativement énumérées et devant être dûment justifiées, à compter du 17 mars à 12h, sans préjudice de mesures plus strictes susceptibles d'être ordonnées par le représentant de l'Etat dans le département. Le ministre des solidarités et de la santé a pris des mesures complémentaires par des plusieurs arrêtés successifs. <br/>
<br/>
              4. Par l'article 4 de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19, a été déclaré l'état d'urgence sanitaire pour une durée de deux mois sur l'ensemble du territoire national. Par un nouveau décret du 23 mars 2020 pris sur le fondement de l'article L. 3131-15 du code de la santé publique issu de la loi du 23 mars 2020, plusieurs fois modifié et complété depuis lors, le Premier ministre a réitéré les mesures précédemment ordonnées tout en leur apportant des précisions ou restrictions complémentaires. Leurs effets ont été prolongés par décret du 27 mars 2020.<br/>
<br/>
              Sur le cadre juridique du litige, l'office du juge des référés et la liberté fondamentale en jeu :<br/>
<br/>
              5. Dans l'actuelle période d'état d'urgence sanitaire, il appartient aux différentes autorités compétentes, en particulier au Premier ministre, de prendre, en vue de sauvegarder la santé de la population, toutes dispositions de nature à prévenir ou à limiter les effets de l'épidémie. Ces mesures, qui peuvent limiter l'exercice des droits et libertés fondamentaux doivent, dans cette mesure, être nécessaires, adaptées et proportionnées à l'objectif de sauvegarde de la santé publique qu'elles poursuivent.<br/>
<br/>
              6. Il résulte de la combinaison des dispositions des articles L. 511-1 et L. 521-2 du code de justice administrative qu'il appartient au juge des référés, lorsqu'il est saisi sur le fondement de l'article L. 521-2 et qu'il constate une atteinte grave et manifestement illégale portée par une personne morale de droit public à une liberté fondamentale, résultant de l'action ou de la carence de cette personne publique, de prescrire les mesures qui sont de nature à faire disparaître les effets de cette atteinte, dès lors qu'existe une situation d'urgence caractérisée justifiant le prononcé de mesures de sauvegarde à très bref délai. Ces mesures doivent, en principe, présenter un caractère provisoire, sauf lorsque aucune mesure de cette nature n'est susceptible de sauvegarder l'exercice effectif de la liberté fondamentale à laquelle il est porté atteinte. Sur le fondement de l'article L. 521-2, le juge des référés peut ordonner à l'autorité compétente de prendre, à titre provisoire, des mesures d'organisation des services placés sous son autorité, dès lors qu'il s'agit de mesures d'urgence qui lui apparaissent nécessaires pour sauvegarder, à très bref délai, la liberté fondamentale à laquelle il est gravement, et de façon manifestement illégale, porté atteinte. Le caractère manifestement illégal de l'atteinte doit s'apprécier notamment en tenant compte des moyens dont dispose l'autorité administrative compétente et des mesures qu'elle a déjà prises. <br/>
<br/>
              7. Pour l'application de l'article L. 521-2 du code de justice administrative, le droit au respect de la vie constitue une liberté fondamentale au sens des dispositions de cet article.<br/>
<br/>
              8. Eu égard à la vulnérabilité des détenus et à leur situation d'entière dépendance vis à vis de l'administration, il appartient à celle-ci, et notamment au garde des sceaux, ministre de la justice et aux directeurs des établissements pénitentiaires, en leur qualité de chefs de service, de prendre les mesures propres à protéger leur vie afin de garantir le respect effectif de la liberté fondamentale énoncé au point précédent. Lorsque la carence de l'autorité publique crée un danger caractérisé et imminent pour la vie des personnes portant ainsi une atteinte grave et manifestement illégale à cette liberté fondamentale, et que la situation permet de prendre utilement des mesures de sauvegarde dans un délai de quarante-huit heures, le juge des référés peut, au titre de la procédure particulière prévue par l'article L. 521-2, prescrire, dans les conditions et les limites définies au point 6, les mesures de nature à faire cesser la situation résultant de cette carence. <br/>
              Sur la demande en référé :  <br/>
<br/>
              9. M. Acentre pénitentiaire de Fresnes depuis le 14 juin 2019, a saisi, le 25 mars 2020, le juge des référés du tribunal administratif de Melun, sur le fondement de l'article L. 521-2 du code de justice administrative, d'une demande tendant à ce qu'il soit enjoint au chef d'établissement du centre pénitentiaire de Fresnes de lui fournir, en quantité suffisante, des masques de protection, gants et gels hydro-alcoolique et de prendre toute mesure nécessaire pour assurer le respect effectif des règles de distance minimale entre les personnes détenues, ressortissant chinois né en 1954, détenu au .... Il relève appel de l'ordonnance du 30 mars 2020 par laquelle le juge des référés a rejeté cette demande au motif qu'aucune atteinte grave et manifestement illégale n'était portée au droit au respect de la vie sans se prononcer sur la condition d'urgence.<br/>
<br/>
              10. Il résulte de l'instruction que, depuis que l'épidémie de covid-19 a atteint la France et au fur et à mesure de l'évolution des stades 1, 2 et 3 de l'épidémie, la ministre de la justice a édicté, au moyen de plusieurs instructions adressées aux services compétents, un certain nombre de mesures visant à prévenir le risque de propagation du virus au sein des établissements pénitentiaires. Ces instructions définissent des orientations générales et arrêtent des mesures d'organisation du service public pénitentiaire qu'il revient aux chefs des 187 établissements pénitentiaires de mettre en oeuvre et d'appliquer sous l'autorité des directions interrégionales des services pénitentiaires. Il appartient aux chefs d'établissements pénitentiaires responsables de l'ordre et de la sécurité au sein de ceux-ci, de s'assurer du respect des consignes données pour lutter contre la propagation du virus et de prendre, dans le champ de leurs compétences, toute mesure propre à garantir le respect effectif des libertés fondamentales des personnes détenues et des personnes y travaillant ou y intervenant.<br/>
<br/>
              11. Il résulte de l'instruction que le nombre de personnes détenues dans les établissements pénitentiaires diminue régulièrement depuis le 17 mars 2020, sous l'effet conjugué de la baisse du nombre d'écrous et de l'application des dispositifs de libération des personnes détenues prévues par l'ordonnance du 25 mars 2020 portant adaptation de règles de procédure pénale. Alors que le nombre de détenus s'élevait ..., au 16 mars 2020, à 2272, il est, au 30 mars 2020, de 2088. S'agissant de la contamination par le virus covid-19, on recense, parmi les personnes détenues dans cet établissement, à la date de clôture de l'instruction, 2 cas confirmés et 16 confinements sanitaires correspondant aux cas symptomatiques et aux personnes placées en quatorzaine. <br/>
<br/>
              12. En appel, M. A... soutient que les mesures prises au sein du centre pénitentiaire de Fresnes sont insuffisantes pour le protéger efficacement du risque de contamination par le virus covid-19 et révèlent une carence portant, de manière caractérisée, une atteinte grave et manifestement illégale au droit au respect de la vie. <br/>
              13. En premier lieu, la ministre de la justice a décidé, dès le 27 février 2020, de limiter les mouvements à l'intérieur des établissements pénitentiaires et de réduire les flux de circulation entre l'intérieur et l'extérieur. A ce titre, ont été décidées, le 17 mars 2020, la suspension des activités socio-culturelles et d'enseignement, du sport en espace confiné, des cultes, de la formation professionnelle, du travail ainsi que la suspension des visites aux parloirs, parloirs familiaux et unités de vie familiale et des entretiens avec les visiteurs de prison. Seuls ont été maintenus, dans des conditions permettant de respecter les règles de sécurité sanitaire, les déplacements qu'impliquent les rendez-vous aux " parloirs avocats ", les promenades et activités de sport en plein air ainsi que l'accès aux douches collectives et, le cas échéant, les missions des unités sanitaires en milieu pénitentiaire (USMP). Il résulte de l'instruction que le chef d'établissement du centre pénitentiaire de Fresnes applique effectivement ces mesures au sein de son établissement. Il a veillé, conformément aux consignes générales, à ce que les groupes de personnes participant à des activités communes restent les mêmes d'un jour sur l'autre afin que les contacts qui demeurent .... S'agissant plus particulièrement des promenades, il résulte de l'instruction qu'il a décidé, afin de concilier la nécessité de respecter les règles de sécurité sanitaire, en particulier en ce qui concerne la distance minimale entre les personnes, et le maintien du droit, posé par le code de procédure pénale, au bénéfice d'une promenade quotidienne d'au moins une heure, de limiter le nombre de personnes se trouvant simultanément dans une même cour, à 12 au maximum pour la promenade du matin et à 25 au maximum pour la promenade de l'après-midi.<br/>
<br/>
              14. En deuxième lieu, la ministre de la justice a également demandé, dès le 27 février 2020, qu'il soit strictement veillé au respect des règles de sécurité sanitaire à l'intérieur des établissements pénitentiaires. Des consignes ont été données de veiller à ce que soient strictement respectés, tant par les personnes détenues que par les personnels pénitentiaires, les " gestes barrières " : lavage régulier des mains, limitation stricte des contacts physiques, distance minimale entre les personnes. A compter du 31 mars, le port d'un masque de protection a été imposé aux personnels en contact direct et prolongé avec les personnes détenues afin d'organiser, au sein de chaque établissement, un " anneau sanitaire ". Il résulte de l'instruction que le chef d'établissement du centre pénitentiaire de Fresnes  applique effectivement, au sein de son établissement, la consigne générale d'effectuer un nettoyage renforcé et une aération régulière des locaux, de fournir gratuitement à toutes les personnes détenues une quantité suffisante de savon et de produits d'hygiène et d'entretien afin de les mettre à même d'appliquer correctement les règles d'hygiène et les " gestes barrière " permettant d'éviter les risques de contamination. Outre les kits d'hygiène fournis ont été ainsi distribués gratuitement, au cours de la dernière semaine, aux personnes détenues ... 4600 flacons de lessive et 6900 flacons d'eau de javel. Il résulte également de l'instruction qu'il a été décidé, ..., de doter toutes les personnes détenues affectées au nettoyage des locaux de masques de protection et de gants.<br/>
<br/>
              15. En troisième lieu, un protocole relatif au signalement et à la détection des cas symptomatiques a été défini, par une note du 6 avril 2020 et l'actualisation, à la même date, de la fiche intitulée " Etablissements pénitentiaires : organisation de la réponse sanitaire par les unités sanitaires en milieu pénitentiaire en collaboration avec les services pénitentiaires " afin que puissent être détectées, dans les meilleurs délais, les personnes détenues présentant les symptômes du covid-19. Ce protocole, qu'il appartient au chef d'établissement du centre pénitentiaire de Fresnes de mettre en oeuvre au sein de son établissement, repose sur une responsabilité partagée entre les personnes détenues, le personnel pénitentiaire et les équipes des unités sanitaires en milieu pénitentiaire. Une information, régulièrement actualisée, sur les symptômes du virus doit être diffusée, notamment par voie d'affichage, afin de mettre les personnes détenues à même de repérer l'apparition de symptômes et de prendre rendez-vous avec l'unité sanitaire. Afin de permettre une plus grande réactivité, il est en outre demandé aux personnels de surveillance de prévenir, par téléphone, soit l'unité sanitaire soit, en dehors de ses horaires d'ouverture, le centre 15, de tout cas préoccupant porté à leur connaissance, à la suite d'un signalement par la personne concernée ou par un co-détenu ou sur la base de leur propre évaluation de la situation. Enfin, à titre préventif, il est recommandé aux équipes des unités sanitaires de faire montre d'une vigilance renforcée, en particulier auprès des personnes vulnérables, en organisant, le cas échéant, des consultations supplémentaires et en profitant du temps de distribution des médicaments en quartier de détention pour procéder au repérage d'éventuels symptômes afin de les signaler aux médecins.<br/>
<br/>
              16. En quatrième lieu, il résulte de l'instruction que, conformément aux consignes générales, les modalités de prise en charge des personnes détenues ... suspectées ou positives au covid-19 visent à limiter la propagation du virus. Toute personne correspondant à un cas confirmé ou un cas symptomatique dont la prise en charge médicale ne justifie pas une hospitalisation fait l'objet d'un confinement sanitaire c'est-à-dire un placement en cellule individuelle, situé, dans un quartier spécifique de l'établissement, à savoir le troisième étage nord de la première division. Elle n'a ainsi plus vocation à entrer en contact avec des personnes détenues asymptomatiques. Elle est munie d'un masque de protection qu'elle doit porter pendant l'ensemble de ses déplacements et de ses rendez-vous. A ces mesures s'ajoute le placement en quatorzaine des personnes ayant été codétenues avec elle de même que celui de tout détenu ayant été en contact avec un agent présentant les symptômes du virus. Il résulte ainsi de l'instruction que 93 personnes détenues ayant été en contact avec un membre de l'équipe de l'unité sanitaire contaminé par le virus ont fait l'objet, à titre préventif, d'un confinement sanitaire. Parmi l'ensemble des personnes détenues ayant fait l'objet d'un confinement sanitaire depuis le début de l'épidémie, 77 en sont aujourd'hui sorties sans présenter aucun symptôme du virus. <br/>
<br/>
              17. Compte tenu des mesures effectivement prises le chef d'établissement du centre pénitentiaire de Fresnes pour limiter les contacts avec l'extérieur et réduire les mouvements à l'intérieur des établissements, des consignes et des mesures effectivement prises pour assurer le respect des " gestes barrière " au sein de cet établissement, en particulier s'agissant de la distribution des produits d'hygiène et d'entretien, de l'application du protocole relatif au signalement et à la détection des cas symptomatiques ainsi que des modalités de prise en charge des cas confirmés ou symptomatiques, il n'apparaît pas, en l'état de l'instruction et à la date de la présente ordonnance, que l'absence de distribution de gants et de gel hydro-alcoolique et, eu égard à la stratégie de gestion et d'utilisation maîtrisée des masques mise en place à l'échelle nationale, en l'état du nombre de masques de protection actuellement disponibles, l'absence de distribution de masques de protection révèleraient une carence portant, de manière caractérisée, une atteinte grave et manifestement illégale au droit au respect de la vie de M. A..., dont il ne résulte pas de l'instruction qu'il présenterait le profil d'une personne spécifiquement vulnérable au virus covid-19. Il n'apparaît pas davantage, en l'état de l'instruction et à la date de la présente ordonnance, que devraient être définies, au motif d'une atteinte grave et manifestement illégale à la liberté fondamentale invoquée, des règles plus strictes que celles actuellement en vigueur au sein du centre pénitentiaire de Fresnes s'agissant du respect des règles de distance minimale entre les personnes.<br/>
<br/>
              18. Il résulte de ce qui précède, sans qu'il soit besoin d'ordonner les mesures d'expertise et d'instruction sollicitées, que M. A... n'est pas fondé à soutenir que c'est à tort que, par l'ordonnance du 30 mars 2020, le juge des référés du tribunal administratif de Melun a rejeté sa demande.<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : L'intervention de l'association " Robin des lois " est admise.<br/>
Article 2: La requête de M. A... est rejetée.<br/>
Article 3 : La présente ordonnance sera notifiée à M. B... A..., à la garde des sceaux, ministre de la justice, au ministre des solidarités et de la santé et à l'association " Robin des lois ".<br/>
Copie en sera adressée au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
