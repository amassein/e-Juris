<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037545369</ID>
<ANCIEN_ID>JG_L_2018_10_000000423044</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/54/53/CETATEXT000037545369.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 12/10/2018, 423044, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-10-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>423044</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Vincent Uher</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:423044.20181012</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 8 août 2018 au secrétariat du contentieux du Conseil d'Etat, Mme A...B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le paragraphe n° 130 des commentaires administratifs publiés au Bulletin officiel des finances publiques (BOFiP) - Impôts sous la référence BOI­RPPM-PVBMI-20-20-10 le 24 juillet 2017, en tant qu'il écarte l'application de l'abattement pour durée de détention prévu par l'article 150-0 D du code général des impôts aux plus-values réalisées antérieurement au 1er janvier 2013 et placées en report d'imposition en application du II de l'article 92 B et du I ter de l'article 160 du code général des impôts au lieu de prescrire l'application d'un abattement global déterminé en fonction de la durée de détention décomptée depuis l'acquisition des titres remis à l'échange jusqu'à la cession des titres issus de l'échange et pratiqué sur la somme de la plus-value mise en report lors de l'échange et de la plus-value  réalisée sur la cession des titres issus de l'échange ;<br/>
<br/>
              2°) de transmettre à la Cour de justice de l'Union européenne une question préjudicielle relative à la compatibilité des dispositions du III de l'article 17 de la loi du 29 décembre 2013 de finances pour 2014, du II de l'article 92 B du code général des impôts et du I ter de l'article 160 du même code dans leur rédaction applicable aux plus-values réalisées antérieurement au 1er janvier 2000, relatives aux modalités d'imposition des plus-values placées en report d'imposition, avec les dispositions de l'article 8 de la directive 90/434/CEE du Conseil du 23 juillet 1990 concernant le régime fiscal commun applicable aux fusions, scissions, apports d'actifs et échanges d'actions intéressant des sociétés d'États membres différents ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - le traité sur le fonctionnement de l'Union européenne, notamment son article 267 ;<br/>
              - la directive 90/434/CEE du Conseil du 23 juillet 1990 ;<br/>
              - la directive 2009/133/CE du Conseil du 19 octobre 2009 ;  <br/>
              - le code général des impôts et le livre des procédures fiscales ; <br/>
              - la loi n° 2013-1278 du 29 décembre 2013, notamment son article 17 ; <br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958, notamment son article 23-5 ;<br/>
              - l'arrêt de la Cour de justice de l'Union européenne du 22 mars 2018, Jacob et Lassus (C-327/16 et C-421/16) ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Uher, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :    <br/>
<br/>
              1. Mme B...demande l'annulation pour excès de pouvoir du paragraphe n° 130 des commentaires administratifs publiés au Bulletin officiel des finances publiques (BOFiP) - Impôts sous la référence BOI­RPPM-PVBMI-20-20-10 le 24 juillet 2017, en tant qu'il écarte l'application de l'abattement pour durée de détention prévu par l'article 150-0 D du code général des impôts aux plus-values réalisées antérieurement au 1er janvier 2013 et placées en report d'imposition en application du II de l'article 92 B et du I ter de l'article 160 du code général des impôts au lieu de prescrire l'application d'un abattement global déterminé en fonction de la durée de détention décomptée depuis l'acquisition des titres remis à l'échange jusqu'à la cession des titres issus de l'échange et pratiqué sur la somme de la plus-value mise en report lors de l'échange et de la plus-value  réalisée sur la cession des titres issus de l'échange. <br/>
<br/>
              2. Aux termes du paragraphe n° 130 des commentaires administratifs publiés le 24 juillet 2017 sous la référence BOI-RPPM-PVBMI-20-20-10 : " (...) l'abattement pour durée de détention ne s'applique pas (...) aux gains nets de cession, d'échange ou d'apport réalisés avant le 1er janvier 2013 et placés en report d'imposition dans les conditions prévues au II de l'article 92 B, au I ter de l'article 160 du CGI et à l'article 150 A bis du CGI dans leur rédaction en vigueur avant le 1er janvier 2000 (...) ".<br/>
<br/>
              3. L'intérêt dont se prévaut Mme B...à l'appui de sa requête tient à la réalisation d'une opération d'échange de titres entre les sociétés de Noyange et Carrefour. Ces sociétés étant toutes deux établies en France, cet intérêt ne lui donne qualité pour demander l'annulation des commentaires administratifs qu'elle attaque qu'en tant que ceux-ci concernent les situations internes, à l'exclusion des opérations intéressant des sociétés d'Etats membres différents, lesquelles sont seules susceptibles d'entrer dans le champ de la directive qu'elle invoque.<br/>
<br/>
              4. D'une part, Mme B...soutient, en soulevant une question prioritaire de constitutionnalité, que les dispositions combinées du III de l'article 17 de la loi du 29 décembre 2013 de finances pour 2014, du II de l'article 92 B et du I ter de l'article 160 du code général des impôts, que réitèrent les énonciations litigieuses, méconnaissent les principes d'égalité devant la loi et d'égalité devant les charges publiques protégés respectivement par les articles 6 et 13 de la Déclaration des droits de l'homme et du citoyen de 1789, en tant que les modalités d'imposition des plus-values réalisées avant le 1er janvier 2010 et placées en report d'imposition en application de ces articles 92 B et 160 sont plus défavorables lorsque les opérations d'échange d'actions n'entrent pas dans les prévisions de la directive " fusions " du 23 juillet 1990 que lorsqu'elles y entrent, ce dont il résulte une discrimination à rebours contraire à ces principes constitutionnels.<br/>
<br/>
              5. D'autre part, Mme B...soutient que les énonciations qu'elle attaque reproduisent des dispositions qui, en autorisant l'imposition de la totalité de la plus-value résultant de l'échange de titres alors que la plus-value de cession des titres reçus à l'échange bénéficie quant à elle, le cas échéant, des abattements pour durée de détention et, ainsi, en ne soumettant pas ces deux plus-values à un même régime d'imposition, sont incompatibles avec les objectifs du paragraphe 1 de l'article 8 de la directive 2009/133/CE du 19 octobre 2009.<br/>
<br/>
              Sur le premier moyen :<br/>
<br/>
              En ce qui concerne le cadre juridique :<br/>
<br/>
              6. Aux termes de l'article 61-1 de la Constitution du 4 octobre 1958 : " Lorsque, à l'occasion d'une instance en cours devant une juridiction, il est soutenu qu'une disposition législative porte atteinte aux droits et libertés que la Constitution garantit, le Conseil constitutionnel peut être saisi de cette question sur renvoi du Conseil d'Etat ou de la Cour de cassation qui se prononce dans un délai déterminé. / Une loi organique détermine les conditions d'application du présent article ". En vertu des dispositions de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel, prises pour l'application de ces dispositions constitutionnelles, le Conseil constitutionnel doit être saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              7. Le Conseil d'Etat, lorsqu'il est saisi d'un moyen contestant la conformité d'une disposition législative aux droits et libertés garantis par la Constitution, doit, en se prononçant par priorité sur le renvoi de la question de constitutionnalité au Conseil constitutionnel, statuer dans un délai de trois mois. En vertu des dispositions de l'article 23-7 de la même ordonnance, si le Conseil d'État ne s'est pas prononcé dans ce délai, la question est transmise au Conseil constitutionnel.<br/>
<br/>
              8. Aux termes de l'article 267 du Traité sur le fonctionnement de l'Union européenne : " La Cour de justice de l'Union européenne est compétente pour statuer, à titre préjudiciel: a) sur l'interprétation des traités, b) sur la validité et l'interprétation des actes pris par les institutions, organes ou organismes de l'Union. / Lorsqu'une telle question est soulevée devant une juridiction d'un des États membres, cette juridiction peut, si elle estime qu'une décision sur ce point est nécessaire pour rendre son jugement, demander à la Cour de statuer sur cette question. / Lorsqu'une telle question est soulevée dans une affaire pendante devant une juridiction nationale dont les décisions ne sont pas susceptibles d'un recours juridictionnel de droit interne, cette juridiction est tenue de saisir la Cour ".<br/>
<br/>
              9. Les dispositions précitées de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel ne font pas obstacle à ce que le juge administratif, juge de droit commun de l'application du droit de l'Union européenne, en assure l'effectivité, soit en l'absence de question prioritaire de constitutionnalité, soit au terme de la procédure d'examen d'une telle question, soit à tout moment de cette procédure, lorsque l'urgence le commande, pour faire cesser immédiatement tout effet éventuel de la loi contraire au droit de l'Union. Le juge administratif dispose de la possibilité de poser à tout instant, dès qu'il y a lieu de procéder à un tel renvoi, en application de l'article 267 du traité sur le fonctionnement de l'Union européenne, une question préjudicielle à la Cour de justice de l'Union européenne. Lorsque l'interprétation ou l'appréciation de la validité d'une disposition du droit de l'Union européenne détermine la réponse à la question prioritaire de constitutionnalité, il appartient au Conseil d'Etat de saisir sans délai la Cour de justice de l'Union européenne.<br/>
<br/>
              En ce qui concerne l'interprétation des dispositions invoquées dans le présent litige :<br/>
<br/>
              10. Aux termes de l'article 8 de la directive 2009/133/CE du Conseil du 19 octobre 2009 concernant le régime fiscal commun applicable aux fusions, scissions, scissions partielles, apports d'actifs et échanges d'actions intéressant des sociétés d'États membres différents, ainsi qu'au transfert du siège statutaire d'une SE ou d'une SCE d'un Etat membre à un autre, qui reprend les dispositions de l'article 8 de la directive 90/434/CEE du Conseil du 23 juillet 1990 concernant le régime fiscal commun applicable aux fusions, scissions, apports d'actifs et échanges d'actions des sociétés d'Etats membres différents : " 1. L'attribution, à l'occasion d'une fusion, d'une scission ou d'un échange d'actions, de titres représentatifs du capital social de la société bénéficiaire ou acquérante à un associé de la société apporteuse ou acquise, en échange de titres représentatifs du capital social de cette dernière société, ne doit, par elle-même, entraîner aucune imposition sur le revenu, les bénéfices ou les plus-values de cet associé. / (...) 6. L'application des paragraphes 1, 2 et 3 n'empêche pas les États membres d'imposer le profit résultant de la cession ultérieure des titres reçus de la même manière que le profit qui résulte de la cession des titres existant avant l'acquisition ".<br/>
<br/>
              11. Aux termes du II de l'article 92 B du code général des impôts, dans sa rédaction applicable aux plus-values réalisées antérieurement au 1er janvier 2000 : " 1. A compter du 1er janvier 1992 ou du 1er janvier 1991 pour les apports de titres à une société passible de l'impôt sur les sociétés, l'imposition de la plus-value réalisée en cas d'échange de titres résultant d'une opération d'offre publique, de fusion, de scission, d'absorption d'un fonds commun de placement par une société d'investissement à capital variable réalisée conformément à la réglementation en vigueur ou d'un apport de titres à une société soumise à l'impôt sur les sociétés, peut être reportée au moment où s'opérera la cession, le rachat, le remboursement ou l'annulation des titres reçus lors de l'échange. (...) ". Aux termes du I ter de l'article 160 du même code, dans sa rédaction applicable aux plus-values réalisées antérieurement au 1er janvier 2000 : " 4. L'imposition de la plus-value réalisée à compter du 1er janvier 1991 en cas d'échange de droits sociaux résultant d'une opération de fusion, scission, d'apport de titres à une société soumise à l'impôt sur les sociétés peut être reportée dans les conditions prévues au II de l'article 92 B. ".<br/>
<br/>
              12. Il résulte de ces dispositions du code général des impôts, selon l'interprétation constante qui en est donnée par le Conseil d'Etat, statuant au contentieux, qu'elles ont pour seul effet de permettre, par dérogation à la règle selon laquelle le fait générateur de l'imposition d'une plus-value est constitué au cours de l'année de sa réalisation, de constater et de liquider la plus-value d'échange l'année de sa réalisation et de l'imposer l'année au cours de laquelle intervient l'événement qui met fin au report d'imposition, qui peut notamment être la cession des titres reçus au moment de l'échange. Le montant de la plus-value est ainsi calculé en appliquant les règles d'assiette en vigueur l'année de sa réalisation, mais son imposition obéit, s'agissant d'un report optionnel, aux règles de calcul de l'impôt en vigueur l'année au cours de laquelle intervient l'événement qui met fin au report d'imposition.<br/>
<br/>
              13. En vertu du 2 de l'article 200 A du code général des impôts, dans sa rédaction applicable aux revenus perçus à compter du 1er janvier 2013 et résultant de la loi du 29 décembre 2012 de finances pour 2013, les gains nets obtenus dans les conditions prévues à l'article 150-0 A sont pris en compte pour la détermination du revenu net global soumis au barème progressif de l'impôt sur le revenu. L'article 150-0 D du code général des impôts dispose, dans sa version issue de la loi du 29 décembre 2013 de finances pour 2014, au deuxième alinéa de son 1, que : " Les gains nets de cession à titre onéreux d'actions, de parts de sociétés, de droits portant sur ces actions ou parts, ou de titres représentatifs de ces mêmes actions, parts ou droits, mentionnés au I de l'article 150-0 A, ainsi que les distributions mentionnées aux 7, 7 bis et aux deux derniers alinéas du 8 du II du même article, à l'article 150-0 F et au 1 du II de l'article 163 quinquies C sont réduits d'un abattement déterminé dans les conditions prévues, selon le cas, au 1 ter ou au 1 quater du présent article ". Ce même article définit, à son 1ter, l'abattement pour durée de détention de droit commun et, à son 1 quater, l'abattement pour durée de détention renforcé applicable à certaines situations. Le III de l'article 17 de la loi du 29 décembre 2013 de finances pour 2014 prévoit que ces dispositions s'appliquent aux gains réalisés à compter du 1er janvier 2013.<br/>
<br/>
              14. L'interprétation des dispositions nationales et l'appréciation de leur compatibilité avec la directive du 19 octobre 2009 dépendent de la réponse aux questions de savoir si les dispositions précitées de l'article 8 de la directive doivent être interprétées en ce sens qu'elles font obstacle à ce que la plus-value réalisée à l'occasion de la cession des titres reçus à l'échange et la plus-value en report soient imposées selon des règles d'assiette et de taux distinctes et, en particulier, si elles s'opposent à ce que les abattements d'assiette destinés à tenir compte de la durée de détention des titres ne s'appliquent pas à la plus-value en report, compte tenu de ce que cette règle d'assiette ne s'appliquait pas à la date à laquelle cette plus-value a été réalisée, et s'appliquent à la plus-value de cession des titres reçus à l'échange en tenant compte de la date de l'échange et non de la date d'acquisition des titres remis à l'échange. <br/>
<br/>
              15. Les questions énoncées au point 14 présentent une difficulté sérieuse d'interprétation du droit de l'Union européenne, qui justifie qu'elles soient renvoyées à la Cour de justice de l'Union européenne.<br/>
<br/>
              En ce qui concerne la question prioritaire de constitutionnalité :<br/>
<br/>
              16. Selon la réponse qui sera donnée aux questions énoncées ci-dessus, il appartiendra au juge de l'excès de pouvoir, soit de juger que les commentaires litigieux réitèrent des dispositions devant être regardées comme incompatibles avec la directive du 19 octobre 2009 en tant qu'elles s'appliquent aux plus-values d'échange d'actions entre sociétés d'Etats membres différents, soit de juger qu'ils réitèrent des dispositions qui ne sont pas incompatibles avec la directive, compte tenu, le cas échéant, de la possibilité d'en donner une interprétation conforme aux objectifs de celle-ci. Tant que l'interprétation de l'article 8 de la directive n'aura pas conduit le juge de l'excès de pouvoir à écarter l'application des énonciations réitérant les dispositions contestées aux plus-values d'échange d'actions entre sociétés d'Etats membres différents, aucune différence dans le traitement fiscal des opérations d'échange n'est susceptible d'en résulter au détriment des plus-values issues d'un échange d'actions entre sociétés françaises. Ainsi, en l'état, la question prioritaire de constitutionnalité invoquée, qui n'est pas nouvelle, ne peut être regardée comme revêtant un caractère sérieux et il n'y a pas lieu de la renvoyer au Conseil constitutionnel.<br/>
<br/>
              17. Dans le cas où, à la suite de la décision de la Cour de justice de l'Union européenne, la requérante présenterait à nouveau au Conseil d'Etat la question prioritaire de constitutionnalité invoquée, l'autorité de la chose jugée par la présente décision du Conseil d'Etat ne ferait pas obstacle au réexamen de la conformité à la Constitution des dispositions du III de l'article 17 de la loi du 29 décembre 2013 de finances pour 2014, du II de l'article 92 B et du I ter de l'article 160 du code général des impôts.<br/>
<br/>
              Sur le second moyen :<br/>
<br/>
              18. Mme B...ne peut utilement se prévaloir, à l'appui des conclusions pour lesquelles elle est recevable à agir telles que définies au point 3, de ce que les dispositions de la loi, telles que réitérées par les énonciations attaquées, seraient incompatibles avec les objectifs de l'article 8, paragraphe 2, de la directive 90/434/CE du Conseil du 23 juillet 1990, repris à l'article 8, paragraphe 1, de la directive 2009/133/CE précitée.<br/>
<br/>
              19. Il résulte de tout ce qui précède qu' il y a lieu de renvoyer à la Cour de justice de l'Union européenne les questions mentionnées au point 14 et de surseoir à statuer sur la requête de Mme B....<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par MmeB....<br/>
Article 2 : Les questions suivantes sont renvoyées à la Cour de justice de l'Union européenne :<br/>
- les dispositions de l'article 8 de la directive du 19 octobre 2009 doivent-elles être interprétées en ce sens qu'elles font obstacle à ce que la plus-value réalisée à l'occasion de la cession des titres reçus à l'échange et la plus-value en report soient imposées selon des règles d'assiette et de taux distinctes '<br/>
- ces mêmes dispositions doivent-elles en particulier être interprétées en ce sens qu'elles s'opposent à ce que les abattements d'assiette destinés à tenir compte de la durée de détention des titres ne s'appliquent pas à la plus-value en report, compte tenu de ce que cette règle d'assiette ne s'appliquait pas à la date à laquelle cette plus-value a été réalisée, et s'appliquent à la plus-value de cession des titres reçus à l'échange en tenant compte de la date de l'échange et non de la date d'acquisition des titres remis à l'échange '<br/>
Article 3 : il est sursis à statuer sur la requête de Mme B...jusqu'à ce que la Cour de justice de l'Union européenne se soit prononcée sur les questions énoncées à l'article 2. <br/>
Article 4 : La présente décision sera notifiée à Mme A...B..., au ministre de l'action et des comptes publics et au greffe de la Cour de justice de l'Union européenne.<br/>
Copie en sera adressée au Conseil constitutionnel et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
