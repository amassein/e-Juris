<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042115619</ID>
<ANCIEN_ID>JG_L_2020_07_000000427962</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/11/56/CETATEXT000042115619.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 10/07/2020, 427962</TITRE>
<DATE_DEC>2020-07-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>427962</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BAUER-VIOLAS, FESCHOTTE-DESBOIS, SEBAGH</AVOCATS>
<RAPPORTEUR>M. Yohann Bouquerel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:427962.20200710</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Rennes d'annuler la décision du 26 janvier 2016 par laquelle le ministre de la défense a refusé de reconnaître ses années d'exposition à l'amiante comme années de " travaux insalubres " au sens du décret du 5 octobre 2004 permettant une liquidation anticipée de sa pension de retraite. Par un jugement n° 1601376 du 15 décembre 2016, le tribunal administratif de Rennes a fait droit à cette demande et a enjoint au ministre de procéder à un réexamen de la situation de M. B... " au vu des états annuels existants ou à faire établir éventuellement au titre des travaux insalubres accomplis par celui-ci au cours de sa carrière ". <br/>
<br/>
              Par un arrêt n° 17NT00732 du 10 décembre 2018, la cour administrative d'appel de Nantes a rejeté le recours formé par la ministre des armées contre ce jugement et a assorti l'injonction prononcée par le tribunal administratif de Rennes d'une astreinte de 500 euros par jour de retard. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 13 février et 13 mai 2019 au secrétariat du contentieux du Conseil d'Etat, la ministre des armées demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le décret n° 67-711 du 18 août 1967 ;<br/>
              - le décret n° 2001-1269 du 21 décembre 2001 ;<br/>
              - le décret n° 2004-1056 du 5 octobre 2004 ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yohann Bouquerel, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Bauer-Violas, Feschotte-Desbois, Sebagh, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. B... a été employé par le ministère de la défense de 1976 à 2013 en qualité d'ouvrier de l'Etat. Depuis le 1er novembre 2013, il bénéficie d'une allocation spécifique de cessation anticipée d'activité en raison de son exposition à l'amiante. Par une décision du 26 janvier 2016, le ministre de la défense a rejeté la demande de M. B... tendant à ce qu'il reconnaisse ses années d'exposition à l'amiante comme années de " travaux insalubres " au sens du décret du 5 octobre 2004 relatif au régime des pensions des ouvriers des établissements industriels de l'Etat et permettant une liquidation anticipée de sa pension de retraite. Par un jugement du 15 décembre 2016, le tribunal administratif de Rennes, saisi par M. B..., a annulé cette décision. La ministre des armées se pourvoit en cassation contre l'arrêt de la cour administrative d'appel de Nantes du 10 décembre 2018 qui a rejeté son appel contre ce jugement. <br/>
<br/>
              2. Aux termes de l'article R. 811-1 du code de justice administrative : " (...) le tribunal administratif statue en premier et dernier ressort :/ (...) 7° Sur les litiges en matière de pensions ".<br/>
<br/>
              3. Le recours juridictionnel exercé contre une décision par laquelle le ministre chargé de la défense rejette une demande, qui tend à valider des services dans des emplois comportant des risques particuliers d'insalubrité en vue d'obtenir une liquidation anticipée de pension de retraite, entre dans la catégorie des litiges relatifs aux pensions pour lesquels, en application des dispositions du 7° de l'article R. 811-1 précité du code de justice administrative, le tribunal administratif statue en premier et dernier ressort. Il suit de là que les conclusions de la ministre des armées tendant à l'annulation du jugement du tribunal administratif de Rennes du 15 décembre 2016 présentées devant la cour administrative d'appel de Nantes, revêtaient le caractère d'un pourvoi en cassation. Par suite, en statuant sur ces conclusions, la cour administrative d'appel de Nantes a méconnu les règles régissant sa compétence. Dès lors et sans qu'il soit besoin d'examiner les moyens du pourvoi de la ministre des armées, son arrêt doit être annulé. <br/>
<br/>
              4. Il appartient au Conseil d'Etat de statuer, en tant que juge de cassation, sur le pourvoi formé par le ministre des armées contre le jugement du 15 décembre 2016 du tribunal administratif de Rennes. <br/>
<br/>
              5. Aux termes de l'article 1er du décret du 21 décembre 2001 relatif à l'attribution d'une allocation spécifique de cessation anticipée d'activité à certains ouvriers de l'Etat relevant du régime des pensions des ouvriers des établissements industriels de l'Etat : " Une allocation spécifique de cessation anticipée d'activité est versée, sur leur demande, aux ouvriers de l'Etat relevant du régime des pensions des ouvriers des établissements industriels de l'Etat qui sont ou ont été employés dans des établissements ou parties d'établissements de construction et de réparation navales, sous réserve qu'ils cessent toute activité professionnelle, lorsqu'ils remplissent les conditions suivantes : / 1° Travailler ou avoir travaillé dans un des établissements ou parties d'établissements mentionnés ci-dessus et figurant sur une liste établie par arrêté du ministre intéressé et des ministres chargés du budget, du travail et de la sécurité sociale, pendant des périodes fixées dans les mêmes conditions, au cours desquelles étaient traités l'amiante ou des matériaux contenant de l'amiante ; / 2° Avoir exercé, pendant les périodes mentionnées au 1°, une profession figurant sur une liste établie par arrêté du ministre intéressé et des ministres chargés du budget, du travail et de la sécurité sociale ; / 3° Avoir atteint l'âge prévu à l'article 3. / (...) ".<br/>
<br/>
              6. Aux termes de l'article 21 du décret du 5 octobre 2004 relatif au régime des pensions des ouvriers des établissements industriels de l'Etat : " I. - La liquidation de la pension intervient : / 1° Lorsque l'intéressé est radié des contrôles par limite d'âge, ou s'il a atteint, à la date d'admission à la retraite, l'âge mentionné à l'article L. 161-17-2 du code de la sécurité sociale, ou de cinquante-sept ans s'il a effectivement accompli dix-sept ans de services dans des emplois comportant des risques particuliers d'insalubrité. Les catégories d'emplois comportant ces risques sont déterminées dans les conditions fixées au II ; (...). II.- La liquidation de la pension à cinquante-sept ans prévue au 1° du I du présent article est réservée aux intéressés accomplissant des travaux ou occupant des emplois dont la liste est fixée aux annexes du décret n° 67-711 du 18 août 1967 fixant les conditions d'application du régime des pensions des ouvriers des établissements industriels de l'Etat. Les intéressés doivent avoir accompli, pendant chacune des dix-sept périodes annales exigées :/ 1° Soit trois cents heures de travail dans une des catégories de travaux insalubres ; / 2° Soit deux cents jours de services dans un des emplois insalubres pour les services effectués jusqu'au 31 décembre 2001 et de cent quatre-vingt jours de services dans un des emplois insalubres pour les services effectués à compter du 1er janvier 2002 ". Au point XVI du I " travaux " de l'annexe du décret du 18 août 1967 précité figurent " les travaux exposant à l'inhalation de poussières susceptibles d'entraîner des pneumoconioses, en l'absence de ventilation artificielle efficace ". <br/>
<br/>
              7. Il résulte des dispositions citées aux points 5 et 6, d'une part, que le versement de l'allocation spécifique de cessation anticipée d'activité à un ouvrier de l'Etat, qui atteint au moins l'âge de cinquante ans, ne fait pas obstacle, par lui-même, à ce que l'intéressé bénéficie ensuite, au terme du versement de l'allocation, d'une liquidation anticipée de sa pension de retraite, à partir de l'âge de cinquante-sept ans, en raison de son exposition à l'accomplissement de travaux insalubres, s'il réunit les conditions prévues par le décret du 5 octobre 2004. Il en résulte, d'autre part, qu'une même période peut être prise en compte pour la détermination des droits à l'allocation spécifique et ensuite pour la détermination des droits à la liquidation anticipée de la pension, dès lors que les conditions fixées respectivement par les deux décrets cités aux points 5 et 6 sont satisfaites.<br/>
<br/>
              8. Il ressort des énonciations du jugement attaqué du tribunal administratif de Rennes, non contestées sur ce point par la ministre des armées, que sa note en date du 22 février 1995, dont il n'est pas établi qu'elle ne soit plus en vigueur, demande aux chefs des établissements placés sous son autorité d'établir des états annuels de travaux insalubres en vue de la constitution des dossiers de liquidation de pension. La ministre ne conteste pas davantage, dans son pourvoi contre ce jugement, les énonciations de celui-ci selon lesquelles il a refusé la reconnaissance, pour l'application de l'article 21 du décret du 5 octobre 2004, de périodes durant lesquelles M. B... soutenait avoir accompli des travaux insalubres au seul motif que ces périodes avaient déjà été prises en compte pour l'attribution de l'allocation spécifique de cessation anticipée d'activité sans vérifier, par l'établissement d'états annuels, si l'intéressé avait accompli des travaux insalubres durant les périodes en cause. Par suite, elle n'est pas fondée à soutenir que le tribunal administratif a commis une erreur de droit et dénaturé les pièces du dossier en jugeant que sa décision du 26 janvier 2016 était illégale au motif qu'elle n'avait pas fait établir, ainsi qu'elle y était tenue, les états annuels de travaux insalubres permettant de vérifier les droits de l'intéressé à une liquidation anticipée de sa pension de retraite. <br/>
<br/>
              9. Il résulte de ce qui précède que le pourvoi de la ministre des armées dirigé contre le jugement du tribunal administratif de Rennes du 15 décembre 2016 doit être rejeté.<br/>
<br/>
              10. Aux termes de l'article L. 911-4 du code de justice administrative : " En cas d'inexécution d'un jugement (...), la partie intéressée peut demander à la juridiction, une fois la décision rendue, d'en assurer l'exécution ". Aux termes du dernier alinéa de l'article R. 921-2 du même code : " Lorsque le jugement (...) dont l'exécution est demandée a fait l'objet d'un pourvoi en cassation, le tribunal administratif (...) demeure compétent pour se prononcer sur la demande d'exécution (...) ". Il résulte de ces dispositions que le jugement des conclusions à fins d'injonction et d'astreinte présentées par M. B... devant la cour administrative d'appel de Nantes doit être attribué au tribunal administratif de Rennes.<br/>
<br/>
              11. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 4 000 euros à verser à M. B... au titre de l'article L. 761-1 du code de justice administrative pour les frais qu'il a exposés devant la cour administrative d'appel de Nantes et le Conseil d'Etat.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 10 décembre 2018 de la cour administrative d'appel de Nantes est annulé.<br/>
Article 2 : Le pourvoi de la ministre des armées contre le jugement du 15 décembre 2016 du tribunal administratif de Rennes est rejeté. <br/>
Article 3 : Le jugement des conclusions à fins d'injonction et d'astreinte présentées par M. B... devant la cour administrative d'appel de Nantes est attribué au tribunal administratif de Rennes.<br/>
Article 4 : L'Etat versera à M. B... une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : La présente décision sera notifiée à la ministre des armées, à M. A... B... et au président du tribunal administratif de Rennes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-08-03 FONCTIONNAIRES ET AGENTS PUBLICS. RÉMUNÉRATION. INDEMNITÉS ET AVANTAGES DIVERS. - ALLOCATION SPÉCIFIQUE DE CESSATION ANTICIPÉE D'ACTIVITÉ (DÉCRET DU 21 DÉCEMBRE 2001) - 1) VERSEMENT FAISANT OBSTACLE AU BÉNÉFICE D'UNE LIQUIDATION ANTICIPÉE DE LA PENSION DE RETRAITE POUR TRAVAUX INSALUBRES (DÉCRET DU 5 OCTOBRE 2004) - ABSENCE - 2) POSSIBILITÉ DE PRENDRE EN COMPTE UNE MÊME PÉRIODE POUR LA DÉTERMINATION DES DROITS À L'ALLOCATION SPÉCIFIQUE ET DES DROITS À LA LIQUIDATION ANTICIPÉE DE LA PENSION - EXISTENCE.
</SCT>
<ANA ID="9A"> 36-08-03 1) Il résulte des articles 1er du décret n° 2001-1269 du 21 décembre 2001, 21 du décret n° 2004-1056 du 5 octobre 2004 et du point XVI du I travaux de l'annexe au décret n° 67-711 du 18 août 1967, d'une part, que le versement de l'allocation spécifique de cessation anticipée d'activité à un ouvrier de l'Etat, qui atteint au moins l'âge de cinquante ans, ne fait pas obstacle, par lui-même, à ce que l'intéressé bénéficie ensuite, au terme du versement de l'allocation, d'une liquidation anticipée de sa pension de retraite à partir de l'âge de cinquante-sept ans, en raison de l'accomplissement de travaux insalubres, s'il réunit les conditions prévues par le décret du 5 octobre 2004.... ,,2) Il en résulte, d'autre part, qu'une même période peut être prise en compte pour la détermination des droits à l'allocation spécifique et ensuite pour la détermination des droits à la liquidation anticipée de la pension dès lors que les conditions fixées respectivement par les deux décrets sont satisfaites.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
