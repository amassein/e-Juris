<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032008511</ID>
<ANCIEN_ID>JG_L_2016_02_000000384473</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/00/85/CETATEXT000032008511.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 5ème SSR, 10/02/2016, 384473, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-02-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384473</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 5ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLEZ</AVOCATS>
<RAPPORTEUR>M. Jean-François de Montgolfier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:384473.20160210</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'université Paris XIII, Mme D...G...et l'Intersyndicat national des internes ont demandé au tribunal administratif de Montreuil d'annuler la décision du 17 décembre 2013 par laquelle la commission de contrôle des opérations électorales a annulé l'élection des représentants des étudiants au collège des usagers de la commission de la recherche de l'université Paris XIII. Par un jugement nos 1312319-1312344-1312346 du 21 février 2014, le tribunal administratif a annulé cette décision et validé les résultats proclamés par le procès-verbal rectificatif du 2 décembre 2013.<br/>
<br/>
              Par un arrêt n° 14VE00712 du 11 juillet 2014, la cour administrative d'appel de Versailles a rejeté l'appel formé par l'Union nationale des étudiants de France (UNEF) et M. F... C...contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 12 septembre et 12 décembre 2014 au secrétariat du contentieux du Conseil d'Etat, l'UNEF et M. C...demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) le versement d'une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'éducation ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-François de Montgolfier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boullez, avocat de l'Union des étudiants de France et autre ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 17 décembre 2013, la commission de contrôle des opérations électorales de l'université Paris XIII a annulé le résultat du scrutin qui s'est déroulé du 25 au 27 novembre 2013 en vue de l'élection des représentants des usagers à la commission de la recherche de cette université, au motif que des internes en médecine étaient inscrits sur les listes électorales, en méconnaissance de l'article D. 719-6 du code de l'éducation ; que par un jugement du 21 février 2014, le tribunal administratif de Montreuil a annulé cette décision au motif que, en ce qu'il exclut les internes en médecine du collège électoral des usagers de la commission de la recherche, l'article D. 719-6 du code de l'éducation méconnaît les dispositions de son article L. 712-5 ; que, par un arrêt du 11 juillet 2014, la cour administrative d'appel de Versailles a rejeté comme irrecevable l'appel de l'Union nationale des étudiants de France (UNEF) contre ce jugement et comme non fondé celui de M.C..., en confirmant le motif d'annulation retenu par le tribunal administratif ; qu'eu égard au moyen qu'ils soulèvent, l'UNEF et M. C...doivent être regardés comme demandant l'annulation de cet arrêt en tant seulement qu'il rejette l'appel de M. C...;<br/>
<br/>
              2. Considérant que l'UNEF ne justifie pas d'un intérêt lui donnant qualité pour contester l'arrêt attaqué en tant qu'il rejette l'appel de M. C...; que, par suite, le pourvoi est irrecevable en tant qu'il émane de l'UNEF ;<br/>
<br/>
              3. Considérant qu'aux termes du quatrième alinéa de l'article L. 712-4 du code de l'éducation, les statuts de chaque université " prévoient également les conditions dans lesquelles est assurée, au sein de la commission de la formation et de la vie universitaire et de la commission de la recherche, la représentation des grands secteurs de formation enseignés dans l'université concernée, à savoir les disciplines juridiques, économiques et de gestion, les lettres et sciences humaines et sociales, les sciences et technologies et les disciplines de santé " ; qu'aux termes du II de l'article L. 712-6-1 code de l'éducation : " La commission de la recherche du conseil académique répartit l'enveloppe des moyens destinée à la recherche telle qu'allouée par le conseil d'administration et sous réserve du cadre stratégique de sa répartition, tel que défini par le conseil d'administration. Elle fixe les règles de fonctionnement des laboratoires et elle est consultée sur les conventions avec les organismes de recherche. Elle adopte les mesures de nature à permettre aux étudiants de développer les activités de diffusion de la culture scientifique, technique et industrielle " ; qu'aux termes de l'article L. 712-5 du même code : " La commission de la recherche comprend de vingt à quarante membres ainsi répartis : (...) 2° De 10 à 15 % de représentants des doctorants inscrits en formation initiale ou continue (...) " ; qu'aux termes de l'article D. 719-6 du même code : " Pour l'élection des membres de la commission de la recherche du conseil académique ou du conseil scientifique ou de l'organe en tenant lieu, les électeurs concernés sont répartis en collèges électoraux dont la composition est fixée sur les bases suivantes. (...) II. - Pour les usagers, le collège comprend les personnes mentionnées au II de l'article D. 719-4 suivant une formation de troisième cycle relevant de l'article L. 612-7 " ;<br/>
<br/>
              4. Considérant qu'aux termes de l'article L. 612-7 du code de l'éducation : " Le troisième cycle est une formation à la recherche et par la recherche qui comporte, dans le cadre de formations doctorales, la réalisation individuelle ou collective de travaux scientifiques originaux. (...) Elles constituent une expérience professionnelle de recherche, sanctionnée, après soutenance de thèse, par la collation du grade de docteur. / Les formations doctorales sont organisées dans le cadre d'écoles doctorales (...) Elles comprennent un encadrement scientifique personnalisé de la meilleure qualité ainsi qu'une formation collective comportant des enseignements, séminaires ou stages destinés à conforter la culture scientifique des doctorants (...) / Le diplôme de doctorat est délivré après la soutenance d'une thèse ou la présentation d'un ensemble de travaux scientifiques originaux (...) " ; qu'aux termes de l'article D. 613-6 du même code : " Les grades ou titres universitaires des disciplines autres que celles relevant de la santé sont conférés par les diplômes nationaux suivants : (...) 11° Maîtrise ; 12° Master (...) 14° Doctorat ; 15° Habilitation à diriger des recherches " ;<br/>
<br/>
              5. Considérant qu'aux termes du premier alinéa de l'article L. 632-4 du code de l'éducation : " Le diplôme d'Etat de docteur en médecine est conféré après soutenance avec succès d'une thèse de doctorat " ; qu'aux termes du premier alinéa de l'article L. 632-5 du même code : " Au cours du troisième cycle des études médicales, les internes reçoivent une formation théorique et pratique à temps plein sous le contrôle des universités " ; qu'aux termes de l'article D. 613-7 : " Les grades ou titres universitaires des disciplines de santé sont conférés par les diplômes nationaux suivants : (...) 5° Diplôme de formation générale en sciences médicales ; (...) 10° Diplôme de fin de deuxième cycle des études médicales ; (...) 13° Diplôme d'Etat de docteur en médecine ; 14° Diplôme d'Etat de docteur en chirurgie dentaire ;15° Diplôme d'Etat de docteur en pharmacie ; (...) 23° Doctorat " ; qu'aux termes enfin du premier alinéa de l'article R. 632-14 du code : " Au cours de leur formation, les internes en médecine peuvent bénéficier, en tenant compte de la qualité de leur projet de recherche, d'une année de recherche dont les modalités d'organisation ainsi que le nombre de postes offerts chaque année sont fixés par arrêté des ministres chargés du budget, de l'enseignement supérieur et de la santé " ;<br/>
<br/>
              6. Considérant qu'il résulte de la combinaison des dispositions citées ci-dessus que les doctorants pouvant, en application de l'article L. 712-5 du code de l'éducation, siéger en qualité de représentants des usagers à la commission de la recherche du conseil académique des universités sont seulement les étudiants qui suivent une formation de troisième cycle à la recherche et par la recherche dispensée au sein d'une école doctorale dans les conditions définies à l'article L. 612-7 du code de l'éducation, c'est-à-dire ceux qui préparent le diplôme du doctorat prévu au 14° de l'article D. 613-6 du même code pour les disciplines autres que celles de la santé et au 23° de l'article D. 613-7 du code pour les disciplines de santé ; que les internes en médecine, qui suivent la formation théorique et pratique prévue par l'article L. 632-5 du code de l'éducation en vue d'obtenir le diplôme d'Etat de docteur en médecine prévu à l'article L. 632-4 et au 13° de son article D. 613-7 et non une formation à la recherche par la recherche, sauf s'ils préparent le doctorat prévu au 23° de l'article D. 613-7 comme ils peuvent le faire en application de l'article R. 632-14 du code, ne relèvent donc pas de cette catégorie ; que la représentation des disciplines de santé à la commission de la recherche prévue par l'article L. 712-4 est assurée par les étudiants préparant le doctorat prévu au 23° de l'article D. 613-7 ;  <br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède qu'en limitant le collège électoral des usagers de la commission de la recherche aux étudiants " suivant une formation de troisième cycle relevant de l'article L. 612-7 ", le II de l'article D. 719-6 du code de l'éducation ne méconnaît pas les dispositions de son article L. 712-5 ; que, par suite, M. C...est fondé à soutenir que la cour administrative d'appel de Versailles a commis une erreur de droit en jugeant, que l'article D. 719-6 du code de l'éducation était contraire à son article L. 712-5 ; que son arrêt doit, pour ce motif, être annulé en tant qu'il rejette l'appel de M. C...;<br/>
<br/>
              8. Considérant que, dans les circonstances de l'espèce, il n'y a pas lieu de faire droit aux conclusions présentées par M. C...au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Versailles du 11 juillet 2014 est annulé en tant qu'il statue sur l'appel de M.C....<br/>
Article 2 : L'affaire est renvoyée dans cette mesure à la cour administrative d'appel de Versailles.<br/>
Article 3 : Le surplus des conclusions du pourvoi est rejeté.<br/>
<br/>
Article 4 : La présente décision sera notifiée à l'Union nationale des étudiants de France, à M. F... C..., à l'université de Paris XIII, à Mme D...G...et à l'Intersyndicat national des internes.<br/>
Copie en sera adressée à la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche, à Mme B...A...et à M. H...E....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
