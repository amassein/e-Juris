<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031973853</ID>
<ANCIEN_ID>JG_L_2016_01_000000373951</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/97/38/CETATEXT000031973853.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème / 5ème SSR, 27/01/2016, 373951</TITRE>
<DATE_DEC>2016-01-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>373951</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème / 5ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>M. Philippe Orban</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:373951.20160127</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 13 décembre 2013, 13 mars 2014 et 17 juin 2015 au secrétariat du contentieux du Conseil d'Etat, M. B...A..., demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 11 septembre 2013 du Conseil national de l'ordre des chirurgiens-dentistes lui refusant son inscription au tableau de l'ordre des chirurgiens-dentistes de Paris ; <br/>
<br/>
              2°) d'enjoindre au Conseil national de l'ordre des chirurgiens-dentistes de l'inscrire au tableau de l'ordre de Paris, dans un délai d'un mois à compter de la notification de la décision ; <br/>
<br/>
              3°) de mettre à la charge du Conseil national de l'ordre des chirurgiens-dentistes la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative et la somme de 35 euros au titre de l'article R. 761-1 du même code.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Philippe Orban, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, avocat de M. A...et à la SCP Lyon-Caen, Thiriez, avocat du Conseil national de l'ordre des chirurgiens-dentistes ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier que M. A...a sollicité du conseil départemental de Paris de l'ordre des chirurgiens-dentistes son inscription au tableau de l'ordre, qui lui a été refusée par une décision du 8 avril 2013 ; que ce refus a été confirmé par le conseil régional d'Ile-de-France de l'ordre des chirurgiens-dentistes par une décision du 27 juin 2013 puis par le Conseil national de l'ordre des chirurgiens-dentistes par la décision du 11 septembre 2013 dont il demande l'annulation ; <br/>
<br/>
              2. Considérant qu'aux termes du 7éme alinéa de l'article R. 4112-5 du code de la santé publique, auquel renvoie l'article R. 4112-5-1 du même code qui définit la procédure applicable au recours administratif formé devant le conseil national de l'ordre : " La convocation indique que le praticien peut se faire assister ou représenter par toute personne de son choix (...) " ; qu'il résulte de ces dispositions, qui visent à garantir les droits du praticien auteur d'un recours, que l'intéressé peut se faire assister par une ou plusieurs personnes de son choix lors de la séance au cours de laquelle son recours est examiné, sous réserve des limites qui peuvent être imposées au titre notamment de la police de la séance ; <br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier que si M. A...a pu être assisté, lors de la séance du 11 septembre 2013, par l'avocat qu'il avait choisi, le conseil national a refusé que le président du " syndicat des dentistes solidaires et indépendants " que M. A...avait également choisi pour l'assister, accède à la salle de séance au seul motif que M. A...bénéficiait déjà de l'assistance de son avocat ; qu'en refusant ainsi à M. A...l'assistance d'une seconde personne de son choix pour le seul motif qu'il ne pouvait se faire assister que d'une seule personne, le Conseil national a méconnu les dispositions citées ci-dessus de l'article R. 4112-5 du code de la santé publique ; que cette irrégularité de la procédure a privé M. A...d'une garantie et a ainsi entaché la décision d'une illégalité de nature à entraîner son annulation ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens de la requête, la décision attaquée doit être annulée ;<br/>
<br/>
              4. Considérant que, eu égard aux motifs de la présente décision, l'exécution de celle-ci implique seulement que la demande d'inscription au tableau de l'ordre des chirurgiens-dentistes de Paris de M. A...soit réexaminée ; qu'il y a lieu d'enjoindre au Conseil national de l'ordre des chirurgiens-dentistes de procéder à ce réexamen dans un délai de deux mois à compter de la notification de la présente décision ;<br/>
<br/>
              5. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. A...qui n'est pas, dans la présente instance, la partie perdante ; qu'en revanche il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du Conseil national de l'ordre des chirurgiens-dentistes la somme de 3 000 euros à verser à M. A...au titre des articles L. 761-1 et R. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La décision du Conseil national de l'ordre des chirurgiens-dentistes du 11 septembre 2013 est annulée.<br/>
Article 2 : Il est enjoint au Conseil national de l'ordre des chirurgiens-dentistes de réexaminer la demande d'inscription au tableau de l'ordre des chirurgiens-dentistes de Paris de M. A...dans un délai de deux mois à compter de la notification de la présente décision.<br/>
Article 3 : Le Conseil national de l'ordre des chirurgiens-dentistes versera à M. A...une somme de 3 000 euros au titre des articles L. 761-1 et R. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par le Conseil national de l'ordre des chirurgiens-dentistes au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à M. B...A...et au Conseil national de l'ordre des chirurgiens-dentistes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">55-01-02-015-01 PROFESSIONS, CHARGES ET OFFICES. ORDRES PROFESSIONNELS - ORGANISATION ET ATTRIBUTIONS NON DISCIPLINAIRES. QUESTIONS PROPRES À CHAQUE ORDRE PROFESSIONNEL. ORDRE DES CHIRURGIENS-DENTISTES. CONSEIL NATIONAL. - RECOURS ADMINISTRATIF DEVANT LE CONSEIL NATIONAL DE L'ORDRE - POSSIBILITÉ DE SE FAIRE ASSISTER PAR PLUSIEURS PERSONNES - EXISTENCE, SOUS RÉSERVE DE LA POLICE DE LA SÉANCE.
</SCT>
<ANA ID="9A"> 55-01-02-015-01 Procédure applicable au recours administratif formé devant le Conseil national de l'ordre des chirurgiens-dentistes (article R. 4112-5-1 du code de la santé publique). Il résulte du 7e alinéa de l'article R. 4112-5 du même code, qui prévoit que la convocation indique que le praticien peut se faire assister ou représenter par toute personne de son choix et qui visent à garantir les droits du praticien auteur d'un recours, que l'intéressé peut se faire assister par une ou plusieurs personnes de son choix lors de la séance au cours de laquelle son recours est examiné, sous réserve des limites qui peuvent être imposées au titre notamment de la police de la séance.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
