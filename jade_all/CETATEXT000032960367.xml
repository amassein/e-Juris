<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032960367</ID>
<ANCIEN_ID>JG_L_2016_07_000000401699</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/96/03/CETATEXT000032960367.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 26/07/2016, 401699, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-07-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>401699</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2016:401699.20160726</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au juge des référés du tribunal administratif de Toulouse, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
              - d'une part, d'enjoindre à la ville de Toulouse de prendre toutes les mesures nécessaires tendant à mettre fin à tout type de harcèlement qu'elle subit, et de réunir le comité d'hygiène, de sécurité et des conditions de travail afin qu'une enquête administrative relative aux causes de sa tentative de suicide soit diligentée ;<br/>
              - d'autre part, d'ordonner à la ville de Toulouse de la rétablir dans ses droits à traitement, salaire et prime de service dans un délai de quinze jours ; de produire au comité d'hygiène, de sécurité et des conditions de travail les pièces nécessaires à l'enquête administrative ; de reconnaître son état de santé comme maladie professionnelle ou contractée en service ; de requalifier en accident du travail imputable au service les arrêts de travail intervenus du 21 au 27 septembre 2012 et du 17 avril au 7 mai 2014 ; de s'assurer que la protection fonctionnelle qui lui a été accordée lui assure une juste réparation des préjudices subis ; et ce, dans un délai de huit jours à compter de la date de l'ordonnance à venir, sous astreinte de 500 euros par jours de retard ;<br/>
              - enfin, de mettre à la charge de la ville de Toulouse les entiers dépens ainsi que le paiement de la somme de 6 545 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
              Par une ordonnance n° 1602860 du 1er juillet 2016, le juge des référés du tribunal administratif de Toulouse a rejeté ses demandes.<br/>
<br/>
              Par une requête enregistrée le 21 juillet 2016 au secrétariat du contentieux du Conseil d'Etat, Mme A...B...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de faire droit à ses demandes de première instance.  <br/>
<br/>
<br/>
              Elle soutient que : <br/>
              - la condition d'urgence est caractérisée par les graves difficultés financières qu'elle rencontre et par la détérioration de son état de santé physique, moral et psychologique ; <br/>
              - il est porté une atteinte grave et manifestement illégale à son droit de ne pas être soumise au harcèlement moral.<br/>
<br/>
                         Vu les autres pièces du dossier ;<br/>
<br/>
                         Vu :<br/>
                         - la loi n° 83-634 du 13 juillet 1983 ;<br/>
                         - le décret n° 82-453 du 28 mai 1982 ;<br/>
                         - le décret n° 87-602 du 30 juillet 1987 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". En vertu de l'article L. 522-3 du code de justice administrative, le juge des référés peut rejeter une requête par une ordonnance motivée, sans instruction contradictoire ni audience publique, lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée. A cet égard, il appartient au juge d'appel de prendre en compte les éléments recueillis par le juge du premier degré dans le cadre de la procédure écrite et orale qu'il a diligentée.<br/>
<br/>
              2. Selon l'article 6 quinquiès de la loi du 13 juillet 1983 portant droits et obligations des fonctionnaires : " Aucun fonctionnaire ne doit subir les agissements répétés de harcèlement moral qui ont pour objet ou pour effet une dégradation des conditions de travail susceptible de porter atteinte à ses droits et à sa dignité, d'altérer sa santé physique ou mentale ou de compromettre son avenir professionnel ". Il résulte de ces dispositions que le droit de ne pas être soumis à un harcèlement moral constitue pour un agent une liberté fondamentale au sens des dispositions de l'article L. 521-2 du code de justice administrative. Il appartient à un agent public qui soutient avoir été victime d'agissements constitutifs d'harcèlement moral de soumettre au juge des éléments de faits susceptibles de faire présumer l'existence d'un tel harcèlement.<br/>
              3. Il ressort de l'instruction que Monique B...a été victime d'un accident reconnu par l'administration comme imputable au service. Elle a été placée en congé de longue durée du 15 janvier 2015 au 14 juillet 2016. Ainsi que l'a jugé à bon droit le juge des référés du tribunal administratif de Toulouse, au vu de l'ensemble des éléments produits devant lui, aucun élément ne permet de retenir en l'état une situation de harcèlement justifiant que le juge des référés fasse usage de pouvoirs qu'il tient de l'article L. 521-2 du code de justice administrative pour faire cesser à bref délai un atteinte grave et manifestement illégale à une liberté fondamentale. Il en résulte qu'il est manifeste que la requête de Mme B...ne peut qu'être rejetée selon la procédure prévue par l'article L. 522-3 du code de justice administrative.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de Mme B...est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à Mme A...B....<br/>
Copie en sera adressée pour information à la ville de Toulouse.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
