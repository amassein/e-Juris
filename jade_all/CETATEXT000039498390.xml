<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039498390</ID>
<ANCIEN_ID>JG_L_2019_12_000000424336</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/49/83/CETATEXT000039498390.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 11/12/2019, 424336</TITRE>
<DATE_DEC>2019-12-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>424336</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP JEAN-PHILIPPE CASTON</AVOCATS>
<RAPPORTEUR>Mme Stéphanie Vera</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:424336.20191211</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au tribunal administratif de Lyon d'annuler les décisions du préfet du Rhône du 23 mars 2017 lui refusant la délivrance d'un titre de séjour, l'obligeant à quitter le territoire français dans le délai de trente jours et désignant le pays à destination duquel il pourrait être reconduit d'office.<br/>
<br/>
              Par un jugement n° 1704406 du 7 décembre 2017, le tribunal administratif de Lyon a rejeté sa demande.<br/>
<br/>
              Par une ordonnance n° 18LY00959 du 18 juin 2018, le président de la cour administrative d'appel de Lyon a rejeté l'appel formé par M. A... contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique enregistrés les 19 septembre et 12 décembre 2018 et 19 juillet 2019, au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) de faire droit à ses conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la SCP Jean-Philippe Caston, son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Stéphanie Vera, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Jean-Philippe Caston, avocat de M. A... ; <br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
<br/>
              1.	Il ressort des pièces du dossier soumis aux juges du fond que M. A..., ressortissant guinéen, né le 6 septembre 1998, est entré en France le 7 septembre 2014. Le 7 octobre 2016, il a sollicité la délivrance d'un titre de séjour sur le fondement de l'article L. 313-15 du code de l'entrée et du séjour des étrangers et du droit d'asile. Par un arrêté du 23 mars 2017, le préfet du Rhône lui a opposé un refus, assorti d'une décision l'obligeant à quitter le territoire français dans un délai de trente jours et désignant le pays de renvoi. M. A... se pourvoit contre l'ordonnance du 18 juin 2018, par laquelle le président de la cour administrative d'appel de Lyon, s'appropriant les motifs du jugement du tribunal administratif de Lyon du 7 décembre 2017, a rejeté l'appel de M. A... dirigé contre ce jugement.  <br/>
<br/>
              Sur les conclusions aux fins de non-lieu présentées par le ministre de l'intérieur :<br/>
<br/>
              2.	Il ressort des écritures en défense du ministre qu'a été délivré le 12 décembre 2018 à M. A... un récépissé valant autorisation provisoire de séjour dans l'attente de l'instruction de sa demande, valable jusqu'au 11 juin 2019 et renouvelé le 17 juin 2019 pour une période allant jusqu'au 16 septembre 2019. Cette décision a eu pour effet d'abroger l'arrêté litigieux du 23 mars 2017 en tant qu'il lui faisait obligation de quitter le territoire français et en tant qu'il fixait le pays de destination. Par suite, les conclusions du pourvoi de M. A... dirigées contre l'ordonnance en tant qu'elle tend à l'annulation de cet arrêté en tant qu'il lui fait obligation de quitter le territoire français et qu'il fixe le pays de destination sont devenues sans objet. Il n'y a, dès lors, pas lieu d'y statuer. <br/>
<br/>
              Sur le surplus des conclusions de M. A... :<br/>
<br/>
              3.	Aux termes de l'article L. 313-15 du code de l'entrée et du séjour des étrangers et du droit d'asile : " A titre exceptionnel et sauf si sa présence constitue une menace pour l'ordre public, la carte de séjour temporaire prévue aux 1° et 2° de l'article L. 313-10 portant la mention "salarié" ou la mention "travailleur temporaire" peut être délivrée, dans l'année qui suit son dix-huitième anniversaire, à l'étranger qui a été confié à l'aide sociale à l'enfance entre l'âge de seize ans et l'âge de dix-huit ans et qui justifie suivre depuis au moins six mois une formation destinée à lui apporter une qualification professionnelle, sous réserve du caractère réel et sérieux du suivi de cette formation, de la nature de ses liens avec sa famille restée dans le pays d'origine et de l'avis de la structure d'accueil sur l'insertion de cet étranger dans la société française. Le respect de la condition prévue à l'article L. 313-2 n'est pas exigé ".<br/>
<br/>
              4. Lorsqu'il examine une demande d'admission exceptionnelle au séjour en qualité de " salarié " ou " travailleur temporaire ", présentée sur le fondement de ces dispositions, le préfet vérifie tout d'abord que l'étranger est dans l'année qui suit son dix-huitième anniversaire, qu'il a été confié à l'aide sociale à l'enfance entre l'âge de seize ans et dix-huit ans, qu'il justifie suivre depuis au moins six mois une formation destinée à lui apporter une qualification professionnelle et que sa présence en France ne constitue pas une menace pour l'ordre public. Il lui revient ensuite, dans le cadre du large pouvoir dont il dispose, de porter une appréciation globale sur la situation de l'intéressé, au regard notamment du caractère réel et sérieux du suivi de cette formation, de la nature de ses liens avec sa famille restée dans son pays d'origine et de l'avis de la structure d'accueil sur l'insertion de cet étranger dans la société française. Il appartient au juge administratif, saisi d'un moyen en ce sens, de vérifier que le préfet n'a pas commis d'erreur manifeste dans l'appréciation ainsi portée. <br/>
<br/>
              5. Pour estimer que le préfet avait pu rejeter la demande de titre de séjour de M. A..., le président de la cour administrative d'appel de Lyon a, par adoption des motifs retenus par les premiers juges, relevé que si M. A..., pris en charge par les services de l'aide sociale à l'enfance du Rhône à l'âge de 16 ans et un mois et inscrit au sein de la Société lyonnaise pour l'enfance et l'adolescence en atelier pâtisserie à compter du 1er juillet 2015 avait fait l'objet d'appréciations élogieuses de la part de ses enseignants, il n'établissait pas, malgré le décès de ses parents, être isolé dans son pays d'origine. En statuant ainsi pour caractériser l'absence d'erreur manifeste d'appréciation commise par le préfet, la cour a fait du critère de l'isolement familial un critère prépondérant pour l'octroi du titre de séjour mentionné à l'article L. 313-15 précité, alors, d'une part, que les dispositions de cet article n'exigent pas que le demandeur soit isolé dans son pays d'origine et, d'autre part, que la délivrance du titre doit procéder, ainsi qu'il a été dit au point 4, d'une appréciation globale sur la situation de la personne concernée au regard du caractère réel et sérieux du suivi de sa formation, des liens avec sa famille restée dans le pays d'origine et de l'avis de la structure d'accueil sur son insertion dans la société française. Elle a par suite commis une erreur de droit. <br/>
<br/>
              6. Il résulte de ce qui précède que M. A... est fondé à demander l'annulation de l'ordonnance du président de la cour administrative d'appel de Lyon en tant qu'elle porte sur le refus de titre de séjour opposé à sa demande.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond dans cette mesure en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              8. Il résulte de ce qui a été dit au point 5 que M. A... est fondé à soutenir que c'est à tort que, par le jugement attaqué dont la cour administrative d'appel s'était appropriée les motifs, le tribunal administratif de Lyon a rejeté ses conclusions tendant à l'annulation de la décision par laquelle le préfet du Rhône a rejeté sa demande de titre de séjour. <br/>
<br/>
              9. Il en résulte également qu'en refusant de délivrer à M. A... le titre mentionné à l'article L. 313-15 au motif que celui-ci ne justifiait pas ne pas avoir conservé de liens familiaux dans son pays d'origine, le préfet du Rhône a commis une erreur de droit. Par suite, M. A... est fondé à demander l'annulation de l'arrêté du 23 mars 2017.<br/>
<br/>
              10. Il y a lieu d'enjoindre au préfet du Rhône de réexaminer la demande de M. A... dans un délai d'un mois.<br/>
<br/>
              11. M. A... ayant obtenu le bénéfice de l'aide juridictionnelle, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la SCP Jean-Philippe Caston, avocat de M. A..., sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de statuer sur les conclusions du pourvoi de M. A... tendant à l'annulation de l'arrêté du Préfet du Rhône du 23 mars 2017 en tant qu'il lui fait obligation de quitter le territoire français et qu'il fixe le pays de destination.<br/>
<br/>
Article 2 : L'ordonnance du 18 juin 2018 du président de la cour administrative d'appel de Lyon, le jugement du tribunal administratif de Lyon du 7 décembre 2017 et l'arrêté du préfet du Rhône du 23 mars 2017 sont annulés en tant qu'ils sont relatifs au refus de titre de séjour opposé à la demande de M. A...<br/>
<br/>
Article 3 : Il est enjoint au préfet du Rhône de réexaminer la demande de M. A... dans un délai d'un mois à compter de la notification de la présente décision.<br/>
<br/>
Article 4 : L'Etat versera à la SCP Jean-Philippe Caston, avocat de M. A..., la somme de 3 000 euros au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
<br/>
Article 5 : La présente décision sera notifiée à M. B... A..., au ministre de l'intérieur et au préfet du Rhône. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">335-01-02-02 ÉTRANGERS. SÉJOUR DES ÉTRANGERS. AUTORISATION DE SÉJOUR. OCTROI DU TITRE DE SÉJOUR. - DEMANDE D'ADMISSION EXCEPTIONNELLE AU SÉJOUR EN QUALITÉ DE SALARIÉ OU TRAVAILLEUR TEMPORAIRE (ART. L. 313-15 DU CESEDA) - POUVOIRS DU PRÉFET - 1) VÉRIFICATION DES CONDITIONS OBJECTIVES FIXÉES PAR L'ARTICLE L. 313-15 DU CESEDA - 2) A) APPRÉCIATION GLOBALE DE LA SITUATION DE L'INTÉRESSÉ AU REGARD DES AUTRES CRITÈRES PRÉVUS PAR CET ARTICLE - B) CONTRÔLE DU JUGE SUR CETTE APPRÉCIATION - CONTRÔLE RESTREINT - 3) ILLUSTRATION.
</SCT>
<ANA ID="9A"> 335-01-02-02 1) Lorsqu'il examine une demande d'admission exceptionnelle au séjour en qualité de salarié ou travailleur temporaire, présentée sur le fondement de l'article L. 313-15 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), le préfet vérifie tout d'abord que l'étranger est dans l'année qui suit son dix-huitième anniversaire, qu'il a été confié à l'aide sociale à l'enfance (ASE) entre l'âge de seize ans et dix-huit ans, qu'il justifie suivre depuis au moins six mois une formation destinée à lui apporter une qualification professionnelle et que sa présence en France ne constitue pas une menace pour l'ordre public.... ,,2) a) Il lui revient ensuite, dans le cadre du large pouvoir dont il dispose, de porter une appréciation globale sur la situation de l'intéressé, au regard notamment du caractère réel et sérieux du suivi de cette formation, de la nature de ses liens avec sa famille restée dans son pays d'origine et de l'avis de la structure d'accueil sur l'insertion de cet étranger dans la société française.... ,,b) Il appartient au juge administratif, saisi d'un moyen en ce sens, de vérifier que le préfet n'a pas commis d'erreur manifeste dans l'appréciation ainsi portée.... ,,3) Cour administrative d'appel ayant relevé, pour estimer que le préfet avait pu rejeter la demande de titre de séjour, que l'intéressé n'établissait pas, malgré le décès de ses parents, être isolé dans son pays d'origine.... ,,En statuant ainsi pour caractériser l'absence d'erreur manifeste d'appréciation commise par le préfet, la cour a fait du critère de l'isolement familial un critère prépondérant pour l'octroi du titre de séjour mentionné à l'article L. 313-15 alors, d'une part, que les dispositions de cet article n'exigent pas que le demandeur soit isolé dans son pays d'origine et, d'autre part, que la délivrance du titre doit procéder d'une appréciation globale sur la situation de la personne concernée au regard du caractère réel et sérieux du suivi de sa formation, de ses liens avec sa famille restée dans le pays d'origine et de l'avis de la structure d'accueil sur son insertion dans la société française. Elle a par suite commis une erreur de droit.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
