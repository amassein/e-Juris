<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034130168</ID>
<ANCIEN_ID>JG_L_2017_02_000000387886</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/13/01/CETATEXT000034130168.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 5ème chambres réunies, 01/02/2017, 387886, Publié au recueil Lebon</TITRE>
<DATE_DEC>2017-02-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>387886</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>Mme Sophie Baron</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:387886.20170201</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. V...AF..., M. AO...AK..., M. AW...AZ..., M. AG... AH..., Mme E...AL..., M. AB...AM..., M. AI... AN..., M. Q... A..., Mme AQ...R..., M. P...AP..., M. D... G..., M. AB...H..., M. N... U..., M. C...AS..., M. O...W..., Mme AV...AT..., Mme AA...AU..., M. T...B..., M. BA... X..., M. AG...I..., Mme AR...I..., M. Y...J..., M. AE...AJ..., M. AD...AJ..., M. S...K..., Mme Z...K..., M. F...AY..., M. AI...AX...et M. AC...L..., salariés de la société Avinov, ont demandé au tribunal administratif de Caen d'annuler pour excès de pouvoir la décision du 25 mars 2014 par laquelle le directeur régional des entreprises, de la concurrence, de la consommation, du travail et de l'emploi de Basse-Normandie a homologué le document unilatéral fixant le plan de sauvegarde de l'emploi de cette société. Par un jugement n° 1401157 du 23 juillet 2014, le tribunal administratif a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 14NT02465 du 15 décembre 2014, la cour administrative d'appel de Nantes a, sur appel des mêmes salariés, annulé ce jugement et la décision du 25 mars 2014.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 11 février et 17 mars 2015 au secrétariat du contentieux du Conseil d'Etat, MeM..., agissant en qualité de liquidateur judiciaire de la société Avinov, demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M. AF...et autres ;<br/>
<br/>
              3°) de mettre à la charge de M. AF...et autres la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie Baron, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de Me M...et à la SCP Didier, Pinet, avocat de M. AF...et autres ; <br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 25 mars 2014, le directeur régional des entreprises, de la concurrence, de la consommation, du travail et de l'emploi de Basse-Normandie a homologué le plan de sauvegarde de l'emploi soumis par l'administrateur judiciaire de la société Avinov ; que ce plan prévoyait, dans le cadre d'une cession de cette société à la société Klein Access Design, le licenciement de 35 des 72 salariés de l'entreprise ; que le choix des salariés licenciés devait résulter de l'application, au sein de chaque catégorie professionnelle concernée par le licenciement, de quatre critères ainsi pondérés : les charges de famille, avec deux points par enfant à charge et cinq points en qualité de parent isolé, l'ancienneté dans l'entreprise, avec deux points par année pleine d'ancienneté au 4 mars 2014, les difficultés de réinsertion, avec cinq ou six points pour les salariés handicapés ou âgés et deux points pour les salariés en congé de maternité ou victimes d'un accident du travail et, enfin, la " qualification professionnelle ", avec une pondération uniforme d'un point par salarié ; que, par un arrêt du 15 décembre 2014, la cour administrative d'appel de Nantes a, sur appel de plusieurs salariés de la société Avinov, annulé le jugement du 23 juillet 2014 du tribunal administratif de Caen qui avait rejeté leur demande et annulé la décision d'homologation du 25 mars 2014, au motif que la pondération uniforme retenue, par le plan de sauvegarde de l'emploi, pour le quatrième critère définissant l'ordre des licenciements, faisait obstacle à son homologation ; que MeM..., liquidateur judiciaire de la société Avinov, demande l'annulation de cet arrêt ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 1233-24-2 du code du travail, relatif aux plans de sauvegarde de l'emploi : " L'accord collectif mentionné à l'article L. 1233-24-1 porte sur le contenu du plan de sauvegarde de l'emploi mentionné aux articles L. 1233-61 à L. 1233-63. / Il peut également porter sur : / (...) 2 La pondération et le périmètre d'application des critères d'ordre des licenciements mentionnés à l'article L. 1233-5 (...) " ; qu'aux termes de l'article L. 1233-24-4 du même code : " A défaut d'accord mentionné à l'article L. 1233-24-1, un document élaboré par l'employeur après la dernière réunion du comité d'entreprise fixe le contenu du plan de sauvegarde de l'emploi et précise les éléments prévus aux 1° à 5° de l'article L. 1233-24-2, dans le cadre des dispositions légales et conventionnelles en vigueur " ; qu'enfin, l'article L. 1233-57-3 de ce code dispose que : " En l'absence d'accord collectif (...), l'autorité administrative homologue le document élaboré par l'employeur mentionné à l'article L. 1233-24-4, après avoir vérifié la conformité de son contenu aux dispositions législatives et aux stipulations conventionnelles relatives aux éléments mentionnés aux 1o à 5o de l'article L. 1233-24-2 (...) " ; qu'il résulte de ces dispositions que, lorsque les critères d'ordre des licenciements fixés dans un plan de sauvegarde de l'emploi figurent dans un document unilatéral élaboré par l'employeur sur le fondement de l'article L. 1233-24-4, il appartient à l'autorité administrative, saisie de la demande d'homologation de ce document, de vérifier la conformité de ces critères et de leurs règles de pondération aux dispositions législatives et conventionnelles applicables ; <br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 1233-5 du code du travail : " Lorsque l'employeur procède à un licenciement collectif pour motif économique et en l'absence de convention ou accord collectif de travail applicable, il définit les critères retenus pour fixer l'ordre des licenciements, après consultation du comité d'entreprise ou, à défaut, des délégués du personnel. / Ces critères prennent notamment en compte : / 1° Les charges de famille, en particulier celle des parents isolés ; / 2° L'ancienneté de service dans l'établissement ou l'entreprise ; / 3° La situation des salariés qui présentent des caractéristiques sociales rendant leur réinsertion professionnelle particulièrement difficile, notamment celle des personnes handicapées et des salariés âgés ; / 4° Les qualités professionnelles appréciées par catégorie. / L'employeur peut privilégier un de ces critères, à condition de tenir compte de l'ensemble des autres critères prévus au présent article. (...) " ; qu'il résulte de la lettre même de ces dispositions qu'en l'absence d'accord collectif ayant prévu d'autres critères, l'employeur qui procède à un licenciement collectif pour motif économique est tenu, pour déterminer l'ordre des licenciements, de prendre en compte l'ensemble des critères qui sont énumérés à l'article L. 1233-5 cité ci-dessus, y compris, contrairement à ce que soutient le requérant, le critère des qualités professionnelles mentionné à son 4° ; que, par suite, en l'absence d'accord collectif ayant fixé les critères d'ordre des licenciements, le document unilatéral de l'employeur fixant le plan de sauvegarde de l'emploi ne saurait légalement, ni omettre l'un de ces critères, ni affecter l'un d'entre eux de la même valeur pour tous les salariés, dès lors que l'omission d'un critère dans le plan de sauvegarde de l'emploi, ou l'interdiction de le moduler, ont pour effet d'empêcher par avance que ce critère puisse être effectivement pris en compte au stade de la détermination de l'ordre des licenciements ; que l'autorité administrative ne saurait, par conséquent, homologuer un tel document, sauf s'il est établi de manière certaine, dès l'élaboration du plan de sauvegarde de l'emploi, que, dans la situation particulière de l'entreprise et au vu de l'ensemble des personnes susceptibles d'être licenciées, aucune des modulations légalement envisageables pour le critère en question ne pourra être matériellement mise en oeuvre lors de la détermination de l'ordre des licenciements ;<br/>
<br/>
              4. Considérant, en premier lieu, qu'en estimant, au vu du dossier qui lui était soumis, que le fait que l'administrateur judiciaire de la société Avinov ne disposait, au moment de l'élaboration du plan de sauvegarde de l'emploi, ni de fiches de postes ni d'évaluations antérieures des salariés, ne l'empêchait pas de fixer tout de même, pour le critère des " qualifications professionnelles ", un ou plusieurs éléments de pondération susceptibles d'être ultérieurement mis en oeuvre, la cour administrative d'appel s'est livrée à une appréciation souveraine des faits de l'espèce, exempte de dénaturation ;<br/>
<br/>
              5. Considérant, en second lieu, qu'il résulte de ce qui a été dit au point 3 que la cour a pu, par suite, en déduire sans erreur de droit qu'en l'absence d'accord collectif ayant fixé les critères d'ordre des licenciements, le plan de sauvegarde de l'emploi de la société Avinov ne pouvait donner au critère de " qualification professionnelle " une valeur fixe et, par suite, le neutraliser, sans méconnaître les dispositions de l'article L. 1233-5 du code du travail ; <br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que MeM..., qui n'est pas fondé à soutenir que l'arrêt qu'il attaque est insuffisamment motivé, n'est pas fondé à en demander l'annulation ; que son pourvoi doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              7. Considérant que, l'Etat n'étant pas partie à la présente instance, les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soient mises à sa charge les sommes qui lui sont demandées à ce titre par les défendeurs ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
Article 1er : Le pourvoi de Maître M...est rejeté. <br/>
Article 2 : Les conclusions de M. AF...et autres présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à Maître M...et à M. V...AF..., premier défendeur dénommé. Les autres défendeurs seront informés de la présente décision par la SCP Hélène Didier et François Pinet, avocat au Conseil d'Etat et à la Cour de cassation, qui les représente devant le Conseil d'Etat. <br/>
Copie en sera adressée à la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">66-07 TRAVAIL ET EMPLOI. LICENCIEMENTS. - VALIDATION OU HOMOLOGATION ADMINISTRATIVE DES PSE (LOI DU 14 JUIN 2013) - HOMOLOGATION D'UN DOCUMENT UNILATÉRAL - CRITÈRES D'ORDRE DES LICENCIEMENTS - 1) CONTRÔLE DE L'ADMINISTRATION - EXISTENCE - 2) OBLIGATION DE PRENDRE EN COMPTE L'ENSEMBLE DES CRITÈRES PRÉVUS À L'ARTICLE L. 1233-5 DU CODE DU TRAVAIL - EXISTENCE.
</SCT>
<ANA ID="9A"> 66-07 1) Lorsque les critères d'ordre des licenciements fixés dans un plan de sauvegarde de l'emploi figurent dans un document unilatéral élaboré par l'employeur sur le fondement de l'article L. 1233-24-4, il appartient à l'autorité administrative, saisie de la demande d'homologation de ce document, de vérifier la conformité de ces critères et de leurs règles de pondération aux dispositions législatives et conventionnelles applicables.... ,,2) Il résulte de la lettre même de l'article L. 1233-5 du code du travail qu'en l'absence d'accord collectif ayant prévu d'autres critères, l'employeur qui procède à un licenciement collectif pour motif économique est tenu, pour déterminer l'ordre des licenciements, de prendre en compte l'ensemble des critères qui sont énumérés à cet article, y compris  le critère des qualités professionnelles mentionné à son 4°. Par suite, en l'absence d'accord collectif ayant fixé les critères d'ordre des licenciements, le document unilatéral de l'employeur fixant le plan de sauvegarde de l'emploi (PSE) ne saurait légalement, ni omettre l'un de ces critères, ni affecter l'un d'entre eux de la même valeur pour tous les salariés, dès lors que l'omission d'un critère dans le plan ou l'interdiction de le moduler ont pour effet d'empêcher par avance que ce critère puisse être effectivement pris en compte au stade de la détermination de l'ordre des licenciements. L'autorité administrative ne saurait, par conséquent, homologuer un tel document, sauf s'il est établi de manière certaine, dès l'élaboration du PSE, que, dans la situation particulière de l'entreprise et au vu de l'ensemble des personnes susceptibles d'être licenciées, aucune des modulations légalement envisageables pour le critère en question ne pourra être matériellement mise en oeuvre lors de la détermination de l'ordre des licenciements.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
