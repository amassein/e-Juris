<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036791246</ID>
<ANCIEN_ID>JG_L_2018_04_000000415891</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/79/12/CETATEXT000036791246.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 11/04/2018, 415891, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-04-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>415891</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Stéphane Hoynck</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHS:2018:415891.20180411</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
     Par un jugement n°1700197 du 16 novembre 2017 enregistré le 22 novembre 2017 au secrétariat du contentieux du Conseil d'Etat, le tribunal administratif de Nouvelle-Calédonie, avant de statuer sur la demande de la province Nord tendant à l'annulation des arrêtés n°2017-763 et 2017-767 du 28 mars 2017 par lequel le gouvernement de la Nouvelle Calédonie a respectivement institué les zones de développement prioritaires (ZODEP) Est et Houaïlou Tribu de Bâ-Kaora, a transmis, en application des dispositions de l'article 205 de la loi organique du 19 mars 1999, le dossier de cette demande au Conseil d'Etat en soumettant à son examen la question de savoir si le gouvernement de la Nouvelle-Calédonie pouvait être désigné par le congrès de Nouvelle-Calédonie comme autorité compétente pour instituer les ZODEP, compte tenu de la compétence des provinces en matière d'aménagement, d'habitat, d'économie, d'agriculture, de culture, d'action sociale et d'environnement. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ; <br/>
              - la loi organique n°99-209 du 19 mars 1999 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Stéphane Hoynck, maître des requêtes, <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
<br/>
              REND L'AVIS SUIVANT<br/>
<br/>
<br/>
<br/>
              1. Aux termes de l'article 204 de la loi organique du 19 mars 1999 relative à la Nouvelle-Calédonie : " I. - Les actes du congrès, de sa commission permanente et de son président (...)mentionnés au II sont exécutoires de plein droit dès qu'il a été procédé à leur publication au Journal officiel de la Nouvelle-Calédonie ou à leur notification aux intéressés, ainsi qu'à leur transmission au haut-commissaire ou à son représentant dans la province, (...) par le président du congrès. (...) / II. - Sont soumis aux dispositions du I les actes suivants : (...) / B. - Pour le gouvernement : 1° Les arrêtés à caractère réglementaire ou individuel qu'il adopte ; (...) ".  Aux termes de l'article 205 de la même loi : " Lorsque le tribunal administratif est saisi d'un recours pour excès de pouvoir dirigé contre les actes mentionnés aux 1° du (...) B (...) du II de l'article 204 et que ce recours est fondé sur un moyen sérieux invoquant l'inexacte application de la répartition des compétences entre l'Etat, la Nouvelle-Calédonie, les provinces et les communes ou que ce moyen est soulevé d'office, il transmet le dossier sans délai pour avis au Conseil d'Etat, par un jugement qui n'est susceptible d'aucun recours. Le Conseil d'Etat examine la question soulevée dans un délai de trois mois et il est sursis à toute décision sur le fond jusqu'à son avis ou, à défaut, jusqu'à l'expiration de ce délai. Le tribunal administratif statue dans un délai de deux mois à compter de la publication de l'avis au Journal officiel de la Nouvelle-Calédonie ou de l'expiration du délai imparti au Conseil d'Etat ". <br/>
<br/>
              2. En application de ces dispositions, le tribunal administratif de Nouvelle-Calédonie a sursis à toute décision au fond sur la demande de la province Nord tendant à l'annulation des arrêtés n°2017-763 et 2017-767 du 28 mars 2017 par lequel le gouvernement de la Nouvelle Calédonie a respectivement institué les zones de développement prioritaires (ZODEP) Est et Houaïlou Tribu de Bâ-Kaora, et a transmis le dossier au Conseil d'Etat en lui posant la question de la légalité des dispositions de l'article 1er de la délibération n°194 du 5 mars 2012 sur le fondement duquel les deux arrêtés contestés ont été pris au regard de la répartition des compétences entre la Nouvelle-Calédonie et la province.<br/>
<br/>
              3. L'article 1er de la délibération dont il est demandé au Conseil d'Etat d'apprécier la légalité au regard de la répartition des compétences entre la Nouvelle-Calédonie et les provinces confie au gouvernement de la Nouvelle-Calédonie la possibilité " afin de favoriser le développement social, économique et écologique de l'ensemble du territoire de la Nouvelle-Calédonie ", d' " instituer des zones de développement prioritaire ". Le reste de la délibération dont la légalité au regard de la répartition des compétences entre la Nouvelle-Calédonie et les provinces n'est pas contestée prévoit la possibilité de faire bénéficier ces zones d'un régime fiscal privilégié, énonce le principe de délimitation géographique de chaque zone, la subdivision de chaque zone en sous-zones spécifiques relatives à la politique de l'habitat, au développement économique, à l'implantation d'infrastructures publiques,  et à la valorisation de la terre, énonce les objectifs propres à chacun de ces axes et définit les modalités de délimitation des ZODEP et le processus d'instruction préalable à l'institution de chaque ZODEP. <br/>
<br/>
              4. La province Nord soutient devant le tribunal administratif de Nouvelle-Calédonie que l'article 1er de cette délibération méconnait la compétence des provinces en matière d'aménagement, d'habitat, d'économie, d'agriculture, de culture, d'action sociale et d'environnement. <br/>
<br/>
              5. Aux termes de l'article 20 de la loi organique du 19 mars 1999, " Chaque province est compétente dans toutes les matières qui ne sont pas dévolues à l'Etat ou à la Nouvelle-Calédonie par la présente loi, ou aux communes par la législation applicable en Nouvelle-Calédonie ". Aux termes de l'article 22 de la même loi, " La Nouvelle-Calédonie est compétente dans les matières suivantes : (...) 21° Principes directeurs du droit de l'urbanisme ". Ces principes directeurs doivent s'entendre non comme correspondant aux principes fondamentaux dont la détermination est réservée au législateur par l'article 34 de la Constitution, ni aux normes adoptées en métropole par le législateur, mais comme les principes relatifs à l'urbanisme et concernant, sur le fond et quant à la procédure, l'encadrement des atteintes au droit de propriété, la détermination des compétences et la garantie de la cohésion territoriale. Constituent donc des principes directeurs du droit de l'urbanisme, au sens de ces dispositions, les règles générales relatives à l'utilisation du sol, aux documents d'urbanisme, aux procédures d'aménagement et au droit de préemption, ainsi que celles relatives à la détermination des autorités compétentes pour élaborer et approuver les documents d'urbanisme, conduire les procédures d'aménagement, délivrer les autorisations d'urbanisme et exercer le droit de préemption. Font également partie de ces principes directeurs les règles générales régissant l'exercice de ces compétences. En ce qui concerne plus précisément les procédures d'aménagement, relèvent des principes directeurs la fixation des finalités des actions ou opérations d'aménagement, la définition de leur champ d'application, la détermination des autorités compétentes pour les mettre en oeuvre, des procédures et modalités auxquelles cette mise en oeuvre est soumise ainsi que des pouvoirs qui peuvent y être exercés. <br/>
<br/>
              6. Comme il a été vu au point 3, l'objectif de la création des ZODEP est de favoriser le développement de ces territoires à travers une politique de l'habitat, un appui à l'implantation d'activités économiques et d'infrastructures publiques et des actions de valorisation de la terre. L'institution, par la délibération litigieuse, de cette nouvelle procédure d'aménagement, la détermination de son contenu, de ses effets, de la collectivité qui peut la mettre en oeuvre, ainsi que des modalités de cette mise en oeuvre, relèvent des principes directeurs du droit de l'urbanisme. Il s'ensuit que la Nouvelle-Calédonie était compétente pour créer cette procédure d'aménagement et confier au gouvernement de Nouvelle-Calédonie la compétence pour instituer ces zones, sans méconnaitre les compétences des provinces.  <br/>
<br/>
              Le présent avis sera notifié au président du tribunal administratif de Nouvelle-Calédonie, au président de l'assemblée de la province Nord de la Nouvelle-Calédonie, au président du congrès de la Nouvelle-Calédonie et au président du gouvernement de la Nouvelle-Calédonie. <br/>
<br/>
              Il sera publié au Journal officiel de la Nouvelle-Calédonie. <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
