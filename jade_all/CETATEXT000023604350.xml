<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000023604350</ID>
<ANCIEN_ID>JG_L_2011_01_000000310270</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/23/60/43/CETATEXT000023604350.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 26/01/2011, 310270</TITRE>
<DATE_DEC>2011-01-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>310270</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BROUCHOT</AVOCATS>
<RAPPORTEUR>M. Jean-Luc  Matt</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Hédary Delphine</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2011:310270.20110126</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 22 octobre 2007 et 28 janvier 2008 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme A...B..., demeurant... ; Mme B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement du 3 octobre 2006 par lequel le tribunal administratif de Nantes a rejeté sa demande tendant à l'annulation de la décision du 15 juin 2004 du directeur départemental de l'agriculture et de la forêt de Loire-Atlantique refusant de lui communiquer une copie de l'intégralité du registre des réclamations déposées durant l'enquête publique relative au projet de remembrement de la commune de Ruffigné ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande de première instance ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 500 euros à verser à Me Brouchot, avocat de MmeB..., au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu la convention d'Aarhus sur l'accès à l'information, la participation du public au processus décisionnel et l'accès à la justice en matière d'environnement ;<br/>
<br/>
              Vu la directive 90/313/CEE du Conseil du 7 juin 1990 ;<br/>
<br/>
              Vu le code de l'environnement ;<br/>
<br/>
              Vu le code rural ;<br/>
<br/>
              Vu la loi n° 78-753 du 17 juillet 1978 ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Matt, chargé des fonctions de Maître des Requêtes,<br/>
<br/>
              - les observations de Me Brouchot, avocat de MmeB...,<br/>
<br/>
              - les conclusions de Mme Delphine Hedary, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à Me Brouchot, avocat de Mme B... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme B...a demandé que lui soit communiquée une copie de l'intégralité du registre des réclamations déposées durant l'enquête publique relative au projet de remembrement de la commune de Ruffigné ; qu'un refus implicite lui ayant été opposé par le directeur départemental de l'agriculture et de la forêt de Loire-Atlantique, Mme B...a saisi la Commission d'accès aux documents administratifs ; qu'à la suite de l'avis partiellement défavorable rendu le 13 mai 2004 par cette commission, qui a estimé que seules les mentions résultant des propres déclarations de Mme B...portées sur le registre des réclamations pouvaient lui être communiquées, le directeur départemental de l'agriculture et de la forêt de Loire-Atlantique, par une lettre du 15 juin 2004, a communiqué à Mme B...une copie des seules mentions résultant de ses propres déclarations portées sur le registre des réclamations ; que Mme B... demande l'annulation du jugement du 3 octobre 2006 par lequel le tribunal administratif de Nantes a rejeté sa demande tendant à l'annulation du refus du directeur départemental de l'agriculture et de la forêt de Loire-Atlantique de lui communiquer l'intégralité du registre des réclamations déposées durant l'enquête publique relative au projet de remembrement de la commune de Ruffigné ;<br/>
<br/>
              Sur les conclusions à fin d'annulation :<br/>
<br/>
              Considérant, d'une part, que l'article L. 121-1 du code rural, dans sa rédaction applicable à la date de la décision attaquée, dispose : " L'aménagement foncier rural a pour objet d'assurer la mise en valeur et l'amélioration des conditions d'exploitation des propriétés agricoles ou forestières (...) / Il est réalisé par la mise en oeuvre, de façon indépendante ou coordonnée, des modes d'aménagement foncier suivants : / (...) 2° Le remembrement (...) régis par les articles L. 123-1 à L. 123-35 (...) / Les opérations d'aménagement foncier sont conduites, sous la responsabilité de l'Etat, par des commissions d'aménagement foncier (...) en veillant au respect et à la mise en valeur des milieux naturels, du patrimoine rural et des paysages. Ces commissions doivent favoriser la concertation entre toutes les parties intéressées (...) " ; que la procédure applicable aux opérations de remembrement prévoit, notamment, la réalisation d'une enquête publique ; qu'aux termes de l'article R. 123-11 du code rural, dans sa rédaction applicable à la date de la décision attaquée : " (...) La durée de l'enquête est d'un mois, sauf prorogation d'une durée maximum de quinze jours décidée par le commissaire enquêteur ou par la commission d'enquête. Durant ce délai, les pièces de l'enquête ainsi qu'un registre destiné à recevoir les réclamations et observations des propriétaires et des autres personnes intéressées sont déposés à la mairie de la commune où la commission a son siège " ;<br/>
<br/>
              Considérant, d'autre part, que l'article L. 124-1 du code de l'environnement, dans sa rédaction applicable à la date de la décision attaquée, dispose : " I. L'accès à l'information relative à l'environnement détenue par les autorités publiques ayant des responsabilités en matière d'environnement s'exerce dans les conditions et selon les modalités définies au titre Ier de la loi n° 78-753 du 17 juillet 1978 portant diverses mesures d'amélioration des relations entre l'administration et le public et diverses dispositions d'ordre administratif, social et fiscal, sous réserve des dispositions ci-après. / II. Ne sont pas communicables les informations relatives à l'environnement dont la consultation ou la communication porterait atteinte aux intérêts protégés énumérés aux sept premiers tirets du I de l'article 6 de la loi susmentionnée du 17 juillet 1978. / L'autorité peut refuser de communiquer une information relative à l'environnement dont la consultation ou la communication porterait atteinte : / 1° A l'environnement auquel elle se rapporte ; / 2° Aux intérêts d'un tiers qui a fourni l'information demandée sans y avoir été contraint par une disposition législative, réglementaire ou par un acte d'une autorité administrative, et qui ne consent pas à sa divulgation. / III. Lorsque la demande d'accès porte sur une information relative à l'environnement qui contient des données relatives aux intérêts protégés en application du II et qu'il est possible de retirer ces données, la partie de l'information non couverte par les secrets protégés est communiquée au demandeur " ; qu'il résulte de ces dispositions que les informations en matière environnementale, au sens de l'article L. 124-1 du code de l'environnement dans sa rédaction alors applicable, sont communicables dans les conditions prévues par l'ensemble du titre Ier de la loi du 17 juillet 1978, y compris les exceptions au droit de communication prévues par le II de l'article 6 de cette loi, seule la disposition figurant au huitième tiret du I de cet article n'étant pas applicable ;<br/>
<br/>
              Considérant que les registres de réclamation mis à disposition du public, en application de l'article R. 123-11 du code rural, pendant la durée de l'enquête publique relative à des opérations de remembrement comportent des informations relatives à l'environnement au sens de l'article L. 124-1 du code de l'environnement et sont donc communicables sur le fondement de cet article ; qu'en jugeant que la communication des mentions d'un tel registre porte atteinte au secret de la vie privée au sens de la loi du 17 juillet 1978 et que ne sont communicables qu'aux réclamants les observations résultant de leurs propres déclarations, alors que les observations figurant sur un tel registre résultent d'interventions volontaires dans le cadre d'une enquête publique relative à l'environnement permettant de recenser les souhaits des personnes concernées, le tribunal administratif de Nantes a commis une erreur de droit ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, Mme B...est fondée à demander l'annulation du jugement attaqué ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Considérant qu'ainsi qu'il a été dit ci-dessus, le directeur départemental de l'agriculture et de la forêt de Loire-Atlantique ne pouvait légalement refuser de communiquer à Mme B...le registre des réclamations au motif que cette communication porterait atteinte au secret de la vie privée, sans invoquer aucune circonstance particulière mettant en cause la vie privée des personnes ayant volontairement porté des mentions sur ce registre dans le cadre d'une enquête publique relative à l'environnement ; que, par suite, la décision du 15 juin 2004 du directeur départemental de l'agriculture et de la forêt de Loire-Atlantique refusant de communiquer à Mme B...l'intégralité du registre des réclamations déposées durant l'enquête publique relative au projet de remembrement de la commune de Ruffigné doit être annulée ;<br/>
<br/>
              Sur les conclusions à fin d'injonction :<br/>
<br/>
              Considérant qu'aux termes de l'article L. 911-1 du code de justice administrative : " Lorsque sa décision implique nécessairement qu'une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public prenne une mesure d'exécution dans un sens déterminé, la juridiction, saisie de conclusions en ce sens, prescrit, par la même décision, cette mesure assortie, le cas échéant, d'un délai d'exécution " ;<br/>
<br/>
              Considérant que la présente décision implique que le ministre de l'agriculture, de l'alimentation, de la pêche, de la ruralité et de l'aménagement du territoire communique à Mme B...l'intégralité du registre des réclamations déposées durant l'enquête publique relative au projet de remembrement de la commune de Ruffigné ; qu'il y a lieu d'enjoindre au ministre de l'agriculture, de l'alimentation, de la pêche, de la ruralité et de l'aménagement du territoire de procéder à cette communication dans un délai d'un mois à compter de la notification de la présente décision ;<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant que Mme B...a obtenu le bénéfice de l'aide juridictionnelle devant le Conseil d'Etat ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que Me Brouchot, avocat de MmeB..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'État le versement à Me Brouchot de la somme de 2 500 euros ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>				D E C I D E  :<br/>
              				--------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Nantes du 3 octobre 2006 est annulé.<br/>
<br/>
Article 2 : La décision du directeur départemental de l'agriculture et de la forêt de Loire-Atlantique refusant de communiquer à Mme B...l'intégralité du registre des réclamations déposées durant l'enquête publique relative au projet de remembrement de la commune de Ruffigné est annulée.<br/>
<br/>
Article 3 : Il est enjoint au ministre de l'agriculture, de l'alimentation, de la pêche, de la ruralité et de l'aménagement du territoire de communiquer à MmeB..., dans un délai d'un mois à compter de la notification de la présente décision, l'intégralité du registre des réclamations déposées durant l'enquête publique relative au projet de remembrement de la commune de Ruffigné.<br/>
<br/>
Article 4 : L'Etat versera à Me Brouchot, avocat de MmeB..., une somme de 2 500 euros en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cet avocat renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
<br/>
Article 5 : La présente décision sera notifiée à Mme A...B...et au ministre de l'agriculture, de l'alimentation, de la pêche, de la ruralité et de l'aménagement du territoire.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-05-012 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE EN PREMIER ET DERNIER RESSORT DES TRIBUNAUX ADMINISTRATIFS. - COMMUNICATION DE DOCUMENTS ADMINISTRATIFS - ARTICLE R. 222-13-3° DU CJA - COMMUNICATION D'INFORMATIONS ADMINISTRATIVES EFFECTUÉE SUR LE FONDEMENT DE L'ARTICLE L. 124-1 DU CODE DE L'ENVIRONNEMENT ET NON PAS SUR CELUI DE LA LOI DU 17 JUILLET 1978 - INCLUSION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">26-06-04 DROITS CIVILS ET INDIVIDUELS. ACCÈS AUX DOCUMENTS ADMINISTRATIFS. ACCÈS AUX INFORMATIONS EN MATIÈRE D'ENVIRONNEMENT. - 1) RENVOI DE L'ARTICLE L. 124-1 DU CODE DE L'ENVIRONNEMENT AUX CONDITIONS PRÉVUES PAR LE TITRE IER DE LA LOI DU 17 JUILLET 1978 - EXCEPTIONS AU DROIT DE COMMUNICATION PRÉVUES PAR LE II DE L'ARTICLE 6 DE CETTE LOI, NOTAMMENT LA PROTECTION DU SECRET DE LA VIE PRIVÉE - INCLUSION DANS LE CHAMP DU RENVOI - 2) COMMUNICATION DES REGISTRES D'ENQUÊTE PUBLIQUE -SECRET DE LA VIE PRIVÉE - VIOLATION - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">44-006-05-03 NATURE ET ENVIRONNEMENT. - COMMUNICATION DES REGISTRES D'ENQUÊTE PUBLIQUE - 1) ACCÈS AUX INFORMATIONS EN MATIÈRE D'ENVIRONNEMENT - RENVOI DE L'ARTICLE L. 124-1 DU CODE DE L'ENVIRONNEMENT AUX CONDITIONS PRÉVUES PAR LE TITRE IER DE LA LOI DU 17 JUILLET 1978 - EXCEPTIONS AU DROIT DE COMMUNICATION PRÉVUES PAR LE II DE L'ARTICLE 6 DE CETTE LOI, NOTAMMENT LA PROTECTION DU SECRET DE LA VIE PRIVÉE - INCLUSION DANS LE CHAMP DU RENVOI - 2) SECRET DE LA VIE PRIVÉE - VIOLATION - ABSENCE.
</SCT>
<ANA ID="9A"> 17-05-012 Les tribunaux administratifs sont compétents en premier et dernier ressort pour statuer sur les litiges se rapportant à une demande de communication de documents (sol. impl.), en vertu de l'article R. 222-13-3° du code de justice administrative (CJA). Ces dispositions leur donnent également compétence pour statuer sur des refus communication d'informations est effectuée sur le fondement de l'article L. 124-1 du code de l'environnement et non pas sur celui de la loi du 17 juillet 1978.</ANA>
<ANA ID="9B"> 26-06-04 1) Les informations en matière environnementale, au sens de l'article L. 124-1 du code de l'environnement, sont communicables dans les conditions prévues par l'ensemble du titre Ier de la loi du 17 juillet 1978, y compris les exceptions au droit de communication prévues par le II de l'article 6 de cette loi, notamment la protection du secret de la vie privée.... ...2) La communication intégrale du registre des réclamations déposées durant une enquête publique n'est pas par elle-même susceptible de porter atteinte au secret de la vie privée.</ANA>
<ANA ID="9C"> 44-006-05-03 1) Les informations en matière environnementale, au sens de l'article L. 124-1 du code de l'environnement, sont communicables dans les conditions prévues par l'ensemble du titre Ier de la loi du 17 juillet 1978, y compris les exceptions au droit de communication prévues par le II de l'article 6 de cette loi, notamment la protection du secret de la vie privée.... ...2) La communication intégrale du registre des réclamations déposées durant une enquête publique n'est pas par elle-même susceptible de porter atteinte au secret de la vie privée.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
