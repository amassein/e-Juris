<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033157848</ID>
<ANCIEN_ID>JG_L_2016_09_000000391638</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/15/78/CETATEXT000033157848.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 26/09/2016, 391638, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-09-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>391638</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Charline Nicolas</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHR:2016:391638.20160926</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              1° Sous le n° 391638, par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 9 juillet, 7 octobre 2015 et 16 mars 2016 au secrétariat du contentieux du Conseil d'Etat, la Fédération de l'administration générale de l'Etat - Force Ouvrière demande au Conseil d'Etat d'annuler pour excès de pouvoir le décret n° 2015-510 du 7 mai 2015 portant charte de la déconcentration.<br/>
<br/>
              2° Sous le n° 391639, par une requête sommaire, un mémoire complémentaire et deux mémoires en réplique, enregistrés les 9 juillet, 8 octobre 2015, 15 février et 16 mars 2016 au secrétariat du contentieux du Conseil d'Etat, la Fédération de l'équipement, de l'environnement, des transports et des services - Force Ouvrière (FEETS-FO) demande au Conseil d'Etat d'annuler pour excès de pouvoir le décret n° 2015-510 du 7 mai 2015 portant charte de la déconcentration.<br/>
<br/>
			....................................................................................<br/>
<br/>
              3° Sous le n° 391608, par une requête et un mémoire complémentaire, enregistrés les 8 juillet et 8 octobre 2015 au secrétariat du contentieux du Conseil d'Etat, la Fédération chimie énergie CFDT, le Syndicat Maine Anjou FCE-CFDT, le Syndicat national de l'environnement SNE-FSU et le Syndicat CGT de l'Agence de l'environnement et de la maîtrise de l'énergie (ADEME) demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le troisième alinéa de l'article 15 du décret n° 2015-510 du 7 mai 2015 portant charte de la déconcentration ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ; <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de l'environnement ;<br/>
              - le code du travail ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - la loi d'orientation n° 92-125 du 6 février 1992, notamment son article 6 ;<br/>
              - la loi n° 2010-751 du 5 juillet 2010 ; <br/>
              - le décret n° 2011-184 du 15 février 2011 ;<br/>
              - le décret n° 2012-225 du 16 février 2012 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Charline Nicolas, auditeur,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de la Fédération chimie énergie CFDT, du Syndicat Maine Anjou FCE-CFDT, du Syndicat SNE-FSU et du Syndicat CGT de l'ADEME ;<br/>
<br/>
              Vu les notes en délibéré, enregistrées le 22 septembre 2016, présentées par la Fédération générale de l'Etat - Force ouvrière et par la Fédération de l'équipement, de l'environnement, des transports et des services - Force ouvrière ;<br/>
<br/>
<br/>
<br/>1. Considérant que, par un décret du 7 mai 2015 portant charte de la déconcentration, le pouvoir réglementaire a abrogé le décret du 1er juillet 1992 qui avait le même objet et fixé les principes de l'organisation déconcentrée des services de l'Etat ; que, par trois requêtes distinctes, la Fédération chimie énergie CFDT, le Syndicat Maine Anjou FCE-CFDT, le Syndicat national de l'environnement SNE-FSU et le Syndicat CGT de l'Agence de l'environnement et de la maîtrise de l'énergie (ADEME), sous le n° 391608, la Fédération de l'administration générale de l'Etat-Force ouvrière (FAGE-FO), sous le n° 391638, et la Fédération de l'équipement, de l'environnement, des transports et des services - Force ouvrière (FEETS-FO), sous le n° 393639, en demandent l'annulation pour excès de pouvoir ; qu'il y a lieu de joindre ces trois requêtes, dirigés contre le même décret, pour statuer par une seule décision ;<br/>
<br/>
              Sur les conclusions des requêtes n°s 391638 et 391639 tendant à ce qu'il soit enjoint au Premier ministre de communiquer aux requérants l'avis de la section de l'administration du Conseil d'Etat sur le projet de décret ainsi que la note de son rapporteur :<br/>
<br/>
              2. Considérant que les stipulations de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales n'impliquent aucunement que soit communiqué au requérant qui demande l'annulation d'un décret l'avis rendu par la formation consultative du Conseil d'Etat sur le projet de texte qui lui a été soumis, non plus que la note du rapporteur ; qu'au demeurant, les membres du Conseil d'Etat qui participent au jugement du recours dirigé contre un tel acte ne peuvent pas davantage prendre connaissance de ces documents, dès lors qu'il n'ont pas été rendus publics par le Gouvernement, ainsi que le rappellent les dispositions de l'article R. 122-21-3 du code de justice administrative ;<br/>
<br/>
              Sur la recevabilité du mémoire en défense produit par la ministre de la décentralisation et de la fonction publique sous les n°s 391638 et 391639 :<br/>
<br/>
              3. Considérant que si le décret du 15 octobre 2015 nommant Mme A...B...directrice adjointe au directeur général de l'administration et de la fonction publique est affecté d'une erreur purement matérielle, dans la mesure où il mentionne une prise d'effet au 16 novembre 2016 et non au 16 novembre 2015, cette erreur est, en tout état de cause, sans incidence sur la recevabilité du mémoire en défense signé par l'intéressée, dont il est constant qu'elle était en fonction à la date à laquelle il a été produit ; <br/>
<br/>
              Sur la recevabilité des conclusions des requêtes n°s 391638 et 391639 : <br/>
<br/>
              4. Considérant que les conclusions des requêtes n°s 391638 et 391639 doivent être regardées comme étant dirigées contre les articles 1er, et 12 à 16 du décret attaqué ; que l'article 1er du décret attaqué, après avoir défini la notion de déconcentration, pose le principe selon lequel elle constitue la règle générale de répartition des attributions et des moyens entre les échelons centraux et territoriaux des administrations civiles de l'Etat et implique une action coordonnée des services déconcentrés et des services territoriaux des établissements publics de l'Etat ; qu'il résulte des statuts de la Fédération de l'administration générale de l'Etat - Force ouvrière et des statuts de la Fédération de l'équipement, de l'environnement, des transports et des services - Force ouvrière, que leur objet respectif est de défendre les intérêts des fonctionnaires et agents des administrations de l'Etat et des personnels des secteurs de l'équipement, de l'environnement, des transports et des services ; que les dispositions de l'article 1er, ne portent, par elles-mêmes, aucune atteinte aux droits et prérogatives des fonctionnaires et agents dont les syndicats requérants assurent la défense des intérêts collectifs, ni n'affectent leurs conditions d'emploi et de travail ; que, par suite, ces syndicats ne sont pas recevables à en demander l'annulation ;<br/>
<br/>
<br/>
<br/>
              Sur la légalité des autres articles du décret contesté :<br/>
<br/>
              Sur les moyens de légalité externe :<br/>
<br/>
              5. Considérant, en premier lieu, qu'aux termes de l'article 18 du décret du 16 février 2012 relatif au Conseil supérieur de la fonction publique de l'Etat : " L'ordre du jour des séances de l'assemblée plénière et des formations spécialisées et les documents y afférents doivent être adressés aux membres du Conseil supérieur par voie électronique au moins quinze jours avant la séance. Ce délai peut être ramené à huit jours en cas d'urgence (...) " ; qu'il ressort des pièces du dossier que les membres du conseil supérieur de la fonction publique de l'Etat ont reçu le 1er avril 2015, soit plus de quinze jours avant la première séance qui s'est tenue le 27 avril suivant, un courriel auquel était joint en pièce attachée le projet de décret ; que par suite, le moyen tiré du non-respect des délais prescrits manque en fait ; que les amendements apportés au projet en séance par le Gouvernement, destinés à lever les ambiguïtés du texte, ne soulevaient pas de questions nouvelles ; que les dispositions de l'article 22 du décret du 16 février 2012, aux termes desquelles " les amendements des membres du Conseil supérieur ayant voix délibérative doivent être présentés au plus tard le septième jour ouvrable précédant la date de l'examen par la formation spécialisée ou par l'assemblée plénière lorsqu'il est fait application du 1° du III de l'article 11 du présent décret " ne sont pas applicables aux amendements présentés par le Gouvernement ; que le seul fait que le procès-verbal de la séance au cours de laquelle a été examiné le projet de décret, produit par l'administration, ne soit pas revêtu de la signature du président, contrairement aux prescriptions de l'article 16 du règlement intérieur du Conseil supérieur, est par lui-même sans incidence sur la régularité de l'avis rendu ; qu'enfin, les requérants ne sauraient utilement soutenir que l'absence de publication de l'avis du Conseil d'Etat sur le projet de décret, lequel intervient en tout état de cause une fois toutes les consultations effectuées, aurait empêché le Conseil supérieur d'être pleinement éclairé ; qu'ainsi les moyens tirés de l'irrégularité de la procédure d'examen du texte suivie devant le Conseil supérieur de la fonction publique de l'Etat ne peuvent qu'être écartés ;<br/>
<br/>
              6. Considérant, en deuxième lieu, que les syndicats requérants, qui, ainsi qu'il a été dit au point 4, ne sont pas recevables à contester l'article 1er du décret du 7 mai 2015, ne peuvent utilement invoquer l'irrégularité tirée de ce que la consultation du Conseil supérieur de la fonction publique de l'Etat n'a pas porté sur cet article ; qu'en tout état de cause, les dispositions de l'article 1er du décret attaqué ne relèvent à aucun titre des questions sur lesquelles le Conseil supérieur doit être saisi pour avis en application de l'article 2 du décret du 16 février 2012, soit obligatoirement, soit parce que sa consultation se substitue à celle de plusieurs comités techniques ministériels ;<br/>
<br/>
              7. Considérant, en troisième lieu, que la circonstance que le projet de réforme a fait l'objet d'une communication en conseil des ministres le 22 avril 2015 ne saurait être regardée, eu égard au contenu de celle-ci, comme ayant privé de portée utile la consultation du Conseil supérieur de la fonction publique de l'Etat ; que les moyens tirés de qu'il aurait été porté atteinte à la liberté syndicale garantie par le préambule de la Constitution de 1946 et à l'exercice des pouvoirs des organisations syndicales représentatives définies par les lois du 11 janvier 1984 et du 10 juillet 2010 ne peuvent, par suite, qu'être écartés ;<br/>
<br/>
              8. Considérant, en quatrième lieu, qu'en vertu des dispositions de l'article L. 2321-1 du code du travail qui lui sont applicables en tant qu'établissement public industriel et commercial, l'Agence de l'environnement et de la maîtrise de l'énergie (ADEME) est dotée d'un comité d'entreprise, lequel, aux termes de l'article L. 2323-6 du même code, dans sa rédaction alors applicable, est " informé et consulté sur les questions intéressant l'organisation, la gestion et la marche générale de l'entreprise et, notamment, sur les mesures de nature à affecter le volume ou la structure des effectifs, la durée du travail, les conditions d'emploi, de travail et de formation professionnelle " ; que, toutefois, l'article 15 du décret attaqué, qui se borne à prévoir que, pour les établissements publics dont le préfet n'est pas le délégué territorial,  " le ou les préfets territorialement compétents sont consultés sur la désignation du responsable territorial de l'établissement public de l'Etat ainsi que sur son évaluation professionnelle ", ne saurait être regardé comme intéressant les conditions d'emploi et de travail des agents de l'ADEME au sens de ces dispositions ; que dès lors, le moyen tiré de ce que la procédure serait irrégulière, en l'absence de consultation du comité d'entreprise de l'ADEME sur les dispositions du troisième alinéa de l'article 15, ne peut qu'être écarté ;  <br/>
<br/>
              9. Considérant, en dernier lieu, qu'il ressort des pièces du dossier, et en particulier des pièces produites par la ministre en réponse à la mesure d'instruction ordonnée par la 7ème chambre, chargée de l'instruction de l'affaire, que la rédaction des articles du décret est conforme soit au projet initial du Gouvernement, soit au texte résultant de l'avis du Conseil d'Etat ; <br/>
<br/>
              Sur les moyens de légalité interne :<br/>
<br/>
              10. Considérant que l'article 12 du décret prévoit la possibilité pour le ministre compétent de déléguer au préfet les actes relatifs à la situation individuelle des agents placés sous son autorité, à l'exception de ceux qui sont soumis à l'avis préalable de la commission administrative paritaire compétente ; que l'article 13 est relatif aux mutualisations entre services déconcentrés et, le cas échéant, entre les services déconcentrés et les échelons territoriaux des établissements publics de l'Etat ; que l'article 14 permet aux préfets de décider conjointement qu'un service déconcentré de l'Etat puisse être chargé, en tout ou partie, d'une mission ou de la réalisation d'actes ou de prestations relevant de ses attributions pour le compte d'un autre service dont le ressort territorial peut différer du sien ; que l'article 15 est relatif à l'articulation entre services déconcentrés et échelons territoriaux des établissements publics ; que l'article 16 permet au préfet de région de proposer des dérogations aux règles fixées par les décrets relatifs à l'organisation des services déconcentrés de l'Etat placés sous son autorité et à la répartition des missions entre ces services ;<br/>
<br/>
              11. Considérant que ces différentes dispositions prévoient des mesures d'organisation du service qui, par elles-mêmes, ne sont pas susceptibles d'affecter les garanties statutaires dont bénéficient les fonctionnaires de l'Etat ; que, contrairement à ce qui est soutenu, elles ne portent aucune atteinte aux prérogatives du Parlement; qu'elles ne portent pas non plus atteinte au principe d'égalité de traitement des agents d'un même corps ; que le moyen tiré de ce qu'elles méconnaîtraient les dispositions applicables à certains corps n'est pas assorti des précisions permettant d'en apprécier le bien-fondé ; <br/>
<br/>
              12. Considérant que l'objet de l'article 12 du décret attaqué est précisément défini ; que les requérants ne sont pas fondés à soutenir qu'il méconnaîtrait le principe d'accessibilité et d'intelligibilité de la loi ; <br/>
<br/>
              13. Considérant que la participation, au demeurant facultative et soumise à une convention, des échelons territoriaux des établissements publics de l'Etat à des projets de mutualisation avec des services déconcentrés de l'Etat, telle que définie au II de l'article 13 du décret attaqué, n'a, contrairement à ce qui est soutenu, ni pour objet ni pour effet de placer l'échelon territorial d'un établissement public sous l'autorité hiérarchique du préfet ; <br/>
<br/>
              14. Considérant qu'ainsi qu'il a été dit au point 9, le troisième alinéa de l'article 15 prévoit que, pour les établissements publics dont le préfet n'est pas le délégué territorial, " le ou les préfets territorialement compétents sont consultés sur la désignation du responsable territorial de l'établissement public de l'Etat ainsi que sur son évaluation professionnelle " ; que contrairement à ce que soutiennent la Fédération chimie énergie CFDT et autres, ces dispositions, qui instituent un simple avis du préfet, ne confèrent pas à celui-ci un pouvoir hiérarchique au sein de l'établissement et, contrairement à ce qui est soutenu, n'affectent pas les règles constitutives de l'ADEME, telles qu'elles résultent de la loi du 19 décembre 1990, désormais codifiée dans le code de l'environnement ; que le moyen tiré de ce que seul le législateur était compétent, s'agissant de l'ADEME, pour prévoir une telle règle doit, par voie de conséquence, être écarté ; <br/>
<br/>
              15. Considérant qu'il résulte de tout ce qui précède, et sans qu'il soit besoin de statuer sur la fin de non-recevoir soulevée par le ministre sous le n° 391608, que les requêtes doivent être rejetées, y compris, sous le même numéro, les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les requêtes de la Fédération de l'administration générale de l'Etat - Force Ouvrière, de la Fédération de l'équipement, de l'environnement, des transports et des services - Force Ouvrière et de la Fédération chimie énergie CFDT et autres sont rejetées.<br/>
Article 2 : La présente décision sera notifiée à la Fédération de l'administration générale de l'Etat - Force Ouvrière, à la Fédération de l'équipement, de l'environnement, des transports et des services - Force Ouvrière, à la Fédération chimie énergie CFDT, au Syndicat Maine Anjou FCE-CFDT, au Syndicat national de l'environnement SNE-FSU, au Syndicat CGT de l'Agence de l'environnement et de la maîtrise de l'énergie et au ministre de l'intérieur et à la ministre de la décentralisation et de la fonction publique.<br/>
Copie en sera adressée pour information au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
