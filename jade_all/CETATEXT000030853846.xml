<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030853846</ID>
<ANCIEN_ID>JG_L_2015_06_000000370546</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/85/38/CETATEXT000030853846.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème SSJS, 29/06/2015, 370546, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-06-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>370546</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR>Mme Esther de Moustier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:370546.20150629</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 25 juillet 2013 au secrétariat du contentieux du Conseil d'Etat, du ministre délégué, chargé du budget ; il demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10MA00679, 10MA00680, 10MA03077 du 7 juin 2013 par lequel la cour administrative d'appel de Marseille, statuant sur les requêtes de M. et Mme B..., après avoir renvoyé au Conseil d'Etat leurs conclusions relatives à la taxe foncière et la taxe d'habitation, a annulé les jugements n° 0900401 et n° 0803753 du 15 décembre 2009 et le jugement n° 0903124 du 27 mai 2010 du tribunal administratif de Nîmes rejetant leur demande tendant à la décharge de l'obligation de payer les sommes de 184 925 euros procédant des dix commandements de payer émis le 27 août 2008 par le trésorier principal de Bagnols-sur-Cèze pour avoir paiement de cotisations d'impôt sur le revenu au titre des années 2000 à 2006, de contributions sociales à compter de l'année 2002, de cotisations de taxe d'habitation et de taxe foncière au titre des années 2003 à 2007, de 14 346 euros procédant de deux commandements de payer émis le 9 juin 2008 pour avoir paiement d'une cotisation d'impôt sur le revenu au titre de l'année 2003 et de cotisations de taxe foncière au titre de l'année 2004, et de 210 520,90 euros procédant de cinq avis à tiers détenteur émis le 14 mai 2009 pour le paiement de cotisations d'impôt sur le revenu des années 2000 à 2007, de cotisations de taxe foncière des années 2003 à 2008 et de cotisations de taxe d'habitation des années 2003 à 2008, et les a déchargés de l'obligation de payer les sommes de 184 925 euros, de 14 346 euros et de 210 520,90 euros ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M. et Mme B...; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de commerce ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Esther de Moustier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delaporte, Briard, Trichet, avocat de M. et Mme A...B...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un jugement du 20 octobre 1989 du tribunal de commerce de Nîmes, M. et Mme B... ont été placés en liquidation judiciaire ; que des commandements de payer et des avis à tiers détenteurs leur ont été décernés par le trésorier principal de Bagnols-sur-Cèze pour avoir paiement de diverses impositions portant sur l'impôt sur le revenu, les contributions sociales et les taxes locales mises à leur charge au titre des années 2000 à 2008 ; que le ministre se pourvoit en cassation contre l'arrêt du 7 juin 2013 par lequel la cour administrative d'appel de Marseille les a déchargés de l'obligation de payer les sommes correspondantes ;<br/>
<br/>
              2. Considérant que cette cour n'a pas répondu à la fin de non-recevoir que lui avait opposée l'administration fiscale, tirée de ce que M. et Mme B...ne pouvaient contester ces actes de recouvrement dès lors qu'ils étaient placés en liquidation judiciaire et que, par suite, seul le liquidateur avait compétence pour exercer cette contestation ; que, dès lors, le ministre est fondé pour ce motif, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, à demander l'annulation de l'arrêt qu'il attaque en ce qui concerne le recouvrement de l'impôt sur le revenu et des contributions sociales ; <br/>
<br/>
              3. Considérant par ailleurs qu'il ressort des termes mêmes de l'article R. 222-13 du code de justice administrative dans sa rédaction applicable au litige que l'expression  " recours relatifs aux taxes syndicales et aux impôts locaux autres que la taxe professionnelle " a une portée générale et qu'elle recouvre aussi bien les recours relatifs à l'assiette de ces prélèvements que ceux relatifs à leur recouvrement ; que, dès lors, l'article R. 811-1 du code de justice administrative dans sa rédaction alors applicable qui prévoyait que le tribunal administratif statuait en premier et dernier ressort dans les litiges relevant du 5° de l'article R. 222-13 du même code, était applicable au contentieux du recouvrement des impôts locaux autres que la taxe professionnelle ; que, par suite, les conclusions des requérants relatives aux cotisations de taxe d'habitation et de taxe foncière mises à leur charge ne pouvaient faire l'objet d'un appel ; que, l'arrêt de la cour doit, dès lors, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, être annulé en tant qu'il porte sur ces impositions ; qu'il y a lieu de regarder les conclusions relatives à ces impositions présentées devant la cour par M. et Mme B...comme des conclusions de cassation dirigées contre les trois jugements par lesquels le tribunal administratif a statué en premier et dernier ressort sur ces impositions ; qu'il n'y a toutefois plus lieu de statuer sur ces pourvois dès lors que, par deux décisions en date de ce jour, le Conseil d'Etat a statué sur ces pourvois que le président de la cour administrative d'appel de Marseille lui avait renvoyés par des ordonnances du 11 février 2013 ; <br/>
<br/>
              4. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que l'Etat, qui n'est pas la partie perdante, verse aux requérants la somme qu'ils  demandent sur leur fondement ;  <br/>
<br/>
<br/>
<br/>                           D E C I D E :<br/>
                                          --------------<br/>
Article 1er : L'arrêt du 7 juin 2013 de la cour administrative d'appel de Marseille est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille en ce qui concerne le recouvrement de l'impôt sur le revenu et des contributions sociales. <br/>
Article 3 : Il n'y a pas lieu de statuer sur les conclusions des pourvois de M. et Mme B...en ce qui concerne les taxes locales.      <br/>
Article 4 : Les conclusions de la requête de M. et Mme B...présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée au ministre des finances et des comptes publics  et à M. et Mme A...B....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
