<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032800967</ID>
<ANCIEN_ID>JG_L_2016_06_000000395321</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/80/09/CETATEXT000032800967.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère chambres réunies, 27/06/2016, 395321, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-06-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>395321</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR>M. Cyrille Beaufils</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:395321.20160627</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              1° Par deux mémoires, enregistrés les 14 avril et 2 juin 2016 au secrétariat du contentieux du Conseil d'État, le syndicat de la magistrature et le syndicat des avocats de France demandent au Conseil d'État, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 et à l'appui de leur requête tendant à l'annulation du décret n° 2015-1272 du 13 octobre 2015 pris pour l'application des articles 41-1-1 du code de procédure pénale et L. 132-10-1 du code de la sécurité intérieure, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution de l'article L. 132-10-1 du code de la sécurité intérieure. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              -	la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              -	l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              -	le code de procédure pénale ;<br/>
              -	le code de la sécurité intérieure ;<br/>
              -	la loi n° 2014-896 du 15 août 2014 ;<br/>
              -	la décision n° 2014-696 DC du 7 août 2014 du Conseil constitutionnel ;<br/>
              -	le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Cyrille Beaufils, auditeur,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Sevaux, Mathonnet, avocat du syndicat de la magistrature et autre ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) " ; qu'il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              Sur l'article L. 132-10-1 du code de la sécurité intérieure :<br/>
<br/>
              2. Considérant que l'article L. 132-10-1 du code de la sécurité intérieure, créé par la loi du 15 août 2014 relative à l'individualisation des peines et renforçant l'efficacité des sanctions pénales, dispose : " I. - Au sein du conseil départemental de prévention de la délinquance et, le cas échéant, de la zone de sécurité prioritaire, l'état-major de sécurité et la cellule de coordination opérationnelle des forces de sécurité intérieure sont chargés d'animer et de coordonner, sur leur territoire, les actions conduites par l'administration pénitentiaire, les autres services de l'Etat, les collectivités territoriales, les associations et les autres personnes publiques ou privées, en vue de favoriser l'exécution des peines et prévenir la récidive. / Dans le cadre de leurs attributions, l'état-major de sécurité et la cellule de coordination opérationnelle des forces de sécurité intérieure : / 1° Sont informés par le procureur de la République, au moins une fois par an, de la politique pénale mise en oeuvre sur leur territoire ; / 2° Examinent et donnent leur avis sur les conditions de mise en oeuvre des mesures prévues à l'article 41-1 du code de procédure pénale ; / 3° Organisent les modalités du suivi et du contrôle en milieu ouvert, par les services et personnes publiques ou privées mentionnés au premier alinéa du présent I, des personnes condamnées sortant de détention, désignées par l'autorité judiciaire compte tenu de leur personnalité, de leur situation matérielle, familiale et sociale ainsi que des circonstances de la commission des faits ; / 4° Informent régulièrement les juridictions de l'application des peines ainsi que le service pénitentiaire d'insertion et de probation des conditions de mise en oeuvre, dans le ressort, du suivi et du contrôle des personnes désignées en application du 3° du présent I et peuvent se voir transmettre par ces mêmes juridictions et ce même service toute information que ceux-ci jugent utile au bon déroulement du suivi et du contrôle de ces personnes. / II. - Les informations confidentielles échangées en application du I du présent article ne peuvent être communiquées à des tiers. / L'échange d'informations est réalisé selon les modalités prévues par un règlement intérieur établi par le conseil départemental de prévention de la délinquance sur la proposition des membres des groupes de travail mentionnés au premier alinéa. / III. - Les modalités d'application du présent article sont fixées par décret en Conseil d'Etat " ;<br/>
<br/>
              3. Considérant que l'article L. 132-10-1 du code de la sécurité intérieure est applicable au litige ; que ses dispositions n'ont pas déjà été déclarées conformes à la Constitution par le Conseil constitutionnel ; que le moyen tiré de ce qu'elles portent atteinte aux droits et libertés garantis par la Constitution, notamment en ce qu'elles méconnaissent la garantie des droits, qui découle de l'article 16 de la Déclaration des droits de l'homme et du citoyen, et en ce que le législateur n'a pas prévu les garanties nécessaires au respect du droit à la vie privée qui découle de l'article 2 de la Déclaration, soulève une question présentant un caractère sérieux ; qu'ainsi, il y a lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée en tant qu'elle porte sur l'article L. 132-10-1 du code de la sécurité intérieure ;<br/>
<br/>
              Sur l'article 41-1-1 du code de procédure pénale : <br/>
<br/>
              4. Considérant que l'article 41-1-1 du code de procédure pénale, également créé par la loi du 15 août 2014 relative à l'individualisation des peines et renforçant l'efficacité des sanctions pénales, dispose : " I. - L'officier de police judiciaire peut, tant que l'action publique n'a pas été mise en mouvement et sur autorisation du procureur de la République, transiger avec les personnes physiques et les personnes morales sur la poursuite : / 1° Des contraventions prévues par le code pénal, à l'exception des contraventions des quatre premières classes pour lesquelles l'action publique est éteinte par le paiement d'une amende forfaitaire en application de l'article 529 ; / 2° Des délits prévus par le code pénal et punis d'une peine d'amende ; / 3° Des délits prévus par le même code et punis d'un an d'emprisonnement au plus, à l'exception du délit d'outrage prévu au deuxième alinéa de l'article 433-5 dudit code ; / 4° Du délit prévu à l'article 311-3 du même code, lorsque la valeur de la chose volée est inférieure à un seuil fixé par décret ; / 5° Du délit prévu à l'article L. 3421-1 du code de la santé publique ; / 6° Du délit prévu au premier alinéa de l'article L. 126-3 du code de la construction et de l'habitation. / Lorsque le procureur de la République autorise le recours à la transaction en application du présent article, l'officier de police judiciaire peut soumettre l'auteur de l'infraction, compte tenu de ses ressources et de ses charges, à l'obligation de consigner une somme d'argent, en vue de garantir le paiement de l'amende mentionnée au 1° du II ou, le cas échéant, de l'amende prononcée en cas de poursuites et de condamnation dans les conditions prévues au dernier alinéa du III. / La transaction autorisée par le procureur de la République, proposée par l'officier de police judiciaire et acceptée par l'auteur de l'infraction est homologuée par le président du tribunal de grande instance ou par un juge par lui désigné, après avoir entendu, s'il y a lieu, l'auteur de l'infraction assisté, le cas échéant, par son avocat. / II. - La proposition de transaction est déterminée en fonction des circonstances et de la gravité de l'infraction, de la personnalité et de la situation matérielle, familiale et sociale de son auteur ainsi que de ses ressources et de ses charges. Elle fixe : / 1° L'amende transactionnelle due par l'auteur de l'infraction et dont le montant ne peut excéder le tiers du montant de l'amende encourue ; / 2° Le cas échéant, l'obligation pour l'auteur de l'infraction de réparer le dommage résultant de celle-ci ; / 3° Les délais impartis pour le paiement et, s'il y a lieu, l'exécution de l'obligation de réparer le dommage. / III. - L'acte par lequel le président du tribunal de grande instance ou le juge par lui désigné homologue la proposition de transaction est interruptif de la prescription de l'action publique. / L'action publique est éteinte lorsque l'auteur de l'infraction a exécuté dans les délais impartis l'intégralité des obligations résultant pour lui de l'acceptation de la transaction. / En cas de non-exécution de l'intégralité des obligations dans les délais impartis ou de refus d'homologation, le procureur de la République, sauf élément nouveau, met en oeuvre les mesures prévues à l'article 41-1 ou une composition pénale, ou engage des poursuites. / IV. - Les opérations réalisées par l'officier de police judiciaire en application des I et II du présent article sont relatées dans un seul procès-verbal. / V. - Les modalités d'application du présent article sont fixées par décret en Conseil d'Etat " ;<br/>
<br/>
              5. Considérant, que l'article 41-1-1 du code de procédure pénale est applicable au litige ; que ses dispositions n'ont pas déjà été déclarées conformes à la Constitution par le Conseil constitutionnel ; que le moyen tiré de ce qu'elles portent atteinte aux droits et libertés garantis par la Constitution, notamment au droit à un procès équitable et à la présomption d'innocence et au principe de légalité des délits et des peines et de la procédure pénale, soulève une question présentant un caractère sérieux ; qu'ainsi, il y a lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les questions de la conformité à la Constitution des articles L. 132-10-1 du code de la sécurité intérieure et 41-1-1 du code de procédure pénale sont renvoyées au Conseil constitutionnel.<br/>
<br/>
<br/>
Article 2 : Il est sursis à statuer sur les requêtes du syndicat de la magistrature, et du syndicat des avocats de France jusqu'à ce que le Conseil constitutionnel ait statué sur les questions prioritaires de constitutionnalité ainsi soulevées. <br/>
<br/>
Article 3 : La présente décision sera notifiée au syndicat de la magistrature, au syndicat des avocats de France et au garde des sceaux, ministre de la justice.<br/>
Copie en sera adressée au Premier ministre, au ministre de l'intérieur, au ministre des finances et des comptes publics, au ministre des outre-mer et au Conseil constitutionnel.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
