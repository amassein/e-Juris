<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026512052</ID>
<ANCIEN_ID>JG_L_2012_10_000000351833</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/51/20/CETATEXT000026512052.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  3ème sous-section jugeant seule, 17/10/2012, 351833, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-10-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>351833</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 3ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Jean Courtial</PRESIDENT>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Christine Allais</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:351833.20121017</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 9 août 2011 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre du budget, des comptes publics et de la réforme de l'Etat ; le ministre demande au Conseil d'État d'annuler l'arrêt n° 09PA07077 du 11 juillet 2011 en tant que, par cet arrêt, la cour administrative d'appel de Paris, faisant partiellement droit à l'appel formé par la société DF Presse contre le jugement n° 0514772/2 du 29 octobre 2009 du tribunal administratif de Paris, a déchargé cette société des suppléments d'impôt sur les sociétés et de contributions additionnelles à cet impôt auxquelles elle a été assujettie au titre de l'exercice clos le 31 décembre 1998 ;<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de procédure civile ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Christine Allais, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de la SCP Piwnica, Molinié, avocat de la société DF Presse, <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Piwnica, Molinié, avocat de la société DF Presse, <br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société DF Presse, qui exerce une activité d'édition de revues à caractère pornographique, a fait l'objet d'une vérification de comptabilité à l'issue de laquelle des redressements lui ont été notifiés, au titre de l'exercice 1998, par un exploit d'huissier du 28 décembre 2001 et, au titre de l'exercice 1999, par une lettre recommandée reçue le 6 mai 2002 ; que, par un jugement du 29 octobre 2009, le tribunal administratif de Paris a rejeté les conclusions de cette société tendant à la décharge des suppléments d'impôt sur les sociétés et de contributions additionnelles à cet impôt auxquels elle a été assujettie au titre des exercices 1998 et 1999, ainsi que des pénalités correspondantes ; que le ministre du budget, des comptes publics et de la réforme de l'Etat se pourvoit en cassation contre l'arrêt de la cour administrative d'appel de Paris du 11 juillet 2011 en tant que, par cet arrêt, la cour, faisant partiellement droit à l'appel formé par la société DF Presse contre le jugement du tribunal administratif de Paris, a déchargé cette société des suppléments d'impôt sur les sociétés et de contributions additionnelles à cet impôt auxquels elle a été assujettie au titre de l'exercice 1998 ;<br/>
<br/>
              2. Considérant, d'une part, qu'aux termes du premier alinéa de l'article L. 169 du livre des procédures fiscales : " Pour (...) l'impôt sur les sociétés, le droit de reprise de l'administration des impôts s'exerce jusqu'à la fin de la troisième année qui suit celle au titre de laquelle l'imposition est due " ; qu'aux termes de l'article L. 189 du même livre : " La prescription est interrompue par la notification d'une proposition de redressement " ; <br/>
<br/>
              3. Considérant, d'autre part, qu'aux termes de l'article 653 du nouveau code de procédure civile, dans sa rédaction applicable en 2001 : " La date de la signification d'un acte d'huissier de justice est celle du jour où elle est faite à personne, à domicile, à résidence ou à parquet ou, dans le cas mentionné à l'article 659, celle de l'établissement du procès-verbal " ; qu'aux termes de l'article 654 du même code : " La signification doit être faite à personne. / La signification à une personne morale est faite à personne lorsque l'acte est délivré à son représentant légal, à un fondé de pouvoir de ce dernier, ou à toute autre personne habilitée à cet effet " ; qu'aux termes de l'article 656 du même code, dans sa rédaction applicable en 2001 : " Si personne ne peut ou ne veut recevoir la copie de l'acte et s'il résulte des vérifications faites par l'huissier de justice et dont il sera fait mention dans l'acte de signification que le destinataire demeure bien à l'adresse indiquée, la signification est réputée faite à domicile ou à résidence. / Dans ce cas, l'huissier de justice est tenu de remettre copie de l'acte en mairie le jour même ou au plus tard le premier jour où les services de la mairie sont ouverts au public. Le maire, son délégué ou le secrétaire de mairie fait mention sur un répertoire de la remise et en donne récépissé. / L'huissier de justice laisse au domicile ou à la résidence du destinataire un avis de passage conformément à ce qui est prévu à l'article précédent. Cet avis mentionne que la copie de l'acte doit être retirée dans le plus bref délai à la mairie, contre récépissé ou émargement, par l'intéressé ou par toute personne spécialement mandatée (...) " ;<br/>
<br/>
              4. Considérant que les mentions d'un acte d'huissier font foi jusqu'à inscription de faux ; qu'il en va ainsi, notamment, de la mention relative à la date de la remise en mairie de la copie de l'acte faite par l'huissier de justice en application des dispositions précitées de l'article 656 du nouveau code de procédure civile ; <br/>
<br/>
              5. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que l'acte d'huissier du 28 décembre 2001 par lequel la société DF Presse s'est vu signifier la notification de redressement du 27 décembre 2001 relative à l'exercice 1998 mentionne que la copie de l'acte a été remise le jour même en mairie du 11ème arrondissement de Paris ; que la société DF Presse n'a présenté aucune demande d'inscription de faux contre cette pièce produite par l'administration ; que, dès lors, en jugeant, après avoir relevé que la copie de l'acte n'avait été déposée en mairie du 11ème arrondissement que le 2 janvier 2002 et que ceci ressortait d'une attestation établie par le secrétaire général adjoint de la mairie produite par la société DF Presse, que la notification de redressement ne pouvait être regardée comme ayant été régulièrement signifiée avant le 31 décembre 2001, date de l'expiration du délai de reprise de l'administration, la cour a dénaturé les pièces du dossier et commis une erreur de droit ; que, dès lors, l'article 1er de son arrêt doit être annulé ;<br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance, une somme à verser à la société DF Presse au titre des frais exposés par elle et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 1er de l'arrêt de la cour administrative d'appel de Paris du 11 juillet 2011 est annulé.<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Paris.<br/>
Article 3 : Les conclusions présentées par la société DF Presse au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au ministre de l'économie et des finances et à la société DF Presse.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
