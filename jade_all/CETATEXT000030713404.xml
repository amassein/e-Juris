<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030713404</ID>
<ANCIEN_ID>JG_L_2015_06_000000373600</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/71/34/CETATEXT000030713404.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème SSJS, 10/06/2015, 373600, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-06-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>373600</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR>M. Jacques Reiller</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:373600.20150610</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
<br/>
              M. et Mme A...B...ont demandé au tribunal administratif de Châlons-en-Champagne la décharge partielle des suppléments d'impôt sur le revenu auxquels ils ont été assujettis au titre de l'année 2000.<br/>
<br/>
              Par un jugement n° 0901198 du 24 mai 2012, le tribunal administratif de Châlons-en-Champagne a partiellement fait droit à leur demande.<br/>
<br/>
              Par un arrêt n° 12NC01615 du 3 octobre 2013, la cour administrative d'appel de Nancy a rejeté l'appel du ministre délégué, chargé du budget, dirigé contre ce jugement.<br/>
<br/>
              Par un pourvoi, enregistré le 29 novembre 2013 au secrétariat du contentieux du Conseil d'Etat, le ministre délégué, chargé du budget, demande au Conseil d'Etat d'annuler cet arrêt en tant que la cour administrative d'appel de Nancy a rejeté son recours.<br/>
<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jacques Reiller, Conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delaporte, Briard, Trichet, avocat de M. et Mme B...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'à la suite d'une vérification de comptabilité de la société en nom collectif Château-Gaillard (SNC), dont M. B...est l'un des associés, et d'un contrôle sur pièces des revenus déclarés par M. et MmeB..., l'administration a remis en cause les déductions que les contribuables, en se prévalant de l'article 163 tervicies du code général des impôts, avaient pratiquées sur leur revenu global de l'année 2000 correspondant à leur quote-part des dépenses réalisées par la SNC en Guadeloupe pour financer des travaux de plantation de bananeraies et l'achat de bovins ; que par jugement du 24 mai 2012, le tribunal administratif de Châlons-en-Champagne a accordé aux contribuables la réduction des cotisations supplémentaires d'impôt sur le revenu auxquelles ils ont été assujettis au titre de l'année 2000 à raison de la remise en cause de la déduction des dépenses de plantation et rejeté le surplus de leurs conclusions relatives aux dépenses d'achat du cheptel ; que, par arrêt du 3 octobre 2013, la cour administrative d'appel de Nancy a rejeté tant le recours du ministre délégué chargé du budget tendant à l'annulation de la réduction d'imposition prononcée par le tribunal administratif que les conclusions incidentes présentées par M. et Mme B... en vue d'obtenir la décharge entière des impositions litigieuses ;<br/>
<br/>
              Sur le pourvoi du ministre chargé du budget :<br/>
<br/>
              2. Considérant que l'article 163 tervicies du code général des impôts alors applicable dispose : " I. - Les contribuables peuvent déduire de leur revenu net global une somme égale au montant hors taxes des investissements productifs (...) qu'ils réalisent dans les départements et territoires d'outre-mer (...), dans le cadre d'une entreprise exerçant une activité dans les secteurs (...) de l'agriculture ; (...) IV. - Les dispositions du présent article (...) ne sont applicables qu'aux investissements neufs réalisés au plus tard le 31 décembre 2002 " ; que l'article 91 sexies de l'annexe II au même code pris pour l'application de l'article 163 tervicies précise : " Les investissements productifs réalisés dans les départements et territoires d'outre-mer et dans les collectivités territoriales de Mayotte et de Saint-Pierre-et-Miquelon dont le montant peut être déduit du revenu net global des contribuables en application du premier alinéa du I de l'article 163 tervicies du code général des impôts s'entendent des acquisitions ou créations d'immobilisations corporelles amortissables, affectées aux activités relevant des secteurs mentionnés au même alinéa " ; <br/>
<br/>
              3. Considérant, en premier lieu, d'une part, que les dispositions précitées de l'article 163 tervicies du code général des impôts, dès lors qu'elles se réfèrent sans restriction au secteur d'activité de l'agriculture, ne sauraient être interprétées, en l'absence d'indication de la volonté du législateur dans les travaux parlementaires ayant précédé leur adoption, comme limitant le bénéfice de l'avantage fiscal qu'elles prévoient aux investissements réalisés par des entreprises appartenant au secteur de la production agricole et par suite aux seuls exploitants agricoles ; que, d'autre part, la circonstance que l'investissement productif réalisé, à caractère immobilier, donne lieu à location ou sous-location est sans incidence sur le droit à bénéfice de la déduction au titre de l'impôt sur le revenu prévu par ces dispositions ; que, dès lors, c'est sans entacher son arrêt d'erreur de droit ni d'inexacte qualification juridique des faits que la cour, après avoir relevé que la SNC Château-Gaillard avait financé les travaux de plantation de bananeraies et avait donné en location ces bananeraies à des entreprises agricoles chargées d'en assurer l'exploitation, a jugé que les immobilisations ainsi réalisées par la SNC Château-Gaillard entraient dans le champ d'application du I de l'article 163 tervicies et que la circonstance que les investissements productifs réalisés avaient donné lieu à location était sans incidence sur le droit au bénéfice de leur déduction sur le revenu global net des associés à proportion de leurs droits dans la SNC Château-Gaillard ; <br/>
<br/>
              4. Considérant, en second lieu, que la cour n'a pas entaché son arrêt de dénaturation des pièces du dossier en estimant, au vu des factures établies au titre de l'année 2000 lors des travaux de réalisation des plantations, que les dépenses de préparation du sol, d'arrachage, de nettoyage, de parage et sortie des plants, d'amendements, de préémergence et de traitement par insecticides et nématicides étaient en relation directe avec la création des plantations ; qu'elle a pu en déduire, sans erreur de qualification juridique sur la nature de ces dépenses, qu'elles pouvaient donner lieu au bénéfice des dispositions de l'article 163 tervicies précitées ;<br/>
<br/>
              5. Considérant qu'il suit de ce qui précède que le ministre chargé du budget n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque ;<br/>
<br/>
              Sur les conclusions incidentes de M. et MmeB... :<br/>
<br/>
              6. Considérant qu'il ressort des pièces soumises aux juges du fond que, par lettre du 9 novembre 2006, la SNC Château-Gaillard a demandé la saisine de la commission départementale des impôts directs et des taxes sur le chiffre d'affaires en ce qui concerne les dépenses relatives à l'achat de bovins ; que le désaccord entre la société et l'administration portait seulement sur la réalité de ces acquisitions et l'identification de ces cheptels ; qu'en jugeant que ces questions ne relevaient pas de la compétence de la commission départementale, la cour a commis une erreur de droit au regard de l'article L. 59 A du livre des procédures fiscales ; que, dès lors et sans qu'il soit besoin d'examiner l'autre moyen soulevé au soutien des conclusions incidentes, l'arrêt de la cour doit, dans cette mesure, être annulé ; <br/>
<br/>
              7. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              8. Considérant qu'il résulte de l'instruction que la société Château-Gaillard a demandé, par lettre du 6 novembre 2006 reçue le 15 novembre par l'administration, que la commission départementale des impôts soit saisie du litige relatif à la matérialité des investissements relatifs à l'achat des bovins ; que ce litige portait sur des questions de fait relevant de la compétence de la commission départementale ; que dès lors le défaut de saisine de la commission départementale, qui a privé les requérants d'une garantie, a entaché d'irrégularité la procédure d'imposition en ce qui concerne ce chef de redressement ; qu'il résulte de ce qui précède que M. et Mme B...sont fondés à demander  l'entière décharge des compléments d'impôt sur le revenu auxquels ils ont été assujettis au titre de l'année 2000 ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative pour l'ensemble de l'instance ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du ministre chargé du budget est rejeté.<br/>
Article 2 : L'arrêt n° 12NC01615 de la cour administrative d'appel de Nancy en date du 3 octobre 2013 est annulé en tant qu'il rejette les conclusions incidentes de M. et MmeB....<br/>
Article 3 : M. et Mme B...sont déchargés des compléments d'impôts sur le revenu auxquels ils restent assujettis au titre de l'année 2000.<br/>
Article 4 : Le jugement n° 0901198 du tribunal administratif de Châlons-en-Champagne en date du 24 mai 2012 est réformé à ce qu'il a de contraire à l'article 3 ci-dessus.  <br/>
Article 5 : L'Etat versera à M. et Mme B...la somme de 4 000 euros en application de l'article L. 761-1 du code de justice administrative.<br/>
Article 6 : La présente décision sera notifiée au ministre des finances et des comptes publics et à M. et Mme A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
