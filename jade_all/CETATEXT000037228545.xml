<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037228545</ID>
<ANCIEN_ID>JG_L_2018_07_000000418185</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/22/85/CETATEXT000037228545.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 18/07/2018, 418185, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-07-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418185</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP RICHARD</AVOCATS>
<RAPPORTEUR>M. Jean-François de Montgolfier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:418185.20180718</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              Procédure contentieuse antérieure<br/>
<br/>
              Mme C...a porté plainte contre M. B... A...devant la chambre disciplinaire de première instance du Centre-Val-de-Loire de l'ordre des médecins. Le conseil départemental d'Indre-et-Loire de l'ordre des médecins s'est associé à la plainte. Par une décision du 4 juillet 2016, la chambre disciplinaire de première instance a infligé à M. A... la sanction d'interdiction d'exercer la médecine pour une durée de trois mois, assortis du sursis.<br/>
<br/>
              Par une décision n° 323 du 22 décembre 2017, la chambre disciplinaire nationale de l'ordre des médecins a, sur appel du conseil départemental d'Indre-et-Loire de l'ordre des médecins, infligé à M. A... la sanction d'interdiction d'exercer la médecine pour une durée de trois mois.<br/>
<br/>
              Procédures devant le Conseil d'Etat<br/>
<br/>
              1° Sous le n° 418185, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 14 février et 18 avril 2018 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel du conseil départemental d'Indre-et-Loire de l'ordre des médecins ;<br/>
<br/>
              3°) de mettre à la charge du conseil départemental d'Indre-et-Loire de l'ordre des médecins la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              2° Sous le n° 418359, par une requête et un nouveau mémoire, enregistrés les 20 février et 18 avril 2018 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat d'ordonner, en application de l'article R. 821-5 du code de justice administrative, qu'il soit sursis à l'exécution de la même décision du 22 décembre 2017 de la chambre disciplinaire nationale de l'ordre des médecins.<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-François de Montgolfier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Richard, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>1.  Considérant que le pourvoi par lequel M. A... demande l'annulation de la décision de la chambre disciplinaire nationale de l'ordre des médecins du 22 décembre 2017 et sa requête tendant à ce qu'il soit sursis à l'exécution de cette décision présentent à juger les mêmes questions ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              2.  Considérant qu'aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux " ;<br/>
<br/>
              3.  Considérant que, pour demander l'annulation de la décision de la chambre disciplinaire nationale de l'ordre des médecins qu'il attaque, M. A... soutient qu'elle est entachée d'insuffisance de motivation en ce qu'elle se fonde sur une absence de " résultats validés " d'examens de biologie médicale, sans les définir ; qu'elle est entachée de dénaturation des pièces du dossier en ce qu'elle estime qu'il a délibérément inscrit une mention erronée dans la prescription de scanner litigieuse ; qu'elle est entachée d'inexacte qualification juridique des faits en ce qu'elle juge qu'il a manqué à son obligation de dispenser des soins consciencieux, qu'il a exposé sa patiente à un risque injustifié et qu'il a manqué à son obligation de moralité ; qu'elle prononce une sanction hors de proportion avec les fautes reprochées ;<br/>
<br/>
              4.  Considérant qu'aucun de ces moyens n'est de nature à permettre l'admission du pourvoi ;<br/>
<br/>
              5.  Considérant que le pourvoi formé par M. A... contre la décision du 22 décembre 2017 de la chambre disciplinaire nationale de l'ordre des médecins n'étant pas admis, les conclusions qu'il présente aux fins de sursis à exécution de cette décision sont devenues sans objet ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
        Article 1er : Le pourvoi de M. A... n'est pas admis.<br/>
<br/>
        Article 2 : Il n'y a pas lieu de statuer sur la requête de M.A....<br/>
<br/>
        Article 3 : La présente décision sera notifiée à M. B... A....<br/>
         Copie en sera adressée au conseil départemental d'Indre-et-Loire de l'ordre des médecins.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
