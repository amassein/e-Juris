<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038166179</ID>
<ANCIEN_ID>JG_L_2019_02_000000417334</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/16/61/CETATEXT000038166179.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 25/02/2019, 417334, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-02-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417334</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Guillaume Leforestier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:417334.20190225</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Lille d'annuler la décision du 9 février 2017 par laquelle le ministre de l'intérieur a refusé de lui accorder la reconstitution de quatre points de son permis de conduire à la suite d'un stage de sensibilisation à la sécurité routière effectué les 9 et 10 novembre 2012 et d'enjoindre au ministre de valider son stage et de créditer son permis de conduire de douze points. Par un jugement n° 1702443 du 16 novembre 2017, le tribunal administratif a rejeté sa demande.   <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés au secrétariat du contentieux du Conseil d'Etat les 15 janvier et 12 avril 2018, M. B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;    <br/>
<br/>
              2°)  réglant l'affaire au fond, de faire droit à sa demande ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de    l'article L. 761-1 du code de justice administrative.    <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la route ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Leforestier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de M.B....<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que M. B...a suivi les 9 et 10 novembre 2012 un stage de sensibilisation à la sécurité routière et a demandé à bénéficier de la reconstitution de points prévue à l'avant-dernier alinéa de l'article L. 223-6 du code de la route. Cette reconstitution lui ayant été refusée au motif qu'une décision constatant la perte de validité de son permis de conduire pour solde de points nul lui avait été notifiée le 5 novembre 2012, il a, le 9 janvier 2013, formé un recours gracieux devant le préfet du Pas-de-Calais en faisant valoir que la notification avait été faite à une adresse où il ne résidait plus. Par un courrier du 5 février suivant, le préfet a pris acte de l'erreur commise et indiqué qu'il en informait le fichier national du permis de conduire en vue d'une reconstitution de points, à laquelle il serait procédé sauf éléments contraires non connus de ses services. Ayant découvert que la validité de son permis n'avait pas été rétablie, M. B...a, le 7 juin 2016, puis à nouveau le 17 janvier 2017, saisi le ministre de l'intérieur qui, par une décision du 9 février suivant, a refusé de procéder à la reconstitution de points. L'intéressé se pourvoit en cassation contre le jugement du 16 novembre 2017 par lequel le tribunal administratif de Lille a rejeté sa demande tendant à l'annulation de cette décision. <br/>
<br/>
              2. Pour rejeter la demande de M.B..., le tribunal administratif s'est notamment fondé sur la circonstance qu'il n'avait pas exercé de recours contentieux dans un délai raisonnable à compter du 9 janvier 2013, date de présentation d'un recours gracieux qui révélait qu'il avait eu connaissance de la décision constatant la perte de validité de son permis de conduire pour solde de points nul. En se prononçant ainsi, alors que le préfet, par sa décision du 5 février 2013, avait accueilli le recours gracieux en indiquant qu'il serait, sauf élément non connu de ses services, procédé à la reconstitution de points sollicitée, si bien que la présentation d'un recours contentieux ne se justifiait pas, le juge du fond a dénaturé les éléments qui lui étaient soumis. <br/>
<br/>
              3. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à M. B...de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Lille du 16 novembre 2017 est annulé.  <br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Lille. <br/>
Article 3 : L'Etat versera à M. B...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : La présente décision sera notifiée à M. A...B...et au ministre de l'intérieur. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
