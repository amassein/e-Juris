<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027345144</ID>
<ANCIEN_ID>JG_L_2013_04_000000357562</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/34/51/CETATEXT000027345144.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 22/04/2013, 357562</TITRE>
<DATE_DEC>2013-04-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>357562</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:357562.20130422</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu l'ordonnance n° 12VE00507 du 9 mars 2012, enregistrée le 13 mars 2012 au secrétariat du contentieux du Conseil d'Etat, par laquelle le président de la cour administrative d'appel de Versailles a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête de M.A... ;<br/>
<br/>
              Vu la requête, enregistrée le 9 février 2012 au greffe de la cour administrative d'appel de Versailles, présentée par M. B... A..., demeurant... ; M. A... demande l'annulation du jugement n° 1107966 du 8 décembre 2011 par lequel le tribunal administratif de Cergy-Pontoise, statuant sur la saisine de la Commission nationale des comptes de campagne et des financements politiques en application des articles L. 52-15 et L. 118-3 du code électoral, l'a déclaré inéligible aux fonctions de conseiller général pour une durée d'un an ; <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu les pièces desquelles il ressort que la requête a été communiquée à la Commission nationale des comptes de campagne et des financements politiques, qui n'a pas produit de mémoire ; <br/>
<br/>
              Vu les pièces desquelles il ressort que, par application des dispositions de l'article R. 611-7 du code de justice administrative, les parties ont été informées que la décision du Conseil d'Etat était susceptible d'être fondée sur le moyen, relevé d'office, tiré de ce qu'était applicable au litige la nouvelle rédaction de l'article L. 118-3 du code électoral issue de la loi du 14 avril 2011 portant simplification de dispositions du code électoral et relative à la transparence financière de la vie politique ;<br/>
<br/>
              Vu le code électoral ; <br/>
<br/>
              Vu la loi n° 2011-412 du 14 avril 2011 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, Maître des Requêtes,  <br/>
<br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que, par une décision du 15 septembre 2011, la Commission nationale des comptes de campagne et des financements politiques a constaté que M. A..., candidat au premier tour des élections qui se sont déroulées les 20 et 27 mars 2011 en vue de la désignation du conseiller général du canton de Magny-en-Vexin (Val-d'Oise), n'avait pas déposé son compte de campagne, en violation de l'article L. 52-12 du code électoral ; que, saisi en application de l'article L. 52-15 du même code, le tribunal administratif de Cergy-Pontoise a déclaré M. A...inéligible pour une durée d'un an, par un jugement du 8 décembre 2011 : que M. A... fait appel de ce jugement ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 52-12 du code électoral, dans sa rédaction issue de l'article 10 de la loi du 14 avril 2011 portant simplification de dispositions du code électoral et relative à la transparence financière de la vie politique : " Chaque candidat ou candidat tête de liste soumis au plafonnement prévu à l'article L. 52-11 et qui a obtenu au moins 1 % des suffrages exprimés est tenu d'établir un compte de campagne (...) Au plus tard avant 18 heures le dixième vendredi suivant le premier tour de scrutin, chaque candidat ou candidat tête de liste présent au premier tour dépose à la Commission nationale des comptes de campagne et des financements politiques son compte de campagne et ses annexes accompagné des justificatifs de ses recettes ainsi que des factures, devis et autres documents de nature à établir le montant des dépenses payées ou engagées par le candidat ou pour son compte. (...) Cette présentation n'est pas nécessaire lorsque aucune dépense ou recette ne figure au compte de campagne. Dans ce cas, le mandataire établit une attestation d'absence de dépense et de recette " ; <br/>
<br/>
              3. Considérant que le manquement à l'obligation de déposer un compte de campagne est constitué à la date à laquelle expire le délai imparti au candidat pour procéder à ce dépôt, lequel  est impératif et ne peut être prorogé ; qu'en l'espèce le délai imparti à M. A...pour déposer son compte de campagne expirait le 27 mai 2011, postérieurement à l'entrée en vigueur de la loi du 14 avril 2011 visée ci-dessus ; que la sanction applicable à ce manquement doit dès lors être définie par application des dispositions de l'article L. 118-3 du code électoral dans sa rédaction issue de cette loi ; qu'aux termes de ces dispositions : " Saisi par la commission instituée par l'article L. 52-14, le juge de l'élection peut déclarer inéligible le candidat dont le compte de campagne, le cas échéant après réformation, fait apparaître un dépassement du plafond des dépenses électorales. / Saisi dans les mêmes conditions, le juge de l'élection peut déclarer inéligible le candidat qui n'a pas déposé son compte de campagne dans les conditions et le délai prescrits à l'article L. 52-12. / Il prononce également l'inéligibilité du candidat dont le compte de campagne a été rejeté à bon droit en cas de volonté de fraude ou de manquement d'une particulière gravité aux règles relatives au financement des campagnes électorales. / L'inéligibilité déclarée sur le fondement des premier à troisième alinéas est prononcée pour une durée maximale de trois ans et s'applique à toutes les élections. Toutefois, elle n'a pas d'effet sur les mandats acquis antérieurement à la date de la décision (...) " ; <br/>
<br/>
              4. Considérant qu'il appartient au juge de l'élection, pour apprécier s'il y a lieu de faire usage de la faculté donnée par ces dispositions de déclarer inéligible un candidat qui n'a pas déposé son compte de campagne dans les conditions et délai prescrits à l'article L. 52-12 du code électoral, de tenir compte de la nature de la règle méconnue, du caractère délibéré ou non du manquement, de l'existence éventuelle d'autres motifs d'irrégularité du compte, du montant des sommes en cause ainsi que de l'ensemble des circonstances de l'espèce ; <br/>
<br/>
              5. Considérant qu'il résulte de l'instruction que M. A...a obtenu 1,46 % des suffrages exprimés ; qu'il est constant qu'il n'a pas déposé son compte de campagne dans le délai imparti par les dispositions de l'article L. 52-12 du code électoral ; qu'il a ainsi méconnu une obligation substantielle ; qu'il fait valoir que l'état de santé et le décès de son père l'ont conduit à quitter sa région de résidence de mai 2011 au 16 août 2011 et à différer, puis à oublier, le dépôt de ce compte ; que les circonstances ainsi invoquées, dont il ne résulte pas de l'instruction qu'elles soient inexactes, n'étaient toutefois pas de nature à le mettre dans l'impossibilité de procéder à ce dépôt avant l'expiration du délai imparti ; que le compte de campagne qu'il a produit le 11 octobre 2011 à l'appui de son mémoire en défense devant le tribunal administratif retrace un ensemble de dépenses et de recettes pour un montant de 1 256,30 euros, dont 1 156,30 euros ont été pris en charge par sa formation politique et 100 euros par lui-même ; qu'il ne résulte de l'instruction ni que ces indications soient inexactes ni que le candidat ait commis d'autres irrégularités au regard des règles de financement des campagnes électorales que l'omission du dépôt de son compte ; qu'au vu de l'ensemble de ces circonstances, il y a lieu de ramener l'inéligibilité prononcée par le tribunal administratif à une durée de six mois ; <br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que M. A... est seulement fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Cergy-Pontoise a prononcé son inéligibilité pour une durée supérieure à six mois ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : M. A...est déclaré inéligible pour une durée de six mois à compter de la date de la présente décision.<br/>
<br/>
Article 2 : Le jugement du 8 décembre 2011 du tribunal administratif de Cergy-Pontoise est réformé en ce qu'il a de contraire à la présente décision.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. B...A...et à la Commission nationale des comptes de campagne et des financements politiques. <br/>
		Copie pour information en sera adressée au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">28-005-04-04 ÉLECTIONS ET RÉFÉRENDUM. DISPOSITIONS GÉNÉRALES APPLICABLES AUX ÉLECTIONS. FINANCEMENT ET PLAFONNEMENT DES DÉPENSES ÉLECTORALES. PORTÉE DE L'INÉLIGIBILITÉ. - ABSENCE DE DÉPÔT DU COMPTE DE CAMPAGNE DANS LES CONDITIONS ET DÉLAI PRESCRITS À L'ARTICLE L. 52-12 DU CODE ÉLECTORAL (ART. L. 118-3 DU MÊME CODE) - FACULTÉ DE PRONONCER L'INÉLIGIBILITÉ - ESPÈCE [RJ1].
</SCT>
<ANA ID="9A"> 28-005-04-04 Il appartient au juge de l'élection, pour apprécier s'il y a lieu de faire usage de la faculté donnée par les dispositions de l'article L. 118-3 du code électoral de déclarer inéligible un candidat qui n'a pas déposé son compte de campagne dans les conditions et délai prescrits à l'article L. 52-12 du code électoral, de tenir compte de la nature de la règle méconnue, du caractère délibéré ou non du manquement, de l'existence éventuelle d'autres motifs d'irrégularité du compte, du montant des sommes en cause ainsi que de l'ensemble des circonstances de l'espèce. Candidat n'ayant pas déposé de compte de campagne dans le délai imparti par les dispositions de l'article L. 52-12 du code électoral. Il s'agit d'un manquement à une obligation substantielle, que les circonstances particulières invoquées (décès du père du candidat pendant le délai de dépôt) ne suffisent pas à expliquer, avec des dépenses de campagne d'un montant de 1 256 euros, en l'absence d'autres irrégularités commises par le candidat. Inéligibilité de six mois.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. CE, 12 avril 2013, Commission nationale des comptes de campagne et des financements politiques c/ M.,, n° 364071, à mentionner aux Tables ; CE, 22 avril 2013, Commission nationale des comptes de campagne et des financements politiques c/ M.,(Election cantonale des Sables-d'Olonne), n° 360590, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
