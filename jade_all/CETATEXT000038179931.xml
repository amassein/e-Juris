<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038179931</ID>
<ANCIEN_ID>JG_L_2019_02_000000413556</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/17/99/CETATEXT000038179931.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 27/02/2019, 413556</TITRE>
<DATE_DEC>2019-02-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>413556</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, SOLTNER, TEXIDOR, PERIER ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Olivier  Fuchs</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:413556.20190227</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Nice d'annuler pour excès de pouvoir la décision du 11 mars 2015 par laquelle l'inspecteur du travail de la 7ème section de l'unité de contrôle n° 1 des Alpes-Maritimes a autorisé la société Vacation Rental à le licencier et celle du ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social du 15 octobre 2015 ayant rejeté son recours hiérarchique. Par un jugement n° 1501912, 1505045 du 1er mars 2016, le tribunal administratif a annulé ces deux décisions.<br/>
<br/>
              Par un arrêt n° 16MA01132 du 29 juin 2017, la cour administrative d'appel de Marseille a rejeté l'appel formé par la société Vacation Rental contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 21 août, 21 novembre et 5 décembre 2017 au secrétariat du contentieux du Conseil d'Etat, la société Vacation Rental demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de M. B...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Fuchs, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Soltner, Texidor, Périer, avocat de la société Vacation Rental et à la SCP Waquet, Farge, Hazan, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 11 mars 2015, l'inspecteur du travail de la 7ème section de l'unité de contrôle n° 1 des Alpes-Maritimes a autorisé la société Vacation Rental à licencier M.B..., salarié protégé, pour motif disciplinaire. Par une décision du 15 octobre 2015, le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social a rejeté le recours hiérarchique formé par M. B...contre cette décision. Par un jugement du 1er mars 2016, le tribunal administratif de Nice a annulé pour excès de pouvoir ces deux décisions. La société Vacation Rental se pourvoit en cassation contre l'arrêt du 29 juin 2017 par lequel la cour administrative d'appel de Marseille a rejeté son appel contre ce jugement.<br/>
<br/>
              2. Aux termes de l'article R. 2421-14 du code du travail, dans sa rédaction alors applicable : " En cas de faute grave, l'employeur peut prononcer la mise à pied immédiate de l'intéressé jusqu'à la décision de l'inspecteur du travail. / La consultation du comité d'entreprise a lieu dans un délai de dix jours à compter de la date de la mise à pied. / La demande d'autorisation de licenciement est présentée dans les quarante-huit heures suivant la délibération du comité d'entreprise. S'il n'y a pas de comité d'entreprise, cette demande est présentée dans un délai de huit jours à compter de la date de la mise à pied (...) ". Les délais, fixés par ces dispositions, dans lesquels la demande d'autorisation de licenciement d'un salarié mis à pied doit être présentée, ne sont pas prescrits à peine de nullité de la procédure de licenciement. Toutefois, eu égard à la gravité de la mesure de mise à pied, l'employeur est tenu de respecter un délai aussi court que possible pour la présenter. Par suite, il appartient à l'administration, saisie par l'employeur d'une demande d'autorisation de licenciement d'un salarié protégé auquel s'appliquent ces dispositions, de s'assurer que ce délai a été, en l'espèce, aussi court que possible pour ne pas entacher d'irrégularité la procédure antérieure à sa saisine.<br/>
<br/>
              3. En premier lieu, la cour administrative d'appel, qui a estimé, par une appréciation souveraine non contestée en cassation, que le délai de vingt-et-un jours entre la date de mise à pied du salarié et la saisine de l'inspecteur du travail était excessif, a pu en déduire, sans erreur de droit, que cette irrégularité faisait obstacle à ce que l'autorité administrative autorise le licenciement litigieux. A ce titre, contrairement à ce que soutient la société requérante, la cour n'avait pas à rechercher si cette irrégularité avait privé le salarié d'une garantie ou eu une influence sur le sens de la décision administrative attaquée, dès lors que le moyen tiré de la méconnaissance du délai de huit jours fixé par l'article R. 2421-14 du code du travail met en cause la légalité interne de la décision prise par l'inspecteur du travail.<br/>
<br/>
              4. En deuxième lieu, il résulte des termes mêmes de l'article R. 2421-14 du code du travail que les délais fixés par ces dispositions commencent à courir à compter de la date de mise à pied du salarié protégé.  La cour n'a donc pas commis d'erreur de droit en jugeant que le délai de huit jours prévu par cet article avait commencé à courir le 5 janvier 2015, dès lors qu'il ressortait des pièces du dossier qui lui était soumis que cette date était celle à laquelle M. B... avait été mis à pied par son employeur et que l'intéressé avait, à cette date, la qualité de salarié protégé, en raison de la connaissance qu'avait l'employeur, depuis le 8 décembre 2014, de sa candidature aux élections de délégués du personnel dont il demandait l'organisation.<br/>
<br/>
              5. Enfin, il ressort des pièces du dossier qui lui était soumis que la cour administrative d'appel a pu, par une appréciation souveraine exempte de dénaturation, estimer que le caractère douteux de la sincérité de la candidature de M. B... n'était pas établi par l'employeur. Eu égard à l'argumentation développée devant elle, la cour a suffisamment motivé son arrêt sur ce point.<br/>
<br/>
              6. Il résulte de tout ce qui précède que le pourvoi de la société Vacation Rental doit être rejeté, y compris, par voie de conséquence, ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative. En revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Vacation Rental une somme de 3 000 euros à verser à M. B...au titre de ces mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société Vacation Rental est rejeté.<br/>
Article 2 : La société Vacation Rental versera à M. B...une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société Vacation Rental et à M. A...B....<br/>
Copie en sera adressée à la ministre du travail.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-07-01-04 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. - MOYEN RELEVANT DE LA LÉGALITÉ INTERNE DE LA DÉCISION PRISE PAR L'INSPECTEUR DU TRAVAIL RELATIVE À L'AUTORISATION DE LICENCIEMENT D'UN SALARIÉ PROTÉGÉ - MOYEN TIRÉ DE LA MÉCONNAISSANCE DU DÉLAI DE SAISINE DE L'ADMINISTRATION PAR L'EMPLOYEUR (ART. R. 2421-14 DU CODE DU TRAVAIL) [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">66-07-01-02 TRAVAIL ET EMPLOI. LICENCIEMENTS. AUTORISATION ADMINISTRATIVE - SALARIÉS PROTÉGÉS. PROCÉDURE PRÉALABLE À L'AUTORISATION ADMINISTRATIVE. - MOYEN RELEVANT DE LA LÉGALITÉ INTERNE DE LA DÉCISION PRISE PAR L'INSPECTEUR DU TRAVAIL RELATIVE À L'AUTORISATION DE LICENCIEMENT D'UN SALARIÉ PROTÉGÉ - MOYEN TIRÉ DE LA MÉCONNAISSANCE DU DÉLAI DE SAISINE DE L'ADMINISTRATION PAR L'EMPLOYEUR (ART. R. 2421-14 DU CODE DU TRAVAIL) [RJ1].
</SCT>
<ANA ID="9A"> 54-07-01-04 Une cour administrative d'appel, qui a estimé que le délai de vingt-et-un jours entre la date de mise à pied du salarié et la saisine de l'inspecteur du travail était excessif, a pu en déduire, sans erreur de droit, que cette irrégularité faisait obstacle à ce que l'autorité administrative autorise le licenciement litigieux. A ce titre, contrairement à ce que soutient la société requérante, la cour n'avait pas à rechercher si cette irrégularité avait privé le salarié d'une garantie ou eu une influence sur le sens de la décision administrative attaquée, dès lors que le moyen tiré de la méconnaissance du délai de huit jours fixé par l'article R. 2421-14 du code du travail met en cause la légalité interne de la décision prise par l'inspecteur du travail.</ANA>
<ANA ID="9B"> 66-07-01-02 Une cour administrative d'appel, qui a estimé que le délai de vingt-et-un jours entre la date de mise à pied du salarié et la saisine de l'inspecteur du travail était excessif, a pu en déduire, sans erreur de droit, que cette irrégularité faisait obstacle à ce que l'autorité administrative autorise le licenciement litigieux. A ce titre, contrairement à ce que soutient la société requérante, la cour n'avait pas à rechercher si cette irrégularité avait privé le salarié d'une garantie ou eu une influence sur le sens de la décision administrative attaquée, dès lors que le moyen tiré de la méconnaissance du délai de huit jours fixé par l'article R. 2421-14 du code du travail met en cause la légalité interne de la décision prise par l'inspecteur du travail.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant d'un moyen tiré de la consultation irrégulière du comité d'entreprise, CE, 12 juillet 1995,,, n° 154219, T. pp. 994-1061-1064.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
