<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043411147</ID>
<ANCIEN_ID>JG_L_2021_04_000000437991</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/41/11/CETATEXT000043411147.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 20/04/2021, 437991, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-04-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>437991</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Olivier Guiard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:437991.20210420</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... C... a demandé au tribunal administratif de Grenoble de prononcer la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles il a été assujetti au titre de l'année 2010. Par un jugement n° 1506836 du 21 décembre 2017, le tribunal administratif de Grenoble, après avoir constaté qu'il n'y avait plus lieu de statuer sur les conclusions de cette demande à concurrence d'une somme de 67 749 euros, a déchargé M. C... des impositions litigieuses. <br/>
<br/>
              Par un arrêt n° 18LY01383 du 26 septembre 2019, la cour administrative d'appel de Lyon, sur appel formé par le ministre de l'action et des comptes publics, a annulé l'article 2 du jugement du tribunal administratif de Grenoble et rétabli les impositions supplémentaires restant en litige, à l'exception de la majoration de 25 % appliquée aux contributions sociales.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 27 janvier et 30 juillet 2020 et le 15 mars 2021 au secrétariat du contentieux du Conseil d'Etat, M. C... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en ce qu'il fait droit à l'appel du ministre ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel du ministre ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Guiard, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Piwnica, Molinié, avocat de M. C... ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 8 avril 2021 présentée par M. C... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que le 23 février 2010, M. C... a acquis de la société Flodrine et de la société Sixtine, respectivement 7 490 et 9 633 actions de la société Vermont aux prix unitaires de 23,35 et 22,72 euros. À l'issue d'une vérification de comptabilité de la société Sixtine et d'un contrôle sur pièces des déclarations de M. C... et de la société Flodrine, l'administration a évalué les actions de la société Vermont à 99,50 euros l'unité et imposé entre les mains de M. C..., en tant que revenus distribués, l'avantage occulte retiré de ces acquisitions à prix minoré. L'administration fiscale, par ailleurs, a considéré que M. C... avait bénéficié d'un second avantage occulte résultant de la prise en charge intégrale, par la société Sixtine, d'une commission rémunérant une prestation de recherche confiée à la société GMBA Ingénierie afin d'identifier des acquéreurs potentiels pour les titres de la société Vermont, cette commission intégrant la part correspondant aux titres détenus par M. C.... Par un jugement du 21 décembre 2017, après avoir constaté qu'il n'y avait plus lieu à statuer sur les conclusions présentées par M. C... à concurrence d'une somme de 67 749 euros, le tribunal administratif de Grenoble l'a déchargé des suppléments d'impôt sur le revenu et de cotisations sociales auxquelles il avait été assujetti en 2010 à la suite de ces rectifications. Par un arrêt du 26 septembre 2019, sur appel du ministre de l'action et des comptes publics, la cour administrative d'appel de Lyon a annulé l'article 2 de ce jugement et rétabli les impositions supplémentaires restant en litige, à l'exception de la majoration appliquée aux contributions sociales. M. C... se pourvoit en cassation contre cet arrêt. <br/>
<br/>
              Sur l'avantage occulte retiré de l'acquisition des titres de la société Vermont :<br/>
<br/>
              2. Aux termes de l'article 111 du code général des impôts : " Sont notamment considérés comme revenus distribués : (...) / c. Les rémunérations et avantages occultes (...) ". <br/>
<br/>
              3. En cas d'acquisition par une société à un prix que les parties ont délibérément majoré par rapport à la valeur vénale de l'objet de la transaction, ou, s'il s'agit d'une vente, délibérément minoré, sans que cet écart de prix comporte de contrepartie, l'avantage ainsi octroyé doit être requalifié comme une libéralité représentant un avantage occulte constitutif d'une distribution de bénéfices au sens des dispositions précédemment citées du c de l'article 111  du code général des impôts, alors même que l'opération est portée en comptabilité et y est assortie de toutes les justifications concernant son objet et l'identité du cocontractant, dès lors que cette comptabilisation ne révèle pas, par elle-même, la libéralité en cause. La preuve d'une telle distribution occulte doit être regardée comme apportée par l'administration lorsqu'est établie l'existence, d'une part, d'un écart significatif entre le prix convenu et la valeur vénale du bien cédé, d'autre part, d'une intention, pour la société, d'octroyer et, pour le cocontractant, de recevoir, une libéralité du fait des conditions de la cession.<br/>
<br/>
              En ce qui concerne l'écart entre le prix convenu et la valeur vénale des titres cédés : <br/>
<br/>
              4. La valeur vénale des titres d'une société non admise à la négociation sur un marché réglementé doit être appréciée compte tenu de tous les éléments dont l'ensemble permet d'obtenir un chiffre aussi voisin que possible de celui qu'aurait entraîné le jeu normal de l'offre et de la demande à la date où la cession est intervenue. L'évaluation des titres d'une telle société doit être effectuée, par priorité, par référence au prix d'autres transactions intervenues dans des conditions équivalentes et portant sur les titres de la même société ou, à défaut, de sociétés similaires. En l'absence de telles transactions, celle-ci peut légalement se fonder sur la combinaison de plusieurs méthodes alternatives.<br/>
<br/>
              5. En premier lieu, si M. C... soutient que la cession des titres de la société Vermont par la société Axel le 7 janvier 2008 au prix de 30 euros l'unité et celles du 23 février 2010 consenties par M. B... au même prix sont en tout point comparables à la cession litigieuse en ce qu'elles portent sur les titres de la même société, qu'elles concernent des associés minoritaires souhaitant se désengager, que l'une porte sur le même nombre de parts et que les autres ont eu lieu le jour même des transactions en cause, et enfin qu'il n'est ni soutenu ni établi que le prix de 30 euros aurait été un prix de convenance, il reconnait lui-même que la situation économique de la société Vermont a significativement évolué entre 2008 et 2010. Par ailleurs, il ressort des pièces du dossier soumis aux juges du fond que les 300 actions cédées par M. B... le 23 février 2010 ne représentaient que 0,4 % des parts de la société Vermont. Il s'ensuit que la cour a pu, sans commettre d'erreur de droit et sans dénaturer les pièces du dossier, écarter ces transactions au motif qu'elles n'étaient pas intervenues dans des conditions équivalentes à celles en litige.<br/>
<br/>
              6. En deuxième lieu, il est toujours loisible à l'administration de se fonder sur des éléments postérieurs à une transaction pour en établir la valeur réelle, sous réserve que ces éléments ne traduisent aucune évolution qui ferait obstacle à ce qu'ils soient valablement pris en compte comme éléments de comparaison compte tenu de la date à laquelle ils sont intervenus. Pour juger qu'il ne résultait pas de l'instruction que les perspectives d'avenir de la société Vermont étaient défavorables à la date des cessions litigieuses, soit le 23 février 2010, la cour s'est fondée, d'une part, sur le rapport du président de la filiale opérationnelle de la société Vermont, présenté à l'assemblée générale des actionnaires le 4 mai 2010, selon lequel en dépit d'un délai de démarrage anormalement long, l'atelier de développement du projet industriel " tube " était désormais opérationnel, et, d'autre part, sur un rapport relatif à l'évolution de cette filiale, établi le 22 juin 2017, qui précisait qu'un accord avec un partenaire avait été conclu le 30 juin 2010 et que la production d'ampoules, le chiffre d'affaires et la trésorerie avaient augmenté de manière significative entre 2009 et 2010. Si le requérant soutient que ces circonstances, postérieures au 23 février 2010, n'étaient pas prévisibles à cette date et que la filiale avait rencontré des difficultés opérationnelles importantes au cours de l'année 2009, il ne ressort pas des pièces du dossier soumis aux juges du fond que les perspectives de développement favorables du projet " tube ", déjà initié depuis plusieurs mois, étaient à ce point incertaines en février 2010 qu'elles faisaient obstacle à ce que la cour, qui a porté sur ces éléments une appréciation souveraine exempte de dénaturation, les prenne en considération pour se prononcer sur la valeur réelle des titres de la société Vermont cédés à M. C.... <br/>
<br/>
              7. En troisième lieu, dès lors qu'elle pouvait tenir compte de tous les éléments permettant d'approcher aussi près que possible la valeur réelle des titres de la société Vermont telle qu'elle aurait résulté du jeu normal de l'offre et de la demande à la date du 23 février 2010, la cour, pour apprécier la pertinence de l'évaluation des actions de la société Vermont réalisée par l'administration, n'a pas commis d'erreur de droit en se fondant, d'une part, sur la circonstance que le 8 janvier 2009, un mandat de recherche avait été confié par l'ensemble des actionnaires de la société Vermont à la société GMBA Ingénierie pour que celle-ci identifie un ou des acquéreurs potentiels pour l'ensemble des titres de l'entreprise au prix unitaire minimum de 99,85 euros, et, d'autre part, sur la circonstance que toutes les actions de la société Vermont avaient été cédées, le 29 décembre 2010, au prix unitaire de 108,50 euros.<br/>
<br/>
              En ce qui concerne l'intention libérale des sociétés cédantes et de M. C... :<br/>
<br/>
              8. Il ressort des pièces du dossier soumis aux juges du fond qu'en leur qualité d'associés de la société Vermont, les sociétés Flodrine et Sixtine ont signé l'une et l'autre le mandat de recherche conclu le 8 janvier 2009 avec la société GMBA Ingénierie, lequel prévoyait que les offres d'achat des acquéreurs potentiels des titres de la société Vermont ne pourraient être acceptées en deçà d'un prix minimal par action de 99,85 euros et que M. C..., en sa qualité de membre du comité de direction de la société Vermont, était également signataire de ce mandat de recherche, tout en étant par ailleurs le dirigeant de la société GMBA Ingénierie. Il ressort également de ces pièces qu'une offre d'achat pour l'ensemble des titres de la société Vermont a été présentée par un tiers le 14 janvier 2010 au prix unitaire de 106,50 euros et il n'était ni établi ni même allégué que M. D..., en sa qualité de président de la société Vermont, et M. C..., chargé de l'exécution du mandat de recherche, tous deux mentionnés en tant que destinataires de cette offre, l'auraient dissimulée aux autres actionnaires de la société. Dès lors, la cour a pu déduire de ces faits, par une appréciation souveraine exempte de dénaturation, que les sociétés Flodrine, Sixtine et M. C... ne pouvaient ignorer la valeur des titres cédés le 23 février 2010, de sorte que l'administration apportait la preuve, qui lui incombait, de l'intention de ces sociétés d'octroyer et de M. C... de recevoir une libéralité sous la forme d'une minoration du prix de cession des actions en cause. <br/>
<br/>
              Sur l'avantage occulte retiré de la prise en charge par la société Sixtine de l'intégralité de la commission versée à la société GMBA Ingénierie :<br/>
<br/>
              9. Lorsqu'une société a pris en charge des dépenses incombant normalement à un tiers sans que la comptabilisation de cette opération ne révèle, par elle-même, l'octroi d'un avantage, il appartient à l'administration, si elle entend faire application des dispositions du c de l'article 111 du code général des impôts pour imposer, dans les mains du tiers, cette somme, d'établir, d'une part, que la prise en charge de cette dépense ne comportait pas de contrepartie pour la société, et d'autre part, qu'il existait une intention, pour celle-ci, d'octroyer, et pour le tiers, de recevoir, une libéralité.<br/>
<br/>
              10. Pour juger que l'administration établissait l'existence d'un avantage occulte imposable entre les mains de M. C... tenant à la prise en charge, par la société Sixtine, de l'intégralité de la commission versée à la société GMBA Ingénierie en rémunération de ses prestations de recherche d'un acquéreur pour l'ensemble des titres de la société Vermont, y compris de la fraction de cette commission correspondant aux titres détenus par M. C..., la cour a retenu, d'une part, que l'administration établissait que la prise en charge de cette fraction de la commission ne s'était accompagnée d'aucune contrepartie avérée pour la société et n'avait été effectuée que dans l'intérêt exclusif du requérant, et, d'autre part, qu'elle établissait l'intention pour la société Sixtine d'octroyer et pour M. C... de recevoir une libéralité. En statuant ainsi, la cour qui n'avait, en tout état de cause, pas à tenir compte d'un jugement, non versé à l'instruction, rendu par le tribunal administratif de Châlons-en-Champagne au bénéfice d'un autre actionnaire, a suffisamment motivé sa décision et n'a pas commis d'erreur de droit.<br/>
<br/>
              11. Il résulte de tout ce qui précède que M. C... n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque. <br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              12. Les dispositions de cet article font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi de M. C... est rejeté. <br/>
Article 2 : La présente décision sera notifiée à M. A... C... et au ministre de l'économie, des finances et de la relance. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
