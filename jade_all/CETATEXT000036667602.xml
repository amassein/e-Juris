<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036667602</ID>
<ANCIEN_ID>JG_L_2018_02_000000417636</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/66/76/CETATEXT000036667602.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 28/02/2018, 417636, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-02-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417636</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU ; LE PRADO</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2018:417636.20180228</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, deux mémoires complémentaires et deux mémoires en réplique, enregistrés le 25 janvier et les 1er, 5 et 15 février 2018 au secrétariat du contentieux du Conseil d'Etat, Mme B...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution de la décision du directeur général de l'Agence nationale de sécurité du médicament et des produits de santé du 24 juillet 2017 modifiant la recommandation temporaire d'utilisation du 17 mars 2017 relative au Baclofène pour la prise en charge des patients alcoolo-dépendants, en ce qu'elle abaisse la posologie maximale autorisée à 80 mg par jour, ensemble les décisions implicites de rejet des demandes d'abrogation qu'elle a présentées à l'encontre de cette décision ;<br/>
<br/>
              2°) d'enjoindre à l'Agence nationale de sécurité du médicament et des produits de santé et au ministre de la santé, d'une part, de retirer sans délai de leurs sites Internet toute mention de la décision litigieuse, d'autre part, d'insérer, dans les deux jours à compter de l'ordonnance à intervenir, en première page de ces sites, un communiqué faisant état de la suspension de l'exécution de cette décision et reproduisant la recommandation temporaire d'utilisation dans sa version initiale et, enfin, de diffuser, dans les deux jours à compter de l'ordonnance qui sera rendue, auprès de tous les médecins généralistes, psychiatres, addictologues, centres de soins, d'accompagnement et de prévention en addictologie, pharmaciens hospitaliers et d'officine, un communiqué les informant de la suspension de l'exécution de la décision litigieuse et de la possibilité de prescrire et de délivrer à nouveau le Baclofène à forte dose, sous astreinte de 500 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'Agence nationale de sécurité du médicament et des produits de santé et de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Elle soutient que : <br/>
              - la condition d'urgence est remplie dès lors que l'exécution de la décision litigieuse, d'une part, préjudicie de manière grave et immédiate à sa situation en ce qu'elle la met dans l'impossibilité de continuer son traitement, l'exposant ainsi à de graves risques médicaux, et, d'autre part, porte atteinte à la santé publique, en ce qu'elle prive de l'accès au soin les patients alcolo-dépendants nécessitant un traitement au Baclofène à dose élevée, les exposant à des risques de rechutes dans l'alcoolisme, dont certains cas sont déjà avérés ;<br/>
              - il existe un doute sérieux sur la légalité de la décision ;<br/>
              - elle est entachée d'un vice de procédure en ce que l'Agence nationale de sécurité du médicament et des produits de santé n'a pas procédé à la réalisation de l'enquête prévue par l'article R. 5121-76-6 du code de la santé publique préalablement à son édiction, n'a pas consulté la Commission d'évaluation initiale du rapport entre les bénéfices et les risques des produits de santé de l'Agence nationale de sécurité du médicament et des produits de santé et n'a pas saisi les titulaires d'autorisation de mise sur le marché des produits à base de Baclofène ;<br/>
              - elle est entachée d'une erreur manifeste dans l'appréciation du rapport entre le bénéfice attendu de l'utilisation du médicament à dose élevée pour le traitement de l'alcoolo-dépendance et les effets indésirables encourus.<br/>
              Par deux mémoires en défense, enregistrés les 12 et le 15 février 2018, l'Agence nationale de sécurité du médicament et des produits de santé conclut au rejet de la requête. Elle soutient que la condition d'urgence n'est pas remplie, au regard notamment des impératifs de santé publique qui seraient compromis par la mesure de suspension demandée, et que les moyens soulevés par Mme B...ne sont pas propres à créer un doute sérieux quant à la légalité de la décision.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, Mme B...et, d'autre part, l'Agence nationale de sécurité du médicament et des produits de santé et la ministre des solidarités et de la santé ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du vendredi 16 février 2018 à 10 heures au cours de laquelle ont été entendus :<br/>
<br/>
              - Me Sureau, avocat au Conseil d'Etat et à la Cour de cassation, avocat de MmeB... ;<br/>
<br/>
              - les représentants de la requérante ;<br/>
<br/>
              - Me Le Prado, avocat au Conseil d'Etat et à la Cour de cassation, avocat de l'Agence nationale de sécurité du médicament et des produits de santé ;<br/>
<br/>
              - les représentants de l'Agence nationale de sécurité du médicament et des produits de santé ;<br/>
<br/>
              - les représentants de la ministre des solidarités et de la santé ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
             1. Par une décision du 17 mars 2014, le directeur général de l'Agence nationale de sécurité du médicament et des produits de santé a établi, pour une durée de trois ans, la recommandation temporaire d'utilisation du Baclofène dans le traitement de l'alcoolo-dépendance, tant pour l'aide au maintien de l'abstinence après sevrage chez les patients dépendants à l'alcool et en échec des autres traitements disponibles que pour la réduction majeure de la consommation d'alcool jusqu'au niveau faible de la consommation telle que définie par l'OMS chez des patients alcoolo-dépendants à haut risque et en échec de traitement disponibles, fixant à 300 mg/jour la dose maximale qui ne devra pas être dépassée. Par une décision du 17 mars 2017, la recommandation a été renouvelée pour une durée d'un an, pour les mêmes traitements et avec la même posologie maximale. Par une décision du 24 juillet 2017, le directeur général de l'Agence nationale de sécurité du médicament et des produits de santé a modifié cette recommandation pour notamment limiter la posologie maximale à 80 mg/jour. Mme B...a saisi le juge des référés du Conseil d'Etat d'une demande, fondée sur l'article L. 521-1 du code de justice administrative, tendant à la suspension de l'exécution de cette dernière décision.<br/>
             2. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision. ".<br/>
<br/>
             3. La condition d'urgence à laquelle est subordonné le prononcé d'une mesure de suspension doit être regardée comme remplie lorsque la décision administrative contestée préjudicie de manière suffisamment grave et immédiate à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre. Il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte litigieux sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue.<br/>
             4. La 1ère chambre de la section du contentieux a prévu d'inscrire dans de brefs délais au rôle d'une séance de jugement la requête tendant à l'annulation de la décision du directeur général de l'Agence nationale de sécurité du médicament et des produits de santé du 24 juillet 2007 modifiant la recommandation temporaire d'utilisation du 17 mars 2017 relative au Baclofène pour la prise en charge des patients alcoolo-dépendants. Dans ces conditions, l'application de cette décision dans l'attente du prochain examen au fond ne fait pas apparaître une situation d'urgence. Il en résulte que, faute que la condition d'urgence soit remplie, les conclusions à fin de suspension de Mme B...doivent être rejetées. Il en va de même, par voie de conséquence, de ses conclusions à fin d'injonction et à fin d'application de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de Mme B...est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à Mme A...B..., à l'Agence nationale de sécurité du médicament et des produits de santé et à la ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
