<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041575521</ID>
<ANCIEN_ID>JG_L_2020_02_000000420743</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/57/55/CETATEXT000041575521.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 13/02/2020, 420743, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-02-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420743</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Charles-Emmanuel Airy</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:420743.20200213</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société à responsabilité limitée (SARL) LD Services a demandé au tribunal administratif de Clermont-Ferrand de prononcer la décharge des suppléments de cotisation foncière des entreprises auxquels elle a été assujettie au titre des années 2010 à 2012 à raison de la prise en compte, dans ses bases d'imposition, de la valeur locative du magasin que la société Babou met à sa disposition à Saran (Loiret). Par un jugement n° 1500495 du 6 décembre 2016, ce tribunal a fait droit à sa demande.<br/>
<br/>
              Par un arrêt n° 17LY00469 du 20 mars 2018, la cour administrative d'appel de Lyon a, sur appel du ministre de l'action et des comptes publics, annulé l'article 1er de ce jugement en tant qu'il prononce la décharge des cotisations supplémentaires de cotisation foncière des entreprises au titre des années 2011 et 2012, remis ces impositions à la charge de la société LD Services et rejeté le surplus des conclusions d'appel du ministre. <br/>
<br/>
              Par un pourvoi enregistré le 18 mai 2018 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'action et des comptes publics demande au Conseil d'Etat d'annuler l'article 3 de cet arrêt. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Charles-Emmanuel Airy, auditeur,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Babou exerce une activité de distribution dans des magasins dont elle confie la gérance à des entreprises indépendantes. Pour son imposition à la taxe professionnelle au titre des années 2002 à 2005, l'administration fiscale avait estimé que les locaux commerciaux au moyen desquels cette activité était exercée restaient à sa disposition pour l'exercice de son activité professionnelle. En application de l'article 1467 du code général des impôts, elle avait, dès lors, intégré leur valeur locative dans les bases d'imposition de cette société. Par des arrêts du 23 décembre 2010 devenus définitifs, la cour administrative d'appel de Lyon a cependant infirmé cette analyse au motif que les locaux étaient sous le contrôle des mandataires auxquels la société Babou en confiait l'exploitation. L'administration fiscale a alors intégré la valeur locative des magasins de la société Babou dans la base imposable à la taxe professionnelle ou à la cotisation foncière des entreprises de chacun des exploitants. La société à responsabilité limitée (SARL) LD Services, qui exploitait un fonds de commerce de vente au détail de produits d'équipement du foyer et de la personne sur le territoire de la commune de Saran (Loiret) en vertu de conventions de gérance-mandat conclues avec la société Babou, s'est ainsi vue notifier des rehaussements de sa base d'imposition à la cotisation foncière des entreprises pour les années 2010, 2011 et 2012. Le ministre de l'action et des comptes publics se pourvoit en cassation contre l'arrêt du 20 mars 2018 de la cour administrative d'appel de Lyon en tant qu'il rejette, par son article 3, l'appel qu'il avait formé contre le jugement du tribunal administratif de Clermont-Ferrand en tant qu'il avait prononcé la décharge des suppléments de cotisation foncière des entreprises mis à la charge de la société LD Services au titre de l'année 2010.<br/>
<br/>
               2. Aux termes de l'article L. 80 B du livre des procédures fiscales : " La garantie prévue au premier alinéa de l'article L. 80 A est applicable : / 1° Lorsque l'administration a formellement pris position sur l'appréciation d'une situation de fait au regard d'un texte fiscal (...) ". Peuvent se prévaloir de cette garantie, pour faire échec à l'application de la loi fiscale, les contribuables qui se trouvent dans la situation de fait sur laquelle l'appréciation invoquée a été portée ainsi que les contribuables qui, à la date de la prise de position de l'administration, ont été partie à l'acte ou participé à l'opération qui a donné naissance à cette situation sans que les autres contribuables puissent utilement invoquer une rupture à leur détriment du principe d'égalité. <br/>
<br/>
              3. Pour demander la décharge des suppléments de cotisation foncière des entreprises auxquels elle a été assujettie du fait de l'inclusion dans ses bases d'imposition de la valeur locative des locaux commerciaux en cause, la société LD Services s'est prévalue, devant les juges du fond, sur le fondement de l'article L. 80 B du livre des procédures fiscales, de la position prise par l'administration fiscale dans des courriers des 20 décembre 2005 et 30 mai 2007 par lesquels elle indiquait à la société Babou que celle-ci avait, au sens et pour l'application de l'article 1467 du code général des impôts, la disposition, pour l'exercice de son activité professionnelle, des locaux commerciaux dont elle confiait la gestion à des tiers et que leur valeur locative entrait, en conséquence, dans l'assiette de son imposition à la taxe professionnelle au titre des années 2002 à 2005 ainsi que dans une décision du 19 janvier 2009 rejetant la réclamation de la société Babou contre les cotisations de taxe professionnelles auxquelles elle avait été assujettie au titre des années 2006 à 2008 dans les rôles de la commune de Saran, à raison du magasin en litige. <br/>
<br/>
              4. La cour administrative d'appel a relevé que les contrats par lesquels la société Babou avait confié la gérance du magasin de Saran à la société LD Services n'avaient été conclus qu'à compter du 1er mai 2008, ce dont elle a déduit que l'administration ne pouvait avoir pris position, par les courriers des 20 décembre 2005 et 30 mai 2007 et par la décision du 19 janvier 2009, sur les conséquences qu'il convenait de tirer de ces contrats pour l'assujettissement à la taxe professionnelle de l'une ou l'autre des parties. En jugeant que la société LD Services était néanmoins fondée à se prévaloir de ces prises de position au motif que le contrat qu'elle avait conclu le 1er mai 2008 avec la société Babou était similaire à celui par lequel ces deux mêmes sociétés avaient été antérieurement liées, pour l'exploitation d'un autre établissement, situé à Grosbliederstroff (Moselle), et qui avait fait l'objet des prises de position de l'administration, la cour administrative d'appel a commis une erreur de droit. <br/>
<br/>
              5. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que le ministre est fondé à demander l'annulation de l'arrêt qu'il attaque, en tant qu'il prononce la décharge de la cotisation supplémentaire de cotisation foncière des entreprises à laquelle la société LD Services a été assujettie au titre de l'année 2010.  <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 3 de l'arrêt du 20 mars 2018 de la cour administrative d'appel de Lyon est annulé.<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Lyon. <br/>
Article 3 : La présente décision sera notifiée au ministre de l'action et des comptes publics et à la société à responsabilité limitée LD Services. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
