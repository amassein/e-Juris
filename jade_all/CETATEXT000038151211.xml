<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038151211</ID>
<ANCIEN_ID>JG_L_2019_02_000000422129</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/15/12/CETATEXT000038151211.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 20/02/2019, 422129, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-02-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>422129</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Pierre Ramain</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:422129.20190220</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A... a demandé à la Cour nationale du droit d'asile d'annuler la décision du 26 juin 2017 par laquelle l'Office français de protection des réfugiés et apatrides (OFPRA) a refusé d'enregistrer la demande d'asile qu'il a présentée alors qu'il se trouvait en rétention administrative.<br/>
<br/>
               Par une décision n° 17028317 du 25 mai 2018, la Cour nationale du droit d'asile a annulé cette décision et renvoyé l'examen de sa demande d'asile devant l'OFPRA. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 10 juillet et 23 août 2018 au secrétariat du contentieux du Conseil d'Etat, l'OFPRA demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) de renvoyer l'affaire devant la Cour nationale du droit d'asile. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention de Genève du 28 juillet 1951 relative au statut des réfugiés et le protocole signé à New York le 31 janvier 1967 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pierre Ramain, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de à la  et à  et à la SCP Rocheteau, Uzan-Sarano, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. L'Office français de protection des réfugiés et apatrides (OFPRA) se pourvoit en cassation contre la décision du 25 mai 2018 par laquelle la Cour nationale du droit d'asile a annulé sa décision du 26 juin 2017 déclarant irrecevable, pour tardiveté, la demande d'asile déposée par M.A..., alors placé en rétention administrative.<br/>
<br/>
              2. Aux termes de l'article L. 551-3 du code de l'entrée et du séjour des étrangers et du droit d'asile : " A son arrivée au centre de rétention, l'étranger reçoit notification des droits qu'il est susceptible d'exercer en matière de demande d'asile. A cette fin, il peut bénéficier d'une assistance juridique et linguistique. Il lui est notamment indiqué que sa demande d'asile ne sera plus recevable pendant la période de rétention si elle est formulée plus de cinq jours après cette notification. Cette irrecevabilité n'est pas opposable à l'étranger qui invoque, au soutien de sa demande, des faits survenus après l'expiration de ce délai. Lorsque le demandeur provient d'un pays considéré comme un pays d'origine sûr en application de l'article L. 722-1, l'autorité administrative peut opposer l'irrecevabilité de la demande d'asile présentée au-delà des cinq premiers jours de rétention dans le seul but de faire échec à l'exécution effective et imminente de la mesure d'éloignement ". Aux termes de l'article R. 556-1 du même code : " L'étranger maintenu en centre ou local de rétention administrative qui souhaite demander l'asile est informé, sans délai, dans une langue qu'il comprend ou dont il est raisonnable de penser qu'il la comprend, de la procédure de demande d'asile, de ses droits et de ses obligations au cours de cette procédure, des conséquences que pourrait avoir le non-respect de ces obligations ou le refus de coopérer avec les autorités et des moyens dont il dispose pour l'aider à présenter sa demande ". Compte tenu de la gravité particulière des effets qui s'attachent, pour des étrangers retenus, au refus d'enregistrement de leur demande d'asile, le délai prévu à l'article L. 551-3 du code de l'entrée et du séjour des étrangers et du droit d'asile doit être regardé comme n'étant pas prescrit à peine d'irrecevabilité dans certains cas particuliers, notamment dans l'hypothèse où un étranger retenu ne peut être regardé comme ayant pu utilement présenter une demande d'asile faute d'avoir bénéficié d'une assistance juridique et linguistique effective.<br/>
<br/>
              3. Il ressort des pièces du dossier soumis à la Cour nationale du droit d'asile que M.A..., de nationalité bangladaise, interpellé par les services de police le 11 mai 2017, a été placé en rétention administrative en vue de son éloignement du territoire français le 12 mai 2017 et a déposé une demande d'asile le 22 juin 2017, au-delà du délai de cinq jours prévu par l'article L. 551-3 précité. Pour juger la demande d'asile de M. A...recevable alors que l'OFPRA l'avait rejetée comme tardive, la Cour a relevé que cette demande avait été déposée le jour même de la notification de ses droits en langue bengali et a estimé que, nonobstant la notification des conditions dans lesquelles il pouvait présenter une demande qui lui avait été faite le 12 mai 2017 en langue française et en langue anglaise, il n'avait pas bénéficié d'une assistance linguistique effective pour présenter sa demande d'asile.<br/>
<br/>
              4. En premier lieu, il ressort des énonciations de la décision attaquée que la Cour nationale du droit d'asile ne s'est pas bornée à constater que M. A...n'avait pas sollicité le concours d'un interprète en langue bengali lors de la notification de ses droits en langues française et anglaise pour en déduire qu'il ne comprenait pas la langue anglaise. Elle a relevé que cette circonstance ne suffisait pas à établir qu'il la comprenait et s'est fondée sur le constat qu'aucun élément au dossier ne permettait d'établir qu'il aurait une connaissance et une maîtrise suffisante de cette langue. <br/>
<br/>
              5. En second lieu, si la langue anglaise constitue l'une des principales langues employées au Bangladesh, que M. A...indique exercer la profession de steward dans une compagnie aérienne sud-coréenne et qu'il a signé des procès-verbaux et accusés de notification en présence d'un interprète en langue anglaise, c'est au terme d'une appréciation souveraine exempte de dénaturation que la Cour nationale du droit d'asile a estimé qu'il ne résultait pas de l'instruction qu'il avait une connaissance et une maîtrise suffisantes de cette langue pour comprendre tant le contenu de la notification de ses droits rédigée dans cette langue que la notification orale du droit d'avoir recours à un interprète dans une langue différente de celle qui lui était proposée par l'administration. <br/>
<br/>
              6. Il résulte de ce qui précède que l'OFPRA n'est pas fondé à demander l'annulation de la décision qu'il attaque. <br/>
<br/>
              7. M. A...a obtenu le bénéfice de l'aide juridictionnelle. Par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Rocheteau, Uzan-Sarano renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'OFPRA le versement à cette SCP de la somme de 2 500 euros.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi de l'Office français de protection des réfugiés et apatrides est rejeté.<br/>
Article 2 : L'Office français de protection des réfugiés et apatrides versera à la SCP Rocheteau, Uzan-Sarano, avocat de M.A..., sous réserve qu'elle renonce à percevoir la somme correspondant à la part contributive de l'Etat, la somme de 2 500 euros au titre des articles 37 de la loi du 10 juillet 1991 et L. 761-1 du code de justice administrative. <br/>
Article 3 : La présente décision sera notifiée à l'Office français de protection des réfugiés et apatrides et à M. B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
