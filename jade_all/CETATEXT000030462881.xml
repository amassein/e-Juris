<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030462881</ID>
<ANCIEN_ID>JG_L_2015_04_000000380455</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/46/28/CETATEXT000030462881.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  5ème sous-section jugeant seule, 08/04/2015, 380455, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-04-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>380455</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 5ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>Mme Leïla Derouich</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:380455.20150408</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Cergy-Pontoise, d'une part, d'annuler la décision 48 SI du 2 novembre 2012 du ministre de l'intérieur constatant la perte de validité de son permis de conduire ainsi que les décisions successives retirant des points de ce permis à la suite d'infractions au code de la route et, d'autre part, d'enjoindre au ministre de l'intérieur de lui restituer les points illégalement retirés. Par un jugement n°1210034 du 13 mars 2014, le tribunal administratif a annulé la décision du 2 novembre 2012 ainsi que les retraits de points consécutifs aux infractions commises les 6 avril 2004, 18 mai 2007 et 19 novembre 2007 et enjoint au ministre de l'intérieur de reconnaître à l'intéressé le bénéfice des points retirés à la suite de ces infractions et de réexaminer sa situation pour en tirer les conséquences sur la validité de son permis.<br/>
<br/>
              Par un pourvoi, enregistré le 19 mai 2014 au secrétariat du contentieux du Conseil d'Etat le ministre de l'intérieur demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de M. A...;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de la route ; <br/>
<br/>
              - le code de procédure pénale ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Leïla Derouich, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que par une lettre 48 SI du 2 novembre 2012 le ministre de l'intérieur a notifié à M. A...le retrait d'un point de son permis de conduire à la suite d'une infraction commise le 29 juin 2011, lui a rappelé les retraits de points antérieurs, consécutifs à des infractions commises les 26 décembre 2002, 6 et 13 avril, 15 juillet et 24 octobre 2004, 18 mai et 19 novembre 2007 et 17 octobre 2009 et portant au total sur quinze points, et a constaté la perte de validité du permis pour solde de points nul ; que, par le jugement du 13 mars 2014 contre lequel le ministre se pourvoit en cassation, le tribunal administratif de Cergy-Pontoise, faisant partiellement droit aux conclusions dont l'intéressé l'avait saisi, a, d'une part, annulé les retraits de points consécutifs aux infractions commises les 6 avril 2004, 18 mai 2007 et 19 novembre 2007 ainsi que la décision constatant la perte de validité de son  permis de conduire et, d'autre part, enjoint à l'administration de reconnaître à M. A... le bénéfice des points illégalement retirés et d'en tirer les conséquences sur le capital de points et le droit de conduire de l'intéressé ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 223-1 du code de la route : " Le permis de conduire est affecté d'un nombre de points. Celui-ci est réduit de plein droit si le titulaire du permis a commis une infraction pour laquelle cette réduction est prévue. / (...) La réalité d'une infraction entraînant retrait de points est établie par le paiement d'une amende forfaitaire ou l'émission du titre exécutoire de l'amende forfaitaire majorée, l'exécution d'une composition pénale ou par une condamnation définitive. (...) " ; que la délivrance, au titulaire du permis de conduire à l'encontre duquel est relevée une infraction donnant lieu à retrait de points, de l'information prévue aux articles L. 223-3 et R. 223-3 du code de la route constitue une garantie essentielle donnée à l'auteur de l'infraction pour lui permettre, avant d'en reconnaître la réalité par le paiement d'une amende forfaitaire ou l'exécution d'une composition pénale, d'en mesurer les conséquences sur la validité de son permis et éventuellement d'en contester la réalité devant le juge pénal ; qu'elle revêt le caractère d'une formalité substantielle et conditionne la régularité de la procédure au terme de laquelle le retrait de points est décidé ;<br/>
<br/>
              3. Considérant, d'une part, qu'il ressort des pièces du dossier soumis aux juges du fond que, pour les infractions commises les 6 avril 2004 et 18 mai 2007, constatées avec interception du véhicule, le ministre de l'intérieur a transmis au tribunal administratif des procès-verbaux de police revêtus de la signature de M. A...sous la mention : " Le contrevenant reconnaît avoir reçu la carte de paiement et l'avis de contravention sur lequel figurent les informations portées au verso du présent formulaire " ; qu'en jugeant que les retraits de points consécutifs à ces infractions étaient irréguliers faute pour le ministre d'apporter la preuve, par la production soit des procès-verbaux d'infraction, soit des souches des quittances du paiement de l'amende entre les mains de l'agent verbalisateur, que l'intéressé avait, lors de leur constatation, bénéficié de l'information exigée par la loi, alors que des procès-verbaux signés figuraient au dossier qui lui était soumis, le tribunal administratif a commis une erreur de fait ; <br/>
<br/>
              4. Considérant, d'autre part, qu'il ressort des pièces du dossier soumis au juge du fond que si le ministre a produit devant le tribunal administratif le procès-verbal constatant l'infraction du 19 novembre 2007 et indiquant que le retrait de points était encouru, ce document n'était pas revêtu de la signature du contrevenant et ne comportait pas la mention " refuse de signer " ; que par suite, en jugeant que le ministre n'apportait pas la preuve que le requérant avait reçu, lors de la constatation de cette infraction, les informations requises par les dispositions des articles L. 223-3 et R. 223-3 du code de la route, le tribunal administratif n'a pas commis d'erreur de droit et n'a pas dénaturé les pièces du dossier ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que le jugement du 13 mars 2014 doit être annulé en tant seulement qu'il annule le retrait de trois points du permis de conduire de M. A... consécutif à l'infraction du 6 avril 2004 et le retrait de deux points consécutif à celle du 18 mai 2007 ; que la seule annulation du retrait de deux points consécutif à l'infraction du 19 novembre 2007 ne suffit pas, compte tenu du fait que le nombre maximal de points est de douze et que les autres retraits de points récapitulés dans la décision constatant la perte de validité du permis portent sur treize points, à justifier l'annulation de cette décision ; que, par suite, le jugement encourt également l'annulation en tant qu'il annule cette décision et enjoint au ministre de l'intérieur de procéder au rétablissement de points ; <br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que les frais exposés par M. A...soient mis à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Cergy-Pontoise du 13 mars 2014 est annulé en tant qu'il annule les décisions du ministre de l'intérieur retirant des points du permis de conduire de M. A...à la suite des infractions des 6 avril 2004 et 18 mai 2007 et la décision du 2 novembre 2012 constatant la perte de validité de son permis de conduire et enjoint au ministre de reconnaître à M. A... le bénéfice des points retirés et de réexaminer sa situation.<br/>
<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Cergy-Pontoise dans la mesure de la cassation prononcée.<br/>
<br/>
Article 3 : Les conclusions de M. A...présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 4 : La présente décision sera notifiée au ministre de l'intérieur et à M. B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
