<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037183353</ID>
<ANCIEN_ID>JG_L_2018_07_000000412681</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/18/33/CETATEXT000037183353.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 11/07/2018, 412681, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-07-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412681</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>M. Laurent Domingo</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:412681.20180711</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société par actions simplifiée (SAS) L'immobilière groupe Casino a demandé au tribunal administratif de Pau de prononcer la décharge des cotisations de taxe d'enlèvement des ordures ménagères auxquelles elle a été assujettie au titre de l'année 2013 dans les rôles de la commune de Laloubère (Hautes-Pyrénées). Par un jugement n° 1501309 du 24 mai 2017, ce tribunal a rejeté cette demande. <br/>
<br/>
              Par un pourvoi, enregistrés le 21 juillet 2017 au secrétariat du contentieux du Conseil d'Etat, la SAS L'immobilière groupe Casino demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Domingo, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de la société L'immobilière groupe Casino.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. La société L'immobilière groupe Casino se pourvoit en cassation contre le jugement du 24 mai 2017 par lequel le tribunal administratif de Pau a rejeté sa demande tendant à la décharge des cotisations de taxe d'enlèvement des ordures ménagères auxquelles elle a été assujettie au titre de l'année 2013 à raison d'un immeuble dont elle est propriétaire dans la commune la commune de Laloubère (Hautes-Pyrénées)<br/>
<br/>
              2. Aux termes des dispositions du I de l'article 1520 du code général des impôts, applicable aux établissements publics de coopération intercommunale, dans sa rédaction applicable à l'imposition en cause : " Les communes qui assurent au moins la collecte des déchets des ménages peuvent instituer une taxe destinée à pourvoir aux dépenses du service dans la mesure où celles-ci ne sont pas couvertes par des recettes ordinaires n'ayant pas le caractère fiscal. (...) ". La taxe d'enlèvement des ordures ménagères n'a pas le caractère d'un prélèvement opéré sur les contribuables en vue de pourvoir à l'ensemble des dépenses budgétaires, mais a exclusivement pour objet de couvrir les dépenses exposées par la commune pour assurer l'enlèvement et le traitement des ordures ménagères et non couvertes par des recettes non fiscales. Ces dépenses sont constituées de la somme de toutes les dépenses de fonctionnement réelles exposées pour le service public de collecte et de traitement des déchets ménagers et des dotations aux amortissements des immobilisations qui lui sont affectées. Il en résulte que le produit de cette taxe et, par voie de conséquence, son taux, ne doivent pas être manifestement disproportionnés par rapport au montant de telles dépenses, tel qu'il peut être estimé à la date du vote de la délibération fixant ce taux. <br/>
<br/>
              3. Pour écarter le moyen, soulevé devant lui, tiré de ce que la délibération du 28 mars 2013 par laquelle le conseil de la communauté d'agglomération du grand Tarbes, compétente  en matière de traitement et de collecte des ordures ménagères, avait fixé le taux de la taxe d'enlèvement des ordures ménagères pour l'année 2013 à niveau manifestement disproportionné par rapport aux dépenses nécessaires à l'exploitation du service, le tribunal administratif a procédé a une comparaison entre le produit de la taxe d'enlèvement des ordures ménagères et les coûts du service d'enlèvement et de traitement des ordures ménagères non couverts par des recettes non fiscales. Il a pris en compte, à cet effet, outre les dépenses réelles de fonctionnement et les dotations aux amortissements des immobilisations qui y sont affectées, les dépenses d'acquisition des immobilisations et les frais financiers. En statuant ainsi, alors qu'il résulte de ce qui a été dit au point 2 qu'aux fins d'apprécier la légalité d'une délibération fixant le taux de taxe d'enlèvement des ordures ménagères, il n'y a pas lieu de tenir compte des données de la section d'investissement, à l'exclusion des dotations aux amortissements qui sont également retracées en opérations d'ordre dans la section de fonctionnement, le tribunal administratif a commis une erreur de droit. <br/>
<br/>
              4. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que la SAS L'immobilière groupe Casino est fondée à demander l'annulation du jugement attaqué. <br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros  à verser à la SAS L'immobilière groupe Casino au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
                            D E C I D E :<br/>
                              --------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Pau est annulé. <br/>
<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Pau.<br/>
<br/>
Article 3 : L'Etat versera la somme de 3 000 euros à la SAS L'immobilière groupe Casino au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la société par actions simplifiée (SAS) L'immobilière groupe Casino et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
