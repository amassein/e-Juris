<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036411823</ID>
<ANCIEN_ID>JG_L_2017_12_000000398239</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/41/18/CETATEXT000036411823.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 27/12/2017, 398239, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-12-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>398239</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>M. Matias de Sainte Lorette</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Yohann Bénard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:398239.20171227</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Nice de prononcer la décharge des cotisations supplémentaires d'impôt sur le revenu auxquelles il a été assujetti au titre des années 2005 à 2007, ainsi que des pénalités correspondantes. Par un jugement n° 1004613 du 25 octobre 2013, le tribunal administratif a décidé qu'il n'y avait pas lieu de statuer sur les conclusions de M. A...à hauteur des dégrèvements prononcés en cours d'instance et a rejeté le surplus des conclusions de sa demande.<br/>
<br/>
              Par un arrêt n° 13MA04926 du 19 janvier 2016, la cour administrative d'appel de Marseille a rejeté l'appel formé par M. A...contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire, un mémoire en réplique et un nouveau mémoire, enregistrés les 25 mars et 27 juin 2016 et les 24 août et 4 décembre 2017 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Par un mémoire en défense et un nouveau mémoire, enregistrés les 16 mai et 19 octobre 2017, le ministre de l'économie et des finances conclut au rejet de la requête. Il soutient que les moyens qu'elle soulève ne sont pas fondés.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Matias de Sainte Lorette, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de M. Yohann Bénard, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 10 du livre des procédures fiscales : " L'administration des impôts contrôle les déclarations ainsi que les actes utilisés pour l'établissement des impôts (...) / A cette fin, elle peut demander aux contribuables tous renseignements, justifications ou éclaircissements relatifs aux déclarations souscrites ou aux actes déposés (...). " et aux termes de l'article L. 11 du même livre : " A moins qu'un délai plus long ne soit prévu par le présent livre, le délai accordé aux contribuables pour répondre aux demandes de renseignements, de justifications ou d'éclaircissements (...) est fixé à trente jours à compter de la réception de cette notification ".<br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que M. A...a fait l'objet, en 2008 et 2009, d'un contrôle sur pièces portant sur les années 2005, 2006 et 2007, à l'issue duquel l'administration fiscale lui a notifié des rectifications en matière de revenus fonciers et de revenus de capitaux mobiliers, et l'a en conséquence assujetti à des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales, ainsi qu'à un rappel de contribution sur les revenus locatifs, assortis des intérêts de retard et de la majoration pour manquement délibéré. Le tribunal administratif de Nice a, par un jugement du 25 octobre 2013, prononcé un non lieu à statuer à hauteur des dégrèvements intervenus en cours d'instance, et a rejeté le surplus des conclusions en décharge présentées par M.A.... La cour administrative d'appel de Marseille a, par l'arrêt attaqué, rejeté l'appel formé contre ce jugement.<br/>
<br/>
              3. Pour juger que l'administration avait respecté le délai de trente jours fixé par l'article L. 11 du livre des procédures fiscales cité au point 1 ci-dessus, la cour administrative d'appel a relevé que la demande de renseignements adressée à M. A...était datée du 23 octobre 2008, que ce dernier n'avait jamais précisé la date à laquelle il avait reçu le courrier de l'administration et n'avait d'ailleurs pas expressément affirmé qu'il s'était écoulé moins de trente jours entre la réception de ce courrier et la date du 17 décembre 2008, à laquelle le service avait reçu sa réponse, pour en déduire qu'il avait bien disposé d'un délai de trente jours pour répondre à la demande qui lui a été adressée. En jugeant ainsi, la cour administrative d'appel de Marseille, qui, contrairement à ce que soutient M. A..., n'a pas fait reposer la charge de la preuve du respect de ces dispositions sur le contribuable, n'a, eu égard aux pièces du dossier qui lui était soumis, pas entaché son arrêt de dénaturation.<br/>
<br/>
              4. Ayant ainsi estimé que M. A...avait effectivement disposé d'un délai de trente jours, la cour a pu juger, sans commettre ni erreur de droit ni erreur de qualification juridique, que la mention de la demande de renseignements selon laquelle la réponse devait être adressée à l'administration fiscale " si possible avant le 28 novembre ", dont M. A...soutient qu'elle l'aurait induit en erreur sur le délai dont il disposait, n'était pas constitutive d'une irrégularité de nature à vicier la procédure d'imposition. <br/>
<br/>
              5. Enfin, la circonstance que la proposition de rectification ait été expédiée à la date à laquelle les observations en réponse à la demande de renseignements de M. A...ont été reçues, soit le 17 décembre 2008, ne suffit pas à établir  que l'administration aurait sciemment refusé de tenir compte du contenu de ces observations en réponse avant d'adresser au contribuable la proposition de rectification, ni que ce dernier, qui ne disposait d'ailleurs, avant l'envoi de cette proposition, d'aucune garantie relative au caractère contradictoire de la procédure, aurait été privé d'une telle garantie. Il suit de là que la cour n'a entaché son arrêt, sur ces points, d'aucune erreur de droit ni d'aucune erreur de qualification juridique.<br/>
<br/>
              6. Il résulte de ce qui précède que M. A...n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. A...est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. B...A...et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
