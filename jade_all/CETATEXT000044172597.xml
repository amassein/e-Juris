<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044172597</ID>
<ANCIEN_ID>JG_L_2021_10_000000437642</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/17/25/CETATEXT000044172597.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 06/10/2021, 437642, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-10-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>437642</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ZRIBI, TEXIER ; CABINET ROUSSEAU ET TAPIE</AVOCATS>
<RAPPORTEUR>M. Clément Tonon</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Philippe Ranquet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:437642.20211006</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Rennes d'annuler les décisions des 21 et 28 décembre 2015 par lesquelles la directrice des ressources humaines et des relations sociales, le directeur de l'économie RH et des ressources et le directeur courrier Haute-Bretagne de la société La Poste ont rejeté ses recours dirigés contre la décision implicite lui refusant le bénéfice de l'allocation spéciale de fin de carrière (ASFC) et de condamner La Poste à lui verser la somme de 29 900 euros. <br/>
<br/>
              Par un jugement n°s 1600846, 1600847 du 23 novembre 2017, le tribunal administratif de Rennes a annulé les décisions des 21 et 28 décembre 2015 et renvoyé M. B... devant son employeur pour la liquidation de l'allocation.<br/>
<br/>
              Par un arrêt n° 18NT00215 du 14 novembre 2019, la cour administrative d'appel de Nantes a rejeté l'appel formé par la société La Poste contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 14 janvier et 24 août 2020 et le 30 mars 2021 au secrétariat du contentieux du Conseil d'Etat, la société La Poste demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de M. B... la somme de 3 600 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des pensions civiles et militaires de retraite ;<br/>
              - le code de la sécurité sociale ;<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-834 du 13 septembre 1984 ;<br/>
              - la loi n° 90-568 du 2 juillet 1990 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Clément Tonon, auditeur,<br/>
<br/>
              - les conclusions de M. Philippe Ranquet, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Zribi, Texier, avocat de La Poste, et au cabinet Rousseau et Tapie, avocat de M. B... ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1.	Il ressort des pièces du dossier soumis aux juges du fond que M. A... B..., fonctionnaire de La Poste, a été admis à la retraite pour invalidité à compter du 11 octobre 2015 et que, dans cette perspective, il a sollicité, le 25 août 2015, le bénéfice de l'allocation spéciale de fin de carrière (ASFC). Par deux décisions des 21 et 28 décembre 2015, la société La Poste a rejeté sa demande d'attribution de cette allocation. Par un jugement du 23 novembre 2017, le tribunal administratif de Rennes a annulé ces décisions et renvoyé M. B... devant La Poste pour la liquidation de l'allocation. La société La Poste se pourvoit en cassation contre l'arrêt du 14 novembre 2019 par lequel la cour administrative d'appel de Nantes a rejeté son appel contre ce jugement.<br/>
<br/>
              2.	D'une part, aux termes de l'article L. 4 du code des pensions civiles et militaires de retraite : " Le droit à la pension est acquis : 1° Aux fonctionnaires après une durée fixée par décret en Conseil d'État ; 2° Sans condition de durée de service aux fonctionnaires radiés des cadres pour invalidité résultant ou non de l'exercice des fonctions ". Aux termes de l'article L. 24 du même code : " I. - La liquidation de la pension intervient : 1° Lorsque le fonctionnaire civil est radié des cadres par limite d'âge, ou s'il a atteint, à la date de l'admission à la retraite, l'âge mentionné à l'article L. 161-17-2 du code de la sécurité sociale, ou de cinquante-sept ans s'il a accompli au moins dix-sept ans de service dans des emplois classés dans la catégorie active./ Sont classés dans la catégorie active les emplois présentant un risque particulier ou des fatigues exceptionnelles. La nomenclature en est établie par décret en Conseil d'État ; 2° Lorsque le fonctionnaire est mis à la retraite pour invalidité et qu'il n'a pas pu être reclassé dans un emploi compatible avec son état de santé (...) ". En vertu du 2ème alinéa du I de l'article L. 14 du même code, " lorsque la durée d'assurance est inférieure au nombre de trimestres nécessaire pour obtenir le pourcentage de la pension mentionné à l'article L. 13, un coefficient de minoration de 1,25 % par trimestre s'applique au montant de la pension liquidée (...) ". Toutefois, le 7ème alinéa du I de l'article L. 14 prévoit que ce coefficient de minoration " n'est pas applicable aux fonctionnaires handicapés dont l'incapacité permanente est au moins égale à un taux fixé par décret ou mis à la retraite pour invalidité (...) ". <br/>
<br/>
              3.	D'autre part, le bénéfice du dispositif d'allocation spéciale de fin de carrière, institué par l'accord-cadre du 22 janvier 2013 sur la qualité de vie au travail à La Poste et reconduit par les accords collectifs relatifs au contrat de générations à La Poste, a été accordé aux fonctionnaires de La Poste par décision publiée au bulletin des ressources humaines 2015-0060 du 27 février 2015 de cette société. Selon le point 2 de cette décision : " Le bénéfice de cette allocation (...) est ouvert pour toute l'année 2015 aux agents fonctionnaires bénéficiaires du service actif, âgés de 56 à 59 ans, qui prennent leur retraite sans avoir au préalable bénéficié d'un dispositif aménagé de fin d'activité tel que le temps partiel aménagé senior (TPAS) ou tout autre dispositif antérieur (EGFA) ". Le point 3 de cette même décision dispose que le montant de l'allocation est modulé " en fonction d'une part, du nombre de trimestres manquants par rapport à la durée d'assurance requise pour obtenir une pension à taux plein et d'autre part de l'âge de départ en retraite des agents concernés ". <br/>
<br/>
              4.	L'institution de cette allocation spéciale de fin de carrière a entendu compenser la décote que subissent, par application du coefficient de minoration prévu au I de l'article L. 14 du code des pensions civiles et militaires de retraite, les agents bénéficiaires du service actif qui partent à la retraite par anticipation et ne peuvent à ce titre prétendre à une retraite à taux plein. Eu égard à cet objet, et ainsi que le mentionnent d'ailleurs expressément les accords collectifs et décisions intervenus depuis 2016, les dispositions de la décision de La Poste publiée le 27 février 2015 ne sauraient être interprétées comme ouvrant le bénéfice de l'allocation aux agents placés à la retraite pour invalidité, dès lors que le coefficient de minoration ne leur est pas applicable. Contrairement à ce qui est soutenu, la différence de traitement qui en résulte pour des agents ayant accompli des services relevant de la catégorie active, selon le motif et les conditions de départ à la retraite, est en rapport direct avec l'objet de la mesure. Il suit de là qu'en jugeant que M. B..., à raison des services actifs qu'il avait accomplis, pouvait prétendre au bénéfice de l'allocation spéciale de fin de carrière alors même qu'il avait été admis à la retraite pour invalidité, la cour administrative d'appel de Nantes a commis une erreur de droit.<br/>
<br/>
              5.	Il résulte de ce qui précède que, sans qu'il soit besoin de se prononcer sur l'autre moyen du pourvoi, la société La Poste est fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              6.	Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond, en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              7.	Il ressort des pièces du dossier que M. B... a été admis à la retraite pour invalidité. Dès lors, il résulte de ce qui a été dit au point 4 que le bénéfice de l'allocation spéciale de fin de carrière, tel qu'étendu aux fonctionnaires de La Poste par la décision de La Poste publiée le 27 février 2015, ne lui était pas ouvert. Par suite, La Poste est fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Rennes a annulé ses décisions des 21 et 28 décembre 2015 et a renvoyé M. B... devant elle aux fins de liquidation de cette allocation.<br/>
<br/>
              8.	Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de la société La Poste, qui n'est pas, dans la présente instance, la partie perdante, une somme à verser à M. B... au titre des frais exposés par lui et non compris dans les dépens. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de M. B... la somme que La Poste demande sur le fondement de ces mêmes dispositions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nantes du 14 novembre 2019 est annulé.<br/>
<br/>
Article 2 : Le jugement du tribunal administratif de Rennes du 23 novembre 2017 est annulé.<br/>
Article 3 : La demande présentée par M. B... devant le tribunal administratif de Rennes et ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : Le surplus des conclusions de la société La Poste est rejeté.<br/>
Article 5 : La présente décision sera notifiée à la société La Poste et à M. A... B....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
