<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042040562</ID>
<ANCIEN_ID>JG_L_2020_06_000000436721</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/04/05/CETATEXT000042040562.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 19/06/2020, 436721, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-06-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>436721</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Christelle Thomas</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Anne Iljic</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:436721.20200619</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un jugement n° 1501511 du 30 mai 2017, le tribunal administratif de Marseille a annulé la décision du 11 février 2015 par laquelle le directeur interrégional des services pénitentiaires de Provence-Alpes-Côte d'Azur a confirmé la décision du directeur du centre de détention de Marseille du 6 janvier 2015 retirant à Mme C... A... son permis de visite à M. D... B..., détenu. <br/>
<br/>
              Par un arrêt n° 17MA03326 du 14 octobre 2019, la cour administrative d'appel de Marseille a rejeté le recours de la garde des sceaux, ministre de la justice, dirigé contre ce jugement. <br/>
<br/>
              Par un pourvoi, enregistré le 13 décembre 2019 au secrétariat du contentieux du Conseil d'Etat, la garde des sceaux, ministre de la justice demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel.  <br/>
<br/>
              Elle soutient qu'en jugeant que le retrait litigieux revêtait un caractère disproportionné au regard de la gravité des faits reprochés à Mme A... et à M. B..., la cour administrative d'appel a entaché son arrêt d'erreur de droit et d'inexacte qualification juridique des faits.<br/>
<br/>
              Le pourvoi a été communiqué à Mme A... qui n'a pas produit de mémoire.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 2009-1436 du 24 novembre 2009 ;<br/>
              - le code de procédure pénale ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 modifiée ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Christelle Thomas, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Anne Iljic, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que par une décision du 11 février 2015, le directeur interrégional des services pénitentiaires de Provence-Alpes-Côte d'Azur a confirmé la décision du 6 janvier 2015 par laquelle le directeur du centre de détention de Marseille a retiré le permis délivré à Mme C... A... pour rendre visite à M. D... B..., à raison de la découverte sur celle-ci de vingt-quatre grammes de cannabis, d'un téléphone portable et d'une carte SIM qu'elle tentait d'introduire au sein de l'établissement pénitentiaire. Par un jugement du 30 mai 2017, le tribunal administratif de Marseille a annulé, à la demande de Mme A..., cette décision. Par un arrêt du 14 octobre 2019, contre lequel la garde des sceaux, ministre de la justice se pourvoit en cassation, la cour administrative d'appel a rejeté l'appel dirigé contre ce jugement. <br/>
<br/>
              2. L'article 35 de la loi pénitentiaire du 24 novembre 2009 dispose que : " Le droit des personnes détenues au maintien des relations avec les membres de leur famille s'exerce soit par les visites que ceux-ci leur rendent, soit, pour les condamnés et si leur situation pénale l'autorise, par les permissions de sortir des établissements pénitentiaires. Les prévenus peuvent être visités par les membres de leur famille ou d'autres personnes, au moins trois fois par semaine, et les condamnés au moins une fois par semaine. / L'autorité administrative ne peut refuser de délivrer un permis de visite aux membres de la famille d'un condamné, suspendre ou retirer ce permis que pour des motifs liés au maintien du bon ordre et de la sécurité ou à la prévention des infractions. / L'autorité administrative peut également, pour les mêmes motifs ou s'il apparaît que les visites font obstacle à la réinsertion du condamné, refuser de délivrer un permis de visite à d'autres personnes que les membres de la famille, suspendre ce permis ou le retirer. / Les permis de visite des prévenus sont délivrés par l'autorité judiciaire. / Les décisions de refus de délivrer un permis de visite sont motivées ". L'article R. 57-8-10 du code de procédure pénale désigne le chef d'établissement comme l'autorité responsable de la délivrance, de la suspension ou du retrait d'un permis de visiter une personne condamnée et le dernier alinéa de l'article R. 57-8-15 du même code dispose que : " Les incidents mettant en cause les visiteurs sont signalés à l'autorité ayant délivré le permis qui apprécie si le permis doit être suspendu ou retiré. " Aux termes de l'article R. 57-8-11 du même code : " Le chef d'établissement fait droit à tout permis de visite qui lui est présenté, sauf à surseoir si des circonstances exceptionnelles l'obligent à en référer à l'autorité qui a délivré le permis, ou si les personnes détenues sont matériellement empêchées, ou si, placées en cellule disciplinaire, elles ont épuisé leur droit à un parloir hebdomadaire. ". <br/>
<br/>
              3. Il résulte de ces dispositions que les décisions tendant à restreindre ou supprimer les permis de visite relèvent du pouvoir de police des chefs d'établissements pénitentiaires. Ces mesures de police qui affectent directement le maintien des liens des personnes détenues avec leurs proches tendent au maintien du bon ordre et de la sécurité au sein des établissements pénitentiaires ou à la prévention des infractions. <br/>
<br/>
              4. Après avoir relevé que la tentative d'introduction des objets et produits retrouvés sur Mme A... était de nature à troubler le maintien de la sécurité et du bon ordre au sein de l'établissement, la cour administrative d'appel a entaché son arrêt d'erreur de droit en déduisant que la décision de retrait du permis de visite de celle-ci revêtait un caractère disproportionné par rapport à la gravité des faits et leur caractère isolé du seul motif que ces objets et produits n'étaient pas de nature à favoriser une action violente de la part de M. B.... <br/>
<br/>
              5. Il résulte de ce qui précède que, sans qu'il soit besoin de se prononcer sur l'autre moyen du pourvoi, la garde des sceaux, ministre de la justice, est fondée à demander l'annulation de l'arrêt qu'elle attaque. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 14 octobre 2019 est annulé. <br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
Article 3 : La présente décision sera notifiée à la garde des sceaux, ministre de la justice et à Mme C... A.... <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
