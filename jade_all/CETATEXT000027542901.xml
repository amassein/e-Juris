<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027542901</ID>
<ANCIEN_ID>JG_L_2013_06_000000358921</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/54/29/CETATEXT000027542901.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 12/06/2013, 358921, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-06-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>358921</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>M. Pascal Trouilly</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:358921.20130612</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 27 avril et 14 mai 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune de Lambesc, représentée par son maire ; la commune de Lambesc demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'ordonnance n° 1201454 du 11 avril 2012 par laquelle le juge des référés du tribunal administratif de Marseille, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, à la demande des sociétés civiles immobilières Merlot et Valmousse, a ordonné la suspension de l'exécution de l'arrêté du 20 janvier 2012 par lequel le maire de la commune a décidé de surseoir à statuer sur la demande de permis de construire présentée par la SCI Merlot pour la réalisation d'un immeuble de deux logements sur un terrain situé route de Coudoux ; <br/>
<br/>
              2°) statuant en référé, de rejeter la demande présentée par les SCI Merlot et Valmousse devant le tribunal administratif de Marseille ; <br/>
<br/>
              3°) de mettre à la charge de la SCI Merlot et de la SCI Valmousse la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu le code de l'urbanisme ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pascal Trouilly, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de la commune de Lambesc et à la SCP Lyon-Caen, Thiriez, avocat de la SCI Merlot et de la SCI Valmousse ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que la SCI Merlot a présenté le 12 octobre 2011 une demande de permis de construire en vue de la réalisation d'une maison de deux logements sur un terrain appartenant à la SCI Valmousse, situé à Lambesc et classé en zone NB du plan d'occupation des sols ; que, par un arrêté du 20 janvier 2012, le maire de la commune a décidé de surseoir à statuer sur cette demande de permis de construire, aux motifs que l'élaboration d'un nouveau plan local d'urbanisme avait été prescrite et que le projet était susceptible de compromettre " la conservation des espaces naturels " et " l'intérêt paysager du site " ; que, par une ordonnance du 11 avril 2012, le juge des référés du tribunal administratif de Marseille a, à la demande des deux sociétés, suspendu l'exécution de cette décision de sursis à statuer, en estimant, d'une part, que la condition d'urgence était remplie et, d'autre part, que deux moyens étaient propres, en l'état de l'instruction, à créer un doute sérieux quant à sa légalité ; que la commune de Lambesc se pourvoit en cassation contre cette ordonnance ; <br/>
<br/>
              2. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ;<br/>
<br/>
              3. Considérant que l'urgence justifie que soit prononcée la suspension d'un acte administratif lorsque l'exécution de celui-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ; que la décision par laquelle l'autorité compétente sursoit à statuer sur une demande de permis de construire, en application des articles L. 111-7 et L. 123-6 du code de l'urbanisme, afin d'éviter que le projet du pétitionnaire ne compromette ou ne rende plus onéreuse l'exécution d'un futur plan local d'urbanisme en cours d'élaboration, ne crée une situation d'urgence que si le requérant justifie, en invoquant des circonstances particulières, que cette décision affecte gravement sa situation ; qu'en estimant que la condition d'urgence était remplie au seul motif que la SCI Valmousse avait conclu avec la SCI Merlot, antérieurement à la décision de sursis à statuer, une promesse synallagmatique portant sur la conclusion d'un bail à construction en vue de la réalisation du projet, comportant une clause de caducité et ayant donné lieu au versement au bailleur, à titre définitif, d'une somme d'argent, alors que ces sociétés ne précisaient pas en quoi l'éventuelle caducité de cette promesse  était de nature à affecter gravement leur situation, le juge des référés a porté sur les intérêts en présence une appréciation entachée de dénaturation ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la commune de Lambesc est fondée à demander l'annulation de l'ordonnance qu'elle attaque ; <br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de statuer sur la demande de suspension en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              5. Considérant que pour justifier de l'urgence à suspendre l'exécution de la décision litigieuse, les sociétés requérantes, dont le siège est d'ailleurs situé à la même adresse, se bornent à invoquer le risque de caducité de la promesse de bail à construction qu'elles ont conclue ; que, compte tenu tant de l'absence d'obstacle à la reconduction de cette promesse que de l'intérêt public qui s'attache à l'exécution du futur plan local d'urbanisme, lequel prévoit notamment de renforcer la préservation du caractère naturel de la zone concernée, comprise dans une zone de protection spéciale Natura 2000, la condition d'urgence ne peut être regardée comme remplie ; qu'il y a lieu, par suite, sans qu'il soit besoin d'examiner si les moyens soulevés par la SCI Merlot et la SCI Valmousse sont de nature à faire naître un doute sérieux quant à la légalité de la décision de sursis à statuer du 20 janvier 2012, de rejeter la demande de suspension présentée par ces sociétés ; <br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que la commune de Lambesc, qui n'est pas la partie perdante dans la présente instance, verse à la SCI Merlot et à la SCI Valmousse les sommes demandées par celles-ci au titre des frais exposés dans cette instance et non compris dans les dépens ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la SCI Merlot et de la SCI Valmousse le versement à la commune de Lambesc d'une somme globale de 3 000 euros au titre des mêmes dispositions ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance n° 1201454 du 11 avril 2012 du juge des référés du tribunal administratif de Marseille est annulée.<br/>
Article 2 : La demande présentée par la SCI Merlot et la SCI Valmousse devant le juge des référés du tribunal administratif de Marseille, ainsi que leurs conclusions présentées devant le Conseil d'Etat au titre de l'article L. 761-1 du code de justice administrative, sont rejetées.<br/>
Article 3 : La SCI Merlot et à la SCI Valmousse verseront à la commune de Lambesc la somme globale de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la commune de Lambesc, à la SCI Merlot et à la SCI Valmousse.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
