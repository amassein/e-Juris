<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036086483</ID>
<ANCIEN_ID>JG_L_2017_11_000000394915</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/08/64/CETATEXT000036086483.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 24/11/2017, 394915</TITRE>
<DATE_DEC>2017-11-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>394915</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Sylvain Monteillet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:394915.20171124</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés les 30 novembre 2015 et 29 février 2016 au secrétariat du contentieux du Conseil d'Etat, la société civile d'exploitation agricole (SCEA) Vignobles Massieu demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2015-1193 du 28 septembre 2015 relatif à l'appellation d'origine contrôlée " Bordeaux " ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le règlement (UE) n° 1308/2013 du Parlement européen et du Conseil du 17 décembre 2013 ; <br/>
              - le règlement (CE) n° 607/2009 de la Commission du 14 juillet 2009 ;<br/>
              - le code rural et de la pêche maritime ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Sylvain Monteillet, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de la société civile d'exploitation agricole vignobles Massieu et à la SCP Didier, Pinet, avocat de l'institut national de l'origine et de la qualité (INAO) ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 9 novembre 2017, présentée par l'INAO ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Par le décret dont la SCEA Vignobles Massieu demande l'annulation pour excès de pouvoir, le Premier ministre a homologué un nouveau cahier des charges de l'appellation d'origine contrôlée (AOC) " Bordeaux ". Dans les termes dans lesquels les conclusions de la requête de la SCEA Vignobles Massieu sont formulées, cette requête doit être regardée comme tendant à l'annulation du décret attaqué en tant que le cahier des charges qu'il homologue renvoie, dans son annexe, à une délibération du 9 juin 2015 par laquelle le comité national des appellations d'origine relatives aux vins et boissons alcoolisés et des eaux-de-vie a approuvé l'aire parcellaire délimitée au titre de la commune de Mourens (Gironde) et en exclut une partie des parcelles cadastrées section ZA n° 10, 12, 13 et 42 appartenant à la SCEA Vignobles Massieu.<br/>
<br/>
              Sur la légalité externe :<br/>
<br/>
              2. En premier lieu, aux termes de l'article L. 641-6 du code rural et de la pêche maritime, dans sa rédaction applicable au litige : " La reconnaissance d'une appellation d'origine contrôlée est proposée par l'Institut national de l'origine et de la qualité, après avis de l'organisme de défense et de gestion prévu à l'article L. 642-17 (...) ". Aux termes du I de l'article R. 641-20-1 du même code, dans sa rédaction applicable au litige : " La demande de modification d'un cahier des charges d'une appellation d'origine, d'une indication géographique protégée ou d'une spécialité traditionnelle garantie est soumise pour approbation au comité national compétent de l'Institut national de l'origine et de la qualité. Lorsque ce dernier estime qu'elle comporte des modifications majeures, la demande est soumise à une procédure nationale d'opposition dans les conditions prévues à l'article R. 641-13 ". Il résulte de ces dispositions que la consultation de l'organisme de défense et de gestion n'est pas requise en cas de modification du cahier des charges d'une appellation d'origine déjà existante. Or l'homologation, par le décret attaqué, du cahier des charges relatifs à l'appellation d'origine contrôlée " Bordeaux " ne constitue pas la reconnaissance d'une nouvelle appellation au sens de l'article L. 641-6 du code rural et de la pêche maritime. Il suit de là que le moyen tiré d'un défaut de consultation doit être écarté.<br/>
<br/>
              3. En deuxième lieu, le décret litigieux ne constitue pas une mesure d'application de l'acte par lequel le directeur de l'INAO a, en application des articles L. 642-17 à L. 642-20 et R. 642-34 du code rural et de la pêche maritime, prononcé la reconnaissance de ces organismes de défense et de gestion. Les actes de reconnaissance de ces organismes ne constituent pas davantage la base légale des décrets attaqués. Ainsi, la requérante ne peut exciper, par voie d'exception, de l'illégalité dont serait, selon elle, entachée la reconnaissance du syndicat viticole des appellations contrôlées " Bordeaux " et " Bordeaux supérieur " en tant qu'organismes de défense et de gestion.<br/>
<br/>
              4. En troisième lieu, aux termes de l'article 93 du règlement (UE) n° 1308/2013 du Parlement européen et du Conseil du 17 décembre 2013 portant organisation commune des marchés des produits agricoles : " 1. Aux fins de la présente section, on entend par : / a) " appellation d'origine ", le nom d'une région, d'un lieu déterminé ou, dans des cas exceptionnels et dûment justifiés, d'un pays, qui sert à désigner un produit visé à l'article 92, paragraphe 1, satisfaisant aux exigences suivantes : i) sa qualité et ses caractéristiques sont dues essentiellement ou exclusivement à un milieu géographique particulier et aux facteurs naturels et humains qui lui sont inhérents ; / ii) il est élaboré exclusivement à partir de raisins provenant de la zone géographique considérée ; / iii) sa production est limitée à la zone géographique considérée (...) ". Aux termes de l'article 94 du même règlement : " 1. Les demandes de protection de dénominations en tant qu'appellations d'origine ou indications géographiques sont accompagnées d'un dossier technique comportant: (...) d) un document unique résumant le cahier des charges visé au paragraphe 2. / 2. Le cahier des charges permet aux parties intéressées de vérifier le respect des conditions de production associées à l'appellation d'origine ou à l'indication géographique. / Le cahier des charges comporte au minimum les éléments suivants : / (...) d) la délimitation de la zone géographique concernée (...) ". Aux termes de l'article 98 du même règlement : " Dans un délai de deux mois à compter de la date de la publication du document unique visé à l'article 94, paragraphe 1, point d), tout État membre ou pays tiers ou toute personne physique ou morale ayant un intérêt légitime et résidant ou établie dans un État membre autre que celui qui a demandé la protection ou dans un pays tiers, peut s'opposer à la protection envisagée, en déposant auprès de la Commission une déclaration dûment motivée concernant les conditions d'admissibilité fixées dans la présente sous-section ". Aux termes de l'article 110 du même règlement : " (...) 2. La Commission peut adopter des actes d'exécution fixant les mesures nécessaires relatives à la présente sous-section en ce qui concerne la procédure applicable (...) pour l'approbation de la modification d'une appellation d'origine (...) ". Aux termes de l'article 5 du règlement (CE) n° 607/2009 de la Commission du 14 juillet 2009 fixant certaines modalités d'application du règlement (CE) n° 479/2008 du Conseil en ce qui concerne les appellations d'origine protégées et les indications géographiques protégées, les mentions traditionnelles, l'étiquetage et la présentations de certains produits du secteur vitivinicole : " La zone géographique est délimitée d'une manière précise, détaillée et univoque ". Aux termes de l'article 20 du même règlement : " Une modification est considérée comme mineure si : / (...) d) elle n'influe pas sur la zone géographique délimitée (...) ". Il en résulte, d'une part, que, pour l'application de l'article R. 641-20-1 du code rural et de la pêche maritime cité au point 2 ci-dessus, une procédure nationale d'opposition est mise en oeuvre lorsque la modification du cahier des charges de l'appellation d'origine est majeure et, d'autre part, qu'une modification doit être regardée comme telle si elle affecte la délimitation précise et détaillée de la zone géographique relative à cette appellation d'origine.<br/>
<br/>
              5. Aux termes de l'article L. 641-6 du code rural et de la pêche maritime, dans sa rédaction applicable au litige : " (...) La proposition de l'institut porte sur la délimitation de l'aire géographique de production, définie comme la surface comprenant les communes ou parties de communes propres à produire l'appellation d'origine, ainsi que sur la détermination des conditions de production qui figurent dans un cahier des charges (...) ". Aux termes de l'article L. 641-7 du même code, dans sa rédaction applicable au litige : " La reconnaissance d'une appellation d'origine contrôlée est prononcée par un décret qui homologue un cahier des charges où figurent notamment la délimitation de l'aire géographique de production de cette appellation ainsi que ses conditions de production (...) ". Aux termes de l'article R. 641-16 du même code, dans sa rédaction applicable au litige : " A l'intérieur de l'aire géographique délimitée par le décret prononçant la reconnaissance du bénéfice d'une appellation d'origine ou par l'arrêté homologuant le cahier des charges d'une indication géographique protégée relative à des vins, des zones affectées à l'une des phases de la production ou de l'élaboration ou de la transformation du produit peuvent être définies ".<br/>
<br/>
              6. Il résulte des dispositions citées aux points 4 et 5 ci-dessus que, si une modification de l'aire géographique délimitée par le cahier des charges d'une appellation d'origine doit, en principe, être regardée comme majeure, une modification de l'aire parcellaire délimitée en application de l'article R. 641-16 du code rural et de la pêche maritime ne peut s'apparenter à une modification majeure que si elle conduit à affecter la consistance de cette aire géographique. En l'espèce, le décret attaqué ne modifie pas l'aire géographique de l'AOC Bordeaux. En outre, il ne ressort pas des pièces du dossier que les modifications de l'aire parcellaire délimitée auxquelles il procède, qui restent circonscrites, affectent la consistance de l'aire géographique. En particulier, les modifications d'aire parcellaire approuvée le 9 juin 2015 par le comité national des appellations d'origine relatives aux vins et boissons alcoolisés et des eaux-de-vie ne portent que sur 167 hectares, représentant 0,48 % de la surface totale de l'aire parcellaire de l'AOC " Bordeaux ". Par suite, la SCEA Vignobles Massieu n'est pas fondée à soutenir que le décret qu'elle attaque est entaché d'un vice de procédure dès lors qu'aucune procédure nationale d'opposition n'a été réalisée.<br/>
<br/>
              Sur la légalité interne :<br/>
<br/>
              7. En premier lieu, aux termes de l'article L. 641-5 du code rural et de la pêche maritime : " Peuvent bénéficier d'une appellation d'origine contrôlée les produits agricoles, forestiers ou alimentaires et les produits de la mer, bruts ou transformés, qui remplissent les conditions fixées par les dispositions de l'article L. 115-1 du code de la consommation, possèdent une notoriété dûment établie et dont la production est soumise à des procédures comportant une habilitation des opérateurs, un contrôle des conditions de production et un contrôle des produits ". Aux termes de l'article L. 115-1 du code de la consommation, dans sa rédaction en vigueur à la date du décret attaqué : " Constitue une appellation d'origine la dénomination d'un pays, d'une région ou d'une localité servant à désigner un produit qui en est originaire et dont la qualité ou les caractères sont dus au milieu géographique, comprenant des facteurs naturels et des facteurs humains ".<br/>
<br/>
              8. Il résulte de ces dispositions et de l'article 93 du règlement du Parlement européen et du Conseil du 17 décembre 2013, cité au point 4, que l'autorité administrative est tenue, pour déterminer l'aire géographique de production, de se fonder à la fois sur des facteurs naturels et des facteurs humains façonnant la qualité ou les caractères propres du produit désigné par l'appellation d'origine.<br/>
<br/>
              9. Si, dans le cas d'une appellation d'origine d'un vin, l'autorité administrative décide, en application des dispositions de l'article R. 641-16 du code rural et de la pêche maritime, de délimiter au sein de l'aire géographique de production une aire, dite parcellaire, comprenant les seules parcelles de vignes aptes à produire le raisin exclusivement utilisé pour l'élaboration du vin objet de l'appellation, elle est en droit, en procédant à cette délimitation, d'exclure de celle-ci toute parcelle comprise dans l'aire géographique ne satisfaisant pas aux exigences découlant des seuls facteurs naturels retenus pour la délimitation de l'aire géographique. Par suite, la SCEA Vignobles Massieu n'est pas fondée à soutenir que le décret qu'elle attaque est entaché d'erreur de droit en ce qu'il exclut de l'aire parcellaire délimitée de l'AOC " Bordeaux ", sur le seul fondement d'un facteur naturel, une partie des parcelles cadastrées section ZA n° 10, 12, 13 et 42 sur le territoire de la commune de Mourens.<br/>
<br/>
              10. En second lieu, il ressort des pièces du dossier que, pour exclure une partie des parcelles litigieuses de l'aire parcellaire délimitée de l'appellation d'origine contrôlée " Bordeaux ", la commission d'experts de l'INAO s'est fondée sur la circonstance que la position basse de ces parcelles affecte la qualité de la vigne, du fait des risques accrus de gel et d'une situation hydrographique défavorable. En outre, la SCEA Vignobles Massieu ne démontre pas en quoi cette exclusion résulterait d'un traitement différent de celui appliqué à des producteurs possédant des parcelles dans une situation identique. Dès lors, le moyen tiré de ce que le décret attaqué est entaché d'erreur d'appréciation et méconnaît le principe d'égalité doit être écarté.<br/>
<br/>
              11. Il résulte de ce qui précède, sans qu'il soit besoin de statuer sur la fin de non-recevoir opposée par le ministre de l'agriculture, de l'agroalimentaire et de la forêt, que la SCEA Vignobles Massieu n'est pas fondée à demander l'annulation du décret qu'elle attaque, en tant qu'il homologue les dispositions du cahier des charges relatives à l'aire parcellaire délimitée de l'AOC " Bordeaux " excluant une partie des parcelles cadastrées section ZA n° 10, 12, 13 et 42, situées sur le territoire de la commune de Mourens et appartenant à cette société.<br/>
<br/>
              12. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mis à la charge de l'État, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la SCEA Vignobles Massieu est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la SCEA Vignobles Massieu, au Premier ministre, au ministre de l'agriculture et de l'alimentation, au ministre de l'économie et des finances et à l'Institut national de l'origine et de la qualité.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">03-05-06-02 AGRICULTURE ET FORÊTS. PRODUITS AGRICOLES. VINS. CONTENTIEUX DES APPELLATIONS. - APPELLATION D'ORIGINE - 1) DEMANDE DE MODIFICATION DU CAHIER DES CHARGES (ART. R. 641-20-1 DU CODE RURAL ET DE LA PÊCHE MARITIME) - CARACTÈRE MAJEUR DE LA MODIFICATION - A) NOTION - B) CONSÉQUENCE - MISE EN OEUVRE D'UNE PROCÉDURE NATIONALE D'OPPOSITION - C) CAS D'UNE MODIFICATION DE L'AIRE GÉOGRAPHIQUE DÉLIMITÉE - CARACTÈRE MAJEUR DE LA MODIFICATION - EXISTENCE EN PRINCIPE - CAS D'UNE MODIFICATION DE L'AIRE PARCELLAIRE (ART. R. 641-16 DU CODE RURAL ET DE LA PÊCHE MARITIME) - CARACTÈRE MAJEUR DE LA MODIFICATION - EXISTENCE, SEULEMENT SI ELLE CONDUIT À AFFECTER LA CONSISTANCE DE L'AIRE GÉOGRAPHIQUE - 2) CONTRÔLE DU JUGE SUR LA DÉLIMITATION D'UNE AIRE PARCELLAIRE - CONTRÔLE NORMAL [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-02-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR. APPRÉCIATIONS SOUMISES À UN CONTRÔLE NORMAL. - DÉLIMITATION D'UNE AIRE PARCELLAIRE D'UNE APPELLATION D'ORIGINE [RJ1].
</SCT>
<ANA ID="9A"> 03-05-06-02 1) a) ll résulte des articles 93, 98 et 110 du règlement (UE) n°1308/2013 du Parlement européen et du Conseil du 17 décembre 2013 portant organisation commune des marchés agricoles et de l'article 5 du règlement (CE) n°607/2009 de la Commission du 14 juillet 2009 fixant certaines modalités d'application du règlement (CE) n°479/2008 du Conseil, d'une part, que, pour l'application de l'article R. 641-20-1 du code rural et de la pêche maritime, une procédure nationale d'opposition est mise en oeuvre lorsque la modification du cahier des charges de l'appellation d'origine est majeure et, d'autre part, qu'une modification doit être regardée comme telle si elle affecte la délimitation précise et détaillée de la zone géographique relative à cette appellation d'origine.,,,b) Il résulte de ces mêmes articles et des articles L. 641-6, L. 641-7 et R. 641-16 du même code que, si une modification de l'aire géographique délimitée par le cahier des charges d'une appellation d'origine doit, en principe, être regardée comme majeure, une modification de l'aire parcellaire délimitée en application de l'article R. 641-16 du même code ne peut s'apparenter à une modification majeure que si elle conduit à affecter la consistance de cette aire géographique.,,,2) Le juge de l'excès de pouvoir exerce un contrôle normal sur la délimitation d'une aire parcellaire d'une appellation d'origine.</ANA>
<ANA ID="9B"> 54-07-02-03 Le juge de l'excès de pouvoir exerce un contrôle normal sur la délimitation d'une aire parcellaire d'une appellation d'origine.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant du contrôle du juge sur la délimitation de l'aire géographique d'une appellation d'origine contrôlée, CE, 10 février 2014, Syndicat viticole de Cussac-Fort-Médoc, n° 356113, p. 25.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
