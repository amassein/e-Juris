<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034900530</ID>
<ANCIEN_ID>JG_L_2017_06_000000398519</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/90/05/CETATEXT000034900530.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 09/06/2017, 398519</TITRE>
<DATE_DEC>2017-06-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>398519</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Yves Doutriaux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:398519.20170609</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés les 4 avril et 4 juillet 2016 au secrétariat du contentieux du Conseil d'Etat, Mme B...A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du 5 octobre 2015 du Président de la République mettant fin à ses fonctions de directrice générale de l'Institut national de la consommation et le décret du 5 octobre 2015 du Président de la République nommant Mme C... directrice générale de l'Institut national de la consommation ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la consommation ;<br/>
              - la loi du 22 avril 1905, notamment son article 65 ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - le décret n° 85-779 du 24 juillet 1985 ;<br/>
              - le décret n° 86-83 du 17 janvier 1986 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yves Doutriaux, conseiller d'Etat, <br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant que Mme A...demande l'annulation pour excès de pouvoir du décret du Président de la République du 5 octobre 2015 mettant fin à ses fonctions de directrice générale de l'Institut national de la consommation et, par voie de conséquence, celle du décret du 5 octobre 2015 du Président de la République nommant Mme C...directrice générale de l'Institut national de la consommation ;<br/>
<br/>
              2.	Considérant que l'Institut national de la consommation, établissement public national à caractère économique et commercial, a pour mission, en application des dispositions du code de la consommation, de fournir un appui technique aux associations de défense des consommateurs et de diffuser à destination du grand public des informations sur les questions de consommation ; que cet institut est dirigé par un directeur général nommé par décret du Président de la République pris sur le rapport du Premier ministre et du ministre chargé de la consommation ; qu'eu égard à ses conditions de nomination et aux caractéristiques de ses fonctions, le directeur général de cet établissement public occupe un emploi supérieur à la décision du Gouvernement ; qu'il peut, en conséquence, être mis fin à tout moment à ses fonctions ; que MmeA..., agent contractuel de l'Etat, ne peut utilement invoquer les dispositions du décret du 17 janvier 1986 relatif aux dispositions générales applicables aux agents contractuels de l'Etat à l'encontre de la décision du Président de la République mettant fin à ses fonctions de directrice générale de l'Institut national de la consommation, ces dispositions n'étant pas applicables aux agents contractuels recrutés pour occuper des emplois supérieurs dont la nomination est laissée à la décision du Gouvernement en vertu de l'article 1er de ce décret ; qu'elle ne peut pas non plus utilement soutenir que sa manière de servir n'était pas de nature à justifier que le Président de la République mette fin à ses fonctions par le décret attaqué ;<br/>
<br/>
              3.	Considérant que la décision litigieuse constitue une mesure prise en considération de la personne ; que, dès lors, Mme A...devait être préalablement mise à même de demander en temps utile la communication de son dossier afin de pouvoir, le cas échéant, faire connaître à l'autorité compétente ses observations sur la mesure envisagée, conformément aux prescriptions de l'article 65 de la loi du 22 avril 1905, applicable à tout agent public ; qu'il ressort des pièces du dossier que Mme A...a reçu le 15 juin 2015 une lettre de la secrétaire d'Etat  chargée du commerce, de l'artisanat, de la consommation et de l'économie sociale et solidaire par laquelle elle a été informée de ce qu'une décision mettant fin à ses fonctions était envisagée et qu'elle avait la possibilité de consulter son dossier et de faire parvenir ses observations dans un délai de dix jours à compter de la date de cette consultation ; que Mme A... a consulté son dossier le 24 août 2015 ; que si, compte tenu de la nature de l'emploi occupé par Mme A...et de son statut de contractuelle, son dossier ne comportait qu'un nombre limité de documents, il ne ressort pas des pièces du dossier que les obligations prescrites par l'article 65 de la loi du 22 avril 1905 aient été, en l'espèce, méconnues ;<br/>
              4.	Considérant qu'il résulte de ce qui précède que Mme A...n'est pas fondée à demander l'annulation du décret du 5 octobre 2015 mettant fin à ses fonctions de directrice générale de l'Institut national de la consommation, ni, par voie de conséquence, celle du décret du même jour nommant son successeur ; que ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de Mme A...est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à Mme B...A...et au ministre de l'économie.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-02-03 FONCTIONNAIRES ET AGENTS PUBLICS. CADRES ET EMPLOIS. RÉPARTITION ET CLASSIFICATION DES EMPLOIS. - EMPLOIS SUPÉRIEURS À LA DISCRÉTION DU GOUVERNEMENT - DIRECTEUR GÉNÉRAL DE L'INC - EXISTENCE.
</SCT>
<ANA ID="9A"> 36-02-03 Eu égard à ses conditions de nomination et aux caractéristiques de ses fonctions, le directeur général de l'Institut national de la consommation (INC) occupe un emploi supérieur à la décision du Gouvernement.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
