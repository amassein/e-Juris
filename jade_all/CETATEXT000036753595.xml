<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036753595</ID>
<ANCIEN_ID>JG_L_2018_03_000000402913</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/75/35/CETATEXT000036753595.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 1ère chambres réunies, 28/03/2018, 402913</TITRE>
<DATE_DEC>2018-03-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>402913</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MEIER-BOURDEAU, LECUYER ; SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Jean-François de Montgolfier</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHR:2018:402913.20180328</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C... E...a demandé au tribunal administratif de Montpellier, d'une part, d'annuler pour excès de pouvoir la décision du 3 octobre 2012  du directeur des ressources humaines du Centre national de la recherche scientifique (CNRS) refusant de transformer son contrat de travail à durée déterminée en contrat à durée indéterminée et, d'autre part, d'enjoindre au CNRS de procéder à cette transformation. Par un jugement n° 1205171 du 28 février 2014, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 14MA01833 du 24 juin 2016, la cour administrative d'appel de Marseille a, sur appel de M. E..., annulé ce jugement et la décision du 3 octobre 2012 et enjoint au CNRS de proposer à M. E... un contrat de travail à durée indéterminée.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 29 août et 22 novembre 2016 au secrétariat du contentieux du Conseil d'Etat, le Centre national de recherche scientifique demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M. E....<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 2012-347 du 12 mars 2012 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-François de Montgolfier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Meier-Bourdeau, Lecuyer, avocat du Centre national de la recherche scientifique et à la SCP Rocheteau, Uzan-Sarano, avocat de M. E...;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 15 mars 2018, présentée par le CNRS ;<br/>
<br/>
<br/>
<br/>
<br/>1.  Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, du 16 février 2005 au 1er janvier 2007, M. E... a accompli un travail de recherche en qualité de " chercheur post-doctoral " au sein du " centre de recherche en biochimie macromoléculaire " du Centre national de la recherche scientifique (CNRS), à Montpellier ; qu'il a ensuite été recruté par le CNRS comme chercheur contractuel, entre janvier 2007 et juin 2012, en vertu de plusieurs contrats à durée déterminée successifs, pour accomplir des travaux de recherche à " l'Institut de génétique moléculaire " du CNRS, à Montpellier ; que le CNRS se pourvoit en cassation contre l'arrêt du 24 juin 2016 par lequel la cour administrative d'appel de Marseille a annulé la décision de son directeur des ressources humaines du 3 octobre 2012 refusant de transformer le dernier contrat à durée déterminée de M. E... en contrat à durée indéterminée ; <br/>
<br/>
              2.  Considérant qu'aux termes des deux premiers alinéas de l'article 8 de la loi du 12 mars 2012 relative à l'accès à l'emploi titulaire et à l'amélioration des conditions d'emploi des agents contractuels dans la fonction publique, à la lutte contre les discriminations et portant diverses dispositions relatives à la fonction publique : " A la date de publication de la présente loi, la transformation de son contrat en contrat à durée indéterminée est obligatoirement proposée à l'agent contractuel, employé par l'Etat, l'un de ses établissements publics ou un établissement public local d'enseignement sur le fondement du dernier alinéa de l'article 3 ou des articles 4 ou 6 de la loi n° 84-16 du 11 janvier 1984 précitée dans sa rédaction antérieure à celle résultant de la présente loi, qui se trouve en fonction ou bénéficie d'un congé prévu par le décret pris en application de l'article 7 de la même loi. / Le droit défini au premier alinéa du présent article est subordonné à une durée de services publics effectifs, accomplis auprès du même département ministériel, de la même autorité publique ou du même établissement public, au moins égale à six années au cours des huit années précédant la publication de la présente loi " ; que, pour annuler la décision litigieuse, la cour administrative d'appel de Marseille a jugé que la période de travail effectuée par M. E...du 16 février 2005 au 1er janvier 2007 en qualité de " chercheur post-doctoral " devait être prise en compte au titre des années de services publics effectifs accomplis auprès du CNRS, au sens de ces dispositions ;<br/>
<br/>
              3. Considérant qu'en estimant que M. E... avait, durant la période du 16 février 2005 au 1er janvier 2007, participé à des travaux de recherche collectivement effectués au sein d'une unité de recherche du CNRS, sous la supervision directe du directeur de cette unité et en étant soumis aux mêmes obligations de travail et aux mêmes sujétions que les chercheurs du CNRS qui concouraient à ces travaux, la cour administrative d'appel a, sans les dénaturer, souverainement apprécié les pièces du dossier qui lui était soumis ; qu'en jugeant que M. E... devait, par suite, être regardé comme ayant accompli, pendant cette période, des services publics effectifs auprès du CNRS, au sens des dispositions citées ci-dessus de l'article 8 de la loi du 12 mars 2012, alors même qu'il n'était pas rémunéré par le CNRS mais touchait une " libéralité " versée annuellement par la Ligue nationale contre le cancer au titre des recherches auxquelles il participait, la cour administrative d'appel a exactement qualifié les faits de l'espèce ; qu'enfin, elle n'a pas commis d'erreur de droit en en déduisant que M. E...remplissait les conditions fixées par cet article pour bénéficier du droit institué par son premier alinéa ;<br/>
<br/>
              4. Considérant qu'il résulte de tout ce qui précède que le CNRS n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à sa charge une somme de 3 000 euros à verser à M. E... au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Le pourvoi du Centre national de la recherche scientifique est rejeté.<br/>
Article 2 : Le Centre national de la recherche scientifique versera à M. C...E...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée au Centre national de la recherche scientifique et à M. C... E.... <br/>
Copie en sera adressée à la ministre de l'enseignement supérieur de la recherche et de l'innovation.<br/>
              Délibéré à l'issue de la séance du 14 mars 2018 où siégeaient : M. Edmond Honorat, président adjoint de la section du contentieux, présidant ; M. C... K..., Mme J... I..., présidents de chambre ; M. B...L..., Mme F...M... ; Mme G...O... ; Mme A...N... ; conseillers d'Etat; Mme H...D..., maître des requêtes et M. Jean-François de Montgolfier, maître des requêtes-rapporteur. <br/>
<br/>
              Lu en séance publique le 28 mars 2018.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
