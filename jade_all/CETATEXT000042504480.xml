<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042504480</ID>
<ANCIEN_ID>JG_L_2020_11_000000433027</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/50/44/CETATEXT000042504480.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 05/11/2020, 433027, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-11-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>433027</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>Mme Louise Cadin</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:433027.20201105</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C... D... a demandé au tribunal administratif de Marseille d'annuler pour excès de pouvoir, d'une part, la décision implicite de rejet, née du silence gardé par le ministre de l'intérieur sur sa demande du 12 juin 2017 dirigée contre la décision du 3 mars 2016 par laquelle le ministre a refusé de lui accorder l'avantage spécifique d'ancienneté au titre de son affectation à la compagnie républicaine de sécurité de Marseille pour la période du 1er septembre 1995 au 31 décembre 2004 et, d'autre part, d'annuler la décision du 29 décembre 2017 par laquelle le ministre de l'intérieur a refusé de lui accorder l'avantage spécifique d'ancienneté au titre de son affectation à la compagnie républicaine de sécurité autoroutière de Provence du 1er janvier 2005 au 1er mai 2012. Par des jugements n° 1707160 du 5 juillet 2018 et n° 1800346 du 12 novembre 2018, le tribunal administratif a rejeté ses demandes.<br/>
<br/>
              Par un arrêt n°s 18MA04130, 19MA00143, la cour administrative d'appel de Marseille a rejeté l'appel formé par M. D... contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire enregistrés les 29 juillet 2019, 29 octobre 2019 et 5 octobre 2020 au secrétariat du contentieux du Conseil d'Etat, M. D... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 91-715 du 26 juillet 1991 ;<br/>
              - la loi n° 95-115 du 4 février 1995 ; <br/>
              - le décret n° 95-313 du 21 mars 1995 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme B... A..., auditrice,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouvenin, Coudray, Grévy, avocat de M. D....<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1.  Il résulte des pièces du dossier soumis aux juges du fond que M. D..., fonctionnaire de police, a sollicité le bénéfice de l'avantage spécifique d'ancienneté (ASA) au titre de ses affectations successives à la compagnie républicaine de sécurité (CRS) de Marseille du 1er septembre 1995 au 31 décembre 2004 puis à la CRS autoroutière de Provence du 1er janvier 2005 au 1er mai 2012. M. D... a demandé au tribunal administratif de Marseille d'annuler les décisions des 3 mars 2016, 12 août, 29 septembre et 29 décembre 2017 lui refusant le bénéfice de cet avantage pour ces services. Le tribunal administratif a rejeté sa demande. M. D... se pourvoit en cassation contre l'arrêt par lequel la cour administrative d'appel de Marseille a rejeté son appel. <br/>
<br/>
              2. En premier lieu, il ressort des pièces du dossier soumis aux juges du fond que la cour administrative d'appel, qui n'avait pas à répondre à l'ensemble des arguments avancés par le requérant, n'était pas, compte tenu la portée des écritures qui lui étaient soumises et sur la portée desquelles elle ne s'est pas méprise, saisie d'un moyen tiré de l'invocation du principe d'égalité. En s'abstenant de viser et de répondre à un tel moyen, elle n'a dès lors pas davantage entaché son arrêt d'irrégularité.<br/>
<br/>
              3. En second lieu, aux termes de l'article 11 de la loi du 26 juillet 1991 portant diverses dispositions relatives à la fonction publique, modifié par l'article 17 de la loi du 25 juillet 1994 : " Les fonctionnaires de l'Etat et les militaires de la gendarmerie affectés pendant une durée fixée par décret en Conseil d'Etat dans un quartier urbain où se posent des problèmes sociaux et de sécurité particulièrement difficiles, ont droit, pour le calcul de l'ancienneté requise au titre de l'avancement d'échelon, à un avantage spécifique d'ancienneté dans des conditions fixées par ce même décret ". Aux termes de l'article 1er du décret du 21 mars 1995 relatif au droit de mutation prioritaire et au droit à l'avantage spécifique d'ancienneté accordés à certains agents de l'Etat affectés dans les quartiers urbains particulièrement difficiles : " Les quartiers urbains où se posent des problèmes sociaux et de sécurité particulièrement difficiles, mentionnés au quatrième alinéa de l'article 60 de la loi du 11 janvier 1984 susvisée et à l'article 11 de la loi du 26 juillet 1991 susvisée, doivent correspondre :/ 1° En ce qui concerne les fonctionnaires de police, à des circonscriptions de police ou à des subdivisions de ces circonscriptions désignées par arrêté conjoint du ministre chargé de la sécurité, du ministre chargé de la ville, du ministre chargé de la fonction publique et du ministre chargé du budget (...) ". Il résulte de ces dispositions que le bénéfice de l'avantage spécifique d'ancienneté n'est ouvert qu'aux fonctionnaires de police affectés administrativement à une circonscription de police ou une subdivision d'une telle circonscription correspondant à un quartier urbain où se posent des problèmes sociaux et de sécurité particulièrement difficiles. Ces dispositions font par suite obstacle à l'attribution d'un avantage spécifique d'ancienneté à un agent affecté administrativement à une unité administrative autre qu'une circonscription de sécurité publique ou une circonscription de sécurité de proximité, laquelle est une division territoriale de base de la direction centrale de la sécurité publique.  <br/>
<br/>
              4. Il est constant que l'affectation à la CRS de Marseille et à la CRS de Provence au titre de laquelle M. D... a sollicité le bénéfice de l'avantage supplémentaire d'ancienneté ne constitue pas, quel que soit le lieu d'exercice de ses fonctions, une affectation à une circonscription de police ni à une subdivision d'une telle circonscription. Par suite, en jugeant que les dispositions de la loi du 26 juillet 1991 et du décret du 21 mars 1995 font obstacle à ce que lui soit attribué l'avantage spécifique d'ancienneté au titre de ces fonctions la cour administrative d'appel, qui a fait une exacte appréciation des circonstances de l'espèce, n'a pas commis d'erreur de droit.<br/>
<br/>
              5. Il résulte de ce qui précède que M. D... n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque. Son pourvoi doit être rejeté ainsi que, par conséquent, les conclusions présentées sur le fondement de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
		Article 1er : Le pourvoi de M. D... est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. C... D... et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
