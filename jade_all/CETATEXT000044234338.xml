<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044234338</ID>
<ANCIEN_ID>JG_L_2021_10_000000451155</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/23/43/CETATEXT000044234338.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 20/10/2021, 451155, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-10-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>451155</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Mélanie Villiers</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:451155.20211020</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La Commission nationale des comptes de campagne et des financements politiques (CNCCFP) a saisi le tribunal administratif de Melun, en application de l'article L. 52-15 du code électoral, sur le fondement de sa décision du 2 décembre 2020 par laquelle elle a rejeté le compte de campagne de M. E... F....<br/>
<br/>
              Par un jugement n°s 2004940, 2004944, 2004946, 2004948, 2010343 du 5 mars 2021, le tribunal administratif de Melun a déclaré M. F... inéligible pour une période d'un an et démissionnaire d'office de son mandat de conseiller municipal de Bussy-Saint-Georges, a proclamé élu conseiller municipal M. D... A... ou, à défaut, le candidat venant sur la liste conduite par M. C..., immédiatement après le dernier élu de cette liste et a rejeté les protestations électorales tendant à l'annulation du second tour des opérations électorales qui se sont déroulées le 28 juin 2020 pour l'élection des conseillers municipaux dans la commune de Bussy-Saint-Georges (Seine-et-Marne).<br/>
<br/>
              Par une requête, enregistrée le 28 mars 2021 au secrétariat du contentieux du Conseil d'Etat, M. F... demande au Conseil d'Etat d'annuler ce jugement en tant qu'il l'a déclaré inéligible et démissionnaire d'office de son mandat de conseiller municipal de Bussy-Saint-Georges. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code électoral ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Mélanie Villiers, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteure publique ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Par une décision du 2 décembre 2020, la Commission nationale des comptes de campagnes et des financements politiques a rejeté le compte de campagne déposé par M. F..., candidat de la liste " F... pour Bussy ", après le premier tour des élections municipales et communautaires qui se sont déroulées le 15 mars 2020 dans la commune de Bussy-Saint-Georges (Seine-et-Marne), cette liste ayant fusionné au second tour avec une autre liste, et a saisi le tribunal administratif de Melun. Par le jugement attaqué du 5 mars 2021, ce tribunal, a déclaré M. F... inéligible pour une durée d'un an et l'a déclaré démissionnaire d'office de son mandat de conseiller municipal. M. F... fait appel de ce jugement.<br/>
<br/>
              Sur la régularité des procédures devant la Commission et le tribunal administratif :<br/>
<br/>
              2. Il résulte de l'instruction que le courrier d'observations et la lettre de relance adressés par la Commission des comptes de campagnes et des financements politiques dans le cadre de la procédure contradictoire ont été notifiés à M. F..., par lettre recommandée, à l'adresse qu'il avait lui-même indiquée sur son compte de campagne. Il ressort du dossier soumis au tribunal administratif de Melun qu'il en est de même des différentes notifications auxquelles a procédé le tribunal administratif dans le cadre de la procédure engagée devant lui. Par suite, faute d'avoir retiré ces lettres recommandées dans le délai imparti ou de justifier d'avoir informé la Commission ou la juridiction d'un éventuel changement d'adresse, M. F... n'est pas fondé à soutenir qu'il n'aurait pas été en mesure de se défendre dans le cadre de ces procédures.<br/>
<br/>
<br/>
              Sur le rejet du compte de campagne :<br/>
<br/>
              3. Aux termes du troisième alinéa l'article L. 52-4 du code électoral, le mandataire " règle les dépenses engagées en vue de l'élection et antérieures à la date du tour de scrutin où elle a été acquise, à l'exception des dépenses prises en charge par un parti ou groupement politique. Les dépenses antérieures à sa désignation payées directement par le candidat ou à son profit (...) font l'objet d'un remboursement par le mandataire et figurent dans son compte de dépôt ". Si, par dérogation à la formalité substantielle que constitue l'obligation de recourir à un mandataire pour toute dépense effectuée en vue de sa campagne, le règlement direct de menues dépenses par un candidat peut être admis, ce n'est qu'à la double condition que leur montant soit faible par rapport au total des dépenses du compte de campagne et négligeable au regard du plafond de dépenses autorisées fixé par l'article L. 52-11 du même code. <br/>
<br/>
              4. Il résulte de l'instruction et il n'est pas contesté que M. F... a déclaré avoir payé directement, sans passer par l'intermédiaire de son mandataire financier, un montant de dépenses de 20 843 euros, représentant 66,2 % du montant total des dépenses déclarées et 53,7 % du plafond des dépenses autorisées pour la commune. Il a ainsi, en tout état de cause, méconnu les dispositions de l'article L. 52-4 du code électoral. Par suite, il n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Melun a jugé que son compte de campagne avait été rejeté à bon droit.<br/>
<br/>
              Sur l'inéligibilité de M. F... :<br/>
<br/>
              5. Aux termes de l'article L. 118-3 du code électoral, dans sa rédaction issue de la loi du 2 décembre 2019 visant à clarifier diverses dispositions du droit électoral : " Lorsqu'il relève une volonté de fraude ou un manquement d'une particulière gravité aux règles de financement des campagnes électorales, le juge de l'élection, saisi par la Commission nationale des comptes de campagne et des financements politiques, peut déclarer inéligible :/ (...) 3° Le candidat dont le compte de campagne a été rejeté à bon droit. (...) ". En application de ces dispositions, en dehors des cas de fraude, le juge de l'élection ne peut prononcer l'inéligibilité d'un candidat sur le fondement de ces dispositions que s'il constate un manquement d'une particulière gravité aux règles de financement des campagnes électorales. Il lui incombe à cet effet de prendre en compte l'ensemble des circonstances de l'espèce et d'apprécier s'il s'agit d'un manquement caractérisé à une règle substantielle relative au financement des campagnes électorales et s'il présente un caractère délibéré.<br/>
<br/>
              6. Ainsi qu'il est dit au point 4, les dépenses électorales déclarées réglées sans recourir au mandataire financier, contrairement aux dispositions de l'article L. 52-4 du code électoral dont les prescriptions, dépourvues d'ambiguïté, présentent un caractère substantiel, se sont élevées à 20 843 euros, montant très significatif tant par rapport au total des dépenses du compte de campagne qu'au regard du plafond de dépenses autorisées. Par suite, le manquement commis par M. F... doit être qualifié de manquement d'une particulière gravité de nature à justifier que, sur le fondement de l'article L. 118-3 du code électoral, M. F... soit déclaré inéligible. Il suit de là que M. F... n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Melun l'a déclaré inéligible pour une durée d'un an.<br/>
<br/>
              7. Il résulte de tout ce qui précède que M. F... n'est pas fondé à demander l'annulation du jugement attaqué en tant qu'il l'a déclaré inéligible pour une durée d'un an et démissionnaire d'office de son mandat de conseiller municipal de Bussy-Saint-Georges.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. F... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. E... F....<br/>
Copie en sera adressée à la Commission nationale des comptes de campagne et des financements politiques, à M. D... A..., au ministre de l'intérieur et à la commune de Bussy-Saint-Georges. <br/>
              Délibéré à l'issue de la séance du 7 octobre 2021 où siégeaient : M. Gilles Pellissier, assesseur, présidant ; M. Mathieu Herondart, conseiller d'Etat et Mme Mélanie Villiers, maître des requêtes-rapporteure. <br/>
<br/>
              Rendu le 20 octobre 2021. <br/>
<br/>
                                   Le président : <br/>
                                   Signé : M. Gilles Pellissier<br/>
<br/>
La rapporteure : <br/>
Signé : Mme Mélanie Villiers<br/>
<br/>
                                   La secrétaire :<br/>
                                   Signé : Mme B... G... <br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
