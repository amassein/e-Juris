<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029359068</ID>
<ANCIEN_ID>JG_L_2013_11_000000350714</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/35/90/CETATEXT000029359068.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème SSJS, 28/11/2013, 350714, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-11-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>350714</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème SSJS</FORMATION>
<TYPE_REC>MAGIERA</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Christophe Eoche-Duval</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Keller</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:350714.20131128</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 8 juillet 2011 au secrétariat du contentieux du Conseil d'Etat, présentée pour Mme A...B..., demeurant au... ; Mme B... demande au Conseil d'Etat : <br/>
<br/>
              1°) de condamner l'Etat à lui verser une indemnité d'un montant de 10 000 euros, augmentée des intérêts calculés au taux légal à compter du 7 septembre 2009, date de réception de sa réclamation préalable, en réparation des préjudices qu'elle estime avoir subis du fait de la durée excessive de procédures engagées devant les juridictions administratives et judiciaires ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Louis Dutheillet de Lamothe, Auditeur,<br/>
<br/>
              - les observations de la SCP Lyon-Caen, Thiriez, avocat de Mme B...,<br/>
<br/>
              - les conclusions de M. Rémi Keller, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Lyon-Caen, Thiriez, avocat de Mme B... ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il résulte de l'instruction que Mme B..., employée à la société d'économie mixte Sommar, a fait l'objet d'un licenciement pour motif économique le 31 décembre 1993 à la suite de la résiliation de la concession accordée à cette société par la ville de Marseille ; que l'intéressée a engagé respectivement les 28 décembre 1998 et 29 septembre 1999, d'une part, une action devant le conseil des prud'hommes de Marseille à l'encontre de son employeur et des sociétés qui lui avaient succédé, d'autre part, une action devant le tribunal administratif de Marseille à l'encontre de la ville de Marseille afin d'obtenir réparation des préjudices liés à son licenciement, qu'elle estimait causés par le concours des fautes commises par les sociétés et par la ville ; <br/>
<br/>
              2. Considérant que, s'agissant de la procédure engagée à l'encontre de la ville de Marseille devant les juridictions de l'ordre administratif, le tribunal administratif a rejeté cette demande par un jugement du 8 octobre 2002 ; que la cour administrative d'appel a fait droit à l'appel interjeté par Mme B... le 16 décembre 2002 par un arrêt du 2 juin 2006 ; que le pourvoi introduit par la ville de Marseille à l'encontre de cet arrêt n'a pas été admis par une décision du Conseil d'Etat statuant au contentieux du 29 octobre 2008 ; <br/>
<br/>
              3. Considérant que, s'agissant de la procédure introduite devant les juridictions de l'ordre judiciaire, le conseil des prud'hommes de Marseille a fait droit à la demande de Mme B... par un jugement du 27 novembre 2001 ; que ce jugement a été annulé par un arrêt de la cour d'appel d'Aix-en-Provence du 4 mars 2004, faisant droit aux conclusions des sociétés ; qu'après que la Cour de cassation a, sur pourvoi de la requérante, cassé cet arrêt par une décision du 10 octobre 2006, la cour d'appel de Lyon a décidé le 12 septembre 2007 de surseoir à statuer jusqu'à ce que la juridiction administrative eût définitivement statué sur la responsabilité de la ville de Marseille ; qu'après la décision de non-admission du Conseil d'Etat statuant au contentieux en date du 29 octobre 2008, cette cour d'appel a condamné, par un arrêt du 22 avril 2009, les sociétés mises en cause à réparer les préjudices matériel et moral subis par la requérante, déduction faites des sommes déjà mises à la charge de la commune de Marseille par la décision de la cour administrative de Marseille du 2 juin 2006 pour indemniser les mêmes préjudices ; <br/>
<br/>
              4. Considérant que Mme B... demande au Conseil d'Etat de condamner l'Etat à réparer les préjudices qu'elle estime avoir subis du fait de la durée excessive des procédures introduites devant les deux ordres de juridiction et qu'elle évalue à 10 000 euros ;<br/>
<br/>
              5. Considérant qu'aux termes de l'article 35 du décret du 26 octobre 1849 : " Lorsque le Conseil d'Etat statuant au contentieux, la Cour de cassation ou tout autre juridiction statuant souverainement et échappant ainsi au contrôle tant du Conseil d'Etat que de la Cour de cassation, est saisi d'un litige qui présente à juger, soit sur l'action introduite, soit sur une exception, une question de compétence soulevant une difficulté sérieuse, et mettant en jeu la séparation des autorités administratives et judiciaires, la juridiction saisie peut, par décision ou arrêt motivé qui n'est susceptible d'aucun recours, renvoyer au Tribunal des conflits le soin de décider sur cette question de compétence " ;<br/>
<br/>
              6. Considérant que, même si les procédures engagées par Mme B... étaient dirigées contre des personnes différentes et relevaient des deux ordres de juridiction, elles tendaient à la réparation d'un même préjudice résultant du concours de fautes commises par une personne publique et des sociétés de droit privé, et ont conduit la juridiction judiciaire à surseoir à statuer jusqu'à ce que la juridiction administrative se soit prononcée ; que la question de savoir s'il est possible, dans un cas où un même litige a dû être porté devant les deux ordres de juridiction, non à cause d'une difficulté portant sur la détermination de l'ordre de juridiction compétent mais parce que le jugement du litige lui-même l'implique, de déterminer un seul ordre de juridiction compétent pour connaître de la responsabilité de l'Etat du fait de la durée excessive de ces procédures, présente une difficulté sérieuse et de nature à justifier le recours à la procédure prévue par l'article 35 du décret du 26 octobre 1849 ; que, par suite, il y a lieu de renvoyer au Tribunal des conflits la question de savoir si l'ensemble des conclusions introduites par Mme B... tendant à l'indemnisation des préjudices qu'elle estime avoir subis du fait de la durée excessive des procédures juridictionnelles qu'elle a engagées devant les deux ordres de juridiction pour la réparation d'un même dommage relève ou non de compétence de la juridiction administrative et de surseoir à toute procédure jusqu'à la décision de ce tribunal ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'affaire est renvoyée au Tribunal des conflits. <br/>
Article 2 : Il est sursis à statuer sur la requête de Mme B... jusqu'à ce que le Tribunal des conflits ait tranché la question de savoir si l'ensemble de ses conclusions tendant à l'indemnisation des préjudices qu'elle estime avoir subis du fait de la durée excessive des procédures juridictionnelles qu'elle a engagées devant les deux ordres de juridiction pour la réparation d'un même dommage relève ou non de la compétence de la juridiction administrative.<br/>
Article 3 : La présente décision sera notifiée à Mme A... B... et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
