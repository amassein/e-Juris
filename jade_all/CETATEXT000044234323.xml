<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044234323</ID>
<ANCIEN_ID>JG_L_2021_10_000000445731</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/23/43/CETATEXT000044234323.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 20/10/2021, 445731, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-10-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>445731</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BENABENT ; CABINET MUNIER-APAIRE</AVOCATS>
<RAPPORTEUR>M. Bertrand Mathieu</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Philippe Ranquet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:445731.20211020</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme F... G..., M. D... E..., Mme H... A... et M. C... A... ont demandé au juge des référés du tribunal administratif de Melun, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de l'arrêté du 6 juin 2019 par lequel le maire de Nogent-sur-Marne a délivré un permis de construire à M. I... B....<br/>
<br/>
              Par une ordonnance n° 2008132 du 14 octobre 2020, le juge des référés du tribunal administratif de Melun a rejeté leur demande.<br/>
<br/>
              Par un pourvoi, enregistré le 28 octobre 2020, M. A... et autres demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) statuant en référé, de suspendre l'exécution de l'arrêté du maire de Nogent-sur-Marne en date du 6 juin 2019 ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
              Vu :<br/>
              - la loi n° 2018-1021 du 23 novembre 2018 ; <br/>
              - l'ordonnance n° 2020-306 du 25 mars 2020 ;<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bertrand Mathieu, conseiller d'Etat en service extraordinaire,<br/>
<br/>
              - les conclusions de M. Philippe Ranquet, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Benabent, avocat de Mme G... et autres, et au cabinet Munier-Apaire, avocat de M. B... ; <br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1.	Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". Aux termes de l'article L. 600-3 du code de l'urbanisme dans sa rédaction issue de la loi du 23 novembre 2018 portant évolution du logement, de l'aménagement et du numérique : " Un recours dirigé contre une décision de non-opposition à déclaration préalable ou contre un permis de construire, d'aménager ou de démolir ne peut être assorti d'une requête en référé suspension que jusqu'à l'expiration du délai fixé pour la cristallisation des moyens soulevés devant le juge saisi en premier ressort. / La condition d'urgence prévue à l'article L. 521-1 du code de justice administrative est présumée satisfaite ".<br/>
<br/>
              2.	L'urgence justifie que soit prononcée la suspension d'un acte administratif lorsque l'exécution de celui-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre. La construction d'un bâtiment autorisée par un permis de construire présente un caractère difficilement réversible et, par suite, lorsque la suspension d'un permis de construire est demandée sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, la condition d'urgence est en principe satisfaite ainsi que le prévoit l'article L. 600-3 du code de l'urbanisme. Il ne peut en aller autrement que dans le cas où le pétitionnaire ou l'autorité qui a délivré le permis justifie de circonstances particulières. Il appartient alors au juge des référés de procéder à une appréciation globale de l'ensemble des circonstances de l'espèce qui lui est soumise.<br/>
<br/>
              3.	Il ressort des pièces du dossier soumis au juge des référés que, par un arrêté du 6 juin 2019, le maire de Nogent-sur-Marne a délivré un permis de construire à M. I... B.... Par une ordonnance du 14 octobre 2020, le juge des référés du tribunal administratif de Melun a rejeté la demande présentée par Mme G..., M. E..., Mme A... et M. A... tendant à la suspension de l'exécution de ce permis de construire sur le fondement de l'article L. 521 1 du code de justice administrative. M. A... et autres se pourvoient en cassation contre cette ordonnance. <br/>
<br/>
              4.	Pour rejeter la demande de suspension de l'exécution de l'arrêté du 6 juin 2019, le juge des référés, devant lequel les requérants faisaient valoir que la préparation du chantier avait commencé et que le début des travaux était imminent, s'est fondé sur leur absence de diligence pour le saisir compte tenu du délai de plusieurs mois s'étant écoulé depuis l'enregistrement de leur demande en annulation de ce permis de construire. En estimant que ce seul élément était de nature à renverser la présomption d'urgence prévue par l'article L. 600-3 du code de l'urbanisme, le juge des référés a commis une erreur de droit.<br/>
<br/>
              5.	Il résulte de ce qui précède que les requérants sont fondés à demander l'annulation de l'ordonnance qu'ils attaquent.  <br/>
<br/>
              6.	Il y a lieu, dans les circonstances de l'espèce, de statuer sur la demande de suspension en application des dispositions de l'article L. 821-2 du code de justice administrative. <br/>
<br/>
              7.	Il résulte des dispositions du premier alinéa de l'article L. 600-3 du code de l'urbanisme, citées au point 1, que l'expiration du délai fixé pour la cristallisation des moyens a pour effet de rendre irrecevable l'introduction d'une requête en référé suspension dirigée contre un permis de construire. Aux termes de l'article R. 600-5 du code de l'urbanisme : " Par dérogation à l'article R. 611-7-1 du code de justice administrative, et sans préjudice de l'application de l'article R. 613-1 du même code, lorsque la juridiction est saisie d'une requête relative à une décision d'occupation ou d'utilisation du sol régie par le présent code, ou d'une demande tendant à l'annulation ou à la réformation d'une décision juridictionnelle concernant une telle décision, les parties ne peuvent plus invoquer de moyens nouveaux passé un délai de deux mois à compter de la communication aux parties du premier mémoire en défense (...) ". Dans l'hypothèse d'une instance avec plus d'un défendeur, la communication aux parties du premier mémoire de l'un des codéfendeurs suffit à déclencher le délai de deux mois pour la cristallisation des moyens.<br/>
<br/>
              8. Il résulte de l'instruction que la demande de M. A... et autres tendant à l'annulation pour excès de pouvoir du permis de construire délivré à M. B... a été enregistrée le 19 novembre 2019 et que le délai commandant la cristallisation des moyens a commencé à courir le 9 mars 2020 à la suite de la communication aux parties du premier mémoire d'un défendeur à l'instance, en l'occurrence celui du pétitionnaire. Par suite, à la date à laquelle M. A... et autres ont présenté leurs conclusions aux fins de suspension, soit le 12 octobre 2020, le délai fixé pour la cristallisation des moyens était expiré, y compris en tenant compte de sa prorogation par l'ordonnance du 25 mars 2020 relative à la prorogation des délais échus pendant la période d'urgence sanitaire et à l'adaptation des procédures pendant cette même période. Dans ces conditions, leur demande en référé suspension est irrecevable. Elle doit être rejetée, de même que, par voie de conséquence, leurs conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              9. Il y a lieu, dans les circonstances de l'espèce, de mettre solidairement à la charge de Mme G..., M. E..., Mme et M. A... une somme de 750 euros à verser à M. B... sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance n° 2008132 du juge des référés du tribunal administratif de Melun du 14 octobre 2020 est annulée.<br/>
Article 2 :  Les conclusions présentées par M. A... et autres devant le juge des référés du tribunal administratif de Melun et le surplus de leurs conclusions devant le Conseil d'Etat est rejeté.<br/>
Article 3 : Mme G..., M. E..., Mme et M. A... verseront à M. B... une somme de 750 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. C... A..., représentant unique désigné pour l'ensemble des requérants, à M. I... B... et à la commune de Nogent-sur-Marne.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
