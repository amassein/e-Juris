<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043466319</ID>
<ANCIEN_ID>JG_L_2021_04_000000451537</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/46/63/CETATEXT000043466319.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 16/04/2021, 451537, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-04-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>451537</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:451537.20210416</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 8 avril 2021 au secrétariat du contentieux du Conseil d'Etat, l'association Ni Voyous Ni Soumis demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution du II de l'article 4 du décret n° 2020-1310 du 29 octobre 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire en tant qu'il limite à une distance de 10 kilomètres à partir du domicile les déplacements autorisés par le 4° de ce II ;<br/>
<br/>
              2°) à titre subsidiaire de préciser que sont autorisés au-delà de la distance de 10 kilomètres du domicile tous les " déplacements ordinaires et habituels des citoyens jusqu'aux installations pour lesquelles ils peuvent justifier de la qualité de membre/adhérent/client, et ceci sans autre justificatif que leur carte de membre/adhérent de la structure concernée ".<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - la condition d'urgence est satisfaite dès lors que la limitation des déplacements à une distance de dix kilomètres du domicile fait obstacle, pour un grand nombre de personnes, à l'exercice de leur droit de pratiquer des activités de loisir ou de la vie courante qui sont pourtant autorisées par les restrictions sanitaires en vigueur, et qu'elle ne répond à aucun motif sérieux, alors que le simple déplacement en véhicule particulier sur une distance plus importante n'introduit aucun risque supplémentaire de propagation du virus de la Covid-19 ;<br/>
              - il est porté une atteinte grave et manifestement illégale à la liberté personnelle et au droit à la santé, eu égard à la limitation de l'accès aux infrastructures nécessaires au sport, ainsi qu'au principe de non-discrimination, les personnes étant traitées différemment en fonction de la densité d'implantation des structures nécessaires à la vie courante.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, et notamment son Préambule ; <br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du même code : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. La circonstance qu'une atteinte à une liberté fondamentale portée par une mesure administrative serait avérée n'est pas de nature à caractériser l'existence d'une situation d'urgence justifiant l'intervention du juge des référés dans le très bref délai prévu par les dispositions de l'article L. 521-2 du code de justice administrative. Il appartient au juge des référés d'apprécier, au vu des éléments que lui soumet le requérant comme de l'ensemble des circonstances de l'espèce, si la condition d'urgence particulièrement requise par l'article L. 521-2 est satisfaite, en prenant en compte tant la situation du requérant et les intérêts qu'il entend défendre que l'intérêt public qui s'attache à l'exécution des mesures prises par l'administration.<br/>
<br/>
              3. Pour justifier de l'urgence qui s'attache à sa saisine du juge des référés du Conseil d'Etat, l'association Ni Voyous Ni Soumis se borne à soutenir que les dispositions du II de l'article 4 du décret du 29 octobre 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire imposent une limitation des déplacements des personnes à une distance de 10 kilomètres autour de leur domicile, et fait obstacle à l'exercice par les citoyens, en particulier ceux qui vivent dans les zones les moins densément équipées, de nombreuses activités, y compris au sein de structures dont ils sont membres clients ou adhérents, et qu'elle ne répond à aucun motif sérieux, dès lors que les déplacements en véhicules particuliers ne créent pas de risque de diffusion du virus de la covid-19.<br/>
<br/>
              4. En se bornant, sans faire état d'aucune circonstance précise en rapport avec les intérêts qu'elle s'est donné pour objet de défendre, à faire état des difficultés générales que les dispositions qu'elle conteste sont susceptibles de causer, l'association requérante n'apporte, alors au demeurant que la gravité persistante et l'étendue nationale de la crise sanitaire liée aux évolutions de la pandémie imposent l'édiction de mesures qui contribuent à réduire la circulation du virus, aucun élément de nature à caractériser l'existence pour elle-même d'une situation d'urgence justifiant l'intervention du juge des référés dans le très bref délai prévu par les dispositions de l'article L. 521-2 du code de justice administrative cité ci-dessus.<br/>
<br/>
              5. Il résulte de tout ce qui précède que la requête de l'association Ni Voyous Ni Soumis doit être rejetée selon la procédure prévue par l'article L. 522-3 du code de justice administrative.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de l'association Ni Voyous Ni Soumis est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à l'association Ni Voyous Ni Soumis.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
