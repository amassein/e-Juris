<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029183020</ID>
<ANCIEN_ID>JG_L_2014_06_000000347154</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/18/30/CETATEXT000029183020.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème / 9ème SSR, 25/06/2014, 347154, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-06-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>347154</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème / 9ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER</AVOCATS>
<RAPPORTEUR>M. Nicolas Labrune</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:347154.20140625</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 1er mars et 1er juin 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société française du tunnel routier du Fréjus (SFTRF), dont le siège est au 20 rue de la Bourse à Lyon (69289 Cedex ) ; la société française du tunnel routier du Fréjus demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 08PA06467 du 31 décembre 2010 par lequel la cour administrative d'appel de Paris a rejeté sa requête tendant à l'annulation du jugement n° 0303527/2 du 30 octobre 2008 du tribunal administratif de Paris rejetant sa demande tendant à la condamnation de l'Etat à lui verser des intérêts moratoires d'un montant de 18 073 944, 93 euros à compter des dates des demandes de remboursement des crédits de taxe sur la valeur ajoutée dont le dépôt a été empêché par l'article 273 ter du code général des impôts ainsi que les intérêts dus sur cette créance d'intérêts ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code civil ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu la loi n° 2000-1353 du 30 décembre 2000 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Nicolas Labrune, maître des requêtes,<br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Blancpain, Soltner, avocat de la société française du tunnel routier du Fréjus ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que, par une décision du 12 septembre 2000 (aff. C-276/97, Commission c/ France), la Cour de justice des Communautés européennes a jugé contraires aux dispositions des articles 2 et 4 de la directive 77/388/CEE du 17 mai 1977 modifiée les dispositions du code général des impôts desquelles il résultait que n'étaient pas imposables à la taxe sur la valeur ajoutée les péages perçus en contrepartie de l'utilisation d'ouvrages de circulation routière, dans la mesure où ce service n'était pas fourni par un organisme de droit public agissant en qualité d'autorité publique ; qu'à la suite de cette décision, l'article 2 de la loi du 30 décembre 2000 de finances rectificative pour 2000 a, d'une part, modifié le code général des impôts pour soumettre ces péages à la taxe sur la valeur ajoutée, à l'exception de ceux perçus par des personnes morales de droit public agissant en qualité d'autorités publiques, et ouvrir, en conséquence, dans les conditions de droit commun, le droit à déduction de la taxe sur la valeur ajoutée qui a grevé les éléments du prix d'une opération imposable, d'autre part, par son VII, autorisé les exploitants concernés à déduire la taxe ayant, le cas échéant, grevé à titre définitif les travaux de construction et de grosses réparations qu'ils avaient réalisés à compter du 1er janvier 1996 au titre d'ouvrages mis en service avant le 12 septembre 2000, de la taxe sur la valeur ajoutée afférente aux péages qui n'avait pas été acquittée du 1er janvier 1996 au 11 septembre 2000 ; <br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société française du tunnel routier du Fréjus (SFTRF) a, en conséquence, formé le 4 juillet 2001 une réclamation par laquelle elle demandait la restitution d'un montant de taxe de 885 623 478 francs (135 012 429 euros) ; que l'administration a fait droit à cette réclamation par décision du 18 octobre 2001 ; que la somme en cause a été créditée sur un compte bancaire de la société le 25 octobre suivant ; que, par lettre du 10 janvier 2003 faisant suite à une demande de la société en date du 22 novembre 2001, l'administration a indiqué à la société que la somme qui lui avait été versée le 25 octobre 2001 serait assortie d'intérêts moratoires dont le point de départ serait la date de la réclamation, soit le 4 juillet 2001, et le point d'arrivée, la date de versement de la somme, soit le 25 octobre suivant ; que ces intérêts, d'un montant de 1 789 364,72 euros, ont été versés à la société le 30 janvier 2003 ; qu'estimant avoir droit à des intérêts d'un montant supérieur, qu'elle évaluait à 19 863 309,65 euros, la SFTRF a saisi le tribunal administratif de Paris, qui a rejeté sa demande par un jugement du 30 octobre 2008 ; que la société demande au Conseil d'Etat d'annuler l'arrêt du 31 décembre 2010 par lequel la cour administrative d'appel de Paris a rejeté sa requête d'appel dirigée contre ce jugement, en tant que la cour a statué sur ses conclusions relatives aux intérêts moratoires dus au titre des remboursements de crédits de taxe sur la valeur ajoutée et non en tant qu'elle a statué sur ceux relatifs au remboursement de la taxe sur la valeur ajoutée acquittée ;<br/>
<br/>
              3. Considérant, en premier lieu, que les demandes de remboursement de crédit de taxe sur la valeur ajoutée constituent, au sens des dispositions de l'article L. 190 du livre des procédures fiscales, des réclamations contentieuses, qui sont soumises à des conditions et délais particuliers fixés par les articles 242-0 A et suivants de l'annexe II au code général des impôts ; que la circonstance que les dispositions du VII de l'article 2 de la loi du 30 décembre 2000 ont rouvert, pour les exploitants concernés, un délai de réclamation est sans incidence sur la nature de ces réclamations et sur les règles qui doivent leur être appliqués en matière d'intérêts moratoires ; que la cour administrative d'appel, en énonçant que la SFTRF n'avait pas présenté au cours de la période de 1996 à 2000, de réclamations dans les formes prévues aux articles 242-0 A et suivants de l'annexe II au code général des impôts et que de telles réclamations constituaient la seule voie ouverte à la société pour obtenir un remboursement de son crédit de taxe sur la valeur ajoutée, s'est fondée sur les règles de mise en oeuvre de l'article L. 190 du livre des procédures fiscales propres au contentieux qui lui était soumis ; qu'il suit de là, d'une part, que la cour n'a pas omis de répondre au moyen, soulevé devant elle, tiré de ce que l'action de la société se situait dans le cadre de l'article L. 190 du livre des procédures fiscales, d'autre part, que la cour n'a pas commis d'erreur de droit au regard des dispositions du VII de l'article 2 de la loi du 30 décembre 2000, lesquelles n'avaient pas institué un régime spécifique de réclamation ;<br/>
<br/>
              4. Considérant, en deuxième lieu, qu'aux termes du premier alinéa de l'article L. 208 du livre des procédures  fiscales, dans sa rédaction applicable au litige : " Quand l'Etat est condamné à un dégrèvement d'impôt par un tribunal ou quand un dégrèvement est prononcé par l'administration à la suite d'une réclamation tendant à la réparation d'une erreur commise dans l'assiette ou le calcul des impositions, les sommes déjà perçues sont remboursées au contribuable et donnent lieu au paiement d'intérêts moratoires dont le taux est celui de l'intérêt légal. Les intérêts courent du jour du paiement. Ils ne sont pas capitalisés " ; qu'il résulte de ces dispositions que les remboursements de taxe sur la valeur ajoutée obtenus par une société après le rejet par l'administration d'une réclamation ont le caractère de dégrèvement contentieux de la même nature que celui prononcé par un  tribunal au sens des dispositions précitées de l'article L. 208 du livre des procédures fiscales ; que la circonstance que le droit à  remboursement ne procéderait pas, à l'origine, d'une erreur commise par l'administration dans l'assiette ou le calcul d'une imposition est sans incidence à cet égard ; que ces remboursements doivent, dès lors, donner lieu au paiement d'intérêts moratoires qui courent, s'agissant de la procédure de remboursement de crédits de taxe sur la valeur ajoutée, pour laquelle il n'y a pas de paiement antérieur de la part du redevable, à compter de la date de la réclamation qui fait apparaître le crédit remboursable ; <br/>
<br/>
              5. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société requérante n'a, au cours des années 1996 à 2000, formé aucune réclamation sur le fondement de l'article L. 190 du livre des procédures fiscales pour obtenir le remboursement des crédits de taxe sur la valeur ajoutée en cause ; que sa première réclamation n'a été déposée que le 4 juillet 2001 ; qu'il y a d'ailleurs été fait droit dans le délai de six mois prévu à l'article R. 198-10 du livre des procédures fiscales et qu'ainsi, le remboursement intervenu le 25 octobre 2001 n'avait pas le caractère d'un dégrèvement contentieux au sens de l'article L. 208 du même livre ; qu'il était loisible à la société requérante de contester la conformité aux principes et aux dispositions du droit de l'Union européenne des dispositions de droit national relatives au régime des entreprises fournissant le service de l'utilisation d'ouvrages de circulation routière au regard de la taxe sur la valeur ajoutée durant toute la période antérieure au 12 septembre 2000, pendant laquelle il lui a été fait application de ce régime ; que, par suite, la cour n'a commis ni erreur de droit, au regard des articles 1153 du code civil et L. 208 du livre des procédures fiscales, ni erreur de qualification juridique des faits en écartant le moyen tiré de ce que toute réclamation tendant au remboursement des crédits de taxe sur la valeur ajoutée en cause qui serait parvenue à l'administration fiscale avant aurait été irrecevable et en se fondant, pour rejeter la demande de la société requérante, sur la circonstance que celle-ci n'avait déposé aucune demande de remboursement des crédits de taxe sur la valeur ajoutée avant le 4 juillet 2001, alors même que la non-conformité des dispositions du code général des impôts mentionnées au point 1 aux dispositions de la directive du 17 mai 1977 n'avait pas encore été révélée par l'arrêt de la Cour de justice des Communautés européennes du 12 septembre 2000 ;<br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que le pourvoi de la SFTRF doit être rejeté ;<br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi de la société française du tunnel routier du Fréjus est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la société française du tunnel routier du Fréjus et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
