<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038098294</ID>
<ANCIEN_ID>JG_L_2019_01_000000426947</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/09/82/CETATEXT000038098294.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 31/01/2019, 426947, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-01-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>426947</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE NERVO, POUPET</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2019:426947.20190131</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. B...A...demandé au juge des référés du tribunal administratif d'Orléans, à titre principal, d'enjoindre au président du conseil départemental d'Indre-et-Loire de le faire bénéficier d'un accueil provisoire d'urgence et d'en aviser immédiatement le procureur de la République sous astreinte de 100 euros par jour de retard, à titre subsidiaire, d'enjoindre à la préfète d'Indre-et-Loire de lui assurer un hébergement d'urgence jusqu'à ce qu'il soit orienté vers une structure d'hébergement stable dans un délai de quarante-huit heures à compter de la notification de l'ordonnance à intervenir et sous astreinte de 100 euros par jour de retard. Par une ordonnance n° 1804580 du 27 décembre 2018, le juge des référés du tribunal administratif d'Orléans a rejeté leur demande.<br/>
<br/>
              Par une requête, enregistrée le 9 janvier 2019 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) de l'admettre au bénéfice de l'aide juridictionnelle provisoire ; <br/>
<br/>
              2°) d'annuler l'ordonnance contestée ;<br/>
<br/>
              3°) à titre principal, d'enjoindre au président du conseil départemental d'Indre-et-Loire d'organiser à son bénéfice un accueil provisoire d'urgence par le service d'aide sociale à l'enfance et d'en aviser immédiatement le procureur de la République, dans un délai de 48 heures à compter de la notification de la présente ordonnance et sous astreinte de 100 euros par jour de retard ; <br/>
<br/>
              4°) à titre subsidiaire, d'enjoindre à la préfète d'Indre-et-Loire de lui assurer un hébergement d'urgence jusqu'à ce qu'il soit orienté vers une structure d'hébergement stable dans un délai de quarante-huit heures à compter de la notification de la présente ordonnance et sous astreinte de 100 euros par jour de retard ; <br/>
<br/>
              5°) de mettre à la charge du département la somme de 1 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi n° 91-647 du 10 juillet 1991. <br/>
<br/>
<br/>
<br/>
              Il soutient que : <br/>
              - sa requête est recevable en dépit de sa minorité dès lors qu'il sollicite un hébergement d'urgence en tant que mineur étranger isolé ; <br/>
              - la condition d'urgence est remplie dès lors qu'il se trouve dans une situation de grande précarité juridique et matérielle ; <br/>
              - il est porté une atteinte grave et manifestement illégale à une liberté fondamentale dès lors qu'il est caractérisé une carence dans la mission d'accueil et d'hébergement des étrangers mineurs telle qu'elle incombe, à titre principal, au département d'Indre-et-Loire et, à titre subsidiaire, au préfet d'Indre-et-Loire ; <br/>
              - l'ordonnance contestée est entachée d'une erreur de droit dès lors qu'en rejetant sa demande aux motifs qu'il ne justifierait pas d'un état de santé nécessitant des soins médicaux et qu'il serait dépourvu d'autorisation de séjour et n'aurait pas déposé de demande d'asile, elle a ajouté à la loi des conditions qui n'y figurent pas.<br/>
<br/>
              Par un mémoire en défense, enregistré le 21 janvier 2019, le département d'Indre-et-Loire conclut au rejet de la requête. Il soutient que :<br/>
              - il constate une augmentation exponentielle du nombre de mineurs non accompagnés demandeurs d'un hébergement d'urgence, à laquelle il ne peut entièrement faire face en dépit de ses efforts pour accroître ses capacités d'hébergement ; <br/>
              - la condition d'urgence n'est pas remplie dès lors que M. A...a été convoqué à un entretien d'évaluation le 17 octobre 2018 auquel il ne s'est pas présenté, son attitude faisant ainsi obstacle à sa mise à l'abri et son hébergement. <br/>
<br/>
              Par un mémoire en défense, enregistré le 21 janvier 2019, le ministre de l'intérieur conclut au rejet de la requête. Il soutient que :<br/>
              - la condition d'urgence n'est pas remplie, dès lors que M. A... a été convoqué pour un rendez-vous le 17 janvier 2019 et ne s'y est pas rendu ; <br/>
              - en tout état de cause, aucune carence ne saurait être reprochée aux services de l'Etat dans le département dès lors que, d'une part, le département d'Indre-et-Loire n'établit pas que ses capacités d'action seraient dépassées et que, d'autre part, il est en mesure de fournir un accueil provisoire d'urgence d'une durée de cinq jours, compensé forfaitairement par le Fonds national de financement de la protection de l'enfance, sans que cette prise en charge ne soit subordonnée ni à une décision judiciaire ni à la réalisation préalable d'une évaluation de situation de la personne se déclarant mineure.<br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. B...A...et, d'autre part, le département d'Indre-et-Loire et le ministre de l'intérieur ;<br/>
              Vu le procès-verbal de l'audience publique du 22 janvier 2019 à 15 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Poupet, avocat au Conseil et à la Cour de cassation, avocate de M. A... ;<br/>
<br/>
              - le représentant du département d'Indre-et-Loire ;<br/>
<br/>
              - la représentante du ministre de l'intérieur ; <br/>
              et à l'issue de laquelle le juge des référés a prolongé l'instruction jusqu'au 24 janvier 2019 à 18 heures puis jusqu'au 25 janvier 2019 à 15 heures.<br/>
<br/>
<br/>
              Par un mémoire, enregistré le 23 janvier 2019, M. A... précise que l'association Utopia 56, si elle n'a pas conclu de partenariat avec le département, est un acteur connu pour son intervention au soutien des mineurs isolés auprès desquels elle collecte les informations et en particulier les dates de rendez-vous pour une évaluation sociale et qu'elle ne consigne la date du rendez-vous qu'après avoir pris connaissance du récépissé délivré par les services de l'aide sociale à l'enfance.<br/>
<br/>
              Par un mémoire, enregistré le 25 janvier 2019, le département d'Indre-et-Loire soutient qu'il ressort des calendriers des évaluations qu'il produit pour les 23, 24, 28 et 29 janvier que ces jeunes n'avaient pas de rendez-vous à ces dates. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la convention internationale relative aux droits de l'enfant ;<br/>
              - le code de l'action sociale et des familles ; <br/>
              - la loi n° 91-647 du 10 juillet 1991 relative à l'aide juridique ; <br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ".<br/>
<br/>
              2. Aux termes de l'article L. 221-1 du code de l'action sociale et des familles : " Le service de l'aide sociale à l'enfance est un service non personnalisé du département chargé des  missions suivantes : / 1° Apporter un soutien matériel, éducatif et psychologique (...) aux mineurs et à leur famille et à tout détenteur de l'autorité parentale  confrontés à des difficultés risquant de mettre en danger la santé, la sécurité, la moralité de ces mineurs (...) ; / 3° Mener en urgence des actions de protection des mineurs mentionnés au 1° du présent article ; (...). ". Aux termes de l'article L. 223-2 du même code : " (...) / En cas d'urgence et lorsque le représentant légal du mineur est dans l'impossibilité de donner son accord, l'enfant est recueilli provisoirement par le service qui en avise immédiatement le procureur de la République. (...) / Si, dans le cas prévu au deuxième alinéa du présent article l'enfant n'a pu être remis à sa famille ou le représentant légal n'a pas pu ou a refusé de donner son accord dans un délai de cinq jours, le service saisit également l'autorité judiciaire en vue de l'application de l'article 375-5 du code civil. ". Pour l'application de ces dispositions, l'article R. 221-11 du même code a prévu que : " I. Le président du conseil départemental du lieu où se trouve une personne se déclarant mineure et privée temporairement ou définitivement de la protection de sa famille met en place un accueil provisoire d'urgence d'une durée de cinq jours à compter du premier jour de sa prise en charge, selon les conditions prévues aux deuxième et quatrième alinéas de l'article L. 223-2. / II. Au cours de la période d'accueil provisoire d'urgence, le président du conseil départemental procède aux investigations nécessaires en vue d'évaluer la situation de cette personne au regard notamment de ses déclarations sur son identité, son âge, sa famille d'origine, sa nationalité et son état d'isolement. (...) / IV Au terme du délai mentionné au I, ou avant l'expiration de ce délai si l'évaluation a été conduite avant son terme, le président du conseil départemental saisit le procureur de la République (...). En ce cas, l'accueil provisoire mentionné au I se prolonge tant que n'intervient pas une décision de l'autorité judiciaire. / S'il estime que la situation de la personne mentionnée au présent article ne justifie pas la saisine de l'autorité judiciaire, il notifie à cette personne une décision de refus de prise en charge (...). En ce cas, l'accueil provisoire d'urgence mentionné au I prend fin. ".<br/>
<br/>
              3. Il résulte de ces dispositions que, sous réserve des cas où la condition de minorité ne serait à l'évidence pas remplie, il incombe aux autorités du département de mettre en place un accueil d'urgence pour toute personne se déclarant mineure et privée temporairement ou définitivement de la protection de sa famille, confrontée à des difficultés risquant de mettre en danger sa santé, sa sécurité ou sa moralité en particulier parce qu'elle est sans abri. Lorsqu'elle entraîne des conséquences graves pour le mineur intéressé, une carence caractérisée dans l'accomplissement de cette mission porte une atteinte grave et manifestement illégale à une liberté fondamentale.<br/>
<br/>
              4. M. B...A..., qui indique être né le 24 juin en 2002 en Côte d'Ivoire et déclare ne pas avoir de famille en France et être sans abri, a demandé au juge des référés du tribunal administratif d'Orléans d'enjoindre au département d'Indre-et-Loire et subsidiairement à l'Etat, sur le fondement des dispositions de l'article L. 521-2 du code de justice administrative, de prendre toute mesure de nature à lui assurer un hébergement d'urgence. Le juge des référés a rejeté sa demande par une ordonnance du 27 décembre 2018. M. A...relève appel de cette ordonnance. Il résulte des éléments produits par le département devant le Conseil d'Etat que l'intéressé, qui avait été convoqué par le service départemental de l'aide sociale à l'enfance pour une évaluation de sa situation le 17 octobre 2018, ne s'est pas présenté à ce rendez-vous. M. A... n'a apporté aucune précision sur les motifs de ce défaut de présentation ou sur sa situation actuelle. Dans ces conditions, la condition d'urgence ne peut être regardée comme remplie. Il en résulte que M. A...n'est pas fondé à se plaindre de ce que, par l'ordonnance attaquée, le juge des référés du tribunal administratif d'Orléans a rejeté sa demande. Il y a lieu de rejeter la présente requête, y compris les conclusions tendant à l'admission provisoire à l'aide juridictionnelle et les conclusions présentées sur le fondement de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. A...est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. B...A..., au département d'Indre-et-Loire et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
