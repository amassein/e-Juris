<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039772900</ID>
<ANCIEN_ID>JG_L_2019_12_000000435005</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/77/29/CETATEXT000039772900.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 27/12/2019, 435005, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-12-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>435005</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Jean-Marc Vié</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:435005.20191227</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Blue Boats a demandé au tribunal administratif de Montpellier, d'une part, d'annuler les décisions du maire de Palavas-les-Flots du 24 mai 2017 et du 1er août 2017 prononçant la résiliation de la convention d'occupation du domaine public conclue le 8 juillet 2014, de prononcer la reprise des relations contractuelles et de l'indemniser des préjudices qu'elle a subis. Par un jugement nos 1703389 - 1704695 du 12 avril 2018, le tribunal a prononcé un non-lieu à statuer sur les conclusions dirigées contre la décision du 24 mai 2017 et a rejeté le surplus des conclusions de la société.<br/>
<br/>
              Par un arrêt n° 18MA02718 du 29 avril 2019, la cour administrative d'appel de Marseille, sur appel de la société Blue Boats, a annulé ce jugement et ordonné la reprise immédiate des relations contractuelles entre la commune de Palavas-les-Flots et la société Blue Boats dans le cadre de la convention conclue le 8 juillet 2014.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 1er juillet 2019 et 1er octobre 2019 au secrétariat du contentieux du Conseil d'Etat, la commune de Palavas-les-Flots a demandé au Conseil d'Etat l'annulation de cet arrêt.<br/>
<br/>
              Par la présente requête, enregistrée le 1er octobre 2019 au secrétariat du contentieux du Conseil d'Etat, la commune de Palavas-les-Flots demande au Conseil d'Etat :<br/>
<br/>
              1°) de surseoir à l'exécution de l'arrêt du 29 avril 2019 de la cour administrative d'appel de Marseille jusqu'à ce qu'il ait été statué sur son pourvoi en cassation. <br/>
<br/>
              2°) de mettre à la charge de la société Blue Boats la somme de 5 000 euros au titre de l'article L.761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Vié, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la commune de Palavas-les-Flots ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Aux termes du premier alinéa de l'article R. 821-5 du code de justice administrative : " La formation de jugement peut, à la demande de l'auteur du pourvoi, ordonner qu'il soit sursis à l'exécution d'une décision juridictionnelle rendue en dernier ressort si cette décision risque d'entraîner des conséquences difficilement réparables et si les moyens invoqués paraissent, en l'état de l'instruction, sérieux et de nature à justifier, outre l'annulation de la décision juridictionnelle, l'infirmation de la solution retenue par les juges du fond ".<br/>
<br/>
              2. La commune de Palavas-les-Flots soutient que la reprise immédiate des relations contractuelles avec la société Blue Boats dans le cadre de la convention conclue le 8 juillet 2014 autorisant cette société à occuper pour une durée de quinze ans une parcelle du domaine public situé sur le quai de l'île de Cazot d'une surface de 313,06 mètres carrés en vue de l'exercice d'une activité de location de bateaux sans permis et d'un service de restauration, ferait obstacle à la réalisation, indispensable, de places de stationnement au bénéfice du personnel de la maison de retraite implantée à proximité de la parcelle occupée et serait ainsi de nature à créer un risque immédiat sur la santé des résidents de cet établissement. Toutefois, l'exécution de l'arrêt du 29 avril 2019 par lequel la cour administrative d'appel de Marseille a prononcé la reprise de ces relations contractuelles, n'est pas, en elle-même, de nature à entraîner les conséquences invoquées par la commune. En outre, contrairement à ce que soutient la commune, la seule circonstance que la reprise des relations contractuelles constitue l'objet du litige n'est pas, par elle-même, de nature à faire regarder l'exécution de l'arrêt du 29 avril 2019 de la cour administrative d'appel de Marseille comme entraînant des conséquences difficilement réparables. La commune de Palavas-les-Flots n'est ainsi pas fondée à demander le sursis à exécution de cet arrêt.<br/>
<br/>
              3. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société Blue Boats qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la commune de Palavas-les-Flots est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la commune de Palavas-les-Flots et à la société Blue Boats.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
