<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028510755</ID>
<ANCIEN_ID>JG_L_2014_01_000000367145</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/51/07/CETATEXT000028510755.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 5ème sous-sections réunies, 20/01/2014, 367145, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-01-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>367145</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 5ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP BARTHELEMY, MATUCHANSKY, VEXLIARD</AVOCATS>
<RAPPORTEUR>M. David Moreau</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:367145.20140120</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 25 mars et 24 juin 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B...A..., demeurant ... ; M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision n° 220 du 16 janvier 2013 par laquelle le Conseil national de l'ordre des médecins, siégeant en formation restreinte, l'a suspendu du droit d'exercer la médecine pour une durée de sept ans et a subordonné la reprise de son activité professionnelle aux résultats d'une nouvelle expertise ;<br/>
<br/>
              2°) de mettre à la charge du Conseil national de l'ordre des médecins une somme de 5 000 euros sur le fondement de l'article L. 761-1 du code de justice administrative ainsi qu'une somme de 35 euros correspondant au montant de la contribution à l'aide juridique sur le fondement de l'article R. 761-1 du même code ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la santé publique ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. David Moreau, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de M. A...et à la SCP Barthélemy, Matuchansky, Vexliard, avocat du Conseil national de l'ordre des médecins ;<br/>
<br/>
<br/>
<br/>
<br/>Sur la légalité de la décision attaquée :<br/>
<br/>
              1. Considérant qu'aux termes de l'article R. 4124-3 du code de la santé publique : " Dans le cas d'infirmité ou d'état pathologique rendant dangereux l'exercice de la profession, la suspension temporaire du droit d'exercer est prononcée par le conseil régional ou interrégional pour une période déterminée, qui peut, s'il y a lieu, être renouvelée. Elle ne peut être ordonnée que sur un rapport motivé établi à la demande du conseil par trois médecins spécialistes désignés comme experts, désignés l'un par l'intéressé, le deuxième par le conseil départemental et le troisième par les deux premiers. (...) / Le conseil peut être saisi soit par le préfet, soit par délibération du conseil départemental ou du conseil national. L'expertise prévue à l'alinéa précédent est effectuée au plus tard dans le délai de deux mois à compter de la saisine du conseil.  / Les experts procèdent ensemble, sauf impossibilité manifeste, à l'expertise. Le rapport d'expertise est déposé au plus tard dans le délai de deux mois à compter de la saisine du conseil. / (...) La notification de la décision informe le praticien que la reprise de l'exercice professionnel ne pourra avoir lieu sans qu'au préalable ait été diligentée une nouvelle expertise médicale, dont il lui incombe de demander l'organisation au conseil départemental. " ; <br/>
<br/>
              2. Considérant que la décision prise en application de ces dispositions peut faire l'objet d'un recours devant le Conseil national de l'ordre des médecins, qui statue par une décision administrative ; qu'il appartient à l'instance ordinale, lorsqu'elle se prononce en application de ces dispositions, d'apprécier, au vu de l'ensemble des éléments du dossier, notamment du rapport des experts, et compte tenu des éléments qui résultent de l'audition de l'intéressé, si le praticien présente une infirmité ou un état pathologique de nature à établir un caractère dangereux dans l'exercice de sa profession avec des patients ; que, dans le cas où, au terme de cette appréciation, elle prend une mesure de suspension du droit d'exercer, il lui incombe de motiver sa décision en indiquant les éléments au regard desquels, au vu de son instruction, elle estime que l'état du praticien intéressé rend dangereux pour les patients l'exercice de sa profession ; <br/>
<br/>
              3. Considérant, en premier lieu, que la décision attaquée énonce les considérations de droit et de fait sur lesquelles elle se fonde ; qu'en particulier, contrairement à ce que soutient M.A..., le Conseil national de l'ordre des médecins ne s'est pas borné à s'en remettre aux conclusions du rapport d'expertise, mais a fait état des éléments, qu'il s'est appropriés, qui selon lui étaient de nature à établir que l'état de santé du requérant était incompatible avec l'exercice de la médecine ; que, par suite, le moyen tiré du défaut de motivation de la décision attaquée doit être écarté ;<br/>
<br/>
              4. Considérant, en deuxième lieu, qu'il résulte de ce qui a été dit précédemment que le moyen tiré de ce que le Conseil national de l'ordre des médecins se serait crû lié à tort par les conclusions de l'expertise doit également être écarté ;<br/>
<br/>
              5. Considérant, en troisième lieu, que le délai de deux mois à compter de la saisine du conseil prévu au troisième alinéa de l'article R. 4124-2 du code de la santé publique pour déposer le rapport d'expertise n'est pas prescrit à peine d'irrégularité ; que, par suite, la circonstance que l'expertise a été rendue près d'un an après l'examen du requérant par les experts est sans incidence sur sa régularité ; que, contrairement à ce que soutient le requérant, les appréciations portées par les experts sur la prise en charge, par M.A..., de son état de santé personnel ne révèlent de leur part aucune partialité ;<br/>
<br/>
              6. Considérant, en quatrième lieu, que les conclusions de l'expertise se fondent sur un examen réalisé le 3 octobre 2011, et non sur des faits anciens comme le soutient le requérant ; que celui-ci ne produit aucun élément postérieur de nature à les remettre en cause ; que, par suite, le moyen tiré de ce que le Conseil national de l'ordre des médecins aurait dû diligenter une expertise complémentaire avant de prendre sa décision doit être écarté ;<br/>
<br/>
              7. Considérant, en dernier lieu, que le rapport d'expertise conclut que l'intéressé présentait lors de son examen du 3 octobre 2011 un déni massif de sa maladie psychiatrique qui le rendait dangereux pour ses patients de façon irrémédiable ; que M. A...n'apporte aucun élément de nature à établir que son état de santé s'était amélioré à la date de la décision contestée ; que, par suite,  il n'est pas fondé à soutenir que le Conseil national de l'ordre des médecins, qui ne s'est pas fondé sur des faits anciens ou erronés, aurait fait une inexacte application des dispositions de l'article R. 4124-3 du code de la santé publique en prononçant sa suspension ; que, par ailleurs, le choix de la durée de la mesure de suspension prononcée ne procède pas d'une erreur manifeste d'appréciation ; <br/>
<br/>
              Sur les dépens :<br/>
<br/>
              8. Considérant que, dans les circonstances de l'espèce, il y a lieu de laisser la contribution pour l'aide juridique à la charge de M. A...;<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              9. Considérant que ces dispositions font obstacle à ce qu'une somme soit mise à la charge du Conseil national de l'ordre des médecins, qui n'est pas la partie perdante dans la présente instance, au titre des frais exposés par M. A...et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
Article 1er : La requête de M. A...est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. B...A...et au Conseil national de l'ordre des médecins.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
