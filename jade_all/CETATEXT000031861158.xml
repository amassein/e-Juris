<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031861158</ID>
<ANCIEN_ID>JG_L_2015_12_000000374836</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/86/11/CETATEXT000031861158.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème / 10ème SSR, 30/12/2015, 374836, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>374836</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème / 10ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER, TEXIDOR</AVOCATS>
<RAPPORTEUR>Mme Marie-Gabrielle Merloz</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:374836.20151230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Euro Stockage et la société Enka ont demandé au tribunal administratif de Montreuil de prononcer la décharge de la retenue à la source à laquelle la première société a été assujettie au titre des dividendes distribués à la société Enka en 2005 et 2006, ainsi que des intérêts de retard correspondants.<br/>
<br/>
              Par un jugement n° 0913878 du 28 avril 2011, le tribunal administratif de Montreuil a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 11VE02457 du 21 novembre 2013, la cour administrative d'appel de Versailles a rejeté l'appel formé par ces sociétés contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 22 janvier et 22 avril 2014 et le 8 avril 2015 au secrétariat du contentieux du Conseil d'Etat, la société Holcim France SAS, venant aux droits de la société Euro Stockage, et la société Enka SA demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 250 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - la directive 90/435/CEE du Conseil du 23 juillet 1990 ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Gabrielle Merloz, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Blancpain, Soltner, Texidor, avocat de la société Holcim France et de la société Enka ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'en vertu des stipulations de l'article 43 du traité instituant la Communauté européenne, devenu l'article 49 du traité sur le fonctionnement de l'Union européenne : " (...) les restrictions à la liberté d'établissement des ressortissants d'un Etat membre dans le territoire d'un autre Etat membre sont interdites. Cette interdiction s'étend également aux restrictions à la création d'agences, de succursales ou de filiales, par les ressortissants d'un État membre établis sur le territoire d'un État membre. " ; qu'en vertu des stipulations de l'article 56 du traité instituant la Communauté européenne, devenu l'article 63 du traité sur le fonctionnement de l'Union européenne : " 1. (...) toutes les restrictions aux mouvements de capitaux entre les États membres et entre les États membres et les pays tiers sont interdites./ 2. (...) toutes les restrictions aux paiements entre les États membres et entre les États membres et les pays tiers sont interdites. " ; qu'aux termes de l'article 1er de la directive 90/435/CEE du 23 juillet 1990 concernant le régime fiscal commun applicable aux sociétés mères et filiales d'Etats membres différents : " (...) 2. La présente directive ne fait pas obstacle à l'application de dispositions nationales ou conventionnelles nécessaires afin d'éviter les fraudes et abus " ;<br/>
<br/>
              2. Considérant que le 2 de l'article 119 bis du code général des impôts soumet à une retenue à la source les revenus distribués par des personnes morales françaises à des personnes qui n'ont pas leur domicile fiscal ou leur siège en France ; que l'article 119 ter de ce code, qui transpose la directive 90/435/CEE du 23 juillet 1990, prévoit, sous certaines conditions, d'exonérer de cette retenue à la source la distribution de revenus à une personne morale qui justifie auprès du débiteur ou de la personne qui assure le paiement de ses revenus être le bénéficiaire effectif des dividendes et avoir son siège de direction effective dans un Etat membre de l'Union européenne sans être considérée, aux termes d'une convention en matière de double imposition conclue avec un Etat tiers, comme ayant sa résidence fiscale hors de l'Union ; que, toutefois, le 3 de l'article 119 ter prévoit que cette exonération ne s'applique pas " lorsque les dividendes distribués bénéficient à une personne morale contrôlée directement ou indirectement par un ou plusieurs résidents d'Etats qui ne sont pas membres de l'Union, sauf si cette personne morale justifie que la chaîne de participations n'a pas comme objet principal ou comme un de ses objets principaux de tirer avantage " de l'exonération ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société Euro Stockage, qui a pour activité principale le stockage de ciment et aux droits de laquelle vient la société Holcim France SAS, a versé en 2005 et 2006 des dividendes à sa société mère et unique actionnaire, la société de droit luxembourgeois Enka, laquelle était détenue à hauteur de 6 899 actions sur 6 900 par la société de droit chypriote Waverley Star Investments Ltd, elle-même entièrement contrôlée par la société Campsores Holding SA, établie en Suisse ; qu'à l'issue de la vérification de comptabilité dont elle a fait l'objet, l'administration a mis à la charge de cette société la retenue à la source prévue au 2 de l'article 119 bis du code général des impôts, au taux de 25 % mentionné au 1 de l'article 187 de ce code dans sa rédaction applicable ; que les sociétés requérantes ont sollicité le bénéfice de l'exonération de retenue à la source prévue à l'article 119 ter de ce code, s'agissant de bénéfices distribués à une société mère résidente d'un Etat membre de la Communauté européenne ; que l'administration a toutefois estimé que ces distributions entraient dans le champ de l'exception prévue par les dispositions du 3 de l'article 119 ter du code ; <br/>
<br/>
              4. Considérant, en premier lieu, que la cour, qui n'était pas tenue de répondre à tous les arguments soulevés, a suffisamment motivé son arrêt pour écarter les moyens tirés, d'une part, de ce que les dispositions des articles 119 bis et 115 quinquies du code général des impôts introduisent une différence de traitement entre les distributions opérées par les filiales et par les succursales contraire à la liberté d'établissement et, d'autre part, de ce que les dispositions du 3 de l'article 119 ter du code général des impôts sont contraires à la clause de non-discrimination prévue à l'article 21 de la convention fiscale franco-luxembourgeoise du 1er avril 1958 ; <br/>
<br/>
              5. Considérant, en second lieu, que les sociétés requérantes font valoir que les dispositions du 3 de l'article 119 ter du code général des impôts sont contraires à la liberté d'établissement et à la liberté de circulation des capitaux et méconnaissent la directive 90/435/CE du 23 juillet 1990 notamment en ce qu'elles instituent une présomption d'évasion fiscale et vont au-delà de ce qui est nécessaire pour prévenir la fraude ;<br/>
<br/>
              6. Considérant que la réponse à ces moyens dépend des questions de savoir, en premier lieu, si, lorsqu'un Etat membre fait usage de la faculté offerte par le 2 de l'article 1er de la directive 90/435/CE du 23 juillet 1990 cité au point 1 ci-dessus, il y a place pour un contrôle des actes ou accords pris pour la mise en oeuvre de cette faculté au regard du droit primaire de l'Union européenne ; en deuxième lieu, si les dispositions du 2 de l'article 1er de cette directive, qui accordent aux Etats membres une large marge d'appréciation pour déterminer quelles dispositions sont " nécessaires afin d'éviter les fraudes et abus ",  doivent être interprétées comme faisant obstacle à ce qu'un Etat membre adopte un mécanisme visant à exclure du bénéfice de l'exonération les dividendes distribués à une personne morale contrôlée directement ou indirectement par un ou plusieurs résidents d'Etats qui ne sont pas membres de l'Union, sauf si cette personne morale justifie que la chaîne de participations n'a pas comme objet principal ou comme un de ses objets principaux de bénéficier de l'exonération ; en troisième lieu, dans l'hypothèse où la conformité au droit de l'Union du mécanisme " anti-abus " mentionné ci-dessus devrait également être appréciée au regard des stipulations du traité, s'il y a lieu d'examiner celle-ci, compte tenu de l'objet de la législation en cause, au regard des stipulations de l'article 43 du traité instituant la Communauté européenne, devenu l'article 49 du traité sur le fonctionnement de l'Union européenne, alors même que la société bénéficiaire de la distribution des dividendes est contrôlée directement ou indirectement, à l'issue d'une chaîne de participations ayant parmi ses objets principaux le bénéfice de l'exonération, par un ou plusieurs résidents d'Etats tiers, lesquels ne peuvent se prévaloir de la liberté d'établissement ; à défaut de réponse positive à la question précédente, si cette conformité doit être examinée au regard des stipulations de l'article 56 du traité instituant la Communauté européenne, devenu l'article 63 du traité sur le fonctionnement de l'Union européenne ; en quatrième lieu, si ces stipulations doivent être interprétées comme faisant obstacle à ce qu'une législation nationale prive d'exonération de retenue à la source les dividendes versés par une société d'un Etat membre à une société établie dans un autre Etat membre, lorsque ces dividendes bénéficient à une personne morale contrôlée directement ou indirectement par un ou plusieurs résidents d'Etats qui ne sont pas membres de l'Union européenne, à moins que celle-ci n'établisse que cette chaîne de participations n'a pas comme objet principal ou comme un de ses objets principaux de bénéficier de l'exonération ;<br/>
<br/>
              7. Considérant que ces questions sont déterminantes pour la solution du litige que doit trancher le Conseil d'Etat ; qu'elles présentent une difficulté sérieuse ; qu'il y a lieu, par suite, d'en saisir la Cour de justice de l'Union européenne en application de l'article 267 du traité sur le fonctionnement de l'Union européenne et, jusqu'à ce que celle-ci se soit prononcée, de surseoir à statuer sur le pourvoi des sociétés Holcim France SAS et Enka SA ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il est sursis à statuer sur le pourvoi des sociétés Holcim France SAS et Enka SA, jusqu'à ce que la Cour de justice de l'Union européenne se soit prononcée sur les questions suivantes :<br/>
1°) Lorsqu'une législation nationale d'un Etat membre utilise en droit interne la faculté offerte par le 2 de l'article 1er de la directive 90/435/CE du 23 juillet 1990, y-a-t-il place pour un contrôle des actes ou accords pris pour la mise en oeuvre de cette faculté au regard du droit primaire de l'Union européenne '<br/>
2°) Les dispositions du 2 de l'article 1er de cette directive, qui accordent aux Etats membres une large marge d'appréciation pour déterminer quelles dispositions sont " nécessaires afin d'éviter les fraudes et abus ", doivent-elles être interprétées comme faisant obstacle à ce qu'un Etat membre adopte un mécanisme visant à exclure du bénéfice de l'exonération les dividendes distribués à une personne morale contrôlée directement ou indirectement par un ou plusieurs résidents d'Etats qui ne sont pas membres de l'Union, sauf si cette personne morale justifie que la chaîne de participations n'a pas comme objet principal ou comme un de ses objets principaux de bénéficier de l'exonération '<br/>
3°) a) Dans l'hypothèse où la conformité au droit de l'Union du mécanisme " anti-abus " mentionné ci-dessus devrait également être appréciée au regard des stipulations du traité, y a-t-il lieu d'examiner celle-ci, compte tenu de l'objet de la législation en cause, au regard des stipulations de l'article 43 du traité instituant la Communauté européenne, devenu l'article 49 du traité sur le fonctionnement de l'Union européenne, alors même que la société bénéficiaire de la distribution des dividendes est contrôlée directement ou indirectement, à l'issue d'une chaîne de participations ayant parmi ses objets principaux le bénéfice de l'exonération, par un ou plusieurs résidents d'Etats tiers, lesquels ne peuvent se prévaloir de la liberté d'établissement '<br/>
b) A défaut de réponse positive à la question précédente, cette conformité doit-elle être examinée au regard des stipulations de l'article 56 du traité instituant la Communauté européenne, devenu l'article 63 du traité sur le fonctionnement de l'Union européenne '<br/>
4°) Les stipulations précitées doivent-elles être interprétées comme faisant obstacle à ce qu'une législation nationale prive d'exonération de retenue à la source les dividendes versés par une société d'un Etat membre à une société établie dans un autre Etat membre, lorsque ces dividendes bénéficient à une personne morale contrôlée directement ou indirectement par un ou plusieurs résidents d'Etats qui ne sont pas membres de l'Union européenne, à moins que celle-ci n'établisse que cette chaîne de participations n'a pas comme objet principal ou comme un de ses objets principaux de bénéficier de l'exonération '<br/>
Article 2 : La présente décision sera notifiée aux sociétés Holcim France SAS et Enka SA, au ministre des finances et des comptes publics et à la Cour de justice de l'Union européenne. <br/>
Copie en sera adressée pour information au Premier ministre. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
