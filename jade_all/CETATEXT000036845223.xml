<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036845223</ID>
<ANCIEN_ID>JG_L_2018_04_000000407898</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/84/52/CETATEXT000036845223.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 26/04/2018, 407898, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-04-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>407898</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>M. Marc Pichon de Vendeuil</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:407898.20180426</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Les sociétés EMTS et Envéo Ingénierie ont demandé au tribunal administratif de Toulon de condamner la communauté d'agglomération Toulon Provence Méditerranée à verser à leur groupement la somme de 337 906,50 euros hors taxes au titre de leur rémunération de maître d'oeuvre. Par un jugement n° 1203034 du 13 février 2015, le tribunal administratif de Toulon a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 15MA01487 du 12 décembre 2016, la cour administrative d'appel de Marseille a, sur appel de la société Envéo Ingénierie, annulé ce jugement et condamné la communauté d'agglomération Toulon Provence Méditerranée à verser à la société Envéo Ingénierie, représentant le groupement constitué par les sociétés EMTS et Envéo Ingénierie, la somme de 405 487,81 euros TTC.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés le 13 février et le 15 mai 2017 au secrétariat du contentieux du Conseil d'Etat, la communauté d'agglomération Toulon Provence Méditerranée demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de la société Envéo Ingénierie la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des marchés publics ;<br/>
              - le décret n° 78-1306 du 26 décembre 1978 ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Pichon de Vendeuil, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de la communauté d'agglomération Toulon Provence Méditerranée et à la SCP Waquet, Farge, Hazan, avocat de la société Envéo Ingénierie et de la société EMTS.<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des énonciations de l'arrêt attaqué que, le 3 mars 2008, le syndicat intercommunal de la région toulonnaise pour le traitement et l'évacuation en mer des eaux usées, aux droits duquel est venue la communauté d'agglomération Toulon Provence Méditerranée, a conclu avec les sociétés EMTS et Envéo Ingénierie un marché de maîtrise d'oeuvre portant sur la réhabilitation d'une station d'épuration ; que le 20 septembre 2010, le groupement a demandé une augmentation du prix du marché ; que le 30 novembre 2010, le président de la communauté d'agglomération Toulon Provence Méditerranée a rejeté cette demande ; que, par un jugement du 13 février 2015, le tribunal administratif de Toulon a rejeté la demande de la société EMTS et de la société Envéo Ingénierie tendant à la condamnation de la communauté d'agglomération à leur verser la somme de 337 906,50 euros HT au titre de leur rémunération de maître d'oeuvre ; que par un arrêt du 12 septembre 2016, la cour administrative d'appel de Marseille a annulé ce jugement et a condamné la communauté d'agglomération à verser à la société Envéo Ingénierie, représentant le groupement EMTS/Envéo Ingénierie, la somme de 405 487,81 euros toutes taxes comprises ; que la communauté d'agglomération se pourvoit en cassation contre cet arrêt ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 40.1 du cahier des clauses administratives générales applicable aux marchés de prestations intellectuelles (CCAG-PI) dans sa rédaction en vigueur à la date de conclusion du marché : " Tout différend entre le titulaire et la personne responsable du marché doit faire l'objet, de la part du titulaire, d'un mémoire de réclamation qui doit être remis à la personne responsable du marché.(...) " ; qu'il résulte de ces stipulations que le différend entre le titulaire et la personne responsable du marché doit faire l'objet, préalablement à toute instance contentieuse, d'un mémoire en réclamation de la part du titulaire du marché ; <br/>
<br/>
              3. Considérant qu'un mémoire du titulaire d'un marché ne peut être regardé comme une réclamation au sens de l'article 40.1 du CCAG-PI que s'il comporte l'énoncé d'un différend et expose de façon précise et détaillée les chefs de la contestation en indiquant, d'une part, les montants des sommes dont le paiement est demandé et, d'autre part, les motifs de ces demandes, notamment les bases de calcul des sommes réclamées ; que par suite, en se bornant à relever, pour juger que le courrier du groupement de maîtrise d'oeuvre en date 20 septembre 2010 devait être regardé comme constituant une réclamation, au sens de cet article 40.1, applicable au marché en cause, et écarter la fin de non recevoir de la communauté d'agglomération tirée de ce que le différend entre elle et son maître d'oeuvre n'avait pas fait l'objet, préalablement à l'instance contentieuse, d'un mémoire en réclamation de la part du groupement, que ce courrier détaillait le montant des prestations dont les sociétés demandaient l'indemnisation et les motifs de cette demande, sans rechercher s'il comportait, en outre, l'énoncé d'un différend, la cour a commis une erreur de droit ; que, par suite et sans qu'il soit besoin d'examiner les autres moyen du pourvoi, son arrêt doit être annulé ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              5. Considérant qu'il résulte de l'instruction que le courrier précité du 20 septembre 2010 ne comportait pas l'énoncé d'un différend dès lors que le groupement proposait différentes solutions pour fonder juridiquement l'octroi d'une augmentation de sa rémunération et indiquait : " Je demeure à votre entière disposition pour m'entretenir avec vous de la faisabilité de cette solution... " ; qu'il ne peut dès lors pas être regardé comme une réclamation au sens de l'article 40.1 du CCAG-PI ; que faute d'avoir respecté la procédure prévue à cet article 40.1, la société Envéo Ingénierie n'est pas fondée à soutenir que la demande de première instance des sociétés membres du groupement était recevable et que c'est à tort que le tribunal administratif de Toulon a rejeté leur demande tendant à la condamnation de la communauté d'agglomération à leur verser une somme de 337 906,50 euros au titre de leur rémunération de maître d'oeuvre ; <br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de la communauté d'agglomération Toulon Provence Méditerranée qui n'est pas, dans la présente instance, la partie perdante, la somme que demande la société Envéo Ingénierie ; qu'il y a lieu, en revanche, de mettre à la charge de la société Envéo Ingénierie le versement à la communauté d'agglomération Toulon Provence Méditerranée de la somme de 4 000 euros au titre des frais exposés et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 12 décembre 2016 est annulé. <br/>
Article 2 : La requête présentée par la société Envéo Ingénierie devant la cour administrative d'appel de Marseille est rejetée.<br/>
Article 3 : La société Envéo Ingénierie versera à la communauté d'agglomération Toulon Provence Méditerranée une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la communauté d'agglomération Toulon Provence Méditerranée, à la société Envéo Ingénierie et à la société EMTS.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
