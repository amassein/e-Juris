<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030509779</ID>
<ANCIEN_ID>JG_L_2015_04_000000368222</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/50/97/CETATEXT000030509779.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème SSJS, 17/04/2015, 368222, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-04-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>368222</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Jean-Marc Anton</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:368222.20150417</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. et Mme A...B...ont demandé au tribunal administratif de Nîmes de les décharger des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles ils ont été assujettis au titre de l'année 2002 ainsi que des pénalités pour manoeuvres frauduleuses dont ces impositions ont été assorties. Par un jugement n° 0802463 du 11 février 2010, le tribunal administratif de Nîmes a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 10MA01090 du 15 mars 2013, la cour administrative d'appel de Marseille a annulé ce jugement et déchargé M. et Mme B...de ces impositions supplémentaires. <br/>
<br/>
              Par un pourvoi, enregistré le 2 mai 2013 au secrétariat du contentieux du Conseil d'Etat, le ministre délégué, chargé du budget demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler les articles 1er et 2 de cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M. et MmeB....<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;		<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Anton, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de M. et Mme A...B...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'à la suite d'un contrôle sur pièces de leurs revenus portant sur l'année 2002, M. et Mme B... ont reçu une proposition de rectification du 19 décembre 2005 par laquelle ils ont été regardés comme bénéficiaires de commissions versées à une société de droit luxembourgeois dont la gérante était Mme B...; que la cour administrative d'appel de Marseille a jugé que l'administration fiscale ne pouvait fonder l'imposition supplémentaire en litige sur des informations recueillies dans le cadre d'une visite domiciliaire visant personnellement M. et Mme B...et mentionnée par cette proposition de rectification, sans engager au préalable un examen de leur situation fiscale personnelle ;<br/>
<br/>
              2. Considérant qu'aux termes du VI de l'article L. 16 B du livre des procédures fiscales, dans sa rédaction applicable au litige : "  L'administration des impôts ne peut opposer au contribuable les informations recueillies qu'après restitution des pièces et documents saisis ou de leur reproduction et mise en oeuvre des procédures de contrôle visées aux premier et deuxième alinéas de l'article L. 47 " ; qu'aux termes de l'article L. 47 du même livre : " Un examen contradictoire de la situation fiscale personnelle d'une personne physique au regard de l'impôt sur le revenu ou une vérification de comptabilité ne peut être engagée sans que le contribuable en ait été informé par l'envoi ou la remise d'un avis de vérification (...) " ; <br/>
<br/>
              3. Considérant, d'une part, que l'administration fiscale n'a produit devant les juges du fond ni sa demande au tribunal de grande instance de Carpentras d'autoriser la visite domiciliaire en litige, ni même l'ordonnance du 15 novembre 2005 par laquelle le juge des libertés et de la détention de ce tribunal aurait autorisé cette visite ; que la proposition de rectification du 19 décembre 2005 adressée aux requérants indiquait que cette ordonnance avait autorisé l'exercice d'un droit de visite et de saisie, en application de l'article L. 16 B  du livre des procédures fiscales, en des locaux et dépendances susceptibles d'être occupés, notamment, par une société de droit luxembourgeois et par M. et Mme B... ; que cette proposition précisait en outre que cette société avait compté M. et Mme B...parmi ses dirigeants ou ses salariés ; que les impositions supplémentaires envisagées portaient sur des revenus présumés distribués à raison de sommes perçues par cette société ; que, dans ces conditions, c'est sans les dénaturer que la cour a déduit des mentions portées à la page 4 de cette proposition de rectification que la visite domiciliaire visait M. et Mme B...à titre personnel ;<br/>
<br/>
              4. Considérant, d'autre part, que si elle a par ailleurs relevé que cette visite avait eu lieu au domicile des requérants, la cour ne s'est pas fondée sur cette circonstance pour en déduire que cette visite les visait personnellement et n'a dès lors ni commis d'erreur de droit ni inexactement qualifié les faits de l'espèce en jugeant, à partir des seules mentions de la proposition de rectification adressée à M. et Mme B...le 19 décembre 2005, que la visite domiciliaire dont il s'agit devait être regardée comme visant M. et Mme B...à titre personnel ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que le pourvoi du ministre délégué, chargé du budget, doit être rejeté ;<br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>                    D E C I D E :<br/>
                                    --------------<br/>
<br/>
Article 1er : Le pourvoi du ministre délégué, chargé du budget est rejeté.<br/>
<br/>
Article 2 : L'Etat versera à M. et Mme B...une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée au ministre des finances et des comptes publics  et à M. et Mme A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
