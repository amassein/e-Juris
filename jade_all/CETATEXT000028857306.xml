<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028857306</ID>
<ANCIEN_ID>JG_L_2014_04_000000345194</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/85/73/CETATEXT000028857306.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème SSJS, 11/04/2014, 345194, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-04-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>345194</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>M. Nicolas Labrune</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Delphine Hedary</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:345194.20140411</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 21 décembre 2010 et 21 mars 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. D...C..., demeurant au ... ; M. C...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09NT02625 du 21 octobre 2010 par lequel la cour administrative d'appel de Nantes a rejeté sa requête tendant à l'annulation du jugement du 18 décembre 2007 du tribunal administratif d'Orléans rejetant ses demandes tendant à la décharge de la cotisation supplémentaire d'impôt sur le revenu à laquelle il a été assujetti au titre de l'année 2002 ainsi que des pénalités correspondantes ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Nicolas Labrune, Auditeur,<br/>
<br/>
              - les conclusions de Mme Delphine Hedary, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, avocat de M. C...;<br/>
<br/>
<br/>
<br/>1. Considérant que, pour demander l'annulation de l'arrêt de la cour administrative d'appel de Nantes du 21 octobre 2010, M. C...soutient qu'en omettant de statuer sur un moyen d'appel présenté en cassation dans l'hypothèse où le Conseil d'Etat déciderait de faire usage de la faculté de régler l'affaire au fond et en ne le mettant pas, en tout état de cause, en mesure de produire des observations suite au renvoi de l'affaire devant la cour administrative d'appel, cette dernière a entaché son arrêt d'un vice de forme et d'un vice de procédure ;<br/>
<br/>
              2. Considérant que, lorsque le Conseil d'Etat, statuant sur un pourvoi en cassation formé contre une décision juridictionnelle, annule cette décision et renvoie l'affaire aux juges du fond, il appartient à la juridiction de renvoi de mettre les parties à même de produire de nouveaux mémoires pour adapter leurs prétentions et argumentations en fonction des motifs et du dispositif de la décision du Conseil d'Etat, puis de viser et d'analyser dans sa nouvelle décision l'ensemble des productions éventuellement présentées devant elle ; qu'en revanche, ni les dispositions du deuxième alinéa de l'article R. 741-2 du code de justice administrative, qui dispose que la décision juridictionnelle contient notamment " l'analyse des conclusions et mémoires ", ni aucune règle générale de procédure n'imposent au juge de renvoi de répondre aux moyens d'appel présentés en cassation dans l'hypothèse où le Conseil d'Etat déciderait de faire usage de la faculté de régler l'affaire au fond prévue par l'article L. 821-2 du code de justice administrative, ni de viser et d'analyser les mémoires produits devant le Conseil d'Etat dans lesquels ces moyens sont soulevés ;<br/>
<br/>
              3. Considérant que, par une décision n° 325783 du 6 novembre 2009, le Conseil d'Etat statuant au contentieux a annulé l'ordonnance n° 08NT00273 du 30 décembre 2008 par laquelle le président de la cour administrative d'appel de Nantes avait rejeté comme entachée d'une irrecevabilité manifeste non susceptible d'être couverte en cours d'instance la requête de M. C...tendant à l'annulation du jugement du 18 décembre 2007 du tribunal administratif d'Orléans rejetant ses demandes tendant à la décharge des impositions en litige et a renvoyé l'affaire devant cette cour ; que, par l'effet de cette décision du Conseil d'Etat, la cour s'est trouvée saisie de plein droit de la requête de M. C...; qu'avant d'y statuer à nouveau, elle devait faire connaître aux parties, qui n'avaient pas présenté d'observations après la cassation de son arrêt, qu'il leur était loisible de produire, dans le délai qu'il lui appartenait de fixer, les mémoires qu'il leur paraîtrait opportun de lui adresser ; qu'il est constant que la cour a rendu son arrêt sans avoir mis M. C...à même de produire de tels mémoires ; qu'ainsi, et sans qu'il soit besoin de statuer sur les autres moyens du pourvoi, M. C...est fondé à en demander l'annulation ;<br/>
<br/>
              4. Considérant qu'aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire " ; qu'il y a lieu, par suite, de régler l'affaire au fond ;<br/>
<br/>
              Sur la régularité du jugement attaqué : <br/>
<br/>
              5. Considérant que, pour contester la régularité du jugement du tribunal administratif d'Orléans, M. C...soutient ne pas avoir été régulièrement convoqué à l'audience, ni présent ou représenté à celle-ci ; qu'il ressort toutefois des extraits de l'application " Sagace ", qui ont été communiqués aux parties, que M. C...a signé le 16 novembre 2007 l'accusé de réception de l'avis d'audience, numéroté 297138, pour ce qui concerne l'instance n° 0701588, et que Me B...a signé le même jour l'accusé de réception de l'avis d'audience, numéroté 297090, pour ce qui concerne l'instance n° 0501223, dans laquelle il était constitué ; que, dans ces conditions, M. C...n'est pas fondé à soutenir que le jugement attaqué aurait été rendu sur une procédure irrégulière ; <br/>
<br/>
              Sur le bien-fondé des impositions :<br/>
<br/>
              6. Considérant qu'il résulte de l'instruction qu'au cours de la vérification de comptabilité dont la SARL Three Com a fait l'objet, l'administration a estimé que le compte courant d'associé ouvert dans les écritures de la société au nom de M.C..., associé-gérant, présentait, au 31 décembre 2001, un solde négatif d'un montant de 72 668,87 euros et, au 31 décembre 2002, de 140 418,73 euros  ; qu'elle a, en conséquence, réintégré, sur le fondement du a de l'article 111 du code général des impôts, dans les revenus imposables de M.C..., au titre de l'année 2002, dans la catégorie des revenus de capitaux mobiliers, une somme de 67 749,86 euros, constituée par la différence entre les deux soldes débiteurs constatés aux 31 décembre 2001 et 2002 ;<br/>
<br/>
              En ce qui concerne la qualification de revenus distribués des sommes en litige :<br/>
<br/>
              7. Considérant qu'aux termes de l'article 111 du code général des impôts : " Sont notamment considérés comme revenus distribués : a. Sauf preuve contraire, les sommes mises à la disposition des associés directement ou par personnes ou sociétés interposées à titre d'avances, de prêts ou d'acomptes (...) " ; qu'en application de ces dispositions, doivent être regardés comme des revenus distribués, sauf preuve contraire, les montants des soldes débiteurs des comptes courants ouverts dans les écritures d'une société au nom de ses associés, actionnaires ou porteurs de parts ;<br/>
<br/>
              8. Considérant que, pour soutenir que la somme imposée entre ses mains correspondait à des loyers et cessions de parts versés au profit de M. A...en paiement de l'acquisition de parts sociales de la SCI " AFP ", M. C...se borne à produire à l'appui de ses allégations la copie d'un courrier daté du 28 juillet 2003 adressé au centre des impôts de Nogent-Le-Rotrou par M.A..., bénéficiaire d'une plus-value de 39 825 euros réalisée lors de la cession de parts sociales de la SCI AFP, mais qui ne comporte pas le nom du cessionnaire ; que, par suite, il n'établit pas l'existence de la transaction avec M. A...dont il se prévaut non plus que de la réalité du paiement allégué ; que, dès lors, M. C...n'apporte pas la preuve, qui lui incombe, que les sommes figurant au débit de son compte courant d'associé ne constituaient pas des revenus distribués dont il avait eu la disposition ; que, par application de l'article 111 a, l'administration était donc fondée à réintégrer la somme de 67 749,86 euros constituée par la différence entre les deux soldes débiteurs constatés aux 31 décembre 2001 et 2002 dans le revenu imposable de M. C...de l'année 2002, au titre des revenus de capitaux mobiliers ;<br/>
<br/>
              En ce qui concerne la déduction de la prime pour l'emploi :<br/>
<br/>
              9. Considérant que M. C...demande que soit déduite de l'imposition due au titre de l'année 2001 une somme de 351 euros correspondant à une partie de la prime pour l'emploi dont il a bénéficié, avec son épouse, au titre de cette même année ; que, toutefois, ce moyen, relatif à la suppression de la prime pour l'emploi au titre de l'année 2001, est sans incidence sur le bien-fondé des impositions dues au titre de l'année 2002, seules en litige dans la présente instance ;<br/>
<br/>
              En ce qui concerne la double imposition des revenus en litige :<br/>
<br/>
              10. Considérant que M. C...soutient qu'en réintégrant dans son revenu imposable des montants correspondant au solde de son compte courant d'associé respectivement au 31 décembre 2001 et au 31 décembre 2002, l'administration soumet la somme imposée entre ses mains à une double imposition ; que, toutefois, ainsi qu'il a été dit ci-dessus, seule la différence entre le montant du solde débiteur du compte courant d'associé de M. C...constaté à la date du 31 décembre 2002 et le montant du même solde débiteur au 31 décembre 2001 a été incluse dans le revenu imposable de M. C...au titre de l'année 2002 ; que, par suite, le moyen tiré de ce que les sommes inscrites audit compte auraient fait l'objet d'une double imposition doit être écarté ;<br/>
<br/>
              Sur la demande de délais :<br/>
<br/>
              11. Considérant que M. C...demande que lui soit accordé un délai pour s'acquitter des suppléments d'imposition mis à sa charge ; qu'il n'appartient toutefois pas au juge de faire droit à une telle demande ;<br/>
<br/>
              Sur les intérêts de retard et les pénalités :<br/>
<br/>
              12. Considérant qu'aux termes de l'article 1729 du code général des impôts dans sa rédaction applicable au litige : " 1. Lorsque la déclaration ou l'acte mentionnés à l'article 1728 font apparaître une base d'imposition ou des éléments servant à la liquidation de l'impôt insuffisants, inexacts ou incomplets, le montant des droits mis à la charge du contribuable est assorti de l'intérêt de retard visé à l'article 1727 et d'une majoration de 40 p. 100 si la mauvaise foi de l'intéressé est établie ou de 80 p. 100 s'il s'est rendu coupable de manoeuvres frauduleuses ou d'abus de droits au sens de l'article L. 64 du livre des procédures fiscales " ;<br/>
<br/>
              13. Considérant, en premier lieu, que la circonstance que M. C...aurait été conduit pour des raisons personnelles à un manque de diligence dans le respect des écritures comptables est sans influence sur l'intérêt de retard dû en vertu des dispositions de l'article 1727 du code général des impôts, qui est applicable sans qu'il y ait lieu de rechercher si le contribuable était, ou non, de bonne foi ;<br/>
<br/>
              14. Considérant, en second lieu, que si M. C...n'apporte pas, ainsi qu'il a été dit au point 8, la preuve de l'existence d'une transaction qui démontrerait que les sommes figurant au débit de son compte courant d'associé ne constituaient pas des revenus distribués dont il avait eu la disposition, il résulte de l'instruction que des circonstances personnelles et familiales expliquent qu'il n'ait pas, au moment de cette transaction, réuni d'éléments de nature à justifier de sa réalité ; que, dans ces conditions et au vu de l'ensemble des circonstances de l'espèce, l'administration n'établit pas l'intention de M. C...d'éluder l'impôt et, dès lors, sa mauvaise foi ; qu'il y a lieu, par suite, de décharger M. C...des pénalités pour mauvaise foi qui lui ont été appliquées ;<br/>
<br/>
              15. Considérant qu'il résulte de tout ce qui précède que M. C...est seulement fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif d'Orléans ne l'a pas déchargé des pénalités pour mauvaise foi qui lui ont été appliquées ;<br/>
<br/>
              16. Considérant, enfin, qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 1500 euros à verser à M. C...au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nantes du 21 octobre 2010 est annulé.<br/>
Article 2 : M. C...est déchargé des pénalités pour mauvaise foi qui lui ont été appliquées.<br/>
Article 3 : Le jugement du tribunal administratif d'Orléans du 18 décembre 2007 est réformé en ce qu'il a de contraire à la présente décision.<br/>
Article 4 : L'Etat versera à M. C...une somme de 1500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : Le surplus des conclusions de la requête d'appel de M. C...est rejeté.<br/>
<br/>
Article 6 : La présente décision sera notifiée à M. D...C...et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
