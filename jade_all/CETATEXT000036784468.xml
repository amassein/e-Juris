<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036784468</ID>
<ANCIEN_ID>JG_L_2018_03_000000418822</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/78/44/CETATEXT000036784468.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 29/03/2018, 418822, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-03-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418822</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2018:418822.20180329</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire complémentaire, enregistrés les 6 et 22 mars 2018 au secrétariat du contentieux du Conseil d'Etat, M. B...A..., agissant en son nom propre, et l'association " Front des patriotes républicains ", dont il est le président, demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) de suspendre l'exécution de la décision du 16 août 2017 par laquelle le ministre d'Etat, ministre de l'intérieur, a rejeté le recours de M. A...tendant à la modification de la nuance politique " extrême-droite " attribuée par le bureau des élections du ministère de l'intérieur aux sept candidats du Front des patriotes républicains lors des élections législatives de juin 2017 ;<br/>
<br/>
              2°) de suspendre l'exécution de la décision d'attribution de la nuance politique " extrême-droite " aux sept candidats du Front des patriotes républicains aux élections législatives de juin 2017 ;<br/>
<br/>
              3°) d'enjoindre au ministre de l'Etat, ministre de l'intérieur, d'attribuer aux sept candidats concernés, dans l'attente de la décision à intervenir au fond, la nuance politique " Divers " (DIV) ;<br/>
<br/>
              4°) d'enjoindre au ministre d'Etat, ministre de l'intérieur, de publier cette attribution de nuance politique, notamment sur la page des résultats des élections législatives 2017 du site web www.interieur.gouv.fr, dans un délai de deux jours à compter de l'ordonnance à intervenir sous astreinte de 300 euros par jour de retard ;<br/>
<br/>
              5°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Les requérants soutiennent que :<br/>
              - la requête n'est pas tardive et que le Conseil d'Etat est compétent pour en connaître en premier et dernier ressort ;<br/>
              - la condition d'urgence est remplie, dès lors que l'attribution de la nuance " extrême-droite " porte un préjudice grave et immédiat à la réputation du Front des patriotes républicains et de ses membres et compromet la sincérité de l'élection législative partielle qui doit avoir lieu les 8 et 22 avril 2018 dans la 5ème circonscription des français à l'étranger ;<br/>
              - il existe un doute sérieux quant à la légalité de la décision contestée ;<br/>
              - la décision du bureau des élections du 2 juin 2017 est insuffisamment motivée, ne mentionne pas les voies et délais de recours et ne précise pas le nom et la qualité des agents chargés d'instruire la demande ;<br/>
              - la décision du ministre de l'intérieur est également entachée d'une insuffisance de motivation et d'une absence de mention des voies et délais de recours, ainsi que d'un vice de procédure ; <br/>
              - qu'elle doit être annulée par voie de conséquence de l'annulation de la décision du bureau des élections ;<br/>
              - qu'elle méconnaît le principe de neutralité et d'objectivité de l'Etat, est entachée d'erreur manifeste d'appréciation et emporte des conséquences graves pour les candidats de ce parti politique.<br/>
<br/>
<br/>
              Par un mémoire en défense, enregistré le 20 mars 2018, le ministre d'Etat, ministre de l'intérieur conclut au rejet de la requête. Il soutient que la condition d'urgence n'est pas remplie et que les moyens invoqués ne sont pas propres à caractériser l'existence d'un doute sérieux quant à la légalité de la décision contestée.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des relations entre le public et l'administration ;<br/>
      - le décret n° 2014-1479 du 9 décembre 2014 ;<br/>
      - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. A...et le Front des patriotes républicains et, d'autre part, le ministre d'Etat, ministre de l'intérieur ; <br/>
              Vu le procès-verbal de l'audience publique du vendredi 23 mars 2018 à 12 heures au cours de laquelle ont été entendus :<br/>
              - Me Coudray, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. A... et du Front des patriotes républicains ; <br/>
              - M. A...;<br/>
              - les représentants du ministre d'Etat, ministre de l'intérieur ; <br/>
<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ;<br/>
<br/>
              2. Considérant que le décret du 9 décembre 2014 relatif à la mise en oeuvre de deux traitements automatisés de données à caractère personnel dénommés " Application élection " et " Répertoire national des élus " prévoit que le ministre de l'intérieur établit une " grille des nuances politiques " retenue pour l'enregistrement des résultats de l'élection afin de faciliter l'agrégation au niveau national et la présentation de ces résultats ;<br/>
<br/>
              3. Considérant que M. A...et le Front des patriotes républicains, dont les candidats se sont vu attribuer la nuance politique " extrême droite " lors des élections législatives de juin 2017, demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de la décision du 16 août 2017 par laquelle le ministre d'Etat, ministre de l'intérieur, a rejeté leur recours tendant à ce que leur soit attribué soit la nuance " FPR ", soit une autre nuance ;<br/>
<br/>
              4. Considérant que la grille des nuances politiques retenue par le ministre de l'intérieur pour les élections législatives de 2017 ne comporte pas de nuance propre au Front des patriotes républicains ; qu'ainsi, le litige, dès lors qu'est en cause le refus par le ministre d'attribuer une nuance propre à ce mouvement politique, et donc de compléter la grille, n'est pas manifestement insusceptible de se rattacher à la compétence du conseil d'Etat en premier ressort ;<br/>
<br/>
              5. Considérant que l'appréciation à laquelle procède le ministre de l'intérieur lorsqu'il détermine la liste des nuances politiques devant figurer dans cette grille, ainsi que le rattachement d'un mouvement politique à l'une d'entre elles, ne peut être censurée par le juge de l'excès de pouvoir que si elle est entachée d'erreur manifeste ; <br/>
<br/>
              6. Considérant que pour demander la suspension de la décision contestée, M. A... et le Front des patriotes républicains soutiennent que la décision du bureau des élections du 2 juin 2017 est insuffisamment motivée, ne mentionne pas les voies et délais de recours et ne précise pas le nom et la qualité des agents chargés d'instruire la demande, que la décision du ministre de l'intérieur est également entachée d'une insuffisance de motivation et d'une absence de mention des voies et délais de recours, ainsi que d'un vice de procédure, qu'elle doit être annulée par voie de conséquence de l'annulation de la décision du bureau des élections et qu'elle méconnaît le principe de neutralité et d'objectivité de l'Etat, est entachée d'erreur manifeste d'appréciation et emporte des conséquences graves pour les candidats de ce parti politique ;<br/>
<br/>
              7. Considérant qu'aucun de ces moyens n'est de nature, en l'état de l'instruction, à créer un doute sérieux sur la légalité de la décision contestée ; que, par suite, et sans qu'il y ait lieu de se prononcer sur la condition d'urgence, la requête de M. A...et du Front des patriotes républicains doit être rejetée, y compris, par voie de conséquence, les conclusions aux fins d'injonction et celles tendant à l'application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. A...et du Front des patriotes républicains est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. B...A..., au Front des patriotes républicains et au ministre d'Etat, ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
