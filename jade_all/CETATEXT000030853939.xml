<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030853939</ID>
<ANCIEN_ID>JG_L_2015_07_000000369851</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/85/39/CETATEXT000030853939.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème SSJS, 03/07/2015, 369851, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-07-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>369851</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER, TEXIDOR</AVOCATS>
<RAPPORTEUR>M. Jean-Luc Matt</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Astrid Nicolazo de Barmon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:369851.20150703</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Soufflet Alimentaire a demandé au tribunal administratif de Lille la réduction de la cotisation de taxe professionnelle à laquelle elle a été assujettie au titre de l'année 2007 à raison de son établissement de Valenciennes, à hauteur du crédit d'impôt prévu par l'article 1647 C sexies du code général des impôts. Par un jugement n° 0903685 du 20 octobre 2011, le tribunal administratif de Lille, après avoir prononcé un non-lieu partiel à statuer à concurrence d'un dégrèvement de 31 000 euros intervenu en cours d'instance, a rejeté le surplus des conclusions de cette demande.<br/>
<br/>
              Par un arrêt n° 12DA00078 du 30 avril 2013, la cour administrative d'appel de Douai a rejeté l'appel formé par la société Soufflet Alimentaire contre ce jugement en tant qu'il avait rejeté le surplus des conclusions de sa demande. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 3 juillet 2013, 26 septembre 2013 et 24 juillet 2014 au secrétariat du contentieux du Conseil d'Etat, la société Soufflet Alimentaire demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Matt, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Marie-Astrid Nicolazo de Barmon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Blancpain, Soltner, Texidor, avocat de la société Soufflet Alimentaire ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Soufflet Alimentaire exerce notamment, dans son établissement de Valenciennes, une activité d'usinage de riz brut et de légumes secs afin de les rendre propres à la consommation humaine. Par une décision du 27 mars 2009, l'administration fiscale a rejeté sa demande tendant à ce que la cotisation de taxe professionnelle à laquelle elle a été assujettie au titre de l'année 2007 soit diminuée du crédit d'impôt prévu par l'article 1647 C sexies du code général des impôts, au motif que cette activité ne présentait pas un caractère industriel. La société se pourvoit en cassation contre l'arrêt de la cour administrative d'appel de Douai du 30 avril 2013 rejetant sa requête tendant à l'annulation du jugement du 20 octobre 2011 par lequel le tribunal administratif de Lille, après avoir prononcé un non-lieu partiel à statuer à concurrence d'un dégrèvement de 31 000 euros intervenu en cours d'instance, a rejeté le surplus des conclusions de sa demande tendant à la réduction de sa cotisation de taxe professionnelle au titre de l'année 2007.<br/>
<br/>
              2. Aux termes de l'article 1647 C sexies du code général des impôts, dans sa rédaction applicable à l'année d'imposition en litige : " I. Les redevables de la taxe professionnelle et les établissements temporairement exonérés de cet impôt en application des articles 1464 B à 1464 F peuvent bénéficier d'un crédit d'impôt, pris en charge par l'Etat et égal à 1 000 euros par salarié employé depuis au moins un an au 1er janvier de l'année d'imposition dans un établissement affecté à une activité mentionnée au premier alinéa de l'article 1465 et situé dans une zone d'emploi reconnue en grande difficulté au regard des délocalisations au titre de la même année. (...)  ". Aux termes du premier alinéa de l'article 1465 du même code, dans sa rédaction applicable à l'année d'imposition en litige : " (...) les collectivités locales et leurs groupements dotés d'une fiscalité propre peuvent (...) exonérer de la taxe professionnelle en totalité ou en partie les entreprises qui procèdent sur leur territoire soit à des extensions ou créations d'activités industrielles ou de recherche scientifique et technique, ou de services de direction, d'études, d'ingénierie et d'informatique, soit à une reconversion dans le même type d'activités, soit à la reprise d'établissements en difficulté exerçant le même type d'activités (...) ". Ont un caractère industriel, au sens de cet article, les entreprises exerçant une activité qui concourt directement à la fabrication ou à la transformation de biens corporels mobiliers et pour laquelle le rôle des installations techniques, matériels et outillages mis en oeuvre est prépondérant.<br/>
<br/>
              3. En jugeant que l'activité litigieuse ne pouvait être regardée comme concourant directement à la fabrication ou à la transformation de biens corporels mobiliers, alors qu'il ressort des énonciations de l'arrêt attaqué que cette activité comprenait notamment le décorticage, la fumigation, le blanchiment par abrasion et l'étuvage du riz brut et qu'il n'était pas sérieusement contesté par l'administration que ces opérations conduisaient à un enrichissement de sa valeur nutritive et à l'amélioration de ses propriétés de cuisson, la cour a donné aux faits de l'espèce une qualification juridique erronée. Par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt doit être annulé.<br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              5. Il résulte de ce qui a été dit au point 3 que l'activité de la société requérante présente un caractère industriel au sens de l'article 1465 du code général des impôts. Dès lors, sans qu'il soit besoin d'examiner les autres moyens de la requête, la société Soufflet Alimentaire est fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Lille a rejeté le surplus des conclusions de sa demande tendant à la réduction de la cotisation de taxe professionnelle à laquelle elle a été assujettie au titre de l'année 2007 à concurrence de la somme de 81 000 euros.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 4 000 euros à verser à la société Soufflet Alimentaire au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : L'arrêt du 30 avril 2013 de la cour administrative d'appel de Douai est annulé.<br/>
Article 2 : La cotisation de taxe professionnelle à laquelle la société Soufflet Alimentaire a été assujettie au titre de l'année 2007 à raison de son établissement de Valenciennes est réduite à concurrence de la somme de 81 000 euros.<br/>
Article 3 : Le jugement du 20 octobre 2011 du tribunal administratif de Lille est réformé en ce qu'il a de contraire à la présente décision.<br/>
Article 4 : L'Etat versera à la société Soufflet Alimentaire une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : La présente décision sera notifiée à la société Soufflet Alimentaire et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
