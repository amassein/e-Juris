<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029812984</ID>
<ANCIEN_ID>JG_L_2014_11_000000372160</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/81/29/CETATEXT000029812984.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  6ème sous-section jugeant seule, 28/11/2014, 372160, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-11-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>372160</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 6ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Henri Loyrette</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:372160.20141128</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 13 septembre 2013 au secrétariat du contentieux du Conseil d'Etat, présentée par l'Association pour la protection des animaux sauvages, dont le siège est BP 505 à Crest Cedex (26401), représentée par Mme A...B...; l'Association pour la protection des animaux sauvages demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 8 juillet 2013 pris pour l'application de l'article R. 427-6 du code de l'environnement et fixant la liste, les périodes et les modalités de destruction des espèces non indigènes d'animaux classés nuisibles sur l'ensemble du territoire métropolitain ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'environnement ; <br/>
<br/>
              Vu le décret n° 2006-672 du 8 juin 2006 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Henri Loyrette, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article R. 427-6 du code de l'environnement : " Le ministre chargé de la chasse fixe par arrêté, après avis du Conseil national de la chasse et de la faune sauvage, les listes des espèces d'animaux classés nuisibles. / I.- La liste mentionnant les périodes et les modalités de destruction des espèces d'animaux classés nuisibles sur l'ensemble du territoire métropolitain est arrêtée chaque année pour une période courant du 1er juillet au 30 juin. (...) / IV.- Le ministre inscrit les espèces d'animaux sur chacune de ces trois listes pour l'un au moins des motifs suivants : / 1° Dans l'intérêt de la santé et de la sécurité publiques ; / 2° Pour assurer la protection de la flore et de la faune ; / 3° Pour prévenir des dommages importants aux activités agricoles, forestières et aquacoles ; / 4° Pour prévenir les dommages importants à d'autres formes de propriété. / Le 4° ne s'applique pas aux espèces d'oiseaux. / Le préfet détermine les espèces d'animaux nuisibles en application du III du présent article pour l'un au moins de ces mêmes motifs. / Les listes des espèces d'animaux susceptibles d'être classés nuisibles ne peut comprendre d'espèces dont la capture ou la destruction est interdite en application de l'article L. 411-1 " ; qu'en application de ces dispositions, l'arrêté attaqué a fixé la liste des animaux classés nuisibles sur l'ensemble du territoire métropolitain jusqu'au 30 juin 2015 et a précisé les périodes et les modalités de destruction des espèces concernées ; <br/>
<br/>
              2. Considérant, en premier lieu, qu'en vertu du premier alinéa de l'article R. 427-6 du code de l'environnement, le ministre chargé de la chasse fixe par arrêté les listes des espèces d'animaux classées nuisibles après avis du Conseil national de la chasse et de la faune sauvage ; qu'il ressort des pièces du dossier que cette instance s'est prononcée le 6 juin 2013 sur le projet d'arrêté litigieux ; que le moyen tiré de ce que les prescriptions de l'article 9 du décret du 8 juin 2006 relatif à la création, à la composition et au fonctionnement de commissions administratives à caractère consultatif, aux termes duquel " Sauf urgence, les membres des commissions reçoivent, cinq jours au moins avant la date de la réunion, une convocation comportant l'ordre du jour et, le cas échéant, les documents nécessaires à l'examen des affaires qui y sont inscrites ", n'auraient pas été respectées, n'est pas assorti des précisions permettant d'en apprécier le bien-fondé ;<br/>
<br/>
              3. Considérant, en deuxième lieu, qu'il résulte des dispositions de l'article R. 427-6 du code de l'environnement, rappelées au point 1, que le ministre peut inscrire une espèce sur la liste des animaux classés nuisibles sur l'ensemble du territoire métropolitain soit, d'une part, lorsque cette espèce y est répandue de façon significative ou, eu égard à ses caractéristiques, lorsqu'elle est en mesure de se répandre à brève échéance sur l'ensemble du territoire, et que sa présence est susceptible de porter gravement atteinte aux intérêts protégés par ces dispositions, soit, d'autre part, lorsqu'il est établi qu'elle est à l'origine d'atteintes significatives aux intérêts protégés ces mêmes dispositions ; qu'il ressort des pièces du dossier que le chien viverrin, le raton laveur et le vison d'Amérique sont présents sur le territoire métropolitain et, eu égard à leurs caractéristiques d'espèces invasives, sont susceptibles de s'y répandre largement à brève échéance ; que ces espèces sont également susceptibles de causer des atteintes graves aux intérêts protégés par le code de l'environnement, le chien viverrin et le raton laveur étant notamment vecteurs de risques sanitaires et le vison d'Amérique causant notamment des dommages importants aux élevages avicoles et piscicoles ; que, dès lors, le ministre n'a pas méconnu l'article R. 427-6 du code de l'environnement en inscrivant ces espèces sur la liste des animaux nuisibles à l'échelle du territoire métropolitain ; <br/>
<br/>
              4 Considérant, en troisième lieu qu'aux termes de l'article R. 427-17 du code de l'environnement : " Le ministre chargé de la chasse fixe les conditions d'utilisation des pièges, notamment de ceux qui sont de nature à provoquer des traumatismes, afin d'assurer la sécurité publique et la sélectivité du piégeage et de limiter la souffrance des animaux " ; que l'association requérante ne peut utilement soutenir que l'arrêté attaqué, qui n'a pas pour objet de définir les conditions d'utilisation des pièges utilisés à l'encontre des espèces qu'il classe nuisible, méconnaîtrait ces dispositions en tant qu'il n'assurerait pas la sélectivité du piégeage ;  <br/>
<br/>
              5. Considérant qu'il résulte de tout ce qui précède que l'association requérante n'est pas fondée à demander l'annulation de l'arrêté qu'elle attaque ; que les dispositions de l'article L. 761-1 font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de l'Association pour la protection des animaux sauvages est rejetée. <br/>
Article 2 : La présente décision sera notifiée à l'Association pour la protection des animaux sauvages et à la ministre de l'écologie, du développement durable et de l'énergie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
