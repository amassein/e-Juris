<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028934625</ID>
<ANCIEN_ID>JG_L_2014_05_000000365216</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/93/46/CETATEXT000028934625.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère sous-section jugeant seule, 14/05/2014, 365216, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-05-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>365216</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAPORTE, BRIARD, TRICHET ; SCP BARTHELEMY, MATUCHANSKY, VEXLIARD, POUPOT</AVOCATS>
<RAPPORTEUR>M. Denis Rapone</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:365216.20140514</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>VU LA PROCEDURE SUIVANTE :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              La société Vivendi a demandé au tribunal administratif de Nice d'annuler pour excès de pouvoir la décision du 2 mars 2009 par laquelle le maire de la commune de Beausoleil (Alpes-Maritimes) a exercé le droit de préemption urbain sur les parcelles AE 375 et AE 368 et sur le bien immobilier supporté par cette dernière. Par un jugement n° 0903120 du 9 décembre 2010, le tribunal administratif de Nice a annulé cette décision.<br/>
<br/>
              Par un arrêt n° 10MA04631 du 15 novembre 2012, la cour administrative d'appel de Marseille a rejeté l'appel formé contre ce jugement par la commune de Beausoleil.<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 15 janvier 2013, 15 avril 2013 et 6 février 2014 au secrétariat du contentieux du Conseil d'Etat, la commune de Beausoleil demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt n° 10MA04631 de la cour administrative d'appel de Marseille du 15 novembre 2012 ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de la société Vivendi la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Elle soutient que :<br/>
              - l'arrêt est insuffisamment motivé ;<br/>
              - en jugeant que le courrier du notaire de la société Vivendi n'était pas un recours gracieux formé pour cette société, la cour a commis une erreur de droit et dénaturé les pièces du dossier ;<br/>
              - les moyens soulevés par les sociétés Vivendi et Veolia Eau devant le tribunal administratif ne sont pas fondés.<br/>
<br/>
              Par deux mémoires en défense, enregistrés les 25 novembre 2013 et 19 mars 2014, la société Véolia Eau CGE et la société Vivendi concluent au rejet du pourvoi et à ce que la somme de 4 000 euros soit mise à la charge de la commune de Beausoleil au titre de l'article L. 761-1 du code de justice administrative. Elles soutiennent que :<br/>
              - les moyens soulevés par la commune ne sont pas fondés ;<br/>
              - ceux des moyens soulevés dans l'hypothèse d'un règlement au fond relèvent d'une cause juridique distincte de celui soulevé dans le délai d'appel et sont donc irrecevables.<br/>
<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - le code de l'urbanisme ;<br/>
              - la loi n° 2000-321 du 12 avril 2000 ; <br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Denis Rapone, Conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delaporte, Briard, Trichet, avocat de la commune de Beausoleil, et à la SCP Barthélemy, Matuchansky, Vexliard, Poupot, avocat de la société Vivendi et de la société Veolia Eau.<br/>
<br/>
              Une note en délibéré, enregistrée le 24 mars 2014, a été présentée pour la commune de Beausoleil.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>CONSIDERANT CE QUI SUIT :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que le notaire de la société Vivendi a présenté, le 10 mars 2009, une demande tendant au retrait de l'arrêté du 2 mars 2009, reçu le 9 mars suivant, par lequel le maire de la commune de Beausoleil a exercé son droit de préemption sur des biens immobiliers dont la société était propriétaire sur le territoire de la commune. Le maire lui a fait connaître son refus par un courrier du 16 mars 2009, reçu le 23 mars suivant. L'avocat de la société Vivendi a ensuite saisi le maire d'un recours gracieux formé le 5 mai 2009 et reçu en mairie le 9 mai suivant, qui a été implicitement rejeté au terme du délai de deux mois. Enfin, la société Vivendi a saisi le tribunal administratif de Nice, par une requête enregistrée le 7 août 2009, d'une demande d'annulation de l'arrêté du 2 mars 2009 et de la décision implicite de rejet de son recours gracieux.<br/>
<br/>
              Sur le pourvoi de la commune de Beausoleil :<br/>
<br/>
              2. Tout demandeur peut valablement saisir l'autorité administrative, dans le délai de recours contentieux, d'un recours gracieux formé par le mandataire de son choix, en vertu d'un mandat exprès.<br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond que le notaire de la société Vivendi avait adressé le 9 janvier 2009 au maire de la commune de Beausoleil la déclaration d'intention d'aliéner les biens objet de la préemption litigieuse, qu'aucun document versé au dossier ne permettait de connaître l'étendue du mandat dont il bénéficiait et qu'aucune des parties n'avait mis en doute le fait que celui-ci, en saisissant le maire, avait agi en vertu d'un mandat donné par la société. Dès lors, en jugeant qu'il devait être regardé comme ayant agi de sa propre initiative, sans avoir de mandat pour défendre les intérêts de la société devant l'autorité administrative, ce dont elle a déduit que le rejet de sa demande de retrait de l'arrêté litigieux n'avait pas fait courir le délai de recours contentieux à l'encontre de la société, la cour administrative d'appel de Marseille a dénaturé les pièces du dossier qui lui était soumis.<br/>
<br/>
              4. Il résulte de ce qui précède que la commune de Beausoleil est fondée, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, à demander l'annulation de l'arrêt attaqué.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              Sur la recevabilité de la demande de première instance de la société Vivendi :<br/>
<br/>
              6. Aux termes du premier alinéa de l'article 18 de la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations : " Sont considérées comme des demandes au sens du présent chapitre les demandes et les réclamations, y compris les recours gracieux ou hiérarchiques, adressées aux autorités administratives ". Aux termes de l'article 19 de la même loi : " Toute demande adressée à une autorité administrative fait l'objet d'un accusé de réception (...) / Les délais de recours ne sont pas opposables à l'auteur d'une demande lorsque l'accusé de réception ne lui a pas été transmis ou ne comporte pas les indications prévues par le décret mentionné au premier alinéa. / Le défaut de délivrance d'un accusé de réception n'emporte pas l'inopposabilité des délais de recours à l'encontre de l'auteur de la demande lorsqu'une décision expresse lui a été régulièrement notifiée avant l'expiration du délai au terme duquel est susceptible de naître une décision implicite (...) ".<br/>
<br/>
              7. En premier lieu, il ressort des pièces du dossier que si le maire de Beausoleil a, par un courrier du 16 mars 2009 notifié le 23 mars suivant, expressément rejeté la demande de retrait de son arrêté du 2 mars 2009 formée par le notaire de la société Vivendi, cette demande n'avait pas fait l'objet d'un accusé de réception et la décision du 16 mars ne comportait pas la mention des voies et délais de recours. Par suite, elle ne pouvait faire courir de nouveau le délai de recours contentieux à l'encontre de la société Vivendi, quand bien même le notaire aurait valablement agi pour son compte.<br/>
<br/>
              8. En second lieu, la commune de Beausoleil ne peut valablement soutenir que, faute de notification régulière de la décision expresse de rejet du 16 mars 2009, la demande de retrait de l'arrêté litigieux aurait fait l'objet d'une décision implicite de rejet qui aurait de nouveau fait courir le délai de recours contentieux.<br/>
<br/>
              9. Par suite, en l'absence d'accusé de réception ou de décision expresse de rejet comportant la mention des voies et délais de recours, le délai de recours contentieux, valablement interrompu par un recours gracieux formé dans le délai de deux mois, n'a pas recommencé à courir à la suite du rejet de ce recours. Dès lors, la requête de la société Vivendi, enregistrée au greffe du tribunal administratif de Nice le 7 août 2009, n'était pas tardive.<br/>
<br/>
              Sur la légalité de l'arrêté litigieux :<br/>
<br/>
              10. Si le défendeur en première instance est en principe recevable à invoquer en appel tous moyens, même pour la première fois, cette  faculté doit cependant se combiner avec l'obligation faite à l'appelant d'énoncer, dans le délai d'appel, la ou les causes juridiques sur lesquelles il entend fonder son appel. Il suit de là que, postérieurement à l'expiration de ce délai et hors le cas où il se prévaudrait d'un moyen d'ordre public, l'appelant n'est recevable à invoquer un moyen nouveau que pour autant que celui-ci repose sur la même cause juridique qu'un moyen ayant été présenté dans le délai d'introduction de l'appel.<br/>
<br/>
              11. Le moyen, qui n'est pas d'ordre public, tiré de ce que le tribunal administratif de Nice a jugé à tort que l'arrêté litigieux méconnaissait les dispositions des articles L. 210-1 et L. 300-1 du code de l'urbanisme ne se rattache pas à la même cause juridique que le moyen, seul  invoqué dans le délai d'appel par la commune, tiré de la tardiveté de la demande de première instance. Ainsi, la commune n'est pas recevable à soulever ce moyen après l'expiration du délai d'appel.<br/>
<br/>
              12. Le surplus de l'argumentation de la commune de Beausoleil, qui tend au rejet de moyens de la société Vivendi qui n'ont pas été accueillis par le tribunal administratif de Nice, est sans incidence sur la solution du litige. <br/>
<br/>
              13. Il résulte de ce qui précède que la commune de Beausoleil n'est pas fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Nice, après avoir admis la recevabilité de la demande de la société Vivendi, a annulé l'arrêté de préemption du 2 mars 2009 et la décision implicite de rejet du recours gracieux formé à l'encontre de cet arrêté.<br/>
<br/>
              Sur l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              14. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société Vivendi, qui n'est pas, dans la présente instance, la partie perdante. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Beausoleil le versement à la société Vivendi et à la société Veolia Eau CGE d'une somme de 1 500 euros chacune au même titre.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 15 novembre 2012 est annulé.<br/>
Article 2 : L'appel de la commune de Beausoleil et ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetés.<br/>
Article 3 : La commune de Beausoleil versera à la société Vivendi et à la société Veolia Eau CGE une somme de 1 500 euros chacune au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la commune de Beausoleil, à la société Vivendi et à la société Veolia Eau CGE.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
