<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037783373</ID>
<ANCIEN_ID>JG_L_2018_12_000000425446</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/78/33/CETATEXT000037783373.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 04/12/2018, 425446, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-12-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>425446</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GADIOU, CHEVALLIER ; SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2018:425446.20181204</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme C...D..., épouseB..., a demandé au juge des référés du tribunal administratif de Paris, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, de prendre toutes mesures utiles afin de permettre l'exportation des gamètes de son fils, M. A...B..., vers un établissement de santé situé en Israël, valablement autorisé à pratiquer les procréations médicalement assistées, dans un délai de huit jours suivant la notification de l'ordonnance à intervenir. Par une ordonnance n° 1819457 du 2 novembre 2018, le juge des référés du tribunal administratif de Paris a rejeté sa demande.<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 16 et 28 novembre 2018 au secrétariat du contentieux du Conseil d'Etat, MmeD..., épouse B...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) de faire droit à sa demande de première instance.<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - elle justifie d'un intérêt à agir en son nom personnel et en sa qualité de mère de M. A...B...;<br/>
              - la condition d'urgence est remplie dès lors que, d'une part, la destruction des gamètes de son fils est susceptible d'intervenir à tout moment et, d'autre part, la législation israélienne limite la possibilité de recours à l'insémination artificielle à un délai de cinq ans suivant le décès du donneur de gamètes ; <br/>
              - les dispositions de l'article L. 2141-2 du code de la santé publique méconnaissent les stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le refus de transfert de gamètes qui lui est opposé porte une atteinte manifestement disproportionnée au droit de son fils et d'elle-même au respect de leur vie privée et familiale dès lors que son fils a manifesté très clairement son désir de paternité et son souhait d'assurer sa descendance, y compris en cas de décès, et a engagé, en France comme à l'étranger, les procédures nécessaires à cet effet, et que le droit israélien autorise la pratique de l'insémination post mortem, y compris à la demande des parents d'une personne décédée.<br/>
<br/>
              Par un mémoire en défense, enregistré le 27 novembre 2018, l'Assistance publique-Hôpitaux de Paris conclut au rejet de la requête.<br/>
<br/>
              Elle soutient que :<br/>
              - la requérante ne justifie pas, en sa qualité de mère d'un déposant de gamètes, d'un intérêt suffisant pour demander au juge des référés d'enjoindre l'exportation de ces gamètes ; elle ne saurait non plus invoquer le droit au respect de la vie privée et familiale de son fils ou de son époux décédé ;<br/>
              - l'utilisation de gamètes à des fins d'insémination est interdite, d'une part, en dehors de tout couple et, d'autre part, en cas de décès d'un des membres du couple ;<br/>
              - M. B...n'avait pas formé de projet parental au moment de son décès ;<br/>
              - la loi israélienne n'autorise pas les parents à disposer des gamètes de leur enfant en vue de procéder à une procréation post-mortem ;<br/>
              - la requérante ne justifie d'aucun lien particulier avec l'Etat d'Israël. <br/>
<br/>
              Par un mémoire en défense, enregistré le 28 novembre 2018, l'Agence de la biomédecine conclut au rejet de la requête. Elle déclare s'associer au mémoire de l'Assistance publique - Hôpitaux de Paris. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de la santé publique ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique d'une part, MmeD..., épouseB..., d'autre part, l'Assistance publique - Hôpitaux de Paris, l'Agence de la biomédecine et la ministre des solidarités et de la santé ;<br/>
<br/>
              Vu le procès-verbal de l'audience publique du jeudi 29 novembre 2018 à 10 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Chevallier, avocat au Conseil d'Etat et à la Cour de cassation, avocat de MmeD..., épouse B...;<br/>
<br/>
              - MmeD..., épouse B...;<br/>
<br/>
              - le représentant de MmeD..., épouse B...;<br/>
<br/>
              - Me Pinet, avocat au Conseil d'Etat et à la Cour de cassation, avocat de l'Assistance publique - Hôpitaux de Paris ;<br/>
<br/>
              - les représentantes de l'Assistance publique - Hôpitaux de Paris ;<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ".<br/>
<br/>
              2. Eu égard à son office, qui consiste à assurer la sauvegarde des libertés fondamentales, il appartient au juge des référés, saisi sur le fondement de l'article L. 521-2 du code de justice administrative, de prendre, en cas d'urgence, toutes les mesures qui sont de nature à remédier aux effets résultant d'une atteinte grave et manifestement illégale portée, par une autorité administrative, à une liberté fondamentale, y compris lorsque cette atteinte résulte de l'application de dispositions législatives qui sont manifestement incompatibles avec les engagements européens ou internationaux de la France, ou dont la mise en oeuvre entraînerait des conséquences manifestement contraires aux exigences nées de ces engagements.<br/>
<br/>
              3. Aux termes de l'article L. 2141-2 du code de la santé publique : " L'assistance médicale à la procréation a pour objet de remédier à l'infertilité d'un couple ou d'éviter la transmission à l'enfant ou à un membre du couple d'une maladie d'une particulière gravité. Le caractère pathologique de l'infertilité doit être médicalement diagnostiqué. / L'homme et la femme formant le couple doivent être vivants, en âge de procréer et consentir préalablement au transfert des embryons ou à l'insémination. Font obstacle à l'insémination ou au transfert des embryons le décès d'un des membres du couple, le dépôt d'une requête en divorce ou en séparation de corps ou la cessation de la communauté de vie, ainsi que la révocation par écrit du consentement par l'homme ou la femme auprès du médecin chargé de mettre en oeuvre l'assistance médicale à la procréation ". L'article L. 2141-11 de ce même code dispose que : " Toute personne dont la prise en charge médicale est susceptible d'altérer la fertilité, ou dont la fertilité risque d'être prématurément altérée, peut bénéficier du recueil et de la conservation de ses gamètes ou de ses tissus germinaux, en vue de la réalisation ultérieure, à son bénéfice, d'une assistance médicale à la procréation, ou en vue de la préservation et de la restauration de sa fertilité. Ce recueil et cette conservation sont subordonnés au consentement de l'intéressé et, le cas échéant, de celui de l'un des titulaires de l'autorité parentale, ou du tuteur, lorsque l'intéressé, mineur ou majeur, fait l'objet d'une mesure de tutelle. / Les procédés biologiques utilisés pour la conservation des gamètes et des tissus germinaux sont inclus dans la liste prévue à l'article L. 2141-1, selon les conditions déterminées par cet article ". Le III de l'article R. 2141-18 du même code dispose également que : " Il est mis fin à la conservation des gamètes ou des tissus germinaux en cas de décès de la personne. Il en est de même si, n'ayant pas répondu à la consultation selon les modalités fixées par l'arrêté prévu aux articles R. 2142-24 et R. 2142-27, elle n'est plus en âge de procréer ". Il résulte de ces dispositions que, d'une part, le dépôt et la conservation des gamètes ne peuvent être autorisés, en France, qu'en vue de la réalisation d'une assistance médicale à la procréation entrant dans les prévisions légales du code de la santé publique et, d'autre part, la conservation des gamètes ne peut être poursuivie après le décès du donneur.<br/>
<br/>
              4. En outre, en vertu des dispositions de l'article L. 2141-11-1 de ce même code : " L'importation et l'exportation de gamètes ou de tissus germinaux issus du corps humain sont soumises à une autorisation délivrée par l'Agence de la biomédecine. / Seul un établissement, un organisme ou un laboratoire titulaire de l'autorisation prévue à l'article L. 2142-1 pour exercer une activité biologique d'assistance médicale à la procréation peut obtenir l'autorisation prévue au présent article./ Seuls les gamètes et les tissus germinaux recueillis et destinés à être utilisés conformément aux normes de qualité et de sécurité en vigueur, ainsi qu'aux principes mentionnés aux articles L. 1244-3, L. 1244-4, L. 2141-2, L. 2141-3, L. 2141-7 et L. 2141-11 du présent code et aux articles 16 à 16-8 du code civil, peuvent faire l'objet d'une autorisation d'importation ou d'exportation. / Toute violation des prescriptions fixées par l'autorisation d'importation ou d'exportation de gamètes ou de tissus germinaux entraîne la suspension ou le retrait de cette autorisation par l'Agence de la biomédecine ".<br/>
<br/>
              5. Il résulte de l'instruction que M. A...B..., fils unique de la requérante, est décédé le 13 janvier 2017, à l'âge de 23 ans, des suites d'une tumeur cancéreuse agressive, diagnostiquée le 21 novembre 2014. Il avait procédé, dès le 25 novembre 2014, à un dépôt de gamètes au centre d'études et de conservation des oeufs et du sperme humain (CECOS) de l'hôpital Cochin à Paris. Au cours du printemps 2017, le président du CECOS de l'hôpital Cochin a refusé verbalement de transmettre à l'Agence de la biomédecine la demande de la requérante tendant au transfert des gamètes de son fils vers un établissement de santé situé en Israël. MmeD..., épouse B...relève appel de l'ordonnance du 2 novembre 2018 par laquelle le juge des référés du tribunal administratif de Paris, statuant sur le fondement de l'article L. 521-2 du code justice administrative, a rejeté sa demande tendant à ce qu'il soit enjoint à l'Assistance Publique - Hôpitaux de Paris de prendre toutes mesures utiles afin de permettre l'exportation des gamètes de son fils décédé vers un établissement de santé situé en Israël et autorisé à pratiquer les procréations médicalement assistées.<br/>
<br/>
              6. Les dispositions mentionnées aux points 3 et 4 ne sont pas incompatibles avec les stipulations de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et, en particulier, de son article 8. <br/>
<br/>
              7. D'une part, l'interdiction posée par l'article L. 2141-2 du code de la santé publique d'utiliser les gamètes d'une personne après son décès pour réaliser une insémination relève de la marge d'appréciation dont chaque Etat dispose, dans sa juridiction, pour l'application de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et elle ne porte pas, par elle-même, une atteinte disproportionnée au droit au respect de la vie privée et familiale, tel qu'il est garanti par les stipulations de l'article 8 de cette convention.<br/>
<br/>
              8. D'autre part, l'article L. 2141-11-1 de ce même code interdit également que les gamètes déposés en France puissent faire l'objet d'une exportation, s'ils sont destinés à être utilisés, à l'étranger, à des fins qui sont prohibées sur le territoire national. Ces dernières dispositions, qui visent à faire obstacle à tout contournement des dispositions de l'article L. 2141-2, ne méconnaissent pas davantage par elles-mêmes les exigences nées de l'article 8 de cette convention.<br/>
<br/>
              9. Toutefois, la compatibilité de la loi avec les stipulations de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ne fait pas obstacle à ce que, dans certaines circonstances particulières, l'application de dispositions législatives puisse constituer une ingérence disproportionnée dans les droits garantis par cette convention. Il appartient par conséquent au juge d'apprécier concrètement si, au regard des finalités des dispositions législatives en cause, l'atteinte aux droits et libertés protégés par la convention qui résulte de la mise en oeuvre de dispositions, par elles-mêmes compatibles avec celle-ci, n'est pas excessive. <br/>
<br/>
              10. La requérante soutient que l'impossibilité de transférer à l'étranger les gamètes de son fils porte, en l'espèce, une atteinte manifestement disproportionnée au droit au respect de la vie privée et familiale de ce dernier et de ses parents. Il résulte de l'instruction que M. A...B...a, le 24 novembre 2014, soit dès l'annonce de sa maladie, pris contact avec le centre FERTAS de Lausanne (Suisse) en vue d'y déposer des gamètes et qu'il a également envisagé alors d'autres dépôts de gamètes à l'étranger, notamment à Bruxelles, Valence ou Barcelone. Il a procédé, le 25 novembre 2014, à un dépôt de gamètes au centre d'études et de conservation des oeufs et du sperme humain (CECOS) de l'hôpital Cochin à Paris et a demandé, le 22 novembre 2016, que leur conservation soit prolongée. Il a, par ailleurs, régulièrement exprimé, notamment devant ses proches et ses médecins, son désir d'être père et d'avoir une descendance, y compris en cas de décès. La requérante fait valoir, à cet égard, que la législation israélienne autorise la procréation médicalement assistée après le décès d'un donneur, y compris à la demande des parents, et que c'est pour cette raison qu'elle demande le transfert des gamètes de son fils vers l'Etat d'Israël. Ces circonstances ne sont cependant pas suffisantes pour permettre de retenir que le refus opposé au transfert de gamètes de M. A...B..., conformément à l'article L. 2141-11-1 du code de la santé publique, porterait, en l'espèce, une atteinte manifestement excessive au droit au respect de la vie privée et familiale de la requérante ou, en tout état de cause, de celle de son fils, garanti par les stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. En particulier, il n'apparaît pas que l'intéressé ait eu un projet parental précis, pour lequel il aurait procédé, à titre préventif, à un dépôt de gamètes en vue de bénéficier d'une assistance médicale à la procréation dans les conditions prévues par le code de la santé publique, ni qu'il ait explicitement consenti, à cette fin, au cas où les tentatives réalisées de son vivant se seraient révélées infructueuses, à l'utilisation de ses gamètes après son décès. Enfin, il n'est pas établi par les pièces versées au dossier qu'une insémination artificielle pourrait être réalisée en Israël à la demande de la mère de l'intéressé. Par suite, la décision contestée ne porte pas d'atteinte grave et manifestement illégale à une liberté fondamentale.<br/>
<br/>
              11. Il résulte de ce qui précède que MmeD..., épouseB..., n'est pas fondée à soutenir que c'est à tort que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Paris a rejeté sa demande. Sa requête ne peut, dès lors, qu'être rejetée.<br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de MmeD..., épouseB..., est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à Mme C...D..., épouseB..., à l'Assistance publique - Hôpitaux de Paris, à l'Agence de la biomédecine et à la ministre des solidarités et de la santé. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
