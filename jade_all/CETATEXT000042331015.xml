<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042331015</ID>
<ANCIEN_ID>JG_L_2020_09_000000443482</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/33/10/CETATEXT000042331015.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 11/09/2020, 443482, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-09-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>443482</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:443482.20200911</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              1°/ Sous le n° 443482, par une requête et un mémoire en réplique, enregistrés les 28 août et 9 septembre 2020 au secrétariat du contentieux du Conseil d'Etat, la Ligue pour la protection des oiseaux (LPO) demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
              1°) d'ordonner la suspension de l'exécution de l'arrêté du ministre de la transition écologique et solidaire du 27 août 2020 relatif à la chasse de la tourterelle des bois en France métropolitaine pour la saison 2020-2021 ; <br/>
<br/>
              2°) d'enjoindre au ministre de la transition écologique et solidaire (MTES) de ne pas ouvrir la chasse de la tourterelle des bois ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat les entiers dépens ainsi que la somme de 2 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Elle soutient que :<br/>
             - la condition d'urgence est satisfaite dès lors que l'arrêté contesté préjudicie de manière grave et immédiate aux intérêts qu'elle entend défendre en ce que, en premier lieu, il autorise la chasse à l'encontre d'une espèce menacée et en période de reproduction sans aucune mesure de préservation de l'habitat de l'espèce ni plan national de gestion, en deuxième lieu, le manque de fiabilité des contrôle peut entrainer une sous-estimation des prélèvements réellement effectués et, en dernier lieu, l'ouverture de la chasse est prévue pour le 29 août 2020 ; <br/>
             - il existe un doute sérieux quant à la légalité de l'arrêté contesté ;<br/>
             - l'arrêté contesté est entaché d'irrégularités tenant, d'une part, à sa signature par une autorité incompétente, et d'autre part, à la méconnaissance par la procédure de consultation publique de l'article L. 123-19-1 II du code de l'environnement, faute pour le public d'avoir été régulièrement informé, dès l'origine de la portée de l'avis  du comité des experts sur la gestion administrative (CEGA) du 13 mai 2019 et faute d'avoir pu se prononcer sur les dispositions relatives aux carnets de prélèvement ; <br/>
             - il méconnaît les objectifs résultant des articles 2 et 7 de la directive n° 2009/147/CE du 30 novembre 2009, transposés aux articles L. 424-1 et suivants du code de l'environnement ainsi que les objectifs du plan international d'actions pour la conservation des tourterelles des bois, dès lors qu'il maintient la chasse de cette espèce au risque de voir son déclin se poursuivre et que la date d'ouverture se situe dans sa période de reproduction.<br/>
              Par un mémoire en défense, enregistré le 7 septembre 2020, la ministre de la transition écologique conclut au rejet de la requête. Elle soutient qu'aucun des moyens n'est de nature à créer un doute sérieux quant à la légalité de l'arrêté contesté.<br/>
<br/>
              Par un mémoire en intervention, enregistré le 8 septembre 2020, la Fédération nationale des chasseurs conclut au rejet de la requête. Elle soutient que son intervention est recevable et qu'aucun des moyens de la requête n'est de nature à créer un doute sérieux quant à la légalité de l'arrêté contesté. <br/>
<br/>
<br/>
<br/>
<br/>
              2°/ Sous le n° 443567, par une requête, un mémoire en réplique et un mémoire complémentaire, enregistrés les 1er, 9 et 10 septembre 2020 au secrétariat du contentieux du Conseil d'Etat, l'association One Voice demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution de l'arrêté du MTES du 27 août 2020 relatif à la chasse de la tourterelle des bois en France métropolitaine pendant la saison 2020-2021 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 2 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Elle soutient que :<br/>
             - sa requête est recevable dès lors, d'une part, que son recours au fond a été introduit dans le délai de recours contentieux et, d'autre part, qu'elle justifie d'un intérêt lui donnant qualité pour agir ;<br/>
              - la condition d'urgence est satisfaite eu égard à la gravité de l'atteinte causée aux intérêts qu'elle entend défendre dès lors, en premier lieu, que l'arrêté autorise la chasse à l'encontre d'une espèce menacée, y compris en période de reproduction, en deuxième lieu, que le dispositif de contrôle du quota de prélèvement n'est pas fiable, en troisième lieu, que l'ouverture de la chasse est prévue pour le 29 août 2020 et, en dernier lieu, qu'aucun intérêt public ne serait mis en échec par la suspension dudit arrêté ;<br/>
              - il existe un doute sérieux quant à la légalité de l'arrêté contesté ;<br/>
              - l'arrêté contesté est entaché d'irrégularités, d'une part, au regard des articles L. 123-19-1 et suivants du code de l'environnement, compte tenu, de l'insuffisance de la note de présentation mise à disposition du public sur le projet d'arrêté et de l'absence de prise en compte des observations du public et d'autre part, en l'absence de production de l'avis du CEGA sur la base duquel il a été rendu ;<br/>
              - il est entaché, d'une part, d'une erreur de fait en ce qu'il tend à réduire et fragiliser davantage encore la population de tourterelles des bois, et d'autre part, d'une erreur de droit, au regard des articles L. 420-1 et L. 424-1 du code de l'environnement, en ce qu'il ne tend pas à remédier au déclin de cette espèce ;<br/>
              - il méconnaît l'article 7 de la directive n° 2009/147/CE dès lors que les prélèvements autorisés compromettent les efforts de conservations déjà entrepris ;<br/>
              - il méconnaît les principes de précaution et de conciliation, garantis notamment à l'article 6 de la charte de l'environnement et à l'article L. 110-1 du code de l'environnement, dès lors qu'il ne respecte pas l'exigence de protection d'une espèce en déclin ;<br/>
              - il méconnaît le principe de non-régression, rappelé notamment à l'article L. 110-1 du code de l'environnement, en ce qu'il fixe un quota plus élevé que les prélèvements opérés pendant la saison 2019-2020.<br/>
<br/>
              Par un mémoire en défense, enregistré le 7 septembre 2020, la ministre de la transition écologique conclut au rejet de la requête. Elle soutient qu'aucun des moyens n'est de nature à créer un doute sérieux quant à la légalité de l'arrêté contesté.<br/>
<br/>
              Par un mémoire en intervention, enregistré le 8 septembre 2020, la Fédération nationale des chasseurs conclut au rejet de la requête. Elle soutient que son intervention est recevable et qu'aucun des moyens de la requête n'est de nature à créer un doute sérieux quant à la légalité de l'arrêté contesté. <br/>
<br/>
              Vu les nouvelles pièces produites par la ministre de la transition écologique, enregistrées le 9 septembre 2020, suite à la mesure d'instruction supplémentaire du 8 septembre 2020 ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, la Ligue pour la protection des oiseaux et l'association One Voice et, d'autre part, la ministre de la transition écologique et solidaire et la fédération nationale des chasseurs ;<br/>
<br/>
              Ont été entendus lors de l'audience publique du 10 septembre 2020, à 14 heures :<br/>
<br/>
              - les représentants de la Ligue pour la protection des oiseaux ; <br/>
<br/>
             - Me Lyon-Caen, avocat au Conseil d'Etat et à la Cour de cassation, avocat de l'association One Voice ; <br/>
             - les représentants de l'association One Voice ;<br/>
             - les représentants de la ministre de la transition écologique et solidaire ;<br/>
             - Me Spinosi, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la fédération nationale des chasseurs ;<br/>
             - le représentant de la fédération nationale des chasseurs ;<br/>
<br/>
              à l'issue de laquelle le juge des référés a différé la clôture de l'instruction au 10  septembre 2020 à 19 heures ; <br/>
<br/>
              Vu les nouvelles pièces, enregistrées le 10 septembre 2020, produites par la ministre de la transition écologique et solidaire ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, et notamment son préambule ;<br/>
              - l'accord sur la conservation des oiseaux d'eau migrateurs d'Afrique-Eurasie conclu le 15 août 1996 ;<br/>
              - la directive 2009/147/CE du parlement européen et du conseil du 30 novembre 2009 ; <br/>
              - le code de l'environnement ;<br/>
              - l'arrêté du 26 juin 1987 fixant la liste des espèces de gibier dont la chasse est autorisée ;<br/>
              - l'arrêté du 30 août 2019 relatif à la chasse de la tourterelle des bois en France métropolitaine pendant la saison 2019-2020 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-1 du même code : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
<br/>
              2. La tourterelle des bois est un oiseau migrateur qui figure, d'une part, parmi les espèces énumérées à l'annexe II, partie B de la directive 2009/147/CE du parlement européen et du conseil du 30 novembre 2009 concernant la conservation des oiseaux sauvages, dont les Etats membres ne peuvent, en vertu du 1. et du 3. de l'article 7 de cette directive, autoriser la chasse que sous certaines conditions, et d'autre part, parmi celles qui sont mentionnées dans l'arrêté du 26 juin 1987 fixant la liste des espèces de gibier dont la chasse est autorisée. Par un arrêté du 30 août 2019, la ministre de la transition écologique et solidaire a fixé, pour la première fois, au titre de la saison de chasse 2019-2020, un quota maximal de prélèvements en le fixant à 18 000 spécimens. Par un arrêté du 27 août 2020, ce quota a été ramené à 17 460 et les modalités d'enregistrement des oiseaux capturés par les chasseurs ont été modifiées. La Ligue pour la protection des oiseaux et l'association One Voice demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, d'ordonner la suspension de l'exécution de ce dernier arrêté. Il y a lieu de les joindre leurs requêtes pour statuer par une seule ordonnance. <br/>
<br/>
              3. La Fédération nationale des chasseurs justifie d'un intérêt suffisant au maintien de l'arrêté attaqué. Ainsi, ses interventions dans le cadre des requêtes nos 443482 et 443567 sont recevables.<br/>
              Sur l'urgence :<br/>
<br/>
              4. L'urgence justifie la suspension de l'exécution d'un acte administratif lorsque celui-ci porte atteinte de manière suffisamment grave et immédiate à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre. Il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte contesté sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue. L'urgence doit être appréciée objectivement et compte tenu de l'ensemble des circonstances de l'affaire.<br/>
<br/>
              5. Il n'est pas contesté qu'eu égard à l'objet de l'arrêté dont la suspension est demandée, qui détermine les conditions dans lesquelles la tourterelle des bois peut être chassée depuis le 29 août 2020 jusqu'au 20 février 2021, au nombre maximal de prélèvements qu'il retient pour la chasse de cette espèce et qui n'est pas encore atteint, ainsi qu'à l'état de conservation de celle-ci, la condition d'urgence est remplie.<br/>
<br/>
              Sur le doute sérieux :<br/>
<br/>
              6. Aux termes de l'article 2 de la directive 2009/147/CE du parlement européen et du conseil du 30 novembre 2009 : " Les États membres prennent toutes les mesures nécessaires pour maintenir ou adapter la population de toutes les espèces d'oiseaux visées à l'article 1 er à un niveau qui corresponde notamment aux exigences écologiques, scientifiques et culturelles, compte tenu des exigences économiques et récréationnelles ". Aux termes du paragraphe 1 de l'article 3 de la directive : " Compte tenu des exigences mentionnées à l'article 2, les États membres prennent toutes les mesures nécessaires pour préserver, maintenir ou rétablir une diversité et une superficie suffisantes d'habitats pour toutes les espèces d'oiseaux visées à l'article 1er ". Aux termes de l'article 7 de la même directive : " 1. En raison de leur niveau de population, de leur distribution géographique et de leur taux de reproductivité dans l'ensemble de la Communauté, les espèces énumérées à l'annexe II peuvent faire l'objet d'actes de chasse dans le cadre de la législation nationale. Les États membres veillent à ce que la chasse de ces espèces ne compromette pas les efforts de conservation entrepris dans leur aire de distribution. / (...) 3. Les espèces énumérées à l'annexe II, partie B, peuvent être chassées seulement dans les États membres pour lesquels elles sont mentionnées. / 4. Les États membres s'assurent que la pratique de la chasse, y compris le cas échéant la fauconnerie, telle qu'elle découle de l'application des mesures nationales en vigueur, respecte les principes d'une utilisation raisonnée et d'une régulation équilibrée du point de vue écologique des espèces d'oiseaux concernées, et que cette pratique soit compatible, en ce qui concerne la population de ces espèces, notamment des espèces migratrices, avec les dispositions découlant de l'article 2. "<br/>
<br/>
              7. Aux termes de l'article L. 424-1 du code de l'environnement : " Sans qu'il soit ainsi dérogé au droit de destruction des bêtes fauves édicté à l'article L. 427-9, le ministre chargé de la chasse prend des arrêtés pour : / -prévenir la destruction ou favoriser le repeuplement des oiseaux ou de toutes espèces de gibier ;/ (...) ". Aux termes de l'article L. 425-16 du même code, dont les dispositions sont issues de la loi du 24 juillet 2019 portant création de l'Office français de la biodiversité, modifiant les missions des fédérations des chasseurs et renforçant la police de l'environnement et qui contribue à la transposition de la directive citée au point précédent : " La gestion adaptative des espèces consiste à ajuster régulièrement les prélèvements de ces espèces en fonction de l'état de conservation de leur population et de leur habitat, en s'appuyant sur les connaissances scientifiques relatives à ces populations. La gestion adaptative repose sur un système de retour d'expérience régulier et contribue à l'amélioration constante des connaissances. Les modalités de cette gestion adaptative sont définies en concertation avec l'ensemble des acteurs concernés ". Aux termes de l'article L. 425-17 du même code : " Le ministre chargé de l'environnement peut déterminer par arrêté le nombre maximal de spécimens des espèces mentionnées à l'article L. 425-16 à prélever annuellement ainsi que les conditions spécifiques de la chasse de ces espèces. Il peut également déterminer, sur proposition de la Fédération nationale des chasseurs et après avis de l'Office français de la biodiversité, le nombre maximal de spécimens qu'un chasseur est autorisé à prélever pendant une période et sur un territoire déterminés. Cet arrêté s'impose aux décisions prises en application du présent chapitre. ". <br/>
<br/>
              8. En premier lieu, il résulte de l'instruction, et notamment de la note de présentation du projet d'arrêté rédigée par l'administration elle-même en vue de la consultation du public sur celui-ci prévu par l'article L. 123-19-1 du code de l'environnement, que le nombre de tourterelles en Europe a diminué de près de 80% entre 1980 et 2015, tout particulièrement sur la voie de migration occidentale dont fait partie la France, laquelle représente avec 400 000 à 480 000 couples, 10% de la population reproductrice européenne, en diminution de 44% sur les dix dernières années. L'espèce est en déclin selon le Museum national d'histoire naturelle et est classée comme vulnérable par l'Union internationale pour la conservation de la nature (UICN). <br/>
<br/>
              9. En deuxième lieu, il est constant qu'un " plan d'action international pour la conservation de la tourterelle des bois " a été adopté en 2018 par les Etats signataires de l'accord sur la conservation des oiseaux d'eau migrateurs d'Afrique-Eurasie (" AEWA "), auquel l'Union européenne est également partie et qu'elle met en oeuvre par la directive du 30 novembre 2009. Ce plan indique que doivent être abordés simultanément, notamment, les pertes d'un bon habitat pour la reproduction et pour l'hivernage des tourterelles ainsi que le caractère non durable de leur chasse. A ce second titre, il comporte deux mesures 3.1.1 et 3.2.1 tendant à " mettre en oeuvre un moratoire temporaire sur la chasse jusqu'à l'élaboration d'un cadre de modélisation pour la gestion adaptative des prélèvements " et à " élaborer un cadre solide de modélisation de la gestion adaptative pour la chasse de la tourterelle des bois pour chaque voie de migration, sur la base de données démographiques et provenant de l'activité de chasse, et proposer des quotas et des saisons de chasse à appliquer au niveau national et local " à travers un groupe de travail international. Lors de l'approbation du plan, la France a indiqué qu'elle n'appliquerait pas le moratoire souhaitant mettre en oeuvre, à son niveau, une modélisation de la gestion adaptative dès le début de 2019.<br/>
<br/>
              10. En troisième lieu, il résulte de l'avis du 13 mai 2019, rendu, en application de l'article D. 421-51 du code de l'environnement, par le comité d'experts sur la gestion adaptative (CEGA), qui réunit notamment des représentants de la fédération nationale des chasseurs et de l'Office français de la biodiversité, que sa recommandation, pour la saison 2019-2020, de retenir un quota de prélèvement n'excédant pas 18 300 individus n'était que subsidiaire, " si un quota de prélèvement non nul devait être attribué ", par rapport à la préconisation principale tendant à retenir un quota de prélèvement fixé à zéro dont il était précisé qu'il conduisait déjà, au regard des modèles démographiques utilisés, à ce que " la probabilité que le déclin de la population se poursuive reste élevée (43%) ". Le comité soulignait que " la gestion des prélèvements doit être accompagnée d'actions fortes visant à améliorer la qualité de l'habitat (disponibilité en ressources alimentaires et sites de nidification) afin de maximiser les chances de restauration des effectifs de Tourterelle des bois ", que " dans le cadre d'une procédure de gestion adaptative, le comité réexaminera sa position à l'issue de la saison de chasse 2019 à la lumière des données nouvellement récoltées et d'éventuelles recommandations émanant d'un plan de gestion européen " et que " bien évidemment, la poursuite de l'évaluation des tendances des populations nicheuses sur un pas de temps annuel s'avère fondamentale ". <br/>
<br/>
              11. En quatrième lieu, il résulte du compte-rendu de la séance du 29 juin 2020 au cours de laquelle le comité précité a examiné le quota de prélèvement litigieux qu'il ne disposait d'aucune nouvelle information utilisable, ce qui l'a conduit à reconduire son avis de l'année précédente, un membre ayant précisé que les premiers résultats de la modélisation engagée au niveau international interviendrait en octobre en vue d'une communication officielle à la fin de l'automne. L'absence de donnée résulte, par ailleurs, des dispositions mêmes de l'arrêté du 30 août 2019 mentionné au point 2 qui ne prévoient une transmission du rapport sur l'évaluation de l'impact des prélèvements de la saison de chasse se terminant en février 2020 sur l'état de conservation de l'espèce qu'avant le 30 novembre de cette année.<br/>
<br/>
              12. En cinquième lieu, il résulte de l'instruction, et notamment de la mise en demeure adressée le 25 juillet 2019 par la commission européenne à la France que si quatre-vingt zones de protection spéciale mentionne la tourterelle des bois dans les formulaires de données standard, la taille minimale de sa population n'est disponible que pour 15 sites représentant, au total, seulement 0,3% de la population française, et d'autre part, que la tourterelle ne fait l'objet d'aucune mesure de conservation spécifique, son habitat n'étant jamais pris en compte dans l'évaluation de l'incidence des projets conduits. Par ailleurs, si l'administration et la fédération nationale des chasseurs ont listé, dans la présente instance, les actions conduites sur les habitats favorables notamment à la tourterelle des bois, aucune d'entre elles ne permet de mesurer l'évolution de l'état de conservation de cette espèce et l'administration a différé, compte tenu notamment de l'état d'urgence sanitaire, l'adoption du plan national de gestion de la tourterelle du premier semestre de l'année 2020 à celui de l'année 2021.<br/>
<br/>
              13. En sixième et dernier lieu, il résulte des indications données à l'audience que la diminution de 3% du quota de prélèvement litigieux par rapport à la première saison où il avait été fixé n'a été déterminée ni au regard des 4950 prélèvements qui ont été enregistrés au titre de cette saison et dont l'exhaustivité ainsi que la représentativité sont d'ailleurs contestées, ni en prenant en compte l'état de conservation de l'espèce et les prélèvements dans les autres pays de la voie de migration occidentale, ni encore en fonction de données relatives à l'évolution de la population française depuis l'année dernière mais uniquement par application d'une règle de trois fondée sur une approximation de la baisse tendancielle de la population européenne sur les décennies passées alors qu'une telle baisse devait conduire si le gouvernement estimait qu'elle perdurait en dépit de la diminution des prélèvements, à interdire ceux-ci, s'agissant d'une espèce aussi vulnérable, et non à réduire proportionnellement leur quota.<br/>
<br/>
              14. Eu égard à l'ensemble de ces circonstances et au principe de précaution, le moyen tiré de ce que l'arrêté attaqué, en tant qu'il fixe un quota de prélèvement supérieur à zéro, méconnaît l'objectif d'amélioration de l'état de conservation de l'espèce, résultant des articles 2 et 7 de la directive n° 2009/147/CE du 30 novembre 2009, transposés notamment aux articles L. 425-16 et L. 425-17 du code de l'environnement, apparaît, en l'état de l'instruction, propre à créer un doute sérieux quant à sa légalité.<br/>
<br/>
              15. Il résulte de tout ce qui précède, sans qu'il soit besoin d'examiner les autres moyens des requêtes, que la Ligue pour la protection des oiseaux et l'association One Voice sont fondées à demander, dans la mesure susmentionnée, la suspension de l'exécution de l'arrêté litigieux. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 2 000 euros à verser à chacune des associations requérantes au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : Les interventions de la Fédération nationale des chasseurs dans le cadre des requêtes n° 443482 et 443567 sont admises. <br/>
Article 2 : L'exécution de l'arrêté de la ministre de la transition écologique et solidaire du 27 août 2020 relatif à la chasse à la tourterelle des bois en France métropolitaine pendant la saison 2020-2021 est suspendue en tant que cet arrêté fixe à un nombre supérieur à zéro le total des prélèvements autorisés pour l'ensemble du territoire métropolitain. <br/>
Article 3 : L'Etat versera une somme de 2 000 euros à la Ligue pour la protection des oiseaux ainsi qu'à l'association One Voice, au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente ordonnance sera notifiée à la Ligue pour la protection des oiseaux, à l'association One Voice, à la ministre de la transition écologique et à la Fédération nationale des chasseurs.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
