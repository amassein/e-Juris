<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037271448</ID>
<ANCIEN_ID>JG_L_2018_07_000000422244</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/27/14/CETATEXT000037271448.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 18/07/2018, 422244, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-07-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>422244</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2018:422244.20180718</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au juge des référés du tribunal administratif de Pau, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre au préfet des Landes, d'une part, d'enregistrer sa demande d'asile en procédure normale dans un délai de quarante-huit heures à compter de la notification de son ordonnance, sous astreinte de 100 euros par jour de retard, et, d'autre part, de lui délivrer une convocation à la préfecture afin de s'y voir remettre l'attestation de demandeur d'asile afférente et le dossier à adresser à l'Office français de protection des réfugiés et apatrides, dans un délai de quarante-huit heures à compter de la notification de l'ordonnance, sous astreinte de 100 euros par jour de retard. Par une ordonnance n° 1801457 du 2 juillet 2018, le juge des référés du tribunal administratif de Pau a rejeté sa demande.<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 15 et 18 juillet 2018 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) de l'admettre au bénéfice de l'aide juridictionnelle provisoire ;<br/>
<br/>
              2°) d'annuler cette ordonnance ;<br/>
<br/>
              3°) d'enjoindre au préfet des Landes d'enregistrer sa demande d'asile en procédure normale et de lui délivrer l'attestation de demandeur d'asile afférente et le dossier à adresser à l'Office français de protection des réfugiés et apatrides, dans un délai de sept jours à compter de la notification de l'ordonnance à intervenir ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
              Il soutient que : <br/>
              - la condition d'urgence est remplie, dès lors que, d'une part, sa demande d'asile, enregistrée le 16 novembre 2017, n'a toujours pas été examinée et, d'autre part, il est susceptible d'être transféré à tout moment vers l'Italie ; <br/>
              - le refus du préfet des Landes d'enregistrer sa demande d'asile en procédure normale en violation de l'article 29 du règlement (UE) n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013 porte une atteinte grave et manifestement illégale à son droit constitutionnel d'asile ; <br/>
              - il n'a jamais été en fuite dès lors que, d'une part, il a respecté les modalités de son assignation à résidence et, d'autre part, il n'a eu connaissance que le 4 juin 2018 de sa convocation au rendez-vous fixé le 1er juin 2018 au cours duquel devaient lui être remis les documents nécessaires à son transfert vers l'Italie.<br/>
<br/>
<br/>
              Par un mémoire en défense, enregistré le 18 juillet 2018, le ministre d'Etat, ministre de l'intérieur, conclut au non-lieu à statuer sur la requête. Il soutient que postérieurement à l'introduction de la requête, le 17 juillet 2018, M. A...a été convoqué le 20 juillet 2018 au guichet unique des demandes d'asile de la préfecture de la Gironde en vue de l'enregistrement de sa demande d'asile en procédure normale, de la délivrance d'une attestation de demandeur d'asile et de la remise du dossier à adresser à l'Office français de protection des réfugiés et apatrides.<br/>
<br/>
              Par un nouveau mémoire, enregistré le 18 juillet 2018, M. A...conclut à ce qu'il soit donné acte de l'engagement pris par le ministre d'Etat, ministre de l'intérieur, mais à ce qu'il soit fait droit à ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le règlement (UE) n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique d'une part, M. A...et, d'autre part, le ministre d'Etat, ministre de l'intérieur ; <br/>
<br/>
              Vu la lettre informant les parties de la radiation de l'affaire du rôle de l'audience publique du 19 juillet 2018 ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ".<br/>
<br/>
              2. Le droit constitutionnel d'asile, qui a le caractère d'une liberté fondamentale, a pour corollaire le droit de solliciter le statut de réfugié. S'il implique que l'étranger qui sollicite la reconnaissance de la qualité de réfugié soit en principe autorisé à demeurer sur le territoire jusqu'à ce qu'il ait été statué sur sa demande, ce droit s'exerce dans les conditions définies par le code de l'entrée et du séjour des étrangers et du droit d'asile. L'article L. 742-3 de ce code prévoit que l'étranger dont l'examen de la demande d'asile relève de la responsabilité d'un autre Etat peut faire l'objet d'un transfert vers l'Etat qui est responsable de cet examen en application des dispositions du règlement du Parlement européen et du Conseil du 26 juin 2013. En application de l'article 29 de ce règlement, et sauf dans les cas de prolongation qu'il prévoit en cas d'emprisonnement ou de fuite de la personne concernée, le transfert ne peut avoir lieu que dans un délai de six mois à compter de l'acceptation de la demande de prise en charge.<br/>
<br/>
              3. M.A..., ressortissant sud-soudanais, relève appel de l'ordonnance du 2 juillet 2018 par laquelle le juge des référés du tribunal administratif de Pau a rejeté sa demande tendant à ce qu'il soit enjoint à l'administration d'enregistrer sa demande d'asile en procédure normale, de lui délivrer l'attestation de demandeur d'asile et de lui remettre le dossier à adresser à l'Office français de protection des réfugiés et apatrides.<br/>
<br/>
              4. Toutefois, par un courriel du 17 juillet 2018, soit postérieurement à l'introduction de sa requête, le préfet des Landes a convoqué M. A...le 20 juillet 2018 à 11 heures 40 au guichet unique des demandeurs d'asile de la préfecture de la Gironde en vue de l'enregistrement de sa demande d'asile en procédure normale, de la délivrance d'une attestation de demandeur d'asile et de la remise du dossier à adresser à l'Office français de protection des réfugiés et apatrides. Dans ces conditions, les conclusions d'appel de M. A...tendant à ce que le juge des référés fasse usage des pouvoirs qu'il tient de l'article L. 521-2 du code de justice administrative sont devenues sans objet. Il n'y a, dès lors, pas lieu d'y statuer.<br/>
<br/>
              5. Dans les circonstances de l'espèce, et sans qu'il y ait lieu de se prononcer sur la demande d'admission provisoire au bénéfice de l'aide juridictionnelle présentée par l'intéressé, il y a lieu de mettre à la charge de l'Etat le versement de la somme de 3 000 euros au profit de M.A..., en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : Il n'y a pas lieu de statuer sur les conclusions de M. A...dirigées contre l'ordonnance du 2 juillet 2018 du juge des référés du tribunal administratif de Pau.<br/>
Article 2 : L'Etat versera une somme de 3 000 euros à M.A..., en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente ordonnance sera notifiée à M. B...A...et au ministre d'Etat, ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
