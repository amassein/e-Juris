<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028023113</ID>
<ANCIEN_ID>JG_L_2013_09_000000369069</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/02/31/CETATEXT000028023113.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  2ème sous-section jugeant seule, 25/09/2013, 369069, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-09-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>369069</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 2ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Airelle Niepce</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Damien Botteghi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:369069.20130925</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 4 juin 2013 au secrétariat du contentieux du Conseil d'Etat, présentée par M. A...B..., demeurant ... ; M. B...demande au Conseil d'Etat d'annuler la décision du 12 mars 2013 par laquelle le ministre de l'intérieur a refusé de modifier le décret du 21 novembre 2012 lui accordant la nationalité française pour y porter le nom de son enfant Naomie Serah Doungmo Fogaing ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code civil ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en audience publique :<br/>
<br/>
              - le rapport de Mme Airelle Niepce, Maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de M. Damien Botteghi, Rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'aux termes de l'article 22-1 du code civil : " L'enfant mineur dont l'un des deux parents acquiert la nationalité française, devient français de plein droit s'il a la même résidence habituelle que ce parent ou s'il réside alternativement avec ce parent dans le cas de séparation ou divorce. / Les dispositions du présent article ne sont applicables à l'enfant d'une personne qui acquiert la nationalité française par décision de l'autorité publique ou par déclaration de nationalité que si son nom est mentionné dans le décret ou dans la déclaration. " ;<br/>
<br/>
              Considérant qu'il résulte de ces dispositions qu'un enfant mineur ne peut devenir français de plein droit par l'effet du décret qui confère la nationalité française à l'un de ses parents qu'à condition, d'une part, que ce parent ait porté son existence, sauf impossibilité ou force majeure, à la connaissance de l'administration chargée d'instruire la demande préalablement à la signature du décret et, d'autre part, qu'il ait, à la date du décret, résidé avec ce parent de manière stable et durable sous réserve, le cas échéant, d'une résidence en alternance avec l'autre parent en cas de séparation ou de divorce ;<br/>
<br/>
              Considérant que M. B...a acquis la nationalité française par l'effet d'un décret du 21 novembre 2012 ; qu'il a demandé la modification de ce décret pour faire bénéficier son enfant Naomie Serah Doungmo Fogaing, née le 5 octobre 2012, de la nationalité française en conséquence de sa naturalisation ; qu'il a formé un recours pour excès de pouvoir contre la décision du 12 mars 2013 par laquelle le ministre chargé des naturalisations a refusé de modifier le décret du 21 novembre 2012 lui accordant la nationalité française pour y porter le nom de l'enfant Naomie Serah Doungmo Fogaing ;<br/>
<br/>
              Considérant que si la décision attaquée a été prise au motif que M. B...n'aurait pas porté l'existence de son enfant Naomie Serah Doungmo Fogaing à la connaissance de l'administration avant l'intervention du décret lui accordant la nationalité française, il ressort des pièces versées au dossier que M. B...avait informé, par la préfecture des Hauts-de-Seine le 7 novembre 2012, les services du ministre chargé des naturalisations de la naissance de cet enfant ; qu'il s'ensuit que le motif opposé par la décision attaquée est matériellement inexact ;<br/>
<br/>
              Considérant toutefois que l'administration peut faire valoir devant le juge de l'excès de pouvoir que la décision dont l'annulation est demandée est légalement justifiée par un motif, de droit ou de fait, autre que celui initialement indiqué, mais également fondé sur la situation existant à la date de cette décision ; qu'il appartient alors au juge, après avoir mis à même l'auteur du recours de présenter ses observations sur la substitution ainsi sollicitée, de rechercher si un tel motif est de nature à fonder légalement la décision, puis d'apprécier s'il résulte de l'instruction que l'administration aurait pris la même décision si elle s'était fondée initialement sur ce motif ; que dans l'affirmative il peut procéder à la substitution demandée, sous réserve toutefois qu'elle ne prive pas le requérant d'une garantie procédurale liée au motif substitué ;<br/>
<br/>
              Considérant que dans son mémoire en défense, communiqué à M.B..., le ministre de l'intérieur demande au Conseil d'Etat de substituer au motif erroné retenu par la décision attaquée un autre motif, tiré de ce que l'enfant Naomie Serah Doungmo Fogaing ne résidait pas habituellement avec son père à la date de l'acquisition de la nationalité française ; qu'il ressort, en effet, des pièces du dossier que l'enfant résidait avec sa mère au Sénégal à la date de signature du décret accordant la nationalité française à M. B...;<br/>
<br/>
              Considérant qu'il résulte de l'instruction que le ministre aurait pris la même décision s'il s'était fondé sur ce motif ; que par suite, dès lors qu'elle ne prive le requérant d'aucune garantie procédurale, il y a lieu, dans les circonstances de l'espèce, de procéder à la substitution de motifs demandée ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que M. B...n'est pas fondé à demander l'annulation pour excès de pouvoir de la décision du 12 mars 2013 par laquelle le ministre de l'intérieur a refusé de modifier le décret du 21 novembre 2012 lui accordant la nationalité française pour y porter le nom de l'enfant Naomie Serah Doungmo Fogaing ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de M. B...est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. A...B...et au ministre de l'intérieur.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
