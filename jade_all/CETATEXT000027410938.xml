<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027410938</ID>
<ANCIEN_ID>JG_L_2013_05_000000352936</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/41/09/CETATEXT000027410938.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  5ème sous-section jugeant seule, 15/05/2013, 352936, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-05-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>352936</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 5ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; SCP ROGER, SEVAUX ; LE PRADO</AVOCATS>
<RAPPORTEUR>Mme Marie Gautier-Melleray</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:352936.20130515</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 26 septembre et 26 décembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B... C..., demeurant..., ; M.  C...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 08LY02216 du 21 avril 2011 de la cour administrative d'appel de Lyon, d'une part, en tant qu'il a refusé de l'indemniser au titre de son préjudice professionnel et de son préjudice d'agrément et, d'autre part, en tant qu'il a limité l'indemnisation de son préjudice pour recours à une tierce personne à 1 780 euros et de son préjudice esthétique à 600 euros, en réparation des conséquences dommageables de l'infection nosocomiale qu'il a contractée lors de son hospitalisation au centre hospitalier de Moulins-Yzeure ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel, et d'assortir les sommes dues par le centre hospitalier des intérêts au taux légal et de la capitalisation des intérêts ;<br/>
<br/>
              3°) de mettre à la charge du centre hospitalier de Moulins-Yzeure la somme de 3 000 euros à verser directement à la SCP Waquet, Farge, Hazan, avocat de M. C..., sur le fondement des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Gautier-Melleray, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de la SCP Waquet, Farge, Hazan avocat de M. C... ;<br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Waquet, Farge, Hazan avocat de M.C... ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux " ;<br/>
<br/>
              2. Considérant que pour demander l'annulation de l'arrêt de la cour administrative d'appel de Lyon qu'il attaque, M.  C...soutient que la cour a commis une erreur de droit, dénaturé les pièces du dossier et insuffisamment motivé son arrêt en jugeant que son invalidité n'était pas la conséquence directe de la faute de l'hôpital au motif qu'il souffrait d'autres affections et séquelles d'accident ; qu'elle a commis une erreur de droit en se référant au salaire horaire moyen d'une aide à domicile pour évaluer son préjudice indemnisable au titre du recours à une tierce personne à 1780 euros, alors qu'en l'espèce, son épouse avait cessé son activité professionnelle pour l'aider, et que son indemnisation devait être calculée sur la base des pertes de salaire de celle-ci ; qu'elle a dénaturé le rapport d'expertise établi par le docteur A...en évaluant la durée d'intervention indemnisable d'une tierce personne à deux heures par jour, alors qu'il ressortait de cette expertise qu'il avait eu besoin d'une assistance très supérieure ; qu'elle a insuffisamment motivé son arrêt en fixant l'indemnisation de son préjudice esthétique à 600 euros sans s'expliquer sur ce chiffre, alors que le barème de l'ONIAM fixait le montant de cette réparation à 650 euros ; qu'elle a commis une erreur de droit, insuffisamment motivé son arrêt et dénaturé les pièces du dossier en rejetant sa demande d'indemnisation au titre du préjudice d'agrément, alors qu'il ressortait des pièces du dossier que la faute du centre hospitalier l'avait privé de la possibilité de s'adonner aux loisirs qu'il pratiquait antérieurement ;<br/>
<br/>
              3. Considérant qu'eu égard aux moyens soulevés il y a lieu d'admettre les conclusions du pourvoi qui sont dirigées contre l'arrêt attaqué en tant qu'il statue sur le préjudice d'agrément ; qu'en revanche, s'agissant des conclusions dirigées contre l'arrêt en tant qu'il s'est prononcé sur les autres préjudices dont M.  C...demandait réparations, aucun des moyens soulevés n'est de nature à permettre l'admission de ces conclusions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les conclusions du pourvoi de M.  C...dirigées contre l'arrêt du 21 avril 2011 de la cour administrative d'appel de Lyon en tant qu'il se prononce sur le préjudice d'agrément sont admises.<br/>
<br/>
Article 2 : Le surplus des conclusions du pourvoi de M.  C...n'est pas admis.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. B... C....<br/>
Copie en sera adressée pour information au centre hospitalier de Moulins-Yzeure, à la caisse primaire d'assurance maladie de l'Allier et à l'ONIAM. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
