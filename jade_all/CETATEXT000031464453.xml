<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031464453</ID>
<ANCIEN_ID>JG_L_2015_11_000000381171</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/46/44/CETATEXT000031464453.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème / 9ème SSR, 09/11/2015, 381171</TITRE>
<DATE_DEC>2015-11-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>381171</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème / 9ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Jacques Reiller</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:381171.20151109</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé à la Cour nationale du droit d'asile d'annuler la décision du 10 mai 2013 par laquelle le directeur général de l'Office français de protection des réfugiés et apatrides a rejeté sa nouvelle demande d'admission au bénéfice de l'asile ou, à défaut, de la protection subsidiaire. Par une décision n° 13020725 du 11 avril 2014, la Cour nationale du droit d'asile a rejeté son recours.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 11 juin et 10 septembre 2014 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ; <br/>
<br/>
              2°) de mettre à la charge de l'Office français de protection des réfugiés et apatrides le versement d'une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention de Genève du 28 juillet 1951 relative aux réfugiés ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la charte des droits fondamentaux de l'Union européenne ;<br/>
              - la directive n° 2005/85/CE  du 1er décembre 2005 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 2003-1176 du 10 décembre 2003 ;<br/>
              - l'ordonnance n° 2004-1248 du 24 novembre 2004 ;<br/>
              - la loi n° 2006-911 du 24 juillet 2006 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jacques Reiller, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de M. A...et à la SCP Foussard, Froger, avocat de l'Office français de protection des réfugiés et apatrides ;<br/>
<br/>
<br/>
<br/>
<br/>Sur l'intervention des associations ELENA et La Cimade :<br/>
<br/>
              1. Considérant que les associations ELENA et La Cimade justifient, eu égard à leurs objets statutaires et à leurs actions, d'un intérêt de nature à les rendre recevables à intervenir dans la présente instance devant le Conseil d'Etat ; que leur intervention doit, par suite, être admise ;<br/>
<br/>
              Sur le pourvoi :<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par décision du 30 avril 2011, le directeur général de l'Office français de protection des réfugiés et apatrides a rejeté la demande d'admission au bénéfice de l'asile ou, à défaut, de la protection subsidiaire présentée par M.A..., ressortissant russe originaire du Daghestan ; que, par une décision du 25 janvier 2013, la Cour nationale du droit d'asile a rejeté son recours contre cette décision ; que, par une décision du 10 mai 2013, le directeur général de l'Office a rejeté la nouvelle demande d'admission au bénéfice de l'asile ou, à défaut, de la protection subsidiaire, présentée par l'intéressé le 27 mars 2013, sans avoir convoqué ce dernier à une audition, au motif que sa demande était manifestement infondée ; que, par la décision attaquée du 11 avril 2014, la Cour nationale du droit d'asile a rejeté le recours de M. A... contre cette deuxième décision ;<br/>
<br/>
              3. Considérant qu'aux termes de l'article 12 de la directive 2005/85/CE du Conseil du 1er décembre 2005 relative à des normes minimales concernant la procédure d'octroi et de retrait du statut de réfugié dans les États membres : " 1. Avant que l'autorité responsable de la détermination ne se prononce, la possibilité est donnée au demandeur d'asile d'avoir un entretien personnel sur sa demande avec une personne compétente en vertu du droit national pour mener cet entretien (...)./ 2. L'entretien personnel peut ne pas avoir lieu lorsque:/ (...) c) l'autorité responsable de la détermination, sur la base d'un examen exhaustif des informations fournies par le demandeur, considère la demande comme infondée dans les cas où les circonstances prévues à l'article 23, paragraphe 4, points a), c), g), h) et j), s'appliquent. (...) " ; qu'aux termes de l'article 23, paragraphe 4, de la même directive : " Les États membres peuvent également décider, dans le respect des principes de base et des garanties fondamentales visés au chapitre II, qu'une procédure d'examen est prioritaire ou est accélérée lorsque:/ (...) h) le demandeur a introduit une demande ultérieure dans laquelle il n'invoque aucun élément nouveau pertinent par rapport à sa situation personnelle ou à la situation dans son pays d'origine (...) " ; qu'aux termes de l'article 28, paragraphe 2, de cette directive : " (...) dans les cas de demande d'asile infondée correspondant à l'une des situations, quelle qu'elle soit, énumérées à l'article 23, paragraphe 4, point a) et points c) à o), les États membres peuvent également considérer une demande comme manifestement infondée, si elle est définie comme telle dans la législation nationale " ; qu'enfin, aux termes de l'article L. 723-3 du code de l'entrée et du séjour des étrangers et du droit d'asile : " L'office convoque le demandeur à une audition. Il peut s'en dispenser s'il apparaît que : / (...) c) Les éléments fournis à l'appui de la demande sont manifestement infondés ; (...) " ;<br/>
<br/>
              4. Considérant que les dispositions de l'article 12, paragraphe 2, de la directive définissent les cas dans lesquels il peut être statué sur une demande d'asile sans entretien personnel préalable avec le demandeur ; qu'au nombre de ces cas figurent ceux, énoncés au point c) de ce paragraphe, correspondant à certaines des circonstances mentionnées à l'article 23, paragraphe 4, de la directive et, en particulier, au point h) de ce paragraphe, qui est celui où le demandeur introduit une nouvelle demande d'asile dans laquelle il n'invoque aucun élément pertinent ; que les dispositions de l'article 28, paragraphe 2, de la directive permettent aux Etats membres de prévoir dans leur droit national qu'une telle demande sera qualifiée de " manifestement infondée " ; que, par suite, en retenant que le c) de l'article L. 723-3 du code de l'entrée et du séjour des étrangers et du droit d'asile n'est contraire ni à l'article 12, paragraphe 2, ni à l'article 28, paragraphe 2, de la directive en ce qu'il autorise le rejet d'une demande de réexamen fondée sur des éléments ne présentant manifestement pas de caractère nouveau sans qu'un entretien individuel avec le demandeur d'asile ait eu lieu, la Cour nationale du droit d'asile n'a pas commis d'erreur de droit ; <br/>
<br/>
              5. Considérant que, si l'article 23, paragraphe 4, définit les cas dans lesquels la procédure d'examen peut être prioritaire ou accélérée, le renvoi par l'article 12, paragraphe 2, point c) aux " circonstances prévues par " certains points de l'article 23, paragraphe 4, n'a ni pour objet ni pour effet de limiter la dispense d'entretien individuel permise par l'article 12, paragraphe 2, point c), aux procédures prioritaires ou accélérées ; que, si l'article 12, paragraphe 2, point c) prévoit que le caractère infondé d'une demande s'apprécie " sur la base d'un examen exhaustif des informations fournies par le demandeur ", ces dispositions ne sauraient, eu égard à leur objet qui est de dispenser d'un entretien individuel avec le demandeur d'asile, imposer que cette appréciation se fasse après un entretien individuel avec ce dernier ; que, ainsi qu'il a été dit au point 4, l'article 28, paragraphe 2, de la directive permet à un Etat membre de regarder comme " manifestement infondée " une nouvelle demande dans laquelle n'est invoqué aucun élément pertinent ; qu'ainsi, en retenant que, lorsque à la suite d'une décision de rejet d'une demande d'asile devenue définitive, l'étranger entend soumettre à l'Office une demande de réexamen, celle-ci peut être rejetée sans entretien, en raison de son caractère manifestement infondé, si l'Office établit notamment que les faits nouveaux allégués reposent sur des éléments dépourvus de valeur probante, la Cour n'a pas commis d'erreur de droit ni, en tout état de cause, d'erreur de qualification juridique ; <br/>
<br/>
              6. Considérant que les stipulations de l'article 13 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, selon lesquelles " toute personne dont les droits et libertés reconnus dans la présente Convention ont été violés, a droit à l'octroi d'un recours effectif devant une instance nationale, alors même que la violation aurait été commise par des personnes agissant dans l'exercice de leurs fonctions officielles ", ne font, en tout état de cause, pas obstacle à ce que les Etats prévoient, lors de la phase administrative de l'examen d'une demande d'asile ou d'une nouvelle demande d'asile, de ne pas entendre l'intéressé lorsque cette demande est manifestement infondée ; que, par suite, le moyen tiré de ce que la Cour aurait méconnu ces stipulations et, donc, commis une erreur de droit en retenant qu'une demande de réexamen d'une demande d'asile peut être rejetée sans entretien préalable avec le demandeur lorsque celui-ci ne fournit aucun élément nouveau susceptible de l'étayer, ne peut qu'être écarté ; <br/>
<br/>
              7. Considérant que si, aux termes de l'article 41 de la charte des droits fondamentaux de l'Union européenne : " Toute personne a le droit de voir ses affaires traitées impartialement, équitablement et dans un délai raisonnable par les institutions et organes de l'Union. / Ce droit comporte notamment : / - le droit de toute personne d'être entendue avant qu'une mesure individuelle qui l'affecterait défavorablement ne soit prise à son encontre ; (...) ", il résulte de la jurisprudence de la Cour de Justice de l'Union européenne que cet article s'adresse non pas aux Etats membres mais uniquement aux institutions, organes et organismes de l'Union ; qu'ainsi, le moyen tiré de leur violation par une autorité d'un Etat membre est inopérant ;  <br/>
<br/>
              8. Considérant, toutefois, qu'il résulte également de la jurisprudence de la Cour de Justice que le droit d'être entendu fait partie intégrante du respect des droits de la défense, principe général du droit de l'Union ; qu'il appartient aux Etats membres, dans le cadre de leur autonomie procédurale, de déterminer les conditions dans lesquelles le respect de ce droit est assuré ; que ce droit se définit comme celui de toute personne de faire connaître, de manière utile et effective, son point de vue au cours d'une procédure administrative avant l'adoption de toute décision susceptible d'affecter de manière défavorable ses intérêts ; qu'il ne saurait cependant être interprété en ce sens que l'autorité nationale compétente est tenue, dans tous les cas, d'entendre l'intéressé lorsque celui-ci a déjà eu la possibilité de présenter, de manière utile et effective, son point de vue sur la décision en cause ;<br/>
<br/>
              9. Considérant que, dans la décision attaquée, la Cour nationale du droit d'asile a relevé, d'une part, que lorsqu'il sollicite le réexamen de sa demande d'asile déjà rejetée par une précédente décision devenue définitive, l'étranger, du fait même de l'accomplissement de cette démarche volontaire, ne saurait ignorer que cette demande est susceptible de faire l'objet d'un refus sans avoir été préalablement convoqué par l'Office à un entretien, s'il ne fournit pas à l'appui de celle-ci d'élément nouveau susceptible, s'il est établi, de justifier les craintes de persécutions qu'il déclare éprouver ou les menaces graves de mauvais traitements qu'il déclare encourir, d'autre part, que l'étranger peut produire, à l'appui de sa demande et à tout moment de la procédure d'instruction, toutes observations écrites et tous éléments complémentaires susceptibles de venir à son soutien, au besoin en faisant état de nouveaux éléments ; que, par suite, en retenant que la seule circonstance que le directeur général de l'Office français de protection des réfugiés et apatrides décide, au vu de l'ensemble des éléments ainsi présentés par l'intéressé, de rejeter sa demande sans le convoquer à un entretien, comme le permet la directive, ne permet pas de regarder l'étranger comme ayant été privé de son droit d'être entendu, la Cour nationale du droit d'asile n'a pas méconnu ce droit ; <br/>
<br/>
              10. Considérant que les moyens tirés de la violation des articles 47 et 48 de la charte des droits fondamentaux de l'Union européenne, relatifs au droit à un recours effectif et à accéder à un tribunal impartial ainsi qu'à la présomption d'innocence et aux droits de la défense, ne sont, en tout état de cause, pas assortis des précisions permettant d'en apprécier le bien fondé ;<br/>
<br/>
              11. Considérant qu'il résulte de tout ce qui précède que le requérant n'est pas fondé à demander l'annulation de la décision de la Cour nationale du droit d'asile qu'il attaque ; <br/>
<br/>
              12. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Office français de protection des réfugiés et apatrides, qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'intervention des associations ELENA et La Cimade est admise.<br/>
Article 2 : Le pourvoi de M. A...est rejeté.<br/>
Article 3 : La présente décision sera notifiée à M. B...A..., aux associations ELENA et La Cimade, et au directeur général de l'Office français de protection des réfugiés et apatrides.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">095-02-07-03 - ABSENCE D'AUDITION PAR L'OFPRA EN CAS DE DEMANDE DE RÉEXAMEN DE LA DEMANDE D'ASILE - MÉCONNAISSANCE DU DROIT D'ÊTRE ENTENDU GARANTI PAR LE DROIT DE L'UNION EUROPÉENNE - ABSENCE [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">095-02-08 - ABSENCE D'AUDITION PAR L'OFPRA EN CAS DE DEMANDE DE RÉEXAMEN DE LA DEMANDE D'ASILE - MÉCONNAISSANCE DU DROIT D'ÊTRE ENTENDU GARANTI PAR LE DROIT DE L'UNION EUROPÉENNE - ABSENCE [RJ2].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">15-02-01 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. PORTÉE DES RÈGLES DU DROIT DE L'UNION EUROPÉENNE. DROIT PRIMAIRE. - CHARTE DES DROITS FONDAMENTAUX - ARTICLE 41 - OBLIGATION POUR LES ETATS MEMBRES - ABSENCE [RJ1].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">15-03-04 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. APPLICATION DU DROIT DE L'UNION EUROPÉENNE PAR LE JUGE ADMINISTRATIF FRANÇAIS. CAS OÙ LE DROIT DE L'UNION NE PEUT ÊTRE UTILEMENT INVOQUÉ. - CHARTE DES DROITS FONDAMENTAUX - ARTICLE 41 - MOYEN TIRÉ DE SA VIOLATION PAR UN ETAT MEMBRE - INOPÉRANCE [RJ1].
</SCT>
<SCT ID="8E" TYPE="PRINCIPAL">15-05-001 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. RÈGLES APPLICABLES. - ARTICLE 41 - MOYEN TIRÉ DE SA VIOLATION PAR UN ETAT MEMBRE - INOPÉRANCE [RJ1].
</SCT>
<SCT ID="8F" TYPE="PRINCIPAL">15-05-002 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. RÈGLES APPLICABLES. - RESPECT DES DROITS DE LA DÉFENSE - DROIT D'ÊTRE ENTENDU - MODALITÉS D'EXERCICE ET LIMITES [RJ1] [RJ 2].
</SCT>
<SCT ID="8G" TYPE="PRINCIPAL">15-05-045-05 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. RÈGLES APPLICABLES. - ABSENCE D'AUDITION PAR L'OFPRA EN CAS DE DEMANDE DE RÉEXAMEN DE LA DEMANDE D'ASILE - MÉCONNAISSANCE DU DROIT D'ÊTRE ENTENDU GARANTI PAR LE DROIT DE L'UNION EUROPÉENNE - ABSENCE [RJ2].
</SCT>
<SCT ID="8H" TYPE="PRINCIPAL">54-07-01-04-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. MOYENS INOPÉRANTS. - MOYEN TIRÉ DE LA VIOLATION PAR UN ETAT MEMBRE DE L'ARTICLE 41 DE LA CHARTE DES DROITS FONDAMENTAUX DE L'UNION EUROPÉENNE [RJ1].
</SCT>
<ANA ID="9A"> 095-02-07-03 D'une part, lorsqu'il sollicite le réexamen de sa demande d'asile déjà rejetée par une précédente décision devenue définitive, l'étranger, du fait même de l'accomplissement de cette démarche volontaire, ne saurait ignorer que cette demande est susceptible de faire l'objet d'un refus sans avoir été préalablement convoqué par l'Office français de protection des réfugiés et apatrides (OFPRA), s'il ne fournit pas à l'appui de celle-ci d'élément nouveau susceptible, s'il est établi, de justifier les craintes de persécutions qu'il déclare éprouver ou les menaces graves de mauvais traitements qu'il déclare encourir.... ,,D'autre part, l'étranger peut produire, à l'appui de sa demande et à tout moment de la procédure d'instruction, toutes observations écrites et tous éléments complémentaires susceptibles de venir à son soutien, au besoin en faisant état de nouveaux éléments.... ,,Par suite, la seule circonstance que le directeur général de l'OFPRA décide, au vu de l'ensemble des éléments ainsi présentés par l'intéressé, de rejeter sa demande sans le convoquer à un entretien, comme le permet la directive 2005/85/CE du 1er décembre 2005, ne permet pas de regarder l'étranger comme ayant été privé de son droit d'être entendu, qui fait partie intégrante du respect des droits de la défense, principe général du droit de l'Union.</ANA>
<ANA ID="9B"> 095-02-08 D'une part, lorsqu'il sollicite le réexamen de sa demande d'asile déjà rejetée par une précédente décision devenue définitive, l'étranger, du fait même de l'accomplissement de cette démarche volontaire, ne saurait ignorer que cette demande est susceptible de faire l'objet d'un refus sans avoir été préalablement convoqué par l'Office français de protection des réfugiés et apatrides (OFPRA), s'il ne fournit pas à l'appui de celle-ci d'élément nouveau susceptible, s'il est établi, de justifier les craintes de persécutions qu'il déclare éprouver ou les menaces graves de mauvais traitements qu'il déclare encourir.... ,,D'autre part, l'étranger peut produire, à l'appui de sa demande et à tout moment de la procédure d'instruction, toutes observations écrites et tous éléments complémentaires susceptibles de venir à son soutien, au besoin en faisant état de nouveaux éléments.... ,,Par suite, la seule circonstance que le directeur général de l'OFPRA décide, au vu de l'ensemble des éléments ainsi présentés par l'intéressé, de rejeter sa demande sans le convoquer à un entretien, comme le permet la directive 2005/85/CE du 1er décembre 2005, ne permet pas de regarder l'étranger comme ayant été privé de son droit d'être entendu, qui fait partie intégrante du respect des droits de la défense, principe général du droit de l'Union.</ANA>
<ANA ID="9C"> 15-02-01 Il résulte de la jurisprudence de la Cour de Justice de l'Union européenne que l'article 41 de la charte des droits fondamentaux de l'Union européenne (droit de voir ses affaires traitées impartialement, équitablement et dans un délai raisonnable) s'adresse non pas aux Etats membres mais uniquement aux institutions, organes et organismes de l'Union. Ainsi, le moyen tiré de sa violation par une autorité d'un Etat membre est inopérant.</ANA>
<ANA ID="9D"> 15-03-04 Il résulte de la jurisprudence de la Cour de Justice de l'Union européenne que l'article 41 de la charte des droits fondamentaux de l'Union européenne (droit de voir ses affaires traitées impartialement, équitablement et dans un délai raisonnable) s'adresse non pas aux Etats membres mais uniquement aux institutions, organes et organismes de l'Union. Ainsi, le moyen tiré de sa violation par une autorité d'un Etat membre est inopérant.</ANA>
<ANA ID="9E"> 15-05-001 Il résulte de la jurisprudence de la Cour de Justice de l'Union européenne que l'article 41 de la charte des droits fondamentaux de l'Union européenne (droit de voir ses affaires traitées impartialement, équitablement et dans un délai raisonnable) s'adresse non pas aux Etats membres mais uniquement aux institutions, organes et organismes de l'Union. Ainsi, le moyen tiré de sa violation par une autorité d'un Etat membre est inopérant.</ANA>
<ANA ID="9F"> 15-05-002 Il résulte de la jurisprudence de la Cour de Justice de l'Union européenne que le droit d'être entendu fait partie intégrante du respect des droits de la défense, principe général du droit de l'Union. Il appartient aux Etats membres, dans le cadre de leur autonomie procédurale, de déterminer les conditions dans lesquelles le respect de ce droit est assuré. Ce droit se définit comme celui de toute personne de faire connaître, de manière utile et effective, son point de vue au cours d'une procédure administrative avant l'adoption de toute décision susceptible d'affecter de manière défavorable ses intérêts. Il ne saurait cependant être interprété en ce sens que l'autorité nationale compétente est tenue, dans tous les cas, d'entendre l'intéressé lorsque celui-ci a déjà eu la possibilité de présenter, de manière utile et effective, son point de vue sur la décision en cause.</ANA>
<ANA ID="9G"> 15-05-045-05 D'une part, lorsqu'il sollicite le réexamen de sa demande d'asile déjà rejetée par une précédente décision devenue définitive, l'étranger, du fait même de l'accomplissement de cette démarche volontaire, ne saurait ignorer que cette demande est susceptible de faire l'objet d'un refus sans avoir été préalablement convoqué par l'Office français de protection des réfugiés et apatrides (OFPRA), s'il ne fournit pas à l'appui de celle-ci d'élément nouveau susceptible, s'il est établi, de justifier les craintes de persécutions qu'il déclare éprouver ou les menaces graves de mauvais traitements qu'il déclare encourir.... ,,D'autre part, l'étranger peut produire, à l'appui de sa demande et à tout moment de la procédure d'instruction, toutes observations écrites et tous éléments complémentaires susceptibles de venir à son soutien, au besoin en faisant état de nouveaux éléments.... ,,Par suite, la seule circonstance que le directeur général de l'OFPRA décide, au vu de l'ensemble des éléments ainsi présentés par l'intéressé, de rejeter sa demande sans le convoquer à un entretien, comme le permet la directive 2005/85/CE du 1er décembre 2005, ne permet pas de regarder l'étranger comme ayant été privé de son droit d'être entendu, qui fait partie intégrante du respect des droits de la défense, principe général du droit de l'Union.</ANA>
<ANA ID="9H"> 54-07-01-04-03 Il résulte de la jurisprudence de la Cour de Justice de l'Union européenne que l'article 41 de la charte des droits fondamentaux de l'Union européenne (droit de voir ses affaires traitées impartialement, équitablement et dans un délai raisonnable) s'adresse non pas aux Etats membres mais uniquement aux institutions, organes et organismes de l'Union. Ainsi, le moyen tiré de sa violation par une autorité d'un Etat membre est inopérant.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CJUE, 5 novembre 2014, Sophie,contre Préfet de police et Préfet de la Seine-Saint-Denis, aff. C-166/13 ; CJUE, 11 décembre 2014, Khaled,contre Préfet des Pyrénées-Atlantiques, aff. C-249/13.,[RJ 2] Rappr., dans le cas d'une OQTF, CE, 4 juin 2014, M.,, n° 370515, p. 152.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
