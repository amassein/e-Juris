<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043243791</ID>
<ANCIEN_ID>JG_L_2021_03_000000436625</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/24/37/CETATEXT000043243791.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 11/03/2021, 436625, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-03-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>436625</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>M. Géraud Sajust de Bergues</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:436625.20210311</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La société BGL Avigros a demandé au tribunal administratif de Melun de prononcer la réduction de la cotisation foncière des entreprises à laquelle elle a été assujettie au titre des années 2013 et 2014. Par un jugement n° 1502900 du 19 octobre 2017, le tribunal administratif de Melun a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 17PA03829 du 4 avril 2019, la cour administrative d'appel de Paris, avant de statuer sur les conclusions de la requête de la société BGL Avigros, a ordonné un supplément d'instruction aux fins, pour les parties, de produire tous documents permettant de déterminer la valeur locative des locaux exploités par la société BGL Avigros. <br/>
<br/>
              Par un arrêt n° 17PA03829 du 17 octobre 2019, la cour administrative d'appel de Paris a fixé la valeur locative des locaux exploités par la société BGL Avigros, a prononcé, à due concurrence, la réduction des cotisations foncières des entreprises mises à sa charge, dans la limite du quantum de sa réclamation, et a réformé le jugement du tribunal administratif de Melun en ce qu'il avait de contraire. <br/>
<br/>
              Par un pourvoi et un mémoire en réplique, enregistrés le 10 décembre 2019 et le 8 octobre 2020 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'action et des comptes publics demande au Conseil d'Etat d'annuler cet arrêt.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Géraud Sajust de Bergues, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Célice, Texidor, Perier, avocat de la société BGL Avigros ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des énonciations de l'arrêt attaqué que la société BGL Avigros exerce une activité de grossiste en volailles et gibiers dans des locaux du bâtiment " volaille " VG 1 du marché d'intérêt national (MIN) de Rungis. Elle a été assujettie, à raison de ces locaux et à la suite d'une restructuration complète du bâtiment, à des cotisations de cotisation foncière des entreprises au titre des années 2013 et 2014, qu'elle a contestées devant le tribunal administratif de Melun au motif d'une surévaluation de la valeur locative des locaux. Elle a fait appel du jugement du 19 octobre 2017 par lequel le tribunal administratif de Melun a rejeté sa demande. Par un arrêt avant-dire droit du 4 avril 2019, la cour administrative d'appel de Paris a jugé que la valeur locative des locaux que la société exploitait au sein du MIN de Rungis devait être déterminée selon la méthode de l'appréciation directe prévue au 3° de l'article 1498 du code général des impôts et a ordonné qu'il soit procédé à un supplément d'instruction contradictoire aux fins pour les parties de produire tous éléments permettant de déterminer la valeur locative de ces locaux, créés postérieurement au 1er janvier 1970. Par un arrêt du 17 octobre 2019, la cour administrative d'appel de Paris a fixé la valeur locative des locaux exploités par la société sur la base d'une valeur du bâtiment VG1 égale à 264 105 euros et prononcé la réduction des cotisations correspondantes, dans la limite du quantum de sa réclamation devant l'administration. <br/>
<br/>
              2. D'une part, aux termes de l'article 1467 du code général des impôts dans sa rédaction applicable au litige : " La cotisation foncière des entreprises a pour base la valeur locative des biens passibles d'une taxe foncière situés en France, à l'exclusion des biens exonérés de taxe foncière sur les propriétés bâties en vertu des 11°, 12° et 13° de l'article 1382, dont le redevable a disposé pour les besoins de son activité professionnelle pendant la période de référence définie aux articles 1467 A et 1478 (...) / La valeur locative des biens passibles d'une taxe foncière est calculée suivant les règles fixées pour l'établissement de cette taxe ". <br/>
<br/>
              3. D'autre part, aux termes de l'article 1498 du code général des impôts dans sa rédaction applicable au litige : " La valeur locative de tous les biens autres que les locaux visés au I de l'article 1496 et que les établissements industriels visés à l'article 1499 est déterminée au moyen de l'une des méthodes indiquées ci-après : / (...) / 3° A défaut de ces bases, la valeur locative est déterminée par voie d'appréciation directe ". Aux termes de l'article 324 AB de l'annexe III au même code : " Lorsque les autres moyens font défaut, il est procédé à l'évaluation directe de l'immeuble en appliquant un taux d'intérêt à sa valeur vénale, telle qu'elle serait constatée à la date de référence si l'immeuble était libre de toute location ou occupation. / Le taux d'intérêt susvisé est fixé en fonction du taux des placements immobiliers constatés dans la région à la date de référence pour des immeubles similaires ". Aux termes de l'article 324 AC de cette même annexe : " En l'absence d'acte et de toute autre donnée récente faisant apparaître une estimation de l'immeuble à évaluer susceptible d'être retenue sa valeur vénale à la date de référence est appréciée d'après la valeur vénale d'autres immeubles d'une nature comparable ayant fait l'objet de transactions récentes situés dans la commune même ou dans une localité présentant du point de vue économique une situation analogue à celle de la commune en cause. / La valeur vénale d'un immeuble peut également être obtenue en ajoutant à la valeur vénale du terrain, estimée par comparaison avec celle qui ressort de transactions récentes relatives à des terrains à bâtir situés dans une zone comparable, la valeur de reconstruction au 1er janvier 1970 dudit immeuble, réduite pour tenir compte, d'une part, de la dépréciation immédiate et, d'autre part, du degré de vétusté de l'immeuble et de son état d'entretien, ainsi que de la nature, de l'importance, de l'affectation et de la situation de ce bien ".<br/>
<br/>
              4. En vertu des articles précités de l'annexe III au code général des impôts, la valeur vénale des immeubles évalués par voie d'appréciation directe doit d'abord être déterminée en utilisant les données figurant dans les différents actes constituant l'origine de la propriété de l'immeuble si ces données, qui peuvent résulter notamment d'actes de cession, de déclarations de succession, d'apport en société ou, s'agissant d'immeubles qui n'étaient pas construits en 1970, de leur  valeur lors de leur première inscription au bilan, ont une date la plus proche possible de la date de référence du 1er janvier 1970.  Si ces données ne peuvent être regardées comme pertinentes du fait qu'elles présenteraient une trop grande antériorité ou postériorité par rapport à cette date, il incombe à l'administration fiscale de proposer des évaluations fondées sur les deux autres méthodes comparatives prévues à l'article 324 AC de la même annexe, en retenant des transactions qui peuvent être postérieures ou antérieures aux actes ou au bilan mentionnés ci-dessus dès lors qu'elles ont été conclues à une date plus proche du 1er janvier 1970. Ce n'est que si l'administration n'est pas à même de proposer des éléments de calcul fondés sur l'une ou l'autre de ces méthodes et si le contribuable n'est pas davantage en mesure de fournir ces éléments de comparaison qu'il y a lieu de retenir, pour le calcul de la valeur locative, les données figurant dans les actes constituant l'origine de la propriété du bien ou, le cas échéant, dans son bilan. <br/>
<br/>
              5. En premier lieu, après avoir déterminé la valeur locative de l'ensemble du bâtiment VG1 selon la méthode proposée par l'administration en défense, conformément au deuxième alinéa de l'article 324 AC cité au point 3 ci-dessus, la cour a constaté que les impositions contestées avaient été initialement établies par l'administration sur la base d'une valeur locative supérieure à ce montant et a prononcé la réduction des cotisations foncières des entreprises dans la mesure de la réduction de la valeur locative ainsi définie. Ce faisant, la cour a implicitement mais nécessairement jugé que la valeur locative des locaux de la société BGL Avigros devait être calculée par l'administration en rapportant la valeur locative du bâtiment VG1 à la surface dont disposait la société pour les besoins de son activité. Par suite, le moyen tiré de ce qu'elle aurait commis une erreur de qualification juridique des faits ou les aurait dénaturés en estimant que la société exploitait l'ensemble du bâtiment " Volaille " et, par suite, commis une erreur de droit, doit être écarté. <br/>
<br/>
              6. En second lieu, le ministre soutient que la cour aurait insuffisamment motivé son arrêt et méconnu son office en se bornant à établir la valeur locative du bâtiment VG1 sans préciser les modalités d'affectation de la quote-part d'occupation des locaux par la société, alors qu'il soutenait que le taux d'occupation de 11,64 % proposé par la société ne pouvait être appliqué de manière linéaire sur la valeur de reconstruction. Toutefois, il ressort des pièces du dossier soumis aux juges du fond que cette dernière argumentation n'a été invoquée, après l'arrêt avant-dire droit mentionnée au point 1, qu'à l'appui du moyen par lequel le ministre a persisté, dans son mémoire enregistré le 29 juillet 2019, à contester, à titre principal, le recours à la méthode d'évaluation par voie d'appréciation directe à propos duquel la cour avait alors déjà épuisé sa compétence, n'a pas été reprise dans la réponse de l'administration au mémoire du 31 juillet 2019 dans lequel la société relevait qu'à titre subsidiaire l'administration ne contestait plus la valeur locative du bâtiment VG1 et demandait, à nouveau, que cette valeur locative ne lui soit affectée qu'à hauteur du taux d'occupation précité et doit, ainsi, être regardée comme ayant été abandonnée. Les moyens précités ne peuvent, dès lors, qu'être écartés.<br/>
<br/>
              7. Il résulte de tout ce qui précède, sans qu'il soit besoin de statuer sur la recevabilité du pourvoi, que le ministre de l'économie, des finances et de la relance n'est pas fondé à demander l'annulation de l'arrêt de la cour administrative d'appel de Paris. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 1 000 euros à verser à la société BGL Avigros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi du ministre de l'action et des comptes publics est rejeté.<br/>
Article 2 : L'Etat versera à la société BGL Avigros la somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée au ministre de l'économie, des finances et de la relance et à la société BGL Avigros. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
