<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026768116</ID>
<ANCIEN_ID>JG_L_2012_12_000000336553</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/76/81/CETATEXT000026768116.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 5ème sous-sections réunies, 12/12/2012, 336553</TITRE>
<DATE_DEC>2012-12-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>336553</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 5ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Olivier Talabardon</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:336553.20121212</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 11 février 2010 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre de l'éducation nationale, porte parole du gouvernement ; le ministre demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 0900538 du 17 décembre 2009 du tribunal administratif de Besançon en tant qu'il annule la disposition de la décision du 23 janvier 2009 du recteur de l'académie de Besançon refusant à M. Grégory A le bénéfice de l'indemnité de sujétions spéciales de remplacement à raison de l'accomplissement de son demi-temps de service au lycée Edouard Belin de Vesoul au titre de l'année scolaire 2008-2009 ;<br/>
<br/>
              2°) réglant l'affaire au fond dans cette mesure, de rejeter la demande de M. A ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 84-16 du 11 janvier 1984 ;<br/>
<br/>
              Vu le décret n° 89-825 du 9 novembre 1989 ;<br/>
<br/>
              Vu le décret n° 99-823 du 17 septembre 1999 ;<br/>
<br/>
              Vu le décret n° 2006-781 du 3 juillet 2006 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Talabardon, Maître des Requêtes en service extraordinaire, <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article 1er du décret du 9 novembre 1989 portant attribution d'une indemnité de sujétions spéciales de remplacement aux personnels assurant des remplacements dans le premier et le second degré : " Peuvent bénéficier d'une indemnité journalière de sujétions spéciales de remplacement pour les remplacements qui leur sont confiés (...) les personnels (...) qui sont nommés pour assurer (...) le remplacement des fonctionnaires appartenant aux corps enseignants, d'éducation ou d'orientation, conformément aux dispositions du décret du 30 septembre 1985 susvisé " ; qu'aux termes des deux premiers alinéas de l'article 2 du même décret : " L'indemnité prévue à l'article 1er (...) est due aux intéressés à partir de toute nouvelle affectation en remplacement, à un poste situé en dehors de leur école ou de leur établissement de rattachement. / Toutefois, l'affectation des intéressés au remplacement continu d'un même fonctionnaire pour toute la durée d'une année scolaire n'ouvre pas droit au versement de l'indemnité. " ; qu'aux termes de l'article 1er du décret du 17 septembre 1999 relatif à l'exercice des fonctions de remplacement dans les établissements d'enseignement du second degré, qui s'est substitué au décret du 30 septembre 1985 auquel se réfère le décret du 9 novembre 1989 : " Des personnels enseignants du second degré, des personnels d'éducation et d'orientation (...) peuvent être chargés (...) d'assurer le remplacement des agents momentanément absents ou d'occuper un poste provisoirement vacant. " ;<br/>
<br/>
              2. Considérant qu'il résulte de la combinaison de ces dispositions que la notion de remplacement, au sens du décret du 9 novembre 1989, doit s'entendre, non seulement de la suppléance d'un fonctionnaire momentanément absent, mais également de l'affectation sur un poste provisoirement vacant ; que, par suite, si l'affectation sur un poste provisoirement vacant doit être regardée comme un remplacement ouvrant en principe droit au bénéfice de l'indemnité de sujétions spéciales de remplacement - laquelle est, en vertu de l'article 5 de ce décret, " exclusive de l'attribution de toute autre indemnité et remboursement des frais de déplacement alloués au même titre " - ce bénéfice est exclu, en application du deuxième alinéa de l'article 2 du même décret, lorsque le remplacement s'effectue pour toute la durée de l'année scolaire, quand bien même l'affectation en cause ne porte pas sur un temps plein ; que le fonctionnaire qui assure un tel remplacement peut alors prétendre, non à l'indemnité prévue par le décret du 9 novembre 1989, mais, le cas échéant, à un défraiement sur le fondement du décret du 3 juillet 2006 fixant les conditions et les modalités de règlement des frais occasionnés par les déplacements temporaires des personnels civils de l'Etat ;<br/>
<br/>
              3. Considérant qu'il ressort du dossier soumis au juge du fond que M. A, conseiller principal d'éducation, était affecté, pour toute la durée de l'année scolaire 2008-2009, sur un poste à mi-temps vacant au lycée Edouard Belin de Vesoul ; qu'ainsi, en jugeant que M. A avait droit au bénéfice de l'indemnité de sujétions spéciales au titre du remplacement qu'il effectuait dans ce lycée, alors que ce remplacement courait pour la totalité de l'année scolaire 2008-2009, le tribunal administratif de Besançon a commis une erreur de droit ; que, par suite et sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, le ministre de l'éducation nationale est fondé à demander l'annulation du jugement attaqué, en tant qu'il annule sur ce point la décision du recteur ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler, dans cette mesure, l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui a été dit ci-dessus que, si M. A pouvait prétendre au remboursement des frais exposés pour se rendre de la commune où était située sa résidence administrative au lieu d'exercice de ses fonctions, son affectation au lycée Edouard Belin de Vesoul pour toute la durée de l'année scolaire n'ouvrait pas droit au versement de l'indemnité journalière de sujétions spéciales de remplacement instituée par le décret du 9 novembre 1989 ; que l'intéressé n'est dès lors pas fondé à demander l'annulation de la décision du 23 janvier 2009 du recteur de l'académie de Besançon en ce qu'elle lui a refusé le bénéfice de cette indemnité ; que, par suite, ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent qu'être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 1er et 2 du jugement du tribunal administratif de Besançon du 17 décembre 2009 sont annulés.<br/>
Article 2 : La demande présentée par M. A devant le tribunal administratif de Besançon tendant, d'une part, à l'annulation de la disposition de la décision du 23 janvier 2009 du recteur de l'académie de Besançon lui refusant le bénéfice de l'indemnité de sujétions spéciales de remplacement, d'autre part, à ce qu'une somme lui soit versée en application de l'article L. 761-1 du code de justice administrative, est rejetée.<br/>
Article 3 : La présente décision sera notifiée au ministre de l'éducation nationale et à M. Grégory A.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
