<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029601180</ID>
<ANCIEN_ID>JG_L_2014_10_000000368689</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/60/11/CETATEXT000029601180.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème SSJS, 01/10/2014, 368689, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-10-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>368689</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLEZ</AVOCATS>
<RAPPORTEUR>M. Florian Blazy</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:368689.20141001</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              M. B...A...a demandé à la Cour nationale du droit d'asile d'annuler la décision du 28 septembre 2011 par laquelle le directeur général de l'Office français de protection des réfugiés et apatrides (OFPRA) a rejeté sa demande d'asile.<br/>
<br/>
              Par une ordonnance n° 12025732 du 30 novembre 2012, la présidente de la Cour nationale du droit d'asile a rejeté sa demande comme irrecevable à raison de sa tardiveté. <br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 21 mai et 21 août 2013 au secrétariat du contentieux du Conseil d'Etat, M.A..., représenté par la SCP Boullez, demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 12025732 du 30 novembre 2012 de la présidente de la Cour nationale du droit d'asile ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'OFPRA la somme de 3 000 euros à verser à la SCP Boullez au titre des articles L. 761-1 du code de justice administrative et 37 de la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
<br/>
              Vu :<br/>
              -	les autres pièces du dossier ;<br/>
              -	le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              -	le décret n° 91-1266 du 19 décembre 1991 ;<br/>
              -	le code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Blazy, Maître des Requêtes,<br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boullez, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes des premier et deuxième alinéas de l'article L. 731-2 du code de l'entrée et du séjour des étrangers et du droit d'asile, dans leur rédaction applicable au litige : " La Cour nationale du droit d'asile statue sur les recours formés contre les décisions de l'Office français de protection des réfugiés et apatrides, prises en application des articles L. 711-1, L. 712-1 à L. 712-3 et L. 723-1 à L. 723-3. A peine d'irrecevabilité, ces recours doivent être exercés dans le délai d'un mois à compter de la notification de la décision de l'office. / Le bénéfice de l'aide juridictionnelle peut être demandé au plus tard dans le délai d'un mois à compter de la réception par le requérant de l'avis de réception de son recours, lequel l'informe dans une langue dont il est raisonnable de supposer qu'il la comprend des modalités de cette demande. ".<br/>
<br/>
              2. Selon l'article 38 du décret du 19 décembre 1991, lorsqu'une action en justice doit être intentée avant l'expiration d'un délai devant une juridiction du premier degré, " l'action est réputée avoir été intentée dans le délai si la demande d'aide juridictionnelle s'y rapportant est adressée au bureau d'aide juridictionnelle avant l'expiration dudit délai et si la demande en justice est introduite dans un nouveau délai de même durée à compter : / a) De la notification de la décision d'admission provisoire ; / b) De la notification de la décision constatant la caducité de la demande ; / c) De la date à laquelle la décision d'admission ou de rejet de la demande est devenue définitive ; / d) Ou, en cas d'admission, de la date, si elle est plus tardive, à laquelle un auxiliaire de justice a été désigné. ".<br/>
<br/>
              3. Il ressort des pièces de la procédure devant la Cour nationale du droit d'asile que la décision du directeur général de l'OFPRA rejetant la demande d'asile formée par M. B... A...a été notifiée à ce dernier le 7 octobre 2011 et qu'une demande d'aide juridictionnelle, valablement formée par le requérant, a été enregistrée par le bureau d'aide juridictionnelle de la Cour le 25 octobre 2011. En l'absence de toute décision du bureau d'aide juridictionnelle, M. A...a été conduit à réitérer sa demande d'aide juridictionnelle par un courrier du 26 juillet 2012.<br/>
<br/>
              4. En prenant en considération la date de réitération de la demande d'aide juridictionnelle de M. A...pour rejeter, à raison de sa tardiveté, sa demande d'annulation de la décision du directeur général de l'OFPRA, alors que sa demande d'aide juridictionnelle avait été régulièrement formée le 25 octobre 2011 et avait ainsi eu pour effet de proroger le délai du recours contentieux, la Cour nationale du droit d'asile a fondé sa décision sur des faits entachés d'une inexactitude matérielle . Par conséquent, pour ce motif et sans qu'il soit besoin de statuer sur l'autre moyen du pourvoi, M. A...est fondé à obtenir l'annulation de l'ordonnance qu'il attaque.<br/>
<br/>
              5. M. A...a obtenu le bénéfice de l'aide juridictionnelle. Par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Boullez, avocat de M.A..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'OFPRA la somme de 3 000 euros à verser à cette société.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 30 novembre 2012 de la présidente de la Cour nationale du droit d'asile est annulée.<br/>
Article 2 : L'affaire est renvoyée devant la Cour nationale du droit d'asile.<br/>
Article 3 : L'OFPRA versera à la SCP Boullez, avocat de M.A..., une somme de 1 500 euros en application des dispositions de l'article L. 761-1 du code de justice administrative et du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : La présente décision sera notifiée à M. B...A...et à l'Office français de protection des réfugiés et apatrides.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
