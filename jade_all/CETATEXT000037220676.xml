<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037220676</ID>
<ANCIEN_ID>JG_L_2018_07_000000405674</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/22/06/CETATEXT000037220676.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 18/07/2018, 405674, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-07-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>405674</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE ; SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>M. François Weil</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:405674.20180718</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Poitiers d'annuler la décision du 24 mai 2012 du maire de Saint-Georges-de-Didonne (Charente-Maritime) lui refusant un permis de construire modificatif, ainsi que la décision implicite ayant rejeté, le 25 septembre 2012, son recours gracieux. <br/>
<br/>
              Par un jugement n° 1202853 du 31 août 2015, le tribunal administratif de Poitiers a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 15BX03539 du 4 octobre 2016, la cour administrative d'appel de Bordeaux a rejeté l'appel formé par Mme A...contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 5 décembre 2016, 6 mars 2017 et 6 juillet 2018 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Saint-Georges-de-Didonne la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Weil, conseiller d'Etat,<br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat de MmeA..., et à la SCP Gaschignard, avocat de la commune de Saint-Georges-de-Didonne ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'il ressort des énonciations de l'arrêt attaqué que Mme A..., propriétaire d'un centre équestre situé sur le territoire de la commune de Saint-Georges-de-Didonne, a obtenu le 12 septembre 2007 un permis de construire en vue d'aménager ce centre et a présenté, le 15 février 2012, une demande de permis de construire modificatif ; que, par un arrêté en date du 24 mai 2012, le maire de la commune a refusé de délivrer le permis modificatif sollicité ; que Mme A...a demandé au tribunal administratif de Poitiers d'annuler cette décision ainsi que la décision implicite de rejet de son recours gracieux ; que, par un jugement du 31 août 2015, le tribunal administratif de Poitiers a rejeté sa demande ; que Mme A... se pourvoit en cassation contre l'arrêt du 4 octobre 2016 par lequel la cour administrative d'appel de Bordeaux a rejeté l'appel qu'elle avait formé contre ce jugement ; <br/>
<br/>
              2.	Considérant, en premier lieu, que la cour administrative d'appel, pour écarter le moyen soulevé par Mme A...et tiré de ce que le permis de construire délivré le 12 septembre 2007 aurait conféré des droits acquis insusceptibles d'être remis en cause, a retenu que les travaux objet du refus de permis modificatif contesté, en particulier l'aménagement d'un cheminement et d'une fumière, n'avaient pas été précédemment autorisés par un permis de construire ; qu'en statuant ainsi, en se fondant sur les éléments versés au dossier, la cour administrative d'appel s'est livrée, sans erreur de droit, à une appréciation souveraine des faits de l'espèce qui est exempte de dénaturation ; qu'elle a, ce faisant, nécessairement écarté l'argumentation tirée des dispositions de l'article L. 111-12 du code de l'urbanisme qui, à la date du refus de permis modificatif contesté, si elles faisaient obstacle à ce qu'un refus de permis puisse être fondé sur l'irrégularité d'une construction achevée depuis plus de dix ans, écartaient leur application lorsque la construction a été réalisée sans permis de construire ;<br/>
<br/>
              3.	Considérant, en deuxième lieu, que la cour administrative d'appel, pour écarter l'argumentation de Mme A...selon laquelle un doute existerait quant à l'implantation du projet dans un espace boisé à protéger, s'est fondée sur la configuration des lieux et les documents du plan local d'urbanisme de la commune, pour juger que la parcelle en cause était incluse dans un espace boisé et que l'intention des auteurs du plan n'avait pas été de l'exclure de la protection du bois dans laquelle elle se trouve ; qu'en statuant ainsi, en l'état des constatations de fait à laquelle elle a procédé dans le cadre de son pouvoir souverain d'appréciation, la cour ne s'est pas livrée à une interprétation inexacte des dispositions et documents du plan local d'urbanisme ; <br/>
<br/>
              4.	Considérant, en troisième lieu, que la cour, après avoir relevé que la partie du projet de Mme A...concernant la fumière, située en espace boisé classé, comprenait la construction d'un mur et la surélévation de deux autres et que les aménagements du chemin desservant la façade nord du bâtiment avaient pour effet de pérenniser son existence et d'isoler son emprise du couvert végétal, a jugé que ces aménagements, même s'ils ne supposent aucune coupe ou abattage d'arbres, constituaient un changement d'affectation du sol de nature à compromettre la conservation, la protection ou la création de boisements ; qu'en estimant ainsi que les aménagements pour lesquels le permis de construire modificatif avait été demandé seraient de nature à porter atteinte à la conservation de l'espace boisé classé dans lequel est incluse la parcelle de MmeA..., la cour s'est livrée à une appréciation souveraine des faits de l'espèce qui, exempte de dénaturation, échappe au contrôle du juge de cassation ;<br/>
<br/>
              5.	Considérant qu'il résulte de ce qui précède que Mme A...n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque ; que les dispositions de l'article L. 761-1 du code de justice administrative font, dès lors, obstacle à ce qu'une somme soit mise à la charge de la commune de Saint-Georges-de-Didonne, qui n'est pas la partie perdante dans la présente instance, au titre des frais exposés par Mme A...et non compris dans les dépens ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées au même titre par la commune de Saint-Georges-de-Didonne ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi de Mme A...est rejeté.<br/>
<br/>
Article 2 : Les conclusions présentées par la commune de Saint-Georges-de-Didonne au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à Mme B...A...et à la commune de Saint-Georges-de-Didonne. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
