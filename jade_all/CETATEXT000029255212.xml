<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029255212</ID>
<ANCIEN_ID>JG_L_2014_07_000000370311</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/25/52/CETATEXT000029255212.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  4ème sous-section jugeant seule, 16/07/2014, 370311, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-07-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>370311</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 4ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Benjamin de Maillard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:370311.20140716</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 18 juillet et 18 octobre 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour l'association " En toute franchise " du département des Bouches-du-Rhône, dont le siège est 1 rue François Boucher, à Marignane (13700) ; l'association " En toute franchise " du département des Bouches-du-Rhône demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision n° 1819 T du 6 juin 2013 par laquelle la Commission nationale d'aménagement commercial a accordé à la SAS Sodime l'autorisation préalable requise en vue de l'extension de 1 295 m² de la surface de vente d'un ensemble commercial " E. Leclerc " de 2 030 m² à Meyrargues (Bouches-du-Rhône), afin de porter sa surface de vente totale à 3 325 m² par l'extension de 1 365 m² d'un magasin généraliste à prédominance alimentaire " E. Leclerc " de 1 800 m², afin de porter sa surface de vente à 3 165 m², par la diminution de 70 m² de la surface de vente de la galerie marchande afin de ramener sa surface de vente à 160 m² ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat et de la SAS Sodime la somme de 3 000 euros chacun au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de commerce ;<br/>
<br/>
              Vu la loi n° 73-1193 du 27 décembre 1973 ;<br/>
<br/>
              Vu la loi n° 2008-776 du 4 août 2008 ;<br/>
<br/>
              Vu le décret n° 2008-1212 du 24 novembre 2008 ; <br/>
<br/>
              Vu l'arrêté du 21 août 2009 fixant le contenu de la demande d'autorisation d'exploitation de certains magasins de commerce de détail ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Benjamin de Maillard, auditeur,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de l'association " En toute franchise " département des Bouches-du-Rhône ;<br/>
<br/>
<br/>
<br/>
<br/>En ce qui concerne la procédure suivie devant la commission nationale : <br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier que les avis des ministres intéressés au sens de l'article R. 752-51 du code de commerce ont été signés par les personnes dûment habilitées à le faire ; que, dès lors, le moyen tiré de l'irrégularité de la consultation des ministres intéressés manque en fait ; <br/>
<br/>
              En ce qui concerne la motivation de la décision attaquée : <br/>
<br/>
              2. Considérant que la décision attaquée énonce de façon circonstanciée les éléments de fait et de droit sur lesquels elle se fonde ; qu'ainsi, le moyen tiré de ce qu'elle serait insuffisamment motivée doit être écarté ; <br/>
<br/>
              En ce qui concerne la délimitation de la zone de chalandise et la composition du dossier de demande : <br/>
<br/>
              3. Considérant que si la requérante soutient que la zone de chalandise du projet aurait été irrégulièrement délimitée, il ne ressort pas des pièces du dossier que cette zone, qui a été déterminée en tenant compte notamment du pouvoir d'attraction des équipements commerciaux existants et qui a été validée par les services instructeurs, serait erronée ;<br/>
<br/>
              4. Considérant que si la requérante soutient que le dossier de demande d'autorisation serait incomplet en ce qui concerne la description des pôles commerciaux situés hors de la zone de chalandise et l'appréciation des effets du projet au regard de l'objectif de développement durable, il ressort des pièces du dossier que la commission nationale disposait des éléments lui permettant d'apprécier la conformité du projet à l'ensemble des objectifs de l'article L. 752-6 du code de commerce ; que, si elle soutient également que l'impact du projet sur les flux de véhicules aurait été sous-estimé, son moyen n'est pas assorti des précisions suffisantes permettant d'en apprécier le bien-fondé ;<br/>
<br/>
              En ce qui concerne l'appréciation de la commission nationale :<br/>
<br/>
              5. Considérant qu'il appartient aux commissions d'aménagement commercial, lorsqu'elles se prononcent sur un projet d'exploitation commerciale soumis à autorisation en application de l'article L. 752-1 du code de commerce, d'apprécier la conformité de ce projet aux objectifs prévus à l'article 1er de la loi du 27 décembre 1973 et à l'article L. 750-1 du code de commerce, au vu des critères d'évaluation mentionnés à l'article L. 752-6 du même code ; que l'autorisation ne peut être refusée que si, eu égard à ses effets, le projet compromet la réalisation de ces objectifs ;<br/>
<br/>
              6. Considérant que le moyen tiré de ce que la commission nationale n'aurait pas été en mesure de bénéficier des informations suffisantes en raison des défaillances de l'observatoire départemental d'aménagement commercial des Bouches-du-Rhône est dépourvu des précisions permettant d'en apprécier le bien-fondé ; <br/>
<br/>
              7. Considérant que, si l'association requérante soutient que la décision attaquée compromettrait la réalisation de l'objectif d'aménagement du territoire en raison de l'impact du projet sur les commerces de proximité, l'animation de la vie urbaine du centre-ville de Meyrargues et les conditions de circulation et de stationnement, il ressort des pièces du dossier que le projet, qui est implanté au sein d'une zone d'activités et n'est distant du centre-ville que de 800 mètres, permettra de diversifier l'offre commerciale locale, notamment en matière de produits non alimentaires, et de diminuer ainsi l'évasion des clients de la zone de chalandise vers les autres pôles commerciaux de la région ; qu'en outre, les flux routiers générés par le projet auront un impact limité au regard des infrastructures routières existantes et de la capacité du parc de stationnement ; <br/>
<br/>
              8. Considérant que, si l'association requérante soutient que la décision attaquée méconnaît l'objectif fixé par le législateur en matière de développement durable au motif que le projet n'est pas desservi par les pistes cyclables, cette circonstance n'est pas, en l'espèce, de nature à entacher d'illégalité la décision attaquée ; qu'en outre, le projet est inséré dans les réseaux de transports collectifs et accessible par les piétons ; que, si la requérante soutient que la décision attaquée méconnaît le critère de la qualité environnementale, il ne ressort pas des pièces du dossier que le projet pourrait avoir un impact négatif sur l'environnement et les milieux naturels, alors que le pétitionnaire a prévu des dispositifs et des aménagements en vue, notamment, de réduire les consommations d'énergie, d'améliorer la gestion des déchets, de limiter les nuisances sonores et olfactives, et de traiter les eaux usées et les eaux de ruissellement ; <br/>
<br/>
              9. Considérant qu'il résulte de ce qui précède que la requérante n'est pas fondée à demander l'annulation de la décision attaquée ;<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              10. Considérant que ces dispositions font obstacle à ce que la somme que demande la requérante soit mise à la charge de la SAS Sodime, qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, en revanche, de faire application de ces mêmes dispositions et de mettre à la charge de la requérante la somme de 3 000 euros, à verser à la SAS Sodime ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La requête de l'association " En toute franchise " du département des Bouches-du-Rhône est rejetée. <br/>
<br/>
Article 2 : L'association " En toute franchise " du département des Bouches-du-Rhône versera à la SAS Sodime la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 3 : La présente décision sera notifiée à l'association " En toute franchise " du département des Bouches-du-Rhône et à la SAS Sodime.<br/>
Copie en sera adressée pour information à la Commission nationale d'aménagement commercial.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
