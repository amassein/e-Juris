<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041800413</ID>
<ANCIEN_ID>JG_L_2020_03_000000433228</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/80/04/CETATEXT000041800413.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 16/03/2020, 433228, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-03-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>433228</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Thibaut Félix</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie Sirinelli</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:433228.20200316</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés le 2 août 2019 et le 17 février 2020 au secrétariat du contentieux du Conseil d'Etat, la société Polytech Health et Aesthetics GmbH demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 2 avril 2019 par laquelle le directeur général de l'Agence nationale de sécurité du médicament et des produits de santé a interdit la mise sur le marché, la distribution, la publicité et l'utilisation d'implants mammaires à enveloppe macro-texturée et d'implants mammaires polyuréthane, et retiré ces produits du marché ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 10 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thibaut Félix, auditeur,  <br/>
<br/>
              - les conclusions de Mme Marie Sirinelli, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article R. 311-1 du code de justice administrative : " Le Conseil d'Etat est compétent pour connaître en premier et dernier ressort : / (...) / 2° Des recours dirigés contre les actes réglementaires des ministres et des autres autorités à compétence nationale (...) ".<br/>
<br/>
              2. Aux termes de l'article L. 5312-1 du code de la santé publique : " L'Agence nationale de sécurité du médicament et des produits de santé peut soumettre à des conditions particulières, restreindre ou suspendre les essais, la fabrication, la préparation, l'importation, l'exploitation, l'exportation, la distribution en gros, le conditionnement, la conservation, la mise sur le marché à titre gratuit ou onéreux, la détention en vue de la vente ou de la distribution à titre gratuit, la publicité, la mise en service, l'utilisation, la prescription, la délivrance ou l'administration d'un produit ou groupe de produits mentionné à l'article L. 5311-1, non soumis à une autorisation ou un enregistrement préalable à sa mise sur le marché, sa mise en service ou son utilisation, lorsque ce produit ou groupe de produits, soit présente ou est soupçonné de présenter, dans les conditions normales d'emploi ou dans des conditions raisonnablement prévisibles, un danger pour la santé humaine, soit est mis sur le marché, mis en service ou utilisé en infraction aux dispositions législatives ou réglementaires qui lui sont applicables. La suspension est prononcée, soit pour une durée n'excédant pas un an en cas de danger ou de suspicion de danger, soit jusqu'à la mise en conformité du produit ou groupe de produits en cas d'infraction aux dispositions législatives ou réglementaires. / L'agence peut interdire ces activités en cas de danger grave ou de suspicion de danger grave pour la santé humaine (...) ". Aux termes de l'article L. 5312-3 du même code : " Dans les cas mentionnés aux articles L. 5312-1 et L. 5312-2, (...) l'agence peut enjoindre [à] la personne physique ou morale responsable de la mise sur le marché, de la mise en service ou de l'utilisation de procéder au retrait du produit ou groupe de produits en tout lieu où il se trouve (...) ".<br/>
<br/>
              3. La société Polytech Health et Aesthetics GmbH doit être regardée comme demandant l'annulation pour excès de pouvoir, en tant que cette décision concerne les implants qu'elle fabrique, de la décision du 2 avril 2019 par laquelle, en application des dispositions citées au point 2, le directeur général de l'Agence nationale de sécurité du médicament et des produits de santé a interdit la mise sur le marché, la distribution, la publicité et l'utilisation de treize modèles d'implants mammaires, soit à enveloppe macro-texturée, soit recouverts de polyuréthane, produits par six fabricants, et leur a ordonné de procéder au retrait des implants.<br/>
<br/>
              4. La décision attaquée, qui n'interdit pas, de façon générale et impersonnelle, une ou plusieurs catégories d'implants, mais seulement certains modèles, précisément définis, d'implants mammaires fabriqués par la société requérante, ne revêt pas le caractère d'un acte réglementaire. Par suite, la décision attaquée n'entre pas dans le champ du 2° de l'article R. 311-1 du code de justice administrative qui donne compétence au Conseil d'Etat pour connaître en premier et dernier ressort des recours dirigés contre les actes réglementaires des ministres et des autres autorités à compétence nationale. Aucune autre disposition du code de justice administrative ne donne compétence au Conseil d'Etat pour connaître en premier et dernier ressort des conclusions de la société Polytech Health et Aesthetics GmbH tendant à l'annulation de cette décision. Il y a lieu, en application de l'article R. 351-1 du code de justice administrative, d'en attribuer le jugement au tribunal administratif de Montreuil, compétent pour en connaître en vertu de l'article R. 312-1 du même code, dès lors que la société requérante a son siège à l'étranger et qu'il ne ressort pas des pièces du dossier qu'elle disposerait d'un établissement en France.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement de la requête de la société Polytech Health et Aesthetics GmbH est attribué au tribunal administratif de Montreuil.<br/>
Article 2 : La présente décision sera notifiée à la société Polytech Health et Aesthetics GmbH et à l'Agence nationale de sécurité du médicament et des produits de santé.<br/>
Copie en sera adressée au ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
