<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032712837</ID>
<ANCIEN_ID>JG_L_2016_06_000000391903</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/71/28/CETATEXT000032712837.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 13/06/2016, 391903, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-06-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>391903</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD ; SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>M. Benjamin de Maillard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:391903.20160613</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Lyon d'annuler pour excès de pouvoir la décision du 27 octobre 2010 par laquelle le ministre du travail, de la solidarité et de la fonction publique a, d'une part, annulé la décision de l'inspecteur du travail du 19 avril 2010 refusant à la société Ecus l'autorisation de le licencier, d'autre part autorisé son licenciement. Par un jugement n° 1007989 du 15 octobre 2013, le tribunal administratif a fait droit à sa demande.<br/>
<br/>
              Par un arrêt n° 13LY03525 du 18 mai 2015, la cour administrative d'appel de Lyon a rejeté l'appel formé par la société Ecus contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 20 juillet et 21 octobre 2015 au secrétariat du contentieux du Conseil d'Etat, la société Ecus demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de M. A...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Benjamin de Maillard, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de la société Ecus et à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de M. A...;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 26 mai 2016, présentée par M.A... ; <br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société Ecus a demandé à l'inspecteur du travail de la 1ère section de l'unité territoriale de Charente l'autorisation de licencier pour faute M.A..., responsable de l'agence de la région Rhône-Alpes, qui exerçait le mandat de délégué du personnel titulaire ; que, par une décision du 19 avril 2010, l'inspecteur du travail a refusé d'autoriser ce licenciement ; que sur recours hiérarchique de l'employeur, le ministre chargé du travail a, par une décision du 27 octobre 2010, annulé la décision de l'inspecteur du travail et autorisé le licenciement pour faute de M. A... ; que la société Ecus demande l'annulation de l'arrêt du 18 mai 2015 par lequel la cour administrative d'appel de Lyon a rejeté son appel formé contre le jugement du tribunal administratif de Lyon annulant la décision du ministre chargé du travail ; <br/>
<br/>
              2. Considérant qu'en vertu des dispositions du code du travail, les salariés légalement investis de fonctions représentatives bénéficient, dans l'intérêt de l'ensemble des salariés qu'ils représentent, d'une protection exceptionnelle ; que, lorsque le licenciement d'un de ces salariés est envisagé, ce licenciement ne doit pas être en rapport avec les fonctions représentatives normalement exercées ou l'appartenance syndicale de l'intéressé ; que, dans le cas où la demande de licenciement est motivée par un comportement fautif, il appartient à l'inspecteur du travail, et le cas échéant au ministre, de rechercher, sous le contrôle du juge de l'excès de pouvoir, si les faits reprochés au salarié sont d'une gravité suffisante pour justifier son licenciement, compte tenu de l'ensemble des règles applicables au contrat de travail de l'intéressé et des exigences propres à l'exécution normale du mandat dont il est investi ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société Ecus a décidé de mettre en place une nouvelle stratégie de développement commercial en janvier 2006, qui mettait l'accent sur la nécessité de prospecter une nouvelle clientèle ; que M. A... a contesté à plusieurs reprises cette stratégie qu'il n'a jamais mise en oeuvre, s'opposant ainsi aux instructions de son employeur ; qu'en jugeant que les faits ainsi relevés n'étaient pas de nature à caractériser une faute d'une gravité suffisante pour justifier un licenciement, au motif que M. A...n'avait fait l'objet d'aucune sanction durant cette période et qu'il avait fait état auprès de sa hiérarchie des difficultés qu'il rencontrait, la cour administrative d'appel n'a pas exactement qualifié les faits qui lui étaient soumis ; que, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, la société Ecus est fondée à en demander l'annulation ; <br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              5. Considérant que, pour les raisons mentionnées au point 3 ci-dessus, la société Ecus est fondée à soutenir que c'est à tort que, pour annuler la décision du ministre chargé du travail du 27 octobre 2010, le tribunal administratif de Lyon s'est fondé sur ce que la faute reprochée à M. A...ne revêtait pas une gravité de nature à justifier son licenciement ; <br/>
<br/>
              6. Considérant, toutefois, qu'il appartient au Conseil d'Etat, saisi de l'ensemble du litige par l'effet dévolutif de l'appel, d'examiner les autres moyens soulevés par M. A...devant le tribunal administratif de Lyon ;<br/>
<br/>
              7. Considérant que la décision du ministre chargé du travail du 27 octobre 2010 autorisant le licenciement de M. A...est fondée sur le caractère fautif du refus persistant de l'intéressé de se conformer aux directives de son employeur ; que le ministre n'a relevé qu'à titre superfétatoire la circonstance que M.A..., en envoyant aux clients et fournisseurs de l'entreprise un message électronique critiquant le comportement de son employeur, avait manqué à son obligation de discrétion ; que, par suite, le moyen tiré de ce que le ministre ne pouvait légalement qualifier de fautif l'envoi de ce message est inopérant ;<br/>
<br/>
              8. Considérant qu'en se fondant, pour autoriser le licenciement de M. A..., sur les éléments produits au cours de l'enquête contradictoire, le ministre, qui a suffisamment motivé sa décision, n'a pas fait porter de charge de la preuve sur le salarié ; qu'il ne ressort pas, en tout état de cause, des pièces du dossier, que la demande de licenciement formulée par la société Ecus résulterait d'agissements de harcèlement moral ou revêtirait un caractère discriminatoire ; <br/>
<br/>
              9. Considérant qu'il résulte de tout ce qui précède que la société Ecus est fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Lyon a annulé la décision du 27 octobre 2010 du ministre chargé du travail ;<br/>
<br/>
              10. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société Ecus qui n'est pas, dans la présente instance, la partie perdante ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées au même titre par cette société ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon du 18 mai 2015 et le jugement du tribunal administratif de Lyon du 15 octobre 2013 sont annulés.<br/>
Article 2 : La demande présentée par M. A...devant le tribunal administratif de Lyon est rejetée.<br/>
Article 3 : Le surplus des conclusions de la société Ecus présenté sur le fondement des dispositions de l'article L.761- 1 du code de justice administrative est rejeté.<br/>
Article 4 : La présente décision sera notifiée à la société Ecus et à M. B...A....<br/>
Copie en sera adressée à la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
