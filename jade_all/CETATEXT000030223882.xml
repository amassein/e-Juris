<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030223882</ID>
<ANCIEN_ID>JG_L_2015_02_000000373101</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/22/38/CETATEXT000030223882.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  7ème sous-section jugeant seule, 11/02/2015, 373101, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-02-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>373101</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 7ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROGER, SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR>M. Arno Klarsfeld</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:373101.20150211</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 4 novembre 2013 et 3 février 2014 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B...A..., demeurant ...; M. A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 12BX01923 du 14 mars 2013 par lequel la cour administrative d'appel de Bordeaux a rejeté les conclusions de sa requête tendant à l'annulation du jugement n° 1103833 du 2 février 2012 par lequel le tribunal administratif de Toulouse a rejeté sa demande tendant à l'annulation de l'arrêté du 21 juillet 2011 du préfet de l'Aveyron portant obligation de quitter le territoire français dans un délai de trente jours et fixation du pays de destination ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement à la SCP Roger, Sevaux et Mathonnet de la somme de 3 000 euros au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le traité sur l'Union européenne ;<br/>
<br/>
              Vu la Charte des droits fondamentaux de l'Union européenne ;<br/>
<br/>
              Vu la directive 2008/115/CE du Parlement européen et du Conseil du 16 décembre 2008 ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu la loi n° 2011-672 du 16 juin 2011 ;<br/>
<br/>
              Vu l'arrêt rendu par la Cour de justice de l'Union européenne le 22 novembre 2012, dans l'affaire C 277/11 ;<br/>
<br/>
              Vu l'arrêt rendu par la Cour de justice de l'Union européenne le 10 septembre 2013, dans l'affaire C 383/13 PPU ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arno Klarsfeld, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Roger, Sevaux, Mathonnet, avocat de M. A...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que M.A..., ressortissant algérien, est entré régulièrement en France le 17 septembre 2002 pour y suivre ses études et a bénéficié à ce titre de cartes de séjour temporaires jusqu'au 16 septembre 2006 ; que, le 10 janvier 2008, il a sollicité le renouvellement de son titre de séjour, demande rejetée par le préfet de police de Paris ; que les requêtes en annulation dirigées contre cette décision introduites devant le tribunal administratif de Paris et la cour administrative d'appel de Paris ont également été rejetées ; qu'à la suite d'un contrôle d'identité effectué par les services de police, le préfet de l'Aveyron a pris à son encontre, le 21 juillet 2012, un arrêté portant refus de séjour, obligation de quitter le territoire français dans un délai de trente jours avec interdiction de retour pour une durée de deux ans et fixation du pays à destination duquel il est susceptible d'être reconduit ; que, par l'arrêt attaqué, la cour administrative d'appel de Bordeaux, après avoir annulé l'arrêté du 21 juillet 2011 du préfet de l'Aveyron portant interdiction de retour de M. A...sur le territoire français pour une durée de deux ans et réformé en conséquence le jugement du 2 février 2012 du tribunal administratif de Toulouse, a rejeté le surplus de ses conclusions ; que M. A...demande l'annulation de cet arrêt en tant qu'il a statué sur ses conclusions tendant à l'annulation du refus de délivrance d'un titre de séjour et de l'obligation de quitter le territoire français ;<br/>
<br/>
              2. Considérant qu'aux termes du I de l'article L. 511-1 du code de l'entrée et du séjour des étrangers et du droit d'asile : " L'autorité administrative peut obliger à quitter le territoire français un étranger non ressortissant d'un Etat membre de l'Union européenne, d'un autre Etat partie à l'accord sur l'Espace économique européen ou de la Confédération suisse et qui n'est pas membre de la famille d'un tel ressortissant au sens des 4° et 5° de l'article L. 121-1, lorsqu'il se trouve dans l'un des cas suivants : / 1° Si l'étranger ne peut justifier être entré régulièrement sur le territoire français, à moins qu'il ne soit titulaire d'un titre de séjour en cours de validité ; / 2° Si l'étranger s'est maintenu sur le territoire français au-delà de la durée de validité de son visa ou, s'il n'est pas soumis à l'obligation du visa, à l'expiration d'un délai de trois mois à compter de son entrée sur le territoire sans être titulaire d'un premier titre de séjour régulièrement délivré ; / 3° Si la délivrance ou le renouvellement d'un titre de séjour a été refusé à l'étranger ou si le titre de séjour qui lui avait été délivré lui a été retiré ; / 4° Si l'étranger n'a pas demandé le renouvellement de son titre de séjour temporaire et s'est maintenu sur le territoire français à l'expiration de ce titre ; / 5° Si le récépissé de la demande de carte de séjour ou l'autorisation provisoire de séjour qui avait été délivré à l'étranger lui a été retiré ou si le renouvellement de ces documents lui a été refusé. / La décision énonçant l'obligation de quitter le territoire français est motivée. Elle n'a pas à faire l'objet d'une motivation distincte de celle de la décision relative au séjour dans les cas prévus aux 3° et 5° du présent I, sans préjudice, le cas échéant, de l'indication des motifs pour lesquels il est fait application des II et III. / L'obligation de quitter le territoire français fixe le pays à destination duquel l'étranger est renvoyé en cas d'exécution d'office " ; que le II de l'article L. 511-1 prévoit que l'étranger dispose en principe d'un délai de trente jours pour satisfaire à l'obligation qui lui est faite de quitter le territoire français, ce délai pouvant toutefois être supprimé par décision de l'autorité administrative dans des cas limitativement énumérés ou être exceptionnellement prorogé eu égard à la situation personnelle de l'étranger ; qu'aux termes du deuxième alinéa de l'article L. 512-3 du même code : " L'obligation de quitter le territoire français ne peut faire l'objet d'une exécution d'office ni avant l'expiration du délai de départ volontaire ou, si aucun délai n'a été accordé, avant l'expiration d'un délai de quarante-huit heures suivant sa notification par voie administrative, ni avant que le tribunal administratif n'ait statué s'il a été saisi (...) " ;<br/>
<br/>
              3. Considérant, en premier lieu, que la cour n'avait pas à répondre au moyen non soulevé devant elle tiré de la méconnaissance des principes généraux du droit de l'Union européenne ; qu'elle n'a pas commis d'erreur de droit en jugeant que l'article 41 de la Charte des droits fondamentaux de l'Union européenne ne s'appliquait pas dans les relations entre autorités nationales et particuliers ;<br/>
<br/>
              4. Considérant, en deuxième lieu, que la cour n'a pas commis d'erreur de droit en jugeant que la prétendue irrégularité de l'interpellation de M. A...était sans incidence sur la légalité de la décision portant refus de titre de séjour et obligation de quitter le territoire français ;<br/>
<br/>
              5. Considérant, enfin, qu'en estimant que cette décision n'avait pas porté atteinte au droit de M. A...au respect de sa vie privée et familiale garanti par les stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, après avoir souverainement relevé que celui-ci était célibataire et sans charge de famille et ne démontrait pas être dépourvu d'attaches familiales dans son pays d'origine où résidait son père et où il avait lui-même vécu jusqu'à l'âge de vingt-trois ans, la cour administrative d'appel de Bordeaux n'a pas commis d'erreur de qualification juridique ;<br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que le pourvoi de M. A...doit être rejeté, y compris ses conclusions tendant à ce que soit mise à la charge de l'Etat le versement à la SCP Roger, Sevaux et Mathonnet, de la somme de 3 000 euros au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. B...A...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
