<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029751388</ID>
<ANCIEN_ID>JG_L_2014_11_000000368876</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/75/13/CETATEXT000029751388.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 12/11/2014, 368876</TITRE>
<DATE_DEC>2014-11-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>368876</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLEZ ; FOUSSARD</AVOCATS>
<RAPPORTEUR>Mme Marie Grosset</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:368876.20141112</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              La société Centre de gestion pour expatriés et entreprises (CGEE) a demandé au tribunal administratif de Paris de la décharger de la somme de 71 111,66 euros correspondant à la prise en charge des frais d'hospitalisation de M. D...à l'hôpital Necker du 3 décembre 2005 au 15 janvier 2006, qui lui a été réclamée par un commandement de payer du 30 septembre 2008 du trésorier-payeur général de l'Assistance publique - Hôpitaux de Paris (AP-HP) et par plusieurs titres exécutoires. Par un jugement n° 0906773 du 14 octobre 2011, le tribunal administratif de Paris a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 11PA05234 du 25 mars 2013, la cour administrative d'appel de Paris a rejeté l'appel formé par la société CGEE contre le jugement du tribunal administratif de Paris du 14 octobre 2011.<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi, un mémoire complémentaire et un nouveau mémoire, enregistrés les 27 mai 2013, 27 août et 9 septembre 2013 au secrétariat du contentieux du Conseil d'Etat, Mme C...B..., agissant en qualité de liquidateur de la société CGEE, demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt n° 11PA05234 de la cour administrative d'appel de Paris du 25 mars 2013 ;<br/>
<br/>
              2°) de lui allouer la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - le code civil ;<br/>
              - le code de commerce ;<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Grosset, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boullez, avocat de MmeB..., es qualité de liquidateur de la société CGEE et à Me Foussard, avocat de l'Assistance publique - Hôpitaux de Paris.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'à la suite de l'hospitalisation de M. A...D..., ressortissant français résidant aux Etats-Unis, à l'hôpital Necker du 3 décembre 2005 au 15 janvier 2006, date de son décès, le trésorier-payeur général de l'Assistance publique - Hôpitaux de Paris a émis à l'encontre de la société Centre de gestion pour expatriés et entreprises (CGEE) un commandement de payer d'un montant de 71 111,66 euros ; <br/>
<br/>
              2. Considérant qu'aux termes du premier alinéa de l'article L. 6145-11 du code de la santé publique : " Les établissements publics de santé peuvent toujours exercer leurs recours, s'il y a lieu, contre les hospitalisés, contre leurs débiteurs et contre les personnes désignées par les articles 205, 206, 207 et 212 du code civil " ; qu'aux termes de l'article R. 6145-5 du même code, transféré à l'article R. 6145-4 à compter du 1er janvier 2006 : " Dans le cas où les frais de séjour des malades ne sont pas susceptibles d'être pris en charge soit par les services de l'aide médicale, soit par un organisme d'assurance maladie, soit par le ministère des anciens combattants et victimes de guerre ou par tout autre organisme public, les intéressés ou, à défaut, leur famille ou un tiers responsable souscrivent un engagement d'acquitter les frais de toute nature afférents au régime choisi. Ils sont tenus, sauf dans les cas d'urgence, de verser au moment de leur entrée dans l'établissement une provision renouvelable calculée sur la base de la durée estimée du séjour. En cas de sortie avant l'expiration du délai prévu, la fraction dépassant le nombre de jours de présence est restituée " ; <br/>
<br/>
              3. Considérant, en premier lieu, que s'il résulte des dispositions de l'article R. 6145-4 du code de la santé publique qu'un établissement public de santé ne peut exiger un engagement d'acquitter les frais de séjour d'un patient qu'à défaut de prise en charge de ces frais par les organismes d'assurance maladie ou par des organismes publics, ces dispositions n'ont ni pour objet ni pour effet de mettre à la charge de l'établissement ou de son comptable une obligation de rechercher, lorsqu'un tel engagement a été souscrit, s'il existe un organisme d'assurance maladie ou un organisme public susceptible de prendre en charge la créance ; que, par suite, la requérante n'est, en tout état de cause, pas fondée à soutenir que la cour aurait commis une erreur de droit en s'abstenant de rechercher si le trésorier-payeur général n'aurait pas dû réclamer la somme litigieuse à la Caisse des Français de l'étranger ; <br/>
<br/>
              4. Considérant, en second lieu, qu'il résulte des dispositions de l'article L. 6145-11 du code de la santé publique qu'un établissement public de santé ne peut légalement émettre un ordre de recettes ou un état exécutoire à l'encontre d'une personne prise en sa seule qualité de signataire de l'engagement prévu par les dispositions de l'article R. 6145-4 du même code, alors qu'elle ne serait pas au nombre des personnes pouvant être légalement déclarées débitrices sur le fondement de l'article L. 6145-11 ; que, toutefois, il ressort des pièces du dossier soumis aux juges du fond que la société CGEE gérait le contrat d'assurance santé souscrit par M. D... auprès de la compagnie d'assurance Artas et que c'est à ce titre qu'elle s'est engagée, le 6 décembre 2005 et les 6 et 9 janvier 2006, par trois documents portant la mention " attestations de prise en charge ", à acquitter les frais de séjour de M. D...à l'hôpital Necker ; que la société CGEE pouvait ainsi être regardée comme un débiteur au sens de l'article L. 6145-11 du code de la santé publique, alors même que son activité de gestionnaire de la couverture sociale des expatriés aurait seulement consisté à encaisser pour le compte de la compagnie d'assurance les cotisations correspondant aux  prestations souscrites et à reverser les sommes payées par l'assureur aux établissements de soins ; que, par suite, c'est sans commettre d'erreur de droit que la cour administrative d'appel de Paris, qui a suffisamment motivé son arrêt et ne s'est pas méprise sur la portée des écritures dont elle était saisie, a jugé que le trésorier-payeur général de l'Assistance publique - Hôpitaux de Paris avait pu légalement mettre à la charge de cette société les frais d'hospitalisation de M. D...qu'elle s'était engagée à acquitter ; <br/>
<br/>
              5. Considérant qu'il résulte de tout ce qui précède que la requérante n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque ; <br/>
<br/>
              6. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge MmeB..., agissant en qualité de liquidateur de la société CGEE, la somme que l'Assistance publique - Hôpitaux de Paris demande au titre de l'article L. 761-1 du code de justice administrative ; que ces dispositions font, en tout état de cause, obstacle à ce qu'il soit fait droit aux conclusions présentées par Mme B...au même titre ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de MmeB..., agissant en qualité de liquidateur de la société CGEE, est rejeté.<br/>
Article 2 : Les conclusions de l'Assistance publique - Hôpitaux de Paris présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à Mme C...B..., agissant en qualité de liquidateur de la société Centre de gestion pour expatriés et entreprises, et à l'Assistance publique - Hôpitaux de Paris.<br/>
Copie en sera adressée pour information au ministre des finances et des comptes publics, à la société Assur Travel et à la société Artas.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">18-03-01 COMPTABILITÉ PUBLIQUE ET BUDGET. CRÉANCES DES COLLECTIVITÉS PUBLIQUES. EXISTENCE. - ENGAGEMENT D'ACQUITTER LES FRAIS AFFÉRENTS À L'HOSPITALISATION DANS UN ÉTABLISSEMENT PUBLIC DE SANTÉ (ARTICLE R. 6145-4 DU CSP) - POSSIBILITÉ D'ÉMETTRE UN ORDRE DE RECETTES OU UN ÉTAT EXÉCUTOIRE À L'ENCONTRE D'UNE PERSONNE PRISE EN SA SEULE QUALITÉ DE SIGNATAIRE DE CET ENGAGEMENT, QUI NE SERAIT PAS AU NOMBRE DES DÉBITEURS DES FRAIS D'HOSPITALISATION DÉSIGNÉS PAR L'ARTICLE L.6145-11 DU CSP  - ABSENCE EN PRINCIPE [RJ1] - EXISTENCE EN L'ESPÈCE (CAS D'UNE SOCIÉTÉ GESTIONNAIRE DE CONTRATS D'ASSURANCE SANTÉ POUR LE COMPTE DE L'ASSUREUR).
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">61-06 SANTÉ PUBLIQUE. ÉTABLISSEMENTS PUBLICS DE SANTÉ. - ENGAGEMENT D'ACQUITTER LES FRAIS AFFÉRENTS À L'HOSPITALISATION DANS UN ÉTABLISSEMENT PUBLIC DE SANTÉ (ARTICLE R. 6145-4 DU CSP) - POSSIBILITÉ D'ÉMETTRE UN ORDRE DE RECETTES OU UN ÉTAT EXÉCUTOIRE À L'ENCONTRE D'UNE PERSONNE PRISE EN SA SEULE QUALITÉ DE SIGNATAIRE DE CET ENGAGEMENT, QUI NE SERAIT PAS AU NOMBRE DES DÉBITEURS DES FRAIS D'HOSPITALISATION DÉSIGNÉS PAR L'ARTICLE L.6145-11 DU CSP  - ABSENCE EN PRINCIPE [RJ1] - EXISTENCE EN L'ESPÈCE (CAS D'UNE SOCIÉTÉ GESTIONNAIRE DE CONTRATS D'ASSURANCE SANTÉ POUR LE COMPTE DE L'ASSUREUR).
</SCT>
<ANA ID="9A"> 18-03-01 Il résulte des dispositions de l'article L. 6145-11 du code de la santé publique (CSP) qu'un établissement public de santé ne peut légalement émettre un ordre de recettes ou un état exécutoire à l'encontre d'une personne prise en sa seule qualité de signataire de l'engagement prévu par les dispositions de l'article R. 6145-4 du même code, alors qu'elle ne serait pas au nombre des personnes pouvant être légalement déclarées débitrices sur le fondement de l'article L. 6145-11.,,,Cas d'une société gestionnaire, pour le compte d'un assureur, de contrats d'assurance santé ayant contracté l'engagement prévu par l'article R. 6145-4. Cette société peut, dans les circonstances de l'espèce, être regardée comme un débiteur au sens de l'article L. 6145-11 du CSP, alors même que son activité de gestionnaire de la couverture sociale des expatriés aurait seulement consisté à encaisser pour le compte de la compagnie d'assurance les cotisations correspondant aux  prestations souscrites et à reverser les sommes payées par l'assureur aux établissements de soins.</ANA>
<ANA ID="9B"> 61-06 Il résulte des dispositions de l'article L. 6145-11 du code de la santé publique (CSP) qu'un établissement public de santé ne peut légalement émettre un ordre de recettes ou un état exécutoire à l'encontre d'une personne prise en sa seule qualité de signataire de l'engagement prévu par les dispositions de l'article R. 6145-4 du même code, alors qu'elle ne serait pas au nombre des personnes pouvant être légalement déclarées débitrices sur le fondement de l'article L. 6145-11.,,,Cas d'une société gestionnaire, pour le compte d'un assureur, de contrats d'assurance santé ayant contracté l'engagement prévu par l'article R. 6145-4. Cette société peut, dans les circonstances de l'espèce, être regardée comme un débiteur au sens de l'article L. 6145-11 du CSP, alors même que son activité de gestionnaire de la couverture sociale des expatriés aurait seulement consisté à encaisser pour le compte de la compagnie d'assurance les cotisations correspondant aux  prestations souscrites et à reverser les sommes payées par l'assureur aux établissements de soins.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, avis, 28 juillet 1995, Kilou, n° 168438, p. 315.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
