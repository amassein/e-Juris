<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042040540</ID>
<ANCIEN_ID>JG_L_2020_06_000000428003</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/04/05/CETATEXT000042040540.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 12/06/2020, 428003, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-06-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428003</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>Mme Myriam Benlolo Carabot</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Anne Iljic</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:428003.20200612</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. D... C... et l'association syndicale autorisée du parc de Santa Lucia ont demandé au tribunal administratif de Toulon d'annuler pour excès de pouvoir l'arrêté du 13 septembre 2016 par lequel le maire de Saint-Raphaël a délivré à M. B... A... un permis de démolir une maison et de construire un bâtiment à usage d'habitation sur un terrain situé 208, allée du Temple d'Amour. Par un jugement n° 1700573 du 13 décembre 2018, le tribunal administratif de Toulon a fait droit à leur demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 14 février, 15 mai 2019 et 18 mai 2020 au secrétariat du contentieux du Conseil d'Etat, la commune de Saint-Raphaël demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de mettre à la charge de M. C... et de l'association syndicale autorisée du parc de Santa Lucia la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 modifiée ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Myriam Benlolo Carabot, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Anne Iljic, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de la commune de Saint-Raphaël et à la SCP Waquet, Farge, Hazan, avocat de l'association syndicale du parc Santa Lucia ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 600-4-1 du code de l'urbanisme : " Lorsqu'elle annule pour excès de pouvoir un acte intervenu en matière d'urbanisme ou en ordonne la suspension, la juridiction administrative se prononce sur l'ensemble des moyens de la requête qu'elle estime susceptibles de fonder l'annulation ou la suspension, en l'état du dossier ".<br/>
<br/>
              2. Saisi d'un pourvoi dirigé contre une décision juridictionnelle reposant sur plusieurs motifs dont l'un est erroné, le juge de cassation, à qui il n'appartient pas de rechercher si la juridiction aurait pris la même décision en se fondant uniquement sur les autres motifs, doit, hormis le cas où ce motif erroné présenterait un caractère surabondant, accueillir le pourvoi. Il en va cependant autrement lorsque la décision juridictionnelle attaquée prononce l'annulation pour excès de pouvoir d'un acte administratif, dans la mesure où l'un quelconque des moyens retenus par le juge du fond peut suffire alors à justifier son dispositif d'annulation. En pareille hypothèse - et sous réserve du cas où la décision qui lui est déférée aurait été rendue dans des conditions irrégulières - il appartient au juge de cassation, si l'un des moyens reconnus comme fondés par cette décision en justifie légalement le dispositif, de rejeter le pourvoi, après avoir, en raison de l'autorité de chose jugée qui s'attache aux motifs constituant le soutien nécessaire du dispositif de la décision juridictionnelle déférée, censuré celui ou ceux de ces motifs qui étaient erronés. <br/>
<br/>
              3. Il ressort des énonciations du jugement attaqué que le tribunal administratif de Toulon a annulé le permis de construire délivré le 13 septembre 2016 à M. A... en se fondant sur la méconnaissance des articles UC3 et UC6 du règlement du plan local d'urbanisme de Saint-Raphaël.<br/>
<br/>
              4. En premier lieu, l'article UC3 du règlement de ce plan local d'urbanisme dispose que: " Les voies nouvelles ou existantes (publiques ou privées) ouvertes à la circulation automobile et devant desservir tous nouveaux projets devront disposer des largeurs de chaussée (bande roulement) suivantes : (...) 5m pour les constructions dont la SHON est comprise entre 300 et 800 m2. / (...) Dans le cas d'une organisation de circulation avec des voies en sens unique, ces minimas sont ramenés à 4 mètres pour les constructions dont la SHON est inférieure ou égale à 800 m2 et 5 mètres pour les constructions dont la SHON est supérieure à 800m2 ". <br/>
<br/>
              5. Il ressort des pièces du dossier soumis aux juges du fond qu'un véhicule ne peut accéder au terrain d'assiette du projet de construction depuis la route principale qu'en empruntant successivement l'allée du château, l'allée du temple d'Amour, l'allée de la pointe des Moines et enfin l'allée de l'observatoire, cette dernière étant à sens unique. D'une part, c'est sans erreur de droit que le tribunal administratif a recherché si ces trois voies respectaient une largeur de chaussée d'au moins 5 mètres conformément aux prescriptions de l'article UC3 précité. D'autre part, si le tribunal administratif a relevé que l'allée du château, l'allée du temple d'Amour et l'allée de la pointe des Moines n'étaient pas à double sens, cette erreur de plume est demeurée sans incidence sur la solution qu'il a retenue. <br/>
<br/>
              6. En second lieu, l'article UC 6 du règlement du plan local d'urbanisme relatif à " l'implantation des constructions par rapport aux voies et emprises publiques " dispose que : " toute construction doit être implantée à une distance au moins égale à : (...) 5 mètres de l'alignement des voies dans le secteur Ucb ".<br/>
<br/>
              7. Il ressort des énonciations du jugement attaqué que, pour accueillir le moyen tiré de la méconnaissance de ces dispositions, le tribunal administratif s'est fondé sur le constat de l'implantation de constructions sur l'alignement de l'allée du temple d'Amour. En statuant ainsi alors que cette voie n'est pas une voie publique mais une voie privée à laquelle, en l'absence de dispositions le mentionnant expressément, l'article UC 6 ne s'applique pas, le tribunal administratif a commis une erreur de droit. <br/>
<br/>
              8. Toutefois, le motif tiré de la méconnaissance de l'article UC3 du règlement du plan local d'urbanisme suffit à justifier légalement l'annulation pour excès de pouvoir du permis de construire du 13 septembre 2016 prononcée par le tribunal. Il s'ensuit que la commune de Saint-Raphaël n'est pas fondée à demander l'annulation du jugement qu'elle attaque. <br/>
<br/>
              9. Il résulte de ce qui précède que le pourvoi de la commune de Saint-Raphaël doit être rejeté, y compris les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées au même titre par l'association syndicale autorisée du parc de Santa Lucia.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                            --------------<br/>
<br/>
Article 1er : Le pourvoi de la commune de Saint-Raphaël est rejeté. <br/>
<br/>
Article 2 : Les conclusions de l'association syndicale autorisée du parc de Santa Lucia présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la commune de Saint-Raphaël, à M. D... C... et à l'association syndicale autorisée du parc de Santa Lucia.<br/>
Copie en sera adressée à M. B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
