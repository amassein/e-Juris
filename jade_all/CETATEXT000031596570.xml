<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031596570</ID>
<ANCIEN_ID>JG_L_2015_12_000000367897</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/59/65/CETATEXT000031596570.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème / 3ème SSR, 09/12/2015, 367897, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-12-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>367897</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème / 3ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP TIFFREAU, MARLANGE, DE LA BURGADE</AVOCATS>
<RAPPORTEUR>Mme Esther de Moustier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:367897.20151209</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et les mémoires complémentaires, enregistrés les 19 avril et 16 juillet 2013 et le 10 avril 2014 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société Property Investment Holding France, dont le siège est 4, rue de Ventadour, à Paris (75001) ; la société demande au Conseil d'Etat : <br/>
<br/>
               1°) d'annuler l'arrêt n° 12PA00386 du 5 février 2013 par lequel la cour administrative d'appel de Paris a rejeté sa requête tendant à l'annulation du jugement n° 0911962 du 25 octobre 2011 du tribunal administratif de Paris rejetant sa demande tendant à la décharge des cotisations supplémentaires d'impôt sur les sociétés et de contribution additionnelle à cet impôt auxquelles elle a été assujettie au titre des exercices 2002, 2003, 2004 et 2005 ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative ;   <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Esther de Moustier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Tiffreau, Marlange, de la Burgade, avocat de la société Property Investment Holding France ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société holding immobilière de droit français Property Investment Holding France (PIH France) a fait l'objet d'une vérification de comptabilité au titre des années 2002 à 2005 à l'issue de laquelle l'administration a remis en cause la déductibilité des honoraires versés par celle-ci à la société de droit néerlandais Property Investment Holding BV (PIH BV), qui détient, indirectement, une part prépondérante de son capital, au motif que les prestations fournies par la société PIH BV en contrepartie de ces honoraires n'étaient pas utiles à la société française et que le paiement de ces honoraires correspondait en conséquence à un transfert indirect de bénéfices à l'étranger ; que des cotisations supplémentaires d'impôt sur les sociétés et de contribution additionnelle à cet impôt ont, par suite, été mises à la charge de la société PIH France au titre des années 2002, 2003, 2004 et 2005 ; que celle-ci se pourvoit en cassation contre l'arrêt du 5 février 2013 par lequel la cour administrative d'appel de Paris, confirmant un jugement du 25 octobre 2011 du tribunal administratif de Paris, a rejeté sa demande tendant à la décharge de ces impositions ; <br/>
<br/>
              2. Considérant qu'aux termes du premier alinéa de l'article 57 du code général des impôts, applicable en matière d'impôt sur les sociétés en vertu de l'article 209 du même code : " Pour l'établissement de l'impôt sur le revenu dû par les entreprises qui sont sous la dépendance ou qui possèdent le contrôle d'entreprises situées hors de France, les bénéfices indirectement transférés à ces dernières, soit par voie de majoration ou de diminution des prix d'achat ou de vente, soit par tout autre moyen, sont incorporés aux résultats accusés par les comptabilités (...) " ; que ces dispositions instituent, dès lors que l'administration établit l'existence d'un lien de dépendance et d'une pratique entrant dans leurs prévisions, une présomption de transfert indirect de bénéfices qui ne peut utilement être combattue par l'entreprise imposable en France que si celle-ci apporte la preuve que les avantages qu'elle a consentis ont été justifiés par l'obtention de contreparties ;<br/>
<br/>
              3. Considérant que la cour a relevé que la société PIH France avait conclu le 23 novembre 2002 avec la société PIH BV une convention d'assistance portant, en contrepartie du versement annuel d'honoraires de 200 000 euros hors taxe, sur la fourniture par cette dernière à la société PIH France de prestations, d'une part, d'assistance au développement, couvrant la recherche de nouveaux investissements, l'étude des investissements et la recherche du financement correspondant, d'autre part, d'assistance administrative et financière, relatives aux relations avec les banques des sociétés du groupe dont la société PIH France est la mère, au contrôle et à la gestion de la trésorerie de ce groupe, à la supervision et à l'assistance des services comptables et administratifs du groupe, à la rédaction et au suivi des procédures de consolidation, à la revue et au contrôle des liasses de consolidation, à l'arrêté des principes comptables du groupe et aux relations avec les commissaires aux comptes du groupe ; qu'elle a également relevé que la société de droit britannique EPIC facturait à la société PIH BV, au titre des prestations de stratégie d'investissement, les dépenses effectuées notamment par son dirigeant, que la société Larix facturait à la société PIH BV en vertu d'une convention de " management agreement " du 18 octobre 2000 des prestations de nature administrative, juridique, comptable et financière et qu'il n'était pas contesté que ces prestations étaient refacturées à la société française par la société PIH BV ; <br/>
<br/>
              4. Considérant qu'après avoir relevé, en premier lieu, que l'administration établissait une présomption de transferts de bénéfice puis, en second lieu, que la société avait recours à des prestataires extérieurs pour la tenue de la comptabilité et le conseil juridique, la cour a pu estimer que les pièces produites ne permettaient pas de regarder les prestations réalisées par la société-mère de la société Larix, la société Vistra, qui consistaient à assurer le développement du groupe dans différents pays, dont la France, à optimiser la gestion de la trésorerie du groupe, à coordonner l'activité des filiales et à contrôler la tenue de la comptabilité, comme des prestations réalisées dans le seul intérêt de l'entreprise ; <br/>
<br/>
              5. Considérant, en revanche, qu'il ressort des pièces du dossier soumis aux juges du fond que les prestations facturées à la société PIH France, qui présentaient un intérêt pour son exploitation propre, ne pouvaient être réalisées ni par son unique salariée, employée à temps partiel pour assurer essentiellement des fonctions de représentation, ni par les prestataires extérieurs, la société Saggel Gestion, le cabinet NSK et MeA..., auxquels étaient confiées des prestations distinctes de celles visées par la convention d'assistance ; qu'en outre, la société requérante faisait valoir que la société EPIC fournissait des prestations pour le choix des investissements, des travaux à effectuer et le moment où vendre les actifs immobiliers ; que, par suite, en estimant au seul motif que la société requérante ne produisait pas la convention conclue entre la société PIH BV et la société EPIC définissant les prestations réalisées par celle-ci, que cette dernière ne réalisait pas de prestations d'assistance au profit de la société française mais en contrôlait les décisions stratégiques, et en en déduisant qu'il n'était pas établi que ces prestations étaient réalisées dans l'intérêt de l'exploitation de la société PIH France et que le paiement par cette dernière des honoraires en litige l'avait ainsi conduite à prendre en charge les frais du contrôle assuré sur sa propre gestion par sa société mère néerlandaise, la cour a entaché de dénaturation l'appréciation qu'elle a portée sur les faits de l'espèce ;<br/>
<br/>
              6. Considérant qu'en l'absence d'éléments permettant de distinguer la fraction des honoraires litigieux correspondant aux prestations assurées par la société EPIC de ceux assurés par la société Larix, il y a lieu sans qu'il soit besoin d'examiner les autres moyens du pourvoi, d'annuler l'arrêt attaqué dans sa totalité ; <br/>
<br/>
              7. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la société PIH France au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>                  D E C I D E :<br/>
                                --------------<br/>
<br/>
Article 1er   : L'arrêt de la cour administrative d'appel de Paris du 5 février 2013 est annulé. <br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br/>
<br/>
Article 3 : L'Etat versera une somme de 3 000 euros à la société Property Investment Holding France au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la société Property Investment Holding France et  au ministre des finances et des comptes publics.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
