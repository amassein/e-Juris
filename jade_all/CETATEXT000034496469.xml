<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034496469</ID>
<ANCIEN_ID>JG_L_2017_04_000000409797</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/49/64/CETATEXT000034496469.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 20/04/2017, 409797, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-04-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>409797</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2017:409797.20170420</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme B...a demandé au juge des référés du tribunal administratif de Lyon, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre au préfet du Rhône de lui proposer un hébergement adapté, dans un délai de vingt quatre heures à compter de la notification de l'ordonnance. Par une ordonnance n° 1702770 du 10 avril 2017, le juge des référés du tribunal administratif de Lyon a rejeté sa demande. <br/>
<br/>
              Par une requête, enregistrée le 14 avril 2017 au secrétariat du contentieux du Conseil d'Etat, Mme B...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement à la SCP Masse-Dessen, Thouvenin et Coudray d'une somme de 3 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Elle soutient que : <br/>
              - l'ordonnance est entachée d'un vice de forme en ce que la minute de la décision n'a pas été signée, en méconnaissance de l'article R. 742-5 du code de justice administrative ;<br/>
              - le juge des référés du tribunal administratif de Lyon a commis une erreur de droit en retenant, pour évaluer la gravité de l'atteinte portée à ses libertés, comme opérant le moyen tiré de ce qu'elle n'aurait pas cherché à organiser son retour ; <br/>
              - il n'a pas pris en compte l'intégralité des éléments pertinents et notamment la question de savoir si sa fille pourrait bénéficier des soins nécessaires une fois de retour en Arménie alors que les pièces du dossier insistaient sur son besoin de vivre dans des conditions de salubrité parfaite ;<br/>
              - en s'abstenant de lui avoir proposé un hébergement répondant à certaines conditions de salubrité, le préfet du Rhône a méconnu les dispositions des articles L. 345-2-2 et L. 345-2-3 du code de l'action sociale et des familles, qui répondent aux exigences posées par l'article 3 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et des articles 1, 3 et 4 de la charte des droits fondamentaux de l'Union européenne.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - la charte des droits fondamentaux de l'Union européenne ;<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures " ; qu'en vertu de l'article L. 522-3 du même code, le juge des référés peut rejeter une requête par une ordonnance motivée, sans instruction contradictoire ni audience publique, lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée ; qu'il appartient au juge d'appel de prendre en considération les éléments recueillis par le juge du premier degré dans le cadre de la procédure écrite et orale qu'il a diligentée ;<br/>
<br/>
              2. Considérant, en premier lieu, que la minute de l'ordonnance attaquée a été signée par le magistrat qui l'a rendue, conformément à ce que prévoit l'article R. 742-5 du code de justice administrative ; <br/>
<br/>
              3. Considérant, en second lieu, qu'il appartient aux autorités de l'Etat, sur le fondement du code de l'action sociale et des familles, de mettre en oeuvre le droit à l'hébergement d'urgence reconnu par la loi à toute personne sans abri qui se trouve en situation de détresse médicale, psychique ou sociale ; qu'une carence caractérisée dans l'accomplissement de cette mission peut faire apparaître, pour l'application de l'article L. 521-2 du code de justice administrative, une atteinte grave et manifestement illégale à une liberté fondamentale lorsqu'elle entraîne des conséquences graves pour la personne intéressée ; qu'il incombe au juge des référés d'apprécier dans chaque cas les diligences accomplies par l'administration en tenant compte des moyens dont elle dispose ainsi que de l'âge, de l'état de la santé et de la situation de famille de la personne intéressée ; que les ressortissants étrangers qui font l'objet d'une obligation de quitter le territoire français ou dont la demande d'asile a été définitivement rejetée et qui doivent ainsi quitter le territoire en vertu des dispositions de l'article L. 743-3 du code de l'entrée et du séjour des étrangers et du droit d'asile n'ayant pas vocation à bénéficier du dispositif d'hébergement d'urgence, une carence constitutive d'une atteinte grave et manifestement illégale à une liberté fondamentale ne saurait être caractérisée, à l'issue de la période strictement nécessaire à la mise en oeuvre de leur départ volontaire, qu'en cas de circonstances exceptionnelles ; <br/>
<br/>
              4. Considérant qu'il résulte de l'instruction que MmeB..., de nationalité arménienne, a présenté une demande d'asile et a bénéficié d'un hébergement en centre d'accueil pour demandeurs d'asile durant la période d'examen de sa demande ; que sa demande d'asile a été rejetée par décision du directeur général de l'Office française de protection des réfugiés et apatrides ; que le recours qu'elle a formé contre cette décision a été rejeté par décision de la Cour nationale du droit d'asile du 23 janvier 2017, notifiée à l'intéressée le 18 février 2017 ; qu'elle a été informée par l'Office français de l'immigration et de l'intégration de la fin de la prise en charge de son hébergement au centre d'accueil le 20 mars 2017 ; qu'elle a alors sollicité le bénéfice d'un hébergement d'urgence ; que le juge des référés du tribunal administratif de Lyon a enjoint au préfet du Rhône, par une ordonnance du 22 mars 2017, de procurer un hébergement provisoire à l'intéressée, pour une période de quinze jours, dans l'attente de son retour en Arménie ; que Mme B...a de nouveau saisi le juge des référés du tribunal administratif de Lyon le 7 avril 2017 pour se voir proposer un hébergement d'urgence adapté pour elle et sa fille ; <br/>
<br/>
              5. Considérant que, pour rejeter cette nouvelle demande, le juge des référés du tribunal administratif a relevé que MmeB..., du fait du rejet définitif de sa demande d'asile et en l'absence de toute autorisation de séjour, n'avait pas vocation à se maintenir durablement sur le territoire français ; que l'intéressée n'avait pris aucune disposition pour même envisager son retour en Arménie, alors qu'elle n'avait bénéficié d'une mesure provisoire d'hébergement que dans l'attente de ce retour ; que l'état de santé de la fille de l'intéressée, née en France en décembre 2015 et qui a été opérée à deux reprises après sa naissance, ne nécessitait que des consultations de contrôle tous les quatre mois environ ; que le juge des référés de première instance a estimé, dans ces conditions, que le refus du préfet de proposer à l'intéressée un hébergement d'urgence adapté ne portait pas d'atteinte grave et manifestement illégale à une liberté fondamentale ; qu'en appel, Mme B...n'apporte pas d'éléments nouveaux qui seraient de nature à remettre en cause cette appréciation exempte d'erreur de droit ; que la requérante n'est, par suite, pas fondée à soutenir que c'est à tort que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Lyon a rejeté sa demande ; qu'il y a lieu de rejeter sa requête selon la procédure prévue à l'article L. 522-3 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de Mme B...est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à Mme A...B....<br/>
Copie en sera adressée au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
