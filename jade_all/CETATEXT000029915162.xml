<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029915162</ID>
<ANCIEN_ID>JG_L_2014_11_000000385569</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/91/51/CETATEXT000029915162.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 11/11/2014, 385569, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-11-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>385569</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2014:385569.20141111</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 6 novembre 2014 au secrétariat du contentieux du Conseil d'Etat, présentée par la société IDEAC, représentée par son représentant légal, dont le siège social est 66, rue Nicolo, à Paris (75116) ; la société requérante demande au juge des référés du Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1425041/9 du 3 novembre 2014 par laquelle le juge des référés du tribunal administratif de Paris, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, a rejeté sa demande tendant, à titre principal, à l'annulation de l'arrêté en date du 29 octobre 2014 par lequel le préfet de police a ordonné la fermeture de son établissement pour une durée de 15 jours et, subsidiairement, à la suspension de cet arrêté ; <br/>
<br/>
              2°) de faire droit à sa demande de première instance ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              elle soutient que : <br/>
<br/>
              - la condition d'urgence est remplie, car la société est en situation de redressement judiciaire et la décision contestée compromet ses chances de se redresser ;<br/>
              - le juge de premier degré a qualifié de manière erronée la relation existant entre Monsieur P., auto-entrepreneur indépendant, et la société IDEAC en y voyant une relation de salariat non déclaré ; la sanction prononcée est disproportionnée eu égard à l'absence de récidive et à la proportion de salariés concernés ; <br/>
              - la décision contestée porte une atteinte grave et manifestement illégale aux libertés fondamentales de commerce et d'industrie ; <br/>
<br/>
<br/>
              Vu l'ordonnance attaquée ;<br/>
<br/>
              Vu le mémoire en défense, enregistré le 7 novembre 2014, présenté par le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social qui conclut au rejet de la requête ; il soutient que l'urgence n'est pas établie et que les moyens ne sont pas fondés ; <br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, la société IDEAC, d'autre part, le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 10 novembre 2014 à 11 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Lyon-Caen, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la société IDEAC ;<br/>
              - les représentants de la société IDEAC ;<br/>
              - la représentante ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a prolongé l'instruction jusqu'à 20 heures ;<br/>
<br/>
              Vu les pièces, enregistrées le 10 novembre 2014, présentées pour la société IDEAC ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code du travail ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures " ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 8221-5 du code du travail : " Est réputé travail dissimulé par dissimulation d'emploi salarié le fait pour tout employeur : 1° Soit de se soustraire intentionnellement à l'accomplissement de la formalité prévue à l'article L. 1221-10, relatif à la déclaration préalable à l'embauche ; 2° Soit de se soustraire intentionnellement à l'accomplissement de la formalité prévue à l'article L. 3243-2, relatif à la délivrance d'un bulletin de paie (...) ; 3° Soit de se soustraire intentionnellement aux déclarations relatives aux salaires ou aux cotisations sociales assises sur ceux-ci (...) " ; qu'aux termes du I de l'article L. 8221-6 du même code : " Sont présumées ne pas être liés avec le donneur d'ordre par un contrat de travail dans l'exécution de l'activité donnant lieu à immatriculation ou inscription (...) 4° les personnes physiques relevant de l'article L.123-1-1 du code de commerce ou du V de l'article 19 de la loi n°96-603 du 5 juillet 1996 relative au développement et à la promotion du commerce et de l'artisanat " ; qu'enfin, aux termes du II de ce même article : " L'existence d'un contrat de travail peut toutefois être établie lorsque les personnes mentionnées au I fournissent directement ou par une personne interposée des prestations à un donneur d'ordre dans des conditions qui les placent dans un lien de subordination juridique permanente à l'égard de celui-ci. Dans ce cas, la dissimulation d'emploi salarié est établie si le donneur d'ordre s'est soustrait intentionnellement par ce moyen à l'accomplissement des obligations incombant à l'employeur mentionnées à l'article L.8221-5. "<br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 8272-2 du code du travail : " Lorsque l'autorité administrative a connaissance d'un procès-verbal relevant une infraction prévue aux 1° à 4° de l'article L. 8211-1, elle peut, eu égard à la répétition et à la gravité des faits constatés et à la proportion de salariés concernés, ordonner par décision motivée la fermeture de l'établissement ayant servi à commettre l'infraction, à titre provisoire et pour une durée ne pouvant excéder trois mois. " ; qu'aux termes de l'article L. 8211-1 de ce même code : " Sont constitutives de travail illégal, dans les conditions prévues par le présent livre, les infractions suivantes : 1° Travail dissimulé ; (...). " ; que, par un arrêté du 27 octobre 2014 pris sur le fondement de ces dispositions, le préfet de police a décidé la fermeture immédiate pour quinze jours de l'établissement exploité par la société IDEAC, au motif que Monsieur P., employé par cette société en qualité d'auto-entrepreneur pour assurer des tâches de nettoyage des locaux de réunion et de loisirs qu'elle met à la disposition de sa clientèle, exerçait en réalité dans les conditions du salariat et n'avait, intentionnellement, fait l'objet de la part du gérant de la société d'aucune déclaration d'emploi salarié ; que, par l'ordonnance dont la société IDEAC relève appel, le juge des référés du tribunal administratif de Paris a rejeté sa demande de suspension de cette sanction ; <br/>
<br/>
              4. Considérant, d'une part, qu'il résulte de l'instruction, ainsi que des éléments recueillis lors de l'audience publique, que la société IDEAC était, à la date des faits reprochés, la seule à employer les services de Monsieur P., auquel elle fournissait l'ensemble des instruments nécessaires à son activité ; que le détail des tâches confiées à ce dernier l'était par voie d'instructions directes du gérant de la société, la consistance précise du service à rendre n'étant pas mentionnée dans " l'accord de prestation de service " conclu entre la société IDEAC et Monsieur P. ; que, par suite, le préfet de police n'a pas qualifié de manière manifestement inexacte la situation de travail de Monsieur P. en estimant qu'elle revêtait les caractères de l'emploi salarié ; <br/>
<br/>
              5. Considérant, d'autre part, que si Monsieur P. soutient, par une attestation écrite versée au dossier par la société IDEAC, qu'il a volontairement choisi le statut d'auto-entrepreneur, il résulte de ses déclarations antérieures portées au procès-verbal dressé par le contrôleur du travail que ce choix, à le supposer libre, lui a été suggéré par le gérant de la société IDEAC ; que le préfet de police n'a ainsi, alors même que la relation de travail était au nombre de celles mentionnées au I de l'article L. 8221-6 du code du travail mentionné ci-dessus, pas qualifié de manière manifestement inexacte les faits reprochés au gérant de la société IDEAC en estimant qu'ils étaient constitutifs de travail dissimulé par dissimulation d'emploi salarié ; <br/>
<br/>
              6. Considérant enfin que, alors même que l'infraction reprochée n'aurait pas eu d'antécédent et que Monsieur P. aurait été la seule personne dans cette situation sur les sept salariés de la société IDEAC à la date de première constatation des faits, le préfet de police n'a pas commis d'erreur manifeste d'appréciation en fixant à quinze jours la durée de la fermeture des locaux eu égard à la gravité du recours à un faux statut de travailleur indépendant qui permettait à cette société de faire travailler Monsieur P. à un tarif horaire inférieur au minimum légal ; <br/>
<br/>
              7. Considérant qu'il résulte de ce qui est dit aux points 4 à 6 ci-dessus que la société IDEAC n'est pas fondée à soutenir que le préfet de police aurait entaché son arrêté du 27 octobre 2014 d'une illégalité manifeste ;<br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que, sans qu'il soit besoin d'examiner l'existence, à la date de la présente ordonnance, soit trois jours avant l'expiration des effets de la sanction contestée, d'une situation d'urgence de nature à justifier l'usage des pouvoirs que le juge des référés tient de l'article L. 521-2 du code de justice administrative, la société IDEAC n'est pas fondée à se plaindre de ce que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Paris a rejeté sa demande ; que sa requête d'appel doit, par suite, être rejetée ;<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er  : La requête de la société IDEAC est rejetée<br/>
Article 2 : La présente ordonnance sera notifiée à la société IDEAC et au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
Copie en sera adressée au préfet de police. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
