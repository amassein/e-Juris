<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029709135</ID>
<ANCIEN_ID>JG_L_2014_11_000000355045</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/70/91/CETATEXT000029709135.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 05/11/2014, 355045, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-11-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>355045</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON ; SCP BOUTET, HOURDEAUX</AVOCATS>
<RAPPORTEUR>M. Guillaume Odinet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:355045.20141105</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 20 décembre et 20 mars 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune de Chagny, représentée par son maire ; la commune demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 11LY0274 du 20 octobre 2011 par lequel la cour administrative d'appel de Lyon a rejeté l'appel qu'elle a formé contre le jugement n° 0700086 du 28 juin 2007 par lequel le tribunal administratif de Dijon a rejeté sa demande tendant à l'annulation pour excès de pouvoir de la décision du président de la communauté de communes de la région de Chagny-en-Bourgogne de procéder au remboursement anticipé de l'emprunt souscrit par celle-ci auprès d'un établissement bancaire et de payer la pénalité correspondante ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la communauté de communes Entre Monts et Dheune, venue aux droits de la communauté de communes de la région de Chagny-en-Bourgogne, la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Odinet, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boré, Salve de Bruneton, avocat de la commune de Chagny.<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une délibération du 25 mars 2004, le conseil de la communauté de communes de la région de Chagny-en-Bourgogne a autorisé son président à signer une convention d'emprunt avec la Caisse d'épargne et de prévoyance de Bourgogne ainsi " qu'à effectuer l'ensemble des opérations prévues au contrat " ; qu'au cours de l'année 2006, le président de la communauté de communes a décidé de procéder à un remboursement anticipé de l'emprunt ; que, par un jugement du 28 juin 2007, le tribunal administratif de Lyon a rejeté la demande de la commune de Chagny tendant à l'annulation de cette décision ; que, par une décision du 9 mai 2011, le Conseil d'Etat, statuant au contentieux, a annulé l'arrêt du 7 juillet 2009 par lequel la cour administrative d'appel de Lyon a annulé ce jugement et rejeté la demande de première instance de la commune de Chagny comme portée devant une juridiction incompétente pour en connaître ; que la commune se pourvoit en cassation contre l'arrêt du 20 octobre 2011 par lequel la cour administrative d'appel de Lyon, à laquelle l'affaire a été renvoyée, a rejeté son appel ; <br/>
<br/>
              2. Considérant, en premier lieu, qu'il ressort des pièces du dossier soumis aux juges du fond que la convention conclue le 16 avril 2004 portait sur un emprunt d'un million cinq cent mille euros incluant deux phases ; qu'au titre de la première phase, dite de préfinancement, la communauté de communes pouvait, pendant trois ans, utiliser des fonds dans la limite du montant global de prêt autorisé, les sommes utilisées portant intérêts selon des modalités fixées par la convention ; qu'au titre de la seconde phase, dite de consolidation, la communauté de communes consolidait les fonds utilisés sur des lignes d'emprunt d'un montant minimal de cent mille euros et d'une durée d'amortissement comprise entre deux et quinze ans ; que, pour chaque ligne d'emprunt consolidée, la communauté de communes pouvait choisir parmi cinq modalités de taux et de périodicité des remboursements, les quatre premières pouvant donner lieu, selon son choix, à un amortissement constant ou à un amortissement progressif ; qu'en vertu de l'article 9 de la convention, la communauté de communes pouvait procéder, à chaque échéance de chacune des lignes d'emprunt, à un remboursement anticipé total ou partiel des emprunts consolidés selon les quatre premières modalités de taux et à un remboursement total des emprunts consolidés selon la cinquième de ces modalités, moyennant le versement, pour chaque remboursement anticipé, d'une indemnité dont les modalités de calcul étaient fixées avec précision ; <br/>
<br/>
              3. Considérant qu'en jugeant que l'usage, par le président de la communauté de communes, de la faculté de remboursement anticipé dont il disposait ainsi en vertu des stipulations contractuelles mentionnées au point 2 constituait une mesure d'exécution de la convention non détachable de celle-ci, pour en déduire que la commune, tierce au contrat, n'était pas recevable à demander l'annulation pour excès de pouvoir de cette décision, la cour n'a commis ni erreur de qualification juridique, ni erreur de droit ;<br/>
<br/>
              4. Considérant, en second lieu, que, pour estimer que la demande de la commune n'était pas recevable, la cour n'a pas jugé que la décision litigieuse était insusceptible d'avoir des incidences financières pour la commune, mais qu'elle n'était pas détachable de la convention d'emprunt du 16 avril 2004 ; que, dès lors, les moyens tirés de ce que la cour aurait commis une erreur de droit et dénaturé les pièces du dossier en jugeant que la décision litigieuse n'affectait pas les intérêts patrimoniaux de la commune ne peuvent qu'être écartés ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que la commune de Chagny n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque ; que ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à sa charge la somme de 3 000 euros à verser à la communauté de communes Entre Monts et Dheune au titre des mêmes dispositions. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
      Article 1er : Le pourvoi de la commune de Chagny est rejeté.<br/>
<br/>
Article 2 : La commune de Chagny versera à la communauté de communes Entre Monts et Dheune la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la commune de Chagny et à la communauté de communes Entre Monts et Dheune.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
