<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028158636</ID>
<ANCIEN_ID>JG_L_2013_10_000000370359</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/15/86/CETATEXT000028158636.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 17/10/2013, 370359, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-10-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>370359</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Didier Ribes</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:370359.20131017</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance n° 1300067 du 15 juillet 2013, enregistrée le 19 juillet 2013 au secrétariat du contentieux du Conseil d'État, par laquelle le président de la 1ère chambre du tribunal administratif de Châlons-en-Champagne, avant qu'il soit statué sur la demande de la commune d'Aubigny-les-Pothées tendant à l'annulation de l'arrêté du 16 novembre 2012 par lequel le préfet des Ardennes a déclaré d'utilité publique la dérivation des eaux souterraines et l'instauration de périmètres de protection autour des captages de la communauté d'agglomération de Charleville-Mézières situés sur le territoire de la commune d'Aubigny-les-Pothées, instauré des périmètres de protection, autorisé la communauté d'agglomération à distribuer les eaux issues des captages et déclaré cessibles, au profit de cette dernière, les terrains nécessaires à l'opération, a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel, de transmettre au Conseil d'État la question de la conformité aux droits et libertés garantis par la Constitution des dispositions de l'article L. 1321-2 du code de la santé publique ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 3 octobre 2013, présentée par la commune d'Aubigny-les-Pothées ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule et son article 61-1 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ; <br/>
<br/>
              Vu le code de l'environnement ;<br/>
<br/>
              Vu le code de l'expropriation pour cause d'utilité publique ;<br/>
<br/>
              Vu le code général de la propriété des personnes publiques ;<br/>
<br/>
              Vu le code de la santé publique ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier Ribes, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'État lui a transmis, en application de l'article 23-2 de cette même ordonnance, la question de la conformité aux droits et libertés garantis par la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ; <br/>
<br/>
              2. Considérant que l'article L. 1321-2 du code de la santé publique prévoit qu'en vue d'assurer la protection de la qualité des eaux, l'acte portant déclaration d'utilité publique des travaux de prélèvement d'eau destinée à l'alimentation des collectivités humaines détermine autour du point de prélèvement un périmètre de protection immédiate dont les terrains sont à acquérir en pleine propriété, un périmètre de protection rapprochée à l'intérieur duquel peuvent être interdits ou réglementés toutes sortes d'installations, travaux, activités, dépôts, ouvrages, aménagement ou occupation des sols de nature à nuire directement ou indirectement à la qualité des eaux et, le cas échéant, un périmètre de protection éloignée à l'intérieur duquel peuvent être réglementés les installations, travaux, activités, dépôts, ouvrages, aménagement ou occupation des sols ci-dessus mentionnés ; qu'il précise notamment que, lorsque des terrains situés dans un périmètre de protection immédiate appartiennent à une collectivité publique, il peut être dérogé à l'obligation d'acquérir les terrains par l'établissement d'une convention de gestion entre la ou les collectivités publiques propriétaires et l'établissement public de coopération intercommunale ou la collectivité publique responsable du captage ;<br/>
<br/>
              3. Considérant que le tribunal administratif de Châlons-en-Champagne est saisi d'un recours de la commune d'Aubigny-les-Pothées tendant à l'annulation de l'arrêté du 16 novembre 2012, pris sur le fondement de l'article L. 1321-2 du code de la santé publique, par lequel le préfet des Ardennes a déclaré d'utilité publique la dérivation des eaux souterraines et l'instauration de périmètres de protection autour des captages de la communauté d'agglomération de Charleville-Mézières situés sur le territoire de la commune d'Aubigny-les-Pothées, instauré des périmètres de protection, autorisé la communauté d'agglomération à distribuer l'eau issue de ces captages et déclaré cessibles, au profit de la communauté d'agglomération, les terrains nécessaires à cette opération ; <br/>
<br/>
              4. Considérant que la commune d'Aubigny-les-Pothées soutient que l'article L. 1321-2 du code de la santé publique, en ne précisant pas les conditions dans lesquelles le public participe à l'élaboration des décisions prises sur son fondement, méconnaît l'article 7 de la Charte de l'environnement et en permettant de priver, sans indemnisation, une commune de la libre disposition de son domaine public et privé, est contraire au principe de libre administration des collectivités territoriales garanti par l'article 72 de la Constitution ; <br/>
<br/>
              5. Considérant, d'une part, que les actes, pris en application de l'article L. 1321-2 du code de la santé publique, déclarant d'utilité publique les travaux de prélèvement d'eau et les servitudes s'imposant dans les périmètres de protection qu'ils définissent relèvent du champ de l'article L. 11-2 du code de l'expropriation pour cause d'utilité publique aux termes duquel " l'utilité publique est déclarée par arrêté ministériel ou par arrêté préfectoral " ; qu'en l'absence de dispositions spécifiques définissant la procédure applicable à cette catégorie d'actes, les dispositions législatives du code de l'expropriation pour cause d'utilité publique relatives aux enquêtes publiques lui sont applicables et garantissent la participation du public à l'élaboration des décisions concernées ;<br/>
<br/>
              6. Considérant, d'autre part, que l'article L. 1321-3 du code de la santé publique prévoit l'indemnisation des propriétaires et occupants de terrains compris dans un périmètre de protection défini en application des dispositions contestées ; que l'article L. 2123-6 du code général de la propriété des personnes publiques précise également que le transfert de gestion des dépendances du domaine public de la personne publique propriétaire au profit du bénéficiaire de l'acte déclaratif d'utilité publique donne lieu à indemnisation en raison de la privation des revenus qui peuvent en résulter pour la personne dessaisie ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux ; que, par suite, il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité transmise par le tribunal administratif de Châlons-en-Champagne.<br/>
Article 2 : La présente décision sera notifiée à la commune d'Aubigny-les-Pothées, à la communauté d'agglomération de Charleville-Mézières et à la ministre des affaires sociales et de la santé.<br/>
Copie en sera adressée au Premier ministre, au Conseil constitutionnel et au tribunal administratif de Châlons-en-Champagne.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
