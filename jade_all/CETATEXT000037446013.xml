<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037446013</ID>
<ANCIEN_ID>JG_L_2018_09_000000396667</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/44/60/CETATEXT000037446013.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 28/09/2018, 396667, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-09-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>396667</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Bruno Bachini</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:396667.20180928</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Clermont-Ferrand d'annuler la décision du ministre de l'agriculture, de l'agroalimentaire et de la forêt du 3 avril 2013 résiliant son contrat d'enseignement à compter du 1er septembre 2013. Par un jugement n° 1300912 du 5 février 2014, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 14LY00681 du 1er décembre 2015, la cour administrative d'appel de Lyon a, sur appel de M.B..., annulé ce jugement et la décision du ministre.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 2 février et 2 mai 2016 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'agriculture, de l'agroalimentaire et de la forêt demande au Conseil d'Etat d'annuler cet arrêt.  <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code rural et de la pêche maritime ;<br/>
              - le décret n° 89-406 du 20 juin 1989 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Bachini, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de M. B...et du syndicat national de l'enseignement initial privé ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 14 septembre 2018, présentée par M. B... ; <br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que le syndicat national de l'enseignement initial privé SNEIP-CGT justifie d'un intérêt suffisant au maintien de l'arrêt attaqué ; qu'ainsi son intervention est recevable ;<br/>
<br/>
              2. Considérant que, par une décision du 3 avril 2013, le ministre de l'agriculture, de l'agroalimentaire et de la forêt a résilié, à compter du 1er septembre 2013, le contrat d'enseignement de M.B..., recruté en contrat à durée indéterminée à compter du 1er septembre 2005 et affecté au lycée d'enseignement agricole et forestier privé Claude Mercier au Mayet-Montagne ; que, par un arrêt du 1er décembre 2015 contre lequel le ministre se pourvoit en cassation, la cour administrative d'appel de Lyon a, sur appel de M.B..., annulé le jugement du tribunal administratif de Clermont-Ferrand du 5 février 2014 rejetant la demande présentée par ce dernier contre la décision du 3 avril 2013, ainsi que cette décision ;  <br/>
<br/>
<br/>
              3. Considérant qu'aux termes du deuxième alinéa de l'article L. 813-8 du code rural et de la pêche maritime relatif aux personnels enseignants des établissements d'enseignement agricoles privés : " Les personnels enseignants et de documentation de ces établissements sont nommés par le ministre de l'agriculture, après vérification de leurs titres et de leurs qualifications, sur proposition du chef d'établissement. Ils sont liés par un contrat de droit public à l'Etat, qui les rémunère directement par référence aux échelles indiciaires des corps équivalents de la fonction publique exerçant des fonctions comparables et ayant les mêmes niveaux de formation. En leur qualité d'agent public, ils ne sont pas, au titre des fonctions pour lesquelles ils sont employés et rémunérés par l'Etat, liés par un contrat de travail à l'établissement au sein duquel l'enseignement leur est confié " ; qu'aux termes de l'article 47 du décret du 20 juin 1989 relatif aux contrats liant l'Etat et les personnels enseignants et de documentation des établissements mentionnés à l'article L. 813-8 du code rural : " A la date fixée chaque année par le ministre chargé de l'agriculture, les chefs d'établissement lui transmettent pour la rentrée scolaire suivante la liste, établie par niveau d'enseignement, discipline ou groupe de disciplines : / (...) 2° Des contrats individuels dont ils proposent la modification ou la résiliation, compte tenu de la réduction ou de la suppression de charges d'enseignement ou de documentation. Pour en établir la liste, le chef d'établissement prend en compte la durée des services d'enseignement, de documentation, de direction ou de formation accomplis par chaque agent dans les établissements d'enseignement publics et privés sous contrat (...) / Le ministre chargé de l'agriculture informe les intéressés de la réduction du service ou de la suppression de leur emploi par lettre recommandée avec demande d'avis de réception " ; qu'il résulte de ces dispositions que les décisions par lesquelles le ministre chargé de l'agriculture modifie ou résilie des contrats d'enseignement doivent tenir compte de la durée des services accomplis par les agents, en comparant ces durées au sein d'un même niveau d'enseignement et d'une même discipline ou d'un même groupe de disciplines ; <br/>
<br/>
              4. Considérant qu'il résulte des termes mêmes de l'arrêt attaqué que, pour annuler la décision ministérielle litigieuse, la cour administrative d'appel a jugé que la durée des services accomplis par M.B..., enseignant en seconde et terminale professionnelle, aurait dû être comparée avec celle d'un autre enseignant de la même discipline, qui intervenait dans les classes du niveau de brevet de technicien supérieur, soit à un niveau d'enseignement différent ; qu'il résulte de ce qui a été dit ci-dessus que, alors même qu'il ressortait des pièces du dossier qui lui était soumis que M. B...aurait été susceptible d'assurer des enseignements dans les classes du niveau de brevet de technicien supérieur, la cour administrative d'appel de Lyon a entaché son arrêt d'une erreur de droit ; que, par suite, sans qu'il soit besoin d'examiner l'autre moyen de son pourvoi, le ministre de l'agriculture, de l'agroalimentaire et de la forêt est fondé à demander l'annulation de l'arrêt qu'il attaque ;  <br/>
<br/>
              5. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'intervention du syndicat national de l'enseignement initial privé SNEIP - CGT est admise.<br/>
Article 2 : L'arrêt de la cour administrative d'appel de Lyon du 1er décembre 2015 est annulé.<br/>
Article 3 : L'affaire est renvoyée à la cour administrative d'appel de Lyon. <br/>
Article 4 : Les conclusions de M. B...présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 5 : La présente décision sera notifiée au ministre de l'agriculture et de l'alimentation, à M. A... B...et au syndicat national de l'enseignement initial privé SNEIP - CGT.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
