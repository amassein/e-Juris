<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043871544</ID>
<ANCIEN_ID>JG_L_2021_06_000000452074</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/87/15/CETATEXT000043871544.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 11/06/2021, 452074, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-06-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>452074</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:452074.20210611</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              I. Sous le n° 452074, par une requête, enregistrée le 28 avril 2021 au secrétariat du contentieux du Conseil d'Etat, Mme A... B... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-3 du code de justice administrative : <br/>
<br/>
              1°) d'enjoindre au Conseil constitutionnel de répondre à son courrier du 18 novembre 2020 l'interrogeant sur le point de savoir si " Dans le cadre de la décision n° 2017-689 QCP 8 février 2018, les effets dans le temps (points 11 et 12 de la décision) de cette décision ont été étudiés pour un statut de loueur en meublé professionnel optionnel ou pour un statut de loueur en meublé professionnel obligatoire " ; <br/>
<br/>
              2°) dans l'hypothèse où il ne serait pas fait droit à cette demande, de lui indiquer la juridiction compétente pour connaître d'une requête tendant à contester une décision du Conseil constitutionnel qui ne prévoit pas de modulation de ses effets dans le temps. <br/>
<br/>
<br/>
              Elle soutient que : <br/>
              - la décision du Conseil constitutionnel n° 2017-689 QPC du 8 février 2018 est entachée d'irrégularité en ce qu'elle ne prévoit pas un report dans le temps des effets de la déclaration d'inconstitutionnalité qu'elle prononce ; <br/>
              - lors l'audience du 30 janvier 2018 relative à cette affaire, le représentant du Premier ministre n'a pas indiqué que la condition d'inscription au registre du commerce et des sociétés garantissait la liberté de choisir du contribuable et le caractère optionnel du statut de loueur en meublé professionnel ; <br/>
              - en prononçant l'abrogation de la condition en cause, ce qui a eu pour effet de rendre le statut de loueur en meublé professionnel obligatoire, la décision du Conseil constitutionnel porte préjudice à ses intérêts ;<br/>
              - cette décision QPC méconnaît les droits de la défense, en particulier le droit d'être entendu par une autorité compétente, dès lors qu'elle préjudicie aux tiers à l'instance en cause, en l'absence de modulation de ses effets dans le temps ; <br/>
              - le courrier par lequel elle a demandé au Conseil constitutionnel des précisions quant aux conséquences de sa décision du 8 février 2018 sur sa situation fiscale, en vue de préparer sa défense dans le cadre de la procédure amiable qu'elle a engagée et poursuivie auprès du collège territorial de second examen des demandes de rescrit de Nanterre, est demeuré sans réponse. <br/>
<br/>
<br/>
              II. Sous le n° 452583, par une requête, enregistrée le 14 mai 2021 au secrétariat du contentieux du Conseil d'Etat, Mme A... B... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de la décision n° 2017-689 QPC du 8 février 2018 du Conseil constitutionnel. <br/>
<br/>
<br/>
              Elle soutient que : <br/>
              - la condition d'urgence est satisfaite dès lors que la décision du Conseil constitutionnel n° 2017-689 QPC du 8 février 2018 produira ses effets à compter du 19 mai 2021 ;  <br/>
              - il est porté une atteinte grave et manifestement illégale à plusieurs libertés fondamentales ;<br/>
              - la décision du 8 février 2018 du Conseil constitutionnel méconnaît les droits de la défense, en l'absence de modulation de ses effets dans le temps ;<br/>
              - elle porte atteinte à ses droits fondamentaux et lui cause un préjudice significatif ; <br/>
              - elle méconnaît le principe d'égalité dès lors qu'elle favorise les contribuables les plus aisés, au détriment des classes moyennes. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Les requêtes visées ci-dessus, respectivement présentées sur le fondement des articles L. 521-3 et L. 521-1 du code de justice administrative, présentent à juger les mêmes questions. Il y a lieu de les joindre pour statuer par une même décision. <br/>
<br/>
              2. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". Aux termes de l'article L. 521-3 du même code : " En cas d'urgence et sur simple requête qui sera recevable même en l'absence de décision administrative préalable, le juge des référés peut ordonner toutes autres mesures utiles sans faire obstacle à l'exécution d'aucune décision administrative ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              3. Le juge des référés du Conseil d'Etat ne peut être régulièrement saisi, en premier et dernier ressort, d'une requête tendant à la mise en oeuvre de l'une des procédures régies par le livre V du code de justice administrative que pour autant que le litige principal auquel se rattache ou est susceptible de se rattacher la mesure d'urgence qu'il lui est demandé de prendre ressortit lui-même de la compétence du Conseil d'Etat. L'article R. 522-8-1 du même code prévoit que, par dérogation aux dispositions du V du livre III relatif au règlement des questions de compétence au sein de la juridiction administrative, le juge des référés qui entend décliner la compétence de la juridiction rejette les conclusions dont il est saisi par voie d'ordonnance.<br/>
<br/>
              4. Les requêtes de Mme B..., présentées sur le fondement des articles L. 521-3 et L. 521-1 du code de justice administrative, tendent à contester la décision n° 2017-689 QPC du 8 février 2018, par laquelle le Conseil constitutionnel a déclaré contraire à la Constitution l'une des trois conditions auxquelles est subordonné le bénéfice du régime de loueur en meublé professionnel, à savoir celle relative à l'inscription au registre du commerce et des sociétés. En conséquence de l'abrogation qui s'en est suivie, il résulte de la loi fiscale que le régime de loueur en meublé professionnel s'applique dès lors que les deux conditions restantes du 2 du IV de l'article 155 du code général des impôts sont satisfaites. Mme B... conteste cette solution et soutient qu'en ne prévoyant pas de modulation de ses effets dans le temps, la décision du Conseil constitutionnel porterait atteinte à ses droits fondamentaux, en ce qu'elle lui fait perdre le bénéfice de l'option pour le statut de loueur en meublé professionnel. Toutefois, ses conclusions, qui tendent à demander au juge des référés du Conseil d'Etat statuant, d'une part, sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de la décision du 8 février 2018 du Conseil constitutionnel et, d'autre part, sur le fondement de l'article L. 521-3 du même code, d'enjoindre au Conseil constitutionnel de répondre à un courrier qu'elle lui a adressé, ne relèvent manifestement pas de l'office du juge des référés et sont, par suite, irrecevables.<br/>
<br/>
              5. Il suite de là qu'il y a lieu de rejeter les requêtes de Mme B... selon la procédure prévue à l'article L. 522-3 du code de justice administrative. <br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : Les requêtes de Mme B... sont rejetées. <br/>
Article 2 : La présente ordonnance sera notifiée à Mme A... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
