<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044807691</ID>
<ANCIEN_ID>JG_L_2021_07_000000444770</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/80/76/CETATEXT000044807691.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 09/07/2021, 444770, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>444770</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Ophélie Champeaux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:444770.20210709</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
               MM. Didier B..., Pascal Laurent, Bernard Henry et Michel E... ont demandé au tribunal administratif de Châlons-en-Champagne d'annuler les opérations électorales qui se sont déroulées le 15 mars 2020 dans la commune de Williers (Ardennes) pour l'élection des conseillers municipaux, de retenir la qualification de fraude électorale et de transmettre le dossier au procureur de la République, de déclarer inéligibles pour une durée de trois ans les défendeurs, auteurs et complices, actifs et passifs, des manœuvres ayant eu pour effet de porter atteinte à la sincérité du scrutin. <br/>
<br/>
               Par un jugement n° 2000661 du 17 septembre 2020, ce tribunal a rejeté cette protestation. <br/>
<br/>
               Par une requête et un mémoire en réplique, enregistrés les 22 septembre 2020 et 25 janvier 2021, MM. B..., Laurent, Henry et E... doivent être regardés comme demandant au Conseil d'Etat : <br/>
<br/>
               1°) d'annuler ce jugement ;<br/>
<br/>
               2°) de faire droit à leur protestation ;<br/>
<br/>
               3°) de déclarer M. A... N..., Mme T... L... K..., M. F... D..., Mme M... P..., M. G... R..., Mme J... O... et Mme S... C... inéligibles en vertu de l'article L. 118-4 du code électoral ;<br/>
               4°) dans le cas où serait retenue l'existence d'une fraude électorale, de transmettre le dossier au procureur de la République compétent en application de l'article L. 117-1 du code électoral.<br/>
<br/>
<br/>
               Vu les autres pièces du dossier ;<br/>
<br/>
               Vu :<br/>
               - le code électoral ;<br/>
               - le code de justice administrative ;<br/>
<br/>
<br/>
               Après avoir entendu en séance publique :<br/>
<br/>
               - le rapport de Mme Ophélie Champeaux, maître des requêtes,  <br/>
<br/>
               - les conclusions de M. Romain Victor, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
               1. A l'issue des opérations électorales qui se sont déroulées le 15 mars 2020 à Williers (Ardennes), les sept sièges de conseillers municipaux ont été pourvus et attribués respectivement à M. A... N..., Mme T... L... K..., M. F... D..., Mme M... P..., M. G... R..., Mme J... O... et à Mme S... C..., qui ont obtenu, chacun, entre 24 et 26 voix, sur un total de 30 suffrages exprimés. MM. Didier B..., Pascal Laurent, Bernard Henry et Michel E... relèvent appel du jugement du 17 septembre 2020 par lequel le tribunal administratif de Châlons-en-Champagne a rejeté la protestation qu'ils avaient formée contre ces opérations électorales. <br/>
<br/>
               2. En premier lieu, il résulte de l'instruction que M. H... E... a été radié des listes électorales au motif qu'il avait perdu son attache dans la commune. Après rejet du recours administratif préalable qu'il avait formé devant la commission de contrôle, M. E... a saisi le tribunal de proximité de Sedan qui, faisant droit à sa demande, a ordonné sa réinscription sur les listes électorales de Williers par un jugement du 26 février 2020, immédiatement mis à disposition des parties au greffe de la juridiction. Dès lors que le délai de dépôt des candidatures pour le scrutin en litige n'expirait que le 27 février 2020 à 18 heures, la circonstance que M. E... ait été, avant cette date, radié des listes électorales n'était pas, par elle-même, de nature à établir l'existence d'une manœuvre visant à l'empêcher de se porter candidat, susceptible d'avoir faussé les résultats du scrutin. <br/>
<br/>
               3. En deuxième lieu, en vertu de l'article L. 18 du code électoral, la décision par laquelle le maire statue sur la demande d'un électeur tendant à obtenir son inscription sur les listes électorales de la commune est notifiée à l'intéressé dans un délai de deux jours. Le premier alinéa de l'article R. 16 du même code précise que : " Les décisions de refus d'inscription ou de radiation prises par le maire sont notifiées par écrit à l'électeur intéressé. La notification de ces décisions mentionne les voies et délais de recours prévus aux III et IV de l'article L. 18 ainsi qu'au I de l'article L. 20. "<br/>
<br/>
               4. Si le juge de l'élection n'est pas compétent pour statuer sur la régularité ou le bien-fondé de l'inscription ou de la radiation d'un électeur sur les listes électorales, il lui appartient en revanche d'apprécier tous les faits révélant des manœuvres ou des irrégularités susceptibles d'avoir altéré la sincérité du scrutin.<br/>
<br/>
               5. Il résulte de l'instruction que les décisions par lesquelles le maire de Williers a rejeté les demandes d'inscription sur les listes électorales de la commune de Mmes I... et Q... B... ont été notifiées à ces dernières par lettre recommandée avec accusé de réception. Si, contrairement à celui adressé à Mme I... B..., le pli adressé à Mme Q... B... a été retourné à son expéditeur faute d'avoir été retiré par son destinataire, il ne résulte pas de l'instruction que ce pli aurait été notifié à une adresse autre que celle portée à la connaissance de la commune. Dans ces conditions, les requérants n'établissent pas que la notification de ces décisions serait intervenue dans des conditions irrégulières.<br/>
<br/>
               6. En troisième lieu, aux termes de l'article L. 19-1 du code électoral : " La liste électorale est rendue publique dans des conditions fixées par décret en Conseil d'État, au moins une fois par an et, en tout état de cause, le lendemain de la réunion de la commission de contrôle, préalable à chaque scrutin, prévue au III de l'article L. 19. " L'article R. 13 du même code précise que : " Le tableau des inscriptions et radiations intervenues depuis la dernière réunion de la commission prévue à l'article L. 19 est mis à disposition des électeurs auprès des services de la commune, aux horaires d'ouverture habituels. Il le demeure jusqu'à l'expiration du délai de recours contentieux prévu au I de l'article L. 20. (...) "<br/>
<br/>
               7. Il est constant que les requérants ont pu, le 19 février 2020, accéder à une liste non définitive des électeurs inscrits et qu'un tableau des inscriptions et radiations est bien demeuré affiché en mairie jusqu'au 15 mars. Si les requérants se plaignent de ce que la liste définitive des électeurs inscrits sur les listes électorales de la commune n'a pas été transmise à l'ensemble de ses habitants, une telle obligation ne résulte ni des dispositions précitées, ni d'une autre disposition du code électoral, de sorte que l''irrégularité qu'ils allèguent n'est pas établie. <br/>
<br/>
               8. En quatrième lieu, si les requérants se prévalent de ce que M. B... aurait fait l'objet de menaces à la suite du dépôt de sa protestation, une telle circonstance, postérieure au déroulement des opérations électorales en litige, est en tout état de cause sans incidence sur la solution du litige. <br/>
<br/>
               9. Il résulte de tout ce qui précède, sans qu'il soit besoin de se prononcer sur la fin de non-recevoir soulevée par M. N..., que les requérants ne sont pas fondés à demander l'annulation du jugement qu'ils attaquent. <br/>
<br/>
               10. En l'absence de toute irrégularité imputable aux intéressés, les conclusions présentées par MM. B..., Laurent, Henry et E... tendant à ce que M. N..., Mme L... K..., M. D..., Mme P..., M. R..., Mme O... et Mme C... soient déclarés inéligible en application des dispositions de l'article L. 118-4 du code électoral ne peuvent qu'être rejetées. Doivent également être rejetées, par voie de conséquence, les conclusions de MM. B..., Laurent, Henry et E... tendant à ce que le dossier soit transmis au procureur de la République compétent en application de l'article L. 117-1 du code électoral.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
               --------------<br/>
<br/>
Article 1er : La requête de MM. B..., Laurent, Henry et E... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à MM. Didier B..., Pascal Laurent, Bernard Henry, Michel E..., Rémi N..., Mme T... L... K..., M. F... D..., Mme M... P..., M. G... R..., Mmes J... O..., S... C... et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
