<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044367611</ID>
<ANCIEN_ID>JG_L_2021_06_000000453625</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/36/76/CETATEXT000044367611.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 16/06/2021, 453625, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-06-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>453625</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:453625.20210616</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 14 juin 2021 au secrétariat du contentieux du Conseil d'Etat, l'association Civitas demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner toutes mesures nécessaires à la sauvegarde de l'intérêt supérieur des enfants à laquelle le Conseil supérieur de l'audiovisuel (CSA) a porté atteinte en refusant d'utiliser les pouvoirs qu'il tient de l'article 23 de la loi n° 2020-936 du 30 juillet 2020 ;<br/>
<br/>
              2°) d'enjoindre au CSA de mettre en demeure les sites internet xvideos.com, pornhub.com, xnxx.com, xhamster.com, tukif.com, jacquieetmicheltv2.net, jacquieetmichel.net et jacquieetmicheltv.net de prendre dans le délai d'un mois toutes mesures de nature à empêcher l'accès des mineurs à leurs contenus ; <br/>
<br/>
              3°) à titre subsidiaire, d'enjoindre au CSA de saisir le président du tribunal judiciaire de Paris aux fins d'ordonner que les personnes mentionnées au 1 du I de l'article 6 de la loi n° 2004-575 du 21 juin 2004 mettent fin à l'accès des mineurs à ces services, y compris lorsqu'ils sont rendus accessibles à partir d'une autre adresse et d'ordonner toutes mesures pour faire cesser leur référencement par les moteurs de recherche ou les annuaires ;<br/>
<br/>
              4°) de mettre à la charge du CSA une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - le Conseil d'Etat est compétent en premier et dernier ressort pour connaître de ce litige ;<br/>
              - sa requête est recevable ;<br/>
              - la condition d'urgence est satisfaite dès lors que l'inaction du CSA met en péril de très nombreux mineurs, en méconnaissance des dispositions de l'article 227-24 du code pénal ;  <br/>
              - il est porté une atteinte grave et manifestement illégale à l'intérêt supérieur des enfants dès lors que le CSA n'a à ce jour pas mis en œuvre la procédure prévue a` l'article 23 de la loi n° 2020-936 du 30 juillet 2020 à l'encontre des sites internet visés, dont l'accès est aisé et qui sont en situation d'illégalité manifeste.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code pénal ; <br/>
              - la loi n° 2020-936 du 30 juillet 2020 ;<br/>
              - la loi n° 2004-575 du 21 juin 2004 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) " En application de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. Le requérant qui saisit le juge des référés sur le fondement des dispositions qui viennent d'être rappelées de l'article L. 521-2 du code de justice administrative doit justifier des circonstances particulières caractérisant la nécessité pour lui de bénéficier à très bref délai d'une mesure de la nature de celles qui peuvent être ordonnées sur le fondement de cet article. La circonstance qu'une atteinte à une liberté fondamentale serait avérée n'est pas de nature à caractériser, à elle seule, l'existence d'une situation d'urgence au sens de cet article. <br/>
<br/>
              3. Aux termes de l'article 23 de la loi du 30 juillet 2020 visant à protéger les victimes de violences conjugales visée ci-dessus : " Lorsqu'il constate qu'une personne dont l'activité est d'éditer un service de communication au public en ligne permet à des mineurs d'avoir accès à un contenu pornographique en violation de l'article 227-24 du code pénal, le président du Conseil supérieur de l'audiovisuel adresse à cette personne, par tout moyen propre à en établir la date de réception, une mise en demeure lui enjoignant de prendre toute mesure de nature à empêcher l'accès des mineurs au contenu incriminé. La personne destinataire de l'injonction dispose d'un délai de quinze jours pour présenter ses observations. / A l'expiration de ce délai, en cas d'inexécution de l'injonction prévue au premier alinéa du présent article et si le contenu reste accessible aux mineurs, le président du Conseil supérieur de l'audiovisuel peut saisir le président du tribunal judiciaire de Paris aux fins d'ordonner, selon la procédure accélérée au fond, que les personnes mentionnées au 1 du I de l'article 6 de la loi n° 2004-575 du 21 juin 2004 pour la confiance dans l'économie numérique mettent fin à l'accès à ce service. Le procureur de la République est avisé de la décision du président du tribunal. / Le président du Conseil supérieur de l'audiovisuel peut saisir, sur requête, le président du tribunal judiciaire de Paris aux mêmes fins lorsque le service de communication au public en ligne est rendu accessible à partir d'une autre adresse. / Le président du Conseil supérieur de l'audiovisuel peut également demander au président du tribunal judiciaire de Paris d'ordonner, selon la procédure accélérée au fond, toute mesure destinée à faire cesser le référencement du service de communication en ligne par un moteur de recherche ou un annuaire. (...) " L'association Civitas demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'ordonner au Conseil supérieur de l'audiovisuel (CSA) de mettre en œuvre les pouvoirs qu'il tient de ces dispositions afin notamment de faire cesser la possibilité pour les mineurs d'accéder à huit sites internet comportant des contenus à caractère pornographique. <br/>
<br/>
              4. Toutefois, l'association requérante, qui indique que plusieurs associations intéressées ont saisi le CSA depuis le 27 novembre 2020 pour qu'il fasse appliquer les dispositions citées au point précédent, et produit le courrier qu'elle a elle-même adressé en ce sens au président du CSA le 11 janvier 2021, se borne à faire valoir que la possibilité pour les mineurs d'accéder sans grande difficulté à des contenus pornographiques en ligne perdure depuis plusieurs années. Alors qu'au demeurant, sa requête ne fait que réitérer sans apporter aucun élément nouveau, l'argumentation qu'elle avait déjà présentée au soutien de sa requête n° 452885, rejetée par ordonnance du 1er juin 2021, elle ne justifie d'aucun élément de nature à caractériser une situation d'urgence exigeant une intervention du juge des référés dans les 48 heures sur le fondement de l'article L. 521-2 du code de justice administrative. <br/>
<br/>
              5. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur l'existence d'une atteinte grave et manifestement illégale à une liberté fondamentale, que la condition d'urgence n'est pas remplie. Par suite, il y a lieu de rejeter la requête, y compris les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative, selon la procédure prévue par l'article L. 522-3 du même code.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de l'association Civitas est rejetée. <br/>
Article 2 : La présente ordonnance sera notifiée à l'association Civitas.<br/>
Copie en sera adressée au Conseil supérieur de l'audiovisuel.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
