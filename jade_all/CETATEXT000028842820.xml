<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028842820</ID>
<ANCIEN_ID>JG_L_2014_04_000000348972</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/84/28/CETATEXT000028842820.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème / 4ème SSR, 11/04/2014, 348972</TITRE>
<DATE_DEC>2014-04-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>348972</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème / 4ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GADIOU, CHEVALLIER ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:348972.20140411</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 3 mai et 29 juillet 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour le Syndicat des réseaux radiophoniques nationaux (SRN), dont le siège est 22, rue Bayard à Paris (75008), représenté par son président ; le Syndicat des réseaux radiophoniques nationaux demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 15 février 2011 par laquelle le Conseil supérieur de l'audiovisuel a agréé l'acquisition de l'intégralité du capital de La Radio de la mer SA par la société Arthur World Participation Group et la syndication de programmes des services La Radio de la mer et Ouï FM, ainsi que la décision du 4 avril 2011 par laquelle il a agréé la modification de la convention de La Radio de la mer proposée par la société Arthur World Participation Group ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement d'une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu les notes en délibéré, enregistrées les 3 et 9 avril 2014, présentées pour la société Arthur World Participation Group et la société Ouï FM ;<br/>
<br/>
              Vu la loi n° 86-1067 du 30 septembre 1986 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gadiou, Chevallier, avocat de la société Arthur World Participation Group et de la société Ouï FM et à la SCP Piwnica, Molinié, avocat de la société Contact FM ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que, par une décision du 15 février 2011, le Conseil supérieur de l'audiovisuel a donné son agrément à l'acquisition de l'intégralité du capital social de La Radio de la mer SA, titulaire des autorisations d'émettre un service radiophonique de catégorie D de même nom consacré à la mer et au monde marin dans huit zones littorales, par la société Arthur World Participation Group, qui exploite le service radiophonique de catégorie D Ouï FM, et à la fusion des programmes de ces deux services ; que, le 4 avril 2011, il a décidé de conclure avec La Radio de la mer SA une convention définissant les obligations particulières d'un service de catégorie D dénommé " La Radio de la mer - Programme Ouï FM " ; que le Syndicat des réseaux radiophoniques nationaux demande l'annulation pour excès de pouvoir de ces deux décisions ; <br/>
<br/>
              Sur la fin de non-recevoir opposée par le Conseil supérieur de l'audiovisuel : <br/>
<br/>
              2. Considérant que les décisions attaquées ont pour effet d'étendre aux zones dans lesquelles La Radio de la mer SA bénéficiait d'une autorisation d'émettre la diffusion des programmes du service Ouï FM ; que le syndicat requérant, qui regroupe les exploitants de plusieurs radios musicales de catégorie D, concurrentes de Ouï FM et faisant, comme ce dernier service, appel au marché publicitaire national, justifie d'un intérêt pour agir à l'encontre de ces décisions ; <br/>
<br/>
              Sur la légalité interne des décisions attaquées : <br/>
<br/>
              3. Considérant qu'aux termes de l'article 29 de la loi du 30 septembre 1986 relative à la liberté de communication : " (...) l'usage des fréquences pour la diffusion de services de radio par voie hertzienne terrestre est autorisé par le Conseil supérieur de l'audiovisuel (...) Le conseil accorde les autorisations en appréciant l'intérêt de chaque projet pour le public, au regard des impératifs prioritaires que sont la sauvegarde du pluralisme des courants d'expression socio-culturels, la diversification des opérateurs, et la nécessité d'éviter les abus de position dominante ainsi que les pratiques entravant le libre exercice de la concurrence " ; qu'aux termes de l'article 42-3 de la même loi, dans sa rédaction alors applicable : " L'autorisation peut être retirée, sans mise en demeure préalable, en cas de modification substantielle des données au vu desquelles l'autorisation avait été délivrée, notamment des changements intervenus dans la composition du capital social ou des organes de direction et dans les modalités de financement " ; qu'il résulte de ces dispositions que, lorsque des circonstances nouvelles sont susceptibles de conduire à une modification substantielle des données au vu desquelles une autorisation a été délivrée, le titulaire de cette autorisation peut, ainsi d'ailleurs que le prévoit le plus souvent la convention conclue, en application de l'article 28 de la loi, avec le Conseil supérieur de l'audiovisuel, saisir celui-ci, afin qu'il lui fasse savoir si, dans l'hypothèse où les modifications en cause seraient effectives, il serait conduit à user de son pouvoir de mettre fin à l'autorisation ou s'il peut agréer l'opération qui lui a été soumise ; qu'il incombe au Conseil supérieur de l'audiovisuel, saisi d'une telle demande, de déterminer, en prenant en compte les circonstances de fait et de droit à la date où il se prononce, notamment en ce qui concerne la diversité des opérateurs, si les modifications envisagées sont, eu égard, le cas échéant, aux engagements pris par les opérateurs intéressés pour en atténuer ou en compenser les effets, de nature à compromettre l'impératif fondamental de pluralisme et l'intérêt du public et justifient, par suite, une abrogation de l'autorisation initialement accordée ;<br/>
<br/>
              4. Considérant qu'en l'espèce, il ressort des pièces du dossier que la convention conclue le 13 mars 2007 entre le Conseil supérieur de l'audiovisuel et La Radio de la mer SA prévoyait que ce service émettrait, après une période de démarrage de dix-huit mois, une durée quotidienne de 1 heure 45 minutes d'informations, de 4 heures 15 minutes de modules parlés consacrés à la mer et de 8 heures de musique d'ambiance, majoritairement composée de titres de plus de trois ans dits " golds ", pour la plupart instrumentaux ; que la nouvelle convention conclue le 5 avril 2011 entre le Conseil supérieur de l'audiovisuel et La Radio de la mer SA pour le service " La Radio de la mer - Programme Ouï FM " prévoit que les modules parlés de La Radio de la mer seront intégrés à une émission matinale d'une durée de 4 heures dénommée " Ouï Love la mer " ; que cette émission comprendra, dans des proportions non précisées, des chroniques thématiques consacrées à l'actualité régionale en plus des chroniques thématiques consacrées à la mer ; qu'elle comprendra également des publicités et un programme musical non précisé, dont la teneur sera définie en fonctions d'enquêtes réalisées auprès du public des zones de diffusion ; que le reste du programme du nouveau service sera constitué, à raison de 20 heures par jour, par la reprise du programme national de Ouï FM, consacré au rock ; qu'ainsi, la convention du 5 avril 2011 prévoit une modification très large du programme de La Radio de la mer, dans lequel le thème et les modules originaux de ce service occuperont une place résiduelle ; que ces modifications sont de nature à porter atteinte à l'intérêt du public, dont il n'est pas allégué qu'il devait être apprécié différemment dans les zones en cause à la date des décisions attaquées par rapport à la date à laquelle les autorisations d'exploiter le service La Radio de la mer avaient été délivrées ; que si La Radio de la mer SA s'est engagée à diffuser l'émission " Ouï Love la mer " à une heure de forte audience, cette circonstance ne saurait suffire à compenser l'altération du programme précédemment autorisé ; qu'au surplus, le document que la société produit pour justifier de cet engagement concerne uniquement la part d'audience de Ouï FM en Île-de-France et n'est pas de nature à établir que la période de diffusion choisie correspondrait à une heure de forte audience, ni même de forte part d'audience, pour le nouveau service ; que si La Radio de la mer SA s'est également engagée à diffuser quotidiennement 14 sessions thématiques consacrées à la mer d'une durée cumulée de 17 minutes 30, ces sessions, dont la durée et le nombre décroissent très fortement après 13 heures, ne sont pas plus, eu égard à leur caractère ponctuel, de nature à préserver l'intérêt du public, tel qu'il a été pris en compte par le Conseil supérieur de l'audiovisuel lors de la délivrance de l'autorisation initiale du service ; qu'ainsi, les engagements pris par l'opérateur ne sont pas de nature à compenser ou à atténuer dans une proportion suffisante les effets des modifications envisagées ; que le Conseil supérieur de l'audiovisuel a commis une erreur d'appréciation en estimant que ces modifications ne justifiaient pas, dans les circonstances de l'espèce, le retrait des autorisations dont bénéficiait cet opérateur ; que par suite, sans qu'il soit besoin d'examiner les autres moyens de la requête, le Syndicat des réseaux radiophoniques nationaux est fondé à demander l'annulation des décisions par lesquelles le Conseil supérieur de l'audiovisuel a agréé ces modifications ; <br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement d'une somme de 3 000 euros au Syndicat des réseaux radiophoniques nationaux au titre des dispositions de l'article L.761-1 du code de justice administrative ; que ces mêmes dispositions font obstacle à ce qu'une somme soit mise à la charge de ce syndicat, qui n'est pas, dans la présente instance, la partie perdante ;  <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les décisions des 15 février et 4 avril 2011 du Conseil supérieur de l'audiovisuel sont annulées. <br/>
<br/>
Article 2 : L'Etat versera la somme de 3 000 euros au Syndicat des réseaux radiophoniques nationaux au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : Les conclusions présentées par la SAS Ouï FM, par MeA..., en qualité d'administrateur judiciaire de la société Contact FM et par la société AWPG au titre des mêmes dispositions sont rejetées. <br/>
<br/>
Article 4 : La présente décision sera notifiée au Conseil supérieur de l'audiovisuel, au Syndicat des réseaux radiophoniques nationaux, à la SAS Ouï FM, à la société AWPG et à MeA..., en qualité d'administrateur judiciaire de la société Contact FM.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-01-04-02-02 PROCÉDURE. INTRODUCTION DE L'INSTANCE. INTÉRÊT POUR AGIR. EXISTENCE D'UN INTÉRÊT. SYNDICATS, GROUPEMENTS ET ASSOCIATIONS. - SERVICES DE RADIO - AGRÉMENT PAR LE CSA D'UNE OPÉRATION D'ACQUISITION - SYNDICAT D'EXPLOITANTS DE RADIOS CONCURRENTES [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">56-04-01 RADIO ET TÉLÉVISION. SERVICES PRIVÉS DE RADIO ET DE TÉLÉVISION. SERVICES DE RADIO. - INTÉRÊT POUR AGIR CONTRE L'AGRÉMENT DONNÉ PAR LE CSA À UNE OPÉRATION D'ACQUISITION - EXISTENCE - SYNDICAT D'EXPLOITANTS DE RADIOS CONCURRENTES [RJ1].
</SCT>
<ANA ID="9A"> 54-01-04-02-02 Un syndicat regroupant des exploitants de radios concurrentes d'un service radiophonique et  faisant, comme ce dernier service, appel au marché publicitaire national, justifie d'un intérêt lui donnant qualité pour agir contre l'agrément donné par le Conseil supérieur de l'audiovisuel (CSA) à l'acquisition, par la société titulaire de l'autorisation d'émettre ce service, d'autres sociétés exploitant des services radiophoniques dans la même catégorie.</ANA>
<ANA ID="9B"> 56-04-01 Un syndicat regroupant des exploitants de radios concurrentes d'un service radiophonique et  faisant, comme ce dernier service, appel au marché publicitaire national, justifie d'un intérêt lui donnant qualité pour agir contre l'agrément donné par le Conseil supérieur de l'audiovisuel (CSA) à l'acquisition, par la société titulaire de l'autorisation d'émettre ce service, d'autres sociétés exploitant des services radiophoniques dans la même catégorie.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., pour l'intérêt à agir d'un exploitant, CE, Section, 29 janvier 1993, Société N.R.J, n° 121953, p. 17.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
