<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044205296</ID>
<ANCIEN_ID>JG_L_2021_10_000000445838</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/20/52/CETATEXT000044205296.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 13/10/2021, 445838, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-10-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>445838</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>Mme Dorothée Pradines</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:445838.20211013</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme M... T... et M. B... O... ont demandé au tribunal administratif de Nancy, d'une part, de recompter les bulletins de vote ainsi que les bulletins de vote déclarés nuls lors des opérations électorales qui se sont déroulées le 15 mars 2020 pour l'élection des conseillers municipaux de la commune de Cons-la-Grandville et, d'autre part, d'annuler ces opérations électorales. Par un jugement n° 2000904 du 30 septembre 2020, le tribunal administratif a annulé les opérations électorales qui se sont déroulées les 15 mars et 28 juin 2020 pour l'élection des conseillers municipaux de la commune de Cons-la-Grandville.<br/>
<br/>
              Par une requête, un mémoire complémentaire et un mémoire en réplique, enregistrés les 30 octobre, 30 novembre et 23 décembre 2020 au secrétariat du contentieux du Conseil d'Etat, Mme AE... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de rejeter les protestations de Mme T... et de M. O... ;<br/>
<br/>
              3°) de mettre à la charge de Mme T... la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code électoral ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Dorothée Pradines, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Célice, Texidor, Perier, avocat de Mme AE... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. À l'issue des opérations électorales qui se sont déroulées le 15 mars 2020 pour le renouvellement des conseillers municipaux de Cons-la-Grandville (Meurthe-et-Moselle), commune de moins de 1 000 habitants, douze des quinze sièges de conseillers municipaux ont été pourvus, les trois restants ayant été pourvus lors des opérations électorales qui se sont déroulées le 28 juin 2020. Mme AE... relève appel du jugement du 30 septembre 2020 par lequel le tribunal administratif de Nancy a annulé les opérations électorales des 15 mars 2020 et 28 juin 2020 pour l'élection des conseillers municipaux de Cons-la-Grandville.<br/>
<br/>
              Sur les conclusions relatives aux opérations électorales : <br/>
<br/>
              2. D'une part, aux termes de l'article L. 66 du code électoral : " Les bulletins ne contenant pas une désignation suffisante ou dans lesquels les votants se sont fait connaître, les bulletins trouvés dans l'urne sans enveloppe ou dans des enveloppes non réglementaires, les bulletins écrits sur papier de couleur, les bulletins ou enveloppes portant des signes intérieurs ou extérieurs de reconnaissance, les bulletins ou enveloppes portant des mentions injurieuses pour les candidats ou pour des tiers n'entrent pas en compte dans le résultat du dépouillement. / Mais ils sont annexés au procès-verbal ainsi que les enveloppes non réglementaires et contresignés par les membres du bureau. / Chacun de ces bulletins annexés doit porter mention des causes de l'annexion. / Si l'annexion n'a pas été faite, cette circonstance n'entraîne l'annulation des opérations qu'autant qu'il est établi qu'elle a eu pour but et pour conséquence de porter atteinte à la sincérité du scrutin. "  <br/>
<br/>
              3. D'autre part, aux termes de l'article R. 68 du code électoral : " Les pièces fournies à l'appui des réclamations et des décisions prises par le bureau, ainsi que les feuilles de pointage sont jointes au procès-verbal. /Les bulletins autres que ceux qui, en application de la législation en vigueur, doivent être annexés au procès-verbal sont détruits en présence des électeurs ".<br/>
<br/>
              4. En premier lieu, il résulte de l'instruction que le procès-verbal des opérations électorales qui se sont déroulées le 15 mars 2020 pour l'élection des conseillers municipaux de la commune de Cons-la-Grandville mentionnait l'existence de sept bulletins nuls au motif qu'ils comportaient plus de noms que de candidats à élire et que le choix de l'électeur ne pouvait être déterminé avec certitude. Il n'est pas contesté que ces sept bulletins ont été déclarés nuls au motif que l'orthographe du nom de M. AF... P... sur certains bulletins de vote ne correspondait pas à celle figurant sur la déclaration de candidature faite en préfecture. Ainsi que l'a jugé à bon droit le tribunal administratif, cette simple erreur matérielle portant sur l'orthographe du patronyme de M. P..., dont il n'est pas contesté que ce dernier est bien inscrit sur les listes électorales de la commune de Cons-la-Grandville et alors qu'il n'est pas soutenu qu'il existait un risque d'homonymie avec d'autres candidats ou de confusion sur l'identité du candidat, n'était pas de nature à induire en erreur les électeurs sur l'identité de M. P... ni, par suite, à altérer la sincérité du scrutin. <br/>
<br/>
              5. En second lieu, il résulte de l'instruction, d'une part, que ces sept bulletins n'ont pas été pris en compte pour déterminer le nombre total de suffrages exprimés, lequel a été arrêté en l'espèce à 223, d'autre part, que si les suffrages exprimés en faveur de M. P... par ces bulletins de vote ont été regardés comme nuls par les scrutateurs, les suffrages exprimés par les mêmes bulletins en faveur des autres candidats y figurant ont, en revanche, été pris en compte dans le décompte des voix qu'ils ont recueillies. Par suite, les contradictions entre le décompte de la totalité des suffrages exprimés et le décompte des suffrages exprimés en faveur des candidats figurant sur les sept bulletins de vote litigieux, alors que plusieurs candidats ont été proclamés élus avec un nombre de suffrages, compris entre 113 et 115, qui ne dépasse pas la majorité absolue une fois réintégrés les sept bulletins invalidés à tort, ont eu une incidence sur la sincérité du décompte des voix et des suffrages exprimés de nature à altérer la sincérité du scrutin. Ainsi, Mme AE... n'est pas fondée à soutenir que le jugement attaqué serait illégal en ce qu'il annule, pour ce motif, l'ensemble des opérations électorales, sans qu'ait d'incidence à cet égard la circonstance, qui ressort des pièces produites pour la première fois en appel, que les bulletins invalidés n'auraient pas été incinérés.<br/>
<br/>
              6. Il résulte de ce qui précède que Mme AE... n'est pas fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Nancy a annulé les opérations électorales qui se sont tenues les 15 mars et 28 juin 2020 pour l'élection des conseillers municipaux dans la commune de Cons-la-Grandville.<br/>
<br/>
              Sur les conclusions de Mme T... tendant à l'inéligibilité de Mme AE... :<br/>
<br/>
              7. Si Mme T... soutient devant le Conseil d'Etat que Mme AE... aurait accompli des manœuvres frauduleuses ayant pour objet ou pour effet de porter atteinte à la sincérité du scrutin, les circonstances qu'elle mentionne à cet égard, notamment le constat d'huissier réalisé à la demande de Mme AE... postérieurement aux opérations électorales, ne sont pas de nature à caractériser de telles manœuvres. Dès lors, ses conclusions tendant à ce que Mme AE... soit déclarée inéligible ne peuvent, en tout état de cause, qu'être rejetées.<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              8. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par Mme T... devant le Conseil d'Etat au titre des dispositions de l'article L. 761-1 du code de justice administrative. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de Mme T..., qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de Mme AE... est rejetée.<br/>
Article 2 : Les conclusions présentées par Mme T... aux fins de déclarer Mme AE... inéligible et au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à Mme AD... AE..., à Mme M... T... et à M. B... O....<br/>
Copie en sera adressée à M. L... H..., à M. S... I..., à M. W... AB..., à M. V... U..., à M. F... C..., à M. AA... N..., à Mme Y... Q..., à Mme A... D..., à Mme X... AG..., à M. E... G..., à M. L... H..., à M. R... Z..., à M. K... J..., à M. L... AC... et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
