<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032928890</ID>
<ANCIEN_ID>JG_L_2016_07_000000396214</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/92/88/CETATEXT000032928890.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 22/07/2016, 396214</TITRE>
<DATE_DEC>2016-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>396214</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Sophie-Caroline de Margerie</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:396214.20160722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et deux mémoires en réplique, enregistrés les 19 janvier, 18 mai et 7 juin 2016 au secrétariat du contentieux du Conseil d'Etat, M. A...B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision du 22 octobre 2015 par laquelle l'Agence française de lutte contre le dopage a prononcé à son encontre la sanction d'interdiction de participer pendant un an aux manifestations sportives autorisées ou organisées par la Fédération française de cyclisme, la Fédération française de cyclotourisme, la Fédération française de triathlon, la Fédération française du sport d'entreprise, la Fédération sportive et culturelle de France, la Fédération sportive et gymnique du travail et l'Union française des oeuvres laïques d'éducation physique ;<br/>
<br/>
              2°) de mettre à la charge de l'Agence française de lutte contre le dopage la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du sport ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie-Caroline de Margerie, conseiller d'Etat,<br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de M.B..., et à la SCP Matuchansky, Poupot, Valdelièvre, avocat de l'Agence française de lutte contre le dopage ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'aux termes de l'article L. 232-17 du code du sport : " " I.-Se soustraire, tenter de se soustraire ou refuser de se soumettre aux contrôles prévus aux articles L. 232-12 à L. 232-16, ou de se conformer à leurs modalités, est passible des sanctions administratives prévues par les articles L. 232-21 à L. 232-23 " ; qu'en vertu du 3° de l'article L. 232-22 du même code, l'Agence française de lutte contre le dopage " peut réformer les décisions prises en application de l'article L. 232-21. Dans ces cas, l'agence se saisit, dans un délai de deux mois à compter de la réception du dossier complet, des décisions prises par les fédérations agréées (...) " ; qu'en vertu de l'article L. 232-23 du même code, l'Agence peut prononcer à l'encontre des sportifs ayant enfreint les dispositions de l'article L. 232-17 une interdiction temporaire ou définitive de participer aux manifestations organisées ou autorisées par les fédérations sportives françaises ; <br/>
<br/>
              2.	Considérant qu'il résulte de l'instruction que M. A...B..., coureur professionnel sous contrat au sein de l'équipe cycliste de l'armée de terre et licencié auprès de la Fédération française de cyclisme, a remporté l'épreuve de cyclisme sur route dite " La Gainsbarre ", qui s'est tenue le 11 avril 2015 à Port-Bail (Manche) ; qu'à l'initiative de l'Agence française de lutte contre le dopage, il a été procédé à l'issue de cette course à un contrôle antidopage pour lequel douze participants, dont M.B..., ont été convoqués par voie d'affichage ; que M. B...ne s'est pas présenté à ce contrôle ; que, pour ce motif, il a fait l'objet d'une sanction disciplinaire prononcée le 10 juin 2015 par la commission nationale de discipline de la Fédération française de cyclisme, lui interdisant, pour une durée de quatre mois, de participer aux compétitions et manifestations sportives autorisées ou organisées par la Fédération française de cyclisme et annulant l'ensemble de ses résultats obtenus depuis le 11 avril 2015 ; que, le 2 juillet 2015,  l'Agence française de lutte contre le dopage a décidé de se saisir du dossier, sur le fondement du pouvoir de réformation que lui confèrent les dispositions précitées du 3° de l'article L. 232-22 du code du sport ; que, par une décision du 22 octobre 2015, l'Agence a prononcé à l'encontre de M. B...une sanction d'interdiction de participer pour une durée d'un an aux manifestations sportives autorisées ou organisées par la Fédération française de cyclisme, la Fédération française de cyclotourisme, la Fédération française de triathlon, la Fédération française du sport d'entreprise, la Fédération sportive et culturelle de France, la Fédération sportive et gymnique du travail et l'Union française des oeuvres laïques d'éducation physique, sous déduction de la période d'interdiction déjà purgée en application de la sanction  prononcée par la Fédération française de cyclisme, et confirmé l'annulation des résultats obtenus par M. B...; que, par une ordonnance du 12 février 2016, le juge des référés du Conseil d'Etat a suspendu l'exécution de cette décision dont M. B... demande l'annulation par la présente requête ;<br/>
<br/>
              3.	Considérant qu'aux termes de l'article D. 232-47 du code du sport : " Une notification du contrôle est remise au sportif désigné pour être contrôlé par la personne chargée du contrôle ou par une personne désignée par elle. (...) La notification précise la date, l'heure, le lieu et la nature du contrôle. Elle doit être signée par le sportif et remise ou transmise sans délai à la personne chargée du contrôle ou à la personne désignée par elle. / Pour les sportifs désignés pour être contrôlés qui ne s'entraînent pas dans un lieu fixe, ou en cas de circonstances particulières ne permettant pas la notification du contrôle par écrit, l'agence fixe les modalités permettant de garantir l'origine et la réception de cette notification. Les fédérations sportives agréées en assurent la diffusion auprès des intéressés. / Le refus de prendre connaissance, de signer ou de retourner la notification est constitutif d'un refus de se soumettre aux mesures de contrôle " ; qu'aux termes de la délibération n° 296 prise en vertu de ces dispositions réglementaires le 12 septembre 2013 par le collège de l'Agence française du lutte contre le dopage : " Pour les compétitions cyclistes de quelque nature qu'elles soient, la personne chargée du contrôle porte à la connaissance de l'organisateur, par tout moyen, l'identité des coureurs désignés pour le contrôle au plus tard avant l'arrivée du vainqueur de l'épreuve. / La liste des coureurs qui sont tenus de se présenter pour le prélèvement d'échantillons doit être affichée, à l'initiative de l'organisateur, aussi bien à proximité immédiate de la ligne d'arrivée qu'à l'entrée du poste de contrôle du dopage. / Les intéressés sont identifiés par leur nom ou par leur numéro de dossard ou, s'il y a lieu, par leur place au classement. / Tout coureur, même en l'absence de notification écrite du contrôle, doit, dans les dix minutes suivant le franchissement par lui de la ligne d'arrivée, se rendre à l'emplacement où la liste des personnes soumises au contrôle a été affichée et, s'il y a lieu, rejoindre immédiatement le poste de contrôle du dopage. (...) " ;<br/>
<br/>
              4.	Considérant qu'au regard, d'une part, des dispositions précitées de l'article D. 232-47 du code du sport qui permettent de déroger, en cas de circonstances particulières, au principe selon lequel le sportif désigné pour se soumettre à un contrôle antidopage doit être informé par écrit de cette obligation, d'autre part, des conditions spécifiques de l'arrivée d'une course cycliste, le collège de l'Agence française de lutte contre le dopage pouvait, comme il l'a fait par la délibération du 12 septembre 2013, prévoir des modalités particulières de notification pour les contrôles pratiqués à l'issue de courses cyclistes ; que cette délibération, sans exclure la remise d'une notification écrite, prévoit que l'information des coureurs désignés pour un contrôle est assurée, d'une part, par l'indication de leur identité aux organisateurs de la manifestation et, d'autre part, par la voie d'un double affichage ; qu'au demeurant, l'information par voie d'affichage est une modalité utilisée par l'Union cycliste internationale ; qu'ainsi, elle assure des garanties suffisantes de l'origine et la réception de cette notification ; que, dès lors, le moyen invoquant, par voie d'exception, l'illégalité de cette délibération, ne peut qu'être écarté ; <br/>
<br/>
              5.	Mais, considérant qu'il résulte de l'instruction que la tenue d'un contrôle antidopage à l'issue de la course cycliste sur route du 11 avril 2015 a fait l'objet d'une information par voie d'affichage à proximité de la ligne d'arrivée ; que cet affichage a été effectué à l'aide d'un formulaire à en-tête de la Fédération française de cyclisme et de l'Union cycliste internationale, et initialement conçu pour comporter le nom de dix coureurs, intitulé " Tableau des coureurs à contrôler " ; que ce tableau faisait en l'espèce apparaître la nature et la date de l'épreuve, le lieu du contrôle ainsi qu'une liste comportant les noms de douze coureurs dont, en dernier, M. B...; qu'il est constant que, contrairement aux prescriptions de la délibération du 12 septembre 2013, le tableau n'a pas fait l'objet d'un autre affichage à l'entrée du poste de contrôle ; que le nom de M. B...étant le dernier mentionné sur le tableau affiché, la personne désignée par l'équipe de M. B...pour vérifier quels étaient les coureurs concernés par le contrôle a cru que ce dernier ne l'était pas, à la différence de l'un de ses coéquipiers, mais qu'il figurait seulement sur la liste de réserve ; qu'en outre, l'escorte qui s'est présentée devant le bus de l'équipe a procédé à une notification du contrôle à ce seul coéquipier, qu'elle a accompagné jusqu'au lieu du contrôle ; qu'il n'est pas contesté que la personne désignée par l'équipe de M. B...pour vérifier le tableau d'affichage a interrogé à deux reprises les commissaires de la course, qui lui ont confirmé que ce dernier n'était pas concerné par le contrôle ; que M.B..., vainqueur de la course, est en outre resté pendant quarante minutes environ dans l'aire du podium, à la disposition des organisateurs de la manifestation, lesquels n'avaient, par ailleurs, pas fait procéder à une information par haut-parleur ; que, par suite, il ne saurait être regardé comme s'étant délibérément soustrait au contrôle au sens des dispositions de l'article L. 232-17 du code du sport ; <br/>
<br/>
              6.	Considérant qu'il résulte de ce qui précède et sans qu'il soit besoin d'examiner les autres moyens de la requête, que M. B...est fondé à demander l'annulation de la décision du 22 octobre 2015 de l'Agence française de lutte contre le dopage ;<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              7.	Considérant qu'il y a lieu, dans les circonstances de l'espèce, de faire application de ces dispositions et de mettre à la charge de l'Agence française de lutte contre le dopage la somme de 3 000 euros au titre des frais exposés par M. B...et non compris dans les dépens ; que ces dispositions font obstacle à ce que soit mise à la charge de M. B..., qui n'est pas, dans la présente instance, la partie perdante, la somme que l'Agence française de lutte contre le dopage demande au même titre ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La décision du 22 octobre 2015 de l'Agence française de lutte contre le dopage est annulée.<br/>
<br/>
Article 2 : L'Agence française de lutte contre le dopage versera une somme de 3 000 euros à M. B... au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. A...B...et à l'Agence française de lutte contre le dopage.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">63-05-05 SPORTS ET JEUX. SPORTS. - NOTIFICATION DES CONTRÔLES ANTIDOPAGE - NOTIFICATION PAR ÉCRIT REMISE CONTRE SIGNATURE, SAUF CIRCONSTANCES PARTICULIÈRES (ART. D. 232-47 DU CODE DU SPORT) - NOTION DE CIRCONSTANCES PARTICULIÈRES - CAS D'UNE COURSE CYCLISTE.
</SCT>
<ANA ID="9A"> 63-05-05 L'article D. 232-47 du code du sport prévoit que la décision de soumettre un sportif à un contrôle antidopage doit lui être notifiée par écrit contre signature, mais permet à l'Agence française de lutte contre le dopage (AFLD) de fixer d'autres modalités en cas de circonstances particulières ne permettant pas la notification du contrôle par écrit. Au regard des conditions spécifiques de l'arrivée d'une course cycliste, le collège de l'AFLD pouvait, comme il l'a fait par une délibération du 12 septembre 2013, prévoir des modalités particulières de notification pour les contrôles pratiqués à l'issue de courses cyclistes. Cette délibération, sans exclure la remise d'une notification écrite, prévoit que l'information des coureurs désignés pour un contrôle est assurée, d'une part, par l'indication de leur identité aux organisateurs de la manifestation et, d'autre part, par la voie d'un double affichage. Au demeurant l'information par voie d'affichage est une modalité utilisée par l'Union cycliste internationale.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
