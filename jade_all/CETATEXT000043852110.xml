<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043852110</ID>
<ANCIEN_ID>JG_L_2021_07_000000444943</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/85/21/CETATEXT000043852110.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 23/07/2021, 444943, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>444943</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUIN-PALAT, BOUCARD</AVOCATS>
<RAPPORTEUR>M. Charles-Emmanuel Airy</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:444943.20210723</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société par actions simplifiée (SAS) Silo Huningue a demandé au tribunal administratif de Strasbourg de prononcer la décharge des cotisations de taxe foncière sur les propriétés bâties auxquelles elle a été assujettie au titre des années 2017 et 2018 dans les rôles de la commune de Village-Neuf (Haut-Rhin), à raison des bâtiments situés 14, rue du Rhône et 9001, rue du Rhin. Par un jugement n° 1902452 du 7 août 2020, ce tribunal a fait droit à sa demande.  <br/>
<br/>
              Par un pourvoi enregistré le 28 septembre 2020 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'économie, des finances et de la relance demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, à titre principal de rejeter la demande présentée par la société Silo Huningue, et à titre subsidiaire de mettre les impositions litigieuses à la charge de la chambre de commerce et d'industrie Sud Alsace Mulhouse.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général de la propriété des personnes publiques ;<br/>
              - le code général des impôts ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Charles-Emmanuel Airy, auditeur,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Thouin-Palat, Boucard, avocat de la société Silo Huningue ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société par actions simplifiée (SAS) Silo Huningue a conclu en octobre 2016 avec Voies Navigables de France (VNF) et avec la Chambre de commerce et d'industrie Sud Alsace Mulhouse (CCISAM), une convention d'amodiation l'autorisant à occuper une parcelle du domaine public fluvial pour y exercer une activité d'exploitation de silos à céréales, d'entrepôts, de dispositifs de chargement et de déchargement et de toutes installations annexes et connexes. Pour les besoins de cette activité, elle a procédé à l'édification sur cette parcelle d'un silo à raison duquel elle a été assujettie à la taxe foncière sur les propriétés bâties au titre des années 2017 et 2018. Après rejet de sa réclamation préalable, elle a saisi le tribunal administratif de Strasbourg d'une demande tendant à la décharge de ces impositions. Le ministre de l'économie, des finances et de la relance se pourvoit en cassation contre le jugement du 7 août 2020 par lequel le magistrat délégué par le président de ce tribunal a prononcé cette décharge. <br/>
<br/>
              2. Aux termes de l'article 1400 du code général des impôts : " I. Sous réserve des dispositions des articles 1403 et 1404, toute propriété, bâtie ou non bâtie, doit être imposée au nom du propriétaire actuel. / II. Lorsqu'un immeuble est grevé d'usufruit ou loué soit par bail emphytéotique, soit par bail à construction, soit par bail à réhabilitation ou fait l'objet d'une autorisation d'occupation temporaire du domaine public constitutive d'un droit réel, la taxe foncière est établie au nom de l'usufruitier, de l'emphytéote, du preneur à bail à construction ou à réhabilitation ou du titulaire de l'autorisation. / (...) ". Aux termes de l'article 2122-6 du code général de la propriété des personnes publiques : " Le titulaire d'une autorisation d'occupation temporaire du domaine public de l'Etat a, sauf prescription contraire de son titre, un droit réel sur les ouvrages, constructions et installations de caractère immobilier qu'il réalise pour l'exercice d'une activité autorisée par ce titre. Ce droit réel confère à son titulaire, pour la durée de l'autorisation et dans les conditions et les limites précisées dans le présent paragraphe, les prérogatives et obligations du propriétaire ".<br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond qu'aux termes de l'article 14 de la convention d'amodiation conclue entre la société Silo Huningue, la CCISAM et VNF : " La présente convention n'est pas constitutive de droits réels au sens de l'article L. 2122-6 du code général de la propriété des personnes publiques " et qu'aux termes de l'article 21.1 de cette même convention : " L'amodiataire demeure propriétaire de toutes les constructions qu'il a édifiées lors de la précédente autorisation (cf. annexe 4). " S'il résulte des stipulations de l'article 14 de cette convention, ainsi que l'a relevé le magistrat délégué, que la société Silo Huningue n'est pas titulaire de droits réels sur les constructions nouvelles qu'elle l'autorise à réaliser, l'article 21.1 de la convention prévoit que cette société demeure propriétaire des constructions qu'elle édifiées en vertu des précédentes autorisations.<br/>
<br/>
              4. Dès lors, en se fondant, pour prononcer la décharge des impositions établies au nom de la société Silo Huningue, sur la seule circonstance qu'elle n'était pas titulaire de droits réels sur les constructions édifiées en vertu de l'autorisation conférée par la convention conclue en octobre 2016, de sorte qu'elle ne pouvait être assujettie à la taxe foncière sur les propriétés bâties à raison de ces constructions sur le fondement des dispositions précitées du II de l'article 1400 du code général des impôts, sans rechercher si la société était, compte tenu de la date de sa construction et en conformément aux stipulations de l'article 21.1 de la convention, propriétaire du silo en litige et par suite susceptible d'être assujettie à la taxe foncière sur les propriétés bâties à raison de celui-ci en application du I du même article, le magistrat désigné par le tribunal administratif de Strasbourg a entaché son jugement d'erreur de droit.<br/>
<br/>
              5. Par suite, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, le ministre de l'économie, des finances et de la relance est fondé à demander l'annulation du jugement qu'il attaque.<br/>
<br/>
              6. Les dispositions de l'article L 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 7 août 2020 du tribunal administratif de Strasbourg est annulé. <br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Strasbourg. <br/>
Article 3 : Les conclusions de la société Silo Huningue sur le fondement de l'article L. 761-1 du code de justice administrative sont rejetées.  <br/>
Article 4 : La présente décision sera notifiée au ministre de l'économie, des finances et de la relance et à la société par actions simplifiée Silo Huningue.  <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
