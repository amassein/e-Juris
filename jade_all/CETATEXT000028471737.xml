<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028471737</ID>
<ANCIEN_ID>JG_L_2014_01_000000365553</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/47/17/CETATEXT000028471737.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 15/01/2014, 365553, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-01-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>365553</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>Mme Sophie-Caroline de Margerie</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:365553.20140115</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 28 janvier et 29 avril 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. A...B..., demeurant ... ; M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 12PA02784 du 28 novembre 2012 par lequel la cour administrative d'appel de Paris a, sur la requête du préfet de police, annulé le jugement n° 1021564 du 7 juin 2012 par lequel le tribunal administratif de Paris a annulé la décision du 29 novembre 2010 du préfet de police refusant d'abroger l'arrêté d'expulsion pris à son encontre le 7 février 1995 et rejeté sa demande d'annulation de cette décision ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel du préfet de police ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en audience publique :<br/>
<br/>
              - le rapport de Mme Sophie-Caroline de Margerie, Conseiller d'Etat,<br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, Rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de M. B...; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M.B..., ressortissant israélien, a fait l'objet de deux condamnations, les 3 mai 1988 et 3 juillet 1990, pour des infractions à la législation sur les stupéfiants, à des peines, respectivement, de dix mois puis de neuf ans d'emprisonnement ; que, par arrêté du 7 février 1995, le ministre de l'intérieur a prononcé son expulsion du territoire français ; qu'en février 2010, l'intéressé a présenté une demande d'abrogation de cet arrêté d'expulsion, qui a été rejetée le 29 novembre suivant ; qu'il a formé un recours pour excès de pouvoir devant le tribunal administratif de Paris qui, par jugement en date du 7 juin 2012, a annulé la décision refusant d'abroger l'arrêté d'expulsion ; que la cour administrative d'appel de Paris a annulé ce jugement par un arrêt du 28 novembre 2012, contre lequel M. B...se pourvoit en cassation ; <br/>
<br/>
              2.	Considérant qu'aux termes de l'article L. 521-1 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Sous réserve des dispositions des articles L. 521-2, L. 521-3 et L. 521-4, l'expulsion peut être prononcée si la présence en France d'un étranger constitue une menace grave pour l'ordre public " ; qu'aux termes des dispositions de l'article L. 524-1 du même code : " L'arrêté d'expulsion peut à tout moment être abrogé. Lorsque la demande d'abrogation est présentée à l'expiration d'un délai de cinq ans à compter de l'exécution effective de l'arrêté d'expulsion, elle ne peut être rejetée qu'après avis de la commission prévue à l'article L. 522-1, devant laquelle l'intéressé peut se faire représenter " ; qu'aux termes des dispositions de l'article L. 524-2 du même code : " Sans préjudice des dispositions de l'article L. 524-1, les motifs de l'arrêté d'expulsion donnent lieu à un réexamen tous les cinq ans à compter de la date d'adoption de l'arrêté. L'autorité compétente tient compte de l'évolution de la menace pour l'ordre public que constitue la présence de l'intéressé en France, des changements intervenus dans sa situation personnelle et familiale et des garanties de réinsertion professionnelle ou sociale qu'il présente, en vue de prononcer éventuellement l'abrogation de l'arrêté (...) " ;<br/>
<br/>
              3.	Considérant qu'aux termes de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " 1. Toute personne a droit au respect de sa vie privée et familiale, de son domicile et de sa correspondance./ 2. Il ne peut y avoir ingérence d'une autorité publique dans l'exercice de ce droit que pour autant que cette ingérence est prévue par la loi et qu'elle constitue une mesure qui, dans une société démocratique, est nécessaire à la sécurité nationale, à la sûreté publique, au bien-être économique du pays, à la défense de l'ordre et à la prévention des infractions pénales, à la protection de la santé ou de la morale, ou à la protection des droits et libertés d'autrui " ; <br/>
<br/>
              4.	Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M.B..., qui est entré en France en 1986, s'est marié en 1988 avec une ressortissante française avec laquelle il a eu quatre enfants nés en France en 1988, 1994, 1996 et 2000 ; que si la cour administrative d'appel a relevé que l'intéressé " n'établit ni le caractère permanent de sa vie familiale avec sa femme et ses enfants, ni sa participation à l'éducation et à l'entretien de ses derniers, ni la nécessité de sa présence auprès de son fils atteint de troubles ", de tels motifs ne peuvent toutefois être utilement pris en compte pour apprécier si le refus d'abroger un arrêté d'expulsion, qui a pour effet d'interdire à l'intéressé de séjourner légalement en France avec sa famille, porte une atteinte disproportionnée au droit au respect de sa vie privée et familiale ; que, par suite, en en déduisant qu'alors même que les condamnations prononcées à l'encontre de M. B... pour infractions à la législation sur les stupéfiants étaient anciennes et que la commission de nouveaux délits n'était pas formellement établie, le refus d'abroger l'arrêté d'expulsion n'avait pas porté d'atteinte disproportionnée au droit au respect de la vie privée et familiale de l'intéressé, la cour administrative d'appel a commis une erreur de droit ; <br/>
<br/>
              5.	Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, M. B...est fondé à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              6.	Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              7.	Considérant qu'il ressort des pièces du dossier que M. B...est marié avec une ressortissante française avec laquelle il a eu quatre enfants, de nationalité française, qui sont mineurs et vivent en France ; que s'il a fait l'objet, en 1990, d'une condamnation à neuf ans d'emprisonnement pour infraction à la législation sur les stupéfiants, il ne ressort pas des éléments versés au dossier que ce comportement délictueux aurait persisté ; que les autres infractions qui lui sont reprochées ne sont pas établies par les pièces du dossier, à l'exception de la conduite d'un véhicule sans permis ; qu'eu égard à l'absence de gravité de ces derniers faits et au caractère ancien des autres faits, malgré leur gravité, le refus opposé par le préfet de police le 29 novembre 2010 d'abroger l'arrêté d'expulsion pris à l'encontre de M. B...le 7 février 1995 porte, dans les circonstances de l'espèce, une atteinte au droit au respect de sa vie privée et familiale une atteinte disproportionnée aux buts en vue desquels il a été pris ;<br/>
<br/>
              8.	Considérant qu'il résulte de ce qui précède que le préfet de police n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Paris a annulé sa décision du 29 novembre 2010 refusant d'abroger l'arrêté d'expulsion pris à l'encontre de M. B...le 7 février 1995 ; <br/>
<br/>
              9.	Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3 000 euros à verser à M. B...au titre de l' article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 28 novembre 2012 est annulé.<br/>
<br/>
Article 2 : La requête du préfet de police devant la cour administrative d'appel de Paris est rejetée.<br/>
<br/>
Article 3 : L'Etat versera à M. B...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Article 4 : La présente décision sera notifiée à M. A...B...et au ministre de l'intérieur. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
