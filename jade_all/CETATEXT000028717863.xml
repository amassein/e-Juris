<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028717863</ID>
<ANCIEN_ID>JG_L_2014_03_000000360970</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/71/78/CETATEXT000028717863.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 12/03/2014, 360970, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-03-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>360970</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>Mme Véronique Rigal</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Delphine Hedary</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:360970.20140312</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 11 juillet et 8 octobre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. et MmeA..., demeurant au ... ; M. et Mme A...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09MA02251 du 11 mai 2012 par lequel la cour administrative d'appel de Marseille a rejeté leur requête tendant à l'annulation du jugement du 23 avril 2009 par lequel le tribunal administratif de Marseille a rejeté leur demande tendant à la décharge des cotisations supplémentaires d'impôt sur le revenu auxquelles ils ont été assujettis au titre des années 1999 et 2000 ainsi que des pénalités correspondantes ;<br/>
<br/>
              2°) réglant l'affaire au fond, de prononcer la décharge des impositions litigieuses ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Véronique Rigal, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Delphine Hedary, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Odent, Poulet, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. et Mme A...ont fait l'objet du 23 septembre 2001 au 5 septembre 2002 d'un examen de situation fiscale personnelle portant sur les années 1998 à 2000 ; qu'un redressement leur a été notifié au titre de l'année 1999 dans la catégorie des revenus d'origine indéterminée, résultant de l'appréhension par M. A...d'une somme en espèces de 3 850 000 francs (586 929 euros) en exécution d'un ordre de paiement du 17 février 1999 de la société Animations Loisirs Congo (ALC) obtenu par l'administration dans le cadre de son droit de communication prévu aux articles L. 82 C et L. 101 du livre des procédures fiscales ; que M. et Mme A...ont contesté les cotisations supplémentaires en découlant devant le tribunal administratif de Marseille qui, par un jugement du 23 avril 2009, a rejeté leur demande en décharge ; que ce jugement a été confirmé par la cour administrative de Marseille par un arrêt du 11 mai 2012 contre lequel les requérants se pourvoient ;<br/>
<br/>
              Sur le moyen tiré d'une contradiction de motifs :<br/>
<br/>
              2. Considérant qu'en vertu des articles L. 16, dans sa rédaction alors en vigueur, et L. 69 du livre des procédures fiscales, l'administration peut, lorsqu'elle a réuni des éléments permettant d'établir qu'un contribuable peut avoir des revenus plus importants que ceux qu'il a déclarés, lui demander des justifications et, s'il s'abstient de répondre à cette demande ou n'apporte pas de justifications suffisantes, le taxer d'office à l'impôt sur le revenu ; que l'administration peut comparer les crédits figurant sur les comptes bancaires ou les comptes courants d'un contribuable au montant brut de ses revenus déclarés pour établir l'existence d'indices de revenus dissimulés l'autorisant à demander à l'intéressé des justifications sans procéder à un examen critique préalable de ces crédits ni, quand elle l'a fait, à se référer comme terme de comparaison aux seuls crédits dont l'origine n'est pas justifiée après le premier examen ; que, toutefois, elle n'est en droit d'user de cette procédure à l'égard de ce contribuable qu'à la condition que les sommes ainsi portées au crédit de ses comptes équivalent au moins au double de ses revenus connus ; qu'il en résulte que la possibilité de recourir à une demande de justification s'apprécie indépendamment de l'établissement ultérieur du caractère imposable des sommes en cause ;<br/>
<br/>
              3. Considérant qu'il ressort des énonciations de l'arrêt attaqué que la cour administrative d'appel a constaté, au terme d'une appréciation souveraine, qu'après déduction des virements de compte à compte identifiés, l'écart de plus du double entre le montant des crédits enregistrés sur les comptes bancaires de M. et Mme A...et celui des revenus bruts qu'ils avaient déclarés pour l'année 1999 permettait d'établir qu'ils avaient pu disposer de revenus plus importants que ceux qu'ils avaient déclarés sans qu'il soit besoin d'y inclure la somme de 3 850 000 francs (586 929 euros) dont la disposition était contestée par les requérants ; qu'en conséquence la cour n'a pas entaché sa décision d'une contradiction de motifs en estimant inopérante l'objection des requérants relative à la disposition de la somme de 3 850 000 francs (586 929 euros) puis en estimant fondé le redressement y afférent ;<br/>
<br/>
              Sur les moyens tirés d'une erreur de droit et d'une dénaturation des pièces du dossier :<br/>
<br/>
              4. Considérant, d'une part, qu'aux termes de l'article 156 du code général des impôts : " L'impôt sur le revenu est établi d'après le montant total du revenu net annuel dont dispose chaque foyer fiscal (...) " ; qu'aux termes de l'article L. 16 du livre des procédures fiscales, dans sa version applicable au litige : " En vue de l'établissement de l'impôt sur le revenu, l'administration peut demander au contribuable des éclaircissements. / (...) Elle peut également lui demander des justifications lorsqu'elle a réuni des éléments permettant d'établir que le contribuable peut avoir des revenus plus importants que ceux qu'il a déclarés. " ; qu'enfin, en application de l'article L. 69 du livre des procédures fiscales : " Sous réserve des dispositions particulières au mode de détermination des bénéfices industriels et commerciaux, des bénéfices agricoles et des bénéfices non commerciaux, sont taxés d'office à l'impôt sur le revenu les contribuables qui se sont abstenus de répondre aux demandes d'éclaircissements ou de justifications prévues à l'article L. 16. " ;<br/>
<br/>
              5. Considérant, d'autre part, qu'en vertu de l'article L. 12 du livre des procédures fiscales, un examen de situation fiscale personnelle ne peut s'étendre sur une période supérieure à un an et s'achève par l'envoi d'une notification de redressement qui, conformément à l'article L. 57 du livre des procédures fiscales, doit être motivée de manière à permettre au contribuable de présenter utilement ses observations ; que l'administration peut produire ultérieurement un élément nouveau, notamment issu du résultat d'une demande d'assistance administrative internationale, sans que cela constitue un prolongement de l'examen de situation fiscale personnelle si elle ne fonde pas un nouveau redressement sur cet élément ; qu'un tel document, qui ne constitue alors qu'un élément de preuve supplémentaire, doit être versé au débat contradictoire ;<br/>
<br/>
              6. Considérant que la cour a relevé que l'administration fiscale avait en sa possession un courrier, à l'en-tête de la société ALC, comportant l'indication du numéro de compte et des coordonnées détaillées de M. A...et ordonnant au Crédit foncier de Monaco de mettre à la disposition de celui-ci une somme en espèce de 3 850 000 francs (586 929 euros) le vendredi 19 février 1999 à 11h30 ; qu'elle a souverainement estimé que M. et Mme A...ne pouvaient être regardés comme ayant apporté des explications de nature à démontrer que cet ordre de paiement n'avait pas été exécuté et qu'ils n'avaient pas appréhendé personnellement cette somme en raison du caractère général, imprécis et parfois contradictoire des réponses qu'ils avaient faites à l'administration et que, de ce fait, ils avaient été régulièrement taxés d'office ; que par suite, en estimant que les renseignements obtenus par l'administration française auprès des autorités monégasques à la suite d'une demande d'assistance administrative internationale du 2 octobre 2003, postérieurement à l'envoi de la notification de redressement du 5 septembre 2002 clôturant les opérations de contrôle sur place, venaient corroborer et non établir le redressement initialement notifié, la cour n'a pas dénaturé les pièces du dossier ; <br/>
<br/>
              7. Considérant qu'ainsi qu'il vient d'être dit, la cour a relevé que l'administration disposait, dès l'envoi de la notification de redressement, des éléments établissant l'appréhension par les époux A...du versement de la société ALC faute pour eux d'avoir apporté la preuve contraire et qu'elle ne les a assujettis à aucun nouveau redressement qui se serait fondé sur les éléments recueillis dans le cadre de la demande d'assistance internationale ; qu'en déduisant de ces faits, qu'elle n'a pas dénaturés, que l'examen de situation fiscale personnelle était achevé le 5 septembre 2002, date de la notification de redressement, et que la production ultérieure du résultat de la procédure d'assistance administrative internationale, versée au débat contradictoire, était sans incidence sur la régularité de la procédure d'imposition, la cour n'a pas commis d'erreur de droit ;<br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que M. et Mme A...ne sont pas fondés à demander l'annulation de l'arrêt de la cour administrative d'appel de Marseille ; que leurs conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi de M. et Mme A...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. et Mme B...A...et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
