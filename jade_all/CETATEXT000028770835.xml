<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028770835</ID>
<ANCIEN_ID>JG_L_2014_03_000000365523</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/77/08/CETATEXT000028770835.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 24/03/2014, 365523, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-03-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>365523</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Pascal Trouilly</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:365523.20140324</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête et le mémoire complémentaire, enregistrés les 25 janvier et 13 février 2013 au secrétariat du contentieux du Conseil d'Etat, présentés par Mme B...A..., demeurant... ; MmeA...  demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision par laquelle le Premier ministre a implicitement refusé d'abroger l'article 2 du décret n° 2011-601 du 27 mai 2011 relatif aux majorations de durée d'assurance pour enfants des assurés sociaux du régime général, du régime agricole et des régimes de retraite des artisans, commerçants, professions libérales, avocats et ministres des cultes et membres des congrégations et collectivités religieuses ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le remboursement de la contribution pour l'aide juridique prévue à l'article R. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la sécurité sociale ; <br/>
<br/>
              Vu la loi n° 2009-1646 du 24 décembre 2009 ; <br/>
<br/>
              Vu le décret  n° 2011-601 du 27 mai 2011 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pascal Trouilly, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que l'article 65 de la loi du 24 décembre 2009 de financement de la sécurité sociale pour 2010 a modifié l'article L. 351-4 du code de la sécurité sociale en prévoyant notamment une majoration de quatre trimestres de la durée d'assurance, au bénéfice des femmes assurées sociales, pour chacun de leurs enfants, au titre de l'incidence sur leur vie professionnelle de la maternité, ainsi que, au bénéfice du père ou de la mère assuré social, une majoration de durée d'assurance de quatre trimestres, attribuée pour chaque enfant mineur au titre de son éducation pendant les quatre années suivant sa naissance ; que ce même article a étendu ce dispositif aux régimes des professions libérales et des avocats, en insérant dans le code de la sécurité sociale un article L. 723-10-1-1, aux termes duquel : " Les assurés du présent régime bénéficient des dispositions prévues à l'article L. 351-4, adaptées en tant que de besoin par décret pour tenir compte des modalités particulières de calcul de la pension de ce régime " ; que, pour l'application de ces dispositions législatives, est intervenu le décret du 27 mai 2011, dont l'article 2 a modifié l'article R. 173-15 du code de la sécurité sociale ; que selon ce dernier article, dans sa nouvelle rédaction : " Les majorations de durée d'assurance prévues à l'article L. 351-4 sont accordées, par priorité, par le régime général de sécurité sociale lorsque l'assuré a été affilié successivement, alternativement ou simultanément à ce régime et aux régimes de protection sociale agricole, des professions artisanales, des professions industrielles et commerciales, des professions libérales, des avocats ou des ministres des cultes et membres des congrégations et collectivités religieuses. / Lorsque l'intéressé a été affilié successivement, alternativement ou simultanément à un ou plusieurs des régimes mentionnés à l'alinéa précédent à l'exception du régime général, les majorations de durée d'assurance sont accordées par le régime auquel l'intéressé a été affilié en dernier lieu et, subsidiairement, en cas d'affiliations simultanées, par le régime susceptible d'attribuer la pension la plus élevée (...) " ; que Mme A... demande l'annulation pour excès de pouvoir du refus implicite du Premier ministre d'abroger l'article 2 du décret du 27 mai 2011, en soutenant que cet article méconnaît le principe d'égalité ; <br/>
<br/>
              2. Considérant que le principe d'égalité ne s'oppose pas à ce que l'autorité investie du pouvoir réglementaire règle de façon différente des situations différentes ni à ce qu'elle déroge à l'égalité pour des raisons d'intérêt général pourvu que, dans l'un comme l'autre cas, la différence de traitement qui en résulte soit en rapport direct avec l'objet de la norme qui l'établit et ne soit pas manifestement disproportionnée au regard des motifs susceptibles de la justifier ; <br/>
<br/>
              3. Considérant, d'une part, que Mme A...soutient que l'article 2 du décret du 27 mai 2011 crée une discrimination entre, d'une part, les avocats ayant cotisé uniquement au régime des avocats et, d'autre part, ceux qui ont cotisé d'abord au régime général, au titre d'emplois exercés pendant leurs années d'études, puis au régime des avocats ; que, toutefois, la situation des avocats ayant cotisé à plusieurs régimes est différente de la situation de ceux qui ont seulement cotisé au régime des avocats ; qu'afin d'éviter que les premiers ne bénéficient d'un cumul de majorations de la durée d'assurance pour enfants, le pouvoir réglementaire a prévu un mécanisme de coordination consistant à attribuer la priorité à l'un des régimes auquel l'assuré a été affilié ; que la différence de traitement qui en résulte est ainsi en rapport direct avec l'objet des dispositions du décret litigieux et n'est pas manifestement disproportionnée au regard du motif qui la justifie ; <br/>
<br/>
              4. Considérant, d'autre part, que Mme A...soutient que les dispositions précitées de l'article R. 173-15 du code de la sécurité sociale, issues de l'article 2 du décret du 27 mai 2011, créent une différence de traitement entre les avocats qui ont cotisé au régime général, qui relèvent du premier alinéa de cet article, et ceux qui ont cotisé à un autre régime de non-salariés, qui relèvent de son second alinéa, dès lors que, dans le premier cas, la priorité est accordée automatiquement au régime général, alors que, dans le second, elle est accordée en principe au régime auquel l'intéressé a été affilié en dernier lieu ; que, toutefois, ces dispositions, dont les effets sur les " coefficients de proratisation " résultant des durées respectives d'affiliation aux différents régimes dépendent des caractéristiques du parcours professionnel de chaque assuré, ne peuvent être regardées comme créant, entre les catégories d'avocats qu'identifie la requérante,  une différence de traitement contraire au principe d'égalité ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que Mme A...n'est pas fondée à demander l'annulation de la décision qu'elle attaque ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit à ses conclusions tendant  à ce que la contribution pour l'aide juridique prévue à l'article R. 761-1, dans sa rédaction alors en vigueur, du code de justice administrative soit mise à la charge de l'Etat ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de Mme A...est rejetée.<br/>
Article 2 : La contribution pour l'aide juridique est laissée à la charge de MmeA....<br/>
Article 3 : La présente décision sera notifiée à Mme B...A..., au Premier ministre et à la ministre des affaires sociales et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
