<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028052026</ID>
<ANCIEN_ID>JG_L_2013_10_000000372280</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/05/20/CETATEXT000028052026.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 07/10/2013, 372280, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-10-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>372280</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY ; SCP THOUIN-PALAT, BOUCARD</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2013:372280.20131007</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 18 septembre 2013 au secrétariat du contentieux du Conseil d'Etat, présentée par M. B...A..., demeurant... ; le requérant demande au juge des référés du Conseil d'Etat :<br/>
<br/>
              1°) d'ordonner, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de la décision du 3 septembre 2013 par laquelle le directeur de l'Institut d'études politiques de Rennes a émis un avis défavorable à la liste des candidats adoptée par délibération du conseil d'administration de l'établissement en date du 31 mai 2013 ;<br/>
<br/>
              2°) de mettre à la charge de l'Institut d'études politiques de Rennes le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              3°) de mettre à la charge de cet établissement le versement de la somme de 35 euros au titre de l'article R. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              il soutient que : <br/>
              - sa requête est recevable ;<br/>
              - la condition d'urgence est remplie dès lors que la décision contestée porte une atteinte grave et immédiate à ses intérêts professionnels ainsi qu'à sa situation personnelle et aux intérêts de l'établissement ;<br/>
              - il existe un doute sérieux quant à la légalité de la décision contestée ;<br/>
              - la décision est insuffisamment motivée ;  <br/>
              - la décision est entachée d'une erreur de droit, dès lors qu'elle est fondée sur une appréciation des mérites scientifiques des candidats et non sur des considérations tenant à l'administration de l'établissement ;<br/>
              - la décision est entachée d'une erreur d'appréciation, dès lors qu'il ne ressort pas des intitulés et du descriptif de cours cités que la géographie ait été insuffisamment prise en compte dans la définition du profil de poste ; en tout état de cause, que les candidats retenus sont compatibles avec les besoins pédagogiques exposés dans la décision contestée ;<br/>
              - la décision est entachée d'un détournement de pouvoir, dès lors que l'avis du 28 juin 2013 et celui du 3 septembre 2013 sont fondés sur des motifs différents  révélant l'existence d'un refus de principe de la part du directeur de l'Institut d'Etudes Politiques de Rennes; <br/>
<br/>
<br/>
              Vu la décision dont la suspension de l'exécution est demandée ;<br/>
<br/>
              Vu la copie de la requête à fin d'annulation de la décision contestée ;<br/>
              Vu le mémoire en défense, enregistré le 27 septembre 2013, présenté pour l'Institut d'études politiques de Rennes, qui conclut au rejet de la requête et à ce qu'il soit mis à la charge de M. A...la somme de 3 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ; il soutient que : <br/>
<br/>
              - la condition d'urgence n'est pas remplie ; en effet ni la situation professionnelle ni la situation personnelle du requérant, qui est maître de conférences à l'université Rennes 1, n'est modifiée par la décision qu'il conteste ; l'intéressé pourra en tout état de cause faire acte de candidature dans le cadre d'une nouvelle procédure de recrutement ;  <br/>
              - aucun des moyens de la requête n'est de nature à susciter un doute sérieux quant à la légalité de la décision contestée ;<br/>
<br/>
              Vu les observations, enregistrées le 27 septembre 2013, présentées par le ministre de l'enseignement supérieur et de la recherche ; <br/>
<br/>
              Vu le mémoire en réplique, enregistré le 30 septembre 2013, présenté par M. A... qui reprend les conclusions de sa requête, par les mêmes moyens ; il conclut également à ce qu'il soit enjoint au directeur de l'IEP de Rennes de statuer à nouveau sur la liste de candidats ; <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
              Vu le code de l'éducation ;<br/>
<br/>
              Vu la loi n° 79-587 du 11 juillet 1979 ; <br/>
<br/>
              Vu la loi n°84-52 du 26 janvier 1984 ;<br/>
<br/>
              Vu le décret n° 84-431 du 6 juin 1984 ;<br/>
<br/>
              Vu le décret n° 89-902 du 18 décembre 1989 ;<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M.A..., d'autre part, l'Institut d'études politiques de Rennes ainsi que la ministre de l'enseignement supérieur et de la recherche ; <br/>
              Vu le procès-verbal de l'audience publique du 1er octobre 2013 à 9 heures 30 au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Boucard, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. A... ;<br/>
<br/>
              - M. A...;<br/>
<br/>
              - le représentant de M.A... ;<br/>
<br/>
              - Me Coudray, avocat au Conseil d'Etat et à la Cour de cassation, avocat de l'Institut d'études politiques de Rennes ;<br/>
<br/>
              - le représentant de l'Institut d'études politiques de Rennes ;<br/>
<br/>
              - les représentants de la ministre de l'enseignement supérieur et de la recherche ;<br/>
              et à l'issue de laquelle le juge des référés a clôturé l'instruction ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; <br/>
<br/>
              2. Considérant que l'urgence justifie la suspension de l'exécution d'un acte administratif lorsque celle-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ; qu'il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte contesté sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue ; que l'urgence doit être appréciée objectivement et compte tenu de l'ensemble des circonstances de l'affaire ;<br/>
<br/>
              3. Considérant que, par une décision du 3 septembre 2013, le directeur de l'Institut d'études politiques de Rennes a émis un avis défavorable à la liste des candidats adoptée par délibération du conseil d'administration de l'établissement en date du 31 mai 2013 en vue du recrutement d'un professeur des universités en aménagement du territoire ; que pour justifier de l'urgence à suspendre l'exécution de cette décision, le requérant soutient que son exécution porterait atteinte de manière grave et immédiate à ses intérêts professionnels ainsi qu'à sa situation personnelle et aux intérêts de l'Institut d'études politiques de Rennes ; <br/>
<br/>
              4. Considérant, en premier lieu, qu'alors même qu'il se situe en deuxième position sur la liste des candidats adoptée par le conseil d'administration de l'établissement et que la candidate classée première a rejoint un autre poste, l'exécution de la décision contestée n'est pas de nature, par elle-même, à porter aux intérêts du requérant, qui est maître de conférences à l'université de Rennes 1 au sein de laquelle il dispense des cours et mène des activités de recherche, une atteinte caractérisant une situation d'urgence au sens de l'article L. 521-1 du code de justice administrative ;  <br/>
<br/>
              5. Considérant, en second lieu, qu'il ne ressort pas des pièces du dossier que la décision contestée porterait atteinte à la continuité et à la qualité de l'enseignement dispensé au sein de l'Institut d'études politiques de Rennes ; qu'en effet, dans l'attente du recrutement d'un professeur des Universités, les cours correspondant au service défini dans la fiche de poste seront assurés, au cours de l'année universitaire 2013-2014, par des enseignants vacataires ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que la condition d'urgence, requise par l'article L. 521-1 du code de justice administrative pour justifier la suspension immédiate de la décision contestée, n'est pas caractérisée ; que, sans qu'il soit besoin de statuer sur l'existence d'un doute sérieux quant à la légalité de la décision contestée, la requête de M. A... doit donc être rejetée y compris les conclusions présentées au titre des dispositions de l'article L. 761-1 du même code ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par l'Institut d'études politiques de Rennes au même titre ;<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. A...est rejetée. <br/>
Article 2 : Les conclusions de l'Institut d'études politiques de Rennes tendant à l'application de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente ordonnance sera notifiée à M. B...A..., à l'Institut d'études politiques de Rennes et à la ministre de l'enseignement supérieur et de la recherche.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
