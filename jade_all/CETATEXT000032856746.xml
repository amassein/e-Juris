<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032856746</ID>
<ANCIEN_ID>JG_L_2016_07_000000400820</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/85/67/CETATEXT000032856746.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 05/07/2016, 400820, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-07-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>400820</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER, PINATEL ; SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2016:400820.20160705</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Procédure contentieuse antérieure :<br/>
<br/>
              L'association des chapelles catholiques et apostoliques et la société Garibaldi ont demandé au juge des référés du tribunal administratif de Paris, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre au préfet de police de leur accorder le concours de la force publique et de prendre toutes mesures nécessaires pour assurer l'exécution de l'ordonnance du 6 janvier 2016 par laquelle le président du tribunal de grande instance de Paris a prescrit l'expulsion immédiate de tous occupants sans droit ni titre de l'immeuble et du terrain situés 27, rue François Bonvin à Paris (15ème arrondissement). Par une ordonnance n° 1607469/9 du 27 mai 2016, le juge des référés du tribunal administratif de Paris a enjoint au préfet de police d'apporter le concours de la force publique pour l'expulsion de tout occupant sans droit ni titre de cette propriété.<br/>
<br/>
              Par une requête en tierce opposition, M. A...B...a demandé au juge des référés du tribunal administratif de Paris, statuant sur le fondement de l'article L. 521-2 du même code, de déclarer non avenue l'ordonnance n° 1607469/9 du 27 mai 2016. Par une ordonnance n° 1608320/9 du 6 juin 2016, le juge des référés du tribunal administratif de Paris a fait droit à sa demande.<br/>
<br/>
              Procédure devant le juge des référés du Conseil d'Etat :<br/>
<br/>
              Par une requête, enregistrée le 21 juin 2016 au secrétariat du contentieux du Conseil d'Etat, l'association des chapelles catholiques et apostoliques et la société Garibaldi demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) à titre principal, de rejeter comme irrecevable la requête en tierce opposition formée par M.B... ;<br/>
<br/>
              3°) à titre subsidiaire, de faire droit à la demande d'injonction qu'ils avaient présentée sur le fondement de l'article L. 521-2 du code de justice administrative, sous une astreinte de 1 000 euros par jour de retard à compter de la décision à intervenir ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat le versement de la somme de 4 000 euros à chacune des requérantes au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Elles soutiennent que :<br/>
              - la requête en tierce opposition était irrecevable dès lors que l'ordonnance du 27 mai 2016 du juge des référés du tribunal administratif de Paris ne préjudiciait pas aux droits de M.B... ;<br/>
              - l'ordonnance attaquée est insuffisamment motivée ;<br/>
              - c'est à tort que l'auteur de l'ordonnance attaquée a jugé que l'ordonnance du 6 janvier 2016 du président du tribunal de grande instance de Paris était caduque à la date de la requête initiale dont elles avaient saisi le juge des référés du tribunal administratif de Paris ;<br/>
              - le juge des référés a commis une erreur de droit en ne rejetant pas au fond la requête en tierce opposition, dès lors que la condition d'urgence était remplie et que le refus du concours de la force publique porte une atteinte grave et manifestement illégale au droit de propriété.<br/>
              Par un mémoire en défense et un nouveau mémoire, enregistrés les 29 et 30 juin 2016, M. B...conclut au rejet de la requête et à ce que soit mise à la charge de l'association des chapelles catholiques et apostoliques et de la société Garibaldi la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative. Il soutient que sa requête en tierce opposition était recevable, que l'ordonnance du 6 janvier 2016 du président du tribunal de grande instance de Paris était caduque et que le constat de cette caducité ressortit à la seule compétence du juge judiciaire.<br/>
              Par un mémoire, enregistré le 30 juin 2016, le ministre de l'intérieur conclut au rejet de la requête. Il soutient, à titre principal, que la requête en tierce opposition de M. B...était recevable et que l'ordonnance attaquée, qui est suffisamment motivée, a à bon droit jugé caduque l'ordonnance du 6 janvier 2016 du président du tribunal de grande instance de Paris, et, à titre subsidiaire, que les conditions de mise en oeuvre de l'article L. 521-2 du code de justice administrative ne sont pas remplies.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, l'association des chapelles catholiques et apostoliques et la société Garibaldi, d'autre part, M.B... et le ministre de l'intérieur ;<br/>
<br/>
              Vu le procès-verbal de l'audience publique du 1er juillet 2016 à 15 heures au cours de laquelle ont été entendus :<br/>
<br/>
              - Me Pinatel, avocat au Conseil d'Etat et à la Cour de cassation, avocat de l'association des chapelles catholiques et apostoliques et de la société Garibaldi ;<br/>
<br/>
              - Me Rousseau, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M.B... ;<br/>
<br/>
              - le représentant de M.B... ;<br/>
<br/>
              - les représentants du ministre de l'intérieur ;<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction ; <br/>
<br/>
<br/>
<br/>
              1. Il résulte de l'instruction que l'association des chapelles catholiques et apostoliques, propriétaire du terrain situé 27, rue François Bonvin à Paris (15ème arrondissement) et de l'immeuble, dit église Sainte-Rita, qui y est édifié, a prévu d'y construire un ensemble de logements, dont la réalisation a été confiée à la société Garibaldi. Par une ordonnance du 6 janvier 2016, le président du tribunal de grande instance de Paris a ordonné l'expulsion des occupants sans droit ni titre de cet immeuble. En raison de l'impossibilité dans laquelle elles se sont trouvées d'obtenir l'exécution de cette ordonnance, l'association et la société ont, dès le 26 janvier 2016, saisi le préfet de police d'une demande de concours de la force publique. Par une ordonnance du 27 mai 2016, le juge des référés du tribunal administratif de Paris, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, a, à leur demande, enjoint au préfet de police d'apporter le concours de la force publique afin de veiller à l'expulsion de tout occupant du tènement. Toutefois, par une ordonnance du 6 juin 2016, le juge des référés du tribunal administratif de Paris a admis la tierce opposition formée par M. A...B...à l'encontre de l'ordonnance du 27 mai 2016, déclaré cette ordonnance non avenue et rejeté la requête qui avait été initialement présentée, sur le fondement de l'article L. 521-2 du code de justice administrative, par l'association des chapelles catholiques et apostoliques et la société Garibaldi. L'association et la société demandent au juge des référés du Conseil d'Etat d'annuler cette ordonnance.<br/>
<br/>
              2. Aux termes de l'article R. 832-1 du code de justice administrative : " Toute personne peut former tierce opposition à une décision juridictionnelle qui préjudicie à ses droits, dès lors que ni elle ni ceux qu'elle représente n'ont été présents ou régulièrement appelés dans l'instance ayant abouti à cette décision ". Le président du tribunal de grande instance de Paris a rendu l'ordonnance du 6 janvier 2016 prescrivant l'expulsion des occupants sans droit ni titre de l'immeuble litigieux en dérogation au principe de la contradiction, après avoir constaté que les occupants interrogés avaient refusé de décliner leur identité. C'est pour la même raison que le juge des référés du tribunal administratif de Paris a rendu son ordonnance du 27 mai 2016  sans pouvoir mettre en cause aucun des occupants sans droit ni titre de l'immeuble. M. B..., qui s'est ultérieurement présenté comme l'un de ces occupants et qui n'était pas désigné par l'ordonnance d'expulsion du 6 janvier 2016, n'était, dès lors, pas recevable à former tierce opposition contre cette ordonnance. Il suit de là que les requérantes sont fondées à soutenir que c'est à tort que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Paris a admis la tierce opposition de M. B... et déclaré non avenue l'ordonnance du 27 mai 2016.<br/>
<br/>
              3. L'annulation de l'ordonnance attaquée a pour effet de faire revivre l'ordonnance du 27 mai 2016. Si le ministre de l'intérieur a, par ses observations et les éléments avancés au cours de l'audience publique, souligné que la mobilisation de diverses personnes et associations autour de l'occupation de l'église ainsi que l'opposition de plusieurs élus locaux à son évacuation avaient accru, depuis la date à laquelle cette ordonnance a été rendue, les risques de troubles à l'ordre public en cas d'opération d'expulsion, dans un contexte d'état d'urgence et de déroulement du championnat d'Europe de football qui mobilise fortement les forces de l'ordre, il lui appartient, s'il s'y croit fondé, de saisir le juge des référés du tribunal administratif de Paris, sur le fondement de l'article L. 521-4 du code de justice administrative, d'une demande tendant à ce que ce juge modifie ou mette fin, au vu de ces éléments nouveaux, à l'injonction qu'il a prononcée par son ordonnance du 27 mai 2016.<br/>
<br/>
              4. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par l'association des chapelles catholiques et apostoliques et la société Garibaldi au titre de l'article L. 761-1 du code de justice administrative. Ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge des requérantes, qui ne sont pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : L'ordonnance du 6 juin 2016 du juge des référés du tribunal administratif de Paris est annulée.<br/>
Article 2 : Le surplus des conclusions de l'association des chapelles catholiques et apostoliques et de la société Garibaldi et les conclusions présentées par M. B...au titre de l'article L. 761-1 du code de justice administrative sont rejetés.<br/>
Article 3 : La présente ordonnance sera notifiée à l'association des chapelles catholiques et apostoliques, à la société Garibaldi, à M. A...B...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
