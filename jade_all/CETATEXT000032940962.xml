<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032940962</ID>
<ANCIEN_ID>JG_L_2016_07_000000392536</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/94/09/CETATEXT000032940962.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 27/07/2016, 392536, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-07-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>392536</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>M. Cyrille Beaufils</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:392536.20160727</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés les 10 août et 10 novembre 2015 au secrétariat du contentieux du Conseil d'Etat, la chambre syndicale nationale des géomètres topographes demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2015-649 du 10 juin 2015 modifiant les décrets n° 96-478 du 31 mai 1996 portant règlement de la profession de géomètre-expert et code des devoirs professionnels et n° 2010-1406 du 12 novembre 2010 relatif au diplôme de géomètre-expert foncier délivré par le Gouvernement, en tant qu'il ne dispense pas les géomètres-topographes souhaitant s'inscrire au tableau de l'ordre des géomètres-experts d'un stage d'une durée de deux ans ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ; <br/>
              - la loi n° 2014-366 du 24 mars 2014 ;<br/>
              - la loi n° 46-942 du 7 mai 1946 ;<br/>
              - le décret n° 2010-1406 du 12 novembre 2010 ;<br/>
              - le décret n° 96-478 du 31 mai 1996 ;<br/>
              - le décret n° 55-471 du 30 avril 1955 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Cyrille Beaufils, auditeur,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la chambre syndicale nationale des géomètres-topographes ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article 26 de la loi du 7 mai 1946 instituant l'ordre des géomètres experts, dans sa rédaction issue de la loi du 24 mars 2014 pour l'accès au logement et un urbanisme rénové : " Peuvent demander leur inscription au tableau de l'ordre les personnes exerçant la profession de géomètre-topographe, dans des conditions définies par décret en Conseil d'Etat, qui peuvent prévoir que le stage mentionné à l'article 4 est réalisé au sein de l'entreprise où ces personnes exercent leur activité " ; que l'article 4 du décret du 12 novembre 2010 relatif au diplôme de géomètre-expert foncier délivré par le Gouvernement prévoit que ce stage, d'une durée de deux ans, consiste en l'exécution de travaux professionnels s'inscrivant dans les activités décrites au 1° de l'article 1er de la loi du 7 mai 1946 précitée, effectués sous la surveillance et la responsabilité d'un membre de l'ordre des géomètres-experts ; que la chambre syndicale nationale des géomètres topographes demande l'annulation pour excès de pouvoir du décret du 10 juin 2015 modifiant le décret du 31 mai 1996 portant règlement de la profession de géomètre expert et code des devoirs professionnels et le décret du 12 novembre 2010 relatif au diplôme de géomètre expert foncier délivré par le Gouvernement, en tant qu'il ne dispense pas les géomètres-topographes souhaitant s'inscrire au tableau de l'ordre des géomètres-experts de la condition de réaliser un stage d'une durée de deux ans ou, à tout le moins, n'en réduit pas la durée ;<br/>
<br/>
              2. Considérant, en premier lieu, qu'aux termes de l'article 22 de la Constitution : " Les actes du Premier ministre sont contresignés, le cas échéant, par les ministres chargés de leur exécution " ; que la requérante ne fait état d'aucune mesure réglementaire ou individuelle que devraient nécessairement signer le ministre de l'économie, du redressement productif et du numérique et le ministre des finances et des comptes publics pour l'exécution du décret attaqué ; que la nécessité de telles mesures ne ressort pas des pièces du dossier ; qu'est à cet égard sans incidence la circonstance qu'en vertu des articles 6 et 30 du décret du 30 avril 1955 relatif à la rénovation et à la conservation du cadastre le directeur général des finances publiques est compétent pour délivrer les agréments pour l'établissement de documents d'arpentage et l'exécution de tous travaux cadastraux ; qu'il suit de là que le moyen tiré de ce que le décret attaqué serait entaché d'irrégularité faute d'avoir été contresigné par ces ministres ne peut qu'être écarté ; <br/>
<br/>
              3. Considérant, en second lieu, que la condition subordonnant l'inscription des géomètres-topographes au tableau de l'ordre des géomètres- experts est prévue par l'article 26 de la loi du 7 mai 1946 citée au point 1 ; que, contrairement à ce qui est soutenu par la requérante, cet article n'a pas instauré une " équivalence " entre l'expérience professionnelle antérieure des géomètres-topographes et celle que le stage a pour objet de leur permettre d'acquérir ; qu'en réservant aux géomètres-experts la réalisation des études et des travaux topographiques qui fixent les limites des biens fonciers, le législateur a entendu garantir la protection de la propriété foncière en confiant sa délimitation à des professionnels spécialement qualifiés et présentant des garanties d'indépendance et de probité ; que, si les missions des géomètres-topographes recoupent en partie celles des géomètres-experts, il ne ressort pas des pièces du dossier, eu égard à l'intention du législateur et aux spécificités de cette dernière profession, qu'en prévoyant, par l'article 2 du décret attaqué que la durée du stage, fixée à 2 ans par l'article 4 du décret du 12 novembre 2010 rappelé au point 1, peut être réduite d'un an au plus pour les personnes qui justifient de quinze ans au moins de pratique professionnelle dans les activités décrites à l'article 1er de la loi du 7 mai 1946, dont cinq ans au moins dans des fonctions d'encadrement, sans prévoir de modalités plus favorables, notamment quant à la durée du stage, pour les géomètres-topographes, l'auteur du décret aurait entaché celui-ci d'une erreur manifeste d'appréciation dans l'exercice de la faculté qui lui est donnée par la loi de fixer les conditions et la durée du stage des professionnels concernés ; <br/>
<br/>
              4. Considérant qu'il résulte de tout ce qui précède que la chambre syndicale nationale des géomètres-topographes n'est pas fondée à demander l'annulation du décret qu'elle attaque ; que sa requête doit, par suite, être rejetée, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de la chambre syndicale nationale des géomètres-topographes est rejetée. <br/>
<br/>
Article 2 : La présente décision sera notifiée à la chambre syndicale nationale des géomètres-topographes, à la ministre du logement et de l'habitat durable et à la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche. <br/>
		Copie en sera adressée au Premier ministre. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
