<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028622876</ID>
<ANCIEN_ID>JG_L_2014_02_000000350908</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/62/28/CETATEXT000028622876.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 05/02/2014, 350908, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-02-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>350908</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOUZIDI, BOUHANNA</AVOCATS>
<RAPPORTEUR>M. Jean-Luc Matt</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:350908.20140205</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 13 juillet 2011 au secrétariat du contentieux du Conseil d'État, présenté par le ministre du budget, des comptes publics et de la réforme de l'État, porte-parole du Gouvernement ; le ministre demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10BX01489 du 24 mai 2011 par lequel la cour administrative d'appel de Bordeaux a annulé partiellement le jugement n° 0602281-0703656-0803623 du 22 avril 2010 du tribunal administratif de Bordeaux et fait droit à la demande de la SNC PBM Import tendant à la décharge des cotisations de taxe professionnelle auxquelles elle a été assujettie au titre des années 2004 à 2006 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la SNC PBM Import ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Matt, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Bouzidi, Bouhanna, avocat de la SNC PBM Import ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du I quater de l'article 1466 A du code général des impôts, dans sa rédaction applicable aux impositions en litige : " Sauf délibération contraire de la collectivité territoriale ou du groupement de collectivités territoriales, les entreprises employant cinquante salariés au plus au 1er janvier 1997 ou à la date de leur création, si elle est postérieure, bénéficient de l'exonération de taxe professionnelle à compter du 1er janvier 1997 dans les conditions prévues au I ter, pour leurs établissements situés dans les zones franches urbaines définies au B du 3 de l'article 42 modifié de la loi n° 95-115 du 4 février 1995 et dont la liste figure au I de l'annexe à la loi n° 96-987 du 14 novembre 1996 relative à la mise en oeuvre du pacte de relance pour la ville. (...) / Pour les établissements existant au 1er janvier 1997 et ceux ayant fait l'objet d'une création entre cette date et le 1er janvier 2008, d'une extension ou d'un changement d'exploitant entre cette date et le 31 décembre 2001, cette exonération est accordée dans la limite d'un montant de base nette imposable fixé à 3 millions de francs. (...) " ; <br/>
<br/>
              2. Considérant qu'il résulte des termes mêmes de ces dispositions que, s'agissant des établissements situés dans une zone franche urbaine ayant fait l'objet d'un changement d'exploitant, seuls ceux pour lesquels ce changement est intervenu avant le 31 décembre 2001 peuvent bénéficier de l'exonération de taxe professionnelle à hauteur des montants prévus par le I quater de l'article 1466 A du code général des impôts, alors même que cette disposition renvoie, pour certaines de ses modalités de mise en oeuvre, au I ter du même article, applicable aux établissements situés dans des zones de redynamisation urbaine et que ce dernier prévoit, pour les établissements en question, qu'à " compter du 1er janvier 2002, en cas de changement d'exploitant au cours de la période d'exonération, celle-ci est maintenue pour la période restant à courir et dans les conditions prévues pour le prédécesseur " ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société Pinault Aquitaine, établie dans une zone franche urbaine, a bénéficié d'une exonération de taxe professionnelle à ce titre pour les années 2002 et 2003 ; que la SNC PBM Import a repris cette société le 1er décembre 2003 ; que la cour administrative d'appel de Bordeaux a jugé que la SNC pouvait continuer à bénéficier de cette exonération, dès lors qu'elle n'était pas tenue de satisfaire à la condition tenant au nombre de salariés employés dans l'entreprise prévue par le I quater de l'article 1466 A du code général des impôts compte tenu du renvoi, opéré par ce même paragraphe I quater, au paragraphe I ter du même article ; qu'en statuant ainsi, alors qu'il résulte de ce qui a été dit ci-dessus que, dès lors qu'elle avait repris la société Pinault Aquitaine postérieurement au 31 décembre 2001, la SNC PBM Import ne pouvait pas légalement bénéficier de l'exonération de taxe professionnelle établie en faveur de certains établissements situés dans les zones franches urbaines, la cour a commis une erreur de droit ; que son arrêt doit donc être annulé ;<br/>
<br/>
              3. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'État qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt du 24 mai 2011 de la cour administrative d'appel de Bordeaux est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Bordeaux.<br/>
Article 3 : Les conclusions présentées par la SNC PBM Import au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au ministre de l'économie et des finances et à la SNC PBM Import.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
