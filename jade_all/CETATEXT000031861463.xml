<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031861463</ID>
<ANCIEN_ID>JG_L_2016_01_000000390036</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/86/14/CETATEXT000031861463.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème SSJS, 06/01/2016, 390036, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-01-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>390036</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>Mme Marie-Anne Lévêque</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2016:390036.20160106</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>M. A...B...a saisi le tribunal administratif de Pau d'une demande d'annulation de la décision du ministre de la défense du 17 mai 2013 par laquelle il a rejeté sa demande d'indemnisation tendant au bénéfice de la loi du 5 janvier 2010 relative à la reconnaissance et à l'indemnisation des victimes des essais nucléaires français. Par un jugement n° 1202252 du 30 juin 2014, le tribunal administratif de Pau a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 14BX02283 du 10 mars 2015, la cour administrative d'appel de Bordeaux a annulé ce jugement et la décision du 17 mai 2013 du ministre de la défense, et lui a enjoint de présenter à l'intéressé une proposition d'indemnisation des préjudices subis du fait de l'affection dont il est atteint. <br/>
<br/>
              Par un pourvoi, enregistré le 11 mai 2015 au secrétariat du contentieux du Conseil d'Etat, le ministre de la défense demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la requête de M.B....<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 2010-2 du 5 janvier 2010 ;<br/>
              - le décret n° 2010-653 du 11 juin 2010, modifié par le décret n° 2012-604 du 30 avril 2012 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Anne Lévêque, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de M. B...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article 1er de la loi du 5 janvier 2010 relative à la reconnaissance et à l'indemnisation des victimes des essais nucléaires français : " Toute personne souffrant d'une maladie radio-induite résultant d'une exposition à des rayonnements ionisants dus aux essais nucléaires français et inscrite sur une liste fixée par décret en Conseil d'Etat conformément aux travaux reconnus par la communauté scientifique internationale peut obtenir réparation intégrale de son préjudice dans les conditions prévues par la présente loi. Si la personne est décédée, la demande de réparation peut être présentée par ses ayants droit. " ; que l'article 2 de cette même loi définit les conditions de temps et de lieu de séjour ou de résidence que le demandeur doit remplir ; qu'aux termes du I de l'article 4 de cette même loi, dans sa version applicable au litige : " Les demandes d'indemnisation sont soumises à un  comité d'indemnisation (...) " et qu'aux termes du II de ce même article : " Ce comité examine si les conditions de l'indemnisation sont réunies. Lorsqu'elles le sont, l'intéressé bénéficie d'une présomption de causalité à moins qu'au regard de la nature de la maladie et des conditions de son exposition le risque attribuable aux essais nucléaires puisse être considéré comme négligeable. Le comité le justifie auprès de l'intéressé (...) " ; qu'aux termes de l'article 7 du décret du 11 juin 2010 modifié, pris pour l'application de la loi du 5 janvier 2010 : " (...) Le comité d'indemnisation détermine la méthode qu'il retient pour formuler sa recommandation au ministre en s'appuyant sur les méthodologies recommandées par l'Agence internationale de l'énergie atomique. (...) " ; <br/>
<br/>
              2. Considérant qu'il résulte de ces dispositions que le législateur a entendu faire bénéficier toute personne souffrant d'une maladie radio-induite ayant résidé ou séjourné, durant des périodes déterminées, dans des zones géographiques situées en Polynésie française et en Algérie, d'une présomption de causalité aux fins d'indemnisation du préjudice subi en raison de l'exposition aux rayonnements ionisants due aux essais nucléaires ; que, toutefois, cette présomption peut être renversée lorsqu'il est établi que le risque attribuable aux essais nucléaires, apprécié tant au regard de la nature de la maladie que des conditions particulières d'exposition du demandeur, est négligeable ; qu'à ce titre, l'appréciation du risque peut notamment prendre en compte le délai de latence de la maladie, le sexe du demandeur, son âge à la date du diagnostic, sa localisation géographique au moment des tirs, les fonctions qu'il exerçait effectivement, ses conditions d'affectation, ainsi que, le cas échéant, les missions de son unité au moment des tirs ; <br/>
<br/>
              3. Considérant que le calcul de la dose reçue de rayonnements ionisants constitue l'un des éléments sur lequel l'autorité chargée d'examiner la demande peut se fonder afin d'évaluer le risque attribuable aux essais nucléaires ; que si, pour ce calcul, l'autorité peut utiliser les résultats des mesures de surveillance de la contamination tant interne qu'externe des personnes exposées qu'il s'agisse de mesures individuelles ou collectives en ce qui concerne la contamination externe, il lui appartient de vérifier, avant d'utiliser ces résultats, que les mesures de surveillance de la contamination interne et externe ont, chacune, été suffisantes au regard des conditions concrètes d'exposition de l'intéressé, et sont ainsi de nature à établir si le risque attribuable aux essais nucléaires était négligeable ; qu'en l'absence de mesures de surveillance de la contamination interne ou externe et en l'absence de données relatives au cas des personnes se trouvant dans une situation comparable à celle du demandeur du point de vue du lieu et de la date de séjour, il appartient à cette même autorité de vérifier si, au regard des conditions concrètes d'exposition de l'intéressé précisées ci-dessus, de telles mesures auraient été nécessaires ; que si tel est le cas, l'administration ne peut être regardée comme rapportant la preuve de ce que le risque attribuable aux essais nucléaires doit être regardé comme négligeable et la présomption de causalité ne peut être renversée ;<br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. B...a été affecté au centre d'expérimentation du Pacifique de novembre 1970 à novembre 1972, puis de novembre 1973 à novembre 1975 ; qu'il se trouvait à Mururoa pendant la campagne de tirs de 1974, durant laquelle 9 essais ont eu lieu ; qu'il est atteint d'un cancer du cerveau et du système nerveux central, diagnostiqué en 1993 ;<br/>
<br/>
              5. Considérant que la cour administrative d'appel de Bordeaux a, par des appréciations souveraines exemptes de dénaturation et une motivation suffisante, estimé que la situation de M. B...pendant la campagne de tirs de 1974 l'avait exposé à un risque particulier d'irradiation interne qui nécessitait la mise en oeuvre de mesures particulières de surveillance et que celles dont il avait fait l'objet avaient été insuffisantes ; que ce faisant, compte tenu de ce qui a été dit aux points 2 et 3, la cour, qui a mis en oeuvre le régime de responsabilité institué par la loi du 5 janvier 2010, n'a pas commis d'erreur de droit en jugeant  que l'administration ne pouvait être regardée comme rapportant la preuve de ce que le risque attribuable aux essais nucléaires devait être regardé comme négligeable ; que, par suite,  le pourvoi du ministre doit être rejeté ;<br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la SCP Masse-Dessen, Thouvenin, Coudray au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du ministre de la défense est rejeté. <br/>
<br/>
Article 2 : L'Etat versera la somme de 3 000 euros à la SCP Masse-Dessen, Thouvenin, Coudray au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. <br/>
<br/>
Article 3 : La présente décision sera notifiée au ministre de la défense et à M. A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
