<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027094735</ID>
<ANCIEN_ID>JG_L_2013_02_000000361757</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/09/47/CETATEXT000027094735.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  6ème sous-section jugeant seule, 07/02/2013, 361757, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-02-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>361757</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 6ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Pierre Chaubon</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:361757.20130207</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance n° 1100894 du 26 juillet 2012, enregistrée le 8 août 2012 au secrétariat du contentieux du Conseil d'État, par laquelle la présidente du tribunal administratif de Basse-Terre a transmis au Conseil d'État, en application de l'article R. 351-2 du code de justice administrative et de l'article R. 114 du code électoral, la saisine de la Commission nationale des comptes de campagne et des financements politiques, enregistrée le 25 novembre 2011 au greffe du tribunal administratif de Basse-Terre, et fondée, en application des articles L. 52-15 et L. 118-3 du code électoral, sur la décision du 27 octobre 2011 par laquelle elle a rejeté le compte de campagne de M. A...B..., candidat à l'élection cantonale générale des 20 et 27 mars 2011 dans la circonscription du 1er canton des Abymes (Guadeloupe) ;<br/>
<br/>
              Vu le mémoire en défense, enregistré le 10 avril 2012 au greffe du tribunal administratif de Basse-Terre, présenté par M. B...qui demande que le tribunal administratif rejette la saisine de la Commission nationale des comptes de campagne et des financements politiques ; il soutient que le dépôt hors délai de son compte de campagne est justifié par la perte des documents administratifs indispensables pour l'établissement du compte ;<br/>
<br/>
              Vu les pièces dont il résulte que la décision de la Commission nationale des comptes de campagne et des financements politiques a été communiquée au ministre de l'intérieur, qui n'a pas produit d'observations ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code électoral ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pierre Chaubon, Conseiller d'Etat,  <br/>
<br/>
               - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il résulte de l'instruction que, par décision du 27 octobre 2011, la Commission nationale des comptes de campagne et des financements politiques a rejeté le compte de campagne de M. A...B..., candidat aux élections qui se sont déroulées les 20 et 27 mars 2011 dans le 1er canton des Abymes (Guadeloupe), au motif que ce compte de campagne a été déposé après la fin du délai prescrit ;<br/>
<br/>
              Sur le bien-fondé du rejet du compte de campagne :<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 52-12 du code électoral, dans sa rédaction issue de la loi du 14 avril 2011 portant simplification de dispositions du code électoral et relative à la transparence financière de la vie politique : " Chaque candidat ou candidat tête de liste soumis au plafonnement prévu à l'article L. 52-11 et qui a obtenu au moins 1 % des suffrages exprimés est tenu d'établir un compte de campagne retraçant, selon leur origine, l'ensemble des recettes perçues et, selon leur nature, l'ensemble des dépenses engagées ou effectuées en vue de l'élection, hors celles de la campagne officielle par lui-même ou pour son compte, au cours de la période mentionnée à l'article L. 52-4. (...) / Au plus tard avant 18 heures le dixième vendredi suivant le premier tour de scrutin, chaque candidat ou candidat tête de liste présent au premier tour dépose à la Commission nationale des comptes de campagne et des financements politiques son compte de campagne et ses annexes accompagné des justificatifs de ses recettes ainsi que des factures, devis et autres documents de nature à établir le montant des dépenses payées ou engagées par le candidat ou pour son compte (...) " ; que le premier alinéa de l'article L. 52-15 du même code dispose que : " La Commission nationale des comptes de campagne et des financements politiques approuve et, après procédure contradictoire, rejette ou réforme les comptes de campagne. Elle arrête le montant du remboursement forfaitaire prévu à l'article L. 52-11-1. " ;<br/>
<br/>
              3. Considérant qu'il est constant que le compte de campagne a été déposé par le candidat le 16 août 2011, soit postérieurement à la date limite du 27 mai 2011, dixième vendredi suivant le premier tour de scrutin organisé le 20 mars 2011 ; qu'ainsi, le délai fixé par les dispositions, citées ci-dessus, de l'article L. 52-12 du code électoral n'a pas été respecté par M. B...; qu'il en résulte que la Commission nationale des comptes de campagne et des financements politiques était fondée à rejeter le compte de campagne de M. B...;<br/>
<br/>
              Sur l'inéligibilité :<br/>
<br/>
              4. Considérant que, saisi par la Commission nationale des comptes de campagne et des financements politiques sur le fondement de l'article L. 52-15 du code électoral, le juge de l'élection peut, en application du deuxième alinéa de l'article L. 118-3 du même code dans sa rédaction issue de la loi du 14 avril 2011, prononcer l'inéligibilité du candidat " qui n'a pas déposé son compte de campagne dans les conditions et le délai prescrits à l'article L. 52-12. / (...) L'inéligibilité déclarée (...) est prononcée  pour une durée maximale de trois ans et s'applique à toutes les élections (...) " ; qu'il résulte de ces dispositions, éclairées par les travaux préparatoires, que la déclaration d'inéligibilité d'un candidat qui n'a pas déposé son compte de campagne dans les conditions et le délai prescrits à l'article L. 52-12 est une simple faculté dont dispose le juge de l'élection, qui doit prendre en compte, pour déterminer s'il y a lieu d'en faire usage, la nature de la règle méconnue, le caractère délibéré ou non du manquement, l'existence éventuelle d'autres motifs d'irrégularité du compte, le montant des sommes en cause ainsi que l'ensemble des circonstances de l'espèce ;<br/>
<br/>
              5. Considérant qu'il résulte de l'instruction que M. B...a déposé son compte de campagne plusieurs semaines après l'expiration du délai prescrit par l'article L. 52-12 du code électoral ; que s'il soutient que ce retard est justifié par la perte des documents du compte de campagne en sa possession, cette circonstance n'est pas de nature à justifier la méconnaissance des dispositions de l'article L. 52-12 ; que la Commission nationale des comptes de campagne et des financements politiques n'a relevé aucune autre irrégularité dans l'établissement du compte de campagne qui fait apparaître 2 245 euros de recettes et 2 066 de dépenses ; que, dans les circonstances de l'espèce, eu égard au caractère substantiel et dépourvu d'ambiguïté de la règle méconnue, il y a lieu de déclarer M. B...inéligible pendant une période d'un an à compter de la date de la présente décision ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : M. A...B...est déclaré inéligible pour une durée d'un an à compter de la date de la présente décision.<br/>
Article 2 : La présente décision sera notifiée à la Commission nationale des comptes de campagne et des financements politiques et à M. A...B....<br/>
Copie en sera adressée pour information au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
