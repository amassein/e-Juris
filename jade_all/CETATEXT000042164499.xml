<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042164499</ID>
<ANCIEN_ID>JG_L_2020_07_000000441968</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/16/44/CETATEXT000042164499.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 21/07/2020, 441968, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-07-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>441968</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP RICHARD</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:441968.20200721</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 18 juillet 2020 au secrétariat du contentieux du Conseil d'Etat, M. B... A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution de la décision de la formation restreinte du Conseil national de l'ordre des médecins du 19 mai 2020 prononçant sa suspension pour une durée de six mois du droit d'exercer la médecine pour état pathologique, en subordonnant la reprise de son activité aux résultats d'une nouvelle expertise ;<br/>
<br/>
              2°) de mettre à la charge du Conseil national de l'ordre des médecins la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - la condition d'urgence est remplie en ce que, d'une part, la décision attaquée porte atteinte à sa réputation, d'autre part, elle emporte pour lui de graves conséquences financières, enfin, elle l'expose à un risque important de perte de sa patientèle, ce qui est de nature à mettre en péril la poursuite de son activité ;<br/>
              - il existe un doute sérieux quant à la légalité de la décision attaquée ;<br/>
              - la formation restreinte du Conseil national de l'ordre des médecins s'est prononcée alors qu'il était hospitalisé, ce qui ne lui a pas permis de se défendre utilement ;<br/>
              - elle a inexactement interprété les dispositions du code de la santé publique, en particulier les articles R. 4124-3 et suivant de ce code, en estimant qu'une mesure de suspension du droit d'exercer la médecine du fait d'une infirmité ou d'un état pathologique devait nécessairement être totale et ne pouvait être limitée à une partie de l'exercice ; <br/>
              - elle a insuffisamment motivé sa décision et a commis une erreur de droit en s'abstenant de rechercher si son état de santé ne lui permettait pas, ainsi que le suggéraient les experts, de poursuivre l'exercice de son activité professionnelle en dehors des gestes techniques de radiographie interventionnelle.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
- le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. Saisie le 17 janvier 2020 par le conseil régional d'Ile-de-France de l'ordre des médecins de la demande qui lui avait été présentée par le conseil départemental du Val-de-Marne de l'ordre des médecins sur le fondement de l'article R. 4124-3 du code de la santé publique, la formation restreinte du Conseil national de l'Ordre des médecins a, le 19 mai 2020, après avoir diligenté une expertise, prononcé la suspension pour six mois de M. B... A..., spécialiste en radiodiagnostic et imagerie médicale, du droit d'exercer la médecine au motif de son état pathologique, en subordonnant la reprise de son activité aux résultats d'une nouvelle expertise. M. A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de la décision du 19 mai 2020.<br/>
<br/>
              3. M. A... soutient :<br/>
              - que la formation restreinte du Conseil national de l'ordre des médecins s'est prononcée alors qu'il était hospitalisé, ce qui ne lui a pas permis de se défendre utilement ;<br/>
              - qu'elle a inexactement interprété les dispositions du code de la santé publique, en particulier les articles R. 4124-3 et suivant de ce code, en estimant qu'une mesure de suspension du droit d'exercer la médecine du fait d'une infirmité ou d'un état pathologique devait nécessairement être totale et ne pouvait être limitée à une partie de l'exercice ; <br/>
              - qu'elle a insuffisamment motivé sa décision et a commis une erreur de droit en s'abstenant de rechercher si son état de santé ne lui permettait pas, ainsi que le suggéraient les experts, de poursuivre l'exercice de son activité professionnelle en dehors des gestes techniques de radiographie interventionnelle.<br/>
<br/>
              4. Aucun de ces moyens n'apparaît, en l'état de l'instruction, propre à créer un doute sérieux quant à la légalité de la décision du 19 mai 2020. Par suite, sans qu'il soit besoin de se prononcer sur l'urgence, il y a lieu de rejeter la requête selon la procédure prévue à l'article L. 522-3 du code de justice administrative, y compris les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. A... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. B... A....<br/>
Copie en sera adressée au Conseil national de l'ordre des médecins.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
