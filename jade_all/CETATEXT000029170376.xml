<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029170376</ID>
<ANCIEN_ID>JG_L_2014_06_000000376862</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/17/03/CETATEXT000029170376.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère SSJS, 27/06/2014, 376862, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-06-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>376862</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Yannick Faure</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:376862.20140627</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              M. A...B...a demandé au juge des référés du tribunal administratif de Limoges, sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
              - de faire cesser les effets de la décision de la commission des droits et de l'autonomie des personnes handicapées de l'Indre du 20 juin 2013 limitant à 21 heures la quotité horaire hebdomadaire consacrée à l'accompagnement de son fils handicapé par un auxiliaire de vie scolaire ;<br/>
              - d'enjoindre au directeur académique des services de l'éducation nationale de la Vienne de mettre en oeuvre, au bénéfice de son enfant, un accompagnement individuel permettant un accès complet aux temps de classe et une participation à l'ensemble des temps périscolaires, correspondant à un total de 34 heures d'accompagnement par semaine, sous astreinte de 500 euros par jour de retard ; <br/>
              - d'enjoindre au préfet de l'Indre de mettre fin aux troubles à l'ordre public résultant, d'une part, de l'inexistence de la commission des droits et de l'autonomie des personnes handicapées de l'Indre et, d'autre part, de la privation du droit à une procédure équitable. <br/>
<br/>
              Par une ordonnance n° 1400537 du 13 mars 2014, le juge des référés du tribunal administratif de Limoges a rejeté sa demande.<br/>
<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi et un mémoire en réplique, enregistrés les 31 mars et 10 juin 2014 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance n° 1400537 du juge des référés du tribunal administratif de Limoges du 13 mars 2014 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge des parties perdantes la somme de 3 600 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
                          Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - la Constitution, notamment son Préambule et ses articles 34 et 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code de l'action sociale et des familles, notamment son article L. 241-9 ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yannick Faure, Auditeur,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de M.B....<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              Sur la question prioritaire de constitutionnalité :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé, y compris pour la première fois en cassation, à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. L'article L. 241-9 du code de l'action sociale et des familles dispose que : " Les décisions relevant du 1° du I de l'article L. 241-6 prises à l'égard d'un enfant ou un adolescent handicapé, ainsi que celles relevant des 2°, 3° et 5° du I du même article peuvent faire l'objet de recours devant la juridiction du contentieux technique de la sécurité sociale. Ce recours, ouvert à toute personne et à tout organisme intéressé, est dépourvu d'effet suspensif, sauf lorsqu'il est intenté par la personne handicapée ou son représentant légal à l'encontre des décisions relevant du 2° du I de l'article L. 241-6. / Les décisions relevant des 1° et 2 du I du même article, prises à l'égard d'un adulte handicapé dans le domaine de la rééducation professionnelle, du travail adapté ou protégé, et du 4° du I dudit article peuvent faire l'objet d'un recours devant la juridiction administrative ".<br/>
<br/>
              3. Il résulte de ces dispositions que la juridiction du contentieux technique de la sécurité sociale a compétence pour connaître des litiges nés des décisions des commissions des droits et de l'autonomie des personnes handicapées, qu'elles concernent des enfants ou des adultes handicapés, et qu'elles soient relatives à leur orientation, à la désignation des établissements ou services susceptibles de les accueillir ou à l'attribution de certaines prestations, à la seule exception des décisions en matière d'orientation et de désignation des établissements ou services prises à l'égard des adultes handicapés dans le domaine de la rééducation professionnelle, du travail adapté ou protégé ainsi qu'à la reconnaissance de la qualité de travailleur handicapé, qui relèvent de la juridiction administrative. <br/>
<br/>
              4. M. B...soutient, en premier lieu, que ces dispositions méconnaissent le principe d'égalité devant la loi, résultant de l'article 6 de la Déclaration des droits de l'homme et du citoyen, en raison de la différence de traitement qu'elles établissent entre enfants et adultes handicapés. Toutefois, le législateur n'a pas instauré de différence de traitement entre enfants et adultes handicapés, mais désigné la juridiction compétente en fonction de la nature de la décision attaquée, les décisions ayant des incidences financières pour la sécurité sociale relevant du contentieux technique de la sécurité sociale, qu'elles concernent les enfants ou les adultes, tandis que les décisions prises en matière de rééducation professionnelle, de travail adapté ou protégé relèvent de la juridiction administrative. Par suite, M. B...n'est pas fondé à soutenir que ces dispositions méconnaissent le principe d'égalité devant la loi.<br/>
<br/>
              5. M. B...soutient, en second lieu, que ces dispositions méconnaissent le droit à un recours juridictionnel effectif, garanti par l'article 16 de la Déclaration des droits de l'homme et du citoyen, et les principes d'égal accès à l'instruction et de liberté de l'enseignement, qui découlent notamment du treizième alinéa du Préambule de la Constitution du 27 octobre 1946, en tant qu'elles donnent compétence à la juridiction du contentieux technique de la sécurité sociale, devant laquelle n'existe pas de procédure d'urgence. Toutefois, la juridiction du contentieux technique de la sécurité sociale est régie, notamment en ce qui concerne la procédure applicable aux recours portés devant elle, par les dispositions du chapitre III du titre IV du livre Ier du code de la sécurité sociale et par les dispositions du code de procédure civile auxquelles elles renvoient. Il s'ensuit que les griefs soulevés sont sans incidence sur la conformité à la Constitution des dispositions de l'article L. 241-9 du code de l'action sociale et des familles.<br/>
<br/>
              6. Par suite, la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux. Ainsi, sans qu'il soit besoin de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée, le moyen tiré de ce que l'article L. 241-9 du code de l'action sociale et des familles porte atteinte aux droits et libertés garantis par la Constitution doit être écarté. <br/>
<br/>
              Sur l'autre moyen du pourvoi :<br/>
<br/>
              7. L'article R. 342-1 du code de justice administrative dispose que : " Le tribunal administratif saisi d'une demande relevant de sa compétence territoriale est également compétent pour connaître d'une demande connexe à la précédente et relevant normalement de la compétence territoriale d'un autre tribunal administratif ".<br/>
<br/>
              8. Il résulte des dispositions citées au point 2 que le tribunal administratif de Limoges n'avait pas compétence pour connaître des conclusions de la demande de M. B...dirigées contre la décision du 26 juin 2013 de la commission des droits et de l'autonomie des personnes handicapées de l'Indre relative au renouvellement de la prestation de compensation du handicap pour son fils. Dès lors, il ne pouvait se reconnaître compétent, sur le fondement des dispositions précitées de l'article R. 342-1 du code de justice administrative, pour connaître de conclusions relevant de la compétence territoriale d'un autre tribunal administratif. Le moyen tiré de ce que l'ordonnance attaquée méconnaîtrait ces dispositions ne peut, dès lors, qu'être écarté.<br/>
<br/>
              9. Il résulte de tout ce qui précède que M. B...n'est pas fondé à demander l'annulation de l'ordonnance qu'il attaque. Ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être également rejetées.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M.B....<br/>
Article 2 : Le pourvoi de M. B...est rejeté.<br/>
Article 3 : La présente décision sera notifiée à M. A...B...et à la ministre des affaires sociales et de la santé.<br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre, au ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
