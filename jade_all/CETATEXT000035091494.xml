<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035091494</ID>
<ANCIEN_ID>JG_L_2017_06_000000398445</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/09/14/CETATEXT000035091494.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Section, 30/06/2017, 398445, Publié au recueil Lebon</TITRE>
<DATE_DEC>2017-06-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>398445</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Section</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY ; SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>Mme Charline Nicolas</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESEC:2017:398445.20170630</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Les sociétés France-Manche et The Channel Tunnel Group ont demandé au tribunal administratif de Rouen d'annuler la décision implicite du président du syndicat mixte de promotion de l'activité transmanche (SMPAT) refusant de prononcer la résiliation du contrat de délégation de service public conclu entre le syndicat et la société Louis Dreyfus Armateurs SAS et portant sur l'exploitation d'une liaison maritime entre Dieppe et Newhaven. Par un jugement n° 1100887 du 16 juillet 2013, le tribunal administratif de Rouen a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 13DA01570 du 28 janvier 2016, la cour administrative d'appel de Douai, sur l'appel des sociétés France-Manche et The Channel Tunnel Group, a annulé ce jugement et la décision implicite du président du SMPAT, puis enjoint au syndicat de résilier le contrat dans un délai de six mois suivant la notification de l'arrêt.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés respectivement les 1er avril, 20 mai et 2 novembre 2016 au secrétariat du contentieux du Conseil d'Etat, le SMPAT demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel des sociétés France-Manche et The Channel Tunnel Group ;<br/>
<br/>
              3°) de mettre à la charge de ces sociétés la somme de 10 000 euros sur le fondement de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code des marchés publics ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Charline Nicolas, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat du syndicat mixte de promotion de l'activité transmanche et à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la société France Manche et de la société The Channel Tunnel Group ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des énonciations de l'arrêt attaqué que, par une convention de délégation de service public conclue le 29 novembre 2006, le syndicat mixte de promotion de l'activité transmanche (SMPAT), qui a pour objet le développement et la promotion de l'activité transmanche entre la Seine-Maritime et le sud de l'Angleterre, a délégué à la société Louis Dreyfus Armateurs SAS l'exploitation, au moyen de deux navires, d'une liaison maritime entre Dieppe et Newhaven ; que les sociétés France-Manche et The Channel Tunnel Group, qui exploitent sous le nom " A... " le tunnel sous la Manche, ont demandé au SMPAT par lettre du 19 novembre 2010 de prononcer la résiliation de ce contrat ; qu'une décision implicite de refus est née du silence gardé pendant plus de deux mois par le président du SMPAT sur cette demande de résiliation ; que les deux sociétés ont alors saisi le tribunal administratif de Rouen d'une demande tendant à l'annulation pour excès de pouvoir de cette décision ; que, par un jugement du 16 juillet 2013, le tribunal administratif a rejeté leur demande ; que, par un arrêt du 28 janvier 2016, contre lequel le SMPAT se pourvoit en cassation, la cour administrative d'appel de Douai, après avoir jugé que le contrat en cause devait être analysé non comme une délégation de service public mais comme un marché public, a annulé ce jugement ainsi que la décision litigieuse en raison de la méconnaissance par le SMPAT des règles du code des marchés publics lors de la procédure de passation du contrat, et a enjoint au SMPAT de résilier le contrat, dont le terme, fixé au 31 décembre 2014, avait été prorogé par avenant jusqu'au 31 décembre 2017, dans un délai de six mois suivant la notification de cet arrêt ; que, par une décision du 22 juillet 2016, le Conseil d'Etat, statuant au contentieux a ordonné le sursis à exécution de cet arrêt jusqu'à ce qu'il soit statué sur le présent pourvoi ; <br/>
<br/>
              2. Considérant qu'un tiers à un contrat administratif susceptible d'être lésé dans ses intérêts de façon suffisamment directe et certaine par une décision refusant de faire droit à sa demande de mettre fin à l'exécution du contrat, est recevable à former devant le juge du contrat un recours de pleine juridiction tendant à ce qu'il soit mis fin à l'exécution du contrat ; que s'agissant d'un contrat conclu par une collectivité territoriale ou un groupement de collectivités territoriales, cette action devant le juge du contrat est également ouverte aux membres de l'organe délibérant de la collectivité territoriale ou du groupement de collectivités territoriales concerné ainsi qu'au représentant de l'Etat dans le département ; <br/>
<br/>
              3. Considérant que les tiers ne peuvent utilement soulever, à l'appui de leurs conclusions tendant à ce qu'il soit mis fin à l'exécution du contrat, que des moyens tirés de ce que la personne publique contractante était tenue de mettre fin à son exécution du fait de dispositions législatives applicables aux contrats en cours, de ce que le contrat est entaché d'irrégularités qui sont de nature à faire obstacle à la poursuite de son exécution et que le juge devrait relever d'office ou encore de ce que la poursuite de l'exécution du contrat est manifestement contraire à l'intérêt général ; qu'à cet égard, les requérants peuvent se prévaloir d'inexécutions d'obligations contractuelles qui, par leur gravité, compromettent manifestement l'intérêt général ; qu'en revanche, ils ne peuvent se prévaloir d'aucune autre irrégularité, notamment pas celles tenant aux conditions et formes dans lesquelles la décision de refus a été prise ; qu'en outre, les moyens soulevés doivent, sauf lorsqu'ils le sont par le représentant de l'Etat dans le département ou par les membres de l'organe délibérant de la collectivité territoriale ou du groupement de collectivités territoriales compte-tenu des intérêts dont ils ont la charge, être en rapport direct avec l'intérêt lésé dont le tiers requérant se prévaut ;  <br/>
<br/>
              4. Considérant que, saisi par un tiers dans les conditions définies ci-dessus, de conclusions tendant à ce qu'il soit mis fin à l'exécution d'un contrat administratif, il appartient au juge du contrat d'apprécier si les moyens soulevés sont de nature à justifier qu'il y fasse droit et d'ordonner après avoir vérifié que sa décision ne portera pas une atteinte excessive à l'intérêt général, qu'il soit mis fin à l'exécution du contrat, le cas échéant avec un effet différé ; <br/>
<br/>
              5. Considérant que ces règles, qui ne portent pas atteinte à la substance du droit au recours des tiers, sont d'application immédiate ; <br/>
<br/>
              6. Considérant que pour écarter la fin de non recevoir opposée par le SMPAT à la demande de première instance des sociétés France-Manche et The Channel Tunnel Group et tirée de leur défaut d'intérêt pour agir, la cour administrative d'appel de Douai s'est fondée sur l'atteinte portée par l'exécution de la convention en litige à leur intérêt commercial compte tenu de la situation de concurrence existant entre la liaison maritime transmanche objet du contrat et l'exploitation du tunnel sous la Manche ; qu'en statuant ainsi, sans avoir recherché si la poursuite de l'exécution de la convention du 29 novembre 2006 était de nature à léser les intérêts de ces sociétés de façon suffisamment directe et certaine, la cour administrative d'appel de Douai a entaché son arrêt d'une erreur de droit ;<br/>
<br/>
              7. Considérant par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que le SMPAT est fondé à demander l'annulation de l'arrêt qu'il attaque ; <br/>
<br/>
              8. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              9. Considérant qu'à l'appui de leur demande, les sociétés France-Manche et The Channel Tunnel Group se prévalent de leur seule qualité de concurrent direct sur les liaisons transmanche de courte durée ; qu'une telle qualité ne suffit pas à justifier qu'elles seraient susceptibles d'être lésées dans leurs intérêts de façon suffisamment directe et certaine par la poursuite de l'exécution du contrat conclu le 29 novembre 2006 pour être recevables à demander au juge du contrat qu'il soit mis fin à l'exécution de celui-ci ; qu'au surplus, les moyens tirés d'illégalités de la procédure de passation du contrat qu'elles soulèvent à l'appui de leur demande d'annulation de la décision litigieuse, ne peuvent, comme tels, être invoqués à l'encontre du refus de mettre fin à l'exécution du contrat et sont dès lors inopérants ; qu'il suit de là que, ces sociétés ne sont pas fondées à se plaindre que, par le jugement attaqué, le tribunal administratif de Rouen a rejeté leur demande ; <br/>
<br/>
              10. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de chacune des sociétés France-Manche et The Channel Tunnel Group la somme de 1 500 euros à verser au SMPAT sur le fondement de l'article L. 761-1 du code de justice administrative ; que ces dispositions font en revanche obstacle à ce qu'une somme soit mise à la charge de ce syndicat qui n'est pas la partie perdante ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt du 28 janvier 2016 de la cour administrative d'appel de Douai est annulé. <br/>
Article 2 : La requête présentée par les sociétés France-Manche et The Channel Tunnel Group devant la cour administrative d'appel de Douai est rejetée. <br/>
Article 3 : Les sociétés France-Manche et The Channel Tunnel Group verseront, chacune, une somme de 1 500 euros au SMPAT au titre de l'article L. 761-1 du code de justice administrative. Leurs conclusions présentées sur le même fondement sont rejetées. <br/>
Article 4 : La présente décision sera notifiée aux sociétés France-Manche et The Channel Tunnel Group ainsi qu'au syndicat mixte de promotion de l'activité transmanche.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-04-02 MARCHÉS ET CONTRATS ADMINISTRATIFS. FIN DES CONTRATS. RÉSILIATION. - CONTESTATION PAR UN TIERS D'UNE DÉCISION REFUSANT DE METTRE FIN À L'EXÉCUTION DU CONTRAT [RJ1] - 1) RÉGIME [RJ2] - A) TITULAIRES ET OBJET DU RECOURS - I) PRINCIPE - II) CAS PARTICULIER D'UN CONTRAT CONCLU PAR UNE COLLECTIVITÉ TERRITORIALE OU UN GROUPEMENT DE COLLECTIVITÉS TERRITORIALES - B) MOYENS INVOCABLES - I) PRINCIPE - II) CONDITIONS - C) POUVOIRS ET DEVOIRS DU JUGE [RJ3] - 2) APPLICATION DANS LE TEMPS[RJ4].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-08-01-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. RECEVABILITÉ. RECEVABILITÉ DU RECOURS POUR EXCÈS DE POUVOIR EN MATIÈRE CONTRACTUELLE. - CONTESTATION DE LA DÉCISION REFUSANT DE METTRE FIN À L'EXÉCUTION DU CONTRAT - RECOURS POUR EXCÈS DE POUVOIR DU TIERS AU CONTRAT - RECEVABILITÉ - ABSENCE (SOL. IMPL.) [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">39-08-01-03 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. RECEVABILITÉ. - CONTESTATION D'UNE DÉCISION REFUSANT DE METTRE FIN À L'EXÉCUTION DU CONTRAT [RJ1] - 1) RÉGIME [RJ2] - A) TITULAIRES ET OBJET DU RECOURS - I) PRINCIPE - II) CAS PARTICULIER D'UN CONTRAT CONCLU PAR UNE COLLECTIVITÉ TERRITORIALE OU UN GROUPEMENT DE COLLECTIVITÉS TERRITORIALES - B) MOYENS INVOCABLES - I) PRINCIPE - II) CONDITIONS - 2) APPLICATION DANS LE TEMPS [RJ4] - 3) ESPÈCE - RECOURS EXERCÉ PAR UNE SOCIÉTÉ CONCURRENTE - RECEVABILITÉ - ABSENCE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">39-08-03-02 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. POUVOIRS ET OBLIGATIONS DU JUGE. POUVOIRS DU JUGE DU CONTRAT. - CONTESTATION D'UNE DÉCISION REFUSANT DE METTRE FIN À L'EXÉCUTION DU CONTRAT [RJ1] - OFFICE DU JUGE DU CONTRAT SAISI PAR UN TIERS TENDANT À CE QU'IL SOIT MIS FIN À L'EXÉCUTION DU CONTRAT [RJ3].
</SCT>
<SCT ID="8E" TYPE="PRINCIPAL">54-02-01-02 PROCÉDURE. DIVERSES SORTES DE RECOURS. RECOURS POUR EXCÈS DE POUVOIR. CONDITIONS DE RECEVABILITÉ. - RECOURS CONTRE LES ACTES DÉTACHABLES D'UN CONTRAT - DÉCISION REFUSANT DE METTRE FIN À L'EXÉCUTION DU CONTRAT - CONSÉQUENCE DE L'ÉLARGISSEMENT DU CHAMP DU RECOURS DE PLEINE JURIDICTION OUVERT AU TIERS - RECEVABILITÉ DU RECOURS POUR EXCÈS DE POUVOIR DU TIERS AU CONTRAT - ABSENCE (SOL. IMPL.) [RJ1].
</SCT>
<SCT ID="8F" TYPE="PRINCIPAL">54-02-02 PROCÉDURE. DIVERSES SORTES DE RECOURS. RECOURS DE PLEIN CONTENTIEUX. - CONTESTATION PAR UN TIERS D'UNE DÉCISION REFUSANT DE METTRE FIN À L'EXÉCUTION DU CONTRAT [RJ1] - 1) RÉGIME [RJ2] - A) TITULAIRES ET OBJET DU RECOURS - I) PRINCIPE - II) CAS PARTICULIER D'UN CONTRAT CONCLU PAR UNE COLLECTIVITÉ TERRITORIALE OU UN GROUPEMENT DE COLLECTIVITÉS TERRITORIALES - B) MOYENS INVOCABLES - I) PRINCIPE - II) CONDITIONS - C) POUVOIRS ET DEVOIRS DU JUGE [RJ3] - 2) APPLICATION DANS LE TEMPS [RJ4].
</SCT>
<ANA ID="9A"> 39-04-02 1) a) i) Un tiers à un contrat administratif susceptible d'être lésé dans ses intérêts de façon suffisamment directe et certaine par une décision refusant de faire droit à sa demande de mettre fin à l'exécution du contrat, est recevable à former devant le juge du contrat un recours de pleine juridiction tendant à ce qu'il soit mis fin à l'exécution du contrat.... ,,ii) S'agissant d'un contrat conclu par une collectivité territoriale ou un groupement de collectivités territoriales, cette action devant le juge du contrat est également ouverte aux membres de l'organe délibérant de la collectivité territoriale ou du groupement de collectivités territoriales concerné ainsi qu'au représentant de l'Etat dans le département.... ,,b) i) Les tiers ne peuvent utilement soulever, à l'appui de leurs conclusions tendant à ce qu'il soit mis fin à l'exécution du contrat, que des moyens tirés de ce que la personne publique contractante était tenue de mettre fin à son exécution du fait de dispositions législatives applicables aux contrats en cours, de ce que le contrat est entaché d'irrégularités qui sont de nature à faire obstacle à la poursuite de son exécution et que le juge devrait relever d'office ou encore de ce que la poursuite de l'exécution du contrat est manifestement contraire à l'intérêt général. A cet égard, les requérants peuvent se prévaloir d'inexécutions d'obligations contractuelles qui, par leur gravité, compromettent manifestement l'intérêt général.... ,,En revanche, ils ne peuvent se prévaloir d'aucune autre irrégularité, notamment pas celles tenant aux conditions et formes dans lesquelles la décision de refus a été prise.... ,,ii) Les moyens soulevés doivent, sauf lorsqu'ils le sont par le représentant de l'Etat dans le département ou par les membres de l'organe délibérant de la collectivité territoriale ou du groupement de collectivités territoriales compte-tenu des intérêts dont ils ont la charge, être en rapport direct avec l'intérêt lésé dont le tiers requérant se prévaut.... ,,c) Saisi par un tiers dans les conditions définies ci-dessus, de conclusions tendant à ce qu'il soit mis fin à l'exécution d'un contrat administratif, il appartient au juge du contrat d'apprécier si les moyens soulevés sont de nature à justifier qu'il y fasse droit et d'ordonner, après avoir vérifié que sa décision ne portera pas une atteinte excessive à l'intérêt général, qu'il soit mis fin à l'exécution du contrat, le cas échéant avec un effet différé.,,,2) Ces règles, qui ne portent pas atteinte à la substance du droit au recours des tiers, sont d'application immédiate.</ANA>
<ANA ID="9B"> 39-08-01-01 Un tiers à un contrat administratif susceptible d'être lésé dans ses intérêts de façon suffisamment directe et certaine par une décision refusant de faire droit à sa demande de mettre fin à l'exécution du contrat, est recevable à former devant le juge du contrat un recours de pleine juridiction tendant à ce qu'il soit mis fin à l'exécution du contrat. Cette décision n'est pas susceptible de faire l'objet d'un recours pour excès de pouvoir.</ANA>
<ANA ID="9C"> 39-08-01-03 1) a) i) Un tiers à un contrat administratif susceptible d'être lésé dans ses intérêts de façon suffisamment directe et certaine par une décision refusant de faire droit à sa demande de mettre fin à l'exécution du contrat, est recevable à former devant le juge du contrat un recours de pleine juridiction tendant à ce qu'il soit mis fin à l'exécution du contrat.... ... ...ii) S'agissant d'un contrat conclu par une collectivité territoriale ou un groupement de collectivités territoriales, cette action devant le juge du contrat est également ouverte aux membres de l'organe délibérant de la collectivité territoriale ou du groupement de collectivités territoriales concerné ainsi qu'au représentant de l'Etat dans le département.... ,,b) i) Les tiers ne peuvent utilement soulever, à l'appui de leurs conclusions tendant à ce qu'il soit mis fin à l'exécution du contrat, que des moyens tirés de ce que la personne publique contractante était tenue de mettre fin à son exécution du fait de dispositions législatives applicables aux contrats en cours, de ce que le contrat est entaché d'irrégularités qui sont de nature à faire obstacle à la poursuite de son exécution et que le juge devrait relever d'office ou encore de ce que la poursuite de l'exécution du contrat est manifestement contraire à l'intérêt général. A cet égard, les requérants peuvent se prévaloir d'inexécutions d'obligations contractuelles qui, par leur gravité, compromettent manifestement l'intérêt général.... ,,En revanche, ils ne peuvent se prévaloir d'aucune autre irrégularité, notamment pas celles tenant aux conditions et formes dans lesquelles la décision de refus a été prise.... ,,ii) Les moyens soulevés doivent, sauf lorsqu'ils le sont par le représentant de l'Etat dans le département ou par les membres de l'organe délibérant de la collectivité territoriale ou du groupement de collectivités territoriales compte-tenu des intérêts dont ils ont la charge, être en rapport direct avec l'intérêt lésé dont le tiers requérant se prévaut.... ,,2) Ces règles, qui ne portent pas atteinte à la substance du droit au recours des tiers, sont d'application immédiate.... ,,3) Recours contre la décision de la personne publique refusant de prononcer la résiliation d'un contrat de délégation de service public portant sur l'exploitation d'une liaison maritime transmanche. La qualité de concurrent direct sur les liaisons transmanche de courte durée dont se prévalent les sociétés requérantes, qui exploitent le tunnel sous la Manche, ne suffit pas à justifier qu'elles seraient susceptibles d'être lésées dans leurs intérêts de façon suffisamment directe et certaine par la poursuite de l'exécution du contrat pour être recevables à demander au juge du contrat qu'il soit mis fin à l'exécution de celui-ci.</ANA>
<ANA ID="9D"> 39-08-03-02 Saisi par un tiers de conclusions tendant à ce qu'il soit mis fin à l'exécution d'un contrat administratif, il appartient au juge du contrat d'apprécier si les moyens soulevés sont de nature à justifier qu'il y fasse droit et d'ordonner, après avoir vérifié que sa décision ne portera pas une atteinte excessive à l'intérêt général, qu'il soit mis fin à l'exécution du contrat, le cas échéant avec un effet différé.</ANA>
<ANA ID="9E"> 54-02-01-02 Un tiers à un contrat administratif susceptible d'être lésé dans ses intérêts de façon suffisamment directe et certaine par une décision refusant de faire droit à sa demande de mettre fin à l'exécution du contrat, est recevable à former devant le juge du contrat un recours de pleine juridiction tendant à ce qu'il soit mis fin à l'exécution du contrat. Fermeture du recours pour excès de pouvoir.</ANA>
<ANA ID="9F"> 54-02-02 1) a) i) Un tiers à un contrat administratif susceptible d'être lésé dans ses intérêts de façon suffisamment directe et certaine par une décision refusant de faire droit à sa demande de mettre fin à l'exécution du contrat, est recevable à former devant le juge du contrat un recours de pleine juridiction tendant à ce qu'il soit mis fin à l'exécution du contrat.... ,,ii) S'agissant d'un contrat conclu par une collectivité territoriale ou un groupement de collectivités territoriales, cette action devant le juge du contrat est également ouverte aux membres de l'organe délibérant de la collectivité territoriale ou du groupement de collectivités territoriales concerné ainsi qu'au représentant de l'Etat dans le département.... ,,b) i) Les tiers ne peuvent utilement soulever, à l'appui de leurs conclusions tendant à ce qu'il soit mis fin à l'exécution du contrat, que des moyens tirés de ce que la personne publique contractante était tenue de mettre fin à son exécution du fait de dispositions législatives applicables aux contrats en cours, de ce que le contrat est entaché d'irrégularités qui sont de nature à faire obstacle à la poursuite de son exécution et que le juge devrait relever d'office ou encore de ce que la poursuite de l'exécution du contrat est manifestement contraire à l'intérêt général. A cet égard, les requérants peuvent se prévaloir d'inexécutions d'obligations contractuelles qui, par leur gravité, compromettent manifestement l'intérêt général.... ,,En revanche, ils ne peuvent se prévaloir d'aucune autre irrégularité, notamment pas celles tenant aux conditions et formes dans lesquelles la décision de refus a été prise.... ,,ii) Les moyens soulevés doivent, sauf lorsqu'ils le sont par le représentant de l'Etat dans le département ou par les membres de l'organe délibérant de la collectivité territoriale ou du groupement de collectivités territoriales compte-tenu des intérêts dont ils ont la charge, être en rapport direct avec l'intérêt lésé dont le tiers requérant se prévaut.... ,,c) Saisi par un tiers dans les conditions définies ci-dessus, de conclusions tendant à ce qu'il soit mis fin à l'exécution d'un contrat administratif, il appartient au juge du contrat d'apprécier si les moyens soulevés sont de nature à justifier qu'il y fasse droit et d'ordonner, après avoir vérifié que sa décision ne portera pas une atteinte excessive à l'intérêt général, qu'il soit mis fin à l'exécution du contrat, le cas échéant avec un effet différé.,,,2) Ces règles, qui ne portent pas atteinte à la substance du droit au recours des tiers, sont d'application immédiate.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Ab. jur. CE, Section, 24 avril 1964, SA de Livraisons industrielles et commerciales (LIC), n° 53518, p. 239., ,[RJ2] Rappr., s'agissant du recours de pleine juridiction ouvert aux tiers contestant la validité du contrat, CE, Assemblée, 4 avril 2014, Département de Tarn-et-Garonne, n° 358994, p. 70., ,[RJ3] Rappr., s'agissant de l'office du juge du contrat saisi par une partie d'un recours contestant la validité du contrat, CE, Assemblée, 28 décembre 2009, Commune de Béziers, n° 304802, p. 509 ; s'agissant de l'office du juge du contrat saisi par une partie d'un recours contestant la validité d'une mesure de résiliation et tendant à la reprise des relations contractuelles, CE, Section, 21 mars 2011, Commune de Béziers, n° 304806, p. 117.,,[RJ4] Rappr., CE, Assemblée, 16 juillet 2007, Société Tropic Travaux Signalisation, n° 291545, p. 360.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
