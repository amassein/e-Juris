<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036757671</ID>
<ANCIEN_ID>JG_L_2017_06_000000411304</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/75/76/CETATEXT000036757671.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 13/06/2017, 411304, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-06-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411304</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2017:411304.20170613</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
              Par une requête, enregistrée le 8 juin 2017 au secrétariat du contentieux du Conseil d'Etat, l'Union populaire républicaine (UPR) demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution de la décision du 6 juin 2017 par laquelle le ministre d'Etat, ministre de l'intérieur, a refusé de modifier la " grille des nuances politiques " retenue pour l'enregistrement des résultats des élections législatives en tant qu'elle classe les candidats de l'Union populaire républicaine dans la rubrique " Divers " et non sous l'intitulé de leur parti politique ;<br/>
<br/>
              2°) d'enjoindre au ministre d'Etat, ministre de l'intérieur de modifier la " grille des nuances politiques " pour classer les candidats de l'Union populaire républicaine sous la rubrique " UPR ", sous astreinte de deux cent euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              L'Union populaire républicaine (UPR) soutient : <br/>
              - que la condition d'urgence est remplie, dès lors que la décision contestée prive de visibilité ses candidats investis aux élections législatives des 11 et 18 juin 2017 ;<br/>
              - qu'il existe un doute sérieux quant à sa légalité ; qu'en effet, la décision contestée est entachée d'une erreur manifeste d'appréciation et constitue un abus de pouvoir, dès lors que le ministre d'Etat, ministre de l'intérieur, n'a pas pris en considération la croissance du nombre de ses adhérents et le nombre de candidats qu'elle a investis aux élections législatives des 11 et 18 juin 2017, et l'oblige ainsi à se voir appliquer une nuance politique qui ne lui est pas propre, alors que d'autres mouvements politiques récemment créés ont bénéficié d'un classement sous leur appellation.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le décret n° 2014-1479 du 9 décembre 2014 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; qu'en vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. Considérant que le décret du 9 décembre 2014 relatif à la mise en oeuvre de deux traitements automatisés de données à caractère personnel dénommés " Application élection " et " Répertoire national des élus " prévoit que le ministre de l'intérieur établit une " grille des nuances politiques " retenue pour l'enregistrement des résultats de l'élection afin de faciliter l'agrégation au niveau national et la présentation de ces résultats ;<br/>
<br/>
              3. Considérant que l'Union populaire républicaine (UPR) demande la suspension de l'exécution de la décision du ministre d'Etat, ministre de l'intérieur, du 6 juin 2017 par laquelle celui-ci a rejeté sa demande tendant à ce que soit modifiée la " grille des nuances politiques " retenue pour l'enregistrement des résultats des élections législatives en tant qu'elle classe les candidats de l'Union populaire républicaine dans la rubrique " Divers " et non sous l'intitulé de leur parti politique  ;<br/>
<br/>
              4. Considérant qu'à supposer que le classement des candidats de ce parti politique, dans la " grille des nuances politiques " établie par le ministre de l'intérieur, sous la rubrique " Divers " ait été de nature à atténuer leur visibilité lors de la campagne électorale, les conséquences alléguées d'un tel classement ne pouvaient être utilement corrigées, ni a fortiori prévenues, à la date à laquelle le Conseil d'Etat a été saisi de la présente requête, correspondant à la veille de la fin de la campagne ; qu'il n'est pas établi, ni même allégué, que la présentation des résultats du 1er tour par le ministère de l'intérieur, du seul fait qu'elle ne mentionne pas, au sein de la catégorie " Divers ", les résultats obtenus par les candidats de l'Union populaire républicaine, porterait aux intérêts de ce parti politique une atteinte grave et immédiate susceptible de caractériser une situation d'urgence au sens des dispositions de l'article L. 521-1 du code de justice administrative ;<br/>
<br/>
              5. Considérant qu'il résulte de tout ce qui précède qu'en l'absence d'urgence, il y a lieu de rejeter, sur le fondement de l'article L. 522-3 du code de justice administrative, les conclusions de l'Union populaire républicaine tendant à la suspension de la décision du ministre d'Etat, ministre de l'intérieur, rejetant sa demande, ainsi, par suite, que ses conclusions aux fins d'injonction et celles tendant à l'application de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de l'Union populetaire républicaine est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à l'Union populaire républicaine.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
