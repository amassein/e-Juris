<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029214518</ID>
<ANCIEN_ID>JG_L_2014_07_000000365132</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/21/45/CETATEXT000029214518.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  6ème sous-section jugeant seule, 09/07/2014, 365132, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-07-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>365132</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 6ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SPINOSI ; SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>Mme Clémence Olsina</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:365132.20140709</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 11 janvier et 15 avril 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune de Batz-sur-Mer, représentée par son maire ; la commune de Batz-sur-Mer demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 0905521 du 9 novembre 2012 par lequel le tribunal administratif de Nantes a annulé, à la demande de M. B...A..., les arrêtés des 18 juin, 23 septembre et 1er décembre 2010, 31 janvier, 2 mars et 4 avril 2011 le plaçant en disponibilité d'office du 27 juin 2010 au 15 avril 2011 ;<br/>
<br/>
              2°) de mettre à la charge de M. A...la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 84-53 du 26 janvier 1984;<br/>
<br/>
              Vu le décret n° 85-1054 du 30 septembre 1985 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Clémence Olsina, auditeur,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de la commune de Batz-sur-Mer et à Me Spinosi, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que, par arrêtés des 18 juin, 23 septembre et 1er décembre 2010, 31 janvier, 2 mars et 4 avril 2011, le maire de Batz-sur-Mer a placé M. B...A...en disponibilité d'office du 27 juin 2010 au 15 avril 2011 ; que, saisi par l'intéressé d'une demande d'annulation de ces arrêtés, le tribunal administratif de Nantes a fait droit à cette demande par un jugement du 9 novembre 2012, contre lequel la commune de Batz-sur-Mer se pourvoit en cassation ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 72 de la loi du 26 janvier 1984 portant dispositions relatives à la fonction publique territoriale : " La disponibilité est prononcée, soit à la demande de l'intéressé, soit d'office à l'expiration des congés prévus aux 2° (...) de l'article 57 (...) " ; qu'aux termes de l'article 81 de la même loi : " Les fonctionnaires territoriaux reconnus, par suite d'altération de leur état physique, inaptes à l'exercice de leurs fonctions peuvent être reclassés dans les emplois d'un autre cadre d'emploi ou corps s'ils ont été déclarés en mesure de remplir les fonctions correspondantes / Le reclassement est subordonné à la présentation d'une demande par l'intéressé " ; qu'aux termes de l'article 82 de la même loi : " En vue de permettre ce reclassement, l'accès à des cadres d'emplois, emplois ou corps d'un niveau supérieur, équivalent ou inférieur est ouvert aux intéressés, quelle que soit la position dans laquelle ils se trouvent, selon les modalités retenues par les statuts particuliers de ces cadres d'emplois, emplois ou corps (...) " ; que l'article 2 du décret du 30 septembre 1985 relatif au reclassement des fonctionnaires territoriaux reconnus inaptes à l'exercice de leurs fonctions précise que : " Lorsque l'état physique d'un fonctionnaire territorial, sans lui interdire d'exercer toute activité, ne lui permet pas d'exercer des fonctions correspondant aux emplois de son grade, l'autorité territoriale ou le président du centre national de la fonction publique territoriale ou le président du centre de gestion, après avis du comité médical, invite l'intéressé soit à présenter une demande de détachement dans un emploi d'un autre corps ou cadres d'emplois, soit à demander le bénéfice des modalités de reclassement prévues à l'article 82 de la loi n° 84-53 du 26 janvier 1984 " ;<br/>
<br/>
              3. Considérant qu'en vertu des dispositions citées ci-dessus, une autorité territoriale ne peut prononcer la disponibilité d'office d'un agent sans l'avoir préalablement invité à présenter, s'il le souhaite, une demande de reclassement dans un autre emploi de la collectivité ou, à défaut, d'une autre collectivité ; qu'il résulte des termes de la lettre du 30 juillet 2009 adressée par la commune de Batz-sur-Mer à M. A...que la commune, qui n'avait pas la possibilité de le reclasser en son sein, l'a invité à présenter une demande de reclassement dans une autre collectivité ; qu'ainsi, la commune requérante est fondée à soutenir qu'en jugeant qu'elle s'était abstenue d'inviter M. A...à présenter une telle demande et en annulant, pour ce motif, les arrêtés litigieux, le tribunal administratif de Nantes a dénaturé les pièces du dossier ; <br/>
<br/>
              4. Considérant que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la commune de Batz-sur-Mer est fondée à demander l'annulation du jugement qu'elle attaque ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la commune au titre de l'article L. 761-1 du code de justice administrative ; que les dispositions de cet article font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de Batz-sur-Mer qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
  Article 1er : Le jugement du tribunal administratif de Nantes du 9 avril 2012 est annulé. <br/>
<br/>
  Article 2 : L'affaire est renvoyée au tribunal administratif de Nantes. <br/>
<br/>
  Article 3 : La présente décision sera notifiée à la commune de Batz-sur-Mer et à M. B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
