<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038691281</ID>
<ANCIEN_ID>JG_L_2019_06_000000416864</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/69/12/CETATEXT000038691281.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 26/06/2019, 416864, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-06-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>416864</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP HEMERY, THOMAS-RAQUIN, LE GUERER</AVOCATS>
<RAPPORTEUR>Mme Louise Bréhier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:416864.20190626</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal des pensions de Nanterre, d'une part, d'annuler l'arrêté ministériel du 15 février 2016 fixant à 35 % le taux global de sa pension militaire d'invalidité et, d'autre part, d'enjoindre à la ministre des armées de calculer à nouveau sa pension en prenant en compte le taux d'invalidité de 20 %, définitivement acquis pour l'infirmité principale de séquelles de contusion du globe oculaire gauche. Par un jugement n° 16/00009 du 24 janvier 2017, le tribunal des pensions a fait droit à sa demande. <br/>
<br/>
              Par un arrêt n° 17/01714 du 24 octobre 2017, la cour régionale des pensions de Versailles a rejeté l'appel formé par la ministre des armées contre ce jugement.<br/>
<br/>
              Par un pourvoi, enregistré le 27 décembre 2018 au secrétariat du contentieux du Conseil d'Etat, la ministre des armées demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des pensions militaires d'invalidité et des victimes de guerre ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Louise Bréhier, auditrice, <br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Hémery, Thomas-Raquin, Le Guerer, avocat de M. A...; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Il ressort des pièces du dossier soumis aux juges du fond que M. A... bénéficiait depuis le 17 avril 1992 d'une pension militaire d'invalidité pour séquelles de contusion du globe oculaire gauche concédée au taux de 19,5 %, arrondi à 20 %. Par arrêté du 12 novembre 2013, M. A...s'est vu concéder une pension militaire d'invalidité mixte au titre de séquelles de contusion du globe oculaire gauche, acouphènes bilatéraux et hypoacousie bilatérale à un taux de 35 %. M. A...a demandé au tribunal des pensions de Nanterre d'annuler l'arrêté ministériel du 15 février 2016 par lequel la ministre des armées, saisie d'une demande de révision de sa pension pour aggravation de ses infirmités auditives, a reconduit et consolidé à titre définitif cette pension aux taux précédemment établis. Le tribunal a fait droit à cette demande et enjoint à la ministre des armées de calculer de nouveau la pension servie à M. A...en tenant compte du taux définitif de 20 % pour l'infirmité de séquelles de contusion du globe oculaire gauche. La ministre des armées se pourvoit en cassation contre l'arrêt de la cour régionale des pensions de Versailles qui a rejeté l'appel qu'elle a formé contre ce jugement. <br/>
<br/>
              2.	Aux termes de l'article L. 29 du code des pensions militaires d'invalidité et des victimes de la guerre, dans sa rédaction applicable à la date du litige : " Le titulaire d'une pension d'invalidité concédée à titre définitif peut en demander la révision en invoquant l'aggravation d'une ou plusieurs infirmités en raison desquelles cette pension a été accordée. Cette demande est recevable sans condition de délai. La pension ayant fait l'objet de la demande est révisée lorsque le degré d'invalidité résultant de l'infirmité ou de l'ensemble des infirmités est reconnu supérieur de 10 % au moins du pourcentage antérieur. (...)  ". <br/>
<br/>
              3.	Aux termes de l'article L. 14 du code des pensions militaires d'invalidité et des victimes de la guerre, dans sa rédaction applicable à la date du litige : " Dans le cas d'infirmités multiples dont aucune n'entraîne l'invalidité absolue, le taux d'invalidité est considéré intégralement pour l'infirmité la plus grave et pour chacune des infirmités supplémentaires, proportionnellement à la validité restante. / A cet effet, les infirmités sont classées par ordre décroissant de taux d'invalidité. / Toutefois, quand l'infirmité principale est considérée comme entraînant une invalidité d'au moins 20 %, les degrés d'invalidité de chacune des infirmités supplémentaires sont élevés d'une, de deux ou de trois catégories, soit de 5, 10, 15 %, et ainsi de suite, suivant qu'elles occupent les deuxième, troisième, quatrième rangs dans la série décroissante de leur gravité. Tous les calculs d'infirmités multiples prévus par le présent code, par les barèmes et textes d'application doivent être établis conformément aux dispositions de l'alinéa premier du présent article sauf dans les cas visés à l'article L. 15 ". Il résulte des dispositions de l'article L. 9 du même code, dans sa rédaction applicable à la date du litige et dont les dispositions ont été reprises à l'article L. 125-3 du code des pensions militaires d'invalidité et des victimes de guerre, que " (...) Quand l'invalidité est intermédiaire entre deux échelons, l'intéressé bénéficie du taux afférent à l'échelon supérieur (...) ". <br/>
<br/>
              4.	Quand le titulaire d'une pension militaire d'invalidité pour infirmité simple sollicite sa révision du fait de l'apparition de nouvelles infirmités ou de l'aggravation de ses infirmités n'entrainant pas une invalidité absolue, le calcul de sa pension révisée doit s'effectuer sur la base du degré réel d'invalidité correspondant à l'infirmité principale déjà pensionnée et du degré réel d'invalidité correspondant aux infirmités supplémentaires avec une exactitude arithmétique, sans qu'il soit possible d'arrondir à l'unité supérieure les chiffres fractionnaires intermédiaires. La règle de l'arrondi énoncée à l'article L. 9 du code des pensions militaires d'invalidité et des victimes de la guerre ne s'applique, le cas échéant, qu'une fois obtenu le degré global d'invalidité pour déterminer le taux de pension correspondant.<br/>
<br/>
              5.	Pour confirmer le jugement par lequel le tribunal des pensions de Nanterre a annulé l'arrêté ministériel du 15 février 2016 et enjoint au ministre des armées de calculer à nouveau la pension concédée à M.A..., la cour régionale des pensions de Versailles a jugé que le chiffre à prendre en compte pour l'infirmité de séquelles de contusion du globe oculaire gauche était le taux définitif de pension de 20 %, concédé le 17 avril 1992 après application de l'arrondi prévu à l'article L. 9 et par application des dispositions citées ci-dessus du troisième alinéa de l'article 14 du code des pensions militaires d'invalidité et des victimes de la guerre. En statuant ainsi, alors qu'il lui appartenait de prendre en compte le degré réel d'invalidité correspondant à l'infirmité, la cour régionale des pensions de Versailles a commis une erreur de droit. Par suite, la ministre des armées est fondée à demander l'annulation de l'arrêt qu'elle attaque. <br/>
<br/>
              6.	Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              7.	Il résulte de ce qui a été dit au point 5 que M.A..., qui ne bénéficie pas d'un droit acquis au maintien du taux de 20 % concédé après application de l'arrondi prévu aux dispositions de l'article L. 9 pour le calcul de la pension dont il sollicitait la révision, n'est pas fondé à soutenir que la ministre des armées aurait dû prendre en compte ce taux pour l'infirmité principale de séquelles de contusion du globe oculaire gauche. <br/>
<br/>
              8.	Ainsi, la ministre des armées est fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal des pensions de Nanterre a annulé l'arrêté ministériel du 13 janvier 2015 et lui a enjoint de calculer à nouveau la pension de M.A....<br/>
              9.	Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt du 24 octobre 2017 de la cour régionale des pensions de Versailles et le jugement du 24 janvier 2017 du tribunal des pensions de Nanterre sont annulés.<br/>
<br/>
Article 2 : La demande présentée par M. A...devant le tribunal des pensions de Nanterre est rejetée.<br/>
<br/>
Article 3 : Les conclusions de M. A...présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 4 : La présente décision sera notifiée à la ministre des armées et à M. B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
