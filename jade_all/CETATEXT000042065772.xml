<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042065772</ID>
<ANCIEN_ID>JG_L_2020_06_000000428159</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/06/57/CETATEXT000042065772.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 29/06/2020, 428159, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-06-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428159</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI, REBEYROL</AVOCATS>
<RAPPORTEUR>M. Yves Doutriaux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:428159.20200629</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A... B... a demandé au tribunal administratif de Lille d'annuler pour excès de pouvoir l'arrêté du 13 avril 2017 par lequel le préfet du Nord a rejeté sa demande de titre de séjour, lui a fait obligation de quitter le territoire français et a fixé le pays de renvoi. <br/>
<br/>
              Par un jugement n° 1704619 du 6 février 2018, le tribunal administratif de Lille a annulé cet arrêté.<br/>
<br/>
              Par un arrêt n° 18DA00437 du 11 octobre 2018, la cour administrative d'appel de Douai a, sur appel du préfet du Nord, annulé ce jugement et rejeté la demande d'annulation de Mme B....<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 18 février et 15 mai 2019 et 29 mai 2020 au secrétariat du contentieux du Conseil d'Etat, Mme B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel du préfet du Nord ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement à la SCP Gatineau, Fattaccini, son avocat, de la somme de 3500 euros au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yves Doutriaux, conseiller d'Etat,<br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, Rebeyrol, avocat de Mme B... ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1.	Il ressort des pièces du dossier soumis aux juges du fond que Mme B..., ressortissante congolaise née en décembre 1995, est entrée en France en juin 2012. Alors mineure et isolée, elle a été confiée au service de l'aide sociale à l'enfance par décision du juge des enfants. A sa majorité et pendant les trois années suivantes, elle a conclu avec le même service un " contrat d'accueil provisoire jeune majeur ". De 2014 à 2017, elle a séjourné en France sous couvert d'une carte de séjour temporaire délivrée, en sa qualité d'étudiante, par le préfet du Nord. L'intéressée, qui a interrompu ses études en 2016, a sollicité auprès du préfet du Nord la délivrance d'une carte de séjour temporaire au titre de la vie privée et familiale. Par un arrêté du 13 avril 2017, le préfet du Nord a rejeté cette demande, lui a fait obligation de quitter le territoire français et a fixé le pays de destination. Mme B... se pourvoit en cassation contre l'arrêt du 11 octobre 2018 par lequel la cour administrative d'appel de Douai a, sur appel du préfet du Nord, infirmé le jugement du 6 février 2018 par lequel le tribunal administratif de Lille avait annulé cet arrêté et a rejeté ses conclusions aux fins d'annulation. <br/>
<br/>
              2.	Pour juger que la décision de refus de titre de séjour prise par le préfet du Nord n'avait pas porté au droit de Mme B... au respect de sa vie privée et familiale une atteinte disproportionnée au but en vue duquel elle a été prise, la cour administrative d'appel de Douai a relevé que l'intéressée ne démontrait pas qu'elle serait isolée en cas de retour dans son pays d'origine et que, après l'interruption de ses études, elle n'exerçait aucune activité professionnelle. Il ressort toutefois des pièces du dossier soumis aux juges du fond que l'intéressée, alors mineure, a dû quitter son pays d'origine en raison des relations dégradées avec sa famille avec laquelle elle n'a plus eu de contact et qu'à la date du refus de titre de séjour, elle séjournait en France depuis près de cinq ans, dont une partie dans le cadre d'une prise en charge par l'aide sociale à l'enfance. Par ailleurs, ainsi que l'a souligné la cour, elle a fait de réels efforts d'intégration, notamment en obtenant de bons résultats dans le cadre de son certificat d'aptitude professionnelle et elle n'a dû interrompre ses études qu'en raison de son impossibilité d'obtenir un contrat en alternance, ce qui l'a conduite à rechercher un emploi. Compte tenu de ces éléments, la cour a inexactement qualifié les faits de l'espèce en jugeant que le refus de titre de séjour n'avait pas méconnu les dispositions du 7° de L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile, ni les stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. Par suite, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, Mme B... est fondée à demander l'annulation de l'arrêt attaqué.<br/>
<br/>
              3.	Mme B... a obtenu le bénéfice de l'aide juridictionnelle. Par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à la SCP Gatineau, Fattaccini, avocat de Mme B..., la somme de 3 000 euros à ce titre, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Douai du 11 octobre 2018 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Douai.<br/>
<br/>
Article 3 : L'Etat versera, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, une somme de 3 000 euros à la SCP Gatineau, Fattaccini, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat. <br/>
Article 4 : La présente décision sera notifiée à Mme A... B... et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
