<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036743974</ID>
<ANCIEN_ID>JG_L_2018_03_000000401476</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/74/39/CETATEXT000036743974.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 26/03/2018, 401476</TITRE>
<DATE_DEC>2018-03-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>401476</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>M. Marc Firoud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:401476.20180326</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              La société Aéroport Montpellier Méditerranée (SAMM) a demandé au tribunal administratif de Montpellier de condamner le service départemental d'incendie et de secours de l'Hérault (SDIS 34) à lui verser une somme de 174 105,51 euros en réparation des préjudices financiers résultant du paiement d'un titre exécutoire illégal. Par un jugement n° 1302535 du 24 novembre 2014, le tribunal administratif de Montpellier a rejeté cette demande. <br/>
<br/>
              Par un arrêt n° 15MA00335 du 9 mai 2016, la cour administrative d'appel de Marseille a, sur appel de la SAMM, annulé ce jugement et condamné le SDIS 34 à payer à cette société une somme de 146 890,34 euros avec intérêts au taux légal, capitalisés à compter du 20 février 2014. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et deux nouveaux mémoires, enregistrés les 13 juillet, 13 octobre 2016, le 22 février 2017 et 13 février 2018 au secrétariat du contentieux du Conseil d'Etat, le SDIS 34 demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter les conclusions d'appel de la SAMM ; <br/>
<br/>
              3°) de mettre à la charge de la SAMM le versement de la somme de 5 000 euros en application de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Firoud, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat du service départemental d'incendie et de secours de l'Hérault et à la SCP Monod, Colin, Stoclet, avocat de la société Aéroport Montpellier Méditerranée.<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une convention du 19 décembre 1997, le service départemental d'incendie et de secours de l'Hérault (SDIS 34) s'est engagé, en contrepartie du paiement d'une redevance mensuelle, à assurer le service de sauvetage et de lutte contre l'incendie de l'aéroport de Fréjorgues, exploité par la chambre de commerce et d'industrie (CCI) de Montpellier ; qu'à la suite du refus de la CCI de renouveler la convention à compter du 1er juillet 2004, le SDIS 34 a émis le 8 mars 2005 un titre exécutoire à son encontre, d'un montant de 5 801 330 euros, correspondant à l'indemnité contractuelle de non-renouvellement ; que, par un jugement du 4 avril 2008, le tribunal administratif de Montpellier a annulé ce titre à la demande de la CCI ; que, par un arrêt du 14 février 2011, la cour administrative d'appel de Marseille a annulé ce jugement et rejeté la demande de la CCI ; que la société Aéroport Montpellier Méditerranée (SAMM), venue aux droits et obligations de la CCI, a procédé au règlement de la somme de 5 801 330 euros par trois paiements intervenus en octobre et décembre 2011 ; que, par une décision n° 348676 du 22 juin 2012, le Conseil d'Etat, statuant au contentieux a annulé l'arrêt de la cour et confirmé l'annulation du titre exécutoire émis le 8 mars 2005 à l'encontre de la CCI ; que le SDIS 34 a alors remboursé à la SAMM la somme que celle-ci lui avait versée en 2011 ; que, par un jugement du 24 novembre 2014, le tribunal administratif de Montpellier a rejeté la demande de la SAMM tendant à ce que le SDIS 34 soit condamné à l'indemniser du préjudice qu'elle estime avoir subi ; que, par un arrêt du 9 mai 2016 contre lequel le SDIS 34 se pourvoit en cassation, la cour administrative d'appel de Marseille a annulé ce jugement et condamné le SDIS 34 à réparer le préjudice subi par  la SAMM ;<br/>
<br/>
              2. Considérant qu'aux termes du 1° de l'article L. 1617-5 du code général des collectivités territoriales : " En l'absence de contestation, le titre de recettes individuel ou collectif émis par la collectivité territoriale ou l'établissement public local permet l'exécution forcée d'office contre le débiteur. / Toutefois, l'introduction devant une juridiction de l'instance ayant pour objet de contester le bien-fondé d'une créance assise et liquidée par une collectivité territoriale ou un établissement public local suspend la force exécutoire du titre / (...) " ; <br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions que l'introduction d'un recours tendant à l'annulation d'un titre de recettes émis par une collectivité territoriale ou un établissement public local suspend la force exécutoire de ce titre ; qu'en cas d'annulation de celui-ci par le tribunal administratif, cette force exécutoire est rétablie en cas d'annulation du jugement par le juge d'appel ou de cassation ; que, dans cette hypothèse, le comptable public peut poursuivre le recouvrement de la créance en cause sur le fondement du titre exécutoire initial ;<br/>
<br/>
              4. Considérant, toutefois, que la cour a relevé que le préjudice dont se prévalait devant elle la SAMM tenait aux intérêts financiers dont étaient assortis les emprunts qu'elle avait souscrits pour payer la somme dont elle était redevable à la suite de l'arrêt du 14 février 2011, ensuite annulé par le Conseil d'Etat, statuant au contentieux ; qu'en jugeant qu'il existait un lien direct entre l'illégalité fautive du titre émis par le SDIS 34 en 2005 et le préjudice ainsi invoqué devant elle, la cour a inexactement qualifié les faits qui lui étaient soumis ; qu'il suit de là que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, le SDIS 34 est fondé à demander l'annulation de l'arrêt attaqué ; <br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              6. Considérant, en premier lieu, que la SAMM soutient le jugement du 24 novembre 2014 du tribunal administratif de Montpellier est irrégulier dès lors qu'il ne comporte pas certaines mentions obligatoires, qu'il est insuffisamment motivé et qu'il ne comporte pas les signatures requises par le code de justice administrative ; que, toutefois, ces moyens manquent en fait ; <br/>
<br/>
              7. Considérant, en second lieu, qu'ainsi qu'il a été dit, le préjudice dont se prévaut la SAMM ne saurait être regardé comme directement lié à l'illégalité fautive du titre émis en 2005 par le SDIS 34 ou à l'attitude malveillante dont aurait fait preuve le SDIS pour l'exécution de ce titre ; que, par suite, la SAMM n'est pas fondée à soutenir que c'est à tort que le tribunal administratif de Montpellier a rejeté ses conclusions tendant à ce que le SDIS 34 soit condamné à l'indemniser ;<br/>
<br/>
              8. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la SAMM la somme de 3 500 euros  à verser au SDIS 34 au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que les mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge du SDIS 34 qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 9 mai 2016 est annulé.<br/>
Article 2 : La requête présentée par la société Aéroport Montpellier Méditerranée devant la cour administrative d'appel de Marseille est rejetée.<br/>
Article 3 : Les conclusions de la société Aéroport Montpellier Méditerranée présentées devant le Conseil d'Etat au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La société Aéroport Montpellier Méditerranée versera une somme de 3 500 euros au service départemental d'incendie et de secours de l'Hérault au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : La présente décision sera notifiée au service départemental d'incendie et de secours de l'Hérault et à la société Aéroport Montpellier Méditerranée.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">18-03-02-01-01 COMPTABILITÉ PUBLIQUE ET BUDGET. CRÉANCES DES COLLECTIVITÉS PUBLIQUES. RECOUVREMENT. PROCÉDURE. ÉTAT EXÉCUTOIRE. - SUSPENSION DE LA FORCE EXÉCUTOIRE D'UN TITRE DE RECETTES EN CAS DE CONTESTATION DU BIEN FONDÉ DE LA CRÉANCE (1° DE L'ART. L. 1617-5 DU CGCT) - ANNULATION, PAR LE JUGE D'APPEL OU DE CASSATION, DU JUGEMENT ANNULANT CE TITRE - CONSÉQUENCES - 1) RÉTABLISSEMENT DE LA FORCE EXÉCUTOIRE DU TITRE - EXISTENCE - CONSÉQUENCE - FACULTÉ DE POURSUIVRE LE RECOUVREMENT DE LA CRÉANCE SUR SON FONDEMENT - EXISTENCE - 2) PRÉJUDICE CONSTITUÉ PAR LES CHARGES DES EMPRUNTS SOUSCRITS POUR PAYER LES SOMMES DUES EN EXÉCUTION DE L'ARRÊT D'APPEL - LIEN DE CAUSALITÉ DIRECT AVEC L'ILLÉGALITÉ FAUTIVE ENTACHANT LE TITRE FINALEMENT ANNULÉ - ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 18-03-02-01-01 1) Il résulte du 1° de l'article L. 1617-5 du code général des collectivités territoriales (CGCT) que l'introduction d'un recours tendant à l'annulation d'un titre de recettes émis par une collectivité territoriale ou un établissement public local suspend la force exécutoire de ce titre. En cas d'annulation de celui-ci par un tribunal administratif, cette force exécutoire est rétablie en cas d'annulation du jugement par le juge d'appel ou de cassation. Dans cette hypothèse, le comptable public peut poursuivre le recouvrement de la créance en cause sur le fondement du titre exécutoire initial.... ,,2) Titre de recettes émis en 2005, annulé par le tribunal administratif dont le jugement a été annulé en appel. Conseil d'Etat annulant l'arrêt de la cour administrative d'appel et le titre exécutoire. Le préjudice constitué par les intérêts financiers dont étaient assortis les emprunts souscrits par la société pour payer la somme dont elle était redevable à la suite de l'arrêt d'appel n'est pas directement lié à l'illégalité fautive entachant le titre émis en 2005.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 2 juin 2017, Communauté de communes Auray Quiberon Terre Atlantique, n° 397571, p. 182.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
