<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030445719</ID>
<ANCIEN_ID>JG_L_2015_03_000000383553</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/44/57/CETATEXT000030445719.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  7ème sous-section jugeant seule, 23/03/2015, 383553, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-03-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>383553</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 7ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>FOUSSARD ; SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR>Mme Charline Nicolas</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:383553.20150323</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Paris, d'une part, l'annulation de l'arrêté du 23 janvier 2012 du maire de Paris mettant fin à son stage d'attaché des administrations parisiennes et refusant sa titularisation dans ce corps et, d'autre part, à ce qu'il soit enjoint au maire de Paris de le réintégrer ou, à titre subsidiaire, à ce que soit ordonné le réexamen de sa titularisation à l'issue de la prolongation de son stage. Par jugement n° 1205894 du 5 juin 2013 le tribunal administratif de Paris a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 13PA02888 du 31 juillet 2014, la cour administrative d'appel de Paris a transmis la requête d'appel de M. A...au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative.<br/>
<br/>
              Par un pourvoi, enregistré le 5 août 2014 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) à titre principal, de renvoyer l'examen de l'affaire à la cour administrative d'appel de Paris ;<br/>
<br/>
              2°) à titre subsidiaire, d'annuler ce jugement n° 1205894 du 5 juin 2013 ;<br/>
<br/>
              3°) de mettre à la charge de la Ville de Paris une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le décret n° 92-1194 du 4 novembre 1992 ;<br/>
              - le décret n° 94-415 du 24 mai 1994 ;<br/>
              - le décret n° 2007-767 du 9 mai 2007 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Charline Nicolas, auditeur,  <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de M. A...et à Me Foussard, avocat de la Ville de Paris ;<br/>
<br/>
<br/>
<br/>Sur la compétence du Conseil d'Etat pour statuer sur les conclusions tendant à l'annulation du jugement du tribunal administratif de Paris du 5 juin 2013 :<br/>
<br/>
              1. Considérant qu'il résulte des dispositions du deuxième alinéa de l'article R. 811-1 du code de justice administrative, combinées avec celles de l'article R. 222-13 du même code, dans leur rédaction en vigueur à la date du jugement du 5 juin 2013 du tribunal administratif de Paris, que le tribunal administratif statue en premier et dernier ressort dans les litiges relatifs à la situation individuelle des agents publics, à l'exception de ceux concernant l'entrée au service, la discipline ou la sortie du service et sauf pour les recours comportant des conclusions tendant au versement ou à la décharge de sommes d'un montant supérieur au montant déterminé par les articles R. 222-14 et R. 222-15 de ce code ;<br/>
<br/>
              2. Considérant que M. A...demande l'annulation du jugement du tribunal administratif de Paris du 5 juin 2013 rejetant ses conclusions tendant à l'annulation de la décision mettant fin à son stage et refusant sa titularisation dans le corps des attachés de la Ville de Paris ; que M.A..., fonctionnaire de la Poste, ayant été nommé en qualité de stagiaire dans ce corps à la suite de sa réussite à un concours interne, le litige qui l'oppose à la Ville de Paris concerne le déroulement de sa carrière et non son entrée au service ; qu'elle est donc au nombre des litiges sur lesquels le tribunal administratif statue en premier et dernier ressort ; qu'il en résulte que, contrairement à ce que soutient M.A..., sa requête tendant à l'annulation du jugement du tribunal administratif de Paris du 5 juin 2013 doit être regardée comme un pourvoi en cassation ;<br/>
<br/>
<br/>
              Sur les conclusions tendant à l'annulation du jugement du tribunal administratif de Paris : <br/>
<br/>
              3. Considérant, en premier lieu, que M. A...a été averti, par le greffe du tribunal, le 30 avril 2013 que l'affaire serait examinée à l'audience du 22 mai 2013 à 9 h 30 ; que si le 17 mai, il a été informé que l'audience se tiendrait non à 9 h 30 mais à 10 h 15, il ne saurait sérieusement soutenir que les dispositions de l'article R. 711-2 du code de justice administrative aux termes desquels les parties sont averties du jour où l'affaire sera appelée à l'audience " sept jours au moins " avant cette date, auraient été méconnues du seul fait de cette modification d'horaire ;<br/>
<br/>
              4. Considérant, en deuxième lieu, qu'un agent public ayant, à la suite de son recrutement ou dans le cadre de la formation qui lui est dispensée, la qualité de stagiaire se trouve dans une situation probatoire et provisoire ; qu'il en résulte qu'alors même que la décision de ne pas le titulariser en fin de stage est fondée sur l'appréciation portée par l'autorité compétente sur son aptitude à exercer les fonctions auxquelles il peut être appelé et, de manière générale, sur sa manière de servir, et se trouve ainsi prise en considération de sa personne, elle n'est pas - sauf à revêtir le caractère d'une mesure disciplinaire - au nombre des mesures qui ne peuvent légalement intervenir sans que l'intéressé ait été mis à même de faire valoir ses observations ou de prendre connaissance de son dossier, et n'est soumise qu'aux formes et procédures expressément prévues par les lois et les règlements ; qu'en jugeant inopérant, pour ce motif, le moyen tiré de l'irrégularité de la procédure tenant à l'absence alléguée de respect par l'administration du principe du contradictoire de la procédure, le tribunal administratif n'a pas commis d'erreur de droit ; qu'est nouveau en cassation, et par suite et en tout état de cause  inopérant, le moyen tiré de ce que l'administration n'a pas suivi la procédure qu'elle avait elle-même établie en indiquant au requérant qu'il pouvait consulter son dossier alors qu'il n'a pas pu le consulter ;  <br/>
<br/>
              5. Considérant, en troisième lieu, qu'il résulte des pièces du dossier soumis aux juges du fond que figurait dans l'ordre du jour adressé aux membres de la commission administrative paritaire l'examen de la situation du requérant à l'issue de la période normale de stage ; que la circonstance que l'administration ait modifié, préalablement à la tenue de la commission, sa position sur la situation de M.A..., choisissant de ne pas le titulariser plutôt que de prolonger son stage ne pouvait en tout état de cause être regardée comme une modification de l'ordre du jour de la commission appelant une information particulière de ses membres ; que le tribunal administratif n'a, dès lors, commis aucune erreur de droit en n'accueillant pas le moyen tiré de l'irrégularité de la procédure sur ce point ; que par une appréciation souveraine, exempte de dénaturation, il a estimé que la commission avait été mise à même de se prononcer sur la situation de M. A...; <br/>
<br/>
              6. Considérant, enfin, qu'en estimant que la progression de M. A...lors de son stage n'avait pas été suffisante et qu'il n'avait pas les qualités et compétences pour occuper un poste d'attaché, la Ville de Paris n'avait pas commis d'erreur manifeste d'appréciation, le tribunal administratif de Paris s'est livré à une appréciation souveraine, exempte de dénaturation ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que le pourvoi de M. A...doit être rejeté, y compris ses conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de M. A...la somme que demande la Ville de Paris en application des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A...est rejeté.<br/>
Article 2 : Les conclusions de la Ville de Paris tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. B...A...et à la Ville de Paris.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
