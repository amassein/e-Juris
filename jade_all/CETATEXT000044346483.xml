<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044346483</ID>
<ANCIEN_ID>JG_L_2021_11_000000457748</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/34/64/CETATEXT000044346483.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 08/11/2021, 457748, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-11-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>457748</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:457748.20211108</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B... A... a demandé au juge des référés du tribunal administratif de Paris, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre au préfet de police de lui délivrer un récépissé de demande de renouvellement de certificat de résidence algérien portant la mention " étudiant ", dans le délai de vingt-quatre heures à compter de l'ordonnance à intervenir, et sous astreinte de 100 euros par jour de retard. <br/>
<br/>
              Par une ordonnance n° 2122292 du 21 octobre 2021, le juge des référés du tribunal administratif de Paris a rejeté sa demande.<br/>
<br/>
              Par une requête et un mémoire, enregistrés les 22 et 25 octobre 2021 au secrétariat du contentieux du Conseil d'Etat, Mme A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) de l'admettre au bénéfice de l'aide juridictionnelle provisoire ;<br/>
<br/>
              2°) d'annuler cette ordonnance ;<br/>
<br/>
              3°) d'enjoindre au préfet de police de lui délivrer un récépissé de demande de renouvellement de certificat de résidence algérien portant la mention " étudiant ", dans le délai de vingt-quatre heures à compter de l'ordonnance à intervenir, et sous astreinte de 100 euros par jour de retard.<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - l'ordonnance du juge des référés du tribunal administratif de Paris est entachée de défaut de motivation dès lors que le juge des référés n'a pas caractérisé l'urgence au regard des atteintes à la liberté d'aller et venir et au droit au respect de la vie privée et familiale ;<br/>
              - la condition d'urgence est satisfaite dès lors que, d'une part, elle ne peut plus poursuivre ses études et, d'autre part, son contrat de travail et son stage prendront fin faute de produire un récépissé, ce qui la privera de toute ressource et la contraindra de quitter son logement ; <br/>
              - le refus du préfet de police de délivrer un récépissé porte atteinte à la liberté d'aller et venir et au droit au respect de la vie privée et familiale dès lors qu'il la prive de tout document lui permettant d'établir la régularité de sa situation et la place dans une situation de grande précarité. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. L'article R. 311-4 du code de l'entrée et du séjour des étrangers en France dispose que " Il est remis à tout étranger admis à souscrire une demande de première délivrance ou de renouvellement de titre de séjour un récépissé qui autorise la présence de l'intéressé sur le territoire pour la durée qu'il précise (...) ". Aux termes de l'article R. 431-2 du même code, " La demande d'un titre de séjour figurant sur une liste fixée par arrêté du ministre chargé de l'immigration s'effectue eu moyen d'un téléservice à compter de la date fixée par le même arrêté ". Aux termes de l'article R. 431-5 de ce code, " Si l'étranger séjourne déjà en France, sa demande est présentée dans les délais suivants : 1° L'étranger qui dispose d'un document de séjour mentionné aux 2° à 8° de l'article L. 411-1 présente sa demande de titre de séjour entre le cent-vingtième jour et le soixantième jour qui précède l'expiration de ce document de séjour lorsque sa demande porte sur un titre de séjour figurant dans la liste mentionnée à l'article R. 431-2. (...) ". Aux termes de l'article R. 431-15-1, " Le dépôt d'une demande présentée au moyen du téléservice mentionné à l'article R. 431-2 donne lieu à la délivrance immédiate d'une attestation dématérialisée de dépôt en ligne. Ce document ne justifie pas de la régularité du séjour de son titulaire. Lorsque l'instruction d'une demande complète et déposée dans le respect des délais mentionnés à l'article R. 431-5 se poursuit au-delà de la date de validité du document de séjour détenu, le préfet est tenu de mettre à la disposition du demandeur via le téléservice mentionné au premier alinéa une attestation de prolongation de l'instruction de sa demande dont la durée de validité ne peut être supérieure à trois mois. Ce document, accompagné du document de séjour expiré, lui permet de justifier de la régularité de son séjour pendant la durée qu'il précise. Lorsque l'instruction se prolonge, en raison de circonstances particulières, au-delà de la date d'expiration de l'attestation, celle-ci est renouvelée aussi longtemps que le préfet n'a pas statué sur la demande (...) ". L'arrêté du 27 avril 2021 pris en application de l'article R. 431-2 du code de l'entrée et du séjour des étrangers et du droit d'asile relatif aux titres de séjour dont la demande s'effectue au moyen d'un téléservice impose d'effectuer au moyen du téléservice mentionné à l'article R. 431-2 du code de l'entrée et du séjour des étrangers et du droit d'asile " à compter du 1er mai 2021, les demandes  (...) de certificats de résidence algériens portant la mention " étudiant " prévus au titre III du protocole annexé à l'accord franco-algérien du 27 décembre 1968 modifié ". <br/>
<br/>
              3. Il résulte de l'instruction devant le juge des référés du tribunal de Paris que Mme A..., ressortissante algérienne, était titulaire, depuis le 13 septembre 2018, d'un certificat de résidence algérien portant la mention " étudiant ". Son dernier titre de séjour était valable jusqu'au 14 octobre 2021. En application des dispositions combinées des articles R. 431-1 et R. 431-5 du code de l'entrée et du séjour en France des étrangers et du droit d'asile et de l'arrêté du 27 avril 2021, citées au point 2, pour bénéficier, en application de l'article R. 431-15-1 du code de l'entrée et du séjour des étrangers en France et du droit d'asile, du document justifiant de la régularité de son séjour pendant l'instruction de sa demande, elle devait présenter sa demande de renouvellement de ce titre de séjour par le biais du téléservice dans un délai compris entre le cent-vingtième jour et le soixantième jour précèdant l'expiration de ce document. N'ayant présenté sa demande de renouvellement que le 15 septembre 2021, elle n'est pas fondée à soutenir que c'est à tort que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Paris a rejeté sa demande tendant, sur le fondement de l'article L. 521-2 du code de justice administrative, à enjoindre au préfet de police de lui délivrer un récépissé de demande de renouvellement de certificat de résidence algérien portant la mention " étudiant ". <br/>
<br/>
              4. Il résulte de tout ce qui précède que la requête de Mme A... doit être rejetée selon la procédure prévue par l'article L. 522-3 du code de justice administrative, sans qu'il y ait lieu de l'admettre au bénéfice de l'aide juridictionnelle provisoire. <br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de Mme A... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à Mme B... A....<br/>
Fait à Paris, le 8 novembre 2021<br/>
Signé : Nathalie Escaut<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
