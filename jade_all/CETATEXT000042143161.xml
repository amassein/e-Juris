<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042143161</ID>
<ANCIEN_ID>JG_L_2020_07_000000441521</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/14/31/CETATEXT000042143161.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 09/07/2020, 441521, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-07-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>441521</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:441521.20200709</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 29 juin 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... D... et M. B... C... demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) de suspendre l'exécution de l'article 20 du décret n° 2020-548 du 11 mai 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 8 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Ils soutiennent que :<br/>
              - leur requête relève de la compétence de premier et dernier ressort du Conseil d'Etat ;<br/>
              - elle est recevable ;<br/>
              - la condition d'urgence est remplie eu égard, d'une part, à la présomption en ce sens induite par la loi n° 2020-290 du 23 mars 2020 d'état d'urgence pour faire face à l'épidémie de covid-19 et, d'autre part, au nombre de morts dus à l'épidémie de covid-19 en France ;<br/>
              - il existe un doute sérieux quant à la légalité des dispositions contestées ;<br/>
              - elles sont entachées d'incompétence en ce que le Rivotril ne peut être regardé comme un médicament approprié pour l'éradication de la catastrophe sanitaire au sens de l'article L. 3131-15 du code de la santé publique, aucune donnée acquise de la science n'existant quant au traitement des malades du covid-19 par ce médicament ;<br/>
              - elles permettent une prescription de cette spécialité non conforme aux indications de son autorisation de mise sur le marché ;<br/>
              - elles sont entachées d'erreur manifeste d'appréciation, le clonazépam injectable ne pouvant être sans faute médicale prescrit aux patients atteints ou susceptibles d'être atteints de covid-19 dès lors qu'il est contre-indiqué en cas d'insuffisance respiratoire grave ;<br/>
              - cette contre-indication et la circonstance que la Haute autorité de santé recommande le clonazépam injectable comme agent de sédation profonde et continue jusqu'au décès révèlent que ces dispositions ont pour objet, non de traiter les patients atteints de covid-19, mais de provoquer délibérément leur mort ;<br/>
              - les dispositions litigieuses permettent de recourir à une sédation profonde et continue dans des conditions méconnaissant les droits des malades résultant de la loi n° 2016-87 du 2 février 2016 et du décret n° 2016-1066 du 3 août 2016, supposant notamment le consentement du malade lorsqu'il peut exprimer sa volonté et une procédure collégiale dans le cas contraire ;<br/>
              - elles portent atteinte au droit fondamental à la protection de la santé et au droit d'égal accès aux soins, en méconnaissance des articles L. 1110-1, L. 1110-3, L. 1110-5 et L. 1411-1 du code de la santé publique, en ce que, d'une part, elles autorisent l'injection de clonazépam à des patients atteints de covid-19, à l'égard desquels il est contre-indiqué, et, d'autre part, elles instaurent une différence de traitement entre les patients ayant accès aux respirateurs artificiels et ceux, notamment âgés et accueillis dans les établissements hébergeant des personnes âgées dépendantes, n'y ayant pas accès, qu'elles discriminent en autorisant leur sédation profonde et continue.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule ;<br/>
              - le code de la santé publique ;<br/>
              - la loi n° 2016-87 du 2 février 2016 ; <br/>
              - la loi n° 2020-290 du 23 mars 2020 ;<br/>
              - la loi n° 2020-546 du 11 mai 2020 ;<br/>
              - le décret n° 2016-1066 du 3 août 2016 ;<br/>
              - le décret n° 2020-293 du 23 mars 2020 ;<br/>
              - le décret n° 2020-548 du 11 mai 2020 ;<br/>
              - le décret n° 2020-663 du 31 mai 2020 ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". Aux termes de l'article L. 522-3 du même code : " Lorsque la demande ne présente pas un caractère d'urgence ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable, ou qu'elle est mal fondée, le juge des référés peut la rejeter par une ordonnance motivée, sans qu'il y ait lieu d'appliquer les deux premiers alinéas de l'article L. 522-1 ".<br/>
<br/>
              Sur les dispositions applicables : <br/>
<br/>
              2. D'une part, aux termes de l'article L. 5121-12-1 du code de la santé publique : " I.- Une spécialité pharmaceutique peut faire l'objet d'une prescription non conforme à son autorisation de mise sur le marché en l'absence de spécialité de même principe actif, de même dosage et de même forme pharmaceutique disposant d'une autorisation de mise sur le marché ou d'une autorisation temporaire d'utilisation dans l'indication ou les conditions d'utilisation considérées, sous réserve qu'une recommandation temporaire d'utilisation établie par l'Agence nationale de sécurité du médicament et des produits de santé sécurise l'utilisation de cette spécialité dans cette indication ou ces conditions d'utilisation. Lorsqu'une telle recommandation temporaire d'utilisation a été établie, la spécialité peut faire l'objet d'une prescription dans l'indication ou les conditions d'utilisations correspondantes dès lors que le prescripteur juge qu'elle répond aux besoins du patient. (...) / En l'absence de recommandation temporaire d'utilisation dans l'indication ou les conditions d'utilisation considérées, une spécialité pharmaceutique ne peut faire l'objet d'une prescription non conforme à son autorisation de mise sur le marché qu'en l'absence d'alternative médicamenteuse appropriée disposant d'une autorisation de mise sur le marché ou d'une autorisation temporaire d'utilisation et sous réserve que le prescripteur juge indispensable, au regard des données acquises de la science, le recours à cette spécialité pour améliorer ou stabiliser l'état clinique de son patient. / (...) / ".<br/>
<br/>
              3. D'autre part, aux termes de l'article L. 3131-15 du code de la santé publique : " Dans les circonscriptions territoriales où l'état d'urgence sanitaire est déclaré, le Premier ministre peut, par décret réglementaire pris sur le rapport du ministre chargé de la santé, aux seules fins de garantir la santé publique : / (...) / 9° En tant que de besoin, prendre toute mesure permettant la mise à la disposition des patients de médicaments appropriés pour l'éradication de la catastrophe sanitaire (...). / Les mesures prescrites en application des 1° à 10° du présent article sont strictement proportionnées aux risques sanitaires encourus et appropriées aux circonstances de temps et de lieu. Il y est mis fin sans délai lorsqu'elles ne sont plus nécessaires. (...) ". <br/>
<br/>
              Sur les circonstances et les mesures prises par le Premier ministre :<br/>
<br/>
              4. En raison de l'émergence d'un nouveau coronavirus, responsable de la maladie covid-19, de caractère pathogène et particulièrement contagieux, et de sa propagation sur le territoire français, après de premières mesures arrêtées par le ministre des solidarités et de la santé et par le Premier ministre, en particulier l'interdiction, décidée par le décret du 16 mars 2020, de déplacement de toute personne, en principe, hors de son domicile, la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19 a déclaré l'état d'urgence sanitaire pour une durée de deux mois à compter du 24 mars 2020. L'article 1er de la loi du 11 mai 2020 prorogeant l'état d'urgence sanitaire et complétant ses dispositions a prorogé cet état d'urgence sanitaire jusqu'au 10 juillet 2020 inclus. L'interdiction de déplacement hors du domicile, sous réserve d'exceptions limitativement énumérées et devant être dûment justifiées, s'est appliquée entre le 17 mars et le 11 mai 2020, avant que ne soient prescrites, par décret du 11 mai 2020, de nouvelles mesures générales, moins contraignantes que celles applicables dans la période antérieure mais continuant d'imposer de strictes sujétions afin de faire face à l'épidémie de covid-19 puis, par décret du 31 mai 2020, des mesures moins contraignantes encore, compte tenu de l'évolution de l'épidémie et de la situation sanitaire. <br/>
<br/>
              5. Le clonazepam est une molécule de la classe des benzodiazépines, dont les propriétés sont notamment anxiolytiques et sédatives, commercialisée par le laboratoire Roche sous le nom de marque de Rivotril en vertu, pour la spécialité pharmaceutique correspondant à sa forme injectable, d'une autorisation de mise sur le marché délivrée le 21 février 1995, avec pour indication thérapeutique le traitement de l'épilepsie. En l'absence de toute recommandation temporaire d'utilisation prise en application de l'article L. 5121-12-1 du code de la santé publique, cette spécialité ne peut être prescrite pour une autre indication qu'à la condition que le prescripteur juge indispensable, au regard des données acquises de la science, le recours à cette spécialité pour améliorer ou stabiliser l'état clinique de son patient et en l'absence d'alternative médicamenteuse appropriée disposant d'une autorisation de mise sur le marché ou d'une autorisation temporaire d'utilisation. Toutefois, par le II de l'article 12-3 du décret du 23 mars 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire, inséré par le décret du 28 mars 2020 visé ci-dessus, dont les effets ont été prolongés par décret du 14 avril 2020, le Premier ministre a, sur le fondement des dispositions citées au point 3, par dérogation à l'article L. 5121-12-1 du code de la santé publique, défini les conditions dans lesquelles cette spécialité peut faire l'objet d'une prescription non conforme à son autorisation de mise sur le marché en vue de la prise en charge des patients atteints ou susceptibles d'être atteints par le virus SARS-CoV-2 dont l'état clinique le justifie. Il a notamment prévu que le médecin qui prescrit cette spécialité dans ce cadre se conforme aux protocoles exceptionnels et transitoires relatifs, d'une part, à la prise en charge de la dyspnée et, d'autre part, à la prise en charge palliative de la détresse respiratoire, établis par la société française d'accompagnement et de soins palliatifs. Ces mesures ont été reprises à l'identique par le II de l'article 20 du décret n° 2020-548 du 11 mai 2020, puis par le II de l'article 51 du décret du 31 mai 2020 visé ci-dessus.<br/>
<br/>
              Sur la demande en référé :<br/>
<br/>
              6. Les requérants demandent au juge des référés du Conseil d'Etat, sur le fondement de l'article L. 521-1 du code de justice administrative, d'ordonner la suspension de l'exécution des dispositions du II de l'article 20 du décret du 11 mai 2020. Ces dispositions ayant été, comme il a été dit, reprises à l'identique au II de l'article 51 du décret du 31 mai 2020, abrogeant le décret du 11 mai 2020, avant l'introduction de la requête, les requérants doivent être regardés comme demandant la suspension de l'exécution tant des dispositions du II de l'article 20 du décret du 11 mai 2020 que des dispositions du II de l'article 51 du décret du 31 mai 2020, seules ces dernières conclusions, dirigées contre les dispositions en vigueur, ayant cependant un objet.<br/>
<br/>
              7. En premier lieu, ainsi qu'il a été dit au point 5, les dispositions attaquées ont pour objet de permettre, dans des conditions qu'elles définissent, la prescription du clonazépam injectable en vue de la prise en charge des patients atteints ou susceptibles d'être atteints par le virus SARS-CoV-2 dont l'état clinique le justifie. Une telle mesure, sécurisant la prescription d'une spécialité dans une indication non conforme à son autorisation de mise sur le marché, est en principe régie par les dispositions de l'article L. 5121-12-1 du code de la santé publique. Dès lors toutefois qu'elle se rapporte en l'espèce, contrairement à ce que soutiennent les requérants, à la mise à la disposition des patients de médicaments appropriés pour l'éradication de la catastrophe sanitaire, elle est au nombre de celles que le Premier ministre est compétent pour prendre en vertu du 9° de l'article L. 3131-15 du code de la santé publique pendant la durée d'application de cet article.<br/>
<br/>
              8. En deuxième lieu, les dispositions attaquées imposent à tout médecin, lorsqu'il prescrit le clonazépam injectable en vue de la prise en charge des patients atteints ou susceptibles d'être atteints par le virus SARS-CoV-2 dont l'état clinique le justifie, de se conformer aux protocoles exceptionnels et transitoires relatifs à la prise en charge de la dyspnée et à la prise en charge palliative de la détresse respiratoire établis par la société française d'accompagnement et de soins palliatifs. Il résulte de ces protocoles qu'ils sont destinés à assurer l'apaisement de la souffrance, y compris en dehors d'une hospitalisation dans un service de réanimation et en l'absence d'accès au midazolam, benzodiazépine normalement préconisée, chez des patients, soit pour lesquels une décision collégiale de limitation de traitements actifs a été prise, soit qui, présentant une forme grave de la maladie, se trouvent en situation de détresse respiratoire asphyxique. Les posologies que ces protocoles précisent, conformes à celles recommandées par la Haute autorité de santé, sont celles, à ajuster par chaque médecin en fonction de la situation de son patient, adaptées pour ces malades en vue du seul soulagement de leur souffrance, s'agissant d'une spécialité contre-indiquée en cas d'insuffisance respiratoire grave lorsqu'elle est utilisée dans l'indication de son autorisation de mise sur le marché. Les dispositions attaquées sont ainsi, contrairement à ce que soutiennent les requérants, de nature à assurer à toute personne, alors même qu'elle ne serait pas hospitalisée, le respect de son droit à recevoir, sous réserve de son consentement libre et éclairé, les traitements et les soins appropriés à son état de santé, tels qu'appréciés par le médecin, notamment s'agissant de l'apaisement de la souffrance créée par une situation de dyspnée ou de détresse respiratoire ainsi, en outre, que du droit reconnu par l'article L. 1110-9 du code de la santé publique à toute personne malade dont l'état le requiert d'accéder à des soins palliatifs et à un accompagnement. Elles n'ont ni pour objet ni pour effet de déroger aux articles L. 1110-5 et suivants du code de la santé publique, issus de la loi du 2 février 2016 créant de nouveaux droits en faveur des malades et des personnes en fin de vie, qui prévoient, notamment, une procédure collégiale lors d'un arrêt des traitements et de la mise en oeuvre d'une sédation profonde et continue maintenue jusqu'au décès, ou aux articles L. 1111-12, issu de la même loi, R. 4127-37-2 et R. 4127-37-3 de ce code, issus du décret du 3 août 2016 visé ci-dessus, pris pour son application, faisant obligation au médecin de s'enquérir, avant la mise en oeuvre d'une telle sédation, de la volonté du patient hors d'état de l'exprimer, dont elles ne dispensent pas le cas échéant du respect. <br/>
<br/>
              9. Il résulte de tout ce qui précède que les moyens soulevés par les requérants, tirés de ce que le Premier ministre aurait, par les dispositions attaquées, méconnu l'étendue de la compétence que lui attribue le 9° de l'article L. 3131-15 du code de la santé publique, illégalement permis la prescription de cette spécialité dans une indication non conforme à son autorisation de mise sur le marché et pris une mesure entachée d'erreur manifeste d'appréciation, destinée à provoquer délibérément la mort, discriminatoire et portant atteinte à l'égal accès aux soins et au droit reconnu à chacun de bénéficier des soins les plus appropriés ainsi qu'aux exigences de recueil du consentement du patient et de collégialité attachées aux décisions relatives aux malades en fin de vie, ne sont manifestement pas propres à créer un doute sérieux quant à la légalité de ces dispositions. Par suite, sans qu'il soit besoin de se prononcer sur la recevabilité de la requête ni sur la condition d'urgence, leur requête doit être rejetée par application des dispositions de l'article L. 522-3 du code de justice administrative, y compris les conclusions qu'ils présentent au titre de l'article L. 761-1 de ce code.<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er: La requête de M. D... et autre est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. A... D..., premier requérant dénommé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
