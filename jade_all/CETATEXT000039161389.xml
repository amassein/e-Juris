<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039161389</ID>
<ANCIEN_ID>JG_L_2019_09_000000421090</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/16/13/CETATEXT000039161389.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 30/09/2019, 421090, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-09-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421090</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>M. Nicolas Agnoux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:421090.20190930</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire enregistrés les 30 mai et 30 août 2018 au secrétariat du contentieux du Conseil d'Etat, la société Campus Ile-de-France demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler les décisions du 30 mars 2018 par lesquelles le ministre d'Etat, ministre de la transition écologique et solidaire a, en premier lieu, annulé ses certificats d'économies d'énergie pour un montant de 3 183 752 kilowattheures cumulés actualisés (kWh cumac), en deuxième lieu, annulé ses certificats d'économies d'énergie au bénéfice des ménages en situation de précarité énergétique pour un montant de 34 286 560 kWh cumac, en troisième lieu, l'a privée de la possibilité d'obtenir des certificats d'énergie selon les modalités prévues au premier alinéa de l'article L. 221-7 et à l'article L. 221-12 du code de l'énergie pendant un délai de neuf mois et, en quatrième lieu, rejeté ses demandes de certificats d'économies d'énergie en cours d'instruction ;<br/>
<br/>
              2°) à titre subsidiaire, de réduire les sanctions prononcées par ces décisions ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'énergie ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Nicolas Agnoux, maître des requêtes, <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Soltner, Texidor, Perier, avocat de la Société Campus Idf ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il résulte de l'instruction que la société Campus Ile-de-France, qui commercialise des carburants automobiles, du fioul et des chaudières, est soumise à des obligations d'économie d'énergie en application de l'article L. 221-1 du code de l'énergie, dont elle s'acquitte à travers la réalisation d'opérations donnant lieu à la délivrance de certificats d'économies d'énergie (CEE). A cette fin, elle a conclu une convention de partenariat avec la société Fouillouze, spécialisée dans les travaux d'isolation et de couverture, par laquelle elle s'engage à inciter des clients à faire réaliser des travaux générateurs d'économie d'énergie par cette entreprise, à laquelle elle attribue une contribution financière dont une part est reversée au bénéficiaire de l'opération, en contrepartie de la remise des dossiers d'installation lui permettant d'obtenir des CEE. Par des décisions du 30 septembre 2016, la société Campus Ile-de-France s'est vu délivrer des CEE pour un montant de 3 183 752 kilowattheures cumulés actualisés (kWh cumac) et des CEE au bénéfice des ménages en situation de précarité énergétique pour un montant de 34 286 560 kWh cumac, au titre de huit opérations d'isolement de planchers, de combles et de toitures réalisées par la société Fouillouze sur des bâtiments situés dans la commune de La Verrière (Yvelines). Toutefois, à l'issue d'un contrôle engagé par l'administration le 21 octobre 2016, le ministre d'Etat, ministre de la transition écologique et solidaire a constaté plusieurs manquements au titre de ces opérations et, par trois décisions du 30 mars 2018, a annulé l'ensemble des certificats d'économies d'énergie délivrés à la société Campus Ile-de-France, l'a privée de la possibilité d'obtenir de nouveaux certificats selon les modalités prévues au premier alinéa de l'article L. 221-7 et à l'article L. 221-12 du code de l'énergie pendant un délai de neuf mois et a rejeté l'ensemble de ses demandes de certificats d'économies d'énergie en cours d'instruction. <br/>
<br/>
              2. D'une part, aux termes des premier, deuxième et sixième alinéas de l'article R. 221-22 du code de l'énergie dans sa rédaction applicable au litige : " La demande de certificats d'économies d'énergie est adressée au ministre chargé de l'énergie. / Un arrêté du ministre chargé de l'énergie définit la liste des pièces du dossier accompagnant le dossier de demande, ainsi que la liste des pièces qui doivent être archivées par le demandeur pour être tenues à la disposition des agents chargés des contrôles dès le dépôt de la demande de certificats d'économies d'énergie. / (...) Le demandeur de certificats d'économies d'énergie doit, à l'appui de sa demande, justifier de son rôle actif et incitatif dans la réalisation de l'opération. Est considérée comme un rôle actif et incitatif toute contribution directe, quelle qu'en soit la nature, apportée, par le demandeur ou par l'intermédiaire d'une personne qui lui est liée contractuellement, à la personne bénéficiant de l'opération d'économies d'énergie et permettant la réalisation de cette dernière. Cette contribution doit être intervenue antérieurement au déclenchement de l'opération ".<br/>
<br/>
              3. Aux termes du point 3 de l'annexe 5 jointe à l'arrêté du 4 septembre 2014 fixant la liste des éléments d'une demande de certificats d'économies d'énergie et les documents à archiver par le demandeur, pris pour l'application des dispositions réglementaires citées au point 2, la preuve du rôle actif, incitatif et antérieur à l'engagement de l'opération est apportée, en cas de contractualisation de l'opération d'économies d'énergie entre le bénéficiaire et un partenaire du demandeur, par " la mention dactylographiée, dans le corps du contrat, de la nature précise de cette contribution, de l'identification du demandeur via sa raison sociale et du fait que le demandeur est à l'origine de la contribution dans le cadre du dispositif des certificats d'économies d'énergie. Le contrat de réalisation de l'opération est accepté et signé par le bénéficiaire au plus tard à la date d'engagement de l'opération. Il est daté par le bénéficiaire du jour de son acceptation. / Par dérogation : / (...) - la mention peut être ajoutée sur le contrat dactylographié ou manuscrit par le biais d'un tampon ou d'un autocollant. La mention ajoutée sur le contrat est datée et signée par le bénéficiaire des travaux à côté de cette mention : un contrat comportant ce type de mention comporte donc deux signatures du bénéficiaire et deux indications de la date de signature du contrat par le bénéficiaire (...) ". Aux termes du point 5 de la même annexe : " Pour chaque opération standardisée d'économies d'énergie réalisée, l'attestation sur l'honneur, telle que définie à l'annexe 7, complétée par le bénéficiaire et par le professionnel ayant mis en oeuvre ou assuré la maîtrise d'oeuvre de l'opération fait partie des pièces justificatives ". Selon l'annexe 7-1 qui détermine le contenu de l'attestation sur l'honneur, le bénéficiaire de l'opération atteste, notamment, que le demandeur lui a " apporté une contribution individualisée (action personnalisée de sensibilisation ou d'accompagnement, aide financière ou équivalent). Cette contribution [l'] a incité à réaliser cette opération d'économies d'énergie ".<br/>
<br/>
              4. D'autre part, aux termes de l'article L. 222-1 du code de l'énergie : " Dans les conditions définies aux articles suivants, le ministre chargé de l'énergie peut sanctionner les manquements qu'il constate, de la part des personnes mentionnées à l'article L. 221-1, aux dispositions des articles L. 221-1 à L. 221-5 ou aux dispositions réglementaires prises pour leur application. ". Aux termes de l'article L. 222-2 du même code : " Le ministre met l'intéressé en demeure de se conformer à ses obligations dans un délai déterminé. Il peut rendre publique cette mise en demeure. / Lorsque l'intéressé ne se conforme pas dans les délais fixés à cette mise en demeure, le ministre chargé de l'énergie peut : / 1° Prononcer à son encontre une sanction pécuniaire dont le montant est proportionné à la gravité du manquement et à la situation de l'intéressé, sans pouvoir excéder le double de la pénalité prévue au premier alinéa de l'article L. 221-4 par kilowattheure d'énergie finale concerné par le manquement et sans pouvoir excéder 2 % du chiffre d'affaires hors taxes du dernier exercice clos, porté à 4 % en cas de nouveau manquement à la même obligation ; / 2° Le priver de la possibilité d'obtenir des certificats d'économies d'énergie selon les modalités prévues au premier alinéa de l'article L. 221-7 et à l'article L. 221-12 ; / 3° Annuler des certificats d'économies d'énergie de l'intéressé, d'un volume égal à celui concerné par le manquement ; / 4° Suspendre ou rejeter les demandes de certificats d'économies d'énergie faites par l'intéressé (...) ". Aux termes de l'article L. 222-6 du même code : " Les décisions sont motivées, notifiées à l'intéressé et publiées au Journal officiel ". Enfin, aux termes de l'article R. 222-12 du même code : " Les décisions du ministre chargé de l'énergie prononçant les sanctions prévues à l'article L. 222-2 peuvent faire l'objet d'un recours de pleine juridiction (...) devant le Conseil d'Etat (...) ".<br/>
<br/>
              Sur la régularité des décisions attaquées :<br/>
<br/>
              5. Les décisions attaquées sont suffisamment circonstanciées en fait comme en droit. Il s'ensuit que le moyen tiré de leur insuffisance de motivation ne peut qu'être écarté.<br/>
<br/>
              Sur le bien-fondé des décisions attaquées :<br/>
<br/>
              En ce qui concerne les manquements reprochés :<br/>
<br/>
              6. En premier lieu, il résulte de l'instruction et il n'est pas contesté que, sur l'ensemble des devis établis par la société Fouillouze à l'attention des bénéficiaires des opérations litigieuses, la mention dactylographiée de la nature précise de la contribution apportée par la société Campus Ile-de-France ne figurait pas dans le corps du devis, mais sur une page indépendante, non numérotée et ne comportant pas d'indication permettant de la relier au devis. Par suite, la société n'a pas produit les pièces requises en application de l'arrêté du 4 septembre 2014 cité au point 3 ci-dessus. Avisée de ce manquement, la société n'a été en mesure de produire, ni devant l'administration ni dans la présente instance, aucune pièce de nature à justifier de son rôle actif, incitatif et antérieur à l'engagement de l'opération, conformément au sixième alinéa de l'article R. 222-12 du code de l'énergie cité au point 2. La seule production des attestations sur l'honneur définies aux annexes 7 et 7-1 de l'arrêté du 4 septembre 2014 ne pouvait, dans les circonstances de l'espèce, suppléer à l'absence des pièces justificatives requises, dès lors qu'elles utilisent une formule standardisée dépourvue de précision sur la nature de la contribution du demandeur et qu'elles ont été établies postérieurement à la réalisation de l'opération. Par suite, la société requérante n'est pas fondée à soutenir que le ministre aurait commis une erreur de droit en retenant qu'elle ne satisfaisait pas à la condition de fond nécessaire à l'octroi des CEE et liée à son rôle actif, incitatif et antérieur aux opérations en litige.<br/>
<br/>
              7. En deuxième lieu, la société Campus Ile-de-France fait grief au ministre d'avoir également motivé sa décision par le fait que les informations permettant d'identifier le demandeur n'étaient pas partie intégrante du devis dactylographié mais auraient été apposées par un tampon, alors que le point 3.1 de l'annexe 5 à l'arrêté du 4 septembre 2014 cité au point 3 ci-dessus autorise ce procédé, à condition que soient ajoutées, à côté de la mention apposée par le tampon, la date et la signature du bénéficiaire des travaux. Toutefois, la société requérante ne justifie pas remplir cette dernière condition. Par suite, elle n'est pas fondée à soutenir qu'elle aurait respecté les obligations prescrites par ces dispositions ni que les sanctions prononcées à son encontre seraient, pour ce seul motif, dépourvues de fondement.<br/>
<br/>
              8. En troisième lieu, la société soutient que le ministre ne pouvait se fonder, pour prononcer les sanctions litigieuses, sur le fait qu'elle aurait surévalué la surface des planchers et des toitures ayant fait l'objet des travaux d'isolation litigieux. Toutefois, le ministre a établi, par comparaison avec l'emprise au sol des bâtiments, enregistrée au cadastre, l'existence d'une surévaluation de la surface des travaux d'isolation pour chacune des huit opérations en cause, à hauteur de 19 à 26 % selon le cas. Si la société Campus Ile-de-France fait valoir, pour la première fois devant le juge, que la société Fouillouze a admis, par un courrier du 30 janvier 2017, que certaines différences de métrage constatées résulteraient d'une erreur commise par son propre personnel, cette circonstance, qui n'affecte du reste que les seules opérations d'isolation de plancher, ne peut être de nature à atténuer le manquement de la société requérante aux obligations, qui n'incombent qu'à elle seule, prescrites pour la délivrance des CEE obtenus au titre d'opérations réalisées par l'intermédiaire d'une entreprise. Par suite, la société requérante n'est pas fondée à soutenir que le ministre aurait méconnu le principe de responsabilité personnelle ni commis à ce titre une erreur de droit. <br/>
<br/>
              En ce qui concerne les sanctions prononcées :<br/>
<br/>
              9. Il résulte de ce qui précède que la société requérante n'est fondée à soutenir, ni que les manquements constatés concernant les pièces justificatives requises ne présenteraient pas un caractère de gravité, faute pour elle de produire d'autre élément établissant son rôle actif, incitatif et antérieur à l'engagement des opérations en litige, ni que le manquement lié à la surévaluation des surfaces de travaux ne lui serait pas imputable. En outre, elle ne conteste pas utilement les sanctions prononcées, en faisant valoir que les bâtiments en cause n'étaient pas référencés sur le site Géoportail du ministère de l'intérieur alors qu'il lui appartenait de veiller à l'exactitude des déclarations qu'elle a produites.<br/>
<br/>
              10. Le moyen tiré de ce que le 1° de l'article L. 222-2 du code de l'énergie soumet la sanction pécuniaire prévue à cet alinéa à un plafond égal à 2 % du chiffre d'affaires hors taxes du dernier exercice clos, qui du reste est porté à 4 % en cas de nouveau manquement à la même obligation, doit être écarté comme inopérant, dès lors que la requérante n'a pas fait l'objet de cette sanction. <br/>
<br/>
              11. Eu égard, d'une part, au cumul des manquements constatés pour chacune des opérations litigieuses, qui portent à la fois sur l'absence de justification du rôle actif et incitatif du demandeur, tant en ce qui concerne les devis que la convention de partenariat conclue le 16 octobre 2015, soit seulement un mois avant l'engagement des travaux litigieux, et sur l'inexactitude des déclarations concernant les travaux réalisés et, d'autre part, à la répétition de manquements similaires à ceux qui avaient donné lieu à une première sanction le 5 août 2016, il ne résulte pas de l'instruction que les sanctions prononcées seraient disproportionnées au regard de la gravité et de la nature des manquements commis. <br/>
<br/>
              12. Enfin, si la société soutient que les sanctions prononcées auraient pour conséquence une perte de 3,3 millions d'euros, résultant de la somme des dépenses déjà exposées pour l'acquisition des CEE annulés et de leur valeur d'achat sur le marché correspondant, cette somme inclut le coût lié aux obligations légales qui lui incombent en application de l'article L. 221-1 du code de l'énergie. En outre, la requérante ne peut valablement rapprocher ce montant, même ainsi corrigé, de son chiffre d'affaires annuel, dès lors qu'il résulte de ses propres écritures qu'il correspond à l'acquisition d'un volume de CEE trois fois supérieur à celui qui résulte pour chaque année de son obligation légale. Par suite, la requérante, qui n'apporte aucun élément probant au soutien de ses allégations relatives aux conséquences des sanctions qui lui ont été infligées, n'est pas fondée à soutenir qu'elles seraient disproportionnées au regard de sa situation financière.<br/>
<br/>
              13. Il résulte de tout ce qui précède que la société Campus Ile-de-France n'est pas fondée à demander l'annulation des décisions qu'elle attaque, ni à obtenir la réformation des sanctions prononcées à son encontre. Sa requête doit, par suite, être rejetée, y compris les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la société Campus Ile-de-France est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la société Campus Ile-de-France et à la ministre de la transition écologique et solidaire.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
