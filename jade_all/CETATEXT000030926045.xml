<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030926045</ID>
<ANCIEN_ID>JG_L_2015_07_000000362293</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/92/60/CETATEXT000030926045.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème SSJS, 22/07/2015, 362293, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>362293</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>M. Patrick Quinqueton</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:362293.20150722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU> Vu la procédure suivante :<br/>
<br/>
              Le préfet de l'Hérault a déféré au tribunal administratif de Montpellier M. A... B... comme prévenu d'une contravention de grande voirie prévue et réprimée par les articles L. 2132-3 et L. 2132-6 du code général de la propriété des personnes publiques, sur le fondement d'un procès-verbal du 1er octobre 2008 constatant la présence d'enrochements sur le domaine public maritime au droit de la parcelle cadastrée AC 327 sur le territoire de la commune de Vias.<br/>
<br/>
              Par un jugement n° 0805957 du 24 septembre 2010, le tribunal administratif de Montpellier a condamné M. B...à payer une amende de 1 000 euros, à retirer les enrochements litigieux et à remettre en état les dépendances du domaine public maritime dont l'occupation irrégulière a été constatée par le procès-verbal de contravention de grande voirie du 1er octobre 2008, dans un délai de trois mois à compter de la notification du jugement, sous astreinte de 100 euros par jour de retard.<br/>
<br/>
              Par un arrêt n° 10MA04204 du 26 juin 2012, la cour administrative d'appel de Marseille a rejeté l'appel de M. B...tendant à l'annulation de ce jugement et au rejet de la demande présentée par le préfet de l'Hérault.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 28 août 2012, 26 mars 2013 et 24 octobre 2014 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement d'une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la décision n° 2013-316 QPC du 24 mai 2013 du Conseil constitutionnel ;<br/>
              - le code général de la propriété des personnes publiques ;<br/>
              - la loi du 16 septembre 1807 relative au dessèchement des marais, notamment son article 33 <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Patrick Quinqueton, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'un procès-verbal de contravention de grande voirie faisant état de la présence d'enrochements sur le domaine public maritime au droit de la parcelle cadastrée AC 327 appartenant à M. A...B..., située sur la commune de Vias (Hérault), a été dressé le 1er octobre 2008 à l'encontre de M. B...; que celui-ci se pourvoit en cassation contre l'arrêt du 26 juin 2012 par lequel la cour administrative d'appel de Marseille a rejeté sa requête tendant à l'annulation du jugement du 24 septembre 2010 du tribunal administratif de Montpellier l'ayant condamné à payer une amende de 1 000 euros, à retirer les enrochements litigieux et à remettre en état les dépendances du domaine public, sous astreinte ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 2111-4 du code général de la propriété des personnes publiques : "  Le domaine public maritime naturel de l'Etat comprend : / 1° Le sol et le sous-sol de la mer entre la limite extérieure de la mer territoriale et, côté terre, le rivage de la mer. / Le rivage de la mer est constitué par tout ce qu'elle couvre et découvre jusqu'où les plus hautes mers peuvent s'étendre en l'absence de perturbations météorologiques exceptionnelles (...) " ; qu'aux termes de l'article L. 2132-3 de ce même code : " Nul ne peut bâtir sur le domaine public maritime ou y réaliser quelque aménagement ou quelque ouvrage que ce soit sous peine de leur démolition, de confiscation des matériaux et d'amende. / Nul ne peut en outre, sur ce domaine, procéder à des dépôts ou à des extractions, ni se livrer à des dégradations " ; qu'aux termes de l'article L. 2132-26 du même code : " Sous réserve des textes spéciaux édictant des amendes d'un montant plus élevé, l'amende prononcée pour les contraventions de grande voirie ne peut excéder le montant prévu par le 5° de l'article 131-13 du code pénal " ; que ce montant est fixé par cet article à " 1 500 euros au plus pour les contraventions de la 5e classe (...) " ; que, par une décision n° 2013-316 QPC du 24 mai 2013, le Conseil constitutionnel a déclaré le 1° de l'article L. 2111-4 du code général de la propriété des personnes publiques conforme à la Constitution sous réserve que le propriétaire ayant, sur le fondement de l'article 33 de la loi du 16 septembre 1807, élevé une digue à la mer sur sa propriété privée ne soit pas forcé de la détruire à ses frais lorsque cet ouvrage se trouve par la suite incorporé au domaine public maritime naturel en raison de la progression du rivage de la mer ; <br/>
<br/>
              3. Considérant que M. B...n'a pas contesté devant les juges d'appel l'affirmation de l'administration, résultant notamment de son mémoire en défense en date du 19 mai 2011, selon laquelle les enrochements litigieux avaient été édifiés à une date à laquelle la parcelle sur laquelle ces enrochements ont été construits appartenait déjà au domaine public maritime, ainsi que cela ressortait notamment d'un courrier adressé à M. B...par la commune de Vias en mars 2008 ; qu'il suit de là, d'une part,  en tout état de cause, que la cour n'a pas commis d'erreur de droit en rejetant ses conclusions dirigées contre l'amende qui lui a été infligée sans rechercher si, à la date de construction de l'ouvrage litigieux, la parcelle sur laquelle il avait été réalisé appartenait au domaine public maritime ; d'autre part, qu'elle n'a pas davantage commis d'erreur de droit en rejetant ses conclusions dirigées contre l'obligation qui lui a été faite de procéder à la remise en état des lieux, alors même que, par la décision n° 2013-316 QPC du 24 mai 2013, le Conseil constitutionnel a jugé que la destruction d'un ouvrage régulièrement construit sur une propriété privée ne peut être imposée à un propriétaire à ses frais au seul motif que la progression de la mer a eu pour effet d'incorporer cet ouvrage au domaine public maritime ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que M. B...n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque ; qu'il y a lieu, par voie de conséquence, de rejeter ses conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>                      D E C I D E :<br/>
                                     --------------<br/>
<br/>
Article 1er : Le pourvoi de M. B... est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. A... B...et à la ministre de l'écologie, du développement durable et de l'énergie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
