<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029986067</ID>
<ANCIEN_ID>JG_L_2014_12_000000374537</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/98/60/CETATEXT000029986067.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 29/12/2014, 374537, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-12-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>374537</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Sophie-Caroline de Margerie</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:374537.20141229</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 10 janvier 2014 au secrétariat du contentieux du Conseil d'Etat, présentée par l'association Agir pour les paysages, dont le siège est 22, rue Meyrueis à Montpellier (34000), représentée par son président en exercice ; l'association Agir pour les paysages demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir, d'une part, l'article 16 du décret n° 2013-606 du 9 juillet 2013 portant diverses modifications du code de l'environnement relatives à la publicité extérieure, aux enseignes et aux préenseignes, d'autre part, la décision implicite du Premier ministre rejetant son recours gracieux du 9 septembre 2013 ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement d'une somme de 2 000 euros sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu le code de l'environnement ;<br/>
<br/>
              Vu le code de la route ;<br/>
<br/>
              Vu la loi n° 2010-788 du 12 juillet 2010 ;<br/>
<br/>
              Vu la loi n° 2012-387 du 22 mars 2012 ;<br/>
<br/>
               Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie-Caroline de Margerie, conseiller d'Etat,<br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant que l'article L. 581-43 du code de l'environnement, dans sa rédaction issue de la loi du 22 mars 2012 relative à la simplification du droit et à l'allègement des démarches administratives, dispose que : " Les publicités, enseignes et préenseignes, qui ont été mises en place avant l'entrée en vigueur des actes pris pour l'application des articles L. 581-4, avant-dernier alinéa, L. 581-7, L. 581-8, L. 581-14 et L. 581-18, deuxième et troisième alinéas et qui ne sont pas conformes à leurs prescriptions, ainsi que celles mises en place dans des lieux entrés dans le champ d'application des articles L. 581-4, L. 581-8 et L. 581-44 en vertu d'actes postérieurs à leur installation, peuvent, sous réserve de ne pas contrevenir à la réglementation antérieure, être maintenues pendant un délai maximal de six ans à compter de l'entrée en vigueur des actes précités. / Les publicités, enseignes et préenseignes soumises à autorisation en vertu du présent chapitre qui ne sont pas conformes à des règlements visés à l'alinéa précédent et entrés en vigueur après leur installation peuvent être maintenues, sous réserve de ne pas contrevenir à la réglementation antérieure, pendant un délai maximal de six ans à compter de l'entrée en vigueur de ces règlements. / Les publicités, enseignes et préenseignes qui ont été mises en place avant l'entrée en vigueur de la loi n° 2010-788 du 12 juillet 2010 portant engagement national pour l'environnement et des décrets en Conseil d'Etat pris pour l'application de l'article 36 de cette loi peuvent, sous réserve de ne pas contrevenir aux dispositions antérieurement applicables, être maintenues pendant un délai maximal de six ans à compter de l'entrée en vigueur de la loi et des décrets en Conseil d'Etat précités./Pour les publicités et préenseignes, un décret peut prévoir un délai moindre, qui ne peut être inférieur à deux ans à compter de sa publication " ; <br/>
<br/>
              2.	Considérant que sur le fondement du troisième alinéa de ces dispositions, l'article 16 du décret attaqué a modifié l'article R. 581-88 du code de l'environnement afin, notamment, de réduire à deux ans le délai de mise en conformité des publicités et préenseignes ; qu'aux termes de l'article R. 581-88 issu du décret attaqué : " I.-Les publicités et préenseignes mises en place avant l'entrée en vigueur d'un règlement local de publicité prévu à l'article L. 581-14 qui ne sont pas conformes aux prescriptions de ce règlement peuvent, y compris si elles sont soumises à autorisation, être maintenues jusqu'au 13 juillet 2015 lorsque l'entrée en vigueur de ce règlement est antérieure à la date de publication du décret n° 2013-606 du 9 juillet 2013 portant diverses modifications des dispositions du code de l'environnement relatives à la publicité, aux enseignes et préenseignes. Lorsque l'entrée en vigueur du règlement est postérieure à la date de publication du décret précité, elles peuvent être maintenues pendant deux ans à compter de la date d'entrée en vigueur dudit règlement. / II.-Les publicités et préenseignes mises en place avant l'entrée en vigueur d'un acte, qui, procédant au classement d'un monument, d'un site ou d'un espace mentionné par le I de l'article L. 581-4, fixant les limites d'une agglomération en application de l'article R. 411-2 du code de la route ou délimitant l'un des espaces énumérés par l'article L. 581-8, a pour effet d'interdire la publicité dans le lieu où elles sont installées, peuvent être maintenues jusqu'au 13 juillet 2015 lorsque l'entrée en vigueur de cet acte est antérieure à la date de publication du décret n° 2013-606 du 9 juillet 2013 portant diverses modifications des dispositions du code de l'environnement relatives à la publicité, aux enseignes et préenseignes. Lorsque l'entrée en vigueur de l'acte est postérieure à la date de publication du décret précité, elles peuvent être maintenues pendant deux ans à compter de la date d'entrée en vigueur dudit acte. / III.-Les publicités et préenseignes mises en place avant le 1er juillet 2012 qui ne sont pas conformes aux dispositions du chapitre Ier du titre VIII du livre V du code de l'environnement issues de la loi n° 2010-788 du 12 juillet 2010 et du décret n° 2012 112 du 30 janvier 2012 peuvent être maintenues jusqu'au 13 juillet 2015 " ;<br/>
<br/>
              3.	Considérant qu'il ressort de l'article L. 581-43 du code de l'environnement dont les dispositions ont été rappelées au point 1. ci-dessus que l'application à certains dispositifs publicitaires de délais de mise en conformité est subordonnée à la condition que ces dispositifs ne contreviennent pas à la réglementation antérieure ; que les dispositions de l'article R. 581-88, qui doivent être lues à  la lumière de celles de l'article L. 581-43 dont elles font application, n'ont pas eu pour objet et n'auraient pu légalement avoir pour effet de supprimer cette condition ;<br/>
<br/>
              4.	Considérant que l'article L. 581-43 du code de l'environnement énumère les actes qui ont pour effet d'interdire la publicité et dont l'entrée en vigueur entraîne une obligation de mise en conformité des dispositifs publicitaires antérieurement mis en place ; que figurent dans cette liste les actes pris pour l'application de l'article L. 581-7 du même code ; que ce dernier article prévoit que : " En dehors des lieux qualifiés d'agglomération par les règlements relatifs à la circulation routière, toute publicité est interdite " ; que les arrêtés municipaux fixant les limites des agglomérations en vertu de l'article R. 411-2 du code de la route sont au nombre de ces règlements ; qu'ainsi, le pouvoir réglementaire pouvait prévoir que les dispositifs publicitaires installés dans un lieu où la publicité est devenue interdite du fait de l'entrée en vigueur d'un arrêté fixant les limites d'une agglomération peuvent être maintenus pendant le délai qu'il fixe ;<br/>
<br/>
              5.	Considérant que figurent également dans la liste de l'article L. 581-43 du code de l'environnement les actes pris pour l'application de l'article L. 581-4 du même code, qui interdit la publicité sur les immeubles classés parmi les monuments historiques ou inscrits à l'inventaire supplémentaire, sur les monuments naturels et dans les sites classés, dans les coeurs des parcs nationaux et les réserves naturelles, et sur les arbres, et autorise le maire ou le préfet à interdire la publicité sur des immeubles présentant un caractère esthétique, historique ou pittoresque ; que l'article R. 581-88 se borne à mentionner les actes procédant au classement d'un monument, d'un site ou d'un espace, sans citer les actes procédant à l'inscription d'un immeuble à l'inventaire supplémentaire ni les arrêtés municipaux interdisant la publicité sur les immeubles présentant un caractère esthétique, historique ou pittoresque, actes pourtant  mentionnés à l'article L. 581-4 ; que, toutefois, le pouvoir réglementaire pouvait ne pas  mentionner ces actes dans le décret attaqué, dès lors que le législateur ne lui avait pas imposé de réduire le délai de mise en conformité des publicités et préenseignes à compter de l'entrée en vigueur de tous les actes dont la liste était dressée dans l'article L. 581-43 du code de l'environnement, mais s'était contenté de lui ouvrir une telle faculté ;<br/>
<br/>
              6.	Considérant qu'il résulte de tout ce qui précède que l'association requérante n'est pas fondée à demander l'annulation pour excès de pouvoir des dispositions du décret qu'elle attaque ; que ses conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative ne peuvent, en conséquence, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de l'association Agir pour les paysages est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée  à l'association Agir pour les paysages, à la ministre de l'écologie, du développement durable et de l'énergie et au Premier ministre.<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
