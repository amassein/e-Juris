<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029781251</ID>
<ANCIEN_ID>JG_L_2014_11_000000382640</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/78/12/CETATEXT000029781251.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  8ème sous-section jugeant seule, 19/11/2014, 382640, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-11-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>382640</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 8ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Patrick Quinqueton</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:382640.20141119</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              		Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique enregistrés les 15 juillet et 15 octobre 2014, M. A...B...demande au Conseil d'Etat d'annuler pour excès de pouvoir le décret n° 2014-245 du 25 février 2014 portant délimitation des cantons dans le département de la Lozère et la décision du 15 mai 2014 par laquelle le ministre de l'intérieur a rejeté le recours gracieux dirigé contre ce décret ; <br/>
<br/>
              il soutient que : <br/>
<br/>
              - le décret attaqué ignore la frontière naturelle que constitue le Mont-Lozère, alors que d'autres choix auraient été possibles sans méconnaître le critère géographique ;  <br/>
              - la délimitation des cantons est arbitraire et entachée d'une erreur manifeste d'appréciation dès lors qu'elle ne tient pas compte des exigences relatives à la superficie des cantons, aux territoires ruraux et à l'existence de massifs montagneux, aboutissant ainsi à la constitution de cantons d'une surface disproportionnée et comportant de très nombreuses communes, comme celui de Saint-Etienne-du-Valdonnez, et à une sous représentation des communes rurales ;<br/>
              - la nouvelle délimitation cantonale a été faite dans un but de rééquilibrage politique comme le montrent la délimitation du canton de Saint-Etienne-du-Valdonnez ainsi que le fait que le canton de Grandrieu aurait dû être rattaché à celui de Langogne, le canton de Villefort à celui du Bleymard et de Chateauneuf-de-Randon, celui de Saint-Etienne-du-Valdonnez à celui du Pont-de-Monvert et à une partie de celui de Florac, ce qui aurait respecté les habitudes de vie et de circulation naturelle des habitants ;<br/>
<br/>
              Par un mémoire en défense, enregistré le 3 octobre 2014, le ministre de l'intérieur conclut au rejet de la requête.  <br/>
<br/>
              il soutient que les moyens soulevés par le requérant ne sont pas fondés ou sont inopérants. <br/>
<br/>
              Par un mémoire en réplique, enregistré le 15 octobre 2014, M. B...persiste dans ses conclusions tendant à l'annulation du décret qu'il attaque, par les mêmes moyens.<br/>
<br/>
<br/>
<br/>
              Vu :<br/>
<br/>
              - les autres pièces du dossier ;<br/>
<br/>
              - le code général des collectivités territoriales ; <br/>
<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Patrick Quinqueton, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que l'article L. 3113-2 du code général des collectivités territoriales, dans sa version issue de la loi du 17 mai 2013, prévoit que : " I. - Les modifications des limites territoriales des cantons, les créations et suppressions de cantons et le transfert du siège de leur chef-lieu sont décidés par décret en Conseil d'Etat après consultation du conseil départemental qui se prononce dans un délai de six semaines à compter de sa saisine. A l'expiration de ce délai, son avis est réputé rendu. (...) III. - La modification des limites territoriales des cantons effectuée en application du I est conforme aux règles suivantes :     a) Le territoire de chaque canton est défini sur des bases essentiellement démographiques ; / b) Le territoire de chaque canton est continu ; / c) Est entièrement comprise dans le même canton toute commune de moins de 3 500 habitants ; / IV. - Il n'est apporté aux règles énoncées au III que des exceptions de portée limitée, spécialement justifiées, au cas par cas, par des considérations géographiques ; ou par d'autres impératifs d'intérêt général " ;<br/>
<br/>
              2. Considérant, en premier lieu, qu'il résulte des dispositions du III de l'article L. 3113-2 du code général des collectivités territoriales que le territoire de chaque canton doit être établi sur des bases essentiellement démographiques, qu'il doit être continu et que toute commune de moins de 3 500 habitants doit être entièrement comprise dans le même canton ; que, si le pouvoir réglementaire est en droit, en vertu du IV du même article, d'apporter des exceptions de portée limitée à la règle consistant à définir le territoire des cantons du département sur des bases essentiellement démographiques pour des considérations géographiques ou d'autres impératifs d'intérêt général, ni ces dispositions, pas plus qu'aucun autre texte ni aucun principe ne lui imposent de faire usage de cette faculté, mais se bornent à encadrer son exercice ; <br/>
<br/>
              3. Considérant que l'article 1er du décret attaqué procède à une nouvelle délimitation de l'ensemble des cantons du département de la Lozère, compte tenu de l'exigence de réduction du nombre des cantons de ce département de vingt-cinq à treize résultant de l'application de l'article L. 191-1 du code électoral, en se fondant sur une population moyenne et en rapprochant la population de chaque canton de cette moyenne ;  que la nouvelle délimitation cantonale a permis de réduire l'écart existant de population entre le canton le moins peuplé et le plus peuplé de 1 à 1,43, alors qu'il était de 1 à 9,93 ; <br/>
<br/>
              4. Considérant que ni les dispositions de l'article L. 3113-2 du code général des collectivités territoriales, ni aucun autre texte non plus qu'aucun principe n'imposent au pouvoir réglementaire de prévoir que les limites des cantons, qui sont des circonscriptions électorales, coïncident avec les limites des cartes des établissements publics de coopération intercommunale ou les " bassins de vie " ; que, de même, ni les dispositions de cet article, ni aucun autre texte non plus qu'aucun principe n'imposent au pouvoir réglementaire de retenir comme critères de délimitation de ces circonscriptions électorales les limites des anciens cantons, le nombre ou la proximité géographique des communes qui composent les cantons et l'absence de disparité de superficie entre ceux-ci ; que, par suite, le requérant ne saurait utilement invoquer le fait qu'il n'aurait pas été tenu compte des spécificités géographiques du département, des exigences relatives à la superficie des cantons et aux territoires ruraux et des habitudes de vie et de circulation des habitants ;<br/>
<br/>
              5. Considérant, en deuxième lieu, qu'il ressort des pièces du dossier que le canton de Saint-Etienne-du-Valdonnez a une continuité territoriale et une population supérieure de seulement 14,53% à la moyenne départementale, que sa vaste superficie  s'explique par la faible densité de sa population et par le relief, qu'en regroupant des communes de la haute vallée du Tarn, il a une cohérence géographique et que si les routes qui relient les communes des versants nord et sud du Mont Lozère sont peu praticables en hiver, ce massif peut être contourné en toute saison ; que, dans ces conditions, le requérant ne peut être regardé comme apportant des éléments de nature à établir que le choix auquel il a été procédé serait arbitraire ou reposerait sur une erreur manifeste d'appréciation ; <br/>
<br/>
              6. Considérant qu'il est constant que si les cantons ruraux d'Aumont-Aubrac et de Saint-Etienne-du-Valdonnez ont une population supérieure à la moyenne départementale de respectivement 16,76 % et 14,53 %, plusieurs autres cantons à caractère rural ont une population inférieure à cette moyenne ;<br/>
<br/>
              7. Considérant que la circonstance que d'autres choix auraient été possibles est sans influence sur la légalité du décret attaqué ; <br/>
<br/>
              8. Considérant que le détournement de pouvoir allégué n'est pas établi ; <br/>
<br/>
              9. Considérant qu'il résulte de ce qui précède que la requête de M. B...doit être rejetée ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              		D E C I D E :<br/>
              		--------------<br/>
<br/>
Article  1er : La requête de M. B... est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. A...B...et au ministre de l'intérieur.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
