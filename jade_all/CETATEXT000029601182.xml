<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029601182</ID>
<ANCIEN_ID>JG_L_2014_10_000000368747</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/60/11/CETATEXT000029601182.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème SSJS, 02/10/2014, 368747, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-10-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>368747</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Florence Chaltiel-Terral</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:368747.20141002</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 22 mai 2013 au secrétariat du contentieux du Conseil d'Etat, présentée par la société Atac et la société Attesa, dont le siège est rue du Maréchal de Lattre de Tassigny à Croix (59170), représentées par leur président directeur général en exercice ; la société Atac et la société Attesa demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision n° 994 T, 1008 T du 14 février 2013 par laquelle la commission nationale d'aménagement commercial leur a refusé l'autorisation préalable requise en vue de procéder à la création d'un ensemble commercial d'une surface totale de vente de 4 421 m², comprenant un supermarché " Simply Market " de 2 000 m², une galerie marchande composée de 5 boutiques d'une surface totale de vente de 621 m², et de deux moyennes surfaces spécialisées dans l'équipement de la personne de 950 m² et 850 m², à Vergèze (Gard) ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de commerce ;<br/>
<br/>
              Vu la loi n° 73-1193 du 27 décembre 1973 ;<br/>
<br/>
              Vu la loi n° 2008-776 du 4 août 2008 ;<br/>
<br/>
              Vu le décret n° 2008-1212 du 24 novembre 2008 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Florence Chaltiel-Terral, maître des requêtes en service extraordinaire, <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
<br/>
<br/>Sur la légalité de la décision attaquée :<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier que les avis des ministres intéressés au sens de l'article R. 752-51 du code de commerce ont été présentés à la Commission nationale d'aménagement commercial et signés par les personnes dûment habilitées à le faire ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 752-49 du code de commerce : " (...) Les membres de la Commission nationale d'aménagement commercial reçoivent l'ordre du jour, accompagné des procès-verbaux des réunions des commissions départementales d'aménagement commercial, des décisions de ces commissions, des recours et des rapports des services instructeurs départementaux. (...) " ; qu'il ne résulte pas de ces dispositions que les avis des ministres intéressés figurent au nombre des documents qui doivent être communiqués aux membres avant la réunion de la commission nationale ; qu'au demeurant, il ressort des pièces du dossier que les membres de la commission ont été en mesure de prendre connaissance en temps utile des documents prévus au deuxième alinéa de l'article R. 752-49 précité du code de commerce ;<br/>
<br/>
              3. Considérant qu'il appartient aux commissions d'aménagement commercial, lorsqu'elles se prononcent sur un projet d'exploitation commerciale soumis à autorisation en application de l'article L. 752-1 du code de commerce, d'apprécier la conformité de ce projet aux objectifs prévus à l'article 1er de la loi du 27 décembre 1973 et à l'article L. 750-1 du code de commerce, au vu des critères d'évaluation mentionnés à l'article L. 752-6 du même code ; que l'autorisation ne peut être refusée que si, eu égard à ses effets, le projet compromet la réalisation de ces objectifs ;<br/>
<br/>
              4. Considérant, en premier lieu, qu'il ressort des pièces du dossier, ainsi que les avis défavorables l'ont souligné, que le projet envisagé, situé à 1,8 km du centre-ville de Vergèze et à 700 m du centre-ville de Codognan en rupture avec les urbanisations existantes, augmentera l'étalement urbain de la commune sans contribuer à l'animation de la vie locale ; qu'il entraînera des perturbations importantes sur la fluidité du trafic automobile sur la RN 113 et la RD 139 de nature à provoquer des difficultés de circulation en entrée d'agglomération alors même qu'il n'était pas établi, à la date de la décision attaquée, que la réalisation des aménagements projetés, à savoir un " tourne-à-gauche " sur la RD 139 vers le chemin de la Monnaie, serait suffisamment certaine à la date de l'ouverture de l'ensemble commercial ; <br/>
<br/>
              5. Considérant, en second lieu, qu'il ressort des pièces du dossier, notamment du rapport de la direction départementale des territoires et de la mer du Gard du 18 décembre 2012 que le projet étant situé en zone inondable, il nécessite des aménagements ; que les mesures proposées pour y faire face ne suffisent à prévenir l'exposition de la RN 113 à un risque de submersion ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède, et sans qu'il soit besoin de statuer sur les fins de non-recevoir soulevées par la société Vergezali, que la commission nationale n'a pas fait une inexacte application des dispositions législatives précitées en estimant que le projet méconnaissait les objectifs fixés par le législateur en matière d'aménagement du territoire et de développement durable ; que, dès lors, la société Atac et la société Attesa ne sont pas fondées à demander l'annulation de la décision du 14 février 2013 de la Commission nationale d'aménagement commercial ;<br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              7. Considérant que ces dispositions font obstacle à ce qu'il soit fait droit aux conclusions présentées par les sociétés requérantes à ce titre ; qu'en revanche, il y a lieu, dans les circonstances de l'espèce, de faire application de ces dispositions et de mettre à la charge de la société Atac et de la société Attesa la somme de 1 500 euros à verser, chacune, à la société Vergezali ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La requête de la société Atac et de la société Attesa est rejetée.<br/>
<br/>
Article 2 : La société Atac et la société Attesa verseront une somme de 1 500 euros chacune à la société Vergezali au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la société Atac, à la société Attesa et à la société Vergezali. <br/>
Copie en sera adressée pour information à la société Distribution Casino France et à la Commission nationale d'aménagement commercial.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
