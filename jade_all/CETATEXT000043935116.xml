<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043935116</ID>
<ANCIEN_ID>JG_L_2021_08_000000455045</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/93/51/CETATEXT000043935116.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 10/08/2021, 455045, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-08-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>455045</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:455045.20210810</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
               M. E... B... et Mme D... C... ont demandé au juge des référés du tribunal administratif de Nantes, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre au préfet de la Loire-Atlantique de leur indiquer un lieu susceptible de les accueillir dans un délai de vingt-quatre heures, ou, à défaut, de leur verser une rente mensuelle pour leur permettre de pourvoir dignement à leurs besoins de logement. Par une ordonnance n° 2107281 du 9 juillet 2021, le juge des référés du tribunal administratif de Nantes a rejeté leur requête. <br/>
<br/>
               Par une requête, enregistrée le 28 juillet 2021 au secrétariat du contentieux du Conseil d'Etat, M. B... et Mme C... demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
               1°) de les admettre au bénéfice de l'aide juridictionnelle à titre provisoire, s'ils n'ont pas reçu de réponse à leurs demandes en cours ;<br/>
<br/>
               2°) d'annuler cette ordonnance ; <br/>
<br/>
               3°) de faire droit à leurs conclusions de première instance ;<br/>
<br/>
               4°) de mettre à la charge de l'Etat la somme de 2 400 euros au titre des articles 37 de la loi n° 91-647 du 10 juillet 1990 et L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
               Ils soutiennent que :<br/>
               - la condition d'urgence est satisfaite dès lors que, d'une part, ils sont sans hébergement alors même qu'ils ont le statut de demandeur à un titre de séjour et, d'autre part, ils se trouvent dans une situation de détresse sociale et médicale ;<br/>
               - il est porté une atteinte grave et manifestement illégale à plusieurs libertés fondamentales ; <br/>
               - il est porté une atteinte grave et manifestement illégale au droit à un hébergement d'urgence dès lors qu'aucune solution d'hébergement d'urgence ne leur a été proposée, alors même qu'ils se trouvent dans une situation de vulnérabilité, qu'il appartenait au préfet d'évaluer ; <br/>
               - l'ordonnance attaquée est entachée d'une erreur de fait en ce qu'elle retient qu'ils n'ont fait aucune démarche pour régulariser leur situation, alors même que le préfet a notifié le 6 novembre 2020 un refus de titre de séjour et une mesure d'éloignement à M. B... ; <br/>
               - il est porté une atteinte grave et manifestement illégale à l'intérêt supérieur de l'enfant dès lors que, d'une part, leur enfant âgé de dix-huit mois est contraint de dormir dans des lieux publics et, d'autre part, l'ordonnance attaquée est entachée d'un défaut de motivation en ce qu'elle ne fait pas spécifiquement référence à la situation de l'enfant et ne recherche pas si son intérêt supérieur commande que ses besoins primaires soient pris en compte dans l'appréciation de l'urgence.<br/>
<br/>
<br/>
               Vu les autres pièces du dossier ;<br/>
<br/>
               Vu : <br/>
               - la convention internationale relative aux droits de l'enfant ;<br/>
               - le code de l'action sociale et des familles ;  <br/>
               - la loi n° 91-647 du 10 juillet 1991 ;<br/>
               - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
               1. Aux termes de l'article L. 521-2 du même code : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
               2. L'article L. 345-2 du code de l'action sociale et des familles prévoit que, dans chaque département, est mis en place, sous l'autorité du préfet, " un dispositif de veille sociale chargé d'accueillir les personnes sans abri ou en détresse ". L'article L. 345-2-2 dispose que : " Toute personne sans abri en situation de détresse médicale, psychique ou sociale a accès, à tout moment, à un dispositif d'hébergement d'urgence (...) ". Aux termes de l'article L. 345-2-3 : " Toute personne accueillie dans une structure d'hébergement d'urgence doit pouvoir y bénéficier d'un accompagnement personnalisé et y demeurer, dès lors qu'elle le souhaite, jusqu'à ce qu'une orientation lui soit proposée ( ...) ". Enfin, aux termes de l'article L. 121-7 du même code : " Sont à la charge de l'Etat au titre de l'aide sociale : (...) 8° Les mesures d'aide sociale en matière de logement, d'hébergement et de réinsertion, mentionnées aux articles L. 345-1 à L. 345-3 (...) ".<br/>
<br/>
               3. Il appartient aux autorités de l'Etat de mettre en œuvre le droit à l'hébergement d'urgence reconnu par la loi à toute personne sans abri qui se trouve en situation de détresse médicale, psychique et sociale. Seule une carence caractérisée des autorités de l'Etat dans la mise en œuvre du droit à l'hébergement d'urgence peut faire apparaître, pour l'application de l'article L. 521-2 du code de justice administrative, une atteinte manifestement illégale à une liberté fondamentale permettant au juge des référés de faire usage des pouvoirs qu'il tient de ce texte, en ordonnant à l'administration de faire droit à une demande d'hébergement d'urgence. Il lui incombe d'apprécier, dans chaque cas, les diligences accomplies par l'administration, en tenant compte des moyens dont elle dispose, ainsi que de l'âge, de l'état de santé et de la situation de famille A... la personne intéressée. Les ressortissants étrangers qui font l'objet d'une obligation de quitter le territoire français ou dont la demande d'asile a été définitivement rejetée, et qui doivent ainsi quitter le territoire en vertu des dispositions de l'article L. 542-4 du code de l'entrée et du séjour des étrangers et du droit d'asile, n'ont pas vocation à bénéficier du dispositif d'hébergement d'urgence. Dès lors, s'agissant des ressortissants étrangers placés dans cette situation particulière, une carence constitutive d'une atteinte grave et manifestement illégale à une liberté fondamentale ne saurait être caractérisée, à l'issue de la période strictement nécessaire à la mise en œuvre de leur départ volontaire, qu'en cas de circonstances exceptionnelles. Constitue une telle circonstance, en particulier lorsque, notamment du fait de leur très jeune âge, une solution appropriée ne pourrait être trouvée dans leur prise en charge hors de leur milieu de vie habituel par le service de l'aide sociale à l'enfance, l'existence d'un risque grave pour la santé ou la sécurité d'enfants mineurs, dont l'intérêt supérieur doit être une considération primordiale dans les décisions les concernant.<br/>
<br/>
               4. Il résulte de l'instruction menée par le juge des référés du tribunal administratif de Nantes que M. B... et Mme C..., ressortissants guinéens, ont vu leurs demandes d'asile rejetées définitivement par la Cour nationale du droit d'asile respectivement le 6 mai 2019 et le 17 septembre 2020. La demande d'asile présentée pour leur fils F..., né en novembre 2019, a été rejetée définitivement par l'Office français de protection des réfugiés et apatrides le 31 mars 2021. La famille a été prise en charge par l'hébergement d'urgence jusqu'au 2 septembre 2020, avant de s'installer à Nantes chez un tiers jusqu'au 25 juin 2021. Les intéressés, qui disposaient d'un délai raisonnable à compter de la notification de ces décisions pour organiser leur départ du territoire, se sont abstenus de toute démarche en vue d'organiser ce départ et se sont maintenus de façon irrégulière sur le territoire national. Il n'est pas établi que l'enfant, malgré son très jeune âge, serait exposé à des risques graves pour sa santé et pour sa sécurité. Il résulte également de cette instruction que, pour faire face à l'insuffisance des places disponibles compte tenu de l'augmentation du nombre des demandes, l'Etat a recouru en Loire-Atlantique de façon importante à l'hébergement hôtelier, sans pour autant parvenir à répondre à l'ensemble des besoins les plus urgents.<br/>
<br/>
               5. Dans ces conditions, sur lesquelles M. B... et Mme C... n'apportent en appel aucun élément nouveau, la situation des intéressés ne caractérise pas une carence de l'Etat constitutive d'une atteinte grave et manifestement illégale à une liberté fondamentale telle que mentionnée au point 3. Ils ne sont pas fondés à se plaindre de ce que, par l'ordonnance attaquée, qui est suffisamment motivée, le juge des référés du tribunal administratif de Nantes a rejeté leur demande. <br/>
<br/>
               6. Il résulte de ce qui précède qu'il est manifeste que l'appel de M. B... et Mme C... ne peut être accueilli. Il y a lieu, par suite, de rejeter leur requête selon la procédure prévue par l'article L. 522-3 du code de justice administrative, y compris leurs conclusions tendant à l'application des articles 37 de la loi du 10 juillet 1991 et L. 761-1 du code de justice administrative, sans qu'il y ait lieu d'admettre les intéressés au bénéfice de l'aide juridictionnelle provisoire.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de M. B... et Mme C... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. E... B... et Mme D... C....<br/>
Copie en sera adressée au ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
