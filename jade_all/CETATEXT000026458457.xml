<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026458457</ID>
<ANCIEN_ID>JG_L_2012_10_000000356271</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/45/84/CETATEXT000026458457.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 04/10/2012, 356271</TITRE>
<DATE_DEC>2012-10-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>356271</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Sophie-Caroline de Margerie</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Damien Botteghi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:356271.20121004</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 27 janvier 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par la Commission nationale des comptes de campagne et des financements politiques, dont le siège est 36, rue du Louvre à Paris (75042) Cedex 01 ; la commission demande au Conseil d'Etat d'annuler le jugement n°1108693 du 10 janvier 2012 du tribunal administratif de Cergy-Pontoise ayant rejeté sa saisine concernant le compte de campagne de M. Christophe C, candidat à l'élection cantonale qui s'est déroulée les 20 et 27 mars 2011 dans le canton de Puteaux (Hauts-de-Seine) ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code électoral, modifié notamment par la loi n° 2011-412  du 14 avril 2011 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie-Caroline de Margerie, Conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Damien Botteghi, Rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Sur l'appel de la Commission nationale des comptes de campagne et des financements politiques :<br/>
<br/>
              1. Considérant que selon l'article L. 52-12 du code électoral, le compte de campagne que les candidats sont tenus d'établir doit retracer " selon leur origine, l'ensemble des recettes perçues et, selon leur nature, l'ensemble des dépenses engagées ou effectuées en vue de l'élection, hors celles de la campagne officielle " au cours de la période mentionnée à l'article L. 52-4 du même code ; que l'article L. 52-15 du même code détermine les compétences de la Commission nationale des comptes de campagne et des financements politiques en matière de contrôle des comptes de campagne, de détermination du montant du remboursement forfaitaire dû par l'Etat et de fixation de la somme qu'un candidat est tenu de reverser au Trésor public en cas de dépassement du plafond des dépenses électorales ;<br/>
<br/>
              2. Considérant que le compte de campagne de M. C, candidat aux élections qui se sont déroulées les 20 et 27 mars 2011 en vue de la désignation du conseiller général du canton de Puteaux (Hauts-de-Seine), a été rejeté par la Commission nationale des comptes de campagne et des financements politiques par décision du 4 octobre 2011 au motif de l'omission, dans le compte de campagne, de toute dépense relative à un site internet intitulé " monputeaux.com " ;<br/>
<br/>
              3. Considérant que, si le site " monputeaux.com " a été créé par M. C en 2002 en vue de diffuser de l'information sur la vie municipale de Puteaux, il résulte de l'instruction que les articles publiés sur ce site pendant les semaines qui ont précédé l'élection ont été, pour l'essentiel, consacrés à rendre compte, en des termes très favorables, de la campagne menée par M. C, à soutenir sa candidature et à critiquer son principal adversaire à l'élection cantonale des 20 et 27 mars 2011 ; que ces articles étaient, pour la plupart d'entre eux, signés de M. C lui-même qui s'exprimait à la première personne du singulier ; que si M. C dispose d'un autre site, intitulé " grebert.net ", qu'il présente comme étant son site de campagne, les deux sites, de présentation très semblable, ont comporté des renvois réciproques de l'un à l'autre pendant la période précédant l'élection ; que dans les circonstances de l'espèce, eu égard au contenu des articles publiés sur le site " monputeaux.com ", à leur tonalité et à l'identité de leur auteur, ce site doit être regardé comme ayant contribué à la campagne électorale de M. C ; qu'ainsi les dépenses afférentes à ce site, pour la période mentionnée à l'article L. 52-4 du code électoral, ont été effectuées en vue de l'élection et auraient dû être retracées au compte de campagne du candidat ; <br/>
<br/>
              4. Considérant toutefois qu'il résulte du supplément d'instruction auquel il a été procédé par le Conseil d'Etat que le coût d'hébergement du site " monputeaux.com " a été de 14,95 euros par mois au cours de la période en cause ; qu'un tel coût peut être tenu comme négligeable ; qu'ainsi, dans les circonstances de l'espèce, le fait que ces dépenses n'aient pas été retracées dans le compte de campagne ne justifiait pas le rejet du compte ; que, dès lors, la Commission nationale des comptes de campagne et des financements politiques n'est pas fondée à se plaindre de ce que, par le jugement attaqué, le tribunal administratif de Cergy-Pontoise a rejeté sa saisine ; <br/>
<br/>
              Sur le remboursement des dépenses électorales dû par l'Etat : <br/>
<br/>
              5. Considérant qu'aux termes du second alinéa de l'article L. 118-2 du code électoral, issu de la loi du 14 avril 2011 portant simplification de dispositions du code électoral et relative à la transparence financière de la vie politique : " Sans préjudice de l'article L. 52-15, lorsqu'il constate que la commission instituée par l'article L. 52-14 n'a pas statué à bon droit, le juge de l'élection fixe le montant du remboursement dû au candidat en application de l'article L. 52-11-1 " ; qu'aux termes de l'article L. 118-3 du même code : " Saisi par la commission instituée par l'article L. 52-14, le juge de l'élection peut déclarer inéligible le candidat dont le compte de campagne, le cas échéant après réformation, fait apparaître un dépassement du plafond des dépenses électorales. / Saisi dans les mêmes conditions, le juge de l'élection peut déclarer inéligible le candidat qui n'a pas déposé son compte de campagne dans les conditions et le délai prescrits à l'article L. 52-12. / Il prononce également l'inéligibilité du candidat dont le compte de campagne a été rejeté à bon droit en cas de volonté de fraude ou de manquement d'une particulière gravité aux règles relatives au financement des campagnes électorales. (...) " ;<br/>
<br/>
              6. Considérant qu'il résulte des dispositions du second alinéa de l'article L. 118-2 du code électoral que, lorsque le juge de l'élection se prononce sur un compte de campagne et sur l'éligibilité d'un candidat, il lui appartient, qu'il soit ou non saisi de conclusions en ce sens, de fixer le montant du remboursement dû par l'Etat au candidat s'il constate que la Commission nationale des comptes de campagne et des financements politiques n'a pas statué à bon droit ; qu'en revanche, il n'appartient pas au juge de l'élection d'ordonner à un candidat le remboursement des sommes qu'il aurait perçues à ce titre ;<br/>
<br/>
              7. Considérant qu'il en va notamment ainsi lorsque le juge de l'élection, saisi par la Commission nationale des comptes de campagne et des financements politiques, se prononce sur l'éligibilité d'un candidat en application des dispositions précitées de l'article L. 118-3 du code électoral ; que, dans cette hypothèse, il lui appartient, avant de statuer sur l'éligibilité du candidat et, le cas échéant, de fixer le montant du remboursement dû par l'Etat, de se prononcer sur le bien-fondé de la décision par laquelle la commission a réformé ou rejeté le compte ; <br/>
<br/>
              8. Considérant que le tribunal administratif de Cergy-Pontoise, après avoir estimé que c'était à tort que la Commission nationale des comptes de campagne et des financements politiques avait rejeté le compte de campagne de M. C, s'est borné à rejeter la saisine de la commission, sans fixer le montant du remboursement dû par l'Etat ; que ce faisant, le tribunal administratif a méconnu son office ; que, par suite, le jugement attaqué doit être annulé en tant qu'il n'a pas fixé le montant du remboursement que l'Etat doit à M. C en application de l'article L. 52-11-1 du code électoral ;<br/>
<br/>
              9. Considérant qu'il y a lieu d'évoquer et de statuer immédiatement, dans cette mesure, sur la saisine par laquelle la Commission nationale des comptes de campagne et des financements politiques a transmis au juge de l'élection le compte de campagne de M. C ;<br/>
<br/>
              10. Considérant qu'aux termes du premier alinéa de l'article L. 52-11-1 du code électoral, dans sa version applicable aux opérations électorales des 20 et 27 mars 2011 : " Les dépenses électorales des candidats aux élections auxquelles l'article L. 52-4 est applicable font l'objet d'un remboursement forfaitaire de la part de l'Etat égal à 50 % de leur plafond de dépenses. Ce remboursement ne peut excéder le montant des dépenses réglées sur l'apport personnel des candidats et retracées dans leur compte de campagne " ; qu'aux termes des deux derniers alinéas du même article, dans leur version issue de la loi du 14 avril 2011 : " Le remboursement forfaitaire n'est pas versé aux candidats qui ont obtenu moins de 5 % des suffrages exprimés au premier tour de scrutin, qui ne se sont pas conformés aux prescriptions de l'article L. 52-11, qui n'ont pas déposé leur compte de campagne dans le délai prévu au deuxième alinéa de l'article L. 52-12 ou dont le compte de campagne est rejeté pour d'autres motifs ou qui n'ont pas déposé leur déclaration de situation patrimoniale, s'ils sont astreints à cette obligation. / Dans les cas où les irrégularités commises ne conduisent pas au rejet du compte, la décision concernant ce dernier peut réduire le montant du remboursement forfaitaire en fonction du nombre et de la gravité de ces irrégularités " ;<br/>
<br/>
              11. Considérant qu'il résulte de ce qui a été dit ci-dessus que c'est à tort que le compte de campagne de M. C a été rejeté par la Commission nationale des comptes de campagne et des financements politiques ; que M. C, qui a obtenu plus de 5 % des suffrages, a droit, en application de l'article L. 52-11-1 du code électoral, à un remboursement forfaitaire égal à la moitié du plafond légal des dépenses, laquelle s'établit à 14 641 euros, le remboursement ne pouvant toutefois excéder le montant des dépenses réglées sur son apport personnel et retracées dans son compte de campagne ; que les dépenses de M. C réglées sur son apport personnel s'étant élevées à un montant de 8 180 euros, c'est à cette somme que doit être fixé le montant du remboursement forfaitaire auquel il a droit ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de réduire le montant du remboursement forfaitaire pour tenir compte de l'irrégularité constatée ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Cergy-Pontoise du 10 janvier 2012 est annulé en tant qu'il n'a pas fixé le montant du remboursement dû par l'Etat à M. C en application de l'article L. 52-11-1 du code électoral.<br/>
<br/>
Article 2 : Le montant du remboursement dû par l'Etat à M. C en application de l'article L. 52-11-1 du code électoral est fixé à 8 180 euros.<br/>
<br/>
Article 3 : Le surplus des conclusions de la requête de la Commission nationale des comptes de campagne et des financements politiques est rejeté.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la Commission nationale des comptes de campagne et des financements politiques et à M. Christophe C. <br/>
Copie en sera adressée au ministre de l'intérieur.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">28-005-02 ÉLECTIONS ET RÉFÉRENDUM. DISPOSITIONS GÉNÉRALES APPLICABLES AUX ÉLECTIONS. CAMPAGNE ET PROPAGANDE ÉLECTORALES. - DÉPENSES AFFÉRENTES À UN SITE INTERNET RENDANT COMPTE DE MANIÈRE TRÈS FAVORABLE DE LA CAMPAGNE D'UN CANDIDAT - CONTRIBUTION À LA CAMPAGNE DE CE DERNIER - QUALIFICATION - DÉPENSE DE CAMPAGNE - MÉTHODE - FAISCEAU D'INDICES - ESPÈCE - INCLUSION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">28-005-04-02-04 ÉLECTIONS ET RÉFÉRENDUM. DISPOSITIONS GÉNÉRALES APPLICABLES AUX ÉLECTIONS. FINANCEMENT ET PLAFONNEMENT DES DÉPENSES ÉLECTORALES. COMPTE DE CAMPAGNE. DÉPENSES. - DÉPENSES AFFÉRENTES À UN SITE INTERNET RENDANT COMPTE DE MANIÈRE TRÈS FAVORABLE DE LA CAMPAGNE D'UN CANDIDAT - CONTRIBUTION À LA CAMPAGNE DE CE DERNIER - QUALIFICATION - DÉPENSE DE CAMPAGNE - MÉTHODE - FAISCEAU D'INDICES - ESPÈCE - INCLUSION.
</SCT>
<ANA ID="9A"> 28-005-02 Site créé en 2002 par un candidat en vue de diffuser de l'information sur la vie municipale de sa commune. Pendant les semaines qui ont précédé l'élection cantonale de 2011, les articles diffusés ont été, pour l'essentiel, consacrés à rendre compte, en des termes très favorables, de la campagne menée par l'intéressé, à soutenir sa candidature et à critiquer son principal adversaire. Ces articles étaient, pour la plupart d'entre eux, signés par le candidat lui-même qui s'exprimait à la première personne du singulier. Si le candidat dispose par ailleurs d'un autre site, qu'il présente comme étant son site de campagne, les deux sites, de présentation très semblable, ont comporté des renvois réciproques de l'un à l'autre pendant la période précédant l'élection.... ...Dans ces circonstances, eu égard au contenu des articles publiés sur le site, à leur tonalité et à l'identité de leur auteur, ce site doit être regardé comme ayant contribué à la campagne électorale du candidat.... ...Ainsi, les dépenses afférentes à ce site, pour la période mentionnée à l'article L. 52-4 du code électoral, ont été effectuées en vue de l'élection et auraient dû être retracées au compte de campagne du candidat.</ANA>
<ANA ID="9B"> 28-005-04-02-04 Site créé en 2002 par un candidat en vue de diffuser de l'information sur la vie municipale de sa commune. Pendant les semaines qui ont précédé l'élection cantonale de 2011, les articles diffusés ont été, pour l'essentiel, consacrés à rendre compte, en des termes très favorables, de la campagne menée par l'intéressé, à soutenir sa candidature et à critiquer son principal adversaire. Ces articles étaient, pour la plupart d'entre eux, signés par le candidat lui-même qui s'exprimait à la première personne du singulier. Si le candidat dispose par ailleurs d'un autre site, qu'il présente comme étant son site de campagne, les deux sites, de présentation très semblable, ont comporté des renvois réciproques de l'un à l'autre pendant la période précédant l'élection.... ...Dans ces circonstances, eu égard au contenu des articles publiés sur le site, à leur tonalité et à l'identité de leur auteur, ce site doit être regardé comme ayant contribué à la campagne électorale du candidat.,,Ainsi, les dépenses afférentes à ce site, pour la période mentionnée à l'article L. 52-4 du code électoral, ont été effectuées en vue de l'élection et auraient dû être retracées au compte de campagne du candidat.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
