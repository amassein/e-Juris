<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027397735</ID>
<ANCIEN_ID>JG_L_2013_05_000000365706</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/39/77/CETATEXT000027397735.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème / 2ème SSR, 07/05/2013, 365706</TITRE>
<DATE_DEC>2013-05-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>365706</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème / 2ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP COUTARD, MUNIER-APAIRE ; SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>M. Fabrice Aubert</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2013:365706.20130507</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 4 et 19 février 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société Segex, dont le siège est 4 boulevard Arago à Wissous (91320), et la société Aximum, dont le siège est 41 boulevard de la République à Chatou (78400) ; les sociétés demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1221993 du 17 janvier 2013 par laquelle le juge des référés du tribunal administratif de Paris, statuant sur la demande de la société Agilis en application de l'article L. 551-1 du code de justice administrative, a, d'une part, annulé la procédure d'appel d'offres lancée le 21 septembre 2012 par la direction régionale et interdépartementale de l'équipement et de l'aménagement d'Ile-de-France en vue de conclure un marché public de services portant sur la mise en place de balisage et de signalisation de déviations sur le réseau routier national d'Ile-de-France, et, d'autre part, enjoint à la direction régionale et interdépartementale de l'équipement et de l'aménagement d'Ile-de-France, si elle entendait conclure le marché, d'en reprendre l'intégralité de la procédure de passation ; <br/>
<br/>
              2°) statuant en référé, de rejeter la demande de la société Agilis ; <br/>
<br/>
              3°) de mettre à la charge de la société Agilis le versement à chacune d'elles de la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des marchés publics ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Fabrice Aubert, Auditeur,  <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Coutard, Munier-Apaire, avocat de la société Segex et de la société Aximum, et à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de la société Agilis ;<br/>
<br/>
<br/>
<br/>1. Considérant, d'une part, qu'aux termes de l'article L. 551-1 du code de justice administrative : " Le président du tribunal administratif, ou le magistrat qu'il délègue, peut être saisi en cas de manquement aux obligations de publicité et de mise en concurrence auxquelles est soumise la passation par les pouvoirs adjudicateurs de contrats administratifs ayant pour objet l'exécution de travaux, la livraison de fournitures ou la prestation de services, avec une contrepartie économique constituée par un prix ou un droit d'exploitation, ou la délégation d'un service public (...) " ; qu'aux termes de l'article L. 551-2 de ce code : " I. Le juge peut ordonner à l'auteur du manquement de se conformer à ses obligations et suspendre l'exécution de toute décision qui se rapporte à la passation du contrat, sauf s'il estime, en considération de l'ensemble des intérêts susceptibles d'être lésés et notamment de l'intérêt public, que les conséquences négatives de ces mesures pourraient l'emporter sur leurs avantages. (...) " ; que, selon l'article L. 551-10 du même code : " Les personnes habilitées à engager les recours prévus aux articles L. 551-1 et L. 551-5 sont celles qui ont un intérêt à conclure le contrat et qui sont susceptibles d'être lésées par le manquement invoqué (...) " ;<br/>
<br/>
              2. Considérant, d'autre part, qu'aux termes de l'article 45 du code des marchés publics : " (...) Lorsque le pouvoir adjudicateur décide de fixer des niveaux minimaux de capacité, il ne peut être exigé des candidats que des niveaux minimaux de capacité liés et proportionnés à l'objet du marché. Les documents, renseignements et les niveaux minimaux de capacité demandés sont précisés dans l'avis d'appel public à la concurrence ou, en l'absence d'un tel avis, dans les documents de la consultation (...) " ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que, par un avis d'appel public à la concurrence publié le 21 septembre 2012, la direction régionale et interdépartementale de l'équipement et de l'aménagement d'Ile-de-France a lancé la procédure de passation d'un marché à bons de commande, d'un montant minimum de 3 millions d'euros et d'un montant maximum de 12 millions d'euros pour une durée de quarante-huit mois, pour la mise en place de balisages et de signalisations de déviation sur le réseau routier d'Ile-de-France ; que la candidature du groupement, dont la société Agilis est le mandataire, a été écartée au motif qu'elle ne satisfaisait pas aux exigences de capacités techniques des candidats, qui devaient justifier être dotés d'au moins douze fourgons équipés de panneaux à messages variables pour la réalisation des prestations du marché ; que par ordonnance du 17 janvier 2013, contre laquelle les sociétés Segex et Aximum, formant l'autre groupement candidat à l'attribution du marché, se pourvoient en cassation, le juge des référés du tribunal administratif de Paris a fait droit à la demande de la société Agilis sur le fondement de l'article L. 551-1 du code de justice administrative, en annulant la procédure de passation au motif que le niveau de capacité technique ainsi exigé des candidats n'était manifestement pas lié et proportionné avec l'objet du marché ;<br/>
<br/>
              4. Considérant qu'en jugeant que le pouvoir adjudicateur avait ainsi manqué à ses obligations résultant de l'article 45 du code des marchés publics, alors que le juge ne peut annuler une procédure de passation d'un marché pour un tel motif que si l'exigence de capacité technique imposée aux candidats est manifestement dépourvue de lien avec l'objet du marché ou manifestement disproportionnée et qu'il ressortait des pièces du dossier soumis à son examen, d'une part, que la diversité des messages potentiels à diffuser par le cocontractant comme l'étendue du réseau routier pouvaient justifier l'exigence d'une dotation minimale importante en véhicules de ce type et, d'autre part, que ces matériels pouvaient être aisément acquis ou loués par les candidats pour satisfaire aux niveaux de capacité technique ainsi exigés, le juge des référés du tribunal administratif de Paris a dénaturé les pièces du dossier ; <br/>
<br/>
<br/>
              5. Considérant que, si le juge des référés a également relevé que le pouvoir adjudicateur avait manqué à ses obligations de publicité et de mise en concurrence en examinant simultanément les candidatures et les offres des candidats, ce motif ne peut, en tout état de cause, être regardé comme justifiant à lui seul le dispositif de l'ordonnance attaquée dès lors que le juge des référés ne s'est pas prononcé sur la question de savoir si la société Agilis avait ou était susceptible d'avoir été lésée par ce manquement ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'ordonnance attaquée doit être annulée ;  <br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de statuer sur la demande en référé en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              7. Considérant, en premier lieu, que le moyen tiré de ce que les documents de la consultation n'identifiaient pas clairement le pouvoir adjudicateur et, par voie de conséquence, ne permettaient pas aux candidats de déterminer le tribunal administratif territorialement compétent en cas de litige ne peut, en tout état de cause, qu'être écarté comme inopérant devant le Conseil d'Etat statuant sur la demande en référé ;<br/>
<br/>
              8. Considérant, en deuxième lieu, que l'exigence de capacité technique de douze fourgons équipés de panneaux à messages variables, qui était, contrairement à ce que soutient la société requérante, prévue sans ambiguïté par les documents de consultation, n'était pas manifestement dépourvue de lien avec l'objet du marché ni manifestement disproportionnée ; que, par suite, le pouvoir adjudicateur n'a, en l'adoptant, manqué ni à ses obligations résultant des dispositions de l'article 45 du code des marchés publics relatives aux niveaux de capacité des candidats, ni à celles résultant des dispositions du III de l'article 6 du même code et relatives aux spécifications techniques ; <br/>
<br/>
              9. Considérant, en troisième lieu, que le moyen tiré par la société Agilis, qui ne satisfaisait pas à l'exigence de capacité technique mentionnée ci-dessus et dont la candidature devait par suite être écartée, de ce que le pouvoir adjudicateur aurait irrégulièrement examiné la recevabilité des candidatures en tenant compte du contenu des offres ne peut qu'être écarté ;<br/>
<br/>
              10. Considérant, en dernier lieu, qu'aux termes de l'article 10 du code des marchés publics : " Afin de susciter la plus large concurrence, et sauf si l'objet du marché ne permet pas l'identification de prestations distinctes, le pouvoir adjudicateur passe le marché en lots séparés (...). Le pouvoir adjudicateur peut toutefois passer un marché global, avec ou sans identification de prestations distinctes, s'il estime que la dévolution en lots séparés est de nature, dans le cas particulier, à restreindre la concurrence, ou qu'elle risque de rendre techniquement difficile ou financièrement coûteuse l'exécution des prestations ou encore qu'il n'est pas en mesure d'assurer par lui-même les missions d'organisation, de pilotage ou de coordination " ; que si le pouvoir adjudicateur a, en l'espèce, engagé la procédure de passation litigieuse sans allotir le marché, il soutient, sans être sérieusement contesté sur ce point, qu'eu égard au caractère à la fois peu prévisible et inégal du volume de prestations nécessaires sur chacun des axes et tunnels routiers d'Ile-de-France, la mobilisation d'un plus grand nombre d'équipements fréquemment inoccupés qu'aurait occasionnée une dévolution en plusieurs lots aurait eu pour conséquence de rendre financièrement plus coûteuse l'exécution de ces prestations ; <br/>
<br/>
              11. Considérant qu'il résulte de tout ce qui précède que la société Agilis n'est pas fondée à demander l'annulation de la procédure de passation litigieuse ; <br/>
<br/>
              12. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge des sociétés Segex et Aximum qui ne sont pas, dans la présente instance, les parties perdantes ; qu'en revanche il y a lieu, dans les circonstances de l'espèce, de mettre sur ce fondement à la charge de la société Agilis la somme de 2 250 euros à verser à chacune des sociétés Segex et Aximum au titre de la procédure suivie tant devant le Conseil d'Etat que devant le juge des référés du tribunal administratif de Paris  ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 17 janvier 2013 du juge des référés du tribunal administratif de Paris est annulée. <br/>
Article 2 : La demande de la société Agilis devant le juge des référés du tribunal administratif de Paris et ses conclusions présentées devant le Conseil d'Etat au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 3 : La société Agilis versa la somme de 2 250 euros à la société Segex et la somme de 2 250 euros à la société Aximum sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : La présente décision sera notifiée à la société Segex, à la société Aximum et à la société Agilis. <br/>
Copie en sera adressée pour information à la ministre de l'écologie, du développement durable et de l'énergie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-08-015-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. PROCÉDURES D'URGENCE. - CONTRÔLE PAR LE JUGE DU RÉFÉRÉ PRÉCONTRACTUEL DE L'OBLIGATION PESANT SUR LE POUVOIR ADJUDICATEUR DE FIXER DES NIVEAUX MINIMAUX DE CAPACITÉ LIÉS ET PROPORTIONNÉS À L'OBJET DU MARCHÉ (ART. 45 DU CMP) - CONTRÔLE DE LA DISPROPORTION MANIFESTE [RJ1].
</SCT>
<ANA ID="9A"> 39-08-015-01 Le juge du référé précontractuel ne peut annuler une procédure de passation d'un marché pour manquement du pouvoir adjudicateur à ses obligations de fixer des niveaux minimaux de capacité liés et proportionnés à l'objet du marché résultant de l'article 45 du code des marchés publics (CMP) que si l'exigence de capacité technique imposée aux candidats est manifestement dépourvue de lien avec l'objet du marché ou manifestement disproportionnée.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., pour d'autres hypothèses de contrôle restreint en référé précontractuel, CE, 21 mai 2010, Commune d'Ajaccio, n° 333737, T. p. 849 ; CE, 1er mars 2012, Département de la Corse du Sud, n° 354159, p. 65.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
