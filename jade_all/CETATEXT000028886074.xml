<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028886074</ID>
<ANCIEN_ID>JG_L_2014_04_000000362268</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/88/60/CETATEXT000028886074.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème SSJS, 30/04/2014, 362268, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-04-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>362268</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>M. Olivier Japiot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:362268.20140430</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>VU LA PROCEDURE SUIVANTE :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              La SAS Eiffage TP a demandé au tribunal administratif de Montreuil la décharge des cotisations supplémentaires de taxe d'apprentissage et de participation des employeurs à l'effort de construction auxquelles elle a été assujettie au titre respectivement des années 2004 à 2006 et 2004 et 2005. Par un jugement n° 0912960 du 27 mai 2010, le tribunal administratif de Montreuil a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 10VE02200 du 19 juin 2012, la cour administrative d'appel de Versailles a annulé ce jugement et rejeté la demande de la SAS Eiffage TP. <br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 28 août 2012, 28 novembre 2012 et 30 janvier 2014 au secrétariat du contentieux du Conseil d'Etat, la SAS Eiffage TP demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'article 3 de l'arrêt du 19 juin 2012 de la cour administrative d'appel de Versailles ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'État la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
<br/>
              - la Constitution, notamment son Préambule et ses articles 34 et 61-1 ;<br/>
<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
              - le code de la construction et de l'habitation ; <br/>
<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              - le code de la sécurité sociale ; <br/>
<br/>
              - le code du travail ;<br/>
<br/>
              - la loi n° 87-588 du 30 juillet 1987 ;<br/>
<br/>
              - la loi n° 95-116 du 4 février 1995 ;<br/>
<br/>
              - la décision n° 2010-84 QPC du Conseil constitutionnel du 13 janvier 2011 ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Japiot, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de la SAS Eiffage TP ;<br/>
<br/>
<br/>
<br/>CONSIDERANT CE QUI SUIT :<br/>
<br/>
              Sur la question prioritaire de constitutionnalité :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé, y compris pour la première fois en cassation, à l'occasion d'une instance devant le Conseil d'État (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. La société requérante est, en application de l'article L. 3141-30 du code du travail, tenue de s'affilier à une caisse de congés payés qui, en contrepartie de cotisations versées par des employeurs, se substitue à eux pour le versement des indemnités de congés payés dues aux salariés qu'ils emploient. Elle soutient que les articles 225 et 235 bis du code général des impôts et L. 242-1 du code de la sécurité sociale, en tant qu'ils s'appliquent aux employeurs tenus à cette obligation, portent atteinte au principe d'égalité devant la loi résultant de l'article 1er de la Déclaration des droits de l'homme et du citoyen, au droit de propriété protégé par ses articles 2 et 17, au principe du consentement à l'impôt résultant de son article 14 et à la garantie des droits résultant de son article 16, combinés avec l'article 34 de la Constitution déterminant la compétence du législateur, ainsi qu'à l'objectif de valeur constitutionnelle d'intelligibilité et d'accessibilité de la loi.<br/>
<br/>
              3. Aux termes de l'article 225 du code général des impôts, dans sa rédaction applicable au litige et devenu l'article 1599 ter B du même code, la taxe d'apprentissage " est assise sur les rémunérations, selon les bases et les modalités prévues aux chapitres Ier et II du titre IV du livre II du code de la sécurité sociale (...) ". En vertu du 1 de l'article 235 bis du même code, dans sa rédaction applicable au litige : " les employeurs qui, au 31 décembre de l'année suivant celle du paiement des rémunérations, n'ont pas procédé, dans les conditions fixées par décret en Conseil d'État aux investissements prévus à l'article L. 313-1 du code de la construction et de l'habitation sont, dans la mesure où ils n'ont pas procédé à ces investissements, assujettis à une cotisation de 2 % calculée sur le montant des rémunérations versées par eux au cours de l'année écoulée, évalué selon les règles prévues aux chapitres Ier et II du titre IV du livre II du code de la sécurité sociale (...) ". L'article L. 242-1 du code de la sécurité sociale, qui fixe l'assiette des cotisations de sécurité sociale, n'est applicable au présent litige, qui ne porte pas sur la détermination des cotisations sociales auxquelles est assujettie la société requérante, qu'en tant qu'il est renvoyé à ses dispositions par les articles 225 et 235 bis du code général des impôts.<br/>
<br/>
              4. Le Conseil constitutionnel, par une décision n° 2010-84 QPC du 13 janvier 2011 a, dans ses motifs et son dispositif, déclaré le 1 de l'article 235 bis du code général des impôts conforme à la Constitution. La décision n° 350093 du 20 novembre 2013 du Conseil d'État statuant au contentieux, faisant application de ces dispositions, ne constitue pas un changement des circonstances de nature à justifier que la question de la conformité à la Constitution de ces dispositions soit de nouveau soumise au Conseil constitutionnel.<br/>
<br/>
              5. Pour la définition de l'assiette de la taxe d'apprentissage, l'article 225 du code général des impôts, en renvoyant à la définition de l'assiette des cotisations sociales telle qu'elle résulte de l'article L. 242-1 du code de la sécurité sociale, soumet expressément à cette taxe les indemnités de congés payés. Ainsi que l'a jugé le Conseil d'État statuant au contentieux par sa décision mentionnée ci-dessus, le montant des indemnités de congés payés à prendre en compte, pour l'application de ces dispositions législatives, correspond à celui que l'employeur aurait versé à ses salariés en l'absence d'affiliation obligatoire à une caisse de congé résultant des dispositions du code du travail et des stipulations des conventions collectives ou accords applicables aux professions concernées ou, à défaut de pouvoir établir cette somme, à un montant évalué, compte tenu de ces dispositions et stipulations, à 11,5 % des rémunérations brutes versées au cours de l'année d'imposition. Le moyen tiré de ce que la loi aurait défini avec une précision insuffisante l'assiette de cette taxe et aurait ainsi méconnu l'article 34 de la Constitution ne saurait donc être regardé comme sérieux. La disposition législative litigieuse ne saurait non plus être regardée comme portant atteinte au principe d'égalité devant la loi, dès lors que les employeurs tenus de s'affilier à une caisse de congés payés ne sont pas dans la même situation que les employeurs qui ne sont pas tenus à une telle affiliation. Enfin, l'objectif de valeur constitutionnelle d'intelligibilité et d'accessibilité de la loi n'est pas au nombre des droits et libertés que la Constitution garantit, au sens de l'article 61-1 de la Constitution. Par suite, la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux.<br/>
<br/>
              6. Il résulte de ce qui précède que, sans qu'il soit besoin de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée, le moyen tiré de ce que les articles 225 et 235 bis du code général des impôts et L. 242-1 du code de la sécurité sociale portent atteinte aux droits et libertés garantis par la Constitution doit être écarté.<br/>
<br/>
              Sur les autres moyens du pourvoi :<br/>
<br/>
              En ce qui concerne le principe de l'imposition des indemnités de congés payés :<br/>
<br/>
              7. Les dispositions de la loi du 4 février 1995, en alignant l'assiette de la taxe d'apprentissage et de la participation des employeurs à l'effort de construction sur celle des cotisations sociales, laquelle comprend les indemnités de congés payés, ont rendue caduque la réponse ministérielle du 13 avril 1976 à M.A..., député, reprise dans l'instruction référencée 5 L-7-76. Par suite, en jugeant que la société requérante n'était pas fondée à se prévaloir, sur le fondement de l'article L. 80 A du livre des procédures fiscales, de cette réponse et de ces instructions, la cour n'a pas commis d'erreur de droit.<br/>
<br/>
              En ce qui concerne le montant de l'imposition :<br/>
<br/>
              8. Selon l'article L. 223-11 du code du travail, devenu l'article L. 3141-22 de ce code, l'indemnité afférente au congé annuel est égale au dixième de la rémunération brute totale perçue par le salarié au cours d'une période de référence définie par décret. Cet article prévoit le calcul de la rémunération brute totale en fonction du salaire gagné dû pour la période précédant le congé et de la durée du travail effectif de l'établissement, en précisant qu'il est tenu compte de l'indemnité de congé de l'année précédente. Toutefois, selon l'article L. 223-6 du même code, devenu l'article L. 3141-10 de ce code, ces dispositions ne portent pas atteinte aux stipulations des conventions ou accords collectifs de travail ou des contrats de travail ni aux usages qui assurent des congés payés de plus longue durée.<br/>
<br/>
              9. Si l'article L. 223-16 du code du travail, devenu l'article L. 3141-30 de ce code, prévoit l'affiliation obligatoire de certains employeurs à une caisse de congé, notamment lorsque les salariés ne sont pas habituellement occupés de façon continue chez un même employeur au cours de la période reconnue pour l'appréciation du droit au congé, ce qui est notamment le cas, en vertu de l'article D. 732-1 du code du travail, devenu l'article D. 3141-12 de ce code, dans les entreprises relevant du secteur du bâtiment et des travaux publics, l'assiette de la taxe d'apprentissage et de la participation des employeurs à l'effort de construction est constituée par l'ensemble des rémunérations dues en contrepartie ou à l'occasion du travail, y compris les indemnités de congés payés, quand bien même le service de ces indemnités est assuré pour le compte de cet employeur par la caisse de congés payés à laquelle il est obligatoirement affilié.<br/>
<br/>
              10. Il résulte de ce qui précède que le montant des indemnités de congés payés à prendre en compte dans l'assiette de la taxe d'apprentissage et de la participation des employeurs à l'effort de construction correspond à celui que l'employeur aurait versé à ses salariés en l'absence d'affiliation obligatoire à une caisse, en application des dispositions du code du travail et des conventions collectives ou accords applicables à la profession. Ce montant ne saurait donc être évalué en retenant les cotisations versées par l'employeur à la caisse de congés payés dès lors que ces cotisations, qui ne constituent pas des rémunérations au sens des dispositions précitées, couvrent par ailleurs des charges autres que les indemnités versées aux salariés, notamment les frais de fonctionnement des caisses. Le montant à prendre en compte ne saurait davantage être fixé à partir des indemnités versées par les différentes caisses aux salariés au titre d'une période retenue pour l'appréciation du droit au congé, dès lors que les sommes versées par les caisses à un salarié peuvent correspondre aux droits à congés payés qu'un salarié a acquis auprès de plusieurs employeurs, qui sont seuls redevables de la taxe d'apprentissage et de la participation des employeurs à l'effort de construction.<br/>
<br/>
              11. A défaut de pouvoir établir exactement les sommes que l'employeur aurait versées à ses salariés au titre des indemnités de congés payés, en l'absence d'affiliation obligatoire à une caisse, il y a lieu de retenir, compte tenu à la fois du taux prévu par l'article L. 233-1 du code du travail, devenu l'article L. 3141-22 de ce code, de l'indemnité de congé payé qui aurait, le cas échéant, été versée par l'employeur au titre de l'année précédente et des indemnités prévues par les conventions collectives, un montant évalué à 11,5 % des rémunérations brutes versées au cours de l'année d'imposition.<br/>
<br/>
              12. Pour évaluer, afin de déterminer l'assiette de la taxe d'apprentissage et de la participation à l'effort de construction dues par la société requérante, le montant des indemnités de congés payés qu'elle aurait versées à ses salariés en l'absence d'affiliation à la caisse de congés payés, la cour s'est fondée sur le rapport entre le nombre de jours de congés payés et la masse salariale des entreprises du bâtiment, en retenant un taux de 13,14 % des rémunérations versées. Il résulte de ce qui a été dit plus haut qu'elle a, ce faisant, commis une erreur de droit. Par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'article 3 de l'arrêt attaqué doit être annulé, en tant qu'il statue sur le montant de l'imposition en litige.<br/>
<br/>
              13. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'État la somme de 1 000 euros à verser à la société requérante au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la SAS Eiffage TP.<br/>
Article 2 : L'article 3 de l'arrêt du 19 juin 2012 de la cour administrative d'appel de Versailles est annulé, en tant qu'il statue sur le montant de l'imposition en litige.<br/>
Article 3 : L'affaire est renvoyée dans cette mesure à la cour administrative d'appel de Versailles.<br/>
Article 4 : L'État versera à la SAS Eiffage TP une somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : La présente décision sera notifiée à la SAS Eiffage TP et au ministre des finances et des comptes publics.<br/>
Copie en sera adressée au Conseil constitutionnel et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
