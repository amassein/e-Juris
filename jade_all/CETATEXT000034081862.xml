<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034081862</ID>
<ANCIEN_ID>JG_L_2017_02_000000395844</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/08/18/CETATEXT000034081862.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 24/02/2017, 395844, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-02-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>395844</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>M. Pierre Lombard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:395844.20170224</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La Société Luchard Industrie a demandé au tribunal administratif d'Amiens l'annulation du titre de perception n°2011/00112 émis à son encontre par le directeur départemental des finances publiques de l'Oise le 17 mai 2011 pour un montant de 533 008 euros. Par un jugement n°1102895 du 26 novembre 2013, le tribunal administratif d'Amiens a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 14DA00178 du 4 novembre 2015 la cour administrative d'appel de Douai a annulé le titre de perception en tant qu'il concerne le versement d'intérêts de retard pour un montant de 128 558 euros et a rejeté le surplus des conclusions de l'appel formé par la Société Luchard Industrie contre le jugement du tribunal administratif d'Amiens.<br/>
<br/>
              Par un pourvoi et un mémoire en réplique, enregistrés le 4 janvier et le 16 juin 2016 au secrétariat du contentieux du Conseil d'Etat, la Société Luchard Industrie demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il a rejeté ses conclusions tendant à l'annulation du titre de perception mettant à sa charge une somme de 404 450 euros ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire entièrement droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 4 000 euros en application de l'article L.761-1 du code de justice administrative. <br/>
<br/>
              Par un mémoire en défense, enregistré le 20 mai 2016, le ministre des finances et des comptes publics conclut au rejet du pourvoi. Il soutient que les moyens ne sont pas fondés. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le traité sur le fonctionnement de l'Union européenne ; <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - le règlement (CE) n°1999/659 du Conseil du 22 mars 1999 ; <br/>
              - l'arrêt 214/07 du 13 novembre 2008 de la Cour de justice de l'Union européenne ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pierre Lombard, auditeur,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Odent, Poulet, avocat de la Société Luchard Industrie ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Luchard Industrie a bénéficié au titre de l'année 2002 de l'exonération d'impôt alors prévu par l'article 44 septies du code général des impôts. Mais le 17 mai 2011, le directeur départemental des finances publiques de l'Oise a émis à son encontre un titre de perception d'un montant correspondant aux cotisations d'impôt sur les sociétés et de contribution additionnelle à l'impôt sur les sociétés dont elle avait été exonérée au titre de l'année 2002, diminuées des aides dites de minimis et majorées des intérêts de retard. L'émission de ce titre de perception faisait suite à une décision de la Commission européenne et un arrêt de la Cour de Justice de l'Union européenne. Par une décision du 16 décembre 2003, la Commission européenne a déclaré que les exonérations octroyées en application de ces dispositions, autres que celles qui remplissent les conditions d'octroi des aides de minimis et des aides à finalités régionales, constituaient des aides d'Etat illégales et ordonné la récupération sans délai des aides versées. Par un arrêt du 13 novembre 2008, Commission contre France, la Cour de justice de l'Union européenne a jugé que la France avait manqué à ses obligations de recouvrement de ces aides. <br/>
<br/>
              2. Par un jugement du 26 novembre 2013, le tribunal administratif d'Amiens a rejeté la demande de la société Luchard Industrie tendant à l'annulation de ce titre de perception. Par un arrêt du 4 novembre 2015, la cour administrative d'appel de Douai a réformé le jugement attaqué, annulé le titre de perception en tant qu'il concerne le versement des intérêts de retard et rejeté le surplus des conclusions de la société requérante. Celle-ci se pourvoi en cassation contre cet arrêt en tant qu'il a rejeté le surplus de se conclusions d'appel. <br/>
<br/>
              3. Aux termes de l'article 14 du règlement n° 659/1999/CE du 22 mars 1999 portant modalités d'application de l'article 93 du traité CE, devenu l'article 108 du traité sur le fonctionnement de l'Union européenne : " 1. En cas de décision négative concernant une aide illégale, la Commission décide que l'Etat membre concerné prend toutes les mesures nécessaires pour récupérer l'aide auprès de son bénéficiaire (ci-après dénommée " décision de récupération "). La Commission n'exige pas la récupération de l'aide si, ce faisant, elle allait à l'encontre d'un principe général du droit communautaire./ (...)/3. (...) la récupération s'effectue sans délai et conformément aux procédures prévues par le droit national de l'Etat membre concerné, pour autant que ces dernières permettent l'exécution immédiate et effective de la décision de la Commission. A cette fin et en cas de procédure devant les tribunaux nationaux, les Etats membres concernés prennent toutes les mesures prévues par leurs systèmes juridiques respectifs, y compris les mesures provisoires, sans préjudice du droit communautaire ". Aux termes de l'article 15 du même règlement : " 1. Les pouvoirs de la Commission en matière de récupération de l'aide sont soumis à un délai de prescription de dix ans. / 2. Le délai de prescription commence le jour où l'aide illégale est accordée au bénéficiaire, à titre d'aide individuelle ou dans le cadre d'un régime d'aide. Toute mesure prise par la Commission ou un Etat membre, agissant à la demande de la Commission, à l'égard de l'aide illégale interrompt le délai de prescription. Chaque interruption fait courir de nouveau le délai. Le délai de prescription est suspendu aussi longtemps que la décision de la Commission fait l'objet d'une procédure devant la Cour de justice des Communautés européennes (...) ".<br/>
<br/>
              En ce qui concerne le principe de la récupération de l'aide :<br/>
<br/>
              4. Il ressort des énonciations de l'arrêt attaqué que la cour administrative d'appel a écarté comme non fondés les moyens soulevés par la société requérante et tirés de ce que l'administration ne pouvait exiger d'elle la récupération de l'aide dont elle avait bénéficié sous forme d'exonération d'impôt sans méconnaître les principes de sécurité juridique et de confiance légitime ainsi que son droit de propriété garanti par les stipulations de l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. <br/>
<br/>
              5. Les moyens mentionnés ci-dessus ne sauraient viser que la décision de principe de récupération des aides illégales, prise par la Commission européenne et dont la Cour de Justice de l'Union européenne a constaté, par l'arrêt du 13 novembre 2008 mentionné au point 1, que la France avait manqué à ses obligations en ne l'exécutant dans le délai imparti. Dans ses conditions, les autorités nationales étaient tenues de récupérer auprès des contribuables, dans les conditions prévues par le droit national, les aides dont ils avaient irrégulièrement bénéficié. Les moyens soulevés devant la cour par la société requérante étaient ainsi inopérants. Il convient d'écarter ces moyens par ce motif qui doit être substitué à ceux retenus par l'arrêt attaqué. <br/>
<br/>
              En ce qui concerne la prescription : <br/>
<br/>
              6. La société requérante peut, en revanche, utilement se prévaloir de la prescription à l'encontre d'une mesure d'exécution de la décision de récupération de l'aide illégale. <br/>
<br/>
              7. En jugeant, pour écarter le moyen tiré de ce que la créance de l'Etat aurait été prescrite, que le régime de récupération des aides d'Etat est entièrement régi par le règlement du 22 mars 1999 notamment en matière de détermination du délai de prescription, lequel est de 10 ans, et en relevant qu'après avoir notamment été interrompue par la décision de la Commission européenne du 16 décembre 2003, la prescription avait de nouveau été interrompue par le recours en manquement introduit par la Commission européenne le 23 avril 2007, et qu'un nouveau délai de dix ans avait commencé à courir à compter de l'intervention de l'arrêt de la Cour de justice des Communautés européennes du 13 novembre 2008, la cour administrative d'appel, qui a suffisamment motivé son arrêt, n'a pas commis d'erreur de droit. Elle n'avait notamment pas à répondre au moyen tiré de ce que le délai de prescription de quatre ans prévu par le règlement (CE) du Conseil du 18 décembre 1995 relatif à la protection des intérêts financiers des Communautés européennes ne pouvait pas être prolongé sans méconnaissance du principe de proportionnalité, dès lors qu'elle a jugé que ce règlement n'était pas applicable. <br/>
<br/>
              8. Il résulte de tout ce qui précède que la société Luchard Industrie n'est pas fondée à demander l'annulation de l'arrêt attaqué et que ses conclusions tendant à l'application de l'article L.761-1 du code de justice administrative ne peuvent qu'être rejetées. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la Société Luchard Industrie est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la Société Luchard Industrie et au ministre de l'économie et des finances. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
