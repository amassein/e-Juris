<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031586622</ID>
<ANCIEN_ID>J1_L_2015_12_000001501324</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/58/66/CETATEXT000031586622.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>CAA de PARIS, 1ère chambre , 03/12/2015, 15PA01324, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-12-03</DATE_DEC>
<JURIDICTION>CAA de PARIS</JURIDICTION>
<NUMERO>15PA01324</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre </FORMATION>
<TYPE_REC>excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>Mme PELLISSIER</PRESIDENT>
<AVOCATS>CALVO PARDO</AVOCATS>
<RAPPORTEUR>Mme Nathalie  AMAT</RAPPORTEUR>
<COMMISSAIRE_GVT>M. ROMNICIANU</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
       Procédure contentieuse antérieure :<br/>
<br/>
       M. D...C...a demandé au tribunal administratif de Paris d'annuler l'arrêté du 7 août 2014 par lequel le préfet de police a refusé de lui délivrer un titre de séjour.<br/>
<br/>
       Par un jugement n° 1416865/2-3 du 29 janvier 2015, le tribunal administratif de Paris a, d'une part, annulé l'arrêté attaqué, d'autre part, enjoint au préfet de police de délivrer à M. C...un titre de séjour mention " vie privée et familiale ". <br/>
<br/>
       Procédure devant la Cour :<br/>
<br/>
       Par une requête enregistrée le 31 mars 2015, le préfet de police demande à la Cour :<br/>
<br/>
       1°) d'annuler le jugement n° 1416865/2-3 du 29 janvier 2015 du tribunal administratif de Paris ;<br/>
<br/>
       2°) de rejeter la demande présentée par M. C...devant le tribunal administratif de Paris.<br/>
       Il soutient que :<br/>
       - c'est à tort que les premiers juges ont annulé sa décision pour méconnaissance des dispositions de l'article L. 313-11 (7°) du code de l'entrée et du séjour des étrangers et du droit d'asile et de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
       - les autres moyens soulevés par M. C...en première instance ne sont pas fondés.<br/>
<br/>
<br/>
       Par un mémoire en défense enregistré le 25 juin 2015, M.C..., représenté par <br/>
Me A...B..., conclut au rejet de la requête et à ce que le versement de la somme de 1 500 euros soit mis à la charge de l'État sur le fondement de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
       Il soutient que les moyens soulevés par le préfet de police ne sont pas fondés.<br/>
<br/>
       Vu les autres pièces du dossier.<br/>
<br/>
       Vu :<br/>
       - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
       - le code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
       - le code de justice administrative.<br/>
<br/>
       Le président de la formation de jugement a dispensé le rapporteur public, sur sa proposition, de prononcer des conclusions à l'audience.<br/>
<br/>
       Les parties ont été régulièrement averties du jour de l'audience.<br/>
<br/>
       Le rapport de Mme Amat a été entendu au cours de l'audience publique.<br/>
<br/>
<br/>
       1. Considérant que M.C..., de nationalité chinoise, né le 17 mars 1968, entré en France en juillet 2002 selon ses déclarations, a sollicité le 6 novembre 2010 son admission au séjour dans le cadre des dispositions des articles L. 313-11 (7°) et L. 313-14 du code de l'entrée et du séjour des étrangers et du droit d'asile ; que par arrêté du 7 août 2014, le préfet de police a refusé de lui délivrer un titre de séjour ; que le préfet de police relève régulièrement appel du jugement du 29 janvier 2015 par lequel le tribunal administratif de Paris a annulé cet arrêté et lui a enjoint de délivrer une carte de séjour " vie privée et familiale " à M.C... ;<br/>
<br/>
       2. Considérant qu'aux termes de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Sauf si sa présence constitue une menace pour l'ordre public, la carte de séjour temporaire portant la mention " vie privée et familiale " est délivrée de plein droit : (...) 7° A l'étranger ne vivant pas en état de polygamie, qui n'entre pas dans les catégories précédentes ou dans celles qui ouvrent droit au regroupement familial, dont les liens personnels et familiaux en France, appréciés notamment au regard de leur intensité, de leur ancienneté et de leur stabilité, des conditions d'existence de l'intéressé, de son insertion dans la société française ainsi que de la nature de ses liens avec la famille restée dans le pays d'origine, sont tels que le refus d'autoriser son séjour porterait à son droit au respect de sa vie privée et familiale une atteinte disproportionnée au regard des motifs du refus, sans que la condition prévue à l'article L. 311-7 soit exigée. L'insertion de l'étranger dans la société française est évaluée en tenant compte notamment de sa connaissance des valeurs de la République (...) " ; qu'aux termes de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " 1. Toute personne a droit au respect de sa vie privée et familiale, de son domicile et de sa correspondance. 2. Il ne peut y avoir ingérence d'une autorité publique dans l'exercice de ce droit que pour autant que cette ingérence est prévue par la loi et qu'elle constitue une mesure qui, dans une société démocratique, est nécessaire à la sécurité nationale, à la sûreté publique, au bien-être économique du pays, à la défense de l'ordre et à la prévention des infractions pénales, à la protection de la santé ou de la morale, ou à la protection des droits et libertés d'autrui " ;<br/>
<br/>
       3. Considérant qu'il est constant que M. C...est entré en France au plus tard en juin 2003, date à laquelle il a sollicité l'asile, et qu'il y réside habituellement depuis cette date ; que la commission du titre de séjour, saisie pour avis par le préfet de police, a, le 28 janvier 2014, émis un avis favorable à la délivrance à l'intéressé du titre de séjour sollicité ; que M. C...est marié depuis 1992 à une compatriote, titulaire à la date de l'arrêté litigieux d'un titre de séjour mention " vie privée et familiale " dont elle a demandé et obtenu, contrairement à ce que soutient le préfet, le renouvellement ; que le fils ainé du couple, âgé de vingt-et-un ans, était également titulaire d'un titre de séjour et résidait et travaillait en France à la date de la décision litigieuse, la cadette, née en France et âgée de 7 ans, étant scolarisée au moins depuis 2012 ; que, par ailleurs, M. C...a travaillé entre juillet 2010 et novembre 2011 et justifie d'une promesse d'embauche ; qu'il a suivi depuis 2011 des cours de français au sein de différentes associations et a finalement obtenu en octobre 2013 le diplôme initial de langue française, niveau A1.1 du cadre européen commun de référence pour les langues ; qu'ainsi compte tenu de la durée et des conditions de séjour en France de M.C..., c'est à juste titre que les premiers juge ont estimé que la décision en litige portait une atteinte disproportionnée à son droit au respect de sa vie privé et familiale et méconnaissait dès lors les dispositions de l'article L. 313-11 (7°) du code de l'entrée et du séjour des étrangers et du droit d'asile et les stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
<br/>
       4. Considérant qu'il résulte de tout ce qui précède que le préfet de police n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Paris a annulé son arrêté en date du 7 août 2014 refusant à M. C...la délivrance d'un titre de séjour et lui a enjoint de lui délivrer un titre de séjour " vie privée et familiale " ;<br/>
<br/>
       5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement de la somme de 1 000 euros au titre des frais exposés par M. C... pour sa défense en appel ;<br/>
<br/>
<br/>
DÉCIDE :<br/>
<br/>
<br/>
Article 1er : La requête du préfet de police est rejetée.<br/>
Article 2 : L'État versera à M. D...C...une somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Le présent arrêt sera notifié à M. D...C...et au ministre de l'intérieur.<br/>
Copie en sera adressée au préfet de police.<br/>
<br/>
<br/>
Délibéré après l'audience du 19 novembre 2015, à laquelle siégeaient :<br/>
<br/>
<br/>
- Mme Pellissier, présidente de chambre,<br/>
- Mme Terrasse, président assesseur,<br/>
- Mme Amat, premier conseiller,<br/>
<br/>
<br/>
Lu en audience publique, le 3 décembre 2015.<br/>
Le rapporteur,<br/>
N. AMATLa présidente,<br/>
S. PELLISSIER<br/>
Le greffier,<br/>
F. TROUYET<br/>
La République mande et ordonne au ministre de l'intérieur en ce qui le concerne ou à tous huissiers de justice à ce requis en ce qui concerne les voies de droit commun contre les parties privées, de pourvoir à l'exécution de la présente décision.<br/>
''<br/>
''<br/>
''<br/>
''<br/>
2<br/>
N° 15PA01324<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">335-01-03 Étrangers. Séjour des étrangers. Refus de séjour.
</SCT>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
