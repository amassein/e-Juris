<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031649083</ID>
<ANCIEN_ID>JG_L_2015_12_000000361185</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/64/90/CETATEXT000031649083.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème SSJS, 18/12/2015, 361185, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-12-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>361185</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ORTSCHEIDT</AVOCATS>
<RAPPORTEUR>Mme Esther de Moustier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:361185.20151218</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              La société Fibelpar a demandé au tribunal administratif de Paris la restitution de la retenue à la source prélevée sur les dividendes qu'elle a reçus des sociétés Groupe Taittinger et Société du Louvre au titre des années 2001 à 2005. Par un jugement n° 0916519 du 14 octobre 2010, le tribunal administratif de Paris a constaté qu'il n'y avait pas lieu de statuer sur cette demande en tant qu'elle portait sur la retenue à la source opérée au titre des années 2004 et 2005, a fait droit à la demande de la société requérante en tant qu'elle portait sur la retenue à la source opérée au titre de l'année 2003 et par son article 4 a rejeté le surplus de sa demande. <br/>
<br/>
              Par un arrêt n° 10PA05942 du 21 mars 2012, la cour administrative d'appel de Paris a rejeté l'appel formé par la société Fibelpar contre l'article 4 de ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 19 juillet et 12 octobre 2012 et le 26 novembre 2015 au secrétariat du contentieux du Conseil d'Etat, la société Fibelpar demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 10 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le traité instituant la Communauté européenne ;<br/>
              - le traité sur le fonctionnement de l'Union européenne : <br/>
              - le code général des impôts et le livre des procédures fiscales ; <br/>
              - le code de justice administrative ;	<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Esther de Moustier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Ortscheidt, avocat de la société Fibelpar ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société de droit belge Fibelpar a perçu au cours des années 2001 et 2002 des dividendes que lui ont versés les sociétés Groupe Taittinger et Société du Louvre ; que ces dividendes ont fait l'objet d'une retenue à la source en application du 2 de l'article 119 bis du code général des impôts ; qu'elle demande l'annulation de l'arrêt du 21 mars 2012 par lequel la cour administrative d'appel de Versailles a rejeté sa requête d'appel tendant à l'annulation du jugement du 14 octobre 2010 du tribunal administratif de Paris en tant qu'il a rejeté sa demande tendant à la restitution de la retenue à la source prélevée au titre des années 2001 et 2002 ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 196-1 du livre des procédures fiscales : " Pour être recevables, les réclamations relatives aux impôts autres que les impôts directs locaux et les taxes annexes à ces impôts, doivent être présentées à l'administration au plus tard le 31 décembre de la deuxième année suivant celle, selon le cas : a) De la mise en recouvrement du rôle ou de la notification d'un avis de mise en recouvrement ; / (...) c) De la réalisation de l'événement qui motive la réclamation. / Toutefois, dans les cas suivants, les réclamations doivent être présentées au plus tard le 31 décembre de l'année suivant celle, selon le cas : (...) b) Au cours de laquelle les retenues à la source et les prélèvements ont été opérés s'il s'agit de contestations relatives à l'application de ces retenues (...) " ; qu'aux termes de l'article R. 421-5 du code de justice administrative : " Les délais de recours ne sont opposables qu'à la condition d'avoir été mentionnés, ainsi que les voies de recours, dans la notification de la décision " ; qu'il résulte de ces dispositions que l'absence de mention sur un avis d'imposition adressé par l'administration au contribuable du caractère obligatoire de la réclamation préalable, ainsi que des délais dans lesquels le contribuable doit exercer cette réclamation, fait obstacle à ce que les délais de réclamation lui soient opposables ; qu'en revanche, ces dispositions ne sont pas applicables lorsque le contribuable demande la restitution d'impositions versées par lui ou acquittées par un tiers sans qu'un titre d'imposition ait été émis ; que, par suite, en jugeant que la société Fibelpar, qui conteste une retenue à la source qui n'a pas donné lieu à l'émission d'un avis d'imposition, ne pouvait se prévaloir des dispositions de l'article R. 421-5 précité du code de justice administrative, la cour, qui a suffisamment motivé son arrêt sur ce point, n'a pas commis d'erreur de droit ; <br/>
<br/>
              3. Considérant, en outre, que l'impôt sur les dividendes est acquitté spontanément par les sociétés non-résidentes et les sociétés résidentes, sans émission préalable d'un titre d'imposition, qu'il s'agisse de l'impôt sur les sociétés pour les sociétés résidentes ou de la retenue à la source pour les sociétés non-résidentes ; que le moyen de la société requérante tiré de ce que la cour aurait méconnu les principes d'équivalence des garanties procédurales entre résidents et non résidents dans l'exercice de la libre circulation des capitaux garantie par l'article 56 du traité instituant la Communauté européenne, alors en vigueur, devenu article 63 du traité sur le fonctionnement de l'Union européenne ne peut dès lors qu'être écarté ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que le pourvoi de la société Fibelpar doit être rejeté, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>                       D E C I D E :<br/>
                                      --------------<br/>
<br/>
Article 1er : Le pourvoi de la société Fibelpar est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la société Fibelpar et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
