<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035803982</ID>
<ANCIEN_ID>JG_L_2017_10_000000400215</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/80/39/CETATEXT000035803982.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 11/10/2017, 400215, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-10-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>400215</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>Mme Marie-Gabrielle Merloz</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:400215.20171011</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Tecogalo - Tectos Falsos LDA a demandé au juge des référés du tribunal administratif de Dijon, d'une part, de mettre fin à la procédure de flagrance fiscale engagée sur le fondement de l'article L. 16-0 BA du livre des procédures fiscales par un procès-verbal de flagrance du 19 novembre 2015 et, d'autre part, d'ordonner la mainlevée de la saisie conservatoire de la créance de 35 678 euros dont elle a fait l'objet sur le fondement de l'article L. 252 B du livre des procédures fiscales.<br/>
<br/>
              Par une ordonnance n° 1503217 du 8 décembre 2015, le juge des référés du tribunal administratif de Dijon a rejeté sa demande.<br/>
<br/>
              Par un jugement n° 1503467 du 3 mars 2016, le tribunal administratif de Dijon a rejeté l'appel formé par la société Tecogalo - Tectos Falsos LDA contre cette ordonnance.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 30 mai et 30 août 2016 et le 8 juin 2017 au secrétariat du contentieux du Conseil d'Etat, la société Tecogalo - Tectos Falsos LDA demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au titre de la procédure de référé engagée, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention du 14 janvier 1971 entre la France et le Portugal tendant à éviter les doubles impositions et à établir des règles d'assistance administrative réciproque en matière d'impôts sur le revenu ;<br/>
              - le code général des impôts et le livre des procédures fiscales ; <br/>
              - le code de justice administrative ; <br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Gabrielle Merloz, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de la Société Tecogalo-tectos Falsos LDA ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés que la société Tecogalo - Tectos Falsos LDA, dont le siège social est au Portugal, exerce une activité de pose de faux plafonds et de fenêtres en aluminium. Elle a fait l'objet, à la suite d'une visite domiciliaire autorisée par ordonnance du 18 novembre 2015 du juge des libertés et de la détention du tribunal de grande instance de Dijon, d'un procès-verbal de flagrance fiscale établi le 19 novembre 2015, sur le fondement des dispositions de l'article L.16-0 BA du livre des procédures fiscales, en raison de l'exercice en France d'une activité non déclarée par son établissement stable situé dans les locaux de la société Tecogalo Construction, 15-17 rue Paul Cabet à Dijon. L'administration fiscale a reconstitué le chiffre d'affaires et les bénéfices de l'établissement stable en France, a soumis le résultat imposable ainsi calculé à l'impôt sur les sociétés au titre de l'exercice en cours, et a procédé à la saisie conservatoire de cette créance d'impôt. La société Tecogalo - Tectos Falsos LDA a demandé au juge des référés du tribunal administratif de Dijon de mettre fin à la procédure de flagrance fiscale et de prononcer la mainlevée des mesures de saisie conservatoire. Par une ordonnance du 9 décembre 2015, le juge des référés a rejeté ces demandes. Par un jugement du 3 mars 2016, le tribunal administratif de Dijon a rejeté l'appel de la société requérante contre cette ordonnance. <br/>
<br/>
              2. Aux termes de l'article L. 16-0 BA du livre des procédures fiscales : " V. Le juge du référé administratif mentionné à l'article L. 279, saisi dans un délai de huit jours à compter de la réception du procès-verbal de flagrance fiscale mentionné au I, met fin à la procédure s'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux sur la régularité de cette procédure. / (...) ". Aux termes de l'article 209 du code général des impôts : " I. Sous réserve des dispositions de la présente section, les bénéfices passibles de l'impôt sur les sociétés sont déterminés (...) en tenant compte uniquement des bénéfices réalisés dans les entreprises exploitées en France ainsi que ceux dont l'imposition est attribuée à la France par une convention internationale relative aux doubles impositions (...) ". Aux termes  du 1 de l'article 7 de la convention franco-portugaise du 14 janvier 1971 : " 1. Les bénéfices d'une entreprise d'un Etat contractant ne sont imposables que dans cet Etat, à moins que l'entreprise n'exerce son activité dans l'autre Etat contractant par l'intermédiaire d'un établissement stable qui y est situé. Si l'entreprise exerce son activité d'une telle façon, les bénéfices de l'entreprise sont imposables dans l'autre Etat, mais uniquement dans la mesure où ils sont imputables audit établissement stable. / (...) " / Aux termes de l'article 5 de la même convention : " 1. Au sens de la présente Convention, l'expression " établissement stable " désigne une installation fixe d'affaires où l'entreprise exerce tout ou partie de son activité. / 2. L'expression " établissement stable " comprend notamment : / a) Un siège de direction ; / b) Une succursale ; / c) Un bureau ; / (...) g) Un chantier de construction ou de montage dont la durée dépasse douze mois. / (...) ".<br/>
<br/>
              3. Le tribunal a relevé que la procédure de visite domiciliaire et la saisie de pièces effectuées par l'administration sur le fondement de l'article L. 16 B du livre des procédures fiscales avaient révélé que l'unique associé et gérant de la société requérante, M. Martins Da Costa, était domicilié en Franceet gérant de la SARL Tecogalo Construction auprès de laquelle la société Tecogalo - Tectos Falsos LDA avait réalisé en France, en qualité de sous-traitante, la totalité de son chiffre d'affaires en 2012 et 85 % de son chiffre d'affaires en 2013, que le timbre humide et des contrats de travail au nom de la société requérante, conclus avec des salariés portugais, étaient présents dans les locaux de la société Tecogalo Construction, que les deux sociétés avaient eu en mai 2015 des salariés communs et qu'il n'était pas justifié que M. MartinsDa Costase soit rendu régulièrement au Portugal, où se trouvait le siège statutaire de la société, ni que des actes de gestion aient été effectués dans ce pays. En déduisant de ces éléments que le moyen tiré de ce que la société requérante ne disposait pas d'un établissement stable en France au sens des stipulations de la convention franco-portugaise n'était pas de nature à créer un doute sérieux sur la légalité de la procédure de flagrance fiscale, le tribunal, qui a suffisamment motivé son jugement, n'a pas méconnu, eu égard à l'office du juge des référés, les dispositions de l'article L. 16-0 BA du livre des procédures fiscales.<br/>
<br/>
              4. En second lieu, le moyen tiré de ce que le tribunal a commis une erreur de droit en s'abstenant de vérifier l'existence de circonstances susceptibles de menacer le recouvrement d'une créance fiscale est nouveau en cassation et, par suite, inopérant.<br/>
<br/>
              5. Il résulte de tout ce qui précède que le pourvoi de la société Tecogalo-Tectos Falsos LDA doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société Tecogalo - Tectos Falsos LDA est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la société Tecogalo - Tectos Falsos LDA et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
