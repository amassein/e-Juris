<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033159726</ID>
<ANCIEN_ID>JG_L_2016_03_000000397780</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/15/97/CETATEXT000033159726.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 17/03/2016, 397780, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-03-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>397780</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2016:397780.20160317</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C...G...a demandé au juge des référés du tribunal administratif de Rennes, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'une part, de suspendre l'exécution de l'arrêté du 24 février 2016, par lequel le ministre de l'intérieur l'a astreint à résider dans la commune de Rennes (Ille-et-Vilaine), avec l'obligation de se présenter trois fois par jour à des horaires déterminés au commissariat de police de Rennes, tous les jours de la semaine, y compris les jours fériés ou chômés, et de demeurer, tous les jours, de 21 heures à 5 heures, dans les locaux où il réside. Par une ordonnance n° 1600989 du 4 mars 2016, le juge des référés du tribunal administratif de Rennes a rejeté sa demande.<br/>
              Par une requête, enregistrée le 9 mars 2016 au secrétariat du contentieux du Conseil d'Etat, M. G...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
              2°) de faire droit à sa demande de première instance.<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - la condition d'urgence est remplie ;<br/>
              - l'arrêté contesté porte une atteinte grave et manifestement illégale à sa liberté d'aller et venir, dès lors qu'il n'existe aucune raison sérieuse de penser que son comportement constitue une menace pour la sécurité et l'ordre publics ;<br/>
              - la mesure d'assignation à résidence n'est pas strictement nécessaire et proportionnée.<br/>
              Par un mémoire en défense enregistré le 11 mars 2016, le ministre de l'intérieur conclut au rejet de la requête. Il soutient que les moyens soulevés par le requérant ne sont pas fondés.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ;<br/>
              - la loi n° 55-385 du 3 avril 1955 ;<br/>
              - la loi n° 2015-1501 du 20 novembre 2015 ;<br/>
              - la loi n° 2016-162 du 19 février 2016 ; <br/>
              - le décret n° 2015-1475 du 14 novembre 2015 ;<br/>
              - le décret n° 2015-1476 du 14 novembre 2015 ;<br/>
              - le décret n° 2015-1478 du 14 novembre 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. G..., d'autre part, le ministre de l'intérieur ; <br/>
              Vu le procès-verbal de l'audience publique du lundi 14 mars 2016 à 11 heures 30 au cours de laquelle ont été entendus :<br/>
<br/>
              - Me Coudray, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. G... ;<br/>
<br/>
              - M.G... ;<br/>
<br/>
              - les représentants de M.G... ;<br/>
              - le représentant du ministre de l'intérieur ;<br/>
              et à l'issue de laquelle le juge des référés a différé la clôture de l'instruction jusqu'au 15 mars à 15 heures ;<br/>
<br/>
              Vu la mesure d'instruction supplémentaire par laquelle le juge des référés a demandé au ministre de l'intérieur de produire le procès-verbal des perquisitions menées dans les locaux de l'association Fajr Al Nida et, le cas échéant, au domicile de ses responsables ;<br/>
<br/>
              Vu le mémoire, enregistré le 14 mars 2016, par lequel M. G...produit des documents relatifs à la mosquée de Rennes ;<br/>
<br/>
              Vu le mémoire, enregistré le 15 mars 2016, par lequel le ministre de l'intérieur produit les procès-verbaux demandés et informe le Conseil d'Etat de la circonstance que M. E... D...est en fuite depuis le 13 mars au matin ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale " ;<br/>
<br/>
              2. Considérant qu'en application de la loi du 3 avril 1955, l'état d'urgence a été déclaré par le décret n° 2015-1475 du 14 novembre 2015, à compter du même jour à zéro heure, sur le territoire métropolitain, prorogé pour une durée de trois mois, à compter du 26 novembre 2015, par l'article 1er de la loi du 20 novembre 2015, puis prorogé à nouveau pour une durée de trois mois à compter du 26 février 2016 par l'article unique de la loi du 19 février 2016 ; qu'aux termes de l'article 6 de la loi du 3 avril 1955, dans sa rédaction issue de la loi du 20 novembre 2015 : " Le ministre de l'intérieur peut prononcer l'assignation à résidence, dans le lieu qu'il fixe, de toute personne résidant dans la zone fixée par le décret mentionné à l'article 2 et à l'égard de laquelle il existe des raisons sérieuses de penser que son comportement constitue une menace pour la sécurité et l'ordre publics dans les circonscriptions territoriales mentionnées au même article 2. (...) / La personne mentionnée au premier alinéa du présent article peut également être astreinte à demeurer dans le lieu d'habitation déterminé par le ministre de l'intérieur, pendant la plage horaire qu'il fixe, dans la limite de douze heures par vingt-quatre heures. / L'assignation à résidence doit permettre à ceux qui en sont l'objet de résider dans une agglomération ou à proximité immédiate d'une agglomération. (...) / L'autorité administrative devra prendre toutes dispositions pour assurer la subsistance des personnes astreintes à résidence ainsi que celle de leur famille. / Le ministre de l'intérieur peut prescrire à la personne assignée à résidence : / 1° L'obligation de se présenter périodiquement aux services de police ou aux unités de gendarmerie, selon une fréquence qu'il détermine dans la limite de trois présentations par jour, en précisant si cette obligation s'applique y compris les dimanches et jours fériés ou chômés (...) " ; qu'il résulte de l'article 1er du décret n° 2015-1476 du 14 novembre 2015, modifié par le décret n° 2015-1478 du même jour, que les mesures d'assignation à résidence sont applicables à l'ensemble du territoire métropolitain à compter du 15 novembre à minuit ;<br/>
<br/>
              3. Considérant qu'il résulte de l'instruction que M. G...a fait l'objet, en application du décret n° 2015-475 du 14 novembre 2015, d'un premier arrêté d'assignation à résidence, le 18 novembre 2015, abrogé et remplacé par un arrêté du 5 janvier 2016 à la suite de l'intervention de la loi du 20 novembre 2015 ; que la mesure d'assignation à résidence a été renouvelée, sur le fondement de la loi du 19 février 2016, par un arrêté du 24 février 2016 du ministre de l'intérieur qui astreint M. G...à résider sur le territoire de la commune de Rennes, lui fait obligation de se présenter trois fois par jour, à 7 heures, 12 heures 45 et 19 heures, au commissariat de police de Rennes, tous les jours de la semaine, y compris les jours fériés ou chômés et lui impose de demeurer tous les jours, de 21 heures à 5 heures, dans les locaux où il réside à Rennes ; que cet arrêté prévoit que M. G...ne peut se déplacer en dehors de son lieu d'assignation à résidence sans avoir obtenu préalablement une autorisation écrite établie par le préfet d'Ille-et-Vilaine ; que, par une ordonnance du 4 mars 2016, le juge des référés du tribunal administratif de Rennes, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, a rejeté la demande de M. G...tendant à la suspension de l'exécution de l'arrêté du 24 février 2016 ; que M. G...relève appel de cette ordonnance ; <br/>
<br/>
              En ce qui concerne la condition d'urgence :<br/>
<br/>
              4. Considérant qu'eu égard à son objet et à ses effets, notamment aux restrictions apportées à la liberté d'aller et venir, une décision prononçant l'assignation à résidence d'une personne, prise par l'autorité administrative en application de l'article 6 de la loi du 3 avril 1955, porte, en principe et par elle-même, sauf à ce que l'administration fasse valoir des circonstances particulières, une atteinte grave et immédiate à la situation de cette personne, de nature à créer une situation d'urgence justifiant que le juge administratif des référés, saisi sur le fondement de l'article L. 521-2 du code de justice administrative, puisse prononcer dans de très brefs délais, si les autres conditions posées par cet article sont remplies, une mesure provisoire et conservatoire de sauvegarde ; que le ministre de l'intérieur ne fait valoir aucune circonstance particulière conduisant à remettre en cause, au cas d'espèce, l'existence d'une situation d'urgence caractérisée de nature à justifier l'intervention du juge des référés dans les conditions d'urgence particulière prévues par l'article L. 521-2 du code de justice administrative ;<br/>
<br/>
              En ce qui concerne la condition tenant à l'atteinte grave et manifestement illégale à une liberté fondamentale :<br/>
<br/>
              5. Considérant qu'il appartient au juge des référés de s'assurer, en l'état de l'instruction devant lui, que l'autorité administrative, opérant la conciliation nécessaire entre le respect des libertés et la sauvegarde de l'ordre public, n'a pas porté d'atteinte grave et manifestement illégale à une liberté fondamentale, que ce soit dans son appréciation de la menace que constitue le comportement de l'intéressé, compte tenu de la situation ayant conduit à la déclaration de l'état d'urgence, ou dans la détermination des modalités de l'assignation à résidence ; que le juge des référés, s'il estime que les conditions définies à l'article L. 521-2 du code de justice administrative sont réunies, peut prendre toute mesure qu'il juge appropriée pour assurer la sauvegarde de la liberté fondamentale à laquelle il a été porté atteinte ;<br/>
<br/>
              6. Considérant qu'il résulte de l'instruction que le ministre de l'intérieur s'est fondé, pour prendre la décision d'assignation à résidence contestée, sur des éléments figurant dans des " notes blanches " des services de renseignement, versées au débat contradictoire ; qu'il ressort de ceux de ces éléments qui ont été repris dans les motifs de l'arrêté du 24 février 2016 que M. G...tient un rôle prépondérant au sein du milieu salafiste rennais depuis de nombreuses années ; qu'il est suspecté d'avoir traduit de nombreux communiqués de revendication d'actes terroristes, à compter de mai 2010, pour le compte du forum " Shomouk Al Islam ", qui constitue le vecteur principal de la propagande jihadiste et qui est un lieu d'échanges opérationnels pour l'organisation " Al Quaida "  ; qu'il a participé aux forums jihadistes " Al Ekhlaas " et " Al Fajola ", jusqu'à leur fermeture à l'automne 2015, sous l'alias " A... " ;  qu'il a incité plusieurs personnes, dont il apparaissait comme le mentor, à partir en Syrie et est en lien avec plusieurs personnes connues pour l'extrême radicalité de leurs convictions ; qu'il a tenu, en 2013, des propos légitimant les enlèvements d'enfants et les meurtres dans le cadre du jihad ; <br/>
<br/>
              7. Considérant que M.G..., s'il admet avoir consulté de nombreux sites d'information générale en raison de son intérêt pour la géopolitique, dément sa participation aux forums jihadistes mentionnés dans l'arrêté contesté et, plus généralement, conteste l'ensemble des mentions qu'il comporte, en particulier son rôle dans la radicalisation de certains membres de la communauté musulmane et la conception de l'islam qui lui est prêtée ; qu'il fait valoir que, professeur dans un lycée professionnel, il a toujours fait l'objet d'appréciations élogieuses, que son épouse est trésorière d'une association de femmes musulmanes qui participe à des actions de déradicalisation et contribue au dialogue interreligieux et que la mosquée qu'il fréquente habituellement, affiliée au Conseil français du culte musulman, promeut une vision tolérante et ouverte de la religion ; que, toutefois, s'il ne pouvait raisonnablement être attendu de M. G...qu'il apporte la preuve, difficile à établir, du caractère inexact de certains des faits qui lui sont imputés, il n'a produit, ni en première instance ni en appel, de documents relatifs à sa situation personnelle, témoignages ou attestations, susceptibles, notamment à travers un éclairage crédible sur ses convictions et sa personnalité, de remettre en cause utilement les éléments auxquels se réfère le ministre ;<br/>
<br/>
              8. Considérant, en outre, qu'il résulte des termes mêmes de l'arrêté contesté que l'assignation à résidence de M. G...a été prolongée en raison du fait que l'intéressé maintenait des contacts réguliers avec des individus radicaux favorables à " Daech " ; que, selon le ministre, M. G...est en lien étroit, depuis plusieurs années, avec MM. E...D...et B...F..., lesquels ont eux-mêmes fait l'objet, en raison de leur soutien au jihad et aux actions terroristes et de leurs projets de départ en Syrie, d'assignations à résidence renouvelées et non contestées ; que M. F...a fréquenté la même association, dénommée " Fajr Al Nida ", que M.G..., lequel a contribué à sa radicalisation et que M. D...est en fuite depuis le 13 mars ; que, face aux éléments précis et circonstanciés relatifs à leurs parcours croisés figurant dans les " notes blanches ", M. G...soutient simplement qu'antérieurement à leurs assignations respectives à résidence, leurs relations se limitaient à la fréquentation de la même mosquée ;<br/>
<br/>
              9. Considérant qu'au vu de l'ensemble de ces éléments, il n'apparaît pas, en l'état de l'instruction, qu'en renouvelant l'assignation à résidence de M.G..., et en la maintenant jusqu'à ce jour, au motif qu'il existe de sérieuses raisons de penser que le comportement de l'intéressé constitue une menace grave pour la sécurité et l'ordre publics, le ministre de l'intérieur ait porté une atteinte grave et manifestement illégale à sa liberté d'aller et venir ; qu'enfin, la circonstance que M. G...ait fait l'objet d'une suspension à titre conservatoire de la part du recteur de l'académie de Rennes est sans incidence sur l'appréciation du caractère proportionné des modalités de la mesure d'assignation à résidence, dont il n'est pas établi qu'elles auraient fait obstacle par elles-mêmes aux activités d'enseignement de l'intéressé ;<br/>
<br/>
              10. Considérant qu'il résulte de ce qui précède que M. G...n'est pas fondé à soutenir que c'est à tort que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Montpellier a rejeté sa demande ; que son appel ne peut donc être accueilli ;<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. G...est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. C...G...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
