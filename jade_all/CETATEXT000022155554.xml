<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000022155554</ID>
<ANCIEN_ID>JG_L_2010_04_000000328383</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/22/15/55/CETATEXT000022155554.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 16/04/2010, 328383</TITRE>
<DATE_DEC>2010-04-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>328383</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Martin</PRESIDENT>
<AVOCATS>FOUSSARD ; SPINOSI</AVOCATS>
<RAPPORTEUR>Mme Christine  Grenier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mlle Courrèges Anne</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2010:328383.20100416</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi et le nouveau mémoire, enregistrés les 28 mai et 4 août 2009 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. Jean-Luc A, demeurant ... ; M. A demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt du 27 mars 2009 par lequel la cour administrative d'appel de Nantes, à la demande de M. Jean-Charles B, a annulé le jugement du 2 mai 2008 du tribunal administratif de Caen ainsi que la décision du 18 février 2005 du préfet de la Manche enregistrant sa déclaration d'exploitation d'une officine de pharmacie à Cherbourg-Octeville ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M. B ;<br/>
<br/>
              3°) de mettre à la charge de M. B la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la santé publique ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Christine Grenier, chargée des fonctions de Maître des requêtes,  <br/>
<br/>
              - les observations de Me Foussard, avocat de M. A et de Me Spinosi, avocat de M. B, <br/>
<br/>
              - les conclusions de Mlle Anne Courrèges, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à Me Foussard, avocat de M. A et à Me Spinosi, avocat de M. B ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 18 février 2005, le préfet de la Manche a enregistré la déclaration préalable présentée par M. A en vue d'exploiter à compter du 1er mars 2005 l'officine que M. B, son propriétaire, s'était engagé à lui céder ; que, par acte authentique du 19 mars suivant, la vente de cette officine entre les deux parties a été régularisée avec effet au 1er mars ; qu'ainsi, à la date d'introduction devant le tribunal administratif de Caen, le 18 avril 2005, de sa demande tendant à l'annulation de l'arrêté préfectoral enregistrant la déclaration préalable d'exploitation de M. A, M. B n'était plus propriétaire de l'officine en cause et, compte tenu de la concordance entre les deux dates d'effet de la vente de celle-ci et du début d'exploitation par son nouveau propriétaire, ne justifiait pas d'un intérêt pour agir, alors même qu'il était le cédant de l'officine et que le fondement du litige concernait la date de transfert effectif de sa propriété ; qu'il suit de là qu'en jugeant recevable sa demande, la cour administrative d'appel de Nantes a commis une erreur de droit ; que, par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt doit être annulé ; <br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Considérant qu'aux termes de l'article L. 5125-16 du code de la santé publique, dans sa rédaction applicable en l'espèce : " Tout pharmacien se proposant d'exploiter une officine doit en faire la déclaration préalable à la préfecture où elle est enregistrée./ Doivent être jointes à cette déclaration les justifications propres à établir que son auteur remplit les conditions exigées par les articles L. 4221-1 et L. 5125-17./ Si l'une ou plusieurs de ces conditions font défaut, le représentant de l'Etat dans le département, après avis du conseil régional de l'ordre des pharmaciens et sur la proposition du directeur régional des affaires sanitaires et sociales, doit refuser l'enregistrement par une décision motivée./ En cas de réclamation, il est statué par le ministre chargé de la santé après avis du conseil régional (...) " ; que ces dispositions n'ont ni pour objet, ni pour effet d'instaurer un recours hiérarchique préalable obligatoire devant être formé avant tout recours contentieux ; qu'il résulte de ce qui précède que c'est à tort que le tribunal administratif de Caen s'est fondé sur la circonstance que M. B n'avait pas formé de recours hiérarchique préalable auprès du ministre de la santé pour estimer que sa demande était irrecevable ; <br/>
<br/>
              Considérant, toutefois, qu'ainsi qu'il a été dit plus haut,  M. B ne justifiait pas d'un intérêt lui donnant qualité pour agir contre la décision d'enregistrement du préfet de la Manche à la date d'introduction de son recours contentieux ; qu'il ne saurait utilement se prévaloir à cet égard de ce que cette décision l'aurait contraint à céder sa pharmacie à M. A ; qu'il suit de là que M. B n'est pas fondé à se plaindre de ce que, par le jugement attaqué, le tribunal administratif de Caen a rejeté sa demande ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de faire application de l'article L. 761-1 du code de justice administrative et de mettre à la charge de M. B le versement à M. A de la somme de 3 000 euros ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 27 mars 2009 de la cour administrative d'appel de Nantes est annulé.<br/>
Article 2 : L'appel présenté par M. B devant la cour administrative d'appel de Nantes est rejeté. <br/>
Article 3 : M. B versera à M. A une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : La présente décision sera notifiée à M. Jean-Luc A, à M. Jean-Charles B et à la ministre de la santé et des sports.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">55-03-04 PROFESSIONS, CHARGES ET OFFICES. CONDITIONS D'EXERCICE DES PROFESSIONS. PHARMACIENS. - CESSION D'UNE OFFICINE - ENREGISTREMENT PAR LE PRÉFET DE LA DÉCLARATION PRÉALABLE DU CESSIONNAIRE (ART. L. 5125-16 DU CODE DE LA SANTÉ PUBLIQUE, RÉDACTION ANTÉRIEURE À L'ORDONNANCE DU 26 AOÛT 2005) - 1) EFFET RÉTROACTIF DE LA VENTE À LA DATE DE DÉBUT D'EXPLOITATION PAR LE CESSIONNAIRE - CONSÉQUENCE - ABSENCE D'INTÉRÊT POUR AGIR DU CÉDANT CONTRE LA DÉCISION PRÉFECTORALE D'ENREGISTREMENT - 2) COMPÉTENCE DU MINISTRE POUR STATUER, LE CAS ÉCHÉANT, SUR LA RÉCLAMATION DIRIGÉE CONTRE LA DÉCISION PRÉFECTORALE - RECOURS ADMINISTRATIF PRÉALABLE OBLIGATOIRE - ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 55-03-04 Décision du préfet d'enregistrer la déclaration préalable d'un pharmacien se proposant d'exploiter une officine, en application de l'article L. 5125-16 du code de la santé publique, dans sa rédaction antérieure à l'ordonnance n° 2005-1040 du 26 août 2005.... ...1) Déclaration présentée par un pharmacien en vue d'exploiter à compter du 1er mars suivant une officine que son propriétaire s'était engagé à lui céder. Vente régularisée par acte authentique du 19 mars, avec effet au 1er mars. Ainsi, à la date d'introduction devant le tribunal administratif, le 18 avril, de sa demande tendant à l'annulation de l'arrêté préfectoral enregistrant la déclaration préalable d'exploitation, le cédant n'était plus propriétaire de l'officine en cause et, compte tenu de la concordance entre les deux dates d'effet de la vente de celle-ci et du début d'exploitation par son nouveau propriétaire, ne justifiait pas d'un intérêt pour agir, alors même que le fondement du litige concernait la date de transfert effectif de sa propriété.... ...2) Les dispositions de l'article L. 5125-16 du code de la santé publique, en prévoyant qu'en cas de réclamation, il est statué par le ministre chargé de la santé après avis du conseil régional  de l'ordre des pharmaciens, n'ont ni pour objet, ni pour effet d'instaurer un recours hiérarchique préalable obligatoire devant être formé avant tout recours contentieux contre la décision préfectorale.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., en ce qu'elle ne procède pas à la substitution de la décision du ministre à celle du préfet, Assemblée, 12 janvier 1968, Entente mutualiste de la Porte Océane, n° 64062, p. 40 ; s'agissant des décisions de refus de licence, Section, 22 décembre 1950, Sieur Martz, n° 4407, p. 633.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
