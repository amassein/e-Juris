<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033308570</ID>
<ANCIEN_ID>JG_L_2016_10_000000390421</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/30/85/CETATEXT000033308570.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 21/10/2016, 390421, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-10-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>390421</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>M. Timothée Paris</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:390421.20161021</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La SARL MT Trading a demandé au tribunal administratif de Nancy de prononcer la décharge des cotisations supplémentaires d'impôt sur les sociétés auxquelles elle a été assujettie au titre des années 2005, 2006 et 2007. Par un jugement n° 1101636 du 5 novembre 2013, le tribunal a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 14NC00040 du 26 mars 2015, la cour administrative d'appel de Nancy a rejeté l'appel de la société MT Trading contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 26 mai et 26 août 2015 au secrétariat du contentieux du Conseil d'Etat, la SARL MT Trading, devenue la société France élévateur, demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Timothée Paris, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Soltner, Texidor, Perier, avocat de la SARL MT Trading ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la SARL MT Trading a procédé à l'acquisition, les 14 et 15 mars 2006, de l'intégralité des titres de la SA société nouvelle AB services ; que 48 % de ces titres ont été acquis auprès de M. E... au prix unitaire de 66,09 euros, environ 50 % de ces titres auprès de M. B...C..., pour moitié, et auprès Mme D...C..., pour l'autre moitié, au prix unitaire de 300 euros, et le solde, soit environ 2 %, auprès de M. A...au prix unitaire de 24,81 euros ; qu'à l'issue de la vérification à laquelle elle a procédé de la comptabilité de la SARL MT Trading, l'administration fiscale a estimé que le prix d'acquisition, par celle-ci, des actions de la SA société nouvelle AB Services auprès de M. E...avait été délibérément minoré par les parties pour dissimuler une libéralité consentie par le vendeur à l'acquéreur ; qu'elle a, en conséquence, corrigé la valeur d'enregistrement de l'immobilisation à l'actif de la société MT Trading, pour y substituer sa valeur vénale, et a rehaussé le bénéfice imposable de la société à hauteur de la variation d'actif net résultant de cette correction ; que la SARL MT Trading a demandé au tribunal administratif de Nancy la décharge des cotisations supplémentaires d'impôt sur les sociétés et des pénalités correspondantes procédant de ces rectifications ; que par un jugement du 5 novembre 2013, le tribunal a rejeté cette demande ; que par un arrêt du  26 mars 2015, contre lequel la SARL MT Trading, devenue société France élévateur, se pourvoit en cassation, la cour administrative d'appel de Nancy a rejeté l'appel de la société contre ce jugement ;<br/>
<br/>
              2. Considérant que la valeur vénale réelle de titres non cotés en bourse sur un marché réglementé doit être appréciée compte tenu de tous les éléments dont l'ensemble permet d'obtenir un chiffre aussi voisin que possible de celui résultant du jeu de l'offre et de la demande à la date à laquelle la cession est intervenue ; que cette valeur doit être établie, en priorité, par référence à la valeur des autres titres de la société telle qu'elle ressort des transactions portant, à la même époque, sur ces titres dès lors que cette valeur ne résulte pas d'un prix de convenance ; que, toutefois, en l'absence de transactions intervenues dans des conditions équivalentes et portant sur les titres de la même société ou, à défaut, de sociétés similaires, l'administration peut légalement se fonder sur l'une des méthodes destinées à déterminer la valeur de l'actif par capitalisation des bénéfices ou d'une fraction du chiffre d'affaires annuel, ou sur la combinaison de plusieurs de ces méthodes ; qu'elle ne saurait toutefois procéder par combinaison entre la méthode par comparaison et l'une ou plusieurs des méthodes alternatives ; qu'il suit de là qu'en jugeant que l'évaluation faite par l'administration fiscale de la valeur vénale des titres de la SA société nouvelle AB services acquis par la société MT Trading de M. E...avait permis d'obtenir le résultat le plus proche possible de celui qui résulterait du jeu de l'offre et de la demande, alors que, selon les motifs de l'arrêt attaqué non argués de dénaturation, cette évaluation procédait d'une combinaison entre, d'une part, la valeur obtenue par la méthode des transactions comparables et, d'autre part, la moyenne arithmétique de la valeur obtenue par la méthode mathématique et la valeur de rentabilité des titres, la cour a entaché son arrêt d'erreur de droit ; que ce moyen, né de l'arrêt attaqué, est, sans qu'il soit besoin de statuer sur les autres moyens soulevés, de nature à conduire à son annulation ;<br/>
<br/>
              3. Considérant qu'il résulte de ce qui précède que la société France élévateur est fondée à demander l'annulation de l'arrêt attaqué ; qu'il y a lieu de mettre à la charge de l'Etat la somme de 3 000 euros au titre des frais exposés par cette société et non compris dans les dépens.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt du 26 mars 2015 de la cour administrative d'appel de Nancy est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nancy.<br/>
<br/>
Article 3 : L'Etat versera à la société France élévateur une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la société France élevateur et au ministre de l'économie et des finances. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
