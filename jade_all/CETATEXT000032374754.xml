<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032374754</ID>
<ANCIEN_ID>JG_L_2016_04_000000370648</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/37/47/CETATEXT000032374754.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 06/04/2016, 370648, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-04-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>370648</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>M. Etienne de Lageneste</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:370648.20160406</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le département de l'Aude a demandé au tribunal administratif de Montpellier d'annuler l'arrêté n° 2009-11-2449 du 1er décembre 2009 par lequel le préfet de l'Aude a classé les digues de protection contre les inondations en rive gauche de l'Aude sur le territoire communal de Coursan en classe C conformément aux dispositions du décret du 11 décembre 2007 et prescrit les mesures nécessaires à leur mise en conformité avec les dispositions du code de l'environnement et d'annuler la décision du 17 mars 2010 par laquelle le préfet de l'Aude a rejeté le recours gracieux de son président tendant au retrait de l'arrêté.<br/>
<br/>
              Par un jugement n° 1001444 du 23 septembre 2011, le tribunal administratif a annulé l'arrêté du 1er décembre 2009 du préfet de l'Aude en tant qu'il attribue la propriété ou la gestion légale des tronçons 18 et 19 de la rive gauche de l'Aude au département de l'Aude, annulé dans cette mesure l'arrêté du préfet de l'Aude en date du 1er décembre 2009 et la décision du préfet en date du 17 mars 2010 rejetant le recours gracieux du département et rejeté les conclusions du département de l'Aude relatives aux tronçons 10 à 13 de la digue en rive gauche de l'Aude sur le territoire de la commune de Coursan.<br/>
<br/>
              Par un arrêt n° 11MA04426 du 28 mai 2013, la cour administrative d'appel de Marseille a annulé le jugement du tribunal administratif en tant qu'il a rejeté les conclusions du département de l'Aude relatives aux tronçons 10 à 13 de la digue située rive gauche de l'Aude sur le territoire de la commune de Coursan, annulé dans cette mesure l'arrêté du préfet de l'Aude en date du 1er décembre 2009 et la décision du préfet de l'Aude en date du 17 mars 2010 rejetant le recours gracieux du département de l'Aude.<br/>
<br/>
              Par un pourvoi, enregistré le 29 juillet 2013 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'écologie, du développement durable et de l'énergie demande au Conseil d'Etat d'annuler cet arrêt.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'environnement ;<br/>
              - le code général de la propriété des personnes publiques ;<br/>
              - le code de justice administrative ;		<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Etienne de Lageneste, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat du conseil général de l'Aude ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le préfet de l'Aude, par un arrêté du 1er décembre 2009 classant en catégorie C les digues sur la rive de l'Aude sur le territoire de la commune de Coursan et prescrivant des mesures pour les rendre conformes au code de l'environnement, a mentionné, parmi les propriétaires concernés, le département de l'Aude pour les tronçons 10 à 13, 18 et 19, puis, le 17 mars 2010, a rejeté la demande du département en date du 25 janvier 2010 tendant au retrait de cet arrêté en tant qu'il était regardé comme le propriétaire de ces tronçons ; que, le 23 septembre 2011, le tribunal administratif de Montpellier a jugé que ce département était le propriétaire des tronçons 10 à 13 et n'a annulé l'arrêté et la décision de rejet qu'en tant qu'ils concernaient les tronçons 18 et 19 ; que le ministre de l'écologie, du développement durable et de l'énergie se pourvoit en cassation contre l'arrêt du 28 mai 2013 par lequel la cour administrative d'appel de Marseille a annulé le jugement, cet arrêté et cette décision de rejet en tant qu'ils portent sur les tronçons 10 à 13 ;<br/>
<br/>
              2. Considérant que la cour administrative d'appel de Marseille a relevé que la propriété des tronçons de digue 10 à 13 n'était établie par aucun titre ou document et que leur construction remontait au plus tard au XIXème siècle ; qu'elle a estimé qu'ils étaient nécessaires au soutien d'une route départementale et qu'ils constituaient ainsi l'accessoire de cette route ; qu'alors qu'il n'était pas contesté par le département que cette partie de la voie avait été incorporée à la voirie départementale, la cour a commis une erreur de droit en ne déduisant pas de ces constatations que les tronçons de digue devaient être présumés comme appartenant au domaine public du département, alors même que la digue a été édifiée dans le but de protéger les populations contre les crues de l'Aude ; que dès lors et sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, le ministre est fondé, pour ce motif, à demander l'annulation de l'arrêt qu'il attaque ;<br/>
<br/>
              3. Considérant que, dans les circonstances de l'espèce, il y a lieu de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              4. Considérant, en premier lieu, que la qualification de pétitionnaire figurant dans l'arrêté du 1er décembre 2009, qui a été inexactement attribuée au département de l'Aude, est sans influence sur la légalité de cet arrêté, qui a pour objet d'édicter des prescriptions tendant à la mise en conformité de la digue avec le code de l'environnement, sans qu'il soit nécessaire de faire une demande préalable en ce sens ;<br/>
<br/>
              5. Considérant, en deuxième lieu, qu'il ressort des pièces du dossier que les tronçons de digue n° 10 à 13 sont physiquement et fonctionnellement indissociables de la voie départementale qu'ils soutiennent et, par suite, en constituent l'accessoire indispensable ;<br/>
<br/>
              6. Considérant, enfin, que si le département soutient que l'Etat ne lui a pas transféré la propriété de ces tronçons lors du transfert de la propriété de la route départementale, il admet s'être abstenu de conclure une convention de remise de ces immeubles ; qu'il ne saurait utilement soutenir qu'il n'a pas la charge des cours d'eau domaniaux, dès lors qu'ainsi qu'il a été dit, la digue constitue le soutien de la voie départementale ; qu'est sans influence sur la qualité de propriétaire de cette partie de la digue la circonstance que son entretien a été assuré par les communes de Cuxac-d'Aude et de Coursan avec l'aval de l'Etat ; que si une digue construite par l'Etat sur le fondement de la loi du 28 mai 1858 constitue un ouvrage public dont l'Etat est propriétaire, il ne ressort pas des pièces du dossier que la digue ait été construite sur ce fondement législatif ; que si le département fait valoir que des constructions privées ont été édifiées sur le tronçon n° 13, il ne produit pas les titres de propriété ou les éléments cadastraux correspondants qui établiraient que les tronçons de digue appartiendraient en partie à des personnes privées et ne soutient pas que leur propriété ferait l'objet d'une revendication ;<br/>
<br/>
              7. Considérant que, dès lors, c'est à bon droit que le préfet de l'Aude a regardé le département de l'Aude comme le propriétaire des tronçons 10 à 13, sans que puisse y faire obstacle la circonstance que la digue a été construite pour protéger les zones environnantes des crues de l'Aude ; qu'il résulte de ce qui précède que le département n'est pas fondé à soutenir que c'est à tort que, par le jugement qu'il attaque, le tribunal administratif de Montpellier a rejeté dans cette mesure sa demande ;<br/>
<br/>
              8. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce soit mis à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante, le versement des sommes que demande le département ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions de la commune de Coursan présentées au titre de ce même article ;<br/>
<br/>
<br/>
<br/>                    D E C I D E :<br/>
                                   --------------<br/>
<br/>
Article 1er : L'arrêt du 28 mai 2013 de la cour administrative d'appel de Marseille est annulé.<br/>
<br/>
Article 2 : La requête du département de l'Aude présentée devant la cour administrative d'appel de Marseille et ses conclusions présentées devant le Conseil d'Etat au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 3 : Les conclusions présentées devant la cour administrative d'appel de Marseille par la commune de Coursan au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la ministre de l'écologie, du développement durable et de l'énergie, au département de l'Aude et à la commune de Coursan.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
