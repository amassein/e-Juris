<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042175683</ID>
<ANCIEN_ID>JG_L_2020_07_000000429517</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/17/56/CETATEXT000042175683.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 29/07/2020, 429517, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-07-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>429517</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Pierre Boussaroque</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie Sirinelli</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:429517.20200729</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 8 avril 2019 et 2 juin 2020 au secrétariat du contentieux du Conseil d'Etat, la Fédération générale des transports et de l'environnement - Confédération française démocratique du travail (FGTE-CFDT) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite par laquelle le Premier ministre a rejeté sa demande du 1er février 2019 tendant à la modification de l'article R. 4222-10 du code du travail ;<br/>
<br/>
              2°) d'enjoindre au Premier ministre de réviser les seuils des concentrations moyennes en poussières totales ou alvéolaires de l'atmosphère inhalées par les travailleurs dans les locaux à pollution spécifique dans le délai de six mois à compter de la notification de la décision à intervenir ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Elle soutient que les dispositions de l'article R. 4222-10 du code du travail sont inadaptées et que la carence de l'Etat à les modifier est désormais devenue illégale. <br/>
<br/>
              Par un mémoire en défense, enregistré le 16 octobre 2019, la ministre du travail conclut au rejet de la requête. Elle soutient que le moyen soulevé n'est pas fondé.<br/>
<br/>
              La requête a été communiquée au Premier ministre, qui n'a pas produit de mémoire.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - le décret n° 84-1093 du 7 décembre 1984 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pierre Boussaroque, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Marie Sirinelli, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. En raison de la permanence de l'acte réglementaire, la légalité des règles qu'il fixe, la compétence de son auteur et l'existence d'un détournement de pouvoir doivent pouvoir être mises en cause à tout moment, de telle sorte que puissent toujours être sanctionnées les atteintes illégales que cet acte est susceptible de porter à l'ordre juridique. Cette contestation peut prendre la forme d'un recours pour excès de pouvoir dirigé contre la décision refusant d'abroger l'acte réglementaire, comme l'exprime l'article L. 243-2 du code des relations entre le public et l'administration aux termes duquel : " L'administration est tenue d'abroger expressément un acte réglementaire illégal ou dépourvu d'objet, que cette situation existe depuis son édiction ou qu'elle résulte de circonstances de droit ou de fait postérieures, sauf à ce que l'illégalité ait cessé (...) ". En outre, l'effet utile de l'annulation pour excès de pouvoir du refus d'abroger un acte réglementaire illégal réside dans l'obligation, que le juge peut prescrire d'office en vertu des dispositions de l'article L. 911-1 du code de justice administrative, pour l'autorité compétente, de procéder à l'abrogation de cet acte afin que cessent les atteintes illégales que son maintien en vigueur porte à l'ordre juridique. Il s'ensuit que lorsqu'il est saisi de conclusions aux fins d'annulation du refus d'abroger un acte réglementaire, le juge de l'excès de pouvoir est conduit à apprécier la légalité de cet acte au regard des règles applicables et des circonstances qui prévalent à la date de sa décision. Il en va de même lorsque l'autorité compétente est saisie d'une demande tendant à la réformation d'un règlement illégal, et qu'elle est, par conséquent, tenue d'y substituer des dispositions de nature à mettre fin à cette illégalité.<br/>
<br/>
              2. L'article R. 4222-10 du code du travail, dont la rédaction, issue du décret du 7 décembre 1984 modifiant les sections I et VII du chapitre II du titre III du livre II du code du travail (2ème partie), n'a, depuis lors, jamais été modifiée, prévoit que : " Dans les locaux à pollution spécifique, les concentrations moyennes en poussières totales et alvéolaires de l'atmosphère inhalée par un travailleur, évaluées sur une période de huit heures, ne doivent pas dépasser respectivement 10 et 5 milligrammes par mètre cube d'air ". La Fédération générale des transports et de l'équipement - Confédération française démocratique du travail a demandé au Premier ministre, par une lettre reçue par ce dernier le 6 mai 2015, de modifier ces dispositions pour fixer des valeurs moins élevées. Par une décision du 31 mars 2017, le Conseil d'Etat, statuant au contentieux, a relevé que le ministre chargé du travail ne contestait pas le caractère inadapté des normes applicables aux poussières alvéolaires en l'état de l'information disponible mais a rejeté la requête de la fédération requérante tendant à l'annulation du refus implicite opposé à sa demande au motif qu'il n'était pas établi, compte tenu notamment de la technicité de la matière, que l'administration, qui n'avait pas refusé d'engager la révision des dispositions de l'article R. 4222-10 du code du travail, était déjà en mesure de fixer de nouvelles valeurs limites de référence. La fédération requérante a de nouveau demandé au Premier ministre, par un courrier qu'il a reçu le 4 février 2019, la modification des dispositions de l'article R. 4222-10 du code du travail. Elle demande l'annulation pour excès de pouvoir du refus implicite opposé par le Premier ministre à cette nouvelle demande. <br/>
<br/>
              3. Aux termes de l'article L. 4121-1 du code du travail : " L'employeur prend les mesures nécessaires pour assurer la sécurité et protéger la santé physique et mentale des travailleurs (...) ". Si, en application de ces dispositions, l'employeur a l'obligation générale d'assurer la sécurité et la protection de la santé des travailleurs placés sous son autorité, il incombe aux autorités publiques chargées de la prévention des risques professionnels de se tenir informées des dangers que peuvent courir les travailleurs dans le cadre de leur activité professionnelle et d'arrêter, en l'état des connaissances scientifiques et des informations disponibles, au besoin à l'aide d'études ou d'enquêtes complémentaires, les mesures les plus appropriées pour limiter et, si possible, éliminer ces dangers. Il en va ainsi, en particulier, en ce qui concerne les lieux de travail, pour lesquels l'article L. 4221-1 du même code prévoit que : " Les établissements et locaux de travail sont aménagés de manière à ce que leur utilisation garantisse la sécurité des travailleurs. / Ils sont tenus dans un état constant de propreté et présentent les conditions d'hygiène et de salubrité propres à assurer la santé des intéressés (...) ".<br/>
<br/>
              4. Le ministre chargé du travail ne conteste pas que les seuils actuellement fixés par l'article R. 4222-10 du code du travail, tant pour les poussières totales que pour les poussières alvéolaires, ne sont plus adaptés pour assurer la sécurité et protéger la santé des travailleurs. <br/>
<br/>
              5. Il ressort des pièces du dossier que les autorités publiques chargées de la prévention des risques professionnels sont informées, notamment depuis un avis rendu le 8 juin 2015 par l'Agence nationale de sécurité sanitaire de l'alimentation, de l'environnement et du travail (ANSES) à propos de la pollution chimique de l'air des enceintes de transports ferroviaires souterrains, de ce que l'état des connaissances scientifiques et des informations disponibles rend nécessaire la révision des valeurs limites de référence fixées à l'article R. 4222-10 du code du travail, en particulier pour les poussières alvéolaires. Compte tenu notamment de la technicité de la matière, l'administration a engagé un processus de révision de l'ensemble des valeurs fixées par ces dispositions en saisissant à cette fin l'ANSES le 18 novembre 2015. Il ressort des pièces du dossier que le processus d'expertise est à ce jour achevé, l'agence ayant rendu le 19 novembre 2019 son avis, qui comporte la proposition de valeurs limites d'exposition professionnelle et fait suite à une expertise collective soumise à une consultation publique du 13 mai au 13 juillet 2019. Il en résulte qu'à la date de la présente décision, l'administration est en mesure de fixer de nouvelles valeurs limites de référence, ainsi qu'elle a elle-même indiqué avoir l'intention de le faire " sans délai " dès l'intervention de cet avis. Par suite, la fédération requérante est fondée à demander l'annulation de la décision qu'elle attaque, refusant de modifier les dispositions de l'article R. 4222-10 du code du travail. <br/>
<br/>
              6. Cette annulation implique nécessairement la modification de ces dispositions réglementaires pour fixer de nouveaux seuils de concentrations moyennes en poussières totales et alvéolaires de l'atmosphère inhalée par les travailleurs dans les locaux à pollution spécifique, de nature à protéger la santé de ces travailleurs. Il y a lieu pour le Conseil d'Etat d'ordonner cette mesure, qui devra intervenir dans un délai de six mois à compter de la notification de la présente décision. <br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3 000 euros à verser à la fédération requérante au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La décision par laquelle le Premier ministre a implicitement rejeté la demande dont la Fédération générale des transports et de l'environnement - Confédération française démocratique du travail l'a saisi le 4 février 2019 est annulée.<br/>
Article 2 : Il est enjoint au Premier ministre de modifier les dispositions de l'article R. 4222-10 du code du travail en fixant des concentrations moyennes en poussières totales et alvéolaires de l'atmosphère inhalée par les travailleurs dans les locaux à pollution spécifique de nature à protéger la santé de ces travailleurs dans le délai de six mois à compter de la notification de la présente décision.<br/>
Article 3 : L'Etat versera à la Fédération générale des transports et de  l'environnement - Confédération française démocratique du travail une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : La présente décision sera notifiée à la Fédération générale des transports et de l'environnement - Confédération française démocratique du travail, au Premier ministre et à la ministre du travail, de l'emploi et de l'insertion.<br/>
Copie en sera adressée à la section du rapport et des études.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
