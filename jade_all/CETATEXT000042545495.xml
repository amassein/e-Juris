<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042545495</ID>
<ANCIEN_ID>JG_L_2020_11_000000446301</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/54/54/CETATEXT000042545495.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 18/11/2020, 446301, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-11-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>446301</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:446301.20201118</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Sous le n° 446301, par une requête, enregistrée le 10 novembre 2020 au secrétariat du contentieux du Conseil d'Etat, l'association Priartem et l'association Agir pour l'environnement demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution de la décision n° 2020-0329 du 31 mars 2020 l'Autorité de régulation des communications électroniques, des postes et de la distribution de la presse (ARCEP) relative au compte rendu de l'instruction des dossiers de candidatures reçues et au résultat de la phase d'attribution des blocs de 50 MHz dans le cadre de la procédure d'attribution d'autorisations d'utilisation de fréquences dans la bande 3,4 - 3,8 GHz en France métropolitaine pour établir et exploiter un réseau radioélectrique mobile ouvert au public ; <br/>
<br/>
              2°) d'ordonner la suspension de la décision n° 2020-1160 du 20 octobre 2020 de l'ARCEP relative au compte rendu et au résultat de la procédure d'attribution d'autorisations d'utilisation de fréquences dans la bande 3,4 - 3,8 GHz en France métropolitaine pour établir et exploiter un réseau radioélectrique mobile ouvert au public ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
              Elles soutiennent que : <br/>
              - la condition d'urgence est satisfaite dès lors que le déploiement effectif de la 5G en France est imminent, la mise en service étant prévue début décembre 2020 au plus tard, alors qu'aucune évaluation des effets environnementaux et sanitaires de la 5G n'a été réalisée ; <br/>
              - il existe un doute sérieux quant à la légalité des décisions contestées ; <br/>
              - les décisions contestées sont entachées d'irrégularité dès lors qu'elles n'ont pas été signées avant d'être publiées ; <br/>
              - elles méconnaissent les dispositions de l'article 7 de la Charte de l'environnement et de l'article L. 123-19-1 du code de l'environnement dès lors qu'elles n'ont pas fait l'objet d'une consultation du public ; <br/>
              - elles méconnaissent les dispositions de la directive n° 2011/42/CE du 27 juin 2001 relative à l'évaluation des incidences de certains plans et programmes sur l'environnement dès lors qu'aucune évaluation environnementale n'a été réalisée en amont afin d'apprécier l'impact environnemental de la 5G dans sa globalité ;<br/>
              - les textes relatifs au déploiement de la 5G peuvent être qualifiés de plans et de programmes dont la mise en oeuvre nécessite une évaluation environnementale préalable ;  <br/>
              - les décisions contestées méconnaissent les divers engagements écologiques de la France dès lors, d'une part, que les effets de la 5G sont incertains et, d'autre part, qu'ils pourraient s'avérer désastreux, entraînant une plus grande exposition des individus aux ondes, un épuisement des ressources naturelles, un accroissement de la consommation énergétique des consommateurs et opérateurs, une augmentation de l'empreinte carbone et des émissions de gaz à effet de serre ; <br/>
              - le déploiement de la 5G méconnaît, en premier lieu, l'exigence de sobriété et d'efficacité énergétique ainsi que l'exigence d'utilisation rationnelle de l'énergie, prévues par les articles L. 100-1 du code de l'énergie et L. 220-1 du code de l'environnement, en deuxième lieu, le principe de protection de la santé humaine prévu par les directives européennes n° 2002/21/CE du 7 mars 2002 et n° 2009/140/CE du 25 novembre 2009 ainsi que les articles L. 32, L. 32-1 et L. 42 du code des postes et des communications électroniques, et, en dernier lieu, le principe de précaution prévu par l'article 5 de la Charte de l'environnement et l'article 191 du Traité sur le fonctionnement de l'Union européenne ;  <br/>
              - l'interruption du déploiement de la 5G en France est nécessaire dès lors que cette mise en place n'est exigée par aucun intérêt supérieur, qu'elle est coûteuse, qu'elle emporte des risques pour la santé des individus et de l'environnement, qu'il n'est pas démontré que ses avantages surpassent ses inconvénients, et, enfin, qu'elle n'est pas justifiée par le respect du principe de neutralité technologique, prévu par l'article 9 de la directive n° 2002/21/CE dans sa rédaction issue de la directive n° 2009/140/CE ainsi que par l'article L. 32-1 du code des postes et des communications électroniques.<br/>
<br/>
<br/>
<br/>
              II. Sous le n° 446495, par une requête, enregistrée le 16 novembre 2020, l'association Priartem et l'association Agir pour l'environnement demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative : <br/>
<br/>
              1°) d'ordonner la suspension de l'exécution des décisions n°s 2020-1254, 2020-1255, 2020-1256 et 2020-1257 du 12 novembre 2020 de l'ARCEP autorisant, respectivement, la société Bouygues Telecom, la société Free Mobile, la société Orange et la société SFR à utiliser les fréquences dans la balance 3,4 - 3,8 GHz en France métropolitaine pour établir et exploiter un réseau radioélectrique mobile ouvert au public ; <br/>
<br/>
              2°) à titre subsidiaire, d'une part, de transmettre à la Cour de justice de l'Union européenne une question préjudicielle portant sur la définition de la notion de plan ou programme élaboré dans le secteur des télécommunications au sens de l'article 3 de la directive n° 2001/42/CE du 27 juin 2001 relative à l'évaluation des incidences de certains plans et programmes sur l'environnement, et, d'autre part, d'ordonner la suspension de l'exécution des décisions n°s 2020-1254, 2020-1255, 2020-1256 et 2020-1257 du 12 novembre 2020 de l'ARCEP ; <br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Elles soutiennent que : <br/>
              - la condition d'urgence est satisfaite dès lors que le déploiement effectif de la 5G sur la bande des 3,5 GHz en France est imminent, prévu à partir du 18 novembre 2020 par les décisions contestées, alors qu'aucun intérêt supérieur n'exige un déploiement immédiat ; <br/>
              - il existe un doute sérieux quant à la légalité des décisions contestées ; <br/>
              - les décisions contestées sont entachées d'irrégularité dès lors qu'elles n'ont pas été signées avant d'être publiées ; <br/>
              - les décisions contestées méconnaissent les dispositions de l'article 7 de la Charte de l'environnement et de l'article L. 123-19-1 du code de l'environnement dès lors qu'elles n'ont pas fait l'objet d'une consultation du public ;<br/>
              - l'arrêté du 30 décembre 2019, sur le fondement duquel ont été prises les décisions contestées, constitue un plan ou programme dont la mise en oeuvre nécessite une évaluation environnementale préalable ; <br/>
              - les décisions contestées constituent elles-mêmes un plan ou un programme dont la mise en oeuvre nécessite une évaluation environnementale préalable ; <br/>
              - l'arrêté du 30 décembre 2019 et les décisions contestées méconnaissent les dispositions de la directive n° 2001/42/CE et l'article L. 122-4 du code de l'environnement dès lors qu'elles n'ont pas été précédées d'une évaluation environnementale, alors qu'elles ont d'importantes incidences sur l'environnement, à savoir de potentielles atteintes sanitaires, un épuisement des ressources naturelles, un accroissement de la consommation énergétique et une augmentation de l'empreinte carbone ; <br/>
              - la demande de transmission d'une question préjudicielle à la Cour de justice de l'Union européenne portant sur le point de savoir si un arrêté fixant le cadre pour le déploiement de la 5G en France dans la bande des 3.5 GHz, tel que l'arrêté du 30 décembre 2019, et des décisions autorisant un opérateur à utiliser des fréquences dans un spectre donné en France métropolitaine pour établir et exploiter un réseau radioélectrique mobile ouvert au public, dans la mesure où, par le biais de leurs annexes, elles définissent un cadre pour le déploiement de la 5G dans la bande des 3.5 GHz en France, doivent être considérés comme un plan ou programme élaboré dans le secteur des télécommunications au sens de l'article 3 de la directive n° 2001/42/CE du 27 juin 2001 relative à l'évaluation des incidences de certains plans et programmes sur l'environnement est légitime ; <br/>
              - le déploiement de la 5G contrevient, d'une part, à l'exigence de sobriété et d'efficacité énergétique, à l'exigence d'une utilisation rationnelle de l'énergie ainsi qu'à l'exigence de lutte contre les émissions de gaz à effet de serre ;<br/>
              - le déploiement de la 5G méconnaît, en premier lieu, l'exigence de sobriété et d'efficacité énergétique ainsi que l'exigence d'utilisation rationnelle de l'énergie, prévues par les articles L. 100-1 du code de l'énergie et L. 220-1 du code de l'environnement, en deuxième lieu, le principe de protection de la santé humaine prévu par les directives européennes n° 2002/21/CE du 7 mars 2002 et n° 2009/140/CE du 25 novembre 2009 ainsi que les articles L. 32, L. 32-1 et L. 42 du code des postes et des communications électroniques, et, en dernier lieu, le principe de précaution prévu par l'article 5 de la Charte de l'environnement et l'article 191 du Traité sur le fonctionnement de l'Union européenne ;  <br/>
              - l'interruption du déploiement de la 5G en France est nécessaire dès lors que cette mise en place n'est exigée par aucun intérêt supérieur, qu'elle est coûteuse, qu'elle emporte des risques pour la santé des individus et de l'environnement, qu'il n'est pas démontré que ses avantages surpassent ses inconvénients, et, enfin, qu'elle n'est pas justifiée par le respect du principe de neutralité technologique, prévu par l'article 9 de la directive n° 2002/21/CE dans sa rédaction issue de la directive n° 2009/140/CE ainsi que par l'article L. 32-1 du code des postes et des communications électroniques.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, et notamment son Préambule ;<br/>
              - le Traité sur le fonctionnement de l'Union européenne ;<br/>
              - la directive n° 2011/42/CE du 27 juin 2001 ;<br/>
              - la directive n° 2002/21/CE du 7 mars 2002 ;<br/>
              - la directive n° 2009/140/CE du 25 novembre 2009 ;<br/>
              - le code de l'énergie ; <br/>
              - le code de l'environnement ; <br/>
              - le code des postes et des communications électroniques ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. Un arrêté du 30 novembre 2019 du ministre de l'économie et des finances a défini les conditions d'attribution de fréquences dans la bande des 3,5 GHz en vue du déploiement des réseaux supports de l'internet dit de cinquième génération (" 5G "). Un décret du 31 décembre 2019 a ensuite précisé le régime des redevances pour l'utilisation des fréquences concernées. Ces deux actes réglementaires ont fait l'objet d'un recours en annulation par les associations Priartem et Agir pour l'environnement. Les demandes de suspension formées à leur encontre devant le juge des référés du Conseil d'Etat ont été rejetées pour défaut d'urgence par ordonnance du 5 mars 2020. <br/>
<br/>
              3. Aux termes de l'article L. 42-2 du code des postes et des communications électroniques : " La sélection des titulaires de ces autorisations se fait par appel à candidatures sur des critères portant sur les conditions d'utilisation mentionnées au II de l'article L. 42-1 ou sur la contribution à la réalisation des objectifs mentionnés à l'article L. 32-1, ou par une procédure d'enchères dans le respect de ces objectifs [...] / L'Autorité de régulation des communications électroniques, des postes et de la distribution de la presse conduit la procédure de sélection et assigne les fréquences correspondantes. "<br/>
<br/>
              4. Sur ce fondement, le 31 mars 2020, l'ARCEP a rendu public le compte-rendu de l'instruction des demandes d'attribution de fréquence et le résultat de la procédure d'allocation des blocs de 50 MHz, attribués sous réserve de l'achèvement de la procédure définitive. Puis, par une nouvelle décision du 20 octobre 2020, elle a rendu compte de la poursuite du processus et alloué de manière définitive l'ensemble des blocs de fréquence aux quatre entreprises qui en avaient sollicité l'attribution. Les associations Priartem et Agir pour l'environnement en ont demandé l'annulation et sous le n° 446301 demandent leur suspension sur le fondement de l'article L. 521-1 du code de justice administrative. <br/>
<br/>
              5. Par quatre décisions de l'ARCEP du 12 novembre 2020, les quatre entreprises ont été autorisées à exploiter les fréquences attribuées dans le but de déployer un réseau radioélectrique mobile ouvert au public. Les deux mêmes associations ont demandé l'annulation de ces décisions et sous le n° 446495 en demandent la suspension sur le fondement de l'article L. 521-1 du code de justice administrative. <br/>
<br/>
              6. L'urgence dont se prévalent les deux associations résulterait selon elles de l'absence d'intérêt supérieur justifiant du déploiement de ces réseaux de cinquième génération, du caractère coûteux du déploiement, et de l'absence d'étude permettant d'établir la proportionnalité entre de potentielles atteintes à la santé et l'intérêt du déploiement autorisé. La généralité des motifs ainsi allégués, à supposer même qu'il reflète l'atteinte grave et immédiate à un intérêt que les associations défendent, n'apparaît pas constitutive d'une urgence pouvant justifier l'examen du sérieux des moyens allégués en vue de la suspension demandée. En outre, la 2e chambre de la section du contentieux sera en mesure d'inscrire les requêtes dirigées contre le décret et l'arrêté de 2019, actes sur lesquels les décisions critiquées sont en partie fondées, au rôle d'une séance de jugement de l'année 2020, permettant ainsi de juger rapidement la plupart des moyens de légalité soulevés à l'encontre de l'ensemble du processus. <br/>
<br/>
              7. Dès lors, faute que la condition d'urgence de l'article L. 521-1 du code de justice administrative soit établie, les requêtes des associations Priartem et Agir pour l'environnement ne peut qu'être rejetées, sans qu'il soit besoin d'examiner leur recevabilité au regard notamment de leur intérêt pour agir non plus que le caractère sérieux des moyens qu'elles allèguent ou le bien-fondé de la question préjudicielle qu'elles demandent d'adresser à la Cour de justice de l'Union européenne. Par voie de conséquence, leurs conclusions tendant à ce que l'Etat leur verse une somme d'argent sur le fondement de l'article L. 761-1 du code de justice administrative ne peuvent qu'être rejetées, l'Etat n'étant pas la partie perdante dans ces deux instances. <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : Les requêtes de l'association Priartem et de l'association Agir pour l'environnement sont rejetées. <br/>
Article 2 : La présente ordonnance sera adressée à l'association Priartem, première dénommée, ainsi qu'à l'Autorité de régulation des communications électroniques, des postes et de la distribution de la presse. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
