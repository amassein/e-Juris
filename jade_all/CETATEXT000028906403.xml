<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028906403</ID>
<ANCIEN_ID>JG_L_2013_07_000000359399</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/90/64/CETATEXT000028906403.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème SSJS, 05/07/2013, 359399, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-07-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>359399</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SPINOSI</AVOCATS>
<RAPPORTEUR>Mme Domitille Duval-Arnould</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:359399.20130705</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête sommaire et le mémoire complémentaire, enregistrés les 14 mai et 14 août 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société Chante France Développement, dont le siège est 507, place des Champs-Elysées BP 10079 à Evry Courcouronnes (91002), représentée par son gérant en exercice domicilié... ; la société Chante France Développement demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision du 31 janvier 2012 par laquelle le Conseil supérieur de l'audiovisuel a rejeté sa candidature en vue de l'exploitation du service de radiodiffusion sonore par voie hertzienne dénommé "Chante France" dans la zone du Mans ;<br/>
<br/>
              2°) d'enjoindre au Conseil supérieur de l'audiovisuel de réexaminer sa candidature dans un délai d'un mois à compter de la notification de la décision à intervenir ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement d'une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu la loi n° 86-1067 du 30 septembre 1986 ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Domitille Duval-Arnould, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Spinosi, avocat de la société Chante France Développement ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier que, par une décision n° 2011-234 du 11 mai 2011, le Conseil supérieur de l'audiovisuel a lancé un appel aux candidatures en vue de l'attribution de fréquences radiophoniques dans le ressort du comité technique radiophonique de Caen ; que le conseil supérieur a délibéré le 31 janvier 2012 sur l'attribution des fréquences disponibles dans la zone du Mans ; que la société Chante France Développement, qui s'était portée candidate en vue de la diffusion du service " Chante France ", demande l'annulation de la décision rejetant sa candidature ;<br/>
<br/>
              Sur la légalité externe de la décision attaquée :<br/>
<br/>
              2. Considérant qu'aux termes de l'article 32 de la loi du 30 septembre 1986 : " Les refus d'autorisation sont notifiés aux candidats et motivés (...) " ; que la lettre du président du Conseil supérieur de l'audiovisuel notifiant à la société requérante la décision attaquée est accompagnée d'un extrait du procès-verbal de la séance plénière du 31 janvier 2012 qui énonce les critères fixés à l'article 29 de la loi du 30 septembre 1986 modifiée dont il a été fait application, notamment les impératifs prioritaires que sont la sauvegarde du pluralisme des courants d'expression socioculturels et la diversification des opérateurs, ainsi que les motifs de fait pour lesquels le Conseil supérieur de l'audiovisuel a préféré la candidature de RTL2 Le Mans à celle de la société Chante France Développement ; que le moyen tiré de l'insuffisance de la motivation de la décision attaquée doit par suite être écarté ;<br/>
<br/>
              Sur la légalité interne de la décision attaquée : <br/>
<br/>
              3. Considérant, d'une part, qu'aux termes de l'article 29 de la loi du 30 septembre 1986 relative à la communication, modifiée : " Le Conseil supérieur de l'audiovisuel accorde les autorisations en appréciant l'intérêt de chaque projet pour le public, au regard des impératifs prioritaires que sont la sauvegarde du pluralisme des courants d'expression socioculturels, la diversification des opérateurs, et la nécessité d'éviter les abus de position dominante ainsi que les pratiques entravant le libre exercice de la concurrence...le conseil veille également au juste équilibre entre les réseaux nationaux de diffusion d'une part , et les services locaux, régionaux et thématiques indépendants, d'autre part " ;<br/>
<br/>
              4. Considérant, d'autre part, que par ses communiqués n° 34 du 29 août 1989 et n° 281 du 10 novembre 1994, le Conseil supérieur de l'audiovisuel, faisant usage des pouvoirs qu'il tient des dispositions de la loi du 30 septembre 1986, a déterminé cinq catégories de services en vue de l'appel à candidatures pour l'exploitation de services de radiodiffusion sonore par voie hertzienne terrestre ; que ces cinq catégories sont ainsi définies : services associatifs éligibles au fonds de soutien, mentionnés à l'article 80 de la loi du 30 septembre 1986 (catégorie A), services locaux ou régionaux indépendants ne diffusant pas de programme national identifié (catégorie B), services locaux ou régionaux diffusant le programme d'un réseau thématique à vocation nationale (catégorie C), services thématiques à vocation nationale (catégorie D) et services généralistes à vocation nationale (catégorie E) ;<br/>
<br/>
              5. Considérant que, dans la zone du Mans, le Conseil supérieur de l'audiovisuel a attribué l'unique fréquence disponible à la société RTL2 Le Mans, en catégorie C ; que pour écarter la candidature de la société requérante dans la catégorie D, il a apprécié l'intérêt pour le public et la spécificité des programmes musicaux proposés et estimé que le format du programme de Chante Fance, faisant une large part à la variété française, était déjà partiellement représenté par MFM Radio et France Bleu Maine précédemment autorisés dans la zone du Mans ; que, ce faisant, le conseil supérieur, qui a tenu compte non seulement du caractère francophone des chansons diffusées mais aussi du genre musical auquel elles se rattachent, n'a entaché sa décision ni d'erreur de droit ni d'erreur d'appréciation ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que la société requérante n'est pas fondée à demander l'annulation de la décision du Conseil supérieur de l'audiovisuel qu'elle attaque ; que, par suite, ses conclusions tendant à ce qu'il soit enjoint au conseil supérieur de réexaminer sa candidature et à ce qu'une somme soit mise à la charge de l'Etat en application des dispositions de l'article L. 761-1 du code de justice administrative ne sauraient être accueillies ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
		Article 1er : La requête de la société Chante France Développement est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la société Chante France Développement, au Conseil supérieur de l'audiovisuel et à la ministre de la culture et de la communication.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
