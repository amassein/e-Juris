<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036664246</ID>
<ANCIEN_ID>JG_L_2018_02_000000414829</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/66/42/CETATEXT000036664246.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 28/02/2018, 414829, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-02-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>414829</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>Mme Sara-Lou Gerber</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHS:2018:414829.20180228</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au juge des référés du tribunal administratif de Bordeaux, sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de la décision du 27 juillet 2017 par laquelle le recteur de l'académie de Bordeaux a rejeté sa demande d'inscription en première année de licence de sciences et techniques des activités physiques et sportives (STAPS) à l'université de Bordeaux pour l'année 2017/2018 et d'enjoindre au recteur de l'inscrire temporairement au sein de cette formation dans un délai de quinze jours, sous une astreinte de 200 euros par jour de retard. <br/>
<br/>
              Par une ordonnance n° 1703763 du 21 septembre 2017, le juge des référés du tribunal administratif a suspendu l'exécution de cette décision et a enjoint au recteur de l'académie de Bordeaux de procéder à l'inscription de Mme B...en première année de licence de STAPS dans l'attente qu'il soit statué au fond sur sa légalité.<br/>
<br/>
              Par un pourvoi, enregistré le 4 octobre 2017 au secrétariat du contentieux du Conseil d'Etat, la ministre de l'enseignement supérieur, de la recherche et de l'innovation demande au Conseil d'Etat d'annuler cette ordonnance. <br/>
<br/>
              La ministre de l'enseignement supérieur, de la recherche et de l'innovation soutient que l'ordonnance qu'elle attaque est entachée d'erreur de droit en ce qu'elle juge que le moyen tiré de l'illégalité des dispositions de la circulaire n° 2017-077 du 24 avril 2017 est propre à créer un doute sérieux quant à la légalité de la décision contestée. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'éducation ;<br/>
              - la circulaire n° 2017-077 du 24 avril 2017 de la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sara-Lou Gerber, auditeur,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de Mme B...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ;<br/>
<br/>
              2. Considérant qu'il résulte des énonciations de l'ordonnance attaquée que, pour ordonner la suspension de l'exécution de la décision du 27 juillet 2017 du recteur de l'académie de Bordeaux rejetant la demande d'inscription en première année de licence de sciences et techniques des activités physiques et sportives (STAPS) présentée par Mme B... et enjoindre au recteur de procéder à son inscription, le juge des référés du tribunal administratif de Bordeaux s'est fondé sur ce que le moyen tiré de l'illégalité de la circulaire du 24 avril 2017 de la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche, sur le fondement de laquelle a été prise la décision contestée, était propre à créer un doute sérieux quant à la légalité de celle-ci ;<br/>
<br/>
              3. Considérant que, par une décision n° 410561, 410641, 411913 du 22 décembre 2017, le Conseil d'Etat, statuant au contentieux, a annulé cette circulaire du 24 avril 2017 ; que la ministre de l'enseignement supérieur, de la recherche et de l'innovation n'est, par suite, pas fondée à soutenir que l'ordonnance qu'elle attaque est entachée d'erreur de droit ; que son pourvoi doit être rejeté ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de faire application des dispositions de l'article L. 761-1 du code de justice administrative et de mettre à la charge de l'Etat une somme de 2 000 euros à verser à Mme B...au titre des frais exposés par elle et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la ministre de l'enseignement supérieur, de la recherche et de l'innovation est rejeté.<br/>
Article 2 : L'Etat versera à Mme B...une somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la ministre de l'enseignement supérieur, de la recherche et de l'innovation et à Mme A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
