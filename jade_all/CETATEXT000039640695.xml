<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039640695</ID>
<ANCIEN_ID>JG_L_2019_12_000000399794</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/64/06/CETATEXT000039640695.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 18/12/2019, 399794, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-12-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>399794</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LESOURD</AVOCATS>
<RAPPORTEUR>M. Aurélien Caron</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:399794.20191218</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une décision du 13 avril 2018, le Conseil d'Etat, statuant au contentieux sur le pourvoi de la société UPM France tendant à l'annulation de l'arrêt n° 13VE02953 du 15 mars 2016 de la cour administrative d'appel de Versailles ayant rejeté sa demande d'indemnisation du préjudice qu'elle estime avoir subi du fait du retard mis par l'Etat à transposer la directive n° 2003/96/CE du Conseil du 27 octobre 2003 restructurant le cadre communautaire de taxation des produits énergétiques et de l'électricité, a sursis à statuer jusqu'à ce que la Cour de justice de l'Union européenne se soit prononcée sur les questions suivantes :<br/>
              " 1- les dispositions du troisième alinéa du paragraphe 5 de l'article 21 de la directive doivent-elles être interprétées en ce sens que l'exonération dont elles autorisent les Etats membres à faire bénéficier les petits producteurs d'électricité, pour autant qu'ils taxent les produits énergétiques utilisés pour produire cette électricité, peut résulter d'une situation, telle que celle qui a été décrite au point 7 de la présente décision pour la période antérieure au 1er janvier 2011, pendant laquelle la France, comme l'y autorisait la directive, n'avait pas encore instauré la taxe intérieure sur la consommation finale d'électricité ni, par voie de conséquence, d'exonération de cette taxe en faveur des petits producteurs '<br/>
              2- en cas de réponse positive à la première question, comment les dispositions du a) du paragraphe 1 de l'article 14 de la directive et celles du troisième alinéa du paragraphe 5 de son article 21 pour les petits producteurs qui consomment l'électricité qu'ils produisent pour les besoins de leur activité doivent-elles être combinées ' Notamment, impliquent-elles une taxation minimale résultant soit de la taxation de l'électricité produite avec exonération du gaz naturel utilisé, soit d'une exonération de taxe sur la production d'électricité, l'Etat étant alors tenu de taxer le gaz naturel utilisé ' ".<br/>
<br/>
              Par un arrêt C-270/18 du 16 octobre 2019, la Cour de justice de l'Union européenne s'est prononcée sur cette question. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - la directive 2003/96/CE du Conseil du 27 octobre 2003 ;<br/>
              - la loi n° 2010-1488 du 7 décembre 2010 ;<br/>
              - le code des douanes ;<br/>
              - l'arrêt C-31/17 du 7 mars 2018 de la Cour de justice de l'Union européenne ;<br/>
              - l'arrêt C-270/18 du 16 octobre 2019 de la Cour de justice de l'Union européenne ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Aurélien Caron, auditeur,<br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lesourd, avocat de la société UPM France ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société UPM France exploite, pour les besoins de son activité de fabrication de papier, une installation de cogénération de chaleur et d'électricité, pour laquelle elle utilise du gaz naturel comme combustible. Le gaz qui lui a été livré entre le 1er janvier 2004 et le 1er avril 2008 a été soumis par son fournisseur, qui en a acquitté le montant, à la taxe intérieure de consommation de gaz naturel prévue à l'article 266 quinquies du code des douanes. Estimant que la fraction de ces livraisons consommée pour produire de l'électricité aurait dû être exonérée de cette taxe conformément aux prévisions de l'article 14 de la directive du Conseil du 27 octobre 2003 restructurant le cadre communautaire de taxation des produits énergétiques et de l'électricité, elle a demandé le remboursement de la taxe ainsi supportée, ainsi que l'indemnisation du préjudice qu'elle estime avoir subi du fait du retard de l'Etat à transposer cette directive. Elle se pourvoit en cassation contre l'arrêt du 15 mars 2016 par lequel la cour administrative d'appel de Versailles a rejeté son appel contre le jugement du tribunal administratif de Cergy-Pontoise qui, après avoir constaté un non-lieu partiel à statuer à concurrence des sommes remboursées au titre de la période du 1er janvier 2007 au 31 mars 2008, a rejeté le surplus de sa demande d'indemnisation.<br/>
<br/>
              2. L'article 14 de la directive du Conseil du 27 octobre 2003 restructurant le cadre communautaire de taxation des produits énergétiques et de l'électricité prévoit, à son paragraphe 1, que " (...) sans préjudice d'autres dispositions communautaires, les États membres exonèrent les produits suivants de la taxation, selon les conditions qu'ils fixent en vue d'assurer l'application correcte et claire de ces exonérations et d'empêcher la fraude, l'évasion ou les abus : / a) les produits énergétiques et l'électricité utilisés pour produire de l'électricité et l'électricité utilisée pour maintenir la capacité de produire de l'électricité. Toutefois, les États membres peuvent taxer ces produits pour des raisons ayant trait à la protection de l'environnement et sans avoir à respecter les niveaux minima de taxation prévus par la présente directive. (...) ". Aux termes de l'article 15 de cette directive : " 1. Sans préjudice d'autres dispositions communautaires, les États membres peuvent appliquer sous contrôle fiscal des exonérations totales ou partielles ou des réductions du niveau de taxation : / (...) c) aux produits énergétiques et à l'électricité utilisés pour la production combinée de chaleur et d'énergie ; (...) ". Le troisième alinéa du paragraphe 5 de son article 21 prévoit en outre que : " Une entité qui produit de l'électricité pour son propre usage est considérée comme un distributeur. Nonobstant les dispositions de l'article 14, paragraphe 1, point a), les États membres peuvent exonérer les petits producteurs d'électricité, pour autant qu'ils taxent les produits énergétiques utilisés pour produire cette électricité. ". Enfin, aux termes du second alinéa du paragraphe 10 de son article 18 : " La République française peut appliquer une période transitoire allant jusqu'au 1er janvier 2009 pour adapter son système actuel de taxation de l'électricité aux dispositions prévues dans la présente directive. Jusqu'à cette date, la moyenne du niveau global de la taxation locale actuelle de l'électricité est prise en compte pour évaluer le respect des taux minima fixés dans la présente directive ".<br/>
<br/>
              3. Par la décision du 13 avril 2018 visée ci-dessus, le Conseil d'Etat, statuant au contentieux, a jugé que la cour administrative d'appel de Versailles a commis une erreur de droit en jugeant que le gaz naturel utilisé par la société UPM France dans son installation de cogénération relevait exclusivement des dispositions de l'article 15 de la directive 2003/96/CE du Conseil du 27 octobre 2003 dans la mesure où l'exonération obligatoire prévue par l'article 14, paragraphe 1, sous a), de cette directive s'applique aux produits énergétiques utilisés pour la production d'électricité lorsque ces produits sont utilisés pour la production combinée d'électricité et de chaleur, au sens de l'article 15, paragraphe 1, sous c) de cette directive. Il a ensuite sursis à statuer sur une demande de substitution de motifs présentée par le ministre de l'action et des comptes publics dans l'attente de la réponse aux questions préjudicielles qu'il a renvoyées à la Cour de Justice de l'Union européenne quant à la portée de l'article 21, paragraphe 5, troisième alinéa de cette directive et sa combinaison avec son article 14, paragraphe 1, sous a).<br/>
<br/>
              4. Dans l'arrêt du 16 octobre 2019 par lequel elle s'est prononcée sur la question dont le Conseil d'Etat statuant au contentieux l'avait saisie à titre préjudiciel, la Cour de justice de l'Union européenne a dit pour droit que l'article 21, paragraphe 5, troisième alinéa, seconde phrase, de la directive 2003/96/CE du Conseil, du 27 octobre 2003, restructurant le cadre communautaire de taxation des produits énergétiques et de l'électricité, doit être interprété en ce sens que l'exonération que prévoit cette disposition pour les petits producteurs d'électricité, pour autant que, par dérogation à l'article 14, paragraphe 1, sous a), de cette directive, les produits énergétiques utilisés pour produire cette électricité soient taxés, ne pouvait être appliquée par la République française durant la période transitoire qui lui était accordée, conformément à l'article 18, paragraphe 10, second alinéa, de ladite directive, jusqu'au 1er janvier 2009 et pendant laquelle cet État membre n'a pas instauré le système de taxation de l'électricité prévu par la même directive. Dans les motifs de son arrêt (point 51), elle a précisé que, durant cette période transitoire, les dispositions relatives à l'exonération des produits énergétiques utilisés pour produire de l'électricité prévues par cette directive étaient pleinement applicables à la République française.<br/>
<br/>
              5. Il résulte de l'interprétation ainsi donnée par la Cour de justice de l'Union européenne que les produits énergétiques utilisés pour produire de l'électricité bénéficiaient, au cours de la période transitoire visée par l'article 18, paragraphe 10, second alinéa, de la directive, de l'exonération prévue par son article 14, paragraphe 1, sous a), sans qu'y fasse obstacle, le cas échéant, la circonstance que l'électricité produite ne fasse l'objet d'aucune taxation. Par suite, le ministre de l'action et des comptes publics n'est pas fondé à demander que soit substitué au motif erroné retenu par la cour, exposé au point 3, le motif tiré de ce que les objectifs de la directive 2003/96/CE subordonneraient l'exonération des produits énergétiques utilisés pour produire de l'électricité à la taxation de l'électricité produite.<br/>
<br/>
              6. Il résulte de ce qui qui précède que la société UPM France est fondée à demander l'annulation de l'arrêt qu'elle attaque. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 5 000 euros à verser à la société UPM France au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt n° 13VE02953 du 15 mars 2016 de la cour administrative de Versailles est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Versailles. <br/>
<br/>
Article 3 : L'Etat versera la somme de 5 000 euros à la société UPM France au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la société UPM France et au ministre de l'action et des comptes publics. <br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
