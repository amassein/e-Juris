<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034600541</ID>
<ANCIEN_ID>JG_L_2017_05_000000402723</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/60/05/CETATEXT000034600541.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 05/05/2017, 402723, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-05-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>402723</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>Mme Manon Perrière</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:402723.20170505</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société par actions simplifiée France Routage a demandé au tribunal administratif de Melun de prononcer, d'une part, la décharge de la cotisation foncière des entreprises, de la taxe pour frais de chambres de commerce et d'industrie, de la cotisation sur la valeur ajoutée des entreprises et de la taxe additionnelle à cette cotisation auxquelles elle a été assujettie au titre des années 2011 et 2012, pour un montant de 450 702 euros et, d'autre part, la décharge de la cotisation foncière des entreprises, de la taxe pour frais de chambres de commerce et d'industrie, de la cotisation sur la valeur ajoutée des entreprises et de la taxe additionnelle à cette cotisation auxquelles elle a été assujettie à raison de son établissement situé à Croissy-Beaubourg au titre de l'année 2011, pour un montant de 73 548 euros. Par un jugement n° 1305045, 1305046 du 8 janvier 2015, le tribunal administratif de Melun a rejeté ses demandes. <br/>
<br/>
              Par un arrêt n° 15PA01028 du 21 juin 2016, la cour administrative d'appel de Paris a rejeté l'appel formé par la société France Routage contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 22 août et 22 novembre 2016 au secrétariat du contentieux du Conseil d'Etat, la société demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Par un mémoire, enregistré le 14 février 2017 au secrétariat du contentieux du Conseil d'Etat, présenté en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958, la société France Routage demande au Conseil d'Etat, à l'appui de son pourvoi tendant à l'annulation de l'arrêt n° 15PA01208 du 21 juin 2016 de la cour administrative d'appel de Paris, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du 1° de l'article 1458 du code général des impôts. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
               - la Constitution, notamment son Préambule et son article 61-1 ; <br/>
               - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Manon Perrière, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Soltner, Texidor, Perier, avocat de la société France Routage.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              Sur la question prioritaire de constitutionnalité :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé, y compris pour la première fois en cassation, à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et qu'elle soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. Aux termes des deux premiers alinéas de l'article 1458 du code général des impôts : " Sont exonérés de la cotisation foncière des entreprises : / 1° Les éditeurs de feuilles périodiques et les sociétés dont ils détiennent majoritairement le capital et auxquelles ils confient l'exécution d'opérations de groupage et de distribution ". La société requérante conteste la conformité aux droits et libertés garantis par la Constitution de ces dispositions. Elle soutient qu'en instaurant une différence de traitement, au détriment des sociétés de brochage et de groupage dont le capital n'est pas détenu majoritairement par des éditeurs de feuilles périodiques, qui n'est pas en rapport avec leur objet, ces dispositions, telles qu'interprétées par la jurisprudence, méconnaissent le principe d'égalité devant l'impôt et le principe d'égalité devant les charges publiques, consacrés respectivement aux articles 6 et 13 de la Déclaration des droits de l'homme et du citoyen.<br/>
<br/>
              3. Les dispositions du 1° de l'article 1458 du code général des impôts ont pour objet d'exonérer de cotisation foncière des entreprises les éditeurs de feuilles périodiques ainsi que les sociétés dont ces éditeurs détiennent majoritairement le capital et auxquelles ils confient l'exécution d'opérations de groupage et de distribution. D'une part, les activités de brochage et de routage ne peuvent être regardées, prises isolément, comme des activités d'édition au sens de ces dispositions. D'autre part, les dispositions précitées n'intègrent pas dans le champ de l'exonération les sociétés de groupage et de distribution qui seraient contrôlées majoritairement par des sociétés d'édition. Il en résulte que les sociétés qui exercent uniquement des activités de brochage et de routage sont exclues  du bénéfice de cette exonération qu'elles soient ou non contrôlées majoritairement par un éditeur. Ainsi, le moyen tiré de ce que les dispositions litigieuses créeraient une rupture d'égalité entre entreprises de brochage et de routage selon qu'elles seraient ou non contrôlées majoritairement par un éditeur ne peut qu'être écarté. <br/>
<br/>
              4. Il suit de là qu'il n'y a pas lieu de renvoyer la question prioritaire de constitutionnalité soulevée au Conseil constitutionnel.<br/>
<br/>
              Sur les moyens soulevés à l'appui du pourvoi :  <br/>
<br/>
               5. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
<br/>
              6. Pour demander l'annulation de l'arrêt qu'elle attaque, la société requérante soutient que la cour administrative d'appel de Paris a commis une erreur de droit et une erreur de qualification juridique des faits en jugeant qu'elle n'entrait pas dans le champ d'application de l'exonération de la cotisation foncière des entreprises prévue par les dispositions du 1° de l'article 1458 du code général des impôts, alors qu'elle exerce une activité économique qui se situe dans le prolongement normal de l'activité d'édition de feuilles périodiques.<br/>
<br/>
              7. Ces moyens ne sont pas de nature à permettre l'admission du pourvoi.<br/>
<br/>
<br/>
<br/>                D E C I D E :<br/>
                               --------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la société France Routage.<br/>
<br/>
Article 2 : Le pourvoi de la société France Routage n'est pas admis.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la société France Routage, au Premier ministre et au ministre de l'économie et des finances.<br/>
Copie en sera adressée au Conseil constitutionnel.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
