<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000020418454</ID>
<ANCIEN_ID>J4_L_2007_03_000000601927</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/20/41/84/CETATEXT000020418454.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Cour Administrative d'Appel de Nantes, 2ème Chambre, 28/03/2007, 06NT01927, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2007-03-28</DATE_DEC>
<JURIDICTION>Cour Administrative d'Appel de Nantes</JURIDICTION>
<NUMERO>06NT01927</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème Chambre</FORMATION>
<TYPE_REC>exécution décision justice adm</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. DUPUY</PRESIDENT>
<AVOCATS>BROSSARD</AVOCATS>
<RAPPORTEUR>M. Roger-Christian  DUPUY</RAPPORTEUR>
<COMMISSAIRE_GVT>M. ARTUS</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance du 10 novembre 2006 par laquelle le président de la Cour administrative de Nantes a décidé l'ouverture de la procédure juridictionnelle prévue à l'article R. 921-6 du code de justice administrative sur la demande présentée par M. et Mme X ;<br/>
<br/>
       Vu la demande enregistrée le 7 février 2006, présentée pour M. et Mme X, demeurant ..., par Me Beucher, avocat au barreau d'Angers ; M. et Mme X demandent à la Cour :<br/>
<br/>
       1°) d'assurer l'exécution du jugement n° 03-620 du 2 juin 2005 par lequel le Tribunal administratif de Nantes a annulé l'arrêté du 23 décembre 2002 du président de la communauté d'agglomération du Grand Angers décidant d'exercer le droit de préemption urbain sur une maison d'habitation située 10, chemin des Chalets sur le territoire de la ville d'Angers (Maine-et-Loire) dont ils s'étaient portés acquéreurs ;<br/>
<br/>
       2°) de condamner la communauté d'agglomération Angers Loire Métropole au paiement d'une astreinte de 500 euros par jour de retard jusqu'à l'exécution du jugement susvisé ;<br/>
<br/>
       3°) de condamner la communauté d'agglomération Angers Loire Métropole à lui verser une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
....................................................................................................................<br/>
<br/>
<br/>
Vu les autres pièces du dossier ;<br/>
<br/>
       Vu le code de l'urbanisme ;<br/>
<br/>
       Vu le code de justice administrative ;<br/>
<br/>
       Les parties ayant été régulièrement averties du jour de l'audience ;<br/>
<br/>
       Après avoir entendu au cours de l'audience publique du 13 mars 2007 :<br/>
<br/>
       - le rapport de M. Dupuy, président-rapporteur ;<br/>
<br/>
       - les observations de Me Beucher, avocat de M. et Mme X ;<br/>
<br/>
       - les observations de Me Brossard, avocat de la communauté d'agglomération Angers Loire Métropole ;<br/>
<br/>
       - et les conclusions de M. Artus, commissaire du gouvernement ;<br/>
<br/>
<br/>
<br/>
       Considérant qu'aux termes de l'article L. 911-4 du code de justice administrative : &#147;En cas d'inexécution d'un jugement ou d'un arrêt, la partie intéressée peut demander au tribunal administratif ou à la cour administrative d'appel qui a rendu la décision d'en assurer l'exécution. Toutefois, en cas d'inexécution d'un jugement frappé d'appel, la demande d'exécution est adressée au juge d'appel. Si le jugement ou l'arrêt dont l'exécution est demandée n'a pas défini les mesures d'exécution, la juridiction saisie procède à cette définition. Elle peut fixer un délai d'exécution et prononcer une astreinte (...)&#148; ; que selon l'article R. 921-6 du même code : &#147;Dans le cas où le président estime nécessaire de prescrire des mesures d'exécution par voie juridictionnelle, et notamment de prononcer une astreinte, ou lorsque le demandeur le sollicite dans le mois qui suit la notification du classement décidé en vertu du dernier alinéa de l'article précédent et, en tout état de cause, à l'expiration du délai de six mois à compter de sa saisine, le président de la cour ou du tribunal ouvre par ordonnance une procédure juridictionnelle. Cette ordonnance n'est pas susceptible de recours. L'affaire est instruite et jugée d'urgence. Lorsqu'elle prononce une astreinte, la formation en fixe la date d'effet.&#148; ;<br/>
<br/>
       Considérant que M. et Mme X demandent à la Cour d'assurer l'exécution du jugement du 2 juin 2005 par lequel le Tribunal administratif de Nantes a prononcé l'annulation de l'arrêté du 23 décembre 2002 du président de la communauté d'agglomération du grand Angers décidant d'exercer le droit de préemption urbain sur un immeuble situé 10, chemin des Chalets à Angers dont les intéressés s'étaient portés acquéreurs ; que l'illégalité de cette décision de préemption a été confirmée par un arrêt du 30 juin 2006 de la Cour rejetant la requête d'appel de la communauté d'agglomération Angers Loire Métropole ;<br/>
<br/>
       Considérant que la circonstance que M. et Mme X avaient, dans leur mémoire en défense produit dans le cadre de l'instance d'appel introduite par la communauté d'agglomération contre le jugement précité du 2 juin 2005, manifesté leur intention d'obtenir l'exécution de ce jugement, s'avère sans incidence sur la recevabilité de la présente demande d'exécution dont ils ont saisi le président de la Cour par requête distincte en application des dispositions précitées, alors, en tout état de cause, qu'il est constant que de telles conclusions n'ont jamais été l'objet d'une décision de la part du juge d'appel ;<br/>
       Considérant que l'annulation, par le juge de l'excès de pouvoir, de l'acte par lequel le titulaire du droit de préemption décide d'exercer ce droit emporte pour conséquence que ce titulaire doit être regardé comme n'ayant jamais décidé de préempter ; qu'ainsi, cette annulation implique nécessairement, sauf atteinte excessive à l'intérêt général apprécié au regard de l'ensemble des intérêts en présence, que le titulaire du droit de préemption, s'il n'a pas entre temps cédé le bien illégalement préempté, prenne toute mesure afin de mettre fin aux effets de la décision annulée ; qu'il lui appartient à cet égard, et avant toute autre mesure, de s'abstenir de revendre à un tiers le bien illégalement préempté ; qu'il doit, en outre, proposer à l'acquéreur évincé puis, le cas échéant, au propriétaire initial, d'acquérir le bien et ce, à un prix visant à rétablir, autant que possible et sans enrichissement sans cause de l'une quelconque des parties, les conditions de la transaction à laquelle l'exercice du droit de préemption a fait obstacle ;<br/>
<br/>
       Considérant que, lorsque le juge administratif est saisi, sur le fondement des dispositions précitées de l'article L. 911-4 du code de justice administrative, de conclusions tendant à ce qu'il assure l'exécution de la décision juridictionnelle annulant la décision de préemption, il lui appartient, après avoir vérifié, au regard de l'ensemble des intérêts en présence, que le rétablissement de la situation initiale ne porte pas une atteinte excessive à l'intérêt général, de prescrire à l'auteur de la décision annulée de prendre les mesures ci-dessus définies dans la limite des conclusions dont il est saisi ;<br/>
<br/>
       Considérant qu'il résulte de l'instruction que si la ville d'Angers a fait réaliser des études en vue de l'implantation d'une zone d'aménagement concerté dans le secteur dit &#147;des Capucins&#148; dont le périmètre englobe le bien illégalement préempté situé 10, chemin des Chalets à Angers, ce projet, d'une part, qui n'a été soumis jusqu'ici qu'à la procédure d'enquête préalable à la déclaration d'utilité publique, ne peut être regardé comme définitivement arrêté, d'autre part, localise le bien en cause dans une zone destinée aux espaces verts dans laquelle il est prévu de conserver quelques bâtiments existants ; qu'il s'ensuit que le rétablissement de la situation initiale n'est de nature à emporter aucune atteinte excessive à l'intérêt général et, dès lors, qu'il y a lieu de prescrire à la communauté d'agglomération Angers Loire Métropole qu'elle propose la rétrocession du bien illégalement préempté à M. et Mme X, qui soutiennent ne pas pouvoir trouver de bien équivalent au triple plan de la localisation, de la consistance et du prix, et ce aux conditions fixées dans la déclaration d'intention d'aliéner ; qu'il y a lieu, également, compte-tenu des circonstances de l'affaire, d'assortir cette injonction, à défaut pour la communauté d'agglomération Angers Loire Métropole de justifier de l'exécution de cette mesure dans un délai d'un mois à compter de la notification du présent arrêt, d'une astreinte de 250 euros par jour de retard ;<br/>
       Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
       Considérant qu'il y a lieu, en application de ces dispositions, de condamner la communauté d'agglomération Angers Loire Métropole à verser à M. et Mme X la somme de 1 500 euros qu'ils demandent au titre des frais exposés et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>
<br/>
DÉCIDE :<br/>
<br/>
Article 1er :	Il est enjoint à la communauté d'agglomération Angers Loire Métropole de proposer à M. et Mme X l'acquisition du bien illégalement préempté situé 10, chemin des Chalets à Angers, aux conditions fixées dans la déclaration d'intention d'aliéner émanant du propriétaire initial.<br/>
Article 2 :	Une astreinte de 250 euros (deux cent cinquante euros) par jour de retard est prononcée à l'encontre de la communauté d'agglomération Angers Loire Métropole si elle ne justifie pas, dans le délai d'un mois suivant la notification du présent arrêt, s'être acquittée de ces prescriptions.<br/>
Article 3 :	La communauté d'agglomération Angers Loire Métropole versera une somme de 1 500 euros (mille cinq cents euros) à M. et Mme X au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 :	Le présent arrêt sera notifié à M. et Mme X, à la communauté d'agglomération Angers Loire Métropole, à Mme Marie-Louise BELANGER et à Mme Marcelle BELANGER. <br/>
           Une copie sera, en outre, adressée au ministre des transports, de l'équipement, du tourisme et de la mer.<br/>
<br/>
<br/>
N° 06NT01927<br/>
2<br/>
1<br/>
<br/>
3<br/>
1<br/>
<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
