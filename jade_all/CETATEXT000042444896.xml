<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042444896</ID>
<ANCIEN_ID>JG_L_2020_10_000000437714</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/44/48/CETATEXT000042444896.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 19/10/2020, 437714, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-10-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>437714</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Isabelle Lemesle</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:437714.20201019</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
     La Commission nationale des comptes de campagne et des financements politiques (CNCCFP) a, le 17 janvier 2020, saisi le Conseil d'État en application de l'article L. 52-15 du code électoral, sur le fondement de sa décision du 2 décembre 2019 rejetant le compte de campagne de M. A... B..., candidat tête de liste à l'élection qui s'est déroulée le 12 mai 2019 en vue de la désignation des membres du congrès et des assemblées provinciales de Nouvelle-Calédonie.<br/>
<br/>
     La saisine a été communiquée à M. B..., qui n'a pas produit de mémoire en défense, ainsi qu'à la province Sud de Nouvelle-Calédonie et au ministre des outre-mer, qui n'ont pas produit d'observations.<br/>
<br/>
<br/>
<br/>
     Vu les autres pièces du dossier ;<br/>
<br/>
     Vu :<br/>
     - La loi organique n°99-209 du 19 mars 1999 ;<br/>
     - le code électoral ;<br/>
     - le code de justice administrative ;	<br/>
<br/>
<br/>
<br/>
<br/>
     Après avoir entendu en séance publique :<br/>
<br/>
     - le rapport de Mme Isabelle Lemesle, conseiller d'Etat,  <br/>
<br/>
     - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. L'article L. 52-12 du code électoral dispose : " Chaque candidat ou candidat tête de liste soumis au plafonnement prévu à l'article L. 52-11 et qui a obtenu au moins 1 % des suffrages exprimés est tenu d'établir un compte de campagne retraçant, selon leur origine, l'ensemble des recettes perçues et, selon leur nature, l'ensemble des dépenses engagées ou effectuées en vue de l'élection, hors celles de la campagne officielle par lui-même ou pour son compte, au cours de la période mentionnée à l'article L. 52-4 (...)/ Au plus tard avant 18 heures le dixième vendredi suivant le premier tour de scrutin, chaque candidat ou candidat tête de liste présent au premier tour dépose à la Commission nationale des comptes de campagne et des financements politiques son compte de campagne et ses annexes accompagné des justificatifs de ses recettes, notamment d'une copie des contrats de prêts conclus en application de l'article L. 52-7-1 du présent code, ainsi que des factures, devis et autres documents de nature à établir le montant des dépenses payées ou engagées par le candidat ou pour son compte. Le compte de campagne est présenté par un membre de l'ordre des experts-comptables et des comptables agréés ; celui-ci met le compte de campagne en état d'examen et s'assure de la présence des pièces justificatives requises. Cette présentation n'est pas nécessaire lorsque aucune dépense ou recette ne figure au compte de campagne. Dans ce cas, le mandataire établit une attestation d'absence de dépense et de recette. Cette présentation n'est pas non plus nécessaire lorsque le candidat ou la liste dont il est tête de liste a obtenu moins de 1 % des suffrages exprimés et qu'il n'a pas bénéficié de dons de personnes physiques selon les modalités prévues à l'article 200 du code général des impôts (...) ". Aux termes de l'article L. 52-15 du même code : " La Commission nationale des comptes de campagne et des financements politiques approuve et, après procédure contradictoire, rejette ou réforme les comptes de campagne. (...) / Lorsque la commission a constaté que le compte de campagne n'a pas été déposé dans le délai prescrit, si le compte a été rejeté ou si, le cas échéant après réformation, il fait apparaître un dépassement du plafond des dépenses électorales, la commission saisit le juge de l'élection (...) ". Aux termes de l'article L. 118-3 du même code : " Saisi par la commission instituée par l'article L. 52-14, le juge de l'élection peut prononcer l'inéligibilité du candidat (...) qui n'a pas déposé son compte de campagne dans les conditions et le délai prescrits à l'article L. 52-12 (...) / L'inéligibilité (...) est prononcée pour une durée maximale de trois ans et s'applique à toutes les élections. Toutefois, elle n'a pas d'effet sur les mandats acquis antérieurement à la date de la décision (...) ". <br/>
<br/>
              2. Il résulte de ces dispositions que le candidat tête de liste qui a recueilli au moins 1% des suffrages exprimés est tenu, dans le délai prescrit, de déposer auprès de la Commission nationale des comptes de campagne et des financements politiques (CNCCFP) son compte de campagne visé par un membre de l'ordre des experts-comptables et des comptables agréés ou à défaut de produire un compte de campagne signé par ses soins et accompagné d'une attestation d'absence de dépenses et de recettes établie par son mandataire. Le manquement à l'obligation de déposer un compte de campagne est constitué à la date à laquelle expire le délai imparti au candidat pour procéder à ce dépôt, lequel est impératif et ne peut être prorogé. En l'espèce, le délai imparti aux candidats à l'élection qui s'est déroulée le 12 mai 2019 en vue de la désignation des membres du congrès et des assemblées provinciales de Nouvelle-Calédonie pour déposer leur compte de campagne expirait le 19 juillet 2019 à 18 heures.<br/>
<br/>
              3. Il appartient au juge de l'élection, pour apprécier s'il y a lieu de faire usage de la faculté donnée par les dispositions précitées de l'article L. 118-3 du code électoral de déclarer inéligible un candidat qui n'a pas déposé son compte de campagne, de tenir compte, eu égard à la nature de la règle méconnue, du caractère délibéré ou non du manquement ainsi que de l'ensemble des circonstances de l'espèce.<br/>
<br/>
              4. Il résulte de l'instruction que la liste " Destin commun calédonien " conduite par M. B... a recueilli 1 667 voix sur 70 959 suffrages exprimés en Province Sud lors de l'élection qui s'est déroulée le 12 mai 2019 en vue de la désignation des membres du congrès et des assemblées provinciales de Nouvelle-Calédonie, soit 2,35 % des suffrages exprimés. Il est constant qu'il n'a pas déposé de compte de campagne et qu'il n'a pas davantage produit d'attestation d'absence de dépense et de recette établie par son mandataire financier. Ce faisant, M. B... a méconnu une obligation substantielle, ce qui justifie le rejet de son compte de campagne. En l'absence de tout élément de nature à justifier la méconnaissance de cette obligation, il y a lieu, en application de l'article L. 118-3 du code électoral, de déclarer M. B... inéligible pendant dix-huit mois à compter de la date de la présente décision.<br/>
<br/>
<br/>
<br/>
<br/>
     D E C I D E :<br/>
     --------------<br/>
<br/>
Article 1er : M. B... est déclaré inéligible pour une durée de 18 mois à compter de la présente décision.<br/>
Article 2 : La présente décision sera notifiée à la Commission nationale des comptes de campagne et des financements politiques et à M. A... B....<br/>
Copie en sera adressée au ministre des outre-mer.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
