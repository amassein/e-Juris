<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027626006</ID>
<ANCIEN_ID>JG_L_2013_06_000000349280</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/62/60/CETATEXT000027626006.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  9ème sous-section jugeant seule, 28/06/2013, 349280, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-06-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>349280</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 9ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUIN-PALAT, BOUCARD</AVOCATS>
<RAPPORTEUR>M. Jean-Luc Matt</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:349280.20130628</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu, 1° sous le n° 349280, le pourvoi sommaire et le mémoire complémentaire, enregistrés les 16 mai et 11 août 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme C...A..., veuveB..., domiciliée..., au Sénégal ; Mme A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 08/00041 du 11 mars 2010 par lequel la cour régionale des pensions militaires de Paris a, sur appel du ministre de la défense, annulé le jugement n° 02/00153 du tribunal départemental des pensions militaires de Paris du 26 mars 2008 faisant droit à sa demande de décristallisation de la pension militaire d'invalidité en sa qualité de veuve de M. B...et condamnant l'Etat à lui verser les arrérages de la pension due depuis le 25 mai 1993 et rejeté cette demande comme irrecevable ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel du ministre ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros en application de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu, 2° sous le n° 349332, le pourvoi sommaire et le mémoire complémentaire, enregistrés les 16 mai et 11 août 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme C...A..., veuveB..., domiciliée..., au Sénégal ; Mme A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le même arrêt n° 08/00041 ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel du ministre; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 000 euros en application de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu le code des pensions civiles et militaires de retraite ;<br/>
<br/>
              Vu le code des pensions militaires d'invalidité et des victimes de la guerre ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Matt, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouin-Palat, Boucard et à la SCP Delaporte, Briard, Trichet, avocat de Mme A...;<br/>
<br/>
<br/>
<br/>1. Considérant que les pourvois de Mme A...sont dirigés contre le même arrêt ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              2. Considérant qu'en vertu de l'article L. 79 du code des pensions militaires d'invalidité et des victimes de la guerre, les juridictions des pensions ne sont compétentes que sur les contestations soulevées par l'application du livre Ier (à l'exception des chapitres I et IV du titre VII) et du livre II de ce code ; que les pensions mixtes de retraite et d'invalidité sont prévues par le code des pensions civiles et militaires de retraite ; qu'il s'ensuit que le contentieux de ces pensions relève de la compétence du juge administratif de droit commun, à l'exception, cependant, des questions relatives à l'existence, à l'origine médicale et au degré de l'invalidité, lesquelles doivent être tranchées par la juridiction des pensions ; qu'il en va de même pour les litiges relatifs à l'indemnité viagère annuelle prévue par l'article 71 de la loi du 26 décembre 1959 portant loi de finances pour 1960 lorsque cette indemnité remplace une pension mixte de retraite et d'invalidité ;<br/>
<br/>
              3. Considérant que la pension de veuve dont est titulaire Mme A... du chef de son époux décédé constitue une pension mixte de retraite et d'invalidité ; que la demande présentée par celle-ci devant la juridiction des pensions et tendant à ce que cette pension fasse l'objet d'une revalorisation dans les mêmes conditions que celles réservées aux pensionnés de nationalité française ne portait sur aucune des questions relevant de la juridiction des pensions ; qu'il en résulte qu'en n'annulant pas d'office pour incompétence le jugement du 26 mars 2008 du tribunal départemental des pensions de Paris statuant sur la demande de MmeA..., la cour régionale des pensions de Paris a entaché son arrêt d'erreur de droit ; qu'il doit, par suite, être annulé ;<br/>
<br/>
              4. Considérant que, dans les circonstances de l'espèce, il y a lieu de régler l'affaire au fond par application de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              5. Considérant qu'ainsi qu'il a été dit ci-dessus, le tribunal départemental des pensions de Paris n'était pas compétent pour statuer sur la demande de Mme A... ; que son jugement doit, par suite, être annulé ;<br/>
<br/>
              6. Considérant qu'en application de l'article R. 351-1 du code de justice administrative, il y a lieu d'attribuer le jugement de la demande de Mme A...au tribunal administratif de Paris, compétent pour en connaître en vertu de l'article R. 312-1 du même code ;<br/>
<br/>
              7. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de de faire droit aux conclusions présentées par Mme A...au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt de la cour régionale des pensions de Paris du 11 mars 2010 et le jugement du tribunal départemental des pensions de Paris du 26 mars 2008 sont annulés.<br/>
Article 2 : Le jugement de la demande de Mme A...est attribué au tribunal administratif de Paris.<br/>
Article 3 : Le surplus des conclusions des pourvois de Mme A...est rejeté.<br/>
Article 4 : La présente décision sera notifiée à Mme C...A..., veuveB..., au ministre de la défense et au président du tribunal administratif de Paris.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
